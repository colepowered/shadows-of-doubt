﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialOverrideController : MonoBehaviour
{
    //Footsteps/impacts will look for the presence of this for material data
    [Header("Override Properties")]
    [Range(0f, 1f)]
    public float concrete = 0f;
    [Range(0f, 1f)]
    public float plaster = 0f;
    [Range(0f, 1f)]
    public float wood = 1f;
    [Range(0f, 1f)]
    public float carpet = 0f;
    [Range(0f, 1f)]
    public float tile = 0f;
    [Range(0f, 1f)]
    public float metal = 0f;
    [Range(0f, 1f)]
    public float glass = 0f;
    [Range(0f, 1f)]
    public float fabric = 0f;
}
