﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using UnityEngine;
using UnityEngine.UI;

public class SoundIndicatorController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public JuiceController juice;
    public Image additionalGraphic;

    [Header("State")]
    [Tooltip("Check true if this is for footstep sounds, as it will simlate the surface being walked on...")]
    public bool isFootstep = false;
    [Tooltip("Keep this updated when checking for footsteps...")]
    public bool rightFoot = false;
    public AudioEvent currentEvent;
    private FMOD.Studio.EventDescription description;
    public List<AudioController.ActiveListener> currentListeners = new List<AudioController.ActiveListener>();
    public float currentHearingRange = 0f;
    public int currentIconCount = 0;
    private int previousIconCount = 0;
    public float colourLerp = 0f;
    public Color col = Color.white;
    public Vector2 iconOffset = Vector2.zero;

    [System.Serializable]
    public class AudioIcon
    {
        public RectTransform rect;
        public Image img;
        public float fadeIn = 0f;
        public bool remove = false;
    }

    public List<AudioIcon> spawnedIcons = new List<AudioIcon>();
    public List<AudioIcon> fullIcons = new List<AudioIcon>();

    public void SetSoundEvent(AudioEvent newEvent, bool updateEvent = true)
    {
        if(newEvent != currentEvent)
        {
            currentEvent = newEvent;

            if (updateEvent) UpdateCurrentEvent();
        }
    }

    public void UpdateCurrentEvent()
    {
        if (currentEvent == null)
        {
            currentIconCount = 0;
            previousIconCount = 0;
            this.enabled = true;
            return;
        }

        //Get surface data
        AudioController.SoundMaterialOverride materialData = null;

        if (isFootstep)
        {
            //Do we use a detailed check which uses a raycast to detect foot impact?
            Vector3 rayStart = Vector3.zero;

            if (rightFoot)
            {
                rayStart = Player.Instance.transform.TransformPoint(new Vector3(0.13f, 0.1f, 0f));
            }
            else
            {
                rayStart = Player.Instance.transform.TransformPoint(new Vector3(-0.13f, 0.1f, 0f));
            }

            RaycastHit hit;

            //Scan
            if (Physics.Raycast(rayStart, Vector3.down, out hit, 2.6f, AudioController.Instance.footstepLayerMask, QueryTriggerInteraction.Ignore))
            {
                //Look for override controllers first...
                MaterialOverrideController moc = hit.transform.GetComponent<MaterialOverrideController>();

                if (moc != null)
                {
                    materialData = new AudioController.SoundMaterialOverride(moc.concrete, moc.wood, moc.carpet, moc.tile, moc.plaster, moc.fabric, moc.metal, moc.glass);
                }
                else
                {
                    //Look for interactable
                    InteractableController i = hit.transform.GetComponent<InteractableController>();

                    if (i != null && i.interactable != null && i.interactable.preset.useMaterialOverride)
                    {
                        materialData = i.interactable.preset.materialOverride;
                    }
                    else
                    {
                        MeshFilter mf = hit.transform.GetComponent<MeshFilter>();

                        if (mf != null)
                        {
                            FurniturePreset f = Toolbox.Instance.GetFurnitureFromMesh(mf.sharedMesh);

                            if (f != null)
                            {
                                materialData = new AudioController.SoundMaterialOverride(f.concrete, f.wood, f.carpet, f.tile, f.plaster, f.fabric, f.metal, f.glass);
                            }
                        }
                    }
                }
            }

            if (materialData == null)
            {
                materialData = new AudioController.SoundMaterialOverride(Player.Instance.currentRoom.floorMaterial.concrete, Player.Instance.currentRoom.floorMaterial.wood, Player.Instance.currentRoom.floorMaterial.carpet, Player.Instance.currentRoom.floorMaterial.tile, Player.Instance.currentRoom.floorMaterial.plaster, Player.Instance.currentRoom.floorMaterial.fabric, Player.Instance.currentRoom.floorMaterial.metal, Player.Instance.currentRoom.floorMaterial.glass);
            }
        }

        //Get occlusion sound values...
        AudioController.Instance.GetOcculusion(Player.Instance.currentNode, Player.Instance.currentNode, currentEvent, 1f, Player.Instance, materialData, out _, out currentListeners, out _, out _, out currentHearingRange);

        //Calculate icons needed
        currentIconCount = Mathf.FloorToInt(currentHearingRange / AudioController.Instance.soundIconRangeUnit);

        //Feedback
        if(currentIconCount > previousIconCount)
        {
            if (juice != null)
            {
                juice.Nudge(new Vector2(1.2f, 1.2f), Vector2.zero);
            }
        }

        previousIconCount = currentIconCount;

        //Game.Log("Updated player with current occlusion: " + currentOcclusion + " and listeners: " + currentListeners.Count);

        //Enable to animate icons
        this.enabled = true;
    }

    private void Update()
    {
        bool exitUpdate = true;

        int neededIcons = 0;
        if (currentIconCount > 0) neededIcons = 5;

        if(spawnedIcons.Count < neededIcons)
        {
            //Stop any existing icons from being removed...
            foreach(AudioIcon i in spawnedIcons)
            {
                i.remove = false;
            }

            AudioIcon newIcon = new AudioIcon();

            //Spawn extra icons
            GameObject newObj= Instantiate(PrefabControls.Instance.soundIndicatorIcon, rect);
            newIcon.rect = newObj.GetComponent<RectTransform>();
            newIcon.img = newObj.GetComponent<Image>();

            newIcon.rect.anchoredPosition = new Vector2(spawnedIcons.Count * -32f + iconOffset.x, newIcon.rect.anchoredPosition.y + iconOffset.y); //Align

            if(spawnedIcons.Count < currentIconCount)
            {
                newIcon.img.color = ControlsDisplayController.Instance.audioFullColor;
                fullIcons.Add(newIcon);
            }
            else newIcon.img.color = ControlsDisplayController.Instance.audioEmptyColor;

            spawnedIcons.Add(newIcon);

            exitUpdate = false;
        }
        else if(spawnedIcons.Count > neededIcons)
        {
            for (int i = 0; i < spawnedIcons.Count; i++)
            {
                if(i > currentIconCount - 1)
                {
                    spawnedIcons[i].remove = true;
                    fullIcons.Remove(spawnedIcons[i]);
                    exitUpdate = false;
                }
            }
        }

        //Fade in/out
        for (int i = 0; i < spawnedIcons.Count; i++)
        {
            AudioIcon icon = spawnedIcons[i];

            if (icon.remove)
            {
                //Fade out
                if (icon.fadeIn > 0f)
                {
                    icon.fadeIn -= Time.deltaTime * 5f;
                    icon.img.canvasRenderer.SetAlpha(icon.fadeIn);
                    icon.rect.localScale = new Vector3(icon.fadeIn, icon.fadeIn, icon.fadeIn);
                    exitUpdate = false;
                }
                //Remove icon
                else
                {
                    Destroy(icon.rect.gameObject);
                    spawnedIcons.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            else if (icon.fadeIn < 1f)
            {
                icon.fadeIn += Time.deltaTime * Mathf.Max(8f - i, 2f);
                icon.fadeIn = Mathf.Clamp01(icon.fadeIn);
                icon.img.canvasRenderer.SetAlpha(icon.fadeIn);
                icon.rect.localScale = new Vector3(icon.fadeIn, icon.fadeIn, icon.fadeIn);
                exitUpdate = false;
            }
        }

        //Change colours
        if (currentListeners.Count > 0)
        {
            if (colourLerp < 1f)
            {
                colourLerp += Time.deltaTime * 4f;
                colourLerp = Mathf.Clamp01(colourLerp);
                col = Color.Lerp(ControlsDisplayController.Instance.audioFullColor, InterfaceControls.Instance.heardSoundIconColour, colourLerp);
                exitUpdate = false;

                foreach(AudioIcon i in fullIcons)
                {
                    i.img.color = col;
                }

                if (additionalGraphic != null) additionalGraphic.color = col;
                if (juice != null) juice.elements[0].originalColour = col;
            }
        }
        else
        {
            if (colourLerp > 0f)
            {
                colourLerp -= Time.deltaTime * 4f;
                colourLerp = Mathf.Clamp01(colourLerp);
                col = Color.Lerp(ControlsDisplayController.Instance.audioFullColor, InterfaceControls.Instance.heardSoundIconColour, colourLerp);
                exitUpdate = false;

                foreach (AudioIcon i in fullIcons)
                {
                    i.img.color = col;
                }

                if (additionalGraphic != null) additionalGraphic.color = col;
                if (juice != null) juice.elements[0].originalColour = col;
            }
        }

        if (exitUpdate)
        {
            this.enabled = false;
        }
    }
}
