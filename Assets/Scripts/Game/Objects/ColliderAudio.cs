using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderAudio : MonoBehaviour
{
    public AudioEvent playSound;

    private void OnTriggerEnter(Collider coll)
    {
        Game.Log("Collider Audio: " + playSound);
        AudioController.Instance.PlayWorldOneShot(playSound, null, null, coll.transform.position, null);
    }
}
