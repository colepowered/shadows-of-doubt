﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PinnedPinButtonController : ButtonController, IPointerEnterHandler, IPointerExitHandler
{
    public Image mainColour;
    public Image mainOverlay;
    public Image pressedColour;
    public Image pressedOverlay;
    public RectTransform mainMOOverlay;

    public Sprite pinnedOverlay;
    public Sprite pinnedOverlayMO;

    public PinnedItemController pinnedController;

    public void Setup(PinnedItemController newItem)
    {
        pinnedController = newItem;

        SetupReferences();
        VisualUpdate();

        UpdatePinColour();
    }

    public void UpdatePinColour()
    {
        Color pinColour = InterfaceController.Instance.GetEvidenceColour(pinnedController.caseElement.color);

        background.color = pinColour;
        icon.color = pinColour;
        SetButtonBaseColour(pinColour);
    }

    void OnEnable()
    {
        SetupReferences();
        VisualUpdate();
    }

    public override void OnLeftClick()
    {
        //Select this
        //SetSelected(true);

        //Is the custom string select active?
        if (CasePanelController.Instance.customStringLinkSelection != null)
        {
            CasePanelController.Instance.FinishCustomStringLinkSelection(pinnedController);
            return;
        }
    }

    public override void OnLeftDoubleClick()
    {
        //pinnedController.ToggleMinimize();
    }

    //public override void OnHoverStart()
    //{
    //    mainOverlay.sprite = pinnedOverlayMO;
    //    mainMOOverlay.gameObject.SetActive(true);
    //}

    //public override void OnHoverEnd()
    //{
    //    if(!isSelected)
    //    {
    //        mainOverlay.sprite = pinnedOverlay;
    //        mainMOOverlay.gameObject.SetActive(false);
    //    }
    //}
}
