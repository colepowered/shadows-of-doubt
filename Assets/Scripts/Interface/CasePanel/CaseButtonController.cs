﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CaseButtonController : ButtonController
{
    public Case thisCase;

    public void Setup(Case newCase)
    {
        SetupReferences();
        thisCase = newCase;
        UpdateVisuals();
    }

    public void UpdateVisuals()
    {
        text.text = thisCase.name;
    }

    public override void OnLeftClick()
    {
        base.OnLeftClick();

        InterfaceController.Instance.ActivateObjectivesDisplay();

        CasePanelController.Instance.SetActiveCase(thisCase);
    }

    public override void OnLeftDoubleClick()
    {
        base.OnLeftDoubleClick();

        PopupMessageController.Instance.PopupMessage("RenameCase", true, true, RButton: "Confirm", anyButtonClosesMsg: true, enableInputField: true, inputFieldDefault: thisCase.name);
        PopupMessageController.Instance.OnLeftButton += OnRenameCancel;
        PopupMessageController.Instance.OnRightButton += OnRenameConfirm;
    }

    public void OnRenameConfirm()
    {
        PopupMessageController.Instance.OnLeftButton -= OnRenameCancel;
        PopupMessageController.Instance.OnRightButton -= OnRenameConfirm;

        thisCase.name = PopupMessageController.Instance.inputField.text;
        UpdateVisuals();
    }

    public void OnRenameCancel()
    {
        PopupMessageController.Instance.OnLeftButton -= OnRenameCancel;
        PopupMessageController.Instance.OnRightButton -= OnRenameConfirm;
    }
}
