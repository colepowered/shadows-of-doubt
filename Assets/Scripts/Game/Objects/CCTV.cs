﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CCTV : Machine
{
    public Transform cameraTransform;

    public void Setup(NewNode newLoc)
    {
        //UpdateMachinePosition();
        //currentGameLocation.cctvs.Add(this);

        //isMachine = true; //Set cctv flag
        //memory = 1f;
        //maxNumberOfSightings = Mathf.RoundToInt(Mathf.Lerp(SocialControls.Instance.maxSightings.x, SocialControls.Instance.maxSightings.y, memory));

        ////Set name
        //SetName();

        ////Listen for name change
        //currentLocation.parentAddress.evidenceEntry.OnNewName += SetName;

        ////Create evidence entry
        //evidenceEntry = EvidenceCreator.Instance.CreateEvidence("CCTV", this, currentLocation.parentAddress.evidenceEntry, true) as EvidenceWitness;

        ////Setup interactable
        //interactable.Setup(InteriorControls.Instance.cctvPreset, null, false, 0, 0, Vector3.zero, this, existingEvidence: evidenceEntry);
    }

    public void SetName()
    {
        //name = currentLocation.roomAddress + " " + StringTables.Get("evidence.generic", "CCTV");
    }

    //Monitoring
    public void OnCitizenEnter(Citizen cc)
    {
        //Create memory
        //MemoryEvent newMemory = new MemoryEvent(TimelineEvent.EventType.sightingArrive, this, cc, cc.currentNode, null, cc.ai.currentAction, null);
    }

    public void OnCitizenExit(Citizen cc)
    {
        //Create memory
        //MemoryEvent newMemory = new MemoryEvent(TimelineEvent.EventType.sightingDepart, this, cc, cc.currentNode, null, cc.ai.currentAction, null);
    }
}
