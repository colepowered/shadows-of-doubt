﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NaughtyAttributes;

public class InterfaceControls : MonoBehaviour
{
    [Header("First Person")]
    [Tooltip("Minimum size of the first person interaction cursor")]
    public Vector2 interactionCursorMin = new Vector2(72, 72);
    [Tooltip("Maximum size of the first person interaction cursor")]
    public Vector2 interactionCursorMax = new Vector2(700, 700);
    [Tooltip("Speed of the first person interaction cursor")]
    public float interactionCursorSpeed = 1f;
    [Tooltip("Interaction text normal colour")]
    public Color interactionTextColour = Color.white;
    public Color interactionTextDistanceColour = Color.grey;
    public Color interactionTextIllegalColour = Color.red;
    [Tooltip("Low health indicator displays if player health is under this (normalized)")]
    public float lowHealthIndicatorThreshold = 0.1f;
    [Tooltip("Display control icons for this long (seconds)")]
    public float controlIconDisplayTime = 2f;

    [Header("Tooltips")]
    [Tooltip("Globally enable tooltips")]
    public bool enableTooltips = true;
    [Tooltip("The default tooltip width")]
    public float tooltipWidth = 300f;
    [Tooltip("The tooltip prefab")]
    public GameObject tooltipObjectPrefab;
    [Tooltip("Delay before tooltip appears")]
    public float toolTipDelay = 0.35f;
    [Tooltip("How fast the tooltip fades in")]
    public float toolTipFadeInSpeed = 3.85f;
    [Tooltip("Default colour")]
    public Color defaultTextColour = Color.black;
    public float contextMenuWidth = 190f;

    [Header("Map")]
    public Sprite playerApartmentSprite;

    [Header("Buttons & Icons")]
    [Tooltip("Unknown icon")]
    public Sprite unknownIconLarge;
    [Tooltip("Default company icon")]
    public Sprite companyIconLarge;
    [Tooltip("Double click delay")]
    public float doubleClickDelay = 0.4f;


    public Sprite stickyNoteButtonSprite;

    public Sprite lockedSprite;
    public Sprite unlockedSprite;

    [Header("References")]
    [Tooltip("The HUD Canvas")]
    public Canvas hudCanvas;
    [Tooltip("The HUD Canvas Rect")]
    public RectTransform hudCanvasRect;
    [Tooltip("The parent of speech bubbles")]
    public RectTransform speechBubbleParent;
    [Tooltip("Container for reticle")]
    public RectTransform reticleContainer;
    [Tooltip("Container for location text")]
    public RectTransform locationTextContainer;

    [Tooltip("Toggles on/off for screenshot mode")]
    public List<RectTransform> screenshotModeToggleObjects = new List<RectTransform>();
    public List<RectTransform> screenShotModeAllowDialogObjects = new List<RectTransform>();

    [Header("HUD")]
    [Tooltip("Interaction control text colour")]
    public Color interactionControlTextColourNormal = Color.white;
    public Color windowTakeItemIconDefaultColor = Color.black;
    [System.NonSerialized]
    public string interactionControlTextNormalHex;
    [Tooltip("Interaction control text colour")]
    public Color interactionControlTextColourIllegal = Color.red;
    [System.NonSerialized]
    public string interactionControlTextIllegalHex;
    [Tooltip("Game message system text display speed")]
    public float gameMessageTextRevealSpeed = 5f;
    [Tooltip("How long to display game message before removing")]
    public float gameMessageDestroyDelay = 1.5f;
    [Tooltip("Anchor for the weapon switch display")]
    public RectTransform weaponSwitchAnchor;
    [Tooltip("Anchor of the first person item")]
    public Transform firstPersonItemsParent;
    public Color interactionTextNormalColour = Color.white;
    [Tooltip("Colour of trespassing alert escalaction 0")]
    public Color trespassingEscalationZero;
    [Tooltip("Colour of trespassing alert escalaction 1")]
    public Color trespassingEscalationOne;

    public RectTransform fastForwardArrow;

    [Tooltip("Height of the movie bars when spotted")]
    public float movieBarHeight = 32f;

    [Header("UI References")]
    public TextMeshProUGUI lockpicksText;
    public TextMeshProUGUI cashText;
    public TextMeshProUGUI socialRankText;
    public TextMeshProUGUI plottedRouteText;

    [Space(7)]
    public AnimationCurve notificationGlowCurve;
    public Color notificationColorMax = Color.white;
    public Color notificationColorMin = Color.clear;

    [Space(7)]
    public Color messageGrey = Color.grey;
    public Color messageRed = Color.red;
    public Color messageGreen = Color.green;
    public Color messageBlue = Color.blue;
    public Color messageYellow = Color.yellow;

    public enum Icon { lookingGlass, lightBulb, key, agent, citizen, pin, footprint, document, door, location, questionMark, eye, books, star, building, hand, run, money, message, lockpick, notebook, empty, skull, passedOut, telephone, printScanner, resolve, time, tick, cross, camera, vandalism, robbery, picture, fist, handcuffs, trash, food};

    [System.Serializable]
    public class IconConfig
    {
        public Icon iconType;
        public Sprite sprite;
    }

    [Header("Icons")]
    public Sprite starchLogo;
    public Sprite elGenLogo;
    public Sprite kensingtonLogo;
    public Sprite KaizenLogo;
    public Sprite candorLogo;
    public Sprite blackMarketLogo;
    public List<IconConfig> iconReference = new List<IconConfig>();

    [Header("Awareness HUD")]
    public Material arrow;
    public Material spotted;
    public Material speech;
    public float awarenessDistanceThreshold = 20f;
    [ColorUsageAttribute(true, true)]
    public Color spottedNormalEmission;
    [ColorUsageAttribute(true, true)]
    public Color arrowNormalEmission;
    [ColorUsageAttribute(true, true)]
    public Color awarenessAlertEmission;

    [Header("UI Speech")]
    public Vector2 textSpaceBuffer = new Vector2(18f, 6f);
    public float textBubbleMinWidth = 70f;
    public float textBubbleMaxWidth = 282f;
    public Color playerSpeechColour = Color.blue;
    public Color callerSpeechColour = Color.red;
    [Tooltip("On-screen speech talk speed")]
    public float visualTalkDisplaySpeed = 5f;
    [Tooltip("How long to display speech before removing")]
    public float visualTalkDisplayDestroyDelay = 1.5f;
    [Tooltip("Give extra on-screen time adding this amount per character")]
    public float visualTalkDisplayStringLengthModifier = 0.05f;
    [Tooltip("On-screen speech text size")]
    public float visualTalkTextSize = 16;
    [Tooltip("The min and max scale of the speech bubble based on distance")]
    public Vector2 speechMinMaxScale = Vector2.one;
    [Tooltip("The min and max scale of the ai indicator based on distance")]
    public Vector2 indicatorMinMaxScale = Vector2.one;
    [Tooltip("The max distance at which ai indicators are active")]
    public float maxIndicatorDistance = 20f;

    [Header("Objectives")]
    public Vector2 uiPointerDistanceRange = new Vector2(32, 64);
    public TextMeshProUGUI caseSolvedText;
    public List<CanvasRenderer> screenMessageFadeRenderers = new List<CanvasRenderer>();
    public RectTransform resolveQuestionsDisplayParent;
    public AnimationCurve caseSolvedAlphaAnim;
    public AnimationCurve caseSolvedKerningAnim;

    [Header("Handbook")]
    public Vector2 handbookWindowPosition = new Vector2(0, 0);

    [Header("Player")]
    public Vector2 lightOrbSize = new Vector2(16, 32);
    public AnimationCurve stealthModeOrbSizeTransitionIn;
    public AnimationCurve stealthModeOrbSizeTransitionOut;
    public RectTransform lightOrbRect;
    public Image lightOrbFillImg;
    public Image lightOrbOutline;
    public Image seenImg;
    public CanvasRenderer seenRenderer;
    public JuiceController seenJuice;

    //public CanvasRenderer normalCrosshairRenderer;
    //public Image normalCrosshair;
    //public CanvasRenderer stealthCrosshairRenderer;
    //public Image stealthCrosshair;
    [Space(7)]
    public RectTransform interactionRect;
    public RectTransform interactionULRect;
    public RectTransform interactionURRect;
    public RectTransform interactionBLRect;
    public RectTransform interactionBRRect;
    public List<Image> interactionFadeInImages = new List<Image>();
    public List<Image> interactionBoundImages = new List<Image>();
    public RectTransform interactionTextContainer;
    public TextMeshProUGUI interactionText;
    public RectTransform readingTextContainer;
    public CanvasRenderer readingContainerRend;
    public TextMeshProUGUI readingText;
    public CanvasRenderer readingTextRend;
    public Vector2 readingBoxMaxSize = new Vector2(820, 300);
    public RectTransform haveKeyIcon;
    public RectTransform lockedIcon;
    public Image lockedImg;
    public RectTransform forbiddenIcon;
    public RectTransform seenIcon;
    public TextMeshProUGUI lockStrengthText;

    [Space(7)]
    //Action interaction display
    public RectTransform actionInteractionDisplay;
    public RectTransform actionInteractionAnchor;
    public TextMeshProUGUI actionInteractionText;

    [Space(7)]
    public Color unheardSoundIconColour = Color.white;
    public Color heardSoundIconColour = Color.red;

    [Header("Case Panel")]
    [Tooltip("Width of the string")]
    public Vector2 stringWidthRange = new Vector2(8, 32);
    [Tooltip("How far away another evidence entry is pinned automatically")]
    public float autoPinDistance = 128f;
    [Tooltip("When auto-pinning, the radius space needed to spawn evidence")]
    public float pinnedEvidenceRadius = 64f;
    [Tooltip("When auto-pinning, the number of possible angle steps to position test")]
    public int angleStepsCount = 36;
    public Rigidbody2D caseBoardRigidbody;
    public RectTransform caseBoardCursorRBContainer;
    public Rigidbody2D caseBoardCursorRigidbody;
    public RectTransform caseBoardContentContainer;
    public float pinnedLinearDrag = 1f;
    public float movingLinearDrag = 0f;
    [Tooltip("Image displayed as a screenshot to replace the rendered camera when in case mode")]
    public RawImage cameraScreenshot;
    public RenderTexture cameraScreenshotRenderTex;
    public float pinnedMovementIntertiaMultiplier = -30f;

    [Header("Evidence")]
    [Tooltip("Default case file colour")]
    public Color defaultCaseFileColour = Color.white;
    [Tooltip("Maximum number of item entries in evidence history")]
    public int maximumEvidenceItemHistory = 80;
    [Tooltip("Interface customisable colours")]
    [ReorderableList]
    public List<PinColours> pinColours = new List<PinColours>();
    [Tooltip("Displayed when the player has photograph information for a citizen")]
    public Sprite citizenPhoto;
    [Tooltip("When true minimize evidence as soon as you pin it")]
    public bool minimizeEvidenceOnPinned = false;
    [Tooltip("Evidence link colour")]
    public Color markedLinkColour;

    public enum EvidenceColours { red, blue, yellow, green, purple, white, black};

    [System.Serializable]
    public class PinColours
    {
        public EvidenceColours colour;
        public Color actualColour;
    }

    [Tooltip("Neutral/Inactive colour")]
    public Color neutralColour = Color.white;
    [Tooltip("Incriminating colour")]
    public Color incriminatingColour = Color.red;
    [Tooltip("Innocent colour")]
    public Color innocentColour = Color.green;
    public Texture2D nullPhotoReference;

    [Header("Windows")]
    [Tooltip("The default window location")]
    public Vector2 defaultWindowLocation = new Vector2(0.66f, 0.15f);
    [Tooltip("Offset applied to default window location per active window")]
    public Vector2 windowCountOffset = new Vector2(-0.05f, 0.05f);
    [Tooltip("Speed of the minimize/restore animation")]
    public float minimizingAnimationSpeed = 12f;
    [Tooltip("Colour of selection buttons when selected")]
    public Color selectionColour = Color.yellow;
    [Tooltip("Colour of selection buttons when not selected")]
    public Color nonSelectionColour = Color.white;
    [Tooltip("The close button X")]
    public Sprite closeSprite;
    public Color closeColour = Color.red;
    [Tooltip("The minimize sprite for the close button")]
    public Sprite minimizeSprite;
    public Color minimizeColour = Color.blue;

    [Header("Cursor Sprites")]
    public Texture2D normalCursor;
    [Tooltip("Displayed when mousing over something that moves")]
    public Texture2D cursorMove;
    [Tooltip("Displayed when mousing over something that can be resized")]
    public Texture2D cursorResizeHorizonal;
    [Tooltip("Displayed when mousing over something that can be resized")]
    public Texture2D cursorResizeVertical;
    [Tooltip("Displayed when mousing over something that can be resized")]
    public Texture2D cursorResizeDiagonalRightLeft;
    [Tooltip("Displayed when mousing over something that can be resized")]
    public Texture2D cursorResizeDiagonalLeftRight;
    [Tooltip("Displayed when mousing over something that needs targeting")]
    public Texture2D cursorTarget;
    [Tooltip("Displayed when mousing over a button by default")]
    public Texture2D cursorButton;
    [Tooltip("Displayed when mousing over text input field")]
    public Texture2D cursorTextEdit;

    [Space(7)]
    public Sprite reactionInvestigateSightSprite;
    public Sprite reactionInvestigateSoundSprite;
    public Sprite reactionPersueSprite;
    public Sprite reactionSearchSprite;
    public Sprite reactionAvoidSprite;
    [Space(7)]
    public Texture reactionInvestigateSightTex;
    public Texture reactionInvestigateSoundTex;
    public Texture reactionPersueTex;
    public Texture reactionSearchTex;
    public Texture reactionAvoidTex;

    //Singleton pattern
    private static InterfaceControls _instance;
    public static InterfaceControls Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        interactionControlTextNormalHex = ColorUtility.ToHtmlStringRGB(interactionControlTextColourNormal);
        interactionControlTextIllegalHex = ColorUtility.ToHtmlStringRGB(interactionControlTextColourIllegal);
    }
}
