﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveDoorWedgeController : MonoBehaviour
{
    public InteractableController controller;
    public Transform wedge1;
    public Transform wedge2;
    public float setupProgress = 0f;

    // Update is called once per frame
    void Update()
    {
        if(setupProgress < 1f)
        {
            setupProgress += Time.deltaTime / 0.4f;
            setupProgress = Mathf.Min(setupProgress, 1f);

            wedge1.localEulerAngles = new Vector3(Mathf.Lerp(0, -45f, setupProgress), 0, 0);
            wedge2.localEulerAngles = new Vector3(Mathf.Lerp(0, 45f, setupProgress), 0, 0);
        }
    }
}
