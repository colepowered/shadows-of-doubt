﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "cloudsave_data", menuName = "Database/Dev Cloud Save Data")]

public class CloudSaveData : SoCustomComparison
{
	[Header("Dev Cloud Save Path")]
	[Tooltip("Path to dropbox save folder")]
	public string path;
}
