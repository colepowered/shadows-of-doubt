﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SpeechBubbleController : MonoBehaviour
{
    public RectTransform rect;
    public RectTransform bubbleRect;
    public string actualString;
    public TextMeshProUGUI text;
    public float stringReveal = 1f;
    public int revealedChars = 0;
    public float distance = 0f;
    public float timeStamp = 0f;

    public float oncreenTime = 0f; //On screen time based on length of string
    public float delayProgress = 0f;
    public float fadeProgress = 0f;
    private bool setFinalText = false; //True once transition in animation is finished

    public SpeechController.QueueElement speech;
    public SpeechController speechController;

    public Vector2 sizeTreshold = new Vector2(1f, 2f);

    public InterfaceController.AwarenessIcon awarenessIcon;

    public Image backgroundImg;
    public CanvasRenderer bgRend;
    public CanvasRenderer textRend;

    public Vector2 bubbleDesiredSize = Vector2.zero;
    public bool displayOnScreen = true;
    public Vector3 desiredPosition = Vector3.zero;

    public bool isPlayer = false;
    private bool firstPositionInit = false; //We need to position this properly on the first frame

    public void Setup(SpeechController.QueueElement newSpeech, SpeechController newSpeechController)
    {
        speech = newSpeech;
        speechController = newSpeechController;

        InterfaceController.Instance.activeSpeechBubbles.Add(this);

        Actor actor = speechController.actor;
        Human hu = null;

        if (actor != null)
        {
            hu = actor as Human;
            actor.isSpeaking = true;

            if (actor.isPlayer)
            {
                isPlayer = true; //If this is the player treat it as VO
                text.color = InterfaceControls.Instance.playerSpeechColour;
            }
        }

        if(speech.forceColour)
        {
            text.color = speech.color;
        }

        bubbleRect.gameObject.SetActive(false);
        bubbleRect.sizeDelta = Vector2.one;

        actualString = Strings.Get(speech.dictRef, speech.entryRef, useGenderReference: true, genderReference: hu).Trim();

        SideJob job = null;

        if(speech.jobRef > -1)
        {
            if(SideJobController.Instance.allJobsDictionary.TryGetValue(speech.jobRef, out job))
            {
                Game.Log("Found job reference to use as additional input object...");
            }
        }

        Human speakingAbout = null;

        if (speech.speakingAbout > -1)
        {
            if (CityData.Instance.GetHuman(speech.speakingAbout, out speakingAbout))
            {
                Game.Log("Found talking about reference to use as additional input object...");
            }
        }

        if (speech.useParsing)
        {
            object inputObject = null;

            Human h = actor as Human;

            if(h != null)
            {
                inputObject = h;
            }
            else
            {
                //Use scope: Talking to on phone
                if(speechController.phoneLine != null)
                {
                    //Talking to person...
                    if (InteractionController.Instance.dialogMode && InteractionController.Instance.remoteOverride != null && InteractionController.Instance.remoteOverride.speechController == speechController && !InteractionController.Instance.talkingTo.isActor.isPlayer)
                    {
                        inputObject = InteractionController.Instance.talkingTo.isActor;
                        Game.Log("Input object telephone: " + inputObject);
                    }
                }

                //Use scope: Interactable
                if (inputObject == null && speechController.interactable != null)
                {
                    inputObject = speechController.interactable;
                }
            }

            object additionalObject = job;
            if (job == null && speakingAbout != null) additionalObject = speakingAbout;

            actualString = Strings.ComposeText(actualString, inputObject, Strings.LinkSetting.forceNoLinks, additionalObject: additionalObject);
        }

        //If shouting use all caps
        if(speech.shouting)
        {
            actualString = actualString.ToUpper();
        }

        //Set on-screen time to base value + string length modifier
        oncreenTime = (InterfaceControls.Instance.visualTalkDisplayDestroyDelay + (actualString.Length * InterfaceControls.Instance.visualTalkDisplayStringLengthModifier)) / Game.Instance.textSpeed;

        text.text = string.Empty;
        text.fontSize = InterfaceControls.Instance.visualTalkTextSize;
        timeStamp = SessionData.Instance.gameTime; //For when we need to display multiple text speeches on screen

        stringReveal = 1f;
        revealedChars = 0;
    }

    void Update()
    {
        //Animate
        if (SessionData.Instance.play)
        {
            //When in dialogue mode, hide everything that isn't talking to the player
            if(!isPlayer && InteractionController.Instance.dialogMode && InteractionController.Instance.talkingTo != speechController.interactable && (InteractionController.Instance.remoteOverride == null || InteractionController.Instance.remoteOverride != speechController.interactable))
            {
                bgRend.gameObject.SetActive(false);
                textRend.gameObject.SetActive(false);
                backgroundImg.enabled = false;
            }
            else if(!bgRend.gameObject.activeSelf)
            {
                bgRend.gameObject.SetActive(true);
                textRend.gameObject.SetActive(true);
                backgroundImg.enabled = true;
            }

            //Update distance
            distance = Vector3.Distance(Player.Instance.lookAtThisTransform.position, speechController.interactable.wPos);
            float distanceNormalized = 1f - Mathf.Clamp01(distance / AudioControls.Instance.speakEvent.hearingRange);

            text.fontSize = InterfaceControls.Instance.visualTalkTextSize;

            //Reveal text
            float revealInterval = Time.deltaTime * (InterfaceControls.Instance.visualTalkDisplaySpeed * SessionData.Instance.currentTimeMultiplier * Game.Instance.textSpeed);

            //Reveal string
            stringReveal += revealInterval;

            if(stringReveal >= 1f && revealedChars < actualString.Length)
            {
                stringReveal = 0f;

                //Reveal text by char
                revealedChars++;
            }

            string lastChar = string.Empty;

            //Animate font size & alpha as it fades in...
            if (revealedChars <= actualString.Length && stringReveal <= 1f)
            {
                float lastCharSize = 1f - stringReveal;

                //Get the alpha hex value
                Color alphaC = Color.white;
                alphaC.a = stringReveal;
                string alphaStr = ColorUtility.ToHtmlStringRGBA(alphaC).Substring(6, 2);

                lastChar = "<size=" + (Mathf.CeilToInt(stringReveal * 100f)).ToString() + "%>" + "<alpha=#" + alphaStr + ">" + actualString.Substring(revealedChars - 1, 1);

                text.text = actualString.Substring(0, revealedChars - 1) + lastChar;
            }
            else if (revealedChars >= actualString.Length)
            {
                if(!setFinalText)
                {
                    text.text = actualString;
                    setFinalText = true;
                }

                delayProgress += Time.deltaTime * SessionData.Instance.currentTimeMultiplier;

                if (delayProgress >= oncreenTime)
                {
                    if (awarenessIcon != null) awarenessIcon.Remove();

                    fadeProgress += Time.deltaTime * 2f * SessionData.Instance.currentTimeMultiplier;
                    fadeProgress = Mathf.Clamp01(fadeProgress);

                    bgRend.SetAlpha(1f - fadeProgress);
                    textRend.SetAlpha(1f - fadeProgress);

                    if (fadeProgress >= 1f)
                    {
                        Destroy(this.gameObject);
                    }
                }
            }

            //Get angle from player...
            Vector3 forwardRelative = Vector3.zero;
            float angleBetween = 0;

            if (!isPlayer)
            {
                Vector3 displayPosition = Vector3.zero;

                if (speechController.actor != null)
                {
                    displayPosition = speechController.actor.lookAtThisTransform.position + new Vector3(0, CitizenControls.Instance.speechBubbleHeight, 0);
                }
                else if (speechController.interactable != null)
                {
                    displayPosition = speechController.interactable.wPos + new Vector3(0, CitizenControls.Instance.speechBubbleHeight, 0);
                }

                forwardRelative = displayPosition - Player.Instance.transform.position;
                angleBetween = Vector3.SignedAngle(forwardRelative, Player.Instance.transform.forward, Vector3.up);
            }

            //Display on screen position or in the anchor display...
            if (!isPlayer && Mathf.Abs(angleBetween) < 75f && InteractionController.Instance.talkingTo != speechController.interactable)
            {
                //Screen position
                if (!displayOnScreen)
                {
                    displayOnScreen = true;
                    this.transform.SetParent(InterfaceControls.Instance.speechBubbleParent, true);
                    InterfaceController.Instance.anchoredSpeech.Remove(this);
                }

                Vector3 displayPosition = Vector3.zero;

                if(speechController.actor != null)
                {
                    displayPosition = speechController.actor.lookAtThisTransform.position + new Vector3(0, 0.35f, 0);
                }
                else if(speechController.interactable != null)
                {
                    displayPosition = speechController.interactable.wPos + new Vector3(0, 0.35f, 0);
                }

                Vector3 screenPoint = CameraController.Instance.cam.WorldToScreenPoint(displayPosition); //Get point above their head and convert it to a screen position
                Vector2 rectPos;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(InterfaceController.Instance.firstPersonUI, screenPoint, null, out rectPos);

                //Set position
                desiredPosition = InterfaceControls.Instance.speechBubbleParent.TransformPoint(rectPos);
            }
            else
            {
                if (displayOnScreen)
                {
                    displayOnScreen = false;

                    //Set parent to anchor
                    this.transform.SetParent(InterfaceController.Instance.speechDisplayAnchor, true);

                    if (!InterfaceController.Instance.anchoredSpeech.Contains(this))
                    {
                        InterfaceController.Instance.anchoredSpeech.Add(this);
                    }

                    //Force update of desired position so we have the correct one ready to set on first frame...
                    InterfaceController.Instance.UpdateAnchoredSpeechPositions();
                }
            }

            //Set size
            text.rectTransform.sizeDelta = new Vector2(InterfaceControls.Instance.textBubbleMaxWidth, 1080); //Give the text plenty of room to begin with so it won't auto-wrap
            if (!displayOnScreen) text.rectTransform.sizeDelta = new Vector2(InterfaceController.Instance.speechDisplayAnchor.sizeDelta.x, 1080);
            text.ForceMeshUpdate();

            float maxWidth = Mathf.Min(text.preferredWidth + InterfaceControls.Instance.textSpaceBuffer.x, InterfaceControls.Instance.textBubbleMaxWidth);
            if (!displayOnScreen) maxWidth = Mathf.Min(text.preferredWidth, InterfaceController.Instance.speechDisplayAnchor.sizeDelta.x);
            float maxHeight = text.preferredHeight + InterfaceControls.Instance.textSpaceBuffer.y;

            text.rectTransform.sizeDelta = new Vector2(maxWidth, maxHeight);

            //Make the bubble and give it extra room
            bubbleDesiredSize = text.rectTransform.sizeDelta + InterfaceControls.Instance.textSpaceBuffer;

            //Postion properly on the first frame
            if (!firstPositionInit)
            {
                rect.position = desiredPosition;
                bubbleRect.sizeDelta = bubbleDesiredSize;

                firstPositionInit = true;
            }
            else
            {
                //This handles the positioning lag
                if (desiredPosition != rect.position)
                {
                    rect.position = desiredPosition;
                }

                //This handles the lag between sizing the bubble background
                if (bubbleDesiredSize != bubbleRect.sizeDelta)
                {
                    bubbleRect.sizeDelta = Vector2.Lerp(bubbleRect.sizeDelta, bubbleDesiredSize, Time.deltaTime * 9f * SessionData.Instance.currentTimeMultiplier);
                }
            }

            //Set scale
            float scale = 1;

            if(!isPlayer) scale = Mathf.Lerp(InterfaceControls.Instance.speechMinMaxScale.x, InterfaceControls.Instance.speechMinMaxScale.y, distanceNormalized);
            rect.localScale = new Vector3(scale, scale, scale);

            bubbleRect.gameObject.SetActive(true);
        }
    }

    private void OnDestroy()
    {
        InterfaceController.Instance.activeSpeechBubbles.Remove(this);

        if (speech.jobHandIn)
        {
            SideJob job = null;

            if (speech.jobRef > -1)
            {
                if(SideJobController.Instance.allJobsDictionary.TryGetValue(speech.jobRef, out job))
                {
                    job.Complete();
                }
            }
        }

        //End dialog with finish speaking...
        if (speech.endsDialog)
        {
            //Trigger end of dialog with player
            if (speech.speakingToRef == Player.Instance.interactable.id)
            {
                DialogController.Instance.OnDialogEnd(speech.dialog, speech.dialogPreset, Player.Instance.interactable, speechController.actor, speech.jobRef);
            }

            if (Player.Instance.activeCall != null) Player.Instance.activeCall.EndCall();
            else ActionController.Instance.Return(null, null, Player.Instance);
        }

        speechController.SetSpeechActive(false);
        if(speechController.actor != null) speechController.actor.isSpeaking = false;
        InterfaceController.Instance.anchoredSpeech.Remove(this);
    }
}
