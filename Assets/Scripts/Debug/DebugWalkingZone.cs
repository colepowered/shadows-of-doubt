﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugWalkingZone : MonoBehaviour
{
	public NewTile tile;

	private Renderer rend;

	public Texture green;
	public Texture yellow;
	public Texture red;
    public Texture orange;
    public Texture blue;
    public Texture violet;
    public Texture turqoise;

    private List<Texture> textureList = new List<Texture>();

    private void Awake()
    {
        textureList.Add(green);
        textureList.Add(yellow);
        textureList.Add(red);
        textureList.Add(orange);
        textureList.Add(blue);
        textureList.Add(violet);
        textureList.Add(turqoise);
    }
}
