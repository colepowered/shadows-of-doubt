﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using TMPro;
using System.IO;
using UnityEditor;

public class DDSControls : MonoBehaviour
{
    [Header("Sprites")]
    [ReorderableList]
    public List<Sprite> backgroundSprites = new List<Sprite>();

    [Header("Fonts")]
    public TMP_FontAsset defaultHandwritingFont;
    public TMP_FontAsset clearModeFont;
    [ReorderableList]
    public List<TMP_FontAsset> fonts = new List<TMP_FontAsset>();

    [Header("Elements")]
    public GameObject textComponent;
    public GameObject elementPrefab;
    [ReorderableList]
    public List<GameObject> elementPrefabs = new List<GameObject>();

    [Header("Import")]
    public string sourcePath;

    //Singleton pattern
    private static DDSControls _instance;
    public static DDSControls Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    [Button]
    public void Import()
    {
#if UNITY_EDITOR
        string sourceBlocksTextFilePath = sourcePath +  "/Strings/ENG/DDS/dds.blocks.csv";
        string sourceDdsCopyFolderPath = sourcePath + "/DDS/";

        string blocksTextFilePath = Application.streamingAssetsPath +  "/Strings/English/DDS/dds.blocks.csv";
        string ddsCopyFolderPath = Application.streamingAssetsPath + "/DDS/";

        System.IO.File.Copy(sourceBlocksTextFilePath, blocksTextFilePath, true);

        Debug.Log("Importing DDS files from " + sourceDdsCopyFolderPath);

        if (System.IO.Directory.Exists(sourceDdsCopyFolderPath))
        {
            UnityEditor.FileUtil.ReplaceDirectory(sourceDdsCopyFolderPath, ddsCopyFolderPath);
            AssetDatabase.Refresh();
        }
        else
        {
            Debug.Log("Source path does not exist!");
        }
#endif
    }
}
