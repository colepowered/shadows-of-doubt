﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

[System.Serializable]
public class NewAIGoal : IComparable<NewAIGoal>
{
    public string name = "NewGoal";

    [Header("Parents")]
    [System.NonSerialized]
    public NewAIController aiController;
    public AIGoalPreset preset;

    [Header("Goal Variables")]
    public float basePriority = 0f;
    private float traitMultiplier = 1f; //Add this on to the end of ranking
    [Tooltip("Is this goal currently active?")]
    public bool isActive = false;
    [Tooltip("The time this should happen at (ideally)")]
    public float triggerTime = 0f;
    [System.NonSerialized]
    public float duration = 0f;
    public float debugWorkStartHour = 0f;
    public float debugWorkEndHour = 0f;
    private NewGameLocation lastEstimatedTravelTime = null;
    [System.NonSerialized]
    public float travelTime = 0f;
    [Tooltip("The amount of time this has been active")]
    public float activeTime = 0f;
    private float lastUpdatedAt = 0f;

    //Passed job reference id for posting
    public int jobID = -1;

    [Header("Priority")]
    [Tooltip("How important is this action compared to others")]
    [System.NonSerialized]
    public float timingWeight = 0f;
    [System.NonSerialized]
    public float nourishmentWeight = 0f;
    [System.NonSerialized]
    public float hydrationWeight = 0f;
    [System.NonSerialized]
    public float altertnessWeight = 0f;
    [System.NonSerialized]
    public float tirednessWeight = 0f;
    [System.NonSerialized]
    public float energyWeight = 0f;
    [System.NonSerialized]
    public float excitementWeight = 0f;
    [System.NonSerialized]
    public float choresWeight = 0f;
    [System.NonSerialized]
    public float hygeieneWeight = 0f;
    [System.NonSerialized]
    public float bladderWeight = 0f;
    [System.NonSerialized]
    public float heatWeight = 0f;
    [System.NonSerialized]
    public float drunkWeight = 0f;
    [System.NonSerialized]
    public float breathWeight = 0f;
    [System.NonSerialized]
    public float poisonedWeight = 0f;
    [System.NonSerialized]
    public float blindedWeight = 0f;
    [Space(7)]
    [ReadOnly]
    public float priority = 0f;

    [Header("Actions")]
    public List<NewAIAction> actions = new List<NewAIAction>();
    public float nextPotterAction = 0f;
    private int doorCheckCycle = 0; //Delay door checks on ticks...

    //Interactables chosen for actions in this goal
    public List<Interactable> chosenInteractablesThisGoal = new List<Interactable>();

    [Header("Location")]
    public NewGameLocation gameLocation; //If this goal requires a specific game location
    public NewRoom roomLocation; //If this goal requires a specific room
    public NewNode passedNode = null; //Optionally pass this for created actions to reference...
    [System.NonSerialized]
    public Interactable passedInteractable = null; //Optionally pass this for created actions to reference...
    public NewGameLocation passedGameLocation = null; //Optionally pass this for created actions to reference...
    public int passedVar = -2;
    [System.NonSerialized]
    public GroupsController.SocialGroup passedGroup = null; //Optionally pass this for meet-ups involving other people
    public int searchProgress = 0; //Search these rooms if appropriate
    public List<NewNode> searchedNodes = new List<NewNode>();
    [System.NonSerialized]
    public MurderController.Murder murderRef; //If this is a murder goal, this is the reference to it...
    //public GameObject spawnedRightItem;
    //public GameObject spawnedLeftItem;

    public SessionData.WeekDay lastCheckedForWorkingDay;
    public SessionData.WeekDay lastCheckedForGroupDay;
    private bool startGameWorkCheck = false;
    private bool startGameGroupCheck = false;

    private NewRoom arrivedRoom; //Used to trigger lighting arrival check once

    public enum DoorActionCheckResult { success, noHandle, beingUsed, duplicate};

    public enum DoorSide { mySide, forceCurrentSide, forceCurrentOtherSide };

    public NewAIGoal(
        NewAIController newController,
        AIGoalPreset newPreset,
        float newTrigerTime,
        float newDuration,
        NewNode newPassedNode = null,
        Interactable newPassedInteractable = null,
        NewGameLocation newPassedGameLocation = null,
        GroupsController.SocialGroup newPassedGroup = null,
        MurderController.Murder newMurderRef = null,
        float newTraitMultiplier = 1f,
        int newPassedVar = -2
        )
    {
        aiController = newController;
        preset = newPreset;
        triggerTime = newTrigerTime;
        duration = newDuration;
        lastUpdatedAt = SessionData.Instance.gameTime; //Important: When this is created set this to the game time
        name = newPreset.name; //Set name
        basePriority = preset.basePriority; //Copy this variable so we can change it per instance
        traitMultiplier = newTraitMultiplier;

        passedNode = newPassedNode;
        passedInteractable = newPassedInteractable;
        passedGameLocation = newPassedGameLocation;
        passedGroup = newPassedGroup;
        passedVar = newPassedVar;
        murderRef = newMurderRef;

        //Is this the invesigate goal?
        if(preset.name == "Investigate")
        {
            aiController.investigationGoal = this;
        }

        //Is this the patrol goal?
        if (preset.name == "PatrolGameLocation")
        {
            aiController.patrolGoal = this;
        }

        //Add to goal list
        aiController.goals.Add(this);

        //Get timing data from job
        if (preset == RoutineControls.Instance.workGoal)
        {
            UpdateNextWorkingTimes();
        }

        //Get timing for meet up
        if(passedGroup != null)
        {
            if (passedGroup.preset != null && preset == Toolbox.Instance.groupsDictionary[passedGroup.preset].meetUpGoal)
            {
                UpdateNextGroupTimes();
            }
        }
    }

    //Update work time
    public void UpdateNextWorkingTimes()
    {
        lastCheckedForWorkingDay = SessionData.Instance.day;

        //Convert start time to the gametime format
        triggerTime = SessionData.Instance.GetNextOrPreviousGameTimeForThisHour(aiController.human.job.workDaysList, aiController.human.job.startTimeDecimalHour, aiController.human.job.endTimeDecialHour);
        duration = aiController.human.job.workHours;
        debugWorkStartHour = aiController.human.job.startTimeDecimalHour;
        debugWorkEndHour = aiController.human.job.endTimeDecialHour;

        if(!startGameWorkCheck && SessionData.Instance.startedGame)
        {
            startGameWorkCheck = true;
        }
    }

    //Update event time
    public void UpdateNextGroupTimes()
    {
        lastCheckedForGroupDay = SessionData.Instance.day;

        triggerTime = passedGroup.GetNextMeetingTime();
        //Game.Log("Updated group trigger time: " + triggerTime + " = " + SessionData.Instance.GameTimeToClock24String(triggerTime, true) + ", " + SessionData.Instance.ShortDateString(triggerTime, true) + " for group " + passedGroup.preset);

        if (!startGameGroupCheck && SessionData.Instance.startedGame)
        {
            startGameGroupCheck = true;
        }
    }

    //Calculate & update priority. This will happen when the AI controller updates itself.
    public void UpdatePriority()
    {
        if(aiController.delayedGoalsForTime.ContainsKey(preset))
        {
            //aiController.human.SelectedDebug("AI: " + aiController.human.name + " has delayed goal " + preset.name);
            priority = 0;
            return;
        }

        //Count time active
        if(isActive)
        {
            activeTime += SessionData.Instance.gameTime - lastUpdatedAt;
            lastUpdatedAt = SessionData.Instance.gameTime;

            if(preset.cancelAfterTime)
            {
                if(activeTime >= preset.cancelAfter)
                {
                    Remove(); //Remove this goal
                    return;
                }
            }
        }

        if (preset == RoutineControls.Instance.workGoal)
        {
            //Update next working times
            if(lastCheckedForWorkingDay != SessionData.Instance.day || !startGameWorkCheck)
            {
                UpdateNextWorkingTimes();
            }

            //Only do this before work: Monitor trigger
            if(SessionData.Instance.gameTime < triggerTime)
            {
                //Estimate travel time and use it to modify trigger time
                if (lastEstimatedTravelTime != aiController.human.currentGameLocation && aiController.human.job.employer != null)
                {
                    NewNode jobLoc = null;

                    if(aiController.human.workPosition != null)
                    {
                        jobLoc = aiController.human.workPosition.node;
                    }
                    else if(aiController.human.job.employer.placeOfBusiness != null && aiController.human.job.employer.placeOfBusiness.nodes.Count > 0)
                    {
                        jobLoc = aiController.human.job.employer.placeOfBusiness.nodes[0];
                    }
                    else if (aiController.human.job.employer.address != null && aiController.human.job.employer.address.nodes.Count > 0)
                    {
                        jobLoc = aiController.human.job.employer.address.nodes[0];
                    }

                    if(jobLoc != null)
                    {
                        travelTime = Toolbox.Instance.TravelTimeEstimate(aiController.human, aiController.human.currentNode, jobLoc);
                        lastEstimatedTravelTime = aiController.human.currentGameLocation;
                    }
                }
            }
        }

        //Get timing for meet up
        if (passedGroup != null)
        {
            if (passedGroup.preset != null && preset == Toolbox.Instance.groupsDictionary[passedGroup.preset].meetUpGoal)
            {
                //Update next working times
                if (lastCheckedForGroupDay != SessionData.Instance.day || !startGameGroupCheck)
                {
                    UpdateNextGroupTimes();
                }

                //Only do this before work: Monitor trigger
                if (SessionData.Instance.gameTime < triggerTime)
                {
                    //Estimate travel time and use it to modify trigger time
                    if (lastEstimatedTravelTime != aiController.human.currentGameLocation)
                    {
                        NewAddress ad = CityData.Instance.addressDictionary[passedGroup.meetingPlace];

                        travelTime = Toolbox.Instance.TravelTimeEstimate(aiController.human, aiController.human.currentNode, ad.anchorNode);
                        lastEstimatedTravelTime = aiController.human.currentGameLocation;
                    }
                }
            }
        }

        //Murder goal priority is based on phases...
        if (murderRef != null)
        {
            if (murderRef.state == MurderController.MurderState.none || murderRef.state == MurderController.MurderState.waitForLocation || murderRef.state == MurderController.MurderState.unsolved || murderRef.state == MurderController.MurderState.solved)
            {
                priority = 0;
            }
            else priority = 10.75f;

            Game.Log("Murder: Setting murder goal priority: " + priority + " (" + murderRef.state + ")");
        }
        //Flee goal is based on nerve value
        else if(preset == RoutineControls.Instance.fleeGoal)
        {
            if (aiController.killerForMurders.Count <= 0 || !aiController.killerForMurders.Exists(item => item.state == MurderController.MurderState.executing || item.state == MurderController.MurderState.post))
            {
                //Relative nerve
                float relNerve = Mathf.Clamp01(aiController.human.currentNerve / aiController.human.maxNerve);

                priority = preset.minMaxPriority.y - (relNerve * preset.minMaxPriority.y);

                if (aiController.reactionState == NewAIController.ReactionState.persuing)
                {
                    if (aiController.investigateLocation != null)
                    {
                        bool trespassInv = aiController.human.IsTrespassing(aiController.investigateLocation.room, out _, out _, false);

                        if (trespassInv)
                        {
                            priority = preset.minMaxPriority.y; //If the AI persues to somewhere illegal, raise alarm instead.
                        }
                    }
                }
            }
            else priority = 0;

            //aiController.human.SelectedDebug("Flee goal priority: " + priority);
        }
        //Steal goal: most active if folks are far from their apartment
        else if (preset == RoutineControls.Instance.stealItem)
        {
            if(passedInteractable == null)
            {
                Game.LogError("Steal goal with no passed interactable!");
            }
            else if(passedInteractable.node == null)
            {
                Game.LogError("Steal interactable with no node assigned!");
            }
            else
            {
                NewGameLocation loc = passedInteractable.node.gameLocation;

                priority = 0;

                //If nobody home or everybody asleep
                if (loc.thisAsAddress != null)
                {
                    foreach (Human h in loc.thisAsAddress.inhabitants)
                    {
                        if(h.isHome && !h.isAsleep)
                        {
                            priority = 0;
                            break;
                        }
                        else if(h.isHome && h.isAsleep)
                        {
                            priority += 7f / loc.thisAsAddress.inhabitants.Count;
                        }
                        //Not home
                        else if(!h.isHome)
                        {
                            float dist = Mathf.Min(Vector3.Distance(h.transform.position, loc.thisAsAddress.entrances[0].worldAccessPoint), 100f); //Max value 100

                            priority += (dist / 10f) / loc.thisAsAddress.inhabitants.Count;
                        }
                    }
                }
            }

        }
        else if (!preset.onlyImportantBetweenHours || SessionData.Instance.decimalClock >= preset.validBetweenHours.x && SessionData.Instance.decimalClock <= preset.validBetweenHours.y)
        {
            //Random variance
            priority = Toolbox.Instance.Rand(0f, (float)preset.randomVariance);

            //Debt collection
            if (preset.useLateDebtPriority)
            {
                if (aiController.human.job != null && aiController.human.job.employer != null)
                {
                    GameplayController.LoanDebt debt = GameplayController.Instance.debt.Find(item => item.companyID == aiController.human.job.employer.companyID);

                    if (debt != null)
                    {
                        if (SessionData.Instance.gameTime > debt.nextPaymentDueBy)
                        {
                            priority += preset.minMaxPriority.y;
                        }
                    }
                }
            }

            float mp = 1f;

            if (preset.multiplyUsingTrashCarried)
            {
                mp = aiController.human.anywhereTrash;
            }

            float rainMp = 1f;

            if(preset.rainFactor == AIGoalPreset.RainFactor.dontDoWhenRaining)
            {
                rainMp = Mathf.Clamp01(1f - SessionData.Instance.currentRain);
            }
            else if(preset.rainFactor == AIGoalPreset.RainFactor.onlyDoWhenRaining)
            {
                rainMp = Mathf.Clamp01(SessionData.Instance.currentRain);
            }

            //Remove over time
            float overTimeMP = 1f;

            if(preset.affectPriorityOverTime)
            {
                overTimeMP += activeTime * preset.multiplierModifierOverOneHour;
            }

            //Use music to affect base priority
            if (preset.useMusic)
            {
                if(!aiController.human.currentRoom.musicPlaying)
                {
                    basePriority = -99;
                    //aiController.human.SelectedDebug("Use music, music NOT playing! Base priority: " + basePriority);
                }
                else
                {
                    //Music must be 3 in-game minutes in...
                    if(SessionData.Instance.gameTime > aiController.human.currentRoom.musicStartedAt + 0.05f)
                    {
                        Interactable activeMusicPlayer = GameplayController.Instance.activeMusicPlayers.Find(item => item.node != null && item.node.room == aiController.human.currentRoom);

                        //Found an active music player here...
                        if(activeMusicPlayer != null)
                        {
                            //Check is playing a dancable track...
                            if(activeMusicPlayer.loopingAudio.Exists(item => item.eventPreset.canDanceTo && item.state == FMOD.Studio.PLAYBACK_STATE.PLAYING))
                            {
                                basePriority = preset.basePriority;
                            }
                            else basePriority = -99;
                        }
                        else basePriority = -99;
                    }
                    else basePriority = -99;
                }
            }

            //Use trespassing to affect base priority
            if(preset.useTrespassing)
            {
                if (aiController.human.isTrespassing)
                {
                    basePriority = 99;
                }
                else basePriority = -99;
            }

            //Add base priority
            priority += basePriority * mp * rainMp * overTimeMP;

            //How close to the time are we?
            if (preset.useTiming)
            {
                float timeDiff = (triggerTime - travelTime) - SessionData.Instance.gameTime;

                //As a proportion of the time window...
                //Before...
                if (timeDiff >= 0f && timeDiff <= preset.earlyTimingWindow)
                {
                    //Cancel if late
                    if (preset.cancelIfLate)
                    {
                        if (timeDiff < -preset.cancelIfThisLate)
                        {
                            Remove(); //Remove this goal
                            return;
                        }
                    }

                    float timeImp = 1f - (Mathf.Max(timeDiff, 0f) / preset.earlyTimingWindow);
                    timingWeight = timeImp * preset.timingImportance;
                    priority += timingWeight;
                }
                //During
                else if (timeDiff < 0f && SessionData.Instance.gameTime < triggerTime + duration)
                {
                    timingWeight = preset.timingImportance;

                    //Lunch break
                    if (preset == RoutineControls.Instance.workGoal && aiController.human.job.lunchBreak)
                    {
                        //For now use 45 mins
                        if (SessionData.Instance.gameTime > triggerTime + aiController.human.job.lunchBreakHoursAfterStart && SessionData.Instance.gameTime < triggerTime + aiController.human.job.lunchBreakHoursAfterStart + 0.45f)
                        {
                            timingWeight = 0f;
                        }
                    }

                    priority += timingWeight;
                }
                else timingWeight = 0f; //Timing weight is nothing outside this window
            }

            //Add stat weights
            nourishmentWeight = (1f - aiController.human.nourishment) * (float)preset.nourishmentImportance;
            priority += nourishmentWeight;

            hydrationWeight = (1f - aiController.human.hydration) * (float)preset.hydrationImportance;
            priority += hydrationWeight;

            altertnessWeight = (1f - aiController.human.alertness) * (float)preset.alertnessImportance;
            priority += altertnessWeight;

            energyWeight = (1f - aiController.human.energy) * (float)preset.energyImportance;
            priority += energyWeight;

            excitementWeight = (1f - aiController.human.excitement) * (float)preset.excitementImportance;
            priority += excitementWeight;

            choresWeight = (1f - aiController.human.chores) * (float)preset.choresImportance;
            priority += choresWeight;

            hygeieneWeight = (1f - aiController.human.hygiene) * (float)preset.hygieneImportance;
            priority += hygeieneWeight;

            bladderWeight = (1f - aiController.human.bladder) * (float)preset.bladderImportance;
            priority += bladderWeight;

            heatWeight = (1f - aiController.human.heat) * (float)preset.heatImportance;
            priority += heatWeight;

            drunkWeight = (1f - aiController.human.drunk) * (float)preset.drunkImportance;
            priority += drunkWeight;

            breathWeight = (1f - aiController.human.breath) * (float)preset.breathImportance;
            priority += breathWeight;

            poisonedWeight = aiController.human.poisoned * (float)preset.poisonImportance;
            priority += poisonedWeight;

            blindedWeight = aiController.human.blinded * (float)preset.blindedImportance;
            priority += blindedWeight;

            //Other goal modifiers
            foreach (AIGoalPreset p in preset.ifGoalsPresent)
            {
                if(aiController.goals.Exists(item => item != this && item.preset == p))
                {
                    priority += preset.otherGoalPriorityModifier;
                    break;
                }
            }

            //if(preset.ignorePriorityMinMax)
            //{
            //    priority = Mathf.Clamp(priority, preset.minMaxPriority.x, preset.minMaxPriority.y);
            //    priority *= traitMultiplier;
            //}
            //else
            //{
                priority = Mathf.Clamp(priority * traitMultiplier, preset.minMaxPriority.x, preset.minMaxPriority.y);
            //}

            //aiController.human.SelectedDebug("Priority for " + preset.name + " is " + priority);
        }
        else priority = 0;
    }

    //Triggered when this goal becomes active
    public void OnActivate()
    {
        if(!isActive)
        {
            if (aiController == null || aiController.human == null || preset == null) return;

            if (Game.Instance.collectDebugData)
            {
                aiController.human.SelectedDebug("Activate: " + name, Actor.HumanDebug.actions);
                aiController.AddDebugAction("Activate: " + name);
            }

            isActive = true;
            aiController.currentGoal = this;
            gameLocation = null;

            //Set on duty
            if (!aiController.human.isOnDuty && (preset == RoutineControls.Instance.workGoal || preset == RoutineControls.Instance.enforcerResponse || preset == RoutineControls.Instance.enforcerGuardDuty || aiController.human.currentGameLocation.isCrimeScene || (aiController.human.isEnforcer && (aiController.human.outfitController.currentOutfit == ClothesPreset.OutfitCategory.work || aiController.human.outfitController.currentOutfit == ClothesPreset.OutfitCategory.outdoorsWork))))
            {
                aiController.human.isOnDuty = true;
            }

            //If this is flee, cancel combat
            if (preset == RoutineControls.Instance.fleeGoal)
            {
                //Cancel combat
                if (!aiController.ko)
                {
                    aiController.CancelCombat();
                }
            }

            //Are consumables allowed? If not then move current to trash...
            if (aiController.human.currentConsumables != null)
            {
                if (preset.trashConsumablesOnActivate && aiController.human.currentConsumables.Count > 0)
                {
                    int safety = 99;

                    while (aiController.human.currentConsumables.Count > 0 && safety > 0)
                    {
                        aiController.human.RemoveCurrentConsumable(aiController.human.currentConsumables[0]);
                        safety--;
                    }
                }
            }

            //Set next potter action time
            if (preset.allowPottering)
            {
                SetNextPotterTime();
            }

            try
            {
                //Pick a location
                //None: Use current location
                if(preset.locationOption == AIGoalPreset.LocationOption.useCurrent)
                {
                    gameLocation = aiController.human.currentGameLocation;
                }
                else if(preset.locationOption == AIGoalPreset.LocationOption.murderLocation)
                {
                    if(murderRef.location != null)
                    {
                        Game.Log("Murder: Setting murder goal location to " + murderRef.location.name);
                        gameLocation = murderRef.location;
                    }
                    else
                    {
                        gameLocation = murderRef.murderer.currentGameLocation; //Assign a location just to stop this goal from being removed...
                    }
                }
                else if(preset.locationOption == AIGoalPreset.LocationOption.passedInteractable && passedInteractable != null)
                {
                    if(passedInteractable.isActor != null)
                    {
                        gameLocation = passedInteractable.isActor.currentGameLocation;
                    }
                    else gameLocation = passedInteractable.node.gameLocation;
                }
                else if(preset.locationOption == AIGoalPreset.LocationOption.passedGamelocation)
                {
                    gameLocation = passedGameLocation;

                    if(passedGameLocation == null && passedNode != null)
                    {
                        gameLocation = passedNode.gameLocation;
                    }
                }
                else if (preset.locationOption == AIGoalPreset.LocationOption.investigate && aiController.investigateLocation != null)
                {
                    //Game.Log(aiController.human.name);
                    gameLocation = aiController.investigateLocation.gameLocation;
                }
                else if (preset.locationOption == AIGoalPreset.LocationOption.patrolLocation)
                {
                    //Game.Log(aiController.human.name);
                    gameLocation = aiController.patrolLocation;
                }
                //Citizen's job
                else if (preset.locationOption == AIGoalPreset.LocationOption.work)
                {
                    if (aiController.human.job != null && aiController.human.job.employer != null)
                    {
                        //Should I own a job position?
                        if (aiController.human.job.preset.ownsWorkPosition)
                        {
                            if (aiController.human.workPosition != null && aiController.human.workPosition.node != null)
                            {
                                gameLocation = aiController.human.workPosition.node.gameLocation;
                            }
                            else
                            {
                                Game.LogError("AI: Citizen should own a job furniture/position but doesn't: " + aiController.human.name + ")");
                                gameLocation = aiController.human.job.employer.placeOfBusiness;
                            }
                        }
                        else
                        {
                            gameLocation = aiController.human.job.employer.placeOfBusiness;
                        }
                    }
                    else
                    {
                        //If unemployed, remove this
                        Remove();
                    }
                }
                else
                {
                    //Do find nearest first so you can fall back on other options if needed...
                    if(preset.locationOption == AIGoalPreset.LocationOption.commercialDecision)
                    {
                        if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Attempting commericial decision for " + preset.name, Actor.HumanDebug.actions);

                        //Do I have enough energy to go out?
                        if (aiController.human.energy >= 0.2f)
                        {
                            //Liklihood increases with the amount of time spent at one location (3 hours == 100% commercial)...
                            float commericalChance = Mathf.Clamp01(aiController.timeAtCurrentAddress / RoutineControls.Instance.commericalDecisionMPTimeSpent);

                            //Also multiply this by player proximity in the hope we generate busy treets while the player is not in the same location
                            if (aiController.human.currentBuilding == Player.Instance.currentBuilding)
                            {
                                if (aiController.human.currentGameLocation == Player.Instance.currentGameLocation)
                                {
                                    commericalChance *= RoutineControls.Instance.commericalDecisionMPlayerSameLocation;
                                }
                                else
                                {
                                    commericalChance *= RoutineControls.Instance.commericalDecisionMPlayerSameBuilding;
                                }
                            }
                            else
                            {
                                commericalChance *= RoutineControls.Instance.commericalDecisionMPlayerElsewhere;
                            }

                            if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("... Commercial chance = " + commericalChance + " (I've been here for " + aiController.timeAtCurrentAddress + ")", Actor.HumanDebug.actions);

                            if (Toolbox.Instance.Rand(0f, 1f) <= commericalChance)
                            {
                                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("... Commercial chance succeded...", Actor.HumanDebug.actions);

                                //Otherwise use other favourite...
                                if (gameLocation == null)
                                {
                                    //Go to nearest fav place if open...
                                    NewAddress fav = null;

                                    //Get favourite
                                    if (aiController.human.favouritePlaces.TryGetValue(preset.desireCategory, out fav))
                                    {
                                        if (fav.company.openForBusinessDesired && fav.company.openForBusinessActual)
                                        {
                                            gameLocation = fav;
                                        }
                                    }
                                }

                                //Am I already in a place that offers this?
                                if (gameLocation == null && (aiController.human.currentGameLocation != null && aiController.human.currentGameLocation.thisAsAddress != null))
                                {
                                    if (aiController.human.currentGameLocation.thisAsAddress.company != null)
                                    {
                                        //Provides this
                                        if (aiController.human.currentGameLocation.thisAsAddress.company.preset.companyCategories.Contains(preset.desireCategory))
                                        {
                                            //Is this my job though?
                                            if (aiController.human.job == null || aiController.human.job.employer == null || aiController.human.job.employer.placeOfBusiness != aiController.human.currentGameLocation)
                                            {
                                                //Is open
                                                if (aiController.human.currentGameLocation.thisAsAddress.IsPublicallyOpen(false))
                                                {
                                                    gameLocation = aiController.human.currentGameLocation;
                                                }
                                            }
                                        }
                                    }
                                }

                                //Lastly, search for closest...
                                try
                                {
                                    if (gameLocation == null)
                                    {
                                        AIActionPreset firstAction = GetFirstAction(null);
                                        passedInteractable = Toolbox.Instance.FindNearestWithAction(firstAction, aiController.human.currentRoom, aiController.human, AIActionPreset.FindSetting.nonTrespassing, true, enforcersAllowedEverywhere: preset.allowEnforcersEverywhere, mustContainDesireCategory: true, containDesireCategory: preset.desireCategory);

                                        if (passedInteractable != null)
                                        {
                                            gameLocation = passedInteractable.node.gameLocation;

                                            if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Made commerical deicsion! " + gameLocation.name, Actor.HumanDebug.actions);
                                        }
                                        else
                                        {
                                            if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("...Unable for find public interactable for " + firstAction.name, Actor.HumanDebug.actions);
                                        }
                                    }
                                }
                                catch
                                {

                                }
                            }
                        }
                        else
                        {
                            if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Energy is too low", Actor.HumanDebug.actions);
                        }

                        //If the commerical decision fails, it's most likely we want to check at home for the task...
                        if(gameLocation == null)
                        {
                            gameLocation = aiController.human.home;
                        }
                    }

                    //Find favourite/nearest commerical
                    if (preset.locationOption == AIGoalPreset.LocationOption.commercial)
                    {
                        //Am I already in a place that offers this?
                        if (aiController.human.currentGameLocation != null && aiController.human.currentGameLocation.thisAsAddress != null)
                        {
                            if (aiController.human.currentGameLocation.thisAsAddress.company != null && (aiController.human.job == null || aiController.human.job.employer != aiController.human.currentGameLocation.thisAsAddress.company))
                            {
                                //Provides this
                                if (aiController.human.currentGameLocation.thisAsAddress.company.preset.companyCategories.Contains(preset.desireCategory))
                                {
                                    //Is open
                                    if (aiController.human.currentGameLocation.thisAsAddress.IsPublicallyOpen(false))
                                    {
                                        gameLocation = aiController.human.currentGameLocation;
                                    }
                                }
                            }
                        }

                        //Otherwise use other favourite...
                        if (gameLocation == null)
                        {
                            //Go to nearest fav place if open...
                            NewAddress fav = null;

                            //Get favourite
                            if (aiController.human.favouritePlaces.TryGetValue(preset.desireCategory, out fav))
                            {
                                if (fav.company.openForBusinessDesired && fav.company.openForBusinessActual)
                                {
                                    gameLocation = fav;
                                }
                            }
                        }

                        if(gameLocation == null)
                        {
                            passedInteractable = Toolbox.Instance.FindNearestWithAction(GetFirstAction(null), aiController.human.currentRoom, aiController.human, AIActionPreset.FindSetting.onlyPublic, false, enforcersAllowedEverywhere: preset.allowEnforcersEverywhere, mustContainDesireCategory: true, containDesireCategory: preset.desireCategory);

                            if (passedInteractable != null && passedInteractable.node != null)
                            {
                                gameLocation = passedInteractable.node.gameLocation;
                            }
                        }
                    }

                    //Find favourite/nearest available: If above fails try this. This will include non-commerical places as well as home
                    if (preset.locationOption == AIGoalPreset.LocationOption.nearestAvailable || ((preset.locationOption == AIGoalPreset.LocationOption.commercial || preset.locationOption == AIGoalPreset.LocationOption.commercialDecision) && gameLocation == null))
                    {
                        bool useDesireCategory = false;
                        if (preset.locationOption == AIGoalPreset.LocationOption.commercialDecision || preset.locationOption == AIGoalPreset.LocationOption.commercial) useDesireCategory = true;
                        passedInteractable = Toolbox.Instance.FindNearestWithAction(GetFirstAction(null), aiController.human.currentRoom, aiController.human, AIActionPreset.FindSetting.nonTrespassing, true, enforcersAllowedEverywhere: preset.allowEnforcersEverywhere, mustContainDesireCategory: useDesireCategory, containDesireCategory: preset.desireCategory);

                        if (passedInteractable != null && passedInteractable.node != null)
                        {
                            gameLocation = passedInteractable.node.gameLocation;
                        }
                    }

                    //Citizen's home: Falling back to home should be handled above
                    else if (preset.locationOption == AIGoalPreset.LocationOption.home)
                    {
                        gameLocation = aiController.human.home;
                    }
                }
            }
            catch
            {
                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Error: Unable to pick location: " + name, Actor.HumanDebug.actions);
            }

            try
            {
                //Pick a room
                if(preset.roomOption == AIGoalPreset.RoomOption.none)
                {
                    roomLocation = null;
                }
                else if(preset.roomOption == AIGoalPreset.RoomOption.bedroom)
                {
                    List<Interactable> beds = null;

                    if(aiController.human.home != null && aiController.human.home.actionReference.TryGetValue(CitizenControls.Instance.sleep, out beds))
                    {
                        if(beds.Count > 0)
                        {
                            roomLocation = beds[0].furnitureParent.anchorNode.room;
                        }
                    }
                }
                else if(preset.roomOption == AIGoalPreset.RoomOption.job)
                {
                    if(aiController.human.job != null && aiController && aiController.human.job.employer != null)
                    {
                        if(aiController.human.job.preset.preferredRooms != null && aiController.human.job.preset.preferredRooms.Count > 0)
                        {
                            List<NewRoom> validRooms = new List<NewRoom>();

                            if(aiController.human.job.employer.placeOfBusiness != null)
                            {
                                foreach(RoomConfiguration r in aiController.human.job.preset.preferredRooms)
                                {
                                    roomLocation = aiController.human.job.employer.placeOfBusiness.rooms.Find(item => item.preset == r);
                                    if (roomLocation != null) validRooms.Add(roomLocation);
                                }
                            }

                            if(validRooms.Count > 0)
                            {
                                roomLocation = validRooms[Toolbox.Instance.Rand(0, validRooms.Count)];
                            }
                        }

                        //Otherwise pick a random room at work
                        if(roomLocation == null)
                        {
                            List<NewRoom> randomRoom = new List<NewRoom>();

                            //Pick from random from the building
                            if(aiController.human.job.preset.jobAIPosition == OccupationPreset.JobAI.randomBuilding)
                            {
                                foreach(KeyValuePair<int, NewFloor> pair in aiController.human.job.employer.address.building.floors)
                                {
                                    foreach(NewAddress ad in pair.Value.addresses)
                                    {
                                        if(ad.company != null && ad != aiController.human.job.employer.address)
                                        {
                                            continue; //Skip private companies than aren't this one
                                        }
                                        else if(ad.residence != null)
                                        {
                                            continue; //Skip homes
                                        }

                                        foreach (NewRoom r in ad.rooms)
                                        {
                                            if (r.isNullRoom) continue;
                                            if (aiController.human.job.preset.bannedRooms.Contains(r.preset)) continue; //Ignore banned rooms

                                            randomRoom.Add(r);
                                        }
                                    }
                                }
                            }
                            //Pick from random from the address
                            else
                            {
                                if(aiController.human.job.employer.placeOfBusiness != null)
                                {
                                    foreach (NewRoom r in aiController.human.job.employer.placeOfBusiness.rooms)
                                    {
                                        if (r.isNullRoom) continue;
                                        if (aiController.human.job.preset.bannedRooms.Contains(r.preset)) continue; //Ignore banned rooms

                                        randomRoom.Add(r);
                                    }
                                }

                                if(aiController.human.job.employer.address != null)
                                {
                                    foreach (NewRoom r in aiController.human.job.employer.address.rooms)
                                    {
                                        if (r.isNullRoom) continue;
                                        if (aiController.human.job.preset.bannedRooms.Contains(r.preset)) continue; //Ignore banned rooms

                                        randomRoom.Add(r);
                                    }
                                }
                            }

                            if(randomRoom.Count > 0)
                            {
                                roomLocation = randomRoom[Toolbox.Instance.Rand(0, randomRoom.Count)];
                            }
                            else
                            {
                                Game.LogError("AI " + aiController.human.citizenName + " has no random rooms to choose from...");
                            }
                        }
                    }
                }

                //Pick an interactable
                if(preset.furnitureOption == AIGoalPreset.FurnitureOption.bed)
                {
                    //Search both get ready and sleep interactables...
                    if (aiController.human.sleepPosition != null && !aiController.human.sleepPosition.node.gameLocation.isCrimeScene)
                    {
                        passedInteractable = aiController.human.sleepPosition;
                    }
                    else if(!aiController.human.isHomeless)
                    {
                        Game.LogError("AI: Citizen has no sleep position or their home is a crime scene: " + aiController.human.name + " (isHomeless = " + aiController.human.isHomeless + ")");
                    }
                }
                else if (preset.furnitureOption == AIGoalPreset.FurnitureOption.job)
                {
                    //If the AI has one, use work position
                    if(aiController.human.job != null && aiController.human.job.employer != null)
                    {
                        //Should I own a job position?
                        if(aiController.human.job.preset.ownsWorkPosition)
                        {
                            if (aiController.human.workPosition != null)
                            {
                                passedInteractable = aiController.human.workPosition;
                                gameLocation = aiController.human.workPosition.node.gameLocation;
                                roomLocation = aiController.human.workPosition.node.room;
                            }
                            else
                            {
                                Game.LogError("AI: Citizen should own a job furniture/position but doesn't: " + aiController.human.name + ", " + aiController.human.job.preset.name + " at " + aiController.human.job.employer.name + " (work ad: " + aiController.human.job.employer.address + ", place of business: " + aiController.human.job.employer.placeOfBusiness);
                            }
                        }
                        else
                        {
                            //Dynamically find a work position
                            if(aiController.human.job.preset.jobAIPosition == OccupationPreset.JobAI.workPosition)
                            {
                                NewRoom startRoom = roomLocation;

                                if(startRoom == null && aiController.human.job.employer.placeOfBusiness != null)
                                {
                                    foreach(NewRoom r in aiController.human.job.employer.placeOfBusiness.rooms)
                                    {
                                        if(r != null)
                                        {
                                            startRoom = r;
                                            break;
                                        }
                                    }
                                }

                                //We can use 'all areas' to search here as it's limited by the job game location anyway
                                passedInteractable = Toolbox.Instance.FindNearestWithAction(aiController.human.job.preset.actionSetup[0].actions[0], startRoom, aiController.human, AIActionPreset.FindSetting.allAreas, false, restrictTo: aiController.human.job.employer.placeOfBusiness, useSpecialCasesOnly: true, mustBeSpecial: aiController.human.job.preset.jobPostion, enforcersAllowedEverywhere: preset.allowEnforcersEverywhere);
                            }
                            else if(aiController.human.job.preset.jobAIPosition == OccupationPreset.JobAI.passedCompanyPosition)
                            {
                                passedInteractable = aiController.human.job.employer.passedWorkPosition;
                                gameLocation = passedInteractable.node.gameLocation;
                                roomLocation = passedInteractable.node.room;
                            }
                        }
                    }
                }
            }
            catch
            {
                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Error: Unable to pick room/interactable: " + name, Actor.HumanDebug.actions);
            }

            try
            {
                //Trigger speech
                if (preset.onTriggerBark.Count > 0 && Toolbox.Instance.Rand(0f, 1f) <= preset.chanceOfOnTrigger)
                {
                    if(aiController.human.speechController != null) aiController.human.speechController.TriggerBark(preset.onTriggerBark[Toolbox.Instance.Rand(0, preset.onTriggerBark.Count)]);
                }

                if (gameLocation == null)
                {
                    if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("AI: No GameLocation for goal " + name + " (" + aiController.human.name + "), removing...", Actor.HumanDebug.actions);
                    Remove();
                }
                else
                {
                    RefreshActions(false);
                }
            }
            catch
            {
                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Error: Unable to trigger speech: " + name, Actor.HumanDebug.actions);
            }
        }
    }

    //Refresh actions: Bool is true when this is being refreshed as opposed to activated
    public void RefreshActions(bool refresh = false)
    {
        if (Game.Instance.collectDebugData) aiController.human.SelectedDebug(aiController.human.name + " Refreshing/creating actions for " + preset.name + " Refresh: " + refresh, Actor.HumanDebug.actions);

        //Remove actions including active
        for (int i = 0; i < actions.Count; i++)
        {
            if(!refresh || !actions[i].preset.dontRemoveOnRefresh)
            {
                actions[i].Remove();
                i--;
            }
        }

        string seed = aiController.human.citizenName + SessionData.Instance.gameTime;

        //Create actions
        if (preset.actionSource == AIGoalPreset.GoalActionSource.thisConfiguration)
        {
            //Create new actions from preset
            for (int i = 0; i < preset.actionsSetup.Count; i++)
            {
                if (preset.actionsSetup[i] == null) continue;

                AIActionPreset act = preset.actionsSetup[i].actions[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.actionsSetup[i].actions.Count, seed, out seed)];

                if (refresh && act.nonRefreshable) continue;

                if (act.skipIfAIIsInState)
                {
                    if (aiController.reactionState == act.skipIfReaction)
                    {
                        continue;
                    }
                }

                NewGameLocation loc = gameLocation;
                if (loc == null && passedInteractable != null && passedInteractable.node != null) loc = passedInteractable.node.gameLocation;
                if (loc == null && roomLocation != null) loc = roomLocation.gameLocation;
                if (loc == null && passedNode != null) loc = passedNode.gameLocation;

                float chance = GetActionChance(preset.actionsSetup[i], loc);

                if(chance <= 0f)
                {
                    if (Game.Instance.collectDebugData) aiController.human.SelectedDebug(aiController.human.name + " Chance for action " + act.name + " is 0, did you mean for this?", Actor.HumanDebug.actions);
                }

                if (chance == 1f || Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) <= chance)
                {
                   aiController.CreateNewAction(this, act, newPassedInteractable: passedInteractable, newPassedRoom: roomLocation, newForcedNode: passedNode, newPassedGroup: passedGroup);
                }
            }
        }
        else if (preset.actionSource == AIGoalPreset.GoalActionSource.jobPreset)
        {
            //Create new actions from job preset
            if (aiController.human.job != null && aiController.human.job.preset != null)
            {
                for (int i = 0; i < aiController.human.job.preset.actionSetup.Count; i++)
                {
                    if (aiController.human.job.preset.actionSetup[i] == null) continue;

                    AIActionPreset act = aiController.human.job.preset.actionSetup[i].actions[Toolbox.Instance.GetPsuedoRandomNumberContained(0, aiController.human.job.preset.actionSetup[i].actions.Count, seed, out seed)];

                    if (refresh && act.nonRefreshable) continue;

                    if (act.skipIfAIIsInState)
                    {
                        if (aiController.reactionState == act.skipIfReaction)
                        {
                            continue;
                        }
                    }

                    NewGameLocation loc = gameLocation;
                    if (loc == null && passedInteractable != null && passedInteractable.node != null) loc = passedInteractable.node.gameLocation;
                    if (loc == null && roomLocation != null) loc = roomLocation.gameLocation;
                    if (loc == null && passedNode != null) loc = passedNode.gameLocation;

                    float chance = GetActionChance(aiController.human.job.preset.actionSetup[i], loc);

                    if (chance == 1f || Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) <= chance)
                    {
                        aiController.CreateNewAction(this, act, newPassedInteractable: passedInteractable, newPassedRoom: roomLocation, newForcedNode: passedNode, newPassedGroup: passedGroup);
                    }
                }
            }
        }
        else if (preset.actionSource == AIGoalPreset.GoalActionSource.murderPreset)
        {
            if (Game.Instance.collectDebugData) aiController.human.SelectedDebug(aiController.human.name + " Read actions from murder controller...", Actor.HumanDebug.actions);

            List<AIGoalPreset.GoalActionSetup> actionList = null;

            Interactable pInteractable = passedInteractable;
            NewRoom pRoom = roomLocation;
            List<InteractablePreset> passedAcquireItems = null;

            if (murderRef == null)
            {
                Game.LogError("AI goal " + preset.name + " is expecting a passed murder ref to set up the action references, but it doesn't have one...");
            }
            else
            {
                if (murderRef.state == MurderController.MurderState.acquireEuipment)
                {
                    if (!murderRef.preset.acquirePassInteractable) pInteractable = null;
                    if (!murderRef.preset.acquirePassRoom) pRoom = null;

                    //Add acquire item action
                    passedAcquireItems = new List<InteractablePreset>();

                    //If not in inventory?
                    if (!murderRef.murderer.inventory.Exists(item => item.preset == murderRef.weaponPreset))
                    {
                        Interactable weaponAtHome = Toolbox.Instance.FindClosestObjectTo(murderRef.weaponPreset, murderRef.murderer.FindSafeTeleport(murderRef.murderer.home).position, murderRef.murderer.home.building, murderRef.murderer.home, null, out _);

                        //Pick this up...
                        if(weaponAtHome != null)
                        {
                            Game.Log("Murder: Found weapon at home, inserting a pick up command...");
                            PickUpItem(weaponAtHome);
                        }
                        else
                        {
                            //Attempt to buy
                            Game.Log("Murder: Attempting to buy weapon...");
                            passedAcquireItems.Add(murderRef.weaponPreset);
                        }
                    }
                    else
                    {
                        Game.Log("Murder: The killer already has this item in their inventory...");
                    }

                    if (murderRef.ammoPreset != null)
                    {
                        //Game.Log("Murder: Ammo preset: " + murderRef.ammoPreset);

                        if (!murderRef.murderer.inventory.Exists(item => item.preset == murderRef.ammoPreset))
                        {
                            Game.Log("Murder: Ammo preset is not in killer's inventory...");
                            Interactable ammoAtHome = Toolbox.Instance.FindClosestObjectTo(murderRef.ammoPreset, murderRef.murderer.FindSafeTeleport(murderRef.murderer.home).position, murderRef.murderer.home.building, murderRef.murderer.home, null, out _);

                            //Pick this up...
                            if (ammoAtHome != null)
                            {
                                Game.Log("Murder: Found weapon at home, inserting a pick up command...");
                                PickUpItem(ammoAtHome);
                            }
                            else
                            {
                                //Attempt to buy
                                Game.Log("Murder: Attempting to buy ammo...");
                                passedAcquireItems.Add(murderRef.ammoPreset);
                            }
                        }
                        else
                        {
                            Game.Log("Murder: The killer already has ammo in their inventory...");
                        }
                    }
                    else Game.Log("Murder: The killer does not need ammunition for this weapon type...");

                    //Go and purchase items...
                    if (passedAcquireItems.Count > 0)
                    {
                        if(Game.Instance.collectDebugData)
                        {
                            string aq = "Murder: Killer acquiring items:";

                            foreach (InteractablePreset p in passedAcquireItems)
                            {
                                aq += " " + p.name;
                            }

                            Game.Log(aq);
                        }

                        murderRef.weaponSource = Toolbox.Instance.FindNearestThatSells(passedAcquireItems[0], murderRef.murderer.home);

                        if(murderRef.weaponSource != null)
                        {
                            murderRef.weaponSourceID = murderRef.weaponSource.companyID;

                            //Intercept with place to buy weapon
                            pInteractable = Toolbox.Instance.FindNearestWithAction(RoutineControls.Instance.purchaseItem, murderRef.murderer.FindSafeTeleport(murderRef.weaponSource.placeOfBusiness).room, aiController.human, AIActionPreset.FindSetting.allAreas, false, restrictTo: murderRef.weaponSource.placeOfBusiness);

                            if (pInteractable != null)
                            {
                                pRoom = pInteractable.node.room;
                                Game.Log("Murder: Found passed interactable for murder weapon/ammo purchase: " + pInteractable.name);
                            }
                            else
                            {
                                Game.Log("Murder: Unable to find purchase point");
                            }

                            actionList = murderRef.preset.acquireActionSetup;
                        }
                        else
                        {
                            Game.Log("Murder: Unable to find weapon source that sells item, spawning a weapon instead...");

                            Interactable spawned = InteractableCreator.Instance.CreateWorldInteractable(passedAcquireItems[0], aiController.human, aiController.human, aiController.human, aiController.human.transform.position, Vector3.zero, null, null);

                            //Pick up
                            if (spawned != null)
                            {
                                Game.Log("Murder: Spanwed and added to inventory...");
                                spawned.SetInInventory(aiController.human);
                            }
                        }
                    }
                    //Otherwise grab them from home...
                    else
                    {
                        Game.Log("Murder: The killer doesn't need to purchase anything new...");
                    }
                }
                else if (murderRef.state == MurderController.MurderState.research)
                {
                    if (!murderRef.preset.researchPassInteractable) pInteractable = null;
                    if (!murderRef.preset.researchPassRoom) pRoom = null;

                    actionList = murderRef.preset.researchActionSetup;
                }
                else if (murderRef.state == MurderController.MurderState.travellingTo)
                {
                    if (!murderRef.preset.travelPassInteractable) pInteractable = null;
                    if (!murderRef.preset.travelPassRoom) pRoom = null;

                    actionList = murderRef.preset.travelActionSetup;
                }
                else if (murderRef.state == MurderController.MurderState.executing)
                {
                    if (!murderRef.preset.executePassInteractable) pInteractable = null;
                    if (!murderRef.preset.executePassRoom) pRoom = null;

                    actionList = murderRef.preset.executionActionSetup;
                }
                else if (murderRef.state == MurderController.MurderState.post)
                {
                    if (!murderRef.preset.postPassInteractable) pInteractable = null;
                    if (!murderRef.preset.postPassRoom) pRoom = null;

                    actionList = murderRef.preset.postActionSetup;
                }
                else if (murderRef.state == MurderController.MurderState.escaping)
                {
                    if (!murderRef.preset.escapePassInteractable) pInteractable = null;
                    if (!murderRef.preset.escapePassRoom) pRoom = null;

                    actionList = murderRef.preset.escapeActionSetup;
                }
            }

            if (actionList != null)
            {
                //Create new actions from murder preset
                for (int i = 0; i < actionList.Count; i++)
                {
                    if (actionList[i] == null)
                    {
                        continue;
                    }

                    AIActionPreset act = actionList[i].actions[Toolbox.Instance.GetPsuedoRandomNumberContained(0, actionList[i].actions.Count, seed, out seed)];

                    if (refresh && act.nonRefreshable) continue;

                    if (act.skipIfAIIsInState)
                    {
                        if (aiController.reactionState == act.skipIfReaction)
                        {
                            continue;
                        }
                    }

                    NewGameLocation loc = gameLocation;
                    if (loc == null && passedInteractable != null && passedInteractable.node != null) loc = passedInteractable.node.gameLocation;
                    if (loc == null && roomLocation != null) loc = roomLocation.gameLocation;
                    if (loc == null && passedNode != null) loc = passedNode.gameLocation;

                    float chance = GetActionChance(actionList[i], loc);

                    if (chance == 1f || Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) <= chance)
                    {
                        aiController.CreateNewAction(this, act, newPassedInteractable: pInteractable, newPassedRoom: pRoom, newForcedNode: passedNode, newPassedGroup: passedGroup, newPassedAcquireItems: passedAcquireItems);
                    }
                }
            }
        }
    }

    //Triggered when this goal becomes inactive
    public void OnDeactivate(float delayReactivationTime)
    {
        //Duty toggle
        aiController.human.isOnDuty = false;

        if (isActive)
        {
            //Make sure to deactivate actions
            if (aiController.currentAction != null)
            {
                aiController.currentAction.OnDeactivate();
            }
            else if (actions.Count > 0 && actions[0].isActive)
            {
                actions[0].OnDeactivate();
            }

            if (delayReactivationTime > 0f && preset != RoutineControls.Instance.fleeGoal && preset != RoutineControls.Instance.investigateGoal)
            {
                if(!aiController.delayedGoalsForTime.ContainsKey(preset))
                {
                    aiController.delayedGoalsForTime.Add(preset, 0f);
                }

                aiController.delayedGoalsForTime[preset] = SessionData.Instance.gameTime + delayReactivationTime;
            }

            if (Game.Instance.collectDebugData)
            {
                aiController.human.SelectedDebug("Deactivate Goal: " + name, Actor.HumanDebug.actions);
                aiController.AddDebugAction("Deactivate Goal: " + name);
            }

            isActive = false;
            aiController.currentGoal = null;
            aiController.currentAction = null;
            gameLocation = null;
            searchProgress = 0;
            searchedNodes.Clear();

            //Remove current actions
            actions.Clear();
        }

        aiController.UpdateHeldItems(AIActionPreset.ActionStateFlag.onGoalDeactivation);
    }

    //Triggered on tick
    public void AITick()
    {
        if (!SessionData.Instance.startedGame) return; //Don't activate AI before game start

        if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Goal AITick: " + preset.name, Actor.HumanDebug.updates);

        InsertActionsCheck();
        if(preset.allowPottering) PotterCheck();

        //Disable checking pulse
        if(preset == RoutineControls.Instance.findDeadBody)
        {
            if(passedInteractable != null)
            {
                if(passedInteractable.isActor != null)
                {
                    if(!passedInteractable.isActor.isStunned)
                    {
                        OnDeactivate(preset.repeatDelayOnFinishActions);
                    }
                }
            }
        }

        if(actions.Count > 0)
        {
            //This will execute a sorted action list...
            if (aiController.currentAction != actions[0])
            {
                //Deactivate any previous actions
                if(aiController.currentAction != null)
                {
                    if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Deactivating current action " + aiController.currentAction.name + " because priority of " + actions[0].name + " is higher...", Actor.HumanDebug.actions);
                    aiController.currentAction.OnDeactivate();
                }

                if (actions.Count > 0)
                {
                    //Wake up if needed
                    if (!actions[0].preset.sleepOnArrival)
                    {
                        if (aiController.human.isAsleep)
                        {
                            //Game.Log("Wake up: " + preset.name);
                            aiController.human.WakeUp();
                            return;
                        }
                    }

                    actions[0].OnActivate();
                }
            }

            //Action tick
            if (aiController.currentAction != null) aiController.currentAction.AITick();
            else if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Current action is null! There are " + actions.Count + " queued actions...", Actor.HumanDebug.actions);
        }
        //If no actions, deactivate this
        else
        {
            if(preset.loopingActions)
            {
                RefreshActions(true);
            }
            else
            {
                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Deactivating goal as there are no actions...", Actor.HumanDebug.actions);

                OnDeactivate(preset.repeatDelayOnFinishActions);

                //Complete goal
                if (preset.completable)
                {
                    Complete();
                }
            }
        }
    }

    //Check if lights are the required setting, and if not, create an action to change that...
    public void InsertActionsCheck()
    {
        //Should I raise the alarm?
        if (preset.raiseAlarm && aiController.human.lastScaredBy != null && (aiController.human.lastScaredBy as Human) != null && aiController.human.lastScaredAt == aiController.human.currentGameLocation)
        {
            bool confined = false;

            //Don't do this if confined to location
            if (aiController.confineLocation != null)
            {
                confined = true;
            }

            if (aiController.avoidLocations != null && aiController.avoidLocations.Count > 0)
            {
                confined = true;
            }

            if (aiController.victimsForMurders.Count > 0)
            {
                foreach (MurderController.Murder m in aiController.victimsForMurders)
                {
                    if (m.location == null) continue;
                    if ((int)m.state < 4) continue;

                    if (m.preset.blockVictimFromLeavingLocation)
                    {
                        confined = true;
                    }
                }
            }

            //Raise the alarm if
            //1) I have seen the player in an illegal situtation
            //2) Where I saw the player is the same alarm as this building
            //3) I'm fleeing/done fighting
            //4) ...Or somebody else is fighting them
            if (/*aiController.inCombat ||*/ !confined && aiController.inFleeState && aiController.human.lastScaredAt != null && aiController.human.lastScaredAt.building != null)
            {
                //Game.Log("AI in combat or fleeing with raise alarm state...");
                bool useBuildingAlarm = true;

                if(aiController.human.lastScaredAt.thisAsAddress != null && aiController.human.lastScaredAt.thisAsAddress.addressPreset != null && aiController.human.lastScaredAt.thisAsAddress.addressPreset.useOwnSecuritySystem)
                {
                    useBuildingAlarm = false;
                }

                if (!aiController.human.currentGameLocation.IsAlarmActive(out _, out _, out _) || (useBuildingAlarm && (aiController.human.lastScaredAt.floor != null && !aiController.human.lastScaredAt.floor.alarmLockdown)))
                {
                    //Game.Log("AI current has building with no alarm active/or this floor isn't locked down...");

                    bool clearForAlarm = true;

                    //If I'm in combat, is anybody else activating the alarm?
                    if (aiController.inCombat)
                    {
                        clearForAlarm = false; //Don't go for alarm by default if in combat...

                        foreach (Actor act in Player.Instance.witnessesToIllegalActivity)
                        {
                            if (act == aiController.human) continue; //Don't count me
                            if (act.isDead) continue;

                            if (act.ai != null)
                            {
                                if (act.ai.currentAction != null)
                                {
                                    if (act.ai.currentAction.preset == RoutineControls.Instance.raiseAlarm)
                                    {
                                        clearForAlarm = false;
                                        //Game.Log("AI: Somebody else is already activating the alarm: " + act.name);
                                        break;
                                    }
                                    else
                                    {
                                        //Somebody else is fighting, but nobody is going for the alarm...
                                        clearForAlarm = true;
                                    }
                                }
                            }
                        }
                    }

                    if (clearForAlarm)
                    {
                        if(!actions.Exists(item => item.preset == RoutineControls.Instance.raiseAlarm))
                        {
                            NewBuilding restrictBuilding = null;
                            NewGameLocation restrictLocation = aiController.human.lastScaredAt;

                            if(useBuildingAlarm)
                            {
                                restrictBuilding = aiController.human.lastScaredAt.building;
                                restrictLocation = null;
                            }

                            Interactable foundAction = Toolbox.Instance.FindNearestWithAction(RoutineControls.Instance.raiseAlarm, aiController.human.currentRoom, aiController.human, AIActionPreset.FindSetting.nonTrespassing, restrictTo: restrictLocation, restrictToBuilding: restrictBuilding, enforcersAllowedEverywhere: preset.allowEnforcersEverywhere);

                            if (foundAction != null)
                            {
                                //Game.Log("Try Insert raise alarm: " + aiController.human.name);
                                TryInsertInteractableAction(foundAction, RoutineControls.Instance.raiseAlarm, 11, duplicateActionCheck: false);
                            }
                            else
                            {
                                Game.Log("AI: Tried to find a raise alarm panel, but there is none found!");
                            }
                        }
                    }
                }
            }
        }

        //Only allow the following if not in combat
        if(!preset.disableActionInsertions && (!aiController.inCombat || (aiController.currentAction != null && (aiController.currentAction.preset == RoutineControls.Instance.searchArea || aiController.currentAction.preset == RoutineControls.Instance.searchAreaEnforcer))))
        {
            //Where am I going? Find the next location that isn't switching lights on/off or any other the other insertable actions...
            NewRoom nextLocation = null;
            NewAIAction nextAction = null; //The next 'proper' action, ie not inserted

            for (int i = 0; i < actions.Count; i++)
            {
                if (!actions[i].isActive) continue;
                if (actions[i].insertedAction) continue;

                if (actions[i].node == null)
                {
                    break;
                }

                nextAction = actions[i];
                nextLocation = actions[i].node.room;
                break;
            }

            if (nextLocation == null || nextAction == null) return;

            //Set this flag, it might be useful for something?
            nextAction.checkedForInsertions = true;

            //Put down inventory items at home
            if(aiController.human.currentGameLocation == aiController.human.home)
            {
                //I'm leaving this location
                if(nextAction.node.gameLocation != aiController.human.currentGameLocation)
                {
                    //Pick up item
                    if(aiController.putDownItems.Count > 0)
                    {
                        for (int i = 0; i < aiController.putDownItems.Count; i++)
                        {
                            Interactable inv = aiController.putDownItems[i];

                            if(inv.rem)
                            {
                                aiController.putDownItems.RemoveAt(i);
                                i--;
                                continue;
                            }

                            //If the put down item is where I am...
                            try
                            {
                                if(aiController.human.currentGameLocation == inv.node.gameLocation)
                                {
                                    //But I'm going elsewhere...
                                    if(nextAction.node.gameLocation != inv.node.gameLocation)
                                    {
                                        PickUpItem(inv);
                                    }
                                }
                            }
                            catch
                            {

                            }

                        }
                    }
                }
                //I'm at home
                else
                {
                    foreach(Interactable i in aiController.human.inventory)
                    {
                        if(i.preset.putDownAtHome)
                        {
                            PutDownItem(i, aiController.human.home);
                        }
                    }
                }
            }

            //If I'm at work or home, make sure entrance is unlocked/locked appropriately...
            if (nextAction.preset.doorsAllowed)
            {
                if (aiController.human.isAtWork)
                {
                    if (!aiController.human.job.preset.selfEmployed && aiController.human.job.employer.placeOfBusiness.thisAsAddress != null)
                    {
                        foreach (NewNode.NodeAccess acc in aiController.human.job.employer.placeOfBusiness.entrances)
                        {
                            if (acc.door != null)
                            {
                                if (aiController.human.job.employer.openForBusinessDesired && acc.door.isLocked)
                                {
                                    TryInsertDoorAction(acc.door, RoutineControls.Instance.unlockDoor, DoorSide.mySide, 3, out _, debug: "Unlock: AtWork - opening time");
                                }
                                else if (!aiController.human.job.employer.openForBusinessDesired //Closing time
                                    && !acc.door.isLocked //Door isn't locked
                                    && (aiController.human.job.employer.placeOfBusiness.thisAsAddress.addressPreset == null || !aiController.human.job.employer.placeOfBusiness.thisAsAddress.addressPreset.disableLockingUp) //Locking up; not disabled
                                    && !aiController.human.job.employer.placeOfBusiness.currentOccupants.Exists(item => !item.isAtWork && !item.isTrespassing)) //Nobody here who isn't at work
                                {
                                    TryInsertDoorAction(acc.door, RoutineControls.Instance.lockDoor, DoorSide.forceCurrentOtherSide, 3, out _, debug: "Lock: AtWork - closing time");
                                }
                            }
                        }
                    }
                }
                else if (aiController.human.isHome)
                {
                    //Only do this if there is no guest pass for the player
                    if(!GameplayController.Instance.guestPasses.ContainsKey(aiController.human.home))
                    {
                        if (aiController.human.isAsleep)
                        {
                            foreach (NewNode.NodeAccess acc in aiController.human.home.entrances)
                            {
                                if (acc.wall == null) continue;

                                if (acc.wall.door != null)
                                {
                                    if (!acc.wall.door.isClosed)
                                    {
                                        if (aiController.human.CanISee(acc.wall.door.doorInteractable))
                                        {
                                            //Get the inside node
                                            NewNode insideNode = acc.wall.node;
                                            if (acc.wall.otherWall.node.gameLocation == aiController.human.home) insideNode = acc.wall.otherWall.node;

                                            bool answerPass = true;

                                            //This inside node is in the same room or an open door away from the actor
                                            if (!aiController.human.currentRoom.nodes.Contains(insideNode))
                                            {
                                                bool adjCheck = false;

                                                foreach (NewNode.NodeAccess adj in aiController.human.currentRoom.entrances)
                                                {
                                                    if (adj.accessType == NewNode.NodeAccess.AccessType.openDoorway)
                                                    {
                                                        if (adj.toNode.room.nodes.Contains(insideNode))
                                                        {
                                                            adjCheck = true;
                                                            break;
                                                        }
                                                    }
                                                }

                                                if (!adjCheck) answerPass = false;
                                            }

                                            if (answerPass)
                                            {
                                                //There is no housemate on this tile (ie they're not anwering the door)
                                                foreach (Actor owner in aiController.human.home.owners)
                                                {
                                                    if (owner.currentNode == insideNode && owner.interactingWith != null)
                                                    {
                                                        answerPass = false;
                                                        break;
                                                    }
                                                }
                                            }

                                            //Locking a door also closes it...
                                            if (answerPass)
                                            {
                                                TryInsertDoorAction(acc.wall.door, RoutineControls.Instance.lockDoor, DoorSide.mySide, 3, out _, insideNode, "AnswerDoor");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                }
            }

            //Only do this if not a street && can't see player while on persuit
            if (nextAction.node.gameLocation.thisAsAddress != null && !aiController.seesOnPersuit)
            {
                //Turn off illegal items
                //Most goals will be able to disable this- but we need the ai to be able to do this before going to sleep (enabling would result in them waking up to deactivate)
                if (!aiController.human.isAsleep && nextAction.preset.deactivateAllowed)
                {
                    //Close doors: Only do this indoors @ own home or working at business
                    if (aiController.human.locationsOfAuthority.Contains(aiController.human.currentGameLocation))
                    {
                        //Only do these if at home, or at workplace
                        //Deactive within this room
                        if (aiController.human.currentRoom != null && (preset.category == AIGoalPreset.GoalCategory.trivial || preset.category == AIGoalPreset.GoalCategory.important))
                        {
                            foreach (Interactable illegallyOn in aiController.human.currentRoom.tamperedInteractables)
                            {
                                if (aiController.human.CanISee(illegallyOn))
                                {
                                    DeactivateInteractable(illegallyOn);
                                }
                            }
                        }

                        //Only do these if at home, or at workplace
                        doorCheckCycle++;

                        if (doorCheckCycle >= 16)
                        {
                            if (aiController.human.currentRoom.roomType.doorSetting == NewDoor.DoorSetting.leaveClosed)
                            {
                                AIActionPreset.DoorRule doorRule = preset.doorRule;
                                if (nextAction.preset.overrideGoalDoorRule) doorRule = nextAction.preset.doorRule;

                                foreach (NewDoor openDoor in aiController.human.currentRoom.openDoors)
                                {
                                    //Skip doors that I'm travelling through as to not get caught in a loop!
                                    if (openDoor == aiController.openedDoor) continue;
                                    if (doorRule == AIActionPreset.DoorRule.dontClose) continue;

                                    if (doorRule == AIActionPreset.DoorRule.onlyCloseToLocation)
                                    {
                                        if (openDoor.wall.node.gameLocation == openDoor.wall.otherWall.node.gameLocation)
                                        {
                                            continue;
                                        }
                                    }

                                    if (!openDoor.isClosed && !aiController.dontEverCloseDoors)
                                    {
                                        //Can I see it?
                                        if (aiController.human.CanISee(openDoor.doorInteractable))
                                        {
                                            TryInsertDoorAction(openDoor, RoutineControls.Instance.closeDoor, DoorSide.mySide, 0, out _, debug: "CloseDoors");
                                        }
                                    }
                                }

                            }

                            doorCheckCycle = 0;
                        }
                    }
                }
            }

            //Keep lights on in this room/gamelocation
            if(aiController.human.currentGameLocation.thisAsAddress != null && aiController.human.currentRoom != null)
            {
                NewAIAction properAction = aiController.currentAction;
                if (properAction.insertedAction) properAction = nextAction;

                //Turn all lights off
                if (aiController.currentAction.preset.turnAllGamelocationLightsOff && properAction.node.gameLocation == aiController.human.currentGameLocation)
                {
                    foreach (NewRoom r in aiController.human.currentGameLocation.rooms)
                    {
                        if (r == properAction.node.room) continue;

                        NewAIAction existingAction = actions.Find(item => (item.preset == RoutineControls.Instance.mainLightOff || item.preset == RoutineControls.Instance.mainLightOn || item.preset == RoutineControls.Instance.secondaryLightOn || item.preset == RoutineControls.Instance.secondaryLightOff) && item.passedRoom == r);
                        if (existingAction != null) existingAction.Remove(existingAction.preset.repeatDelayOnActionFail);

                        if (IsLastOccupantOfRoom(r, true))
                        {
                            RoomLightingCheck(r, RoomConfiguration.AILightingBehaviour.LightingPreference.allOff);
                        }
                    }
                }

                List<RoomConfiguration.AILightingBehaviour> lightingBehaviour = null;

                if (aiController.currentAction != null && aiController.currentAction.preset.overrideGoalLightRule && (!aiController.currentAction.preset.onlyOverrideIfAtGamelocation || (aiController.currentAction.node != null && aiController.human.currentGameLocation == aiController.currentAction.node.gameLocation)))
                {
                    lightingBehaviour = aiController.currentAction.preset.lightingBehaviour;
                }
                else if (aiController.currentGoal != null && aiController.currentGoal.preset.overrideLightingBehaviour && (!aiController.currentGoal.preset.onlyOverrideIfAtGamelocation || (aiController.currentAction.node != null && aiController.human.currentGameLocation == aiController.currentAction.node.gameLocation)))
                {
                    lightingBehaviour = aiController.currentGoal.preset.lightingBehaviour;
                }
                else
                {
                    lightingBehaviour = aiController.human.currentRoom.preset.lightingBehaviour;
                }

                if(lightingBehaviour != null && lightingBehaviour.Count > 0)
                {
                    //Find which rule is relevant based on time of day...
                    RoomConfiguration.AILightingBehaviour relevantRule = lightingBehaviour[0];

                    if(lightingBehaviour.Count > 1)
                    {
                        foreach(RoomConfiguration.AILightingBehaviour r in lightingBehaviour)
                        {
                            if(r.dayRule == RoomConfiguration.AILightingBehaviour.TimeOfDay.always)
                            {
                                relevantRule = r;
                                break;
                            }
                            else if(r.dayRule == RoomConfiguration.AILightingBehaviour.TimeOfDay.daytime && SessionData.Instance.dayProgress > 0.25f && SessionData.Instance.dayProgress < 0.75f)
                            {
                                relevantRule = r;
                                break;
                            }
                            else
                            {
                                relevantRule = r;
                                break;
                            }
                        }
                    }

                    //Arrvied at the destination
                    if (properAction.node.room == aiController.human.currentRoom && !aiController.seesOnPersuit)
                    {
                        //Arrvied at the destination; make sure my preference is executed
                        RoomLightingCheck(aiController.human.currentRoom, relevantRule.destinationBehaviour); //Passing by room is higher priority than passing by action
                        arrivedRoom = aiController.human.currentRoom;
                    }
                    else
                    {
                        //Arrived in a new room (not action destination): Do this if current action isn't inserted
                        if (arrivedRoom != aiController.human.currentRoom && !aiController.seesOnPersuit && !aiController.currentAction.insertedAction)
                        {
                            RoomLightingCheck(aiController.human.currentRoom, relevantRule.passthroughBehaviour);
                            arrivedRoom = aiController.human.currentRoom;
                        }

                        //I'm leaving the room
                        if (properAction.node.room != aiController.human.currentRoom)
                        {
                            //I'm leaving the gamelocation
                            if(properAction.node.gameLocation != aiController.human.currentGameLocation)
                            {
                                //Only do this for locations of authority
                                if(aiController.human.locationsOfAuthority.Contains(aiController.human.currentGameLocation))
                                {
                                    if (relevantRule.exitGameLocationBehaviour != RoomConfiguration.AILightingBehaviour.LightingPreference.none)
                                    {
                                        if(IsLastOccupantOfGameLocation(aiController.human.currentGameLocation, true))
                                        {
                                            foreach (NewRoom r in aiController.human.currentGameLocation.rooms)
                                            {
                                                NewAIAction existingAction = actions.Find(item => (item.preset == RoutineControls.Instance.mainLightOff || item.preset == RoutineControls.Instance.mainLightOn || item.preset == RoutineControls.Instance.secondaryLightOn || item.preset == RoutineControls.Instance.secondaryLightOff) && item.passedRoom == r);
                                                if (existingAction != null) existingAction.Remove(existingAction.preset.repeatDelayOnActionFail);
                                                RoomLightingCheck(r, relevantRule.exitGameLocationBehaviour);
                                            }
                                        }
                                    }
                                }
                            }
                            //I'm just leaving the room
                            else
                            {
                                if(relevantRule.exitRoomBehaviour != RoomConfiguration.AILightingBehaviour.LightingPreference.none)
                                {
                                    NewAIAction existingAction = actions.Find(item => (item.preset == RoutineControls.Instance.mainLightOff || item.preset == RoutineControls.Instance.mainLightOn || item.preset == RoutineControls.Instance.secondaryLightOn || item.preset == RoutineControls.Instance.secondaryLightOff) && item.passedRoom == aiController.human.currentRoom);
                                    if (existingAction != null) existingAction.Remove(existingAction.preset.repeatDelayOnActionFail);

                                    if (IsLastOccupantOfRoom(aiController.human.currentRoom, true))
                                    {
                                        RoomLightingCheck(aiController.human.currentRoom, relevantRule.exitRoomBehaviour);
                                    }
                                }
                            }
                        }
                    }

                }
            }

            //Check behind if spooked
            if(aiController.spooked >= 0.95f && aiController.human.isMoving && aiController.reactionState == NewAIController.ReactionState.none)
            {
                if (!actions.Exists(item => item.preset == RoutineControls.Instance.lookBehindSpooked))
                {
                    Game.Log("LOOK BEHIND " + aiController.human.name);
                    aiController.spookCounter++;
                    aiController.spookCounter = Mathf.Clamp(aiController.spookCounter, 0, 2);
                    Game.Log("Spook counter: " + aiController.spookCounter);
                    aiController.CreateNewAction(this, RoutineControls.Instance.lookBehindSpooked, true, null, newInsertedActionPriority: 7);
                }
            }

            //Pick up suspicious items
            if (aiController.reactionState == NewAIController.ReactionState.none || aiController.reactionState == NewAIController.ReactionState.searching)
            {
                //Only if this goal is trivial
                if(preset.category == AIGoalPreset.GoalCategory.trivial || preset.category == AIGoalPreset.GoalCategory.important)
                {
                    //Light source is enabled in this room, or it's the street
                    if(aiController.human.isOnStreet || (aiController.human.currentRoom.mainLightStatus || aiController.human.currentRoom.secondaryLightStatus))
                    {
                        //I have authroity here
                        if(aiController.human.locationsOfAuthority.Contains(aiController.human.currentGameLocation))
                        {
                            //One at a time only
                            if (aiController.currentGoal != null && !aiController.currentGoal.actions.Exists(item => item.preset == RoutineControls.Instance.pickupFromFloor))
                            {
                                //Loop world objects in this room
                                foreach(Interactable i in aiController.human.currentRoom.worldObjects)
                                {
                                    if(i.PickUpTarget(aiController.human))
                                    {
                                        //Vision test
                                        bool canSee = aiController.human.CanISee(i);

                                        if (canSee)
                                        {
                                            Game.Log("AI PICKUP " + i.name);

                                            TryInsertInteractableAction(i, RoutineControls.Instance.pickupFromFloor, 4, i.node, true);
                                            break; //One at a time only
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Trash disposal
            if (aiController.human.trash.Count > 0 && !aiController.human.isLitterBug)
            {
                for (int i = 0; i < aiController.human.trash.Count; i++)
                {
                    MetaObject t = CityData.Instance.FindMetaObject(aiController.human.trash[i]);

                    if(t != null)
                    {
                        InteractablePreset p = null;

                        if (Toolbox.Instance.objectPresetDictionary.TryGetValue(t.preset, out p))
                        {
                            //Check compatible...
                            if (p.disposal == Human.DisposalType.homeOnly && !aiController.human.isHome) continue;
                            else if (p.disposal == Human.DisposalType.workOnly && aiController.human.job.employer != null && !aiController.human.isAtWork) continue;
                            else if (p.disposal == Human.DisposalType.homeOrWork && !aiController.human.isHome && !aiController.human.isAtWork) continue;

                            if (!actions.Exists(item => item.preset == RoutineControls.Instance.disposal))
                            {
                                Interactable foundAction = Toolbox.Instance.FindNearestWithAction(RoutineControls.Instance.disposal, aiController.human.currentRoom, aiController.human, AIActionPreset.FindSetting.nonTrespassing, restrictTo: aiController.human.currentGameLocation, enforcersAllowedEverywhere: preset.allowEnforcersEverywhere);

                                if (foundAction != null)
                                {
                                    if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Added dispose action: " + foundAction.name + ", " + foundAction.node.gameLocation.name, Actor.HumanDebug.actions);

                                    //Game.Log("Try Insert raise alarm: " + aiController.human.name);
                                    if (TryInsertInteractableAction(foundAction, RoutineControls.Instance.disposal, 8, duplicateActionCheck: false))
                                    {
                                        break; //If action is inserted, break loop through trash
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Police put up police tape at crime scenes
            if(aiController.human.isEnforcer && aiController.human.isOnDuty)
            {
                //Responsing to crime scene
                foreach(KeyValuePair<NewGameLocation, GameplayController.EnforcerCall> pair in GameplayController.Instance.enforcerCalls)
                {
                    if (pair.Value == null || pair.Key == null) continue;

                    //Is responding to this...
                    if(pair.Value.state == GameplayController.EnforcerCallState.arrived)
                    {
                        if(pair.Key.thisAsAddress != null)
                        {
                            //The guard puts the police tape up
                            if (pair.Value.response.Contains(aiController.human.humanID) && preset == RoutineControls.Instance.enforcerGuardDuty)
                            {
                                //Check entrance for police tape...
                                foreach (NewNode.NodeAccess acc in pair.Key.entrances)
                                {
                                    if (acc.walkingAccess && acc.door != null)
                                    {
                                        if (!acc.door.forbiddenForPublic)
                                        {
                                            if (!pair.Key.currentOccupants.Exists(item => item.ai != null && item.ai.currentGoal != null && item.ai.currentGoal.actions.Exists(item2 => item2.preset == RoutineControls.Instance.putUpPoliceTape && item2.interactable == acc.door.doorInteractable)))
                                            {
                                                TryInsertInteractableAction(acc.door.doorInteractable, RoutineControls.Instance.putUpPoliceTape, 5, duplicateActionCheck: false);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (pair.Key.thisAsStreet != null)
                        {
                            //The guard puts the police tape up
                            if (pair.Value.response.Contains(aiController.human.humanID) && preset == RoutineControls.Instance.enforcerGuardDuty)
                            {
                                foreach(MurderController.Murder m in MurderController.Instance.activeMurders)
                                {
                                    if(m.location == pair.Key && m.location.isCrimeScene)
                                    {
                                        if(m.victim != null)
                                        {
                                            if(m.victim.isDead)
                                            {
                                                bool needsCrimeScene = true;

                                                foreach(NewRoom r in m.location.rooms)
                                                {
                                                    foreach(NewNode n in r.nodes)
                                                    {
                                                        if(n.interactables.Exists(item => item.preset == InteriorControls.Instance.streetCrimeScene))
                                                        {
                                                            needsCrimeScene = false;
                                                            break;
                                                        }
                                                    }
                                                }

                                                if(needsCrimeScene) TryInsertInteractableAction(m.victim.interactable, RoutineControls.Instance.putUpStreetCrimeScene, 5, duplicateActionCheck: false);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //Place an item down somewhere at home
    public void PutDownItem(Interactable inventoryItem, NewGameLocation location)
    {
        //No existing actions for me
        if (!actions.Exists(item => item.preset == RoutineControls.Instance.AIPutDownItem && item.passedInteractable == inventoryItem))
        {
            aiController.CreateNewAction(this, RoutineControls.Instance.AIPutDownItem, true, newPassedInteractable: inventoryItem, newInsertedActionPriority: 3);
        }
    }

    //Pick up an item from home
    public void PickUpItem(Interactable inventoryItem)
    {
        //No existing actions for me
        if (!actions.Exists(item => item.preset == RoutineControls.Instance.AIPickUpItem && item.passedInteractable == inventoryItem))
        {
            aiController.CreateNewAction(this, RoutineControls.Instance.AIPickUpItem, true, newPassedInteractable: inventoryItem, newInsertedActionPriority: 100);
        }
    }

    public void RoomLightingCheck(NewRoom room, RoomConfiguration.AILightingBehaviour.LightingPreference pref)
    {
        //Main light must be on (secondary lights off)
        if(pref == RoomConfiguration.AILightingBehaviour.LightingPreference.none)
        {
            return;
        }
        else if (pref == RoomConfiguration.AILightingBehaviour.LightingPreference.mainOn)
        {
            TurnSecondaryLightsOff(room);
            TurnMainLightOn(room);
        }
        else if (pref == RoomConfiguration.AILightingBehaviour.LightingPreference.mainOnSecondaryAny)
        {
            TurnMainLightOn(room);
        }
        else if (pref == RoomConfiguration.AILightingBehaviour.LightingPreference.secondaryOn)
        {
            if (room.actionReference.ContainsKey(RoutineControls.Instance.secondaryLightOn))
            {
                TurnMainLightOff(room);
                TurnSecondaryLightOn(room);
            }
            else
            {
                TurnMainLightOn(room); //Fall back to main light
            }
        }
        else if (pref == RoomConfiguration.AILightingBehaviour.LightingPreference.eitherPriorityMain)
        {
            if (!room.actionReference.ContainsKey(RoutineControls.Instance.secondaryLightOn))
            {
                TurnMainLightOn(room);
            }
            else if (!room.mainLightStatus && !room.secondaryLightStatus)
            {
                TurnMainLightOn(room); //Prefer main light
            }
        }
        else if (pref == RoomConfiguration.AILightingBehaviour.LightingPreference.eitherPrioritySecondary)
        {
            if (!room.actionReference.ContainsKey(RoutineControls.Instance.secondaryLightOn))
            {
                TurnMainLightOn(room);
            }
            else if (!room.mainLightStatus && !room.secondaryLightStatus)
            {
                TurnSecondaryLightOn(room); //Prefer secondary light
            }
        }
        else if (pref == RoomConfiguration.AILightingBehaviour.LightingPreference.allOff)
        {
            if (room.secondaryLightStatus && room.actionReference.ContainsKey(RoutineControls.Instance.secondaryLightOn))
            {
                TurnSecondaryLightsOff(room);
            }

            if(room.mainLightStatus)
            {
                TurnMainLightOff(room);
            }
        }
        else if (pref == RoomConfiguration.AILightingBehaviour.LightingPreference.mainOff)
        {
            if (room.mainLightStatus)
            {
                TurnMainLightOff(room);
            }
        }
        else if (pref == RoomConfiguration.AILightingBehaviour.LightingPreference.secondaryOff)
        {
            if (room.secondaryLightStatus)
            {
                TurnSecondaryLightsOff(room);
            }
        }
    }

    public bool IsLastOccupantOfRoom(NewRoom room, bool trueIfAsleep = false)
    {
        foreach(Actor a in room.currentOccupants)
        {
            if (a.isPlayer) continue;
            if (a == aiController.human) continue;
            if (!trueIfAsleep && a.isAsleep) continue;

            return false;
        }

        return true;
    }

    public bool IsLastOccupantOfGameLocation(NewGameLocation gl, bool trueIfAsleep = false)
    {
        foreach (Actor a in gl.currentOccupants)
        {
            if (a.isPlayer) continue;
            if (a == aiController.human) continue;
            if (!trueIfAsleep && a.isAsleep) continue;

            return false;
        }

        return true;
    }

    //Execute pottering functions (insert random various actions)
    public void PotterCheck()
    {
        //Is it time to potter?!
        if(SessionData.Instance.gameTime >= nextPotterAction)
        {
            //Get list of potter actions
            List<AIActionPreset> potterActions = null;

            if (preset.potterSource == AIGoalPreset.GoalActionSource.thisConfiguration)
            {
                potterActions = preset.potterActions;
            }
            else if (preset.potterSource == AIGoalPreset.GoalActionSource.jobPreset)
            {
                if (aiController.human.job != null && aiController.human.job.preset != null)
                {
                    bool potterPass = false;

                    //Only potter is somebody else is working the same job as me
                    if(aiController.human.job.preset.onlyPotterIfSomebodyElseWorking)
                    {
                        NewRoom prefRoom = null;

                        if (aiController.human.job.preset.preferredRooms.Count > 0)
                        {
                            List<NewRoom> validRooms = new List<NewRoom>();

                            foreach (RoomConfiguration r in aiController.human.job.preset.preferredRooms)
                            {
                                prefRoom = aiController.human.job.employer.placeOfBusiness.rooms.Find(item => item.preset == r);
                                if (prefRoom != null) validRooms.Add(roomLocation);
                            }

                            if (validRooms.Count > 0)
                            {
                                prefRoom = validRooms[Toolbox.Instance.Rand(0, validRooms.Count)];
                            }
                        }

                        if(prefRoom != null)
                        {
                            //Find all furniture that I work at
                            if(prefRoom.specialCaseInteractables.ContainsKey(aiController.human.job.preset.jobPostion))
                            {
                                foreach(Interactable i in prefRoom.specialCaseInteractables[aiController.human.job.preset.jobPostion])
                                {
                                    foreach(KeyValuePair<Interactable.UsePointSlot, Human> pair in i.usagePoint.users)
                                    {
                                        if(pair.Value != null && pair.Value != aiController.human)
                                        {
                                            //Found used by other human, go ahead and potter!
                                            potterPass = true;
                                            break;
                                        }
                                    }

                                    if (potterPass) break;
                                }
                            }
                        }
                    }
                    else potterPass = true;

                    if(potterPass) potterActions = aiController.human.job.preset.potterActions;
                }
            }

            //Insert a random potter action with a low-ish priority
            if(potterActions != null && potterActions.Count > 0)
            {
                //Find closest interactable within this address
                AIActionPreset randomAction = potterActions[Toolbox.Instance.Rand(0, potterActions.Count)];

                if (roomLocation == null)
                {
                    //Pick a room
                    if (preset.roomOption == AIGoalPreset.RoomOption.none)
                    {
                        roomLocation = null;
                    }
                    else if (preset.roomOption == AIGoalPreset.RoomOption.bedroom)
                    {
                        List<Interactable> beds = null;

                        if (aiController.human.home != null && aiController.human.home.actionReference.TryGetValue(CitizenControls.Instance.sleep, out beds))
                        {
                            if (beds.Count > 0)
                            {
                                roomLocation = beds[0].furnitureParent.anchorNode.room;
                            }
                        }
                    }
                    else if (preset.roomOption == AIGoalPreset.RoomOption.job)
                    {
                        if (aiController.human.job.employer != null)
                        {
                            if (aiController.human.job.preset.preferredRooms.Count > 0)
                            {
                                List<NewRoom> validRooms = new List<NewRoom>();

                                foreach (RoomConfiguration r in aiController.human.job.preset.preferredRooms)
                                {
                                    roomLocation = aiController.human.job.employer.placeOfBusiness.rooms.Find(item => item.preset == r);
                                    if (roomLocation != null) validRooms.Add(roomLocation);
                                }

                                if (validRooms.Count > 0)
                                {
                                    roomLocation = validRooms[Toolbox.Instance.Rand(0, validRooms.Count)];
                                }
                            }

                            //Otherwise pick a random room at work
                            if (roomLocation == null)
                            {
                                List<NewRoom> randomRoom = new List<NewRoom>();

                                //Pick from random from the building
                                if (aiController.human.job.preset.jobAIPosition == OccupationPreset.JobAI.randomBuilding)
                                {
                                    foreach (KeyValuePair<int, NewFloor> pair in aiController.human.job.employer.address.building.floors)
                                    {
                                        foreach (NewAddress ad in pair.Value.addresses)
                                        {
                                            if (ad.company != null && ad != aiController.human.job.employer.address)
                                            {
                                                continue; //Skip private companies than aren't this one
                                            }
                                            else if (ad.residence != null)
                                            {
                                                continue; //Skip homes
                                            }

                                            foreach (NewRoom r in ad.rooms)
                                            {
                                                if (r.isNullRoom) continue;
                                                if (aiController.human.job.preset.bannedRooms.Contains(r.preset)) continue; //Ignore banned rooms

                                                randomRoom.Add(r);
                                            }
                                        }
                                    }
                                }
                                //Pick from random from the address
                                else
                                {
                                    foreach (NewRoom r in aiController.human.job.employer.placeOfBusiness.rooms)
                                    {
                                        if (r.isNullRoom) continue;
                                        if (aiController.human.job.preset.bannedRooms.Contains(r.preset)) continue; //Ignore banned rooms

                                        randomRoom.Add(r);
                                    }

                                    foreach (NewRoom r in aiController.human.job.employer.address.rooms)
                                    {
                                        if (r.isNullRoom) continue;
                                        if (aiController.human.job.preset.bannedRooms.Contains(r.preset)) continue; //Ignore banned rooms

                                        randomRoom.Add(r);
                                    }
                                }

                                if (randomRoom.Count > 0)
                                {
                                    roomLocation = randomRoom[Toolbox.Instance.Rand(0, randomRoom.Count)];
                                }
                                else
                                {
                                    Game.LogError("AI has no random rooms to choose from...");
                                }
                            }
                        }
                    }

                    if(roomLocation == null) Game.LogError("RoomLocation in goal " + preset.name + " is null for " + aiController.human.name);
                }
                else
                {
                    //We can use all areas here, as it's limited by gamelocation anyway...
                    Interactable foundAction = Toolbox.Instance.FindNearestWithAction(randomAction, roomLocation, aiController.human, AIActionPreset.FindSetting.allAreas, false, restrictTo: gameLocation, enforcersAllowedEverywhere: preset.allowEnforcersEverywhere);

                    if(foundAction != null)
                    {
                        TryInsertInteractableAction(foundAction, randomAction, 1, duplicateActionCheck: false);
                    }
                }
            }

            //Set next potter time
            SetNextPotterTime();
        }
    }

    private void SetNextPotterTime()
    {
        nextPotterAction = SessionData.Instance.gameTime;

        if(preset.potterSource == AIGoalPreset.GoalActionSource.thisConfiguration)
        {
            nextPotterAction += Toolbox.Instance.Rand(preset.potterFrequency.x, preset.potterFrequency.y);
        }
        else if(preset.potterSource == AIGoalPreset.GoalActionSource.jobPreset)
        {
            if(aiController.human.job != null && aiController.human.job.preset != null)
            {
                nextPotterAction += Toolbox.Instance.Rand(aiController.human.job.preset.potterFrequency.x, aiController.human.job.preset.potterFrequency.y);
            }
        }
    }

    //Attempt to set an action for this interactable: Returns true if the action was able to be inserted
    public bool TryInsertInteractableAction(Interactable with, AIActionPreset newPreset, int priority, NewNode forcedNode = null, bool duplicateActionCheck = true)
    {
        if(with != null && with.nextAIInteraction == null)
        {
            //No existing actions for me
            if (!duplicateActionCheck || !actions.Exists(item => item.preset == newPreset && (with == null || item.passedInteractable == with)))
            {
                NewAIAction newAction = aiController.CreateNewAction(this, newPreset, true, newPassedInteractable: with, newForcedNode: forcedNode, newInsertedActionPriority: priority, newPassedGroup: passedGroup);

                if (newAction != null)
                {
                    with.SetNextAIInteraction(newAction, aiController);
                    return true;
                }
                else return false;
            }
            else if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Insert interactable action failed: Duplicate", Actor.HumanDebug.actions);
        }
        else if(with == null && Game.Instance.collectDebugData) aiController.human.SelectedDebug("Insert interactable action failed: interactable is Null", Actor.HumanDebug.actions);
        else if(Game.Instance.collectDebugData)
        {
            aiController.human.SelectedDebug("Insert interactable action failed: interactable is busy with next action: " + with.nextAIInteraction.preset.name + "(" + with.nextAIInteraction.goal.aiController.name +")", Actor.HumanDebug.actions);
        }

        return false;
    }

    //Special version of above for doors: Returns true if the action was able to be inserted
    public bool TryInsertDoorAction(NewDoor door, AIActionPreset preset, DoorSide doorSide, int priority, out DoorActionCheckResult result, NewNode forcedNode = null, string debug = "", bool immediateTick = false)
    {
        result = DoorActionCheckResult.success;

        //Leave the option to force a node...
        if(forcedNode == null)
        {
            if(doorSide == DoorSide.mySide)
            {
                forcedNode = null;
            }
            else if(doorSide == DoorSide.forceCurrentSide)
            {
                if(door.wall.node.room == aiController.human.currentRoom)
                {
                    forcedNode = door.wall.node;
                }
                else if (door.wall.otherWall.node.room == aiController.human.currentRoom)
                {
                    forcedNode = door.wall.otherWall.node;
                }
                else
                {
                    if (door.wall.node.gameLocation == aiController.human.currentGameLocation)
                    {
                        forcedNode = door.wall.node;
                    }
                    else if (door.wall.otherWall.node.gameLocation == aiController.human.currentGameLocation)
                    {
                        forcedNode = door.wall.otherWall.node;
                    }
                    else
                    {
                        forcedNode = null;
                    }
                }
            }
            else if(doorSide == DoorSide.forceCurrentOtherSide)
            {
                if (door.wall.node.room == aiController.human.currentRoom)
                {
                    forcedNode = door.wall.otherWall.node;
                }
                else if (door.wall.otherWall.node.room == aiController.human.currentRoom)
                {
                    forcedNode = door.wall.node;
                }
                else
                {
                    if (door.wall.node.gameLocation == aiController.human.currentGameLocation)
                    {
                        forcedNode = door.wall.otherWall.node;
                    }
                    else if (door.wall.otherWall.node.gameLocation == aiController.human.currentGameLocation)
                    {
                        forcedNode = door.wall.node;
                    }
                    else
                    {
                        forcedNode = null;
                    }
                }
            }
        }

        if(door.handleInteractable != null)
        {
            if (door.handleInteractable.nextAIInteraction == null || door.handleInteractable.nextAIInteraction.goal.aiController == aiController) //Procceed if this is me...
            {
                //No existing actions for me
                if (!actions.Exists(item => item.preset == preset && item.passedInteractable == door.handleInteractable))
                {
                    NewAIAction act = aiController.CreateNewAction(this, preset, true, newPassedInteractable: door.handleInteractable, newForcedNode: forcedNode, newInsertedActionPriority: priority, newDebug: debug, newPassedGroup: passedGroup);

                    if(act != null)
                    {
                        //Force the interaction point of the handle (we should have the correct node here but not always the interaction point)
                        door.handleInteractable.SetNextAIInteraction(act, aiController);
                        if (immediateTick) aiController.AITick(); //Immedaite tick

                        return true;
                    }
                }
                else
                {
                    result = DoorActionCheckResult.duplicate;
                    if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("AI: Action " + preset.name + " already exists for this door in goal " + name + "...", Actor.HumanDebug.actions);
                }
            }
            else
            {
                result = DoorActionCheckResult.beingUsed;
                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("AI: Door is being used by somebody else: " + door.handleInteractable.nextAIInteraction.preset.name + ", " + door.handleInteractable.nextAIInteraction.goal.aiController.human.name, Actor.HumanDebug.actions);

                //Interaction time out
                if(door.handleInteractable.nextAIInteraction != null)
                {
                    if(SessionData.Instance.gameTime >= door.handleInteractable.nextAIInteraction.createdAt + 0.2f)
                    {
                        door.handleInteractable.SetNextAIInteraction(null, aiController);
                    }
                }
            }
        }
        else
        {
            result = DoorActionCheckResult.noHandle;
        }

        return false;
    }

    //Trun the main light on
    //Passing an action will mean the force run variable will also be passed intact
    private void TurnMainLightOn(NewAIAction thisAction)
    {
        if (!thisAction.node.room.mainLightStatus && thisAction.node.room.mainLights.Count > 0)
        {
            string trespassDebug = string.Empty;

            if (!preset.allowTrespass && aiController.human.IsTrespassing(thisAction.node.room, out _, out trespassDebug, preset.allowEnforcersEverywhere))
            {
                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Lights in " + thisAction.node.name + " no valid authority: " + trespassDebug, Actor.HumanDebug.actions);
                return;
            }

            if (!actions.Exists(item => item.preset == RoutineControls.Instance.mainLightOn && item.passedRoom == thisAction.node.room))
            {
                //Search for other AI with the same idea...
                foreach(Actor actor in thisAction.node.room.currentOccupants)
                {
                    if (actor.ai == null) continue;
                    if (actor.isDead || actor.isAsleep || actor.isStunned) continue;
                    if (actor.ai.currentGoal == null) continue;

                    if (actor.ai.currentGoal.actions.Exists(item => item.preset == RoutineControls.Instance.mainLightOn && item.passedRoom == thisAction.node.room))
                    {
                        return;
                    }
                }

                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Turn on main light in " + thisAction.node.room.name + ", " + thisAction.node.room.gameLocation.name, Actor.HumanDebug.actions);
                aiController.CreateNewAction(this, RoutineControls.Instance.mainLightOn, true, thisAction.node.room, newForceRun: thisAction.forceRun, newInsertedActionPriority: 5, newDebug: aiController.human.currentNode.name);
            }
            else
            {
                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("...Action already exists.", Actor.HumanDebug.actions);
            }
        }
        //Cancel action if already on
        else
        {
            if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("...Main light status is already on", Actor.HumanDebug.actions);

            NewAIAction existingAction = actions.Find(item => item.preset == RoutineControls.Instance.mainLightOn && item.passedRoom == thisAction.node.room);
            if (existingAction != null) existingAction.Remove(existingAction.preset.repeatDelayOnActionFail);
        }
    }

    private void TurnMainLightOn(NewRoom where)
    {
        if (!where.mainLightStatus && where.mainLights.Count > 0)
        {
            string trespassDebug = string.Empty;

            if (!preset.allowTrespass && aiController.human.IsTrespassing(where, out _, out trespassDebug, preset.allowEnforcersEverywhere))
            {
                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Lights in " + where.name + " no valid authority: " + trespassDebug, Actor.HumanDebug.actions);
                return;
            }

            if (!actions.Exists(item => item.preset == RoutineControls.Instance.mainLightOn && item.passedRoom == where))
            {
                //Search for other AI with the same idea...
                foreach (Actor actor in where.currentOccupants)
                {
                    if (actor.ai == null) continue;
                    if (actor.isDead || actor.isAsleep || actor.isStunned) continue;
                    if (actor.ai.currentGoal == null) continue;

                    if (actor.ai.currentGoal.actions.Exists(item => item.preset == RoutineControls.Instance.mainLightOn && item.passedRoom == where))
                    {
                        return;
                    }
                }

                aiController.CreateNewAction(this, RoutineControls.Instance.mainLightOn, true, where, newInsertedActionPriority: 5, newDebug: aiController.human.currentNode.name);
            }
            //#if UNITY_EDITOR
            //else if (UnityEditor.Selection.Contains(aiController.gameObject))
            //{
            //    Game.Log("...Action already exists.");
            //}
            //#endif
        }
        //Cancel action if already on
        else
        {
            //#if UNITY_EDITOR
            //if (UnityEditor.Selection.Contains(aiController.gameObject))
            //{
            //    Game.Log("...Main light status is already on");
            //}
            //#endif

            NewAIAction existingAction = actions.Find(item => item.preset == RoutineControls.Instance.mainLightOn && item.passedRoom == where);
            if (existingAction != null) existingAction.Remove(existingAction.preset.repeatDelayOnActionFail);
        }
    }

    //Trun the main light off
    //Passing an action will mean the force run variable will also be passed intact
    private void TurnMainLightOff(NewAIAction thisAction)
    {
        if (thisAction.node.room.mainLightStatus && thisAction.node.room.mainLights.Count > 0)
        {
            string trespassDebug = string.Empty;

            if (!preset.allowTrespass && aiController.human.IsTrespassing(thisAction.node.room, out _, out trespassDebug, preset.allowEnforcersEverywhere))
            {
                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Lights in " + thisAction.node.name + " no valid authority: " + trespassDebug, Actor.HumanDebug.actions);
                return;
            }

            if (!actions.Exists(item => item.preset == RoutineControls.Instance.mainLightOff && item.passedRoom == thisAction.node.room))
            {
                //Search for other AI with the same idea...
                foreach (Actor actor in thisAction.node.room.currentOccupants)
                {
                    if (actor.ai == null) continue;
                    if (actor.isDead || actor.isAsleep || actor.isStunned) continue;
                    if (actor.ai.currentGoal == null) continue;

                    if (actor.ai.currentGoal.actions.Exists(item => item.preset == RoutineControls.Instance.mainLightOff && item.passedRoom == thisAction.node.room))
                    {
                        return;
                    }
                }

                aiController.CreateNewAction(this, RoutineControls.Instance.mainLightOff, true, thisAction.node.room, newForceRun: thisAction.forceRun, newInsertedActionPriority: 6, newDebug: aiController.human.currentNode.name);
            }
            //#if UNITY_EDITOR
            //else if (UnityEditor.Selection.Contains(aiController.gameObject))
            //{
            //    Game.Log("...Action already exists.");
            //}
            //#endif
        }
        //Cancel action if already off
        else
        {
            NewAIAction existingAction = actions.Find(item => item.preset == RoutineControls.Instance.mainLightOff && item.passedRoom == thisAction.node.room);
            if (existingAction != null) existingAction.Remove(existingAction.preset.repeatDelayOnActionFail);

            //#if UNITY_EDITOR
            //if (UnityEditor.Selection.Contains(aiController.gameObject))
            //{
            //    Game.Log("...Main light status is already off");
            //}
            //#endif
        }
    }

    private void TurnMainLightOff(NewRoom where)
    {
        if (where.mainLightStatus && where.mainLights.Count > 0)
        {
            string trespassDebug = string.Empty;

            if (!preset.allowTrespass && aiController.human.IsTrespassing(where, out _, out trespassDebug, preset.allowEnforcersEverywhere))
            {
                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Lights in " + where.name + " no valid authority: " + trespassDebug, Actor.HumanDebug.actions);
                return;
            }

            if (!actions.Exists(item => item.preset == RoutineControls.Instance.mainLightOff && item.passedRoom == where))
            {
                //Search for other AI with the same idea...
                foreach (Actor actor in where.currentOccupants)
                {
                    if (actor.ai == null) continue;
                    if (actor.isDead || actor.isAsleep || actor.isStunned) continue;
                    if (actor.ai.currentGoal == null) continue;

                    if (actor.ai.currentGoal.actions.Exists(item => item.preset == RoutineControls.Instance.mainLightOff && item.passedRoom == where))
                    {
                        return;
                    }
                }

                aiController.CreateNewAction(this, RoutineControls.Instance.mainLightOff, true, where, newInsertedActionPriority: 6, newDebug: aiController.human.currentNode.name);
            }
        }
        //Cancel action if already off
        else
        {
            #if UNITY_EDITOR
            if (UnityEditor.Selection.Contains(aiController.gameObject))
            {
                Game.Log("...Main light status is already off");
            }
            #endif

            NewAIAction existingAction = actions.Find(item => item.preset == RoutineControls.Instance.mainLightOff && item.passedRoom == where);
            if (existingAction != null) existingAction.Remove(existingAction.preset.repeatDelayOnActionFail);
        }
    }

    //Trun the most important/closest secondary light on
    //Passing an action will mean the force run variable will also be passed intact
    private void TurnSecondaryLightOn(NewAIAction thisAction)
    {
        if (!thisAction.node.room.secondaryLightStatus && thisAction.node.room.secondaryLights.Count > 0)
        {
            string trespassDebug = string.Empty;

            if (!preset.allowTrespass && aiController.human.IsTrespassing(thisAction.node.room, out _, out trespassDebug, preset.allowEnforcersEverywhere))
            {
                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Lights in " + thisAction.node.name + " no valid authority: " + trespassDebug, Actor.HumanDebug.actions);
                return;
            }

            if (!actions.Exists(item => item.preset == RoutineControls.Instance.secondaryLightOn && item.passedRoom == thisAction.node.room))
            {
                //Search for other AI with the same idea...
                foreach (Actor actor in thisAction.node.room.currentOccupants)
                {
                    if (actor.ai == null) continue;
                    if (actor.isDead || actor.isAsleep || actor.isStunned) continue;
                    if (actor.ai.currentGoal == null) continue;

                    if (actor.ai.currentGoal.actions.Exists(item => item.preset == RoutineControls.Instance.secondaryLightOn && item.passedRoom == thisAction.node.room))
                    {
                        return;
                    }
                }

                aiController.CreateNewAction(this, RoutineControls.Instance.secondaryLightOn, true, newPassedRoom: thisAction.node.room, newForceRun: thisAction.forceRun, newInsertedActionPriority: 5, newDebug: aiController.human.currentNode.name);
            }
            #if UNITY_EDITOR
            else if (UnityEditor.Selection.Contains(aiController.gameObject))
            {
                Game.Log("...Action already exists.");
            }
            #endif
        }
        //Cancel action if already on
        else
        {
            #if UNITY_EDITOR
            if (UnityEditor.Selection.Contains(aiController.gameObject))
            {
                Game.Log("...Secondary light status is already on");
            }
            #endif

            NewAIAction existingAction = actions.Find(item => item.preset == RoutineControls.Instance.secondaryLightOn && item.passedRoom == thisAction.node.room);
            if (existingAction != null) existingAction.Remove(existingAction.preset.repeatDelayOnActionFail);
        }
    }

    private void TurnSecondaryLightOn(NewRoom where)
    {
        if (!where.secondaryLightStatus && where.secondaryLights.Count > 0)
        {
            string trespassDebug = string.Empty;

            if (!preset.allowTrespass && aiController.human.IsTrespassing(where, out _, out trespassDebug, preset.allowEnforcersEverywhere))
            {
                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Lights in " + where.name + " no valid authority: " + trespassDebug, Actor.HumanDebug.actions);
                return;
            }

            if (!actions.Exists(item => item.preset == RoutineControls.Instance.secondaryLightOn && item.passedRoom == where))
            {
                //Search for other AI with the same idea...
                foreach (Actor actor in where.currentOccupants)
                {
                    if (actor.ai == null) continue;
                    if (actor.isDead || actor.isAsleep || actor.isStunned) continue;
                    if (actor.ai.currentGoal == null) continue;

                    if (actor.ai.currentGoal.actions.Exists(item => item.preset == RoutineControls.Instance.secondaryLightOn && item.passedRoom == where))
                    {
                        return;
                    }
                }

                aiController.CreateNewAction(this, RoutineControls.Instance.secondaryLightOn, true, where, newInsertedActionPriority: 5, newDebug: aiController.human.currentNode.name);
            }
            #if UNITY_EDITOR
            else if (UnityEditor.Selection.Contains(aiController.gameObject))
            {
                Game.Log("...Action already exists.");
            }
            #endif
        }
        //Cancel action if already on
        else
        {
            #if UNITY_EDITOR
            if (UnityEditor.Selection.Contains(aiController.gameObject))
            {
                Game.Log("...Secondary light status is already on");
            }
            #endif

            NewAIAction existingAction = actions.Find(item => item.preset == RoutineControls.Instance.secondaryLightOn && item.passedRoom == where);
            if (existingAction != null) existingAction.Remove(existingAction.preset.repeatDelayOnActionFail);
        }
    }

    //Turn all secondary lights off
    private void TurnSecondaryLightsOff(NewAIAction thisAction)
    {
        if (thisAction.node.room.secondaryLightStatus && thisAction.node.room.secondaryLights.Count > 0)
        {
            string trespassDebug = string.Empty;

            if (!preset.allowTrespass && aiController.human.IsTrespassing(thisAction.node.room, out _, out trespassDebug, preset.allowEnforcersEverywhere))
            {
                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Lights in " + thisAction.node.name + " no valid authority: " + trespassDebug, Actor.HumanDebug.actions);
                return;
            }

            foreach (Interactable inter in thisAction.node.room.secondaryLights)
            {
                //Find the turn off interaction
                InteractablePreset.InteractionAction releventInteraction = inter.preset.GetActions().Find(item => item.effectSwitchStates.Exists(item2 => item2.switchState == InteractablePreset.Switch.switchState && !item2.boolIs));
                
                if(releventInteraction != null)
                {
                    AIActionPreset releventAction = releventInteraction.action;

                    if (!inter.sw0)
                    {
                        //Cancel action if already on
                        NewAIAction existingAction = actions.Find(item => item.preset == releventAction && item.passedInteractable == inter);
                        if (existingAction != null) existingAction.Remove(existingAction.preset.repeatDelayOnActionFail);

                        continue; //Skip if already off
                    }

                    if (!actions.Exists(item => item.preset == releventAction && item.passedInteractable == inter))
                    {
                        //Search for other AI with the same idea...
                        foreach (Actor actor in thisAction.node.room.currentOccupants)
                        {
                            if (actor.ai == null) continue;
                            if (actor.isDead || actor.isAsleep || actor.isStunned) continue;
                            if (actor.ai.currentGoal == null) continue;

                            if (actor.ai.currentGoal.actions.Exists(item => item.preset == releventAction && item.passedInteractable == inter))
                            {
                                return;
                            }
                        }

                        aiController.CreateNewAction(this, releventAction, true, newForceRun: thisAction.forceRun, newPassedInteractable: inter, newInsertedActionPriority: 6, newDebug: aiController.human.currentNode.name);
                    }
                }

            }
        }
    }

    private void TurnSecondaryLightsOff(NewRoom where)
    {
        if (where.secondaryLightStatus && where.secondaryLights.Count > 0)
        {
            string trespassDebug = string.Empty;

            if (!preset.allowTrespass && aiController.human.IsTrespassing(where, out _, out trespassDebug, preset.allowEnforcersEverywhere))
            {
                if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Lights in " + where.name + " no valid authority: " + trespassDebug, Actor.HumanDebug.actions);
                return;
            }

            foreach (Interactable inter in where.secondaryLights)
            {
                //Find the turn off interaction
                InteractablePreset.InteractionAction releventInteraction = inter.preset.GetActions().Find(item => item.effectSwitchStates.Exists(item2 => item2.switchState == InteractablePreset.Switch.switchState && !item2.boolIs));
                if (releventInteraction == null) continue;
                AIActionPreset releventAction = releventInteraction.action;

                if (!inter.sw0)
                {
                    //Cancel action if already on
                    NewAIAction existingAction = actions.Find(item => item.preset == releventAction && item.passedInteractable == inter);
                    if (existingAction != null) existingAction.Remove(existingAction.preset.repeatDelayOnActionFail);

                    continue; //Skip if already off
                }

                if (!actions.Exists(item => item.preset == releventAction && item.passedInteractable == inter))
                {
                    //Search for other AI with the same idea...
                    foreach (Actor actor in where.currentOccupants)
                    {
                        if (actor.ai == null) continue;
                        if (actor.isDead || actor.isAsleep || actor.isStunned) continue;
                        if (actor.ai.currentGoal == null) continue;

                        if (actor.ai.currentGoal.actions.Exists(item => item.preset == releventAction && item.passedInteractable == inter))
                        {
                            return;
                        }
                    }

                    aiController.CreateNewAction(this, releventAction, true, newPassedInteractable: inter, newInsertedActionPriority: 6, newDebug: aiController.human.currentNode.name);
                }
            }
        }
    }

    //Deactivate an illegally activated object
    private void DeactivateInteractable(Interactable thisInteractable)
    {
        InteractablePreset.InteractionAction findAction = thisInteractable.preset.GetActions().Find(item => item.action.tamperResetAction);

        if (findAction != null)
        {
            TryInsertInteractableAction(thisInteractable, findAction.action, 4);

            aiController.tamperedObject = thisInteractable;

            //Bark: Comment about tampered object
            aiController.human.speechController.TriggerBark(SpeechController.Bark.discoverTamper);
        }
    }

    //Insert actions to unlock a door (also insert following lock event)
    public bool InsertUnlockAction(NewDoor door, bool lockBehind)
    {
        //Get the node from where I should unlock the door...
        NewNode unlockNode = null;
        NewNode lockNode = null;

        //Get the inside node
        if (aiController.human.currentRoom == door.wall.node.room)
        {
            unlockNode = door.wall.node;
            lockNode = door.wall.otherWall.node;
        }
        else if(aiController.human.currentRoom == door.wall.otherWall.node.room)
        {
            lockNode = door.wall.node;
            unlockNode = door.wall.otherWall.node;
        }
        else
        {
            if (aiController.human.currentGameLocation == door.wall.node.gameLocation)
            {
                unlockNode = door.wall.node;
                lockNode = door.wall.otherWall.node;
            }
            else if (aiController.human.currentGameLocation == door.wall.otherWall.node.gameLocation)
            {
                lockNode = door.wall.node;
                unlockNode = door.wall.otherWall.node;
            }
        }

        if (unlockNode == null) Game.Log("Unable to find unlock node");
        if (lockNode == null) Game.Log("Unable to find lock node");

        //Unlock door
        bool unlockActionOK = true;

        if (door.isLocked)
        {
            unlockActionOK = TryInsertDoorAction(door, RoutineControls.Instance.unlockDoor, DoorSide.mySide, 99, out _, unlockNode, "InsertUnlockAction");
        }

        //If locking again, create this first as it will insert @ 0, so unlock door will eventually appear first...
        if (unlockActionOK && lockBehind && door.preset.lockType != DoorPreset.LockType.none)
        {
            if(aiController.human.currentGameLocation == null || aiController.human.currentGameLocation.thisAsAddress == null || !GameplayController.Instance.guestPasses.ContainsKey(aiController.human.currentGameLocation.thisAsAddress) || Player.Instance.currentGameLocation != aiController.human.currentGameLocation)
            {
                TryInsertDoorAction(door, RoutineControls.Instance.lockDoor, DoorSide.forceCurrentOtherSide, 98, out _, lockNode, "InsertLockAction");
            }
        }

        return unlockActionOK;
    }

    //Insert actions to lock a door from the actor's side...
    public bool InsertLockAction(NewDoor door)
    {
        //lock door
        bool lockActionOK = true;

        if (!door.isLocked && door.preset.lockType != DoorPreset.LockType.none)
        {
            if (aiController.human.currentGameLocation == null || aiController.human.currentGameLocation.thisAsAddress == null || !GameplayController.Instance.guestPasses.ContainsKey(aiController.human.currentGameLocation.thisAsAddress) || Player.Instance.currentGameLocation != aiController.human.currentGameLocation)
            {
                lockActionOK = TryInsertDoorAction(door, RoutineControls.Instance.lockDoor, DoorSide.mySide, 98, out _, debug: "InsertLockAction2: " + aiController.human.currentNode.name);
            }
            else lockActionOK = false;
        }

        return lockActionOK;
    }

    //Insert action to remove player from hiding place
    public void InsertPlayerHidingPlaceRemoval()
    {
        if(InteractionController.Instance.lockedInInteraction != null)
        {
            Game.Log("Insert action for removing player from hiding... " + InteractionController.Instance.lockedInInteraction.name);

            TryInsertInteractableAction(InteractionController.Instance.lockedInInteraction, RoutineControls.Instance.pullPlayerFromHiding, 99);
        }
    }

    //Triggered when an action is completed
    public void OnCompletedAction(NewAIAction completed)
    {
        //Repeat actions
        if(completed.repeat)
        {
            bool rep = true;

            //Repeat while having consumable items...
            if(completed.preset.repeatWhileHavingConsumables)
            {
                if(aiController.human.currentConsumables.Count <= 0)
                {
                    rep = false;
                }
            }

            if(rep)
            {
                aiController.CreateNewAction(this, completed.preset, newPassedGroup: passedGroup);
            }
        }
    }

    //Complete this goal
    public void Complete()
    {
        if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Completed a goal: " + preset.name, Actor.HumanDebug.actions);

        //Game.Log("Complete goal " + name);
        aiController.OnCompleteGoal(this);
        Remove();

        aiController.AITick(); //Force tick
    }

    //Remove this goal
    public void Remove()
    {
        //Game.Log("Remove goal " + preset.name);

        if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Remove goal " + preset.name, Actor.HumanDebug.actions);

        //If this is currently active, set current to null
        if(isActive)
        {
            for (int i = 0; i < actions.Count; i++)
            {
                if(actions[i].isActive)
                {
                    actions[i].OnDeactivate();
                }
            }

            if (Game.Instance.collectDebugData)
            {
                aiController.human.SelectedDebug("Set current goal to null", Actor.HumanDebug.actions);
            }

            aiController.currentGoal = null;
            isActive = false;
        }

        aiController.goals.Remove(this);
    }

    //Get Goal Data
    public StateSaveData.CurrentGoalStateSave GetGoalStateSave()
    {
        if (preset.disableSave) return null;

        StateSaveData.CurrentGoalStateSave ret = new StateSaveData.CurrentGoalStateSave();

        ret.preset = preset.name;
        ret.trigerTime = triggerTime;
        ret.duration = duration;
        if(passedNode != null) ret.passedNode = passedNode.nodeCoord;
        if(passedInteractable != null) ret.passedInteractable = passedInteractable.id;
        ret.priority = priority;
        ret.var = passedVar;
        ret.activeTime = activeTime;

        if(roomLocation != null)
        {
            ret.room = roomLocation.roomID;
        }
        
        if(gameLocation != null)
        {
            if(gameLocation.thisAsAddress != null)
            {
                ret.gameLocation = gameLocation.thisAsAddress.id;
                ret.isAddress = true;
            }
            else
            {
                ret.gameLocation = gameLocation.thisAsStreet.streetID;
                ret.isAddress = false;
            }
        }

        if(passedGroup != null)
        {
            ret.passedGroup = passedGroup.id;
        }

        ret.jobID = jobID;

        foreach(NewAIAction act in actions)
        {
            StateSaveData.AIActionStateSave newAction = new StateSaveData.AIActionStateSave();

            newAction.preset = act.preset.name;
            if(act.node != null) newAction.node = act.node.nodeCoord;
            if(act.interactable != null) newAction.interactable = act.interactable.id;
            if(act.passedInteractable != null) newAction.passedInteractable = act.passedInteractable.id;
            if(act.passedRoom != null) newAction.passedRoom = act.passedRoom.roomID;
            if(act.forcedNode != null) newAction.forcedNode = act.forcedNode.nodeCoord;
            newAction.repeat = act.repeat;

            if(act.passedGroup != null)
            {
                newAction.passedGroup = act.passedGroup.id;
            }

            ret.actions.Add(newAction);
        }

        return ret;
    }

    //Default comparer (priority)
    public int CompareTo(NewAIGoal otherObject)
    {
        return this.priority.CompareTo(otherObject.priority);
    }

    //Get the first action from this preset...
    public AIActionPreset GetFirstAction(NewGameLocation loc)
    {
        if (preset == null) return null;
        string seed = aiController.human.citizenName + SessionData.Instance.gameTime;

        for (int i = 0; i < preset.actionsSetup.Count; i++)
        {
            AIGoalPreset.GoalActionSetup actionSetup = preset.actionsSetup[i];
            if (actionSetup == null || actionSetup.actions.Count <= 0) continue;

            float chance = GetActionChance(actionSetup, loc);

            if(chance == 1f || Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) <= chance)
            {
                return actionSetup.actions[Toolbox.Instance.GetPsuedoRandomNumberContained(0, actionSetup.actions.Count, seed, out seed)];
            }
        }

        return null;
    }

    //Get chance of this action
    public float GetActionChance(AIGoalPreset.GoalActionSetup actionSetup, NewGameLocation loc)
    {
        float chance = actionSetup.chance;

        if(loc != null)
        {
            if(actionSetup.condition == AIGoalPreset.ActionCondition.atHomeOnly && loc != aiController.human.home)
            {
                return 0f;
            }
            else if (actionSetup.condition == AIGoalPreset.ActionCondition.atHomeNoGuestPass && (loc != aiController.human.home || GameplayController.Instance.guestPasses.ContainsKey(aiController.human.home)))
            {
                return 0f;
            }
            else if (actionSetup.condition == AIGoalPreset.ActionCondition.noGuestPass && (aiController.human.currentGameLocation != null && aiController.human.currentGameLocation.thisAsAddress != null && GameplayController.Instance.guestPasses.ContainsKey(aiController.human.currentGameLocation.thisAsAddress)))
            {
                return 0f;
            }
            else if(actionSetup.condition == AIGoalPreset.ActionCondition.inPublicOnly)
            {
                if (loc != aiController.human.home && (aiController.human.job == null || aiController.human.job.employer == null || loc != aiController.human.job.employer.placeOfBusiness))
                {

                }
                else return 0f;
            }
            else if(actionSetup.condition == AIGoalPreset.ActionCondition.atWorkOnly)
            {
                if (aiController.human.job != null && aiController.human.job.employer != null && loc == aiController.human.job.employer.placeOfBusiness)
                {

                }
                else return 0f;
            }
            else if(actionSetup.condition == AIGoalPreset.ActionCondition.onlyIfEscalated)
            {
                if (aiController.human.escalationLevel >= 2)
                {
                    
                }
                else
                {
                    if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Skipping action " + actionSetup.actions[0].name + " beacuse escalation isn't high enough: " + aiController.human.escalationLevel, Actor.HumanDebug.actions);
                    return 0f;
                }
            }
            else if(actionSetup.condition == AIGoalPreset.ActionCondition.onlyIfDead)
            {
                if(passedInteractable != null && passedInteractable.isActor != null && passedInteractable.isActor.isDead)
                {

                }
                else
                {
                    if (Game.Instance.collectDebugData) aiController.human.SelectedDebug("Skipping action " + actionSetup.actions[0].name + " beacuse passed actor is null or alive", Actor.HumanDebug.actions);
                    return 0f;
                }
            }
        }

        //Status rules
        foreach(AIGoalPreset.StatusModifierRule rule in actionSetup.statusModifiers)
        {
            if(rule.status == AIGoalPreset.StatusType.alertness)
            {
                if(rule.condition == AIGoalPreset.StatusCondition.isEqualOrAbove && aiController.human.alertness >= rule.value)
                {
                    chance += rule.chanceModifier;
                }
                else if(rule.condition == AIGoalPreset.StatusCondition.isEqualOrBelow && aiController.human.alertness <= rule.value)
                {
                    chance += rule.chanceModifier;
                }
            }
            else if(rule.status == AIGoalPreset.StatusType.bladder)
            {
                if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrAbove && aiController.human.bladder >= rule.value)
                {
                    chance += rule.chanceModifier;
                }
                else if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrBelow && aiController.human.bladder <= rule.value)
                {
                    chance += rule.chanceModifier;
                }
            }
            else if (rule.status == AIGoalPreset.StatusType.chores)
            {
                if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrAbove && aiController.human.chores >= rule.value)
                {
                    chance += rule.chanceModifier;
                }
                else if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrBelow && aiController.human.chores <= rule.value)
                {
                    chance += rule.chanceModifier;
                }
            }
            else if (rule.status == AIGoalPreset.StatusType.energy)
            {
                if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrAbove && aiController.human.energy >= rule.value)
                {
                    chance += rule.chanceModifier;
                }
                else if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrBelow && aiController.human.energy <= rule.value)
                {
                    chance += rule.chanceModifier;
                }
            }
            else if (rule.status == AIGoalPreset.StatusType.excitement)
            {
                if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrAbove && aiController.human.excitement >= rule.value)
                {
                    chance += rule.chanceModifier;
                }
                else if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrBelow && aiController.human.excitement <= rule.value)
                {
                    chance += rule.chanceModifier;
                }
            }
            else if (rule.status == AIGoalPreset.StatusType.health)
            {
                if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrAbove && aiController.human.currentHealthNormalized >= rule.value)
                {
                    chance += rule.chanceModifier;
                }
                else if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrBelow && aiController.human.currentHealthNormalized <= rule.value)
                {
                    chance += rule.chanceModifier;
                }
            }
            else if (rule.status == AIGoalPreset.StatusType.heat)
            {
                if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrAbove && aiController.human.heat >= rule.value)
                {
                    chance += rule.chanceModifier;
                }
                else if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrBelow && aiController.human.heat <= rule.value)
                {
                    chance += rule.chanceModifier;
                }
            }
            else if (rule.status == AIGoalPreset.StatusType.hydration)
            {
                if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrAbove && aiController.human.hydration >= rule.value)
                {
                    chance += rule.chanceModifier;
                }
                else if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrBelow && aiController.human.hydration <= rule.value)
                {
                    chance += rule.chanceModifier;
                }
            }
            else if (rule.status == AIGoalPreset.StatusType.hygeine)
            {
                if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrAbove && aiController.human.hygiene >= rule.value)
                {
                    chance += rule.chanceModifier;
                }
                else if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrBelow && aiController.human.hygiene <= rule.value)
                {
                    chance += rule.chanceModifier;
                }
            }
            else if (rule.status == AIGoalPreset.StatusType.nerve)
            {
                if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrAbove && aiController.human.currentNerve >= rule.value)
                {
                    chance += rule.chanceModifier;
                }
                else if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrBelow && aiController.human.currentNerve <= rule.value)
                {
                    chance += rule.chanceModifier;
                }
            }
            else if (rule.status == AIGoalPreset.StatusType.nourishment)
            {
                if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrAbove && aiController.human.nourishment >= rule.value)
                {
                    chance += rule.chanceModifier;
                }
                else if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrBelow && aiController.human.nourishment <= rule.value)
                {
                    chance += rule.chanceModifier;
                }
            }
            else if (rule.status == AIGoalPreset.StatusType.breath)
            {
                if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrAbove && aiController.human.breath >= rule.value)
                {
                    chance += rule.chanceModifier;
                }
                else if (rule.condition == AIGoalPreset.StatusCondition.isEqualOrBelow && aiController.human.breath <= rule.value)
                {
                    chance += rule.chanceModifier;
                }
            }
            else if(rule.status == AIGoalPreset.StatusType.onDutyEnforcer)
            {
                if (rule.condition == AIGoalPreset.StatusCondition.isTrue && aiController.human.isEnforcer && aiController.human.isOnDuty)
                {
                    chance += rule.chanceModifier;
                }
                else if (rule.condition == AIGoalPreset.StatusCondition.isFalse && (!aiController.human.isEnforcer || !aiController.human.isOnDuty))
                {
                    chance += rule.chanceModifier;
                }
            }
        }

        //Check picking rules
        foreach (AIGoalPreset.GoalModifierRule rule in actionSetup.traitModifiers)
        {
            bool pass = false;

            if (rule.rule == CharacterTrait.RuleType.ifAnyOfThese)
            {
                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (aiController.human.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = true;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifAllOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (!aiController.human.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifNoneOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (aiController.human.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifPartnerAnyOfThese)
            {
                if (aiController.human.partner != null)
                {
                    foreach (CharacterTrait searchTrait in rule.traitList)
                    {
                        if (aiController.human.partner.characterTraits.Exists(item => item.trait == searchTrait))
                        {
                            pass = true;
                            break;
                        }
                    }
                }
                else pass = false;
            }

            if (pass)
            {
                chance += rule.priorityMultiplier;
            }
            else if (rule.mustPassForApplication)
            {
                return 0f;
            }
        }

        return chance;
    }
}
