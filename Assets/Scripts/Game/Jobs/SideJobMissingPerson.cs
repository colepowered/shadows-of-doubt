﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SideJobMissingPerson : SideJob
{
    [Header("Saved Values")]
    public bool readyToPost = false;

    [Header("Unsaved Values")]
    private NewAIGoal exitBuilding;

    public SideJobMissingPerson(JobPreset newPreset, SideJobController.JobPickData newData, bool immediatePost) : base(newPreset, newData, immediatePost)
    {

    }

    //Called on setup, but in this case we want things to happen before hand...
    public override void PostJob()
    {
        if (readyToPost)
        {
            base.PostJob();
        }
    }

    public override void AcceptJob()
    {
        base.AcceptJob();

        //Know hand-in immediately
        OnAcquireJobInfo(string.Empty);

        //Add hand-in location
        thisCase.handIn.Clear();

        foreach (NewNode.NodeAccess n in poster.home.entrances)
        {
            if (n.door != null)
            {
                if (n.door.peekInteractable != null)
                {
                    thisCase.handIn.Add(-n.door.wall.id); //As the peek interactables have no proper interactable ID, use the MINUS value of the wall ID instead...
                }
            }
        }
    }

    public override void GameWorldLoop()
    {
        base.GameWorldLoop();

        //Pre-post events
        if (!readyToPost)
        {
            if (motiveStr == "Suicide-Depression" || motiveStr == "Suicide-Debt")
            {
                if (exitBuilding == null)
                {
                    //Create a 'exit building' goal
                    exitBuilding = purp.ai.CreateNewGoal(RoutineControls.Instance.exitBuilding, 0f, 0f);
                    Game.Log("Jobs: Created exit building goal for " + purp.name);
                }
                else if (!purp.ai.goals.Contains(exitBuilding))
                {
                    Game.Log("Jobs: Disappearing " + purp.name);
                    purp.RemoveFromWorld(true); //Remove from game world
                    readyToPost = true; //Now ready to post
                }
            }
        }
        else if (readyToPost && post == null)
        {
            PostJob();
        }

        //Objectives state loops
        if (thisCase != null && thisCase.isActive)
        {
            //Phase 0: Call the fake number
            if (phase == 0)
            {
                //Only run this once
                if (phase != phaseChange)
                {
                    phaseChange = phase;
                }
            }
        }
    }
}
