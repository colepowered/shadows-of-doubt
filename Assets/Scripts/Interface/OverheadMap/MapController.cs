﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using NaughtyAttributes;
using System.Linq;

public class MapController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform contentRect;
    public RectTransform paperRect;
    public RectTransform viewport;
    public ZoomContent zoomController;
    public DragCoverage drag;
    public CustomScrollRect scrollRect;
    public RectTransform controlsRect;
    public Canvas contentCanvas;
    public ButtonController mapCloseButton;
    public RectTransform mapCursor;
    public ContextMenuController mapContextMenu;
    public TextMeshProUGUI districtMapName;
    public ButtonController centreOnPlayerButton;
    public ButtonController controllerSelectMapButton;

    [Header("Drawing")]
    public bool drawingMode = false;
    public bool eraseMode = false;
    public Color drawingColour = Color.white;
    public RectTransform drawBrushRect;
    public ButtonController toggleDrawingButton;
    public ColourSelectorButtonController colourButton;
    public ButtonController eraserButton;
    public ButtonController clearButton;

    [Header("State")]
    public int load = -1;
    public bool displayPlayerCharacter = true;
    public bool displayFirstPerson = false; //Temporary display
    public RectTransform playerCharacterRect;
    public NewNode mapCursorNode;
    private NewNode cursorNodeChange;
    public Vector2 cursorPos;
    public List<MapAddressButtonController> mapUpdateList = new List<MapAddressButtonController>();
    public List<MapDuctsButtonController> ductsUpdateList = new List<MapDuctsButtonController>();

    [Header("Map Overlays")]
    public RectTransform routesRect;
    public RectTransform linesRouteRect;
    public RectTransform citizensRouteRect;
    public RectTransform sightingsRoutRect;
    public RectTransform overlayAll;
    public RectTransform pinsRect;
    public RectTransform tooltipOverride;

    public class PointerData
    {
        public RectTransform pointerObject;
        public RectTransform followRect;
        public Vector2 followPos; //Used if no follow rect
        public float pointerShow = 0f;
    }

    private List<PointerData> pointers = new List<PointerData>();

    //Tracked objects are objects that move and display on the map
    public Dictionary<Transform, List<RectTransform>> dynamicTrackedObjects = new Dictionary<Transform, List<RectTransform>>();
    public Dictionary<Transform, List<RectTransform>> staticTrackedObjects = new Dictionary<Transform, List<RectTransform>>();

    //Pins are all objects pinned, but not all are considered track (ie some are static)
    public Dictionary<InfoWindow, MapPinButtonController> pinnedObjects = new Dictionary<InfoWindow, MapPinButtonController>();
    public List<InfoWindow> invisiblePins = new List<InfoWindow>();

    [Header("Key")]
    public TextMeshProUGUI keyUnexplored;
    public TextMeshProUGUI keyExploredSafe;
    public TextMeshProUGUI keyExploredPrivate;
    public TextMeshProUGUI keyVent;
    public TextMeshProUGUI keyDuct;
    public TextMeshProUGUI keyOpenHoursOnly;

    [Header("Setup")]
    public float nodePositionMultiplier = 32f;
    private float realPositionMultiplier = 1f;
    public float positionBuffer = 0f;
    public float edgeBuffer = 128f;
    public float focusSpeed = 8f;

    public float openProgress = 0f;
    public float savedSize = 952f;

    public RectTransform baseLayer;
    public FloorZoomController fzc;

    private bool forceFocusActive = false;
    private float forceFocusProgress = 0f;
    private RectTransform focusRect;
    private Vector2 focusPos; //Used if there is no rect above

    //Routes
    //[System.Serializable]
    public class MapRoute
    {
        public NewNode start;
        public NewNode end;
        public NewGameLocation destinationTextOverride; //Display text for this location at the end instead of using the actual node
        public Human human;
        public PathFinder.PathData pathData;
        public bool nodeSpecific = false; //Arrive at end if player reaches this exact node (if false it will use gamelocation)

        //Use these for keeping track what's drawn
        private NewNode drawnFrom = null;
        private NewNode drawnTo = null;

        //True if this has failed and an attempt to the entrance of the gamelocation has instead been made
        private bool failedAttempt = false;

        //Spawned objects
        public Dictionary<GameObject, NewNode> spawnedObjects = new Dictionary<GameObject, NewNode>();

        //Constructor; draw line
        public MapRoute(NewNode newStart, NewNode newEnd, Human newHuman, bool newNodeSpecific, NewGameLocation newDestinationTextOverride)
        {
            start = newStart;
            end = newEnd;
            human = newHuman;
            nodeSpecific = newNodeSpecific;
            destinationTextOverride = newDestinationTextOverride;

            //This is a player route
            if(human == Player.Instance)
            {
                MapController.Instance.playerRoute = this;
                MapController.Instance.DisplayDirectionArrow(true);
            }

            //Update: Create before it is added to the list as this will remove it
            UpdatePathData();
            UpdateDrawnRoute();
        }

        public void UpdatePathData()
        {
            //Update path data
            Game.Log("Interface: Updating player route path...");
            pathData = PathFinder.Instance.GetPath(start, end, human);

            if((pathData == null || pathData.accessList == null || pathData.accessList.Count <= 0) && !failedAttempt)
            {
                //Instead use entrances from the gamelocation
                //Get a coordinate within this...
                NewNode.NodeAccess access = end.gameLocation.entrances.Find(item => item.accessType == NewNode.NodeAccess.AccessType.door && !item.employeeDoor);

                try
                {
                    if (access.toNode.gameLocation == end.gameLocation) end = access.fromNode;
                    else end = access.toNode;

                    pathData = PathFinder.Instance.GetPath(start, end, human);

                    failedAttempt = true;
                }
                catch
                {
                    Remove();
                }
            }
        }

        public void UpdateDrawnRoute()
        {
            //Update if change is needed
            if (start != drawnFrom || end != drawnTo)
            {
                //Remove previous
                List<GameObject> toRemove = new List<GameObject>();

                foreach (KeyValuePair<GameObject, NewNode> pair in spawnedObjects)
                {
                    toRemove.Add(pair.Key);
                }

                foreach (GameObject obj in toRemove)
                {
                    spawnedObjects.Remove(obj);
                    Destroy(obj);
                }

                if (pathData == null || pathData.accessList == null || pathData.accessList.Count <= 0) return;

                //Spawn object for each coordinate
                Vector2 currentPosition = MapController.Instance.NodeCoordToMap(pathData.accessList[0].fromNode.nodeCoord);

                for (int i = 0; i < pathData.accessList.Count; i++)
                {
                    NewNode fromNode = pathData.accessList[i].fromNode;

                    if(!MapController.Instance.mapLayers.ContainsKey((int)fromNode.nodeCoord.z))
                    {
                        Game.LogError("Map does not contain layer " + (int)fromNode.nodeCoord.z);
                    }

                    GameObject newObj = Instantiate(PrefabControls.Instance.routeLine, MapController.Instance.mapLayers[(int)fromNode.nodeCoord.z].baseContainer);
                    spawnedObjects.Add(newObj, fromNode);
                    //newObj.GetComponent<Image>().color = timeline.parentWindow.baseColour; //Set colour

                    //Position
                    RectTransform lineRect = newObj.GetComponent<RectTransform>();
                    lineRect.anchoredPosition = currentPosition;

                    //Now position is set, update the current to the next point. Do this to save caluclating positions twice
                    NewNode nextLoc = pathData.accessList[i].toNode;
                    currentPosition = MapController.Instance.NodeCoordToMap(pathData.accessList[i].toNode.nodeCoord);

                    //Set position to objects - offset
                    Vector3 differenceVector = currentPosition - lineRect.anchoredPosition;

                    //Calculate with based on whether this is a file link
                    lineRect.sizeDelta = new Vector2(differenceVector.magnitude + 8f, lineRect.sizeDelta.y);

                    //Set angle
                    float angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
                    lineRect.rotation = Quaternion.Euler(0, 0, angle);

                    lineRect.anchoredPosition = new Vector2(lineRect.anchoredPosition.x, lineRect.anchoredPosition.y - MapController.Instance.nodePositionMultiplier * 1.5f); //I don't know why this is happening but lines are appearing to high on the map, workaround here <-

                    //Set transparency
                    //int floorDiff = Mathf.Abs(MapController.Instance.load - (int)pathData.accessList[i].fromNode.nodeCoord.z);

                    //if (floorDiff > 0)
                    //{
                    //    float transProgress = 1f - (Mathf.Clamp(floorDiff, 0, 11) / 10f);
                    //    float alpha = Mathf.Lerp(0.1f, 0.5f, transProgress);
                    //    newObj.GetComponent<CanvasRenderer>().SetAlpha(alpha);
                    //}
                    //else
                    //{
                    //    newObj.GetComponent<CanvasRenderer>().SetAlpha(1f);
                    //}
                }

                //Update draw to refs
                drawnFrom = start;
                drawnTo = end;
            }
        }

        public void Remove()
        {
            Game.Log("Interface: Remove player route");

            List<GameObject> toRemove = new List<GameObject>();

            foreach (KeyValuePair<GameObject, NewNode> pair in spawnedObjects)
            {
                toRemove.Add(pair.Key);
            }

            foreach (GameObject obj in toRemove)
            {
                spawnedObjects.Remove(obj);
                Destroy(obj);
            }

            //This is a player route
            if (human == Player.Instance)
            {
                MapController.Instance.playerRoute = null;
                MapController.Instance.DisplayDirectionArrow(false);

                //Add cancel to context menu
                if (!MapController.Instance.mapContextMenu.disabledItems.Contains("CancelRoute"))
                {
                    MapController.Instance.mapContextMenu.disabledItems.Add("CancelRoute");
                }

                InterfaceControls.Instance.plottedRouteText.gameObject.SetActive(false);
            }
        }
    }

    public MapRoute playerRoute = null;

    //Actions - used as callback for end of frame
    Action UpdateTimelineCitizens;

    //New map
    [Header("Graphics")]
    public float mapResolutionDivision = 1f; //Divide the real resolution by this to save memory
    public int wallWidth = 2; //How many pixels wide to draw the walls
    public Color roomBaseColor = Color.grey;
    [Tooltip("Add this amount to the above once highlighted")]
    public Color highlightedColourAdditive = Color.white;
    [Tooltip("This will be drawn to all floor textures")]
    public Texture2D publicFloorTexture;
    public Texture2D privateFloorTexture;
    public Texture2D nullRoomTexture;
    public Texture2D undiscoveredTexture;
    [Tooltip("This will be used for drawing walls")]
    public Texture2D wallTexture;
    public Texture2D wallTexCorners;
    public List<Texture2D> wallEdge = new List<Texture2D>();
    public List<Texture2D> wallDoorway = new List<Texture2D>();
    public List<Texture2D> wallWindow = new List<Texture2D>();
    public List<Texture2D> outsideWindow = new List<Texture2D>();
    public List<Texture2D> dividerLeft = new List<Texture2D>();
    public List<Texture2D> dividerRight = new List<Texture2D>();
    public List<Texture2D> stairwell = new List<Texture2D>();
    public Texture2D vent;
    public Texture2D ventUpwardsConnection;
    public Texture2D ventDownwardsConnection;

    [Header("Direction Arrow")]
    public GameObject directionalArrowContainer;
    public bool displayDirectionArrow = false;
    public Transform directionalArrow;
    public float directionalArrowDesiredFade = 0f;
    public float directionalArrowAlpha = 0f;
    public Material arrowMaterial;

    [Header("Canvas Components")]
    public Dictionary<int, MapLayer> mapLayers = new Dictionary<int, MapLayer>();
    public List<MapAddressButtonController> buttons = new List<MapAddressButtonController>();

    //Debug
    private List<GameObject> spawnedDebugComponents = new List<GameObject>();

    public struct MapLayer
    {
        public Canvas canvas;
        public CanvasGroup canvasGroup;

        //Layers within
        public RectTransform backgroundContainer;
        public RectTransform baseContainer;
        public RectTransform ductsContainer;

        public DrawingController drawingController;

        public Dictionary<Vector2, RawImage> baseBackgroundImages;
        public Dictionary<Vector2, Image> wallImages;
    }

    /*
        Walls setup:

        0 = nothing/blank
        1 = top only
        2 = right only
        3 = bottom only
        4 = left only

        5 = top and right
        6 = top and bottom
        7 = top and left

        8 = right and bottom
        9 = right and left

        10 = bottom and left

        11 = top, right and bottom
        12 = top, bottom and left
        13 = top, right and left

        14 = right, bottom and left

        15 = all
    */

    //Singleton pattern
    private static MapController _instance;
    public static MapController Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void Setup()
    {
        //Calculate real position multiplier
        realPositionMultiplier = nodePositionMultiplier / PathFinder.Instance.nodeSize.x;
        UpdateSize();

        BuildMap();

        //Set map name
        districtMapName.text = CityData.Instance.cityName;

        //Set key text
        keyUnexplored.text = Strings.Get("ui.interface", "MapKeyUnexplored");
        keyExploredSafe.text = Strings.Get("ui.interface", "MapKeyExploredSafe");
        keyExploredPrivate.text = Strings.Get("ui.interface", "MapKeyExploredPrivate");
        keyVent.text = Strings.Get("ui.interface", "MapKeyVent");
        keyDuct.text = Strings.Get("ui.interface", "MapKeyDuct");
        keyOpenHoursOnly.text = Strings.Get("ui.interface", "MapKeyOpenHoursOnly");

        //LoadBaseNodes();
        //LoadStreetNames();
        SetFloorLayer(load, true); //Load the current player postion

        //Update floor bar
        fzc.floorSlider.slider.minValue = CityData.Instance.floorRange.x;
        fzc.floorSlider.slider.maxValue = CityData.Instance.floorRange.y;
        fzc.floorSlider.slider.value = 0;

        //Setup display player char
        if (displayPlayerCharacter)
        {
            AddNewTrackedObject(Player.Instance.transform, PrefabControls.Instance.mapCharacterMarker, new Vector2(22, 22), PrefabControls.Instance.characterMarkerColor, true, Player.Instance);
        }

        //Start closed
        drag.parentRect.sizeDelta = new Vector2(0 * savedSize, drag.parentRect.sizeDelta.y);
        if (displayFirstPerson) displayFirstPerson = false;
        drag.gameObject.SetActive(false);
        fzc.gameObject.SetActive(false);
        DisplayDirectionArrow(false);
        this.enabled = false;
    }

    public void ControllerMapHoverChange(ButtonController hoveredButton, bool hovered)
    {
        CasePanelController.Instance.mapMO.ForceMouseOver(hovered);
        CasePanelController.Instance.mapScroll.controlEnabled = hovered;
        InteractionController.Instance.UpdateInteractionText();
    }

    //Build map layers (seperate canvases)
    public void BuildMap()
    {
        //Set size of background
        paperRect.sizeDelta = new Vector2(PathFinder.Instance.nodeCitySize.x * nodePositionMultiplier + positionBuffer,
            PathFinder.Instance.nodeCitySize.y * nodePositionMultiplier + positionBuffer);

        contentRect.sizeDelta = new Vector2(paperRect.sizeDelta.x + edgeBuffer * 2,
            paperRect.sizeDelta.y + edgeBuffer * 2);

        //baseLayer.anchoredPosition = new Vector2(edgeBuffer, edgeBuffer);
        pinsRect.anchoredPosition = new Vector2(edgeBuffer, edgeBuffer);
        overlayAll.anchoredPosition = new Vector2(edgeBuffer, edgeBuffer);
        routesRect.anchoredPosition = new Vector2(edgeBuffer, edgeBuffer);

        for (int i = (int)CityData.Instance.floorRange.x; i <= CityData.Instance.floorRange.y; i++)
        {
            MapLayer newLayer = new MapLayer();

            GameObject newCanvas = Instantiate(PrefabControls.Instance.mapLayerCanvas, contentRect);
            newLayer.canvas = newCanvas.GetComponent<Canvas>();
            newLayer.canvasGroup = newCanvas.GetComponent<CanvasGroup>();

            //Create a container for the background objects
            GameObject newContainer = Instantiate(PrefabControls.Instance.mapRectContainer, newCanvas.transform);
            newLayer.backgroundContainer = newContainer.GetComponent<RectTransform>();
            newLayer.backgroundContainer.anchoredPosition = new Vector2(edgeBuffer, edgeBuffer);
            newLayer.backgroundContainer.transform.name = "Background";

            //Create a container for the base objects
            GameObject newContainer2 = Instantiate(PrefabControls.Instance.mapRectContainer, newCanvas.transform);
            newLayer.baseContainer = newContainer2.GetComponent<RectTransform>();
            newLayer.baseContainer.transform.name = "Base";
            newLayer.baseContainer.anchoredPosition = new Vector2(edgeBuffer, edgeBuffer);

            //Create a container for air ducts
            GameObject newContainer3 = Instantiate(PrefabControls.Instance.mapRectContainer, newCanvas.transform);
            newLayer.ductsContainer = newContainer3.GetComponent<RectTransform>();
            newLayer.ductsContainer.transform.name = "Ducts";
            newLayer.ductsContainer.anchoredPosition = new Vector2(edgeBuffer, edgeBuffer);

            newLayer.baseBackgroundImages = new Dictionary<Vector2, RawImage>();
            newLayer.wallImages = new Dictionary<Vector2, Image>();

            List<NewFloor> floorsRepresented = new List<NewFloor>();
            List<NewGameLocation> addressRepresented = new List<NewGameLocation>();
            List<NewDoor> doorsRepresented = new List<NewDoor>();
            List<NewTile> alleyBlockersRepresented = new List<NewTile>();

            //Load node bases
            for (int l = 0; l < PathFinder.Instance.nodeCitySize.x; l++)
            {
                for (int u = 0; u < PathFinder.Instance.nodeCitySize.y; u++)
                {
                    Vector2 mapNodePos = new Vector2(l, u);
                    Vector3Int nodePos = new Vector3Int(l, u, i);

                    NewNode foundNode = null;

                    if (PathFinder.Instance.nodeMap.TryGetValue(nodePos, out foundNode))
                    {
                        //Setup doors...
                        foreach (NewWall wall in foundNode.walls)
                        {
                            if (wall.door != null)
                            {
                                if(!doorsRepresented.Contains(wall.door))
                                {
                                    NewNode otherNode = wall.node;
                                    if (wall.node == foundNode) otherNode = wall.otherWall.node;

                                    //Doors open outwards into the largest room...
                                    if(foundNode.room.nodes.Count >= otherNode.room.nodes.Count)
                                    {
                                        GameObject newDoorObj = Instantiate(PrefabControls.Instance.doorMapComponent, newLayer.baseContainer);
                                        RectTransform newDoorObjRect = newDoorObj.GetComponent<RectTransform>();

                                        newDoorObjRect.gameObject.SetActive(true); //Activate
                                        newDoorObjRect.anchoredPosition = RealPosToMap(wall.parentWall.position); //Set pos
                                        newDoorObjRect.localEulerAngles = new Vector3(0, 0, wall.parentWall.localEulerAngles.y);

                                        doorsRepresented.Add(wall.door);

                                        wall.door.mapDoorObject = newDoorObjRect.GetChild(0) as RectTransform;

                                        //Add to room reference in order to update
                                        wall.node.room.mapDoors.Add(newDoorObjRect);
                                        wall.otherWall.node.room.mapDoors.Add(newDoorObjRect);

                                        //Set as inactive if not discovered
                                        if(wall.node.room.explorationLevel < 1 && wall.otherWall.node.room.explorationLevel < 1)
                                        {
                                            newDoorObj.SetActive(false);
                                        }
                                    }
                                }
                            }
                        }

                        if (!addressRepresented.Contains(foundNode.gameLocation))
                        {
                            addressRepresented.Add(foundNode.gameLocation);
                        }

                        if(foundNode.gameLocation.floor != null)
                        {
                            if (!floorsRepresented.Contains(foundNode.gameLocation.floor))
                            {
                                floorsRepresented.Add(foundNode.gameLocation.floor);
                            }
                        }

                        //Setup street obstacles
                        if(foundNode.gameLocation.thisAsStreet != null && foundNode.isObstacle && !alleyBlockersRepresented.Contains(foundNode.tile))
                        {
                            GameObject block = Instantiate(PrefabControls.Instance.floorPlanBlockWall, newLayer.backgroundContainer);
                            block.GetComponent<RectTransform>().anchoredPosition = NodeCoordToMap(CityData.Instance.RealPosToNode(CityData.Instance.TileToRealpos(foundNode.tile.globalTileCoord)));
                            alleyBlockersRepresented.Add(foundNode.tile);
                        }
                    }
                }
            }

            //Create address buttons
            foreach (NewGameLocation gameLoc in addressRepresented)
            {
                GameObject newButtonObj = Instantiate(PrefabControls.Instance.mapButtonComponent, newLayer.backgroundContainer);
                MapAddressButtonController newButton = newButtonObj.GetComponent<MapAddressButtonController>();
                newButton.gameObject.SetActive(true); //Activate

                Vector2 minPos = new Vector2(999999, 999999);
                Vector2 rangeX = new Vector2(999999, -999999);
                Vector2 rangeY = new Vector2(999999, -999999);

                //Parent background to button
                foreach (NewNode node in gameLoc.nodes)
                {
                    //Get a would-be anchored position
                    Vector2 nodeAnchored = NodeCoordToMap(node.nodeCoord);

                    minPos.x = Mathf.Min(nodeAnchored.x - nodePositionMultiplier * 0.5f, minPos.x);
                    minPos.y = Mathf.Min(nodeAnchored.y - nodePositionMultiplier * 0.5f, minPos.y);

                    rangeX.x = Mathf.Min(nodeAnchored.x - nodePositionMultiplier * 0.5f, rangeX.x);
                    rangeX.y = Mathf.Max(nodeAnchored.x + nodePositionMultiplier * 0.5f, rangeX.y);

                    rangeY.x = Mathf.Min(nodeAnchored.y - nodePositionMultiplier * 0.5f, rangeY.x);
                    rangeY.y = Mathf.Max(nodeAnchored.y + nodePositionMultiplier * 0.5f, rangeY.y);
                }

                newButton.range = new Vector2(rangeX.y - rangeX.x, rangeY.y - rangeY.x);
                newButton.rect.anchoredPosition = minPos;
                newButton.rect.sizeDelta = newButton.range;
                newButton.rect.localScale = Vector3.one;

                newButton.Setup(gameLoc);
                newButton.transform.SetAsFirstSibling(); //Make sure walls appear infront

                buttons.Add(newButton);
            }

            //Create duct graphics
            foreach (NewFloor floor in floorsRepresented)
            {
                GameObject newButtonObj = Instantiate(PrefabControls.Instance.mapDuctComponent, newLayer.ductsContainer);
                MapDuctsButtonController newButton = newButtonObj.GetComponent<MapDuctsButtonController>();
                newButton.gameObject.SetActive(true); //Activate

                Vector2 minPos = new Vector2(999999, 999999);
                Vector2 rangeX = new Vector2(999999, -999999);
                Vector2 rangeY = new Vector2(999999, -999999);

                //Parent background to button
                foreach(NewGameLocation loc in floor.addresses)
                {
                    foreach (NewNode node in loc.nodes)
                    {
                        //Get a would-be anchored position
                        Vector2 nodeAnchored = NodeCoordToMap(node.nodeCoord);

                        minPos.x = Mathf.Min(nodeAnchored.x - nodePositionMultiplier * 0.5f, minPos.x);
                        minPos.y = Mathf.Min(nodeAnchored.y - nodePositionMultiplier * 0.5f, minPos.y);

                        rangeX.x = Mathf.Min(nodeAnchored.x - nodePositionMultiplier * 0.5f, rangeX.x);
                        rangeX.y = Mathf.Max(nodeAnchored.x + nodePositionMultiplier * 0.5f, rangeX.y);

                        rangeY.x = Mathf.Min(nodeAnchored.y - nodePositionMultiplier * 0.5f, rangeY.x);
                        rangeY.y = Mathf.Max(nodeAnchored.y + nodePositionMultiplier * 0.5f, rangeY.y);
                    }
                }

                newButton.range = new Vector2(rangeX.y - rangeX.x, rangeY.y - rangeY.x);
                newButton.rect.anchoredPosition = minPos;
                newButton.rect.sizeDelta = newButton.range;
                newButton.rect.localScale = Vector3.one;

                newButton.Setup(floor);
                newButton.transform.SetAsFirstSibling();
            }

            //Create drawing layer
            GameObject newDrawingLayer = Instantiate(PrefabControls.Instance.mapRectContainer, newCanvas.transform);

            //Setup drawing controller
            newLayer.drawingController = newDrawingLayer.AddComponent<DrawingController>();

            newLayer.drawingController.container = newDrawingLayer.GetComponent<RectTransform>();
            newLayer.drawingController.container.sizeDelta = contentRect.sizeDelta;
            newLayer.drawingController.container.anchoredPosition = Vector2.zero;
            newLayer.drawingController.container.transform.name = "Drawing";
            newLayer.drawingController.img = newDrawingLayer.AddComponent<RawImage>();
            newLayer.drawingController.img.texture = null;
            newLayer.drawingController.img.color = Color.clear;
            newLayer.drawingController.brushImage = drawBrushRect.GetComponent<RawImage>();
            newLayer.drawingController.brush = PrefabControls.Instance.drawingBrush;

            newLayer.drawingController.eraserButton = eraserButton;
            newLayer.drawingController.toggleDrawingButton = toggleDrawingButton;
            newLayer.drawingController.colourButton = colourButton;
            newLayer.drawingController.clearButton = clearButton;

            newLayer.drawingController.drawBrushRect = drawBrushRect;

            //Add to dictionary
            mapLayers.Add(i, newLayer);
        }

        mapCursor.SetAsLastSibling();
        routesRect.transform.SetAsLastSibling();
        pinsRect.transform.SetAsLastSibling();
        overlayAll.transform.SetAsLastSibling();
        drawBrushRect.SetAsLastSibling();
        tooltipOverride.SetAsLastSibling();

        //Load city boundary
        foreach (NewBuilding building in CityData.Instance.buildingDirectory)
        {
            if (building.preset.customDrawOnMap)
            {
                //These are visible across all layers
                GameObject newBuildingMap = Instantiate(PrefabControls.Instance.mapBuildingGraphic, overlayAll);
                RectTransform newBuildingRect = newBuildingMap.GetComponent<RectTransform>();
                RawImage raw = newBuildingMap.GetComponent<RawImage>();
                raw.texture = building.preset.tex;

                newBuildingRect.eulerAngles = new Vector3(0, 0, -building.buildingModelBase.transform.eulerAngles.y);
                newBuildingRect.anchoredPosition = NodeCoordToMap(CityData.Instance.RealPosToNode(CityData.Instance.CityTileToRealpos(building.cityTile.cityCoord)));
            }
        }

        //Load street names
        foreach (StreetController rc in CityData.Instance.streetDirectory)
        {
            //Find the longest straight line in the tile data...
            List<NewTile> largestSequence = new List<NewTile>();
            List<NewTile> largestSequence2 = new List<NewTile>(); //Sequence of identical length
            bool largestIsHorizontal = true;

            foreach (NewTile tile in rc.tiles)
            {
                //Search left and right, and up and down individually...
                for (int i = 0; i < 2; i++)
                {
                    List<NewTile> openSet = new List<NewTile>();
                    List<NewTile> closedSet = new List<NewTile>();
                    openSet.Add(tile);
                    int safety = 99;

                    //Search left/right
                    if (i == 0)
                    {
                        while (openSet.Count > 0 && safety > 0)
                        {
                            NewTile current = openSet[0];

                            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                            {
                                if (Mathf.Abs(v2.y) > 0) continue; //Only search left/right
                                Vector3Int searchCoord = current.globalTileCoord + new Vector3Int(v2.x, 0, 0);

                                NewTile foundTile;

                                if (PathFinder.Instance.tileMap.TryGetValue(searchCoord, out foundTile))
                                {
                                    if (!foundTile.isObstacle)
                                    {
                                        if(rc.tiles.Contains(foundTile))
                                        {
                                            //Add to open set
                                            if (!openSet.Contains(foundTile) && !closedSet.Contains(foundTile))
                                            {
                                                openSet.Add(foundTile);
                                            }
                                        }
                                    }
                                }
                            }

                            openSet.RemoveAt(0);
                            closedSet.Add(current);
                            safety--;
                        }
                    }
                    //Search up/down
                    else if (i == 1)
                    {
                        while (openSet.Count > 0 && safety > 0)
                        {
                            NewTile current = openSet[0];

                            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                            {
                                if (Mathf.Abs(v2.x) > 0) continue; //Only search up/down
                                Vector3Int searchCoord = current.globalTileCoord + new Vector3Int(0, v2.y, 0);

                                NewTile foundTile;

                                if (PathFinder.Instance.tileMap.TryGetValue(searchCoord, out foundTile))
                                {
                                    if (!foundTile.isObstacle)
                                    {
                                        if(rc.tiles.Contains(foundTile))
                                        {
                                            //Add to open set
                                            if (!openSet.Contains(foundTile) && !closedSet.Contains(foundTile))
                                            {
                                                openSet.Add(foundTile);
                                            }
                                        }
                                    }
                                }
                            }

                            openSet.RemoveAt(0);
                            closedSet.Add(current);
                            safety--;
                        }
                    }

                    //Is this sequence biggest than the previous largest?
                    if (closedSet.Count > largestSequence.Count)
                    {
                        largestSequence = closedSet;
                        largestSequence2.Clear(); //Clear past similar set

                        if (i == 0)
                        {
                            largestIsHorizontal = true;
                        }
                        else if (i == 1)
                        {
                            largestIsHorizontal = false;
                        }
                    }
                    else if (closedSet.Count == largestSequence.Count)
                    {
                        //Check that the different between coordinates is 1...
                        bool pass = true;
                        int failCount = 0;
                        int failThreshold = Mathf.RoundToInt(largestSequence.Count * 0.25f); //The other can have up to 75% adjacent tiles

                        if (largestIsHorizontal)
                        {
                            foreach (NewTile searchTile in closedSet)
                            {
                                if (!largestSequence.Exists(item => Mathf.Abs(item.globalTileCoord.y - searchTile.globalTileCoord.y) == 1))
                                {
                                    failCount++;
                                }

                                if (failCount >= failThreshold)
                                {
                                    pass = false;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            foreach (NewTile searchTile in closedSet)
                            {
                                if (!largestSequence.Exists(item => Mathf.Abs(item.globalTileCoord.x - searchTile.globalTileCoord.x) == 1))
                                {
                                    failCount++;
                                }

                                if (failCount >= failThreshold)
                                {
                                    pass = false;
                                    break;
                                }
                            }
                        }

                        //This set is the same as the largest but placed adjacent...
                        if (pass)
                        {
                            largestSequence2 = closedSet;
                        }
                    }
                }
            }

            //The largest sequence needs to cover at least 3 tiles to be valid...
            if (largestSequence.Count < 3) continue;

            //The text object appears in the centre of the tile space
            //Create steet name
            GameObject roadName = Instantiate(PrefabControls.Instance.streetName, mapLayers[(int)rc.nodes[0].nodeCoord.z].backgroundContainer);
            RectTransform roadRect = roadName.GetComponent<RectTransform>();
            TextMeshProUGUI nameText = roadName.GetComponent<TextMeshProUGUI>();
            nameText.text = rc.name;

            //Average position
            Vector3 av = Vector3.zero;

            foreach (NewTile tile in largestSequence)
            {
                foreach (NewNode node in tile.nodes)
                {
                    av += node.nodeCoord;
                }
            }

            foreach (NewTile tile in largestSequence2)
            {
                foreach (NewNode node in tile.nodes)
                {
                    av += node.nodeCoord;
                }
            }

            //foreach (NewTile tile in largestSequence) av += tile.globalTileCoord * 3f;
            av /= (float)((largestSequence.Count + largestSequence2.Count) * 9);

            roadRect.anchoredPosition = NodeCoordToMap(av);
            if (!largestIsHorizontal) roadRect.localEulerAngles = new Vector3(0, 0, 90); //Set angle to vertical
        }

        //Load district names
        foreach(DistrictController dc in CityData.Instance.districtDirectory)
        {
            if (dc.cityTiles.Count <= 0) continue;

            //Pick the most central city tile
            Vector2 centreAv = Vector2.zero;

            foreach(CityTile t in dc.cityTiles)
            {
                centreAv += t.cityCoord;
            }

            centreAv /= (float)dc.cityTiles.Count;

            //Now find the closest tile to this
            float closestDist = Mathf.Infinity;
            CityTile closestTile = dc.cityTiles[0];

            foreach(CityTile t in dc.cityTiles)
            {
                float dist = Vector2.Distance(t.cityCoord, centreAv);

                if(closestTile == null || dist < closestDist)
                {
                    closestTile = t;
                    closestDist = dist;
                }
            }

            //We now have the tile closest to the centre of the district
            //Create steet name
            GameObject districtName = Instantiate(PrefabControls.Instance.districtName, mapLayers[0].backgroundContainer);
            RectTransform districtRect = districtName.GetComponent<RectTransform>();
            TextMeshProUGUI nameText = districtName.GetComponent<TextMeshProUGUI>();
            nameText.text = dc.name;

            //To position, get the node coordinate: Display on top of city tile
            Vector3 nodeCoord = CityData.Instance.CityTileToRealpos(closestTile.cityCoord + new Vector2(0, 0.5f));
            districtRect.anchoredPosition = NodeCoordToMap(CityData.Instance.RealPosToNode(nodeCoord));
        }
    }

    //Add a map address button update call
    public void AddUpdateCall(MapAddressButtonController loc)
    {
        if (loc == null) return;

        if(!mapUpdateList.Contains(loc))
        {
            //Game.Log("Adding map image update call for " + loc.name);
            mapUpdateList.Add(loc);
        }
    }

    public void AddDuctUpdateCall(MapDuctsButtonController loc)
    {
        if (loc == null) return;

        if(!ductsUpdateList.Contains(loc))
        {
            //Game.Log("Adding ducts image update call for " + loc.name);
            ductsUpdateList.Add(loc);
        }
    }

    //Pinned update
    public void OnPinNewEvidence(Evidence ev)
    {
        ////Is this evidence pin compatible?
        //if(win.passedEvidence.preset.name == "location" || win.passedEvidence.preset.name == "citizen" || win.passedEvidence.preset.name == "building")
        //{
        //    //Create pin object
        //    if (!pinnedObjects.ContainsKey(win))
        //    {
        //        GameObject newPinnedImage = Instantiate(PrefabControls.Instance.mapPinnedIcon, pinsRect);
        //        MapPinButtonController pinCont = newPinnedImage.GetComponent<MapPinButtonController>();
        //        pinCont.Setup(win);
        //        pinnedObjects.Add(win, pinCont);

        //        //Listen for data key changes for toggling visibility
        //        if(win.passedEvidence.preset.useDataKeys)
        //        {
        //            win.OnWindowDataKeysUpdated += PinnedDataKeyChange;
        //        }

        //        //Is this object dynamic?
        //        if (win.passedEvidence.preset.name == "citizen")
        //        {
        //            AddNewTrackedObject(win.passedEvidence.controller.gameObject.transform, pinCont.rect, true);
        //        }
        //        else
        //        {
        //            AddNewTrackedObject(win.passedEvidence.controller.gameObject.transform, pinCont.rect, false);
        //        }
        //    }
        //}
    }

    //Pinned update
    public void OnUnpinEvidence(Evidence ev)
    {
        ////Is this evidence pin compatible?
        //if (win.passedEvidence.preset.name == "location" || win.passedEvidence.preset.name == "citizen" || win.passedEvidence.preset.name == "building")
        //{
        //    //Create pin object
        //    if (pinnedObjects.ContainsKey(win))
        //    {
        //        //Is this object dynamic?
        //        if (win.passedEvidence.preset.name == "citizen")
        //        {
        //            RemoveTrackedObject(win.passedEvidence.controller.gameObject.transform, pinnedObjects[win].rect);
        //        }

        //        Destroy(pinnedObjects[win].gameObject);
        //        pinnedObjects.Remove(win);

        //        //Unsubscribe from data key changes
        //        if (win.passedEvidence.preset.useDataKeys)
        //        {
        //            win.OnWindowDataKeysUpdated -= PinnedDataKeyChange;
        //            invisiblePins.Remove(win);
        //        }
        //    }
        //}
    }

    public void PinnedDataKeyChange()
    {
        Game.Log("Pinned data key change");

        //Check if visible/invisible and add to list appropriately
        foreach(KeyValuePair<InfoWindow, MapPinButtonController> pair in pinnedObjects)
        {
            if(pair.Key.passedEvidence.preset.useDataKeys)
            {
                if (pair.Key.evidenceKeys.Contains(Evidence.DataKey.name))
                {
                    invisiblePins.Remove(pair.Key);
                    pair.Value.gameObject.SetActive(true);
                }
                else
                {
                    invisiblePins.Add(pair.Key);
                    pair.Value.gameObject.SetActive(false);
                }
            }
        }
    }

    //Adding a tracked object will automatically position/rotate it on the map and display or hide it based on the loaded floor
    public void AddNewTrackedObject(Transform gameObj, Sprite mapIcon, Vector2 size, Color colour, bool isDynamic, object buttonReference)
    {
        //Create a new map object
        GameObject newChar = Instantiate(PrefabControls.Instance.playerMarker, overlayAll);
        RectTransform newMapObj = newChar.GetComponent<RectTransform>();
        newMapObj.sizeDelta = size;
        Image img = newChar.GetComponent<Image>();
        img.sprite = mapIcon;

        ButtonController butt = newChar.GetComponent<ButtonController>();
        butt.SetButtonBaseColour(colour);
        butt.genericReference = buttonReference;
        butt.OnPress += PressTracked;
        butt.OnHoverChange += HoverTracked;

        if (isDynamic)
        {
            if (!dynamicTrackedObjects.ContainsKey(gameObj)) dynamicTrackedObjects.Add(gameObj, new List<RectTransform>());
            dynamicTrackedObjects[gameObj].Add(newMapObj);
        }
        else
        {
            if (!staticTrackedObjects.ContainsKey(gameObj)) staticTrackedObjects.Add(gameObj, new List<RectTransform>());
            staticTrackedObjects[gameObj].Add(newMapObj);
        }

        //Set position & transparency
        UpdateTrackedObject(gameObj, newMapObj);
    }

    public void PressTracked(ButtonController pressedButton)
    {
        if(pressedButton.genericReference != null)
        {
            Evidence getEv = pressedButton.genericReference as Evidence;

            if(getEv != null)
            {
                SessionData.Instance.PauseGame(true);
                InterfaceController.Instance.SpawnWindow(getEv, Evidence.DataKey.photo);
            }
        }
    }

    public void HoverTracked(ButtonController hoveredButton, bool hovered)
    {
        if(hovered)
        {
            if (hoveredButton.genericReference != null)
            {
                Evidence getEv = hoveredButton.genericReference as Evidence;

                if (getEv != null)
                {
                    if(hoveredButton.tooltip != null)
                    {
                        hoveredButton.tooltip.mainText = getEv.GetNameForDataKey(Evidence.DataKey.photo);
                    }
                }

                Player getPlayer = hoveredButton.genericReference as Player;

                if (getPlayer != null)
                {
                    if (hoveredButton.tooltip != null)
                    {
                        hoveredButton.tooltip.mainText = Player.Instance.GetCitizenName();
                    }
                }
            }
        }
    }

    public void RemoveTrackedObject(Transform gameObj)
    {
        if(dynamicTrackedObjects.ContainsKey(gameObj))
        {
            for (int i = 0; i < dynamicTrackedObjects[gameObj].Count; i++)
            {
                ButtonController butt = dynamicTrackedObjects[gameObj][i].GetComponent<ButtonController>();
                butt.OnPress -= PressTracked;
                butt.OnHoverChange -= HoverTracked;

                Destroy(dynamicTrackedObjects[gameObj][i]);
            }

            dynamicTrackedObjects.Remove(gameObj);
        }

        if(staticTrackedObjects.ContainsKey(gameObj))
        {
            for (int i = 0; i < staticTrackedObjects[gameObj].Count; i++)
            {
                ButtonController butt = staticTrackedObjects[gameObj][i].GetComponent<ButtonController>();
                butt.OnPress -= PressTracked;
                butt.OnHoverChange -= HoverTracked;

                Destroy(staticTrackedObjects[gameObj][i]);
            }

            staticTrackedObjects.Remove(gameObj);
        }
    }

    //Update if the object is visible on this map plane
    public void UpdateTrackedObject(Transform gameObj, RectTransform mapObj)
    {
        //Determin if this object is active/inactive on the loaded layer
        int floor = Mathf.FloorToInt(gameObj.position.y / PathFinder.Instance.nodeSize.z);

        //mapObj.SetParent(floorLayers[floor].overlayLayer);
        mapObj.localEulerAngles = new Vector3(0, 0, -gameObj.localRotation.eulerAngles.y);
        mapObj.anchoredPosition = RealPosToMap(gameObj.transform.position);

        //Set it's transparency also
        int floorDiff = Mathf.Abs(load - floor);

        if (floorDiff > 0)
        {
            float transProgress = 1f - (Mathf.Clamp(floorDiff, 0, 11) / 10f);
            float alpha = Mathf.Lerp(0.1f, 0.5f, transProgress);
            mapObj.GetComponent<CanvasRenderer>().SetAlpha(alpha);
        }
        else
        {
            mapObj.GetComponent<CanvasRenderer>().SetAlpha(1f);
        }
    }

    //Centre the view on a tracked object
    public void CentreOnTrackedObject(Transform gameObj, bool instant = false)
    {
        //Kill inertia
        scrollRect.velocity = Vector2.zero;

        RectTransform mapObj = null;

        if (dynamicTrackedObjects.ContainsKey(gameObj)) mapObj = dynamicTrackedObjects[gameObj][0];
        else if (staticTrackedObjects.ContainsKey(gameObj)) mapObj = staticTrackedObjects[gameObj][0];
        else return;

        //Determin if this object is active/inactive on the loaded layer
        int floor = Mathf.FloorToInt(gameObj.position.y / PathFinder.Instance.nodeSize.z);

        //Load the correct floor
        if (load != floor) SetFloorLayer(floor);

        //Centre the camera
        CentreOnObject(mapObj, instant);
    }

    //Centre the view on a map object (must be drawn)
    public void CentreOnObject(RectTransform mapObj, bool instant = false, bool showPointer = false)
    {
        //Kill inertia
        scrollRect.velocity = Vector2.zero;
        focusRect = mapObj;

        if (!instant)
        {
            forceFocusProgress = 0f;
            forceFocusActive = true;
        }
        else
        {
            //Get the position of the object relative to the scrollrect content
            Vector3 rel = contentRect.InverseTransformPoint(focusRect.position) + new Vector3(contentRect.sizeDelta.x * 0.5f, contentRect.sizeDelta.y * 0.5f, 0);

            Vector2 mapScrollPos = new Vector2(Mathf.Clamp01(rel.x / contentRect.sizeDelta.x), Mathf.Clamp01(rel.y / contentRect.sizeDelta.y));
            scrollRect.content.pivot = new Vector2(0.5f, 0.5f);
            scrollRect.normalizedPosition = mapScrollPos;
            //Game.Log(mapScrollPos);
        }

        //Show pointer
        if(showPointer)
        {
            GameObject newPointer = Instantiate(PrefabControls.Instance.mapPointer, pinsRect);
            RectTransform pointerRect = newPointer.GetComponent<RectTransform>();

            FlashController pointerFlash = newPointer.GetComponent<FlashController>();
            pointerFlash.Flash(3);

            PointerData newData = new PointerData();
            newData.pointerObject = pointerRect;
            newData.followRect = mapObj;
            newData.pointerShow = 1.5f;

            pointers.Add(newData);

            //Set size and position
            pointerRect.sizeDelta = mapObj.sizeDelta + new Vector2(8f, 8f);
            pointerRect.position = mapObj.position - new Vector3(4f, 4f, 0f);
        }
    }

    //Centre the view on world coordinate
    public void CentreOnNodeCoordinate(Vector3 pathCoord, bool instant = false, bool showPointer = false)
    {
        //Kill inertia
        scrollRect.velocity = Vector2.zero;

        //Real coord to map
        Vector2 mapCoord = NodeCoordToMap(pathCoord);

        focusRect = null;
        focusPos = mapCoord;

        //Determin if this object is active/inactive on the loaded layer
        int floor = Mathf.FloorToInt(pathCoord.z);

        //Load the correct floor
        if (load != floor) SetFloorLayer(floor);

        if (!instant)
        {
            forceFocusProgress = 0f;
            forceFocusActive = true;
        }
        else
        {
            contentRect.anchoredPosition = ClampMapScrollPosition(focusPos);
        }

        //Show pointer
        if (showPointer)
        {
            GameObject newPointer = Instantiate(PrefabControls.Instance.mapPointer, pinsRect);
            RectTransform pointerRect = newPointer.GetComponent<RectTransform>();

            FlashController pointerFlash = newPointer.GetComponent<FlashController>();
            pointerFlash.Flash(3);

            PointerData newData = new PointerData();
            newData.pointerObject = pointerRect;
            newData.followPos = mapCoord;
            newData.pointerShow = 1.5f;

            pointers.Add(newData);

            //Set size and position
            pointerRect.sizeDelta = new Vector2(32, 32);
            pointerRect.position = mapCoord;
        }
    }

    public Vector2 ClampMapScrollPosition(Vector2 focusPos)
    {
        float clampX = (contentRect.sizeDelta.x * 0.5f) - (viewport.rect.width * 0.5f);
        float clampY = (contentRect.sizeDelta.y * 0.5f) - (viewport.rect.height * 0.5f);

        Game.Log("Focus pos: " + focusPos);

        //Clamp to scroll area
        focusPos = new Vector2
            (
                Mathf.Clamp(-focusPos.x, -clampX, clampX),
                Mathf.Clamp(-focusPos.y, -clampY, clampY)
            );

        Game.Log("Clamped: " + focusPos);

        //The anchored position of the rect is the inverse of where this rect's anchor should be.
        return focusPos;
    }

    //Load a floor layer
    public void SetFloorLayer(int newFloor, bool forceLoad = false)
    {
        //Clamp to floor range
        newFloor = Mathf.Clamp(newFloor, (int)CityData.Instance.floorRange.x, (int)CityData.Instance.floorRange.y);

        //Don't do this if the floor is already loaded...
        if (newFloor == load && !forceLoad) return;

        load = newFloor;

        //Cycle through layers and activate/deactivate them
        for (int i = (int)CityData.Instance.floorRange.x; i <= CityData.Instance.floorRange.y; i++)
        {
            if (i == load)
            {
                if (!mapLayers[i].canvas.gameObject.activeSelf)
                {
                    mapLayers[i].canvas.gameObject.SetActive(true);
                }
            }
            else
            {
                if (mapLayers[i].canvas.gameObject.activeSelf)
                {
                    mapLayers[i].canvas.gameObject.SetActive(false);
                }

                //Disable drawing
                mapLayers[i].drawingController.SetDrawingActive(false);
            }
        }

        //Enable drawing mode on active layer
        if (drawingMode)
        {
            mapLayers[load].drawingController.SetDrawingActive(true);
        }

        if(!mapLayers.ContainsKey(load))
        {
            Game.LogError("Map layer has no entry for " + load + " (it contains " + mapLayers.Count + " entries)");
        }

        //Switch drawing brush to the correct layer
        drawBrushRect.SetParent(mapLayers[load].drawingController.container);
        drawBrushRect.SetAsLastSibling();

        //Updates
        if (fzc != null)
        {
            //Switch on/off desaturate layer
            if (load == 0)
            {
                fzc.floorText.text = "G";
            }
            else
            {
                fzc.floorText.text = load.ToString();
            }

            fzc.floorSlider.SetValueWithoutNotify(load);

            //Update tracked objects
            foreach (KeyValuePair<Transform, List<RectTransform>> pair in dynamicTrackedObjects)
            {
                foreach (RectTransform rt in pair.Value)
                {
                    UpdateTrackedObject(pair.Key, rt);
                }
            }

            //Also update static tracked objects as the transparency needs to be changed
            foreach (KeyValuePair<Transform, List<RectTransform>> pair in staticTrackedObjects)
            {
                foreach (RectTransform rt in pair.Value)
                {
                    UpdateTrackedObject(pair.Key, rt);
                }
            }

            //Update player route
            if(playerRoute != null)
            {
                playerRoute.UpdateDrawnRoute();
            }
        }
    }

    public Vector2 NodeCoordToMap(Vector3 pos)
    {
        return new Vector2((pos.x + 0.5f) * nodePositionMultiplier + positionBuffer, (pos.y + 0.5f) * nodePositionMultiplier + positionBuffer);
    }

    public Vector2 RealPosToMap(Vector3 coords)
    {
        Vector2 output = new Vector2
        (
            ((coords.x + ((CityData.Instance.citySize.x * 0.5f) * CityControls.Instance.cityTileSize.x))) * realPositionMultiplier + positionBuffer,
            ((coords.z + ((CityData.Instance.citySize.y * 0.5f) * CityControls.Instance.cityTileSize.y))) * realPositionMultiplier + positionBuffer
        );

        return output;
    }

    public Vector2 MapToNode(Vector2 coords)
    {
        Vector2 output = new Vector2
            (
                Mathf.Clamp(Mathf.FloorToInt(coords.x / nodePositionMultiplier), 0, PathFinder.Instance.nodeCitySize.x - 1),
                Mathf.Clamp(Mathf.FloorToInt(coords.y / nodePositionMultiplier), 0, PathFinder.Instance.nodeCitySize.y - 1)
            );

        return output;
    }

    private void Update()
    {
        if (!SessionData.Instance.startedGame) return;

        if(!MainMenuController.Instance.mainMenuActive)
        {
            //Update buttons (1 per frame)
            if (mapUpdateList.Count > 0)
            {
                mapUpdateList[0].UpdateMapImageEndOfFrame();
                mapUpdateList.RemoveAt(0);
            }

            //Update ducts
            if (ductsUpdateList.Count > 0)
            {
                if (ductsUpdateList[0] != null) ductsUpdateList[0].UpdateMapImageEndOfFrame();
                ductsUpdateList.RemoveAt(0);
            }

            //Update tracked objects
            foreach (KeyValuePair<Transform, List<RectTransform>> pair in dynamicTrackedObjects)
            {
                foreach (RectTransform rt in pair.Value)
                {
                    UpdateTrackedObject(pair.Key, rt);
                }
            }

            //If displaying in temporary first person mode, always focus on the player
            if (displayFirstPerson)
            {
                if (mapCloseButton != null) mapCloseButton.gameObject.SetActive(false);

                if (!InputController.Instance.player.GetButton("Map"))
                {
                    CloseMap(); //Trigger close
                }

                CentreOnTrackedObject(Player.Instance.transform, true);
            }
            else if (mapCloseButton != null && !mapCloseButton.gameObject.activeSelf)
            {
                mapCloseButton.gameObject.SetActive(true);
            }

            //Set pointers
            for (int i = 0; i < pointers.Count; i++)
            {
                PointerData pd = pointers[i];

                //Remove
                if (pd.pointerShow <= 0f)
                {
                    Destroy(pd.pointerObject.gameObject);
                    pointers.RemoveAt(i);
                    i--;
                    continue;
                }
                else
                {
                    //Set size and position
                    if (pd.followRect != null)
                    {
                        pd.pointerObject.sizeDelta = new Vector2(Mathf.Max(pd.followRect.sizeDelta.x + 8f, 32), Mathf.Max(pd.followRect.sizeDelta.y + 8f, 32));
                        pd.pointerObject.position = pd.followRect.position;
                    }
                    else
                    {
                        pd.pointerObject.sizeDelta = new Vector2(32, 32);
                        pd.pointerObject.localPosition = pd.followPos;
                    }

                    pd.pointerShow -= Time.fixedDeltaTime;
                }
            }

            //Force focus
            if (forceFocusActive)
            {
                Vector3 rel = Vector3.zero;

                if (focusRect != null)
                {
                    rel = contentRect.InverseTransformPoint(focusRect.position) + new Vector3(contentRect.sizeDelta.x * 0.5f, contentRect.sizeDelta.y * 0.5f, 0);
                }
                else
                {
                    rel = focusPos;
                }

                Vector2 mapScrollPos = new Vector2(Mathf.Clamp01(rel.x / contentRect.sizeDelta.x), Mathf.Clamp01(rel.y / contentRect.sizeDelta.y));

                forceFocusProgress += Time.deltaTime * focusSpeed;

                scrollRect.content.pivot = new Vector2(0.5f, 0.5f);
                scrollRect.normalizedPosition = Vector2.Lerp(scrollRect.normalizedPosition, mapScrollPos, forceFocusProgress);

                if (forceFocusProgress >= 1f)
                {
                    forceFocusProgress = 0f;
                    forceFocusActive = false;
                }
            }

            //Controller select
            if(!InputController.Instance.mouseInputMode && CasePanelController.Instance.mapScroll.controlEnabled)
            {
                //Select closest to middle...
                //Get screen postion of middle of rect
                Vector3[] corners = new Vector3[4];
                (scrollRect.transform as RectTransform).GetWorldCorners(corners);

                Vector3 middle = Vector3.zero;

                foreach (Vector3 corner in corners)
                {
                    middle.x += corner.x;
                    middle.y += corner.y;
                }

                middle.x /= 4;
                middle.y /= 4;

                MapAddressButtonController closest = null;
                float closestDist = Mathf.Infinity;

                foreach(MapAddressButtonController but in buttons)
                {
                    if(but.gameObject.activeInHierarchy)
                    {
                        float dist = Vector3.Distance(but.gameObject.transform.position, middle);

                        if(dist < closestDist)
                        {
                            closest = but;
                            closestDist = dist;
                        }
                    }
                }

                foreach (MapAddressButtonController but in buttons)
                {
                    if (but.gameObject.activeInHierarchy && but == closest)
                    {
                        but.OnHoverStart();
                    }
                    else
                    {
                        but.OnHoverEnd();
                    }
                }

                //Open address
                if(closest != null && InputController.Instance.player.GetButtonDown("Select"))
                {
                    closest.OnLeftDoubleClick();
                }
            }

            //Map cursor
            if (!drawingMode && mapContextMenu != null && mapContextMenu.spawnedMenu == null)
            {
                Vector2 mousePoint = Vector2.zero;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(overlayAll, Input.mousePosition, null, out mousePoint);
                mapCursor.localPosition = mousePoint;

                Vector2 mapCoord = MapToNode(mapCursor.anchoredPosition);
                Vector3Int nodeCoord = new Vector3Int(Mathf.RoundToInt(mapCoord.x), Mathf.RoundToInt(mapCoord.y), load);

                if (PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out mapCursorNode))
                {
                    //Only run this on node change
                    if (mapCursorNode != cursorNodeChange)
                    {
                        cursorPos = NodeCoordToMap(nodeCoord);

                        //Enable/disable menu items
                        if (mapCursorNode.gameLocation == null || mapCursorNode.gameLocation.evidenceEntry == null)
                        {
                            if (!mapContextMenu.disabledItems.Contains("OpenEvidence"))
                            {
                                mapContextMenu.disabledItems.Add("OpenEvidence");
                            }

                            if (!mapContextMenu.disabledItems.Contains("PlotRoute"))
                            {
                                mapContextMenu.disabledItems.Add("PlotRoute");
                            }
                        }
                        else
                        {
                            mapContextMenu.disabledItems.Remove("OpenEvidence");

                            if (mapCursorNode.isObstacle)
                            {
                                if (!mapContextMenu.disabledItems.Contains("PlotRoute"))
                                {
                                    mapContextMenu.disabledItems.Add("PlotRoute");
                                }
                            }
                            else
                            {
                                mapContextMenu.disabledItems.Remove("PlotRoute");
                            }
                        }

                        if (!drawingMode)
                        {
                            mapCursor.gameObject.SetActive(true);
                        }

                        cursorNodeChange = mapCursorNode;
                    }

                    mapCursor.anchoredPosition = cursorPos;
                }
                else
                {
                    mapCursorNode = null;
                    cursorNodeChange = null;
                    mapCursor.gameObject.SetActive(false);
                }
            }
        }
    }

    private void OnEnable()
    {
        //Listen for selected colour change
        colourButton.OnChangeColour += OnChangeDrawingColour;

        //On map controls
        controllerSelectMapButton.OnHoverChange += ControllerMapHoverChange;

        //Check and listen for pinned items update
        CasePanelController.Instance.OnPinEvidence += OnPinNewEvidence;
        CasePanelController.Instance.OnUnpinEvidence += OnUnpinEvidence;

        ControllerMapHoverChange(null, false);

        if (drag != null)
        {
            drag.OnDragged += UpdateSize;
        }

        //Update player route
        if(playerRoute != null)
        {
            playerRoute.UpdateDrawnRoute();
        }
    }

    private void OnDisable()
    {
        //Listen for selected colour change
        colourButton.OnChangeColour -= OnChangeDrawingColour;

        //On map controls
        controllerSelectMapButton.OnHoverChange -= ControllerMapHoverChange;

        //Check and listen for pinned items update
        CasePanelController.Instance.OnPinEvidence -= OnPinNewEvidence;
        CasePanelController.Instance.OnUnpinEvidence -= OnUnpinEvidence;

        if (drag != null)
        {
            drag.OnDragged -= UpdateSize;
        }
    }

    private void UpdateSize()
    {
        savedSize = drag.parentRect.sizeDelta.x;
    }

    public void OpenMap(bool firstPerson, bool playSound = true)
    {
        //Game.Log("OpenMap");
        displayFirstPerson = firstPerson;

        InterfaceController.Instance.minimapCanvas.gameObject.SetActive(true);
        this.enabled = true;

        StopCoroutine("Open");
        StopCoroutine("Close");
        StartCoroutine("Open");

        SessionData.Instance.TutorialTrigger("map");

        //Play SFX
        if (playSound) AudioController.Instance.Play2DSound(AudioControls.Instance.mapSlideIn);

        //Enable top bar navigation
        if(InterfaceController.Instance.minimapCanvas.gameObject.activeSelf && !displayFirstPerson)
        {
            InterfaceController.Instance.notebookButton.RefreshAutomaticNavigation();
            InterfaceController.Instance.upgradesButton.RefreshAutomaticNavigation();
            InterfaceController.Instance.mapButton.RefreshAutomaticNavigation();
            CasePanelController.Instance.selectNoCaseButton.RefreshAutomaticNavigation();
            CasePanelController.Instance.stickNoteButton.RefreshAutomaticNavigation();

            //Toolbox.Instance.AddNavigationInput(InterfaceController.Instance.notebookButton.button, newDown: centreOnPlayerButton.button);
            //Toolbox.Instance.AddNavigationInput(InterfaceController.Instance.upgradesButton.button, newDown: controllerSelectMapButton.button);
            //Toolbox.Instance.AddNavigationInput(InterfaceController.Instance.mapButton.button, newDown: controllerSelectMapButton.button);
            //Toolbox.Instance.AddNavigationInput(CasePanelController.Instance.selectNoCaseButton.button, newDown: controllerSelectMapButton.button);
            //Toolbox.Instance.AddNavigationInput(CasePanelController.Instance.stickNoteButton.button, newDown: controllerSelectMapButton.button);
        }
    }

    IEnumerator Open()
    {
        if (!displayFirstPerson) fzc.gameObject.SetActive(true);
        else fzc.gameObject.SetActive(false);

        //Position on player by default
        CentreOnTrackedObject(Player.Instance.transform, true);

        while(openProgress < 1f)
        {
            openProgress += Time.deltaTime * 20f;
            openProgress = Mathf.Clamp01(openProgress);
            drag.SetSize(openProgress * savedSize);
            InterfaceController.Instance.minimapCanvasGroup.alpha = openProgress;

            yield return null;
        }

        //Enable drag
        drag.gameObject.SetActive(true);
        scrollRect.enabled = true;
    }

    public void CloseMap(bool playSound = true)
    {
        //Game.Log("CloseMap");
        StopCoroutine("Open");
        StopCoroutine("Close");
        StartCoroutine("Close");

        //Play SFX
        if (playSound) AudioController.Instance.Play2DSound(AudioControls.Instance.mapSlideOut);

        //Disable top bar navigation
        Navigation nav = new Navigation { mode = Navigation.Mode.Explicit };
        nav.selectOnLeft = InterfaceController.Instance.notebookButton.button.FindSelectableOnLeft();
        nav.selectOnRight = InterfaceController.Instance.notebookButton.button.FindSelectableOnRight();
        InterfaceController.Instance.notebookButton.button.navigation = nav;

        Navigation nav2 = new Navigation { mode = Navigation.Mode.Explicit };
        nav2.selectOnLeft = InterfaceController.Instance.upgradesButton.button.FindSelectableOnLeft();
        nav2.selectOnRight = InterfaceController.Instance.upgradesButton.button.FindSelectableOnRight();
        InterfaceController.Instance.upgradesButton.button.navigation = nav2;

        Navigation nav3 = new Navigation { mode = Navigation.Mode.Explicit };
        nav3.selectOnLeft = InterfaceController.Instance.mapButton.button.FindSelectableOnLeft();
        nav3.selectOnRight = InterfaceController.Instance.mapButton.button.FindSelectableOnRight();
        InterfaceController.Instance.mapButton.button.navigation = nav3;

        Navigation nav4 = new Navigation { mode = Navigation.Mode.Explicit };
        nav4.selectOnLeft = CasePanelController.Instance.selectNoCaseButton.button.FindSelectableOnLeft();
        nav4.selectOnRight = CasePanelController.Instance.selectNoCaseButton.button.FindSelectableOnRight();
        CasePanelController.Instance.selectNoCaseButton.button.navigation = nav4;

        Navigation nav5 = new Navigation { mode = Navigation.Mode.Explicit };
        nav5.selectOnLeft = CasePanelController.Instance.stickNoteButton.button.FindSelectableOnLeft();
        nav5.selectOnRight = CasePanelController.Instance.stickNoteButton.button.FindSelectableOnRight();
        CasePanelController.Instance.stickNoteButton.button.navigation = nav5;
    }

    IEnumerator Close()
    {
        scrollRect.enabled = false;

        //Enable drag
        if (displayFirstPerson) displayFirstPerson = false;
        drag.gameObject.SetActive(false);

        while (openProgress > 0f)
        {
            openProgress -= Time.deltaTime * 20f;
            openProgress = Mathf.Clamp01(openProgress);
            drag.SetSize(openProgress * savedSize);

            InterfaceController.Instance.minimapCanvasGroup.alpha = openProgress;

            yield return null;
        }

        //fzc.gameObject.SetActive(false);
        this.enabled = false;
        InterfaceController.Instance.minimapCanvas.gameObject.SetActive(false);
    }

    //Locate a piece of evidence on the map
    public void LocateEvidenceOnMap(Evidence ev)
    {
        //Evidence is location
        EvidenceLocation loc = ev as EvidenceLocation;
        EvidenceBuilding build = ev as EvidenceBuilding;

        if (loc != null)
        {
            if(!InterfaceController.Instance.showDesktopMap)
            {
                InterfaceController.Instance.SetShowDesktopMap(true, true);
            }

            //Get a coordinate within this...
            if(loc.locationController.nodes.Count > 0)
            {
                NewNode anyNode = loc.locationController.nodes[0];
                SetFloorLayer((int)anyNode.nodeCoord.z); //Set floor

                //Centre on button
                MapAddressButtonController foundButton = buttons.Find(item => item.gameLocation.evidenceEntry == loc);

                if(foundButton != null)
                {
                    CentreOnObject(foundButton.rect, instant: false, showPointer: true);
                }
            }
        }
        else if(build != null)
        {
            if (!InterfaceController.Instance.showDesktopMap)
            {
                InterfaceController.Instance.SetShowDesktopMap(true, true);
            }

            foreach (NewAddress lob in build.building.lobbies)
            {
                if (lob.entrances.Count <= 0) continue;

                //Centre on button
                MapAddressButtonController foundButton = buttons.Find(item => item.gameLocation == lob);

                if (foundButton != null)
                {
                    CentreOnObject(foundButton.rect, instant: false, showPointer: true);
                    break;
                }
            }
        }
    }

    //Locate a room on the map
    public void LocateRoomOnMap(NewRoom room)
    {
        if (room != null)
        {
            if (!InterfaceController.Instance.showDesktopMap)
            {
                InterfaceController.Instance.SetShowDesktopMap(true, true);
            }

            //Get a coordinate within this...
            NewNode anyNode = room.nodes.FirstOrDefault();
            SetFloorLayer((int)anyNode.nodeCoord.z); //Set floor

            //Find the middle of the room in node coordinates...
            Vector3 middle = Vector3.zero;

            foreach(NewNode n in room.nodes)
            {
                middle += n.nodeCoord;
            }

            middle /= (float)room.nodes.Count;

            CentreOnNodeCoordinate(middle, instant: false, showPointer: true);
        }
    }

    //Plot a new route (player)
    public void PlotPlayerRoute(Evidence ev)
    {
        Game.Log("Interface: Plot route from evidence...");

        //Evidence is location
        EvidenceLocation loc = ev as EvidenceLocation;
        EvidenceBuilding build = ev as EvidenceBuilding;

        if (loc != null)
        {
            Game.Log("Interface: Plotting route to location...");
            PlotPlayerRoute(loc.locationController);
        }
        else if (build != null)
        {
            bool plottedRoute = false;

            foreach(NewAddress lob in build.building.lobbies)
            {
                if (lob.entrances.Count <= 0)
                {
                    continue;
                }
                else
                {
                    Game.Log("Interface: Plotting route to building...");
                    PlotPlayerRoute(lob);
                    plottedRoute = true;
                    break;
                }
            }

            if(!plottedRoute)
            {
                if(build.building.mainEntrance != null)
                {
                    PlotPlayerRoute(build.building.mainEntrance.node, false);
                    plottedRoute = true;
                }
            }
        }
    }

    public void PlotPlayerRoute(NewGameLocation loc)
    {
        //Behaves slightly different depening on which type of location this is;
        //An address will point to the main door just outside
        //The street will just trigger when the player arrives there
        if(loc != null)
        {
            if(loc.thisAsAddress != null)
            {
                PlotPlayerRoute(loc.thisAsAddress);
            }
            else if(loc.thisAsStreet != null)
            {
                PlotPlayerRoute(loc.thisAsStreet);
            }
        }
    }

    public void PlotPlayerRoute(NewAddress loc)
    {
        if (loc != null)
        {
            Game.Log("Interface: Plotting route to address: " + loc.name);

            //Get a coordinate within this...
            NewNode outsideDoor = null;

            //Attempt to find door access
            NewNode.NodeAccess access = loc.entrances.Find(item => item.accessType == NewNode.NodeAccess.AccessType.door && (item.fromNode.gameLocation != loc || item.toNode.gameLocation != loc));

            if(access != null)
            {
                if(loc.IsPublicallyOpen(true) || loc.isLobby)
                {
                    if (access.toNode.gameLocation == loc) outsideDoor = access.toNode;
                    else outsideDoor = access.fromNode;
                }
                else
                {
                    if (access.toNode.gameLocation == loc) outsideDoor = access.fromNode;
                    else outsideDoor = access.toNode;
                }
            }
            else
            {
                access = loc.entrances.Find(item => item.accessType == NewNode.NodeAccess.AccessType.openDoorway && !item.employeeDoor);

                if (access != null)
                {
                    if (access.toNode.gameLocation == loc) outsideDoor = access.fromNode;
                    else outsideDoor = access.toNode;
                }
                else
                {
                    access = loc.entrances.Find(item => item.walkingAccess);

                    if (access != null)
                    {
                        if (access.toNode.gameLocation == loc) outsideDoor = access.fromNode;
                        else outsideDoor = access.toNode;
                    }
                    else
                    {
                        if(loc.entrances.Count > 0)
                        {
                            access = loc.entrances[0];

                            if (access.toNode.gameLocation == loc) outsideDoor = access.fromNode;
                            else outsideDoor = access.toNode;
                        }
                    }
                }
            }

            if (outsideDoor != null) PlotPlayerRoute(outsideDoor, true, loc);
            else Game.Log("Interface: Unable to find outside door of " + loc.name);
        }
    }

    //Plot route to street; use outside access and non specific node
    public void PlotPlayerRoute(StreetController loc)
    {
        if (loc != null)
        {
            //Get a coordinate within this...
            NewNode outsideDoor = null;

            //Attempt to find foor access
            NewNode.NodeAccess access = loc.entrances.Find(item => item.accessType == NewNode.NodeAccess.AccessType.door && (item.fromNode.building != loc.building || item.toNode.building != loc.building));

            if (access != null)
            {
                if (access.fromNode.gameLocation == loc) outsideDoor = access.fromNode;
                else outsideDoor = access.toNode;
            }
            else
            {
                access = loc.entrances.Find(item => item.accessType == NewNode.NodeAccess.AccessType.openDoorway && !item.employeeDoor);

                if (access != null)
                {
                    if (access.fromNode.gameLocation == loc) outsideDoor = access.fromNode;
                    else outsideDoor = access.toNode;
                }
                else
                {
                    access = loc.entrances.Find(item => item.walkingAccess);

                    if (access != null)
                    {
                        if (access.fromNode.gameLocation == loc) outsideDoor = access.fromNode;
                        else outsideDoor = access.toNode;
                    }
                    else
                    {
                        if (loc.entrances.Count > 0)
                        {
                            access = loc.entrances[0];

                            if (access.fromNode.gameLocation == loc) outsideDoor = access.fromNode;
                            else outsideDoor = access.toNode;
                        }
                    }
                }
            }

            if (outsideDoor != null) PlotPlayerRoute(outsideDoor, false);
            else Game.Log("Unable to find outside door of " + loc.name);
        }
    }

    public void PlotPlayerRoute(NewNode loc, bool nodeSpecific, NewGameLocation destinationTextOverride = null)
    {
        if (loc != null)
        {
            if (playerRoute != null)
            {
                if(playerRoute.end != loc)
                {
                    playerRoute.Remove();
                }
                else
                {
                    playerRoute.UpdatePathData();
                    playerRoute.UpdateDrawnRoute();
                    return;
                }
            }

            SetFloorLayer((int)loc.nodeCoord.z); //Set floor

            if (Game.Instance.routeTeleport)
            {
                Player.Instance.Teleport(loc, null);
            }
            else
            {
                //Image img = baseBackgroundImages[new Vector2(insideDoor.nodeCoord.x, insideDoor.nodeCoord.y)];
                //MapAddressButtonController button = buttons.Find(item => item.gameLocation == loc);

                Game.Log("Interface: Adding new player route: " + Player.Instance.currentNode.position + " -> " + loc.position + " (node specific: " + nodeSpecific + ")");
                MapRoute newRoute = new MapRoute(Player.Instance.currentNode, loc, Player.Instance, nodeSpecific, destinationTextOverride);
            }

            //Add cancel to context menu
            if(mapContextMenu.disabledItems.Contains("CancelRoute"))
            {
                mapContextMenu.disabledItems.Remove("CancelRoute");
            }

            InterfaceControls.Instance.plottedRouteText.gameObject.SetActive(true);

            string plotRouteText = loc.gameLocation.name;

            //Add floor text
            if(loc.gameLocation.building != null && loc.gameLocation.floor != null && loc.gameLocation.floor.floor != 0)
            {
                if (loc.gameLocation.floor.floor < 0)
                {
                    plotRouteText = " (" + Strings.Get("names.rooms", "basement", Strings.Casing.firstLetterCaptial) + " " + loc.gameLocation.floor.floor + ")";
                }
                else if (loc.gameLocation.floor.floor > 0)
                {
                    plotRouteText +=  " (" + Toolbox.Instance.GetNumbericalStringReference(Mathf.Abs(loc.gameLocation.floor.floor)) + " " + Strings.Get("evidence.generic", "floor") + ")";
                }
            }

            InterfaceControls.Instance.plottedRouteText.text = plotRouteText;
        }
    }

    //Remove a route
    public void RemovePlayerRoute()
    {
        if (playerRoute != null)
        {
            playerRoute.Remove();
        }
    }

    //Set transparency of object following citizen route
    private void SetTimelineCitizenTransparency(int citizenFloor, RectTransform objectRect)
    {
        //Set its transparency
        //Game.Log(load);
        //Game.Log(citizenFloor);

        int floorDiff = Mathf.Abs(load - citizenFloor);

        if (floorDiff > 0)
        {
            float transProgress = 1f - (Mathf.Clamp(floorDiff, 0, 11) / 10f);
            float alpha = Mathf.Lerp(0.05f, 0.2f, transProgress);
            objectRect.gameObject.GetComponent<CanvasRenderer>().SetAlpha(alpha);
        }
        else
        {
            objectRect.gameObject.GetComponent<CanvasRenderer>().SetAlpha(1f);
        }
    }

    //Get point along total path using a float, as well as the exact world point. Also return the last point index (of pathdata list) and the distance since from the last point
    Vector3 FindWorldPoint(PathFinder.PathData pathData, float percentAlong, out int lastPointIndex, out float distanceSinceLastPoint, out int nextPointIndex)
    {
        lastPointIndex = 0;
        distanceSinceLastPoint = 0f;
        nextPointIndex = 0;

        return Vector3.zero;

        //if (pathData.accessList.Count < 1)
        //{
        //    return Vector3.zero; // if the list is empty
        //}
        //else if (pathData.accessList.Count < 2)
        //{
        //    return pathData.accessList[0].transform.position; //if there is only one point in the list
        //}

        ////Get the real world distance along the line to look for
        //float dist = pathData.pathDistance * Mathf.Clamp(percentAlong, 0, 1);
        //Vector3 pos = pathData.accessList[0].transform.position;

        ////Loop through all but the last coord
        //for (int i = 0; i < pathData.accessList.Count - 1; i++)
        //{
        //    lastPointIndex = i;
        //    nextPointIndex = i + 1;
        //    distanceSinceLastPoint = 0f;

        //    //Get distance between this and next
        //    Vector3 v0 = pathData.accessList[lastPointIndex].transform.position;
        //    Vector3 v1 = pathData.accessList[nextPointIndex].transform.position;
        //    float thisDist = (v1 - v0).magnitude;

        //    if (thisDist < dist)
        //    {
        //        dist -= thisDist; //if the remaining distance is more than the distance between these vectors then minus the current distance from the remaining and go to the next vector
        //        continue;
        //    }

        //    distanceSinceLastPoint = dist;
        //    return Vector3.Lerp(v0, v1, dist / thisDist); //if the distance between these vectors is more or equal to the remaining distance then find how far along the gap it is and return
        //}

        //distanceSinceLastPoint = 0f;
        //return pathData.accessList[pathData.accessList.Count - 1].transform.position;
    }

    //Display the directional arrow
    public void DisplayDirectionArrow(bool val)
    {
        displayDirectionArrow = val;

        //Also use this option that allows the player to toggle it
        if(Game.Instance.enableDirectionalArrow)
        {
            directionalArrowContainer.SetActive(displayDirectionArrow);
        }
        else directionalArrowContainer.SetActive(false);
    }

    //Reset this
    public void ResetThis()
    {
        load = -1;
        displayPlayerCharacter = true;
        displayFirstPerson = false; //Temporary display

        while(pointers.Count > 0)
        {
            Destroy(pointers[0].pointerObject.gameObject);
            Destroy(pointers[0].followRect.gameObject);
            pointers.RemoveAt(0);
        }

        pointers = new List<PointerData>();

        //Tracked objects are objects that move and display on the map
        foreach(KeyValuePair<Transform, List<RectTransform>> pair in dynamicTrackedObjects)
        {
            while(pair.Value.Count > 0)
            {
                pair.Value.RemoveAt(0);
            }
        }

        foreach (KeyValuePair<Transform, List<RectTransform>> pair in staticTrackedObjects)
        {
            while (pair.Value.Count > 0)
            {
                pair.Value.RemoveAt(0);
            }
        }

        dynamicTrackedObjects = new Dictionary<Transform, List<RectTransform>>();
        staticTrackedObjects = new Dictionary<Transform, List<RectTransform>>();

        //Pins are all objects pinned, but not all are considered track (ie some are static)
        foreach (KeyValuePair<InfoWindow, MapPinButtonController> pair in pinnedObjects)
        {
            Destroy(pair.Value.gameObject);
        }

        pinnedObjects = new Dictionary<InfoWindow, MapPinButtonController>();
        invisiblePins = new List<InfoWindow>();
        forceFocusActive = false;
        forceFocusProgress = 0f;
        focusPos = Vector2.zero; //Used if there is no rect above

        if(playerRoute != null)
        {
            playerRoute.Remove();
        }

        foreach(KeyValuePair<int, MapLayer> pair in mapLayers)
        {
            Destroy(pair.Value.canvas.gameObject);
        }

        mapLayers = new Dictionary<int, MapLayer>();

        while(buttons.Count > 0)
        {
            Destroy(buttons[0]);
        }

        buttons = new List<MapAddressButtonController>();
    }

    //Toggle drawing mode
    public void ToggleDrawingMode()
    {
        drawingMode = !drawingMode;

        if(drawingMode)
        {
            toggleDrawingButton.background.color = InterfaceControls.Instance.selectionColour;

            if(load > -1)
            {
                //Enable drawing for the active layer
                mapLayers[load].drawingController.SetDrawingActive(true);

                //Disable cursor
                mapCursor.gameObject.SetActive(false);
            }
        }
        else
        {
            toggleDrawingButton.background.color = InterfaceControls.Instance.nonSelectionColour;

            //Set drawing inactive for everything
            foreach(KeyValuePair<int, MapLayer> pair in mapLayers)
            {
                pair.Value.drawingController.SetDrawingActive(false); 
            }

            //Disable cursor
            mapCursor.gameObject.SetActive(true);
        }
    }

    public void OnChangeDrawingColour()
    {
        //Set drawing colour for everything
        foreach (KeyValuePair<int, MapLayer> pair in mapLayers)
        {
            pair.Value.drawingController.SetBrushColour(colourButton.selectedColour);
        }
    }

    public void ToggleEraser()
    {
        eraseMode = !eraseMode;

        //Set eraser for everything
        foreach (KeyValuePair<int, MapLayer> pair in mapLayers)
        {
            pair.Value.drawingController.SetEraserMode(eraseMode);
        }

        if (eraseMode)
        {
            eraserButton.background.color = InterfaceControls.Instance.selectionColour;
        }
        else
        {
            eraserButton.background.color = InterfaceControls.Instance.nonSelectionColour;
        }
    }

    public void ClearDrawing()
    {
        if(load > -1)
        {
            mapLayers[load].drawingController.ResetDrawingTexture();
        }
    }

    public void OpenEvidence()
    {
        if(mapCursorNode != null && mapCursorNode.gameLocation != null)
        {
            InterfaceController.Instance.SpawnWindow(mapCursorNode.gameLocation.evidenceEntry);
        }
    }

    public void PlotRoute()
    {
        if (mapCursorNode != null && mapCursorNode.gameLocation != null)
        {
            PlotPlayerRoute(mapCursorNode, true);
        }
    }

    public void CancelRoute()
    {
        RemovePlayerRoute();
    }

    //Show debug data for node access on the map
    public void DebugAccess()
    {
        //Remove previous
        while(spawnedDebugComponents.Count > 0)
        {
            Destroy(spawnedDebugComponents[0]);
            spawnedDebugComponents.RemoveAt(0);
        }

        //Create lines with access
        if (mapCursorNode == null || mapCursorNode.accessToOtherNodes == null) return;

        foreach(KeyValuePair<NewNode, NewNode.NodeAccess> pair in mapCursorNode.accessToOtherNodes)
        {
            GameObject newObj = Instantiate(PrefabControls.Instance.debugAccess, MapController.Instance.mapLayers[(int)pair.Key.nodeCoord.z].baseContainer);
            spawnedDebugComponents.Add(newObj);

            TextMeshProUGUI tmp = newObj.GetComponentInChildren<TextMeshProUGUI>();
            tmp.text = pair.Value.accessType.ToString();

            //Position
            RectTransform debugRect = newObj.GetComponent<RectTransform>();
            debugRect.anchoredPosition = NodeCoordToMap(pair.Key.nodeCoord);

            debugRect.anchoredPosition = new Vector2(debugRect.anchoredPosition.x - MapController.Instance.nodePositionMultiplier * 1.5f, debugRect.anchoredPosition.y - MapController.Instance.nodePositionMultiplier * 1.5f); //I don't know why this is happening but lines are appearing to high on the map, workaround here <-

            Image i = newObj.GetComponent<Image>();

            if (pair.Value.walkingAccess)
            {
                i.color = Color.green;
            }
            else i.color = Color.red;
        }
    }
}
