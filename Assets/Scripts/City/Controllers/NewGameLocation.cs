﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class NewGameLocation : Controller
{
    [System.NonSerialized]
    public NewAddress thisAsAddress;
    [System.NonSerialized]
    public StreetController thisAsStreet;
    public string seed;

    [Header("Location")]
    public DistrictController district;
    public NewBuilding building; //Reference to the building (set in addresses only)
    public NewFloor floor; //Reference to the floor (set in addresses only)
    public int residenceNumber = 0; //Residence number relative to this floor (set in addresses only)
    public MapAddressButtonController mapButton;

    [Space(7)]
    public bool isLobby = false; //Game Only: True if this is a building lobby (set in addresses only)
    //public bool isMainLobby = false; //Game Only: True if this lobby features the main entrance
    public bool isOutside = false; //Is this location outside?
    public bool isCrimeScene = false; //Is this currently a crime scene?
    public float loggedAsCrimeScene; //The gametime at which this was last logged as a crime scene
    public AddressPreset.AccessType access = AddressPreset.AccessType.allPublic;

    [Header("Contents")]
    public NewRoom nullRoom; //Null space room
    public List<NewRoom> rooms = new List<NewRoom>(); //List of rooms
    public List<NewNode> nodes = new List<NewNode>(); //List of nodes
    public List<Actor> currentOccupants = new List<Actor>(); //Updated list of occupants
    public DesignStylePreset designStyle; //Design style chosen for the address
    public List<ArtPreset> artPieces = new List<ArtPreset>(); //Reference of art pieces chosen for this room (needed so they don't repeat)
    public bool placedKey = false; //Used on generation for placing keys to addresses
    public List<Interactable> securityCameras = new List<Interactable>();
    public List<Interactable> stacks = new List<Interactable>(); //Used for spawning objects on objects (eg books in book stacks)
    public List<Telephone> telephones = new List<Telephone>();

    public Dictionary<FurnitureClass.OwnershipClass, Dictionary<FurnitureLocation, List<Human>>> furnitureBelongsTo = new Dictionary<FurnitureClass.OwnershipClass, Dictionary<FurnitureLocation, List<Human>>>(); //Dictates which spawn positions belong to which citizens, if any

    [Header("AI Navigation")]
    public List<NewNode.NodeAccess> entrances = new List<NewNode.NodeAccess>(); //Entrances/exits to this
    [System.NonSerialized]
    public NewNode.NodeAccess streetAccess; //If this features access to street, reference it here
    public NewNode anchorNode; //Used in operations where a citizen simply needs to move to this address: This is the inside node to the entrance 
    public Dictionary<AIActionPreset, List<Interactable>> actionReference = new Dictionary<AIActionPreset, List<Interactable>>(); //A reference to objects in this room and their actions.
    public Dictionary<AIActionPreset, List<Interactable>> nearestPublicActionReference = new Dictionary<AIActionPreset, List<Interactable>>(); //A reference to nearest publicly available actions not at this address 
    public Dictionary<Actor, TrespassEscalation> escalation = new Dictionary<Actor, TrespassEscalation>();

    [System.Serializable]
    public class TrespassEscalation
    {
        public int actor;
        public bool isPlayer = false;
        public float lastEscalationCheck;
        public float timeEscalation;
    }

    [Header("Evidence")]
    [System.NonSerialized]
    public EvidenceLocation evidenceEntry; //Reference to evidence entry for this

    //Used in object placement
    public class ObjectPlacement
    {
        public FurniturePreset.SubObject location;
        public FurnitureLocation furnParent;
        public Interactable existing;
        public Interactable subSpawn = null;
    }

    //Fill this list of objects to place, then run through and place them on city generation
    public List<ObjectPlace> objectsToPlace = new List<ObjectPlace>();
    public bool objectPoolPlaced = false;

    //Class for new object placement method
    public class ObjectPlace
    {
        public InteractablePreset interactable;
        public Human belongsTo;
        public Human writer;
        public Human receiver;
        public List<Interactable.Passed> passedVars = null;
        public int security = 0;
        public InteractablePreset.OwnedPlacementRule ownership = InteractablePreset.OwnedPlacementRule.nonOwnedOnly;
        public int priority = 0;
        public object passedObject = null;
        public HashSet<NewRoom> dontPlaceInRooms = null;
    }

    //Class used in new object placement
    public class Placement
    {
        public NewRoom room;
        public FurnitureLocation furniture;
        public SubObjectClassPreset placementClass;
        public FurniturePreset.SubObject subObject;
        public Interactable subSpawn = null;
        public float rank = 0f;
    }

    //Event for location
    //public delegate void CrimeSceneStatusChange();
    //public event CrimeSceneStatusChange OnCrimeSceneStatusChange;

    //Base class of street and address: An actor should always be at a game location
    public void CommonSetup(bool newIsOutside, DistrictController newDistrict, DesignStylePreset newDefaultStyle)
    {
        district = newDistrict;
        isOutside = newIsOutside;

        thisAsAddress = this as NewAddress;
        thisAsStreet = this as StreetController;

        if(newDefaultStyle != null) SetDesignStyle(newDefaultStyle);

        GameObject newRoomObj = Instantiate(PrefabControls.Instance.room, this.transform);
        nullRoom = newRoomObj.GetComponent<NewRoom>();
        nullRoom.SetupAll(this, CityControls.Instance.nullDefaultRoom);
        nullRoom.isBaseNullRoom = true;

        seed = Toolbox.Instance.SeedRand(0, 9999999).ToString();

        if(!CityData.Instance.gameLocationDirectory.Contains(this))
        {
            CityData.Instance.gameLocationDirectory.Add(this);
        }
    }

    //Add a node to this address
    public void AddNewNode(NewNode newNode)
    {
        if (!nodes.Contains(newNode))
        {
            //Remove existing
            if (newNode.gameLocation != null) newNode.gameLocation.RemoveNode(newNode);

            nodes.Add(newNode);
            //newNode.transform.SetParent(this.transform, true);

            //Set location info
            newNode.gameLocation = this;

            //Add node to the default room
            nullRoom.AddNewNode(newNode);

            //Only set if address
            if (thisAsAddress != null)
            {
                newNode.floor = thisAsAddress.floor;
                newNode.building = thisAsAddress.building;

                //Refresh wall data
                if (SessionData.Instance.isFloorEdit)
                {
                    GenerationController.Instance.UpdateGeometryFloor(thisAsAddress.floor, "NewGameLocation");

                    //If in address display mode, chance the colour of the material
                    if (FloorEditController.Instance.displayMode == FloorEditController.EditorDisplayMode.displayAddressDesignation)
                    {
                        if (newNode.spawnedFloor != null)
                        {
                            newNode.spawnedFloor.GetComponent<MeshRenderer>().material = FloorEditController.Instance.adddressDesignationMaterial;
                            newNode.spawnedFloor.GetComponent<MeshRenderer>().material.SetColor("_UnlitColor", thisAsAddress.editorColour); //set the material equal to the clone
                        }
                    }
                }
            }
        }
    }

    //Remove a node from this address
    public void RemoveNode(NewNode newNode)
    {
        nodes.Remove(newNode);

        //Set location info
        newNode.gameLocation = null;
        newNode.floor = null;
        newNode.building = null;
    }

    //Add a room to this address
    public void AddNewRoom(NewRoom newRoom)
    {
        if (!rooms.Contains(newRoom))
        {
            //Remove existing
            if (newRoom.gameLocation != null) newRoom.gameLocation.RemoveRoom(newRoom);

            rooms.Add(newRoom);

            //Set location info
            newRoom.gameLocation = this;

            //Set if address
            if(thisAsAddress != null)
            {
                newRoom.floor = thisAsAddress.floor;
                newRoom.building = thisAsAddress.building;
            }

            //Tree update
            foreach (NewNode node in newRoom.nodes)
            {
                node.gameLocation = this;
            }
        }
    }

    //Remove a room from this address
    public void RemoveRoom(NewRoom newRoom)
    {
        if (rooms.Contains(newRoom))
        {
            rooms.Remove(newRoom);

            //Set location info
            newRoom.gameLocation = null;
            newRoom.floor = null;
            newRoom.building = null;
            Destroy(newRoom.gameObject);
        }
    }

    public virtual void AddOccupant(Actor newOcc)
    {
        currentOccupants.Add(newOcc);

        //Add to company staff
        if(thisAsAddress != null)
        {
            if(thisAsAddress.company != null && !thisAsAddress.company.preset.isSelfEmployed)
            {
                thisAsAddress.company.OnAddressCitizenEnter(newOcc as Citizen);
            }
        }
    }

    public virtual void RemoveOccupant(Actor remOcc)
    {
        currentOccupants.Remove(remOcc);

        //Add to company staff
        if (thisAsAddress != null)
        {
            if (thisAsAddress.company != null)
            {
                thisAsAddress.company.OnAddressCitizenExit(remOcc as Citizen);
            }
        }
    }

    public NewNode.NodeAccess GetMainEntrance()
    {
        NewNode.NodeAccess best = null;

        foreach(NewNode.NodeAccess acc in entrances)
        {
            if (!acc.walkingAccess) continue;
            if (acc.accessType != NewNode.NodeAccess.AccessType.door && acc.accessType != NewNode.NodeAccess.AccessType.openDoorway) continue;
            if (acc.fromNode.gameLocation == acc.toNode.gameLocation) continue;

            if (acc.toNode.gameLocation.thisAsStreet != null || acc.fromNode.gameLocation.thisAsStreet != null) return acc;

            best = acc;
        }

        return best;
    }

    public void SetDesignStyle(DesignStylePreset newStyle)
    {
        designStyle = newStyle;

        //Pick a wood colour choice for this address
        if(thisAsAddress != null && (SessionData.Instance.isFloorEdit || CityConstructor.Instance.generateNew || SessionData.Instance.play))
        {
            thisAsAddress.wood = InteriorControls.Instance.woods[Toolbox.Instance.GetPsuedoRandomNumber(0, InteriorControls.Instance.woods.Count, thisAsAddress.id.ToString())];

            //Generate colour schemes for rooms: First do rooms that have their own style so others may copy if needed.
            foreach (NewRoom room in rooms)
            {
                if (room.preset != null && room.preset.decorSetting == RoomConfiguration.DecorSetting.ownStyle)
                {
                    //Generate colours
                    room.UpdateColourSchemeAndMaterials();
                }
            }

            foreach (NewRoom room in rooms)
            {
                if (room.preset != null && room.preset.decorSetting != RoomConfiguration.DecorSetting.ownStyle)
                {
                    //Generate colours
                    room.UpdateColourSchemeAndMaterials();
                }
            }
        }
    }

    public void AddEntrance(NewNode fromNode, NewNode toNode, bool forceAccessType = false, NewNode.NodeAccess.AccessType forcedAccessType = NewNode.NodeAccess.AccessType.adjacent, bool forceWalkable = false)
    {
        if (!entrances.Exists(item => item.fromNode == fromNode && item.toNode == toNode))
        {
            //The doorway is always the parent wall: This only exists if there's a wall inbetween obvs.
            NewDoor foundDoor = null;
            NewWall foundWall = fromNode.walls.Find(item => item.otherWall != null && item.otherWall.node == toNode && item.parentWall.preset.sectionClass != DoorPairPreset.WallSectionClass.wall && item.parentWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventLower && item.parentWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventUpper && item.preset.sectionClass != DoorPairPreset.WallSectionClass.ventTop);

            if (foundWall != null)
            {
                foundDoor = foundWall.parentWall.door;
                foundWall = foundWall.parentWall;
            }

            NewNode.NodeAccess entrance = new NewNode.NodeAccess(fromNode, toNode, foundWall, foundDoor, forceAccessType, forcedAccessType, forceWalkable);
            entrances.Add(entrance);

            //I'm not sure why this is happening, but entrances are being added to the GL without being added to their respective rooms. Attempts to make sure this is working through this safety event...
            if(!fromNode.room.entrances.Exists(item => item.fromNode == fromNode && item.toNode == toNode))
            {
                fromNode.room.AddEntrance(fromNode, toNode);
            }

            //Add street access reference
            if(entrance.fromNode.gameLocation.thisAsStreet != null || entrance.toNode.gameLocation.thisAsStreet != null)
            {
                streetAccess = entrance;
            }

            //Is this a street? If so add to the street access dictionary
            //Only add if the other location is not a street...
            if(thisAsStreet != null)
            {
                if(toNode.gameLocation.thisAsAddress != null)
                {
                    if (!PathFinder.Instance.streetEntrances.Exists(item => item.fromNode == fromNode && item.toNode == toNode))
                    {
                        PathFinder.Instance.streetEntrances.Add(entrance);
                    }
                }
            }
        }

        //Assign achor node
        if(anchorNode == null)
        {
            if (fromNode.gameLocation == this) anchorNode = fromNode;
            else if (toNode.gameLocation == this) anchorNode = toNode;
        }
    }

    public void RemoveEntrance(NewNode fromNode, NewNode toNode)
    {
        if(thisAsStreet)
        {
            if (toNode.gameLocation.thisAsAddress != null)
            {
                NewNode.NodeAccess streetEntrance = PathFinder.Instance.streetEntrances.Find(item => item.fromNode == fromNode && item.toNode == toNode);
                PathFinder.Instance.streetEntrances.Remove(streetEntrance);
            }
        }

        NewNode.NodeAccess entrance = entrances.Find(item => item.fromNode == fromNode && item.toNode == toNode);
        entrances.Remove(entrance);
    }

    public Interactable PlaceObject(InteractablePreset interactable, Human belongsTo, Human writer, Human reciever, out FurnitureLocation pickedFurn, bool passVariable = false, Interactable.PassedVarType passedVarType= Interactable.PassedVarType.jobID, int passedValue = -1, bool forceSecuritySettings = false, int forcedSecurity = 0, InteractablePreset.OwnedPlacementRule forcedOwnership = InteractablePreset.OwnedPlacementRule.nonOwnedOnly, int forcedPriority = 0, RetailItemPreset retailItem = null, bool printDebug = false, HashSet < NewRoom> dontPlaceInRooms = null, string loadGUID = null, NewNode placeClosestTo = null, string ddsOverride = "", bool ignoreLimits = false)
    {
        List<Interactable.Passed> p = null;
        pickedFurn = null;

        if(passVariable)
        {
            p = new List<Interactable.Passed>();
            p.Add(new Interactable.Passed(passedVarType, passedValue));
        }

        return PlaceObject(interactable, belongsTo, writer, reciever, out pickedFurn, p, forceSecuritySettings, forcedSecurity, forcedOwnership, forcedPriority, retailItem, printDebug, dontPlaceInRooms, loadGUID, placeClosestTo, ddsOverride, ignoreLimits);
    }

    //Place an object in this address: Use importance to override existing object
    public Interactable PlaceObject(InteractablePreset interactable, Human belongsTo, Human writer, Human receiver, out FurnitureLocation pickedFurn, List<Interactable.Passed> passedVars = null, bool forceSecuritySettings = false, int forcedSecurity = 0, InteractablePreset.OwnedPlacementRule forcedOwnership = InteractablePreset.OwnedPlacementRule.nonOwnedOnly, int forcedPriority = 0, object passedObject = null, bool printDebug = false, HashSet<NewRoom> dontPlaceInRooms = null, string loadGUID = null, NewNode placeClosestTo = null, string ddsOverride = "", bool ignoreLimits = false)
    {
        string randomKey = seed;

        //Add dds override
        if(ddsOverride.Length > 0)
        {
            if (passedVars == null) passedVars = new List<Interactable.Passed>();
            passedVars.Add(new Interactable.Passed(Interactable.PassedVarType.ddsOverride, 0, ddsOverride));
        }

        pickedFurn = null;

        //If not using forced settings then grab them from the preset
        if(!forceSecuritySettings)
        {
            forcedSecurity = interactable.securityLevel;
            forcedOwnership = interactable.ownedRule;
            forcedPriority = Mathf.Max(interactable.perGameLocationObjectPriority, interactable.perOwnerObjectPriority); //Choose max
        }

        if (!objectPoolPlaced && CityConstructor.Instance.generateNew)
        {
            Game.Log("Object: Object pool has not yet been placed, adding to that instead...");

            AddToPlacementPool(interactable, belongsTo, writer, receiver, passedVars, forcedSecurity, forcedOwnership, forcedPriority, passedObject, dontPlaceInRooms);
            return null;
        }

        if (printDebug) Game.Log("Object: Placing item " + interactable.name + " with security: " + forcedSecurity.ToString() + ", ownership: " + forcedOwnership.ToString() + " and priority: " + forcedPriority.ToString() + " belonging to " + belongsTo.name);

        bool warmItem = false;

        //Figure out if this is still warm...
        RetailItemPreset retailItem = passedObject as RetailItemPreset;

        if(retailItem != null)
        {
            if(retailItem.isHot)
            {
                float purchaseTime = passedVars.Find(item => item.varType == Interactable.PassedVarType.time).value;

                if (SessionData.Instance.gameTime - purchaseTime <= GameplayControls.Instance.foodHotTime)
                {
                    warmItem = true;
                }
            }
        }

        //Limit per address...
        if(!ignoreLimits && (interactable.limitPerAddress || interactable.limitInCommercial || interactable.limitInResidential))
        {
            int itemCount = 0;
            int limit = 99999;

            if (interactable.limitPerAddress && thisAsAddress != null)
            {
                limit = Mathf.Min(interactable.perAddressLimit, limit);
            }

            if(interactable.limitInCommercial && thisAsAddress != null && thisAsAddress.company != null)
            {
                limit = Mathf.Min(interactable.perCommercialLimit, limit);
            }

            if (interactable.limitInResidential && thisAsAddress != null && thisAsAddress.residence != null)
            {
                limit = Mathf.Min(interactable.perResidentialLimit, limit);
            }

            foreach (NewRoom r in rooms)
            {
                foreach (FurnitureLocation f in r.individualFurniture)
                {
                    itemCount += f.spawnedInteractables.FindAll(item => item.preset == interactable).Count;

                    if (itemCount >= limit)
                    {
                        break;
                    }
                }

                if (itemCount >= limit)
                {
                    break;
                }
            }

            if (itemCount >= limit)
            {
                if (printDebug) Game.Log("Object: " + name + " has maximum number of " + interactable.name + " objects according to interactable limit setting.");
                return null;
            }
        }

        //Attempt to place in relevent folder/collection...
        if (interactable.attemptToStoreInFolder != null && Toolbox.Instance.RandContained(0f, 1f, randomKey, out randomKey) <= interactable.folderPlacementChance)
        {
            bool placed = false;
            Interactable sp = null;

            foreach (NewRoom r in rooms)
            {
                foreach (FurnitureLocation f in r.individualFurniture)
                {
                    if (interactable.folderOwnershipMustMatch)
                    {
                        sp = f.spawnedInteractables.Find(item => item.evidence != null && item.evidence.preset == interactable.attemptToStoreInFolder && item.belongsTo == belongsTo);
                    }
                    else
                    {
                        sp = f.spawnedInteractables.Find(item => item.evidence != null && item.evidence.preset == interactable.attemptToStoreInFolder);
                    }

                    if (sp != null)
                    {
                        EvidenceMultiPage mp = sp.evidence as EvidenceMultiPage;

                        if (mp != null)
                        {
                            MetaObject newMeta = new MetaObject(interactable, belongsTo, writer, receiver, passedVars);
                            mp.AddContainedMetaObjectToNewPage(newMeta);
                        }

                        placed = true;
                        break;
                    }
                }

                if (placed) break;
            }

            if (!placed)
            {
                Game.Log("CityGen: Could not find " + interactable.attemptToStoreInFolder.name + " for placing " + interactable.name + " at " + name);
            }
            else
            {
                return sp;
            }

            if (!placed && interactable.dontPlaceIfNoFolder)
            {
                return null;
            }
        }

        //Pick a random location
        ObjectPlacement pickedPos = GetBestSpawnLocation(interactable, warmItem, belongsTo, writer, receiver, out pickedFurn, passedVars, forceSecuritySettings, forcedSecurity, forcedOwnership, forcedPriority, passedObject, printDebug, dontPlaceInRooms, loadGUID, placeClosestTo, ddsOverride, ignoreLimits);

        if (pickedPos == null)
        {
            if (printDebug) Game.Log("Object: No furniture allows " + interactable.name + " to spawn in " + this.name);
            return null;
        }

        if (printDebug) Game.Log("Object: Chose placement on " + pickedPos.furnParent.furniture.name);

        if(pickedPos.existing != null)
        {
            pickedPos.existing.SafeDelete(true);
        }

        Interactable newObj = null;

        //Pick a placement upon furniture...
        if (pickedPos.subSpawn == null)
        {
            //Furniture interactables must be created first...
            if (!pickedPos.furnParent.createdInteractables)
            {
                pickedPos.furnParent.CreateInteractables();
            }

            if (printDebug) Game.Log("Object: Chose placement on " + pickedPos.furnParent.furniture.name);
            pickedFurn = pickedPos.furnParent;
            newObj = InteractableCreator.Instance.CreateFurnitureSpawnedInteractableThreadSafe(interactable, pickedPos.furnParent.anchorNode.room, pickedPos.furnParent, pickedPos.location, belongsTo, writer, receiver, passedVars, null, passedObject);
        }
        //Pick a placement on an existing object
        else
        {
            //Furniture interactables must be created first...
            if (!pickedPos.subSpawn.furnitureParent.createdInteractables)
            {
                pickedPos.subSpawn.furnitureParent.CreateInteractables();
            }

            if (printDebug) Game.Log("Object: Chose placement on subspawned " + pickedPos.subSpawn.preset.name);
            int ssIndex = Toolbox.Instance.RandContained(0, pickedPos.subSpawn.ssp.Count, randomKey, out randomKey);
            InteractablePreset.SubSpawnSlot ss = pickedPos.subSpawn.ssp[ssIndex];

            //Spawn in a small radius around this
            Vector2 c = (UnityEngine.Random.insideUnitCircle * 0.2f);
            Vector3 r = pickedPos.subSpawn.wPos + ss.localPos;
            r += new Vector3(c.x, 0, c.y);

            Vector3 e = pickedPos.subSpawn.wEuler + ss.localEuler;

            newObj = InteractableCreator.Instance.CreateWorldInteractable(interactable, belongsTo, writer, receiver, r, e, passedVars, passedObject );

            //Remove slot from interactable
            pickedPos.subSpawn.ssp.RemoveAt(ssIndex);
        }

        //Set priority
        if(newObj != null) newObj.opp = forcedPriority;

        return newObj;
    }

    //Return the best location for this
    public ObjectPlacement GetBestSpawnLocation(InteractablePreset interactable, bool warmItem, Human belongsTo, Human writer, Human receiver, out FurnitureLocation pickedFurn, List<Interactable.Passed> passedVars = null, bool forceSecuritySettings = false, int forcedSecurity = 0, InteractablePreset.OwnedPlacementRule forcedOwnership = InteractablePreset.OwnedPlacementRule.nonOwnedOnly, int forcedPriority = 0, object passedObject = null, bool printDebug = false, HashSet<NewRoom> dontPlaceInRooms = null, string loadGUID = null, NewNode placeClosestTo = null, string ddsOverride = "", bool ignoreLimits = false, bool usePutDownPosition = false)
    {
        pickedFurn = null;

        string randomKey = seed + interactable.presetName;

        //Rank system
        ObjectPlacement ret = null;
        float score = -99999f;

        foreach (NewRoom room in rooms)
        {
            if (room.preset == null) continue; //Skip if room has no preset
            if (!ignoreLimits && dontPlaceInRooms != null && dontPlaceInRooms.Contains(room)) continue; //Don't place in this room
            if (!ignoreLimits && !room.preset.allowPersonalAffects && belongsTo != null && forcedOwnership == InteractablePreset.OwnedPlacementRule.ownedOnly) continue;

            if (interactable.limitToCertainRooms && !ignoreLimits)
            {
                if (!interactable.onlyInRooms.Contains(room.preset))
                {
                    if (printDebug) Game.Log("Object: Interactable banned from rooms of type " + room.preset.name + "...");
                    continue; //Banned from this room
                }
            }
            else
            {
                if (interactable.banFromRooms.Contains(room.preset) && !ignoreLimits)
                {
                    if (printDebug) Game.Log("Object: Interactable banned from rooms of type " + room.preset.name + "...");
                    continue; //Banned from this room
                }
            }

            //Limit to x per room
            if (!ignoreLimits && interactable.limitPerRoom)
            {
                int itemCount = 0;

                foreach (FurnitureLocation f in room.individualFurniture)
                {
                    itemCount += f.spawnedInteractables.FindAll(item => item.preset == interactable && item.furnitureParent == f).Count;
                    if (itemCount >= interactable.perRoomLimit) break;
                }

                if (itemCount >= interactable.perRoomLimit)
                {
                    if (printDebug) Game.Log("Object: Limit per room hit in " + room.preset.name + "...");
                    continue;

                }
            }

            if (printDebug) Game.Log("Object: Checking for placement in " + room.preset.name + "...");

            foreach (FurnitureClusterLocation furn in room.furniture)
            {
                foreach (FurnitureLocation obj in furn.clusterList)
                {
                    if (obj.furniture == null)
                    {
                        Game.Log("Misc Error: Object " + furn.cluster.name + " index " + furn.clusterList.IndexOf(obj) + " doesn't have a furniture preset associated!");
                        continue;
                    }

                    if (printDebug) Game.Log("Object: Checking for placement on furniture " + obj.furniture.name);

                    //Limit to x per object
                    if (!ignoreLimits && interactable.limitPerObject)
                    {
                        if (obj.spawnedInteractables.FindAll(item => item.preset == interactable && item.furnitureParent == obj).Count >= interactable.perObjectLimit)
                        {
                            if (printDebug) Game.Log("Object: Reached limit for this type of object: " + interactable.perObjectLimit);
                            continue;
                        }
                    }

                    //List matching class
                    List<FurniturePreset.SubObject> valid = new List<FurniturePreset.SubObject>();

                    List<SubObjectClassPreset> classes = interactable.subObjectClasses;
                    if (usePutDownPosition) classes = interactable.putDownPositions;

                    foreach (SubObjectClassPreset placeClass in classes)
                    {
                        //Don't put warm items in the fridge
                        if (warmItem)
                        {
                            if (placeClass == InteriorControls.Instance.fridge)
                            {
                                if (printDebug) Game.Log("Object: Can't place warm item in fridge...");
                                continue;
                            }
                        }


                        List<FurniturePreset.SubObject> subObjs = obj.furniture.subObjects.FindAll(item => item.preset == placeClass);

                        if (subObjs.Count > 0)
                        {
                            //Put down positions; for now we need static locations as put down objects are parented to the world and not the furniture itself
                            if (usePutDownPosition)
                            {
                                valid.AddRange(subObjs);

                                //foreach (FurniturePreset.SubObject so in subObjs)
                                //{
                                //    if (so.parent != null && so.parent.Length > 0) continue;
                                //    valid.Add(so);
                                //}
                            }
                            else
                            {
                                if (printDebug) Game.Log("Object: Found matching classes on " + obj.furniture.name + " (" + subObjs.Count + " potential points matching class " + placeClass.name + ")");

                                valid.AddRange(subObjs);
                            }
                        }
                        else
                        {
                            if (printDebug) Game.Log("Object: Cannot find any points of class " + placeClass.name + " on furniture " + obj.furniture.name);
                        }
                    }

                    if(!usePutDownPosition)
                    {
                        foreach (SubObjectClassPreset placeClass in interactable.backupClasses)
                        {
                            List<FurniturePreset.SubObject> subObjs = obj.furniture.subObjects.FindAll(item => item.preset == placeClass);

                            if (subObjs.Count > 0)
                            {
                                if (printDebug) Game.Log("Object: Found matching backup classes on " + obj.furniture.name + " (" + subObjs.Count + " potential points matching class " + placeClass.name + ")");

                                //Insert into list
                                foreach (FurniturePreset.SubObject s in subObjs)
                                {
                                    //Detect overwrite
                                    Interactable existing = obj.spawnedInteractables.Find(item => item.subObject == s);

                                    float rank = -80 - Toolbox.Instance.RandContained(0, 10, randomKey, out randomKey);
                                    if (existing != null) rank -= 100;

                                    if(rank > score)
                                    {
                                        ret = new ObjectPlacement { location = s, furnParent = obj};
                                        score = rank;
                                    }
                                }
                            }
                            else
                            {
                                if (printDebug) Game.Log("Object: Cannot find any points of backup class " + placeClass.name + " on furniture " + obj.furniture.name);
                            }
                        }
                    }
                    else
                    {
                        foreach (SubObjectClassPreset placeClass in interactable.backupPutDownPositions)
                        {
                            List<FurniturePreset.SubObject> subObjs = obj.furniture.subObjects.FindAll(item => item.preset == placeClass);

                            if (subObjs.Count > 0)
                            {
                                if (printDebug) Game.Log("Object: Found matching backup classes on " + obj.furniture.name + " (" + subObjs.Count + " potential points matching class " + placeClass.name + ")");

                                //Insert into list
                                foreach (FurniturePreset.SubObject s in subObjs)
                                {
                                    //Detect overwrite
                                    Interactable existing = obj.spawnedInteractables.Find(item => item.subObject == s);

                                    float rank = -80 - Toolbox.Instance.RandContained(0, 10, randomKey, out randomKey);
                                    if (existing != null) rank -= 100;

                                    if (rank > score)
                                    {
                                        ret = new ObjectPlacement { location = s, furnParent = obj };
                                        score = rank;
                                    }
                                }
                            }
                            else
                            {
                                if (printDebug) Game.Log("Object: Cannot find any points of backup class " + placeClass.name + " on furniture " + obj.furniture.name);
                            }
                        }
                    }

                    if (valid.Count > 0)
                    {
                        //Validate and rank available class-matching spawn points...
                        foreach (FurniturePreset.SubObject loc in valid)
                        {
                            float rank = 10 + Toolbox.Instance.RandContained(-0.1f, 0.1f, randomKey, out randomKey);

                            //Security match (+10)
                            int thisPlacementSecurity = room.preset.securityLevel + loc.security;

                            if (thisPlacementSecurity == forcedSecurity)
                            {
                                rank += 10;
                            }
                            //It's better to put this item in a more secure place over a less secure place
                            else if (thisPlacementSecurity > forcedSecurity)
                            {
                                rank += 10 - (Mathf.Abs(thisPlacementSecurity - forcedSecurity) * 2);
                            }
                            else
                            {
                                rank += 10 - (Mathf.Abs(thisPlacementSecurity - forcedSecurity) * 4);
                            }

                            //Over write (-10)
                            Interactable existing = obj.spawnedInteractables.Find(item => item.subObject == loc);

                            if (existing != null)
                            {
                                if (existing.opp > forcedPriority)
                                {
                                    if (printDebug) Game.Log("Object: Existing item priority is higher (" + existing.name + " " + +existing.opp + ") than the item to be placed (" + forcedPriority + ") skipping this option...");

                                    //This can't happen
                                    continue;
                                }
                                else
                                {
                                    rank -= 100;
                                }
                            }

                            //Ownership match (+10)
                            if (forcedOwnership == InteractablePreset.OwnedPlacementRule.both)
                            {
                                rank += 10;
                            }
                            else if (forcedOwnership == InteractablePreset.OwnedPlacementRule.ownedOnly)
                            {
                                if (belongsTo == null)
                                {
                                    continue;
                                }
                                else
                                {
                                    //Belongs to nobody
                                    if (loc.belongsTo == FurniturePreset.SubObjectOwnership.nobody)
                                    {
                                        continue;
                                    }
                                    //Belongs to everybody on the furniture ownership map, so check for presence...
                                    else if (loc.belongsTo == FurniturePreset.SubObjectOwnership.everybody)
                                    {
                                        if (!obj.ownerMap.ContainsKey(new FurnitureLocation.OwnerKey(belongsTo)) && (belongsTo.home == null || !obj.ownerMap.ContainsKey(new FurnitureLocation.OwnerKey(belongsTo.home))))
                                        {
                                            continue;
                                        }
                                    }
                                    //Belongs to a specific person on the ownership map...
                                    else
                                    {
                                        int ownedBy = (int)loc.belongsTo - 2; //Reach index 0 by minusing 2 from this enum
                                        Human mustBelongTo = null;
                                        NewAddress mustBelongToAd = null;

                                        foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> pair in obj.ownerMap)
                                        {
                                            if(pair.Key.human != null)
                                            {
                                                if (pair.Value == ownedBy)
                                                {
                                                    mustBelongTo = pair.Key.human; //Found who this must belong to
                                                    break;
                                                }
                                            }
                                            else if(pair.Key.address != null)
                                            {
                                                if (pair.Value == ownedBy)
                                                {
                                                    mustBelongToAd = pair.Key.address; //Found who this must belong to
                                                    break;
                                                }
                                            }
                                        }

                                        if (mustBelongTo != null && mustBelongTo != belongsTo)
                                        {
                                            continue;
                                        }
                                        else if (mustBelongToAd != null && mustBelongToAd != belongsTo.home)
                                        {
                                            continue;
                                        }
                                        else if (mustBelongTo == null && mustBelongToAd == null) continue;
                                    }
                                }
                            }
                            else if (forcedOwnership == InteractablePreset.OwnedPlacementRule.prioritiseOwned)
                            {
                                if (belongsTo != null)
                                {
                                    //Belongs to everybody on the furniture ownership map, so check for presence...
                                    if (loc.belongsTo == FurniturePreset.SubObjectOwnership.everybody)
                                    {
                                        if (!obj.ownerMap.ContainsKey(new FurnitureLocation.OwnerKey(belongsTo)))
                                        {
                                            rank += 10;
                                        }
                                    }
                                    //Belongs to a specific person on the ownership map...
                                    else
                                    {
                                        int ownedBy = (int)loc.belongsTo - 2; //Reach index 0 by minusing 2 from this enum
                                        Human mustBelongTo = null;
                                        NewAddress mustBelongToAd = null;

                                        foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> pair in obj.ownerMap)
                                        {
                                            if(pair.Key.human != null)
                                            {
                                                if (pair.Value == ownedBy)
                                                {
                                                    mustBelongTo = pair.Key.human; //Found who this must belong to
                                                    break;
                                                }
                                            }
                                            else if (pair.Key.address != null)
                                            {
                                                if (pair.Value == ownedBy)
                                                {
                                                    mustBelongToAd = pair.Key.address; //Found who this must belong to
                                                    break;
                                                }
                                            }
                                        }

                                        if (mustBelongTo != null && mustBelongTo == belongsTo)
                                        {
                                            rank += 10;
                                        }
                                        else if (mustBelongToAd != null && mustBelongToAd == belongsTo.home)
                                        {
                                            rank += 10;
                                        }
                                    }
                                }
                            }
                            else if (forcedOwnership == InteractablePreset.OwnedPlacementRule.nonOwnedOnly)
                            {
                                if (loc.belongsTo != FurniturePreset.SubObjectOwnership.nobody)
                                {
                                    continue;
                                }
                            }
                            else if (forcedOwnership == InteractablePreset.OwnedPlacementRule.prioritiseNonOwned)
                            {
                                if (loc.belongsTo == FurniturePreset.SubObjectOwnership.nobody)
                                {
                                    rank += 10;
                                }
                            }

                            //Spawn closest to
                            if (placeClosestTo != null)
                            {
                                float dist = Vector3.Distance(obj.anchorNode.nodeCoord, placeClosestTo.nodeCoord);
                                rank += 10 - Mathf.RoundToInt(dist);
                            }

                            if (rank > score)
                            {
                                ret = new ObjectPlacement { location = loc, furnParent = obj};
                                score = rank;
                            }
                        }
                    }

                    //Search for sub spawning
                    if (interactable.useSubSpawning)
                    {
                        foreach (Interactable sp in obj.spawnedInteractables)
                        {
                            //Enough slots left...
                            if (sp.ssp != null && sp.ssp.Count > 0 && sp.preset.subSpawnClass != null)
                            {
                                //Matching class
                                if (interactable.subObjectClasses.Contains(sp.preset.subSpawnClass))
                                {
                                    //Insert into list
                                    float rank = 22 + Toolbox.Instance.RandContained(-0.1f, 0.1f, randomKey, out randomKey);

                                    if (rank > score)
                                    {
                                        ret = new ObjectPlacement { subSpawn = sp };
                                        score = rank;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return ret;
    }

    //Add something to the placement pool
    public void AddToPlacementPool(InteractablePreset interactable, Human belongsTo, Human writer, Human receiver, List<Interactable.Passed> passedVars = null, int security = 0, InteractablePreset.OwnedPlacementRule ownership = InteractablePreset.OwnedPlacementRule.nonOwnedOnly, int priority = 0, object passedObject = null, HashSet<NewRoom> dontPlaceInRooms = null)
    {
        if (interactable == null) return;

        if(thisAsAddress != null)
        {
            if (interactable.limitPerAddress || interactable.limitInCommercial || interactable.limitInResidential)
            {
                int limit = 99999;

                if (interactable.limitPerAddress)
                {
                    limit = Mathf.Min(interactable.perAddressLimit, limit);
                }

                if (interactable.limitInCommercial && thisAsAddress.company != null)
                {
                    limit = Mathf.Min(interactable.perCommercialLimit, limit);
                }

                if (interactable.limitInResidential && thisAsAddress.residence != null)
                {
                    limit = Mathf.Min(interactable.perResidentialLimit, limit);
                }

                if (objectsToPlace.FindAll(item => item.interactable == interactable).Count >= limit) return;
            }
        }

        ObjectPlace newPlacement = new ObjectPlace();
        newPlacement.interactable = interactable;
        newPlacement.belongsTo = belongsTo;
        newPlacement.writer = writer;
        newPlacement.receiver = receiver;
        newPlacement.passedVars = passedVars;
        newPlacement.security = security;
        newPlacement.ownership = ownership;
        newPlacement.priority = priority;
        newPlacement.passedObject = passedObject;
        newPlacement.dontPlaceInRooms = dontPlaceInRooms;

        objectsToPlace.Add(newPlacement);
    }

    //New method of object placement: Gather everything in a list first, then run through and place them...
    public void PlaceObjects()
    {
        string randomKey = seed;

        if (Game.Instance.devMode && Game.Instance.collectDebugData)
        {
            foreach (NewRoom r in rooms)
            {
                r.palcementKey1 = randomKey;
            }
        }

        //Add 'always place at gamelocation' interactables...
        foreach(InteractablePreset p in Toolbox.Instance.placeAtGameLocationInteractables)
        {
            if(p.autoPlacement == InteractablePreset.AutoPlacement.always ||
                (p.autoPlacement == InteractablePreset.AutoPlacement.onlyInCompany && thisAsAddress != null && thisAsAddress.company != null) ||
                (p.autoPlacement == InteractablePreset.AutoPlacement.onlyInHomes && thisAsAddress != null && thisAsAddress.residence != null) ||
                (p.autoPlacement == InteractablePreset.AutoPlacement.onlyOnStreet && thisAsStreet != null))
            {
                int frequency = Toolbox.Instance.RandContained(p.frequencyPerGamelocationMin, p.frequencyPerGameLocationMax + 1, randomKey, out randomKey);

                for (int i = 0; i < frequency; i++)
                {
                    AddToPlacementPool(p, null, null, null, null, p.securityLevel, p.ownedRule, p.perGameLocationObjectPriority, null, null);
                }
            }
        }

        if (Game.Instance.devMode && Game.Instance.collectDebugData)
        {
            foreach (NewRoom r in rooms)
            {
                r.palcementKey2 = randomKey;
            }
        }

        //Add 'owner's' interactables...
        if (thisAsAddress != null)
        {
            foreach (Human h in thisAsAddress.inhabitants)
            {
                foreach (InteractablePreset p in Toolbox.Instance.placePerOwnerInteractables)
                {
                    if (p.autoPlacement == InteractablePreset.AutoPlacement.always || (p.autoPlacement == InteractablePreset.AutoPlacement.onlyInCompany && thisAsAddress != null && thisAsAddress.company != null) || (p.autoPlacement == InteractablePreset.AutoPlacement.onlyInHomes && thisAsAddress != null && thisAsAddress.residence != null) || (p.autoPlacement == InteractablePreset.AutoPlacement.onlyOnStreet && thisAsStreet != null))
                    {
                        //Pass trait check...
                        //Check picking rules
                        bool passRules = true;
                        int freqModMin = 0;
                        int freqModMax = 0;

                        foreach (InteractablePreset.TraitPick rule in p.traitModifiers)
                        {
                            bool pass = false;

                            if (rule.rule == CharacterTrait.RuleType.ifAnyOfThese)
                            {
                                foreach (CharacterTrait searchTrait in rule.traitList)
                                {
                                    if (h.characterTraits.Exists(item => item.trait == searchTrait))
                                    {
                                        pass = true;
                                        break;
                                    }
                                }
                            }
                            else if (rule.rule == CharacterTrait.RuleType.ifAllOfThese)
                            {
                                pass = true;

                                foreach (CharacterTrait searchTrait in rule.traitList)
                                {
                                    if (!h.characterTraits.Exists(item => item.trait == searchTrait))
                                    {
                                        pass = false;
                                        break;
                                    }
                                }
                            }
                            else if (rule.rule == CharacterTrait.RuleType.ifNoneOfThese)
                            {
                                pass = true;

                                foreach (CharacterTrait searchTrait in rule.traitList)
                                {
                                    if (h.characterTraits.Exists(item => item.trait == searchTrait))
                                    {
                                        pass = false;
                                        break;
                                    }
                                }
                            }
                            else if (rule.rule == CharacterTrait.RuleType.ifPartnerAnyOfThese)
                            {
                                if (h.partner != null)
                                {
                                    foreach (CharacterTrait searchTrait in rule.traitList)
                                    {
                                        if (h.partner.characterTraits.Exists(item => item.trait == searchTrait))
                                        {
                                            pass = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (!pass && rule.mustPassForApplication)
                            {
                                passRules = false;
                                break;
                            }
                            else if(pass)
                            {
                                freqModMin += rule.appliedFrequencyMin;
                                freqModMax += rule.appliedFrequencyMax;
                            }
                        }

                        if(passRules)
                        {
                            int freqLow = p.frequencyPerOwnerMin + freqModMin;
                            int freqHigh = p.frequencyPerOwnerMax + 1 + freqModMax;

                            if(p.multiplyByMessiness)
                            {
                                freqLow = Mathf.RoundToInt(freqLow * (1f - h.conscientiousness));
                                freqHigh = Mathf.RoundToInt(freqHigh * (1f - h.conscientiousness));
                            }

                            int frequency = Toolbox.Instance.GetPsuedoRandomNumberContained(freqLow, freqHigh, randomKey, out randomKey);

                            Human writer = h;

                            if (p.writerIs == EvidencePreset.BelongsToSetting.partner && writer.partner != null)
                            {
                                writer = writer.partner;
                            }
                            else if (p.writerIs == EvidencePreset.BelongsToSetting.boss && writer.job != null && writer.job.boss != null && writer.job.boss.employee != null)
                            {
                                writer = writer.job.boss.employee;
                            }
                            else if (p.writerIs == EvidencePreset.BelongsToSetting.paramour && writer.paramour != null)
                            {
                                writer = writer.paramour;
                            }
                            else if (p.writerIs == EvidencePreset.BelongsToSetting.doctor)
                            {
                                writer = writer.GetDoctor();
                            }
                            else if(p.writerIs == EvidencePreset.BelongsToSetting.landlord)
                            {
                                writer = writer.GetLandlord();
                            }

                            Human receiver = h;

                            if (p.receiverIs == EvidencePreset.BelongsToSetting.partner && receiver.partner != null)
                            {
                                receiver = receiver.partner;
                            }
                            else if (p.receiverIs == EvidencePreset.BelongsToSetting.boss && receiver.job != null && receiver.job.boss != null && receiver.job.boss.employee != null)
                            {
                                receiver = receiver.job.boss.employee;
                            }
                            else if (p.receiverIs == EvidencePreset.BelongsToSetting.paramour && receiver.paramour != null)
                            {
                                receiver = receiver.paramour;
                            }
                            else if(p.receiverIs == EvidencePreset.BelongsToSetting.doctor)
                            {
                                receiver = receiver.GetDoctor();
                            }
                            else if(p.receiverIs == EvidencePreset.BelongsToSetting.landlord)
                            {
                                receiver = receiver.GetLandlord();
                            }

                            if(writer == receiver && !p.canBeFromSelf && (p.writerIs != EvidencePreset.BelongsToSetting.self || p.receiverIs != EvidencePreset.BelongsToSetting.self))
                            {
                                continue;
                            }
                            else
                            {
                                for (int i = 0; i < frequency; i++)
                                {
                                    AddToPlacementPool(p, h, writer, receiver, null, p.securityLevel, p.ownedRule, p.perOwnerObjectPriority, null, null);
                                }
                            }
                        }
                    }
                }
            }
        }

        if (Game.Instance.devMode && Game.Instance.collectDebugData)
        {
            foreach (NewRoom r in rooms)
            {
                r.palcementKey3 = randomKey;
            }
        }

        //For use later, put all books into this list
        Dictionary<BookPreset, Human> allShelfBooks = new Dictionary<BookPreset, Human>();

        if (Game.Instance.devMode && Game.Instance.collectDebugData)
        {
            foreach (NewRoom r in rooms)
            {
                r.palcementKey4 = randomKey;
            }
        }

        //Add special items from address...
        if (thisAsAddress != null && thisAsAddress.addressPreset != null)
        {
            //Add special items from address
            foreach (InteractablePreset p in thisAsAddress.addressPreset.specialItems)
            {
                if(p == null)
                {
                    Game.LogError("Null interactable preset special item found in " + thisAsAddress.addressPreset.name);
                    continue;
                }
                else
                {
                    AddToPlacementPool(p, null, null, null, null, p.securityLevel, p.ownedRule, p.perGameLocationObjectPriority, null, null);
                }
            }

            if (Game.Instance.devMode && Game.Instance.collectDebugData)
            {
                foreach (NewRoom r in rooms)
                {
                    r.palcementKey51 = randomKey;
                }
            }

            //Add personal affects from citizens
            if (thisAsAddress.residence != null)
            {
                foreach (Human cit in thisAsAddress.inhabitants)
                {
                    foreach (InteractablePreset inter in cit.personalAffects)
                    {
                        AddToPlacementPool(inter, cit, cit, null, null, inter.securityLevel, inter.ownedRule, inter.perOwnerObjectPriority, null, null);
                    }

                    //Add books (non-shelf)
                    for (int i = 0; i < cit.booksAwayFromShelf; i++)
                    {
                        if (cit.nonShelfBooks.Count <= 0) break;
                        BookPreset b = cit.nonShelfBooks[Toolbox.Instance.GetPsuedoRandomNumberContained(0, cit.nonShelfBooks.Count, randomKey, out randomKey)];

                        if (b.spawnRule != BookPreset.SpawnRules.onlyAtWork)
                        {
                            //Chance to spawn @ home
                            if (b.spawnRule != BookPreset.SpawnRules.homeOrWork || (b.spawnRule == BookPreset.SpawnRules.homeOrWork && Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, randomKey, out randomKey) > 0.5f))
                            {
                                //Should this be placed in an owned place (ie secret)?
                                if (b.spawnRule == BookPreset.SpawnRules.secret)
                                {
                                    AddToPlacementPool(InteriorControls.Instance.bookNonShelfSecret, cit, cit, null, null, InteriorControls.Instance.bookNonShelfSecret.securityLevel, InteriorControls.Instance.bookNonShelfSecret.ownedRule, InteriorControls.Instance.bookNonShelfSecret.perOwnerObjectPriority, b, null);
                                }
                                else
                                {
                                    AddToPlacementPool(InteriorControls.Instance.bookNonShelf, cit, cit, null, null, InteriorControls.Instance.bookNonShelf.securityLevel, InteriorControls.Instance.bookNonShelf.ownedRule, InteriorControls.Instance.bookNonShelf.perOwnerObjectPriority, b, null);
                                }

                                //Remove book from pool...
                                cit.nonShelfBooks.Remove(b);
                                cit.library.Remove(b);
                            }
                        }
                    }

                    //Add remaining books to chance of spawning in shelf...
                    foreach (BookPreset b in cit.library)
                    {
                        if (!allShelfBooks.ContainsKey(b))
                        {
                            allShelfBooks.Add(b, cit);
                        }
                    }
                }
            }

            if (Game.Instance.devMode && Game.Instance.collectDebugData)
            {
                foreach (NewRoom r in rooms)
                {
                    r.palcementKey52 = randomKey;
                }
            }

            //Add work affects
            if (thisAsAddress.company != null)
            {
                foreach (Occupation employee in thisAsAddress.company.companyRoster)
                {
                    if (employee.employee == null) continue;

                    foreach (InteractablePreset inter in employee.employee.workAffects)
                    {
                        if (inter == null)
                        {
                            Game.LogError("Null work affect found for " + employee.employee.name);
                        }

                        AddToPlacementPool(inter, employee.employee, employee.employee, null, null, inter.securityLevel, inter.ownedRule, inter.perOwnerObjectPriority, null, null);
                    }

                    //Add books... IMPORTANT: This seems to cause a discrepency in placed objects
                    //for (int i = 0; i < employee.employee.booksAwayFromShelf; i++)
                    //{
                    //    if (employee.employee.nonShelfBooks.Count <= 0) break;

                    //    BookPreset b = employee.employee.nonShelfBooks[Toolbox.Instance.GetPsuedoRandomNumberContained(0, employee.employee.nonShelfBooks.Count, randomKey, out randomKey)];

                    //    //Chance to spawn @ work
                    //    if (b.spawnRule == BookPreset.SpawnRules.onlyAtWork || (b.spawnRule == BookPreset.SpawnRules.homeOrWork && Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, randomKey, out randomKey) > 0.5f))
                    //    {
                    //        AddToPlacementPool(InteriorControls.Instance.bookNonShelfSecret, employee.employee, employee.employee, null, null, InteriorControls.Instance.bookNonShelfSecret.securityLevel, InteriorControls.Instance.bookNonShelfSecret.ownedRule, InteriorControls.Instance.bookNonShelfSecret.perOwnerObjectPriority, b, null);

                    //        //Remove book from pool...
                    //        employee.employee.nonShelfBooks.Remove(b);
                    //        employee.employee.library.Remove(b);
                    //    }
                    //}
                }
            }
        }

        if (Game.Instance.devMode && Game.Instance.collectDebugData)
        {
            foreach (NewRoom r in rooms)
            {
                r.palcementKey5 = randomKey;
            }
        }

        //Sort objects by placement priority...
        objectsToPlace.Sort((p1, p2) => p2.priority.CompareTo(p1.priority)); //Highest first

        //Gather placement positions...
        Dictionary<SubObjectClassPreset, List<Placement>> placements = new Dictionary<SubObjectClassPreset, List<Placement>>();
        Dictionary<InteractablePreset, List<Interactable>> limitPerAddress = new Dictionary<InteractablePreset, List<Interactable>>(); //For quickly keeping track of items limited by address
        Dictionary<InteractablePreset, Dictionary<NewRoom, List<Interactable>>> limitPerRoom = new Dictionary<InteractablePreset, Dictionary<NewRoom, List<Interactable>>>();
        Dictionary<InteractablePreset, Dictionary<FurnitureLocation, List<Interactable>>> limitPerObject = new Dictionary<InteractablePreset, Dictionary<FurnitureLocation, List<Interactable>>>();

        foreach (NewRoom room in rooms)
        {
            if (Game.Instance.devMode && Game.Instance.collectDebugData)
            {
                room.poolSizeOnPlacement = objectsToPlace.Count;
                room.palcementKey6 = randomKey;
            }

            foreach (FurnitureClusterLocation furn in room.furniture)
            {
                foreach (FurnitureLocation obj in furn.clusterList)
                {
                    if(obj.furniture != null)
                    {
                        foreach(FurniturePreset.SubObject sub in obj.furniture.subObjects)
                        {
                            Placement newPlacement = new Placement { room = room, furniture = obj, placementClass = sub.preset, subObject = sub };

                            if(!placements.ContainsKey(newPlacement.placementClass))
                            {
                                placements.Add(newPlacement.placementClass, new List<Placement>());
                            }

                            placements[newPlacement.placementClass].Add(newPlacement);
                        }
                    }
                }
            }
        }

        //Now run through and place each item...
        while(objectsToPlace.Count > 0)
        {
            ObjectPlace pl = objectsToPlace[0];

            //Attempt to place in relevent folder/collection...
            if (pl.interactable.attemptToStoreInFolder != null && Toolbox.Instance.RandContained(0f, 1f, randomKey, out randomKey) <= pl.interactable.folderPlacementChance)
            {
                bool placed = false;

                foreach (NewRoom r in rooms)
                {
                    foreach (FurnitureLocation f in r.individualFurniture)
                    {
                        Interactable sp = null;

                        if (pl.interactable.folderOwnershipMustMatch)
                        {
                            sp = f.spawnedInteractables.Find(item => item.evidence != null && item.evidence.preset == pl.interactable.attemptToStoreInFolder && item.belongsTo == pl.belongsTo);
                        }
                        else
                        {
                            sp = f.spawnedInteractables.Find(item => item.evidence != null && item.evidence.preset == pl.interactable.attemptToStoreInFolder);
                        }

                        if (sp != null)
                        {
                            EvidenceMultiPage mp = sp.evidence as EvidenceMultiPage;

                            if (mp != null)
                            {
                                MetaObject newMeta = new MetaObject(pl.interactable, pl.belongsTo, pl.writer, pl.receiver, null);
                                mp.AddContainedMetaObjectToNewPage(newMeta);
                            }

                            placed = true;
                            break;
                        }
                    }

                    if (placed) break;
                }

                if(!placed)
                {
                    Game.Log("CityGen: Could not find " + pl.interactable.attemptToStoreInFolder.name + " for placing " + pl.interactable.name + " at " + name);
                }

                if (placed || pl.interactable.dontPlaceIfNoFolder)
                {
                    objectsToPlace.RemoveAt(0);
                    continue;
                }
            }

            bool warmItem = false;

            //Figure out if this is still warm...
            RetailItemPreset retailItem = pl.passedObject as RetailItemPreset;

            if (retailItem != null)
            {
                if (retailItem.isHot)
                {
                    float purchaseTime = pl.passedVars.Find(item => item.varType == Interactable.PassedVarType.time).value;

                    if (SessionData.Instance.gameTime - purchaseTime <= GameplayControls.Instance.foodHotTime)
                    {
                        warmItem = true;
                    }
                }
            }

            //Limit per address...
            if (pl.interactable.limitPerAddress)
            {
                if(limitPerAddress.ContainsKey(pl.interactable))
                {
                    if(limitPerAddress[pl.interactable].Count >= pl.interactable.perAddressLimit)
                    {
                        Game.Log("Object: " + name + " has maximum number of " + pl.interactable.name + " objects according to 'limit per gamelocation' setting.");

                        //Remove placement
                        objectsToPlace.RemoveAt(0);
                        continue;
                    }
                }
            }

            //Create a shuffled, valid list of options...
            List<Placement> valid = new List<Placement>();

            //Use the dictionary to filter by class...
            int totalClasses = pl.interactable.subObjectClasses.Count + pl.interactable.backupClasses.Count;

            for (int i = 0; i < totalClasses; i++)
            {
                SubObjectClassPreset placeClass = null;

                if (i < pl.interactable.subObjectClasses.Count)
                {
                    placeClass = pl.interactable.subObjectClasses[i];
                }
                else
                {
                    placeClass = pl.interactable.backupClasses[i - pl.interactable.subObjectClasses.Count];
                }

                if (!placements.ContainsKey(placeClass)) continue; //Skip if none...

                //Get the filtered list of possible classes...
                List<Placement> matchingClasses = placements[placeClass];

                //Apply filtering
                for (int u = 0; u < matchingClasses.Count; u++)
                {
                    Placement temp = matchingClasses[u];

                    //Apply filters here...
                    if (pl.dontPlaceInRooms != null && pl.dontPlaceInRooms.Contains(temp.room)) continue; //Ignore these rooms...
                    if (!temp.room.preset.allowPersonalAffects && pl.belongsTo != null) continue; //Don't allow personal affects...

                    if(pl.interactable.limitToCertainRooms)
                    {
                        if (!pl.interactable.onlyInRooms.Contains(temp.room.preset)) continue; //Banned from this room...
                    }
                    else
                    {
                        if (pl.interactable.banFromRooms.Contains(temp.room.preset)) continue; //Banned from this room...
                    }

                    if(pl.interactable.limitToCertainBuildings)
                    {
                        if (temp.room.building == null || !pl.interactable.onlyInBuildings.Contains(temp.room.building.preset)) continue;
                    }

                    //Limit to x per room
                    if (pl.interactable.limitPerRoom)
                    {
                        if(limitPerRoom.ContainsKey(pl.interactable))
                        {
                            if(limitPerRoom[pl.interactable].ContainsKey(temp.room))
                            {
                                if (limitPerRoom[pl.interactable][temp.room].Count >= pl.interactable.perRoomLimit) continue; //Limit per room
                            }
                        }
                    }

                    //Limit to x per object
                    if (pl.interactable.limitPerObject)
                    {
                        if (limitPerObject.ContainsKey(pl.interactable))
                        {
                            if (temp.furniture != null && limitPerObject[pl.interactable].ContainsKey(temp.furniture))
                            {
                                if (limitPerObject[pl.interactable][temp.furniture].Count >= pl.interactable.perObjectLimit) continue; //Limit per object
                            }
                        }
                    }

                    if (warmItem)
                    {
                        if (temp.placementClass == InteriorControls.Instance.fridge) continue; //Don't put warm items in the fridge
                    }

                    //Apply a rank to this...
                    temp.rank = Toolbox.Instance.RandContained(0f, 0.1f, randomKey, out randomKey); //base random
                    if (pl.interactable.useSubSpawning && temp.subSpawn != null) temp.rank += 1f; //Prioritise sub spawns

                    InteractablePreset.OwnedPlacementRule ownershipSetting = pl.ownership;

                    if (pl.interactable.overrideWithOnlyOwnedSpawnAtWork)
                    {
                        if (pl.belongsTo != null && pl.belongsTo.job != null && pl.belongsTo.job.employer != null)
                        {
                            if (pl.belongsTo.job.employer.address == temp.room.gameLocation)
                            {
                                ownershipSetting = InteractablePreset.OwnedPlacementRule.ownedOnly;
                            }
                        }
                    }

                    //Ownership match
                    if (ownershipSetting == InteractablePreset.OwnedPlacementRule.both)
                    {
                        temp.rank += 1f;
                    }
                    else if (ownershipSetting == InteractablePreset.OwnedPlacementRule.ownedOnly)
                    {
                        if (pl.belongsTo == null)
                        {
                            continue;
                        }
                        else
                        {
                            //Belongs to nobody
                            if (temp.subObject.belongsTo == FurniturePreset.SubObjectOwnership.nobody)
                            {
                                continue;
                            }
                            //Belongs to everybody on the furniture ownership map, so check for presence...
                            else if(temp.subObject.belongsTo == FurniturePreset.SubObjectOwnership.everybody)
                            {
                                if (!temp.furniture.ownerMap.ContainsKey(new FurnitureLocation.OwnerKey(pl.belongsTo)))
                                {
                                    continue;
                                }
                            }
                            //Belongs to a specific person on the ownership map...
                            else
                            {
                                int ownedBy = (int)temp.subObject.belongsTo - 2; //Reach index 0 by minusing 2 from this enum
                                Human mustBelongTo = null;
                                NewAddress mustBelongToAd = null;

                                foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> pair in temp.furniture.ownerMap)
                                {
                                    if(pair.Key.human != null)
                                    {
                                        if (pair.Value == ownedBy)
                                        {
                                            mustBelongTo = pair.Key.human; //Found who this must belong to
                                            break;
                                        }
                                    }
                                    else if(pair.Key.address != null)
                                    {
                                        if(pair.Value == ownedBy)
                                        {
                                            mustBelongToAd = pair.Key.address;
                                            break;
                                        }
                                    }
                                }

                                if (mustBelongTo != null && mustBelongTo != pl.belongsTo)
                                {
                                    continue;
                                }
                                else if (mustBelongToAd != null && mustBelongToAd != pl.belongsTo.home)
                                {
                                    continue;
                                }
                                else if (mustBelongTo == null && mustBelongToAd == null) continue;
                            }
                        }
                    }
                    else if(ownershipSetting == InteractablePreset.OwnedPlacementRule.prioritiseOwned)
                    {
                        if (pl.belongsTo != null)
                        {
                            //Belongs to everybody on the furniture ownership map, so check for presence...
                            if (temp.subObject.belongsTo == FurniturePreset.SubObjectOwnership.everybody)
                            {
                                if (!temp.furniture.ownerMap.ContainsKey(new FurnitureLocation.OwnerKey(pl.belongsTo)))
                                {
                                    temp.rank += 1f;
                                }
                            }
                            //Belongs to a specific person on the ownership map...
                            else
                            {
                                int ownedBy = (int)temp.subObject.belongsTo - 2; //Reach index 0 by minusing 2 from this enum
                                Human mustBelongTo = null;
                                NewAddress mustBelongToAd = null;

                                foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> pair in temp.furniture.ownerMap)
                                {
                                    if(pair.Key.human != null)
                                    {
                                        if (pair.Value == ownedBy)
                                        {
                                            mustBelongTo = pair.Key.human; //Found who this must belong to
                                            break;
                                        }
                                    }
                                    else if(pair.Key.address != null)
                                    {
                                        if(pair.Value == ownedBy)
                                        {
                                            mustBelongToAd = pair.Key.address;
                                            break;
                                        }
                                    }
                                }

                                if (mustBelongTo != null && mustBelongTo == pl.belongsTo)
                                {
                                    temp.rank += 1f;
                                }
                                else if (mustBelongToAd != null && mustBelongToAd == pl.belongsTo.home)
                                {
                                    temp.rank += 1f;
                                }
                            }
                        }
                    }
                    else if (ownershipSetting == InteractablePreset.OwnedPlacementRule.nonOwnedOnly)
                    {
                        if (temp.subObject.belongsTo != FurniturePreset.SubObjectOwnership.nobody)
                        {
                            continue;
                        }
                    }
                    else if(ownershipSetting == InteractablePreset.OwnedPlacementRule.prioritiseNonOwned)
                    {
                        if (temp.subObject.belongsTo == FurniturePreset.SubObjectOwnership.nobody)
                        {
                            temp.rank += 1f;
                        }
                    }

                    //Security match (+10)
                    int thisPlacementSecurity = temp.room.preset.securityLevel + temp.subObject.security;

                    if (thisPlacementSecurity == pl.security)
                    {
                        temp.rank += 1f;
                    }
                    //It's better to put this item in a more secure place over a less secure place
                    else if (thisPlacementSecurity > pl.security)
                    {
                        temp.rank += 1f - (Mathf.Abs(thisPlacementSecurity - pl.security) * 0.25f);
                    }
                    else
                    {
                        temp.rank += 1f - (Mathf.Abs(thisPlacementSecurity - pl.security) * 0.5f);
                    }

                    valid.Add(temp);
                }

                //Don't scan backup classes if we have valid placements...
                if(i >= pl.interactable.subObjectClasses.Count - 1 && valid.Count > 0)
                {
                    break;
                }
            }

            //If we reach these there are valid positions, so pick one...
            if(valid.Count > 0)
            {
                //Sort, highest first...
                valid.Sort((p1, p2) => p2.rank.CompareTo(p1.rank)); //Highest first

                Placement chosen = valid[0];

                Interactable newObj = null;

                //Pick a placement upon furniture...
                if (chosen.subSpawn == null)
                {
                    //Furniture interactables must be created first...
                    if (!chosen.furniture.createdInteractables)
                    {
                        chosen.furniture.CreateInteractables();
                    }

                    newObj = InteractableCreator.Instance.CreateFurnitureSpawnedInteractableThreadSafe(pl.interactable, chosen.furniture.anchorNode.room, chosen.furniture, chosen.subObject, pl.belongsTo, pl.writer, pl.receiver, pl.passedVars, pl.interactable.isLight, pl.passedObject);
                }
                //Pick a placement on an existing object
                else
                {
                    //Furniture interactables must be created first...
                    if (!chosen.subSpawn.furnitureParent.createdInteractables)
                    {
                        chosen.subSpawn.furnitureParent.CreateInteractables();
                    }

                    int ssIndex = Toolbox.Instance.RandContained(0, chosen.subSpawn.ssp.Count, randomKey, out randomKey);
                    InteractablePreset.SubSpawnSlot ss = chosen.subSpawn.ssp[ssIndex];

                    //Spawn in a small radius around this
                    Vector2 c = (UnityEngine.Random.insideUnitCircle * 0.2f);
                    Vector3 r = chosen.subSpawn.wPos + ss.localPos;
                    r += new Vector3(c.x, 0, c.y);

                    Vector3 e = chosen.subSpawn.wEuler + ss.localEuler;

                    newObj = InteractableCreator.Instance.CreateWorldInteractable(pl.interactable, pl.belongsTo, pl.writer, pl.receiver, r, e, pl.passedVars, pl.passedObject);

                    //Remove slot from interactable
                    chosen.subSpawn.ssp.RemoveAt(ssIndex);
                }

                //Set priority
                if (newObj != null)
                {
                    newObj.opp = pl.priority;

                    //Add to limit per address counts...
                    if(newObj.preset.limitPerAddress)
                    {
                        if(!limitPerAddress.ContainsKey(newObj.preset))
                        {
                            limitPerAddress.Add(newObj.preset, new List<Interactable>());
                        }

                        limitPerAddress[newObj.preset].Add(newObj);
                    }

                    //Add to limit per room...
                    if(newObj.preset.limitPerRoom)
                    {
                        if(!limitPerRoom.ContainsKey(newObj.preset))
                        {
                            limitPerRoom.Add(newObj.preset, new Dictionary<NewRoom, List<Interactable>>());
                        }

                        if(!limitPerRoom[newObj.preset].ContainsKey(chosen.room))
                        {
                            limitPerRoom[newObj.preset].Add(chosen.room, new List<Interactable>());
                        }

                        limitPerRoom[newObj.preset][chosen.room].Add(newObj);
                    }

                    //Add to limit per object...
                    if (newObj.preset.limitPerObject)
                    {
                        if (!limitPerObject.ContainsKey(newObj.preset))
                        {
                            limitPerObject.Add(newObj.preset, new Dictionary<FurnitureLocation, List<Interactable>>());
                        }

                        if (!limitPerObject[newObj.preset].ContainsKey(chosen.furniture))
                        {
                            limitPerObject[newObj.preset].Add(chosen.furniture, new List<Interactable>());
                        }

                        limitPerObject[newObj.preset][chosen.furniture].Add(newObj);
                    }

                    //Setup new subspawns here...
                    if(newObj.ssp != null && newObj.ssp.Count > 0 && newObj.preset.subSpawnClass != null)
                    {
                        //Add a new placement
                        Placement newPlacement = new Placement { room = chosen.room, furniture = chosen.furniture, placementClass = newObj.preset.subSpawnClass, subObject = chosen.subObject, subSpawn = chosen.subSpawn };

                        if (!placements.ContainsKey(newPlacement.placementClass))
                        {
                            placements.Add(newPlacement.placementClass, new List<Placement>());
                        }

                        placements[newPlacement.placementClass].Add(newPlacement);
                    }

                    //Remove placement if it isn't a subspawn location or has none left
                    if (chosen.subSpawn != null && chosen.subSpawn.ssp.Count > 0)
                    {
                        //Leave this...
                    }
                    //Otherwise remove...
                    else
                    {
                        //Remove this placement as an option
                        placements[chosen.placementClass].Remove(chosen);

                        //If no more placements here then remove key
                        if (placements[chosen.placementClass].Count <= 0)
                        {
                            placements.Remove(chosen.placementClass);
                        }
                    }

                    //Add stack?
                    if(newObj.preset.specialCaseFlag == InteractablePreset.SpecialCase.bookStack)
                    {
                        stacks.Add(newObj);
                    }
                }
            }
            else
            {
                //Game.Log("CityGen: Unable to place object " + pl.interactable.name + "; no valid places left in " + name);
            }

            //If location is completely filled then exit loop
            if(placements.Count <= 0)
            {
                break;
            }

            //Remove placement
            objectsToPlace.RemoveAt(0);
        }

        //Spawn stacked items...
        if (thisAsAddress != null)
        {
            //Add spare key
            if(thisAsAddress.addressPreset != null)
            {
                if (Toolbox.Instance.RandContained(0f, 1f, randomKey, out randomKey) <= thisAsAddress.addressPreset.chanceOfExternalSpareKey)
                {
                    Toolbox.Instance.SpawnSpareKey(thisAsAddress);
                }
            }

            //Spawn stacked items...
            if (thisAsAddress.stacks.Count > 0)
            {
                InteractablePreset.SpecialCase stackType = InteractablePreset.SpecialCase.bookStack;

                //Books need a special process as we're looking to spawn from a pool of available titles...
                //Game.Log("CityGen: Attempting to place " + allShelfBooks.Count + " shelf books at " + name + "...");

                //Book shelf
                List<Interactable> matchingStacks = thisAsAddress.stacks.FindAll(item => item.preset.specialCaseFlag == stackType && item.ssp.Count > 0);

                //Game.Log("CityGen: ... Found " + matchingStacks.Count + " matching stacks...");

                //List of keys for available books to spawn
                List<BookPreset> typeKeys = new List<BookPreset>(allShelfBooks.Keys);

                //While there is space to spawn on shelfs and there are books to spawn, then spawn them!
                while (matchingStacks.Count > 0 && allShelfBooks.Count > 0)
                {
                    int stackIndex = Toolbox.Instance.RandContained(0, matchingStacks.Count, randomKey, out randomKey);
                    int bookKeyIndex = Toolbox.Instance.RandContained(0, typeKeys.Count, randomKey, out randomKey);

                    Interactable bookStack = matchingStacks[stackIndex];
                    BookPreset bookToSpawn = typeKeys[bookKeyIndex];

                    //Pick valid spawn position
                    int slotIndex = Toolbox.Instance.RandContained(0, bookStack.ssp.Count, randomKey, out randomKey);

                    //We now have the spawn position! Spawn a book!
                    //The book will spawn as a parent of the furniture, not the book collection, so calculate spawning positions...
                    Vector3 localPos = bookStack.lPos + bookStack.ssp[slotIndex].localPos;
                    Vector3 localEuler = bookStack.lEuler + bookStack.ssp[slotIndex].localEuler;

                    Interactable newObj = InteractableCreator.Instance.CreateBookInteractable(InteriorControls.Instance.bookShelf, bookStack.furnitureParent.anchorNode.room, bookStack.furnitureParent, allShelfBooks[bookToSpawn], localPos, localEuler, bookToSpawn);

                    //Remove slot
                    bookStack.ssp.RemoveAt(slotIndex);

                    //If all positions are gone then remove from list
                    if (bookStack.ssp.Count <= 0)
                    {
                        matchingStacks.RemoveAt(stackIndex);
                    }

                    //Remove book from list
                    typeKeys.RemoveAt(bookKeyIndex);
                    allShelfBooks.Remove(bookToSpawn);
                }
            }
        }

        //Clear lists
        objectsToPlace = null;
        objectPoolPlaced = true;
    }

    //Returns true if open to the public or an employee
    public bool IsPublicallyOpen(bool forPlayer)
    {
        if (isCrimeScene) return false;

        if (thisAsAddress != null && thisAsAddress.access == AddressPreset.AccessType.allPublic)
        {
            if(forPlayer && thisAsAddress.addressPreset != null && thisAsAddress.addressPreset.needsPassword)
            {
                if(!GameplayController.Instance.playerKnowsPasswords.Contains(thisAsAddress.id))
                {
                    return false; //This needs a password and the player doesn't know yet...
                }
            }

            if (thisAsAddress.company != null)
            {
                if (!thisAsAddress.company.preset.publicFacing) return false;

                if (thisAsAddress.company.openForBusinessActual && thisAsAddress.company.openForBusinessDesired)
                {
                    return true;
                }
                else return false;
            }

            //Are the open hours here dictated by an adjacent company?
            if (thisAsAddress.addressPreset != null && thisAsAddress.addressPreset.openHoursDicatedByAdjoiningCompany)
            {
                foreach (NewNode.NodeAccess acc in entrances)
                {
                    if (!acc.walkingAccess) continue;

                    if (acc.toNode.gameLocation != this)
                    {
                        if (acc.toNode.gameLocation.thisAsAddress != null && acc.toNode.gameLocation.thisAsAddress.company != null && acc.toNode.gameLocation.thisAsAddress.company.publicFacing)
                        {
                            if (acc.toNode.gameLocation.thisAsAddress.company.openForBusinessActual && acc.toNode.gameLocation.thisAsAddress.company.openForBusinessDesired)
                            {
                                return true;
                            }
                            else return false;
                        }
                    }
                    else if (acc.fromNode.gameLocation != this)
                    {
                        if (acc.fromNode.gameLocation.thisAsAddress != null && acc.fromNode.gameLocation.thisAsAddress.company != null && acc.toNode.gameLocation.thisAsAddress.company.publicFacing)
                        {
                            if (acc.fromNode.gameLocation.thisAsAddress.company.openForBusinessActual && acc.fromNode.gameLocation.thisAsAddress.company.openForBusinessDesired)
                            {
                                return true;
                            }
                            else return false;
                        }
                    }
                }

                return true; //Default to open
            }
        }
        else if(thisAsStreet != null)
        {
            return true;
        }

        return false;
    }

    //Adds actor to the escalation reference and/or adds additional time to it
    public void AddEscalation(Actor actor)
    {
        if (!escalation.ContainsKey(actor))
        {
            int aid = 0;
            Human hu = actor as Human;
            if (hu != null) aid = hu.humanID;

            escalation.Add(actor, new TrespassEscalation { actor = aid, isPlayer = actor.isPlayer, lastEscalationCheck = SessionData.Instance.gameTime, timeEscalation = 0f });

            if (!CitizenBehaviour.Instance.tempEscalationBoost.Contains(this))
            {
                CitizenBehaviour.Instance.tempEscalationBoost.Add(this);
            }
        }

        float timeAddition = SessionData.Instance.gameTime - escalation[actor].lastEscalationCheck;
        escalation[actor].timeEscalation += timeAddition;
        escalation[actor].timeEscalation = Mathf.Clamp(escalation[actor].timeEscalation, 0, GameplayControls.Instance.additionalEscalationTime);
        escalation[actor].lastEscalationCheck = SessionData.Instance.gameTime;

        Game.Log("Setting escalation (+) for player by " + actor.name + ": " + escalation[actor].timeEscalation);
    }

    //Gets the additional escalation level for this location
    public int GetAdditionalEscalation(Actor actor)
    {
        if (escalation.ContainsKey(actor))
        {
            int escLevel = 0;

            if (escalation[actor].timeEscalation >= GameplayControls.Instance.additionalEscalationTime)
            {
                escLevel++;
            }

            //if (escalation[actor].timeEscalation >= GameplayControls.Instance.additionalEscalationTime * 2f)
            //{
            //    escLevel++;
            //}

            return escLevel;
        }
        else return 0;
    }

    //Removes escalation time for this location
    public void RemoveEscalation(Actor actor, bool removeAll = false)
    {
        if (escalation.ContainsKey(actor))
        {
            float timeAddition = SessionData.Instance.gameTime - escalation[actor].lastEscalationCheck;
            escalation[actor].timeEscalation -= timeAddition;
            escalation[actor].lastEscalationCheck = SessionData.Instance.gameTime;

            Game.Log("Setting escalation (-) for player by " + actor.name + ": " + escalation[actor].timeEscalation);

            if (escalation[actor].timeEscalation <= 0f || removeAll)
            {
                escalation.Remove(actor);

                if (escalation.Count <= 0)
                {
                    CitizenBehaviour.Instance.tempEscalationBoost.Remove(this);
                }
            }
        }
    }

    //Add security camera
    public void AddSecurityCamera(Interactable newInteractable)
    {
        securityCameras.Add(newInteractable);
    }

    //Set this as crime scene
    public void SetAsCrimeScene(bool val)
    {
        if(val != isCrimeScene)
        {
            isCrimeScene = val;
            Game.Log(name + " is crime scene: " + isCrimeScene);

            if(isCrimeScene)
            {
                NewspaperController.Instance.GenerateNewNewspaper();
            }
        }

        if (isCrimeScene)
        {
            if(!GameplayController.Instance.crimeScenes.Contains(this))
            {
                GameplayController.Instance.crimeScenes.Add(this);
            }
        }
        else
        {
            GameplayController.Instance.crimeScenes.Remove(this);

            //Remove put up crime scenes
            foreach(NewRoom r in rooms)
            {
                foreach(NewNode n in r.nodes)
                {
                    for (int i = 0; i < n.interactables.Count; i++)
                    {
                        Interactable inter = n.interactables[i];

                        if(inter.preset == InteriorControls.Instance.streetCrimeScene)
                        {
                            inter.SafeDelete();
                            i--;
                        }
                    }
                }
            }
        }
    }

    public virtual bool IsAlarmSystemTarget(Human human)
    {
        return false;
    }

    public virtual bool IsAlarmActive(out float retAlarmTimer, out NewBuilding.AlarmTargetMode retTargetMode, out List<Human> retTargets)
    {
        retAlarmTimer = 0f;
        retTargetMode = NewBuilding.AlarmTargetMode.illegalActivities;
        retTargets = null;

        return false;
    }

    public virtual bool IsOutside()
    {
        return isOutside;
    }

    //Generate a replicable seed based on this location class. Must be done after the room is setup as it uses node locations to generate this.
    public string GetReplicableSeed()
    {
        int sum = 0;

        //I'm using the sum of coordinates here so it doesn't matter if this node list is loaded in in a different order
        foreach(NewNode n in nodes)
        {
            sum += (int)n.nodeCoord.x + (int)n.nodeCoord.y + (int)n.nodeCoord.z;
        }

        return sum.ToString();
    }

    //Remove all furniture & items
    [Button]
    public void RemoveEverything()
    {
        RemoveAllInhabitantFurniture(true, FurnitureClusterLocation.RemoveInteractablesOption.remove);
    }

    public void RemoveAllInhabitantFurniture(bool removeSkipAddressInhabitantsFurniture, FurnitureClusterLocation.RemoveInteractablesOption spawnedOnFurnitureRemovalOption)
    {
        foreach(NewRoom r in rooms)
        {
            r.RemoveAllInhabitantFurniture(removeSkipAddressInhabitantsFurniture, spawnedOnFurnitureRemovalOption);
        }
    }

    [Button]
    public void DisplayAccess()
    {
        //while(PrefabControls.Instance.pathfindDebugParent.childCount > 0)
        //{
        //    Destroy(PrefabControls.Instance.pathfindDebugParent.GetChild(0));
        //}

        foreach (NewRoom r in rooms)
        {
            foreach(NewNode n in r.nodes)
            {
                foreach (KeyValuePair<NewNode, NewNode.NodeAccess> pair in n.accessToOtherNodes)
                {
                    if(!pair.Value.walkingAccess || pair.Value.employeeDoor || pair.Value.toNode.noPassThrough)
                    {
                        GameObject debugNode = Instantiate(PrefabControls.Instance.pathfindInternalDebug, PrefabControls.Instance.pathfindDebugParent);
                        debugNode.GetComponent<DebugPathfind>().Setup(pair.Value, pair.Value.fromNode.room, null);
                    }
                    else
                    {
                        GameObject debugNode = Instantiate(PrefabControls.Instance.pathfindNodeDebug, PrefabControls.Instance.pathfindDebugParent);
                        debugNode.GetComponent<DebugPathfind>().Setup(pair.Value, pair.Value.fromNode.room, null);
                    }
                }
            }

        }
    }

    [Button]
    //Get square meterage
    public int GetSQM(bool print = true)
    {
        float ret = 0f;

        foreach(NewRoom r in rooms)
        {
            ret += r.nodes.Count;
        }

        //* by node side
        ret *= PathFinder.Instance.nodeSize.x;

        if (print) Game.Log(Mathf.RoundToInt(ret));

        return Mathf.RoundToInt(ret);
    }

    [Button]
    //Get price/land value
    public int GetPrice(bool print = true)
    {
        float ret = 0f;
        float plusMinusVariance = 0f;

        float normalizedValue = Toolbox.Instance.GetNormalizedLandValue(this, print);

        ret = Mathf.Lerp(GameplayControls.Instance.propertyValueRange.x, GameplayControls.Instance.propertyValueRange.y, GameplayControls.Instance.propertyValueCurve.Evaluate(normalizedValue));

        plusMinusVariance = normalizedValue * (GameplayControls.Instance.propertyValueRange.y * 0.025f);
        plusMinusVariance = Mathf.RoundToInt(Toolbox.Instance.GetPsuedoRandomNumber(-plusMinusVariance, plusMinusVariance, rooms.Count.ToString() + nodes.Count.ToString() + CityData.Instance.seed));
        ret += plusMinusVariance;

        ret *= Game.Instance.housePriceMultiplier;
        ret = Mathf.RoundToInt(ret / 100f) * 100; //Nearest 100

        if(print) Game.Log(Mathf.RoundToInt(ret) + "= Land value: " + Mathf.Lerp(GameplayControls.Instance.propertyValueRange.x, GameplayControls.Instance.propertyValueRange.y, normalizedValue) + " + variance: " + plusMinusVariance);

        return Mathf.RoundToInt(ret);
    }

    [Button]
    public void GetAIActions()
    {
        foreach (KeyValuePair<AIActionPreset, List<Interactable>> pair in actionReference)
        {
            Game.Log(pair.Key.name + " (" + pair.Value.Count + ")");
        }
    }

    [Button]
    public void IsThisOutside()
    {
        Game.Log(IsOutside());
    }
}
