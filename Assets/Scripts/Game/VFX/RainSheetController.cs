﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System;

public class RainSheetController : MonoBehaviour
{
    [Header("Components")]
    [ReorderableList]
    public List<RainSheet> sheets = new List<RainSheet>();
    //public Transform rainCone;

    [Header("Settings")]
    public bool indoorRaycast = true;
    public int raycastsPerFrame = 3;
    private int rainBlockOnlyMask;
    private int rainBlockAndRoomMeshMask;
    private int sheetCursor = 0;
    public float rainSheetHeight = 10f;
    public bool snowMode = false;
    public Material material;
    public Material snowMaterial;

    [System.Serializable]
    public class RainSheet
    {
        public Transform rainSheetTransform;
        public MeshRenderer renderer;
    }

    // Start is called before the first frame update
    void Start()
    {
        //Block rain for building models and special layer 28: 'RainBlocker'
        rainBlockOnlyMask = Toolbox.Instance.CreateLayerMask(Toolbox.LayerMaskMode.onlyCast, 27, 28);
        rainBlockAndRoomMeshMask = Toolbox.Instance.CreateLayerMask(Toolbox.LayerMaskMode.onlyCast, 27, 28, 29);

        SetSnowMode(false, true);
    }

    public void SetSnowMode(bool val, bool forceUpdate = false)
    {
        if(val != snowMode || forceUpdate)
        {
            snowMode = val;

            foreach (RainSheet r in sheets)
            {
                if(snowMode)
                {
                    r.renderer.sharedMaterial = snowMaterial;
                }
                else
                {
                    r.renderer.sharedMaterial = material;
                }
            }
        }
    }

    public void SetEnabled(bool val)
    {
        this.gameObject.SetActive(val);
    }

    // Update is called once per frame
    void Update()
    {
        if(SessionData.Instance.currentRain > 0f || SessionData.Instance.currentSnow > 0f)
        {
            //Always position on player
            this.transform.position = new Vector3(Player.Instance.transform.position.x, rainSheetHeight + 0.01f, Player.Instance.transform.position.z);
            //rainCone.transform.position = new Vector3(Player.Instance.transform.position.x, rainCone.transform.position.y, Player.Instance.transform.position.z);

            if (indoorRaycast)
            {
                for (int i = 0; i < raycastsPerFrame; i++)
                {
                    RainSheet updateSheet = sheets[sheetCursor];

                    //Reset to proper scale first...
                    updateSheet.rainSheetTransform.localScale = Vector3.one;

                    float yScaleMin = 1f; //The Y scale will be the lowest detected by rays

                    //Cast 2 rays; one on each vertical edge...
                    for (int u = 0; u < 3; u++)
                    {
                        //Start at top of sheet
                        Vector3 worldRayPos = updateSheet.rainSheetTransform.position;
                        if (u == 1) worldRayPos = updateSheet.rainSheetTransform.TransformPoint(new Vector3(-0.5f, 0f, 0f));
                        else if (u == 2) worldRayPos = updateSheet.rainSheetTransform.TransformPoint(new Vector3(-1f, 0f, 0f));

                        //Cast a raycast downwards
                        Ray ray = new Ray(worldRayPos, Vector3.down);

                        RaycastHit hit;

                        //Cast down until we hit something
                        if (Physics.Raycast(ray, out hit, rainSheetHeight, rainBlockAndRoomMeshMask))
                        {
                            //Find scale
                            float dist = ray.origin.y - hit.point.y;
                            float yScale = dist / rainSheetHeight;

                            yScaleMin = Mathf.Min(yScaleMin, Mathf.Clamp01(yScale));

                            if((Game.Instance.devMode && Game.Instance.collectDebugData)) Debug.DrawRay(ray.origin, Vector3.down * dist, Color.red, 0.1f);

                            //Cast a ray back up to find if there is a ceiling here...
                            Ray upRay = new Ray(hit.point, Vector3.up);
                            RaycastHit upHit;

                            if (Physics.Raycast(upRay, out upHit, dist, rainBlockAndRoomMeshMask))
                            {
                                float minusDist = upHit.point.y - hit.point.y;
                                yScale = (dist - minusDist) / rainSheetHeight;
                                yScaleMin = Mathf.Min(yScaleMin, Mathf.Clamp01(yScale));
                                if ((Game.Instance.devMode && Game.Instance.collectDebugData)) Debug.DrawRay(upRay.origin + new Vector3(0.02f, 0, 0), Vector3.up * (dist - minusDist), Color.cyan, 0.1f);
                            }
                        }
                        //If no hit, perform an inverse raycast that starts at the ground level and travels upwards to negate the problem of casts starting inside the building collider...
                        else
                        {
                            worldRayPos += new Vector3(0f, -rainSheetHeight, 0f);
                            Ray inverseRay = new Ray(worldRayPos, Vector3.up);

                            if (Physics.Raycast(inverseRay, out hit, rainSheetHeight, rainBlockAndRoomMeshMask))
                            {
                                //Find scale
                                float invDist = hit.point.y - inverseRay.origin.y;
                                float invYScale = 1f - invDist / rainSheetHeight;

                                yScaleMin = Mathf.Min(yScaleMin, Mathf.Clamp01(invYScale));

                                if ((Game.Instance.devMode && Game.Instance.collectDebugData)) Debug.DrawRay(inverseRay.origin + new Vector3(0.02f, 0, 0), Vector3.up * invDist, Color.yellow, 0.1f);
                            }
                            ////If still no hit, then assume this is outside...
                            //else
                            //{
                            //    Debug.DrawRay(worldRayPos, Vector3.up * rainSheetHeight, Color.green, 0.1f);
                            //}
                        }
                    }

                    //Set local Y scale
                    updateSheet.rainSheetTransform.localScale = new Vector3(1f, yScaleMin, 1f);

                    sheetCursor++;

                    if (sheetCursor >= sheets.Count)
                    {
                        sheetCursor = 0; //Reset cursor
                        break;
                    }
                }
            }
        }
    }
}
