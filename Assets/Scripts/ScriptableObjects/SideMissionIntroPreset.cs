﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "sidejobintro_data", menuName = "Database/Side Job Intro Preset")]

public class SideMissionIntroPreset : SoCustomComparison
{
    [Header("Rewards")]
    public int rewardModifier = 0;

    [Header("Elements")]
    public List<SideMissionObjectiveBlock> blocks = new List<SideMissionObjectiveBlock>();

    public enum SideMissionElementType { playerCallsNumber, acquireInformation, askStaff, spawnItems, photoOfItemLocation, openedBriefcase, postSubmission, playerHasCamera, setGooseChaseCall, setMeeting, handDossier, setupHomeInvestigation, submitToPoster, setHomeMeeting, setGooseChaseCallIndoorOnly, tailBriefcase, playerHasItemInPossession, leaveItemAtSecretLocation, destroyItem, playerHasHandcuffs, telephoneSubmission, placeItemInPosterMailbox, placeItemOfTypeInPosterMailbox };

    [System.Serializable]
    public class SideMissionObjectiveBlock
    {
        public string name;
        public SideMissionElementType elementType;
        public string dialogReference;
        public JobPreset.JobTag tagReference;
        public List<JobPreset.StartingSpawnItem> spawnItems = new List<JobPreset.StartingSpawnItem>();
        public bool enableUpdateWhileTalking = false;
        public float objectiveDelay = 0f;
        public List<InteractablePreset> validItems = new List<InteractablePreset>();
        public List<FurniturePreset> validFurniture = new List<FurniturePreset>();
        public List<JobPreset.DifficultyTag> disableOnDifficulties = new List<JobPreset.DifficultyTag>();
        public List<SideMissionIntroPreset> onlyCompativleWithIntros = new List<SideMissionIntroPreset>();
        public List<SideMissionHandInPreset> onlyCompatibleWithHandIns = new List<SideMissionHandInPreset>();
        public List<JobPreset.JobTag> triggerFailIfItemDestroyed = new List<JobPreset.JobTag>();
    }
}
