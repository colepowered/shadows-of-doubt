﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using NaughtyAttributes;
using System.Linq;

//Controller assigned to each citizen in the city
//This script must be super efficient as it's being run by each citizen (so ~4k times!)
//Script pass 1
public class Citizen : Human
{
    [Header("Citizen Attributes")]
    public bool alwaysPassDialogSuccess = false;

    //Sorting
    [System.NonSerialized]
	public float customSort = 0f;

    //Create evidence file (this will already be done, but use this to create extra evidence detals/facts)
    public override void SetupEvidence()
    {
        base.SetupEvidence();

        CreateDetails();

        //Generate note(s) for different data keys
        //evidenceEntry.SetNote((new Evidence.DataKey[] { Evidence.DataKey.suspect }).ToList(), StringTables.Get("evidence.body", "citizen_suspect"));
        evidenceEntry.SetNote((new Evidence.DataKey[] { Evidence.DataKey.name }).ToList(), Strings.GetTextForComponent("e6f7dae9-af6a-4d58-b7eb-44b6bba58dcb", this));
    }

    //Run this every day for checking if it's someone's birthday: If so create cards (or remove them)
    public void BirthdayCheck()
    {
        bool birthdayVicinity = false; //Has it been 7 days since this person's birthday?

        string[] birthdayInts = birthday.Split('/');

        int birthdayMonth = 0;
        int birthdayDay = 0;
        int.TryParse(birthdayInts[0], out birthdayMonth);
        int.TryParse(birthdayInts[1], out birthdayDay);

        if ((SessionData.Instance.monthInt + 1) == birthdayMonth)
        {
            //Day has passed
            if ((SessionData.Instance.dateInt + 1) >= birthdayDay)
            {
                if ((SessionData.Instance.dateInt + 1) <= birthdayDay + 7)
                {
                    birthdayVicinity = true;
                }
            }
        }
        //It's the next month: Check for birthdays at the end of last month
        else if ((SessionData.Instance.monthInt + 1) == birthdayMonth + 1 || (SessionData.Instance.monthInt == 0 && birthdayMonth == 12))
        {
            int daysPrev = 7 - (SessionData.Instance.dateInt + 1);
            int check = SessionData.Instance.daysInMonths[birthdayMonth - 1] - daysPrev;

            if (SessionData.Instance.dateInt >= check)
            {
                birthdayVicinity = true;
            }
        }

        //Spawn cards
        if (birthdayVicinity && birthdayCards.Count <= 0 && home != null)
        {
            //Only if player isn't here
            if (Player.Instance.currentGameLocation != home)
            {
                //From acquaintances I know & like
                foreach (Acquaintance aq in acquaintances)
                {
                    if (aq.known > SocialControls.Instance.knowBirthdayThreshold)
                    {
                        if (aq.like >= 0.25f)
                        {
                            //Work connection
                            if (aq.connections.Contains(Acquaintance.ConnectionType.boss) || aq.connections.Contains(Acquaintance.ConnectionType.familiarWork) || aq.connections.Contains(Acquaintance.ConnectionType.workOther) || aq.connections.Contains(Acquaintance.ConnectionType.workTeam))
                            {
                                if (job.employer != null)
                                {
                                    //Only if player isn't here
                                    if (Player.Instance.currentGameLocation != job.employer.address)
                                    {
                                        FurnitureLocation chosenFurn = null;
                                        Interactable spawnedLoc = job.employer.address.PlaceObject(InteriorControls.Instance.birthdayCard, this, aq.with, this, out chosenFurn, true, Interactable.PassedVarType.humanID, aq.with.humanID);
                                        birthdayCards.Add(spawnedLoc); //Add to reference...

                                        //Game.Log("Placed birthday card from " + aq.with.name + " to " + aq.from.name + " at " + job.employer.address + " in " + chosenFurn.furniture.name + " index: " + spawnedLoc.subObject);
                                    }
                                }
                            }
                            else
                            {
                                FurnitureLocation chosenFurn = null;
                                Interactable spawnedLoc = home.PlaceObject(InteriorControls.Instance.birthdayCard, this, aq.with, this, out chosenFurn, true, Interactable.PassedVarType.humanID);
                                birthdayCards.Add(spawnedLoc); //Add to reference...

                                //Game.Log("Placed birthday card from " + aq.with.name + " to " + aq.from.name + " at " + home.name);
                            }
                        }
                    }
                }
            }
            else if (!birthdayVicinity && birthdayCards.Count > 0)
            {
                for (int i = 0; i < birthdayCards.Count; i++)
                {
                    if (Player.Instance.currentGameLocation != birthdayCards[i].node.gameLocation)
                    {
                        birthdayCards[i].SafeDelete();
                        birthdayCards.RemoveAt(i);
                        i--;
                    }
                }
            }
        }
    }

    //Forced ragdroll duration is in gametime
    public override void RecieveDamage(float amount, Actor fromWho, Vector3 damagePosition, Vector3 damageDirection, SpatterPatternPreset forwardSpatter, SpatterPatternPreset backSpatter, SpatterSimulation.EraseMode spatterErase = SpatterSimulation.EraseMode.onceExecutedAndOutOfAddressPlusDespawnTime, bool alertSurrounding = true, bool forceRagdoll = false, float forcedRagdollDuration = 0f, float shockMP = 1f, bool enableKill = false, bool allowRecoil = true, float ragdollForceMP = 1f)
    {
        float extraDmg = Mathf.Min(currentHealth - amount, 0) * -1; //Used for calculating ragdoll force MP; gets extra damage and returns as + float

        base.RecieveDamage(amount, fromWho, damagePosition, damageDirection, forwardSpatter, backSpatter, spatterErase, shockMP: shockMP, enableKill: enableKill, allowRecoil: allowRecoil, ragdollForceMP: ragdollForceMP); //Base function deals damage

        //There will be blood
        if (amount >= 0.01f)
        {
            //Use damage to set the blood spatter count multiplier: Use ratio of maximum health. Half of their health uses the maximum amount
            float spatterMP = Mathf.Clamp01((amount / maximumHealth) * 2f);

            Vector3 localDir = this.transform.InverseTransformDirection(damageDirection);
            Vector3 localPos = this.transform.InverseTransformPoint(damagePosition);

            if (forwardSpatter != null) new SpatterSimulation(this, localPos, localDir, forwardSpatter, spatterErase, spatterMP, false);
            if (backSpatter != null) new SpatterSimulation(this, localPos, -localDir, backSpatter, spatterErase, spatterMP, false);

            //Cancel conversation
            if (ai != null)
            {
                if (ai.human.inConversation)
                {
                    ai.human.currentConversation.EndConversation();
                }

                if(amount / maximumHealth > 0.05f)
                {
                    speechController.TriggerBark(SpeechController.Bark.takeDamage);
                }
            }
        }

        Game.Log("Impact from " + fromWho + " to " + GetCitizenName() + ". Health left: " + currentHealth);

        //AI Response
        if (!Game.Instance.noReactOnAttack && (ai == null || (ai.currentGoal == null || ai.currentGoal.preset != RoutineControls.Instance.fleeGoal)))
        {
            if (ai != null && fromWho != null && fromWho != this)
            {
                if (Game.Instance.collectDebugData) SelectedDebug("Recieve damage reaction...", HumanDebug.misc);

                //Persue
                ai.SetPersue(fromWho, alertSurrounding, 1, true, CitizenControls.Instance.punchedResponseRange);
            }
        }

        //KO & ragdoll
        if(currentHealth <= 0f || forceRagdoll)
        {
            //When zero health is reached, KO (this is different to being murdered)
            if (ai != null)
            {
                float forceMP = ragdollForceMP + (extraDmg * CitizenControls.Instance.damageRecieveForceMultiplier);
                Game.Log("KO with force mp of " + forceMP);

                ai.SetKO(true, damagePosition, damageDirection, forceRagdoll, forcedRagdollDuration, forceMultiplier: forceMP);

                if(enableKill)
                {
                    Interactable weapon = null;
                    if(fromWho != null && fromWho.ai != null) weapon = fromWho.ai.currentWeapon;

                    MurderController.Murder murder = MurderController.Instance.activeMurders.Find(item => item.victim == this && item.murderer == fromWho);

                    Murder(fromWho as Human, true, murder, weapon);
                }
            }
        }
        else
        {
            if (animationController != null && allowRecoil)
            {
                animationController.TakeDamageRecoil(damagePosition);
            }
        }
    }

    public override void SetCombatSkill(float newSkill)
    {
        base.SetCombatSkill(newSkill);

        animationController.mainAnimator.SetFloat("combatSkill", combatSkill);
    }

    //Create a wound closest to this point
    public void CreateWoundClosestToPoint(Vector3 point, Vector3 normal, InteractablePreset woundPreset)
    {
        CitizenOutfitController.CharacterAnchor closestAnchor = CitizenOutfitController.CharacterAnchor.upperTorso;
        Vector3 closestLocal = GetNearestVert(point, out closestAnchor);
        Transform closestBodyPart = outfitController.anchorReference[closestAnchor];

        if (closestBodyPart != null)
        {
            Wound newWound = new Wound();
            newWound.humanID = humanID;
            newWound.timestamp = SessionData.Instance.gameTime;
            newWound.anchor = closestAnchor;

            //Create wound
            newWound.interactable = InteractableCreator.Instance.CreateTransformInteractable(woundPreset, closestBodyPart, this, null, closestLocal, Quaternion.FromToRotation(Vector3.up, normal).eulerAngles + closestBodyPart.eulerAngles, null);
            newWound.interactable.objectRef = newWound;
            newWound.interactable.MarkAsTrash(true);

            currentWounds.Add(newWound);
        }
    }
}
	
