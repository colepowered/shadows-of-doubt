﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

/// <summary>
/// Component to encapsulate loading assets from addressables at the start of the game.
/// Acts as a temporary buffer for loaded assets until they can be passed to Toolbox.
/// This is mainly to prevent problems with async loading breaking logic that expects 
/// assets to be available immediately in the Main scene.
/// </summary>
public class AssetLoader
{
    #region Constants

    public static readonly string DATA_GROUP = "data";
    public static readonly string AMBIENT_ZONES_GROUP = "ambient_zones";
    public static readonly string MUSIC_CUES_GROUP = "music_cues";
    public static readonly string CHAPTERS_GROUP = "chapters";
    public static readonly string ACTIONS_GROUP = "actions";
    public static readonly string FLOOR_DATA_GROUP = "floor_data";
    public static readonly string BUILDING_DATA_GROUP = "building_data";
    public static readonly string FURNITURE_GROUP = "furniture";
    public static readonly string INTERACTABLES_GROUP = "interactables";
    public static readonly string CLOTHES_GROUP = "clothes";
    public static readonly string LAYOUT_CONFIG_GROUP = "layout_config";
    public static readonly string ROOM_CONFIG_GROUP = "room_config";
    public static readonly string ROOM_PRESETS_GROUP = "room_type_presets";
    public static readonly string DOOR_PAIR_PRESETS_GROUP = "door_pair_presets";

    #endregion

    #region Fields and Properties

    private static AssetLoader instance = null;
    public static AssetLoader Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new AssetLoader();
            }
            return instance;
        }
    }

    private List<ScriptableObject> allPresets;
    private List<AmbientZone> allAmbientZones;
    private List<MusicCue> allMusicCues;
    private List<ChapterPreset> allChapters;
    private List<AIActionPreset> allActions;
    private List<TextAsset> allFloorData;
    private List<BuildingPreset> allBuildingData;
    private List<FurniturePreset> allFurniture;
    private List<InteractablePreset> allInteractables;
    private List<ClothesPreset> allClothes;

    private List<LayoutConfiguration> allLayoutConfigurations;
    private List<RoomConfiguration> allRoomConfigurations;
    private List<RoomTypePreset> allRoomTypePresets;
    private List<DoorPairPreset> allDoorPairPresets;

    #endregion

    #region Addressables Asset Loading

    private void SortScriptableObject(ScriptableObject scriptableObject)
    {
        if (scriptableObject is BuildingPreset)
        {
            allBuildingData.Add((BuildingPreset)scriptableObject);
        }
        else if (scriptableObject is ChapterPreset)
        {
            allChapters.Add((ChapterPreset)scriptableObject);
        }
        else if (scriptableObject is AIActionPreset)
        {
            allActions.Add((AIActionPreset)scriptableObject);
        }
        else if (scriptableObject is FurniturePreset)
        {
            allFurniture.Add((FurniturePreset)scriptableObject);
        }
        else if (scriptableObject is InteractablePreset)
        {
            allInteractables.Add((InteractablePreset)scriptableObject);
        }
        else if (scriptableObject is ClothesPreset)
        {
            allClothes.Add((ClothesPreset)scriptableObject);
        }
        else if (scriptableObject is AmbientZone)
        {
            allAmbientZones.Add((AmbientZone)scriptableObject);
        }
        else if (scriptableObject is MusicCue)
        {
            allMusicCues.Add((MusicCue)scriptableObject);
        }
    }

    private static float TimeDiff(float time)
    {
        return Time.time - time;
    }

    private static string TimeDiffStr(float time)
    {
        return TimeDiff(time).ToString();
    }

    public async Task PerformInitialLoadAsync()
    {
        allBuildingData = new List<BuildingPreset>();
        allFurniture = new List<FurniturePreset>();
        allInteractables = new List<InteractablePreset>();
        allClothes = new List<ClothesPreset>();
        allAmbientZones = new List<AmbientZone>();
        allMusicCues = new List<MusicCue>();
        allActions = new List<AIActionPreset>();
        allChapters = new List<ChapterPreset>();

        Debug.Log("Beginning load of group: " + DATA_GROUP);
        float time = Time.time;

        int originalVSync = QualitySettings.vSyncCount;

        Application.backgroundLoadingPriority = ThreadPriority.High;
        if (SteamManager.Initialized && Steamworks.SteamUtils.IsSteamRunningOnSteamDeck())
        {
            QualitySettings.vSyncCount = 1;
        }

        AsyncOperationHandle<IList<ScriptableObject>> asyncOperationHandleData = Addressables.LoadAssetsAsync<ScriptableObject>(DATA_GROUP, SortScriptableObject);
        await asyncOperationHandleData.Task;
        allPresets = (List<ScriptableObject>)asyncOperationHandleData.Result;

        Debug.Log(string.Format("Finished load of group: {0}, took {1} seconds", DATA_GROUP, TimeDiffStr(time)));

        Debug.Log("Beginning load of group: " + FLOOR_DATA_GROUP);
        time = Time.time;
        AsyncOperationHandle<IList<TextAsset>> asyncOperationHandleFloorData = Addressables.LoadAssetsAsync<TextAsset>(FLOOR_DATA_GROUP, null);
        await asyncOperationHandleFloorData.Task;
        allFloorData = (List<TextAsset>)asyncOperationHandleFloorData.Result;

        Debug.Log(string.Format("Finished load of group: {0}, took {1} seconds", FLOOR_DATA_GROUP, TimeDiffStr(time)));

        if (SteamManager.Initialized && Steamworks.SteamUtils.IsSteamRunningOnSteamDeck())
        {
            QualitySettings.vSyncCount = originalVSync;
        }
        Application.backgroundLoadingPriority = ThreadPriority.Normal;
    }

    // Synchronous fallbacks used for editor cases

    public List<ScriptableObject> GetAllPresets()
    {
        if (allPresets == null)
        {
            AsyncOperationHandle<IList<ScriptableObject>> asyncOperationHandle = Addressables.LoadAssetsAsync<ScriptableObject>(DATA_GROUP, null);
            allPresets = (List<ScriptableObject>)asyncOperationHandle.WaitForCompletion();
        }
        return allPresets;
    }

    public List<AmbientZone> GetAllAmbientZones()
    {
        if (allAmbientZones == null)
        {
            AsyncOperationHandle<IList<AmbientZone>> asyncOperationHandle = Addressables.LoadAssetsAsync<AmbientZone>(AMBIENT_ZONES_GROUP, null);
            allAmbientZones = (List<AmbientZone>)asyncOperationHandle.WaitForCompletion();
        }
        return allAmbientZones;
    }

    public List<MusicCue> GetAllMusicCues()
    {
        if (allMusicCues == null)
        {
            AsyncOperationHandle<IList<MusicCue>> asyncOperationHandle = Addressables.LoadAssetsAsync<MusicCue>(MUSIC_CUES_GROUP, null);
            allMusicCues = (List<MusicCue>)asyncOperationHandle.WaitForCompletion();
        }
        return allMusicCues;
    }

    public List<ChapterPreset> GetAllChapters()
    {
        if (allChapters == null)
        {
            AsyncOperationHandle<IList<ChapterPreset>> asyncOperationHandle = Addressables.LoadAssetsAsync<ChapterPreset>(CHAPTERS_GROUP, null);
            allChapters = (List<ChapterPreset>)asyncOperationHandle.WaitForCompletion();
        }
        return allChapters;
    }

    public List<AIActionPreset> GetAllActions()
    {
        if (allActions == null)
        {
            AsyncOperationHandle<IList<AIActionPreset>> asyncOperationHandle = Addressables.LoadAssetsAsync<AIActionPreset>(ACTIONS_GROUP, null);
            allActions = (List<AIActionPreset>)asyncOperationHandle.WaitForCompletion();
        }
        return allActions;
    }

    public List<TextAsset> GetAllFloorData()
    {
        if (allFloorData == null)
        {
            AsyncOperationHandle<IList<TextAsset>> asyncOperationHandle = Addressables.LoadAssetsAsync<TextAsset>(FLOOR_DATA_GROUP, null);
            allFloorData = (List<TextAsset>)asyncOperationHandle.WaitForCompletion();
        }
        return allFloorData;
    }

    public List<BuildingPreset> GetAllBuildingPresets()
    {
        if (allBuildingData == null)
        {
            AsyncOperationHandle<IList<BuildingPreset>> asyncOperationHandle = Addressables.LoadAssetsAsync<BuildingPreset>(BUILDING_DATA_GROUP, null);
            allBuildingData = (List<BuildingPreset>)asyncOperationHandle.WaitForCompletion();
        }
        return allBuildingData;
    }

    public List<FurniturePreset> GetAllFurniture()
    {
        if (allFurniture == null)
        {
            AsyncOperationHandle<IList<FurniturePreset>> asyncOperationHandle = Addressables.LoadAssetsAsync<FurniturePreset>(FURNITURE_GROUP, null);
            allFurniture = (List<FurniturePreset>)asyncOperationHandle.WaitForCompletion();
        }
        return allFurniture;
    }

    public List<InteractablePreset> GetAllInteractables()
    {
        if (allInteractables == null)
        {
            AsyncOperationHandle<IList<InteractablePreset>> asyncOperationHandle = Addressables.LoadAssetsAsync<InteractablePreset>(INTERACTABLES_GROUP, null);
            allInteractables = (List<InteractablePreset>)asyncOperationHandle.WaitForCompletion();
        }
        return allInteractables;
    }

    public List<ClothesPreset> GetAllClothes()
    {
        if (allClothes == null)
        {
            AsyncOperationHandle<IList<ClothesPreset>> asyncOperationHandle = Addressables.LoadAssetsAsync<ClothesPreset>(CLOTHES_GROUP, null);
            allClothes = (List<ClothesPreset>)asyncOperationHandle.WaitForCompletion();
        }
        return allClothes;
    }

    public List<LayoutConfiguration> GetAllLayoutConfigurations()
    {
        if (allLayoutConfigurations == null)
        {
            AsyncOperationHandle<IList<LayoutConfiguration>> asyncOperationHandle = Addressables.LoadAssetsAsync<LayoutConfiguration>(LAYOUT_CONFIG_GROUP, null);
            allLayoutConfigurations = (List<LayoutConfiguration>)asyncOperationHandle.WaitForCompletion();
        }
        return allLayoutConfigurations;
    }

    public List<RoomConfiguration> GetAllRoomConfigurations()
    {
        if (allRoomConfigurations == null)
        {
            AsyncOperationHandle<IList<RoomConfiguration>> asyncOperationHandle = Addressables.LoadAssetsAsync<RoomConfiguration>(ROOM_CONFIG_GROUP, null);
            allRoomConfigurations = (List<RoomConfiguration>)asyncOperationHandle.WaitForCompletion();
        }
        return allRoomConfigurations;
    }

    public List<RoomTypePreset> GetAllRoomTypePresets()
    {
        if (allRoomTypePresets == null)
        {
            AsyncOperationHandle<IList<RoomTypePreset>> asyncOperationHandle = Addressables.LoadAssetsAsync<RoomTypePreset>(ROOM_PRESETS_GROUP, null);
            allRoomTypePresets = (List<RoomTypePreset>)asyncOperationHandle.WaitForCompletion();
        }
        return allRoomTypePresets;
    }

    public List<DoorPairPreset> GetAllDoorPairPresets()
    {
        if (allDoorPairPresets == null)
        {
            AsyncOperationHandle<IList<DoorPairPreset>> asyncOperationHandle = Addressables.LoadAssetsAsync<DoorPairPreset>(DOOR_PAIR_PRESETS_GROUP, null);
            allDoorPairPresets = (List<DoorPairPreset>)asyncOperationHandle.WaitForCompletion();
        }
        return allDoorPairPresets;
    }

#endregion
}