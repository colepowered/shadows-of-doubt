﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

//Controls dragging of a case window
//Script pass 1
public class DragCasePanel : MonoBehaviour, IPointerDownHandler, IDragHandler
{
	//References
	private Vector2 pointerOffset;
	public RectTransform pinnedContainer;
	public RectTransform panelRect;
    public PinnedItemController itemController;

    public bool multipleParentInstances = false;
    private List<DragCasePanel> pinnedFiles = new List<DragCasePanel>();
    public List<Vector2> offsets = new List<Vector2>();

    //private bool controllerDrag = false;

    public void Setup(PinnedItemController newController)
    {
        itemController = newController;
        pinnedContainer = CasePanelController.Instance.pinnedContainer;
        panelRect = itemController.gameObject.GetComponent<RectTransform>();

        this.transform.SetAsLastSibling();
    }

	public void OnPointerDown(PointerEventData data)
	{
        //Set as last sibling
		panelRect.SetAsLastSibling();

        //Screenpoint of cursor on board
        if (InputController.Instance.mouseInputMode)
        {
            if(data.button != PointerEventData.InputButton.Left)
            {
                return;
            }

            RectTransformUtility.ScreenPointToLocalPointInRectangle(panelRect, data.position, data.pressEventCamera, out pointerOffset);
        }
        else pointerOffset = Vector2.zero;

        Game.Log("Interface: Drag Case Panel Pointer Down: " + panelRect.name + " offset = " + pointerOffset);

        //As we may need to move more than 1 if multiple items are selected, updated the offset for all selected
        foreach (PinnedItemController pbc in InterfaceController.Instance.selectedPinned)
        {
            if (pbc == itemController.pinButtonController) continue; //We don't need to do itself

            //Set as last sibling
            pbc.dragController.panelRect.SetAsLastSibling();

            //Screenpoint of cursor on board
            if (InputController.Instance.mouseInputMode)
            {
                RectTransformUtility.ScreenPointToLocalPointInRectangle(pbc.dragController.panelRect, data.position, data.pressEventCamera, out pbc.dragController.pointerOffset);
            }
            else pointerOffset = Vector2.zero;
        }
	}

	public void OnDrag(PointerEventData data)
	{
        if (data.button != PointerEventData.InputButton.Left)
        {
            return;
        }

        ForceDrag(data.position);

        //Game.Log("Interface: Force drag: " + data.position);

        foreach (PinnedItemController pbc in InterfaceController.Instance.selectedPinned)
        {
            if (pbc == itemController) continue; //We don't need to do itself

            //Set as last sibling
            pbc.dragController.ForceDrag(data.position);
        }
    }

    public void ForceDrag(Vector2 cursorPosition)
    {
        if (panelRect == null) return;

        SetPositionCursor(cursorPosition, pointerOffset);

        //Position files
        for (int i = 0; i < pinnedFiles.Count; i++)
        {
            DragCasePanel fileDrag = pinnedFiles[i];
            Vector2 offset = offsets[i];

            Vector2 relative = new Vector2(panelRect.localPosition.x - offset.x, panelRect.localPosition.y - offset.y);
            fileDrag.SetPositionDirect(relative);
        }
    }

    public void ForceDragController(Vector2 newLocalPosition)
    {
        if (panelRect == null) return;

        //Set as last sibling
        panelRect.SetAsLastSibling();

        SetPositionDirect(newLocalPosition);

        //Position files
        for (int i = 0; i < pinnedFiles.Count; i++)
        {
            DragCasePanel fileDrag = pinnedFiles[i];
            Vector2 offset = offsets[i];

            Vector2 relative = new Vector2(panelRect.localPosition.x - offset.x, panelRect.localPosition.y - offset.y);
            fileDrag.SetPositionDirect(relative);
        }
    }

    Vector2 ClampCursor (Vector2 rawPointerPosition)
	{
		Vector3[] canvasCorners = new Vector3[4];
		pinnedContainer.GetWorldCorners (canvasCorners);

		float clampedX = Mathf.Clamp (rawPointerPosition.x, canvasCorners[0].x, canvasCorners[2].x);
		float clampedY = Mathf.Clamp (rawPointerPosition.y, canvasCorners[0].y, canvasCorners[2].y);

		Vector2 newPointerPosition = new Vector2 (clampedX, clampedY);
		return newPointerPosition;
	}

	public void SetPositionCursor(Vector2 pointerPosition, Vector2 offset)
	{
		//Clamp
		pointerPosition = ClampCursor(pointerPosition);

		Vector2 localPointerPosition;

		if (RectTransformUtility.ScreenPointToLocalPointInRectangle (
			pinnedContainer, pointerPosition, null, out localPointerPosition
		))
		{
            SetPositionDirect(localPointerPosition - pointerOffset);
		}
	}

    public void SetPositionDirect(Vector2 localPosition)
    {
        panelRect.localPosition = ClampToCorkboard(localPosition);
        itemController.OnMoveThis();
    }

    Vector2 ClampToCorkboard(Vector2 original)
    {
        float clampedX = Mathf.Clamp(original.x, pinnedContainer.rect.xMin, pinnedContainer.rect.xMax);
        float clampedY = Mathf.Clamp(original.y, pinnedContainer.rect.yMin, pinnedContainer.rect.yMax);
        Vector2 ret = new Vector2(clampedX, clampedY);

        return ret;
    }

    Vector2 RadiusClamp(Vector2 original, Vector2 point, float radius)
    {
        Vector2 ret = original;

        if(Vector2.Distance(original, point) > radius)
        {
             ret = Vector2.MoveTowards(point, original, radius);
        }

        return ret;
    }
}