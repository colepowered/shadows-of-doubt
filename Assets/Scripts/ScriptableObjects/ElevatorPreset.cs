﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "elevator_data", menuName = "Database/Elevator Preset")]

public class ElevatorPreset : SoCustomComparison
{
    public List<GameObject> stairWellPrefabs;
    public List<GameObject> stairsPrefabs;
    public float rotationOffset;
    public Material bottomMaterial;
    public Material topMaterial;
}
