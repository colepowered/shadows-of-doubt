﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.IO;
using System.Linq;
using NaughtyAttributes;
using UnityEngine.Rendering;

//A scriptable object that can be used to make building presets.
//Script pass 1
[CreateAssetMenu(fileName = "building_data", menuName = "Database/Building Preset")]

public class BuildingPreset : SoCustomComparison
{
    public enum Density { low, medium, high, veryHigh };
    public enum LandValue { veryLow, low, medium, high, veryHigh };

    [System.Serializable]
    public class InteriorFloorSetting
    {
        [Tooltip("This setting will appear for x floors")]
        public int floorsWithThisSetting = 1;
        [Tooltip("Possible floor presets (choose at random)")]
        public List<TextAsset> blueprints = new List<TextAsset>();
        [Tooltip("How far air vents are allowed to extrude from the outer wall of the building (0 if none)")]
        public int airVentMaximumExtrusion = 0;
        [Tooltip("Possible floor variants featuring control rooms (choose at random)")]
        public List<TextAsset> controlRoomVariants = new List<TextAsset>();
        [Tooltip("When player is on this floor, force these model parents to be hidden")]
        public List<string> forceHideModels = new List<string>();
        [Tooltip("When the player is on this floor, in this specific room type, force this model parents to be hidden (overrides outside rooms)")]
        public List<ForceHideModelsForRoom> forceHideModelsInRooms = new List<ForceHideModelsForRoom>();
        [Tooltip("When the player is outside on this floor, force this model parents to be hidden")]
        public List<string> forceHideModelsOutside = new List<string>();

        public bool overrideCeilingHeight = false;
        public int newCeilingHeight = 51;
    }

    [System.Serializable]
    public class ForceHideModelsForRoom
    {
        public RoomConfiguration roomConfig;
        public List<string> forceHideModels = new List<string>();
    }

    public bool disable = false;

    [Header("Models")]
    [Tooltip("Reference to the building model prefab")]
    public GameObject prefab;
    [Tooltip("The emission texture used to light up windows on this model (unlit)")]
    public Texture2D emissionMapUnlit;
    [Tooltip("The emission texture used to light up windows on this model (lit)")]
    public Texture2D emissionMapLit;
    [Tooltip("The height of this building")]
    public float buildingHeight = 0f;
    [Tooltip("The local position of the lightning rod")]
    public Vector3 lightningRodLocalPos;
    [Tooltip("The material to use on default walls. Leave blank to use default (brick)")]
    public List<MaterialGroupPreset> defaultExteriorWallMaterial;
    [Tooltip("The material key to use on the exterior of the building")]
    public Toolbox.MaterialKey exteriorKey;
    [Tooltip("Check if this building supports alley blocks")]
    public bool enableAlleywayWalls = true;
    [Tooltip("Allow this building to feature quoins")]
    public bool enableExteriorQuoins = true;

    [Space(7)]
    public bool overrideEvidencePhotoSettings = false;
    [EnableIf("overrideEvidencePhotoSettings")]
    public Vector3 relativeCamPhotoPos = new Vector3(0, 2.25f, 23f);
    [EnableIf("overrideEvidencePhotoSettings")]
    public Vector3 relativeCamPhotoEuler = new Vector3(-45f, 180f, 0);

    [Header("Environment")]
    public bool overrideDistrictEnvironment = true;
    [EnableIf("overrideDistrictEnvironment")]
    public SessionData.SceneProfile sceneProfile = SessionData.SceneProfile.indoors;

    [Header("Special")]
    [Tooltip("The max amount of lost and found items to spawn at one time")]
    [Range(0, 5)]
    public int maxLostAndFound = 0;

    [Header("Blueprints")]
    [Tooltip("The layouts of above-ground floors, starting with ground floor")]
    public List<InteriorFloorSetting> floorLayouts = new List<InteriorFloorSetting>();
    [Tooltip("The layouts of below-ground floors, starting with basement level 1")]
    public List<InteriorFloorSetting> basementLayouts = new List<InteriorFloorSetting>();
    [Tooltip("How many control rooms should this building feature?")]
    public Vector2 controlRoomRange = Vector2.zero;
    public List<DesignStylePreset> forceBuildingDesignStyles = new List<DesignStylePreset>();
    public StairwellPreset stairwellRegular;
    public StairwellPreset stairwellLarge;

    [Space(7)]
    public bool overrideGrubiness = false;
    [EnableIf("overrideGrubiness")]
    public float grubinessOverride = 0;

    [Header("Zoning")]
    public ZoneType displayedZone = ZoneType.privateProperty;
    public enum ZoneType { residential, commercial, industrial, municipal, publicProperty, privateProperty };
    public bool allowedInAllDistricts = true;
    [DisableIf("allowedInAllDistricts")]
    [Tooltip("Appears in this district")]
    public List<DistrictPreset> allowedInDistricts = new List<DistrictPreset>();
    [Tooltip("Appears in density range: Not required but choices will be weighted towards this")]
    public Density densityMinimum = Density.low;
    public Density densityMaximum = Density.veryHigh;
    [Tooltip("Appears in land value range: Not required but choices will be weighted towards this")]
    public LandValue landValueMinimum = LandValue.veryLow;
    public LandValue landValueMaximum = LandValue.veryHigh;

    [Space(7)]
    [Tooltip("Try and make sure the city has at least this many buildings of this type")]
    public int minimum = 1;
    [Tooltip("How important is it that the city features the above minimum amount of buildings?")]
    [Range(0, 10)]
    public int featureImportance = 4;
    [Tooltip("Hard limit on the number of buildings per city")]
    public int hardLimit = 99;
    [Tooltip("Desired ratio on the number of these buildings (1 means the whole city can be these)")]
    [Range(0f, 1f)]
    public float desiredRatio = 0.05f;
    [Tooltip("Modernity: Used to choose decor- how modern the building is")]
    [Range(0, 10)]
    public int modernity = 5;
    [Tooltip("Used in choosing decor: The lobby area room type")]
    public AddressPreset lobbyPreset;
    [Tooltip("True if this is supposed to not have floors")]
    public bool nonEnterable = false;
    [Tooltip("True if this is a boundary piece")]
    public bool boundary = false;
    [Tooltip("True if this is a boundary corner piece")]
    public bool boundaryCorner = false;

    [Header("Naming")]
    public bool overrideNaming = false;
    [EnableIf("overrideNaming")]
    public List<string> possibleNames = new List<string>();

    [Header("Map")]
    public bool customDrawOnMap = false;
    public Texture2D tex;

    [Header("Window Mapping")]
    [Tooltip("The mesh to use to find the window coordinates, cable coordinates")]
    public Mesh captureMesh;
    [Tooltip("A map with white blocks mapping the window areas: IMPORTANT: Make sure texture image compression is off & read/write is on.")]
    public Texture2D windowMap;
    [Tooltip("A map with red pixels for cable connections and green for external signage: IMPORTANT: Make sure texture image compression is off & read/write is on.")]
    public Texture2D addonMap;
    public List<WindowUVFloor> sortedWindows = new List<WindowUVFloor>();
    public int floorCount = 1;
    public float meshHeight;

    [Header("Building Addon Points")]
    public List<CableLinkPoint> cableLinkPoints = new List<CableLinkPoint>();
    public AnimationCurve cableSpawnChanceOverHeight = AnimationCurve.Linear(0f, 1f, 1f, 0.4f);
    public List<CableLinkPoint> sideSignPoints = new List<CableLinkPoint>();

    [Header("Signage")]
    public List<GameObject> possibleNeonSigns = new List<GameObject>();
    public Vector2 signsPerBuildingRange = new Vector2(0, 3);
    [Tooltip("Offset for horizontal lettering signs")]
    public Vector3 horizontalSignOffset = new Vector3(0f, 4.046f, 0f);

    [System.Serializable]
    public struct CableLinkPoint
    {
        public Vector3 localPos;
        public Vector3 localRot;
    }

    [Header("Smokestacks")]
    public bool featuresSmokestack = false;
    [EnableIf("featuresSmokestack")]
    [Tooltip("Interval in gametime")]
    public Vector2 spawnInterval = new Vector2(0.1f, 0.33f);
    [EnableIf("featuresSmokestack")]
    public GameObject spritePrefab;
    [EnableIf("featuresSmokestack")]
    public Vector3 spawnOffset;

    [System.Serializable]
    public class WindowUVFloor
    {
        public List<WindowUVBlock> front = new List<WindowUVBlock>();
        public List<WindowUVBlock> back = new List<WindowUVBlock>();
        public List<WindowUVBlock> left = new List<WindowUVBlock>();
        public List<WindowUVBlock> right = new List<WindowUVBlock>();
    }

    [System.Serializable]
    public class WindowUVBlock
    {
        public Vector2 originPixel;
        public Vector2 rectSize;
        public Vector2 centrePixel;

        //Calculate left and right mesh positions in order to easily tell the plane the window is on...
        public Vector3 localMeshPositionLeft;
        public Vector3 localMeshPositionRight;

        [Space(7)]
        public int floor = 0; //Position from the bottom
        public Vector2 side;
        //public int side; //Offset of the side: 1= front, 2=back, 3=left, 4=right
        public int horizonal = 0; //Position from the left edge
    }

    //4 Directions array: CityData is not initialised at edit time, so can't use that
    private Vector2[] offsetArrayX4 =
    {
        new Vector2 (0, -1),
        new Vector2 (-1, 0),
        new Vector2 (1, 0),
        new Vector2 (0, 1),
    };

    [Button]
    public void GenerateWindowData()
    {
        sortedWindows.Clear();
        sortedWindows.TrimExcess();
        List<WindowUVBlock> windowBlocks = new List<WindowUVBlock>();
        List<Vector2> closedSet = new List<Vector2>();
        int attempts = 0;
        floorCount = 0;

        //Find the scale of the capture mesh from comparing those in the prefab
        Transform[] children = prefab.transform.GetComponentsInChildren<Transform>(true);
        Vector3 localScale = Vector3.one;
        Vector3 localRot = Vector2.zero;
        Vector3 localPos = Vector3.zero;

        //Convert the local point to that of the base model...
        Matrix4x4 m = Matrix4x4.TRS(localPos, Quaternion.Euler(localRot), localScale);

        foreach (Transform t in children)
        {
            MeshFilter f = t.gameObject.GetComponent<MeshFilter>();

            if (f != null)
            {
                if (f.sharedMesh == captureMesh)
                {
                    Debug.Log("Capture mesh found in prefab! Local scale is " + t.localScale.x + ", " + t.localScale.y + ", " + t.localScale.z + ", rotation: " + t.localEulerAngles + ",  pos: " + t.localPosition);
                    localScale = t.localScale;
                    localRot = t.localEulerAngles;
                    localPos = t.localPosition;

                    m = Matrix4x4.TRS(localPos, Quaternion.Euler(localRot), localScale);

                    break;
                }
            }
        }

        //Scan for white blocks in the window map
        for (int i = 0; i < windowMap.width; i++)
        {
            for (int u = 0; u < windowMap.height; u++)
            {
                Vector2 pixelCoord = new Vector2(i, u);

                if (closedSet.Contains(pixelCoord)) continue; //If this is already assigned to a block, skip it

                Color pixelColour = windowMap.GetPixel(i, u);

                if (pixelColour == Color.white)
                {
                    attempts++;
                    //if (attempts > 1) return;

                    WindowUVBlock newBlockData = new WindowUVBlock();
                    newBlockData.originPixel = pixelCoord;
                    newBlockData.rectSize = Vector2.one;

                    //Flood fill to get this block
                    List<Vector2> openSet = new List<Vector2>();
                    List<Vector2> thisBlock = new List<Vector2>();
                    openSet.Add(pixelCoord);

                    while (openSet.Count > 0)
                    {
                        Vector2 currentPixel = openSet[0];
                        thisBlock.Add(currentPixel);
                        closedSet.Add(currentPixel);

                        newBlockData.rectSize.x = Mathf.Max(newBlockData.rectSize.x, currentPixel.x - newBlockData.originPixel.x + 1);
                        newBlockData.rectSize.y = Mathf.Max(newBlockData.rectSize.y, currentPixel.y - newBlockData.originPixel.y + 1);

                        foreach (Vector2 v2 in offsetArrayX4)
                        {
                            Vector2 foundPixel = new Vector2(currentPixel.x + v2.x, currentPixel.y + v2.y);

                            if (foundPixel.x < 0 || foundPixel.x >= windowMap.width) continue;
                            if (foundPixel.y < 0 || foundPixel.y >= windowMap.height) continue;

                            if (closedSet.Contains(foundPixel)) continue;
                            if (openSet.Contains(foundPixel)) continue;
                            if (thisBlock.Contains(foundPixel)) continue;

                            Color foundPixelColour = windowMap.GetPixel((int)foundPixel.x, (int)foundPixel.y);

                            if (foundPixelColour == Color.white)
                            {
                                openSet.Add(foundPixel);
                            }
                            else closedSet.Add(foundPixel);
                        }

                        openSet.RemoveAt(0);
                    }

                    //You should now have a complete block
                    newBlockData.centrePixel = new Vector2(newBlockData.originPixel.x + Mathf.Floor(newBlockData.rectSize.x * 0.5f), newBlockData.originPixel.y + Mathf.Floor(newBlockData.rectSize.y * 0.5f));

                    //Get the local mesh postion of the centre...
                    //Uses a scale of 0-1, so divide the coordinate by width/height
                    Vector2 normalizedLeft = new Vector2(newBlockData.centrePixel.x / (float)windowMap.width, newBlockData.centrePixel.y / (float)windowMap.height);
                    Vector2 normalizedRight = new Vector2((newBlockData.centrePixel.x + 1) / (float)windowMap.width, newBlockData.centrePixel.y / (float)windowMap.height);

                    //Apply model rotation
                    newBlockData.localMeshPositionLeft = m.MultiplyPoint3x4(UvTo3D(normalizedLeft));
                    newBlockData.localMeshPositionRight = m.MultiplyPoint3x4(UvTo3D(normalizedRight));

                    //If either of the above are not found, skip this window
                    if (newBlockData.localMeshPositionLeft == Vector3.zero || newBlockData.localMeshPositionRight == Vector3.zero)
                    {
                        Game.Log("Unable to find local mesh position at " + normalizedLeft + " - " + normalizedRight);
                        continue;
                    }

                    windowBlocks.Add(newBlockData);
                }
                else continue;
            }
        }

        Debug.Log("Found " + windowBlocks.Count + " windows...");

        //Calculate window positions relative to each other
        //Seperate into floors first
        Dictionary<int, List<WindowUVBlock>> seperatedByFloors = new Dictionary<int, List<WindowUVBlock>>();
        windowBlocks.Sort((p1, p2) => p1.localMeshPositionLeft.y.CompareTo(p2.localMeshPositionLeft.y)); //Sort by local Y coordinate

        //The first entry must represent the first floor
        int floorCursor = 1;
        float yPos = windowBlocks[0].localMeshPositionLeft.y;
        seperatedByFloors.Add(floorCursor, new List<WindowUVBlock>());
        seperatedByFloors[floorCursor].Add(windowBlocks[0]);
        windowBlocks[0].floor = floorCursor;

        for (int i = 1; i < windowBlocks.Count; i++)
        {
            WindowUVBlock thisBlock = windowBlocks[i];

            //Compare to the previously assigned YPosition: If within a certain threshold, this window belongs to this floor. If not then it must be a new one...
            float yDiff = Mathf.Abs(thisBlock.localMeshPositionLeft.y - yPos);

            if (yDiff > 1f)
            {
                //This is a new floor
                floorCursor++;
                floorCount = floorCursor; //The total number of floors
            }

            if (!seperatedByFloors.ContainsKey(floorCursor))
            {
                seperatedByFloors.Add(floorCursor, new List<WindowUVBlock>());
            }

            seperatedByFloors[floorCursor].Add(thisBlock);
            thisBlock.floor = floorCursor;

            yPos = thisBlock.localMeshPositionLeft.y; //Record the Y postion, so the next entry can be compared to this...
        }

        Debug.Log("Floor count according to found windows: " + floorCursor + " (+ ground)");

        //Seperate into sides: Compare the left and right local positions. If their Z positions are the same they are on the front or back (-X is front, + is back).
        //If their X positions are the same they are on the sid (-Z is right, + is left)
        for (int i = 0; i < windowBlocks.Count; i++)
        {
            WindowUVBlock thisBlock = windowBlocks[i];

            if (Mathf.Round(thisBlock.localMeshPositionLeft.x * 100f) / 100f == Mathf.Round(thisBlock.localMeshPositionRight.x * 100f) / 100f)
            {
                if (thisBlock.localMeshPositionRight.x < 0)
                {
                    thisBlock.side = new Vector2(-1, 0);
                }
                else if (thisBlock.localMeshPositionRight.x >= 0)
                {
                    thisBlock.side = new Vector2(1, 0);
                }
            }
            else if (Mathf.Round(thisBlock.localMeshPositionLeft.z * 100f) / 100f == Mathf.Round(thisBlock.localMeshPositionRight.z * 100f) / 100f)
            {
                if (thisBlock.localMeshPositionRight.z < 0)
                {
                    thisBlock.side = new Vector2(0, -1);
                }
                else if (thisBlock.localMeshPositionRight.z >= 0)
                {
                    thisBlock.side = new Vector2(0, 1);
                }
            }
        }

        //Merge duplicate windows: For when the window UV might be split in two. We should have enough data to do this properly...
        for (int i = 0; i < windowBlocks.Count; i++)
        {
            WindowUVBlock thisBlock = windowBlocks[i];

            //Find elements of the same side and floor with matching X and Z elements
            List<WindowUVBlock> matching = windowBlocks.FindAll(item => item.floor == thisBlock.floor && item.side == thisBlock.side &&
            Mathf.Round(item.localMeshPositionLeft.x * 100f) / 100f == Mathf.Round(thisBlock.localMeshPositionLeft.x * 100f) / 100f
            && Mathf.Round(item.localMeshPositionLeft.z * 100f) / 100f == Mathf.Round(thisBlock.localMeshPositionLeft.z * 100f) / 100f
            && item != thisBlock);

            //Merge the two elements
            if (matching.Count > 0)
            {
                for (int u = 0; u < matching.Count; u++)
                {
                    WindowUVBlock merge = matching[u];

                    //Origin pixel is the min
                    thisBlock.originPixel = new Vector2(Math.Min(thisBlock.originPixel.x, merge.originPixel.x), Math.Min(thisBlock.originPixel.y, merge.originPixel.y));

                    //Get the merge max pixel
                    Vector2 mergeMax = new Vector2(merge.originPixel.x + merge.rectSize.x, merge.originPixel.y + merge.rectSize.y);

                    thisBlock.rectSize = new Vector2(Mathf.Max(thisBlock.rectSize.x, mergeMax.x - merge.originPixel.x), Mathf.Max(thisBlock.rectSize.y, mergeMax.y - merge.originPixel.y));

                    //Remove matching
                    seperatedByFloors[merge.floor].Remove(merge);
                    windowBlocks.Remove(merge);
                }

                //You should now have a complete block
                thisBlock.centrePixel = new Vector2(thisBlock.originPixel.x + Mathf.Floor(thisBlock.rectSize.x * 0.5f), thisBlock.originPixel.y + Mathf.Floor(thisBlock.rectSize.y * 0.5f));
            }
        }

        //Sort by left to right for all sides...
        for (int i = 1; i <= floorCount; i++)
        {
            List<WindowUVBlock> thisFloor = seperatedByFloors[i];

            //Create new floor
            WindowUVFloor newFloor = new WindowUVFloor();

            //Order Front
            newFloor.front = thisFloor.FindAll(item => item.side == new Vector2(0, 1));
            newFloor.front.Sort((p1, p2) => p1.localMeshPositionLeft.x.CompareTo(p2.localMeshPositionLeft.x)); //Lowest
            newFloor.front.Reverse();

            //Save index in list as horizontal value
            for (int u = 0; u < newFloor.front.Count; u++)
            {
                newFloor.front[u].horizonal = u;
            }

            //Order Back
            newFloor.back = thisFloor.FindAll(item => item.side == new Vector2(0, -1));
            newFloor.back.Sort((p1, p2) => p1.localMeshPositionLeft.x.CompareTo(p2.localMeshPositionLeft.x)); //Lowest

            //Save index in list as horizontal value
            for (int u = 0; u < newFloor.back.Count; u++)
            {
                newFloor.back[u].horizonal = u;
            }

            //Order Left
            newFloor.left = thisFloor.FindAll(item => item.side == new Vector2(-1, 0));
            newFloor.left.Sort((p1, p2) => p1.localMeshPositionLeft.z.CompareTo(p2.localMeshPositionLeft.z)); //Lowest
            newFloor.left.Reverse();

            //Save index in list as horizontal value
            for (int u = 0; u < newFloor.left.Count; u++)
            {
                newFloor.left[u].horizonal = u;
            }

            //Order Right
            newFloor.right = thisFloor.FindAll(item => item.side == new Vector2(1, 0));
            newFloor.right.Sort((p1, p2) => p1.localMeshPositionLeft.z.CompareTo(p2.localMeshPositionLeft.z)); //Lowest

            //Save index in list as horizontal value
            for (int u = 0; u < newFloor.right.Count; u++)
            {
                newFloor.right[u].horizonal = u;
            }

            sortedWindows.Add(newFloor);
        }

        //Force save data
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(this);
#endif
    }

    [Button]
    public void GenerateAddonData()
    {
        cableLinkPoints = new List<CableLinkPoint>();
        sideSignPoints = new List<CableLinkPoint>();

        //Find the scale of the capture mesh from comparing those in the prefab
        Transform[] children = prefab.transform.GetComponentsInChildren<Transform>(true);
        Vector3 localScale = Vector3.one;
        Vector3 localRot = Vector2.zero;
        Vector3 localPos = Vector3.zero;

        //Convert the local point to that of the base model...
        Matrix4x4 m = Matrix4x4.TRS(localPos, Quaternion.Euler(localRot), localScale);

        foreach (Transform t in children)
        {
            MeshFilter f = t.gameObject.GetComponent<MeshFilter>();

            if (f != null)
            {
                if (f.sharedMesh == captureMesh)
                {
                    Debug.Log("Capture mesh found in prefab! Local scale is " + t.localScale.x + ", " + t.localScale.y + ", " + t.localScale.z + ", rotation: " + t.localEulerAngles + ",  pos: " + t.localPosition);
                    localScale = t.localScale;
                    localRot = t.localEulerAngles;
                    localPos = t.localPosition;

                    //Convert the local point to that of the base model...
                    m = Matrix4x4.TRS(localPos, Quaternion.Euler(localRot), localScale);

                    break;
                }
            }
        }

        //Scan for white blocks in the window map
        for (int i = 0; i < addonMap.width; i++)
        {
            for (int u = 0; u < addonMap.height; u++)
            {
                Vector2 pixelCoord = new Vector2(i, u);
                Color pixelColour = addonMap.GetPixel(i, u);

                if (pixelColour == Color.red || pixelColour == Color.green)
                {
                    CableLinkPoint newPoint = new CableLinkPoint();

                    Vector3 p = UvTo3D(new Vector2(pixelCoord.x / (float)addonMap.width, pixelCoord.y / (float)addonMap.height));

                    //Get the local mesh postion of the centre...
                    //Uses a scale of 0-1, so divide the coordinate by width/height
                    Vector2 normalizedLeft = new Vector2(pixelCoord.x / (float)addonMap.width, pixelCoord.y / (float)addonMap.height);
                    Vector2 normalizedRight = new Vector2((pixelCoord.x + 1) / (float)addonMap.width, pixelCoord.y / (float)addonMap.height);

                    Vector3 locLeft = UvTo3D(normalizedLeft);
                    Vector3 locRight = UvTo3D(normalizedRight);

                    if (Mathf.Round(locLeft.x * 100f) / 100f == Mathf.Round(locRight.x * 100f) / 100f)
                    {
                        //Front
                        if (locRight.x < 0)
                        {
                            newPoint.localRot = new Vector3(0, 270, 0);
                        }
                        //Back
                        else if (locRight.x >= 0)
                        {
                            newPoint.localRot = new Vector3(0, 90, 0);
                        }
                    }
                    else if (Mathf.Round(locLeft.z * 100f) / 100f == Mathf.Round(locRight.z * 100f) / 100f)
                    {
                        //Right
                        if (locRight.z < 0)
                        {
                            newPoint.localRot = new Vector3(0, 180, 0);
                        }
                        //Left
                        else if (locRight.z >= 0)
                        {
                            newPoint.localRot = new Vector3(0, 0, 0);
                        }
                    }

                    //Apply model rotation
                    newPoint.localPos = m.MultiplyPoint3x4(p);
                    newPoint.localRot = newPoint.localRot + localRot;

                    if (pixelColour == Color.red) cableLinkPoints.Add(newPoint);
                    else if (pixelColour == Color.green) sideSignPoints.Add(newPoint);
                }
                else continue;
            }
        }

        Debug.Log("Found " + cableLinkPoints.Count + " cable link points, " + sideSignPoints.Count + " side sign points.");

        //Force save data
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(this);
#endif
    }

    //Calculate height of all colliders in the prefab
    [Button]
    public void CalculateMeshHeight()
    {
        var total = new Bounds(prefab.transform.position, Vector3.zero);

        foreach (var child in prefab.transform.GetComponentsInChildren<Collider>())
        {
            total.Encapsulate(child.bounds);
        }

        meshHeight = total.size.y;

        //Force save data
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(this);
#endif
    }

    //Get local mesh coordinates of this UV coordinate
    public Vector3 UvTo3D(Vector2 uv)
    {
        if (captureMesh == null || !captureMesh.isReadable)
        {
            Game.LogError("Mesh is not readable!");
            return Vector3.zero;
        }

        //Mesh mesh = instancedObject.GetComponent<Mesh>();
        int[] tris = captureMesh.triangles;
        Vector2[] uvs = captureMesh.uv;
        Vector3[] verts = captureMesh.vertices;

        for (int i = 0; i < tris.Length; i += 3)
        {
            Vector2 u1 = uvs[tris[i]]; // get the triangle UVs
            Vector2 u2 = uvs[tris[i + 1]];
            Vector2 u3 = uvs[tris[i + 2]];

            // calculate triangle area - if zero, skip it
            float a = Area(u1, u2, u3);
            if (a == 0) continue;

            // calculate barycentric coordinates of u1, u2 and u3
            // if anyone is negative, point is outside the triangle: skip it
            float a1 = Area(u2, u3, uv) / a;
            if (a1 < 0) continue;

            float a2 = Area(u3, u1, uv) / a;
            if (a2 < 0) continue;

            float a3 = Area(u1, u2, uv) / a;
            if (a3 < 0) continue;

            // point inside the triangle - find mesh position by interpolation...
            Vector3 p3D = a1 * verts[tris[i]] + a2 * verts[tris[i + 1]] + a3 * verts[tris[i + 2]];

            //Return as local coordinates
            return p3D;
        }

        //point outside any uv triangle: return Vector3.zero
        return Vector3.zero;
    }

    //calculate signed triangle area using a kind of "2D cross product":
    public float Area(Vector2 p1, Vector2 p2, Vector2 p3)
    {
        Vector2 v1 = p1 - p3;
        Vector2 v2 = p2 - p3;
        return (v1.x * v2.y - v1.y * v2.x) / 2;
    }

    public InteriorFloorSetting GetFloorSetting(int floor, int index)
    {
        if (floor >= 0)
        {
            return floorLayouts[index];
        }
        else
        {
            return basementLayouts[index];
        }
    }
}
