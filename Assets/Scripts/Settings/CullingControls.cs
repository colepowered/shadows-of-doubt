using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class CullingControls : MonoBehaviour
{
    [Header("FoVs")]
    [Tooltip("The angle which you can see other buildings")]
    public float visibleBuildingFoV = 91f;
    [Tooltip("The angle which you can see other rooms from entrances")]
    public float visibleRoomFoV = 91f;

    [Header("Distances")]
    public float fromOutsideToInsideDistanceMax = 25;
    public float fromInsideToInsideDistanceMax = 35;
    [Space(7)]
    public float fromOutsideToOutsideDistanceMax = 75;
    public float fromInsideToOutsideDistanceMax = 75;
    [Space(7)]
    [Tooltip("Distance within which rooms are drawn through windows")]
    public float windowCullingRange = 25f;
    [Tooltip("Distance within which rooms are drawn through open doors")]
    public float doorCullingRange = 25f;
    [Tooltip("Distance within which exterior air ducts are rendered")]
    public float exteriorDuctCullingRange = 35f;
    [Tooltip("Distance within which connected rooms are drawn when inside a duct")]
    public float ductRoomCullingRange = 20f;

    [Header("Air Ducts")]
    public float airDuctLODThreshold = 0.3f;

    //Singleton pattern
    private static CullingControls _instance;
    public static CullingControls Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
}
