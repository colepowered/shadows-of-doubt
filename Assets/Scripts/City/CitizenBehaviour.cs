﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;
using UnityStandardAssets.Characters.FirstPerson;
using System.Linq;

//This scripts starts/stops the citizens throughout their daily routines
//Script pass 1
public class CitizenBehaviour : MonoBehaviour
{
    //New AI System
    public List<NewAIController> veryLowTickRate = new List<NewAIController>();
    public List<NewAIController> lowTickRate = new List<NewAIController>();
    public List<NewAIController> mediumTickRate = new List<NewAIController>();
    public List<NewAIController> highTickRate = new List<NewAIController>();
    public List<NewAIController> veryHighTickRate = new List<NewAIController>();
    [System.NonSerialized]
    public List<NewAIController> updateList = new List<NewAIController>();
    public int tickCounter = 1;
    public int AITicksPerFrame = 20;
    public int executionsThisFrame = 0;
    public int aiTickBacklog = 0;
    public int visibleHumans = 0;
    public int frequentTickCounter = 1;

    private int dynamicShadowUpdateCounter = 0;
    private List<LightController> lightUpdateQueue = new List<LightController>();

    public HashSet<Actor> actorsInStealthMode = new HashSet<Actor>();

    private float passiveIncomeTimer = 0f;

    //Process routines per fixed update
    public float currentCursorGameTime = 0f;
    public int maxRoutineInitsPerFrame = 30;
    public int lastUpdateInitCount = 0;

    public bool initialPositioning = false;

    public float triggerHeadache = 0f; //Trigger a headache when drunkness disappears

    //Interior lights
    public List<NewBuilding> buildingEmissionTexturesToUpdate = new List<NewBuilding>();

    private List<ResidenceController> residenceLightPool = new List<ResidenceController>();
    private List<NewBuilding> buildingPool = new List<NewBuilding>();
    //private bool interiorLightSwitchingHour = false;

    //Exterior lights
    private List<Interactable> streetLightPool = new List<Interactable>();

    public float timeOnLastGameWorldUpdate = 0f;

    [Header("Smokestacks")]
    public List<Smokestack> smokestacks = new List<Smokestack>();

    [System.Serializable]
    public class Smokestack
    {
        public NewBuilding building;
        public float timer = 0f;
    }

    [Header("Scene Captures")]
    public List<SceneRecorder> sceneRecorders = new List<SceneRecorder>();

    //Temporary boosted escalation rooms
    public List<NewGameLocation> tempEscalationBoost = new List<NewGameLocation>();

    //Events
    public delegate void GameWorldLoop();
    public event GameWorldLoop OnGameWorldLoop;

    //Singleton pattern
    private static CitizenBehaviour _instance;
	public static CitizenBehaviour Instance { get { return _instance; } }

    //Reusable collections
    private List<Interactable> toRemove = new List<Interactable>();

    void Awake ()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
	}

	public void StartGame()
	{
        //Calling this after the game has started will start the repeating routines
        GameSpeedChange();
    }

    public void GameSpeedChange()
    {
        //Only start if the game is started
        if (SessionData.Instance.startedGame)
        {
            //Stop routines (this will cancel all invokes on this script)
            CancelInvoke();

            //Only start rotuines if in play mode
            if (SessionData.Instance.play)
            {
                timeOnLastGameWorldUpdate = SessionData.Instance.gameTime;

                if(!SessionData.Instance.isFloorEdit)
                {
                    InvokeRepeating("RoutineCheck", 0f, GameplayControls.Instance.routineUpdateFrequency / SessionData.Instance.currentTimeMultiplier);
                    InvokeRepeating("LightLevelLoop", 0f, GameplayControls.Instance.stealthModeLoopUpdateFrequency / SessionData.Instance.currentTimeMultiplier);
                }

                InvokeRepeating("GameWorldCheck", 0f, GameplayControls.Instance.gameWorldUpdateFrequency / SessionData.Instance.currentTimeMultiplier);
            }
        }
    }

    //Check routine
	public void RoutineCheck()
	{
        //New AI System
        //Very high tick rate: Check every time this is executed
        for (int i = 0; i < veryHighTickRate.Count; i++)
        {
            if (!veryHighTickRate[i].dueUpdate)
            {
                updateList.Insert(0, veryHighTickRate[i]); //These AI get priority
                veryHighTickRate[i].dueUpdate = true;
            }
        }

        if (tickCounter % 5 == 0)
        {
            for (int i = 0; i < highTickRate.Count; i++)
            {
                if (!highTickRate[i].dueUpdate)
                {
                    updateList.Add(highTickRate[i]);
                    highTickRate[i].dueUpdate = true;
                }
            }
        }

        if (tickCounter % 10 == 0)
        {
            for (int i = 0; i < mediumTickRate.Count; i++)
            {
                if (!mediumTickRate[i].dueUpdate)
                {
                    updateList.Add(mediumTickRate[i]);
                    mediumTickRate[i].dueUpdate = true;
                }
            }
        }

        if (tickCounter % 15 == 0)
        {
            for (int i = 0; i < lowTickRate.Count; i++)
            {
                if (!lowTickRate[i].dueUpdate)
                {
                    updateList.Add(lowTickRate[i]);
                    lowTickRate[i].dueUpdate = true;
                }
            }
        }

        if (tickCounter % 20 == 0)
        {
            for (int i = 0; i < veryLowTickRate.Count; i++)
            {
                if (!veryLowTickRate[i].dueUpdate)
                {
                    updateList.Add(veryLowTickRate[i]);
                    veryLowTickRate[i].dueUpdate = true;
                }
            }
        }

        tickCounter++;
        if (tickCounter > 20) tickCounter = 1;


		//int inits = 0;
  //      int readIndex = 0;

		//while(globalCitizenRoutineList.Count > 0 && globalCitizenRoutineList[readIndex].triggerTime <= SessionData.Instance.gameTime)
		//{
		//	//Send to citizen to execute this routine action
		//	Actor citScript = globalCitizenRoutineList[readIndex].actor;

		//	//If no delay is placed on citizen (then don't remove order)
		//	//if(!citizenDelays.ContainsKey(citScript))
		//	//{
		//		////If not dead!
		//		//if(!citScript.isDead)
		//		//{
		//			citScript.ai.ExecuteAction(globalCitizenRoutineList[readIndex], false);
		//			inits++;
		//		//}

  //              //Update the last successfully executed routine
  //              currentCursorGameTime = globalCitizenRoutineList[readIndex].triggerTime;

		//		//Remove routine from list when processed.
		//		globalCitizenRoutineList.RemoveAt(readIndex);

  //              //Reset read index to 0
  //              readIndex = 0;
		//	//}
  // //         //If this contains a delay...
		//	//else
		//	//{
  // //             //Try next read index
  // //             readIndex++;
  // //         }
  //      }

  //      //Update delays
  //      UpdateCitizenDelayList();

  //      lastUpdateInitCount = inits;
	}

    private void Update()
    {
        if(SessionData.Instance.play)
        {
            //Perform AI update
            executionsThisFrame = 0;

            while (updateList.Count > 0 && executionsThisFrame < AITicksPerFrame)
            {
                if(updateList[0] != null)
                {
                    updateList[0].AITick(); //Execute AI tick
                }

                updateList.RemoveAt(0);
                executionsThisFrame++;
            }

            aiTickBacklog = updateList.Count;

            //Update 1 building's texture per frame
            if(buildingEmissionTexturesToUpdate.Count > 0)
            {
                buildingEmissionTexturesToUpdate[0].emissionTextureInstanced.Apply();
                buildingEmissionTexturesToUpdate.RemoveAt(0);
            }

            //Burning barrels (pass wind param)
            for (int l = 0; l < GameplayController.Instance.burningBarrels.Count; l++)
            {
                Interactable i = GameplayController.Instance.burningBarrels[l];

                if (i == null)
                {
                    continue;
                }

                //Must be on and spawned
                if(i.sw0)
                {
                    if(i.controller != null)
                    {
                        foreach(AudioController.LoopingSoundInfo loop in i.loopingAudio)
                        {
                            if(loop != null)
                            {
                                loop.audioEvent.setParameterByName("Wind", SessionData.Instance.currentWind);
                            }
                        }
                    }
                }
            }

            //Switch resetting
            if(GameplayController.Instance.switchRessetingObjects.Count > 0)
            {
                toRemove.Clear();

                foreach(KeyValuePair<Interactable, float> pair in GameplayController.Instance.switchRessetingObjects)
                {
                    if(SessionData.Instance.gameTime >= pair.Value + pair.Key.preset.resetTimer)
                    {
                        pair.Key.ResetToDefaultSwitchState();
                        toRemove.Add(pair.Key);
                    }
                }

                if(toRemove.Count > 0)
                {
                    foreach(Interactable i in toRemove)
                    {
                        if(i != null)
                        {
                            GameplayController.Instance.switchRessetingObjects.Remove(i);
                        }
                    }
                }
            }

            //Handle active grenades
            for (int i = 0; i < GameplayController.Instance.activeGrenades.Count; i++)
            {
                Interactable g = GameplayController.Instance.activeGrenades[i];

                if (g.val > 0f)
                {
                    g.SetValue(g.val - Time.deltaTime);

                    //Play beep
                    //Use recent call check as this will never be used for a grenade
                    if (g.recentCallCheck <= 0f)
                    {
                        g.UpdateWorldPositionAndNode(true);
                        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.grenadeBeep, null, g.node, g.wPos, interactable: g);
                        g.recentCallCheck = g.val * 0.2f;
                    }
                    else g.recentCallCheck -= Time.deltaTime;
                }
                else
                {
                    Toolbox.Instance.ExplodeGrenade(g);
                    i--;
                }
            }

            //Active gadgets
            for (int l = 0; l < GameplayController.Instance.activeGadgets.Count; l++)
            {
                Interactable i = GameplayController.Instance.activeGadgets[l];
                if (i == null) continue;

                //Count codebreaker...
                if (i.preset.specialCaseFlag == InteractablePreset.SpecialCase.codebreaker)
                {
                    //Check code...
                    Interactable lockRef = i.objectRef as Interactable;

                    List<int> password = new List<int>();
                    ComputerLogin login = null;
                    string strP = string.Empty;
                    float rightMP = 0;

                    if (lockRef.preset.isComputer)
                    {
                        rightMP = -1;
                        login = lockRef.controller.computer.GetComponentInChildren<ComputerLogin>();
                        if(login != null && login.loginSelection.selected != null) password = login.loginSelection.selected.option.human.passcode.GetDigits();
                    }
                    else
                    {
                        password = lockRef.GetPasswordFromSource(out _);
                    }

                    int intPassword = 0;

                    if (password.Count >= 4)
                    {
                        strP = password[0].ToString() + password[1].ToString() + password[2].ToString() + password[3].ToString();
                        int.TryParse(strP, out intPassword);
                    }

                    for (int t = 0; t < 8; t++)
                    {
                        //Parse digits
                        int roundedAttempt = Mathf.RoundToInt(i.cs);

                        if (i.cs < 10000)
                        {
                            //Correct guess!
                            if (roundedAttempt >= intPassword && password.Count >= 4)
                            {
                                if (lockRef.preset.isComputer)
                                {
                                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.codebreakerSuccess, null, i.node, i.GetWorldPosition(), i);
                                    GameplayController.Instance.AddPasscode(login.loginSelection.selected.option.human.passcode, true);
                                    login.OnInputCode(password, 0f);
                                }
                                else
                                {
                                    //Unarm the lock here so it plays the sfx
                                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.codebreakerSuccess, null, i.node, i.GetWorldPosition(), i);
                                    lockRef.SetLockedState(false, Player.Instance);
                                    GameplayController.Instance.AddPasscode(lockRef.GetPasswordSource(), true);
                                }

                                i.SetSwitchState(false, null, forceUpdate: true); //This will make sure the player cannot pick this back up
                                i.UpdateCurrentActions();
                                InteractionController.Instance.UpdateInteractionText();

                                //Start timer to self destruction
                                i.cs = 10000;

                                //Send on crack animation
                                if (i.controller != null)
                                {
                                    i.controller.transform.GetComponentInChildren<ActiveCodebreakerController>().OnCrack(strP);
                                }
                            }
                            else
                            {
                                //* = guesses per second
                                i.cs += Mathf.Min(Time.deltaTime * 120f, 1f); //Make sure to not add more than one or you could skip the correct password!
                            }
                        }
                        else
                        {
                            i.cs += Time.deltaTime;

                            if (i.cs > 10008)
                            {
                                //Create physics used codebreaker
                                if (i.controller != null)
                                {
                                    Interactable usedCodebreaker = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.codebreakerUsed, Player.Instance, null, null, i.wPos, i.wEuler, null, null);
                                    usedCodebreaker.ForcePhysicsActive(false, true, (i.controller.transform.up * 0.5f) + (i.controller.transform.right * rightMP));
                                }

                                i.Delete();

                                break;
                            }
                        }
                    }
                }
            }

            //Dynamic light update
            //if(dynamicShadowUpdateCounter <= 0)
            //{
            //    for (int i = 0; i < CityData.Instance.dynamicShadowSystemLights.Count; i++)
            //    {
            //        LightController lc = CityData.Instance.dynamicShadowSystemLights[i];

            //        if (lc == null)
            //        {
            //            CityData.Instance.dynamicShadowSystemLights.RemoveAt(i);
            //            i--;
            //            continue;
            //        }
            //        else if(lc.hdrpLightData != null && lc.useShadows)
            //        {
            //            //Draw shadows
            //            lc.hdrpLightData.RequestShadowMapRendering();
            //        }
            //    }

            //    dynamicShadowUpdateCounter = Game.Instance.dynamicShadowUpdateFrames;
            //}
            //else
            //{
            //    dynamicShadowUpdateCounter--;
            //}

            //Update a maximum light count per frame
            if(lightUpdateQueue.Count <= 0)
            {
                for (int i = 0; i < CityData.Instance.dynamicShadowSystemLights.Count; i++)
                {
                    LightController lc = CityData.Instance.dynamicShadowSystemLights[i];

                    if (lc == null)
                    {
                        CityData.Instance.dynamicShadowSystemLights.RemoveAt(i);
                        i--;
                        continue;
                    }
                    else if (lc.hdrpLightData != null && lc.isOn && lc.useShadows && lc.lightComponent.enabled)
                    {
                        if(Vector3.Distance(CameraController.Instance.cam.transform.position, lc.lightComponent.transform.position) <= (lc.preset.shadowFadeDistance * Game.Instance.shadowFadeDistanceMultiplier))
                        {
                            lightUpdateQueue.Add(lc);
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < Game.Instance.maxUpdateDynamicShadowsPerFrame; i++)
                {
                    if (lightUpdateQueue.Count <= 0) break;

                    LightController lc = lightUpdateQueue[0];

                    if (lc.hdrpLightData != null && lc.useShadows && lc.isOn && lc.lightComponent.enabled)
                    {
                        //Draw shadows
                        lc.hdrpLightData.RequestShadowMapRendering();
                    }

                    lightUpdateQueue.RemoveAt(0);
                }
            }

        }
    }

    private void LateUpdate()
    {
        //Handle citizen frequent updates
        for (int i = 0; i < CityData.Instance.citizenDirectory.Count; i++)
        {
            Citizen cit = CityData.Instance.citizenDirectory[i];

            if (cit.ai == null) continue;
            if (!cit.ai.enabled) continue;

            if(cit.ai.tickRate == NewAIController.AITickRate.veryHigh || cit.visible)
            {
                cit.ai.FrequentUpdate();
            }
            else if(cit.ai.tickRate == NewAIController.AITickRate.high)
            {
                if (tickCounter % 2 == 0)
                {
                    cit.ai.FrequentUpdate();
                }
            }
            else if(cit.ai.tickRate == NewAIController.AITickRate.medium)
            {
                if (tickCounter % 4 == 0)
                {
                    cit.ai.FrequentUpdate();
                }
            }
            else if(cit.ai.tickRate == NewAIController.AITickRate.low)
            {
                if (tickCounter % 6 == 0)
                {
                    cit.ai.FrequentUpdate();
                }
            }
            else if(cit.ai.tickRate == NewAIController.AITickRate.veryLow)
            {
                if (tickCounter % 8 == 0)
                {
                    cit.ai.FrequentUpdate();
                }
            }

            frequentTickCounter++;
            if (frequentTickCounter > 8) frequentTickCounter = 1;
        }
    }

    //Check misc/game world
    void GameWorldCheck()
    {
        //Fire event
        if(OnGameWorldLoop != null)
        {
            OnGameWorldLoop();
        }

        GameplayController.Instance.UpdateConversationDelays();

        //Check for required scene captures
        int captures = 0;

        //Rank scene records by last capture last

        for (int i = 0; i < sceneRecorders.Count; i++)
        {
            if(captures >= GameplayControls.Instance.maxCapturesPerFrame)
            {
                sceneRecorders.Sort((p1, p2) => p1.lastCaptureAt.CompareTo(p2.lastCaptureAt)); //Rank so oldest scene capture is first
                break;
            }
            else if (sceneRecorders[i] != null && SessionData.Instance.gameTime >= sceneRecorders[i].lastCaptureAt + GameplayControls.Instance.captureInterval)
            {
                sceneRecorders[i].ExecuteCapture(true, false);
                captures++;
            }
        }

        //Perform open/close check on companies
        for (int i = 0; i < CityData.Instance.companyDirectory.Count; i++)
        {
            if(CityData.Instance.companyDirectory[i] != null)
            {
                CityData.Instance.companyDirectory[i].OpenCloseCheck();
            }
        }

        //Add contamination to evidence
        float timePassed = SessionData.Instance.gameTime - timeOnLastGameWorldUpdate;

        //Reset security
        for (int i = 0; i < GameplayController.Instance.turnedOffSecurity.Count; i++)
        {
            Interactable sec = GameplayController.Instance.turnedOffSecurity[i];
            if (sec == null) continue;

            float strength = sec.GetSecurityStrength();
            sec.SetValue(sec.val + ((timePassed / GameplayControls.Instance.securityResetTime) * strength));

            if(sec.val >= strength)
            {
                sec.val = strength;

                if(sec.node != null && (sec.node.gameLocation.thisAsAddress == null || (sec.node.gameLocation.thisAsAddress.GetBreakerSecurity() != null && sec.node.gameLocation.thisAsAddress.GetBreakerSecurity().sw0)))
                {
                    sec.SetSwitchState(true, null);
                }
            }
        }

        //Update alarms
        for (int i = 0; i < GameplayController.Instance.activeAlarmsBuildings.Count; i++)
        {
            NewBuilding b = GameplayController.Instance.activeAlarmsBuildings[i];
            if (b == null) continue;

            //Fill rooms with gas
            foreach (Interactable gas in b.otherSecurity)
            {
                if (gas == null) continue;

                if (gas.preset.specialCaseFlag == InteractablePreset.SpecialCase.gasReleaseSystem)
                {
                    //Allow an hour cooldown before releasing the next lot of gas
                    if(SessionData.Instance.gameTime > gas.node.room.lastRoomGassed + 0.75f)
                    {
                        if (gas.sw0)
                        {
                            Dictionary<NewRoom, float> gasSourceOpenSet = new Dictionary<NewRoom, float>();
                            HashSet<NewRoom> closedSet = new HashSet<NewRoom>();
                            gasSourceOpenSet.Add(gas.node.room, 1f);
                            int safety = 32;

                            while (gasSourceOpenSet.Count > 0 && safety > 0)
                            {
                                NewRoom currentRoom = null;
                                float currentGas = -1f;

                                //Get room with highest gas level
                                foreach (KeyValuePair<NewRoom, float> pair in gasSourceOpenSet)
                                {
                                    if (pair.Value > currentGas)
                                    {
                                        currentRoom = pair.Key;
                                        currentGas = pair.Value;
                                    }
                                }

                                if (currentRoom == null) break;

                                currentRoom.AddGas((timePassed / GameplayControls.Instance.gasFillTime) * currentGas);

                                //Scan entrances...
                                foreach (NewNode.NodeAccess acc in currentRoom.entrances)
                                {
                                    if (!acc.walkingAccess) continue;
                                    NewRoom otherRoom = acc.GetOtherRoom(currentRoom);
                                    if (closedSet.Contains(otherRoom)) continue;
                                    if (otherRoom.IsOutside()) continue;

                                    if (acc.accessType == NewNode.NodeAccess.AccessType.adjacent || acc.accessType == NewNode.NodeAccess.AccessType.bannister || acc.accessType == NewNode.NodeAccess.AccessType.verticalSpace || acc.accessType == NewNode.NodeAccess.AccessType.openDoorway)
                                    {
                                        float passGas = currentGas - 0.4f;

                                        if (!gasSourceOpenSet.ContainsKey(otherRoom))
                                        {
                                            gasSourceOpenSet.Add(otherRoom, passGas);
                                        }
                                        else gasSourceOpenSet[otherRoom] += passGas;
                                    }
                                    else if (acc.accessType == NewNode.NodeAccess.AccessType.door)
                                    {
                                        float passGas = currentGas - 0.6f;

                                        if (!gasSourceOpenSet.ContainsKey(otherRoom))
                                        {
                                            gasSourceOpenSet.Add(otherRoom, passGas);
                                        }
                                        else gasSourceOpenSet[otherRoom] += passGas;
                                    }
                                }

                                closedSet.Add(currentRoom);
                                gasSourceOpenSet.Remove(currentRoom);

                                safety--;
                            }
                        }
                    }
                }
            }

            //Only decrease alarm timer if cameras can't see anything wrong
            bool camPass = true;

            foreach(Interactable c in b.securityCameras)
            {
                if(c != null && c.controller != null && c.controller.securitySystem != null)
                {
                    if (c.controller.securitySystem.seenIllegalThisCheck.Count > 0)
                    {
                        camPass = false;
                        break;
                    }
                }
            }

            if(camPass)
            {
                b.alarmTimer -= timePassed;
                if (b.alarmTimer <= 0f) b.SetAlarm(false, null, null);
            }
        }

        for (int i = 0; i < GameplayController.Instance.activeAlarmsLocations.Count; i++)
        {
            NewAddress b = GameplayController.Instance.activeAlarmsLocations[i];
            if (b == null) continue;

            //Fill rooms with gas
            foreach(Interactable gas in b.otherSecurity)
            {
                if (gas == null) continue;

                if(gas.preset.specialCaseFlag == InteractablePreset.SpecialCase.gasReleaseSystem)
                {
                    Game.Log("Gas release system found");

                    if(gas.sw0)
                    {
                        Dictionary<NewRoom, float> gasSourceOpenSet = new Dictionary<NewRoom, float>();
                        HashSet<NewRoom> closedSet = new HashSet<NewRoom>();
                        gasSourceOpenSet.Add(gas.node.room, 1f);
                        int safety = 32;

                        while(gasSourceOpenSet.Count > 0 && safety > 0)
                        {
                            NewRoom currentRoom = null;
                            float currentGas = -1f;

                            //Get room with highest gas level
                            foreach(KeyValuePair<NewRoom, float> pair in gasSourceOpenSet)
                            {
                                if(pair.Value > currentGas)
                                {
                                    currentRoom = pair.Key;
                                    currentGas = pair.Value;
                                }
                            }

                            if (currentRoom == null) break;

                            currentRoom.AddGas((timePassed / GameplayControls.Instance.gasFillTime) * currentGas);

                            //Scan entrances...
                            foreach(NewNode.NodeAccess acc in currentRoom.entrances)
                            {
                                if (!acc.walkingAccess) continue;
                                NewRoom otherRoom = acc.GetOtherRoom(currentRoom);
                                if (closedSet.Contains(otherRoom)) continue;
                                if (otherRoom.IsOutside()) continue;

                                if (acc.accessType == NewNode.NodeAccess.AccessType.adjacent || acc.accessType == NewNode.NodeAccess.AccessType.bannister || acc.accessType == NewNode.NodeAccess.AccessType.verticalSpace || acc.accessType == NewNode.NodeAccess.AccessType.openDoorway)
                                {
                                    float passGas = currentGas - 0.4f;

                                    if (!gasSourceOpenSet.ContainsKey(otherRoom))
                                    {
                                        gasSourceOpenSet.Add(otherRoom, passGas);
                                    }
                                    else gasSourceOpenSet[otherRoom] += passGas;
                                }
                                else if(acc.accessType == NewNode.NodeAccess.AccessType.door)
                                {
                                    float passGas = currentGas - 0.6f;

                                    if (!gasSourceOpenSet.ContainsKey(otherRoom))
                                    {
                                        gasSourceOpenSet.Add(otherRoom, passGas);
                                    }
                                    else gasSourceOpenSet[otherRoom] += passGas;
                                }
                            }

                            closedSet.Add(currentRoom);
                            gasSourceOpenSet.Remove(currentRoom);

                            safety--;
                        }
                    }
                }
            }

            //Only decrease alarm timer if cameras can't see anything wrong
            bool camPass = true;

            foreach (Interactable c in b.securityCameras)
            {
                if (c != null && c.controller != null && c.controller.securitySystem != null && c.sw0)
                {
                    if (c.controller.securitySystem.seenIllegalThisCheck.Count > 0)
                    {
                        camPass = false;
                        break;
                    }
                }
            }

            if (camPass)
            {
                b.alarmTimer -= timePassed;
                if (b.alarmTimer <= 0f) b.SetAlarm(false, null);
            }
        }

        //Empty gas
        for (int i = 0; i < GameplayController.Instance.gasRooms.Count; i++)
        {
            NewRoom r = GameplayController.Instance.gasRooms[i];
            if (r == null) continue;
            r.AddGas(timePassed / -GameplayControls.Instance.gasEmptyTime);
        }

        for (int i = 0; i < GameplayController.Instance.alteredSecurityTargetsLocations.Count; i++)
        {
            NewAddress b = GameplayController.Instance.alteredSecurityTargetsLocations[i];
            if (b == null) continue;

            if(SessionData.Instance.gameTime >= b.targetModeSetAt + GameplayControls.Instance.securityResetTime)
            {
                b.SetTargetMode(NewBuilding.AlarmTargetMode.illegalActivities, false);
                GameplayController.Instance.alteredSecurityTargetsLocations.RemoveAt(i);
                i--;
            }
        }

        for (int i = 0; i < GameplayController.Instance.alteredSecurityTargetsBuildings.Count; i++)
        {
            NewBuilding b = GameplayController.Instance.alteredSecurityTargetsBuildings[i];
            if (b == null) continue;

            if (SessionData.Instance.gameTime >= b.targetModeSetAt + GameplayControls.Instance.securityResetTime)
            {
                b.SetTargetMode(NewBuilding.AlarmTargetMode.illegalActivities, false);
                GameplayController.Instance.alteredSecurityTargetsBuildings.RemoveAt(i);
                i--;
            }
        }

        //Handle proxy detonators
        foreach (KeyValuePair<Interactable, float> pair in GameplayController.Instance.proxyTrackers)
        {
            //Check current room and adjacent rooms for humans...
            List<NewRoom> roomCheck = new List<NewRoom>();
            roomCheck.Add(pair.Key.node.room);
            roomCheck.AddRange(pair.Key.node.room.adjacentRooms);

            bool detonating = false;

            foreach(NewRoom r in roomCheck)
            {
                foreach (Actor a in r.currentOccupants)
                {
                    Human h = a as Human;

                    if(h != null)
                    {
                        float distance = Vector3.Distance(h.transform.position, pair.Key.wPos);

                        if(distance < pair.Value)
                        {
                            //Can we draw a straight line to the person?
                            if(DataRaycastController.Instance.NodeRaycast(pair.Key.node, h.currentNode, out _))
                            {
                                detonating = true;
                                break;
                            }
                        }
                    }
                }
            }

            if(detonating)
            {
                if(!pair.Key.sw2)
                {
                    pair.Key.SetCustomState2(true, null, forceUpdate: true); //Set armed
                }
            }
            else
            {
                if (pair.Key.sw2)
                {
                    pair.Key.SetCustomState2(false, null, forceUpdate: true); //Reset
                }
            }
        }

        //Check for expired fingerprints
        List<Interactable> printsInactive = new List<Interactable>();

        foreach (Interactable i in GameplayController.Instance.objectsWithDynamicPrints)
        {
            if (i == null) continue;

            //If this object is visible then don't remove the fingerprint...
            if (i.controller != null) continue;

            for (int u = 0; u < i.df.Count; u++)
            {
                Interactable.DynamicFingerprint p = i.df[u];

                if(p.life == Interactable.PrintLife.timed)
                {
                    if (SessionData.Instance.gameTime > p.created + GameplayControls.Instance.fingerprintLife)
                    {
                        i.RemoveDynamicPrint(p);
                    }
                }
            }

            if(i.df.Count <= 0)
            {
                printsInactive.Add(i);
            }
        }

        //Remove objects with no prints left...
        while(printsInactive.Count > 0)
        {
            GameplayController.Instance.objectsWithDynamicPrints.Remove(printsInactive[0]);
            printsInactive.RemoveAt(0);
        }

        //Update smokestack triggers
        for (int i = 0; i < smokestacks.Count; i++)
        {
            Smokestack st = smokestacks[i];
            st.timer -= timePassed; //Count down

            if (st.timer <= 0f)
            {
                //Trigger spawn animation if active or player is inside
                if(st.building.displayBuildingModel || Player.Instance.currentBuilding == st.building)
                {
                    GameObject newSprite = Instantiate(st.building.preset.spritePrefab, st.building.buildingModelBase.transform);
                    newSprite.transform.localPosition = st.building.preset.spawnOffset;
                }

                //Set timer
                st.timer = Toolbox.Instance.Rand(st.building.preset.spawnInterval.x, st.building.preset.spawnInterval.y);

                if (AudioDebugging.Instance.overrideSmokeStackEmissionFrequency && Game.Instance.devMode)
                {
                    st.timer = AudioDebugging.Instance.chemSmokeStackEmissionFrequency / 60f;
                }
            }
        }

        //Do spatter removal check
        for (int i = 0; i < GameplayController.Instance.spatter.Count; i++)
        {
            SpatterSimulation sp = GameplayController.Instance.spatter[i];

            if (sp.eraseMode == SpatterSimulation.EraseMode.neverOrManual) continue;
            else if (sp.eraseMode == SpatterSimulation.EraseMode.useDespawnTime)
            {
                //Erase after time whether it's seen or not
                float timeActive = SessionData.Instance.gameTime - sp.createdAt;

                if (timeActive >= GameplayControls.Instance.spatterRemovalTime)
                {
                    sp.Remove();
                    i--;
                    continue;
                }
            }
            else if (sp.eraseMode == SpatterSimulation.EraseMode.useDespawnTimeOnceExecuted)
            {
                if (sp.isExecuted)
                {
                    float timeActive = SessionData.Instance.gameTime - sp.executedAt;

                    if (timeActive >= GameplayControls.Instance.spatterRemovalTime)
                    {
                        sp.Remove();
                        i--;
                        continue;
                    }
                }
            }
            else if (sp.eraseMode == SpatterSimulation.EraseMode.onceExecutedAndOutOfAddressPlusDespawnTime)
            {
                if (sp.isExecuted)
                {
                    if(sp.room != null && Player.Instance.currentGameLocation != sp.room.gameLocation)
                    {
                        //Create timestamp
                        if(sp.eraseModeTimeStamp < 0f)
                        {
                            sp.eraseModeTimeStamp = SessionData.Instance.gameTime;
                        }
                        else
                        {
                            float timeActive = SessionData.Instance.gameTime - sp.eraseModeTimeStamp;

                            if (timeActive >= GameplayControls.Instance.spatterRemovalTime)
                            {
                                sp.Remove();
                                i--;
                                continue;
                            }
                        }
                    }
                    else
                    {
                        //Create timestamp
                        if (sp.eraseModeTimeStamp < 0f)
                        {
                            sp.eraseModeTimeStamp = SessionData.Instance.gameTime;
                        }
                        else
                        {
                            float timeActive = SessionData.Instance.gameTime - sp.eraseModeTimeStamp;

                            if (timeActive >= GameplayControls.Instance.spatterRemovalTime)
                            {
                                sp.Remove();
                                i--;
                                continue;
                            }
                        }
                    }
                }
            }
            else if (sp.eraseMode == SpatterSimulation.EraseMode.onceExecutedAndOutOfBuildingPlusDespawnTime)
            {
                if (sp.isExecuted)
                {
                    if (sp.room != null && Player.Instance.currentBuilding != sp.room.gameLocation.building)
                    {
                        //Create timestamp
                        if (sp.eraseModeTimeStamp < 0f)
                        {
                            sp.eraseModeTimeStamp = SessionData.Instance.gameTime;
                        }
                        else
                        {
                            float timeActive = SessionData.Instance.gameTime - sp.eraseModeTimeStamp;

                            if (timeActive >= GameplayControls.Instance.spatterRemovalTime)
                            {
                                sp.Remove();
                                i--;
                                continue;
                            }
                        }
                    }
                    else
                    {
                        //Create timestamp
                        if (sp.eraseModeTimeStamp < 0f)
                        {
                            sp.eraseModeTimeStamp = SessionData.Instance.gameTime;
                        }
                        else
                        {
                            float timeActive = SessionData.Instance.gameTime - sp.eraseModeTimeStamp;

                            if (timeActive >= GameplayControls.Instance.spatterRemovalTime)
                            {
                                sp.Remove();
                                i--;
                                continue;
                            }
                        }
                    }
                }
            }
        }

        //Do interactable reset check
        for (int i = 0; i < GameplayController.Instance.interactablesMoved.Count; i++)
        {
            Interactable sp = GameplayController.Instance.interactablesMoved[i];

            //Don't do this for stuff in the player's apartment
            if(sp.node.gameLocation == Player.Instance.home)
            {
                GameplayController.Instance.interactablesMoved.RemoveAt(i);
                i--;
                continue;
            }

            if(!sp.spR)
            {
                GameplayController.Instance.interactablesMoved.RemoveAt(i);
                i--;
                continue;
            }

            if(InteractionController.Instance.carryingObject == null || InteractionController.Instance.carryingObject.interactable != sp)
            {
                //Conditions for reset: Must have time elapsed, and not be in gamelocation of either current position or spawn position
                if(sp.node == null || Player.Instance.currentGameLocation != sp.node.gameLocation)
                {
                    if (sp.spawnNode == null || Player.Instance.currentGameLocation != sp.spawnNode.gameLocation)
                    {
                        if (SessionData.Instance.gameTime >= sp.lma + GameplayControls.Instance.objectPositionResetTime)
                        {
                            //Must not be in inventory
                            if(sp.inInventory == null)
                            {
                                //Must not be drawn
                                if(sp.controller == null || sp.node == null || !sp.node.room.isVisible)
                                {
                                    if(sp.preset.GetPhysicsProfile().removeOnReset)
                                    {
                                        sp.SafeDelete(false);
                                    }
                                    else
                                    {
                                        //GameplayController.Instance.interactablesMoved.RemoveAt(i); //sp.remove will also remove this
                                        sp.originalPosition = false;
                                        sp.SetOriginalPosition(true);
                                    }

                                    i--; //The above will remove from this list so skip back one.
                                }
                            }
                        }
                    }
                }
            }
        }

        //Temp escalation removal check
        for (int i = 0; i < tempEscalationBoost.Count; i++)
        {
            NewGameLocation tempEsc = tempEscalationBoost[i];

            //Remove escalation if not trespassing
            List<Actor> toRemove = new List<Actor>();
            List<Actor> toRemoveAll = new List<Actor>();

            foreach (KeyValuePair<Actor, NewGameLocation.TrespassEscalation> pair in tempEsc.escalation)
            {
                bool allowEnforcersEverywhere = false;
                if (pair.Key.ai != null && pair.Key.ai.currentGoal != null) allowEnforcersEverywhere = pair.Key.ai.currentGoal.preset.allowEnforcersEverywhere;

                pair.Key.UpdateTrespassing(allowEnforcersEverywhere);

                if(pair.Key.currentGameLocation != tempEsc)
                {
                    toRemove.Add(pair.Key);
                }
                else if(!pair.Key.IsTrespassing(pair.Key.currentRoom, out _, out _, allowEnforcersEverywhere))
                {
                    toRemove.Add(pair.Key);
                }
            }

            foreach(Actor a in toRemove)
            {
                tempEsc.RemoveEscalation(a);
            }

            foreach (Actor a in toRemoveAll)
            {
                tempEsc.RemoveEscalation(a, true);
            }
        }

        //AI investigates smelly player
        if(Player.Instance.hygiene < 0.99f)
        {
            if(Player.Instance.isTrespassing)
            {
                if(Player.Instance.currentRoom != null)
                {
                    foreach(Actor a in Player.Instance.currentRoom.currentOccupants)
                    {
                        if (a == null) continue;
                        if (a.isDead || a.isAsleep || a.isStunned) continue;

                        if(a.ai != null && !a.ai.investigationGoal.isActive)
                        {
                            if(Toolbox.Instance.Rand(0f, 1f) >= Player.Instance.hygiene)
                            {
                                a.speechController.TriggerBark(SpeechController.Bark.stench);
                                a.ai.Investigate(Player.Instance.currentNode, Player.Instance.transform.position, Player.Instance, NewAIController.ReactionState.investigatingSound, 0.25f, 0);
                            }
                        }
                    }
                }
            }
        }

        //Remove guest passes
        if(GameplayController.Instance.guestPasses.Count > 0)
        {
            List<NewAddress> toRemove = new List<NewAddress>();

            foreach(KeyValuePair<NewAddress, Vector2> pair in GameplayController.Instance.guestPasses)
            {
                if(SessionData.Instance.gameTime > pair.Value.x)
                {
                    toRemove.Add(pair.Key);
                }
            }

            foreach(NewAddress loc in toRemove)
            {
                GameplayController.Instance.guestPasses.Remove(loc);

                //Trigger room change to update state
                if(Player.Instance.currentRoom.gameLocation == loc)
                {
                    Player.Instance.OnRoomChange();
                }
            }
        }

        //Process submitted cases
        if(GameplayController.Instance.caseProcessing.Count > 0)
        {
            List<Case> toRemove = new List<Case>();

            foreach(KeyValuePair<Case, float> pair in GameplayController.Instance.caseProcessing)
            {
                if(pair.Key.caseType == Case.CaseType.murder || pair.Key.caseType == Case.CaseType.mainStory)
                {
                    //If suspect arrested @ location, then make sure they are removed from the world for the processing time...
                    Case.ResolveQuestion identifiedQuestion = pair.Key.resolveQuestions.Find(item => item.tag == JobPreset.JobTag.A);
                    Case.ResolveQuestion detainQuestion = pair.Key.resolveQuestions.Find(item => item.tag == JobPreset.JobTag.B);

                    if(detainQuestion != null && identifiedQuestion != null && detainQuestion.isValid && detainQuestion.inputType == Case.InputType.location)
                    {
                        Game.Log("Checking for detained suspect " + identifiedQuestion.input+ " at " + detainQuestion.input + "...");
                        List<NewGameLocation> theseLocations = CityData.Instance.gameLocationDirectory.FindAll(item => item.name.ToLower() == detainQuestion.input.ToLower());

                        foreach(NewGameLocation gl in theseLocations)
                        {
                            Game.Log("...Scanning occupants of " + gl.name + " ("+gl.currentOccupants.Count +") ...");

                            foreach (Actor a in gl.currentOccupants)
                            {
                                if (a.isPlayer) continue;
                                if (a.isDead) continue;

                                Citizen c = a as Citizen;
                                if (c == null) continue;

                                if (a.ai == null) continue;

                                //Is anybody here arrested that matches the name given?
                                if(a.ai.restrained && c.GetCitizenName().ToLower() == identifiedQuestion.input.ToLower())
                                {
                                    Game.Log("...Submit " + c.GetCitizenName() + " for suspect processing...");

                                    if(!pair.Key.suspectsDetained.Contains(c.humanID))
                                    {
                                        pair.Key.suspectsDetained.Add(c.humanID);
                                    }

                                    //Remove from world for now
                                    c.RemoveFromWorld(true);
                                }
                            }
                        }
                    }
                }

                //Find the waiting objective so we can pass the progress...
                Objective waitObj = pair.Key.currentActiveObjectives.Find(item => item.queueElement.entryRef == "case processing");

                float completeTime = pair.Value + GameplayControls.Instance.caseResultProcessTime;
                float progress = Mathf.Clamp01((SessionData.Instance.gameTime - pair.Value) / (completeTime - pair.Value));

                if (waitObj != null)
                {
                    waitObj.SetProgress(progress);
                    //Game.Log("Case processing progress: " + waitObj.progress);
                }

                if(progress >= 1f)
                {
                    pair.Key.Resolve();
                    if(waitObj != null) waitObj.Complete();
                    toRemove.Add(pair.Key);
                }
            }

            //Remove keys
            foreach(Case c in toRemove)
            {
                GameplayController.Instance.caseProcessing.Remove(c);
            }
        }

        //Remove un-needed time evidence
        for (int i = 0; i < GameplayController.Instance.timeEvidence.Count; i++)
        {
            EvidenceTime t = GameplayController.Instance.timeEvidence[i];

            bool needed = false;

            foreach(Case c in CasePanelController.Instance.activeCases)
            {
                if(c.caseElements.Exists(item => item.id == t.evID))
                {
                    needed = true;
                    break;
                }
            }

            if (needed) continue;

            foreach (Case c in CasePanelController.Instance.archivedCases)
            {
                if (c.caseElements.Exists(item => item.id == t.evID))
                {
                    needed = true;
                    break;
                }
            }

            if (needed) continue;

            if(GameplayController.Instance.history.Exists(item => item.evID == t.evID))
            {
                needed = true;
                continue;
            }

            if(InterfaceController.Instance.activeWindows.Exists(item => item.passedEvidence == t))
            {
                needed = true;
                continue;
            }

            if(!needed)
            {
                GameplayController.Instance.evidenceDictionary.Remove(t.evID);
                GameplayController.Instance.timeEvidence.RemoveAt(i);
                i--;
            }
        }

        //Process late loan payments
        for (int i = 0; i < GameplayController.Instance.debt.Count; i++)
        {
            GameplayController.LoanDebt debt = GameplayController.Instance.debt[i];

            //Payment is officially late!
            if(SessionData.Instance.gameTime > debt.nextPaymentDueBy && SessionData.Instance.gameTime > debt.dueCheck)
            {
                if(debt.debt > 0)
                {
                    debt.missedPayments += Mathf.Min(debt.payments, debt.debt);
                    Game.Log("Gameplay: Added a missed payment of " + Mathf.Min(debt.payments, debt.debt));

                    //Calculate next payment due
                    float dueInHours = 24 - SessionData.Instance.decimalClock; //Remaining hours of today
                    dueInHours += 24.01f; //Payment due by end of tomorrow
                    debt.dueCheck = SessionData.Instance.gameTime += dueInHours;
                }
            }
        }

        //Add/Remove police tape when not drawn
        foreach(NewGameLocation crimeScene in GameplayController.Instance.crimeScenes)
        {
            if(crimeScene.thisAsAddress != null)
            {
                //Needs police tape...
                foreach(NewNode.NodeAccess acc in crimeScene.entrances)
                {
                    if(acc.walkingAccess && acc.door != null)
                    {
                        //No tape yet
                        if(!acc.door.forbiddenForPublic)
                        {
                            //Both sides of this door must be invisible
                            if(!CityData.Instance.visibleRooms.Contains(acc.door.wall.node.room))
                            {
                                if (!CityData.Instance.visibleRooms.Contains(acc.door.wall.otherWall.node.room))
                                {
                                    acc.door.SetForbidden(true); //Spawn tape
                                }
                            }
                        }
                    }
                }
            }
        }

        //Remove tape when not crime scene
        for (int i = 0; i < GameplayController.Instance.policeTapeDoors.Count; i++)
        {
            NewDoor door = GameplayController.Instance.policeTapeDoors[i];

            if (!GameplayController.Instance.crimeScenes.Contains(door.wall.node.gameLocation))
            {
                if (!GameplayController.Instance.crimeScenes.Contains(door.wall.otherWall.node.gameLocation))
                {
                    if(ChapterController.Instance != null && ChapterController.Instance.chapterScript != null && !Game.Instance.sandboxMode)
                    {
                        ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                        if(intro != null)
                        {
                            if (intro.apartment == door.wall.node.gameLocation || intro.apartment == door.wall.otherWall.node.gameLocation) continue;
                        }
                    }

                    door.SetForbidden(false); //Spawn tape
                }
            }
        }

        //Remove un-needed date evidence
        for (int i = 0; i < GameplayController.Instance.dateEvidence.Count; i++)
        {
            EvidenceDate t = GameplayController.Instance.dateEvidence[i];

            bool needed = false;

            foreach (Case c in CasePanelController.Instance.activeCases)
            {
                if (c.caseElements.Exists(item => item.id == t.evID))
                {
                    needed = true;
                    break;
                }
            }

            if (needed) continue;

            foreach (Case c in CasePanelController.Instance.archivedCases)
            {
                if (c.caseElements.Exists(item => item.id == t.evID))
                {
                    needed = true;
                    break;
                }
            }

            if (needed) continue;

            if (GameplayController.Instance.history.Exists(item => item.evID == t.evID))
            {
                needed = true;
                continue;
            }

            if (InterfaceController.Instance.activeWindows.Exists(item => item.passedEvidence == t))
            {
                needed = true;
                continue;
            }

            if (!needed)
            {
                GameplayController.Instance.evidenceDictionary.Remove(t.evID);
                GameplayController.Instance.dateEvidence.RemoveAt(i);
                i--;
            }
        }

        //Process player stats
        //if(!Game.Instance.disableSurvivalStatusesInStory || !Toolbox.Instance.IsStoryMissionActive(out _, out _))
        //{
        //    Player.Instance.AddNourishment(GameplayControls.Instance.playerHungerRate * -timePassed);
        //    Player.Instance.AddHydration(GameplayControls.Instance.playerThirstRate * -timePassed);
        //    Player.Instance.AddAlertness(GameplayControls.Instance.playerTirednessRate * -timePassed);
        //    //Player.Instance.AddHygiene(RoutineControls.Instance.hygeieneRate * -timePassed);
        //}

        //Remove drunk
        if (Player.Instance.drunk > 0f)
        {
            if (Player.Instance.drunk > 0.5f)
            {
                triggerHeadache = Mathf.Max(triggerHeadache, Player.Instance.drunk);
            }

            Player.Instance.AddDrunk(timePassed * -0.4f); //2.5 hours to completely recover
        }

        //Trigger hangover
        if (Player.Instance.drunk < 0.15f && CitizenBehaviour.Instance.triggerHeadache > 0f)
        {
            float hang = Mathf.Min(CitizenBehaviour.Instance.triggerHeadache * (timePassed / 0.01f), CitizenBehaviour.Instance.triggerHeadache); //Apply hangover over 15 mins

            Player.Instance.AddHeadache(hang);
            Player.Instance.AddHygiene(hang * -0.7f);
            Player.Instance.AddHydration(hang * -0.5f);

            CitizenBehaviour.Instance.triggerHeadache -= hang;
        }

        //Remove bruised
        if (Player.Instance.bruised > 0f)
        {
            Player.Instance.AddBruised(timePassed * -0.2f); //4 hours to completely recover
        }

        //Remove black eye
        if (Player.Instance.blackEye > 0f)
        {
            Player.Instance.AddBlackEye(timePassed * -0.2f); //4 hours to completely recover
        }

        //Remove sick
        if (Player.Instance.sick > 0f)
        {
            Player.Instance.AddSick(timePassed * -0.5f); //2 hours to completely recover
        }

        //Remove numb
        if (Player.Instance.numb > 0f)
        {
            Player.Instance.AddNumb(timePassed * -2f); //30 mins to completely recover
        }

        //Remove poisoned
        if (Player.Instance.poisoned > 0f)
        {
            Player.Instance.AddHealth(Player.Instance.poisoned * -10f * timePassed);
            Player.Instance.AddPoisoned(-timePassed, null); //1 hours to completely recover
        }

        //Lose heath from toxic gas
        if (Player.Instance.gasLevel > 0f && !Player.Instance.playerKOInProgress)
        {
            Player.Instance.AddHealth(Player.Instance.gasLevel * -10f * timePassed);
        }

        //Remove bleeding
        if (Player.Instance.bleeding > 0f)
        {
            Player.Instance.AddBleeding(timePassed * -0.5f); //2 hours to completely recover
        }

        //Add starch addiction
        float starchAdd = UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.starchAddiction);

        if(starchAdd != 0f)
        {
            Player.Instance.AddStarchAddiction(timePassed * starchAdd); //1 hour for complete addiction strength
        }

        //Add energy
        //Is sleeping...
        if(Player.Instance.spendingTimeMode && InteractionController.Instance.lockedInInteraction != null && InteractionController.Instance.lockedInInteraction.preset.specialCaseFlag == InteractablePreset.SpecialCase.sleepPosition)
        {
            //Add well rested
            if (Player.Instance.energy >= 0.8f)
            {
                Player.Instance.AddWellRested(timePassed / 2f); //2 hours of sleep to completely gain well rested
            }

            //Lower these rates if asleep...
            Player.Instance.AddNourishment((GameplayControls.Instance.playerHungerRate * 0.5f) * -timePassed);
            Player.Instance.AddHydration((GameplayControls.Instance.playerThirstRate* 0.5f) * -timePassed);
            Player.Instance.AddEnergy(timePassed / 4f); //4 hours of sleep to completely gain energy
        }
        //Remove energy and well rested
        else
        {
            if (!Game.Instance.disableSurvivalStatusesInStory || !Toolbox.Instance.IsStoryMissionActive(out _, out _))
            {
                Player.Instance.AddNourishment(GameplayControls.Instance.playerHungerRate * -timePassed);
                Player.Instance.AddHydration(GameplayControls.Instance.playerThirstRate * -timePassed);
                Player.Instance.AddAlertness(GameplayControls.Instance.playerTirednessRate * -timePassed);
            }

            Player.Instance.AddWellRested(timePassed * -0.5f); //2 hours to completely remove
        }

        Player.Instance.AddAlertness(GameplayControls.Instance.playerTirednessRate * -timePassed);

        //Remove wet
        if (Player.Instance.wet > 0f)
        {
            Player.Instance.AddWet(timePassed * (-0.25f + Player.Instance.heat) * -3f);
        }

        //Remove blackout
        if (Player.Instance.blackedOut > 0f)
        {
            Player.Instance.AddBlackedOut(timePassed * -5f);
        }

        //Remove broken leg
        if(Player.Instance.brokenLeg > 0f)
        {
            Player.Instance.AddBrokenLeg(timePassed * -0.1f); //10 hours to completely recover
        }

        //Remove headache
        if (Player.Instance.headache > 0f)
        {
            if(!Player.Instance.isRunning)
            {
                Player.Instance.AddHeadache(timePassed * -0.75f); //1.5 hours to completely recover
            }
            else if(Player.Instance.isRunning && Player.Instance.headache >= 0.1f)
            {
                Player.Instance.AddHeadache(timePassed * 2f); //Makes it worse if you run
            }
        }

        //Shower cleans player, warms them up and makes them wet
        if (Player.Instance.currentNode != null)
        {
            foreach(Interactable i in Player.Instance.currentNode.interactables)
            {
                if(i.preset.specialCaseFlag == InteractablePreset.SpecialCase.shower)
                {
                    if(i.sw0)
                    {
                        Player.Instance.AddHygiene(timePassed * 35f); //Clean the player!
                        Player.Instance.AddHeat(timePassed * 42f); //Warm them up!
                        Player.Instance.AddWet(timePassed * 50f); //Make them wet!
                        break;
                    }
                }
            }
        }

        //Add player heat
        if(!CutSceneController.Instance.cutSceneActive)
        {
            if (Player.Instance.isOnStreet)
            {
                Player.Instance.AddHeat(timePassed * SessionData.Instance.temperature * 1.5f);
                Player.Instance.AddWet(timePassed * SessionData.Instance.currentRain * 8f);
            }
            else
            {
                if(Player.Instance.inAirVent)
                {
                    Player.Instance.AddHeat(timePassed * GameplayControls.Instance.airDuctTemperature * 1.75f);
                }
                else
                {
                    Player.Instance.AddHeat(timePassed * GameplayControls.Instance.indoorTemperature * 2.1f);
                }
            }
        }

        //Search for proximity to heat sources
        if (Player.Instance.heat < 1f && Player.Instance.currentRoom != null)
        {
            foreach(Interactable i in Player.Instance.currentRoom.heatSources)
            {
                float dist = Vector3.Distance(i.wPos, Player.Instance.transform.position);

                if(dist <= 5f)
                {
                    Player.Instance.AddHeat(timePassed * (GameplayControls.Instance.heatSourceTemperature * (1f - (dist / 5f))));
                }
            }
        }

        //Decline of health when cold
        if(Player.Instance.heat < 0.5f)
        {
            Player.Instance.AddHealth(-0.1f * timePassed);
        }

        //Bleed
        if(Player.Instance.bleeding > 0f)
        {
            Player.Instance.AddHealth(Player.Instance.bleeding * 3f * -timePassed);

            //Project blood on floor
            new SpatterSimulation(Player.Instance, new Vector3(0, 1, 0), Vector3.down, GameplayControls.Instance.bleedingSpatter, SpatterSimulation.EraseMode.onceExecutedAndOutOfAddressPlusDespawnTime, Player.Instance.bleeding, false);
        }

        //Passive income
        int passiveIncome = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.passiveIncome));

        if(passiveIncome > 0f)
        {
            passiveIncomeTimer += timePassed;

            //Passive income every 2 hours
            if(passiveIncomeTimer >= 2f)
            {
                GameplayController.Instance.AddMoney(Mathf.Max(passiveIncome, 1), true, "passiveincome");
                passiveIncomeTimer = 0f;
            }
        }

        //Repair doors
        List<NewDoor> toR = new List<NewDoor>();

        foreach(NewDoor d in GameplayController.Instance.damagedDoors)
        {
            if(d.wall.currentDoorStrength < d.wall.baseDoorStrength)
            {
                if(d.wall.node.gameLocation != Player.Instance.currentGameLocation && d.wall.otherWall.node.gameLocation != Player.Instance.currentGameLocation)
                {
                    if(d.wall.node.building != Player.Instance.currentBuilding && d.wall.otherWall.node.building != Player.Instance.currentBuilding)
                    {
                        d.wall.SetDoorStrength(Mathf.Min(d.wall.currentDoorStrength + 0.01f, d.wall.baseDoorStrength));
                    }
                }
            }
            else
            {
                toR.Add(d);
            }
        }

        foreach(NewDoor d in toR)
        {
            GameplayController.Instance.damagedDoors.Remove(d);
        }

        //Create side jobs
        SideJobController.Instance.JobCreationCheck();

        List<NewGameLocation> enforcerToRemove = new List<NewGameLocation>();

        //Make dead bodies smell!
        foreach(Human h in CityData.Instance.deadCitizensDirectory)
        {
            if(h.death != null && h.death.isDead && !h.death.reported && h.currentRoom != null && !h.unreportable)
            {
                h.death.smell += timePassed / GameplayControls.Instance.smellTime;

                int roomExtend = Mathf.FloorToInt(h.death.smell);
                HashSet<NewRoom> openSet = new HashSet<NewRoom>();
                openSet.Add(h.currentRoom);
                HashSet<NewRoom> closedSet = new HashSet<NewRoom>();

                Game.Log("Murder: Checking dead body smell (" + h.death.smell + ") of " + h.citizenName + " at " + h.currentRoom.GetName() + ", scanning room extents: " + roomExtend + "...");

                while(openSet.Count > 0 && roomExtend > 0)
                {
                    NewRoom current = openSet.First();

                    Game.Log("Murder: Smell scanning: " + current.name + " with " + current.currentOccupants.Count + " occupants and " + current.entrances.Count + " entrances...");

                    //Can anyone here smell it??
                    List<Actor> occ = new List<Actor>(current.currentOccupants);

                    foreach(Actor a in occ)
                    {
                        Human o = a as Human;

                        if(o != null && !o.isPlayer && o.ai != null && !o.isDead && o != h.death.GetKiller())
                        {
                            if (o.ai.goals.Exists(item => (item.preset == RoutineControls.Instance.smellDeadBody || item.preset == RoutineControls.Instance.findDeadBody) && item.passedInteractable == h.interactable))
                            {
                                Game.Log("Murder: "  + o.GetCitizenName() + " already smells dead body...");
                                break;
                            }

                            if (!o.isAsleep && !o.isStunned)
                            {
                                Game.Log("Murder: " + o.GetCitizenName() + " smells dead body...");
                                o.ai.CreateNewGoal(RoutineControls.Instance.smellDeadBody, 0f, 0f, newPassedInteractable: h.interactable, newPassedVar: (int)Human.Death.ReportType.smell);
                            }
                        }
                    }

                    foreach(NewNode.NodeAccess acc in current.entrances)
                    {
                        if (!acc.walkingAccess) continue;
                        NewRoom otherRoom = acc.GetOtherRoom(current);

                        if(!closedSet.Contains(otherRoom) && !openSet.Contains(otherRoom))
                        {
                            openSet.Add(otherRoom);
                        }
                    }

                    openSet.Remove(current);
                    closedSet.Add(current);
                    roomExtend--;
                }
            }
            else
            {
                //if (h.death == null) Game.Log("Murder: Dead citizen " + h.citizenName + " Death class is null!");
                //if (!h.death.isDead) Game.Log("Murder: Dead citizen " + h.citizenName + " Murder dead flag is false!");
                //if (h.death.reported) Game.Log("Murder: Dead citizen " + h.citizenName + " Body is classed as reported!");
                //if (h.currentRoom == null) Game.Log("Murder: Dead citizen " + h.citizenName + " Current room is null!");
                //if (h.unreportable) Game.Log("Murder: Dead citizen " + h.citizenName + " Unreportable flag is true!");
            }
        }

        //Handle enforcers
        foreach(KeyValuePair<NewGameLocation, GameplayController.EnforcerCall> pair in GameplayController.Instance.enforcerCalls)
        {
            //Assign enforcers
            //Choose enforcers to respond...
            if(pair.Value.state == GameplayController.EnforcerCallState.logged)
            {
                List<Human> asignees = new List<Human>();

                //Call 2 enforcers to the scene...
                List<Human> pool = new List<Human>();
                List<float> ranks = new List<float>();

                foreach(Human h in GameplayController.Instance.enforcers)
                {
                    if (h.isStunned || h.isDead || (h.ai.currentGoal != null && h.ai.currentGoal.preset == RoutineControls.Instance.enforcerResponse)) continue;

                    //Skip if existing responder...
                    bool exitingResponder = false;

                    foreach(KeyValuePair<NewGameLocation, GameplayController.EnforcerCall> pair2 in GameplayController.Instance.enforcerCalls)
                    {
                        if(pair2.Value.response != null && pair2.Value.response.Contains(h.humanID))
                        {
                            exitingResponder = true;
                            break;
                        }
                    }

                    if (exitingResponder) continue;

                    //Determin rank: Closest
                    float rank = -Vector3.Distance(h.transform.position, pair.Key.entrances[0].worldAccessPoint);

                    //Boost if on duty
                    if(h.ai.currentGoal != null && h.ai.currentGoal.preset == RoutineControls.Instance.workGoal)
                    {
                        rank += 1000;
                    }

                    if (h.isStunned) rank -= 10000;
                    if (h.isAsleep) rank -= 100;

                    pool.Add(h);
                    ranks.Add(rank);
                }

                //If there isn't enough of a reponse available, delay this...
                if (pool.Count < 2)
                {
                    continue;
                }

                while (asignees.Count < 2 && pool.Count > 0)
                {
                    int bestIndex = -1;
                    float r = -999999999;

                    for (int i = 0; i < ranks.Count; i++)
                    {
                        if(ranks[i] > r)
                        {
                            bestIndex = i;
                            r = ranks[i];
                        }
                    }

                    if (bestIndex > -1)
                    {
                        asignees.Add(pool[bestIndex]);
                        pool.RemoveAt(bestIndex);
                        ranks.RemoveAt(bestIndex);
                    }
                    else break;
                }

                if (pair.Value.response == null) pair.Value.response = new List<int>();

                foreach(Human h in asignees)
                {
                    Game.Log("Gameplay: Enforcer responding to call at " + pair.Key.name + ": " + h.GetCitizenName());

                    pair.Value.response.Add(h.humanID);

                    //Immediate teleport
                    if(pair.Value.immedaiteTeleport)
                    {
                        if(pair.Key.entrances.Count > 0)
                        {
                            NewNode.NodeAccess ent = pair.Key.GetMainEntrance();

                            if(ent != null)
                            {
                                NewRoom otherRoom = ent.GetOtherRoom(pair.Key);

                                if(otherRoom != null)
                                {
                                    NewNode teleportNode = null;
                                    float teleportDist = 0f;

                                    foreach (NewNode node in otherRoom.nodes)
                                    {
                                        if (node.noPassThrough) continue;
                                        if (node.noAccess) continue;
                                        if (node.accessToOtherNodes.Count <= 0) continue;

                                        //Try to find a node without doors
                                        float d = Vector3.Distance(node.position, ent.worldAccessPoint);

                                        if (teleportNode == null || d > teleportDist)
                                        {
                                            teleportNode = node;
                                            teleportDist = d;
                                        }
                                    }

                                    Game.Log("Gameplay: Teleporting enforcer " + h.GetCitizenName() + " to " + otherRoom.name + ", pos: " + teleportNode.position);

                                    h.Teleport(teleportNode, null);
                                }
                            }
                        }
                    }

                    //Force-cancel current action
                    if (h.ai.currentGoal != null && h.ai.currentGoal.preset != RoutineControls.Instance.enforcerResponse)
                    {
                        if (h.ai.currentAction != null)
                        {
                            h.ai.currentAction.Remove(h.ai.currentAction.preset.repeatDelayOnActionFail);
                        }

                        Game.Log("Gameplay: Enforcer " + h.GetCitizenName() + " deactivating goal " + h.ai.currentGoal.preset.name + " to enable enforcer response");
                        h.ai.currentGoal.OnDeactivate(1f);
                    }

                    //Create orders...
                    NewAIGoal responseGoal = h.ai.CreateNewGoal(RoutineControls.Instance.enforcerResponse, 0f, 0f, newPassedGameLocation: pair.Key);
                    responseGoal.OnActivate(); //Force activate

                    //Force update tick
                    h.ai.AITick(true);
                }

                //We can now move to the next phase
                pair.Value.state = GameplayController.EnforcerCallState.responding;
            }
            else if(pair.Value.state == GameplayController.EnforcerCallState.responding)
            {
                foreach(int i in pair.Value.response)
                {
                    Human cop = null;

                    if(CityData.Instance.GetHuman(i, out cop))
                    {
                        if(pair.Key.currentOccupants.Contains(cop))
                        {
                            pair.Value.arrivalTime = SessionData.Instance.gameTime;
                            pair.Value.state = GameplayController.EnforcerCallState.arrived;

                            break;
                        }
                    }
                }
            }
            else if(pair.Value.state == GameplayController.EnforcerCallState.arrived)
            {
                //Tag as crime scene if dead...
                foreach(Actor a in pair.Key.currentOccupants)
                {
                    if((a.isDead || (a.ai != null && a.ai.ko)) && !a.unreportable)
                    {
                        pair.Value.isCrimeScene = true;

                        //Tag as found if not done so
                        Human h = a as Human;

                        if(h != null && h.death != null && h.death.isDead && !h.death.reported)
                        {
                            foreach (int i in pair.Value.response)
                            {
                                Human cop = null;

                                if (CityData.Instance.GetHuman(i, out cop))
                                {
                                    if (pair.Key.currentOccupants.Contains(cop))
                                    {
                                        h.death.SetReported(cop, Human.Death.ReportType.visual);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Human h = a as Human;

                        if (h != null && h.ai != null && h.home == pair.Key)
                        {
                            if(!a.ai.goals.Exists(item => item.preset == RoutineControls.Instance.layLow))
                            {
                                float duration = 1f;
                                if (pair.Value.isCrimeScene) duration = 12f;

                                NewGameLocation layLowLocation = h.home;

                                if(layLowLocation == null || layLowLocation.isCrimeScene)
                                {
                                    Company layLowComp = CityData.Instance.companyDirectory.Find(item => item.publicFacing && item.placeOfBusiness != null && !item.placeOfBusiness.isCrimeScene && item.IsOpenAtThisTime(SessionData.Instance.gameTime));

                                    if(layLowComp != null)
                                    {
                                        layLowLocation = layLowComp.placeOfBusiness;
                                    }
                                }

                                if(layLowLocation != null)
                                {
                                    NewAIGoal layLow = a.ai.CreateNewGoal(RoutineControls.Instance.layLow, 0f, duration, newPassedGameLocation: layLowLocation);
                                }
                            }
                        }
                    }
                }

                //Search for 20 mins...
                if (!pair.Value.isCrimeScene)
                {
                    if(SessionData.Instance.gameTime > pair.Value.arrivalTime + GameplayControls.Instance.crimeSceneSearchLength)
                    {
                        bool present = false;

                        foreach (int i in pair.Value.response)
                        {
                            Human cop = null;

                            if (CityData.Instance.GetHuman(i, out cop))
                            {
                                cop.ai.dontEverCloseDoors = true; //Stop closing doors

                                if (pair.Key.currentOccupants.Contains(cop))
                                {
                                    present = true;
                                }
                            }
                        }

                        if(present)
                        {
                            pair.Value.state = GameplayController.EnforcerCallState.completed;
                        }
                    }
                }
                //One cop searches for 20 mins, when other arrives they guard the scene for 24 hrs
                else
                {
                    if (SessionData.Instance.gameTime > pair.Value.arrivalTime + GameplayControls.Instance.crimeSceneSearchLength)
                    {
                        //Officially set as crime scene
                        pair.Key.SetAsCrimeScene(true);
                        pair.Key.loggedAsCrimeScene = pair.Value.logTime;

                        List<Human> present = new List<Human>();

                        foreach (int i in pair.Value.response)
                        {
                            Human cop = null;

                            if (CityData.Instance.GetHuman(i, out cop))
                            {
                                cop.ai.dontEverCloseDoors = true; //Stop closing doors

                                if (pair.Key.currentOccupants.Contains(cop))
                                {
                                    if(!present.Contains(cop))
                                    {
                                        present.Add(cop);
                                    }
                                }
                            }
                        }

                        //One of the present is now the guard...
                        if(present.Count > 0 && pair.Value.guard <= -1)
                        {
                            Human guard = present[0];
                            pair.Value.guard = guard.humanID; //Set guard
                            Game.Log("Gameplay: Set " + guard.GetCitizenName() + " as guard for crime scene");

                            //Cancel response goal

                            //Add guard duty goal
                            //Create orders...
                            //Pass the main entrance's location
                            NewGameLocation mainEntranceLocation = pair.Key;

                            List<NewGameLocation> validEntrances = new List<NewGameLocation>();

                            foreach (NewNode.NodeAccess acc in mainEntranceLocation.entrances)
                            {
                                if (acc.walkingAccess)
                                {
                                    validEntrances.Add(acc.GetOtherGameLocation(pair.Key));
                                }
                            }

                            guard.ai.CreateNewGoal(RoutineControls.Instance.enforcerGuardDuty, 0f, 24f, newPassedGameLocation: validEntrances[Toolbox.Instance.Rand(0, validEntrances.Count)]);

                            //Cancel response goal
                            NewAIGoal callGoal = guard.ai.goals.Find(item => item.preset == RoutineControls.Instance.enforcerResponse);
                            if (callGoal != null) callGoal.Complete();

                            //guard.ai.AITick();
                        }
                        //We've picked the guard, everybody else can leave...
                        else if(pair.Value.guard > -1)
                        {
                            for (int i = 0; i < pair.Value.response.Count; i++)
                            {
                                int responder = pair.Value.response[i];
                                if (responder == pair.Value.guard) continue;

                                Human cop = null;

                                if (CityData.Instance.GetHuman(responder, out cop))
                                {
                                    NewAIGoal callGoal = cop.ai.goals.Find(item => item.preset == RoutineControls.Instance.enforcerResponse);

                                    cop.ai.CloseDoorsNormallyAfterLeavingGamelocation(pair.Key);
                                    if (callGoal != null) callGoal.Complete();
                                }

                                pair.Value.response.RemoveAt(i);
                                i--;
                            }

                            //Undo security lockdown
                            //if (pair.Key.thisAsAddress != null)
                            //{
                            //    if (!pair.Key.thisAsAddress.IsAlarmActive())
                            //    {
                            //        pair.Key.thisAsAddress.floor.SetAlarmLockdown(false);
                            //    }
                            //}

                            //Guard for 24 hours
                            if (SessionData.Instance.gameTime >= pair.Value.arrivalTime + GameplayControls.Instance.crimeSceneSearchLength + GameplayControls.Instance.crimeSceneLength)
                            {
                                pair.Value.state = GameplayController.EnforcerCallState.completed;
                            }
                        }
                    }
                }
            }
            else if(pair.Value.state == GameplayController.EnforcerCallState.completed)
            {
                Game.Log("Gameplay: Enforcer call to " + pair.Key.name + " is completed...");

                //Cancel crime scene
                if(pair.Value.isCrimeScene)
                {
                    //Send to cleanup pool
                    if(!GameplayController.Instance.crimeSceneCleanups.Contains(pair.Key))
                    {
                        GameplayController.Instance.crimeSceneCleanups.Add(pair.Key);
                    }
                }

                pair.Value.isCrimeScene = false;
                pair.Key.SetAsCrimeScene(false);

                //Complete goals
                foreach (int i in pair.Value.response)
                {
                    Human cop = null;

                    if (CityData.Instance.GetHuman(i, out cop))
                    {
                        //Cancel goals
                        NewAIGoal callGoal = cop.ai.goals.Find(item => item.preset == RoutineControls.Instance.enforcerResponse);
                        if (callGoal != null) callGoal.Complete();

                        NewAIGoal guardGoal = cop.ai.goals.Find(item => item.preset == RoutineControls.Instance.enforcerGuardDuty);
                        if (guardGoal != null) guardGoal.Complete();

                        cop.ai.CloseDoorsNormallyAfterLeavingGamelocation(pair.Key);
                    }
                }

                if (!enforcerToRemove.Contains(pair.Key))
                {
                    enforcerToRemove.Add(pair.Key);
                }
            }
        }

        foreach(NewGameLocation gl in enforcerToRemove)
        {
            //Undo security lockdown
            //if(gl.thisAsAddress != null)
            //{
            //    if(!gl.thisAsAddress.building.alarmActive)
            //    {
            //        gl.thisAsAddress.floor.SetAlarmLockdown(false);
            //    }
            //}

            //Remove enforcer call
            GameplayController.Instance.enforcerCalls.Remove(gl);
        }

        //Fix broken windows
        if(GameplayController.Instance.brokenWindows.Count > 0)
        {
            List<Vector3> toFix = new List<Vector3>();

            foreach(KeyValuePair<Vector3, float> pair in GameplayController.Instance.brokenWindows)
            {
                //Time to fix...
                if(SessionData.Instance.gameTime >= pair.Value + GameplayControls.Instance.brokenWindowResetTime)
                {
                    toFix.Add(pair.Key);
                }
            }

            //Fix windows
            //Remove from the dictionary; once enabled again the window will be set to fixed
            foreach(Vector3 v3 in toFix)
            {
                Game.Log("Fixing window at " + v3);
                GameplayController.Instance.brokenWindows.Remove(v3);
            }
        }

        //Handle kettles
        if(GameplayController.Instance.activeKettles.Count > 0)
        {
            for (int i = 0; i < GameplayController.Instance.activeKettles.Count; i++)
            {
                Interactable kettle = GameplayController.Instance.activeKettles[i];

                if(kettle.sw0)
                {
                    if(!kettle.sw2)
                    {
                        kettle.SetCustomState2(true, null);
                    }

                    if(kettle.cs < 1f)
                    {
                        kettle.cs += timePassed * 7f;
                        kettle.cs = Mathf.Clamp01(kettle.cs);

                        //Update params
                        kettle.UpdateLoopingAudioParams();
                    }
                }
                else
                {
                    if (kettle.cs > 0f)
                    {
                        kettle.cs -= timePassed * 30f;
                        kettle.cs = Mathf.Clamp01(kettle.cs);

                        //Update params
                        kettle.UpdateLoopingAudioParams();

                        if (kettle.cs <= 0f)
                        {
                            if (kettle.sw2)
                            {
                                kettle.SetCustomState2(false, null);
                            }

                            GameplayController.Instance.activeKettles.RemoveAt(i);
                            i--;
                        }
                    }
                }
            }
        }

        //Handle hotel bills
        if (GameplayController.Instance.hotelGuests.Count > 0)
        {
            for (int i = 0; i < GameplayController.Instance.hotelGuests.Count; i++)
            {
                GameplayController.HotelGuest guest = GameplayController.Instance.hotelGuests[i];

                //Add payment due every 24 hours
                if(SessionData.Instance.gameTime >= guest.nextPayment)
                {
                    guest.bill += guest.roomCost;
                    guest.nextPayment += 24;
                }

                //Reposses items if the player is out of the building
                if (SessionData.Instance.gameTime >= guest.lastPayment + 24 + CityControls.Instance.kickoutTime)
                {
                    Human roomBelongsTo = guest.GetHuman();

                    if (roomBelongsTo != null && roomBelongsTo.isPlayer)
                    {
                        NewAddress hotelRoom = guest.GetAddress();

                        if(hotelRoom != null && roomBelongsTo.currentBuilding != hotelRoom.building)
                        {
                            List<Interactable> playerInteractables = new List<Interactable>();

                            foreach(NewRoom r in hotelRoom.rooms)
                            {
                                foreach(NewNode n in r.nodes)
                                {
                                    for (int u = 0; u < n.interactables.Count; u++)
                                    {
                                        Interactable roomObject = n.interactables[u];

                                        if(roomObject != null)
                                        {
                                            if(roomObject.preset.spawnable && roomObject.preset.prefab != null)
                                            {
                                                if(roomObject.belongsTo != null)
                                                {
                                                    playerInteractables.Add(roomObject);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            foreach(Interactable obj in playerInteractables)
                            {
                                Game.Log("Gameplay: Repossessing " + obj.GetName());
                                obj.SafeDelete();
                            }

                            //Game message
                            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "hotel_kickout1"), InterfaceControls.Icon.money, colourOverride: true, col: InterfaceControls.Instance.messageRed);
                            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "hotel_kickout2"), InterfaceControls.Icon.money, colourOverride: true, col: InterfaceControls.Instance.messageRed);

                            //Kick out of hotel
                            GameplayController.Instance.RemoveHotelGuest(hotelRoom, roomBelongsTo, false);

                            //Add player as a security threat
                            StatusController.Instance.SetWantedInBuilding(hotelRoom.building, GameplayControls.Instance.buildingWantedTime);
                        }
                    }
                }
            }
        }

        //Handle music players
        if (GameplayController.Instance.activeMusicPlayers.Count > 0)
        {
            for (int i = 0; i < GameplayController.Instance.activeMusicPlayers.Count; i++)
            {
                Interactable mp = GameplayController.Instance.activeMusicPlayers[i];
                mp.UpdateMusicPlayer();
            }
        }

        //Remove door knock values
        if (GameplayController.Instance.doorKnockAttempts.Count > 0)
        {
            List<NewDoor> toRemove = new List<NewDoor>();

            foreach(KeyValuePair<NewDoor, List<GameplayController.DoorKnockAttempt>> pair in GameplayController.Instance.doorKnockAttempts)
            {
                for (int i = 0; i < pair.Value.Count; i++)
                {
                    GameplayController.DoorKnockAttempt dka = pair.Value[i];

                    dka.value -= timePassed / 0.5f;

                    if (dka.value <= 0f)
                    {
                        pair.Value.RemoveAt(i);
                        i--;
                        continue;
                    }
                }

                if(pair.Value.Count <= 0)
                {
                    toRemove.Add(pair.Key);
                }
            }

            foreach(NewDoor d in toRemove)
            {
                GameplayController.Instance.doorKnockAttempts.Remove(d);
            }
        }

        //Clean up crime scenes
        if(GameplayController.Instance.crimeSceneCleanups.Count > 0)
        {
            for (int i = 0; i < GameplayController.Instance.crimeSceneCleanups.Count; i++)
            {
                NewGameLocation toClean = GameplayController.Instance.crimeSceneCleanups[i];

                //Only do this if the player is out of the building
                if (Player.Instance.currentGameLocation != null && Player.Instance.currentGameLocation == toClean) continue;
                if (Player.Instance.previousGameLocation != null && Player.Instance.previousGameLocation == toClean) continue;
                if (toClean.building != null && Player.Instance.currentBuilding == toClean.building) continue;

                //Only process cleanup if past delay time
                if(SessionData.Instance.gameTime < toClean.loggedAsCrimeScene + GameplayControls.Instance.crimeSceneCleanupDelay)
                {
                    continue;
                }

                //Don't clean up crime scene if in story mode
                if (!Game.Instance.sandboxMode) continue;

                //Remove dead body (banish them from the game world!)
                for (int l = 0; l < toClean.currentOccupants.Count; l++)
                {
                    Actor a = toClean.currentOccupants[l];

                    if(a.isDead)
                    {
                        Human h = a as Human;

                        foreach(MurderController.Murder m in MurderController.Instance.activeMurders)
                        {
                            if(m.victim == h)
                            {
                                m.OnCleanCrimeScene();
                            }
                        }

                        foreach (MurderController.Murder m in MurderController.Instance.inactiveMurders)
                        {
                            if (m.victim == h)
                            {
                                m.OnCleanCrimeScene();
                            }
                        }

                        if (h != null)
                        {
                            //h.SetResidence(null); //Remove from home (This was removing important DDS home references, see if this works better without removing; half way solution could be needed)
                            h.RemoveFromWorld(true);
                        }
                    }
                }

                if(toClean.thisAsAddress != null)
                {
                    //Close doors
                    foreach (NewNode.NodeAccess acc in toClean.entrances)
                    {
                        if (acc.door != null)
                        {
                            if (acc.door.forbiddenForPublic) acc.door.SetForbidden(false);
                            acc.door.SetOpen(0f, null, true);
                        }
                    }

                    //Do the following if there is now nobody living there...
                    if(toClean.thisAsAddress.residence != null && (toClean.thisAsAddress.company == null || toClean.thisAsAddress.company.preset.isSelfEmployed))
                    {
                        List<Human> nonDeadResidents = toClean.thisAsAddress.owners.FindAll(item => !item.isDead);

                        //If this is an address, remove all furniture
                        if(nonDeadResidents.Count <= 0)
                        {
                            toClean.RemoveAllInhabitantFurniture(false, FurnitureClusterLocation.RemoveInteractablesOption.remove);
                            toClean.thisAsAddress.inhabitants.Clear();
                            toClean.thisAsAddress.owners.Clear();

                            //TODO: If this is an address, put the apartment up for sale
                        }
                    }
                }

                GameplayController.Instance.crimeSceneCleanups.RemoveAt(i);
                i--;
            }
        }

        //Murderer loop
        MurderController.Instance.Tick(timePassed);

        //Track time since last loop
        timeOnLastGameWorldUpdate = SessionData.Instance.gameTime;
    }

    //Stealth mode loops
    void LightLevelLoop()
    {
        if(!actorsInStealthMode.Contains(Player.Instance))
        {
            Player.Instance.StealthModeLoop();
        }

        foreach(Actor act in actorsInStealthMode)
        {
            act.StealthModeLoop();
        }
    }

    //Triggered every hour
    public void OnHourChange()
    {
        float removeStrengthOverTime = 1f / GameplayControls.Instance.maximumFootprintTime;

        //Check and remove old footprints
        foreach(KeyValuePair<NewRoom, List<GameplayController.Footprint>> pair in GameplayController.Instance.activeFootprints)
        {
            for (int i = 0; i < pair.Value.Count; i++)
            {
                GameplayController.Footprint fp = pair.Value[i];

                fp.str -= removeStrengthOverTime;
                fp.bl -= removeStrengthOverTime;

                //Remove completely (on strength reching below 0.05)
                if(Mathf.Max(fp.str, fp.bl) <= 0.1f)
                {
                    GameplayController.Instance.footprintsList.Remove(fp);
                    pair.Value.RemoveAt(i);
                    i--;
                    continue;
                }
            }
        }

        CleanupController.Instance.TrashUpdate();
    }

    //Triggered at midnight (and on game start)
    public void OnDayChange()
    {
        UpdateForSale();

        Player.Instance.claimedAccidentCover = false; //Reset accident cover claim
        Player.Instance.foodHygeinePhotos.Clear();
        Player.Instance.sanitaryHygeinePhotos.Clear();
        Player.Instance.illegalOpsPhotos.Clear();

        foreach(Citizen c in CityData.Instance.citizenDirectory)
        {
            foreach (DialogPreset dia in Toolbox.Instance.defaultDialogOptions)
            {
                if (dia.dailyReplenish)
                {
                    if (c.evidenceEntry.dialogOptions.ContainsKey(dia.tiedToKey))
                    {
                        if(!c.evidenceEntry.dialogOptions[dia.tiedToKey].Exists(item => item.preset == dia))
                        {
                            c.evidenceEntry.AddDialogOption(dia.tiedToKey, dia);
                        }
                    }
                    else c.evidenceEntry.AddDialogOption(dia.tiedToKey, dia);
                }
            }

            //Replenish one wallet item per day
            c.WalletItemCheck(1);
        }

        //Check for vandalism removal
        foreach(NewAddress add in CityData.Instance.addressDirectory)
        {
            if(add.vandalism.Count > 0)
            {
                for (int i = 0; i < add.vandalism.Count; i++)
                {
                    NewAddress.Vandalism v = add.vandalism[i];

                    if(SessionData.Instance.gameTime >= v.time + GameplayControls.Instance.vandalismTimeout)
                    {
                        add.vandalism.RemoveAt(i);
                        i--;
                    }
                }
            }
        }

        //Check for new lost and found
        foreach(NewBuilding building in CityData.Instance.buildingDirectory)
        {
            int toSpawn = building.preset.maxLostAndFound - building.lostAndFound.Count;

            for (int i = 0; i < toSpawn; i++)
            {
                building.TriggerNewLostAndFound();
            }
        }
    }

    public void UpdateForSale()
    {
        //Add/remove for sale
        foreach (ResidenceController r in CityData.Instance.residenceDirectory)
        {
            if (r.address.inhabitants.Count <= 0 && Player.Instance.residence != r && r.preset != null && r.preset.enableForSale)
            {
                //Skip selling the slophouse as found in the intro mission...
                if (ChapterController.Instance != null && ChapterController.Instance.chapterScript != null)
                {
                    ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                    if (intro != null)
                    {
                        if (r.address == intro.slophouse)
                        {
                            if (r.address.saleNote != null)
                            {
                                r.address.saleNote.Delete();
                            }

                            GameplayController.Instance.forSale.Remove(r.address);
                            continue;
                        }
                    }
                }

                if (!GameplayController.Instance.forSale.Contains(r.address))
                {
                    GameplayController.Instance.forSale.Add(r.address);
                }
            }
            else if (GameplayController.Instance.forSale.Contains(r.address))
            {
                //Be sure to remove 'for sale' ad
                if(r.address.saleNote != null)
                {
                    r.address.saleNote.Delete();
                }

                GameplayController.Instance.forSale.Remove(r.address);
            }
        }

        foreach (NewAddress ad in GameplayController.Instance.forSale)
        {
            //Create/spawn ad
            if(ad.saleNote == null)
            {
                FurnitureLocation jobBoard = null;

                //Skip selling the slophouse as found in the intro mission...
                if(ChapterController.Instance != null && ChapterController.Instance.chapterScript != null)
                {
                    ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                    if(intro != null)
                    {
                        if(ad == intro.slophouse)
                        {
                            if(ad.saleNote != null)
                            {
                                ad.saleNote.Delete();
                            }

                            continue;
                        }
                    }
                }

                //If we're at this point then just pick a random board...
                if (CityData.Instance.jobBoardsDirectory.Count > 0)
                {
                    jobBoard = CityData.Instance.jobBoardsDirectory[Toolbox.Instance.Rand(0, CityData.Instance.jobBoardsDirectory.Count)];
                }

                List<FurniturePreset.SubObject> placePool = new List<FurniturePreset.SubObject>();

                for (int i = 0; i < jobBoard.furniture.subObjects.Count; i++)
                {
                    FurniturePreset.SubObject so = jobBoard.furniture.subObjects[i];
                    if (!InteriorControls.Instance.saleNote.subObjectClasses.Contains(so.preset)) continue; //class must match...
                    if (jobBoard.spawnedInteractables.Exists(item => item.subObject == so)) continue; //item is already here...
                    placePool.Add(so);
                }

                if(placePool.Count > 0)
                {
                    FurniturePreset.SubObject chosenLocation = placePool[Toolbox.Instance.Rand(0, placePool.Count)];

                    List<Interactable.Passed> spawnPass = new List<Interactable.Passed>();
                    spawnPass.Add(new Interactable.Passed(Interactable.PassedVarType.addressID, ad.id));

                    Game.Log("Decor: Placing sale note for " + ad.name + " at " + jobBoard.anchorNode.gameLocation.name);

                    ad.saleNote = InteractableCreator.Instance.CreateFurnitureSpawnedInteractable(InteriorControls.Instance.saleNote, jobBoard, chosenLocation, null, null, null, spawnPass, null, null);
                }
                else
                {
                    Game.Log("Decor: Unable to place sale note for " + ad.name);
                }
            }
        }
    }
}
