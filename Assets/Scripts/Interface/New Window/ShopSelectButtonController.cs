﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopSelectButtonController : ButtonController
{
    public InteractablePreset preset;
    public Interactable sellInteractable;
    public SyncDiskPreset syncDisk;
    public int price;
    public TextMeshProUGUI priceText;
    public TextMeshProUGUI specialText;
    public InfoWindow thisWindow;
    public BuyInterfaceController buyController;
    public Image mainImage;
    public bool todayOnly = false;
    public bool sellMode = false;

    public void Setup(InteractablePreset newPreset, int newPrice, BuyInterfaceController newBuyController, InfoWindow newThisWindow, SyncDiskPreset newSyncDisk = null, bool newTemp = false, Interactable newSellInteractable = null, bool newSellMode = false)
    {
        preset = newPreset;
        price = newPrice;
        thisWindow = newThisWindow;
        buyController = newBuyController;
        syncDisk = newSyncDisk;
        todayOnly = newTemp;
        sellInteractable = newSellInteractable;
        sellMode = newSellMode;

        base.SetupReferences();

        UpdateButtonText();
        UpdateTooltip();
    }

    public override void UpdateButtonText()
    {
        //Set name
        if(syncDisk != null && !sellMode)
        {
            if(todayOnly)
            {
                specialText.gameObject.SetActive(true);
                specialText.text = Strings.Get("ui.interface", "Special Offer: Today Only") + "\n" + Strings.Get("ui.interface", "Install at Sync Clinic");
            }

            text.text = "#" + syncDisk.syncDiskNumber + " " + Strings.Get("evidence.syncdisks", syncDisk.name);
        }
        else if(sellMode && sellInteractable != null && (buyController.company == null || !buyController.company.preset.enableSellingOfIllegalItems) && StatusController.Instance.activeFineRecords.Exists(item => item.crime == StatusController.CrimeType.theft && item.objectID == sellInteractable.id))
        {
            specialText.gameObject.SetActive(true);
            specialText.text = Strings.Get("ui.interface", "You cannot sell stolen items here");
        }
        else
        {
            specialText.gameObject.SetActive(false);

            if(sellInteractable != null) text.text = sellInteractable.GetName();
            else if(preset != null) text.text = Strings.Get("evidence.names", preset.name);
        }
        
        priceText.text = CityControls.Instance.cityCurrency + price;

        if(preset != null)
        {
            icon.sprite = preset.iconOverride;
            mainImage.sprite = preset.staticImage;
        }
    }

    public void UpdateTooltip()
    {
        if(syncDisk != null && !sellMode)
        {
            //Set main text
            tooltip.mainText = "#" + syncDisk.syncDiskNumber + " " + Strings.Get("evidence.syncdisks", syncDisk.name);

            string options = string.Empty;

            if (syncDisk.mainEffect1 != SyncDiskPreset.Effect.none)
            {
                options += Strings.ComposeText(Strings.Get("evidence.syncdisks", syncDisk.mainEffect1Description), syncDisk, additionalObject: new int[] { 0, 0 });
            }

            if (syncDisk.mainEffect2 != SyncDiskPreset.Effect.none)
            {
                options += "\n" + Strings.Get("evidence.syncdisks", "OR") + "\n" + Strings.ComposeText(Strings.Get("evidence.syncdisks", syncDisk.mainEffect2Description), syncDisk, additionalObject: new int[] { 1, 0 });
            }

            if (syncDisk.mainEffect3 != SyncDiskPreset.Effect.none)
            {
                options += "\n" + Strings.Get("evidence.syncdisks", "OR") + "\n" + Strings.ComposeText(Strings.Get("evidence.syncdisks", syncDisk.mainEffect3Description), syncDisk, additionalObject: new int[] { 2, 0 });
            }

            tooltip.detailText = options;
        }
        else
        {
            tooltip.mainText = Strings.GetTextForComponent(preset.summaryMessageSource, null);
        }
    }

    public override void OnLeftClick()
    {
        Game.Log("Player: Purchase/sell " + preset.name);

        if(!sellMode)
        {
            if(price <= GameplayController.Instance.money)
            {
                if(!FirstPersonItemController.Instance.IsSlotAvailable())
                {
                    PopupMessageController.Instance.PopupMessage("dropitem", true, false, "Confirm", anyButtonClosesMsg: true);
                }
                //Go ahead with purchase...
                else
                {
                    PopupMessageController.Instance.PopupMessage("purchaseask", true, true, "Cancel", "Confirm", anyButtonClosesMsg: true);
                    PopupMessageController.Instance.OnRightButton += PurchaseExecute;
                    PopupMessageController.Instance.OnLeftButton += Cancel;
                }
            }
        }
        else
        {
            if(sellInteractable != null)
            {
                PopupMessageController.Instance.PopupMessage("sellask", true, true, "Cancel", "Confirm", anyButtonClosesMsg: true);
                PopupMessageController.Instance.OnRightButton += SellExecute;
                PopupMessageController.Instance.OnLeftButton += Cancel;
            }
        }

        base.OnLeftClick();
    }

    public void PurchaseExecute()
    {
        PopupMessageController.Instance.OnRightButton -= PurchaseExecute;
        PopupMessageController.Instance.OnLeftButton -= Cancel;

        Interactable newObj = InteractableCreator.Instance.CreateWorldInteractable(preset, Player.Instance, Player.Instance, null, thisWindow.passedInteractable.wPos + new Vector3(0, 3.5f, 0), thisWindow.passedInteractable.wEuler, null, syncDisk);

        //Execute purchase
        if (newObj != null)
        {
            Game.Log("Successfully created newObject " + newObj.name + " id " + newObj.id);
            newObj.SetSpawnPositionRelevent(false); //Set spawn to irrelevent so there's no 'put back' action

            if (FirstPersonItemController.Instance.PickUpItem(newObj, true, false, playSound: false))
            {
                AudioController.Instance.Play2DSound(AudioControls.Instance.purchaseItem);

                newObj.MarkAsTrash(true); //Remove this once we can
                GameplayController.Instance.AddMoney(-price, true, "purchase");
                buyController.UpdatePurchaseAbility();
                //thisWindow.CloseWindow();

                if (thisWindow != null && thisWindow.passedInteractable != null && thisWindow.passedInteractable.preset.menuOverride != null)
                {
                    if(thisWindow.passedInteractable.preset.menuOverride.purchaseAudio != null)
                    {
                        AudioController.Instance.PlayWorldOneShot(thisWindow.passedInteractable.preset.menuOverride.purchaseAudio, Player.Instance, thisWindow.passedInteractable.node, thisWindow.passedInteractable.wPos, thisWindow.passedInteractable);
                    }
                }
            }
            else
            {
                Game.LogError("Unable to pickup item " + newObj.name);
                newObj.Delete();
            }
        }
        else
        {
            Game.LogError("Unable to create world item " + preset.name);
        }
    }

    public void SellExecute()
    {
        PopupMessageController.Instance.OnRightButton -= SellExecute;
        PopupMessageController.Instance.OnLeftButton -= Cancel;

        if (sellInteractable != null)
        {
            FirstPersonItemController.InventorySlot invSlot = FirstPersonItemController.Instance.slots.Find(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic && item.interactableID == sellInteractable.id);

            if (invSlot != null)
            {
                AudioController.Instance.Play2DSound(AudioControls.Instance.purchaseItem);
                GameplayController.Instance.AddMoney(price, true, "sell");
                buyController.UpdatePurchaseAbility();
                sellInteractable.SafeDelete(true);
                buyController.UpdateElements(); //Refresh sell screen
            }
        }
        else
        {
            Game.LogError("Unable to find sell interactable");
        }
    }

    public void Cancel()
    {
        PopupMessageController.Instance.OnRightButton -= PurchaseExecute;
        PopupMessageController.Instance.OnRightButton -= SellExecute;
        PopupMessageController.Instance.OnLeftButton -= Cancel;
    }

    public void UpdatePurchaseAbility()
    {
        if (!sellMode && price > GameplayController.Instance.money)
        {
            button.interactable = false;
            priceText.text = "<color=#FD313F>" + CityControls.Instance.cityCurrency + price;
        }
        else if(sellMode && sellInteractable != null && (buyController.company == null || !buyController.company.preset.enableSellingOfIllegalItems))
        {
            //Is stolen?
            if (StatusController.Instance.activeFineRecords.Exists(item => item.crime == StatusController.CrimeType.theft && item.objectID == sellInteractable.id))
            {
                button.interactable = false;
                priceText.text = CityControls.Instance.cityCurrency + price;
            }
            else
            {
                button.interactable = true;
                priceText.text = CityControls.Instance.cityCurrency + price;
            }
        }
        else
        {
            button.interactable = true;
            priceText.text = CityControls.Instance.cityCurrency + price;
        }
    }
}
