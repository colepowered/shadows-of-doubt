﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEditor;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "roomtype_data", menuName = "Database/Room Type Preset")]

public class RoomTypePreset : SoCustomComparison
{
    [Header("Contents")]
    public RoomConfiguration forceConfiguration;

    [Header("Size Settings")]
    [Tooltip("Chance this has of being included")]
    [Range(0f, 1f)]
    public float chance = 1f;
    [Tooltip("The address has to be this big for this rooom to be included")]
    [Range(0, 90)]
    public int minimumAddressSize = 18;
    [Tooltip("Maximum number of these room types per addresses")]
    [Range(1, 10)]
    public int maximumRoomTypesPerAddress = 1;
    [Space(7)]
    [Tooltip("The priority given to this room: Higher priority rooms will override size variables of others.")]
    [Range(0, 10)]
    public int cyclePriority = 5;

    [Header("Room Shape Settings")]
    [Tooltip("The room at it's smallest must be able to fit this shape inside it.")]
    public Vector2 minimumRoomAreaShape = new Vector2(2, 2);
    [Tooltip("The room at it's largest can feature this shape.")]
    public Vector2 maximumRoomAreaShape = new Vector2(10, 10);
    [Tooltip("All nodes in the room must tesselate with this shape.")]
    public Vector2 tesselationShape = new Vector2(2, 2);

    [Header("Room Placement Weighting")]
    [Range(-3, 3)]
    public int floorSpaceWeight = 1;
    [Range(-3, 3)]
    public int exteriorWallWeight = 1;
    [Range(-3, 3)]
    public int exteriorWindowWeight = 1;
    [Range(-3, 3)]
    public int entranceWeight = 0;

    [Header("Adjoining Rules")]
    [Tooltip("This must adjoin one of these rooms. If none adjoin all.")]
    [ReorderableList]
    public List<RoomTypePreset> mustAdjoinRooms = new List<RoomTypePreset>();
    [Tooltip("Should I feature doors from this preset or the bordering preset? Choose the highest priority")]
    [Range(0, 10)]
    public int doorPriority = 1;
    [Tooltip("Chance of doorway not featuring a door")]
    [Range(0f, 1f)]
    public float chanceOfNoDoor = 0f;
    [Tooltip("Maximum number of doors")]
    public int maxDoors = 99;
    public bool forceNoDoors = false;
    [Tooltip("Doors will use the 'highest' privacy setting between the 2 rooms")]
    public NewDoor.DoorSetting doorSetting;
    [Tooltip("Entrances to this room can be replaced by a divider if there are 2 or more adjoining walls")]
    public bool allowRoomDividers = false;
    [EnableIf("allowRoomDividers")]
    public int maxDividers = 99;
    [Tooltip("Only allow room dividers from these room types:")]
    [ReorderableList]
    public List<RoomTypePreset> onlyAllowDividersAdjoining = new List<RoomTypePreset>();

    [Header("Special Rules")]
    [Tooltip("If true the address entrance is allowed to open into this room")]
    public bool allowMainAddressEntrance = false;
    [Tooltip("If true the address entrance is allowed to open into this room")]
    public bool allowSecondaryAddressEntrance = false;
    [Tooltip("If both are present, prefer main entrance. Doubles entrance ranking score, so make sure it is >0")]
    public bool preferMainAddressEntrance = true;
    [Tooltip("If true this must be connected to an entrance, either by the entrance opening into it or otherwise through a created hallway. This will also skip the doorway creation process for this room.")]
    public bool mustConnectWithEntrance = false;
    [Tooltip("If true this room can be overriden with no penalty")]
    public bool overridable = false;
    [Tooltip("This room can be overwritten rooms with priority up to this value")]
    [Range(0, 10)]
    public int overwriteWithPriorityUpTo = 10;
    [Tooltip("Also block overrides from these room types")]
    [ReorderableList]
    public List<RoomTypePreset> blockOverridesFromType = new List<RoomTypePreset>();
    [Tooltip("If  the block list above is active then it makes sense to limit this room to only being overwritten once, so rooms outside of this block list can't then overwrite this node again.")]
    public int overwriteLimit = 999;
    [Tooltip("If true this room will exapnd into null space after all rooms are assigned, regardness of shape settings above.")]
    public bool expandIntoNull = false;
    [Tooltip("Limitations to null room expansion: Will only choose to expand if there are this many or more adjacent tiles")]
    public int expandIntoNullAdjacencyMinimum = 2;
    [Tooltip("If true this will share common features with adjacent rooms of the same type (such as light switches and ceiling light styles")]
    public bool shareFeaturesWithCommonAdjacent = false;
    [Tooltip("If true corridor ends can be integrated into this room (eg ends of hallways etc)")]
    public bool allowCorridorReplacement = false;

    [Header("Floor Settings Override")]
    public bool overrideFloorHeight = false;
    [EnableIf("overrideFloorHeight")]
    public int floorHeight = 0;

    [Header("Debug")]
    public RoomConfiguration copyFrom;
}
