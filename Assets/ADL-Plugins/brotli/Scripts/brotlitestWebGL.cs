﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class brotlitestWebGL : MonoBehaviour {
#if UNITY_WEBGL || UNITY_TVOS

    private bool downloadDone, downloadError;

    private int lz4 = 0;
    private int fixedDecompress = 0, newBufferSize = 0;
    // a reusable buffer
    private byte[] buff;

    // buffer operations buffers
    byte[] bt = null, bt2 = null;

    private ulong[] progress3 = new ulong[1];

    private Texture2D tex = null;

    // fixed size buffer, that don't gets resized, to perform decompression of buffers in them and avoid memory allocations.
    private byte[] fixedOutBuffer = new byte[1024*1024*4];

    // Use this for initialization
    void Start () {

        buff = new byte[0];

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        
        tex = new Texture2D(1,1,TextureFormat.RGBA32, false);

        StartCoroutine(DownloadTestFile());

    }


	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)) { Application.Quit(); }
    }


    void OnGUI() {

        if (downloadDone) {
            GUI.Label(new Rect(50, 0, 350, 30), "package downloaded, ready to extract");
        } else {
            if(downloadError) GUI.Label(new Rect(50, 150, 250, 50), "Download Error!");
        }

        if(lz4 == 1) GUI.DrawTexture(new Rect(360, 10, 575, 300), tex);

        GUI.Label(new Rect(50, 60, 250, 40), "Fixed buffer: " + fixedDecompress.ToString());
        GUI.Label(new Rect(50, 100, 250, 40), "New buffer: " + fixedDecompress.ToString());
     }


     IEnumerator DoTests() {

                 bt2 = new byte[1];

                // decompress the .brotli buffer that contains an image
                if (brotli.decompressBuffer(bt, ref bt2, true)) lz4 =1;

                // verify by loading the extracted buffer to a texture
                tex.LoadImage(bt2);

                yield return true;

                // decompress to a fixed size buffer
                fixedDecompress = brotli.decompressBuffer(bt, fixedOutBuffer, true);

                // decompress to a new returned buffer
                var newBuffer = brotli.decompressBuffer(bt, true);
                if(newBuffer != null) newBufferSize = newBuffer.Length;

                newBuffer = null; buff = null;
                bt2 = null; bt = null;
                
    }

  IEnumerator DownloadTestFile() {

        Debug.Log("starting download");

        string fileD = "https://dl.dropbox.com/s/jri8c8a4zg5g2o6/bufferpng.brbuf";

        // replace the link to the brotli file with your own (although this will work also)
        using (UnityEngine.Networking.UnityWebRequest www = UnityEngine.Networking.UnityWebRequest.Get(fileD)) {
            #if UNITY_5 || UNITY_4
            yield return www.Send();
            #else
            yield return www.SendWebRequest();
            #endif

            if (www.error != null) {
                Debug.Log(www.error);
                downloadError = true;
            } else  {
                downloadDone = true;

                bt = new byte[www.downloadHandler.data.Length];
                Array.Copy(www.downloadHandler.data, bt, www.downloadHandler.data.Length);
                yield return true;

                StartCoroutine (DoTests());

                Debug.Log("download done");
            }
        }
    }



#else
        void OnGUI(){
            GUI.Label(new Rect(10, 10, 500, 40), "Only for WebGL or tvOS.");
        }
#endif
}


