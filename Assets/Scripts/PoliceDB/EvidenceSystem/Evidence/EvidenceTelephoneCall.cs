﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceTelephoneCall : EvidenceTime
{
    public Evidence callFrom = null;
    public Evidence callTo = null;

    public EvidenceTelephoneCall(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        //Make call facts: Parse from ID
        string[] parseKey = evID.Split('|');

        if(parseKey.Length > 3)
        {
            string[] ev = parseKey[3].Split('>');

            if (ev.Length > 0)
            {
                Game.Log("Parsing call from: " + ev[0]);

                if(GameplayController.Instance.evidenceDictionary.TryGetValue(ev[0], out callFrom))
                {
                    Game.Log("Creating call from fact");

                    EvidenceCreator.Instance.CreateFact("CallFrom", callFrom, this, forceDiscoveryOnCreate: true);
                }
            }

            if (ev.Length > 1)
            {
                Game.Log("Parsing call to: " + ev[1]);

                if (GameplayController.Instance.evidenceDictionary.TryGetValue(ev[1], out callTo))
                {
                    //This is the telephone entry, but we want the location instead...
                    callTo = callTo.parent;

                    Game.Log("Creating call to fact");
                    EvidenceCreator.Instance.CreateFact("CallTo", this, callTo, forceDiscoveryOnCreate: true);
                }
            }
        }
    }

    public override void BuildDataSources()
    {
        //if (dataSource == null) dataSource = new Dictionary<string, EvidenceLinkData>();

        string time = string.Empty;

        if (timeFrom == timeTo)
        {
            time += SessionData.Instance.TimeAndDate(timeFrom, false, true, true);
        }
        else
        {
            time += SessionData.Instance.TimeAndDate(timeFrom, false, true, true) + " — " + SessionData.Instance.TimeAndDate(timeTo, false, true, true);
        }

        //dataSource.Add("time", new EvidenceLinkData(time));

        //if(callFrom != null) dataSource.Add("callfrom", new EvidenceLinkData(callFrom.GetNameForDataKey(DataKey.name), EvidenceLinkData.OnLinkExecute.spawnWindow, callFrom));
        //if(callTo != null) dataSource.Add("callto", new EvidenceLinkData(callTo.GetNameForDataKey(DataKey.name), EvidenceLinkData.OnLinkExecute.spawnWindow, callTo));

        //if (!dataSource.ContainsKey("summary"))
        //{
        //    string summary = Strings.ComposeText(Strings.Get("evidence.body", preset.name, Strings.Casing.asIs), ref dataSource, this, true);
        //    dataSource.Add("summary", new EvidenceLinkData(summary));
        //}
    }
}
