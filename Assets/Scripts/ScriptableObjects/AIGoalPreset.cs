﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "aigoal_data", menuName = "Database/AI/AI Goal")]

public class AIGoalPreset : SoCustomComparison
{
    [Header("Application")]
    [Tooltip("If true will be added to citizen upon creation")]
    public bool startingGoal = false;
    [Tooltip("Is this goal designed for...")]
    [EnableIf("startingGoal")]
    public StartingGoal appliesTo = StartingGoal.all;
    [EnableIf("startingGoal")]
    public List<OccupationPreset> appliedToTheseJobs;
    [EnableIf("startingGoal")]
    [Tooltip("Valid if any of these items are found at home...")]
    public List<InteractablePreset> onlyIfFeaturesItemsAtHome = new List<InteractablePreset>();
    [Tooltip("If true, don't save with game state")]
    public bool disableSave = false;
    [Tooltip("A general category we can use to help goals interact with each other")]
    public GoalCategory category = GoalCategory.trivial;

    public enum GoalCategory { trivial, important, vital };

    public enum StartingGoal { all, nonHomelessOnly, homelessOnly };

    [Header("Priority")]
    [Range(0, 11)]
    [Tooltip("The base priority")]
    public int basePriority = 5;
    [Range(0, 12)]
    [Tooltip("Random variance to add to the priority")]
    public int randomVariance = 1;
    [Tooltip("Clamp min/max priority")]
    public Vector2 minMaxPriority = new Vector2(0f, 10f);
    [Tooltip("Multiply base priority by amount of trash carried")]
    public bool multiplyUsingTrashCarried = false;
    [Tooltip("If the player owes debt then give this maximum priority")]
    public bool useLateDebtPriority = false;
    [Tooltip("If true this will only be ranked within the following hours")]
    public bool onlyImportantBetweenHours = false;
    [EnableIf("onlyImportantBetweenHours")]
    public Vector2 validBetweenHours = new Vector2(9f, 20.5f);
    [Tooltip("Don't update the priority of goals (apart from investigate) while this is active")]
    public bool dontUpdateGoalPriorityWhileActive = false;
    [Tooltip("Overrides all priority update rules when created")]
    public bool forcePriorityUpdateOnCreation = false;
    [Tooltip("When raining, this acts as a multiplier")]
    public RainFactor rainFactor = RainFactor.none;
    [Tooltip("Only important when music is playing in the room")]
    public bool useMusic = false;
    [Tooltip("Only important if this citizen is trespassing")]
    public bool useTrespassing = false;
    [Tooltip("Lose priority over time")]
    public bool affectPriorityOverTime = false;
    [Tooltip("Over the course of one hour, add this to the overall priority multiplier")]
    [EnableIf("affectPriorityOverTime")]
    public float multiplierModifierOverOneHour = -0.1f;

    public enum RainFactor { none, onlyDoWhenRaining, dontDoWhenRaining}

    [Header("Trait Modifiers")]
    public List<GoalModifierRule> goalModifiers = new List<GoalModifierRule>();


    [System.Serializable]
    public class GoalModifierRule
    {
        public CharacterTrait.RuleType rule = CharacterTrait.RuleType.ifAnyOfThese;

        public List<CharacterTrait> traitList = new List<CharacterTrait>();
        [ShowIf("isTrait")]
        [Tooltip("If this isn't true then it won't be picked for application at all.")]
        public bool mustPassForApplication = true;
        [Tooltip("Add this to a default priority multiplier of 1.")]
        public float priorityMultiplier = 0.5f;
    }

    [Header("Other Goal Modifiers")]
    public List<AIGoalPreset> ifGoalsPresent = new List<AIGoalPreset>();
    public float otherGoalPriorityModifier = 0f;

    //[Tooltip("If true, this will ignore clamping on min/max priority when combined with the above multiplier")]
    //public bool ignorePriorityMinMax = false;

    [Header("Timing Priority")]
    public bool useTiming = false;
    [Range(0, 10)]
    [EnableIf("useTiming")]
    [Tooltip("How important is timing to this goal? (Will add this much to overall if @ time)")]
    public int timingImportance = 3;
    [Range(0f, 3f)]
    [EnableIf("useTiming")]
    [Tooltip("When will the priority start being boosted: From this amount of time before trigger time")]
    public float earlyTimingWindow = 0.5f;
    [EnableIf("useTiming")]
    [Tooltip("Cancel the goal if too late (below time)")]
    public bool cancelIfLate = false;
    [EnableIf("cancelIfLate")]
    [Range(0f, 3f)]
    [Tooltip("Cancel the goal if this late to execute")]
    public float cancelIfThisLate = 1f;
    [Tooltip("Cancel if this has been active for too long")]
    public bool cancelAfterTime = false;
    [EnableIf("cancelAfterTime")]
    [Range(0f, 24f)]
    [Tooltip("Cancel the goal if it has been active for this time")]
    public float cancelAfter = 1f;
    [Tooltip("Run if this citizen becomes late")]
    public bool runIfLate = false;

    [Header("Stat Priority")]
    [Range(0, 10)]
    [Tooltip("Increases priority with hunger (inverse nourishment)")]
    public int nourishmentImportance = 0;
    [Range(0, 10)]
    [Tooltip("Increases priority with thirst (inverse hydration)")]
    public int hydrationImportance = 0;
    [Range(0, 10)]
    [Tooltip("Increases priority with laziness (inverse altertness)")]
    public int alertnessImportance = 0;
    [Range(0, 10)]
    [Tooltip("Increases priority with tiredness (inverse energy)")]
    public int energyImportance = 0;
    [Range(0, 10)]
    [Tooltip("Increases priority with bordem (inverse excitement)")]
    public int excitementImportance = 0;
    [Range(0, 10)]
    [Tooltip("Increases priority with todo (inverse chores)")]
    public int choresImportance = 0;
    [Range(0, 10)]
    [Tooltip("Increases priority with dirtiness (inverse hygiene)")]
    public int hygieneImportance = 0;
    [Range(0, 10)]
    [Tooltip("Increases priority with loo (inverse bladder)")]
    public int bladderImportance = 0;
    [Range(0, 10)]
    [Tooltip("Increases priority with need for heat (inverse heat)")]
    public int heatImportance = 0;
    [Range(0, 10)]
    [Tooltip("Increases priority with need for heat (inverse heat)")]
    public int drunkImportance = 0;
    [Range(0, 10)]
    [Tooltip("Increases priority with need for breath")]
    public int breathImportance = 0;
    [Range(0, 15)]
    [Tooltip("Increases priority when poisioned")]
    public int poisonImportance = 0;
    [Range(0, 50)]
    [Tooltip("Increases priority when blinded")]
    public int blindedImportance = 0;

    [Header("Completion")]
    [Tooltip("This goal will be removed when all actions have been completed")]
    public bool completable = false;
    [DisableIf("completable")]
    [Tooltip("When actions are completed, restart the above list")]
    public bool loopingActions = false;

    [Header("Interuption")]
    [Tooltip("If false this action cannot be interupted once started")]
    public bool interuptable = true;
    [EnableIf("interuptable")]
    public bool unteruptableByFollowingCategories = false;
    [EnableIf("interuptable")]
    public List<GoalCategory> uninteruptableByCategories = new List<GoalCategory>();
    [Tooltip("If true this action will use this threshold before it is interupted")]
    [EnableIf("interuptable")]
    public bool useInteruptionThreshold = false;
    [EnableIf("useInteruptionThreshold")]
    [Range(0, 10)]
    [Tooltip("Other goals will have to reach this much above the current priority before this one is interupted...")]
    public int interuptionThreshold = 1;

    public enum LocationOption { useCurrent, home, work, commercial, nearestAvailable, investigate, commercialDecision, patrolLocation, passedInteractable, passedGamelocation, murderLocation};
    public enum RoomOption { none, bedroom, job };
    public enum FurnitureOption { none, bed, job};
    //None: No specific location, so try to use current
    //Home: Go home
    //Work: Go to work
    //Commerical: Pick a location from favourites or nearby

    [Header("Delay")]
    [Tooltip("If use point is busy, delay goal from repeating for this time...")]
    public float repeatDelayOnBusy = 0.1f;
    [Tooltip("If interupted by a more important goal, delay goal from repeating for this time...")]
    public float repeatDelayOnInterupt = 0.1f;
    [Tooltip("If no actions left, delay goal from repeating for this time...")]
    public float repeatDelayOnFinishActions = 0.1f;


    [Header("Location")]
    [Tooltip("If enabled, enforcers on duty will be allowed everywhere to execute this action.")]
    public bool allowEnforcersEverywhere = false;

    [Space(7)]
    [InfoBox("Select 'Use Current' when none is needed (location is selected within action).\nNearest Available: Finds the nearest interactable using the first action, and passes it along with gamelocation\nCommercial/Commercial Decision: Will execute a decision based on current stats. Will pass a gamelocation, and sometimes a specific interactable.", EInfoBoxType.Normal)]
    public LocationOption locationOption = LocationOption.useCurrent;
    [InfoBox("The below is only relevent for commerical decisions...", EInfoBoxType.Normal)]
    public CompanyPreset.CompanyCategory desireCategory = CompanyPreset.CompanyCategory.meal;
    [Space(7)]
    public RoomOption roomOption = RoomOption.none;
    [Space(7)]
    public FurnitureOption furnitureOption = FurnitureOption.none;
    [InfoBox("If this is true, the first action's found room location (inside active action) becomes the passed room for the entire goal.", EInfoBoxType.Normal)]
    public bool actionFoundRoomBecomesPassedRoom = false;

    [Header("Action Setup")]
    [Tooltip("Where should this goal get the actions from?")]
    public GoalActionSource actionSource = GoalActionSource.thisConfiguration;
    public List<GoalActionSetup> actionsSetup = new List<GoalActionSetup>();

    [System.Serializable]
    public class GoalActionSetup
    {
        public List<AIActionPreset> actions = new List<AIActionPreset>();
        public ActionCondition condition = ActionCondition.always;
        public float chance = 1f;
        public List<GoalModifierRule> traitModifiers = new List<GoalModifierRule>();
        public List<StatusModifierRule> statusModifiers = new List<StatusModifierRule>();
    }

    [System.Serializable]
    public class StatusModifierRule
    {
        public StatusType status;
        public StatusCondition condition;
        public float value;
        public float chanceModifier;
    }

    public enum StatusType { health, nerve, nourishment, hydration, alertness, energy, excitement, chores, hygeine, bladder, heat, breath, onDutyEnforcer};

    public enum StatusCondition { isEqualOrAbove, isEqualOrBelow, isTrue, isFalse};

    public enum ActionCondition { always, atHomeOnly, inPublicOnly, atWorkOnly, onlyIfEscalated, onlyIfDead, atHomeNoGuestPass, noGuestPass };

    [Tooltip("Potentially raise alarm if certain conditions are met.")]
    public bool raiseAlarm = false;
    [Tooltip("Allow AI to trespass while performing this goal")]
    public bool allowTrespass = false;
    [Tooltip("Disable all action insertions during this goal")]
    public bool disableActionInsertions = false;
    [Tooltip("Send consumables to trash on activation")]
    public bool trashConsumablesOnActivate = true;
    [Tooltip("Disable the ability to throw objects in combat for this goal")]
    public bool disableThrowing = false;
    [Tooltip("Disable trigger to mugging if this goal is active")]
    public bool diabledMugging = false;

    [Space(5)]
    [Tooltip("Pottering: Occasionally the AI will insert one of these actions into the goal.")]
    public bool allowPottering = false;
    [EnableIf("allowPottering")]
    public GoalActionSource potterSource = GoalActionSource.thisConfiguration;
    [EnableIf("allowPottering")]
    [Tooltip("How often the AI 'potters'. Can be overridden by the above setting")]
    public Vector2 potterFrequency = new Vector2(0.5f, 1f);
    [EnableIf("allowPottering")]
    [ReorderableList]
    public List<AIActionPreset> potterActions = new List<AIActionPreset>();


    public enum GoalActionSource { thisConfiguration, jobPreset, murderPreset};

    //public enum LightingPreference { mainOn, secondaryOn, eitherPriorityMain, eitherPrioritySecondary, secondaryInEvening, off, doesntMatter};
    //[Header("Basic Actions")]
    //[Tooltip("Turn lights in this room to...")]
    //public LightingPreference lightingPreference = LightingPreference.mainOn;
    //[Tooltip("Execute the switching on/off of lights as below:")]
    //public AIActionPreset.LightRule lightRule = AIActionPreset.LightRule.normal;

    [Tooltip("Override the location's lighting behaviour")]
    public bool overrideLightingBehaviour = false;
    [EnableIf("overrideLightingBehaviour")]
    public bool onlyOverrideIfAtGamelocation = true;
    [EnableIf("overrideLightingBehaviour")]
    public List<RoomConfiguration.AILightingBehaviour> lightingBehaviour = new List<RoomConfiguration.AILightingBehaviour>();

    [Tooltip("Execute the closing of doors as below:")]
    public AIActionPreset.DoorRule doorRule = AIActionPreset.DoorRule.normal;

    [Header("Speech")]
    [Range(0f, 1f)]
    public float chanceOfOnTrigger = 0.5f;
    public List<SpeechController.Bark> onTriggerBark = new List<SpeechController.Bark>();
}
