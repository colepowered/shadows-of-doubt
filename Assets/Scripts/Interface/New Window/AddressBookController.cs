﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AddressBookController : MonoBehaviour
{
    public WindowContentController windowContent;
    public InfoWindow parentWindow;
    public TextMeshProUGUI descriptionText;

    private void OnEnable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        if (windowContent == null) windowContent = this.gameObject.GetComponentInParent<WindowContentController>();
        parentWindow.passedEvidence.OnDataKeyChange += CheckEnabled;
        parentWindow.OnWindowRefresh += CheckEnabled;

        CheckEnabled();
    }

    private void OnDisable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange -= CheckEnabled;
        parentWindow.OnWindowRefresh -= CheckEnabled;
    }

    public void CheckEnabled()
    {
        descriptionText.text = string.Empty;

        HashSet<Human> closedSet = new HashSet<Human>();
        string str = string.Empty;

        NewAddress address = null;
        Evidence ev = parentWindow.passedEvidence;

        if (parentWindow.passedInteractable != null)
        {
            Interactable.Passed p = parentWindow.passedInteractable.pv.Find(item => item.varType == Interactable.PassedVarType.addressID);

            if (p != null)
            {
                CityData.Instance.addressDictionary.TryGetValue(Mathf.RoundToInt(p.value), out address);
            }
        }
        else if (parentWindow.passedEvidence.interactable != null)
        {
            Interactable.Passed p = parentWindow.passedEvidence.interactable.pv.Find(item => item.varType == Interactable.PassedVarType.addressID);

            if(p != null)
            {
                CityData.Instance.addressDictionary.TryGetValue(Mathf.RoundToInt(p.value), out address);
            }
        }
        
        //Backup old method; grab address from user (you can delete this after several version past 32.01 when the above changes will propagate properly into generated cities)
        if(parentWindow.passedInteractable != null)
        {
            if(parentWindow.passedInteractable.belongsTo != null)
            {
                address = parentWindow.passedInteractable.belongsTo.home;
            }
        }

        if (address == null) return;

        Game.Log("Checking " + address.inhabitants.Count + " inhabitants...");

        HashSet<string> existinNames = new HashSet<string>();

        List<Human> livesHere = new List<Human>(address.inhabitants);
        livesHere.Sort((p1, p2) => p1.humanID.CompareTo(p2.humanID));

        //Foreach inhabitant of home, add address
        foreach (Human h in livesHere)
        {
            Game.Log(h.GetCitizenName() + " has " + h.acquaintances.Count + " aquaintances...");

            string citHandwriting = string.Empty;
            if (h.handwriting != null) citHandwriting = "<font=\"" + h.handwriting.fontAsset.name + "\">";

            List<Acquaintance> knows = new List<Acquaintance>(h.acquaintances);
            knows.Sort((p2, p1) => p1.known.CompareTo(p2.known));

            foreach (Acquaintance aq in knows)
            {
                if (aq.with.home == null || aq.from.home == null) continue; //Skip if homeless

                if (h != aq.with && (aq.with.home.telephones == null || aq.with.home.telephones.Count <= 0))
                {
                    Game.Log(aq.with.GetCitizenName() + " has no telephone at their home!");
                    continue; //No telephone
                }

                if (h != aq.from && (aq.from.home.telephones == null || aq.from.home.telephones.Count <= 0))
                {
                    Game.Log(aq.from.GetCitizenName() + " has no telephone at their home!");
                    continue; //No telephone
                }

                if (aq.known < SocialControls.Instance.telephoneBookInclusionThreshold) continue; //Skip entry if not well known enough

                Game.Log("Aq know enough to feature: " + aq.known);

                if (!closedSet.Contains(aq.with) && !address.inhabitants.Contains(aq.with))
                {
                    //Add fact if this doesn't exist...
                    if(ev != null)
                    {
                        //ev.AddFactLink(aq.with.factDictionary["LivesAt"], Evidence.DataKey.name, false);
                        //ev.AddFactLink(EvidenceCreator.Instance.CreateFact("ReferencedIn", aq.with.evidenceEntry, ev), Evidence.DataKey.name, false);
                    }

                    //Create link data: The constructor will assign a link ID you can use directly in compliation of the directory text.
                    Strings.LinkData adLink = Strings.AddOrGetLink(aq.with.home.evidenceEntry, Toolbox.Instance.GetList<Evidence.DataKey>(Evidence.DataKey.name, Evidence.DataKey.location, Evidence.DataKey.address, Evidence.DataKey.telephoneNumber));
                    Strings.LinkData phoneLink = Strings.AddOrGetLink(aq.with.home.telephones[0].telephoneEntry, Toolbox.Instance.GetList<Evidence.DataKey>(Evidence.DataKey.initialedName, Evidence.DataKey.address, Evidence.DataKey.telephoneNumber));
                    Strings.LinkData nameLink = null;

                    string casualName = string.Empty;

                    if(existinNames.Contains(casualName))
                    {
                        casualName = aq.with.GetInitialledName();
                        nameLink = Strings.AddOrGetLink(aq.with.evidenceEntry, Toolbox.Instance.GetList<Evidence.DataKey>(Evidence.DataKey.initialedName, Evidence.DataKey.address, Evidence.DataKey.telephoneNumber));
                    }
                    else
                    {
                        casualName = aq.with.GetCasualName();
                        nameLink = Strings.AddOrGetLink(aq.with.evidenceEntry, Toolbox.Instance.GetList<Evidence.DataKey>(Evidence.DataKey.firstName, Evidence.DataKey.address, Evidence.DataKey.telephoneNumber));
                    }

                    existinNames.Add(casualName);
                    
                    str += '\n' + citHandwriting + "<link=" + nameLink.id.ToString() + ">" + casualName + "</link>" + '\n' + "<link=" + adLink.id.ToString() + ">" + aq.with.home.name + "</link>" + '\n' + "<link=" + phoneLink.id.ToString() + ">" + aq.with.home.telephones[0].numberString + "</link>" + '\n';
                        
                    closedSet.Add(aq.with);
                }
                else
                {
                    Game.Log("Closed set contains " + aq.with.GetCitizenName());
                }

                Game.Log("Aq from " + aq.from.GetCitizenName());

                if (!closedSet.Contains(aq.from) && !address.inhabitants.Contains(aq.from) && aq.from.home != null && aq.from.home.telephones.Count > 0)
                {
                    //Create link data: The constructor will assign a link ID you can use directly in compliation of the directory text.
                    //Strings.LinkData adLink = Strings.AddOrGetLink(aq.from.home.evidenceEntry, Toolbox.Instance.GetList<Evidence.DataKey>(Evidence.DataKey.name, Evidence.DataKey.address, Evidence.DataKey.telephoneNumber));
                    //Strings.LinkData citLink = Strings.AddOrGetLink(aq.from.evidenceEntry, Toolbox.Instance.GetList<Evidence.DataKey>(Evidence.DataKey.name, Evidence.DataKey.address, Evidence.DataKey.telephoneNumber));
                    //Strings.LinkData phoneLink = Strings.AddOrGetLink(aq.from.home.telephones[0].telephoneEntry, Toolbox.Instance.GetList<Evidence.DataKey>(Evidence.DataKey.name, Evidence.DataKey.address, Evidence.DataKey.telephoneNumber));

                    //str += '\n' + citHandwriting + "<link=" + citLink.id.ToString() + ">" + aq.from.name + "</link>" + '\n' + "<link=" + adLink.id.ToString() + ">" + aq.from.home.name + "</link>" + '\n' + "<link=" + phoneLink.id.ToString() + ">" + aq.from.home.telephones[0].numberString + "</link>" + '\n';
                    //closedSet.Add(aq.from);

                    //Add fact if this doesn't exist
                    if(ev != null)
                    {
                        //ev.AddFactLink(aq.from.factDictionary["LivesAt"], Evidence.DataKey.name, false);
                        //ev.AddFactLink(EvidenceCreator.Instance.CreateFact("ReferencedIn", aq.from.evidenceEntry, ev), Evidence.DataKey.name, false);
                    }

                    //Create link data: The constructor will assign a link ID you can use directly in compliation of the directory text.
                    Strings.LinkData adLink = Strings.AddOrGetLink(aq.from.home.evidenceEntry, Toolbox.Instance.GetList<Evidence.DataKey>(Evidence.DataKey.initialedName, Evidence.DataKey.address, Evidence.DataKey.telephoneNumber));
                    Strings.LinkData phoneLink = Strings.AddOrGetLink(aq.from.home.telephones[0].telephoneEntry, Toolbox.Instance.GetList<Evidence.DataKey>(Evidence.DataKey.initialedName, Evidence.DataKey.address, Evidence.DataKey.telephoneNumber));
                    Strings.LinkData nameLink = null;

                    string casualName = string.Empty;

                    if (existinNames.Contains(casualName))
                    {
                        casualName = aq.from.GetInitialledName();
                        nameLink = Strings.AddOrGetLink(aq.from.evidenceEntry, Toolbox.Instance.GetList<Evidence.DataKey>(Evidence.DataKey.initialedName, Evidence.DataKey.address, Evidence.DataKey.telephoneNumber));
                    }
                    else
                    {
                        casualName = aq.from.GetCasualName();
                        nameLink = Strings.AddOrGetLink(aq.from.evidenceEntry, Toolbox.Instance.GetList<Evidence.DataKey>(Evidence.DataKey.firstName, Evidence.DataKey.address, Evidence.DataKey.telephoneNumber));
                    }

                    existinNames.Add(casualName);

                    str += '\n' + citHandwriting + "<link=" + nameLink.id.ToString() + ">" + casualName + "</link>" + '\n' + "<link=" + adLink.id.ToString() + ">" + aq.from.home.name + "</link>" + '\n' + "<link=" + phoneLink.id.ToString() + ">" + aq.from.home.telephones[0].numberString + "</link>" + '\n';

                    closedSet.Add(aq.with);
                }
                else
                {
                    Game.Log("Closed set contains " + aq.from.GetCitizenName());
                }
            }
        }

        descriptionText.text = str;
    }
}
