﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using TMPro;

public class ObjectivesContentController : MonoBehaviour
{
    [Header("References")]
    public WindowContentController wcc;
    public RectTransform pageRect;
    public RectTransform objectiveContainer;
    public SideJob job;
    public TextMeshProUGUI jobDetails;

    public List<ObjectiveContentListEntry> spawnedStartingObjectives = new List<ObjectiveContentListEntry>();

    [Header("Prefabs")]
    public GameObject elementPrefab;

    public void Setup(WindowContentController newWcc)
    {
        wcc = newWcc;

        //Get the job...
        if(wcc.window.passedEvidence != null && wcc.window.passedEvidence.interactable != null && wcc.window.passedEvidence.interactable.jobParent != null)
        {
            job = wcc.window.passedEvidence.interactable.jobParent;
        }
        else if(wcc.window.passedInteractable != null && wcc.window.passedInteractable.jobParent != null)
        {
            job = wcc.window.passedInteractable.jobParent;
        }

        //Listen for job objectives change
        if(job != null) job.AcquireInfo += UpdateJobDetails;

        UpdateJobDetails();
    }

    private void OnDestroy()
    {
        if (job != null) job.AcquireInfo -= UpdateJobDetails;
    }

    public void UpdateJobDetails()
    {
        if(job != null && job.knowHandInLocation)
        {
            if(job.jobInfoDialogMsg == null || job.jobInfoDialogMsg.Length <= 0)
            {
                jobDetails.text = Strings.Get("ui.interface", "See job note for details.");
            }
            else
            {
                jobDetails.text = Strings.GetTextForComponent(job.jobInfoDialogMsg, job, null, linkSetting: Strings.LinkSetting.forceLinks, lineBreaks: "\n\n");
            }
        }
        else
        {
            jobDetails.text = Strings.Get("ui.interface", "Persue current objectives detailed in the job note to acquire more details about the job...");
        }

        jobDetails.ForceMeshUpdate();

        //Set size of page
        pageRect.sizeDelta = new Vector2(pageRect.sizeDelta.x, Mathf.Max(jobDetails.GetPreferredValues().y + 32, 466));

        //Make sure scroll is at top
        pageRect.anchoredPosition = new Vector2(0, pageRect.sizeDelta.y * -0.5f);
    }
}
