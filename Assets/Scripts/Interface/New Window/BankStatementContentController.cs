﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class BankStatementContentController : MonoBehaviour
{
    public WindowContentController windowContent;
    public InfoWindow parentWindow;
    public TextMeshProUGUI descriptionText;
    public GameObject entryPrefab;
    public string transactionMessageID;
    public VerticalLayoutGroup entryLayoutGroup;
    public List<BankStatementEntryController> spawnedEntries = new List<BankStatementEntryController>();

    public class Transaction
    {
        public string text;
        public int amount;
    }

    private void OnEnable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        if (windowContent == null) windowContent = this.gameObject.GetComponentInParent<WindowContentController>();

        if(parentWindow != null)
        {
            if(parentWindow.passedEvidence != null) parentWindow.passedEvidence.OnDataKeyChange += CheckEnabled;
            parentWindow.OnWindowRefresh += CheckEnabled;
        }

        CheckEnabled();
    }

    private void OnDisable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange -= CheckEnabled;
        parentWindow.OnWindowRefresh -= CheckEnabled;
    }

    public void CheckEnabled()
    {
        //Spawn entries
        while (spawnedEntries.Count > 0)
        {
            Destroy(spawnedEntries[0].gameObject);
            spawnedEntries.RemoveAt(0);
        }

        if (parentWindow != null && parentWindow.passedEvidence != null)
        {
            Human belongsTo = parentWindow.passedEvidence.reciever;

            List<Transaction> transactionPool = new List<Transaction>();
            List<int> parsedDisplayGroups = new List<int>();

            //Parse this in the regular fashion...
            List<string> parsed = belongsTo.ParseDDSMessage(transactionMessageID, null, out parsedDisplayGroups, passedObject: belongsTo, debug: true);

            Game.Log("Bank statement: Parsed " + parsed.Count + " blocks from " + transactionMessageID + " " + belongsTo.citizenName);

            for (int i = 0; i < parsed.Count; i++)
            {
                string str = parsed[i];
                int grp = 1;

                try
                {
                    grp = parsedDisplayGroups[i];
                }
                catch
                {

                }

                //Use assigned group to dictate the price
                int amount = -Toolbox.Instance.GetPsuedoRandomNumber((grp - 1) * 50, grp * 50, str);

                //If salary, override amount
                if(grp == 52 && belongsTo.job != null)
                {
                    amount = Mathf.RoundToInt((belongsTo.job.salary * 1000) / 12f);
                }

                Transaction transaction = new Transaction();
                transaction.text = Strings.ComposeText(Strings.Get("dds.blocks", str), belongsTo);
                transaction.amount = amount;

                transactionPool.Add(transaction);
            }

            //Shuffle transaction pool
            for (int i = 0; i < transactionPool.Count; i++)
            {
                Transaction temp = transactionPool[i];
                int randomIndex = Toolbox.Instance.GetPsuedoRandomNumber(i, transactionPool.Count, belongsTo.citizenName + CityData.Instance.seed);
                transactionPool[i] = transactionPool[randomIndex];
                transactionPool[randomIndex] = temp;
            }

            float socClassMp = 1f;

            foreach(CharacterTrait t in CitizenControls.Instance.savingsBoostTrait)
            {
                if(belongsTo.characterTraits.Exists(item => item.trait == t))
                {
                    socClassMp += 0.1f;
                }
            }

            foreach (CharacterTrait t in CitizenControls.Instance.savingsDebuffTrait)
            {
                if (belongsTo.characterTraits.Exists(item => item.trait == t))
                {
                    socClassMp -= 0.1f;
                }
            }

            int startingBalance = Mathf.RoundToInt(CitizenControls.Instance.societalClassSavingsCurve.Evaluate(belongsTo.societalClass * socClassMp));

            float yPos = 0;

            for (int i = 0; i < transactionPool.Count; i++)
            {
                Transaction temp = transactionPool[i];

                GameObject newT = Instantiate(entryPrefab, this.transform);
                BankStatementEntryController bs = newT.GetComponent<BankStatementEntryController>();

                startingBalance += temp.amount;
                bs.Setup(temp.text, temp.amount, startingBalance);
                //bs.rect.anchoredPosition = new Vector2(bs.rect.anchoredPosition.x, yPos);
                yPos += bs.rect.sizeDelta.y;

                spawnedEntries.Add(bs);
            }

            if (windowContent != null && windowContent.pageRect != null)
            {
                Vector2 newPageScale = new Vector2(windowContent.pageRect.sizeDelta.x, Mathf.Max(windowContent.pageRect.sizeDelta.y, yPos + 100));
                Game.Log("Bank statement: New scale: " + newPageScale);

                windowContent.pageRect.sizeDelta = newPageScale;
                windowContent.rect.sizeDelta = newPageScale;
                windowContent.normalSize = newPageScale;
                windowContent.UpdateFitScale();
            }
        }
    }
}
