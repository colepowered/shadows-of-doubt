﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CruncherAppContent : MonoBehaviour
{
    public ComputerController controller;

    public virtual void Setup(ComputerController cc)
    {
        controller = cc;
        OnSetup();
    }

    //Called on setup
    public virtual void OnSetup()
    {

    }

    //Print a document
    public virtual void PrintButton()
    {
        controller.SetTimedLoading(Toolbox.Instance.Rand(0.5f, 1f));
    }

    public void OnPlayerTakePrint()
    {
        controller.printedDocument.OnRemovedFromWorld -= OnPlayerTakePrint;

        //Reset printer position
        controller.printedDocument = null;
        controller.printerParent.localPosition = controller.printOutStartPos;
    }
}
