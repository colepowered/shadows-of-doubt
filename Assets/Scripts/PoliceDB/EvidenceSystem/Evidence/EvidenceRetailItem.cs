﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceRetailItem : Evidence
{
    public Company soldHere;
    public RetailItemPreset retailItem;
    public EvidenceTime purchaseTimeEvidence;
    public float purchaseTime = 0f;
    public bool isAbstract = false;
    public Fact soldAtFact;

    //Constructor
    public EvidenceRetailItem(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        foreach(object obj in newPassedObjects)
        {
            Interactable.Passed passed = obj as Interactable.Passed; //Company is passed

            if (passed != null)
            {
                if(passed.varType == Interactable.PassedVarType.addressID)
                {
                    soldHere = CityData.Instance.companyDirectory.Find(item => item.address.id == (int)passed.value); //Find address id
                }
                else if(passed.varType == Interactable.PassedVarType.time)
                {
                    purchaseTime = passed.value;
                }
            }

            Interactable inter = obj as Interactable;

            if (inter != null)
            {
                interactable = inter;
                interactablePreset = inter.preset;
            }

            //Only include this if this isn't a game world object and is instead a menu item or abstract object
            InteractablePreset interP = obj as InteractablePreset;

            if(interP != null)
            {
                interactablePreset = interP;
                isAbstract = true;
            }

            Company cc = obj as Company;

            if(cc != null)
            {
                soldHere = cc;
                isAbstract = true;
            }

            RetailItemPreset r = obj as RetailItemPreset;

            if (r != null)
            {
                retailItem = r;
            }
        }

        if(interactablePreset == null && retailItem != null)
        {
            interactablePreset = retailItem.itemPreset;
        }

        if(interactablePreset == null && interactable != null)
        {
            interactablePreset = interactable.preset;
        }
    }

    //Override data sources: This needs it's own, not the owners like in EvidenceItem
    public override void BuildDataSources()
    {
        //base.BuildDataSources();

        //if(dataSource == null) dataSource = new Dictionary<string, EvidenceLinkData>();

        if(interactablePreset == null && interactable != null)
        {
            interactablePreset = interactable.preset;
        }

        //if(!dataSource.ContainsKey("summary") && interactablePreset != null)
        //{
        //    dataSource.Add("summary", new EvidenceLinkData(Strings.Casing.asIs, "evidence.body", interactablePreset.name.ToLower(), ev: this));
        //}

        //Add purchased at
        if (soldHere != null)
        {
            //if(!dataSource.ContainsKey("soldhere")) dataSource.Add("soldhere", new EvidenceLinkData(soldHere.name, ev: soldHere.address.evidenceEntry));

            Fact purchasedAt = EvidenceCreator.Instance.CreateFact("PurchasedAt", this, soldHere.address.evidenceEntry) as Fact;
            AddFactLink(purchasedAt, Evidence.DataKey.name, true);
        }
    }

    public override string GetSummary(List<DataKey> keys)
    {
        if(interactablePreset != null && interactablePreset.summaryMessageSource != null && interactablePreset.summaryMessageSource.Length > 0)
        {
            return Strings.GetTextForComponent(interactablePreset.summaryMessageSource, interactable);
        }

        if (retailItem != null && retailItem.itemPreset != null && retailItem.itemPreset.summaryMessageSource != null && retailItem.itemPreset.summaryMessageSource.Length > 0)
        {
            return Strings.GetTextForComponent(retailItem.itemPreset.summaryMessageSource, interactable);
        }

        Game.Log("Unable to get summary for EvidenceRetailItem: No interactable preset or retail item has been passed");

        return base.GetSummary(keys);
    }

    //Name: Use iteractable preset
    public override string GenerateName()
    {
        if (interactablePreset == null && interactable != null)
        {
            interactablePreset = interactable.preset;
        }

        string ret = string.Empty;

        if (interactablePreset != null)
        {
            ret = Strings.Get("evidence.names", interactablePreset.name);
        }

        //Replace with brand name
        if (isAbstract && retailItem != null)
        {
            if (retailItem.brandName.Length > 0)
            {
                ret = Strings.Get("evidence.names", retailItem.brandName);
            }
        }

        //If singleton then include purchase place name
        if (isFound && soldHere != null && isAbstract)
        {
            ret += " " + Strings.Get("evidence.names", "from") + " " + soldHere.name;

            //Add sold fact
            if(soldAtFact == null)
            {
                soldAtFact = EvidenceCreator.Instance.CreateFact("SoldAt", this, soldHere.address.evidenceEntry, forceDiscoveryOnCreate: true);
            }
        }

        return ret;
    }

    //Set ineractable name to evidence name
    public override void OnDiscovery()
    {
        base.OnDiscovery();

        UpdateName();

        //Set note
        if (!isAbstract)
        {
            //Add if this is still warm
            if (retailItem != null && retailItem.isHot)
            {
                //if (!dataSource.ContainsKey("foundtime"))
                //{
                //    dataSource.Add("foundtime", new EvidenceLinkData(SessionData.Instance.TimeStringOnDay(SessionData.Instance.gameTime, true, true)));
                //}

                if (SessionData.Instance.gameTime - purchaseTime <= GameplayControls.Instance.foodHotTime)
                {
                    //SetNote(Toolbox.Instance.allDataKeys, Strings.Get("evidence.body", interactablePreset.name) + " " + Strings.Get("evidence.generic", "stillwarm"));

                    //Time evidence
                    if (purchaseTimeEvidence == null)
                    {
                        purchaseTimeEvidence = EvidenceCreator.Instance.GetTimeEvidenceRange(purchaseTime, GameplayControls.Instance.foodHotTime, true, true, 15, "PurchasedTime", parentID: evID) as EvidenceTime;
                    }
                }
                else
                {
                    //SetNote(Toolbox.Instance.allDataKeys, Strings.Get("evidence.body", interactablePreset.name) + " " + Strings.Get("evidence.generic", "gonecold"));

                    //Time evidence
                    //if (purchaseTimeEvidence == null)
                    //{
                    //    float timeTo = SessionData.Instance.gameTime - GameplayControls.Instance.foodHotTime;
                    //    float timeFrom = SessionData.Instance.gameTime - GameplayControls.Instance.foodHotTime - 48;
                    //    purchaseTimeEvidence = EvidenceCreator.Instance.GetTimeEvidence(timeFrom, timeTo) as EvidenceTime;
                    //    EvidenceCreator.Instance.CreateFact("PurchasedTime", this, purchaseTimeEvidence);
                    //}
                }
            }
            else if (interactablePreset != null)
            {
                //SetNote(Toolbox.Instance.allDataKeys, Strings.Get("evidence.body", interactablePreset.name));
            }
        }
        else if(interactablePreset != null)
        {
            //SetNote(Toolbox.Instance.allDataKeys, Strings.Get("evidence.body", interactablePreset.name + "_singleton"));
        }

        //Update interactable name
        if (interactable != null)
        {
            //Game.Log("Interactable update name = " + GetNameForDataKey(DataKey.name));
            interactable.UpdateName(true, DataKey.name);
        }
    }
}
