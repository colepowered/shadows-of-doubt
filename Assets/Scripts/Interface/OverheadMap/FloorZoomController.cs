﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FloorZoomController : MonoBehaviour
{
    public SliderController floorSlider;
    public TextMeshProUGUI floorText;

    public void AddFloor(int addVal)
    {
        MapController.Instance.SetFloorLayer(MapController.Instance.load + addVal);
    }

    public void OnSliderChangeFloor()
    {
        MapController.Instance.SetFloorLayer(Mathf.RoundToInt(floorSlider.slider.value), false);
    }

    public void CentreOnPlayer()
    {
        MapController.Instance.CentreOnTrackedObject(Player.Instance.transform, true);
    }
}
