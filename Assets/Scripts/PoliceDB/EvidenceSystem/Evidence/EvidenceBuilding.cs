﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;

public class EvidenceBuilding : Evidence
{
    public NewBuilding building;

    //Constructor
    public EvidenceBuilding(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        //Set citizen controller
        building = newController as NewBuilding;
    }

    //Get note with text composed with the data reference from this evidence
    public override string GetNoteComposed(List<Evidence.DataKey> keys, bool useLinks = true)
    {
        Strings.LinkSetting links = Strings.LinkSetting.forceNoLinks;
        if (useLinks) links = Strings.LinkSetting.forceLinks;

        return Strings.ComposeText(GetNote(keys), building, links, evidenceKeys: keys);
    }

    public override string GetSummary(List<DataKey> keys)
    {
        if (building != null && building.floors.ContainsKey(0) && building.floors[0].addresses.Count > 0)
        {
            return Strings.GetTextForComponent("b4195f2d-aea2-438d-a57f-84a71c4540de", building.floors[0].addresses[0], dataKeys: keys);
        }

        return base.GetSummary(keys);
    }

    //Name
    public override string GenerateName()
    {
        return building.name;
    }

    //Return a list of know info 
    public override string GetNote(List<DataKey> keys)
    {
        StringBuilder sb = new StringBuilder();

        //string checkboxEmpty = "<sprite=\"icons\" name=\"Checkbox Empty\">";
        string checkboxChecked = "<sprite=\"icons\" name=\"Checkbox Checked\">";
        string handwritingFont = "<font=\"PapaManAOE SDF\">";
        string fontEnd = "</font>";

        string checkbox = string.Empty;

        //Blueprints/type
        checkbox = checkboxChecked;

        sb.Append(checkbox + Strings.Get("descriptors", "Type", Strings.Casing.firstLetterCaptial) + ": ");
        if(building != null && building.preset != null) sb.Append(handwritingFont + Strings.Get("names.rooms", building.preset.name)  + fontEnd);

        //Location
        checkbox = checkboxChecked;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "District", Strings.Casing.firstLetterCaptial) + ": ");
        if(building != null && building.cityTile != null && building.cityTile.district != null) sb.Append(handwritingFont + building.cityTile.district.name + fontEnd);

        //Other notes
        //sb.Append("\n" + Strings.Get("descriptors", "Additional Notes", Strings.Casing.firstLetterCaptial) + ": \n" + handwritingFont);

        return sb.ToString();
    }
}
