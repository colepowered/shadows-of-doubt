﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using TMPro;

[CreateAssetMenu(fileName = "handwriting_data", menuName = "Database/Handwriting Preset")]

public class HandwritingPreset : SoCustomComparison
{
	[Header("Font")]
	public TMP_FontAsset fontAsset;

	[Header("Suitability")]
	public float baseChance = 0.1f;
	[InfoBox("If enabled: The below traits will be used to calculate the likihood of this being chosen vs others.")]
	[ReorderableList]
	public List<CharacterTrait.TraitPickRule> characterTraits = new List<CharacterTrait.TraitPickRule>();
}
