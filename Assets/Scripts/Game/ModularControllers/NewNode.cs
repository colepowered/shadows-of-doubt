﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System;

public class NewNode
{
    [Header("Transform")]
    public string name;
    public Vector3 position;
    public GameObject physicalObject; //In some cases you may need to spawn this as an actual object...

    [Header("Location")]
    public NewBuilding building; //Reference to the building
    public NewFloor floor; //Reference to the floor
    public NewGameLocation gameLocation; //Reference to the address
    public NewRoom room; //Reference to the room
    public NewTile tile; //Reference to the tile

    [Space(5)]
    public Vector2Int floorCoord; //Coordinate local to the floor (not representitive of actual world position)
    public Vector2Int localTileCoord; //Coordinate local to the tile (not representitive of actual world position)
    public Vector3Int nodeCoord; //Coordinate on a game world level

    [Header("Node Contents")]
    public List<NewWall> walls = new List<NewWall>();
    public Dictionary<Vector2, NewWall> wallDict = new Dictionary<Vector2, NewWall>();
    public int wallDesignation = -1; //-1 means uncalculated

    [Header("Details")]
    public int floorHeight = 0; //The default floor height, nodes will inherit this value
    //public int ceilingHeight = 42; //The default ceiling height, nodes will inherit this value
    public enum FloorTileType { none, floorAndCeiling, floorOnly, CeilingOnly, noneButIndoors };
    public FloorTileType floorType = FloorTileType.floorAndCeiling; //Parameters of the floor
    public List<Vector2> preventEntrances; //Prevent entrances on walls spawned with these offsets...
    public int overwriteLimit = 999; //Used in room generation only to limit the number of times this node can be overwritten

    [Header("Spawned Objects")]
    public GameObject floorPrefab;
    public GameObject spawnedFloor;
    public GameObject ceilingPrefab;
    public GameObject spawnedCeiling;

    [Header("AI Navigation")]
    public float nodeWeightMultiplier = 1f; //The weight of moving to this node from another (used in AI pathing)
    public bool isObstacle = false; //If true then this node is completely blocked off from others
    //public bool isAlley = false; //If true this this is an alley way between buildings
    //public bool isBackstreet = false; //True if this is street space behind a building
    public bool isOutside = false; //True if this is exterior
    public bool isConnected = false; //True if this node has been connected to adjacent NOTE: This is now used for stairwells only
    public bool stairwellLowerLink = false; //If true this is the link to the bottom stair (same floor)
    public bool stairwellUpperLink = false; //If true this is the link to the top stair (above floor)
    public bool isInaccessable = false; //If true, this can only be accessed by nodes above or below it, therefore it cannot be used in the pathfinder...
    public bool isSetup = false;
    public bool isIndoorsEntrance = false; //True if this node is on one end of an entrance/exit to indoors/outside
    public bool ceilingAirVent = false; //True if these is a ceiling air vent on this node
    public bool floorAirVent = false;
    public bool noPassThrough = false; //If true an AI can walk here but not pass through it en-route
    public bool noAccess = false; //If true AI cannot access this node at all
    [System.NonSerialized] //If this is serialized it cannot be set to null!
    public RoomConfiguration forcedRoom; //Forced room setting
    public string forcedRoomRef; //Reference string including the address preset parent for above
    //public Dictionary<NewNode, float> entranceWeights = new Dictionary<NewNode, float>(); //If this is an entrance, this will be a reference to the distance of every other room entrance. IMPORTANT: Uses the 'other' room node as otherwise this won't support multiple doorways...

    //Multiple AI navigation
    public class NodeSpace
    {
        public NewNode node;
        public NodeSpaceOccupancy occ = NodeSpaceOccupancy.empty;
        public Actor occupier;
        public Vector3 position; //World position

        public void SetEmpty()
        {
            occupier = null;
            occ = NodeSpaceOccupancy.empty;

            if(node.occupiedSpace.Contains(this))
            {
                node.occupiedSpace.Remove(this);
            }
        }

        public void SetOccuppier(Actor newOcc, NodeSpaceOccupancy occType)
        {
            //Set empty first
            if(occupier != newOcc && occupier != null)
            {
                SetEmpty();
            }

            occupier = newOcc;
            occ = occType;

            if(!node.occupiedSpace.Contains(this))
            {
                node.occupiedSpace.Add(this);
            }

            if(occ == NodeSpaceOccupancy.reserved)
            {
                occupier.AddReservedNodeSpace(this);
            }
        }
    }

    public enum NodeSpaceOccupancy { empty, position, reserved};

    //private Dictionary<Human, NodeSpace> referenceByHuman = new Dictionary<Human, NodeSpace>(); //Reference to the above using humans
    //private Dictionary<Vector3, List<NodeSpace>> referenceByPos = new Dictionary<Vector3, List<NodeSpace>>(); //Reference to the above using local pos

    //New node space system
    public NodeSpace defaultSpace; //The space to use when the node is empty or just by default (usually the middle)
    public Dictionary<Vector3, NodeSpace> walkableNodeSpace = new Dictionary<Vector3, NodeSpace>(); //Reference to all node space on this node
    public HashSet<NodeSpace> occupiedSpace = new HashSet<NodeSpace>(); //Holds references to reserved or occupied space
    public bool detectGeometry = true;

    //public Dictionary<Human, Vector3> humansTravelling = new Dictionary<Human, Vector3>(); //Human actors traveling to or on this node.
    //private Dictionary<Vector3, Human> travellingHumans = new Dictionary<Vector3, Human>(); //Inverse of the above so we can quickly check using either reference.
    //public List<Vector3> debugTakenPositions = new List<Vector3>();

    [Header("Furniture")]
    public bool allowNewFurniture = true; //If true, placement of furniture that's not on wall or a rig is allowed
    //public bool walkableTile = true; //If true the ai can access this node properly through walking
    public List<FurnitureLocation> individualFurniture = new List<FurnitureLocation>(); //Created from the above, reference to the individual furniture objects

    [Header("Interactables")]
    public List<Interactable> interactables = new List<Interactable>(); //List of interactables on this node

    [Header("Air Ducts")]
    public List<AirDuctGroup.AirDuctSection> airDucts = new List<AirDuctGroup.AirDuctSection>();

    [Header("Audio Source")]
    public AudioEvent audioEvent;
    public AudioController.LoopingSoundInfo loop;
    public Vector3 audioOffset;

    //[Header("Debug")]
    //public List<string> parentDebug = new List<string>();
    //public List<string> floorDebug = new List<string>();
    //public List<string> debugPossibleFurniture = new List<string>();
    //public int entranceWeightCount = 0;

    [Button]
    public void TestForRoomEntrnace()
    {
        if (room.entrances.Exists(item => item.fromNode == this))
        {
            Game.Log("Parent room contains an entrance from this node!");
        }
        else Game.Log("Parent room DOES NOT contains an entrance from this node!");
    }

    //This class contains data needed for travelling from one node to another- including weight and doorway
    [System.Serializable]
    public class NodeAccess : IEquatable<NodeAccess>
    {
        public string name;
        public int id = 0;
        public static int assignId = 0;

        public float weight = 1.8f; //The overall cost of moving here: Usually game distance plus any extra weight
        public NewDoor door; //The doorway will point towards the parent wall, ie the parent of the actual door
        public NewWall wall; //The wall between the two nodes

        //Street to street: Connections between streets, door, openDoorway: Doorway but no door, verticalSpace: Stairs/elevator, adjacent: Adjacent node
        public enum AccessType { streetToStreet, door, openDoorway, verticalSpace, adjacent, window, bannister};
        public AccessType accessType = AccessType.adjacent;

        public NewNode fromNode; //In entrances, 'From' is the node inside the room
        public NewNode toNode; //In entrances, 'to' is the node outside the room

        public bool walkingAccess = true; //If true AI can walk here...
        public bool employeeDoor = false; //If true only employees can use this door. Won't be included on most pathfinding routes.

        public Vector3 worldAccessPoint = Vector3.zero; //The middle point between the two nodes in world space
        public NodeAccess oppositeAccess; //The opposing access

        public Dictionary<NodeAccess, float> entranceWeights = new Dictionary<NodeAccess, float>(); //Weights leading to other gamelocation entrances/exits

        //public int debugFloor;
        //public string debugInfo;

        private bool hasHash;
        private int hash;

        bool IEquatable<NodeAccess>.Equals(NodeAccess other)
        {
            return other.GetHashCode() == GetHashCode();
        }

        public override int GetHashCode()
        {
            if (!hasHash)
            {
                hash = HashCode.Combine(name, id);
                hasHash = true;
            }
            return hash;
        }

        public NodeAccess(NewNode newFrom, NewNode newTo, NewWall newWall, NewDoor newDoorway, bool forceAccessType = false, AccessType forcedAccessType = AccessType.adjacent, bool forceWalkable = false/*, string newDebugInfo = ""*/)
        {
            //Assign ID
            id = assignId;
            assignId++;
            fromNode = newFrom;
            toNode = newTo;
            wall = newWall;
            door = newDoorway;
            //debugFloor = (int)toNode.nodeCoord.z;
            //debugInfo = newDebugInfo;

            if (forceAccessType)
            {
                accessType = forcedAccessType;
            }
            else
            {
                accessType = AccessType.adjacent;

                if (newFrom.room != newTo.room)
                {
                    accessType = AccessType.openDoorway;
                }

                if (door != null)
                {
                    accessType = AccessType.door;

                    if (toNode.gameLocation != fromNode.gameLocation)
                    {
                        //Determin if this is an employee-only door...
                        //Only true if a public company has more than 1 entrance
                        if (fromNode.gameLocation.thisAsAddress != null)
                        {
                            if (fromNode.gameLocation.thisAsAddress.company != null && fromNode.gameLocation.thisAsAddress.residence == null)
                            {
                                if (fromNode.gameLocation.thisAsAddress.company.publicFacing)
                                {
                                    //This is a main entrance if it opens onto the street, or employee only if it doesn't
                                    if (toNode.gameLocation.thisAsStreet == null && fromNode.room.preset != null && fromNode.room.preset.forbidden == RoomConfiguration.Forbidden.alwaysForbidden && (fromNode.floor == null || fromNode.floor.floor == 0))
                                    {
                                        employeeDoor = true;
                                    }
                                }
                            }

                            if(toNode.gameLocation.thisAsStreet != null)
                            {
                                fromNode.isIndoorsEntrance = true;
                                toNode.isIndoorsEntrance = true;
                            }
                        }

                        if (toNode.gameLocation.thisAsAddress != null)
                        {
                            if (toNode.gameLocation.thisAsAddress.company != null && toNode.gameLocation.thisAsAddress.residence == null)
                            {
                                if (toNode.gameLocation.thisAsAddress.company.publicFacing)
                                {
                                    //This is a main entrance if it opens onto the street, or employee only if it doesn't
                                    if (fromNode.gameLocation.thisAsStreet == null && toNode.room.preset != null && toNode.room.preset.forbidden == RoomConfiguration.Forbidden.alwaysForbidden && (toNode.floor == null || toNode.floor.floor == 0))
                                    {
                                        employeeDoor = true;
                                    }
                                }
                            }

                            if (fromNode.gameLocation.thisAsStreet != null)
                            {
                                fromNode.isIndoorsEntrance = true;
                                toNode.isIndoorsEntrance = true;
                            }
                        }
                    }

                    //Building entrance
                    if(toNode.building != fromNode.building)
                    {
                        //Inside facing out
                        if(wall.node.building != null && wall.otherWall.node.building == null)
                        {
                            if(wall.node.tile.isMainEntrance && wall.node.floor.floor == 0)
                            {
                                wall.node.building.AddBuildingEntrance(wall, true);
                            }
                            else if(wall.node.tile.isEntrance && wall.node.floor.floor == 0)
                            {
                                wall.node.building.AddBuildingEntrance(wall, false);
                            }
                        }
                        //Outside facing in
                        if (wall.node.building == null && wall.otherWall.node.building != null)
                        {
                            if (wall.otherWall.node.tile.isMainEntrance && wall.otherWall.node.floor.floor == 0)
                            {
                                wall.otherWall.node.building.AddBuildingEntrance(wall.otherWall, true);
                            }
                            else if (wall.otherWall.node.tile.isEntrance && wall.otherWall.node.floor.floor == 0)
                            {
                                wall.otherWall.node.building.AddBuildingEntrance(wall.otherWall, false);
                            }
                        }
                    }
                }
                else
                {
                    if (wall != null && (wall.preset.sectionClass == DoorPairPreset.WallSectionClass.window || wall.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge || wall.preset.sectionClass == DoorPairPreset.WallSectionClass.wall))
                    {
                        if (!wall.node.room.IsOutside() && !wall.otherWall.node.room.IsOutside() && wall.node.building == wall.otherWall.node.building)
                        {
                            accessType = AccessType.bannister;
                        }
                        else
                        {
                            accessType = AccessType.window;

                            if(wall == null)
                            {
                                Game.LogError("No wall for window!");
                            }
                        }
                    }
                    else
                    {
                        //Nodes are in different streets
                        if (newFrom.gameLocation.thisAsStreet != null && newTo.gameLocation.thisAsStreet != null && newFrom.gameLocation != newTo.gameLocation)
                        {
                            accessType = AccessType.streetToStreet;
                        }
                        //Nodes are in different vertical space
                        else if (fromNode.nodeCoord.z != toNode.nodeCoord.z)
                        {
                            accessType = AccessType.verticalSpace;
                        }
                    }
                }
            }

            if(forceWalkable)
            {
                walkingAccess = true;
            }
            else if(accessType == AccessType.bannister || accessType == AccessType.window || accessType == AccessType.verticalSpace || fromNode.isObstacle || toNode.isObstacle)
            {
                walkingAccess = false;
            }

            //Calculate weight
            weight = Vector3.Distance(fromNode.position, toNode.position) * toNode.nodeWeightMultiplier;
            if(Game.Instance.devMode && Game.Instance.collectDebugData) name = fromNode.room.name + " -> " + toNode.room.name + "("+weight +")";

            //Calculate world access point
            worldAccessPoint = (newFrom.position + newTo.position) * 0.5f;

            if(toNode.accessToOtherNodes.ContainsKey(fromNode))
            {
                oppositeAccess = toNode.accessToOtherNodes[fromNode];
            }

            //Add reference
            PathFinder.Instance.nodeAccessReference.Add(id, this);
        }

        //Get the 'other' node from this
        public NewNode GetOther(NewNode fromThis)
        {
            if (fromNode == fromThis) return toNode;
            else return fromNode;
        }

        public NewRoom GetOtherRoom(NewRoom fromThis)
        {
            if (fromNode.room == fromThis) return toNode.room;
            else return fromNode.room;
        }

        public NewRoom GetOtherRoom(NewGameLocation fromThis)
        {
            if (fromNode.gameLocation == fromThis) return toNode.room;
            else return fromNode.room;
        }

        //Also check against streets vs addresses
        public NewNode GetOtherGameLocation(NewNode fromThis)
        {
            if(fromNode.gameLocation != fromThis.gameLocation)
            {
                //Would normally return from node, as gamelocation is different. But they can be different but also both the street, in which case, check the to node
                if(fromNode.gameLocation.thisAsStreet != null && fromThis.gameLocation.thisAsStreet != null)
                {
                    if (toNode.gameLocation != fromThis.gameLocation)
                    {
                        //The to node is a different game location, and also an address. This is the correct return.
                        if (toNode.gameLocation.thisAsStreet == null)
                        {
                            return toNode;
                        }
                    }
                }

                return fromNode;
            }

            return toNode; //If the fromNode is the same as the fromThis, then the fromNode shouldn't be chosen no matter what.
        }

        public NewGameLocation GetOtherGameLocation(NewGameLocation fromThis)
        {
            if (fromNode.gameLocation != fromThis)
            {
                //Would normally return from node, as gamelocation is different. But they can be different but also both the street, in which case, check the to node
                if (fromNode.gameLocation.thisAsStreet != null && fromThis.thisAsStreet != null)
                {
                    if (toNode.gameLocation != fromThis)
                    {
                        //The to node is a different game location, and also an address. This is the correct return.
                        if (toNode.gameLocation.thisAsStreet == null)
                        {
                            return toNode.gameLocation;
                        }
                    }
                }

                return fromNode.gameLocation;
            }

            return toNode.gameLocation; //If the fromNode is the same as the fromThis, then the fromNode shouldn't be chosen no matter what.
        }

        public void PreComputeEntranceWeights()
        {
            //Disable employee door if this is the only entrance...
            if(employeeDoor)
            {
                if (fromNode.gameLocation.entrances.Exists(item => item != this && item.accessType == AccessType.door && !item.employeeDoor))
                {

                }
                else employeeDoor = false; //Cancel employee door if only entrance

                if (toNode.gameLocation.entrances.Exists(item => item != this && item.accessType == AccessType.door && !item.employeeDoor))
                {

                }
                else employeeDoor = false; //Cancel employee door if only entrance
            }

            if(fromNode.gameLocation.thisAsAddress != null)
            {
                foreach(NewNode.NodeAccess access1 in fromNode.gameLocation.entrances)
                {
                    if (access1 == null) continue;

                    //Must be a valid entrance...
                    if (!access1.walkingAccess) continue;
                    if (access1.employeeDoor) continue;

                    if(!entranceWeights.ContainsKey(access1))
                    {
                        try
                        {
                            entranceWeights.Add(access1, Vector3.Distance(worldAccessPoint, access1.worldAccessPoint));
                        }
                        catch
                        {

                        }
                    }
                }
            }
            else
            {
                foreach (NewNode.NodeAccess access1 in PathFinder.Instance.streetEntrances)
                {
                    if (access1 == null) continue;

                    //Must be a valid entrance...
                    if (!access1.walkingAccess) continue;
                    if (access1.employeeDoor) continue;

                    if (!entranceWeights.ContainsKey(access1))
                    {
                        try
                        {
                            entranceWeights.Add(access1, Vector3.Distance(worldAccessPoint, access1.worldAccessPoint));
                        }
                        catch
                        {

                        }
                    }
                }
            }

            if (toNode.gameLocation.thisAsAddress != null)
            {
                foreach (NewNode.NodeAccess access1 in toNode.gameLocation.entrances)
                {
                    if (access1 == null) continue;

                    //Must be a valid entrance...
                    if (!access1.walkingAccess) continue;

                    if (!entranceWeights.ContainsKey(access1))
                    {
                        try
                        {
                            entranceWeights.Add(access1, Vector3.Distance(worldAccessPoint, access1.worldAccessPoint));
                        }
                        catch
                        {

                        }
                    }
                }
            }
            else
            {
                foreach (NewNode.NodeAccess access1 in PathFinder.Instance.streetEntrances)
                {
                    if (access1 == null) continue;

                    //Must be a valid entrance...
                    if (!access1.walkingAccess) continue;

                    if (!entranceWeights.ContainsKey(access1))
                    {
                        try
                        {
                            entranceWeights.Add(access1, Vector3.Distance(worldAccessPoint, access1.worldAccessPoint));
                        }
                        catch
                        {

                        }
                    }
                }
            }
        }
    }

    //Key == Other node, Value == access data
    public Dictionary<NewNode, NodeAccess> accessToOtherNodes = new Dictionary<NewNode, NodeAccess>();
    //public List<NodeAccess> accessDebug = new List<NodeAccess>(); //Exists just for viewing in the inspector

    //Setup
    public void Setup(NewTile newTile, NewGameLocation newGameLoc, Vector2Int newLocalCoord)
    {
        newTile.AddNewNode(this);
        newGameLoc.AddNewNode(this); //Add to address

        //this.transform.SetParent(tile.gameObject.transform, true);
        //parentDebug.Add("Setup parent to tile");

        gameLocation.nullRoom.AddNewNode(this); //Add to default room

        //Set coordinates
        localTileCoord = newLocalCoord;
        floorCoord = new Vector2Int((tile.floorCoord.x * CityControls.Instance.nodeMultiplier) + localTileCoord.x, (tile.floorCoord.y * CityControls.Instance.nodeMultiplier) + localTileCoord.y);

        //Inherit default ceiling & floor values
        if (floor != null)
        {
            floorHeight = floor.defaultFloorHeight;
            //ceilingHeight = floor.defaultCeilingHeight;

            //Override floor/ceiling height with room settings
            if(room.roomType.overrideFloorHeight)
            {
                SetFloorHeight(room.roomType.floorHeight);
            }
            else SetFloorHeight(floor.defaultFloorHeight);

            //Add to floor's node map
            if (!floor.nodeMap.ContainsKey(floorCoord))
            {
                floor.nodeMap.Add(floorCoord, this);
            }
        }

        //Set node coordinate & add to pathfinder node map
        nodeCoord = new Vector3Int(newTile.globalTileCoord.x * CityControls.Instance.nodeMultiplier + newLocalCoord.x, newTile.globalTileCoord.y * CityControls.Instance.nodeMultiplier + newLocalCoord.y, newTile.globalTileCoord.z);

        if(!PathFinder.Instance.nodeMap.ContainsKey(nodeCoord))
        {
            PathFinder.Instance.nodeMap.Add(nodeCoord, this);

            PathFinder.Instance.nodeRangeX.x = Mathf.Min(nodeCoord.x, PathFinder.Instance.nodeRangeX.x);
            PathFinder.Instance.nodeRangeX.y = Mathf.Max(nodeCoord.x, PathFinder.Instance.nodeRangeX.y);

            PathFinder.Instance.nodeRangeY.x = Mathf.Min(nodeCoord.y, PathFinder.Instance.nodeRangeY.x);
            PathFinder.Instance.nodeRangeY.y = Mathf.Max(nodeCoord.y, PathFinder.Instance.nodeRangeY.y);

            PathFinder.Instance.nodeRangeZ.x = Mathf.Min(nodeCoord.z, PathFinder.Instance.nodeRangeZ.x);
            PathFinder.Instance.nodeRangeZ.y = Mathf.Max(nodeCoord.z, PathFinder.Instance.nodeRangeZ.y);
        }

        //Game.Log("Node " + nodeCoord + " setup");

        //name = room.name + " Node";

        //Set the world position
        position = CityData.Instance.NodeToRealpos(nodeCoord);
        position = new Vector3(position.x, position.y + (floorHeight * 0.1f), position.z);
        
        //this.transform.localPosition = new Vector3((PathFinder.Instance.tileSize.x * -0.5f) + (PathFinder.Instance.tileSize.x / CityControls.Instance.nodeMultiplier * localTileCoord.x) + (PathFinder.Instance.tileSize.x / CityControls.Instance.nodeMultiplier * 0.5f),
        //    floorHeight * 0.1f,
        //    (PathFinder.Instance.tileSize.y * -0.5f) + (PathFinder.Instance.tileSize.y / CityControls.Instance.nodeMultiplier * localTileCoord.y) + (PathFinder.Instance.tileSize.x / CityControls.Instance.nodeMultiplier * 0.5f));

        //If editor, spawn trigger for cursor
        if (SessionData.Instance.isFloorEdit)
        {
            //Spawn blueprint
            if(physicalObject == null)
            {
                physicalObject = Toolbox.Instance.SpawnObject(PrefabControls.Instance.node, room.transform);
                physicalObject.transform.position = position;
            }

            BoxCollider editorTrigger = physicalObject.AddComponent<BoxCollider>();
            editorTrigger.isTrigger = true;
            editorTrigger.size = new Vector3(PathFinder.Instance.tileSize.x / CityControls.Instance.nodeMultiplier, 0.1f, PathFinder.Instance.tileSize.x / CityControls.Instance.nodeMultiplier);
        }

        //By default this is ceiling + floor
        if (tile.isEdge || tile.isOutside)
        {
            SetFloorType(FloorTileType.none);
        }
        else
        {
            SetFloorType(FloorTileType.floorAndCeiling);
        }

        isSetup = true;
    }

    //Used as if this was a transform: Just add the 'local' position to the actual postion
    public Vector3 TransformPoint(Vector3 localPos)
    {
        return position + localPos;
    }

    public Vector3 InverseTransformPoint(Vector3 worldPos)
    {
        return worldPos - position;
    }

    //Load from data
    public void Load(CitySaveData.NodeCitySave data, NewRoom newRoom)
    {
        //Set to room
        newRoom.AddNewNode(this);

        floorCoord = data.fc; //Coordinate local to the floor (not representitive of actual world position)
        localTileCoord = data.ltc; //Coordinate local to the tile (not representitive of actual world position)
        nodeCoord = data.nc; //Coordinate on a game world level
        SetFloorHeight(data.fh); //The default floor height, nodes will inherit this value
        //ceilingHeight = data.ch; //The default ceiling height, nodes will inherit this value
        floorType = data.ft; //Parameters of the floor
        //preventEntrances = data.pe; //Prevent entrances on walls spawned with these offsets...
        //nodeWeightMultiplier = data.nwm; //The weight of moving to this node from another (used in AI pathing)
        isObstacle = data.io; //If true then this node is completely blocked off from others
        //isAlley = data.ia; //If true this this is an alley way between buildings
        //isBackstreet = data.ib; //True if this is street space behind a building
        isOutside = data.ios; //True if this is exterior
        //isConnected = data.ic; //True if this node has been connected to adjacent
        stairwellLowerLink = data.sll; //If true this is the link to the bottom stair (same floor)
        stairwellUpperLink = data.sul; //If true this is the link to the top stair (above floor)
        forcedRoomRef = data.frr; //Reference string including the address preset parent for above
        SetCeilingVent(data.cav);
        SetFloorVent(data.fav);
        allowNewFurniture = data.anf; //If true, placement of furniture that's not on wall or a rig is allowed
        //walkableTile = data.wt; //If true the ai can access this node properly through walking

        //Load walls
        foreach (CitySaveData.WallCitySave wall in data.w)
        {
            //GameObject newWallObj = Toolbox.Instance.SpawnObject(PrefabControls.Instance.wall, room.transform);
            //NewWall newWall = newWallObj.GetComponent<NewWall>();
            NewWall newWall = new NewWall();
            newWall.Load(wall, this);
        }

        //Load forced room
        if (data.fr != null && data.fr.Length > 0)
        {
            string[] layoutNames = forcedRoomRef.Split('.');

            string lastElement = layoutNames[layoutNames.Length - 1];

            //Get the preset
            RoomConfiguration forcedRoomPreset = null;

            if (Toolbox.Instance.LoadDataFromResources<RoomConfiguration>(lastElement, out forcedRoomPreset))
            {
                SetForcedRoom(forcedRoomPreset);
            }
        }

        //Force inside/outside
        if (newRoom.preset.forceOutside == RoomConfiguration.OutsideSetting.forceInside)
        {
            SetAsOutside(false);
        }
        else if (newRoom.preset.forceOutside == RoomConfiguration.OutsideSetting.forceOutside)
        {
            SetAsOutside(true);
        }

        //entranceWeights = new Dictionary<Vector3, float>(); //If this is an entrance, this will be a reference to the distance of every other room entrance. IMPORTANT: Uses the 'other' room node as otherwise this won't support multiple doorways...

        //furniture = new List<FurnitureCitySave>(); //List of furniture to spawn

        //interactables = new List<InteractableCitySave>(); //List of interactables on this node

        //Set name
        //name = room.name + " Node " + nodeCoord.ToString();
        //this.name = name;
    }

    //Add a wall
    public void AddNewWall(NewWall newWall)
    {
        if (!walls.Contains(newWall))
        {
            //Remove existing
            if (newWall.node != null) newWall.node.RemoveWall(newWall);

            walls.Add(newWall);

            wallDict.Add(newWall.wallOffset, newWall);

            //Set location info
            newWall.node = this;

            if (preventEntrances != null && preventEntrances.Contains(newWall.wallOffset))
            {
                newWall.preventEntrance = true;
            }
            else newWall.preventEntrance = false;
        }
    }

    //Remove a wall
    public void RemoveWall(NewWall newWall)
    {
        if (walls.Contains(newWall))
        {
            walls.Remove(newWall);
            wallDict.Remove(newWall.wallOffset);

            //Set location info
            newWall.node = null;
        }
    }

    //Spawn the floor
    public void SpawnFloor(bool prepForCombinedMeshes)
    {
        Toolbox.Instance.DestroyObject(spawnedFloor); //Refresh

        if(floorType == NewNode.FloorTileType.floorAndCeiling || floorType == NewNode.FloorTileType.floorOnly)
        {
            MeshRenderer renderer = null;

            //Spawn optimized floor
            if (tile.useOptimizedFloor && tile.anchorNode == this)
            {
                floorPrefab = PrefabControls.Instance.floorTile;

                spawnedFloor = Toolbox.Instance.SpawnObject(floorPrefab, room.transform);
                spawnedFloor.transform.position = position;
                spawnedFloor.transform.localEulerAngles = new Vector3(0, tile.rotation, 0);

                if(!prepForCombinedMeshes)
                {
                    if (renderer == null) renderer = spawnedFloor.GetComponent<MeshRenderer>();
                    if (renderer == null) renderer = spawnedFloor.AddComponent<MeshRenderer>();

                    MaterialsController.Instance.SetMaterialGroup(spawnedFloor, room.floorMaterial, room.floorMatKey, renderer: renderer);
                }
            }
            else if(!tile.useOptimizedFloor)
            {
                if (floorAirVent) floorPrefab = PrefabControls.Instance.smallFloorTileVent;
                else floorPrefab = PrefabControls.Instance.smallFloorTile;

                spawnedFloor = Toolbox.Instance.SpawnObject(floorPrefab, room.transform);
                spawnedFloor.transform.position = position;
                spawnedFloor.transform.localEulerAngles = new Vector3(0, tile.rotation, 0);

                if (!prepForCombinedMeshes)
                {
                    if (renderer == null) renderer = spawnedFloor.GetComponent<MeshRenderer>();
                    if (renderer == null) renderer = spawnedFloor.AddComponent<MeshRenderer>();

                    MaterialsController.Instance.SetMaterialGroup(spawnedFloor, room.floorMaterial, room.floorMatKey, renderer: renderer);
                }
            }

            if(spawnedFloor != null)
            {
                //If in address display mode, chance the colour of the material
                if (SessionData.Instance.isFloorEdit)
                {
                    if(renderer == null) renderer = spawnedFloor.GetComponent<MeshRenderer>();

                    if (FloorEditController.Instance.displayMode == FloorEditController.EditorDisplayMode.displayAddressDesignation && renderer != null)
                    {
                        renderer.material = FloorEditController.Instance.adddressDesignationMaterial;
                        renderer.material.SetColor("_UnlitColor", gameLocation.thisAsAddress.editorColour); //set the material equal to the clone
                    }
                }

                //Set light layer
                if (!prepForCombinedMeshes)
                {
                    if (renderer == null) renderer = spawnedFloor.GetComponent<MeshRenderer>();
                    if (renderer == null) renderer = spawnedFloor.AddComponent<MeshRenderer>();

                    if (renderer != null)
                    {
                        bool forceStreetLight = room.IsOutside();
                        if (room.preset != null && room.preset.forceStreetLightLayer) forceStreetLight = room.preset.forceStreetLightLayer;
                        Toolbox.Instance.SetLightLayer(renderer, building, forceStreetLight);
                    }
                }
            }
        }
    }

    //Spawn the ceiling (replace)
    public void SpawnCeiling(bool prepForCombinedMeshes)
    {
        Toolbox.Instance.DestroyObject(spawnedCeiling); //Refresh

        if (floorType == NewNode.FloorTileType.floorAndCeiling || floorType == NewNode.FloorTileType.CeilingOnly)
        {
            MeshRenderer renderer = null;

            //Spawn optimized floor
            if (tile.useOptimizedCeiling && tile.anchorNode == this)
            {
                ceilingPrefab = PrefabControls.Instance.ceilingTile;

                spawnedCeiling = Toolbox.Instance.SpawnObject(ceilingPrefab, room.transform);
                spawnedCeiling.transform.position = position + new Vector3(spawnedCeiling.transform.localPosition.x, (floor.defaultCeilingHeight * 0.1f) - (floorHeight * 0.1f), spawnedCeiling.transform.localPosition.z);

                if(!prepForCombinedMeshes)
                {
                    if (renderer == null) renderer = spawnedCeiling.GetComponent<MeshRenderer>();
                    if (renderer == null) renderer = spawnedCeiling.AddComponent<MeshRenderer>();

                    bool uniqueMat = false;
                    if (room.preset != null) uniqueMat = room.preset.boostCeilingEmission;
                    room.ceilingMat = MaterialsController.Instance.SetMaterialGroup(spawnedCeiling, room.ceilingMaterial, room.ceilingMatKey, uniqueMat, renderer: renderer);
                    room.uniqueCeilingMaterial = uniqueMat;

                    //Make this invisible in the floor editor
                    if (SessionData.Instance.isFloorEdit && !SessionData.Instance.play)
                    {
                        if(renderer != null) renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    }

                    //Set light layer
                    bool forceStreetLight = room.IsOutside();
                    if (room.preset != null && room.preset.forceStreetLightLayer) forceStreetLight = room.preset.forceStreetLightLayer;
                    Toolbox.Instance.SetLightLayer(renderer, building, forceStreetLight);
                }
            }
            else if(!tile.useOptimizedCeiling)
            {
                if (ceilingAirVent) ceilingPrefab = PrefabControls.Instance.smallCeilingTileVent;
                else ceilingPrefab = PrefabControls.Instance.smallCeilingTile;

                spawnedCeiling = Toolbox.Instance.SpawnObject(ceilingPrefab, room.transform);
                spawnedCeiling.transform.position = position + new Vector3(spawnedCeiling.transform.localPosition.x, (floor.defaultCeilingHeight * 0.1f) - (floorHeight * 0.1f), spawnedCeiling.transform.localPosition.z);

                if (!prepForCombinedMeshes)
                {
                    if (renderer == null) renderer = spawnedCeiling.GetComponent<MeshRenderer>();
                    if (renderer == null) renderer = spawnedCeiling.AddComponent<MeshRenderer>();

                    bool uniqueMat = false;
                    if (room.preset != null) uniqueMat = room.preset.boostCeilingEmission;
                    room.ceilingMat = MaterialsController.Instance.SetMaterialGroup(spawnedCeiling, room.ceilingMaterial, room.ceilingMatKey, uniqueMat, renderer: renderer);
                    room.uniqueCeilingMaterial = uniqueMat;

                    if (SessionData.Instance.isFloorEdit && !SessionData.Instance.play)
                    {
                        if(renderer != null) renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
                    }

                    //Set light layer
                    bool forceStreetLight = room.IsOutside();
                    if (room.preset != null && room.preset.forceStreetLightLayer) forceStreetLight = room.preset.forceStreetLightLayer;
                    Toolbox.Instance.SetLightLayer(renderer, building, forceStreetLight);
                }
            }
        }
    }

    //Set this to a floor tile type
    public void SetFloorType(FloorTileType newType)
    {
        floorType = newType;

        //floorDebug.Add(newType.ToString());

        if (floorType == FloorTileType.none)
        {
            //Set to null address
            if(room.floor != null)
            {
                room.floor.outsideAddress.AddNewNode(this);
            }
        }
        else if(room.floor != null)
        {
            //Set default assigned address
            if (gameLocation == room.floor.outsideAddress || gameLocation.name.Length <= 0)
            {
                room.floor.lobbyAddress.AddNewNode(this);
            }
        }

        //Do this if not being loaded or in editor
        if(SessionData.Instance.isFloorEdit || SessionData.Instance.isTestScene || CityConstructor.Instance.generateNew)
        {
            GenerationController.Instance.UpdateGeometryFloor(floor, "NewNode");
        }
    }

    //Set as obstacle
    public void SetAsObstacle(bool val)
    {
        isObstacle = val;
    }

    //Set as alley
    //public void SetAsAlley(bool val)
    //{
    //    isAlley = val;
    //}

    //Set as backstreet
    //public void SetAsBackstreet(bool val)
    //{
    //    isBackstreet = val;
    //}

    //Set as outside
    public void SetAsOutside(bool val)
    {
        isOutside = val;

        //Game.Log("Set node to outside: " + isOutside);
    }

    //Add access to another node (& vice versa)
    public void AddAccessToOtherNode(NewNode newNode, bool twoWay = true, bool forceAccessType = false, NewNode.NodeAccess.AccessType forcedAccessType = NodeAccess.AccessType.adjacent, bool forceWalkable = false/*, string newDebug = ""*/)
    {
        if (newNode == this) return;
        if (newNode.isObstacle && newNode.isObstacle) return;

        //Calculate weight
        //Use distance and * by the weight multiplier
        float routeWeight = Vector3.Distance(this.position, newNode.position) * newNode.nodeWeightMultiplier;

        //The doorway is always the parent wall: This only exists if there's a wall inbetween obvs.
        NewDoor foundDoor = null;
        NewWall foundWall = walls.Find(item => item.otherWall != null && item.otherWall.node == newNode);

        if (foundWall != null)
        {
            foundDoor = foundWall.parentWall.door;
            foundWall = foundWall.parentWall;
        }

        //If doesn't contain, add
        if (!accessToOtherNodes.ContainsKey(newNode))
        {
            NodeAccess newAccess = new NodeAccess(this, newNode, foundWall, foundDoor, forceAccessType, forcedAccessType, forceWalkable/*, nodeCoord + ">" + newNode.nodeCoord + " " + newDebug*/);
            accessToOtherNodes.Add(newNode, newAccess);
            //if(Game.Instance.devMode) accessDebug.Add(newAccess);
        }
        //If already contains, compare access weight and choose lowest
        else if(routeWeight < accessToOtherNodes[newNode].weight)
        {
            NodeAccess newAccess = new NodeAccess(this, newNode, foundWall, foundDoor, forceAccessType, forcedAccessType, forceWalkable/*, nodeCoord + ">" + newNode.nodeCoord + " " + newDebug*/);
            accessToOtherNodes[newNode] = newAccess;
        }

        //Add entrance to other room
        if (newNode.room != room)
        {
            room.AddEntrance(this, newNode, forceAccessType, forcedAccessType, forceWalkable);
        }

        //Add reverse
        if (twoWay)
        {
            //Calculate weight
            //Use distance and * by the weight multiplier
            //routeWeight = Vector3.Distance(position, newNode.position) * nodeWeightMultiplier;

            foundDoor = null;

            if (foundWall != null)
            {
                foundWall = foundWall.otherWall;
                foundDoor = foundWall.parentWall.door;
                foundWall = foundWall.parentWall;
            }

            //If doesn't contain, add
            if (!newNode.accessToOtherNodes.ContainsKey(this))
            {
                NodeAccess newAccess = new NodeAccess(newNode, this, foundWall, foundDoor, forceAccessType, forcedAccessType, forceWalkable/*, nodeCoord + ">" + newNode.nodeCoord + " 2Way " + newDebug*/);

                newNode.accessToOtherNodes.Add(this, newAccess);
                //if (Game.Instance.devMode) newNode.accessDebug.Add(newAccess);
            }
            //If already contains, compare access weight and choose lowest
            else if (routeWeight < newNode.accessToOtherNodes[this].weight)
            {
                NodeAccess newAccess = new NodeAccess(newNode, this, foundWall, foundDoor, forceAccessType, forcedAccessType, forceWalkable/*, nodeCoord + ">" + newNode.nodeCoord + " 2Way " + newDebug*/);

                newNode.accessToOtherNodes[this] = newAccess;
                //if (Game.Instance.devMode) newNode.accessDebug.Add(newAccess);
            }

            //Add entrance to other room
            if (newNode.room != room)
            {
               newNode.room.AddEntrance(newNode, this, forceAccessType, forcedAccessType, forceWalkable);
            }
        }
    }

    //Remove access to another node
    public void RemoveAccessToOtherNode(NewNode newNode, bool twoWay = true)
    {
        if(accessToOtherNodes.ContainsKey(newNode))
        {
            accessToOtherNodes[newNode].walkingAccess = false;
            accessToOtherNodes.Remove(newNode);
        }

        if (newNode.room != room)
        {
            NewNode.NodeAccess accessThis = room.entrances.Find(item => item.fromNode == this && item.toNode == newNode);
            if (accessThis != null) accessThis.walkingAccess = false;

            NewNode.NodeAccess accessOther = newNode.room.entrances.Find(item => item.fromNode == newNode && item.toNode == this);
            if (accessOther != null) accessOther.walkingAccess = false;

            //room.RemoveEntrance(this, newNode);
            //room.RemoveEntrance(newNode, this);

            //newNode.room.RemoveEntrance(this, newNode);
            //newNode.room.RemoveEntrance(newNode, this);
        }

        //Is the access in this node limited to vertical space? If so mark it as inaccessable...
        bool limited = true;

        foreach (KeyValuePair<NewNode, NewNode.NodeAccess> acc in accessToOtherNodes)
        {
            if (acc.Value.walkingAccess)
            {
                limited = false;
                break;
            }
        }

        if (limited)
        {
            isInaccessable = true;
            noAccess = true;
        }

        if (twoWay)
        {
            if (newNode.accessToOtherNodes.ContainsKey(this))
            {
                newNode.accessToOtherNodes[this].walkingAccess = false;
                newNode.accessToOtherNodes.Remove(this);
            }

            limited = true;

            foreach (KeyValuePair<NewNode, NewNode.NodeAccess> acc in newNode.accessToOtherNodes)
            {
                if (acc.Value.walkingAccess)
                {
                    limited = false;
                    break;
                }
            }

            if (limited)
            {
                newNode.isInaccessable = true;
                newNode.noAccess = true;
            }
        }
    }

    //Set forced room
    public void SetForcedRoom(RoomConfiguration newRoom)
    {
        forcedRoom = newRoom;
    }

    //Add interactable to this node
    public void AddInteractable(Interactable newInteractable)
    {
        if (newInteractable == null) return;

        if (!interactables.Contains(newInteractable))
        {
            //if (newInteractable.preset.name == "Lightswitch")
            //{
            //    Game.Log("Add lightswitch to node " + position + " room " + room.name + " " + room.roomID);
            //}

            interactables.Add(newInteractable); //Add to this node...

            //Add special case reference
            if (newInteractable.preset.specialCaseFlag != InteractablePreset.SpecialCase.none)
            {
                if (!room.specialCaseInteractables.ContainsKey(newInteractable.preset.specialCaseFlag))
                {
                    room.specialCaseInteractables.Add(newInteractable.preset.specialCaseFlag, new List<Interactable>());
                }

                if (!room.specialCaseInteractables[newInteractable.preset.specialCaseFlag].Contains(newInteractable))
                {
                    room.specialCaseInteractables[newInteractable.preset.specialCaseFlag].Add(newInteractable);
                }
            }

            //Add actions to room & address dictionary
            foreach (InteractablePreset.InteractionAction action in newInteractable.preset.GetActions())
            {
                if (action == null || action.action == null)
                {
                    Game.Log("Misc Error: Null action detected in " + newInteractable.preset.name);
                    continue;
                }

                if (!action.usableByAI) continue;

                if (!room.actionReference.ContainsKey(action.action))
                {
                    room.actionReference.Add(action.action, new List<Interactable>());
                }

                room.actionReference[action.action].Add(newInteractable);
                room.actionReference[action.action].Sort((p1, p2) => p2.preset.AIPriority.CompareTo(p1.preset.AIPriority)); //Using P2 first gives highest first

                if ((Game.Instance.devMode && Game.Instance.collectDebugData)) room.debugAddActions.Add("Add: " + action.action.name + ": " + newInteractable.name + " (" + room.actionReference.Count + ")");

                //Add to address reference also
                if (!gameLocation.actionReference.ContainsKey(action.action))
                {
                    gameLocation.actionReference.Add(action.action, new List<Interactable>());
                }

                gameLocation.actionReference[action.action].Add(newInteractable);
                gameLocation.actionReference[action.action].Sort((p1, p2) => p2.preset.AIPriority.CompareTo(p1.preset.AIPriority)); //Using P2 first gives highest first
            }

            //Load if geometry is already loaded
            if (room.geometryLoaded)
            {
                if (newInteractable.furnitureParent != null)
                {
                    if (newInteractable.furnitureParent.spawnedObject != null)
                    {
                        newInteractable.LoadInteractableToWorld();
                    }
                }
                else newInteractable.LoadInteractableToWorld();
            }

            //Trigger thrown item contens in parent room...
            //if(newInteractable.preset.specialCaseFlag == InteractablePreset.SpecialCase.thrownItem)
            //{
            //    room.thrownItems.Add(newInteractable);
            //}

            //Add to heat sources
            if(newInteractable.preset.isHeatSource)
            {
                room.heatSources.Add(newInteractable);
            }
        }
    }

    //Remove interactable from this node
    public void RemoveInteractable(Interactable newInteractable)
    {
        //Remove special case reference
        if (newInteractable.preset.specialCaseFlag != InteractablePreset.SpecialCase.none)
        {
            if (room.specialCaseInteractables.ContainsKey(newInteractable.preset.specialCaseFlag))
            {
                room.specialCaseInteractables[newInteractable.preset.specialCaseFlag].Remove(newInteractable);
            }
        }

        //Add actions to room & address dictionary
        foreach (InteractablePreset.InteractionAction action in newInteractable.preset.GetActions())
        {
            if (!action.usableByAI) continue;

            if (room.actionReference.ContainsKey(action.action))
            {
                room.actionReference[action.action].Remove(newInteractable);
            }

            if ((Game.Instance.devMode && Game.Instance.collectDebugData)) room.debugAddActions.Add("Remove: " + action.action.name + ": " + newInteractable.name + " (" + room.actionReference.Count + ")");

            //Add to address reference also
            if (gameLocation.actionReference.ContainsKey(action.action))
            {
                gameLocation.actionReference[action.action].Remove(newInteractable);
            }
        }

        interactables.Remove(newInteractable); //Remove from this node

        //Trigger thrown item contens in parent room...
        //if (newInteractable.preset.specialCaseFlag == InteractablePreset.SpecialCase.thrownItem)
        //{
        //    room.thrownItems.Remove(newInteractable);
        //}

        //Add to heat sources
        if (newInteractable.preset.isHeatSource)
        {
            room.heatSources.Remove(newInteractable);
        }
    }

    //Debug teleport player to location
    [Button("Teleport Player")]
    public void DebugTeleportPlayerToLocation()
    {
        Player.Instance.Teleport(this, null);
    }

    //Set node height offset (measurement in voxels == 0.1f)
    public void SetFloorHeight(int val)
    {
        floorHeight = val;
        position = new Vector3(position.x, tile.position.y + (floorHeight * 0.1f), position.z);

        //Set height of walls
        foreach(NewWall wall in walls)
        {
            wall.position = new Vector3(
                            wall.position.x,
                            position.y + (floorHeight * -0.1f), //Inverse the floor height to counteract the placement of the node's floor
                            wall.position.z
                                            );

            //Update position of physical & spawned objects
            if (wall.physicalObject != null)
            {
                wall.physicalObject.transform.position = wall.position;

                FloorEditWallDetector w = wall.physicalObject.GetComponent<FloorEditWallDetector>();
                w.debugNodePosition = position;
                w.debugFloorHeight = floorHeight;
            }

            if (wall.spawnedWall != null) wall.spawnedWall.transform.position = wall.position;
        }

        if(spawnedCeiling != null && floor != null)
        {
            spawnedCeiling.transform.position = new Vector3(spawnedCeiling.transform.position.x, position.y + ((floor.defaultCeilingHeight * 0.1f) - (floorHeight * 0.1f)), spawnedCeiling.transform.position.z);
        }
    }

    public void SetAllowNewFurniture(bool val)
    {
        allowNewFurniture = val;
        //debugPossibleFurniture.Add(debug);
    }

    //Generate save data
    public CitySaveData.NodeCitySave GenerateSaveData()
    {
        CitySaveData.NodeCitySave output = new CitySaveData.NodeCitySave();

        //output.tile = tile; //Reference to the tile
        output.fc = floorCoord; //Coordinate local to the floor (not representitive of actual world position)
        output.ltc = localTileCoord; //Coordinate local to the tile (not representitive of actual world position)
        output.nc = Vector3Int.RoundToInt(nodeCoord); //Coordinate on a game world level

        //Get wall
        foreach(NewWall wall in walls)
        {
            output.w.Add(wall.GenerateSaveData());
        }

        output.fh = floorHeight; //The default floor height, nodes will inherit this value
        //output.ch = ceilingHeight; //The default ceiling height, nodes will inherit this value
        output.ft = floorType; //Parameters of the floor
        //output.pe = preventEntrances; //Prevent entrances on walls spawned with these offsets...
        //output.nwm = nodeWeightMultiplier; //The weight of moving to this node from another (used in AI pathing)
        output.io = isObstacle; //If true then this node is completely blocked off from others
        //output.ia = isAlley; //If true this this is an alley way between buildings
        //output.ib = isBackstreet; //True if this is street space behind a building
        output.ios = isOutside; //True if this is exterior
        //output.ic = isConnected; //True if this node has been connected to adjacent
        output.sll = stairwellLowerLink; //If true this is the link to the bottom stair (same floor)
        output.sul = stairwellUpperLink; //If true this is the link to the top stair (above floor)
        if(forcedRoom != null) output.fr = forcedRoom.name; //Forced room setting
        output.frr = forcedRoomRef; //Reference string including the address preset parent for above
        output.anf = allowNewFurniture; //If true, placement of furniture that's not on wall or a rig is allowed
        output.cav = ceilingAirVent;
        output.fav = floorAirVent;
        //output.wt = walkableTile; //If true the ai can access this node properly through walking
                                            //public List<FurnitureLocation> furniture = new List<FurnitureLocation>(); //List of furniture to spawn
                                            //public List<Interactable> interactables = new List<Interactable>(); //List of interactables on this node

        return output;
    }

    //Add human traveller: The returned bool is false if there is no room here. Return use node position.
    public bool AddHumanTraveller(Actor newActor, Interactable.UsagePoint usagePoint, out Vector3 usePosition)
    {
        if(newActor.ai != null) newActor.ai.DebugDestinationPosition("> Add " + newActor.name + " to node: " + position);
        if (Game.Instance.collectDebugData) newActor.SelectedDebug("> Add " + newActor.name + " to node: " + position, Actor.HumanDebug.movement);

        if (usagePoint != null && newActor.ai != null)
        {
            if (newActor.ai != null) newActor.ai.DebugDestinationPosition("...Given usage point from " + usagePoint.interactable.name);
            if (Game.Instance.collectDebugData) newActor.SelectedDebug("...Given usage point from " + usagePoint.interactable.name, Actor.HumanDebug.movement);
        }

        if (newActor.reservedNodeSpace.Count > 0)
        {
            if (newActor.ai != null) newActor.ai.DebugDestinationPosition("Clearing actor reserved space");
            if (Game.Instance.collectDebugData) newActor.SelectedDebug("Clearing actor reserved space", Actor.HumanDebug.movement);

            //Remove all reserved node space on this node
            newActor.RemoveReservedNodeSpace();
        }

        //Potentially override the usage point with a door to open.
        //bool usagePointIsDoor = false;

        //Scan for a door to open in the node after next, this way we can walk the AI to the proper door opening position...
        if(usagePoint == null || usagePoint.node != this)
        {
            if(newActor.ai != null && newActor.ai.currentAction != null && newActor.ai.currentAction.path != null)
            {
                if (Game.Instance.collectDebugData)
                {
                    newActor.ai.DebugDestinationPosition("AI is on path node " + newActor.ai.pathCursor + "/" + newActor.ai.currentAction.path.accessList.Count);
                    newActor.SelectedDebug("AI is on path node " + newActor.ai.pathCursor + "/" + newActor.ai.currentAction.path.accessList.Count, Actor.HumanDebug.movement);
                }

                if (newActor.ai.pathCursor < newActor.ai.currentAction.path.accessList.Count - 1)
                {
                    NewNode.NodeAccess nextNextAcc = newActor.ai.currentAction.path.accessList[newActor.ai.pathCursor + 1];

                    //There is a door here...
                    if (nextNextAcc.door != null && nextNextAcc.door.isClosed)
                    {
                        //Override useage point
                        usagePoint = nextNextAcc.door.handleInteractable.usagePoint;
                        //usagePointIsDoor = true;

                        if (Game.Instance.collectDebugData)
                        {
                            newActor.SelectedDebug("The next usage point has been overridden by a door at path position " + (newActor.ai.pathCursor + 1) + "/" + newActor.ai.currentAction.path.accessList.Count, Actor.HumanDebug.movement);
                            newActor.ai.DebugDestinationPosition("The next usage point has been overridden by a door at path position " + (newActor.ai.pathCursor + 1) + "/" + newActor.ai.currentAction.path.accessList.Count);
                        }
                    }
                }
            }
        }

        //If the end of the path is reached, look for the interaction point...
        if(usagePoint != null)
        {
            if (newActor.ai != null && newActor.ai.currentAction != null && newActor.ai.currentAction.path != null)
            {
                //Get usage point from action
                if (newActor.ai.pathCursor >= newActor.ai.currentAction.path.accessList.Count)
                {
                    usePosition = usagePoint.GetUsageWorldPosition(position, newActor); //Get the usage point

                    if (Game.Instance.collectDebugData)
                    {
                        newActor.ai.DebugDestinationPosition("Reached end of path, get usage position of: " + usePosition);
                        newActor.SelectedDebug("Reached end of path, get usage position of: " + usePosition, Actor.HumanDebug.movement);
                    }

                    return true;
                }
            }
        }


        //We've been given a usage point, check to see if we're there yet...
        //if (usagePoint != null)
        //{
        //    if (usagePoint.node == this || usagePointIsDoor)
        //    {
        //        usePosition = usagePoint.GetUsageWorldPosition(position, newActor); //Get the usage point

        //        if (newActor.ai != null)
        //        {
        //            newActor.ai.DebugDestinationPosition("Usage point node is reached. New usage point assigned of " + usePosition);
        //            newActor.ai.DebugDestinationPosition("Difference from last destination point: " + (usePosition - newActor.ai.currentDestinationPositon));
        //        }

        //        return true;
        //    }
        //    else if (newActor.ai != null)
        //    {
        //        newActor.SelectedDebug("AI is not yet at desitnation: " + usagePoint.interactable.node.room.gameLocation.name);
        //        newActor.ai.DebugDestinationPosition("AI is not at usage point node: This node pos = " + position + ", usage point node pos: " + usagePoint.node.position);

        //        if (newActor.ai.currentAction != null && newActor.ai.currentAction.path != null)
        //        {
        //            newActor.ai.DebugDestinationPosition("AI is on path node " + newActor.ai.pathCursor + "/" + newActor.ai.currentAction.path.accessList.Count);
        //        }
        //    }
        //}

        //Only bother with the following if the node is drawn (as it can be quite expensive...)
        //if(!room.gameObject.activeInHierarchy)
        //{
        //    if(newActor.ai != null) newActor.ai.DebugDestinationPosition("The current room is not drawn, so position directly on node");
        //    usePosition = this.position;
        //    return true;
        //}
        //If the default is free, use that first
        if(defaultSpace != null && (defaultSpace.occ == NodeSpaceOccupancy.empty || defaultSpace.occupier == newActor))
        {
            if (Game.Instance.collectDebugData)
            {
                if (newActor.ai != null) newActor.ai.DebugDestinationPosition("There is nobody else on this node, so use the centre position");
                newActor.SelectedDebug("There is nobody else on this node, so use the centre position", Actor.HumanDebug.movement);
            }

            usePosition = defaultSpace.position;
            defaultSpace.SetOccuppier(newActor, NodeSpaceOccupancy.reserved);

            return true;
        }
        //If this is an empty node, attempt to 'fit' more than 1 citizen here by raycasting at divisions inside...
        else
        {
            //Collected valid positions
            List<NodeSpace> validPositions = new List<NodeSpace>();

            foreach(KeyValuePair<Vector3, NodeSpace> pair in walkableNodeSpace)
            {
                //If this position is already taken by a travelling citizen, we know to skip it already...
                if (pair.Value.occ != NodeSpaceOccupancy.empty && pair.Value.occupier != newActor) continue;

                //Spherecast
                //Notes: SphereCast will not detect colliders for which the sphere overlaps the collider
                RaycastHit hit;

                //The idea is to start just below the floor and raycast up to check for collisions with other objects (should include furniture, other characters etc).
                int layerMask = Toolbox.Instance.CreateLayerMask(Toolbox.LayerMaskMode.castAllExcept, 2, 18, 23);

                //Scan
                if (Physics.SphereCast(pair.Value.position + new Vector3(0f, -0.35f, 0f), 0.34f, new Vector3(0, 1, 0), out hit, 2.5f, layerMask, QueryTriggerInteraction.Ignore))
                {
                    if(detectGeometry)
                    {
                        //We've hit something, this isn't valid...
                        continue;
                    }
                    else
                    {
                        //Only stop if we hit a person...
                        if(hit.transform.parent.CompareTag("Citizen"))
                        {
                            continue;
                        }
                    }
                }

                validPositions.Add(pair.Value);
            }

            //There is no room left on this node at all!
            if(validPositions.Count <= 0)
            {
                if (Game.Instance.collectDebugData)
                {
                    if (newActor.ai != null) newActor.ai.DebugDestinationPosition("There is no room left on this node! Default to centre position but return false.");
                    newActor.SelectedDebug("There is no room left on this node! Default to centre position but return false.", Actor.HumanDebug.movement);
                }

                usePosition = position;

                if(defaultSpace != null)
                {
                    usePosition = defaultSpace.position;
                    defaultSpace.SetOccuppier(newActor, NodeSpaceOccupancy.reserved);
                }

                return false;
            }

            //We should now have a list of valid positions that are free on the node, but which one to choose? We should rank them...
            if(validPositions.Count > 1)
            {
                float bestRank = 9999f;
               NodeSpace bestPos = validPositions[0];

                //Rank consists of how close it is to me now + how close it is to our next destination / 2
                for (int i = 0; i < validPositions.Count; i++)
                {
                    NodeSpace valid = validPositions[i];
                    float rank = Vector3.Distance(newActor.transform.position, valid.position);

                    //Find lowest rank
                    if(rank < bestRank)
                    {
                        bestRank = rank;
                        bestPos = valid;
                    }
                }

                usePosition = bestPos.position;
                bestPos.SetOccuppier(newActor, NodeSpaceOccupancy.reserved);

                if (Game.Instance.collectDebugData)
                {
                    if (newActor.ai != null) newActor.ai.DebugDestinationPosition("There are obstacles on this node, but space was found. New use position is: " + usePosition);
                    newActor.SelectedDebug("There are obstacles on this node, but space was found. New use position is: " + usePosition, Actor.HumanDebug.movement);
                }

                return true;
            }
            //Choose the 1 available location
            else
            {
                usePosition = validPositions[0].position;
                validPositions[0].SetOccuppier(newActor, NodeSpaceOccupancy.reserved);

                if (newActor.ai != null) newActor.ai.DebugDestinationPosition("There is 1 obstacle on this node, but space was found. New use position is: " + usePosition);
                newActor.SelectedDebug("There is 1 obstacle on this node, but space was found. New use position is: " + usePosition, Actor.HumanDebug.movement);

                return true;
            }
        }

        //usePosition = position;
        //return true;
    }

    //Get all walkable sublocations for node, including furniture
    public void UpdateWalkableSublocations()
    {
        List<Vector3> sublocations = new List<Vector3>();

        detectGeometry = true;
        bool blockDefault = false;

        foreach (FurnitureLocation furnLoc in individualFurniture)
        {
            foreach (FurnitureClass furnClass in furnLoc.furnitureClasses)
            {
                if (furnClass.ignoreGeometryInPhysicsCheck)
                {
                    detectGeometry = false;
                }

                if(furnClass.blockDefaultSublocations)
                {
                    blockDefault = true;
                }
            }

            //Add custom locations
            List<Vector3> customSublocations = null;

            if (furnLoc.sublocations.TryGetValue(this, out customSublocations))
            {
                sublocations.AddRange(customSublocations);
            }
        }

        //Default sublocations will be used if there are no custom ones, and there is no furniture class with a blocking flag...
        if(sublocations.Count <= 0 && !blockDefault)
        {
            sublocations.AddRange(CitizenControls.Instance.nodeLocalSubdivisions); //If none then use the default
        }

        float distanceToMiddile = Mathf.Infinity;

        walkableNodeSpace.Clear();

        foreach(Vector3 v3 in sublocations)
        {
            if(!walkableNodeSpace.ContainsKey(v3))
            {
                walkableNodeSpace.Add(v3, new NodeSpace { position = TransformPoint(v3), node = this });
            }
            else
            {
                walkableNodeSpace[v3].position = TransformPoint(v3);
            }

            //Find the default/middle
            float dist = Vector3.Distance(position, walkableNodeSpace[v3].position);

            if(dist < distanceToMiddile)
            {
                distanceToMiddile = dist;
                defaultSpace = walkableNodeSpace[v3];
            }
        }
    }

    //Clear all travellers
    public void ClearTravellers()
    {
        foreach(KeyValuePair<Vector3, NodeSpace> pair in walkableNodeSpace)
        {
            pair.Value.SetEmpty();
        }

        occupiedSpace.Clear();
    }

    //Set audio source
    public void SetAsAudioSource(AudioEvent newEvent, Vector3 newOffset)
    {
        audioEvent = newEvent;
        audioOffset = newOffset;

        //Cancel existing
        if(loop != null)
        {
            AudioController.Instance.StopSound(loop, AudioController.StopType.immediate, "Stop existing loop");
            loop = null;
        }

        if(audioEvent != null)
        {
            loop = AudioController.Instance.PlayWorldLoopingStatic(audioEvent, null, this, position + audioOffset, pauseWhenGamePaused: true);
        }
    }

    //Set ceiling vent
    public void SetCeilingVent(bool val)
    {
        ceilingAirVent = val;

        //Vector3 aboveNode = nodeCoord + new Vector3(0, 0, 1);
        //NewNode foundNode = null;

        //if(PathFinder.Instance.nodeMap.TryGetValue(aboveNode, out foundNode))
        //{
        //    if(foundNode.floorAirVent != ceilingAirVent)
        //    {
        //        if(ceilingHeight)
        //        foundNode.SetFloorVent(ceilingAirVent);
        //    }
        //}

        //Respawn
        if(ceilingAirVent)
        {
            if (spawnedCeiling != null)
            {
                tile.SetFloorCeilingOptimization(false, true);
            }
            else tile.SetFloorCeilingOptimization(false, false);
        }
        else
        {
            if (spawnedCeiling != null)
            {
                tile.SetFloorCeilingOptimization(tile.CanBeOptimized(), true);
            }
            else tile.SetFloorCeilingOptimization(tile.CanBeOptimized(), false);
        }
    }

    public void SetFloorVent(bool val)
    {
        floorAirVent = val;

        //Vector3 belowNode = nodeCoord + new Vector3(0, 0, -1);
        //NewNode foundNode = null;

        //if (PathFinder.Instance.nodeMap.TryGetValue(belowNode, out foundNode))
        //{
        //    if(foundNode.ceilingAirVent != floorAirVent)
        //    {
        //        foundNode.SetCeilingVent(floorAirVent);
        //    }
        //}

        //Respawn
        if (floorAirVent)
        {
            if (spawnedFloor != null)
            {
                tile.SetFloorCeilingOptimization(false, true);
            }
            else tile.SetFloorCeilingOptimization(false, false);
        }
        else
        {
            if (spawnedFloor != null)
            {
                tile.SetFloorCeilingOptimization(tile.CanBeOptimized(), true);
            }
            else tile.SetFloorCeilingOptimization(tile.CanBeOptimized(), false);
        }
    }
}
