﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class GameplayControls : MonoBehaviour
{
    [Header("Cut Scenes")]
    public CutScenePreset intro;
    public CutScenePreset outro;

    [Header("Time")]
    public SessionData.TimeSpeed startingTimeSpeed = SessionData.TimeSpeed.normal;
    //1.0f is 1 second == 1 minute in-game.
    [Tooltip("0 = slomo, 1 = normal, 2 = fast, 3 = ultrafast, 4 = sim")]
    [ReorderableList]
    public List<float> timeMultipliers = new List<float>();
    public int startingDate = 0;
    public int startingMonth = 0;
    public int startingYear = 1;

    //The time data on the game's 0:00:00:0000 hour
    [Tooltip("Only time cycles idependent of the actual time data are needed here")]
    public int yearZeroLeapYearCycle = 1;
    public int dayZero = 6;
    public int publicYearZero = 1978;

    [Header("Routines")]
    public float routineUpdateFrequency = 0.5f;
    public float gameWorldUpdateFrequency = 0.75f;
    public float doorSequenceUpdateFrequency = 0.15f;
    public float stealthModeLoopUpdateFrequency = 0.1f;

    [Header("First Person")]
    [Tooltip("Player height (normal)")]
    public float playerHeightNormal = 1.64f;
    [Tooltip("Player height (crouched)")]
    public float playerHeightCrouched = 0.96f;
    public AnimationCurve crouchHeightCurve;
    public AnimationCurve leanCurve;
    public AnimationCurve joltCurve;
    [Tooltip("FPS Camera Y as offset of centre of player")]
    public float cameraHeightNormal = 0.78f;
    [Tooltip("FPS Camera Y as offset of centre of player")]
    public float cameraHeightCrouched = 0.68f;
    [Tooltip("Range from which things can be interacted-with by the player")]
    public float interactionRange = 1.85f;
    [Tooltip("Range from which things can be read by the player")]
    public float readingRange = 1.2f;
    [Tooltip("The distance from which things are carried by the player")]
    public float carryDistance = 1f;
    [Tooltip("The throw force of the player")]
    public float throwForce = 500f;
    [Tooltip("Field of view (Normal)")]
    public float fovNormal = 70f;
    [Tooltip("Field of view (interaction)")]
    public float fovInteraction = 40f;
    [Tooltip("How much FPS model lag")]
    public float fpsModelLag = 60f;
    [Tooltip("Player walk speed")]
    public float playerWalkSpeed = 4f;
    [Tooltip("Player run speed")]
    public float playerRunSpeed = 8f;
    [Tooltip("Jump height")]
    public float jumpHeight = 3.5f;
    [Tooltip("Player stealth mode walk multiplier")]
    public float playerStealthWalkMuliplier = 0.5f;
    [Tooltip("Player stealth mode run multiplier")]
    public float playerStealthRunMultiplier = 0.9f;
    [Tooltip("Head bob multiplier")]
    public float headBobMultiplier = 0.55f;
    [Tooltip("Player height multiplier in air ducts")]
    public float ductPlayerHeight = 0.25f;
    [Tooltip("Camera height multiplier in air ducts")]
    public float ductCamHeight = 2.4f;
    [Tooltip("Player height boost on enter air duct")]
    public float ductPlayerPosY = -0.5f;
    [Tooltip("Air duct entry point")]
    public Vector3 airDuctEntry = new Vector3(-0.35f, 0, 0.05f);
    [Tooltip("Air duct exit point")]
    public Vector3 airDuctExit = new Vector3(0, 0, 0.5f);
    [Tooltip("Normal skin width")]
    public float normalSkinWidth = 0.08f;
    [Tooltip("Carrying skin width")]
    public float carryingSkinWidth = 0.2f;
    [Tooltip("Default return transition")]
    public PlayerTransitionPreset defaultReturnTransition;
    public PlayerTransitionPreset enterVentTransition;
    public PlayerTransitionPreset exitVentTransition;
    public PlayerTransitionPreset citizensArrestTranstion;
    public PlayerTransitionPreset citizenTalkToTransition;
    public PlayerTransitionPreset doorPeekEnter;
    public PlayerTransitionPreset doorPeekExit;
    public PlayerTransitionPreset lockpickEnter;
    public PlayerTransitionPreset lockpickExit;
    public PlayerTransitionPreset sabotageEnter;
    public PlayerTransitionPreset sabotageExit;
    public PlayerTransitionPreset bargeDoorEnter;
    public PlayerTransitionPreset bargeDoorFail;
    public PlayerTransitionPreset bargeDoorSuccess;
    public PlayerTransitionPreset punchedReaction;
    public PlayerTransitionPreset playerKO;
    public PlayerTransitionPreset playerUseComputer;
    public PlayerTransitionPreset playerComputerExit;
    public PlayerTransitionPreset playerTakePrint;
    public PlayerTransitionPreset playerTakePrintExit;
    public PlayerTransitionPreset playerSearch;
    public PlayerTransitionPreset playerSearchExit;
    public PlayerTransitionPreset focusOnInteractable;

    [Header("Depth of Field")]
    public float dofNormalNearStart = 0;
    public float dofNormalNearEnd = 0;
    public float dofNormalFarStart = 0;
    public float dofNormalFarEnd = 0;
    [Space(7)]
    public float dofTalkingNearStart = 0;
    public float dofTalkingNearEnd = 0;
    public float dofTalkingFarStart = 0;
    public float dofTalkingFarEnd = 0;
    [Space(7)]
    public float dofPausedNearStart = 0;
    public float dofPausedNearEnd = 0;
    public float dofPausedFarStart = 0;
    public float dofPausedFarEnd = 0;
    [Space(7)]
    public float dofChangeTime = 0.25f;

    [Tooltip("Start the game with these first person items")]
    [ReorderableList]
    public List<FirstPersonItem> startingItems = new List<FirstPersonItem>();
    public FirstPersonItem nothingItem;
    public FirstPersonItem watchItem;
    public FirstPersonItem fistsItem;
    public FirstPersonItem coinItem;
    public FirstPersonItem printReader;
    [Tooltip("How long to display the item switch interface when it is activated.")]
    public float itemSwitchCounter = 2f;

    [Header("Stealth")]
    [Tooltip("Curve for ambient light levels throughout the day")]
    public AnimationCurve stealthAmbientLightLevel;
    [Tooltip("The above is multiplied by this when inside to give ambient level")]
    public float interiorAmbientLightMultiplier = 0.1f;
    [Tooltip("Transform for the floor light measuring point (the camera is used for the upper)")]
    public Transform floorLightMeasure;
    [Tooltip("Curve for direct sun light levels throughout the day")]
    public AnimationCurve stealthSunLightLevel;
    [Tooltip("How long in gametime a building alarm lasts once triggered: From high to low so we can lerp with skill multiplier")]
    public Vector2 buildingAlarmTime = new Vector2(0.4f, 0.1f);
    [Tooltip("How fast a camera/turret tracks it's target once alert")]
    public float securityTrackSpeed = 12f;
    [Tooltip("Citizen FoV")]
    public float citizenFOV = 160f;
    [Tooltip("Security FoV")]
    public float securityFOV = 80f;
    [Tooltip("Sabotage land value multiplier")]
    public float sabotageLandValueMP = 0.5f;
    [Tooltip("Citizen max sight range")]
    public float citizenSightRange = 25f;
    [Tooltip("Security max sight range")]
    public float securitySightRange = 16f;
    [Tooltip("Minimum stealth detection threshold. If target is closer than this, even targets with 0 visibility are spotted")]
    public float minimumStealthDetectionRange = 0.75f;
    [Tooltip("Sentry gun weapon config")]
    public MurderWeaponPreset sentryGunWeapon;
    [Tooltip("Sentry gun rate of fire")]
    public float sentryGunROF = 5f;
    [Tooltip("Sentry gun cone of fire")]
    public float sentryGunDamage = 0.25f;
    [Tooltip("Sentry gun cone of fire")]
    public float sentryGunAccuracy = 0.1f;
    [Tooltip("Maximum distance at which the player can officially 'spot' a citizen (used for triggering outlines etc)")]
    public float playerMaxSpotDistance = 20f;
    [Tooltip("Perform a player spotting check every x frames")]
    public int playerSpotUpdateEveryXFrame = 5;
    [Tooltip("The time before a spotted actor becomes invisible to the player (seconds)")]
    public float spottedGraceTime = 4f;
    [Tooltip("The time it takes for a previously spotted actor to become invisible to the player again (seconds)")]
    public float spottedFadeSpeed = 1f;
    [Tooltip("While sighted, the grace time multiplier for spotting a person is x1, how long is it for hearing a player?")]
    public float audioOnlySpotGraceTimeMultiplier = 0.5f;
    [Tooltip("Maximum distance for surveillance capturing the image of the player")]
    public float playerImageCaptureMaxRange = 14f;
    [Tooltip("If security catches player, any fines are now active for this long...")]
    public float buildingWantedTime = 24f;
    [Tooltip("How long before breakers are reset")]
    public float breakerResetTime = 0.25f;
    [Tooltip("How long before turned off security is reactivated")]
    public float securityResetTime = 2f;
    [Tooltip("How long in gametime does it take for a room to fill up with toxic gas")]
    public float gasFillTime = 0.1f;
    [Tooltip("How long in gametime does it take for a room to empty of toxic gas")]
    public float gasEmptyTime = 0.25f;

    [Space(7)]
    [Tooltip("How much time spent at a location trespassing before +1 is added to the escalation level")]
    public float additionalEscalationTime = 0.1f;

    [Header("Skills")]
    [Tooltip("Start the game with this amount of money")]
    public int startingMoney = 100;
    [Tooltip("Start the game with this presence level")]
    public int startingLockpicks = 3;
    [Tooltip("How much lock strength can be used up by a single lockpick as a range dictated by skill")]
    public Vector2 lockpickEffectivenessRange = new Vector2(0.2f, 1f);
    [Tooltip("Lockpicking speed multiplier")]
    public Vector2 lockpickSpeedRange = new Vector2(0.4f, 1f);
    [Tooltip("How much door strength damage is done when barged")]
    public Vector2 bargeDamageRange = new Vector2(0.05f, 0.1f);
    [Space(7)]
    public float baseMaxPlayerHealth = 1f;
    [Tooltip("The player recovers this amount of health (normalized) over time (game time 1 hour)")]
    public float playerRecoveryRate = 0.1f;
    [Tooltip("The player's starting combat skill")]
    public float playerCombatSkill = 1f;
    [Tooltip("The player's starting combat heft (damage per punch)")]
    public float playerCombatHeft = 0.25f;
    [Tooltip("The default number of inventory slots")]
    public int defaultInventorySlots = 3;
    [Tooltip("Damage multiplier for physics objects hitting the player")]
    public float incomingPlayerPhysicsDamageMultiplier = 0.5f;

    [Space(7)]
    public float commonSyncDisksPer200Citizens = 7;
    public float mediumSyncDisksPer200Citizens = 5;
    public float rareSyncDisksPer200Citizens = 3;
    public float veryRareSyncDisksPer200Citizens = 1;

    [Space(7)]
    public int corpSabotageMoney = 200;
    public int corpSabotageManagementBonus = 100;
    public int moneyForAddresses = 20;
    public int moneyForNewLocations = 20;
    public int moneyForAirDucts = 20;
    public int moneyForPasscodes = 100;
    public int moneyForReading = 20;
    public int moneyForStreetCleaning = 2;
    public int passiveIncome = 10;
    public float upgradeHeightModifier = 0.5f;
    public float upgradeRunSpeed = 2f;
    public float upgradeReach = 1.5f;
    public float upgradeHealth = 1f;
    public float upgradeRegen = 0.1f;
    [Tooltip("Applied to all fines: From high to lower and lerped with the health insurance skill")]
    public Vector2 legalInsuranceMultiplier = new Vector2(1f, 0.2f);

    [Space(7)]
    public int socialCreditForSideJobs = 50;
    public int socialCreditForMurders = 200;

    [Header("Social Credit")]
    public AnimationCurve socialCreditLevelCurve;

    [Header("Evidence")]
    [Tooltip("Hot food is warm for this time after purchase")]
    public float foodHotTime = 1f;
    public float timeOfDeathAccuracy = 1f;
    public EvidencePreset retailItemSoldDiscovery;
    public EvidencePreset retailItemNoSoldDiscovery;

    [Header("First Person Skin Materials")]
    public Material fistMaterial;
    public Material fingerUpperMaterial;
    public Material fingerLowerMaterial;
    public Material fingerTipMaterial;
    public Material thumbJointMaterial;

    [Header("Physics")]
    [Tooltip("Physics object interpolation: 'By default interpolation is turned off. Commonly rigidbody interpolation is used on the player's character. Physics is running at discrete timesteps, while graphics is renderered at variable frame rates. This can lead to jittery looking objects, because physics and graphics are not completely in sync. The effect is subtle but often visible on the player character, especially if a camera follows the main character. It is recommended to turn on interpolation for the main character but disable it for everything else.'")]
    public RigidbodyInterpolation interpolation = RigidbodyInterpolation.Interpolate;
    public float physicsOffTime = 3f;
    public PhysicsProfile defaultObjectPhysicsProfile;

    [Header("Trash")]
    [Tooltip("The world is allowed to have this much trash before it starts removing...")]
    public int globalCreatedTrashLimit = 500;
    [Tooltip("Trash limit per bin")]
    public int binTrashLimit = 12;

    [Header("Calls")]
    [Tooltip("Each building logs this many phone calls")]
    public int buildingCallLogMax = 100;

    [Header("Citizens")]
    [Tooltip("Multiply citizen speed in presimulation by this amount")]
    public float preSimSpeedMultiplier = 1.5f;
    [Tooltip("How much money a wallet contains based on citizien class")]
    public AnimationCurve walletCashAmountBasedOnWealth;
    public CharacterTrait creditCardTrait;
    public CharacterTrait donorCardTrait;

    [Header("Combat")]
    [Tooltip("How close the block has to land for a successful block (total range = 1.0)")]
    public float successfulBlockThreshold = 0.1f;
    [Tooltip("How close the block has to land for a perfect block (total range = 1.0)")]
    public float perfectBlockThreshold = 0.01f;
    [Tooltip("The minimum base attack delay (AI time between attack) in seconds. Modified by combat skill using left from min to max.")]
    public Vector2 baseAttackDelay = new Vector2(0.5f, 0.2f);
    [Tooltip("Blocking will use this base attack delay instead of the above. Modified by combat skill using left from min to max")]
    public Vector2 blockedAttackDelay = new Vector2(0.8f, 0.6f);
    [Tooltip("Blocking perfectly use this base attack delay instead of the above. Modified by combat skill using left from min to max")]
    public Vector2 perfectBlockAttackDelay = new Vector2(2f, 1.5f);
    [Tooltip("How much time before an enemy gets up after being KO'd (game time)")]
    public Vector2 koTimeRange = new Vector2(1f, 1f);
    [Tooltip("The force applied to an NPC ragdoll on KO")]
    public float playerKOPunchForce = 200f;
    [Tooltip("How much time passes when the player is KO'd (in-game hours)")]
    public float koTimePass = 5f;
    [Tooltip("How long (game time) will a citizen be restrained by handcuffs?")]
    public float restrainedTimer = 1f;
    [Tooltip("When using a takedown, how long a citizen will stay down for")]
    public float takedownTimer = 0.5f;
    [Tooltip("The fuse for a thrown grenade")]
    public float thrownGrenadeFuse = 3f;
    [Tooltip("The fuse for a proxy grenade")]
    public float proxyGrenadeFuse = 2.5f;

    [Space(5)]
    public PlayerTransitionPreset successfulBlockTransition;
    public PlayerTransitionPreset unsuccessfulBlockTransition;
    public PlayerTransitionPreset counterTransition;

    [Header("Tailing")]
    public float maxPlayerLookAtTailingDistance = 12f;
    [Tooltip("How fast the AI gains spooked while being looked at")]
    public float playerLookAtSpookRate = 0.2f;
    public float loseSpookedRate = -0.015f;
    public AnimationCurve screenCentreSpookCurve;

    [Header("Mugging")]
    [Tooltip("Chance for player to be mugged if the conditions are right")]
    public float muggingChance = 1f;

    [Header("Cleanup")]
    [Tooltip("Spatter removal time: Only applys when the spatter simulation is set to use this erase mode. In-game hours.")]
    public float spatterRemovalTime = 3f;
    [Tooltip("Moved objects reset time in-game hours.")]
    public float objectPositionResetTime = 4f;
    [Tooltip("Broken windows will become boarded up after this time")]
    public float brokenWindowBoardTime = 2.5f;
    [Tooltip("Broken windows will reset after this time")]
    public float brokenWindowResetTime = 24f;

    [Header("Crime/Cases")]
    [Tooltip("Fine amount for breaking windows")]
    public int breakingWindowsFine = 500;
    [Tooltip("Vandalism fine multiplier")]
    public int vandalismFineMultiplier = 4;
    [Tooltip("How long it takes to cancel out address vandalism")]
    public float vandalismTimeout = 24f;
    [Tooltip("Minimum amount of time for illegal actions to be present (seconds)")]
    public float illegalActionMinimumTime = 1f;
    [Tooltip("How many items can be tampered with before it is considered a crime")]
    public int tamperGrace = 3;
    [Tooltip("How far a physics object can be moved before it is considered a crime")]
    public float physicsTamperDistance = 1f;
    [Tooltip("Dynamic fingerprints stay in the world for this many in-game hours.")]
    public float fingerprintLife = 24f;
    [Tooltip("Maximum number of dynamic prints on an object at any one time.")]
    public int maxDynamicPrintsPerObject = 8;
    public InteractablePreset fignerprintPreset;
    [Tooltip("Time until suspects are detained after a call-in (gametime)")]
    public float detainDelay = 0.25f;
    [Tooltip("Time until results of the case are processed after detaining (gametime)")]
    public float caseResultProcessTime = 1f;
    [Tooltip("The number of murder victims needed to get the top rank")]
    public int bestCaseVictimCount = 1;
    [Tooltip("The number of murder victims needed to get the worst rank")]
    public int worstCaseVictimCount = 4;
    [Tooltip("Multiplier for job difficulty")]
    public AnimationCurve sideJobDifficultyRewardMultiplier;
    [Tooltip("Used for side jobs; leave item at this secret location")]
    public List<FurniturePreset> secretLocationFurniture = new List<FurniturePreset>();
    [Tooltip("Chance of triggering combat for stealing items from citizen's inventory")]
    public float stealTriggerChance = 0.5f;
    [Tooltip("The max number of side jobs/custom cases")]
    public int maxCases = 5;
    [Tooltip("The between a crime scene being sweeped, and the cleanup time")]
    public float crimeSceneCleanupDelay= 5f;
    [Tooltip("The minimum/maxinum distance a mission photo can be from the object")]
    public Vector2 missionPhotoMinMaxDistance = new Vector2(1f, 10f);
    [Tooltip("Scoring curve between min/max distances for a mission photo")]
    public AnimationCurve missionPhotoDistanceScoreCurve;

    [Header("Footprints")]
    [Tooltip("The maximum number of footprints per room")]
    public int maximumFootprintsPerRoom = 20;
    [Tooltip("The maximum time a footprint remains active (with a dirt value of 1)")]
    public float maximumFootprintTime = 24f;
    [Tooltip("Min/max size of footprints lerped shoe size")]
    public Vector2 footprintScaleRange = Vector2.one;
    [Tooltip("Each step removes this level of dirt from the citizen")]
    public float stepDirtRemoval = -0.01f;
    [Tooltip("Each step removes this level of blood from the citizen")]
    public float stepBloodRemoval = -0.01f;
    [Tooltip("Each step outside adds this level of dirt + material specific values")]
    public float outdoorStepDirtAccumulation = 0.011f;
    public InteractablePreset footprintPreset;

    [Header("Murders")]
    [Tooltip("How long do enforcers search a crime scene?")]
    public float crimeSceneSearchLength = 0.18f;
    [Tooltip("How long does a crime scene stay active after enforcers arrive?")]
    public float crimeSceneLength = 2f;
    [Tooltip("Time for a dead body smell to extend one additional room (starts with 0)")]
    public float smellTime = 1f;
    [Tooltip("Murder turn-in questions")]
    public List<Case.ResolveQuestion> murderResolveQuestions = new List<Case.ResolveQuestion>();
    [Tooltip("Retirement turn-in questions")]
    public List<Case.ResolveQuestion> retirementResolveQuestions = new List<Case.ResolveQuestion>();

    [Header("Computers")]
    [Tooltip("The cursor object to load")]
    public GameObject OScursor;
    [Tooltip("Cursor load sprite")]
    public Sprite loadCursor;

    [Header("Surveillance")]
    [InfoBox("Don't lower this past 75 or it will screw with the introduction set up process")]
    [Tooltip("Surveillance camera capture FoV")]
    public float captureFoV = 180f;
    [Tooltip("Surveillance camera capture range (nodes/visuals)")]
    public float captureRange = 18f;
    [Tooltip("Surveillance camera capture range (humans)")]
    public float humanCaptureRange = 12.5f;
    [Tooltip("Gap between capturing for cameras (gametime 0.1667 = 10 mins)")]
    public float captureInterval = 0.1667f;
    [Tooltip("How many captures a camera can hold before overwriting (288 = 48 hours @ 10 mins)")]
    public int cameraCaptureMemory = 288;
    [Tooltip("How old a camera capture can be before overwriting")]
    public float cameraCaptureMaxTime = 48f;
    [Tooltip("Maximum camera captures per gameworld cycle")]
    public int maxCapturesPerFrame = 24;

    [Header("Upgrades")]
    [ReorderableList]
    public List<SyncDiskColour> syncDiskColours = new List<SyncDiskColour>();

    [System.Serializable]
    public class SyncDiskColour
    {
        public SyncDiskPreset.Manufacturer category;
        public Color mainColour = Color.white;
        public Color colour1 = Color.white;
        public Color colour2 = Color.white;
        public Color colour3 = Color.white;
    }

    public int defaultDiskSlots = 3;

    [Header("General")]
    [Tooltip("Scrolling sensitivity of the mouse wheel when not zooming")]
    public int mouseWheelEvidenceScrollSensitivity = 22;
    [Tooltip("Indoor temperature (-2.5 - 2.5)")]
    public float indoorTemperature = 1.5f;
    [Tooltip("Air duct temperature")]
    public float airDuctTemperature = -1.5f;
    [Tooltip("Heat source temperature")]
    public float heatSourceTemperature = 10f;
    [Tooltip("Oscillators")]
    public AnimationCurve oscillatorX;
    public AnimationCurve oscillatorY;
    public Vector2 drunkOscillationSpeed = new Vector2(0.2f, 0.5f);
    [Tooltip("Shiver fluctuation")]
    public AnimationCurve shiverFluctuation;
    public Vector2 shiverOscillationSpeed = new Vector2(1f, 1.2f);
    [Tooltip("Drunk Lens Distort")]
    public AnimationCurve drunkLensDistortOscillator;
    public Vector2 drunkLensDistortSpeed = new Vector2(1f, 1.2f);
    public PlayerTransitionPreset tripTransition;
    [Tooltip("Headache fluctuation")]
    public AnimationCurve headacheFluctuation;
    public SpatterPatternPreset bleedingSpatter;
    public float fallDamageMultiplier = 0.1f;
    public StatusPreset detainedStatus;
    public StatusPreset wantedInBuildingStatus;

    [Space(7)]
    public float playerHungerRate = 0.125f;
    public float playerThirstRate = 0.2f;
    public float playerTirednessRate = 0.12f;
    public float playerEnergyRate = 0.058f;

    [Space(7)]
    public float combatHitChanceOfBruised = 0.1f;
    public float combatHitChanceOfBlackEye = 0.06f;
    public float combatHitChanceOfBrokenLeg = 0.01f;
    public float combatHitChanceOfBleeding = 0.1f;

    [Header("Pricing")]
    public Vector2 propertyValueRange = new Vector2(800, 100000);
    public AnimationCurve propertyValueCurve;

    [Header("Loan Sharks")]
    [Tooltip("How much the player gets now")]
    public int defaultLoanAmount = 2000;
    [Tooltip("How much extra the player pays in full")]
    public int defaultLoanExtra = 250;
    [Tooltip("The daily repayment per day")]
    public int defaultLoanRepayment = 250;

    [Header("Scope Bases")]
    public DDSScope humanScope;
    public DDSScope itemScope;
    public DDSScope murderScope;
    public DDSScope locationScope;
    public DDSScope evidenceScope;
    public DDSScope sideJobScope;
    public DDSScope syncDiskScope;
    public DDSScope groupScope;

    //Singleton pattern
    private static GameplayControls _instance;
    public static GameplayControls Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        startingDate = Mathf.Max(startingDate, 0);
        startingMonth = Mathf.Max(startingMonth, 0);
        startingYear = Mathf.Max(startingYear, 1);

        while (yearZeroLeapYearCycle >= 4)
        {
            yearZeroLeapYearCycle -= 4;
        }

        while (dayZero >= 7)
        {
            dayZero -= 7;
        }
    }
}
