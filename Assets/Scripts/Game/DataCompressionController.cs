using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

public class DataCompressionController : MonoBehaviour
{
    //Singleton pattern
    private static DataCompressionController _instance;
    public static DataCompressionController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public async Task<bool> CompressAndSaveDataAsync<T>(T input, string filePath, int compressionQuality = 9) where T : class
    {
        Game.Log("CityGen: Converting class to JSON UTF8 text...");
        bool success = false;
        string jsonString = JsonUtility.ToJson(input, false);

        await Task.Run(() =>
        {
            var bytes = System.Text.Encoding.UTF8.GetBytes(jsonString); //Get bytes

            if (bytes != null)
            {
                Game.Log("CityGen: Successfully encoded class to bytes: " + bytes.Length);
                byte[] buff = new byte[0];
                ulong[] progress = new ulong[1];

                //compress a byte buffer (we write the output buffer to a file for debug purposes.)
                if (brotli.compressBuffer(bytes, ref buff, progress, true, compressionQuality))
                {
                    Game.Log("CityGen: Writing compressed data to file " + filePath);
                    File.WriteAllBytes(filePath, buff);
                    success = true;
                }
                else
                {
                    Game.LogError("Unable to compress byte data");
                    success = false;
                }
            }
        });

        return success;
    }

    public async Task<bool> LoadCompressedDataAsync<T>(string filePath, Action<T> onComplete) where T : class
    {
        T output = default;

        //Get the output type
        Type typeParameterType = typeof(T);

        Game.Log("CityGen: Attempting to load compressed data from " + filePath);

        if (!File.Exists(filePath))
        {
            Game.LogError("Unable to load file at path " + filePath);
            return false;
        }

        bool success = false;
        byte[] loadedBytes = null;

        await Task.Run(() =>
        {
            loadedBytes = File.ReadAllBytes(filePath);
        });

        if (loadedBytes != null)
        {
            Game.Log("CityGen: Loaded " + loadedBytes.Length + " from file...");

            // NEW BUFFER FUNCTION
            try
            {
                string jsonString = null;

                await Task.Run(() =>
                {
                    var newBuffer = brotli.decompressBuffer(loadedBytes, true);

                    if (newBuffer != null)
                    {
                        Game.Log("CityGen: Successfully decompressed " + newBuffer.Length + " bytes from file. Attempting to parse into class " + typeParameterType + "...");
                        jsonString = System.Text.Encoding.UTF8.GetString(newBuffer);
                    }
                });

                if (!string.IsNullOrEmpty(jsonString))
                {
                    Game.Log("CityGen: Succesfully decoded compressed data into a JSON string...");
                    output = JsonUtility.FromJson<T>(jsonString);
                    success = true;
                }
                else
                {
                    Game.LogError("CityGen: Unable to parse decoded compressed data into a JSON string...");
                    success = false;
                }

                //var newBuffer = brotli.decompressBuffer(loadedBytes, true);

                //if (newBuffer != null)
                //{
                //    Game.Log("CityGen: Successfully decompressed " + newBuffer.Length + " bytes from file. Attempting to parse into class " + typeParameterType + "...");

                //    string jsonString = System.Text.Encoding.UTF8.GetString(newBuffer);
                //    output = JsonUtility.FromJson<T>(jsonString);
                //    success = true;
                //}
            }
            catch
            {
                Game.Log("CityGen: Unable to load compressed save game with buffer...");
            }

            if (!success)
            {
                Game.LogError("CityGen: Failed to load save game using buffer, attempting to use old method (file decompress)...");

                try
                {
                    string tempFile = Application.persistentDataPath + "/save/" + "CompressedSave.temp";
                    ulong[] progress = new ulong[1];
                    string jsonString = null;

                    await Task.Run(() =>
                    {
                        brotli.decompressFile(RestartSafeController.Instance.saveStateFileInfo.FullName, tempFile, progress);

                        //Parse to class file
                        using (StreamReader streamReader = File.OpenText(tempFile))
                        {
                            jsonString = streamReader.ReadToEnd();
                        }
                    });

                    if (!string.IsNullOrEmpty(jsonString))
                    {
                        Game.Log("CityGen: Succesfully decoded compressed data into a JSON string...");
                        output = JsonUtility.FromJson<T>(jsonString);
                        success = true;
                    }
                    else
                    {
                        Game.LogError("CityGen: Unable to parse decoded compressed data into a JSON string...");
                        success = false;
                    }
                }
                catch
                {
                    Game.LogError("CityGen: Unable to load compressed save game with temp file decompress...");
                    success = false;
                }
            }
        }

        onComplete?.Invoke(output);
        return success;
    }
}
