﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEditor;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "syncdisk_data", menuName = "Database/Sync Disk Preset")]

public class SyncDiskPreset : SoCustomComparison
{
	[BoxGroup("Disable")]
	[Tooltip("Disable this in-game completely.")]
	public bool disabled = false;

	[Header("Configuration")]
	public int syncDiskNumber = 1;
	public InteractablePreset interactable;
	public Rarity rarity = Rarity.medium;
	public Manufacturer manufacturer = Manufacturer.ElGen;
	public bool canBeSideJobReward = true;

	public enum Rarity { common, medium, rare, veryRare};

	public enum Manufacturer { ElGen, Kaizen, KensingtonIndigo, StarchKola, CandorNews, BlackMarket}

	public enum Effect { none, streetCleaningMoney, readingMoney, readingSeriesBonus, starchLoan, starchAddiction, reduceMedicalCosts, legalInsurance, accidentCover, awakenAtHome, increaseHealth, increaseInventory, increaseRegeneration, priceModifier, dialogChanceModifier, doorBargeModifier, fallDamageModifier, sideJobPayModifier, punchPowerModifier, throwPowerModifier, blockIncoming, focusFromDamage, noBrokenBones, reachModifier, holdingBlocksBullets, fistsThreatModifier, noBleeding, incomingDamageModifier, passiveIncome, installMalware, malwareOwnerBonus, footSizePerception, heightPerception, wealthPerception, salaryPerception, singlePerception, agePerception, starchAmbassador, starchGive, lockpickingSpeedModifier, lockpickingEfficiencyModifier, triggerIllegalOnPick, KOTimeModifier, securityBreakerModifier, securityGraceTimeModifier, noSmelly, noCold, noTired, kitchenPhotos, bathroomPhotos, illegalOpsPhotos, playerHeightModifier, removeSideEffect, moneyForLocations, moneyForDucts, moneyForAddresses, moneyForPasscodes, maxSpeedModifier };

	public enum UpgradeEffect { none, modifyEffect, bothConfigurations, readingSeriesBonus, reduceUninstallCost, reduceMedicalCosts, accidentCover, legalInsurance, awakenAtHome, increaseHealth, increaseInventory, increaseRegeneration, priceModifier, dialogChanceModifier, doorBargeModifier, fallDamageModifier, sideJobPayModifier, punchPowerModifier, throwPowerModifier, blockIncoming, focusFromDamage, noBrokenBones, reachModifier, holdingBlocksBullets, fistsThreatModifier, noBleeding, incomingDamageModifier, passiveIncome, installMalware, malwareOwnerBonus, footSizePerception, heightPerception, wealthPerception, removeSideEffect, salaryPerception, singlePerception, agePerception, starchAmbassador, starchGive, lockpickingSpeedModifier, lockpickingEfficiencyModifier, triggerIllegalOnPick, KOTimeModifier, securityBreakerModifier, securityGraceTimeModifier, noSmelly, noCold, noTired, kitchenPhotos, bathroomPhotos, illegalOpsPhotos, playerHeightModifier, moneyForLocations, moneyForDucts, moneyForAddresses, moneyForPasscodes, maxSpeedModifier };

	public enum SpecialCase { none, cancelSideEffect };

	[Header("Usage")]
	public string mainEffect1Name;
	public string mainEffect1Description;
	public Effect mainEffect1;
	public float mainEffect1Value = 0.2f;
	[ShowAssetPreview]
	public Sprite mainEffect1Icon;

	[Space(7)]
	public string mainEffect2Name;
	public string mainEffect2Description;
	public Effect mainEffect2;
	public float mainEffect2Value = 0.2f;
	[ShowAssetPreview]
	public Sprite mainEffect2Icon;

	[Space(7)]
	public string mainEffect3Name;
	public string mainEffect3Description;
	public Effect mainEffect3;
	public float mainEffect3Value = 0.2f;
	[ShowAssetPreview]
	public Sprite mainEffect3Icon;

	[Header("Upgrade Option 1")]
	public List<string> option1UpgradeNameReferences = new List<string>();
	public List<UpgradeEffect> option1UpgradeEffects = new List<UpgradeEffect>();
	public List<float> option1UpgradeValues = new List<float>();
	[Space(7)]
	[Header("Upgrade Option 2")]
	public List<string> option2UpgradeNameReferences = new List<string>();
	public List<UpgradeEffect> option2UpgradeEffects = new List<UpgradeEffect>();
	public List<float> option2UpgradeValues = new List<float>();
	[Space(7)]
	[Header("Upgrade Option 3")]
	public List<string> option3UpgradeNameReferences = new List<string>();
	public List<UpgradeEffect> option3UpgradeEffects = new List<UpgradeEffect>();
	public List<float> option3UpgradeValues = new List<float>();

	[Header("Effects")]
	public string sideEffectDescription;
	public Effect sideEffect = Effect.none;
	public float sideEffectValue = 0f;

	[Header("Costs")]
	public int price = 500;
	public int uninstallCost = 0;

	[Header("Ownership")]
	[Range(0f, 1f)]
	public float minimumWealthLevel = 0f;

	[Range(0, 5)]
	public int traitWeight = 1;

	[System.Serializable]
	public class TraitPick
	{
		public CharacterTrait.RuleType rule = CharacterTrait.RuleType.ifAnyOfThese;
		public List<CharacterTrait> traitList = new List<CharacterTrait>();
		[Tooltip("If this isn't true then it won't be picked for application at all.")]
		public bool mustPassForApplication = false;
		public int appliedFrequency = 1;
	}

	[ReorderableList]
	public List<TraitPick> traits = new List<TraitPick>();

	[Range(0, 5)]
	public int occupationWeight = 0;

	[ReorderableList]
	public List<OccupationPreset> occupation = new List<OccupationPreset>();

	[Header("Debug")]
	public SyncDiskPreset copyFrom;

	[Button]
	public void CopyOwnershipStats()
	{
		if(copyFrom != null)
		{
			minimumWealthLevel = copyFrom.minimumWealthLevel;
			traitWeight = copyFrom.traitWeight;
			traits.Clear();
			traits.AddRange(copyFrom.traits);
			occupationWeight = copyFrom.occupationWeight;
			occupation.Clear();
			occupation.AddRange(copyFrom.occupation);

#if UNITY_EDITOR
			EditorUtility.SetDirty(this);
#endif
		}
	}
}
