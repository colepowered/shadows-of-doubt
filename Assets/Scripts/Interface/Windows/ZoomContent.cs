﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//Controls the zooming of window content.
//Script pass 1
public class ZoomContent : MonoBehaviour
{
    [Header("Zoom settings")]
    [Tooltip("Which controller axis to poll")]
    public string zoomAxis = "EvidenceZoom";
    [Tooltip("Use controller input to affect zoom level")]
    public bool enableZoomWithMouseWheel = true;
    [Tooltip("Toggle true if this is the first person map")]
    public bool enableInFirstPersonMap = false;

    [Space(7)]
    public bool useZoomSteps = true;
    [EnableIf("useZoomSteps")]
    public int numberOfSteps = 10;
    [DisableIf("useZoomSteps")]
    public float zoomSensitivity = 0.2f;
    [DisableIf("useZoomSteps")]
    public float controllerSensitivityMultiplier = 0.5f;

    [Space(7)]
    [Tooltip("Scaling of the zoom level: Normalized values")]
    public AnimationCurve zoomCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
    [Tooltip("How fast this zooms in/out")]
    public float smoothZoomSpeed = 4f;
    [Tooltip("Min/max limits of the zoom")]
    public Vector2 zoomLimit = new Vector2(1, 4);
    [Tooltip("How much the centre of the viewpoint changes to the cursor position")]
    public float zoomToCursorPercentage = 1f;

    [Space(7)]
    public float zoom = 1f;
    public float desiredZoom = 1f;
    public float normalizedZoom = 1f;
    [ReadOnly]
  	public float zoomProgress = 1f;
    [ReadOnly]
    public Vector2 normalSize;
    [ReadOnly]
    public float axisInputDelay = 0f;

    [Space(7)]
    [Tooltip("If the mouse is over one of these UI elements then allow zoom")]
    public List<string> allowedMouseOverTags = new List<string>();
    [Tooltip("Zoom these additional rectTransforms")]
    public List<RectTransform> additionalRects = new List<RectTransform>();

    [Header("References")]
    public InfoWindow window;
    public RectTransform containerRect;
    public CustomScrollRect scroll;
    public RectTransform scrollRectArea;
    public ViewportMouseOver viewportMouseOver;
    public WindowContentController contentController;

    public enum ZoomPivot { mousePosition, playerMapPosition };

    void Awake()
    {
        window = this.gameObject.GetComponentInParent<InfoWindow>();

        if (scroll == null) scroll = window.scrollRect;

        containerRect = this.GetComponent<RectTransform>();
        scrollRectArea = scroll.gameObject.GetComponent<RectTransform>();
        viewportMouseOver = scroll.viewport.gameObject.GetComponent<ViewportMouseOver>();
        contentController = this.gameObject.GetComponent<WindowContentController>();

        normalSize = containerRect.sizeDelta;

        //If using zoom steps, start the step count on the closest value to the default zoom (1)
        //if (useZoomSteps)
        //{
        //    float s = zoomLimit.y - zoomLimit.x;
        //    zoomStep = 0;

        //    float closestValue = 0;

        //    for (int i = 0; i < numberOfSteps; i++)
        //    {
        //        float val = zoomLimit.x + (i * s);

        //        if(Mathf.Abs(1 - val))
        //        {

        //        }
        //    }
        //}
    }

    private void Start()
    {
        //TODO: Calculate the existing default zoom position based on scale (usually 1)
        //At the moment this is manually configured
    }

    void SetPivotPoint(float pivotBias, ZoomPivot usePivot = ZoomPivot.mousePosition)
    {
        if(scrollRectArea == null)
        {
            Game.LogError("Missing scrollRectArea for zoom! Assign this");
            return;
        }

        if (containerRect == null)
        {
            Game.LogError("Missing containerRect for zoom! Assign this");
            return;
        }

        if (scroll == null)
        {
            Game.LogError("Missing scroll for zoom! Assign this");
            return;
        }

        //Get screen postion of middle of rect
        Vector3[] corners = new Vector3[4];
     	scrollRectArea.GetWorldCorners(corners);

     	Vector2 middle = Vector2.zero;

     	foreach (Vector3 corner in corners)
     	{
     		middle.x += corner.x;
     		middle.y += corner.y;
     	}

        try
        {
     	    middle.x /= 4;
     	    middle.y /= 4;
		    middle = RectTransformUtility.WorldToScreenPoint(null, middle);

            //Get a middle ground between zooming to the centre of the screen, and zooming to the mouse
            Vector2 pivotBiasPosition = Input.mousePosition;

            if(usePivot == ZoomPivot.playerMapPosition)
            {
                pivotBiasPosition = RectTransformUtility.WorldToScreenPoint(null, MapController.Instance.playerCharacterRect.position);
            }

		    Vector2 zoomPos = Vector2.LerpUnclamped(middle, pivotBiasPosition, pivotBias);

		    //Get mouse position relative to viewport
		    Vector2 zoomCentre = Vector2.zero;
		    RectTransformUtility.ScreenPointToLocalPointInRectangle (containerRect, zoomPos, null, out zoomCentre);

		    //Get the new centre as a normalized position
		    float normalX = Mathf.Clamp((zoomCentre.x + containerRect.sizeDelta.x * containerRect.pivot.x) / containerRect.sizeDelta.x, 0f, 1f);
		    float normalY = Mathf.Clamp((zoomCentre.y + containerRect.sizeDelta.y * containerRect.pivot.y) / containerRect.sizeDelta.y, 0f, 1f);
		    Vector2 newPivot = new Vector2(normalX, normalY);

		    Vector2 size = new Vector2(containerRect.rect.size.x * containerRect.localScale.x, containerRect.rect.size.y * containerRect.localScale.y);
            Vector2 deltaPivot = containerRect.pivot - newPivot;
            Vector3 deltaPosition = new Vector3(deltaPivot.x * size.x, deltaPivot.y * size.y);

            containerRect.pivot = newPivot;
		    containerRect.localPosition -= deltaPosition;

		    //Update various things to do with the original unity script
		    scroll.SetAnchorPos(containerRect.anchoredPosition);

		    //If scrolling, update the start position
		    scroll.ScrollZoom(deltaPosition);
        }
        catch
        {
            Game.LogError("Unable to set pivot for zoom");
        }
    }

    public void ResetPivot()
    {
        if(containerRect != null) containerRect.pivot = new Vector2(0.5f, 0.5f);
    }

    void LateUpdate()
    {
        bool allowed = false;

        try
        {
            if(viewportMouseOver.isOver || (enableInFirstPersonMap && MapController.Instance.displayFirstPerson && SessionData.Instance.play))
            {
                //If window is null, this is the case board...
                if(window == null)
                {
                    if(InterfaceController.Instance.desktopMode)
                    {
                        if(InterfaceController.Instance.pinnedBeingDragged == null)
                        {
                            if(InterfaceController.Instance.currentMouseOverElement.Count <= 0)
                            {
                                allowed = true;
                            }
                            else if(allowedMouseOverTags.Count > 0)
                            {
                                allowed = true;

                                foreach(MonoBehaviour uiElement in InterfaceController.Instance.currentMouseOverElement)
                                {
                                    if(!allowedMouseOverTags.Contains(uiElement.transform.tag))
                                    {
                                        //Game.Log("Interface: " + name + " zoom blocked by mouse over element: " + uiElement.transform.tag);
                                        allowed = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                //Game.Log("Interface: Zoom blocked by mouse over element count: " + InterfaceController.Instance.currentMouseOverElement.Count);
                            }
                        }
                        else
                        {
                            //Game.Log("Interface: Zoom blocked by drag element");
                        }
                    }
                    else
                    {
                        //Game.Log("Interface: Zoom blocked by not being in desktop mode");
                    }
                }
                else
                {
                    allowed = true;
                }
            }
            else
            {
                //Game.Log("Interface: Zoom blocked because mouse is not over " + name);
            }

            if (allowed && enableZoomWithMouseWheel && !InterfaceController.Instance.playerTextInputActive)
            {
                ZoomPivot pivotSetting = ZoomPivot.mousePosition;
                if (enableInFirstPersonMap && MapController.Instance.displayFirstPerson && SessionData.Instance.play) pivotSetting = ZoomPivot.playerMapPosition;

                if (InputController.Instance.player.GetButtonDown("CaseBoard"))
                {
                    if(!enableInFirstPersonMap && !InputController.Instance.mouseInputMode)
                    {
                        SetPivotPoint(0f, pivotSetting);
                    }
                    else
                    {
                        SetPivotPoint(1f, pivotSetting);
                    }
                }

                //Zoom
                if (Mathf.Abs(InputController.Instance.player.GetAxis(zoomAxis)) > 0.05f && axisInputDelay <= 0f)
                {
                    float wheel = InputController.Instance.player.GetAxis(zoomAxis);
                    axisInputDelay = 0.1f;

                    //If using zoom steps, add or minus one per axis change
                    if(useZoomSteps)
                    {
                        int stepAlter = 0;

                        if (wheel > 0f)
                        {
                            stepAlter = Mathf.Clamp(Mathf.CeilToInt(wheel), -1, 1);
                        }
                        else
                        {
                            stepAlter = Mathf.Clamp(Mathf.FloorToInt(wheel), -1, 1);
                        }

                        if (stepAlter == 0) return;

                        //How much is a step?
                        float perStep = (zoomLimit.y - zoomLimit.x) / (float)numberOfSteps;
                        desiredZoom += perStep * stepAlter;

                        //Game.Log("Interface: Zoom step alter: " + desiredZoom + " (" + perStep + " * " + stepAlter + ") " + InputController.Instance.player.GetAxis(zoomAxis));
                    }
                    else
                    {
                        //Set the desired new zoom
                        float cS = 1f;
                        if (!InputController.Instance.mouseInputMode) cS = controllerSensitivityMultiplier;
                        desiredZoom += wheel * zoomSensitivity * cS;
                    }

                    //Clamp to avoid the desired level from being out of boundary
                    desiredZoom = Mathf.Clamp(desiredZoom, zoomLimit.x, zoomLimit.y);

                    //Clamp to 2dp for rounding causing weirdness
                    int dP = Mathf.RoundToInt(desiredZoom * 100f);
                    desiredZoom = dP / 100f;

                    //Game.Log("Interface: Set desired zoom for " + name + ": " + desiredZoom);

                    //If zooming out, use the screen zoom only
                    if(desiredZoom != zoom)
                    {
                        float zoomMethod = zoomToCursorPercentage;
                        if (!InputController.Instance.mouseInputMode) zoomMethod = 0f;

                        if (wheel < 0f) zoomMethod = 0f;

                        SetPivotPoint(zoomMethod, pivotSetting);

                        //Reset progress
                        zoomProgress = 0f;
                    }
                }

                if (axisInputDelay > 0f) axisInputDelay -= Time.deltaTime;
            }
            else
            {
                //Game.Log("Interface: Zoom in " + name + " is not allowed! " + allowed + ", " + enableZoomWithMouseWheel + ", " + InterfaceController.Instance.playerTextInputActive);
            }

		    //Lerp zoom movement
		    if (zoomProgress < 1f)
		    {
			    //Amount to lerp
			    zoomProgress += smoothZoomSpeed * Time.smoothDeltaTime;
                zoomProgress = Mathf.Clamp01(zoomProgress);

			    //New Zoom & clamp
			    zoom = Mathf.Lerp(zoom, desiredZoom, zoomProgress);

                //Game.Log("Zoom lerp from " + zoom + " to " + desiredZoom + " (progress: " + zoomProgress + ")");

                if (contentController != null)
                {
                    zoom = Mathf.Clamp(zoom, Mathf.Max(zoomLimit.x, contentController.fitScale), zoomLimit.y);
                    //Game.Log("Zoom clamp: " + zoom);
                }

                //Get normalized
                normalizedZoom = GetNormalizedZoom(zoom);
                ApplyZoom(normalizedZoom);
		    }
        }
        catch
        {
            Game.LogError("Viewport error!");
        }
    }

    public float GetNormalizedZoom(float zoom)
    {
        return Mathf.Clamp01((zoom - zoomLimit.x) / (zoomLimit.y - zoomLimit.x));
    }

    public void ApplyZoom(float normalizedZoom)
    {
        //Get actual value from graph
        float z = Mathf.Clamp01(zoomCurve.Evaluate(normalizedZoom)); //Curve should return 0 - 1

        //Game.Log("Apply zoom: " + z + " (" + normalizedZoom + ")");

        //Expand normalized out to scale
        z *= (zoomLimit.y - zoomLimit.x);
        z += zoomLimit.x;

        z = Mathf.RoundToInt(z * 100f) / 100f; //Clamp 2DP

        //Change scale
        if (containerRect != null) containerRect.localScale = new Vector3(z, z, 1f);

        foreach (RectTransform rec in additionalRects)
        {
            if (rec != null) rec.localScale = new Vector3(z, z, 1f);
        }
    }

    //Set zoom
    public void SetZoom(float newZoom)
    {
		//New Zoom & clamp
		zoom = Mathf.Clamp (newZoom, zoomLimit.x, zoomLimit.y);

        //Game.Log("Set zoom: " + zoom);
		desiredZoom = zoom;

        normalizedZoom = GetNormalizedZoom(zoom);
        ApplyZoom(normalizedZoom);
    }
}