﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "motive_data", menuName = "Database/Motive Preset")]

public class MotivePreset : SoCustomComparison
{
    [System.Serializable]
    public class ModifierRule
    {
        public CharacterTrait.RuleType rule = CharacterTrait.RuleType.ifAnyOfThese;

        public List<CharacterTrait> traitList = new List<CharacterTrait>();
        [Tooltip("If this isn't true then it won't be picked for application at all.")]
        public bool mustPassForApplication = true;
        public int score = 0;
    }

    [Header("Purpetrator")]
    public bool allowHomelessPurps = false;
    public bool allowJoblessPurps = true;
    public bool purpMustLiveAtDifferentAddressToPoster = true;
    public bool allowEnforcers = true;
    [ReorderableList]
    [Tooltip("Purps must follow these trait rules")]
    public List<ModifierRule> purpTraitModifiers = new List<ModifierRule>();
    [Space(7)]
    [Tooltip("Purp must have one of these jobs...")]
    public bool usePurpJobs = false;
    [ReorderableList]
    public List<OccupationPreset> purpJobs = new List<OccupationPreset>();

    [Header("Posters")]
    public bool allowHomelessPosters = false;
    public bool allowJoblessPosters = true;
    public bool usePosterConnections = true;
    [Tooltip("Posters must be one of these connections (poster connection to purp)...")]
    public List<Acquaintance.ConnectionType> acceptableConnections = new List<Acquaintance.ConnectionType>();

    public bool usePosterTraits = false;
    [EnableIf("usePosterTraits")]
    public List<ModifierRule> posterTraitModifiers = new List<ModifierRule>();

    [Header("Exempt")]
    [Tooltip("The chosen purp is exempt from further side jobs.")]
    public bool purpIsExemptFromPostingOtherJobs = true;
    public bool purpIsExemptFromPurpingOtherJobs = false;
    [Tooltip("The chosen poster is exempt from further side jobs.")]
    public bool posterIsExemptFromPostingOtherJobs = true;
    public bool posterIsExemptFromPurpingOtherJobs = true;
}
