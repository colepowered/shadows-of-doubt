using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;

public class CutSceneController : MonoBehaviour
{
    [Header("Components")]
    public Image displayImage;
    public CanvasRenderer displayImageRend;

    [Header("State")]
    public bool cutSceneActive = false;
    public float sceneTimer = 0f;
    public CutScenePreset preset;
    public int cursor = 0;
    CutScenePreset.CutSceneElement previousElement;
    List<CutScenePreset.CameraMovement> currentCamMovement;
    CutScenePreset.CameraMovement currentFrom;
    CutScenePreset.CameraMovement currentTo;

    public float currentShotTimer = 0f;

    //Saved variables
    private Vector3 playerSavedPosition;
    private Quaternion camSavedLocalQuat;
    private bool savedFreeCam;
    private bool savedInaudible;
    private bool savedInvisible;
    private bool savedInvincible;
    private bool savedPhotoMode;
    private bool triggeredFadeOut = false;
    private CutScenePreset.CutSceneElement finalShot;
    private bool triggeredImage = false;
    private float imageFadeIn = 0f;
    private float imageFadeOut = 1f;

    [Header("Debug")]
    public CutScenePreset debugLoad;

    //Singleton pattern
    private static CutSceneController _instance;
    public static CutSceneController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void PlayCutScene(CutScenePreset newPreset)
    {
        if(cutSceneActive)
        {
            Game.Log("Gameplay: Cut scene " + preset.name + " is already active...");
            return;
        }

        preset = newPreset;
        cursor = 0;
        sceneTimer = 0f;
        previousElement = null;

        if(preset.fadeIn)
        {
            //Fade in
            InterfaceController.Instance.fade = 1f;
            InterfaceController.Instance.Fade(0f, preset.fadeInTime, true);
        }

        if (preset.fadeOut)
        {
            triggeredFadeOut = false;

            for (int i = 0; i < preset.elementList.Count; i++)
            {
                if (preset.elementList[i].elementType == CutScenePreset.ElementType.newShot) finalShot = preset.elementList[i];
            }
        }

        SetActive(true);

        displayImage.sprite = preset.displayImage;
        displayImageRend.SetAlpha(0f);
        imageFadeIn = 0f;
        imageFadeOut = 0f;
        triggeredImage = false;
    }

    private void Update()
    {
        if (cutSceneActive)
        {
            if(SessionData.Instance.play)
            {
                if (cursor < preset.elementList.Count)
                {
                    CutScenePreset.CutSceneElement currentElement = preset.elementList[cursor];
                    sceneTimer += Time.deltaTime;

                    while(currentElement.disable && cursor < preset.elementList.Count)
                    {
                        cursor++;
                        currentElement = preset.elementList[cursor];
                    }

                    bool newShot = false;

                    //Run once on new element change
                    if(previousElement != currentElement)
                    {
                        if (currentElement.elementType == CutScenePreset.ElementType.newShot)
                        {
                            currentShotTimer = 0f;
                            newShot = true;
                            currentCamMovement = new List<CutScenePreset.CameraMovement>(currentElement.movement);
                            currentCamMovement.Sort((p1, p2) => p1.atDuration.CompareTo(p2.atDuration)); //Sort lowest timing first
                            currentFrom = currentCamMovement[0];

                            Game.Log("Gameplay: Loading current shot with " + currentCamMovement.Count + " movement nodes...");
                            if (currentCamMovement.Count > 1) currentTo = currentCamMovement[1];
                            else currentTo = null;

                            //Trigger dds
                            if (currentElement.ddsMessage != null && currentElement.ddsMessage.Length > 0)
                            {
                                //Gather list of things to say from this message
                                List<string> say = Player.Instance.ParseDDSMessage(currentElement.ddsMessage, null, out _);

                                //We can add all these to the citizen's speak queue at once
                                foreach (string str in say)
                                {
                                    Game.Log("Gameplay: Trigger cut scene text: " + str);
                                    Player.Instance.speechController.Speak("dds.blocks", str, true, interupt: true, forceColour: true, color: Color.white, delay: currentElement.messageDelay);
                                }
                            }

                            
                        }
                        else if(currentElement.elementType == CutScenePreset.ElementType.ddsMessage)
                        {
                            //Trigger dds
                            if (currentElement.ddsMessage != null && currentElement.ddsMessage.Length > 0)
                            {
                                //Gather list of things to say from this message
                                List<string> say = Player.Instance.ParseDDSMessage(currentElement.ddsMessage, null, out _);

                                //We can add all these to the citizen's speak queue at once
                                foreach (string str in say)
                                {
                                    Game.Log("Gameplay: Trigger cut scene text: " + str);
                                    Player.Instance.speechController.Speak("dds.blocks", str, true, interupt: true, forceColour: true, color: Color.white, delay: currentElement.messageDelay);
                                }
                            }

                            cursor++;
                        }

                        previousElement = currentElement;
                    }

                    //Animate shot
                    if(currentElement.elementType == CutScenePreset.ElementType.newShot)
                    {
                        currentShotTimer += Time.deltaTime;

                        if(currentFrom != null)
                        {
                            //Shot is moving
                            if(currentTo != null)
                            {
                                if (currentShotTimer <= currentTo.atDuration)
                                {
                                    float localProgress = Mathf.InverseLerp(currentFrom.atDuration, currentTo.atDuration, currentShotTimer);

                                    //Lerp pos
                                    Vector3 camFromPos = currentFrom.camPos;
                                    Vector3 camToPos = currentTo.camPos;
                                    Vector3 zeroTile = CityData.Instance.CityTileToRealpos(Vector2.zero);

                                    if(currentFrom.anchor == CutScenePreset.AnchorType.blockCorner)
                                    {
                                        camFromPos += zeroTile;
                                    }

                                    if (currentTo.anchor == CutScenePreset.AnchorType.blockCorner)
                                    {
                                        camToPos += zeroTile;
                                    }

                                    float lerpPos = 0f;
                                    float lerpRot = 0f;

                                    if(currentTo.overridePosGraph)
                                    {
                                        lerpPos = currentTo.lerpPositionGraphOverride.Evaluate(localProgress);
                                    }
                                    else
                                    {
                                        lerpPos = currentElement.lerpPositionGraph.Evaluate(localProgress);
                                    }

                                    if (currentTo.overrideRotGraph)
                                    {
                                        lerpRot = currentTo.lerpRotationGraphOverride.Evaluate(localProgress);
                                    }
                                    else
                                    {
                                        lerpRot = currentElement.lerpRotationGraph.Evaluate(localProgress);
                                    }

                                    Vector3 camPos = Vector3.Lerp(camFromPos, camToPos, lerpPos);
                                    Quaternion camRot = Quaternion.Slerp(Quaternion.Euler(currentFrom.camEuler), Quaternion.Euler(currentTo.camEuler), lerpRot);

                                    UpdateCam(camPos, camRot, newShot);

                                    //Trigger fade out
                                    if(preset.fadeOut && !triggeredFadeOut && currentElement == finalShot && currentTo == currentCamMovement[currentCamMovement.Count - 1])
                                    {
                                        if(currentShotTimer >= currentTo.atDuration - preset.fadeOutTime)
                                        {
                                            InterfaceController.Instance.Fade(1f, preset.fadeOutTime, true);
                                            triggeredFadeOut = true;
                                        }
                                    }
                                }
                                //Shot reaches next node
                                else
                                {
                                    //Move to next
                                    currentFrom = currentTo;

                                    int currentIndex = currentCamMovement.IndexOf(currentTo);
                                    int nextIndex = currentIndex + 1;
                                    Game.Log("Gameplay: Movement finished, searching for next at " + nextIndex + "/" + currentCamMovement.Count);

                                    if (nextIndex < currentCamMovement.Count)
                                    {
                                        Game.Log("Gameplay: Setting next movement at index " + nextIndex + "/" + currentCamMovement.Count);
                                        currentTo = currentCamMovement[nextIndex];
                                    }
                                    else
                                    {
                                        Game.Log("Gameplay: Shot has finished " + (currentIndex + 1) + "/" + currentCamMovement.Count);

                                        //This shot has finished movement
                                        currentTo = null;
                                        currentFrom = null;

                                        //Move the cursor
                                        cursor++;
                                    }
                                }
                            }
                        }
                    }

                    //Animate image
                    if(preset.displayImage != null)
                    {
                        if(!triggeredImage)
                        {
                            if(sceneTimer >= preset.imageFadeIn)
                            {
                                triggeredImage = true;
                            }
                        }
                        else
                        {
                            if(imageFadeIn < 1f)
                            {
                                imageFadeIn += Time.deltaTime / preset.imageFadeInSpeed;
                                imageFadeIn = Mathf.Clamp01(imageFadeIn);
                                imageFadeOut = imageFadeIn;
                                displayImageRend.SetAlpha(imageFadeIn);
                            }
                            else
                            {
                                if(sceneTimer >= preset.imageFadeOut)
                                {
                                    if(imageFadeOut > 0f)
                                    {
                                        imageFadeOut -= Time.deltaTime / preset.imageFadeOutSpeed;
                                        imageFadeOut = Mathf.Clamp01(imageFadeOut);
                                        displayImageRend.SetAlpha(imageFadeOut);
                                    }
                                }
                            }
                        }
                    }
                }
                //Scene has finished
                else
                {
                    SetActive(false);
                }
            }
        }
        else
        {
            this.enabled = false;
        }
    }

    private void UpdateCam(Vector3 position, Quaternion rotation, bool updateMixing)
    {
        Player.Instance.transform.position = position;
        CameraController.Instance.cam.transform.rotation = rotation;

        Player.Instance.UpdateMovementPhysics();
        if (updateMixing) AudioController.Instance.UpdateMixing();
    }

    private void SetActive(bool val)
    {
        if(cutSceneActive != val)
        {
            cutSceneActive = val;
            Game.Log("Gameplay: Set cut scene active: " + cutSceneActive);

            if(cutSceneActive)
            {
                //Turn off motion blur
                SessionData.Instance.motionBlur.active = false;

                //Save variables
                playerSavedPosition = Player.Instance.transform.position;
                camSavedLocalQuat = CameraController.Instance.cam.transform.localRotation;

                savedFreeCam = Game.Instance.freeCam;
                savedInaudible = Game.Instance.inaudiblePlayer;
                savedInvincible = Game.Instance.invinciblePlayer;
                savedInvisible = Game.Instance.invisiblePlayer;
                savedPhotoMode = Game.Instance.screenshotMode;
                MapController.Instance.directionalArrowContainer.SetActive(false);

                Game.Instance.SetFreeCamMode(true);
                Game.Instance.SetScreenshotMode(true, allowDialog: true);
                Game.Instance.inaudiblePlayer = true;
                Game.Instance.invisiblePlayer = true;
                Game.Instance.invinciblePlayer = true;

                Player.Instance.EnablePlayerMovement(false);
                Player.Instance.EnablePlayerMouseLook(false, true);

                displayImage.gameObject.SetActive(true);
                
                //Close UI
                InterfaceController.Instance.RemoveAllMouseInteractionComponents();
                this.enabled = true; //Enable update
            }
            else
            {
                //Load saved variables
                Game.Instance.SetFreeCamMode(savedFreeCam);
                Game.Instance.SetScreenshotMode(savedPhotoMode);

                Game.Instance.invisiblePlayer = savedInvisible;
                Game.Instance.inaudiblePlayer = savedInaudible;
                Game.Instance.invinciblePlayer = savedInvincible;

                Player.Instance.transform.position = playerSavedPosition;
                CameraController.Instance.cam.transform.localRotation = camSavedLocalQuat;
                Player.Instance.fps.InitialiseController(true, false);

                if (Game.Instance.enableDirectionalArrow)
                {
                    MapController.Instance.directionalArrowContainer.SetActive(MapController.Instance.displayDirectionArrow);
                }
                else
                {
                    MapController.Instance.directionalArrowContainer.SetActive(false);
                }

                if (!Player.Instance.transitionActive && Player.Instance.answeringPhone == null)
                {
                    Player.Instance.EnablePlayerMovement(true);
                    Player.Instance.EnablePlayerMouseLook(true);
                }

                //Reactivate motion blur
                PlayerPrefsController.GameSetting findSetting = PlayerPrefsController.Instance.gameSettingControls.Find(item => item.identifier.ToLower() == "motionblur"); //Case insensitive find
                SessionData.Instance.motionBlur.active = System.Convert.ToBoolean(findSetting.intValue);

                displayImage.gameObject.SetActive(false);

                //Do finish stuff
                if (preset.onEnd == CutScenePreset.OnEndScene.resumeGameplay)
                {
                    if (preset.fadeOut)
                    {
                        InterfaceController.Instance.Fade(0f, 2f, true);
                    }
                }
                if (preset.onEnd == CutScenePreset.OnEndScene.startGame)
                {
                    if (preset.fadeOut)
                    {
                        InterfaceController.Instance.Fade(0f, 2f, true);
                    }

                    //Fire game start event
                    CityConstructor.Instance.TriggerStartEvent();
                }
                else if (preset.onEnd == CutScenePreset.OnEndScene.endGame)
                {
                    RestartSafeController.Instance.loadFromDirty = false;

                    //Reset the scene to safely remove everything
                    AudioController.Instance.StopAllSounds();
                    UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
                }
            }
        }
    }

    [Button]
    public void PlayScene()
    {
        if(debugLoad != null)
        {
            PlayCutScene(debugLoad);
        }
    }

    [Button]
    public void StopScene()
    {
        if (cutSceneActive)
        {
            SetActive(false);

            //Remove dialog
            for (int i = 0; i < Player.Instance.speechController.speechQueue.Count; i++)
            {
                if(!Player.Instance.speechController.speechQueue[i].isObjective)
                {
                    Player.Instance.speechController.speechQueue.RemoveAt(i);
                    i--;
                }
            }
        }
    }
}
