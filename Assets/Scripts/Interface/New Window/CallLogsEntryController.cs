﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CallLogsEntryController : MonoBehaviour
{
    public RectTransform rect;
    public NewBuilding building;
    public TelephoneController.PhoneCall logged;
    public TextMeshProUGUI timeText;
    public TextMeshProUGUI durationText;
    public ButtonController fromButton;
    public ButtonController toButton;

    public void Setup(TelephoneController.PhoneCall newLogged, NewBuilding newBuilding)
    {
        logged = newLogged;
        building = newBuilding;

        fromButton.text.text = Strings.Get("evidence.generic", "From") + " > ";

        //Create data source...
        if (logged.fromNS != null && logged.fromNS.interactable != null)
        {
            //The call was made from this building, we know exactly where...
            if (logged.fromNS.interactable.node.building != null && logged.fromNS.interactable.node.building == building)
            {
                fromButton.text.text += logged.fromNS.interactable.node.gameLocation.name;
            }
            //The call was made from another building, we only know the building name...
            else if (logged.fromNS.interactable.node.building != null)
            {
                fromButton.text.text += logged.fromNS.interactable.node.building.name;
            }
            //The call was made from the street
            else if (logged.fromNS.interactable.node.gameLocation != null)
            {
                fromButton.text.text += logged.fromNS.interactable.node.gameLocation.name;
            }
        }
        else
        {
            Game.LogError("Unable to get telephone from " + logged.from);
            fromButton.text.text += Strings.Get("evidence.generic", "Unknown Location").ToUpper();
        }

        toButton.text.text = Strings.Get("evidence.generic", "To") + " > ";

        //Is this is incoming, be non-specific about where it came from
        if (logged.toNS != null && logged.toNS.interactable != null)
        {
            //The call was made to this building, we know exactly where...
            if (logged.toNS.interactable.node.building != null && logged.toNS.interactable.node.building == building)
            {
                toButton.text.text += logged.toNS.interactable.node.gameLocation.name;
            }
            //The call was made to another building, we only know the building name...
            else if (logged.toNS.interactable.node.building != null)
            {
                toButton.text.text += logged.toNS.interactable.node.building.name;
            }
            //The call was made from the street
            else if (logged.toNS.interactable.node.gameLocation != null)
            {
                //Use a custom link here...
                toButton.text.text += logged.toNS.interactable.node.gameLocation.name;
            }
        }
        else
        {
            Game.LogError("Unable to get telephone from " + logged.to);
            toButton.text.text += Strings.Get("evidence.generic", "Unknown Location").ToUpper();
        }

        try
        {
            EvidenceTime GetTimeEv = EvidenceCreator.Instance.GetTimeEvidence(logged.time, logged.time, "TelephoneCall", parentID: logged.fromNS.telephoneEntry.evID + ">" + logged.toNS.telephoneEntry.evID);

            Strings.LinkData timeLink = Strings.AddOrGetLink(GetTimeEv);
            timeText.text = "<link=" + timeLink.id.ToString() + ">" + SessionData.Instance.ShortDateString(logged.time, false) + " " + SessionData.Instance.GameTimeToClock24String(logged.time, false) + "</link>";

            //durationText.text = Strings.Get("evidence.generic", "Duration") + ": " + SessionData.Instance.DecimalToTimeLengthString(logged.duration);
            durationText.text = string.Empty; //Not working so leave blank for now
        }
        catch
        {
            timeText.text = string.Empty;
            durationText.text = string.Empty;
        }
    }

    public void FromButton()
    {
        try
        {
            if (logged.fromNS != null && logged.fromNS.interactable != null)
            {
                //The call was made from this building, we know exactly where...
                if (logged.fromNS.interactable.node.building != null && logged.fromNS.interactable.node.building == building)
                {
                    SessionData.Instance.PauseGame(true);
                    InterfaceController.Instance.SpawnWindow(logged.fromNS.interactable.node.gameLocation.evidenceEntry, Evidence.DataKey.name);
                }
                //The call was made from another building, we only know the building name...
                else if (logged.fromNS.interactable.node.building != null)
                {
                    SessionData.Instance.PauseGame(true);
                    InterfaceController.Instance.SpawnWindow(logged.fromNS.interactable.node.building.evidenceEntry, Evidence.DataKey.name);
                }
                //The call was made from the street
                else if (logged.fromNS.interactable.node.gameLocation != null)
                {
                    SessionData.Instance.PauseGame(true);
                    InterfaceController.Instance.SpawnWindow(logged.fromNS.interactable.node.gameLocation.evidenceEntry, Evidence.DataKey.name);
                }
            }
        }
        catch
        {
            Game.Log("Unable to open call 'from' location");
        }
    }

    public void ToButton()
    {
        //The call was made to this building, we know exactly where...
        try
        {
            if (logged.toNS.interactable.node.building != null && logged.toNS.interactable.node.building == building)
            {
                SessionData.Instance.PauseGame(true);
                InterfaceController.Instance.SpawnWindow(logged.toNS.interactable.node.gameLocation.evidenceEntry, Evidence.DataKey.name);
            }
            //The call was made to another building, we only know the building name...
            else if (logged.toNS.interactable.node.building != null)
            {
                SessionData.Instance.PauseGame(true);
                InterfaceController.Instance.SpawnWindow(logged.toNS.interactable.node.building.evidenceEntry, Evidence.DataKey.name);
            }
            //The call was made from the street
            else if (logged.toNS.interactable.node.gameLocation != null)
            {
                SessionData.Instance.PauseGame(true);
                InterfaceController.Instance.SpawnWindow(logged.toNS.interactable.node.gameLocation.evidenceEntry, Evidence.DataKey.name);
            }
        }
        catch
        {
            Game.Log("Unable to open call 'to' location");
        }
    }
}
