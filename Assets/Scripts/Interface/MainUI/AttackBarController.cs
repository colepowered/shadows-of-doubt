﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackBarController : MonoBehaviour
{
    public NewAIController ai;

    public RectTransform rect;
    public RectTransform barAnchor;
    public RectTransform attackProgress;
    public RectTransform blockPoint;
    public RectTransform perfectBlockPoint;

    public float barProgress = 0f; //A special version of the attack progress that counts until the hit (1)
    public bool displayOnScreen = true;
    public float distance = 0f;

    [Header("Graphic Elements")]
    public List<CanvasRenderer> allGraphics = new List<CanvasRenderer>();
    public List<CanvasRenderer> backgroundGraphics = new List<CanvasRenderer>();
    public List<CanvasRenderer> blockGraphics = new List<CanvasRenderer>();
    public List<CanvasRenderer> hitGraphics = new List<CanvasRenderer>();

    [Header("Removal")]
    public float removalProgress = 0f;
    public bool removeHit = false;
    public bool removeBlocked = false;
    public bool removeAbort = false;
    public float abortProgress = 0f;

    public void Setup(NewAIController newAi)
    {
        ai = newAi;

        foreach(CanvasRenderer r in allGraphics)
        {
            r.SetAlpha(0);
        }

        //Set thresholds
        blockPoint.sizeDelta = new Vector2(GameplayControls.Instance.successfulBlockThreshold * rect.sizeDelta.x, blockPoint.sizeDelta.y);
        perfectBlockPoint.sizeDelta = new Vector2(GameplayControls.Instance.perfectBlockThreshold * rect.sizeDelta.x, perfectBlockPoint.sizeDelta.y);
    }

    private void Update()
    {
        //Calculate bar progress towards damage valid
        barProgress = Mathf.Clamp01(ai.attackProgress / ai.currentWeaponPreset.attackTriggerPoint);

        //Always set the attack progress
        attackProgress.anchoredPosition = new Vector2(barProgress * rect.sizeDelta.x, 0);

        //Get angle from player...
        Vector3 forwardRelative = (ai.human.transform.position + new Vector3(0, 1.5f, 0)) - Player.Instance.transform.position;
        float angleBetween = Vector3.SignedAngle(forwardRelative, Player.Instance.transform.forward, Vector3.up);

        foreach (CanvasRenderer r in allGraphics)
        {
            r.SetAlpha(0);
        }

        //Display on screen position or in the anchor display...
        if (Mathf.Abs(angleBetween) < 75f)
        {
            //Screen position
            if (!displayOnScreen)
            {
                displayOnScreen = true;
            }

            Vector3 screenPoint = CameraController.Instance.cam.WorldToScreenPoint(ai.human.transform.position + new Vector3(0, 1.5f, 0)); //Get point above their head and convert it to a screen position
            Vector2 rectPos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(InterfaceController.Instance.firstPersonUI, screenPoint, null, out rectPos);

            //Set position
            rect.position = InterfaceControls.Instance.speechBubbleParent.TransformPoint(rectPos);

            //Set size
            //Update distance
            distance = Vector3.Distance(Player.Instance.transform.position, ai.human.transform.position);
            float distanceRel = 1f - Mathf.Clamp01(distance / AudioControls.Instance.speakEvent.hearingRange);

            foreach (CanvasRenderer r in allGraphics)
            {
                r.SetAlpha(distanceRel);
            }

            //Set scale
            float scale = Mathf.Lerp(InterfaceControls.Instance.speechMinMaxScale.x, InterfaceControls.Instance.speechMinMaxScale.y, distanceRel);
            rect.localScale = new Vector3(scale, scale, scale);
        }
        else
        {
            displayOnScreen = false;
        }

        //Removal safety event
        if(!ai.attackActive && !removeAbort && !removeBlocked && !removeHit)
        {
            removeAbort = true;
            abortProgress = ai.attackProgress;
        }

        //Removal animations
        if(removeAbort || removeBlocked || removeHit || Player.Instance.playerKOInProgress)
        {
            removalProgress += Time.deltaTime * 4f;

            //Fade out
            if(displayOnScreen && !Player.Instance.playerKOInProgress)
            {
                foreach (CanvasRenderer r in allGraphics)
                {
                    r.SetAlpha(1f - removalProgress);
                }
            }
            else
            {
                foreach (CanvasRenderer r in allGraphics)
                {
                    r.SetAlpha(0f);
                }
            }

            attackProgress.anchoredPosition = new Vector2(abortProgress * rect.sizeDelta.x, 0);

            //Scale icons
            if (removeBlocked)
            {
                float locScale = Mathf.Lerp(1f, 3f, removalProgress);
                blockPoint.localScale = new Vector3(locScale, locScale, locScale);

                if (displayOnScreen)
                {
                    foreach (CanvasRenderer r in blockGraphics)
                    {
                        r.SetAlpha(Mathf.Clamp01(2f - removalProgress * 2));
                    }
                }
            }
            else if (removeHit)
            {
                float locScale = Mathf.Lerp(1f, 3f, removalProgress);
                attackProgress.localScale = new Vector3(locScale, locScale, locScale);

                if (displayOnScreen)
                {
                    foreach (CanvasRenderer r in hitGraphics)
                    {
                        r.SetAlpha(Mathf.Clamp01(2f - removalProgress * 2));
                    }
                }

                attackProgress.anchoredPosition = new Vector2(1f * rect.sizeDelta.x, 0);
            }

            //Remove object
            if (removalProgress >= 1f)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
