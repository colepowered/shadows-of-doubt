using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TwitchRootObject
{
    public List<TwitchUserData> data;

    public TwitchRootObject()
    {
        this.data = new List<TwitchUserData>();
    }
}

[System.Serializable]
public class TwitchUserData
{
    public string user_name; // How the name is displayed
}

[System.Serializable]
public class TwitchAudienceData
{
    public string user_id;
    public string login;
    public string _links;
    public int chatter_count;
    public Chatters chatters;

    public TwitchRootObject followers;
    public TwitchRootObject chattersNew;
    public TwitchRootObject vipsNew;
    public TwitchRootObject moderatorsNew;
}

[System.Serializable]
public class Chatters
{
    public string[] broadcaster;
    public string[] vips;
    public string[] moderators;
    public string[] staff;
    public string[] admins;
    public string[] global_mods;
    public string[] viewers;
}

[System.Serializable]
public class CitizenReplacement
{
    public string name;
    public string firstName;
    public string surName;
}