using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class PasscodesEntryController : MonoBehaviour
{
    public NewAddress address;
    public NewRoom room;
    public Human human;
    public Interactable interactable;
    public Evidence evidence;
    public GameplayController.Passcode passcode;
    public TextMeshProUGUI text;
    public ButtonController locateOnMapButton;
    public ButtonController enterCodeButton;
    public RawImage evidenceImage;
    public Image icon;
    public string nameString;
    public string passcodeString;

    public void Setup(GameplayController.Passcode newPasscode)
    {
        passcode = newPasscode;

        locateOnMapButton.SetupReferences();
        enterCodeButton.SetupReferences();
        locateOnMapButton.VisualUpdate();
        enterCodeButton.VisualUpdate();

        //Parse passcode details
        if(passcode.type == GameplayController.PasscodeType.address)
        {
            if(CityData.Instance.addressDictionary.TryGetValue(passcode.id, out address))
            {
                evidence = address.evidenceEntry;
                icon.sprite = evidence.GetIcon();
            }
        }
        else if(passcode.type == GameplayController.PasscodeType.citizen)
        {
            if (CityData.Instance.GetHuman(passcode.id, out human))
            {
                evidence = human.evidenceEntry;
                icon.sprite = evidence.GetIcon();
            }

            locateOnMapButton.SetInteractable(false);
        }
        else if(passcode.type == GameplayController.PasscodeType.room)
        {
            if(CityData.Instance.roomDictionary.TryGetValue(passcode.id, out room))
            {
                evidence = room.gameLocation.evidenceEntry;
                icon.sprite = evidence.GetIcon();
            }
        }
        else if (passcode.type == GameplayController.PasscodeType.interactable)
        {
            interactable = CityData.Instance.interactableDirectory.Find(item => item.id == passcode.id);

            if (interactable != null)
            {
                evidence = interactable.evidence;
                if(evidence != null) icon.sprite = evidence.GetIcon();

                if (evidence == null)
                {
                    evidence = interactable.node.gameLocation.evidenceEntry;
                }
            }
        }

        if(evidence != null)
        {
            evidence.OnNewName += VisualUpdate;
            evidence.OnDataKeyChange += VisualUpdate;
        }

        VisualUpdate();

        //Check if 'enter code' should be active
        InterfaceController.Instance.OnNewActiveCodeInput += ActiveCodeInputCheck;
        ActiveCodeInputCheck(InterfaceController.Instance.activeCodeInput);
    }

    public void VisualUpdate()
    {
        if (evidenceImage != null && evidence != null) evidenceImage.texture = evidence.GetPhoto(evidence.GetTiedKeys(Evidence.DataKey.name));

        if (passcode.type == GameplayController.PasscodeType.room)
        {
            nameString = room.GetName() + ", " + evidence.GetNameForDataKey(Evidence.DataKey.name);
        }
        else if(passcode.type == GameplayController.PasscodeType.interactable)
        {
            nameString = interactable.GetName() + ", " + evidence.GetNameForDataKey(Evidence.DataKey.name);
        }
        else if(evidence != null)
        {
            nameString = evidence.GetNameForDataKey(Evidence.DataKey.name);
        }

        passcodeString = passcode.GetDigit(0).ToString() + passcode.GetDigit(1).ToString() + passcode.GetDigit(2).ToString() + passcode.GetDigit(3).ToString();

        text.text = nameString + ": " + passcodeString;
    }

    public void ActiveCodeInputCheck(KeypadController keypad)
    {
        //Check if 'enter code' should be active
        if (InterfaceController.Instance.activeCodeInput != null && !InterfaceController.Instance.activeCodeInput.isTelephone)
        {
            enterCodeButton.SetInteractable(true);
        }
        else enterCodeButton.SetInteractable(false);
    }

    private void OnDestroy()
    {
        InterfaceController.Instance.OnNewActiveCodeInput -= ActiveCodeInputCheck;

        if(evidence != null)
        {
            evidence.OnNewName -= VisualUpdate;
            evidence.OnDataKeyChange -= VisualUpdate;
        }
    }

    public void OpenEvidence()
    {
        if(evidence != null)
        {
            InterfaceController.Instance.SpawnWindow(evidence, Evidence.DataKey.name);
        }
    }

    public void LocateOnMap()
    {
        if(passcode.type == GameplayController.PasscodeType.address)
        {
            MapController.Instance.LocateEvidenceOnMap(evidence);
        }
        else if(passcode.type == GameplayController.PasscodeType.room)
        {
            MapController.Instance.LocateRoomOnMap(room);
        }
        else if(passcode.type == GameplayController.PasscodeType.interactable)
        {
            if (!InterfaceController.Instance.showDesktopMap)
            {
                InterfaceController.Instance.SetShowDesktopMap(true, true);
            }

            //Get a coordinate within this...
            MapController.Instance.SetFloorLayer((int)interactable.node.nodeCoord.z); //Set floor

            MapController.Instance.CentreOnNodeCoordinate(interactable.node.nodeCoord);
        }
    }

    public void EnterCode()
    {
        if (InterfaceController.Instance.activeCodeInput != null && !InterfaceController.Instance.activeCodeInput.isTelephone)
        {
            //Set input tab active
            InterfaceController.Instance.activeCodeInput.parentWindow.SetActiveContent(InterfaceController.Instance.activeCodeInput.windowContent);

            //Input code
            InterfaceController.Instance.activeCodeInput.OnInputCode(passcode.GetDigits());

            enterCodeButton.OnDeselect();
        }
    }
}
