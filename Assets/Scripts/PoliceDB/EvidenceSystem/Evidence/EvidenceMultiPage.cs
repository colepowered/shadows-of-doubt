﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;

public class EvidenceMultiPage : Evidence
{
    [System.Serializable]
    public class MultiPageContent
    {
        public int page = 0;

        public string evID;
        public int meta = 0;

        public string discEvID;
        public Evidence.Discovery disc;

        public string seperation = "\n\n";
        public string str;
        public int order = -1;

        public Evidence GetEvidence()
        {
            Evidence ret = null;

            if(evID != null && evID.Length > 0)
            {
                Toolbox.Instance.TryGetEvidence(evID, out ret);
            }

            return ret;
        }
    }

    public List<MultiPageContent> pageContent = new List<MultiPageContent>();
    public int page = 0;

    //Events
    public delegate void PageChanged(int newPage);
    public event PageChanged OnPageChanged;

    //Constructor
    public EvidenceMultiPage(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        GameplayController.Instance.multiPageEvidence.Add(this);
    }

    public int AddStringContentToNewPage(string newStr, string appendSeperation = "\n\n", int order = -1)
    {
        int nextPage = -1;

        //Return existing...
        nextPage = pageContent.FindIndex(item => item.page == page && newStr == item.str);

        if (nextPage > -1)
        {
            return nextPage;
        }

        nextPage = 0;

        foreach (MultiPageContent mpc in pageContent)
        {
            nextPage = Mathf.Max(nextPage, mpc.page + 1);
        }

        AddStringContentToPage(nextPage, newStr, appendSeperation, order);

        return nextPage;
    }

    //Add string content to page
    public void AddStringContentToPage(int page, string newStr, string appendSeperation = "\n\n", int order = -1)
    {
        if (pageContent == null) pageContent = new List<MultiPageContent>();
        //Check for existing
        else if (pageContent.Exists(item => item.page == page && newStr == item.str)) return;

        MultiPageContent newContent = new MultiPageContent();
        newContent.page = page;
        newContent.str = newStr;
        newContent.seperation = appendSeperation;
        newContent.order = order;

        pageContent.Add(newContent);
    }

    public int AddContainedMetaObjectToNewPage(MetaObject containedMetaObject)
    {
        int nextPage = -1;

        //Return existing...
        nextPage = pageContent.FindIndex(item => containedMetaObject.id == item.meta);

        if (nextPage > -1)
        {
            return nextPage;
        }

        nextPage = 0;

        foreach (MultiPageContent mpc in pageContent)
        {
            nextPage = Mathf.Max(nextPage, mpc.page + 1);
        }

        AddContainedMetaObjectToPage(nextPage, containedMetaObject);

        return nextPage;
    }

    //This meta object is saved with this evidence
    public void AddContainedMetaObjectToPage(int page, MetaObject containedMetaObject)
    {
        if (pageContent == null) pageContent = new List<MultiPageContent>();
        //Check for existing
        else if (pageContent.Exists(item => containedMetaObject.id == item.meta)) return;

        MultiPageContent newContent = new MultiPageContent();
        newContent.page = page;
        newContent.meta = containedMetaObject.id;

        pageContent.Add(newContent);
    }

    public int AddEvidenceToNewPage(Evidence evidenceToAdd)
    {
        int nextPage = -1;

        //Return existing...
        nextPage = pageContent.FindIndex(item => item.page == page && evidenceToAdd.evID == item.evID);

        if (nextPage > -1)
        {
            return nextPage;
        }

        nextPage = 0;

        foreach (MultiPageContent mpc in pageContent)
        {
            nextPage = Mathf.Max(nextPage, mpc.page + 1);
        }

        AddEvidenceToPage(nextPage, evidenceToAdd);

        return nextPage;
    }

    //Add evidence to a page
    public void AddEvidenceToPage(int page, Evidence evidenceToAdd)
    {
        if (pageContent == null) pageContent = new List<MultiPageContent>();
        else if (pageContent.Exists(item => item.page == page && evidenceToAdd.evID == item.evID)) return;

        MultiPageContent newContent = new MultiPageContent();
        newContent.page = page;
        newContent.evID = evidenceToAdd.evID;

        pageContent.Add(newContent);
    }

    public int AddEvidenceDiscoveryToNewPage(Evidence evidenceToApplyTo, Evidence.Discovery discovery)
    {
        int nextPage = -1;

        //Return existing...
        nextPage = pageContent.FindIndex(item => item.page == page && evidenceToApplyTo.evID == item.discEvID && item.disc == discovery);

        if (nextPage > -1)
        {
            return nextPage;
        }

        nextPage = 0;

        foreach (MultiPageContent mpc in pageContent)
        {
            nextPage = Mathf.Max(nextPage, mpc.page + 1);
        }

        AddEvidenceDiscoveryToPage(nextPage, evidenceToApplyTo, discovery);

        return nextPage;
    }

    //Add evidence discovery to a page
    public void AddEvidenceDiscoveryToPage(int page, Evidence evidenceToApplyTo, Evidence.Discovery discovery)
    {
        if (pageContent == null) pageContent = new List<MultiPageContent>();
        else if (pageContent.Exists(item => item.page == page && evidenceToApplyTo.evID == item.discEvID && item.disc == discovery)) return;

        MultiPageContent newContent = new MultiPageContent();
        newContent.page = page;
        newContent.discEvID = evidenceToApplyTo.evID;
        newContent.disc = discovery;

        pageContent.Add(newContent);
    }

    //Set page
    public void SetPage(int newPage, bool loopPages)
    {
        if(newPage != page)
        {
            if (pageContent.Count > 0)
            {
                if (pageContent.Exists(item => item.page == newPage))
                {
                    page = newPage;
                    //Game.Log(GetNameForDataKey(DataKey.name) + " set page: " + page);
                }
                else if(loopPages)
                {
                    int pageMin = 999999;
                    int pageMax = -999999;

                    foreach(MultiPageContent mpc in pageContent)
                    {
                        if (mpc.page < pageMin) pageMin = mpc.page;
                        if (mpc.page > pageMax) pageMax = mpc.page;
                    }

                    int tryPage = newPage;

                    while(true)
                    {
                        if(tryPage < pageMax)
                        {
                            tryPage++;
                        }
                        else
                        {
                            tryPage = pageMin;
                        }

                        if (pageContent.Exists(item => item.page == tryPage))
                        {
                            page = tryPage;
                            break;
                        }
                    }
                }
            }
            else page = 0;

            //Fire event
            if(OnPageChanged != null)
            {
                OnPageChanged(page);
            }
        }
    }

    public List<MultiPageContent> GetContentForPage(int newPage)
    {
        List<MultiPageContent> ret = pageContent.FindAll(item => item.page == newPage);
        ret.Sort((p1, p2) => p1.order.CompareTo(p2.order)); //Lowest first
        Game.Log("Returning " + ret.Count + " content for page " + newPage + "...");
        return ret;
    }

    public string GetCurrentPageStringContent()
    {
        string ret = string.Empty;

        List<MultiPageContent> content = pageContent.FindAll(item => item.page == page);

        //Sort list by order
        content.Sort((p1, p2) => p1.order.CompareTo(p2.order)); //Lowest first

        //Compose msg
        for (int i = 0; i < content.Count; i++)
        {
            MultiPageContent mpc = content[i];

            if(i > 0)
            {
                ret += mpc.seperation;
            }

            ret += mpc.str;
        }

        return ret.Trim();
    }
}
