using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CitizenProfileProgressController : MonoBehaviour
{
    public InfoWindow parentWindow;
    public TextMeshProUGUI progressText;

    private void OnEnable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange += CheckEnabled;
        parentWindow.OnWindowRefresh += CheckEnabled;

        CheckEnabled();
    }

    private void OnDisable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange -= CheckEnabled;
        parentWindow.OnWindowRefresh -= CheckEnabled;
    }

    public void CheckEnabled()
    {
        List<Evidence.DataKey> tiedKeys = parentWindow.passedEvidence.GetTiedKeys(parentWindow.evidenceKeys);

        int keyCount = 0;
        int profileKeys = parentWindow.passedEvidence.preset.GetValidProfileKeys().Count;

        foreach (Evidence.DataKey key in tiedKeys)
        {
            bool count = false;

            if (parentWindow.passedEvidence.preset.IsKeyValid(key, out count))
            {
                if(count) keyCount++;
            }
        }

        int percentProgress = Mathf.RoundToInt(((float)keyCount / (float)profileKeys) * 100f);

        progressText.text = Strings.Get("descriptors", "Profile") + "\n" + percentProgress.ToString() + "%";
    }
}
