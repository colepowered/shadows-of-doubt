﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

public class PrintController : MonoBehaviour
{
    public FingerprintScannerController.Print printData;
    public Material fingerprintMaterial;
    public Material instancedMaterial;
    public DecalProjector projector;
    public Color visibleColour = Color.white;
    public Color invisibleColour = Color.clear;
    public float scanProgress = 0f;
    public bool printConfirmed = false;

    public InteractableController printInteractable;

    public void Setup(FingerprintScannerController.Print newPrint)
    {
        instancedMaterial = projector.material = Instantiate(fingerprintMaterial);

        printData = newPrint;

        //Set direction
        this.transform.rotation = Quaternion.FromToRotation(Vector3.up, printData.normal);

        //Set rotation
        this.transform.localEulerAngles = new Vector3(this.transform.localEulerAngles.x, Toolbox.Instance.GetPsuedoRandomNumber(0f, 360f, printData.worldPos.ToString()), this.transform.localEulerAngles.z);

        //Setup already confirmed...
        Interactable confirmedInteractable = null;

        if(GameplayController.Instance.confirmedPrints.TryGetValue(printData.worldPos, out confirmedInteractable))
        {
            Game.Log("Player: Found existing fingerprint");
            scanProgress = 1f;
            printConfirmed = true;
            printInteractable = this.gameObject.AddComponent<InteractableController>();
            printInteractable.Setup(confirmedInteractable);
            InteractionController.Instance.OnPlayerLookAtChange();
        }
        else
        {
            Game.Log("Player: Existing fingerprint not found (" + printData.worldPos +")");
        }
    }

    //Set transparency of material to distance from ray
    private void Update()
    {
        float dist = Vector3.Distance(this.transform.position, FirstPersonItemController.Instance.scannerRayPoint);
        float distToEdge = Mathf.InverseLerp(0f, FirstPersonItemController.Instance.printDetectionRadius, dist);
        instancedMaterial.SetColor("_BaseColor", Color.Lerp(visibleColour, invisibleColour, distToEdge));
    }

    public void ResetScan()
    {
        if (printConfirmed) return; //Don't reset if we've created interactable
        scanProgress = 0f;
    }

    public void PrintConfirmed()
    {
        if(!printConfirmed)
        {
            printConfirmed = true;

            //I don't think this can ever happen, but don't create a print at exactly this position...
            if (GameplayController.Instance.confirmedPrints.ContainsKey(printData.worldPos)) return;

            //Create interactable
            printInteractable = this.gameObject.AddComponent<InteractableController>();

            Interactable newPrint = null;
            Human belongsTo = null;
            bool isDynamic = false;

            if (printData.dynamicOwner == null)
            {
                //Get possible owners...
                List<Human> ownerPool = Toolbox.Instance.GetFingerprintOwnerPool(printData.room, printData.furniture, printData.interactable, printData.source, printData.worldPos, true);

                //Pick owner...
                //Use random based on world position so it's predictable...
                belongsTo = ownerPool[Toolbox.Instance.GetPsuedoRandomNumber(0, ownerPool.Count, printData.worldPos.ToString())];
            }
            else
            {
                belongsTo = printData.dynamicOwner; //Print is dynamic and already has owner assigned...
                isDynamic = true;
            }

            Game.Log("Player: Discovered print belonging to " + belongsTo.name + " (Dynamic: " + isDynamic + ")");

            //Get prints letter loop
            if (belongsTo.fingerprintLoop <= -1)
            {
                belongsTo.fingerprintLoop = GameplayController.Instance.printsLetterLoop;
                GameplayController.Instance.printsLetterLoop++;
            }

            newPrint = InteractableCreator.Instance.CreateFingerprintInteractable(belongsTo, printData.worldPos, Vector3.zero, printData);

            printInteractable.Setup(newPrint);

            InteractionController.Instance.OnPlayerLookAtChange();

            //Story mode; display a prompt to inspect the fingerprint
            if (ChapterController.Instance != null && ChapterController.Instance.chapterScript != null)
            {
                ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                if (intro != null)
                {
                    if(intro.thisCase != null && intro.thisCase.currentActiveObjectives.Exists(item => item.queueElement.entryRef == "Scan for fingerprints"))
                    {
                        string msg = Strings.ComposeText(Strings.Get("ui.gamemessage", "fingerprint_prompt"), null, Strings.LinkSetting.forceNoLinks);

                        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, msg, InterfaceControls.Icon.printScanner);
                    }
                }
            }
        }
    }
}
