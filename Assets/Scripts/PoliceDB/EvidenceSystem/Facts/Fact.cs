﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using System;

//Connects multiple evidence together
//[System.Serializable]
public class Fact : CaseComponent
{
    //Preset containing data for this
    [System.NonSerialized]
    public FactPreset preset;

    //To/from entries
    public List<Evidence> fromEvidence = null;
    public List<Evidence> toEvidence = null;

    //Lists of from/to data keys
    public List<Evidence.DataKey> fromDataKeys = null;
    public List<Evidence.DataKey> toDataKeys = null;

    //'New Information'
    public bool isSeen = false;

    //This is a custom fact created by the player to tie evidence together
    public bool isCustom = false;
    public string customName;

    //Events
    //Triggered when connecting evidence changes data keys
    public delegate void ConnectingEvidenceChangeDataKey();
    public event ConnectingEvidenceChangeDataKey OnConnectingEvidenceChangeDataKey;

    //Triggered when the fact is 'seen' for the first time
    public delegate void IsSeen();
    public event IsSeen OnSeen;

    //Constructor
    public Fact(FactPreset newPreset, List<Evidence> newFromEvidence, List<Evidence> newToEvidence, List<object> newPassedObjects, List<Evidence.DataKey> overrideFromKeys, List<Evidence.DataKey> overrideToKeys, bool isCustomFact)
    {
        //Add to fact list
        GameplayController.Instance.factList.Add(this);

        //Set the preset
        preset = newPreset;

        //Set the to/from entries
        fromEvidence = newFromEvidence;
        toEvidence = newToEvidence;

        isCustom = isCustomFact;

        //Get the mystery keys- if null then apply to all keys
        if (overrideFromKeys == null)
        {
            if (preset.fromDataKeys.Count > 0)
            {
                fromDataKeys = new List<Evidence.DataKey>(preset.fromDataKeys);
            }
            else
            {
                fromDataKeys = Toolbox.Instance.allDataKeys;
            }
        }
        else fromDataKeys = overrideFromKeys;

        if (overrideToKeys == null)
        {
            if (preset.toDataKeys.Count > 0)
            {
                toDataKeys = new List<Evidence.DataKey>(preset.toDataKeys);
            }
            else
            {
                toDataKeys = Toolbox.Instance.allDataKeys;
            }
        }
        else toDataKeys = overrideToKeys;

        //Set the sprite reference for icons
        SetNewIcon(preset.iconSpriteLarge);

        //Connect fact
        ConnectFact();

        //Discover on create
        if (preset.discoverOnCreate || Game.Instance.discoverAllEvidence) SetFound(true);

        //Listen for connecting evidence data key changes
        foreach(Evidence ev in fromEvidence)
        {
            if (ev == null) continue;
            ev.OnDataKeyChange += OnConnectionsChangedDataKeys;
        }

        foreach (Evidence ev in toEvidence)
        {
            if (ev == null) continue;
            ev.OnDataKeyChange += OnConnectionsChangedDataKeys;
        }

        //Set icon- the preset will override the 'other' evidence icon
        if (preset.iconSpriteLarge != null) SetNewIcon(preset.iconSpriteLarge);
    }

    public override string GetIdentifier()
    {
        string f_ = string.Empty;

        foreach(Evidence ev in fromEvidence)
        {
            f_ += ev.GetIdentifier();
        }

        string t_ = string.Empty;

        foreach (Evidence ev in toEvidence)
        {
            t_ += ev.GetIdentifier();
        }

        return preset.name + "-" + f_ + "-" + t_;
    }

    //Generate real/object name
    public override string GenerateName()
    {
        string ret = string.Empty;

        //Get from names
        for (int i = 0; i < fromEvidence.Count; i++)
        {
            Evidence fromEv = fromEvidence[i];

            if (i > 0) ret += " & ";
            ret += fromEv.GetNameForDataKey(Evidence.DataKey.name);
        }

        ret += " " + Strings.Get("evidence.generic", preset.name) + " ";

        //Get to names
        for (int i = 0; i < toEvidence.Count; i++)
        {
            Evidence toEv = toEvidence[i];

            if (i > 0) ret += " & ";
            ret += toEv.GetNameForDataKey(Evidence.DataKey.name);
        }

        ret = ret.Trim();

        return ret;
    }

    //Connect the fact
    public virtual void ConnectFact()
    {
        //Connect from evidence
        foreach(Evidence ev in fromEvidence)
        {
            //Game.Log(ev);

            if (ev == null) continue;

            //Loop data keys to add to evidence
            ev.AddFactLink(this, fromDataKeys, true);

            //If this fact is alread discovered, trigger OnConnectedFactDiscovery, if not, listen for discovery
            if (ev.isFound)
            {
                OnConnectedEvidenceDiscovery(ev);
            }
            else
            {
                //Listen for discovery
                ev.OnDiscoveredThis += OnConnectedEvidenceDiscovery;
            }
        }

        //Connect to evidence
        foreach (Evidence ev in toEvidence)
        {
            if (ev == null) continue;

            //Loop data keys to add to evidence
            ev.AddFactLink(this, toDataKeys, false);

            //If this fact is alread discovered, trigger OnConnectedFactDiscovery, if not, listen for discovery
            if (ev.isFound)
            {
                OnConnectedEvidenceDiscovery(ev);
            }
            else
            {
                //Listen for discovery
                ev.OnDiscoveredThis += OnConnectedEvidenceDiscovery;
            }
        }
    }

    //On discovery
    public override void OnDiscovery()
    {
        //Game.Log("Discover Fact: " + preset.name);

        //Set 'is seen'
        if (!preset.countsAsNewInformationOnDiscovery) SetSeen();

        //Loop from keys
        foreach (Evidence.DataKey key2 in fromDataKeys)
        {
            foreach (Evidence ev in fromEvidence)
            {
                foreach (Evidence.DataKey key in preset.applyFromKeysOnDiscovery)
                {
                    ev.MergeDataKeys(key, key2);
                }
            }
        }

        //Loop from keys
        foreach (Evidence.DataKey key2 in toDataKeys)
        {
            foreach (Evidence ev in toEvidence)
            {
                foreach (Evidence.DataKey key in preset.applyToKeysOnDiscovery)
                {
                    ev.MergeDataKeys(key, key2);
                }
            }
        }

        //Add money for address discovery
        int moneyForAddress = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.moneyForAddresses));

        if(moneyForAddress > 0 && preset.name == "LivesAt")
        {
            GameplayController.Instance.AddMoney(moneyForAddress, false, "moneyforaddresses");
        }
    }

    //Set 'seen'
    public void SetSeen()
    {
        if(!isSeen)
        {
            isSeen = true;

            //Fire event
            if (OnSeen != null) OnSeen();
        }
    }

    //Triggered when connected evidence is discovered
    public virtual void OnConnectedEvidenceDiscovery(CaseComponent discovered)
    {
        //Check connected evidence for discovery of this
        if(!isFound)
        {
            Evidence discEv = discovered as Evidence;
            if (discEv != null) discEv.UpdateDiscoveries();
        }
    }

    //Get name from the connecting evidence. Optional: Get name for a specific link
    public virtual string GetName(Evidence.FactLink specificLink = null)
    {
        string ret = string.Empty;

        //Get from names
        if(specificLink == null || !specificLink.thisIsTheFromEvidence)
        {
            for (int i = 0; i < fromEvidence.Count; i++)
            {
                Evidence fromEv = fromEvidence[i];

                if(fromEv == null)
                {
                    Game.LogError("Null from evidence in fact " + preset.name);
                    continue;
                }

                if (i > 0) ret += " & ";
                ret += fromEv.GetNameForDataKey(fromDataKeys);

                //Game.Log(preset.name + " name from keys = " + fromEv.GetNameForDataKey(fromDataKeys));

                //foreach (Evidence.DataKey k in fromDataKeys)
                //{
                //    Game.Log(k.ToString());
                //}

                //List<Evidence.DataKey> tk = fromEv.GetTiedKeys(Evidence.DataKey.name);

                //foreach (Evidence.DataKey k in tk)
                //{
                //    Game.Log("name=" + k.ToString());
                //}
            }
        }
        else
        {
            ret += specificLink.thisEvidence.GetNameForDataKey(fromDataKeys);
        }

        ret += " " + Strings.Get("evidence.generic", preset.name) + " ";

        //Get to names
        if(specificLink == null || specificLink.thisIsTheFromEvidence)
        {
            for (int i = 0; i < toEvidence.Count; i++)
            {
                Evidence toEv = toEvidence[i];

                if (toEv == null)
                {
                    Game.LogError("Null to evidence in fact " + preset.name);
                    continue;
                }

                if (i > 0) ret += " & ";
                ret += toEv.GetNameForDataKey(toDataKeys);
            }
        }
        else
        {
            ret += specificLink.thisEvidence.GetNameForDataKey(fromDataKeys);
        }

        ret += FoundAtName() + GenerateNameSuffix();

        //Trim extra whitespace
        ret = ret.Trim();

        return ret;
    }

    //Get other evidence (singular)
    public Evidence GetOther(Evidence ev)
    {
        if(fromEvidence.Count <= 0)
        {
            Game.LogError("Evidence for fact " + name + " features no 'from' evidence!");
            return null;
        }

        if (toEvidence.Count <= 0)
        {
            Game.LogError("Evidence for fact " + name + " features no 'to' evidence!");
            return null;
        }

        if (fromEvidence.Contains(ev))
        {
            return toEvidence[0];
        }
        else if(toEvidence.Contains(ev))
        {
            return fromEvidence[0];
        }

        return null;
    }

    //Get other evidence (list)
    public List<Evidence> GetOther(List<Evidence> ev)
    {
        foreach(Evidence e in ev)
        {
            if (fromEvidence.Contains(e))
            {
                return toEvidence;
            }
            else if (toEvidence.Contains(e))
            {
                return fromEvidence;
            }
        }

        return null;
    }

    //Triggered when connecting evidence merges/adds data keys
    public void OnConnectionsChangedDataKeys()
    {
        if (OnConnectingEvidenceChangeDataKey != null) OnConnectingEvidenceChangeDataKey();
    }

    //Get serialized save string for this: This can act as an ID and also contain all the information needed to create this.
    public string GetSerializedString()
    {
        string ret = string.Empty;

        //I'm not sure what's returning errors in the below, but wrap this in a try to cover all bases as it messes up save games...
        try
        {
            //1st: Preset
            if(preset != null) ret += preset.name + "|";

            //2nd: From evidence
            if(fromEvidence != null)
            {
                foreach(Evidence ev in fromEvidence)
                {
                    if (ev == null) continue;
                    ret += ev.evID + ",";
                }
            }

            ret += "|";

            //3rd: To evidence
            if(toEvidence != null)
            {
                foreach (Evidence ev in toEvidence)
                {
                    if (ev == null) continue;
                    ret += ev.evID + ",";
                }
            }

            ret += "|";

            //4th: From data keys
            if(fromDataKeys != null)
            {
                foreach (Evidence.DataKey k in fromDataKeys)
                {
                    ret += (int)k + ",";
                }
            }

            ret += "|";

            //5th: To data keys
            if(toDataKeys != null)
            {
                foreach (Evidence.DataKey k in toDataKeys)
                {
                    ret += (int)k + ",";
                }
            }

            //6th: discovered, 7th: is seen, 8th: is custom, 9th: Custom name
            ret += "|" + Convert.ToInt32(isFound) + "|" + Convert.ToInt32(isSeen) + "|" + Convert.ToInt32(isCustom) + "|" + customName;

            return ret;
        }
        catch
        {
            return ret;
        }
    }

    //Set custom name
    public virtual void SetCustomName(string str)
    {
        customName = str;
    }
}
