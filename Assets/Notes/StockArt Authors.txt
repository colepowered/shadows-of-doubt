Baseball landscape: Tim Eiden: https://www.pexels.com/photo/man-holding-baseball-bat-1374370/
Baseball square: Anastasia Zhenina: https://www.pexels.com/photo/sport-ball-baseball-play-46859/
Baseball portrait: Mandie Inman: https://www.pexels.com/photo/person-holding-baseball-bat-896567/
Mountain portrait: Eberhard Grossgas: https://www.pexels.com/photo/white-and-black-mountain-range-2086621/
Sunset portrait: Daniel Frese: https://www.pexels.com/photo/silhouette-photography-of-coconut-trees-2983489/
Great wall portrait: Paulo Marcelo Ma...: https://www.pexels.com/photo/great-wall-of-china-2412603/
Cherry Blossom portrait: John-Mark Smith: https://www.pexels.com/photo/low-angle-view-of-pink-flowers-against-blue-sky-250591/
Stoneheng landscape: Berd Feurich: https://www.pexels.com/photo/photo-of-people-standing-near-stonehenge-2497294/
https://ccsearch.creativecommons.org/photos/3232312f-9df8-47ec-8f7f-ca189cac8d18
https://ccsearch.creativecommons.org/photos/fa3af86d-2c5f-4472-949a-cb605fd2c2c2
https://ccsearch.creativecommons.org/photos/f831afbf-f8bd-4d40-a530-1a8124905e47
https://ccsearch.creativecommons.org/photos/a44e247c-f8b6-4dc5-afb6-16802f05342d
https://ccsearch.creativecommons.org/photos/7a20b316-6d24-4b41-9bc2-f85af0869da5
https://ccsearch.creativecommons.org/photos/d981ec0c-a3b5-4fe1-af83-98f06c26024a
https://ccsearch.creativecommons.org/photos/3f16caed-3cdc-48f6-b971-f3d6ffe3d3a9
Skyscraper portrait: Jonas Ferlin: https://www.pexels.com/photo/mirror-facade-of-tall-building-1963557/
Tunnel portrait: Anthony Macajone: https://www.pexels.com/photo/gray-concrete-building-1874636/
Dancers portrait: Pixabay: https://www.pexels.com/photo/active-dance-dancer-dancing-270789/
Dancers landscape: Marko Zirdum: https://www.pexels.com/photo/woman-and-man-dancing-under-light-2188012/
Books portrait: Pixabay: https://www.pexels.com/photo/books-stack-old-antique-33283/
Chess Landscape: Pixabay: https://www.pexels.com/photo/brown-and-black-wooden-chess-piece-163427/
Chess square: Claire Thubault: https://www.pexels.com/photo/playing-rock-game-chess-57018/
Coffee landscape: Pixabay: https://www.pexels.com/photo/aroma-aromatic-art-artistic-434213/