﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;
using NaughtyAttributes;
using System;
//using Mono.Cecil;

//Controller for the game's interface.
//Script pass 1
public class InterfaceController : MonoBehaviour
{
    [Header("Canvases")]
    public Canvas caseCanvas;
    public CanvasGroup caseCanvasGroup;
    public GraphicRaycaster caseCanvasRaycaster;
    public Canvas minimapCanvas;
    public CanvasGroup minimapCanvasGroup;
    public Canvas controlsCanvas;
    public CanvasGroup controlsCanvasGroup;
    public Canvas controlPanelCanvas;
    public CanvasGroup controlPanelCanvasGroup;
    public Canvas gameWorldCanvas;
    public CanvasGroup gameWorldCanvasGroup;
    public Canvas windowCanvas;
    public GraphicRaycaster windowRaycaster;
    public CanvasGroup windowCanvasGroup;
    public Canvas statusCanvas;
    public CanvasGroup statusCanvasGroup;
    public Canvas upgradesCanvas;
    public CanvasGroup upgradesCanvasGroup;
    public Canvas dialogCanvas;
    public CanvasGroup dialogCanvasGroup;
    public Canvas interactionProgressCanvas;
    public CanvasGroup interactionProgressCanvasGroup;

    [Header("UI Scaling Transforms")]
    [ReorderableList]
    public List<RectTransform> uiScaling = new List<RectTransform>();

    [Header("References")]
    public ButtonController notebookButton;
    public ButtonController upgradesButton;
    public ButtonController mapButton;
    public ButtonController personButton;
    public RectTransform firstPersonUI;
    public RectTransform caseReferenceAnchor;
    public GameObject backgroundBlur;
    public RectTransform speechDisplayAnchor;
    public RectTransform objectiveSideAnchor;
    public RectTransform objectiveTextBackground;
    public TextMeshProUGUI objectiveTitleText;
    public CanvasRenderer objectiveTitleTextRenderer;
    public CanvasRenderer objectiveBackgroundRenderer;
    public RectTransform uiPointerContainer;
    public Image takeDamageIndicatorImg;
    public JuiceController takeDamageIndicatorJuice;
    public Image lowHealthIndicatorImg;
    public RectTransform movieBarTop;
    public RectTransform movieBarBottom;
    public JuiceController movieBarJuice;
    public TextMeshProUGUI timeText;
    public RectTransform speechAnchor;
    public RectTransform objectCycleAnchor;
    public TextMeshProUGUI timerText;
    public ControllerViewRectScroll caseScrollingViewRect;

    [Space(7)]
    public SoundIndicatorController footstepAudioIndicator;

    [Header("States")]
    public bool desktopMode = false;
    public float desktopModeTransition = 0f;
    public float desktopModeDesiredTransition = 0f;
    public bool showDesktopMap = false;
    public bool showDesktopCaseBoard = false;
    public ButtonController selectedElement = null;
    public string selectedElementTag;
    public List<MonoBehaviour> currentMouseOverElement = new List<MonoBehaviour>();
    private InfoWindow detectiveNotebook;
    public bool crosshairVisible = true;
    public bool playerTextInputActive = false; //True while the player is inputting something
    public List<SpeechBubbleController> activeSpeechBubbles = new List<SpeechBubbleController>();

    //Enabled
    public bool interfaceIsActive = true;

    public static int assignStickyNoteID = 1;

    [Header("Location Text")]
    public TextMeshProUGUI locationText;
    private Coroutine displayedTextCoroutine;
    public bool locationTextDisplayed = false;

    [Header("In-Game Title Text")]
    public TextMeshProUGUI titleText;
    public CanvasRenderer titleTextRenderer;

    [Header("Game Message System")]
    public RectTransform gameMessageParent;
    public bool messageCoroutineRunning = false;
    public enum GameMessageType { notification, gameHeader, keyMerge, helpPointer, socialCredit }; //Notification appears top left, head bottom right

    public class GameMessage
    {
        public GameMessageType messageType;
        public int numerical;
        public string message;
        public Sprite graphic;
        public AudioEvent additionalSFX;
        public bool colourOverride;
        public Color col;
        public int mergeType;
        public float delay = 0f;
        public RectTransform moveOnDestroy;
        public GameMessageController.PingOnComplete ping;
        public bool keyMerge = false;
        public bool socCredit = false;
        public Evidence keyMergeEvidence;
        public List<Evidence.DataKey> mergedKeys;
    }

    public List<GameMessage> notificationQueue = new List<GameMessage>();
    public List<GameMessage> gameHeaderQueue = new List<GameMessage>();
    public List<GameMessage> helpPointerQueue = new List<GameMessage>();

    public GameObject currentNotification;
    //private float notificationDelay = 0f;
    public GameMessage currentGameHeader;
    private float gameHeaderDelay = 0f;
    public float gameHeaderTimer = 0f; //For keeping track of the display time
    private float typewriterDelay = 0f;
    private float gameHeaderFadeDelay = 0f;
    public bool gameHeaderDisplayed = false; //True once the typewriter reveal has finished
    public bool gameSceenDisplayed = false;
    public bool gameScreenQueued = false;
    public bool levelUpScreenActive = false;
    public ScreenDisplayType currentGameScreen = ScreenDisplayType.displayResolve;

    [Space(7)]
    public RectTransform notebookNotificationIcon;
    public JuiceController notebookNotificationJuice;
    public RectTransform syncDiskNotificationIcon;
    public JuiceController syncDiskNotificationJuice;
    public RectTransform lockpicksNotificationIcon;
    public TextMeshProUGUI lockpicksNotificationText;
    public JuiceController lockpicksNotificationJuice;
    public List<CanvasRenderer> lockpicksNotificationRenderers;
    public bool lockpickNotificationActive = false;
    public int lastLockpicks = 0;
    public RectTransform moneyNotificationIcon;
    public TextMeshProUGUI moneyNotificationText;
    public JuiceController moneyNotificationJuice;
    public List<CanvasRenderer> moneyNotificationRenderers;
    public bool moneyNotificationActive = false;
    public int lastMoney = 0;

    [Space(7)]
    private GameMessage currentHelpPointer;
    public RectTransform helpPointerRect;
    public List<CanvasRenderer> helpPointerRenderers = new List<CanvasRenderer>();
    public TextMeshProUGUI helpPointerText;
    private string helpPointerTextDisplay;
    private float helpPointerProgress = 1f;
    private float helpPointerFadeOut = 1f;
    private float helpPointerTimer = 0f;
    private float helpPointerDesiredHeight = 44f;

    [Header("Objective System")]
    [NonSerialized]
    public Objective currentlyDisplaying = null;
    public List<Objective> displayedObjectives = new List<Objective>();
    public List<ChecklistButtonController> objectiveList = new List<ChecklistButtonController>();

    [Header("Radial Selection")]
    public RectTransform selectionArrowPivot;
    public List<CanvasRenderer> radialRenderers = new List<CanvasRenderer>();
    public AnimationCurve radialActivateScale;
    public AnimationCurve radialDeactivateScale;
    public AnimationCurve radialActivateRotate;
    public AnimationCurve radialDeactivateRotate;

    [Header("Dragging")]
    public GameObject dragged = null;
	public string draggedTag = string.Empty;
    public Vector2 dragCursorOffset;
    public PinnedItemController pinnedBeingDragged;
    public float windowFadeProgress = 0f;
    public bool windowFullFade = false;

    [Header("Display Objectives")]
    public float objectivesDisplayTimer = 0f;
    public float objectivesAlpha = 0f;

    [Header("Box Select")]
    public bool boxSelectActive = false;
    public RectTransform boxSelect;
    public List<PinnedItemController> selectedPinned = new List<PinnedItemController>();

    [Header("Active Windows")]
    //Dictionary containing references to all window files, accessed by lower case name
    public Dictionary<string, WindowStylePreset> windowDictionary = new Dictionary<string, WindowStylePreset>();

    //List containing references to all active windows by window script.
    public List<InfoWindow> activeWindows = new List<InfoWindow>();

    //List of evidence buttons currently on-screen
    public string openHelpToPage = ""; //Used to open the detectives handbook to a specific page

    public RectTransform windowFocus;

    public KeypadController activeCodeInput; //If there is a current window with a code input active...

    [Header("Game Fading")]
    public CanvasRenderer fadeOverlay;
    public AnimationCurve fadeOverlayAlphaCurve;
    public float desiredFade = 0f;
    private float fadeTime = 2f;
    private bool fadeAudio = false;
    public float fade = 0f;

    [Header("Pause Rendering")]
    //private int savedCameraCulling = 0;
    private CameraClearFlags savedCameraClear;

    [Header("Awareness Compass")]
    public GameObject compassContainer;
    public Transform backgroundTransform;
    public MeshRenderer compassMeshRend;
    public float compassDesiredAlpha = 0f;
    public float compassActualAlpha = 0f;
    public List<AwarenessIcon> awarenessIcons = new List<AwarenessIcon>();
    public List<SpeechBubbleController> anchoredSpeech = new List<SpeechBubbleController>();

    [Header("First Person Item")]
    public GameObject firstPersonModel;
    public Animator firstPersonAnimator;

    public enum AwarenessType { actor, transform, position};
    public enum AwarenessBehaviour { alwaysVisible, invisibleInfront};

    [System.Serializable]
    public class AwarenessIcon
    {
        public AwarenessType awarenessType;
        public AwarenessBehaviour awarenessBehaviour;

        public Actor actor;
        public Transform targetTransform;
        public Vector3 targetPosition;

        public GameObject spawned;
        public Transform imageTransform;
        public Material imageMaterial; //Instance of material so we can fade it
        public Transform arrowTransform;
        public Material arrowMaterial; //Instance of material so we can fade it
        public Texture overrideTexture;

        public float fadeIn = 0f; //Used to fade in alpha as well as move the direction arrow
        public float springAction = 0f; //Used for moving the arrow slowly back again, creating an animation
        public float removalProgress = 0f; //Used for fade removal
        public bool removalFlag = false; //If true fade until removal

        public float alpha = 0f; //This value is if below was 1 (* together to get actual)
        public float displayAlpha = 0f; //This represents how much to display this. Can only be increased. If behind the player it's 1, if infront it's 0.

        public float maxDistance = 20f; //Invisible beyond this distance
        public bool setup = false; //Because serializable

        public int priority = 0; //The highest priority icon for a actor/transform will be shown and the others invisible.

        //Used for AI alert
        public bool triggerAlert = false;
        public float alertProgress = 0f;

        //Remove this from the awareness hud
        public void Remove(bool instant = false)
        {
            //Decrease priority if removing
            priority = -99;

            if(instant)
            {
                Destroy(spawned);
                InterfaceController.Instance.awarenessIcons.Remove(this);
            }

            removalFlag = true;

            //Sort awarenesss icons by priority...
            InterfaceController.Instance.awarenessIcons.Sort((p1, p2) => p2.priority.CompareTo(p1.priority)); //Using P2 first gives highest first
        }

        public void SetAlpha(float val)
        {
            float removal = 1f - removalProgress;

            //The alpha cannot exceed the display alpha
            Color rgb = imageMaterial.GetColor("_UnlitColor");
            imageMaterial.SetColor("_UnlitColor", new Color(rgb.r, rgb.g, rgb.b, val * displayAlpha * removal));
            arrowMaterial.SetColor("_UnlitColor", new Color(rgb.r, rgb.g, rgb.b, val * displayAlpha * removal));
            alpha = val;
        }

        public void SetTexture(Texture tex)
        {
            overrideTexture = tex;
            if(imageMaterial != null) imageMaterial.SetTexture("_UnlitColorMap", tex);
        }

        public float GetActualAlpha()
        {
            float removal = 1f - removalProgress;
            return alpha * displayAlpha * removal;
        }

        //Trigger an alert animation
        public void TriggerAlert()
        {
            triggerAlert = true;
        }
    }

    [Header("Depth of Field")]
    public float desiredDofNearStart = 0;
    public float desiredDofNearEnd = 0;
    public float desiredDofFarStart = 0;
    public float desiredDofFarEnd = 0;

    public float dofProgress = 0f;

    [Header("Popup Messages")]
    public PopupMessageController popupController;

    public enum ScreenDisplayType { missionComplete, missionFailed, newMurderCase, socialCreditLevelUp, unsolved, displayResolve, apartmentPurchase};

    [Header("Debug")]
    public int debugLevel;

    //Events
    //public delegate void PanelToggle();
    //public event PanelToggle OnPanelChange;

    public delegate void InputCode(List<int> code);
    public event InputCode OnInputCode;

    public delegate void NewActiveCodeInput(KeypadController keypad);
    public event NewActiveCodeInput OnNewActiveCodeInput;

    //Singleton pattern
    private static InterfaceController _instance;
    public static InterfaceController Instance { get { return _instance; } }

	void Awake()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
	}

	// Use this for initialization
	void Start ()
	{
        if (SessionData.Instance != null && !SessionData.Instance.isDialogEdit)
        {
            UpdateCursorSprite();
        }
        else if(SessionData.Instance.isDialogEdit)
        {
            Game.Log("Disabling interface controller as this is dialog editor...");
            this.enabled = false;
        }

        if (SessionData.Instance.isDialogEdit) return;

        if (!SessionData.Instance.isFloorEdit)
        {
            //Game.Log("Initialising pop-up controller...");
            popupController.Setup();
        }

        //By default, enable the case board on desktop mode
        SetShowDesktopCaseBoard(true);

        //Disable intital desktop canvases
        //watchCanvas.gameObject.SetActive(false);
        if(controlPanelCanvas != null) controlPanelCanvas.gameObject.SetActive(false);

        if(timerText != null)
        {
            timerText.gameObject.SetActive(Game.Instance.timeLimited);
            timerText.text = string.Empty;
        }

        //Set compass alpha
        if (!SessionData.Instance.isFloorEdit)
        {
            compassActualAlpha = compassDesiredAlpha;
            Color rgb = compassMeshRend.sharedMaterial.GetColor("_UnlitColor");
            compassMeshRend.sharedMaterial.SetColor("_UnlitColor", new Color(rgb.r, rgb.g, rgb.b, compassActualAlpha));
        }

        if(titleText != null) titleText.text = string.Empty;
    }

    void Update()
    {
        try
        {
            //Only do the following if no popup is present
            if(PopupMessageController.Instance != null && !PopupMessageController.Instance.active)
            {
                //Not allowed in menu
                if(!MainMenuController.Instance.mainMenuActive)
                {
                    //Display anchored speech
                    UpdateAnchoredSpeechPositions();

                    //Handle objectives display; display objectives onscreen for a short amount of time before fading
                    bool objectivesAlphaChange = false;

                    if (objectivesDisplayTimer > 0f)
                    {
                        if(!desktopMode) objectivesDisplayTimer -= Time.deltaTime; //Only fade out in-game

                        if (objectivesAlpha < 1f)
                        {
                            objectivesAlpha += Time.deltaTime / 0.1f;
                            objectivesAlpha = Mathf.Clamp01(objectivesAlpha);
                            objectivesAlphaChange = true;
                        }
                    }
                    else if (objectivesDisplayTimer <= 0f && objectivesAlpha > 0f)
                    {
                        objectivesDisplayTimer = Mathf.Max(objectivesDisplayTimer, 0);
                        objectivesAlpha -= Time.deltaTime;
                        objectivesAlpha = Mathf.Clamp01(objectivesAlpha);
                        objectivesAlphaChange = true;
                    }

                    //Update current objectives
                    float yPos = 0;

                    //Display only objectives for the active case...
                    if (CasePanelController.Instance.activeCase != null)
                    {
                        for (int u = 0; u < CasePanelController.Instance.activeCase.inactiveCurrentObjectives.Count; u++)
                        {
                            Objective obj = CasePanelController.Instance.activeCase.inactiveCurrentObjectives[u];

                            //Activate inactive...
                            if (!displayedObjectives.Contains(obj) && !obj.isCancelled && !obj.isComplete)
                            {
                                InterfaceController.Instance.ActivateObjectivesDisplay();
                                obj.Activate(true);
                                u--;
                            }
                        }

                        for (int u = 0; u < CasePanelController.Instance.activeCase.currentActiveObjectives.Count; u++)
                        {
                            Objective obj = CasePanelController.Instance.activeCase.currentActiveObjectives[u];

                            //Activate inactive...
                            if (!displayedObjectives.Contains(obj) && !obj.isCancelled && !obj.isComplete)
                            {
                                obj.Activate(true);
                            }

                            if (!obj.clearedForAnimation && !messageCoroutineRunning && currentNotification == null && !gameSceenDisplayed && !gameScreenQueued)
                            {
                                obj.clearedForAnimation = true;
                            }

                            if (obj.clearedForAnimation)
                            {
                                obj.CheckingLoop();
                            }

                            if (obj.objectiveList != null)
                            {
                                obj.objectiveList.desiredAnchoredPosition = new Vector2(0, yPos);
                                yPos -= obj.objectiveList.rect.sizeDelta.y + 4;

                                if (obj.objectiveList.rect.anchoredPosition != obj.objectiveList.desiredAnchoredPosition)
                                {
                                    obj.objectiveList.rect.anchoredPosition = Vector2.Lerp(obj.objectiveList.rect.anchoredPosition, obj.objectiveList.desiredAnchoredPosition, Time.deltaTime * 12f);
                                }
                            }
                        }
                    }

                    //Hide objectives that shouldn't be displayed
                    for (int u = 0; u < displayedObjectives.Count; u++)
                    {
                        Objective obj = displayedObjectives[u];

                        if (CasePanelController.Instance.activeCase != obj.thisCase)
                        {
                            obj.Remove();
                            u--;
                        }
                    }

                    //Fade out objective list
                    for (int u = 0; u < objectiveList.Count; u++)
                    {
                        ChecklistButtonController cbc = objectiveList[u];

                        if(objectivesAlphaChange)
                        {
                            if (cbc.bgRend != null) cbc.bgRend.SetAlpha(objectivesAlpha);
                            if (cbc.textRend != null) cbc.textRend.SetAlpha(objectivesAlpha);
                            if (cbc.iconRend != null) cbc.iconRend.SetAlpha(objectivesAlpha);
                            if (cbc.barRend != null) cbc.barRend.SetAlpha(objectivesAlpha);
                            if (cbc.progressBGrend != null) cbc.progressBGrend.SetAlpha(objectivesAlpha);
                        }

                        if (cbc.fadeOut)
                        {
                            ActivateObjectivesDisplay();

                            if (cbc.strikeThroughProgress < cbc.objective.name.Length)
                            {
                                cbc.strikeThroughProgress += Time.deltaTime * 5f;
                                cbc.text.text = "<s>" + cbc.objective.name;
                                cbc.text.text.Insert(Mathf.RoundToInt(cbc.strikeThroughProgress), "</s>");
                            }

                            if (cbc.fadeInProgress > 0f)
                            {
                                cbc.fadeInProgress -= Time.deltaTime * 0.75f;
                                cbc.fadeInProgress = Mathf.Clamp01(cbc.fadeInProgress);

                                if(cbc.bgRend != null) cbc.bgRend.SetAlpha(cbc.fadeInProgress * objectivesAlpha);
                                if(cbc.textRend != null) cbc.textRend.SetAlpha(cbc.fadeInProgress * objectivesAlpha); 
                                if(cbc.iconRend != null) cbc.iconRend.SetAlpha(cbc.fadeInProgress * objectivesAlpha);
                                if(cbc.barRend != null) cbc.barRend.SetAlpha(cbc.fadeInProgress * objectivesAlpha);
                                if (cbc.progressBGrend != null) cbc.progressBGrend.SetAlpha(cbc.fadeInProgress * objectivesAlpha);
                            }
                            else
                            {
                                if (cbc != null && cbc.gameObject != null) Destroy(cbc.gameObject);
                                objectiveList.RemoveAt(u);
                                u--;
                            }
                        }
                        else if(cbc.objective != null)
                        {
                            //Removal safety check
                            if(cbc.objective.thisCase != null && cbc.objective.thisCase.job != null && cbc.objective.thisCase.job.state == SideJob.JobState.ended && (cbc.objective.isCancelled || cbc.objective.isComplete))
                            {
                                cbc.Remove();
                            }
                        }
                    }
                }
            }
        }

        catch
        {

        }

        try
        {
             if (!SessionData.Instance.isFloorEdit && SessionData.Instance.play)
            {
                //Always keep the compass background pointing north
                backgroundTransform.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);

                //Update all current icons
                compassDesiredAlpha = 0f;

                //The highest priority icons will loop first, so if any subsequent ones are detected then hide them...
                HashSet<object> displayedIcons = new HashSet<object>();

                for (int i = 0; i < awarenessIcons.Count; i++)
                {
                    AwarenessIcon icon = awarenessIcons[i];

                    Vector3 forwardRelative = Vector3.zero;
                    Vector3 currentPosition = icon.targetPosition;
                    object subject = null;

                    //Look towards target
                    if (icon.awarenessType == AwarenessType.actor)
                    {
                        currentPosition = icon.actor.transform.position;
                        subject = icon.actor as object;
                    }
                    else if (icon.awarenessType == AwarenessType.transform)
                    {
                        currentPosition = icon.targetTransform.position;
                        subject = icon.targetTransform as object;
                    }

                    //If there are no previously displayed icons with this subject then become visible
                    if (!displayedIcons.Contains(subject))
                    {
                        displayedIcons.Add(subject);

                        if(!icon.spawned.activeSelf)
                        {
                            icon.spawned.SetActive(true);
                        }
                    }
                    else
                    {
                        icon.spawned.SetActive(false);
                        continue;
                    }

                    //World position of this
                    Vector3 relativePos = CameraController.Instance.cam.transform.position - currentPosition;
                    relativePos.y = 0;
                    icon.spawned.transform.rotation = Quaternion.LookRotation(relativePos, Vector3.up);

                    //Animate alert
                    if(icon.triggerAlert)
                    {
                        if(icon.alertProgress < 1f)
                        {
                            icon.alertProgress += Time.deltaTime * 5f;
                            icon.alertProgress = Mathf.Clamp01(icon.alertProgress);

                            //Reverse removal progress
                            if(icon.removalProgress > 0f)
                            {
                                icon.removalProgress -= Time.deltaTime * 7f;
                                icon.removalProgress = Mathf.Clamp01(icon.removalProgress);
                            }

                            //Increase size
                            float scale = 2f + icon.alertProgress;
                            icon.imageTransform.localScale = new Vector3(scale, scale, scale);
                            icon.arrowTransform.localScale = new Vector3(0.066f, 0.1f, 0.1f + (icon.alertProgress * 0.1f));

                            //Emission
                            icon.imageMaterial.SetColor("_EmissiveColor", Color.Lerp(InterfaceControls.Instance.spottedNormalEmission, InterfaceControls.Instance.awarenessAlertEmission, icon.alertProgress));
                            icon.arrowMaterial.SetColor("_EmissiveColor", Color.Lerp(InterfaceControls.Instance.arrowNormalEmission, InterfaceControls.Instance.awarenessAlertEmission, icon.alertProgress));
                        }
                        else
                        {
                            icon.triggerAlert = false;
                        }
                    }
                    else if(icon.alertProgress > 0f)
                    {
                        icon.alertProgress -= Time.deltaTime * 0.5f;
                        icon.alertProgress = Mathf.Clamp01(icon.alertProgress);

                        //Increase size
                        float scale = 2f + icon.alertProgress;
                        icon.imageTransform.localScale = new Vector3(scale, scale, scale);
                        icon.arrowTransform.localScale = new Vector3(0.066f, 0.1f, 0.1f + (icon.alertProgress * 0.1f));

                        //Emission
                        icon.imageMaterial.SetColor("_EmissiveColor", Color.Lerp(InterfaceControls.Instance.spottedNormalEmission, InterfaceControls.Instance.awarenessAlertEmission, icon.alertProgress));
                        icon.arrowMaterial.SetColor("_EmissiveColor", Color.Lerp(InterfaceControls.Instance.arrowNormalEmission, InterfaceControls.Instance.awarenessAlertEmission, icon.alertProgress));
                    }

                    //This is used in calculation of display alpha
                    forwardRelative = currentPosition - Player.Instance.transform.position;
                    forwardRelative.y = 0;

                    //Set display alpha (additive only)
                    if (icon.awarenessBehaviour == AwarenessBehaviour.invisibleInfront)
                    {
                        float angleBetween = Vector3.SignedAngle(forwardRelative, Player.Instance.transform.forward, Vector3.up);
                        icon.displayAlpha = Mathf.Clamp01(Mathf.Abs(angleBetween) / 75f - 1f + (icon.alertProgress * 2f)); //Add alert progress to alpha
                    }
                    else if (icon.awarenessBehaviour == AwarenessBehaviour.alwaysVisible)
                    {
                        icon.displayAlpha = 1f;
                    }

                    //Use distance limit
                    if(Vector3.Distance(Player.Instance.transform.position, currentPosition) >= icon.maxDistance)
                    {
                        icon.displayAlpha = 0f;
                    }

                    //Fade in
                    float normalZ = -3f;

                    if (icon.fadeIn < 1f)
                    {
                        icon.fadeIn += Time.deltaTime * 5f;
                        icon.fadeIn = Mathf.Max(icon.fadeIn, icon.alertProgress * 2f); //Set to alert progress
                        icon.fadeIn = Mathf.Clamp01(icon.fadeIn);
                        icon.SetAlpha(Mathf.Clamp01(icon.fadeIn * 1.5f)); //Fade in faster than the arrow movement

                        //Move arrow outwards
                        normalZ = Mathf.SmoothStep(-3f, -5f, icon.fadeIn);
                        icon.arrowTransform.localPosition = new Vector3(0, 0, normalZ);
                    }
                    else if (icon.springAction < 1f)
                    {
                        icon.springAction += Time.deltaTime * 0.5f;

                        normalZ = Mathf.SmoothStep(-6f, -5f, icon.springAction);
                        icon.arrowTransform.localPosition = new Vector3(0, 0, normalZ);
                    }

                    //Alert
                    if(icon.triggerAlert)
                    {
                        //Move arrow outwards
                        icon.arrowTransform.localPosition = new Vector3(0, 0, Mathf.SmoothStep(normalZ, -5f, icon.alertProgress));
                    }
                    else if(icon.alertProgress > 0f)
                    {
                        icon.arrowTransform.localPosition = new Vector3(0, 0, Mathf.SmoothStep(normalZ, -5f, icon.alertProgress));
                    }

                    //Removal
                    if (icon.removalFlag && !icon.triggerAlert)
                    {
                        if (icon.removalProgress < 1f)
                        {
                            icon.removalProgress += Time.deltaTime * 2f;
                            icon.removalProgress = Mathf.Clamp01(icon.removalProgress);
                        }

                        if (icon.removalProgress >= 1f)
                        {
                            Destroy(icon.spawned);
                            awarenessIcons.RemoveAt(i);

                            //Sort awarenesss icons by priority...
                            InterfaceController.Instance.awarenessIcons.Sort((p1, p2) => p2.priority.CompareTo(p1.priority)); //Using P2 first gives highest first

                            i--;
                            continue;
                        }
                    }

                    //Update alpha
                    icon.SetAlpha(icon.alpha);

                    //Always keep icon facing the player
                    icon.imageTransform.rotation = CameraController.Instance.cam.transform.rotation;
                    //icon.imageTransform.LookAt(CameraController.Instance.cam.transform.position, CameraController.Instance.cam.transform.up);

                    //The compass is only as visible as the most visible pointer with a slight lag
                    compassDesiredAlpha = Mathf.Max(compassDesiredAlpha, icon.GetActualAlpha());
                }

                compassDesiredAlpha = Mathf.Clamp01(compassDesiredAlpha);

                //Be quick to match if the desired alpha is higher...
                if (compassDesiredAlpha > compassActualAlpha)
                {
                    compassActualAlpha = compassDesiredAlpha;

                    Color rgb = compassMeshRend.sharedMaterial.GetColor("_UnlitColor");
                    compassMeshRend.sharedMaterial.SetColor("_UnlitColor", new Color(rgb.r, rgb.g, rgb.b, compassActualAlpha));
                }
                else if (compassDesiredAlpha < compassActualAlpha)
                {
                    compassActualAlpha -= Time.deltaTime;

                    if (compassDesiredAlpha > compassActualAlpha)
                    {
                        compassActualAlpha = compassDesiredAlpha;
                    }

                    Color rgb = compassMeshRend.sharedMaterial.GetColor("_UnlitColor");
                    compassMeshRend.sharedMaterial.SetColor("_UnlitColor", new Color(rgb.r, rgb.g, rgb.b, compassActualAlpha));
                }
            }
        }

        catch
        {

        }

        try
        {
            //Update the position of the cursor rigidbody
            if(desktopMode)
            {
                Vector2 localPoint = Vector2.zero;

                if(InputController.Instance.mouseInputMode)
                {
                    RectTransformUtility.ScreenPointToLocalPointInRectangle(InterfaceControls.Instance.caseBoardContentContainer, Input.mousePosition, null, out localPoint);
                    InterfaceControls.Instance.caseBoardCursorRBContainer.localPosition = localPoint;
                }
                else
                {
                    //If using a controller, the 'cursor' is always in the middle of the viewport.
                    if(selectedElement != null)
                    {
                        InterfaceControls.Instance.caseBoardCursorRBContainer.position = selectedElement.transform.position;
                    }
                    else InterfaceControls.Instance.caseBoardCursorRBContainer.position = CasePanelController.Instance.caseboardScroll.transform.position;
                }

                //Start drag
                if (Input.GetMouseButtonDown(0) && pinnedBeingDragged == null && ContextMenuController.activeMenu == null && InputController.Instance.mouseInputMode)
                {
                    //Clean mouse over elements
                    //ClearAllMouseOverElements();

                    if(currentMouseOverElement.Count <= 0)
                    {
                        //Initialise box select
                        if (!boxSelectActive)
                        {
                            //Don't start a new box if over a type of button
                            GameObject newObj = Instantiate(PrefabControls.Instance.boxSelect, InterfaceControls.Instance.caseBoardContentContainer);
                            boxSelect = newObj.GetComponent<RectTransform>();
                            boxSelect.localPosition = localPoint;
                            boxSelectActive = true;

                            Game.Log("Interface: New box select active...");
                        }
                    }
                }

                //Drag select evidence
                if (Input.GetMouseButton(0) && boxSelectActive && InputController.Instance.mouseInputMode)
                {
                    //Set pivot
                    if (localPoint.x - boxSelect.localPosition.x < 0)
                    {
                        boxSelect.pivot = new Vector2(1, boxSelect.pivot.y);
                    }
                    else
                    {
                        boxSelect.pivot = new Vector2(0, boxSelect.pivot.y);
                    }

                    if (localPoint.y - boxSelect.localPosition.y < 0)
                    {
                        boxSelect.pivot = new Vector2(boxSelect.pivot.x, 1);
                    }
                    else
                    {
                        boxSelect.pivot = new Vector2(boxSelect.pivot.x, 0);
                    }

                    //Set size
                    boxSelect.sizeDelta = new Vector2(Mathf.Abs(localPoint.x - boxSelect.localPosition.x), Mathf.Abs(localPoint.y - boxSelect.localPosition.y));

                    //Select as we drag...
                    //Get the pin controllers
                    List<PinnedItemController> newSelected = new List<PinnedItemController>();

                    Rect worldRect = Toolbox.Instance.GetWorldRect(boxSelect, InterfaceControls.Instance.caseBoardContentContainer.localScale);

                    //Find inside this rect
                    foreach (PinnedItemController pinned in CasePanelController.Instance.spawnedPins)
                    {
                        if (pinned.pinButtonController != null)
                        {
                            if (worldRect.Contains(pinned.pinButtonController.rect.position, true))
                            {
                                if (!newSelected.Contains(pinned))
                                {
                                    newSelected.Add(pinned);
                                }
                            }
                        }
                    }

                    //Deselect all windows that aren't in the new rect
                    for (int i = 0; i < selectedPinned.Count; i++)
                    {
                        PinnedItemController pbc = selectedPinned[i];

                        if(pbc == null)
                        {
                            selectedPinned.RemoveAt(i);
                            InteractionController.Instance.UpdateInteractionText(); //Update text to show custom string action
                            i--;
                        }
                        else if (!newSelected.Contains(pbc))
                        {
                            Game.Log("Interface: Deselect " + pbc.name);
                            pbc.SetSelected(false, false);
                            i--;
                        }
                    }

                    foreach(PinnedItemController pinned in newSelected)
                    {
                        pinned.SetSelected(true, true);
                    }
                }
                else if(boxSelectActive)
                {
                    if(boxSelect != null)
                    {
                        Game.Log("Interface: Box select inactive");
                        Destroy(boxSelect.gameObject);
                        boxSelectActive = false;
                    }
                }
            }
        }

        catch
        {

        }


        try
        {
            //Fade out windows when dragging pinned item
            if(pinnedBeingDragged != null)
            {
                if(!windowFullFade)
                {
                    windowFullFade = true;
                }
            }
        }
        catch
        {

        }

        try
        {
            if(windowFullFade)
            {
                if (windowFadeProgress < 0.95f)
                {
                    windowFadeProgress += 3.75f * Time.deltaTime;
                    windowFadeProgress = Mathf.Clamp01(windowFadeProgress);
                    InterfaceController.Instance.windowCanvasGroup.alpha = 1f - windowFadeProgress;
                }
                else
                {
                    windowFullFade = false;
                }
            }
            else if(windowFadeProgress > 0f)
            {
                windowFadeProgress -= 4.75f * Time.deltaTime;
                windowFadeProgress = Mathf.Clamp01(windowFadeProgress);
                InterfaceController.Instance.windowCanvasGroup.alpha = 1f - windowFadeProgress;
            }
        }

        catch
        {

        }

        //Depth of field
        try
        {
            if(dofProgress < 1f && Game.Instance.depthBlur)
            {
                dofProgress += Time.deltaTime / GameplayControls.Instance.dofChangeTime;
                SessionData.Instance.dof.nearFocusStart.value = Mathf.SmoothStep(SessionData.Instance.dof.focusDistance.value, desiredDofNearStart, dofProgress);
                SessionData.Instance.dof.nearFocusEnd.value = Mathf.SmoothStep(SessionData.Instance.dof.focusDistance.value, desiredDofNearEnd, dofProgress);
                SessionData.Instance.dof.farFocusStart.value = Mathf.SmoothStep(SessionData.Instance.dof.focusDistance.value, desiredDofFarStart, dofProgress);
                SessionData.Instance.dof.farFocusEnd.value = Mathf.SmoothStep(SessionData.Instance.dof.focusDistance.value, desiredDofFarEnd, dofProgress);

                if (dofProgress >= 1f)
                {
                    //Turn off when at threshold
                    if(desiredDofNearStart == GameplayControls.Instance.dofNormalNearStart && desiredDofNearEnd == GameplayControls.Instance.dofNormalNearEnd)
                    {
                        if (desiredDofFarStart == GameplayControls.Instance.dofNormalFarStart && desiredDofFarEnd == GameplayControls.Instance.dofNormalFarEnd)
                        {
                            SessionData.Instance.dof.active = false;
                        }
                    }
                }
                else
                {
                    SessionData.Instance.dof.active = true;
                }
            }
        }
        catch
        {

        }
    }

    public void UpdateDOF()
    {
        if(!Game.Instance.depthBlur)
        {
            SessionData.Instance.dof.active = false;
            return;
        }

        //Game.Log("Update DOF: " + SessionData.Instance.play + ", " + SessionData.Instance.isDecorEdit);

        float newNearStart = GameplayControls.Instance.dofNormalNearStart;
        float newNearEnd = GameplayControls.Instance.dofNormalNearEnd;
        float newFarStart = GameplayControls.Instance.dofNormalFarStart;
        float newFarEnd = GameplayControls.Instance.dofNormalFarEnd;

        //Disable depth of field in decor edit mode so you can more clearly see things
        if (!SessionData.Instance.play && !SessionData.Instance.isDecorEdit && (PlayerApartmentController.Instance != null && !PlayerApartmentController.Instance.decoratingMode && !PlayerApartmentController.Instance.furniturePlacementMode))
        {
            newNearStart = GameplayControls.Instance.dofPausedNearStart;
            newNearEnd = GameplayControls.Instance.dofPausedNearEnd;
            newFarStart = GameplayControls.Instance.dofPausedFarStart;
            newFarEnd = GameplayControls.Instance.dofPausedFarEnd;
        }
        //Fade in
        else if(desiredFade > 0f)
        {
            newNearStart = Mathf.Lerp(GameplayControls.Instance.dofNormalNearStart, GameplayControls.Instance.dofPausedNearStart, desiredFade);
            newNearEnd = Mathf.Lerp(GameplayControls.Instance.dofNormalNearEnd, GameplayControls.Instance.dofPausedNearEnd, desiredFade);
            newFarStart = Mathf.Lerp(GameplayControls.Instance.dofNormalFarStart, GameplayControls.Instance.dofPausedFarStart, desiredFade);
            newFarEnd = Mathf.Lerp(GameplayControls.Instance.dofNormalFarEnd, GameplayControls.Instance.dofPausedFarEnd, desiredFade);
        }
        else if(InteractionController.Instance != null && InteractionController.Instance.dialogMode)
        {
            newNearStart = GameplayControls.Instance.dofTalkingNearStart;
            newNearEnd = GameplayControls.Instance.dofTalkingNearEnd;
            newFarStart = GameplayControls.Instance.dofTalkingFarStart;
            newFarEnd = GameplayControls.Instance.dofTalkingFarEnd;
        }

        if(newNearStart != desiredDofNearEnd || newNearEnd != desiredDofNearEnd || newFarStart != desiredDofFarStart || newFarEnd != desiredDofFarEnd)
        {
            dofProgress = 0f;
            SessionData.Instance.dof.active = true;

            desiredDofNearStart = newNearStart;
            desiredDofNearEnd = newNearEnd;
            desiredDofFarStart = newFarStart;
            desiredDofFarEnd = newFarEnd;
        }
    }

    //Select all pins
    public void DeselectAllPins()
    {
        Game.Log("Interface: Deselect all pinned");

        //Deselect all windows that aren't in the new rect
        List<PinnedItemController> pinned = new List<PinnedItemController>(selectedPinned);

        for (int i = 0; i < selectedPinned.Count; i++)
        {
            PinnedItemController pbc = selectedPinned[i];
            if(pbc != null) pbc.SetSelected(false, false);
        }

        selectedPinned.Clear();
        InteractionController.Instance.UpdateInteractionText(); //Update text to show custom string action
    }

    public void UpdateAnchoredSpeechPositions()
    {
        if (anchoredSpeech.Count > 0)
        {
            //Sort this list by timestamp
            anchoredSpeech.Sort((p1, p2) => p1.timeStamp.CompareTo(p2.timeStamp)); //Highest first
            float yPos = 0;

            //Is there something the player is saying (VO) in the list? If so then it needs to take top priority...
            List<SpeechBubbleController> playerVO = anchoredSpeech.FindAll(item => item.isPlayer);

            foreach(SpeechBubbleController vo in playerVO)
            {
                anchoredSpeech.Remove(vo);
                anchoredSpeech.Insert(0, vo); //Insert @ top
            }

            for (int i = 0; i < anchoredSpeech.Count; i++)
            {
                SpeechBubbleController sp = anchoredSpeech[i];

                yPos -= sp.bubbleRect.sizeDelta.y * 0.5f;
                sp.desiredPosition = speechDisplayAnchor.TransformPoint(new Vector3(0, yPos, 0));
                yPos -= sp.bubbleRect.sizeDelta.y * 0.5f + 4f;

                //If more than 2 entries or player is speaking, hide offscreen
                if (i > 1 || sp.isPlayer)
                {
                    yPos -= 300;
                }
            }
        }
    }
    
    public AwarenessIcon AddAwarenessIcon(AwarenessType newType, AwarenessBehaviour newBehaviour, Actor newActor, Transform newTransform, Vector3 newPosition, Material newMat, int newPriority, bool forceMaxDistance = false, float maxDist = 20f)
    {
        //Check for existing
        AwarenessIcon existing = awarenessIcons.Find(item => item.awarenessType == newType && item.awarenessBehaviour == newBehaviour && item.actor == newActor && item.targetTransform == newTransform && item.targetPosition == newPosition);

        if(existing != null)
        {
            Game.Log("Interface: Found existing awareness icon for " + newActor);

            //If this is in the process of removal, then stop
            if(existing.removalFlag)
            {
                existing.removalFlag = false;
                existing.removalProgress = 0f;
            }

            return existing;
        }

        AwarenessIcon newEntry = new AwarenessIcon();
        newEntry.awarenessType = newType;
        newEntry.awarenessBehaviour = newBehaviour;

        newEntry.actor = newActor;
        newEntry.targetTransform = newTransform;
        newEntry.targetPosition = newPosition;
        newEntry.priority = newPriority;
        newEntry.maxDistance = InterfaceControls.Instance.awarenessDistanceThreshold;
        if (forceMaxDistance) newEntry.maxDistance = maxDist;

        newEntry.spawned = Instantiate(PrefabControls.Instance.awarenessIndicator, backgroundTransform);
        newEntry.imageMaterial = Instantiate(newMat);
        newEntry.arrowMaterial = Instantiate(InterfaceControls.Instance.arrow);
        newEntry.setup = true;

        if(newEntry.overrideTexture != null)
        {
            newEntry.imageMaterial.SetTexture("_UnlitColorMap", newEntry.overrideTexture);
        }

        foreach (Transform tr in newEntry.spawned.transform)
        {
            foreach (Transform tr2 in tr.transform)
            {
                if (tr2.tag == "AwarenessIndicator")
                {
                    newEntry.imageTransform = tr2;
                    newEntry.imageTransform.gameObject.GetComponent<MeshRenderer>().material = newEntry.imageMaterial; //Set instanced material
                }
                else
                {
                    newEntry.arrowTransform = tr2;
                    newEntry.arrowTransform.gameObject.GetComponent<MeshRenderer>().material = newEntry.arrowMaterial; //Set instanced material
                }
            }
        }

        if (newEntry.actor != null) newEntry.targetPosition = newEntry.actor.transform.position;
        else if (newEntry.targetTransform != null) newEntry.targetPosition = newEntry.targetTransform.position;

        //Calculate display alpha: If this is 90 degrees of the player it's 1, if dead infront it's 0
        Vector3 forwardRelative = newEntry.targetPosition - Player.Instance.transform.position;
        forwardRelative.y = 0;

        //Set display alpha (additive only)
        if(newEntry.awarenessBehaviour == AwarenessBehaviour.invisibleInfront)
        {
            float angleBetween = Vector3.SignedAngle(forwardRelative, Player.Instance.transform.forward, Vector3.up);
            newEntry.displayAlpha = Mathf.Clamp01(Mathf.Abs(angleBetween) / 75f - 1f);
        }
        else if(newEntry.awarenessBehaviour == AwarenessBehaviour.alwaysVisible)
        {
            newEntry.displayAlpha = 1f;
        }

        awarenessIcons.Add(newEntry);

        //Sort awarenesss icons by priority...
        InterfaceController.Instance.awarenessIcons.Sort((p1, p2) => p2.priority.CompareTo(p1.priority)); //Using P2 first gives highest first

        return newEntry;
    }

    public UIPointerController AddUIPointer(Objective newObjective)
    {
        GameObject newPointer = Instantiate(PrefabControls.Instance.uiPointer, uiPointerContainer);
        UIPointerController pointer = newPointer.GetComponent<UIPointerController>();
        pointer.Setup(newObjective);

        return pointer;
    }

    //Spawn an info window
    public InfoWindow SpawnWindow(  Evidence passedEvidence,
                                    Evidence.DataKey passedEvidenceKey = Evidence.DataKey.name, //Optional passed singular key (default is name)
                                    List<Evidence.DataKey> passedEvidenceKeys = null, //Optional passed evidence keys (overrides above)
                                    string presetName = "", //Optional non-evidence preset (uses dictionary to find)
                                    bool worldInteraction = false,
                                    bool autoPosition = true,
                                    Vector2 forcePosition = new Vector2(),
                                    Interactable passedInteractable = null,
                                    Case passedCase = null,
                                    Case.CaseElement forcedPinnedElement = null,
                                    bool passDialogSuccess = true
                                  )
    {
        //Game.Log("Window: Spawn request for " + passedEvidence.name + " using preset " + presetName);

        WindowStylePreset windowPreset = null;

        //Does this window represent evidence?
        if (presetName.Length <= 0 && passedEvidence != null)
        {
            //If no data keys have been passed, use the singular key, which defaults to 'name'
            if(passedEvidenceKeys == null)
            {
                passedEvidenceKeys = new List<Evidence.DataKey>();
                passedEvidenceKeys.Add(passedEvidenceKey);
            }

            //Get current tied keys
            passedEvidenceKeys = passedEvidence.GetTiedKeys(passedEvidenceKeys);

            if (!passedEvidence.isFound && (worldInteraction || passedEvidence.preset.markAsDiscoveredOnAnyInteraction))
            {
                passedEvidence.SetFound(true); //Set found
            }

            //Search to see if this evidence window combination exists already...
            //Shortlist of windows representing this evidence
            List<InfoWindow> evWindows = activeWindows.FindAll(item => item.passedEvidence == passedEvidence);

            //Do any of the active windows contain these keys?
            InfoWindow foundWindow = null;

            if (ContextMenuController.activeMenu != null)
            {
                ContextMenuController.activeMenu.ForceClose();
            }

            Game.Log("Interface: Checking against " + activeWindows.Count + " existing windows, with " + passedEvidenceKeys.Count + " passed keys...");

            foreach (InfoWindow win in evWindows)
            {
                foreach(Evidence.DataKey key in passedEvidenceKeys)
                {
                    //Key must be unique to count
                    if(passedEvidence.preset.IsKeyUnique(key))
                    {
                        //Found an evidence window that contains one of these keys
                        if (win.evidenceKeys.Contains(key))
                        {
                            Game.Log("Interface: Exists already: " + win.name + " (" + key.ToString() + ")");

                            //If window is active flash that, if not flash icon
                            win.transform.SetAsLastSibling();

                            //Set to world interaction
                            if(worldInteraction && !win.isWorldInteraction)
                            {
                                win.SetWorldInteraction(true);
                            }

                            foundWindow = win;
                            break;
                        }
                    }
                }

                if (foundWindow != null) break;
            }

            if (foundWindow != null)
            {
                return foundWindow;
            }

            //Get the window preset from the evidence preset
            windowPreset = passedEvidence.preset.windowStyle;
        }
        else
        {
            //If not evidence, derrive the window preset from the string argument
            presetName = presetName.ToLower();

            try
            {
                windowPreset = windowDictionary[presetName];
            }
            catch
            {
                Game.Log("Window: Could not load window preset, there is no type: " + windowPreset + " Dict count: " + windowDictionary.Count);
                return null;
            }
        }

        //Spawn window
        GameObject newWindow = Instantiate(PrefabControls.Instance.infoWindow, InterfaceController.Instance.windowCanvas.transform);

        //Get window script
        InfoWindow script = newWindow.GetComponent<InfoWindow>();
        script.SetPivot(new Vector2(0, 1));

        //Add to list
        activeWindows.Add(script);

        //Setup window
        script.Setup(windowPreset, passedEvidence, passedEvidenceKeys, worldInteraction, passedInteractable, passedCase, forcedPinnedElement, passDialogSuccess);

        //Set default position
        RectTransform newRect = newWindow.GetComponent<RectTransform>();

        Vector2 anch = Vector2.zero;
        Vector2 offset = Vector2.zero;

        if (autoPosition)
        {
            //Anchored position from screen position
            anch = new Vector2(InterfaceControls.Instance.hudCanvasRect.rect.width * InterfaceControls.Instance.defaultWindowLocation.x, -InterfaceControls.Instance.hudCanvasRect.rect.height * InterfaceControls.Instance.defaultWindowLocation.y);

            //Apply offset
            offset = new Vector2(InterfaceControls.Instance.hudCanvasRect.rect.width * (InterfaceControls.Instance.windowCountOffset.x * activeWindows.Count), -InterfaceControls.Instance.hudCanvasRect.rect.height * (InterfaceControls.Instance.windowCountOffset.y * activeWindows.Count));
        }
        else
        {
            anch = new Vector2(InterfaceControls.Instance.hudCanvasRect.rect.width * forcePosition.x , -InterfaceControls.Instance.hudCanvasRect.rect.height * forcePosition.y) - new Vector2(windowPreset.defaultSize.x * 0.5f, windowPreset.defaultSize.y * -0.5f);
            Game.Log("Interface: Forcing position for " + script.name + ": " + forcePosition + " = " + anch);
        }

        script.SetAnchoredPosition(anch + offset);
        //script.SetLocalPosition(anch + offset);

        //Add to history
        if (passedEvidence != null && !passedEvidence.preset.disableHistory)
        {
            GameplayController.Instance.AddHistory(passedEvidence, passedEvidenceKeys);
        }

        newWindow.transform.SetAsLastSibling();

        //Restore?
        if(script.pinned)
        {
            InterfaceController.Instance.RestoreWindow(script);
        }

        //Close the upgrades screen
        if (UpgradesController.Instance.isOpen)
        {
            UpgradesController.Instance.CloseUpgrades();
        }

        //Set selection mode to window
        if (CasePanelController.Instance.controllerMode)
        {
            if(CasePanelController.Instance.currentSelectMode != CasePanelController.ControllerSelectMode.windows)
            {
                CasePanelController.Instance.SetControllerMode(true, CasePanelController.ControllerSelectMode.windows);
            }

            CasePanelController.Instance.SetSelectedWindow(script);
        }

        //Close quick menu
        if (PinnedItemController.activeQuickMenu != null)
        {
            PinnedItemController.activeQuickMenu.Remove();
        }

        //Return the window
        return script;
    }

    //Set the currently dragged object
    public void SetDragged(GameObject drag, string tag, Vector2 dCursorOffset)
    {
    	dragged = drag;
    	draggedTag = tag;
        dragCursorOffset = dCursorOffset;
    }

    //Return window
    public InfoWindow GetWindow(Evidence winEntry)
    {
        if (winEntry == null) return null;

        int index = activeWindows.FindIndex(item => item.passedEvidence == winEntry);

        if (index > -1)
        {
            return activeWindows[index];
        }
        else return null;
    }

    //Return specific window
    public InfoWindow GetWindow(Evidence winEntry, List<Evidence.DataKey> evKeys)
    {
        List<InfoWindow> wins = activeWindows.FindAll(item => item.passedEvidence == winEntry);

        foreach(InfoWindow win in wins)
        {
            foreach(Evidence.DataKey key in evKeys)
            {
                if (win.evidenceKeys.Contains(key))
                {
                    return win;
                }
            }
        }

        return null;
    }

    //Run a minimize/restore animation
    public void MinimizeWindow(InfoWindow window)
    {
        if (window == null || window.currentPinnedCaseElement == null || window.currentPinnedCaseElement.pinnedController == null)
        {
            Game.Log("Interface: Destroying window as there is no case element to minimize to...");
            Destroy(window.gameObject);
            return;
        }

        //Calculate scale
        Vector2 minimizedScale = new Vector2(window.currentPinnedCaseElement.pinnedController.rect.sizeDelta.x / window.rect.sizeDelta.x, window.currentPinnedCaseElement.pinnedController.rect.sizeDelta.y / window.rect.sizeDelta.y);

        StartCoroutine(WindowScaleAnimation(window, window.currentPinnedCaseElement.pinnedController.rect.position, window.currentPinnedCaseElement.pinnedController.rect.pivot, minimizedScale, true));
    }

    public void RestoreWindow(InfoWindow window)
    {
        if (window == null || window.currentPinnedCaseElement == null || window.currentPinnedCaseElement.pinnedController == null)
        {
            Game.Log("Interface: Destroying window as there is no case element to restore to...");
            Destroy(window.gameObject);
            return;
        }

        //Set actual window scale to mini
        RectTransform itemCanvas = window.windowCanvas.GetComponent<RectTransform>();
        itemCanvas.pivot = window.currentPinnedCaseElement.pinnedController.rect.pivot;
        itemCanvas.localScale = new Vector2(window.currentPinnedCaseElement.pinnedController.rect.sizeDelta.x / window.rect.sizeDelta.x, window.currentPinnedCaseElement.pinnedController.rect.sizeDelta.y / window.rect.sizeDelta.y);
        itemCanvas.position = window.currentPinnedCaseElement.pinnedController.rect.position;

        StartCoroutine(WindowScaleAnimation(window, window.currentPinnedCaseElement.resPos, window.currentPinnedCaseElement.resPiv, Vector2.one, false));
    }

    IEnumerator WindowScaleAnimation(InfoWindow window, Vector2 toPosition, Vector2 toPivot, Vector2 toScale, bool removeAtEnd)
    {
        window.gameObject.SetActive(true);

        //Get the canvas rect
        RectTransform itemCanvas = window.windowCanvas.GetComponent<RectTransform>();
        GraphicRaycaster gr = window.GetComponent<GraphicRaycaster>();
        if (gr != null) gr.enabled = false; //Block interaction when animating

        //Movement direction used to apply feedback force
        Vector2 movementDirection = new Vector2(toPosition.x - itemCanvas.position.x, toPosition.y - itemCanvas.position.y);
        Vector2 startScale = itemCanvas.localScale;

        Game.Log("Interface: Window scale animation start: " + window + ", " + toPosition + ", " + toPivot + ", " + toScale + ", " + removeAtEnd);

        float progress = 0f;

        while(progress < 1f)
        {
            progress += InterfaceControls.Instance.minimizingAnimationSpeed * Time.deltaTime;
            progress = Mathf.Clamp01(progress);

            //Lerp pivot
            itemCanvas.pivot = Vector2.Lerp(itemCanvas.pivot, toPivot, progress);

            //Lerp size
            itemCanvas.localScale = Vector2.Lerp(startScale, toScale, progress);

            //Lerp position
            itemCanvas.position = Vector3.Lerp(itemCanvas.position, toPosition, progress);

            float clampedX = Mathf.Clamp(itemCanvas.localPosition.x, InterfaceControls.Instance.hudCanvasRect.rect.width * -0.5f, InterfaceControls.Instance.hudCanvasRect.rect.width * 0.5f);
            float clampedY = Mathf.Clamp(itemCanvas.localPosition.y, InterfaceControls.Instance.hudCanvasRect.rect.height * -0.5f, InterfaceControls.Instance.hudCanvasRect.rect.height * 0.5f);
            itemCanvas.localPosition = new Vector3(clampedX, clampedY, 0);

            yield return null;
        }

        //Game.Log("Window scale animation completed");

        if (removeAtEnd)
        {
            //Feedback pinned object
            if(window != null && window.currentPinnedCaseElement != null && window.currentPinnedCaseElement.pinnedController != null)
            {
                window.currentPinnedCaseElement.pinnedController.juice.Nudge(new Vector2(1.2f, 1.2f), Vector2.zero, affectRotation: false);
                if (window.currentPinnedCaseElement.pinnedController.rb != null) window.currentPinnedCaseElement.pinnedController.rb.AddForce(movementDirection * 13f);
            }

            Game.Log("Interface: destroy window " + window.name + " (upon minimize animation)");
            Destroy(window.gameObject);
        }
        else
        {
            if (gr != null) gr.enabled = true;
        }
    }

    //Force close all tooltip and context menus
    public void RemoveAllMouseInteractionComponents()
    {
        if (TooltipController.activeTooltip != null) TooltipController.activeTooltip.ForceClose();
        if (ContextMenuController.activeMenu != null) ContextMenuController.activeMenu.ForceClose();
    }

    //Display the location text for a certain amount of time
    public void DisplayLocationText(float duration, bool forceUpdate)
    {
        if(displayedTextCoroutine != null) StopCoroutine(displayedTextCoroutine);
        displayedTextCoroutine = StartCoroutine(DisplayLocText(duration, forceUpdate));
    }

    IEnumerator DisplayLocText(float duration, bool forceUpdate = false)
    {
        float timeDisplayed = 0f;
        float progress = 0f;

        //Force update
        if(forceUpdate)
        {
            if (Player.Instance.currentGameLocation != null && locationText != null) locationText.text = Player.Instance.currentGameLocation.name;
            else if(locationText != null) locationText.text = string.Empty;
        }

        //Show location text
        ShowLocationText(1f);

        while (timeDisplayed < duration)
        {
            if (SessionData.Instance.play)
            {
                timeDisplayed += Time.deltaTime;
                progress = Mathf.Clamp01(timeDisplayed / duration);
            }

            yield return null;
        }

        //Hide location text
        HideLocationText(1f);
    }

    //Fade location text
    public void ShowLocationText(float fadeSpeed)
    {
        if (locationText == null) return;
        StopCoroutine("LocationTextFade");
        StartCoroutine(LocationTextFade(true, fadeSpeed));
    }

    //Unfade location text
    public void HideLocationText(float fadeSpeed)
    {
        if (locationText == null) return;
        StopCoroutine("LocationTextFade");
        StartCoroutine(LocationTextFade(false, fadeSpeed));
    }

    IEnumerator LocationTextFade(bool show = true, float fadeSpeed = 1f)
    {
        if (show)
        {
            if(locationText != null) locationText.gameObject.SetActive(true);
            locationTextDisplayed = true;
        }

        //Tween
        float snapProgress = 0f;

        //Pickup previous progress
        if (show) snapProgress = locationText.canvasRenderer.GetAlpha();
        else snapProgress = 1f - locationText.canvasRenderer.GetAlpha();

        while (snapProgress < 1f)
        {
            if(!SessionData.Instance.play)
            {
                yield return null;
            }

            snapProgress += fadeSpeed * Time.deltaTime;
            snapProgress = Mathf.Clamp01(Mathf.CeilToInt(snapProgress * 100f) / 100f);

            if(locationText != null)
            {
                if (show)
                {
                    locationText.canvasRenderer.SetAlpha(snapProgress);
                }
                else
                {
                    locationText.canvasRenderer.SetAlpha(1f - snapProgress);
                }
            }

            yield return null;
        }

        if (!show)
        {
            if(locationText != null)
            {
                locationText.gameObject.SetActive(false);
            }
            
            locationTextDisplayed = false;
        }
    }

    //Open current location as evidence
    public void OpenCurrentLocationAsEvidence()
    {
        if(Player.Instance.currentGameLocation != null)
        {
            SpawnWindow(Player.Instance.currentGameLocation.evidenceEntry, passedEvidenceKey: Evidence.DataKey.name);
        }
        else if (Player.Instance.currentGameLocation != null)
        {
            if(Player.Instance.currentGameLocation != null)
            SpawnWindow(Player.Instance.currentGameLocation.evidenceEntry, passedEvidenceKey: Evidence.DataKey.name);
        }
    }

    //Open apartment as evidence
    public void OpenApartmentAsEvidence()
    {
        if (Player.Instance.home != null)
        {
            SpawnWindow(Player.Instance.home.evidenceEntry, passedEvidenceKey: Evidence.DataKey.name);
        }
        //Has the player a hotel room?
        else
        {
            GameplayController.HotelGuest guest = GameplayController.Instance.hotelGuests.Find(item => item.GetHuman() == Player.Instance);

            if(guest !=null)
            {
                NewAddress add = guest.GetAddress();

                if(add != null)
                {
                    SpawnWindow(add.evidenceEntry, passedEvidenceKey: Evidence.DataKey.name);
                }
            }
        }
    }

    //Set interface active/inactive
    public void SetInterfaceActive(bool val)
    {
        interfaceIsActive = val;

        if(interfaceIsActive)
        {
            //Enable hud while loading
            InterfaceControls.Instance.hudCanvas.gameObject.SetActive(true);

            Player.Instance.firstFrame = true; //Needed to re-init some HUD elements
        }
        else
        {
            //Disable hud while loading
            InterfaceControls.Instance.hudCanvas.gameObject.SetActive(false);
        }
    }

    //New main UI system
    //Set desktop mode (UI cursor enabled)
    public void SetDesktopMode(bool val, bool showPanels)
    {
        desktopMode = val;

        Game.Log("Player: Set desktop mode: " + desktopMode + ", showing panels: " + showPanels);

        if (SessionData.Instance.isFloorEdit) desktopMode = false; //Disable in floor edit mode

        if (desktopMode)
        {
            InterfaceController.Instance.ActivateObjectivesDisplay();

            //ControlsDisplayController.Instance.RestoreDefaultDisplayArea();
            ControlsDisplayController.Instance.SetControlDisplayArea(60, 0, 300, 300);

            desktopModeDesiredTransition = 1f;

            if (desktopModeTransition != desktopModeDesiredTransition)
            {
                StopCoroutine("DesktopModeTransition");
                StartCoroutine("DesktopModeTransition");
            }

            //Close any UI tooltips
            RemoveAllMouseInteractionComponents();

            //Set mouse lock
            Player.Instance.EnablePlayerMouseLook(false);

            //If case board is closed, open it
            if (showDesktopCaseBoard && showPanels)
            {
                ShowCaseBoard(true);
            }
            else
            {
                ShowCaseBoard(false);
            }

            //If map is closed, open it
            if(showDesktopMap && showPanels)
            {
                ShowDesktopMap(true, false);
            }
            else
            {
                ShowDesktopMap(false, false);
            }

            CasePanelController.Instance.OnShowCaseBoard();

            uiPointerContainer.gameObject.SetActive(false);
            controlPanelCanvas.gameObject.SetActive(true);
            dialogCanvas.gameObject.SetActive(false);
            interactionProgressCanvas.gameObject.SetActive(false);

            //Set to decor mode if a window exists
            if (Player.Instance.currentGameLocation == Player.Instance.home || Player.Instance.apartmentsOwned.Contains(Player.Instance.currentGameLocation))
            {
                if(activeWindows.Exists(item => item.preset.name == "ApartmentDecor"))
                {
                    SessionData.Instance.isDecorEdit = true;
                    UpdateDOF();
                }
            }
            else
            {
                InfoWindow dec = activeWindows.Find(item => item.preset.name == "ApartmentDecor");
                if (dec != null) dec.CloseWindow(false);
            }

            //watchCanvas.gameObject.SetActive(true);
        }
        else
        {
            if (ContextMenuController.activeMenu != null)
            {
                ContextMenuController.activeMenu.ForceClose();
            }

            //Close the upgrades screen
            if (UpgradesController.Instance.isOpen)
            {
                UpgradesController.Instance.CloseUpgrades();
            }

            if (ControlsDisplayController.Instance != null)
            {
                if (InteractionController.Instance.talkingTo != null)
                {
                    ControlsDisplayController.Instance.SetControlDisplayArea(82, 0, 300, 300);
                }
                else if(Player.Instance.setAlarmMode)
                {
                    ControlsDisplayController.Instance.SetControlDisplayArea(450, 0, 300, 300);
                }
                else
                {
                    ControlsDisplayController.Instance.RestoreDefaultDisplayArea();
                }
            }

            //Remove focus
            RemoveWindowFocus();

            //Is the custom string select active?
            if (CasePanelController.Instance != null)
            {
                if(CasePanelController.Instance.customLinkSelectionMode) CasePanelController.Instance.CancelCustomStringLinkSelection();
                if (CasePanelController.Instance.pickModeActive) CasePanelController.Instance.SetPickModeActive(false, null);
            }

            //Close all windows that are in world interaction mode
            for (int i = 0; i < activeWindows.Count; i++)
            {
                InfoWindow activeWin = activeWindows[i];

                //Remove if not pinnable
                if (activeWin.isWorldInteraction && !activeWin.pinnable)
                {
                    activeWin.CloseWindow();
                    i--;
                    continue;
                }

                if (activeWin.pinButton != null && activeWin.pinButton.pointerDown)
                {
                    activeWin.pinButton.ForcePointerUp();
                }
            }

            //Cancel corkboard physics
            if(CasePanelController.Instance != null)
            {
                foreach(PinnedItemController pinned in CasePanelController.Instance.spawnedPins)
                {
                    if (pinned.rb != null)
                    {
                        pinned.rb.simulated = false; //Turn off simulation when inactive
                    }

                    if (pinned.isDragging)
                    {
                        pinned.ForceCancelDrag();
                    }
                }

                if(PinnedItemController.activeQuickMenu != null)
                {
                    PinnedItemController.activeQuickMenu.Remove(true);
                }
            }

            desktopModeDesiredTransition = 0f;

            if (desktopModeTransition != desktopModeDesiredTransition)
            {
                try
                {
                    StopCoroutine("DesktopModeTransition");
                    StartCoroutine("DesktopModeTransition");
                }
                catch
                {

                }
            }

            //Close case board is closed, open it
            ShowCaseBoard(false);

            if(CasePanelController.Instance != null) CasePanelController.Instance.OnHideCaseBoard();

            //Close map
            if (MapController.Instance != null) MapController.Instance.displayFirstPerson = true;
            ShowDesktopMap(false, false);

            if(dialogCanvas != null) dialogCanvas.gameObject.SetActive(true);
            if(interactionProgressCanvas != null) interactionProgressCanvas.gameObject.SetActive(true);

            //Close any UI tooltips
            RemoveAllMouseInteractionComponents();

            if(uiPointerContainer != null)
            {
                uiPointerContainer.gameObject.SetActive(true);
            }

            //Enable mouse look
            Player.Instance.EnablePlayerMouseLook(true);
        }
    }

    //Toggle if the map is shown when in desktop mode
    public void ToggleSetShowDesktopMap()
    {
        if(showDesktopMap)
        {
            SetShowDesktopMap(false, true);
        }
        else
        {
            SetShowDesktopMap(true, true);
        }
    }

    public void SetShowDesktopMap(bool val, bool playSound)
    {
        showDesktopMap = val;
        mapButton.icon.enabled = showDesktopMap;

        //If the desktop is open, open/close map
        if (desktopMode)
        {
            ShowDesktopMap(showDesktopMap, playSound);
        }
    }

    public void ShowDesktopMap(bool val, bool playSound)
    {
        if (minimapCanvas == null) return;

        //If the desktop is open, open/close map
        if (val)
        {
            if(MapController.Instance != null) MapController.Instance.OpenMap(false, playSound);
        }
        else
        {
            if (MapController.Instance != null) MapController.Instance.CloseMap(playSound);
        }
    }

    public void ToggleShowInventory()
    {
        BioScreenController.Instance.SetInventoryOpen(!BioScreenController.Instance.isOpen, true);
    }

    //Toggle if the case board is shown when in desktop mode
    public void ToggleSetShowDesktopCaseBoard()
    {
        if (showDesktopCaseBoard)
        {

            SetShowDesktopCaseBoard(false);
        }
        else
        {
            SetShowDesktopCaseBoard(true);
        }
    }

    public void SetShowDesktopCaseBoard(bool val)
    {
        if (caseCanvas == null) return;
        if (Player.Instance.playerKOInProgress) val = false;

        showDesktopCaseBoard = val;

        //If the desktop is open, open/close map
        if (desktopMode)
        {
            ShowCaseBoard(showDesktopCaseBoard);
        }
    }

    public void ShowCaseBoard(bool val)
    {
        //!! For some reason disabling any objects in this canvas causes the positions of the pinned items to move. I've no idea why!
        if (caseCanvas == null) return;

        if (Player.Instance.playerKOInProgress) val = false;

        //If the desktop is open, open/close map
        if (val)
        {
            caseCanvas.gameObject.SetActive(true);

            //Manually reset pinned possitions
            foreach(PinnedItemController pinned in CasePanelController.Instance.spawnedPins)
            {
                pinned.transform.localScale = Vector3.one; //Again, no idea why they are changing scale here!
                pinned.transform.localPosition = pinned.caseElement.v;
            }

            //Update strings
            CasePanelController.Instance.UpdatePinned();
        }
        else
        {
            //Deselect box
            if (boxSelectActive)
            {
                Destroy(boxSelect.gameObject);
                boxSelectActive = false;
            }

            //Deselect pins
            DeselectAllPins();

            caseCanvas.gameObject.SetActive(false);
        }
    }

    //Turn on/off background blur
    public void SetBackgroundBlur(bool val)
    {
        if (backgroundBlur == null) return;
        backgroundBlur.SetActive(val);
    }

    public void NewHelpPointer(string helpSection)
    {
        HelpContentPage helpContent = null;

        if(Toolbox.Instance.allHelpContent.TryGetValue(helpSection, out helpContent))
        {
            //Get text
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            //The first message should skip the first block (subject)
            string text = string.Empty;

            List<string> say = Player.Instance.ParseDDSMessage(helpContent.messageID, null, out _);

            if(say.Count > 0)
            {
                text = Strings.ComposeText(Strings.Get("dds.blocks", say[0]), null, Strings.LinkSetting.forceNoLinks);
                NewGameMessage(GameMessageType.helpPointer, 0, text);
            }
        }
    }

    public void NewGameMessage(GameMessageType newType, int newNumerical, string newMessage, InterfaceControls.Icon newIcon = InterfaceControls.Icon.agent, AudioEvent additionalSFX = null, bool colourOverride = false, Color col = new Color(), int newMergeType = -1, float newMessageDelay = 0f, RectTransform moveToOnDestroy = null, GameMessageController.PingOnComplete ping = GameMessageController.PingOnComplete.none, Evidence keyMergeEvidence = null, List<Evidence.DataKey> keyMergeKeys = null, Sprite iconOverride = null)
    {
        //Add to queue
        GameMessage newMess = new GameMessage();
        newMess.messageType = newType;
        newMess.numerical = newNumerical;
        newMess.graphic = InterfaceControls.Instance.iconReference.Find(item => item.iconType == newIcon).sprite;
        if (iconOverride != null) newMess.graphic = iconOverride;
        newMess.message = newMessage;
        newMess.additionalSFX = additionalSFX;
        newMess.colourOverride = colourOverride;
        newMess.col = col;
        newMess.mergeType = newMergeType;
        newMess.delay = newMessageDelay;
        newMess.moveOnDestroy = moveToOnDestroy;
        newMess.ping = ping;
        newMess.keyMergeEvidence = keyMergeEvidence;
        newMess.mergedKeys = keyMergeKeys;
        if (newType == GameMessageType.keyMerge) newMess.keyMerge = true;
        else if (newType == GameMessageType.socialCredit) newMess.socCredit = true;

        //Add to appropraite queue
        if(newType == GameMessageType.notification || newType == GameMessageType.keyMerge || newType == GameMessageType.socialCredit)
        {
            if (notificationQueue.Exists(item => item.message == newMessage)) return; //Don't allow duplicates here...
            notificationQueue.Add(newMess);
        }
        else if(newType == GameMessageType.gameHeader)
        {
            gameHeaderQueue.Add(newMess);
        }
        else if(newType == GameMessageType.helpPointer)
        {
            helpPointerQueue.Add(newMess);
        }

        //Start coroutine if not running
        if(!messageCoroutineRunning)
        {
            StartCoroutine(GameMessages());
        }
    }

    IEnumerator GameMessages()
    {
        while(gameSceenDisplayed)
        {
            yield return null;
        }

        messageCoroutineRunning = true;
        bool waitedAFrame = false;
        AudioController.LoopingSoundInfo typewriterSoundTriggered = null;

        while (notificationQueue.Count + gameHeaderQueue.Count + helpPointerQueue.Count > 0)
        {
            //if(PopupMessageController.Instance.active || !SessionData.Instance.play)
            //{
            //    yield return null; //Don't resolve when paused
            //}

            //Wait a frame before displaying anything, so we can collect multiple messages
            if(!waitedAFrame)
            {
                waitedAFrame = true;
                yield return null;
            }

            //Load next help points
            if (helpPointerQueue.Count > 0 && !PopupMessageController.Instance.active && !CutSceneController.Instance.cutSceneActive && (SessionData.Instance.play || !BioScreenController.Instance.isOpen))
            {
                //Load next game message
                if(currentHelpPointer == null)
                {
                    helpPointerRect.gameObject.SetActive(true);

                    currentHelpPointer = helpPointerQueue[0];
                    helpPointerTextDisplay = currentHelpPointer.message;
                    helpPointerProgress = 0f;
                    helpPointerFadeOut = 1f;
                    helpPointerTimer = 0f;

                    helpPointerText.SetText(currentHelpPointer.message);
                    helpPointerDesiredHeight = helpPointerText.preferredHeight + 20;
                    helpPointerText.SetText(string.Empty);

                    helpPointerRect.sizeDelta = new Vector2(helpPointerRect.sizeDelta.x, 44f);

                    foreach(CanvasRenderer r in helpPointerRenderers)
                    {
                        r.SetAlpha(0f);
                    }
                }
                else
                {
                    if(helpPointerProgress < 1f)
                    {
                        helpPointerProgress += Time.deltaTime / 0.5f;
                        helpPointerProgress = Mathf.Clamp01(helpPointerProgress);

                        helpPointerText.text = helpPointerTextDisplay.Substring(0, Mathf.CeilToInt(Mathf.Lerp(0, helpPointerTextDisplay.Length, helpPointerProgress)));
                        helpPointerRect.sizeDelta = new Vector2(helpPointerRect.sizeDelta.x, Mathf.Lerp(helpPointerRect.sizeDelta.y, helpPointerDesiredHeight, helpPointerProgress));

                        foreach (CanvasRenderer r in helpPointerRenderers)
                        {
                            r.SetAlpha(helpPointerProgress);
                        }
                    }
                    else
                    {
                        helpPointerTimer += Time.deltaTime;

                        if(helpPointerTimer >= helpPointerTextDisplay.Length * 0.045f)
                        {
                            if(helpPointerFadeOut > 0f)
                            {
                                helpPointerFadeOut -= Time.deltaTime / 0.5f;
                                helpPointerFadeOut = Mathf.Clamp01(helpPointerFadeOut);

                                foreach (CanvasRenderer r in helpPointerRenderers)
                                {
                                    r.SetAlpha(helpPointerFadeOut);
                                }
                            }
                            //Remove
                            else
                            {
                                currentHelpPointer = null;
                                helpPointerQueue.RemoveAt(0);
                                helpPointerRect.gameObject.SetActive(false);
                            }
                        }
                    }
                }
            }
            //Load next notification
            else if (currentNotification == null && notificationQueue.Count > 0 && SessionData.Instance.play && !PopupMessageController.Instance.active && !CutSceneController.Instance.cutSceneActive)
            {
                //Gather similar messages to merge...
                GameMessage current = notificationQueue[0];
                List<GameMessage> similarMessages = new List<GameMessage>();

                if(current.mergeType > -1)
                {
                    for (int i = 1; i < notificationQueue.Count; i++)
                    {
                        GameMessage next = notificationQueue[i];

                        if (next.mergeType == current.mergeType)
                        {
                            //Merge into this message
                            current.numerical += next.numerical;
                            similarMessages.Add(next);
                            notificationQueue.RemoveAt(i);
                            i--;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                //Create valeu add string
                string valueAdd = string.Empty;
                string extra = string.Empty;

                if(current.numerical != 0)
                {
                    string plus = "+";
                    if (current.numerical < 0) plus = string.Empty;
                    valueAdd = plus + current.numerical.ToString() + " ";
                }

                if(similarMessages.Count > 0)
                {
                    extra = "... x" + similarMessages.Count.ToString(); 
                }

                if(notificationQueue[0].keyMerge)
                {
                    currentNotification = Instantiate(PrefabControls.Instance.keyMergeGameMessage, gameMessageParent);
                }
                else if (notificationQueue[0].socCredit)
                {
                    currentNotification = Instantiate(PrefabControls.Instance.socialCreditGameMessage, gameMessageParent);
                }
                else
                {
                    currentNotification = Instantiate(PrefabControls.Instance.gameMessage, gameMessageParent);
                }

                currentNotification.GetComponent<GameMessageController>().Setup(notificationQueue[0].graphic, valueAdd + notificationQueue[0].message + extra, notificationQueue[0].moveOnDestroy, notificationQueue[0].colourOverride, notificationQueue[0].col, notificationQueue[0].ping, notificationQueue[0].keyMergeEvidence, notificationQueue[0].mergedKeys, notificationQueue[0].numerical);

                //Play sound
                AudioController.Instance.Play2DSound(AudioControls.Instance.gameMessage);

                if(notificationQueue[0].additionalSFX != null)
                {
                    AudioController.Instance.Play2DSound(notificationQueue[0].additionalSFX);
                }

                notificationQueue.RemoveAt(0);
            }
            //Load next game header
            else if (gameHeaderQueue.Count > 0 && SessionData.Instance.play && !PopupMessageController.Instance.active && !CutSceneController.Instance.cutSceneActive)
            {
                //Start a new message
                if(currentGameHeader == null)
                {
                    currentGameHeader = gameHeaderQueue[0];
                    gameHeaderDisplayed = false;
                    gameHeaderTimer = 2f;
                    titleTextRenderer.SetAlpha(1f);
                    gameHeaderFadeDelay = 1f;
                    titleText.text = string.Empty;
                    gameHeaderDelay = currentGameHeader.delay;
                    Game.Log("Interface: New game header: " + currentGameHeader.message);
                }

                if(gameHeaderDelay > 0f)
                {
                    gameHeaderDelay -= Time.deltaTime;
                }
                else
                {
                    //Typewriter reveal
                    if (!gameHeaderDisplayed)
                    {
                        if (typewriterDelay > 0f)
                        {
                            typewriterDelay -= Time.deltaTime;
                        }
                        else
                        {
                            int stringLength = titleText.text.Length;

                            if (currentGameHeader.message.Length > stringLength)
                            {
                                titleText.text = currentGameHeader.message.Substring(0, stringLength + 1);
                                typewriterDelay = 0.06f;

                                char nextLetter = titleText.text[titleText.text.Length - 1];

                                if (nextLetter == ' ')
                                {
                                    //Invoke("PlayTypewriterSpace", AudioControls.Instance.typewriterSpaceEventDelay);
                                }
                                else
                                {
                                    Invoke("PlayTypewriterKey", AudioControls.Instance.typewriterKeystrokeEventDelay);
                                }
                            }

                            if (titleText.text.Length >= currentGameHeader.message.Length)
                            {
                                Game.Log("Interface: Game header length " + titleText.text.Length + " >= " + currentGameHeader.message.Length);
                                gameHeaderDisplayed = true; //Header is now displayed
                            }
                        }
                    }
                    else if (gameHeaderDisplayed)
                    {
                        //Stop loop
                        if (typewriterSoundTriggered != null)
                        {
                            AudioController.Instance.StopSound(typewriterSoundTriggered, AudioController.StopType.fade, "Stop typewriter");
                            typewriterSoundTriggered = null;
                        }

                        if (gameHeaderTimer > 0f)
                        {
                            gameHeaderTimer -= Time.deltaTime;
                        }
                        else
                        {
                            //Fade out
                            if (gameHeaderFadeDelay > 0f)
                            {
                                gameHeaderFadeDelay -= Time.deltaTime * 3f;
                                gameHeaderFadeDelay = Mathf.Clamp01(gameHeaderFadeDelay);
                                titleTextRenderer.SetAlpha(gameHeaderFadeDelay);
                            }
                            else
                            {
                                gameHeaderQueue.RemoveAt(0);
                                currentGameHeader = null;
                                gameHeaderDisplayed = false;
                                titleText.text = string.Empty;
                            }
                        }
                    }
                }
            }
            //else if (Game.Instance.collectDebugData)
            //{
            //    if (currentNotification != null) Game.Log("gamemessage waiting on current notification");
            //    if (CutSceneController.Instance.cutSceneActive) Game.Log("gamemessage waiting on cut scene");
            //    if (!SessionData.Instance.play) Game.Log("gamemessage waiting on play");
            //}

            yield return null;
        }

        //Safety check: Stop loop
        if (typewriterSoundTriggered != null)
        {
            AudioController.Instance.StopSound(typewriterSoundTriggered, AudioController.StopType.fade, "Stop typewriter");
            typewriterSoundTriggered = null;
        }

        messageCoroutineRunning = false;
    }

    private void PlayTypewriterKey()
    {
        AudioController.Instance.Play2DSound(AudioControls.Instance.typewriter); //Play keystroke
    }

    private void PlayTypewriterSpace()
    {
        //AudioController.Instance.Play2DSound(AudioControls.Instance.typeWriterSpace); //Play space
    }

    public void ToggleNotebookButton()
    {
        ToggleNotebook();
    }

    public void ToggleNotebook(string startingPage = "", bool openHelpSection = false)
    {
        detectiveNotebook = activeWindows.Find(item => item.preset.name == "DetectivesNotebook");

        if(detectiveNotebook == null)
        {
            bool resumeOnClose = true;
            if (!SessionData.Instance.play) resumeOnClose = false;

            notebookButton.icon.enabled = true;

            SessionData.Instance.PauseGame(true);
            openHelpToPage = startingPage.ToLower();

            detectiveNotebook = SpawnWindow(null, presetName: "DetectivesNotebook", autoPosition: false, forcePosition: InterfaceControls.Instance.handbookWindowPosition, worldInteraction: resumeOnClose);

            if(openHelpSection)
            {
                try
                {
                    detectiveNotebook.SetActiveContent(detectiveNotebook.tabs[1].content);
                }
                catch
                {

                }
            }

            ////Add a small close promp to the X
            //PulseGlowController glow = detectiveNotebook.closeButton.gameObject.AddComponent<PulseGlowController>();
            //glow.originalColour = detectiveNotebook.closeButton.icon.color;
            //glow.imageToGlow = detectiveNotebook.closeButton.icon;
            //glow.lerpColour = new Color(1f, 0.85f, 0.85f);
            //glow.SetGlow(true);
            //glow.glowActive = true;

            detectiveNotebook.OnWindowClosed += ResetToggleNotebookButton;
        }
        else
        {
            notebookButton.icon.enabled = false;

            ResetToggleNotebookButton();
            detectiveNotebook.CloseWindow();

            if (!InterfaceController.Instance.desktopMode && !PopupMessageController.Instance.active && !MainMenuController.Instance.mainMenuActive)
            {
                SessionData.Instance.ResumeGame();
            }
        }
    }

    public void OpenNotebookNoPause(string startingPage = "", bool openHelpSection = false)
    {
        detectiveNotebook = activeWindows.Find(item => item.preset.name == "DetectivesNotebook");

        if (detectiveNotebook == null)
        {
            notebookButton.icon.enabled = true;
            openHelpToPage = startingPage.ToLower();
            detectiveNotebook = SpawnWindow(null, presetName: "DetectivesNotebook", autoPosition: false, forcePosition: InterfaceControls.Instance.handbookWindowPosition, worldInteraction: false);

            if (openHelpSection)
            {
                try
                {
                    detectiveNotebook.SetActiveContent(detectiveNotebook.tabs[1].content);
                }
                catch
                {

                }
            }

            detectiveNotebook.OnWindowClosed += ResetToggleNotebookButton;
        }
    }

    public void ResetToggleNotebookButton()
    {
        notebookButton.icon.enabled = false;
        detectiveNotebook.OnWindowClosed -= ResetToggleNotebookButton;
    }

    public void ToggleUpgrades()
    {
        if (upgradesCanvas == null) return;

        //If the desktop is open, open/close map
        if (!UpgradesController.Instance.isOpen)
        {
            UpgradesController.Instance.OpenUpgrades();
        }
        else
        {
            UpgradesController.Instance.CloseUpgrades();
        }
    }

    //Set desired fade level
    public void Fade(float fadeVal, float newFadeTime = 2f, bool newFadeAudio = false)
    {
        desiredFade = fadeVal;
        fadeTime = newFadeTime;
        fadeAudio = newFadeAudio;

        Game.Log("Interface: Fade to " + desiredFade + " with time " + fadeTime);

        StopCoroutine("FadeGame");
        StartCoroutine("FadeGame");
    }

    IEnumerator FadeGame()
    {
        if(fadeOverlay != null) fadeOverlay.gameObject.SetActive(true);
        desiredFade = Mathf.Clamp01(desiredFade);

        //Update depth of field
        UpdateDOF();

        while (desiredFade != fade)
        {
            if(!SessionData.Instance.play)
            {
                yield return null;
            }
            else
            {
                if (fade < desiredFade)
                {
                    fade += Time.deltaTime / fadeTime;
                }
                else if (fade > desiredFade)
                {
                    fade -= Time.deltaTime / fadeTime;
                }

                fade = Mathf.Clamp01(fade);

                if (fadeOverlay != null)
                {
                    float fa = fadeOverlayAlphaCurve.Evaluate(fade);
                    fadeOverlay.SetAlpha(fa);
                }

                //Vignette
                SessionData.Instance.vignette.active = true;
                SessionData.Instance.vignette.intensity.value = Mathf.Max(fade, StatusController.Instance.vignetteAmount);

                if (fadeAudio)
                {
                    //TODO: Do this later
                }
            }

            InteractionController.Instance.InteractionRaycastCheck();

            yield return null;
        }

        if (StatusController.Instance.vignetteAmount <= 0f)
        {
            SessionData.Instance.vignette.active = false;
        }

        if (fade <= 0f)
        {
            if (fadeOverlay != null) fadeOverlay.gameObject.SetActive(false);
            InteractionController.Instance.InteractionRaycastCheck();
        }

        //Update depth of field
        UpdateDOF();
    }

    IEnumerator DesktopModeTransition()
    {
        RectTransform canvasRect = caseCanvas.GetComponent<RectTransform>();
        RectTransform windowRect = windowCanvas.GetComponent<RectTransform>();

        bool setControlDisplay = false;

        //Reset camera rendering
        if(!desktopMode)
        {
            //To Enable the editor/prettier mode of this uncomment the following:
            //Destroy(CameraController.Instance.cam.targetTexture);
            //CameraController.Instance.cam.targetTexture = null;
            //InterfaceControls.Instance.cameraScreenshot.gameObject.SetActive(false);
            //CameraController.Instance.cam.enabled = true;

            //Workaround for build weird glitches when no camera present
            //CameraController.Instance.cam.cullingMask = savedCameraCulling;
            //CameraController.Instance.cam.clearFlags = savedCameraClear;
        }

        while(desktopModeTransition != desktopModeDesiredTransition)
        {
            if(desktopModeTransition < desktopModeDesiredTransition)
            {
                desktopModeTransition += ((desktopModeDesiredTransition - desktopModeTransition) * 5f + 1) * Time.deltaTime;
            }
            else if (desktopModeTransition > desktopModeDesiredTransition)
            {
                desktopModeTransition -= ((desktopModeTransition - desktopModeDesiredTransition) * 10f + 1) * Time.deltaTime;
            }

            desktopModeTransition = Mathf.Clamp01(desktopModeTransition);
            desktopModeDesiredTransition = Mathf.Clamp01(desktopModeDesiredTransition);

            float scaleFactor = Mathf.Max(desktopModeTransition * desktopModeTransition * desktopModeTransition, 0.2f);
            Vector3 scaleVector = new Vector3(scaleFactor, scaleFactor, scaleFactor);

            canvasRect.localScale = scaleVector;
            windowRect.localScale = scaleVector;

            caseCanvasGroup.alpha = desktopModeTransition;
            controlPanelCanvasGroup.alpha = desktopModeTransition;
            windowCanvasGroup.alpha = desktopModeTransition;
            upgradesCanvasGroup.alpha = desktopModeTransition;

            foreach (InfoWindow window in activeWindows)
            {
                window.windowCanvasGroup.alpha = desktopModeTransition;
                window.contentCanvasGroup.alpha = desktopModeTransition;
                window.RestoreAnchoredPosition();
            }

            //Fade out other UI elements
            //controlsCanvasGroup.alpha = 1f - desktopModeTransition;
            gameWorldCanvasGroup.alpha = 1f - desktopModeTransition;

            //Controls canvas: Fade out then in again with case controls
            if(desktopModeTransition <= 0.5f)
            {
                if (!setControlDisplay && !desktopMode)
                {
                    InteractionController.Instance.UpdateInteractionText();
                    setControlDisplay = true;
                }

                controlsCanvasGroup.alpha = 1f - desktopModeTransition * 2f;
            }
            else
            {
                if(!setControlDisplay && desktopMode)
                {
                    InteractionController.Instance.UpdateInteractionText();
                    setControlDisplay = true;
                }

                controlsCanvasGroup.alpha = (desktopModeTransition - 0.5f) * 2f;
            }

            //DOF blur
            SessionData.Instance.vignette.active = true;
            SessionData.Instance.vignette.intensity.value = Mathf.Lerp(0f, 0.5f, desktopModeTransition) + StatusController.Instance.vignetteAmount;

            if (desktopModeTransition <= 0f && desktopModeDesiredTransition <= 0f)
            {
                if(StatusController.Instance.vignetteAmount <= 0f) SessionData.Instance.vignette.active = false;
                controlPanelCanvas.gameObject.SetActive(false);
            }

            yield return null;
        }

        if(desktopMode)
        {
            //Enable corkboard physics
            foreach(PinnedItemController pinned in CasePanelController.Instance.spawnedPins)
            {
                if(pinned.rb != null) pinned.rb.simulated = true;
            }

            //To Enable the editor/prettier mode of this uncomment the following:
            //CameraController.Instance.cam.targetTexture = Instantiate(InterfaceControls.Instance.cameraScreenshotRenderTex); //Take a camera screenshot
            //InterfaceControls.Instance.cameraScreenshot.texture = CameraController.Instance.cam.targetTexture;
            //CameraController.Instance.cam.Render();
            //RenderTexture.active = CameraController.Instance.cam.targetTexture;
            //CameraController.Instance.cam.targetTexture = null;
            //InterfaceControls.Instance.cameraScreenshot.gameObject.SetActive(true); //Enable screenshot display
            //CameraController.Instance.cam.enabled = false; //Disable camera rendering

            //Workaround for build weird glitches when no camera present
            //savedCameraCulling = CameraController.Instance.cam.cullingMask;
            //savedCameraClear = CameraController.Instance.cam.clearFlags;
            //CameraController.Instance.cam.clearFlags = CameraClearFlags.Nothing;
            //CameraController.Instance.cam.cullingMask = 0;

            speechAnchor.gameObject.SetActive(false);
            objectCycleAnchor.gameObject.SetActive(false);
        }
        else
        {
            speechAnchor.gameObject.SetActive(true);
            objectCycleAnchor.gameObject.SetActive(!InteractionController.Instance.dialogMode);
        }

        InteractionController.Instance.UpdateInteractionText();
    }

    //Input a code through password button
    public void InputCodeButton(List<int> code)
    {
        //Fire event
        if(OnInputCode != null)
        {
            OnInputCode(code);
        }
    }

    public void AddMouseOverElement(MonoBehaviour mono)
    {
        if (mono == null) return;

        if (!currentMouseOverElement.Contains(mono))
        {
            Game.Log("Interface: Add mouse over: " + mono.name);
            currentMouseOverElement.Add(mono);
        }

        UpdateCursorSprite();
    }

    public void RemoveMouseOverElement(MonoBehaviour mono)
    {
        if (mono == null) return;

        Game.Log("Interface: Remove mouse over: " + mono.name);
        currentMouseOverElement.Remove(mono);
        UpdateCursorSprite();
    }

    public void ClearAllMouseOverElements()
    {
        Game.Log("Interface: Clearing mouse over elements");
        currentMouseOverElement.Clear();
        UpdateCursorSprite();
    }

    //Update the cursor mouse graphic
    public void UpdateCursorSprite()
    {
        //Game.Log("Menu: Update cursor sprite");

        if (EventSystem.current != null && EventSystem.current.IsPointerOverGameObject())
        {
            if(currentMouseOverElement.Count > 0)
            {
                MonoBehaviour currentElement = currentMouseOverElement[currentMouseOverElement.Count - 1];

                //Target cursor
                if(CasePanelController.Instance != null && (CasePanelController.Instance.customLinkSelectionMode || CasePanelController.Instance.pickModeActive) && !PopupMessageController.Instance.active)
                {
                    Cursor.SetCursor(InterfaceControls.Instance.cursorTarget, new Vector2(64, 64), CursorMode.Auto);
                    InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                    return;
                }

                //Move cursor
                if(currentElement as DragPanel != null)
                {
                    Cursor.SetCursor(InterfaceControls.Instance.cursorMove, new Vector2(64, 64), CursorMode.Auto);
                    InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                    return;
                }
                else if(currentElement as PinnedPinButtonController != null)
                {
                    Cursor.SetCursor(InterfaceControls.Instance.cursorMove, new Vector2(64, 64), CursorMode.Auto);
                    InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                    return;
                }
                else if (currentElement as PinnedItemController != null)
                {
                    Cursor.SetCursor(InterfaceControls.Instance.cursorMove, new Vector2(64, 64), CursorMode.Auto);
                    InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                    return;
                }
                else if(currentElement as CustomScrollRect != null && (currentElement as CustomScrollRect).isScrolling)
                {
                    Cursor.SetCursor(InterfaceControls.Instance.cursorMove, new Vector2(64, 64), CursorMode.Auto);
                    InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                    return;
                }
                //Resize cursor
                else if(currentElement as ResizePanel != null)
                {
                    ResizePanel rz = currentElement as ResizePanel;

                    if(rz.pivot.x == 0 && rz.pivot.y == 0)
                    {
                        Cursor.SetCursor(InterfaceControls.Instance.cursorResizeDiagonalRightLeft, new Vector2(64, 64), CursorMode.Auto);
                        InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                        return;
                    }
                    else if (rz.pivot.x == 1 && rz.pivot.y == 0)
                    {
                        Cursor.SetCursor(InterfaceControls.Instance.cursorResizeDiagonalLeftRight, new Vector2(64, 64), CursorMode.Auto);
                        InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                        return;
                    }
                    else if (rz.pivot.x == 0 && rz.pivot.y == 1)
                    {
                        Cursor.SetCursor(InterfaceControls.Instance.cursorResizeDiagonalLeftRight, new Vector2(64, 64), CursorMode.Auto);
                        InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                        return;
                    }
                    else if (rz.pivot.x == 1 && rz.pivot.y == 1)
                    {
                        Cursor.SetCursor(InterfaceControls.Instance.cursorResizeDiagonalRightLeft, new Vector2(64, 64), CursorMode.Auto);
                        InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                        return;
                    }
                    else if (rz.pivot.x == 0.5f)
                    {
                        Cursor.SetCursor(InterfaceControls.Instance.cursorResizeVertical, new Vector2(64, 64), CursorMode.Auto);
                        InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                        return;
                    }
                    else if (rz.pivot.y == 0.5f)
                    {
                        Cursor.SetCursor(InterfaceControls.Instance.cursorResizeHorizonal, new Vector2(64, 64), CursorMode.Auto);
                        InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                        return;
                    }
                }
                else if(currentElement as DragCoverage != null)
                {
                    Cursor.SetCursor(InterfaceControls.Instance.cursorResizeHorizonal, new Vector2(64, 64), CursorMode.Auto);
                    InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                    return;
                }
                //Button cursor
                else if (currentElement as ButtonController != null && (currentElement as ButtonController).interactable)
                {
                    Cursor.SetCursor(InterfaceControls.Instance.cursorButton, new Vector2(49, 15), CursorMode.Auto);
                    InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                    return;
                }
                else if(currentElement as ForceMouseOverInput != null)
                {
                    ForceMouseOverInput inp = currentElement as ForceMouseOverInput;

                    if(inp.cursorType == 0)
                    {
                        Cursor.SetCursor(InterfaceControls.Instance.cursorTextEdit, new Vector2(64, 64), CursorMode.Auto);
                        InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                        return;
                    }
                    else if(inp.cursorType == -1)
                    {
                        Cursor.SetCursor(InterfaceControls.Instance.normalCursor, new Vector2(10, 13), CursorMode.Auto);
                        InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                        return;
                    }
                }
                else if (currentElement as WindowRenameTitleController != null)
                {
                    WindowRenameTitleController wr = currentElement as WindowRenameTitleController;

                    Cursor.SetCursor(InterfaceControls.Instance.cursorTextEdit, new Vector2(64, 64), CursorMode.Auto);
                    InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
                    return;
                }
            }
        }

        try
        {
            Cursor.SetCursor(InterfaceControls.Instance.normalCursor, new Vector2(10, 13), CursorMode.Auto);
            InputController.Instance.SetCursorVisible(InputController.Instance.cursorVisible);
        }
        catch
        {

        }
    }

    //Minimize all evidence
    public void MinimizeAll()
    {
        int safety = 999;

        while(activeWindows.Count > 0 && safety > 0)
        {
            activeWindows[0].CloseWindow();
            safety--;

        }
    }

    public void ShowWindowFocus()
    {
        if(windowFocus != null && !windowFocus.gameObject.activeSelf)
        {
            Game.Log("Interface: Show window focus");
            windowFocus.gameObject.SetActive(true);
            windowFocus.SetAsLastSibling();
        }
    }

    //Remove the window focus object
    public void RemoveWindowFocus()
    {
        if (windowFocus != null)
        {
            Game.Log("Interface: Remove window focus");
            windowFocus.gameObject.SetActive(false);
        }
    }

    public void CrosshairReaction()
    {
        Instantiate(PrefabControls.Instance.crosshairReaction, InterfaceControls.Instance.lightOrbRect);
    }

    public Color GetEvidenceColour(InterfaceControls.EvidenceColours col)
    {
        return InterfaceControls.Instance.pinColours.Find(item => item.colour == col).actualColour;
    }

    public void PingLockpicks()
    {
        if(!lockpickNotificationActive)
        {
            StartCoroutine(ExecutePing(lockpicksNotificationIcon, lockpicksNotificationJuice, lockpicksNotificationText, lastLockpicks, lockpicksNotificationRenderers, false));
        }

        lastLockpicks = GameplayController.Instance.lockPicks;
    }

    public void PingMoney()
    {
        if(!moneyNotificationActive)
        {
            StartCoroutine(ExecutePing(moneyNotificationIcon, moneyNotificationJuice, moneyNotificationText, lastMoney, moneyNotificationRenderers, true));
        }

        lastMoney = GameplayController.Instance.money;
    }

    IEnumerator ExecutePing(RectTransform pingRect, JuiceController pingJuice, TextMeshProUGUI textPing, int originalValue, List<CanvasRenderer> renderers, bool isMoney)
    {
        float progress = 0f;
        pingRect.gameObject.SetActive(true);
        pingJuice.Flash(2, false);
        textPing.text = originalValue.ToString();
        string moneyStr = string.Empty;
        if (isMoney) moneyStr = CityControls.Instance.cityCurrency;

        foreach (CanvasRenderer rend in renderers)
        {
            rend.SetAlpha(1f);
        }

        while (progress < 2f)
        {
            int newPingToValue = GameplayController.Instance.money;
            if (!isMoney) newPingToValue = GameplayController.Instance.lockPicks;

            if(originalValue < newPingToValue)
            {
                originalValue += Mathf.CeilToInt(Time.deltaTime * 60f);

                if(originalValue + 100 < newPingToValue)
                {
                    originalValue += Mathf.CeilToInt(10 * Time.deltaTime * 60f);
                }

                originalValue = Mathf.Min(originalValue, newPingToValue);
                textPing.text = moneyStr + originalValue.ToString();
            }
            else if(originalValue > newPingToValue)
            {
                originalValue -= Mathf.CeilToInt(Time.deltaTime * 60f);

                if (originalValue - 100 > newPingToValue)
                {
                    originalValue -= Mathf.CeilToInt(10 * Time.deltaTime * 60f);
                }

                originalValue = Mathf.Max(originalValue, newPingToValue);
                textPing.text = moneyStr + originalValue.ToString();
            }
            else
            {
                progress += Time.deltaTime / 1f;

                if(progress > 1f)
                {
                    foreach(CanvasRenderer rend in renderers)
                    {
                        rend.SetAlpha(1f - (progress - 1f));
                    }
                }
            }

            yield return null;
        }

        pingRect.gameObject.SetActive(false);

        if (isMoney)
        {
            moneyNotificationActive = false;
        }
        else lockpickNotificationActive = false;
    }

    public void SetCrosshairVisible(bool val)
    {
        crosshairVisible = val;

        //If in pause mode, this is always invisible
        if (!SessionData.Instance.play) crosshairVisible = false;

        Game.Log("Player: Set crosshair visible: " + crosshairVisible);

        if(InterfaceControls.Instance.reticleContainer != null) InterfaceControls.Instance.reticleContainer.gameObject.SetActive(crosshairVisible);

        if(crosshairVisible)
        {
            //Set alpha
            if(InterfaceControls.Instance.seenRenderer != null) InterfaceControls.Instance.seenRenderer.SetAlpha(Player.Instance.seenIconLag);

            //Set orb to represent visibility
            if(InterfaceControls.Instance.lightOrbFillImg != null) InterfaceControls.Instance.lightOrbFillImg.canvasRenderer.SetAlpha(Player.Instance.visibilityLag * 0.9f);
        }
    }

    public void SetPlayerTextInput(bool val)
    {
        playerTextInputActive = val;
    }

    public void SetActiveCodeInput(KeypadController keypad)
    {
        activeCodeInput = keypad;

        //Fire event
        if (OnNewActiveCodeInput != null)
        {
            OnNewActiveCodeInput(keypad);
        }
    }

    //Set the objectives to be displayed for a short amount of time
    public void ActivateObjectivesDisplay()
    {
        objectivesDisplayTimer = 7f;

        //Display objectives for ages if in tutorial section
        int chapt = 0;

        if (Toolbox.Instance.IsStoryMissionActive(out _, out chapt))
        {
            if (chapt < 31)
            {
                objectivesDisplayTimer = 99999f;
            }
        }
    }

    [Button]
    public void NewMurderCaseDisplay()
    {
        Game.Log("Interface: New murder case display");
        StartCoroutine(DisplayMissionEndText(ScreenDisplayType.newMurderCase));
    }

    [Button]
    public void MissionCompleteDisplay()
    {
        ExecuteMissionCompleteDisplay(CasePanelController.Instance.activeCase);
    }

    [Button]
    public void ApartmentPurchaseDisplay()
    {
        Game.Log("Interface: New apartment purchased");
        StartCoroutine(DisplayMissionEndText(ScreenDisplayType.apartmentPurchase));
    }

    public void ExecuteMissionCompleteDisplay(Case forCase)
    {
        Game.Log("Interface: Mission complete display");
        StartCoroutine(DisplayMissionEndText(ScreenDisplayType.missionComplete, forCase));

        if(forCase.caseType == Case.CaseType.mainStory || forCase.caseType == Case.CaseType.murder)
        {
            GameplayController.Instance.AddSocialCredit(GameplayControls.Instance.socialCreditForMurders, true, "Mission complete debug");
        }
        else if(forCase.caseType == Case.CaseType.sideJob)
        {
            GameplayController.Instance.AddSocialCredit(GameplayControls.Instance.socialCreditForSideJobs, true, "Mission complete debug");
        }
    }

    [Button]
    public void SocialCreditLevelUpDisplay()
    {
        if(!levelUpScreenActive)
        {
            Game.Log("Interface: Social credit level up display");
            StartCoroutine(DisplayMissionEndText(ScreenDisplayType.socialCreditLevelUp));
        }
    }

    [Button]
    public void MissionFailedDisplay()
    {
        ExecuteMissionFailedDisplay(CasePanelController.Instance.activeCase);
    }

    public void ExecuteMissionFailedDisplay(Case forCase)
    {
        Game.Log("Interface: Mission failed display");
        StartCoroutine(DisplayMissionEndText(ScreenDisplayType.missionFailed, forCase));
    }

    [Button]
    public void UnsolvedDisplay()
    {
        ExecuteMissionUnsolvedDisplay(CasePanelController.Instance.activeCase);
    }

    public void ExecuteMissionUnsolvedDisplay(Case forCase)
    {
        Game.Log("Interface: Mission unsolved display");
        StartCoroutine(DisplayMissionEndText(ScreenDisplayType.unsolved, forCase));
    }

    public void ExecuteResolveDisplay(Case forCase)
    {
        Game.Log("Interface: Mission resolve display");
        StartCoroutine(DisplayMissionEndText(ScreenDisplayType.displayResolve, forCase));

    }

    [Button]
    public void DisplayCreditThresholdForLevel()
    {
        Game.Log(GameplayController.Instance.GetSocialCreditThresholdForLevel(debugLevel));
    }

    IEnumerator DisplayMissionEndText(ScreenDisplayType newType, Case forCase = null)
    {
        Game.Log("Displaying " + newType.ToString());

        if(newType == ScreenDisplayType.socialCreditLevelUp)
        {
            levelUpScreenActive = true;
        }

        while(gameSceenDisplayed || gameScreenQueued)
        {
            yield return null; //Wait for existing screen to finish
        }

        //if(newType != ScreenDisplayType.displayResolve) InterfaceControls.Instance.caseSolvedText.gameObject.SetActive(true);
        CanvasRenderer rend = InterfaceControls.Instance.caseSolvedText.canvasRenderer;

        float lastsFor = 5.5f;
        float timer = lastsFor;
        float progress = 0f;

        rend.SetAlpha(InterfaceControls.Instance.caseSolvedAlphaAnim.Evaluate(1f - progress));
        InterfaceControls.Instance.caseSolvedText.characterSpacing = InterfaceControls.Instance.caseSolvedKerningAnim.Evaluate(progress);
        gameSceenDisplayed = false;

        bool firstFrame = true;
        gameScreenQueued = true;

        while (timer > 0f)
        {
            //Only run this when no other onscreen messages are running
            if(!messageCoroutineRunning && currentNotification == null && !CutSceneController.Instance.cutSceneActive && Player.Instance.searchInteractable == null && !InteractionController.Instance.dialogMode && SessionData.Instance.play)
            {
                if(firstFrame)
                {
                    currentGameScreen = newType;

                    if (newType == ScreenDisplayType.missionComplete)
                    {
                        AudioController.Instance.Play2DSound(AudioControls.Instance.caseComplete);
                        InterfaceControls.Instance.caseSolvedText.text = Strings.Get("ui.gamemessage", "Case Completed");
                        MusicController.Instance.MusicTriggerCheck(MusicCue.MusicTriggerEvent.caseComplete);
                    }
                    else if (newType == ScreenDisplayType.missionFailed)
                    {
                        AudioController.Instance.Play2DSound(AudioControls.Instance.caseUnsolved);
                        InterfaceControls.Instance.caseSolvedText.text = "<color=#FF7575>" + Strings.Get("ui.gamemessage", "Job Failed");
                        MusicController.Instance.MusicTriggerCheck(MusicCue.MusicTriggerEvent.caseFailed);
                    }
                    else if (newType == ScreenDisplayType.unsolved)
                    {
                        AudioController.Instance.Play2DSound(AudioControls.Instance.caseUnsolved);
                        InterfaceControls.Instance.caseSolvedText.text = "<color=#FF7575>" + Strings.Get("ui.gamemessage", "Unsolved");
                        MusicController.Instance.MusicTriggerCheck(MusicCue.MusicTriggerEvent.caseUnsolved);
                    }
                    else if (newType == ScreenDisplayType.newMurderCase)
                    {
                        AudioController.Instance.Play2DSound(AudioControls.Instance.newMurderCase);
                        InterfaceControls.Instance.caseSolvedText.text = Strings.Get("ui.gamemessage", "New Murder Case");
                        MusicController.Instance.MusicTriggerCheck(MusicCue.MusicTriggerEvent.newMurderCase);
                    }
                    else if (newType == ScreenDisplayType.socialCreditLevelUp)
                    {
                        AudioController.Instance.Play2DSound(AudioControls.Instance.socialLevelUp);
                        InterfaceControls.Instance.caseSolvedText.text = Strings.Get("ui.gamemessage", "Social Credit Level Up") + ": " + GameplayController.Instance.GetCurrentSocialCreditLevel();
                        MusicController.Instance.MusicTriggerCheck(MusicCue.MusicTriggerEvent.socialCreditLevelUp);
                    }
                    else if(newType == ScreenDisplayType.displayResolve)
                    {
                        //Display resolve questions
                        InterfaceControls.Instance.resolveQuestionsDisplayParent.gameObject.SetActive(true);

                        AudioController.Instance.Play2DSound(AudioControls.Instance.revealCaseResults);

                        if (forCase != null)
                        {
                            for (int i = 0; i < forCase.resolveQuestions.Count; i++)
                            {
                                GameObject newQ = Instantiate(PrefabControls.Instance.revealQuestionObject, InterfaceControls.Instance.resolveQuestionsDisplayParent);
                                RevealResolveController cont = newQ.GetComponent<RevealResolveController>();
                                cont.Setup(forCase.resolveQuestions[i], forCase, i * 0.5f);
                            }
                        }

                        lastsFor = Mathf.Max(forCase.resolveQuestions.Count * 1.5f, 5.5f);
                        timer = lastsFor;
                        MusicController.Instance.MusicTriggerCheck(MusicCue.MusicTriggerEvent.resolveScreen);
                    }
                    else if (newType == ScreenDisplayType.apartmentPurchase)
                    {
                        AudioController.Instance.Play2DSound(AudioControls.Instance.socialLevelUp);
                        InterfaceControls.Instance.caseSolvedText.text = Strings.Get("ui.gamemessage", "New Apartment");
                        //MusicController.Instance.MusicTriggerCheck(MusicCue.MusicTriggerEvent.socialCreditLevelUp);
                    }

                    if (newType != ScreenDisplayType.displayResolve) InterfaceControls.Instance.caseSolvedText.gameObject.SetActive(true);

                    firstFrame = false;
                    gameSceenDisplayed = true;
                    gameScreenQueued = false;
                }

                timer -= Time.deltaTime;
                progress = (lastsFor - timer) / lastsFor;

                //Fade in/out other renderers
                if(progress <= 0.4f)
                {
                    foreach(CanvasRenderer r in InterfaceControls.Instance.screenMessageFadeRenderers)
                    {
                        r.SetAlpha(progress / 0.4f);
                    }
                }
                else if(progress >= 0.8f)
                {
                    foreach (CanvasRenderer r in InterfaceControls.Instance.screenMessageFadeRenderers)
                    {
                        r.SetAlpha(1f - ((progress - 0.8f) / 0.2f));
                    }
                }

                //Game.Log("Mission complete progress: " + progress);

                if(newType != ScreenDisplayType.displayResolve)
                {
                    rend.SetAlpha(InterfaceControls.Instance.caseSolvedAlphaAnim.Evaluate(1f - progress));
                    InterfaceControls.Instance.caseSolvedText.characterSpacing = InterfaceControls.Instance.caseSolvedKerningAnim.Evaluate(progress);
                }
            }
            else if(Game.Instance.collectDebugData)
            {
                //if (messageCoroutineRunning) Game.Log("display waiting on messageCoroutine");
                //if (currentNotification != null) Game.Log("display waiting on current notification");
                //if (CutSceneController.Instance.cutSceneActive) Game.Log("display waiting on cut scene");
                //if (Player.Instance.searchInteractable != null) Game.Log("display waiting on search interactable");
                //if (InteractionController.Instance.dialogMode) Game.Log("display waiting on dialog mode");
                //if (!SessionData.Instance.play) Game.Log("display waiting on play");
            }

            yield return null;
        }

        InterfaceControls.Instance.caseSolvedText.gameObject.SetActive(false);
        InterfaceControls.Instance.resolveQuestionsDisplayParent.gameObject.SetActive(false);

        if (newType == ScreenDisplayType.missionComplete)
        {
            NewGameMessage(GameMessageType.gameHeader, 0, Strings.Get("ui.gamemessage", "Case Closed", Strings.Casing.upperCase), InterfaceControls.Icon.agent);
        }
        else if (newType == ScreenDisplayType.unsolved)
        {
            NewGameMessage(GameMessageType.gameHeader, 0, Strings.Get("ui.gamemessage", "Case Unsolved", Strings.Casing.upperCase), InterfaceControls.Icon.agent, colourOverride: true, col: Color.red);
        }
        else if (newType == ScreenDisplayType.newMurderCase)
        {
            NewGameMessage(GameMessageType.gameHeader, 0, Strings.Get("ui.gamemessage", "Case Opened", Strings.Casing.upperCase), InterfaceControls.Icon.agent);
        }

        foreach (CanvasRenderer r in InterfaceControls.Instance.screenMessageFadeRenderers)
        {
            r.SetAlpha(0f);
        }

        gameSceenDisplayed = false;
        gameScreenQueued = false;

        if (newType == ScreenDisplayType.socialCreditLevelUp)
        {
            levelUpScreenActive = false;
        }
    }

    //Workaround for Unity version change in how/when it calls OnPointerExit (it now does it for all objects, even if they are a child)
    //If return false then return out of a OnPointerExit function (this mimics the old behaviour)
    //NOTE: They've possibly reverted this AGAIN in latest versions- be prepared to remove the if statement below if we update again...
    public bool StupidUnityChangeToTheWayOnPointerExitHandles(PointerEventData eventData, Transform t)
    {
        if (eventData != null)
        {
            if (eventData.pointerCurrentRaycast.gameObject != null)
            {
                if (eventData.pointerCurrentRaycast.gameObject.transform.IsChildOf(t))
                {
                    return false;
                }
            }
        }

        return true;
    }
}
