﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Generates blocks within the city
//Script pass 1
public class BlocksCreator : Creator
{
    //Chunks to load per frame
    public int loadChunk = 10;

    //Singleton pattern
    private static BlocksCreator _instance;
    public static BlocksCreator Instance { get { return _instance; } }

	void Awake ()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
	}

	public override void StartLoading()
	{
        Game.Log("CityGen: Setup city blocks...");
        StartCoroutine("Blocks");
	}

	IEnumerator Blocks ()
	{
        //Copy of a list of city tiles to pick randomly from
        List<CityTile> randomList = new List<CityTile>();
        List<CityTile> borderTiles = new List<CityTile>();
        string seed = CityData.Instance.seed;

        foreach (KeyValuePair<Vector2Int, CityTile> pair in CityData.Instance.cityTiles)
        {
            //Create border tile list and interior list
            if(pair.Key.x == 0 || pair.Key.x == CityData.Instance.citySize.x - 1 || pair.Key.y == 0 || pair.Key.y == CityData.Instance.citySize.y - 1)
            {
                borderTiles.Add(pair.Value);
            }
            else
            {
                randomList.Add(pair.Value);
            }
        }

        //Monitor progress
        float cityBlocksTotal = randomList.Count;
        float cityBlocksProgress = 0f;

        //Create border block & add border tiles
        GameObject newBoundary = Instantiate(PrefabControls.Instance.block, PrefabControls.Instance.cityContainer.transform);
        CityData.Instance.borderBlock = newBoundary.GetComponent<BlockController>();
        CityData.Instance.borderBlock.Setup(borderTiles[0].district);

        foreach(CityTile tile in borderTiles)
        {
            //Add this tile
            CityData.Instance.borderBlock.AddCityTile(tile);
        }

        //Calculate x blocks every frame
        while (randomList.Count > 0)
        {
            //Cycle through the list of coordinates, each time choose a random entry, load the coords and then delete that entry.
            for (int u = 0; u < loadChunk; u++)
            {
                if (randomList.Count <= 0) break;

                //If the tile has already been assigned a block, skip it.
                CityTile randomTile = randomList[Toolbox.Instance.GetPsuedoRandomNumberContained(0, randomList.Count, seed, out seed)];

                //Remove the entry
                randomList.Remove(randomTile);

                //If the tile has already been assigned a block, skip it.
                if (randomTile.block != null)
                {
                    continue;
                }

                //There's going to be a new block here regardless.
                GameObject newBlockObj = Instantiate(PrefabControls.Instance.block, PrefabControls.Instance.cityContainer.transform);
                BlockController newBlock = newBlockObj.GetComponent<BlockController>();
                newBlock.Setup(randomTile.district);

                //Add this tile
                newBlock.AddCityTile(randomTile);

                //List used to load expanded tiles to check
                List<CityTile> expansionCheck = new List<CityTile>();

                //Add this tile to check it and begin the loop
                expansionCheck.Add(randomTile);

                //Used to make sure block size is within bounds
                bool sizeCheck = true;

                //While there are tiles to check, keep this looping to expand block
                int loopFailSafe = 9999;

                while (expansionCheck.Count > 0 && sizeCheck && loopFailSafe > 0)
                {
                    CityTile current = expansionCheck[0];

                    //Calculate distance from centre- used to modify chance to expand.
                    float dist = Vector2.Distance(current.cityCoord, new Vector2(CityData.Instance.citySize.x * 0.5f, CityData.Instance.citySize.y * 0.5f));

                    //Make a list of tiles that can be expanded to.
                    foreach (Vector2Int v in CityData.Instance.offsetArrayX4)
                    {
                        //If block has reached size limit, break out
                        if (newBlock.cityTiles.Count >= CityControls.Instance.maxBlockSize)
                        {
                            sizeCheck = false;
                        }

                        CityTile adjacentTile = null;

                        if (CityData.Instance.cityTiles.TryGetValue(new Vector2Int(current.cityCoord.x + v.x, current.cityCoord.y + v.y), out adjacentTile))
                        {
                            //Check this isn't a border
                            if(borderTiles.Contains(adjacentTile))
                            {
                                continue;
                            }

                            //Check for same district
                            if (adjacentTile.district == current.district)
                            {
                                //Chance to expand:
                                //Base number (default 100)
                                float citySize = Mathf.Min(CityData.Instance.citySize.x, CityData.Instance.citySize.y);
                                float distMP = (dist / (citySize * 0.5f)) * CityControls.Instance.blockExpandCentreMultiplier;

                                //Set favoured expansion direction
                                float favMP = CityControls.Instance.nonFavouredExpandMultiplier;
                                //0 = favour horizontal, 1= favour vertical
                                if (adjacentTile.cityCoord.x != current.cityCoord.x && newBlock.favourVertical == 0) favMP = 1f;
                                if (adjacentTile.cityCoord.y != current.cityCoord.y && newBlock.favourVertical == 1) favMP = 1f;

                                if (Toolbox.Instance.GetPsuedoRandomNumber(0, (100 * citySize) * favMP, adjacentTile.district.seed + current.district.seed) < (CityControls.Instance.blockExpandChance * citySize) * distMP)
                                {
                                    //Add to block
                                    newBlock.AddCityTile(adjacentTile);

                                    //Add to check list
                                    expansionCheck.Add(adjacentTile);
                                }
                            }
                        }
                    }

                    //Remove top entry
                    expansionCheck.RemoveAt(0);

                    if (loopFailSafe <= 0)
                    {
                        Game.LogError("Loop failsafe triggered!");
                        break;
                    }

                    loopFailSafe--;
                }

                //+1 to progress
                cityBlocksProgress += 1f;

                //Remove the entry
                randomList.Remove(randomTile);
            }

            //Calculate ~progress
            CityConstructor.Instance.loadingProgress = cityBlocksProgress / cityBlocksTotal;

            yield return null;
        }

        //Safety
        //1: Check all tiles have a block assigned, if not create a new single block.
        for (int i = 0; i < CityData.Instance.citySize.x; i++)
        {
            for (int n = 0; n < CityData.Instance.citySize.y; n++)
            {
                CityTile thisTile = null;

                if (CityData.Instance.cityTiles.TryGetValue(new Vector2Int(i, n), out thisTile))
                {
                    if (thisTile.block == null)
                    {
                        //Create new block
                        GameObject newBlockObj = Instantiate(PrefabControls.Instance.block, PrefabControls.Instance.cityContainer.transform);
                        BlockController newBlock = newBlockObj.GetComponent<BlockController>();
                        newBlock.Setup(thisTile.district);

                        //Add this tile
                        newBlock.AddCityTile(thisTile);
                    }
                }
            }
        }

        Game.Log("CityGen: " + CityData.Instance.blocksDirectory.Count + " blocks created...");

        //Complete!
        SetComplete();
	}
}
