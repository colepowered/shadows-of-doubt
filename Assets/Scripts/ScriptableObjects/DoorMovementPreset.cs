﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "doormovement_data", menuName = "Database/Door Movement Preset")]

public class DoorMovementPreset : SoCustomComparison
{
    [Header("Relative State Positions")]
    public Vector3 closedRelativePos = Vector3.zero;
    public Vector3 openRelativePos = Vector3.zero;
    [Space(5)]
    public Vector3 closedRelativeEuler = Vector3.zero;
    public Vector3 openRelativeEuler = Vector3.zero;
    [Space(5)]
    public Vector3 closedRelativeScale = Vector3.zero;
    public Vector3 openRelativeScale = Vector3.zero;

    [Header("State Movement")]
    [Tooltip("How fast the door opens")]
    public float doorOpenSpeed = 1f;
    [Tooltip("How fast the door closes")]
    public float doorCloseSpeed = 1f;
    public AnimationCurve animationCurve = new AnimationCurve();

    [Header("Physics")]
    public PhysicsBehaviour collisionBehaviour = PhysicsBehaviour.ignore;
    public bool behaviourAppliesWhenOpening = true;
    public bool behaviourAppliesWhenClosing = true;

    public enum PhysicsBehaviour { ignore, physicsEnabled, stopDoorMovement}

    [Header("Audio")]
    public AudioEvent openAction;
    public AudioEvent closeAction;
    public AudioEvent openFinished;
    public AudioEvent closeFinished;
    public AudioEvent objectImpact;
    [Tooltip("If this is true then occlusion won't be calculated.")]
    public bool ignoreOcclusion = false;
    [Tooltip("If true then switch state 1 will be active while animating")]
    public bool switchState1AnimationSync = false;
}
