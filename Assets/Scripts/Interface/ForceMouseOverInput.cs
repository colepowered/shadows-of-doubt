﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ForceMouseOverInput : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public int cursorType = 0;
    public bool mouseOver = false;

    public void OnPointerEnter(PointerEventData eventData)
    {
        InterfaceController.Instance.AddMouseOverElement(this);
        mouseOver = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;

        InterfaceController.Instance.RemoveMouseOverElement(this);
        mouseOver = false;
    }

    void OnDestroy()
    {
        InterfaceController.Instance.RemoveMouseOverElement(this);
    }

    void OnDisable()
    {
        if (InterfaceController.Instance != null)
        {
            InterfaceController.Instance.RemoveMouseOverElement(this);
            mouseOver = false;
        }
    }
}
