﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeechController : MonoBehaviour
{
    public Actor actor;
    [System.NonSerialized]
    public Interactable interactable;
    public Telephone phoneLine;

    [Header("Speech")]
    public SpeechBubbleController activeSpeechBubble;
    public bool endAfterThisSpeech = false;
    public float lastSpeech = 0f;
    public List<QueueElement> speechQueue = new List<QueueElement>();
    public float speechDelay = 0f;
    public bool speechActive = false;

    [System.Serializable]
    public class QueueElement
    {
        public string dictRef;
        public string entryRef;
        public bool useParsing = true;
        public float delay = 0f;
        public bool delayActivated = false;
        public bool shouting = false;
        public bool interupt = false;
        public bool forceColour = false;
        public Color color;
        public int speakingAbout = -1;
        public int jobRef = -1;
        public bool endsDialog = false;
        public bool jobHandIn = false;
        public int speakingToRef = -1;
        public AIActionPreset.AISpeechPreset dialog = null;
        public string dialogPreset;

        public bool isObjective = false;
        public bool usePointer = false;
        public Vector3 pointerPosition;
        public List<Objective.ObjectiveTrigger> triggers;
        public Objective.OnCompleteAction onComplete;
        public bool removePreviousObjectives = false;
        public string chapterString;
        public bool isSilent = false;
        public bool allowCrouchPrompt = false;
        public InterfaceControls.Icon icon = InterfaceControls.Icon.lookingGlass;
        public int caseID = -1;
        public bool forceBottom = false;

        //Construct speech
        public QueueElement(string newDictRef, string newEntryRef, bool newUseParsing, float newDelay, bool newIsShouting, bool newInterupt, bool newForceColour = false, Color newColor = new Color(), Human newSpeakingAbout = null, bool newEndsDialog = false, bool newJobHandIn = false, SideJob newJobRef = null, DialogPreset newDialogPreset = null, AIActionPreset.AISpeechPreset newDialog = null, Interactable newSpeakingTo = null)
        {
            dictRef = newDictRef;
            entryRef = newEntryRef;
            useParsing = newUseParsing;
            delay = newDelay;
            shouting = newIsShouting;
            isObjective = false;
            interupt = newInterupt;
            forceColour = newForceColour;
            color = newColor;
            endsDialog = newEndsDialog;
            jobHandIn = newJobHandIn;
            dialog = newDialog;
            if(newJobRef != null) jobRef = newJobRef.jobID;
            if(newSpeakingAbout != null) speakingAbout = newSpeakingAbout.humanID;
            if (newSpeakingTo != null) speakingToRef = newSpeakingTo.id;
            if (newDialogPreset != null) dialogPreset = newDialogPreset.name;
        }

        //Construct objective
        public QueueElement(int newCaseID, string newName, bool newUseUIPointer, Vector3 newUseUIPosition, InterfaceControls.Icon newIcon, List<Objective.ObjectiveTrigger> newTriggers, Objective.OnCompleteAction newOnCompleteAction, float newDelay = 0f, bool newRemoveObjectives = false, string newChapterString = "", bool newIsSilent = false, bool newAllowCrouchPromt = false, SideJob newJobRef = null, bool newForceBottom = false, bool newUseParsing = true)
        {
            entryRef = newName;
            caseID = newCaseID;
            isObjective = true;
            useParsing = newUseParsing;
            forceBottom = newForceBottom;

            if (newJobRef != null)
            {
                jobRef = newJobRef.jobID;
            }

            usePointer = newUseUIPointer;
            pointerPosition = newUseUIPosition;
            delay = newDelay;
            triggers = newTriggers;
            onComplete = newOnCompleteAction;
            removePreviousObjectives = newRemoveObjectives;
            chapterString = newChapterString;
            isSilent = newIsSilent;
            allowCrouchPrompt = newAllowCrouchPromt;
            icon = newIcon;
        }
    }

    public enum Bark { persuit, lostTarget, answeringDoor, answerDoor, giveUpSearch, hearsSuspicious, seesSuspicious, enforcerRadio, idleSounds, discoverTamper, fallOffChair, sleeping, yawn, hearsObject, stench, seeBody, examineBody, mourn, enforcersKnock, scared, cower, attack, confrontMessingAround, pickUpMisplaced, takeDamage, frustration, outOfBreath, cold, drunkIdle, targetDown, restrained, restrainedIdle, dazed, trespass, threatenByItem, threatenByCombat, soundAlarm, doorBlocked, spooked, exposedConfront, spookConfront };

    //Trigger bark
    public virtual void TriggerBark(Bark newBark)
    {
        if (actor.isDead || actor.isStunned) return;
        if (InterfaceController.Instance.activeSpeechBubbles.Count >= CitizenControls.Instance.maxSpeechBubbles) return;
        if (actor.inConversation) return;
        if (actor.interactable != null && InteractionController.Instance.talkingTo == actor.interactable) return;

        if (newBark == Bark.persuit)
        {
            Speak("f1d8d284-cee7-4243-b77e-342c698029a2", interupt: true);
        }
        else if(newBark == Bark.lostTarget)
        {
            Speak("fbe69a11-2a9b-4c6b-b979-6637b66f7bb9", interupt: true);
        }
        else if(newBark == Bark.answeringDoor)
        {
            Speak("1f677cec-70a1-4c97-9000-131135807b31", shout: true, interupt: true);
        }
        else if(newBark == Bark.answerDoor)
        {
            if(actor.currentGameLocation != null && actor.currentGameLocation.thisAsAddress != null && actor.currentGameLocation.thisAsAddress.addressPreset != null && actor.currentGameLocation.thisAsAddress.addressPreset.needsPassword)
            {
                if(!GameplayController.Instance.playerKnowsPasswords.Contains(actor.currentGameLocation.thisAsAddress.id))
                {
                    Speak("279cee3f-6799-4e7a-a93a-3e1c638e808c", interupt: true); //Ask for the password
                }
                else Speak("d73fa4d4-6e57-46d0-8cd2-1f27dd84e541", interupt: true);
            }
            else Speak("d73fa4d4-6e57-46d0-8cd2-1f27dd84e541", interupt: true);
        }
        else if(newBark == Bark.giveUpSearch)
        {
            Speak("e0dd615f-6c72-45da-97ab-b7817bf11609", interupt: true);
        }
        else if(newBark == Bark.hearsSuspicious)
        {
            Speak("32b64e58-660f-4a9c-be30-961233a8bf03", interupt: true);
        }
        else if(newBark == Bark.seesSuspicious)
        {
            Speak("d29bbe40-437f-4ddf-adc3-84fd33aa7c30", interupt: true);
        }
        else if(newBark == Bark.enforcerRadio)
        {
            Speak("4249d7a9-3d34-4fe0-96ae-3ab8d348ab3b", interupt: true);
        }
        else if(newBark == Bark.idleSounds)
        {
            Speak("18b75486-cee4-4d52-ae06-dbdbacdd531b");
        }
        else if(newBark == Bark.discoverTamper)
        {
            Speak("a7e07cb7-4b4b-46d3-99fc-ff1adb044913", interupt: true);
        }
        else if(newBark == Bark.fallOffChair)
        {
            Speak("7df85447-902e-4f41-9691-76d0edd318a5", interupt: true);
        }
        else if(newBark == Bark.sleeping)
        {
            Speak("23719aae-b101-49f8-994f-ec7759d795c8");
        }
        else if(newBark == Bark.yawn)
        {
            Speak("1a5c991a-e4e0-4e05-a436-122395774555", interupt: true);
        }
        else if(newBark == Bark.hearsObject)
        {
            Speak("28fe0ce5-60db-4c17-8103-88fcb0a41919", interupt: true);
        }
        else if (newBark == Bark.stench)
        {
            Speak("31f09ad7-0274-4b95-be11-29f9c1680da9", interupt: true);
        }
        else if (newBark == Bark.seeBody)
        {
            Speak("4dcf2086-e959-4c70-9f32-7f4ba55f63c8", interupt: true);
        }
        else if (newBark == Bark.examineBody)
        {
            Speak("84b48e9d-2db5-4000-86c1-77100d9635b3", interupt: true);
        }
        else if (newBark == Bark.mourn)
        {
            Speak("fabeec40-1090-4a41-8600-cfab8a4a8bb2", interupt: true);
        }
        else if (newBark == Bark.enforcersKnock)
        {
            Speak("8d0dc67c-b8d4-466d-a827-585aa77a9455", interupt: true, shout: true);
        }
        else if (newBark == Bark.scared)
        {
            Speak("a05f9f17-143d-411e-a16c-af7f4181f29f", interupt: true);
        }
        else if (newBark == Bark.attack)
        {
            Speak("88814142-7e3a-4452-9f36-06a2fa853c21", interupt: true);
        }
        else if (newBark == Bark.confrontMessingAround)
        {
            Speak("84b9a4a2-7ff9-4a5e-a624-94d8cf7e4ffb", interupt: true);
        }
        else if (newBark == Bark.pickUpMisplaced)
        {
            Speak("246df5c6-c562-424a-aa10-519b15ebd395", interupt: true);
        }
        else if (newBark == Bark.takeDamage)
        {
            Speak("70ee1e8a-a41f-4265-86ce-0bb24f561360", interupt: true);
        }
        else if (newBark == Bark.frustration)
        {
            Speak("6bc04ea6-e946-4f8e-aa8e-ec4cb99a69f6", interupt: true);
        }
        else if (newBark == Bark.outOfBreath)
        {
            Speak("8448cf9f-ad0c-4e71-aa05-3e878cc5d32a");
        }
        else if (newBark == Bark.cold)
        {
            Speak("d748cbac-ea16-46bf-9c46-153a0c73a01f");
        }
        else if (newBark == Bark.drunkIdle)
        {
            Speak("f17c6437-b71c-4745-a5d2-67ae78a5596e");
        }
        else if (newBark == Bark.targetDown)
        {
            Speak("989c6280-a58b-46cb-9392-5fa683489463");
        }
        else if (newBark == Bark.restrained)
        {
            Speak("7e54efc0-9a06-4ea5-9d52-6993cdd67a60");
        }
        else if (newBark == Bark.restrainedIdle)
        {
            Speak("d086041a-3048-4614-baff-a3d5550a4099");
        }
        else if (newBark == Bark.dazed)
        {
            Speak("42e04710-ce12-489c-afd9-a0954b909876");
        }
        else if (newBark == Bark.trespass)
        {
            Speak("ad2123f9-d1cb-479a-b6c4-fd3cca346f63");
        }
        else if (newBark == Bark.threatenByCombat)
        {
            Speak("6e8d2443-ba0c-4f81-8075-31e4bdbd8d84", interupt: true);
        }
        else if (newBark == Bark.threatenByItem)
        {
            Speak("6e011e82-dfdc-4abe-b2fc-1f776bdc89da", interupt: true);
        }
        else if (newBark == Bark.soundAlarm)
        {
            Speak("c37dd664-29b2-40b4-a8bf-6748ee4e05f6", interupt: true);
        }
        else if (newBark == Bark.doorBlocked)
        {
            Speak("7aba83bc-9782-4d8d-87a6-b5d2dfa8b997", interupt: true);
        }
        else if (newBark == Bark.spooked)
        {
            Speak("5a75ed21-287a-4592-a419-a3cb279e292f", interupt: true);
        }
        else if (newBark == Bark.exposedConfront)
        {
            Speak("df8e6c66-5907-43cb-a9c5-f1f63d39e199", interupt: true);
        }
        else if (newBark == Bark.spookConfront)
        {
            Speak("75cd1949-c792-45b0-8eed-664d5a47f0cf", interupt: true);
        }
    }

    //Say something
    public virtual void Speak(ref List<AIActionPreset.AISpeechPreset> speechOptions, Human speakAbout = null, SideJob sideJob = null, DialogPreset dialogPreset = null, Interactable saysTo = null)
    {
        if (speechOptions.Count <= 0) Game.LogError("Speech has 0 options");

        if (CutSceneController.Instance.cutSceneActive)
        {
            if (CutSceneController.Instance.preset.disableAISpeech)
            {
                if (!actor.isPlayer) return;
            }
        }

        List<AIActionPreset.AISpeechPreset> randomList = new List<AIActionPreset.AISpeechPreset>();

        foreach (AIActionPreset.AISpeechPreset preset in speechOptions)
        {
            //Must feature trait
            if (preset.mustFeatureTrait.Count > 0)
            {
                Human hu = actor as Human;

                if (hu != null)
                {
                    bool pass = false;

                    foreach (CharacterTrait t in preset.mustFeatureTrait)
                    {
                        if (hu.characterTraits.Exists(item => item.trait == t))
                        {
                            pass = true;
                            break;
                        }
                    }

                    if (!pass) continue;
                }
                else continue;
            }

            //Can't feature trait
            if (preset.cantFeatureTrait.Count > 0)
            {
                Human hu = actor as Human;

                if (hu != null)
                {
                    bool pass = true;

                    foreach (CharacterTrait t in preset.cantFeatureTrait)
                    {
                        if (hu.characterTraits.Exists(item => item.trait == t))
                        {
                            pass = false;
                            break;
                        }
                    }

                    if (!pass) continue;
                }
                else continue;
            }

            if(preset.onlyIfEnfocerOnDuty && (!actor.isEnforcer || !actor.isOnDuty))
            {
                continue;
            }
            else if(preset.onlyIfEnfocerOnDuty && actor.isEnforcer && actor.isOnDuty)
            {
                continue;
            }

            if(preset.mustBeKillerWithMotive.Count > 0)
            {
                //I am the killer
                if (MurderController.Instance.currentMurderer == actor)
                {
                    if (!preset.mustBeKillerWithMotive.Contains(MurderController.Instance.chosenMO))
                    {
                        continue;
                    }
                }
                else continue;
            }

            for (int i = 0; i < preset.chance; i++)
            {
                randomList.Add(preset);
            }
        }

        if (randomList.Count > 0)
        {
            AIActionPreset.AISpeechPreset chosen = randomList[Toolbox.Instance.Rand(0, randomList.Count)];

            //Tie keys...
            if(actor != null && actor.evidenceEntry != null)
            {
                if(chosen.tieKeys.Count > 0)
                {
                    foreach (Evidence.DataKey key in chosen.tieKeys)
                    {
                        Game.Log("Debug: Merging key " + key.ToString() + "...");

                        foreach (Evidence.DataKey key2 in chosen.tieKeys)
                        {
                            if (key == key2) continue;
                            actor.evidenceEntry.MergeDataKeys(key, key2);
                        }
                    }
                }

                //Discovery triggers...
                foreach (Evidence.Discovery trigger in chosen.applyDiscovery)
                {
                    actor.evidenceEntry.AddDiscovery(trigger);
                }
            }

            //Use DDS if possible, fall back to speech dictionary...
            Human h = actor as Human;
            if (h == null) h = Player.Instance as Human;

            if (chosen != null && chosen.ddsMessageID != null && chosen.ddsMessageID.Length > 0)
            {
                if(Toolbox.Instance.allDDSMessages.ContainsKey(chosen.ddsMessageID))
                {
                    //Get relationship of talking to...
                    Acquaintance aq = null;

                    h.FindAcquaintanceExists(Player.Instance, out aq);

                    //Gather list of things to say from this message
                    List<string> say = h.ParseDDSMessage(chosen.ddsMessageID, aq, out _, forceRealRandom: true, passedObject: sideJob);

                    bool endsDialog = false;
                    bool jobHandIn = false;

                    //We can add all these to the citizen's speak queue at once
                    for (int i = 0; i < say.Count; i++)
                    {
                        //Last entry ends dialog
                        if(chosen.endsDialog && i >= say.Count - 1)
                        {
                            endsDialog = true;
                        }

                        if(chosen.jobHandIn && i >= say.Count - 1)
                        {
                            jobHandIn = true;
                        }

                        Speak("dds.blocks", say[i], true, shout: chosen.shout, interupt: chosen.interupt, speakingAbout: speakAbout, endsDialog: endsDialog, jobHandIn: jobHandIn, sideJob: sideJob, dialogPreset: dialogPreset, dialog: chosen, speakingTo: saysTo);

                        //Start combat
                        if (chosen.startCombat && i >= say.Count - 1 && h.ai != null)
                        {
                            Game.Log("Setting combat persuit from dialog option...");
                            h.ai.SetPersue(Player.Instance, false, 2, true);
                        }

                        //Start flee
                        if (chosen.flee && i >= say.Count - 1 && h.ai != null)
                        {
                            Game.Log("Setting flee from dialog option...");
                            h.AddNerve(-9999999, Player.Instance);
                        }

                        //Start give up self
                        if (chosen.giveUpSelf && i >= say.Count - 1 && h.ai != null)
                        {
                            Game.Log("Setting give up self from dialog option...");
                            h.ai.CreateNewGoal(RoutineControls.Instance.giveSelfUp, 0f, 0f);
                        }
                    }
                }
                else
                {
                    Game.LogError("Missing DDS message: " + chosen.ddsMessageID);
                }
            }
            else
            {
                Game.LogError("No DDS message for speech... [" + chosen.dictionaryString + "]");
                //Speak("witness.speech", chosen.dictionaryString, chosen.useParsing, chosen.shout, interupt: chosen.interupt, speakingAbout: speakAbout, endsDialog: chosen.endsDialog, jobHandIn: chosen.jobHandIn, sideJob: sideJob);
            }
        }
    }

    public virtual void Speak(string ddsMessage, bool shout = false, bool interupt = false, Human speakAbout = null, SideJob sideJob = null)
    {
        if (CutSceneController.Instance.cutSceneActive)
        {
            if (CutSceneController.Instance.preset.disableAISpeech)
            {
                if (!actor.isPlayer) return;
            }
        }

        if (ddsMessage != null && ddsMessage.Length > 0)
        {
            if (Toolbox.Instance.allDDSMessages.ContainsKey(ddsMessage))
            {
                //Get relationship of talking to...
                Acquaintance aq = null;
                List<string> say = null;

                Human h = actor as Human;

                if (h != null)
                {
                    h.FindAcquaintanceExists(Player.Instance, out aq);

                    //Gather list of things to say from this message
                    say = h.ParseDDSMessage(ddsMessage, aq, out _, forceRealRandom: true, passedObject: sideJob);
                }
                else
                {
                    say = Player.Instance.ParseDDSMessage(ddsMessage, null, out _, forceRealRandom: true, passedObject: sideJob);
                }

                bool endsDialog = false;
                bool jobHandIn = false;

                //We can add all these to the citizen's speak queue at once
                for (int i = 0; i < say.Count; i++)
                {
                    Speak("dds.blocks", say[i], true, shout: shout, interupt: interupt, speakingAbout: speakAbout, endsDialog: endsDialog, jobHandIn: jobHandIn, sideJob: sideJob);
                }
            }
            else
            {
                Game.LogError("Missing DDS message: " + ddsMessage);
            }
        }
        else
        {
            Game.LogError("No DDS message for speech...");
            //Speak("witness.speech", chosen.dictionaryString, chosen.useParsing, chosen.shout, interupt: chosen.interupt, speakingAbout: speakAbout, endsDialog: chosen.endsDialog, jobHandIn: chosen.jobHandIn, sideJob: sideJob);
        }
    }

    //The below should not be being used with the new DDS system
    public virtual void Speak(string dictionary, string speechEntryRef, bool useParsing = false, bool shout = false, bool interupt = false, float delay = 0f, bool forceColour = false, Color color = new Color(), Human speakingAbout = null, bool endsDialog = false, bool jobHandIn = false, SideJob sideJob = null, DialogPreset dialogPreset = null, AIActionPreset.AISpeechPreset dialog = null, Interactable speakingTo = null)
    {
        //Must be in ground tile vicinity
        if(actor != null)
        {
            if (!actor.isPlayer && (actor.currentCityTile == null || !actor.currentCityTile.isInPlayerVicinity))
            {
                return;
            }

            if (CutSceneController.Instance.cutSceneActive)
            {
                if (CutSceneController.Instance.preset.disableAISpeech)
                {
                    if (!actor.isPlayer) return;
                }
            }

            if (Game.Instance.collectDebugData) actor.SelectedDebug("Speech: " + actor.name + ": " + Strings.Get(dictionary, speechEntryRef), Actor.HumanDebug.actions);
        }

        if (!SessionData.Instance.startedGame) return;

        speechQueue.Add(new QueueElement(dictionary, speechEntryRef, useParsing, delay, shout, interupt, forceColour, color, speakingAbout, endsDialog, jobHandIn, sideJob, dialogPreset, dialog, speakingTo));

        //Start update
        this.enabled = true;
    }

    private void Update()
    {
        //Disable if speech loop 0
        if(speechQueue.Count <= 0)
        {
            if (activeSpeechBubble == null)
            {
                SetSpeechActive(false);
                if(actor!= null) actor.isSpeaking = false;
            }

            this.enabled = false;
        }
        else
        {
            try
            {
                SetSpeechActive(true);

                if (PopupMessageController.Instance != null && PopupMessageController.Instance.active)
                {
                    return;
                }
                //Process in order
                else if (activeSpeechBubble == null && speechDelay <= 0f)
                {
                    QueueElement sp = speechQueue[0];

                    //Must be in ground tile vicinity
                    if (actor != null)
                    {
                        //Wait for help pointers (outside of cut scenes)
                        if (actor.isPlayer && InterfaceController.Instance.helpPointerQueue.Count > 0 && !sp.interupt && !CutSceneController.Instance.cutSceneActive)
                        {
                            Game.Log("Objective: Speech queue is waiting...");
                            return;
                        }

                        if (!sp.isObjective && !actor.isPlayer && !actor.currentCityTile.isInPlayerVicinity)
                        {
                            speechQueue.RemoveAt(0);
                            return;
                        }

                        if(actor.isDead || actor.isStunned)
                        {
                            speechQueue.RemoveAt(0);
                            return;
                        }
                    }

                    //Apply speech delay first...
                    if (!sp.delayActivated)
                    {
                        speechDelay = sp.delay;
                        sp.delayActivated = true;
                    }

                    if (speechDelay > 0f)
                    {
                        return;
                    }

                    //Objective
                    if (sp.isObjective)
                    {
                        Case thisCase = CasePanelController.Instance.activeCases.Find(item => item.id == sp.caseID);
                        if (thisCase == null) thisCase = CasePanelController.Instance.archivedCases.Find(item => item.id == sp.caseID);

                        //Check this isn't a duplicate...
                        if(thisCase != null)
                        {
                            if(thisCase.currentActiveObjectives.Exists(item => item.queueElement.entryRef == sp.entryRef))
                            {
                                Game.LogError("Objective: Duplicate objective " + sp.entryRef + " found: Not creating another one!");
                                if (speechQueue.Count > 0) speechQueue.RemoveAt(0);
                                return;
                            }
                        }

                        //Remove previous objectives
                        if (sp.removePreviousObjectives)
                        {
                            if(thisCase != null)
                            {
                                Game.Log("Removing previous objectives for " + thisCase.name);

                                for (int i = 0; i < thisCase.currentActiveObjectives.Count; i++)
                                {
                                    thisCase.currentActiveObjectives[i].Cancel();
                                }
                            }

                            for (int i = 0; i < Player.Instance.speechController.speechQueue.Count; i++)
                            {
                                if (Player.Instance.speechController.speechQueue[i].isObjective)
                                {
                                    if (Player.Instance.speechController.activeSpeechBubble != null && Player.Instance.speechController.activeSpeechBubble.speech == Player.Instance.speechController.speechQueue[i])
                                    {
                                        Destroy(Player.Instance.speechController.activeSpeechBubble.gameObject);
                                        Player.Instance.speechController.activeSpeechBubble = null;
                                    }

                                    Player.Instance.speechController.speechQueue.RemoveAt(i);
                                    i--;
                                    continue;
                                }
                            }
                        }

                        if(thisCase != null) new Objective(sp); //Only do this if we have a case reference. We should already have one but a past bug means that we could not in a save game.
                        if (speechQueue.Count > 0) speechQueue.RemoveAt(0);
                        return;
                    }

                    if (SessionData.Instance.play)
                    {
                        if (SessionData.Instance.currentTimeSpeed == SessionData.TimeSpeed.normal)
                        {
                            float volume = 1f;
                            AudioEvent audEvent = null;

                            //Try to find an accompanying voice audio event...
                            Toolbox.Instance.voiceActedDictionary.TryGetValue(sp.entryRef, out audEvent);

                            //If there isn't one then use these dummy events...
                            if (audEvent == null)
                            {
                                audEvent = AudioControls.Instance.speakEvent;
                                if (sp.shouting) audEvent = AudioControls.Instance.shoutEvent;
                            }

                            NewNode currentNode = null;

                            //Get node
                            if(actor != null)
                            {
                                currentNode = actor.currentNode;
                            }
                            else
                            {
                                currentNode = interactable.node;
                            }

                            int penetrationCount = 0;

                            if (actor != null && actor.isPlayer) volume = 1f;
                            else volume = AudioController.Instance.GetOcculusion(Player.Instance.currentNode, currentNode, audEvent, 1f, actor, null, out penetrationCount, out _, out _, out _, out _);

                            if (volume <= 0.01f)
                            {
                                speechQueue.RemoveAt(0);
                                return;
                            }

                            //As this is not a 'real' sound, we need to emulate the falloff and detect if the player can hear it...
                            if(actor != Player.Instance)
                            {
                                float soundLevel = AudioController.Instance.GetPlayersSoundLevel(currentNode, audEvent, volume, null);

                                if (soundLevel < 0.1f)
                                {
                                    speechQueue.RemoveAt(0);
                                    return;
                                }

                                if (actor != null)
                                {
                                    actor.HeardByPlayer();
                                }
                            }

                            GameObject newSpeech = Instantiate(PrefabControls.Instance.speechBubble, InterfaceControls.Instance.speechBubbleParent);
                            activeSpeechBubble = newSpeech.GetComponent<SpeechBubbleController>();
                            activeSpeechBubble.Setup(sp, this);

                            lastSpeech = SessionData.Instance.gameTime;

                            //Setup awareness
                            if (actor != null && !actor.isPlayer)
                            {
                                activeSpeechBubble.awarenessIcon = InterfaceController.Instance.AddAwarenessIcon(InterfaceController.AwarenessType.actor, InterfaceController.AwarenessBehaviour.invisibleInfront, actor, null, Vector3.zero, InterfaceControls.Instance.speech, 0, true, audEvent.hearingRange);
                            }
                        }

                        speechQueue.RemoveAt(0);
                    }
                }
                else if (speechDelay > 0f && SessionData.Instance.play)
                {
                    speechDelay -= Time.deltaTime;
                }
            }
            catch
            {
                Game.LogError("Objective: Error processing speech queue!");
            }
        }
    }

    public void SetSpeechActive(bool val)
    {
        if(speechActive != val)
        {
            speechActive = val;

            if(InteractionController.Instance.dialogMode && (InteractionController.Instance.talkingTo == interactable || (InteractionController.Instance.isRemote && InteractionController.Instance.remoteOverride == interactable)))
            {
                InteractionController.Instance.RefreshDialogOptions();
            }
        }
    }

    private void OnEnable()
    {
        if (speechQueue.Count > 0)
        {
            this.enabled = true;
        }
    }
}
