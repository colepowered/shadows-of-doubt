﻿using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using NaughtyAttributes;

public class InteractableCreator : MonoBehaviour
{
    [Header("Debug")]
    public int debugFindID;

    //Singleton pattern
    private static InteractableCreator _instance;
    public static InteractableCreator Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    //Used for creating a citizen interactable (NOT saved, evidence saved separately)
    public Interactable CreateCitizenInteractable(InteractablePreset preset, Human citizen, Transform trans, Evidence evidence)
    {
        Interactable newI = new Interactable(preset);

        newI.save = false; //This is NOT saved with the city

        //As this is not saved, we can populate the non serialized fields immediately...
        newI.parentTransform = trans;

        //Assign evidence
        newI.evidence = evidence;

        newI.SetSpawnPositionRelevent(false);

        //Set polymorph ref
        newI.SetPolymorphicReference(citizen);
        newI.isActor = citizen;

        //Set current node
        newI.node = citizen.currentNode;

        //Set speech controller
        if (citizen.speechController != null && preset == CitizenControls.Instance.citizenInteractable)
        {
            newI.speechController = citizen.speechController;
            newI.speechController.interactable = newI;
            newI.speechController.actor = citizen;
        }

        newI.SetOwner(citizen);
        newI.SetWriter(citizen);

        newI.MainSetupStart();
        newI.OnCreate();

        return newI;
    }

    //Used for creating misc interactables tied to an always-spawned transform (NOT saved, evidence saved separately)
    public Interactable CreateTransformInteractable(InteractablePreset preset, Transform trans, Human belongsTo, Evidence evidence, Vector3 localPos, Vector3 localEuler, List<Interactable.Passed> passedVars)
    {
        Interactable newI = new Interactable(preset);

        newI.save = false; //This is NOT saved with the city

        //As this is not saved, we can populate the non serialized fields immediately...
        newI.parentTransform = trans;
        newI.evidence = evidence;

        newI.SetSpawnPositionRelevent(false);

        newI.lPos = localPos;
        newI.lEuler = localEuler;

        newI.pv = passedVars;

        newI.SetOwner(belongsTo, true);
        newI.SetWriter(belongsTo);

        newI.MainSetupStart();
        newI.OnCreate();

        return newI;
    }

    //Used for creating a furniture-tied interactable (NOT SAVED)
    public Interactable CreateFurnitureIntegratedInteractable(
        InteractablePreset preset,
        NewRoom room,
        FurnitureLocation furniture,
        Human belongsTo,
        Human writer,
        Human recevier,
        Vector3 localPos,
        Vector3 localEuler,
        InteractableController.InteractableID pairTo,
        FurniturePreset.SubObjectOwnership pairToOwner,
        LightingPreset isLight,
        List<Interactable.Passed> passedVars)
    {
        Interactable newI = new Interactable(preset);

        //This is saved
        newI.save = false; //Maybe don't save this with city????
        newI.AssignFurnitureBasedID(furniture); //Assign an ID as this is saved...

        newI.furnitureParent = furniture;
        newI.fp = furniture.id;

        newI.SetSpawnPositionRelevent(false);

        newI.lPos = localPos;
        newI.lEuler = localEuler;

        newI.pv = passedVars;

        newI.SetOwner(belongsTo, true);
        newI.SetWriter(writer);
        newI.SetReciever(recevier);

        newI.pt = (int)pairTo;
        newI.pto = (int)pairToOwner;

        newI.MainSetupStart();
        newI.OnCreate();

        if (isLight != null)
        {
            newI.SetAsLight(isLight, -1, preset.isMainLight, null);
            newI.lp = isLight.name;
            newI.ml = preset.isMainLight;
        }

        return newI;
    }

    //Used for creating furniture-tied interactables that are positioned on a furniture spawn point - this version is compatible with threaded race condtions done on a room-by-room thread as it uses the room reference as ID (SAVED)
    public Interactable CreateFurnitureSpawnedInteractableThreadSafe(InteractablePreset preset, NewRoom room, FurnitureLocation furniture, FurniturePreset.SubObject subObject, Human belongsTo, Human writer, Human recevier, List<Interactable.Passed> passedVars, LightingPreset isLight, object passedObject, string ddsOverride = "")
    {
        Interactable newI = new Interactable(preset);

        //This is saved
        newI.save = true; //Saved with city
        newI.AssignRoomBasedID(room); //Assign an ID as this is saved...

        newI.SetSpawnPositionRelevent(true);

        newI.furnitureParent = furniture;
        newI.fp = furniture.id;
        newI.subObject = subObject;
        newI.fsoi = furniture.furniture.subObjects.IndexOf(subObject); //Get the sub object index
        newI.lPos = subObject.localPos;
        newI.lEuler = subObject.localRot;

        if (ddsOverride.Length > 0)
        {
            if (passedVars == null) passedVars = new List<Interactable.Passed>();
            passedVars.Add(new Interactable.Passed(Interactable.PassedVarType.ddsOverride, 0, ddsOverride));
        }

        newI.pv = passedVars;

        //Parse passed object...
        if (passedObject != null)
        {
            //newI.retailItem = passedObject as RetailItemPreset;
            //if (newI.retailItem != null) newI.ri = newI.retailItem.name;

            newI.syncDisk = passedObject as SyncDiskPreset;
            if (newI.syncDisk != null) newI.sd = newI.syncDisk.name;

            newI.book = passedObject as BookPreset;
            if (newI.book != null) newI.bo = newI.book.name;
        }

        newI.SetOwner(belongsTo, true);
        newI.SetWriter(writer);
        newI.SetReciever(recevier);

        newI.MainSetupStart();
        newI.OnCreate();

        if (isLight != null)
        {
            newI.SetAsLight(isLight, -1, preset.isMainLight, null);
            newI.lp = isLight.name;
            newI.ml = false;
        }

        return newI;
    }

    //Used for creating furniture-tied interactables that are positioned on a furniture spawn point - this version is INCOMPATIBLE with threaded race condtions done on a room-by-room thread (SAVED)
    public Interactable CreateFurnitureSpawnedInteractable(InteractablePreset preset, FurnitureLocation furniture, FurniturePreset.SubObject subObject, Human belongsTo, Human writer, Human recevier, List<Interactable.Passed> passedVars, LightingPreset isLight, object passedObject, string ddsOverride = "")
    {
        Interactable newI = new Interactable(preset);

        //This is saved
        newI.save = true; //Saved with city
        newI.AssignIDWorld(); //Assign an ID as this is saved...

        newI.SetSpawnPositionRelevent(true);

        newI.furnitureParent = furniture;
        newI.fp = furniture.id;
        newI.subObject = subObject;
        newI.fsoi = furniture.furniture.subObjects.IndexOf(subObject); //Get the sub object index
        newI.lPos = subObject.localPos;
        newI.lEuler = subObject.localRot;

        if (ddsOverride.Length > 0)
        {
            if (passedVars == null) passedVars = new List<Interactable.Passed>();
            passedVars.Add(new Interactable.Passed(Interactable.PassedVarType.ddsOverride, 0, ddsOverride));
        }

        newI.pv = passedVars;

        //Parse passed object...
        if (passedObject != null)
        {
            //newI.retailItem = passedObject as RetailItemPreset;
            //if (newI.retailItem != null) newI.ri = newI.retailItem.name;

            newI.syncDisk = passedObject as SyncDiskPreset;
            if (newI.syncDisk != null) newI.sd = newI.syncDisk.name;

            newI.book = passedObject as BookPreset;
            if (newI.book != null) newI.bo = newI.book.name;
        }

        newI.SetOwner(belongsTo, true);
        newI.SetWriter(writer);
        newI.SetReciever(recevier);

        newI.MainSetupStart();
        newI.OnCreate();

        if (isLight != null)
        {
            newI.SetAsLight(isLight, -1, preset.isMainLight, null);
            newI.lp = isLight.name;
            newI.ml = false;
        }

        return newI;
    }

    //Used for creating a spawned interactable (SAVED)
    public Interactable CreateWorldInteractable(InteractablePreset preset, Human belongsTo, Human writer, Human recevier, Vector3 worldPos, Vector3 worldEuler, List<Interactable.Passed> passedVars, object passedObject, string ddsOverride = "")
    {
        Interactable newI = new Interactable(preset);

        //This is saved
        newI.save = true; //Saved with city
        newI.AssignIDWorld(); //Assign an ID as this is saved...

        newI.SetSpawnPositionRelevent(false);

        //This will make sure objects are parented to the correct room they're in...
        newI.wo = true;

        //Save local positions
        newI.lPos = worldPos;
        newI.lEuler = worldEuler;
        newI.wPos = worldPos;
        newI.lEuler = worldEuler;

        if(ddsOverride.Length > 0)
        {
            if (passedVars == null) passedVars = new List<Interactable.Passed>();
            passedVars.Add(new Interactable.Passed(Interactable.PassedVarType.ddsOverride, 0, ddsOverride));
        }

        newI.pv = passedVars;

        //Parse passed object...
        if(passedObject != null)
        {
            //newI.retailItem = passedObject as RetailItemPreset;
            //if (newI.retailItem != null) newI.ri = newI.retailItem.name;

            newI.syncDisk = passedObject as SyncDiskPreset;
            if (newI.syncDisk != null) newI.sd = newI.syncDisk.name;

            newI.book = passedObject as BookPreset;
            if (newI.book != null) newI.bo = newI.book.name;
        }

        newI.SetOwner(belongsTo, true);
        newI.SetWriter(writer);
        newI.SetReciever(recevier);

        newI.MainSetupStart();
        newI.OnCreate();

        if (preset != null && preset.isLight != null)
        {
            Game.Log("Object: Setting world object as light");
            newI.SetAsLight(preset.isLight, -1, preset.isMainLight, null);
            newI.lp = preset.isLight.name;
            newI.ml = preset.isMainLight;
        }

        return newI;
    }

    //Used for creating spawned interactable from a meta object (SAVED)
    public Interactable CreateWorldInteractableFromMetaObject(MetaObject meta, InteractablePreset preset, Vector3 worldPos, Vector3 worldEuler)
    {
        Interactable newI = new Interactable(preset);

        //This is saved
        newI.save = true; //Save this with city (maybe not needed????)
        newI.id = meta.id; //Copy meta ID

        newI.SetSpawnPositionRelevent(false);

        //This will make sure objects are parented to the correct room they're in...
        newI.wo = true;

        //Save local positions
        newI.lPos = worldPos;
        newI.lEuler = worldEuler;
        newI.wPos = worldPos;
        newI.lEuler = worldEuler;

        newI.pv = new List<Interactable.Passed>(meta.passed);

        if (meta.owner > -1)
        {
            Human o = null;

            if(CityData.Instance.GetHuman(meta.owner, out o))
            {
                newI.SetOwner(o, true);
            }
        }

        if (meta.writer > -1)
        {
            Human o = null;

            if (CityData.Instance.GetHuman(meta.writer, out o))
            {
                newI.SetWriter(o);
            }
        }

        if (meta.reciever > -1)
        {
            Human o = null;

            if (CityData.Instance.GetHuman(meta.reciever, out o))
            {
                newI.SetReciever(o);
            }
        }

        //Get evidence from meta object
        newI.evidence = meta.GetEvidence(true, CityData.Instance.RealPosToNodeInt(worldPos));

        newI.MainSetupStart();
        newI.OnCreate();

        //Remove the meta object
        meta.Remove();

        return newI;
    }

    //Create spawned interactable parented to a door (SAVED)
    public Interactable CreateDoorParentedInteractable(InteractablePreset preset, NewDoor door, Human belongsTo, Vector3 localPos, Vector3 localEuler, List<Interactable.Passed> passedVars, string ddsOverride = "")
    {
        Interactable newI = new Interactable(preset);

        //This is saved
        newI.save = true; //Save this with city (there shouldn't be any anyway)
        newI.AssignIDWorld(); //Assign an ID as this is saved...

        newI.SetSpawnPositionRelevent(false);

        //As this is not saved, we can populate the non serialized fields immediately...
        newI.parentTransform = door.transform;
        newI.dp = door.wall.id;

        newI.lPos = localPos;
        newI.lEuler = localEuler;

        if (ddsOverride.Length > 0)
        {
            if (passedVars == null) passedVars = new List<Interactable.Passed>();
            passedVars.Add(new Interactable.Passed(Interactable.PassedVarType.ddsOverride, 0, ddsOverride));
        }

        newI.pv = passedVars;
        newI.SetOwner(belongsTo, true);
        newI.SetWriter(belongsTo);

        newI.MainSetupStart();
        newI.OnCreate();

        return newI;
    }

    //Used for creating a main light interactable (SAVED)
    public Interactable CreateMainLightInteractable(InteractablePreset preset, NewRoom room, Vector3 worldPos, Vector3 worldEuler, LightingPreset lightPreset, Interactable.LightConfiguration preconfiguredLight, int lightZoneSize = -1)
    {
        Interactable newI = new Interactable(preset);

        //This is saved
        newI.save = true; //Save this with city
        newI.AssignRoomBasedID(room); //Assign an ID as this is saved...

        newI.SetSpawnPositionRelevent(false);

        //This will make sure objects are parented to the correct room they're in...
        newI.wo = true;

        //Save local positions
        newI.lPos = worldPos;
        newI.lEuler = worldEuler;
        newI.wPos = worldPos;
        newI.lEuler = worldEuler;

        newI.MainSetupStart();
        newI.OnCreate();

        newI.SetAsLight(lightPreset, lightZoneSize, true, preconfiguredLight);
        newI.lp = lightPreset.presetName;
        newI.ml = true;
        newI.lzs = lightZoneSize;

        //Game.Log("After set light: " + newI.wPos.y);

        return newI;
    }

    //Used for spawning a book (SAVED)
    public Interactable CreateBookInteractable(InteractablePreset preset, NewRoom room, FurnitureLocation furniture, Human belongsTo, Vector3 localPos, Vector3 localEuler, BookPreset book)
    {
        Interactable newI = new Interactable(preset);

        //This is saved
        newI.save = true; //Save this with city
        newI.AssignRoomBasedID(room); //Assign an ID as this is saved...

        newI.SetSpawnPositionRelevent(false);

        newI.furnitureParent = furniture;
        newI.fp = furniture.id;

        newI.lPos = localPos;
        newI.lEuler = localEuler;

        newI.SetOwner(belongsTo, true);
        newI.SetWriter(belongsTo);

        newI.book = book;
        newI.bo = book.name;

        newI.MainSetupStart();
        newI.OnCreate();

        return newI;
    }

    //Used for creating a confirmed fingerprint (SAVED)
    public Interactable CreateFingerprintInteractable(Human belongsTo, Vector3 worldPos, Vector3 worldEuler, FingerprintScannerController.Print print)
    {
        Game.Log("Player: Create new fingerprint interactable...");

        Interactable newI = new Interactable(GameplayControls.Instance.fignerprintPreset);

        //This is saved
        newI.save = false; //Don't need to save this with city
        newI.AssignIDWorld(); //Assign an ID as this is saved...

        newI.SetSpawnPositionRelevent(false);

        //Create savable class for print
        newI.print.Clear();
        newI.print.Add(new Interactable.SavedPrint { worldPos = print.worldPos } );
        if (print.interactable != null) newI.print[0].interactableID = print.interactable.id;

        //This will make sure objects are parented to the correct room they're in...
        newI.wo = true;

        //Save local positions
        newI.lPos = worldPos;
        newI.lEuler = worldEuler;
        newI.wPos = worldPos;
        newI.lEuler = worldEuler;

        newI.SetOwner(belongsTo, true);
        newI.SetWriter(belongsTo);

        newI.MainSetupStart();
        newI.OnCreate();

        return newI;
    }

    //Used for creating a confirmed footprint (SAVED)
    public Interactable CreateFootprintInteractable(Human belongsTo, Vector3 worldPos, Vector3 worldEuler, GameplayController.Footprint print)
    {
        Game.Log("Player: Create new fingerprint interactable...");

        Interactable newI = new Interactable(GameplayControls.Instance.footprintPreset);

        //This is saved
        newI.save = false; //Don't need to save this with city
        newI.AssignIDWorld(); //Assign an ID as this is saved...

        newI.SetSpawnPositionRelevent(false);

        //Create savable class for print
        newI.print.Clear();
        newI.print.Add(new Interactable.SavedPrint { worldPos = print.wP });

        //This will make sure objects are parented to the correct room they're in...
        newI.wo = true;

        //Save local positions
        newI.lPos = worldPos;
        newI.lEuler = worldEuler;
        newI.wPos = worldPos;
        newI.lEuler = worldEuler;

        newI.SetOwner(belongsTo, true);
        newI.SetWriter(belongsTo);

        newI.MainSetupStart();
        newI.OnCreate();

        return newI;
    }

    //Used for creating a furniture-tied interactable (NOT saved)
    public Interactable CreateInteractableLock(
        InteractablePreset preset,
        FurnitureLocation furniture,
        Human belongsTo,
        Vector3 localPos,
        Vector3 localEuler,
        InteractableController.InteractableID pairTo)
    {
        Interactable newI = new Interactable(preset);

        newI.save = false; //Dont need to save this with city

        newI.furnitureParent = furniture;
        newI.fp = furniture.id;

        newI.SetSpawnPositionRelevent(false);

        newI.lPos = localPos;
        newI.lEuler = localEuler;

        newI.SetOwner(belongsTo, true);
        newI.SetWriter(belongsTo);
        newI.pt = (int)pairTo;

        newI.MainSetupStart();
        newI.OnCreate();

        return newI;
    }

    [Button]
    public void FindInteractable()
    {
        Interactable i = CityData.Instance.interactableDirectory.Find(item => item.id == debugFindID);

        if (i != null)
        {
            Game.Log("Found item " + i.id + ": " + i.name + " at " + i.wPos + " spawned: " + i.controller);

            if(i.node != null)
            {
                Game.Log("Location: " + i.node.room.GetName() + " at " + i.node.gameLocation.name);
            }
        }
        else Game.Log("Unable to find item " + debugFindID);
    }

    [Button]
    public void ForceSpawnCheck()
    {
        Interactable i = CityData.Instance.interactableDirectory.Find(item => item.id == debugFindID);

        if (i != null)
        {
            Game.Log("Found item " + i.id + ": " + i.name + " at " + i.wPos + " spawned: " + i.controller);

            i.SpawnCheck();
        }
        else Game.Log("Unable to find item " + debugFindID);
    }

    [Button]
    public void ListFurnitureParentSpawned()
    {
        Interactable i = CityData.Instance.interactableDirectory.Find(item => item.id == debugFindID);

        if (i != null)
        {
            Game.Log("Found item " + i.id + ": " + i.name + " at " + i.wPos + " spawned: " + i.controller);

            if(i.furnitureParent != null)
            {
                Game.Log("Furniture parent is " + i.furnitureParent.id);

                foreach(Interactable sp in i.furnitureParent.spawnedInteractables)
                {
                    Game.Log(sp.GetName() + " " + sp.id + " is a spawned interactable of furniture " + i.furnitureParent.furniture.name + ": " + i.controller);
                }
            }
        }
        else Game.Log("Unable to find item " + debugFindID);
    }
}
