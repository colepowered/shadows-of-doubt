﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

[CreateAssetMenu(fileName = "playertransition_data", menuName = "Database/Player Transition Preset")]

public class PlayerTransitionPreset : SoCustomComparison
{
    public enum TransitionPosition { relativeToInteractable, relativeToPlayer};

    [System.Serializable]
    public class SFXSetting
    {
        public AudioEvent soundEvent;
        public float atProgress;
    }

    [Header("Transition")]
    [Tooltip("Transition speed in seconds")]
    public float transitionTime = 1f;

    [Header("Control")]
    [Tooltip("Player keeps movement control during transition")]
    public bool retainMovementControl = false;
    [Tooltip("If above is true, this movement multiplier is applied")]
    public AnimationCurve controlCurve;
    [Tooltip("Mouse look control curve")]
    public AnimationCurve mouseLookControlCurve;

    [Header("Height")]
    [Tooltip("Player height multiplier")]
    public float playerHeightMP = 1f;
    [Tooltip("Camera height multiplier")]
    public float CamHeightMP = 1f;
    [Tooltip("If true, both the above will be different depending on whether the player is crouched or stood up...")]
    public bool factorInCrouching = false;
    [Space(5)]
    public AnimationCurve heightCurve;
    public AnimationCurve camHeightCurve;

    [Header("Movement")]
    [Space(5)]
    public bool useXMovement = true;
    [EnableIf("useXMovement")]
    public AnimationCurve playerXCurve;
    public bool useYMovement = true;
    [EnableIf("useYMovement")]
    public AnimationCurve playerYCurve;
    public bool useZMovement = true;
    [EnableIf("useZMovement")]
    public AnimationCurve playerZCurve;
    [Space(5)]
    public TransitionPosition transitionRelativity = TransitionPosition.relativeToInteractable;
    [Space(5)]
    [Tooltip("Performing this transition won't override a previous stored position...")]
    public bool disableWriteReturnPosition = false;
    [Tooltip("If true then transition to the stored return postion using the curve below.")]
    public bool transitionToSavedReturnPosition = false;
    [Tooltip("If true then transition from the player's current position to the correct position as described in these settings.")]
    public bool transitionFromExistingPosition = true;
    [Tooltip("Transition curve for the above")]
    public AnimationCurve positionTransitionCurve;
    [Space(5)]
    [Tooltip("Useful for door interactables")]
    public bool invertXPositionBasedOnRelativePlayerX = false;
    public bool invertYPositionBasedOnRelativePlayerY = false;
    public bool invertZPositionBasedOnRelativePlayerZ = false;
    [Tooltip("Use a raycast to check this position is valid")]
    public bool raycastCheck = false;
    [EnableIf("raycastCheck")]
    public PlayerTransitionPreset onFailUse;
    [Space(7)]
    [Tooltip("Enable movement upon end transition")]
    public bool allowMovementOnEnd = false;
    [Tooltip("The movement speed on end")]
    [EnableIf("allowMovementOnEnd")]
    public bool restoreNormalMovementSpeed = false;
    [DisableIf("restoreNormalMovementSpeed")]
    public float customMovementSpeed = 1f;
    public bool disableGravity = false;
    public bool disableHeadBob = false;

    [Header("Mouse Look")]
    public bool useXLook = true;
    [EnableIf("useXLook")]
    public AnimationCurve playerXLookCurve;
    public bool useYLook = true;
    [EnableIf("useYLook")]
    public AnimationCurve playerYLookCurve;
    public bool useZLook = true;
    [EnableIf("useZLook")]
    public AnimationCurve playerZLookCurve;
    [Space(5)]
    public TransitionPosition lookRelativity = TransitionPosition.relativeToInteractable;
    [Tooltip("If the above is set to relative to player, use the forward direction multiplied by this to determin the look position. This must be higher than the distance the player can move forward.")]
    public float forwardPositionModifier = 2f;
    [Tooltip("Multiply the X, Y and Z movement by this. Useful for when relative to player and adjusting the forward position modifier...")]
    public float lookMovementMultiplier = 1f;
    [Space(5)]
    public bool applyCameraRoll = false;
    public AnimationCurve cameraRoll;
    public float rollMultiplier = 27.5f;
    [Tooltip("Make sure camera roll is reset at the end of the transition")]
    public bool resetCameraRoll = true;
    [Space(5)]
    [Tooltip("If true then transition from the player's current mouse position to the correct position as described in these settings.")]
    public bool transitionFromExistingMouse = true;
    [Tooltip("Transition curve for the above")]
    public AnimationCurve mouseTransitionCurve;

    [Header("VFX")]
    public bool useChromaticAberration = false;
    [EnableIf("useChromaticAberration")]
    public AnimationCurve chromaticAberrationCurve;
    public bool useGain = false;
    [EnableIf("useGain")]
    public AnimationCurve gainCurve;

    [Header("SFX")]
    [ReorderableList]
    public List<SFXSetting> sfx = new List<SFXSetting>();

    [Header("First Person")]
    [Tooltip("Force selection of nothing upon transition begin")]
    public bool forceHolsterOnTransition = true;
    [Tooltip("Restore first person item after transition has ended")]
    [EnableIf("forceHolsterOnTransition")]
    public bool restoreHolsterOnTransitionEnd = false;
    [Tooltip("Allow weapon selection after transition has finished")]
    public bool allowWeaponSwitchingAfterTransition = false;
    [Space(7)]
    [Tooltip("A recoil state can be switched to manually in the transition to add these in addition to the normal look curves")]
    public AnimationCurve playerXRecoilLookCurve;
    public AnimationCurve playerYRecoilLookCurve;
    public AnimationCurve playerZRecoilLookCurve;

    [Header("Return")]
    public bool useCustomReturnPosition = false;
    [Tooltip("Return position relative to the interactable")]
    [EnableIf("useCustomReturnPosition")]
    public Vector3 returnPostion;
}
