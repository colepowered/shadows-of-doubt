using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class WindowDebugger : MonoBehaviour
{
    public BuildingPreset preset;

    [Button]
    public void SpawnObjectsOnWindows()
    {
        //Find the scale of the capture mesh from comparing those in the prefab
        Transform[] children = transform.GetComponentsInChildren<Transform>(true);
        Transform windowObject = null;

        foreach (Transform t in children)
        {
            MeshFilter f = t.gameObject.GetComponent<MeshFilter>();

            if (f != null)
            {
                if (f.sharedMesh == preset.captureMesh)
                {
                    Debug.Log("Capture mesh found in prefab! Local scale is " + t.localScale.x + ", " + t.localScale.y + ", " + t.localScale.z + ", rotation: " + t.localEulerAngles + ",  pos: " + t.localPosition);
                    windowObject = t;

                    break;
                }
            }
        }

        for (int i = 0; i < preset.sortedWindows.Count; i++)
        {
            BuildingPreset.WindowUVFloor floor = preset.sortedWindows[i];
            GameObject newFloorObj = new GameObject();
            newFloorObj.transform.SetParent(this.transform, false);
            newFloorObj.name = "Floor " + (i + 1);

            foreach (BuildingPreset.WindowUVBlock block in floor.front)
            {
                GameObject newObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                newObj.transform.SetParent(this.transform, false);
                newObj.transform.localPosition = (block.localMeshPositionLeft + block.localMeshPositionRight) / 2f;
                newObj.transform.SetParent(newFloorObj.transform, true);
                newObj.transform.name = block.floor + " | " + block.side + " | " + block.horizonal + " (" + block.localMeshPositionLeft + " - " + block.localMeshPositionRight + ")";

                MeshRenderer rend = newObj.GetComponent<MeshRenderer>();
                Material newMat = Instantiate(rend.sharedMaterial);
                float ratio = block.horizonal / 10f;
                Color col = Color.Lerp(new Color(0, 0, 0.25f), new Color(0, 0, 1f), ratio);

                newMat.SetColor("_BaseColor", col);
                rend.sharedMaterial = newMat;
            }

            foreach (BuildingPreset.WindowUVBlock block in floor.back)
            {
                GameObject newObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                newObj.transform.SetParent(this.transform, false);
                newObj.transform.localPosition = (block.localMeshPositionLeft + block.localMeshPositionRight) / 2f;
                newObj.transform.SetParent(newFloorObj.transform, true);
                newObj.transform.name = block.floor + " | " + block.side + " | " + block.horizonal + " (" + block.localMeshPositionLeft + " - " + block.localMeshPositionRight + ")";

                MeshRenderer rend = newObj.GetComponent<MeshRenderer>();
                Material newMat = Instantiate(rend.sharedMaterial);
                float ratio = block.horizonal / 10f;
                Color col = Color.Lerp(new Color(0.25f, 0.25f, 0.25f), new Color(1f, 1f, 1f), ratio);

                newMat.SetColor("_BaseColor", col);
                rend.sharedMaterial = newMat;
            }

            foreach (BuildingPreset.WindowUVBlock block in floor.left)
            {
                GameObject newObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                newObj.transform.SetParent(this.transform, false);
                newObj.transform.localPosition = (block.localMeshPositionLeft + block.localMeshPositionRight) / 2f;
                newObj.transform.SetParent(newFloorObj.transform, true);
                newObj.transform.name = block.floor + " | " + block.side + " | " + block.horizonal + " (" + block.localMeshPositionLeft + " - " + block.localMeshPositionRight + ")";

                MeshRenderer rend = newObj.GetComponent<MeshRenderer>();
                Material newMat = Instantiate(rend.sharedMaterial);
                float ratio = block.horizonal / 10f;
                Color col = Color.Lerp(new Color(0.25f, 0f, 0f), new Color(1f, 0f, 0f), ratio);

                newMat.SetColor("_BaseColor", col);
                rend.sharedMaterial = newMat;
            }

            foreach (BuildingPreset.WindowUVBlock block in floor.right)
            {
                GameObject newObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                newObj.transform.SetParent(this.transform, false);
                newObj.transform.localPosition = (block.localMeshPositionLeft + block.localMeshPositionRight) / 2f;
                newObj.transform.SetParent(newFloorObj.transform, true);
                newObj.transform.name = block.floor + " | " + block.side + " | " + block.horizonal + " (" + block.localMeshPositionLeft + " - " + block.localMeshPositionRight + ")";

                MeshRenderer rend = newObj.GetComponent<MeshRenderer>();
                Material newMat = Instantiate(rend.sharedMaterial);
                float ratio = block.horizonal / 10f;
                Color col = Color.Lerp(new Color(0f, 0.25f, 0f), new Color(0f, 1f, 0f), ratio);

                newMat.SetColor("_BaseColor", col);
                rend.sharedMaterial = newMat;
            }
        }
    }
}
