﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using NaughtyAttributes;

//Stores all evidence
public class GameplayController : MonoBehaviour
{
    /*
	Dictionary of dictionaries method.

	Use dictionary to hold police records.

	Use an ID int as a dictionary key.
	*/

    public Dictionary<string, Evidence> evidenceDictionary = new Dictionary<string, Evidence>();

    public List<Fact> factList = new List<Fact>(); //List of all facts

    //Incrimination sources
    public List<Evidence> singletonEvidence = new List<Evidence>();
    public List<EvidenceDate> dateEvidence = new List<EvidenceDate>();
    public List<EvidenceTime> timeEvidence = new List<EvidenceTime>();
    public List<EvidenceMultiPage> multiPageEvidence = new List<EvidenceMultiPage>();

    //Keep track of suspects with incrimination
    [System.Serializable]
    public class History
    {
        public string evID;
        public List<Evidence.DataKey> keys;
        public float lastAccess;
        public int locationID;
    }

    public List<History> history = new List<History>(); //History of opened evidence
    [NonSerialized]
    public List<History> itemOnlyHistory = new List<History>(); //For keeping reference of items only

    //Dictionary of confirmed fingerprints
    public Dictionary<Vector3, Interactable> confirmedPrints = new Dictionary<Vector3, Interactable>();
    public int printsLetterLoop = 0; //Assigned as player discovers fingerprint sets...
    public HashSet<Interactable> objectsWithDynamicPrints = new HashSet<Interactable>(); //List of objects with dynamic prints

    //Existing matches; key- match type, value list<mystery>
    //Mystery
    public Dictionary<MatchPreset, List<Evidence>> parentMatches = new Dictionary<MatchPreset, List<Evidence>>();

    //Existing matches
    public Dictionary<MatchPreset, List<FactMatches>> matchesDetails = new Dictionary<MatchPreset, List<FactMatches>>();

    //Guest passes
    //Players allowed at locations until this expires (x = time at expiry, y = for hours value)
    public Dictionary<NewAddress, Vector2> guestPasses = new Dictionary<NewAddress, Vector2>();

    //Vmail threads
    public Dictionary<int, StateSaveData.MessageThreadSave> messageThreads = new Dictionary<int, StateSaveData.MessageThreadSave>();
    public int assignMessageThreadID = 1;

    //Enforcers
    public List<Human> enforcers = new List<Human>();
    public Dictionary<NewGameLocation, EnforcerCall> enforcerCalls = new Dictionary<NewGameLocation, EnforcerCall>();

    //Suspect pickup
    public Dictionary<Case, float> caseProcessing = new Dictionary<Case, float>();

    //Hospital beds
    public List<Interactable> hospitalBeds = new List<Interactable>();

    //Broken windows; index by position and time of breakage...
    public Dictionary<Vector3, float> brokenWindows = new Dictionary<Vector3, float>();

    //Lost & found items
    [System.Serializable]
    public class LostAndFound
    {
        public string preset;
        public int ownerID;
        public int buildingID;
        public int spawnedItem;
        public int spawnedNote;
        public int rewardMoney;
        public int rewardSC;
    }

    //Door knocking attempts
    public class DoorKnockAttempt
    {
        public Actor human;
        public float value;
    }

    public Dictionary<NewDoor, List<DoorKnockAttempt>> doorKnockAttempts = new Dictionary<NewDoor, List<DoorKnockAttempt>>();

    //Active gadgets items
    public List<Interactable> activeGadgets = new List<Interactable>();

    //Active crime scenes
    public HashSet<NewGameLocation> crimeScenes = new HashSet<NewGameLocation>();
    public List<NewDoor> policeTapeDoors = new List<NewDoor>();
    public List<NewGameLocation> crimeSceneCleanups = new List<NewGameLocation>();

    //Active breakers
    public List<Interactable> closedBreakers = new List<Interactable>();

    //Turned off security
    public List<Interactable> turnedOffSecurity = new List<Interactable>();

    //Burning barrels
    public List<Interactable> burningBarrels = new List<Interactable>();

    //Switch resets
    public Dictionary<Interactable, float> switchRessetingObjects = new Dictionary<Interactable, float>();

    //Player knows passwords
    public List<int> playerKnowsPasswords = new List<int>(); //List of addresses the player is clear to enter

    //Rooms with gas
    public List<NewRoom> gasRooms = new List<NewRoom>();

    //Corporate sabotage
    public List<string> companiesSabotaged = new List<string>();

    //Conversation delay
    public Dictionary<string, float> globalConversationDelay = new Dictionary<string, float>();

    //Read
    public List<string> booksRead = new List<string>();

    public List<Interactable> activeKettles = new List<Interactable>();
    public List<Interactable> activeMusicPlayers = new List<Interactable>();

    public Dictionary<string, Material> graffitiCache = new Dictionary<string, Material>();

    //Motion trackers
    public Dictionary<Interactable, List<NewNode>> activeTrackers = new Dictionary<Interactable, List<NewNode>>();
    public HashSet<NewNode> trackedNodes = new HashSet<NewNode>();
    public Dictionary<Interactable, float> proxyTrackers = new Dictionary<Interactable, float>();

    public List<Interactable> activeGrenades = new List<Interactable>();

    //Default item button
    public GameObject setDefaultItemButton;
	public GameObject defaultItemButton;

    [Header("Player Stats")]
    public int money = 0;
    public int lockPicks = 0;

    //Social credit
    public int socialCredit = 0;

    //Lockpicking
    public float currentLockpickStrength = 1f;

    //Peril
    public int perilFine = 0;

    //Cases
    public string[] doeLetters = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
    public float timeSinceLastUpdateLoop = 0f;
    public float lastUpdateLoop = 0f;

    [Header("Passcodes")]
    public List<Passcode> acquiredPasscodes = new List<Passcode>();

    [System.Serializable]
    public class Passcode
    {
        public List<int> digits = new List<int>();
        public PasscodeType type;
        public int id;
        public bool used = false;
        public List<int> notes = new List<int>(); //Placed notes

        //Debug
        //public List<string> debugNotePlacement = new List<string>();

        public Passcode(PasscodeType newType)
        {
            type = newType;
        }

        public string GetNotePlacements()
        {
            string ret = string.Empty;

            foreach(int i in notes)
            {
                Interactable note = null;

                if(CityData.Instance.savableInteractableDictionary.TryGetValue(i, out note))
                {
                    ret += "\n" + note.id + " " + note.name + " at " + note.GetWorldPosition() + ", " + note.node.room.name + ", " + note.node.gameLocation.name;
                }
            }

            Game.Log(ret);
            return ret;
        }

        public List<int> GetDigits()
        {
            if(Game.Instance.overridePasscodes)
            {
                List<int> ret = new List<int>();

                string numStr = Game.Instance.overriddenPasscode.ToString();

                //Ensure this is 4 digits
                while (numStr != null && numStr.Length < 4)
                {
                    numStr = "0" + numStr;
                }

                try
                {
                    for (int i = 0; i < 4; i++)
                    {
                        int p = 0;

                        int.TryParse(numStr.Substring(i, 1), out p);
                        ret.Add(p);
                    }

                    return ret;
                }
                catch
                {
                    return digits;
                }
            }

            return digits;
        }

        public int GetDigit(int index)
        {
            index = Mathf.Clamp(index, 0, 3);

            if (Game.Instance.overridePasscodes)
            {
                List<int> ret = new List<int>();

                string numStr = Game.Instance.overriddenPasscode.ToString();

                //Ensure this is 4 digits
                while (numStr != null && numStr.Length < 4)
                {
                    numStr = "0" + numStr;
                }

                try
                {
                    for (int i = 0; i < 4; i++)
                    {
                        int p = 0;

                        int.TryParse(numStr.Substring(i, 1), out p);
                        ret.Add(p);
                    }

                    return ret[index];
                }
                catch
                {
                    return digits[index];
                }
            }

            return digits[index];
        }
    }

    public enum PasscodeType { citizen, room, address, interactable};

    [Header("Phone Numbers")]
    public List<PhoneNumber> acquiredNumbers = new List<PhoneNumber>();

    [Serializable]
    public class PhoneNumber
    {
        public int number;
        public string textOverride;
        public bool loc = false; //Do I know the location?
        public List<int> p = new List<int>(); //Do I know the residents/owners?
    }

    [Header("Apartments for Sale")]
    public List<NewAddress> forSale = new List<NewAddress>();

    [Header("Hotel")]
    public List<HotelGuest> hotelGuests = new List<HotelGuest>();

    [Serializable]
    public class HotelGuest
    {
        public int addID; //Address ID
        public int humanID;
        public int roomCost;
        public int bill; //Running total of the current bill
        public float lastPayment;
        public float nextPayment;

        public Human GetHuman()
        {
            Human ret = null;
            CityData.Instance.GetHuman(humanID, out ret, true);
            return ret;
        }

        public NewAddress GetAddress()
        {
            NewAddress ret = null;
            CityData.Instance.addressDictionary.TryGetValue(addID, out ret);
            return ret;
        }

        public void PayBill(int amount)
        {
            bill = 0;
            lastPayment = SessionData.Instance.gameTime;
        }

        //Executed when we load a save game...
        public void FromLoadGame()
        {
            //Setup locations of authority
            Human person = GetHuman();

            if (person != null)
            {
                NewAddress add = GetAddress();

                if (add != null)
                {
                    person.AddLocationOfAuthorty(add);
                }
            }
        }
    }

    [Header("Culling State")]
    public List<NewRoom> roomsVicinity = new List<NewRoom>();
    public List<AirDuctGroup> ductsVicinity = new List<AirDuctGroup>();
    public HashSet<Human> activeRagdolls = new HashSet<Human>(); //List of active locations with ragdolls...
    public HashSet<Interactable> activePhysics = new HashSet<Interactable>(); //List of active locations with object physics...

    [Header("Clean-up")]
    public List<SpatterSimulation> spatter = new List<SpatterSimulation>(); //Spatters need to be checked for removal
    public List<Interactable> interactablesMoved = new List<Interactable>(); //Moved interactables need to be reset
    public HashSet<NewDoor> damagedDoors = new HashSet<NewDoor>();

    [Header("Security")]
    public List<NewBuilding> activeAlarmsBuildings = new List<NewBuilding>();
    public List<NewAddress> activeAlarmsLocations = new List<NewAddress>();
    public List<NewBuilding> alteredSecurityTargetsBuildings = new List<NewBuilding>();
    public List<NewAddress> alteredSecurityTargetsLocations = new List<NewAddress>();

    [System.Serializable]
    public class EnforcerCall
    {
        public bool isStreet = false; //Is this street or address?
        public int id = 0; //Location ID
        public float logTime; //Call logged at
        public EnforcerCallState state = EnforcerCallState.logged;
        public List<int> response; //Response
        public float arrivalTime = 0f;
        public bool isCrimeScene = false;
        public bool immedaiteTeleport = false;
        public int guard = -1;
    }

    public enum EnforcerCallState { logged, responding, arrived, completed};

    [Header("Footprints")]
    public List<Footprint> footprintsList = new List<Footprint>();
    public Dictionary<NewRoom, List<Footprint>> activeFootprints = new Dictionary<NewRoom, List<Footprint>>();

    //Dictionary of confirmed footprints
    public Dictionary<Vector3, Interactable> confirmedFootprints = new Dictionary<Vector3, Interactable>();

    [System.Serializable]
    public class Footprint
    {
        public int hID; //Human ID
        public int rID; //Room ID
        public Vector3 wP; //Position (world)
        public Vector3 eU; //Euler (world)
        public float str; //Strength
        public float bl; //Blood
        public float t; //Time stamp

        public Footprint (Human human, Vector3 position, Vector3 euler, float dirt, float blood, NewRoom forceRoom = null)
        {
            hID = human.humanID;
            wP = position;
            eU = euler;

            str = Toolbox.Instance.RoundToPlaces(dirt * Toolbox.Instance.Rand(0.75f, 1f), 2);
            bl = Toolbox.Instance.RoundToPlaces(blood * Toolbox.Instance.Rand(0.75f, 1f), 2);

            t = SessionData.Instance.gameTime;

            if (forceRoom == null) forceRoom = human.currentRoom;
            rID = forceRoom.roomID;

            //Add to active footprints
            if (!GameplayController.Instance.activeFootprints.ContainsKey(forceRoom))
            {
                GameplayController.Instance.activeFootprints.Add(forceRoom, new List<Footprint>());
            }

            GameplayController.Instance.activeFootprints[forceRoom].Add(this);
            GameplayController.Instance.footprintsList.Add(this);

            //Make sure the room updates with this footprint soon...
            forceRoom.QueueFootprintUpdate();
        }
    }

    //Stored and pre-processed dynamic text images, ie graffiti with dynamic text
    public Dictionary<ArtPreset, Material> dynamicTextImages = new Dictionary<ArtPreset, Material>();

    //Loan shark debt
    public List<LoanDebt> debt = new List<LoanDebt>();

    [System.Serializable]
    public class LoanDebt
    {
        public int companyID;
        public int debt;
        public int payments;
        public int missedPayments;
        public float nextPaymentDueBy;
        public float dueCheck;

        public int GetRepaymentAmount()
        {
            return Mathf.Min(payments + missedPayments, debt);
        }
    }

    [Header("Difficulty")]
    [Tooltip("Dictates which missions can spawn...")]
    public int jobDifficultyLevel = 0;

    //Events
    //Case status change; open a new active case or close one
    //public delegate void ChangeActiveCases();
    //public event ChangeActiveCases OnCaseStatusChange;

    //Triggered when matches are changed
    public delegate void MatchesChange();
    public event MatchesChange OnMatchesChanged;

    //Triggered when a new evidence history entry is added
    public delegate void NewEvidenceHistory();
    public event NewEvidenceHistory OnNewEvidenceHistory;

    //Triggered when phone data is added
    public delegate void NewPhoneData();
    public event NewPhoneData OnNewPhoneData;

    //Actions - used as callback for end of frame
    Action UpdateMatch;

    //Singleton pattern
    private static GameplayController _instance;
	public static GameplayController Instance { get { return _instance; } }

	void Awake()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }

        //Setup actions
        UpdateMatch += UpdateMatches;
    }

	void Start ()
	{
		defaultItemButton = setDefaultItemButton;
    }

    public void UpdateConversationDelays()
    {
        List<string> toRemove = new List<string>();

        foreach(KeyValuePair<string,  float> pair in globalConversationDelay)
        {
            //Reset conversations after 1.25 in game hours
            if(SessionData.Instance.gameTime >= pair.Value + 1.25f)
            {
                toRemove.Add(pair.Key);
            }
        }

        foreach(string str in toRemove)
        {
            globalConversationDelay.Remove(str);
        }
    }


    //Add a new match
    public void AddNewMatch(MatchPreset match, Evidence newEntry)
    {
        if(SessionData.Instance.startedGame) Game.Log("Evidence: Add match: " + newEntry.name + " to " + match.name);

        if (!parentMatches.ContainsKey(match))
        {
            parentMatches.Add(match, new List<Evidence>());
        }

        parentMatches[match].Add(newEntry);

        //If this isn't discovered yet, listen for discovery
        if(!newEntry.isFound)
        {
            newEntry.OnDiscovered += OnDiscoverMatchEvidence;
        }
        else
        {
            UpdateMatchesEndOfFrame();
        }

        //Fire event
        if (OnMatchesChanged != null) OnMatchesChanged();
    }

    //Called when a undiscovered piece of match evidence is discovered
    private void OnDiscoverMatchEvidence(Evidence ev)
    {
        UpdateMatchesEndOfFrame();

        //Unsubscribe from discovery
        ev.OnDiscovered -= OnDiscoverMatchEvidence;
    }

    //Update matches- other scripts can point to this as a shortcut for updating matches on end of frame
    public void UpdateMatchesEndOfFrame()
    {
        if (!SessionData.Instance.startedGame) return; //Skip this if the game has't been started- city constructor will run it once
        Toolbox.Instance.InvokeEndOfFrame(UpdateMatch, "Check matches");
    }

    //Remove match
    public void RemoveMatch(MatchPreset match, Evidence newEntry)
    {
        if (parentMatches.ContainsKey(match))
        {
            if (parentMatches[match].Contains(newEntry))
            {
                parentMatches[match].Remove(newEntry);

                //Remove key if 0
                if(parentMatches[match].Count <= 0)
                {
                    parentMatches.Remove(match);
                }

                Toolbox.Instance.InvokeEndOfFrame(UpdateMatch, "Check matches");
            }

            //Fire event
            if (OnMatchesChanged != null) OnMatchesChanged();
        }
    }

    //Update matches
    public void UpdateMatches()
    {
        foreach(KeyValuePair<MatchPreset, List<Evidence>> pair in parentMatches)
        {
            MatchPreset match = pair.Key;
            if (match.canOnlyBeMatchedWith) continue; //Skip if no match condition

            Game.Log("Evidence: Update matches for " + match.name + "...");

            //Look for matches of this
            MatchPreset matchWith = match;

            //Loop through list to match...
            List<Evidence> matchFromEvidence = null;

            if (match.onlyMatchWithThis != null)
            {
                matchWith = match.onlyMatchWithThis;
            }

            if (parentMatches.ContainsKey(match))
            {
                //Get all found matches
                matchFromEvidence = parentMatches[match].FindAll(item => item.isFound);
            }

            //Loop through list to match with...
            //These are usually the same
            List<Evidence> matchToEvidence = new List<Evidence>();

            if (parentMatches.ContainsKey(matchWith))
            {
                //Get all found matches
                matchToEvidence = parentMatches[matchWith].FindAll(item => item.isFound);
                if (match.onlyMatchWithThis == null) matchFromEvidence = matchToEvidence;
            }
            else
            {
                Game.Log("Evidence: ...ParentMatches does not contain key " + matchWith.name);
            }

            Game.Log("Evidence: ...Found " + matchFromEvidence.Count + ">" + matchToEvidence.Count + " entries in this category.");

            //Build two lists to determin required details
            List<Evidence> requiredFrom = new List<Evidence>();
            List<Evidence> requiredTo = new List<Evidence>();

            for (int i = 0; i < matchFromEvidence.Count; i++)
            {
                Evidence matchFrom = matchFromEvidence[i];

                for (int u = 0; u < matchToEvidence.Count; u++)
                {
                    Evidence matchTo = matchToEvidence[u];

                    //Don't link to self
                    if (!match.canMatchWithItself && matchFrom == matchTo)
                    {
                        //Game.Log("Match: From == to: Continue...");
                        continue;
                    }

                    //Match parent check
                    if (match.onlyMatchWithMatchParents)
                    {
                        //If neither evidence is a match parent, continue
                        //Game.Log("Match: Parent check fail: Continue...");
                        if (!matchFrom.preset.isMatchParent && !matchTo.preset.isMatchParent) continue;
                    }

                    //Is this a match?
                    if (!FactMatches.MatchCheck(match, matchFrom, matchTo))
                    {
                        //Game.Log("Match: Match fail...");
                        continue;
                    }
                    //else Game.Log("Evidence: Found match: " + matchFrom + " > " + matchTo);

                    bool duplicateCheck = true; //Check for duplicate matches

                    //If the match already exists within the required in some form...
                    for (int l = 0; l < requiredFrom.Count; l++)
                    {
                        Evidence fromCheck = requiredFrom[l];

                        if(fromCheck == matchFrom) //If this is the first entry, check for corresponding entries for the 2nd
                        {
                            if(requiredTo[l] == matchTo)
                            {
                                duplicateCheck = false;
                                break;
                            }
                        }

                        if (fromCheck == matchTo) //If this is the second entry, check for corresponding entries for the 1st
                        {
                            if (requiredTo[l] == matchFrom)
                            {
                                duplicateCheck = false;
                                break;
                            }
                        }
                    }

                    //If the duplicate check is passed, add a match entry
                    if(duplicateCheck)
                    {
                        requiredFrom.Add(matchFrom);
                        requiredTo.Add(matchTo);
                    }
                    else
                    {
                        //Game.Log("Match: Duplicate check fail: Continue...");
                    }
                }
            }

            Game.Log("Evidence: Matches required for " + match.name + " = " + requiredFrom.Count);

            //Create key if needed
            if (!matchesDetails.ContainsKey(match))
            {
                matchesDetails.Add(match, new List<FactMatches>());
            }

            //Go through existing list
            for (int i = 0; i < matchesDetails[match].Count; i++)
            {
                FactMatches matchDetail = matchesDetails[match][i];

                //If a null entry, this has obviously been destroyed- remove entry
                if (matchDetail == null)
                {
                    matchesDetails[match].RemoveAt(i);
                    i--;
                    continue;
                }

                //If this is supposed to exist, remove from found files list
                if ((requiredFrom.Contains(matchDetail.fromEvidence[0]) && requiredTo.Contains(matchDetail.toEvidence[0])) ||
                    (requiredFrom.Contains(matchDetail.toEvidence[0]) && requiredTo.Contains(matchDetail.fromEvidence[0])))
                {
                    requiredFrom.Remove(matchDetail.fromEvidence[0]);
                    requiredTo.Remove(matchDetail.toEvidence[0]);
                }
                //If it's not found in the list, it's safe to remove
                else
                {
                    //RemoveEvidence(matchDetail);
                    //matchesDetails[match].RemoveAt(i);
                    //i--;
                }
            }

            //Create remaining
            for (int i = 0; i < requiredFrom.Count; i++)
            {
                Game.Log("Evidence: Create remaining; " + requiredFrom[i].name + " to " + requiredTo[i].name);

                //Get link items from entry
                Evidence fromItem = requiredFrom[i];
                Evidence toItem = requiredTo[i];

                Game.Log("Evidence: Spawn new match from " + requiredFrom[i].name + " to " + requiredTo[i].name);
                List<object> passedObjs = new List<object>();
                passedObjs.Add(match);
                FactMatches newMatch = EvidenceCreator.Instance.CreateFact("Matches", fromEvidenceSingular: fromItem, toEvidenceSingular: toItem, forceDiscoveryOnCreate: true, passedObjects: passedObjs, overrideFromKeys: match.linkFromKeys, overrideToKeys: match.linkToKeys) as FactMatches;

                if (newMatch != null)
                {
                    matchesDetails[match].Add(newMatch);
                }
                else
                {
                    Game.Log("Evidence: New match was not created, it must exist already in reverse form");
                }
            }
        }

        //Display stats to debug
        //foreach (KeyValuePair<MatchPreset, List<FactMatches>> pair in matchesDetails)
        //{
        //    Game.Log("Evidence: " + pair.Value.Count + " match facts for match type " + pair.Key.name);
        //}
    }

    //Add to evidence history
    public void AddHistory(Evidence entry, List<Evidence.DataKey> keys)
    {
        if (entry == null) return;
        List<Evidence.DataKey> keys1 = entry.GetTiedKeys(keys);

        //Does this exist already?
        List<History> existing = history.FindAll(item => item.evID == entry.evID);

        foreach(History sus in existing)
        {
            Evidence foundEvidence = null;

            if(evidenceDictionary.TryGetValue(sus.evID, out foundEvidence))
            {
                List<Evidence.DataKey> keys2 = foundEvidence.GetTiedKeys(keys);

                foreach(Evidence.DataKey key in keys1)
                {
                    if(keys2.Contains(key))
                    {
                        //Already exists: Update last time
                        Game.Log("Interface: " + sus.evID + " already existings in history, ignoring...");
                        sus.lastAccess = SessionData.Instance.gameTime;

                        //Sort history by access time
                        history.Sort((p2, p1) => p1.lastAccess.CompareTo(p2.lastAccess));
                        itemOnlyHistory.Sort((p2, p1) => p1.lastAccess.CompareTo(p2.lastAccess));

                        //Fire event
                        if (OnNewEvidenceHistory != null)
                        {
                            OnNewEvidenceHistory();
                        }

                        return;
                    }
                }
            }
        }

        bool isItem = false;
        if (entry.interactable != null && entry.interactable.preset.spawnable) isItem = true;

        //Remove first history if needed
        if(isItem && itemOnlyHistory.Count >= InterfaceControls.Instance.maximumEvidenceItemHistory)
        {
            History lastItem = itemOnlyHistory[itemOnlyHistory.Count - 1];
            history.Remove(lastItem);
            itemOnlyHistory.RemoveAt(itemOnlyHistory.Count - 1);
        }

        History newSus = new History();
        newSus.evID = entry.evID;
        newSus.keys = keys1;
        newSus.lastAccess = SessionData.Instance.gameTime;

        if(Player.Instance.currentGameLocation.thisAsAddress != null)
        {
            newSus.locationID = Player.Instance.currentGameLocation.thisAsAddress.id;
        }

        history.Add(newSus);

        if(isItem)
        {
            itemOnlyHistory.Add(newSus);
        }

        //Fire event
        if(OnNewEvidenceHistory != null)
        {
            OnNewEvidenceHistory();
        }

        Game.Log("Interface: Added history: " + entry.evID + " total: " + history.Count + " items: " + itemOnlyHistory.Count + "/"+ InterfaceControls.Instance.maximumEvidenceItemHistory);
    }

    //Add Money
    public void AddMoney(int addVal, bool displayMessage, string reason)
    {
        if (SessionData.Instance.isFloorEdit) return;
        if (SessionData.Instance.isDialogEdit) return;
        if (addVal == 0) return;

        money += addVal;
        money = Mathf.Max(money, 0);

        Game.Log("Gameplay: Add money: " + addVal);

        //Add game message
        if (displayMessage && addVal != 0)
        {
            string plus = string.Empty;
            if (addVal >= 0) plus = "+";

            SessionData.Instance.TutorialTrigger("money");

            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, plus + CityControls.Instance.cityCurrency + addVal.ToString(), InterfaceControls.Icon.money, moveToOnDestroy: InterfaceController.Instance.moneyNotificationIcon, ping: GameMessageController.PingOnComplete.money);
        }

        FirstPersonItemController.Instance.PlayerMoneyCheck();

        //Set display text
        if (InterfaceControls.Instance.cashText != null)
        {
            InterfaceControls.Instance.cashText.text = CityControls.Instance.cityCurrency + money.ToString();
            if (BioScreenController.Instance.cashText != null) BioScreenController.Instance.cashText.text = InterfaceControls.Instance.cashText.text;
        }
    }

    public void SetMoney(int newVal)
    {
        if (SessionData.Instance.isFloorEdit) return;
        if (SessionData.Instance.isDialogEdit) return;

        InterfaceController.Instance.lastMoney = money;
        money = Mathf.Max(newVal, 0);

        Game.Log("Gameplay: Set money: " + money);

        FirstPersonItemController.Instance.PlayerMoneyCheck();

        //Set display text
        if (InterfaceControls.Instance.cashText != null)
        {
            InterfaceControls.Instance.cashText.text = CityControls.Instance.cityCurrency + money.ToString();
            if(BioScreenController.Instance.cashText != null) BioScreenController.Instance.cashText.text = InterfaceControls.Instance.cashText.text;
        }
    }

    //Social credit
    public void AddSocialCredit(int addVal, bool displayMessage, string reason)
    {
        if (SessionData.Instance.isFloorEdit) return;
        if (SessionData.Instance.isDialogEdit) return;
        if (addVal == 0) return;

        int previousCred = socialCredit;

        socialCredit += addVal;
        socialCredit = Mathf.Max(socialCredit, 0);

        Game.Log("Gameplay: Add social credit: " + socialCredit);

        //Add game message
        if (displayMessage && addVal != 0)
        {
            string plus = string.Empty;
            if (addVal >= 0) plus = "+";

            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, addVal, Strings.Get("ui.gamemessage", "Social credit"), InterfaceControls.Icon.resolve, AudioControls.Instance.gainSocialCredit, newMessageDelay: 3);
        }

        if(addVal > 0 && displayMessage)
        {
            SessionData.Instance.TutorialTrigger("socialcredit");
        }

        BioScreenController.Instance.OnChangePoints(displayMessage);
    }

    public void SetSocialCredit(int newVal)
    {
        if (SessionData.Instance.isFloorEdit) return;
        if (SessionData.Instance.isDialogEdit) return;

        socialCredit = Mathf.Max(newVal, 0);

        Game.Log("Gameplay: Set social credit: " + socialCredit);

        BioScreenController.Instance.OnChangePoints(false);
    }

    public int GetCurrentSocialCreditLevel()
    {
        return GetSocialCreditLevel(socialCredit);
    }

    public int GetNextSocialCreditLevelThreshold()
    {
        return GetSocialCreditThresholdForLevel(GetCurrentSocialCreditLevel() + 1);
    }

    public int GetSocialCreditLevel(int points)
    {
        int ret = 1;

        int p = points;

        while(p > 0)
        {
            //Enough points for next level?
            p -= Mathf.RoundToInt(GameplayControls.Instance.socialCreditLevelCurve.Evaluate(ret + 1));
            if (p >= 0) ret++;
            else break;
        }

        //Game.Log("Social credit level for " + points + " points : " + ret);

        return ret;
    }

    public int GetSocialCreditThreshold(int points)
    {
        return GetSocialCreditThresholdForLevel(GetSocialCreditLevel(points));
    }

    public int GetSocialCreditThresholdForLevel(int level)
    {
        int ret = 0;

        while(level > 0)
        {
            ret += Mathf.RoundToInt(GameplayControls.Instance.socialCreditLevelCurve.Evaluate(level));
            level--;
        }

        //Game.Log("Social credit threshold for level " + level + ": " + ret);

        return ret;
    }

    //Add Presence
    public void AddLockpicks(int addVal, bool displayMessage)
    {
        if (SessionData.Instance.isFloorEdit) return;
        if (SessionData.Instance.isDialogEdit) return;
        if (addVal == 0) return;

        lockPicks += addVal;

        //Add game message
        if (displayMessage)
        {
            string plus = string.Empty;
            if (addVal > 0) plus = "+";

            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, plus + addVal + " " + Strings.Get("ui.gamemessage", "lockpicks"), InterfaceControls.Icon.lockpick, null, moveToOnDestroy: InterfaceController.Instance.lockpicksNotificationIcon, ping: GameMessageController.PingOnComplete.lockpicks);
        }

        //Set display text
        InterfaceControls.Instance.lockpicksText.text = lockPicks.ToString();
    }

    public void SetLockpicks(int newVal)
    {
        if (SessionData.Instance.isFloorEdit) return;
        if (SessionData.Instance.isDialogEdit) return;

        lockPicks = newVal;
        InterfaceController.Instance.lastLockpicks = lockPicks;

        //Set display text
        InterfaceControls.Instance.lockpicksText.text = lockPicks.ToString();
    }

    //Use up a lockpick
    public void UseLockpick(float val)
    {
        currentLockpickStrength -= val / Mathf.LerpUnclamped(GameplayControls.Instance.lockpickEffectivenessRange.x, GameplayControls.Instance.lockpickEffectivenessRange.y, UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.lockpickingEfficiencyModifier));
        currentLockpickStrength = Mathf.Clamp01(currentLockpickStrength);

        if(currentLockpickStrength <= 0f)
        {
            DepleteLockpick();
        }
    }

    //Deplete a lockpick
    public void DepleteLockpick()
    {
        AddLockpicks(-1, false);
        currentLockpickStrength = 1f; //Reset lockpick strength
    }

    //Add commendations
    //public void AddCommendations(int addVal)
    //{
    //    if (SessionData.Instance.isFloorEdit) return;
    //    if (SessionData.Instance.isDialogEdit) return;

    //    commendations += addVal;
    //    commendations = Mathf.Max(commendations, 0);

    //    //Set display text
    //    InterfaceControls.Instance.commendationText.text = Mathf.Clamp(commendations, -99, 99).ToString();

    //    //Flash if +
    //    if (addVal > 0)
    //    {
    //        InterfaceControls.Instance.commendationFlash.Flash(1);

    //        //Add game message
    //        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, InterfaceControls.Instance.commendationSprite, 0, Strings.Get("ui.gamemessage", "New Commendation"), AudioControls.Instance.commendation, colourOverride: true, col: InterfaceControls.Instance.messageBlue);
    //    }
    //}

    public void UpdateCullingForRoom(NewRoom currentRoom, bool updateSound, bool inAirVent, AirDuctGroup currentDuct, bool immediateLoad = false)
    {
        //Update this whenever rooms are altered in any way
        if (updateSound)
        {
            AudioController.Instance.UpdateAllLoopingSoundOcclusion();
        }

        //The required list is the culling room tree: Already configured
        HashSet<NewRoom> required = new HashSet<NewRoom>();
        HashSet<AirDuctGroup> requiredDucts = new HashSet<AirDuctGroup>();

        if (!SessionData.Instance.isFloorEdit && !SessionData.Instance.isTestScene)
        {
            //Add rooms required by murder and physics simulations...
            foreach (MurderController.Murder m in MurderController.Instance.activeMurders)
            {
                foreach (int r in m.cullingActiveRooms)
                {
                    NewRoom ro = null;

                    if(CityData.Instance.roomDictionary.TryGetValue(r, out ro))
                    {
                        required.Add(ro);
                    }
                }
            }

            foreach (Human h in activeRagdolls)
            {
                //Add room and adjacent rooms
                if (!required.Contains(h.currentRoom))
                {
                    required.Add(h.currentRoom);
                }

                foreach (NewRoom r in h.currentRoom.adjacentRooms)
                {
                    if (!required.Contains(r))
                    {
                        required.Add(r);
                    }
                }
            }

            foreach (Interactable p in activePhysics)
            {
                if(p.node == null)
                {
                    p.UpdateWorldPositionAndNode(false);

                    if (p.node == null) continue;
                }

                //Add room and adjacent rooms
                if (!required.Contains(p.node.room))
                {
                    required.Add(p.node.room);
                }

                foreach(NewRoom r in p.node.room.adjacentRooms)
                {
                    if (!required.Contains(r))
                    {
                        required.Add(r);
                    }
                }
            }

            //Air vent culling...
            if (inAirVent)
            {
                if (currentDuct == null)
                {
                    Game.Log("Player: Duct culling failed: No current duct");
                    return; //This needs a room to function
                }

                Game.Log("Player: Update culling for duct " + currentDuct.ductID + " with " + currentDuct.ventRooms.Count + " vent rooms...");

                requiredDucts.Add(currentDuct);

                if(Player.Instance.currentRoom != null && !required.Contains(Player.Instance.currentRoom))
                {
                    required.Add(Player.Instance.currentRoom);
                    //Game.Log("Duct culling room, add player current room: " + Player.Instance.currentRoom.name + " " + Player.Instance.currentRoom.roomID);
                }

                foreach (NewRoom room in currentDuct.ventRooms)
                {
                    if (!required.Contains(room))
                    {
                        Vector3 roomPos = room.middleRoomPosition;

                        //Game.Log("Duct culling room, scanning room: " + room.name + " " + room.roomID);

                        float dist = Vector3.Distance(Player.Instance.transform.position, roomPos);

                        if (dist < CullingControls.Instance.ductRoomCullingRange || room.IsOutside())
                        {
                            required.Add(room);
                            //Game.Log("Duct culling room, add room: " + room.name + " " + room.roomID);

                            //Add the pre-generated culling tree
                            foreach (KeyValuePair<NewRoom, List<NewRoom.CullTreeEntry>> cull in room.cullingTree)
                            {
                                if (cull.Key == null) continue;

                                //Only pick rooms from this level if in same building...
                                if (cull.Key.building == Player.Instance.currentBuilding)
                                {
                                    float heightDiff = Vector3.Distance(Player.Instance.transform.position, roomPos);
                                    if (heightDiff > PathFinder.Instance.tileSize.z + 1f) continue; //Greater than a floor's difference then skip
                                }

                                if (!required.Contains(cull.Key))
                                {
                                    //Check for required doors...
                                    foreach (NewRoom.CullTreeEntry e in cull.Value)
                                    {
                                        if (e.requiredOpenDoors == null || e.requiredOpenDoors.Count <= 0)
                                        {
                                            required.Add(cull.Key);
                                            //Game.Log("Duct culling room, add room: " + cull.Key.name + " " + cull.Key.roomID);
                                        }
                                        //Do door check...
                                        else
                                        {
                                            bool doorPass = true;

                                            foreach (int d in e.requiredOpenDoors)
                                            {
                                                NewDoor getDoor = null;

                                                if (CityData.Instance.doorDictionary.TryGetValue(d, out getDoor))
                                                {
                                                    if (getDoor.isClosed && !getDoor.peekedUnder)
                                                    {
                                                        doorPass = false;
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    Game.LogError("Cannot find door at wall " + d);
                                                }
                                            }

                                            if (doorPass)
                                            {
                                                required.Add(cull.Key);
                                                //Game.Log("Duct culling room, add room: " + cull.Key.name + " " + cull.Key.roomID);
                                            }
                                            else
                                            {
                                                //Game.Log("Duct culling room, DON'T add room (door fail): " + cull.Key.name + " " + cull.Key.roomID);
                                            }
                                        }
                                    }
                                }

                                //Add ducts
                                foreach (AirDuctGroup airDuctGrp in cull.Key.ductGroups)
                                {
                                    if (!requiredDucts.Contains(airDuctGrp))
                                    {
                                        requiredDucts.Add(airDuctGrp);
                                    }
                                }
                            }
                        }
                    }
                }

                foreach (AirDuctGroup ductGrp in currentDuct.adjoiningGroups)
                {
                    requiredDucts.Add(ductGrp);
                }
            }
            //Not in air duct
            else
            {
                if (currentRoom == null)
                {
                    Game.Log("Player: room culling failed: No current room");
                    return; //This needs a room to function
                }

                //For null rooms, don't update culling
                if (currentRoom.isNullRoom && currentRoom.cullingTree.Count <= 1 && !SessionData.Instance.isTestScene)
                {
                    if (currentRoom.IsOutside())
                    {
                        Game.Log("Player: Player is in outside window room, attempt to use nearest street room instead...");

                        NewNode foundNode = Toolbox.Instance.GetNearestGroundLevelOutside(Player.Instance.transform.position);

                        if (foundNode != null)
                        {
                            currentRoom = foundNode.room;
                            Game.Log("Player: Found replacement closest street room: " + currentRoom.gameLocation.name);
                        }
                        else
                        {
                            Game.Log("Player: Failed to find closest street room...");
                        }
                    }
                    else
                    {
                        Game.Log("Player: Player is in null room; " + currentRoom.name + " don't update culling tree for " + currentRoom.name);
                        return;
                    }
                }

                //Detect culling tree error
                if (currentRoom.cullingTree.Count <= 0 && !SessionData.Instance.isTestScene)
                {
                    Game.Log("Player: Culling tree of " + currentRoom.name + " is zero! There is probably something wrong here...");
                }

                //Game.Log("Player: Executing culling tree for " + currentRoom.name + " with " + currentRoom.cullingTree.Count + " entries");

                //Add the pre-generated culling tree
                foreach (KeyValuePair<NewRoom, List<NewRoom.CullTreeEntry>> cull in currentRoom.cullingTree)
                {
                    if (cull.Key == null) continue;

                    if (!required.Contains(cull.Key))
                    {
                        //Check for required doors...
                        foreach (NewRoom.CullTreeEntry e in cull.Value)
                        {
                            //If no doors are needed to be open...
                            if (e.requiredOpenDoors == null || e.requiredOpenDoors.Count <= 0)
                            {
                                required.Add(cull.Key);

                                //Auto add atrium top
                                if (cull.Key.atriumTop != null)
                                {
                                    if (!required.Contains(cull.Key.atriumTop))
                                    {
                                        required.Add(cull.Key.atriumTop);
                                    }
                                }

                                //Add atrium rooms above and below
                                foreach(NewRoom r in cull.Key.atriumRooms)
                                {
                                    if (!required.Contains(r))
                                    {
                                        required.Add(r);
                                    }
                                }

                                //Add shared streets...
                                if (cull.Key.gameLocation.thisAsStreet != null)
                                {
                                    foreach (StreetController st in cull.Key.gameLocation.thisAsStreet.sharedGroundElements)
                                    {
                                        foreach (NewRoom r in st.rooms)
                                        {
                                            if (!required.Contains(r))
                                            {
                                                required.Add(r);
                                            }
                                        }
                                    }
                                }
                            }
                            //Do door check...
                            else
                            {
                                bool doorPass = true;

                                foreach (int d in e.requiredOpenDoors)
                                {
                                    NewDoor getDoor = null;

                                    if (CityData.Instance.doorDictionary.TryGetValue(d, out getDoor))
                                    {
                                        if (getDoor.isClosed && !getDoor.peekedUnder)
                                        {
                                            doorPass = false;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        Game.LogError("Cannot find door at wall " + d);
                                    }
                                }

                                if (doorPass)
                                {
                                    required.Add(cull.Key);

                                    //Auto add atrium top
                                    if (cull.Key.atriumTop != null)
                                    {
                                        if (!required.Contains(cull.Key.atriumTop))
                                        {
                                            required.Add(cull.Key.atriumTop);
                                        }
                                    }

                                    //Add atrium rooms above and below
                                    foreach (NewRoom r in cull.Key.atriumRooms)
                                    {
                                        if (!required.Contains(r))
                                        {
                                            required.Add(r);
                                        }
                                    }

                                    //Add shared streets...
                                    if (cull.Key.gameLocation.thisAsStreet != null)
                                    {
                                        foreach (StreetController st in cull.Key.gameLocation.thisAsStreet.sharedGroundElements)
                                        {
                                            foreach (NewRoom r in st.rooms)
                                            {
                                                if (!required.Contains(r))
                                                {
                                                    required.Add(r);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //Add ducts
                    foreach (AirDuctGroup airDuctGrp in cull.Key.ductGroups)
                    {
                        if (!requiredDucts.Contains(airDuctGrp))
                        {
                            requiredDucts.Add(airDuctGrp);
                        }
                    }
                }
            }

            //Add exterior air ducts
            foreach (NewBuilding building in CityData.Instance.buildingDirectory)
            {
                if (building.displayBuildingModel)
                {
                    float dist = Vector3.Distance(currentRoom.middleRoomPosition, building.transform.position);

                    if (dist < CullingControls.Instance.exteriorDuctCullingRange)
                    {
                        foreach (AirDuctGroup ductGrp in building.airDucts)
                        {
                            if (ductGrp.isExterior)
                            {
                                if (!requiredDucts.Contains(ductGrp))
                                {
                                    requiredDucts.Add(ductGrp);
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {
            if (FloorEditController.Instance != null && FloorEditController.Instance.editFloor != null)
            {
                foreach (NewAddress add in FloorEditController.Instance.editFloor.addresses)
                {
                    foreach (NewRoom room in add.rooms)
                    {
                        required.Add(room);
                    }
                }
            }
        }

        //if (inAirVent)
        //{
        //    foreach (NewRoom r in required)
        //    {
        //        Game.Log("Air duct required 1: " + r.name + " (" + r.roomID + ")");
        //    }
        //}

        //Execute culling
        for (int i = 0; i < roomsVicinity.Count; i++)
        {
            NewRoom thisRoom = roomsVicinity[i];

            //if(inAirVent) Game.Log("Air duct vicinity: " + thisRoom.name + " (" + thisRoom.roomID + ")");

            if (thisRoom == null)
            {
                roomsVicinity.RemoveAt(i);
                i--;
                continue;
            }

            if (required.Contains(thisRoom))
            {
                //if (inAirVent) Game.Log("Required contains (set visible): " + thisRoom.name + " (" + thisRoom.roomID + ")");

                thisRoom.SetVisible(true, false, "Tree by " + currentRoom.name, immediateLoad: immediateLoad);
                required.Remove(thisRoom);
            }
            else
            {
                //if (inAirVent) Game.Log("Required doesn't contain (set invisible): " + thisRoom.name + " (" + thisRoom.roomID + ")");

                //Cull room
                thisRoom.SetVisible(false, false, "Lacking in tree by " + currentRoom.name, immediateLoad: immediateLoad);
                roomsVicinity.RemoveAt(i);
                i--;
                continue;
            }
        }

        foreach (NewRoom roomReq in required)
        {
            //Display room
            roomReq.SetVisible(true, false, "Tree", immediateLoad: immediateLoad);
            roomsVicinity.Add(roomReq);
        }

        //Execute duct culling
        for (int i = 0; i < ductsVicinity.Count; i++)
        {
            AirDuctGroup thisDuct = ductsVicinity[i];

            if (thisDuct == null)
            {
                ductsVicinity.RemoveAt(i);
                i--;
                continue;
            }

            if (requiredDucts.Contains(thisDuct))
            {
                thisDuct.SetVisible(true);
                requiredDucts.Remove(thisDuct);
            }
            else
            {
                //Cull room
                thisDuct.SetVisible(false);
                ductsVicinity.RemoveAt(i);
                i--;
                continue;
            }
        }

        foreach (AirDuctGroup ductReq in requiredDucts)
        {
            //Display duct
            ductReq.SetVisible(true);
            ductsVicinity.Add(ductReq);
        }
    }

    //Add a guest pass
    public void AddGuestPass(NewAddress loc, float forHours)
    {
        if(!guestPasses.ContainsKey(loc))
        {
            guestPasses.Add(loc, Vector2.zero);
        }

        guestPasses[loc] = new Vector2(SessionData.Instance.gameTime + forHours, forHours);
        Game.Log("Player: Adding guest pass for " + loc.name + " for " + forHours + " hours");

        //Trigger room change to update state
        if (Player.Instance.currentRoom.gameLocation == loc)
        {
            Player.Instance.OnRoomChange();
        }
    }

    public void AddGuestPass(NewAddress loc, Vector2 directData)
    {
        if (!guestPasses.ContainsKey(loc))
        {
            guestPasses.Add(loc, Vector2.zero);
        }

        guestPasses[loc] = directData;
        Game.Log("Player: Adding guest pass for " + loc.name);

        //Trigger room change to update state
        if (Player.Instance.currentRoom.gameLocation == loc)
        {
            Player.Instance.OnRoomChange();
        }
    }

    //Call enforcers to location
    public void CallEnforcers(NewGameLocation newLocation, bool forceCrimeScene = false, bool immediateTeleport = false)
    {
        if (newLocation == null) return;

        if(!enforcerCalls.ContainsKey(newLocation))
        {
            EnforcerCall newCall = new EnforcerCall();

            newCall.isCrimeScene = forceCrimeScene;
            newCall.immedaiteTeleport = immediateTeleport;

            if(newLocation.thisAsAddress != null)
            {
                newCall.isStreet = false;
                newCall.id = newLocation.thisAsAddress.id;

                //Set security lockdown
                newLocation.thisAsAddress.floor.SetAlarmLockdown(true);
            }
            else if(newLocation.thisAsStreet != null)
            {
                newCall.isStreet = true;
                newCall.id = newLocation.thisAsStreet.streetID;
            }
            else
            {
                return;
            }

            newCall.logTime = SessionData.Instance.gameTime;

            Game.Log("Gameplay: New enforcer call added at " + newLocation.name);
            enforcerCalls.Add(newLocation, newCall);

            //Do we need to add an objective to the current murder case?
            if(InteractionController.Instance.dialogMode)
            {
                StartCoroutine(WaitForEndCall(newLocation));
            }
            else
            {
                NewMurderCaseNotify(newLocation);
            }
        }
    }

    //If the call is from the player, wait until they have put down the phone...
    IEnumerator WaitForEndCall(NewGameLocation newLocation)
    {
        while(InteractionController.Instance.dialogMode)
        {
            yield return null;
        }

        NewMurderCaseNotify(newLocation);
    }

    private void NewMurderCaseNotify(NewGameLocation newLocation)
    {
        foreach (MurderController.Murder m in MurderController.Instance.activeMurders)
        {
            if (Game.Instance.sandboxMode || (ChapterController.Instance != null && (ChapterController.Instance.chapterScript as ChapterIntro) != null && (ChapterController.Instance.chapterScript as ChapterIntro).completed))
            {
                if (newLocation.currentOccupants.Exists(item => item == m.victim && item.isDead))
                {
                    //Display message
                    InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "New Enforcer Call") + ": " + newLocation.name, InterfaceControls.Icon.skull, additionalSFX: AudioControls.Instance.enforcerScannerMsg);

                    //Create case
                    MurderController.Instance.OnVictimDiscovery();

                    //Add find dead body objective
                    if (MurderController.Instance.currentActiveCase != null)
                    {
                        Game.Log("Murder: Adding next crime scene objective...");
                        Objective.ObjectiveTrigger exploreCrimeScene = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.exploreCrimeScene, "", newGameLocation: newLocation);
                        string objName = Strings.Get("missions.postings", "Explore Reported Crime Scene") + newLocation.name;
                        MurderController.Instance.currentActiveCase.AddObjective(objName, exploreCrimeScene, onCompleteAction: Objective.OnCompleteAction.nothing, ignoreDuplicates: true, useParsing: false);
                    }
                }
            }
        }
    }

    public void AddPasscode(Passcode newCode, bool displayMessage = true)
    {
        if(newCode == null || newCode.GetDigits().Count < 4)
        {
            return;
        }

        if(!acquiredPasscodes.Contains(newCode))
        {
            acquiredPasscodes.Add(newCode);

            if(displayMessage)
            {
                string message = string.Empty;

                if (newCode.type == GameplayController.PasscodeType.room)
                {
                    NewRoom room = null;

                    if(CityData.Instance.roomDictionary.TryGetValue(newCode.id, out room))
                    {
                        message = room.GetName() + ", " + room.gameLocation.evidenceEntry.GetNameForDataKey(Evidence.DataKey.name);
                    }
                }
                else if(newCode.type == PasscodeType.address)
                {
                    NewAddress address = null;

                    if(CityData.Instance.addressDictionary.TryGetValue(newCode.id, out address))
                    {
                        message = address.evidenceEntry.GetNameForDataKey(Evidence.DataKey.name);
                    }
                }
                else if(newCode.type == PasscodeType.citizen)
                {
                    Human human = null;

                    if (CityData.Instance.GetHuman(newCode.id, out human))
                    {
                        message = human.evidenceEntry.GetNameForDataKey(Evidence.DataKey.name);
                    }
                }
                else if(newCode.type == PasscodeType.interactable)
                {
                    Interactable interactable = CityData.Instance.interactableDirectory.Find(item => item.id == newCode.id);

                    if (interactable != null)
                    {
                        message = interactable.GetName() + ", " + interactable.node.gameLocation.evidenceEntry.GetNameForDataKey(Evidence.DataKey.name);
                    }
                }

                message += " " + newCode.GetDigit(0).ToString() + newCode.GetDigit(1).ToString() + newCode.GetDigit(2).ToString() + newCode.GetDigit(3).ToString();


                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "passcode") +": " + message, InterfaceControls.Icon.agent, moveToOnDestroy: InterfaceController.Instance.notebookNotificationIcon);
            }

            int moneyForPasscode = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.moneyForPasscodes));

            if (moneyForPasscode > 0)
            {
                GameplayController.Instance.AddMoney(moneyForPasscode, true, "moneyforpasscodes");
            }
        }
    }

    public void AddOrMergePhoneNumberData(int newNumber, bool knowLocation, List<Human> knowCitizens, string textOverride = "", bool displayMessage = true)
    {
        List<int> citizens = new List<int>();

        if(knowCitizens != null)
        {
            foreach(Human h in knowCitizens)
            {
                citizens.Add(h.humanID);
            }
        }

        PhoneNumber existing = acquiredNumbers.Find(item => item.number == newNumber);

        if (existing == null)
        {
            GameplayController.PhoneNumber num = new PhoneNumber { number = newNumber, loc = knowLocation, p = citizens, textOverride = textOverride };

            acquiredNumbers.Add(num);

            if (displayMessage)
            {
                string message = string.Empty;

                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "telephone") + ": " + Toolbox.Instance.GetTelephoneNumberString(newNumber), InterfaceControls.Icon.telephone, moveToOnDestroy: InterfaceController.Instance.notebookNotificationIcon);
            }
        }
        else
        {
            existing.loc = knowLocation;
            existing.p = citizens;
            existing.textOverride = textOverride;
        }

        if(OnNewPhoneData != null)
        {
            OnNewPhoneData();
        }
    }

    public void AddDoorKnockAttempt(NewDoor door, Actor human)
    {
        DoorKnockAttempt found = null;

        if (doorKnockAttempts.ContainsKey(door))
        {
            found = doorKnockAttempts[door].Find(item => item.human == human);
        }
        else
        {
            doorKnockAttempts.Add(door, new List<DoorKnockAttempt>());
        }

        if(found != null)
        {
            found.value += 1f;
        }
        else
        {
            DoorKnockAttempt newAttempt = new DoorKnockAttempt { human = human, value = 1f };
            doorKnockAttempts[door].Add(newAttempt);
        }
    }

    public float GetDoorKnockAttemptValue(NewDoor door, Actor human)
    {
        DoorKnockAttempt found = null;

        if (doorKnockAttempts.ContainsKey(door))
        {
            found = doorKnockAttempts[door].Find(item => item.human == human);
        }

        if (found != null)
        {
            if (human.isEnforcer && human.isOnDuty) return found.value * 2f; //Enforcers quicker to barge
            else return found.value;
        }
        else return 0f;
    }

    public void KnockOnDoor(NewDoor door, Actor actor, int knocks = 2, float forceAdditionalUrgency = 0f)
    {
        float urgencyValue = GetDoorKnockAttemptValue(door, actor) + forceAdditionalUrgency;

        if((Game.Instance.devMode && Game.Instance.collectDebugData) && actor != null)
        {
            string actionDebug = string.Empty;
            if (actor.ai != null && actor.ai.currentAction != null) actionDebug = actor.ai.currentAction.preset.name + " (" + actor.ai.currentAction.goal.preset.name + ")";

            string trespassDebug = string.Empty;

            if (actor.ai != null && actor.ai.currentAction != null)
            {
                if (actor.ai.currentAction.goal.passedInteractable != null && actor.ai.currentAction.goal.passedInteractable.node != null)
                {
                    bool tp = actor.IsTrespassing(actor.ai.currentAction.goal.passedInteractable.node.room, out _, out trespassDebug);
                    trespassDebug = "Trespass goal location (interactable): " + tp + " " + trespassDebug;
                }
                else if (actor.ai.currentAction.goal.roomLocation != null)
                {
                    bool tp = actor.IsTrespassing(actor.ai.currentAction.goal.roomLocation, out _, out trespassDebug);
                    trespassDebug = "Trespass goal location (room): " + tp + " " + trespassDebug;
                }
                else if (actor.ai.currentAction.goal.passedNode != null)
                {
                    bool tp = actor.IsTrespassing(actor.ai.currentAction.goal.passedNode.room, out _, out trespassDebug);
                    trespassDebug = "Trespass goal location (node): " + tp + " " + trespassDebug;
                }

                for (int i = 0; i < actor.ai.currentAction.goal.actions.Count; i++)
                {
                    NewAIAction act = actor.ai.currentAction.goal.actions[i];

                    if(!act.insertedAction && act.node != null)
                    {
                        string actD = string.Empty;
                        bool tp2 = actor.IsTrespassing(act.node.room, out _, out actD);
                        trespassDebug += ", Trespass action " + act.preset.name + " location (node): " + tp2 + " " + actD;
                        break;
                    }
                }
            }

            Game.Log("Debug: Knock by " + actor.name + " with urgency: " + urgencyValue + " at " + actor.currentRoom.name + ", " + actor.currentGameLocation.name + " - door to " + door.wall.node.gameLocation.name + " action: " + actionDebug + " trespass: " + trespassDebug );
        }

        StartCoroutine(DoorKnockSounds(door, actor, urgencyValue, knocks));

        AddDoorKnockAttempt(door, actor as Human);
    }

    //Moved this out of the door class so it can be active any time...
    IEnumerator DoorKnockSounds(NewDoor door, Actor actor, float nextUrgency, int knocks = 2)
    {
        door.knockingInProgress = true;

        if (nextUrgency <= 1.5f)
        {
            door.doorInteractable.actionAudioEventOverrides[RoutineControls.Instance.knockOnDoor] = door.preset.audioKnockLight;
        }
        else if (nextUrgency > 1.5f && nextUrgency < 3.5f)
        {
            door.doorInteractable.actionAudioEventOverrides[RoutineControls.Instance.knockOnDoor] = door.preset.audioKnockMed;
        }
        else
        {
            door.doorInteractable.actionAudioEventOverrides[RoutineControls.Instance.knockOnDoor] = door.preset.audioKnockHeavy;
        }

        //Force update of interaction text
        if(actor.isPlayer) InteractionController.Instance.UpdateInteractionText();

        //Volume and quickness increases with attempts
        float urgency = Mathf.Clamp01(nextUrgency / 8f);
        float vol = Mathf.Lerp(0.9f, 1f, urgency);
        float delay = Mathf.Lerp(0.26f, 0.22f, urgency);

        AudioEvent knockAudio = door.preset.audioKnockLight;

        if (urgency > 0.33f)
        {
            knockAudio = door.preset.audioKnockMed;

            if (urgency > 0.66f)
            {
                knockAudio = door.preset.audioKnockHeavy;
            }
        }

        //Trigger knock in address controller (other side to actor)
        if (door.wall.otherWall.node.gameLocation.thisAsAddress != null)
        {
            if (actor.currentRoom == door.wall.node.room)
            {
                door.wall.otherWall.node.gameLocation.thisAsAddress.OnDoorKnockByActor(door, urgency, actor);
            }
            else
            {
                door.wall.node.gameLocation.thisAsAddress.OnDoorKnockByActor(door, urgency, actor);
            }
        }

        while (knocks > 0 && door.isClosed)
        {
            //Play sound
            AudioController.Instance.PlayWorldOneShot(knockAudio, actor, door.parentedWall.node, door.doorInteractable.wPos, volumeOverride: vol, additionalSources: door.bothNodesForAudioSource);

            knocks--;
            yield return new WaitForSeconds(delay);
        }

        door.knockingInProgress = false;
    }

    public void SetJobDifficultyLevel(int newInt)
    {
        jobDifficultyLevel = newInt;
    }

    //public int GetJobDifficultyLevel()
    //{
    //    if (Game.Instance.forceSideJobDifficulty)
    //    {
    //        return Game.Instance.forcedJobDifficulty;
    //    }
    //    else return jobDifficultyLevel;
    //}

    //A cache system for generated graffiti
    public void AddToGraffitiCache(string obj, Material mat)
    {
        if (!graffitiCache.ContainsKey(obj))
        {
            if(graffitiCache.Count > 50)
            {
                try
                {
                    List<string> keys = new List<string>(graffitiCache.Keys);
                    graffitiCache.Remove(keys.FirstOrDefault());
                }
                catch
                {

                }
            }

            graffitiCache.Add(obj, mat);
        }
    }

    public void AddMotionTracker(Interactable newTracker, int range)
    {
        if (newTracker == null) return;

        if (!activeTrackers.ContainsKey(newTracker))
        {
            activeTrackers.Add(newTracker, new List<NewNode>());
        }
        else activeTrackers[newTracker].Clear();

        NewWall attachedWall = null;
        float closest = Mathf.Infinity;
        Vector3 dir = new Vector3(0, 0, -1);

        foreach(NewWall w in newTracker.node.walls)
        {
            float dist = Vector3.Distance(w.position, newTracker.wPos);

            if(dist < closest)
            {
                attachedWall = w;
                closest = dist;
                dir = -new Vector3(w.wallOffset.x, 0, w.wallOffset.y).normalized;
            }
        }

        activeTrackers[newTracker].Add(newTracker.node); //Add base node

        //For up to 3 additional nodes add coverage in that direction
        Vector3 searchCoord = newTracker.node.nodeCoord;
        NewNode prevNode = newTracker.node;

        for (int i = 0; i < range; i++)
        {
            searchCoord += new Vector3Int(Mathf.RoundToInt(dir.x), Mathf.RoundToInt(dir.z), 0);
            NewNode foundNode;

            if(PathFinder.Instance.nodeMap.TryGetValue(searchCoord, out foundNode))
            {
                //Must have access
                if(prevNode.accessToOtherNodes.ContainsKey(foundNode))
                {
                    if(prevNode.accessToOtherNodes[foundNode].accessType == NewNode.NodeAccess.AccessType.adjacent || prevNode.accessToOtherNodes[foundNode].accessType == NewNode.NodeAccess.AccessType.openDoorway || prevNode.accessToOtherNodes[foundNode].accessType == NewNode.NodeAccess.AccessType.streetToStreet)
                    {
                        if(!activeTrackers[newTracker].Contains(foundNode))
                        {
                            activeTrackers[newTracker].Add(foundNode); //Add base node
                        }

                        prevNode = foundNode;

                        continue;
                    }
                }
            }

            break; //If no node here end search
        }

        //Add nodes to hash set for quick checking
        foreach(NewNode n in activeTrackers[newTracker])
        {
            if(!trackedNodes.Contains(n))
            {
                trackedNodes.Add(n);

                //GameObject debug = Instantiate(PrefabControls.Instance.debugNodeDisplay);
                //debug.transform.position = n.position;
            }
        }

        Game.Log("Gameplay: Adding motion tracker to node " + newTracker.node + " at " + newTracker.node.gameLocation.name + " in the direction of " + dir + " : " + new Vector3(dir.x, dir.z, 0) + " covering " + activeTrackers[newTracker].Count + " nodes");
    }

    public void RemoveMotionTracker(Interactable newTracker)
    {
        if (newTracker == null) return;

        if (activeTrackers.ContainsKey(newTracker))
        {
            foreach (NewNode n in activeTrackers[newTracker])
            {
                //See if this node is tracked by another tracker...
                bool del = true;

                foreach (KeyValuePair<Interactable, List<NewNode>> pair in activeTrackers)
                {
                    if (pair.Key == newTracker) continue;

                    if (pair.Value.Contains(n))
                    {
                        del = false;
                        break;
                    }
                }

                if (del) trackedNodes.Remove(n);
            }

            activeTrackers.Remove(newTracker);
        }
    }

    public void AddProxyDetonator(Interactable newTracker, float range)
    {
        if (newTracker == null) return;
        Game.Log("Gameplay: Adding new proxy detonator " + newTracker.name + " with range " + range);

        newTracker.SetValue(GameplayControls.Instance.proxyGrenadeFuse);

        if (!proxyTrackers.ContainsKey(newTracker))
        {
            proxyTrackers.Add(newTracker, range);
        }
        else proxyTrackers[newTracker] = range;
    }

    public void RemoveProxyDetonator(Interactable newTracker)
    {
        newTracker.SetValue(GameplayControls.Instance.proxyGrenadeFuse);

        if (proxyTrackers.ContainsKey(newTracker))
        {
            proxyTrackers.Remove(newTracker);
        }
    }

    public void SetPlayerKnowsPassword(NewAddress newAddress)
    {
        if (newAddress == null) return;

        if(!playerKnowsPasswords.Contains(newAddress.id))
        {
            playerKnowsPasswords.Add(newAddress.id);
            MapController.Instance.AddUpdateCall(newAddress.mapButton); //Refresh map button
        }
    }

    //Process all 'art' that needs dynamic text appended
    public void ProcessDynamicTextImages()
    {
        foreach(ArtPreset art in Toolbox.Instance.allArt)
        {
            if(art.useDynamicText)
            {
                //Get the texture
                Texture2D tex = art.material.GetTexture("_BaseColorMap") as Texture2D;

                //Generate the text
                TextToImageController.TextToImageSettings newSettings = new TextToImageController.TextToImageSettings();
                newSettings.color = art.textColour;
                newSettings.textSize = art.textSize;
                newSettings.font = art.textFont;
                newSettings.enableProcessing = true;
                newSettings.useAlpha = true;
                newSettings.trim = true;
                newSettings.trimPadding = 4;

                if (art.dynamicTextSource == ArtPreset.DynamicTextSouce.blackMarketTraderPassword)
                {
                    NewAddress place = CityData.Instance.addressDirectory.Find(item => item.addressPreset != null && item.addressPreset.name == "BlackmarketTrader");
                    if (place != null) newSettings.textString = place.GetPassword();
                }
                else if(art.dynamicTextSource == ArtPreset.DynamicTextSouce.weaponsDealerPassword)
                {
                    NewAddress place = CityData.Instance.addressDirectory.Find(item => item.addressPreset != null && item.addressPreset.name == "WeaponsDealer");
                    if (place != null) newSettings.textString = place.GetPassword();
                }

                Texture2D generatedText = TextToImageController.Instance.CaptureTextToImage(newSettings);

                //Calculate the size of the new image
                int newWidth = Mathf.Max(tex.width, generatedText.width);
                int newHeight = tex.height + generatedText.height;

                Vector2 imageCoordinate = new Vector2(0, generatedText.height); //The text is always positioned below and cented to the image
                Vector2 textCoordinate = Vector2.zero;

                //If the image is wider than the text, start with the image at 0 and calculate the horizontal start coordinate of the text
                if(tex.width > generatedText.width)
                {
                    imageCoordinate.x = 0;
                    textCoordinate.x = Mathf.FloorToInt((tex.width - generatedText.width) * 0.5f);
                }
                else if(tex.width < generatedText.width)
                {
                    textCoordinate.x = 0;
                    imageCoordinate.x = Mathf.FloorToInt((generatedText.width - tex.width) * 0.5f);
                }
                //If they are equal, they both start @ 0

                Texture2D newTex = new Texture2D(newWidth, newHeight);
                newTex.filterMode = FilterMode.Point;

                //Start with clear
                for (int x = 0; x < newTex.width; x++)
                {
                    for (int y = 0; y < newTex.height; y++)
                    {
                        newTex.SetPixel(x, y, Color.clear);
                    }
                }

                //Write the image
                for (int x = 0; x < tex.width; x++)
                {
                    for (int y = 0; y < tex.height; y++)
                    {
                        Color writePixel = tex.GetPixel(x, y);
                        newTex.SetPixel((int)imageCoordinate.x + x, (int)imageCoordinate.y + y, writePixel);
                    }
                }

                //Write the text
                for (int x = 0; x < generatedText.width; x++)
                {
                    for (int y = 0; y < generatedText.height; y++)
                    {
                        Color writePixel = generatedText.GetPixel(x, y);
                        newTex.SetPixel((int)textCoordinate.x + x, (int)textCoordinate.y + y, writePixel);
                    }
                }

                newTex.Apply(); //Apply pixel data

                //Save material to dictionary
                Material newMat = Instantiate(art.material);
                newMat.SetTexture("_BaseColorMap", newTex);
                dynamicTextImages.Add(art, newMat);
            }
        }
    }

    public void AddNewDebt(Company company, int amount, int paymentExtra, int repayments)
    {
        Game.Log("Gameplay: Adding new player debt for " + company.name + " to the tune of " + amount + "...");
        LoanDebt newDebt = debt.Find(item => item.companyID == company.companyID);
        GameplayController.Instance.AddMoney(amount, true, "Loan");

        if (newDebt != null)
        {
            newDebt.debt += amount + paymentExtra;
            newDebt.payments += repayments;
        }
        else
        {
            newDebt = new LoanDebt { companyID = company.companyID, debt = amount + paymentExtra, payments = repayments };

            //Calculate next payment due
            float dueInHours = 24 - SessionData.Instance.decimalClock; //Remaining hours of today
            dueInHours += 24.01f; //Payment due by end of tomorrow
            newDebt.nextPaymentDueBy = SessionData.Instance.gameTime += dueInHours;

            debt.Add(newDebt);
        }
    }

    public void DebtPayment(Company company)
    {
        if (company == null) return;
        LoanDebt newDebt = debt.Find(item => item.companyID == company.companyID);

        if (newDebt != null)
        {
            int amount = newDebt.GetRepaymentAmount();
            newDebt.debt -= amount;
            GameplayController.Instance.AddMoney(-amount, true, "Debt payoff");

            Game.Log("Gameplay: Paying player debt for " + company.name + ": " + amount + " (" + newDebt.debt + " remaining)");

            if (newDebt.debt <= 0)
            {
                debt.Remove(newDebt);
            }
            else
            {
                //Calculate next payment due
                float dueInHours = 24 - SessionData.Instance.decimalClock; //Remaining hours of today
                dueInHours += 24.01f; //Payment due by end of tomorrow
                newDebt.nextPaymentDueBy = SessionData.Instance.gameTime += dueInHours;
            }
        }
    }

    public void ShortDebtPayment(Company company, int amount)
    {
        if (company == null) return;
        LoanDebt newDebt = debt.Find(item => item.companyID == company.companyID);

        if (newDebt != null)
        {
            newDebt.debt -= amount;
            GameplayController.Instance.AddMoney(-amount, true, "Debt payoff");

            Game.Log("Gameplay: Paying player debt for " + company.name + ": " + amount + " (" + newDebt.debt + " remaining)");

            if (newDebt.debt <= 0)
            {
                debt.Remove(newDebt);
            }
        }
    }

    public void AddHotelGuest(Human human, bool expensiveRoom)
    {
        if (human == null) return;
        if (human.currentBuilding == null) return;

        List<NewAddress> allRooms = CityData.Instance.addressDirectory.FindAll(item => item.building == human.currentBuilding && item.residence != null && item.residence.preset != null && item.residence.preset.isHotelRoom);
        
        //Remove already occupied rooms
        foreach(HotelGuest g in hotelGuests)
        {
            allRooms.RemoveAll(item => item.id == g.addID);
        }

        List<NewAddress> cheapRooms = new List<NewAddress>();
        List<NewAddress> expensiveRooms = new List<NewAddress>();

        //Calculate average land value of all hotel rooms to split them
        float roomAv = 0f; 

        foreach(NewAddress ad in allRooms)
        {
            roomAv += ad.normalizedLandValue;
        }

        roomAv /= (float)allRooms.Count;

        foreach (NewAddress ad in allRooms)
        {
            if(ad.normalizedLandValue <= roomAv)
            {
                cheapRooms.Add(ad);
            }
            else
            {
                expensiveRooms.Add(ad);
            }
        }

        Game.Log("Gameplay: There are " + cheapRooms.Count + " cheap rooms and " + expensiveRooms.Count + " expensive rooms...");

        if(expensiveRoom)
        {
            if(expensiveRooms.Count > 0)
            {
                AddHotelGuest(expensiveRooms[Toolbox.Instance.Rand(0, expensiveRooms.Count)], human, CityControls.Instance.hotelCostUpper);
            }
            else if (cheapRooms.Count > 0)
            {
                AddHotelGuest(cheapRooms[Toolbox.Instance.Rand(0, cheapRooms.Count)], human, CityControls.Instance.hotelCostUpper);
            }
        }
        else
        {
            if (cheapRooms.Count > 0)
            {
                AddHotelGuest(cheapRooms[Toolbox.Instance.Rand(0, cheapRooms.Count)], human, CityControls.Instance.hotelCostLower);
            }
            else if (expensiveRooms.Count > 0)
            {
                AddHotelGuest(expensiveRooms[Toolbox.Instance.Rand(0, expensiveRooms.Count)], human, CityControls.Instance.hotelCostLower);
            }
        }
    }

    public void AddHotelGuest(NewAddress address, Human human, int cost)
    {
        if (address == null) return;
        if (human == null) return;
 
        Game.Log("Gameplay: Adding new hotel geust; " + human.GetCitizenName() + " at room " + address.name + " for " + cost + " a night...");

        HotelGuest newGuest = new HotelGuest { addID = address.id, humanID = human.humanID, roomCost = cost, bill = cost, lastPayment = SessionData.Instance.gameTime, nextPayment = SessionData.Instance.gameTime + 24 };
        hotelGuests.Add(newGuest);

        human.AddLocationOfAuthorty(address);

        if(human.isPlayer)
        {
            Player.Instance.AddToKeyring(address, true);

            if(MapController.Instance.playerRoute == null) MapController.Instance.PlotPlayerRoute(address);
        }
    }

    public void RemoveHotelGuest(NewAddress address, Human human, bool removeKey = true)
    {
        if (address == null) return;
        if (human == null) return;

        Game.Log("Gameplay: Removing all hotel guests at; " + human.GetCitizenName() + " room " + address.name);

        hotelGuests.RemoveAll(item => item.addID == address.id && item.humanID == human.humanID);

        human.RemoveLocationOfAuthority(address);

        if (human.isPlayer && removeKey)
        {
            Player.Instance.RemoveFromKeyring(address);
        }
    }
}
