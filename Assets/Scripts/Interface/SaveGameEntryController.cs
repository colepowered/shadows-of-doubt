﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;

public class SaveGameEntryController : ButtonController
{
    public FileInfo info;
    public bool selected = false;
    public bool isInternal = false;

    public void Setup(FileInfo newInfo)
    {
        SetupReferences();
        info = newInfo;

        UpdateButtonText();
    }

    public override void UpdateButtonText()
    {
        //Is this a cloud save?
        if(info != null)
        {
            //string cloudStr = string.Empty;

            //if (Toolbox.Instance.devCloudSaveConfig != null)
            //{
            //    if (Toolbox.Instance.devCloudSaveConfig.path.Length > 0 && info.DirectoryName == Toolbox.Instance.devCloudSaveConfig.path)
            //    {
            //        cloudStr = "DevCloud: ";
            //    }
            //}

            //string[] elements = info.Name.Split('_');
            //string playerName = elements[0];
            //string cityName = "city";
            //if (elements.Length > 1) cityName = elements[1];
            //string creationTime = string.Empty;

            ////Parse time and turn it into a displayable format...
            //if (elements.Length > 3 && elements[3].Length > 4)
            //{
            //    creationTime = elements[3].Substring(0, elements[3].Length - 4);
            //}

            //if(creationTime.Length > 0)
            //{
            //    string[] timeSplit = creationTime.Split('-');
            //    if (timeSplit.Length >= 5) creationTime = timeSplit[0] + "/" + timeSplit[1] + "/" + timeSplit[2] + " " + timeSplit[3] + ":" + timeSplit[4];
            //}

            //text.text = cloudStr + playerName + " " + cityName + " (" + creationTime + ")";

            text.text = info.Name.Substring(0, info.Name.Length - info.Extension.Length);
        }
        else
        {
            text.text = Strings.Get("ui.interface", "New Save");
        }
    }

    public override void OnLeftClick()
    {
        MainMenuController.Instance.SelectNewSave(this);
        base.OnLeftClick();
    }
}
