﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class WindowRenameTitleController : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public InfoWindow window;
    public TMP_InputField inputField;

    //Click Settings
    private float lastLeftClick = 0f;
    private float lastRightClick = 0f;

    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        if(inputField.isFocused)
        {
            InterfaceController.Instance.AddMouseOverElement(this);
        }
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;
        InterfaceController.Instance.RemoveMouseOverElement(this);
    }

    void OnDestroy()
    {
        InterfaceController.Instance.RemoveMouseOverElement(this);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if ((Time.time - lastLeftClick) <= InterfaceControls.Instance.doubleClickDelay)
            {
                OnLeftClick();
                OnLeftDoubleClick();
                //Game.Log("Double Left Click: " + name);
            }
            else
            {
                OnLeftClick();
                //Game.Log("Left Click: " + name);
            }

            lastLeftClick = Time.time; // save the current time
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            if ((Time.time - lastRightClick) <= InterfaceControls.Instance.doubleClickDelay)
            {
                OnRightClick();
                OnRightDoubleClick();
                //Game.Log("Double Right Click: " + name);
            }
            else
            {
                OnRightClick();
                //Game.Log("Right Click: " + name);
            }

            lastRightClick = Time.time; // save the current time
        }
    }

    public virtual void OnLeftClick()
    {
        return;
    }

    public virtual void OnRightClick()
    {
        return;
    }

    public virtual void OnLeftDoubleClick()
    {
        window.Rename();
    }

    public virtual void OnRightDoubleClick()
    {
        return;
    }
}
