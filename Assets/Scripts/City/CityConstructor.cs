﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;
using TMPro;
using UnityEngine.EventSystems;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine.SceneManagement;

//A parent script that generates a city by calling all sub creation scripts in order.
//Script pass 1
public class CityConstructor : MonoBehaviour
{
    //The current city save file
    [System.NonSerialized]
    public CitySaveData currentData;
    [System.NonSerialized]
    public StateSaveData saveState;
    public bool generateNew = true; //True if generating a new city
    public bool isLoaded = false;

    //Save city
    public int saveChunk = 1;

    //Evidence Compile list
    public List<Evidence> evidenceToCompile = new List<Evidence>();

    //Load state
    public enum LoadState { parsingFile, setupCityBoundary, generateDistricts, generateBlocks, generateDensity, generateBuildings, generatePathfinding,
                            generateBlueprints, generateCompanies, connectRooms, generateCitizens, generateRelationships, gatherData, generateAirDucts, generateEvidence, generateInteriors,
                            prepareCitizens, loadObjects, finalizing, savingData, loadState, preSim, loadComplete };

    public LoadState loadState;
    private List<LoadState> allLoadStates = new List<LoadState>();

    public int loadCursor = 9999;
	public float loadingProgress = 0f;
    public int displayedProgress = 0;
    public bool stateComplete = false;
	public bool loadingOperationActive = false;

    public bool preSimActive = false;
    public bool preSimOccured = false; //True if the pre sim has happened

    //Useful reference for loading of levels
    public Dictionary<int, NewWall> loadingWallsReference = new Dictionary<int, NewWall>();
    public Dictionary<int, FurnitureLocation> loadingFurnitureReference = new Dictionary<int, FurnitureLocation>();

    //Force switch state updates: For sounds that need to be played when the interactable is created (do so after all nodes are created & player is placed)
    public List<Interactable> updateSwitchState = new List<Interactable>();

    private float timeStamp = 0f;

    //For measuring load times
    [NonSerialized]
    public CollectedLoadTimeInfo debugLoadTime = null;

    [System.Serializable]
    public class CollectedLoadTimeInfo
    {
        public string build;
        public string citySize;
        public bool generateNew = false;
        public Dictionary<LoadState, int> loadTimes = new Dictionary<LoadState, int>();

        //Room type decor generation times by room type
        public Dictionary<NewRoom, List<DecorClusterGenerationTimeInfo>> decorTimes = new Dictionary<NewRoom, List<DecorClusterGenerationTimeInfo>>();
    }

    [System.Serializable]
    public class DecorClusterGenerationTimeInfo
    {
        public FurnitureCluster cluster;
        public bool found;
        public float time;
    }

    [System.Serializable]
    public class DecorTotalTime
    {
        public NewRoom room;
        public float totalTime;
    }

    //Events
    public delegate void OnStartGame();
    public event OnStartGame OnGameStarted;

    public delegate void LoadFinalize();
    public event LoadFinalize OnLoadFinalize;

    //Singleton pattern
    private static CityConstructor _instance;
	public static CityConstructor Instance { get { return _instance; } }

	void Awake ()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }

        this.enabled = false;
	}

    //Generate & load a new city using data from the main menu
    public void GenerateNewCity()
    {
        Game.Log("Menu: Generating new city...");

        //Create a new data file for saving the city...
        currentData = new CitySaveData();
        currentData.build = Game.Instance.buildID;
        currentData.cityName = RestartSafeController.Instance.cityName;
        currentData.seed = RestartSafeController.Instance.seed;

        currentData.citySize = new Vector2(RestartSafeController.Instance.cityX, RestartSafeController.Instance.cityY);
        //currentData.populationAmount = (CityData.PopulationAmount)RestartSafeController.Instance.population;

        generateNew = true; //Set generate flag
        StartLoading();
    }

    //Load a save game
    public async void LoadSaveGame()
    {
        await LoadSaveStateFile();

        Game.Log("CityGen: Loading save game, the generated city this save uses is: " + saveState.cityShare);

        //Set player details from save state
        RestartSafeController.Instance.newGamePlayerFirstName = saveState.playerFirstName;
        RestartSafeController.Instance.newGamePlayerSurname = saveState.playerSurname;
        RestartSafeController.Instance.newGamePlayerGender = saveState.playerGender;
        RestartSafeController.Instance.newGamePartnerGender = saveState.partnerGender;
        RestartSafeController.Instance.newGamePlayerSkinTone = saveState.playerSkinColour;

        NewspaperController.Instance.currentState = saveState.newspaperState;

        //Does the city in this save game have a file already?
        //Check city files...
        FileInfo foundCityFile = null;

        //Get embedded files
        var embed = new DirectoryInfo(Application.streamingAssetsPath + "/Cities");
        List<FileInfo> embeddedFiles = embed.GetFiles("*.cit", SearchOption.AllDirectories).ToList();

        //Get legacy cities
        var legacy = new DirectoryInfo(Application.streamingAssetsPath + "/Legacy");
        embeddedFiles.AddRange(legacy.GetFiles("*.cit", SearchOption.AllDirectories).ToList());

        //Parse setup data from share code
        string pSeed = string.Empty;
        string pName = string.Empty;
        int pSizeX = 5;
        int pSizeY = 5;
        string pVersion;

        Toolbox.Instance.ParseShareCode(saveState.cityShare, out pName, out pSizeX, out pSizeY, out pVersion, out pSeed);

        foreach (FileInfo map in embeddedFiles)
        {
            string fileName = string.Empty;
            string fileSeed = string.Empty;
            int fileSizeX = -1;
            int fileSizeY = -1;
            string fileVersion = string.Empty;

            Toolbox.Instance.ParseShareCode(map.Name.Substring(0, map.Name.Length - map.Extension.Length), out fileName, out fileSizeX, out fileSizeY, out fileVersion, out fileSeed);

            if (fileSeed == pSeed && pName == fileName && fileSizeX == pSizeX && fileSizeY == pSizeY)
            {
                foundCityFile = map;
                break;
            }
        }

        if(foundCityFile == null)
        {
            var info = new DirectoryInfo(Application.persistentDataPath + "/Cities");
            List<FileInfo> createdFiles = info.GetFiles().ToList();

            foreach (FileInfo map in createdFiles)
            {
                if (map.Extension.ToLower() == ".cit" || map.Extension.ToLower() == ".citb")
                {
                    string fileName = string.Empty;
                    string fileSeed = string.Empty;
                    int fileSizeX = -1;
                    int fileSizeY = -1;
                    string fileVersion = string.Empty;

                    Toolbox.Instance.ParseShareCode(map.Name.Substring(0, map.Name.Length - map.Extension.Length), out fileName, out fileSizeX, out fileSizeY, out fileVersion, out fileSeed);

                    if (fileSeed == pSeed && pName == fileName && fileSizeX == pSizeX && fileSizeY == pSizeY)
                    {
                        foundCityFile = map;
                        break;
                    }
                }
            }
        }

        //We have a matching name. Parse the city file to check seed also...
        if(foundCityFile != null)
        {
            Game.Log("CityGen: Found matching city file at " + foundCityFile.FullName);
            RestartSafeController.Instance.loadCityFileInfo = foundCityFile;

            LoadCityStartNewGame();
        }
        //Can't find a matching file, generate it using the city share code...
        else
        {
            Game.Log("CityGen: Cannot find a matching city file, generating new one...");

            //Incompatible version dialog
            try
            {
                int selectedV = Toolbox.Instance.VersionToNumbers(pVersion);
                int lastCompatibleV = Toolbox.Instance.VersionToNumbers(Game.Instance.lastCompatibleCities);

                Game.Log("CityGen: Selected version: " + selectedV + ", last comaptible: " + lastCompatibleV);

                if (selectedV < lastCompatibleV)
                {
                    PopupMessageController.Instance.PopupMessage("incompatible_version", true, true, RButton: "Confirm");
                    PopupMessageController.Instance.OnLeftButton += IncompatibleVersionCancel;
                    PopupMessageController.Instance.OnRightButton += IncompatibleVersionConfirm;
                    return;
                }
            }
            catch
            {
                Game.LogError("CityGen: Unable to perform incompatible city version check...");
            }

            GenerateCityFromShareCode();
        }
    }

    public void IncompatibleVersionConfirm()
    {
        PopupMessageController.Instance.OnLeftButton -= IncompatibleVersionCancel;
        PopupMessageController.Instance.OnRightButton -= IncompatibleVersionConfirm;
        GenerateCityFromShareCode();
    }

    public void IncompatibleVersionCancel()
    {
        PopupMessageController.Instance.OnLeftButton -= IncompatibleVersionCancel;
        PopupMessageController.Instance.OnRightButton -= IncompatibleVersionConfirm;

        AudioController.Instance.StopAllSounds();
        UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
    }

    private void GenerateCityFromShareCode()
    {
        Game.Log("Menu: ...Continuing to generate a city using this version using the saved share code...");

        currentData = new CitySaveData();

        string parsedCityName = "New City";
        int parsedSizeX = 5;
        int parsedSizeY = 5;
        //int parsedPop = 4;
        string parsedSeed = "anfw2ajfal2w54f3o";
        string parsedBuild = string.Empty;

        //Parse setup data from share code
        Toolbox.Instance.ParseShareCode(saveState.cityShare, out parsedCityName, out parsedSizeX, out parsedSizeY, out parsedBuild, out parsedSeed);

        currentData.cityName = parsedCityName;
        currentData.citySize = new Vector2(parsedSizeX, parsedSizeY);
        //currentData.populationAmount = (CityData.PopulationAmount)parsedPop;
        currentData.seed = parsedSeed;
        currentData.build = parsedBuild;

        generateNew = true; //Set generate flag
        StartLoading();
    }

    //Load a city from file and start a new game
    public void LoadCityStartNewGame()
    {
        generateNew = false; //Set generate flag
        StartLoading();
    }

    public void StartLoading()
    {
        //Reset ID assigns
        NewRoom.assignRoomFloorID = 1;
        NewRoom.assignRoomID = 1;
        Interactable.worldAssignID = 10000000;
        NewWall.assignID = 1;
        NewBuilding.assignID = 1;
        NewAddress.assignID = 1;
        StreetController.assignID = 1;
        DistrictController.assignID = 1;
        GameplayController.Instance.printsLetterLoop = 0;
        SceneRecorder.assignCapID = 1;
        GameplayController.Instance.assignMessageThreadID = 1;
        GroupsController.assignID = 1;
        InterfaceController.assignStickyNoteID = 1;
        Case.assignCaseID = 1;
        Human.assignID = 2; //Player is ID 1
        Human.assignTraitID = 1;

        MeshPoolingController.Instance.roomMeshes = new Dictionary<NewRoom, MeshPoolingController.RoomMeshCache>();

        Game.Instance.sandboxMode = RestartSafeController.Instance.sandbox;

        MainMenuController.Instance.LoadTip();

        Toolbox.Instance.lastRandomNumberKey = string.Empty; //Reset last generated seed
        loadingWallsReference.Clear();
        loadingFurnitureReference.Clear();

        preSimActive = false;
        preSimOccured = false;

        //Remove debug objects
        if(Game.Instance.debugContainer != null)
        {
            Destroy(Game.Instance.debugContainer.gameObject);
        }

        MainMenuController.Instance.mouseOverText.text = string.Empty;

        //Get all load state enums
        allLoadStates = LoadState.GetValues(typeof(LoadState)).Cast<LoadState>().ToList();

        //Create a new class collecting timing info...
        if(Game.Instance.devMode && Game.Instance.collectDebugData)
        {
            debugLoadTime = new CollectedLoadTimeInfo();
            debugLoadTime.build = Game.Instance.buildID;
            debugLoadTime.generateNew = generateNew;
            debugLoadTime.citySize = CityData.Instance.citySize.ToString();
        }

        //Initiate the loading UI popup
        MainMenuController.Instance.loadingSlider.value = 0f;
        MainMenuController.Instance.loadingText.text = Strings.Get("ui.loading", "Generating") + "...";

        //Commence loading
        loadingOperationActive = false; //Loading has started

        //New loading state (the first one)
        loadCursor = 0;
        loadState = (LoadState)loadCursor; //Start with state zero
        MainMenuController.Instance.loadingText.text = Strings.Get("ui.loading", loadState.ToString());

        Game.Instance.playerFirstName = RestartSafeController.Instance.newGamePlayerFirstName;
        Game.Instance.playerSurname = RestartSafeController.Instance.newGamePlayerSurname;
        Game.Instance.playerGender = RestartSafeController.Instance.newGamePlayerGender;
        Game.Instance.partnerGender = RestartSafeController.Instance.newGamePartnerGender;
        Game.Instance.playerSkinColour = RestartSafeController.Instance.newGamePlayerSkinTone;
        Player.Instance.descriptors.skinColour = Game.Instance.playerSkinColour;

        //Set time
        if(Game.Instance.sandboxMode)
        {
            SessionData.Instance.SetGameTime(GameplayControls.Instance.startingYear, GameplayControls.Instance.startingMonth, GameplayControls.Instance.startingDate, GameplayControls.Instance.dayZero, Game.Instance.sandboxStartTime, GameplayControls.Instance.yearZeroLeapYearCycle);
        }
        else
        {
            ChapterPreset ch = ChapterController.Instance.allChapters[Game.Instance.loadChapter];
            SessionData.Instance.SetGameTime(ch.startingYear, ch.startingMonth, ch.startingDate, ch.dayZero, ch.startingHour, ch.yearZeroLeapYearCycle);
        }

        loadingProgress = 0f;
        displayedProgress = -1;
        stateComplete = false;

        this.enabled = true;
    }

    private Task loadFullCityDataTask = null;
	void Update()
	{
        //Pause load if popup
        if(PopupMessageController.Instance != null)
        {
            if (PopupMessageController.Instance.active) return;
        }

        //Load new tip
        if(MainMenuController.Instance.nextTipTimer <= 0f)
        {
            MainMenuController.Instance.LoadTip();
        }
        else
        {
            MainMenuController.Instance.nextTipTimer -= Time.deltaTime;
        }

        if (loadCursor < allLoadStates.Count) //While there is stuff to load...
		{
            //Set progress dialog
            MainMenuController.Instance.loadingSlider.value = loadingProgress;
            if(Mathf.Round(loadingProgress * 100f) != displayedProgress) SetLoadingText();

            if (!loadingOperationActive ||
                (loadFullCityDataTask != null &&
                loadFullCityDataTask.IsCompleted))
            {
                //Start a new timer for this load operation...
                if (Game.Instance.devMode && Game.Instance.collectDebugData && debugLoadTime != null)
                {
                    //Record up to pre-sim operation...
                    Game.Log("CityGen: Starting timer for load phase " + loadState.ToString() + " " + (int)loadState);
                    timeStamp = Time.realtimeSinceStartup;
                }

                //Parse file
                if (loadState == LoadState.parsingFile)
                {
                    if (!generateNew && loadFullCityDataTask == null)
                    {
                        loadFullCityDataTask = LoadFullCityDataAsync();
                    }

                    if ((loadFullCityDataTask == null && generateNew) ||
                        (loadFullCityDataTask != null && loadFullCityDataTask.IsCompleted))
                    {
                        loadFullCityDataTask = null;

                        //Load to city Data so we don't have to keep the save file in memory...
                        CityData.Instance.cityName = currentData.cityName;
                        CityData.Instance.citySize = currentData.citySize;
                        CityData.Instance.cityBuiltWith = currentData.build;
                        //CityData.Instance.populationAmount = currentData.populationAmount;
                        CityData.Instance.seed = currentData.seed;

                        Game.Log("CityGen: Loading city " + currentData.cityName + " " + currentData.build + "  with seed " + CityData.Instance.seed);

                        loadingProgress = 1f;
                        stateComplete = true;
                    }
                }
                //Set up city boundary
                else if (loadState == LoadState.setupCityBoundary)
                {
                    SetupCityBoundary();
                    loadingProgress = 1f;
                    stateComplete = true;
                }
                //District generation
                else if (loadState == LoadState.generateDistricts)
                {
                    DistrictsCreator.Instance.StartLoading();
                }
                //Blocks generation
                else if (loadState == LoadState.generateBlocks)
                {
                    //Not needed upon load; it's done when districts are loaded.
                    if(generateNew)
                    {
                        BlocksCreator.Instance.StartLoading();
                    }
                    else
                    {
                        loadingProgress = 1f;
                        stateComplete = true;
                    }
                }
                //Density generation
                else if (loadState == LoadState.generateDensity)
                {
                    //Not needed upon load; it's saved in tile data
                    if (generateNew)
                    {
                        DensityCreator.Instance.StartLoading();
                    }
                    else
                    {
                        loadingProgress = 1f;
                        stateComplete = true;
                    }
                }
                //Building generation
                else if (loadState == LoadState.generateBuildings)
                {
                    BuildingCreator.Instance.StartLoading();

                    //Create singletons
                    CityData.Instance.CreateSingletons();
                }
                //Pathfinding map generation
                else if (loadState == LoadState.generatePathfinding)
                {
                    PathFinder.Instance.CompilePathFindingMap();
                    loadingProgress = 1f;
                    stateComplete = true;
                }
                //Building blueprints generation
                else if (loadState == LoadState.generateBlueprints)
                {
                    BlueprintsCreator.Instance.StartLoading();
                }
                //Calculate land value, Create residence/companies
                else if (loadState == LoadState.generateCompanies)
                {
                    if(generateNew)
                    {
                        List<NewAddress> add = new List<NewAddress>(CityData.Instance.addressDirectory);

                        string seed = CityData.Instance.seed;

                        while(add.Count > 0)
                        {
                            int randomIndex = Toolbox.Instance.GetPsuedoRandomNumberContained(0, add.Count, seed, out seed);

                            NewAddress address = add[randomIndex];

                            address.CalculateLandValue();

                            //Assign an appropriate purpose & generate configs
                            address.AssignPurpose();

                            add.RemoveAt(randomIndex);
                        }
                    }

                    loadingProgress = 1f;
                    stateComplete = true;
                }
                //Connect rooms
                else if (loadState == LoadState.connectRooms)
                {
                    if(!generateNew)
                    {
                        //Make sure walls are parented
                        foreach (KeyValuePair<int, NewWall> pair in loadingWallsReference)
                        {
                            if(!loadingWallsReference.TryGetValue(pair.Value.otherWallID, out pair.Value.otherWall))
                            {
                                Game.LogError("Cannot find wall " + pair.Value.otherWallID + " in loading walls reference of " + loadingWallsReference.Count);
                            }

                            if (!loadingWallsReference.TryGetValue(pair.Value.parentWallID, out pair.Value.parentWall))
                            {
                                Game.LogError("Cannot find wall " + pair.Value.parentWallID + " in loading walls reference of " + loadingWallsReference.Count);
                            }

                            if (!loadingWallsReference.TryGetValue(pair.Value.childWallID, out pair.Value.childWall))
                            {
                                Game.LogError("Cannot find wall " + pair.Value.childWallID + " in loading walls reference of " + loadingWallsReference.Count);
                            }
                        }

                        //Set materials: This will spawn doors
                        foreach (NewAddress add in CityData.Instance.addressDirectory)
                        {
                            foreach (NewRoom roo in add.rooms)
                            {
                                foreach (NewNode nod in roo.nodes)
                                {
                                    //Set preset
                                    foreach (NewWall wal in nod.walls)
                                    {
                                        wal.SetDoorPairPreset(wal.preset, false, wal.preset.divider);
                                    }
                                }
                            }
                        }
                    }

                    //Wall setup for streets
                    foreach (StreetController st in CityData.Instance.streetDirectory)
                    {
                        GenerationController.Instance.UpdateWallsRoom(st.nullRoom);
                    }

                    //Get window UV data
                    foreach (NewBuilding building in CityData.Instance.buildingDirectory)
                    {
                        foreach (KeyValuePair<int, NewFloor> pair in building.floors)
                        {
                            pair.Value.AssignWindowUVData();
                        }
                    }

                    RoomsLoader.Instance.StartLoading();
                }
                //Generate citizens
                else if (loadState == LoadState.generateCitizens)
                {
                    //DEBUG: CHECK Entrances
                    foreach (NewGameLocation gl in CityData.Instance.gameLocationDirectory)
                    {
                        foreach (NewNode.NodeAccess acc in gl.entrances)
                        {
                            if (!acc.fromNode.room.entrances.Exists(item => item.fromNode == acc.fromNode && item.toNode == acc.toNode))
                            {
                                Game.LogError("An entrance in a gamelocation was not found in it's room!");
                            }
                        }
                    }

                    CitizenCreator.Instance.StartLoading();
                }
                //Generate relationships
                else if (loadState == LoadState.generateRelationships)
                {
                    if(generateNew)
                    {
                        RelationshipCreator.Instance.StartLoading();

                        Game.Log("CityGen: Load state key after relationships: " + Toolbox.Instance.lastRandomNumberKey);

                        GroupsController.Instance.CreateGroups(); //Do this after building initial relationships...
                    }
                    else
                    {
                        GroupsController.Instance.LoadGroups();

                        loadingProgress = 1f;
                        stateComplete = true;
                    }
                }
                //Gather data
                else if (loadState == LoadState.gatherData)
                {
                    GatherData();
                    loadingProgress = 1f;
                    stateComplete = true;
                }
                //Create air ducts
                else if (loadState == LoadState.generateAirDucts)
                {
                    AirDuctsCreator.Instance.StartLoading();
                }
                //Generate evidence
                else if (loadState == LoadState.generateEvidence)
                {
                    StartingEvidenceCreator.Instance.StartLoading();
                    //loadingProgress = 1f;
                    //stateComplete = true;
                }
                //Generate interiors
                else if (loadState == LoadState.generateInteriors)
                {
                    if (generateNew)
                    {
                        InteriorCreator.Instance.StartLoading();
                    }
                    else
                    {
                        //Load ownership
                        foreach(NewGameLocation ad in CityData.Instance.gameLocationDirectory)
                        {
                            foreach(NewRoom room in ad.rooms)
                            {
                                room.LoadOwners();
                            }
                        }

                        //Load company sales records and passed work positions
                        foreach (Company cc in CityData.Instance.companyDirectory)
                        {
                            cc.CreateItemSingletons();
                            cc.UpdatePassedWorkPosition();
                        }

                        //Load citizen favourites
                        foreach (CitySaveData.HumanCitySave humanData in CityConstructor.Instance.currentData.citizens)
                        {
                            Human created = null;

                            if (CityData.Instance.citizenDictionary.TryGetValue(humanData.humanID, out created))
                            {
                                created.LoadFavourites(humanData);
                            }
                        }

                        loadingProgress = 1f;
                        stateComplete = true;
                    }
                }
                //Prepare citizens
                else if (loadState == LoadState.prepareCitizens)
                {
                    foreach (Citizen cit in CityData.Instance.citizenDirectory)
                    {
                        cit.PrepForStart();
                    }

                    Player.Instance.PrepForStart();

                    loadingProgress = 1f;
                    stateComplete = true;
                }
                //Load objects
                else if (loadState == LoadState.loadObjects)
                {
                    if (!generateNew)
                    {
                        //Pre-load any references needed for objects setup here...
                        if (saveState != null)
                        {
                            SaveStateController.Instance.PreLoadCases(ref saveState);
                        }

                        ObjectsCreator.Instance.StartLoading();
                    }
                    else
                    {
                        loadingProgress = 1f;
                        stateComplete = true;
                    }
                }
                //Gather data
                else if (loadState == LoadState.finalizing)
                {
                    Finalized();

                    loadingProgress = 1f;
                    stateComplete = true;
                }
                else if (loadState == LoadState.savingData)
                {
                    if(generateNew)
                    {
                        StartCoroutine(SaveCityData());
                    }
                    else
                    {
                        loadingProgress = 1f;
                        stateComplete = true;
                    }
                }
                else if(loadState == LoadState.loadState)
                {
                    FinalizePostSave();

                    if (saveState != null)
                    {
                        SaveStateController.Instance.LoadSaveState(saveState);
                    }
                    else
                    {
                        //Setup inventory slots
                        UpgradeEffectController.Instance.OnSyncDiskChange(true);
                        FirstPersonItemController.Instance.SetSlotSize(GameplayControls.Instance.defaultInventorySlots + Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.increaseInventory)));

                        //New Game: Load chapter from debug
                        if (!Game.Instance.sandboxMode)
                        {
                            ChapterController.Instance.LoadChapter(ChapterController.Instance.allChapters[Game.Instance.loadChapter], true);
                        }
                        //Give player essential items
                        else
                        {
                            //Gadgets
                            //Player.Instance.home.PlaceObject(InteriorControls.Instance.printReader, Player.Instance, null, out _, null, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseOwned, forcedSecurity: 2);
                            //Player.Instance.home.PlaceObject(InteriorControls.Instance.handcuffs, Player.Instance, null, out _, null, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseOwned, forcedSecurity: 2);
                        }
                    }

                    //If sandbox, set up...
                    if (Game.Instance.sandboxMode && saveState == null)
                    {
                        //Turn on lights in player's apartment
                        if(Player.Instance.home != null)
                        {
                            foreach (NewRoom room in Player.Instance.home.rooms)
                            {
                                room.SetMainLights(true, "Set main lights on");

                                //Set unlocked doors
                                foreach (NewNode.NodeAccess acc in Player.Instance.home.entrances)
                                {
                                    if (acc.wall.door != null)
                                    {
                                        acc.wall.door.SetLocked(false, Player.Instance, false);
                                    }
                                }
                            }
                        }
                    }

                    loadingProgress = 1f;
                    stateComplete = true;
                }
                //Pre simulation for chapters
                else if (loadState == LoadState.preSim)
                {
                    //Only do presim if we're starting a new game...
                    if (!Game.Instance.sandboxMode && !preSimOccured && !preSimActive && saveState == null && ChapterController.Instance.loadedChapter != null && ChapterController.Instance.loadedChapter.usePreSimulation)
                    {
                        SetPreSim(true);
                    }
                    else
                    {
                        loadingProgress = 1f;
                        stateComplete = true;
                    }
                }
                else if(loadState == LoadState.loadComplete)
                {
                    //Ask about tutorial text
                    if (saveState == null && ((ChapterController.Instance.loadedChapter != null && ChapterController.Instance.loadedChapter.askToEnableTutorial) || !PlayerPrefsController.Instance.playedBefore || SessionData.Instance.enableTutorialText))
                    {
                        PopupMessageController.Instance.PopupMessage("TutorialEnable", true, true, "Off", "On", anyButtonClosesMsg: true, newPauseState: PopupMessageController.AffectPauseState.no);
                        PopupMessageController.Instance.OnLeftButton += DisableTutorial;
                        PopupMessageController.Instance.OnRightButton += EnableTutorial;
                    }
                    else
                    {
                        loadingProgress = 1f;
                        stateComplete = true;
                    }
                }

                loadingOperationActive = true;
            }

            //Switch to next loading state
            if (stateComplete && loadCursor < allLoadStates.Count)
            {
                //Record the time take to load...
                if (Game.Instance.devMode && Game.Instance.collectDebugData && debugLoadTime != null)
                {
                    int seconds = Mathf.CeilToInt(Time.realtimeSinceStartup - timeStamp);
                    Game.Log("CityGen: Completed load phase " + loadState.ToString() + " " + (int)loadState + " in " + seconds + " seconds...");

                    //Record up to pre-sim operation...
                    if (!debugLoadTime.loadTimes.ContainsKey(loadState))
                    {
                        debugLoadTime.loadTimes.Add(loadState, seconds); //Record in seconds, this should be accurate enough
                    }
                    else debugLoadTime.loadTimes[loadState] = seconds;
                }

                loadCursor++;

                if (loadCursor < allLoadStates.Count)
                {
                    loadState = (LoadState)loadCursor;
                    Game.Log("CityGen: Load state " + loadCursor + " (" + loadState + "). Random key: " + Toolbox.Instance.lastRandomNumberKey);
                }

                loadingProgress = 0f;
                stateComplete = false;
                loadingOperationActive = false;
                SetLoadingText();
            }
		}
        else
        {
            Game.Log("CityGen: Loading cycle is complete!");

            if (Game.Instance.devMode && Game.Instance.collectDebugData && debugLoadTime != null)
            {
                WriteSavingTimings(ref debugLoadTime);
                WriteRoomDecorTimings(ref debugLoadTime);
                WriteGeneratedObjectDetails();
            }

            //Fire load finalize event: This needs to come after data save
            if (OnLoadFinalize != null)
            {
                OnLoadFinalize();
            }

            //Loading complete
            loadingOperationActive = false;
            isLoaded = true;
            MainMenuController.Instance.loadingText.text = Strings.Get("ui.loading", "Entering") + " " + currentData.cityName + "...";

            //Game is loaded!
            //Active start button
            MainMenuController.Instance.loadingSlider.value = 1f;

            //Disable to stop update function
            this.enabled = false;

            StartGame();

            saveState = null; //Remove current save state
        }
    }

    private void WriteSavingTimings(ref CollectedLoadTimeInfo info)
    {
        if (!info.generateNew) return; //Only write freshly generated timings

        string path = Application.persistentDataPath + "/generation_timing_" + info.build + info.citySize + ".txt";
        string[] existingContents = null;
        List<string> toWrite = new List<string>();

        try
        {
            existingContents = System.IO.File.ReadAllLines(path);
            Game.Log("CityGen: Read existing load time contents: " + existingContents.Length + " lines...");

            //Get all times from existing document...
            for (int i = 0; i < existingContents.Length; i++)
            {
                string line = existingContents[i];
                string[] split = line.Split(new char[] { ' ', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);
                float total = 0f;
                int entries = 0;

                if (split.Length > 2)
                {
                    //Parse the load state
                    string loadState = split[0].Substring(0, split[0].Length - 1);
                    Game.Log("CityGen: Parsing load state " + loadState + "...");

                    int lsIndex = allLoadStates.FindIndex(item => item.ToString() == loadState);

                    if(lsIndex > -1)
                    {
                        LoadState ls = allLoadStates[lsIndex];
                        Game.Log("CityGen: Parsed load state " + ls.ToString() + " from " + loadState);

                        //Parse the total
                        if (float.TryParse(split[3], out total))
                        {
                            total += info.loadTimes[ls];
                        }
                        else Game.Log("CityGen: Unable to parse float from " + split[1]);

                        //Parse the number of entries
                        if (int.TryParse(split[2], out entries))
                        {
                            entries++;
                        }
                        else Game.Log("CityGen: Unable to parse int from " + split[2]);

                        float newAverage = (float)total / (float)entries;
                        Game.Log("CityGen: New average for " + loadState + " from " + entries +" entries: " + newAverage + " (this: " + info.loadTimes[ls] + ", previous: " + split[1] + ")");

                        string newLine = ls.ToString() + ": " + newAverage + " (" + entries + " (" + total + "))";
                        toWrite.Add(newLine);
                    }
                    else
                    {
                        Game.Log("CityGen: Unable to get index in existing for " + loadState);
                    }
                }
            }
        }
        catch
        {
            Game.Log("CityGen: No existing load time contents, creating new file at " + path);

            foreach(KeyValuePair<LoadState, int> pair in info.loadTimes)
            {
                string newLine = pair.Key.ToString() + ": " + pair.Value.ToString() + " (1 (" + pair.Value.ToString() + "))";
                toWrite.Add(newLine);
            }
        }

        StreamWriter writer = new StreamWriter(path, false);
        Game.Log("CityGen: Writing " + toWrite.Count + " lines to " + path);

        for (int i = 0; i < toWrite.Count; i++)
        {
            writer.WriteLine(toWrite[i]);
        }

        writer.Close();
    }

    private void WriteRoomDecorTimings(ref CollectedLoadTimeInfo info)
    {
        if (!generateNew) return;

        string path = Application.persistentDataPath + "/decortime_info.txt";
        List<string> toWrite = new List<string>();

        List<DecorTotalTime> totalTimes = new List<DecorTotalTime>();
        Dictionary<NewRoom, List<string>> toWriteRoom = new Dictionary<NewRoom, List<string>>();

        foreach(KeyValuePair<NewRoom, List<DecorClusterGenerationTimeInfo>> pair in info.decorTimes)
        {
            try
            {
                DecorTotalTime tot = new DecorTotalTime();
                tot.room = pair.Key;

                toWriteRoom.Add(pair.Key, new List<string>());

                toWriteRoom[pair.Key].Add(string.Empty);
                toWriteRoom[pair.Key].Add(string.Empty);
                toWriteRoom[pair.Key].Add(string.Empty);
                toWriteRoom[pair.Key].Add("-----Furniture cluster timings for room " + pair.Key.roomID + " (" + pair.Key.nodes.Count + " nodes)-----");
                toWriteRoom[pair.Key].Add(string.Empty);

                pair.Value.Sort((p2, p1) => p1.time.CompareTo(p2.time)); //Highest time first

                foreach (DecorClusterGenerationTimeInfo t in pair.Value)
                {
                    toWriteRoom[pair.Key].Add(t.cluster.name + ": " + t.time + " Found: " + t.found);
                    tot.totalTime += t.time;
                }

                totalTimes.Add(tot);
            }
            catch
            {
                continue;
            }
        }

        totalTimes.Sort((p2, p1) => p1.totalTime.CompareTo(p2.totalTime)); //Highest time first

        StreamWriter writer = new StreamWriter(path, false);
        Game.Log("CityGen: Writing " + toWrite.Count + " lines to " + path);

        foreach (DecorTotalTime d in totalTimes)
        {
            if(toWriteRoom.ContainsKey(d.room))
            {
                for (int i = 0; i < toWriteRoom[d.room].Count; i++)
                {
                    writer.WriteLine(toWriteRoom[d.room][i]);
                }
            }
        }

        writer.Close();
    }

    private void WriteGeneratedObjectDetails()
    {
        if (!generateNew) return;

        string path = Application.persistentDataPath + "/object_info.txt";
        List<string> toWrite = new List<string>();

        toWrite.Add(CityData.Instance.seed);
        toWrite.Add(CityData.Instance.interactableDirectory.Count + " objects...");
        toWrite.Add("World id assign: " + Interactable.worldAssignID);
        toWrite.Add(string.Empty);

        Dictionary<InteractablePreset, int> objectCounts = new Dictionary<InteractablePreset, int>();

        CityData.Instance.interactableDirectory.Sort((p2, p1) => p1.preset.presetName.CompareTo(p2.preset.presetName));

        foreach(Interactable i in CityData.Instance.interactableDirectory)
        {
            if(!objectCounts.ContainsKey(i.preset))
            {
                objectCounts.Add(i.preset, 0);
            }

            objectCounts[i.preset]++;
        }

        foreach(KeyValuePair<InteractablePreset, int> pair in objectCounts)
        {
            toWrite.Add(pair.Key.name + ": " + pair.Value);
        }

        toWrite.Add(string.Empty);
        toWrite.Add(string.Empty);

        int furnCount = 0;
        Dictionary<FurnitureClass, int> furnCounts = new Dictionary<FurnitureClass, int>();

        foreach (NewRoom r in CityData.Instance.roomDirectory)
        {
            foreach(FurnitureLocation f in r.individualFurniture)
            {
                if (!furnCounts.ContainsKey(f.furnitureClasses[0]))
                {
                    furnCounts.Add(f.furnitureClasses[0], 0);
                }

                furnCounts[f.furnitureClasses[0]]++;

                furnCount++;
            }
        }

        toWrite.Add(furnCount + " furniture...");
        toWrite.Add(string.Empty);

        foreach (KeyValuePair<FurnitureClass, int> pair in furnCounts)
        {
            toWrite.Add(pair.Key.name + ": " + pair.Value);
        }

        toWrite.Add(string.Empty);
        toWrite.Add(string.Empty);

        for (int i = 0; i < CityData.Instance.roomDirectory.Count; i++)
        {
            NewRoom r = CityData.Instance.roomDirectory[i];
            toWrite.Add("Room " + r.roomID + " " + r.GetName() + " ended with " + r.seed + ", furniture placed: " + r.furnitureAssignID + " & " + r.interactableAssignID + " objects placed. Clusters: " + r.clustersPlaced + ", Objects (pool: " + r.poolSizeOnPlacement + " with key " + r.palcementKey1 + " -> " + r.palcementKey2 + " -> " + r.palcementKey3 + " -> " + r.palcementKey4 + " -> (" + r.palcementKey51 + " -> " + r.palcementKey52 + ") -> " + r.palcementKey5 + " -> " + r.palcementKey6 + ", original " + r.keyAtStart + "): " + r.itemsPlaced);
        }

        toWrite.Add(string.Empty);
        toWrite.Add(string.Empty);

        for (int i = 0; i < CityData.Instance.addressDirectory.Count; i++)
        {
            NewAddress r = CityData.Instance.addressDirectory[i];
            toWrite.Add("Address " + r.id + " " + r.name + " ended with seed " + r.seed);
        }

        toWrite.Add(string.Empty);
        toWrite.Add(string.Empty);

        for (int i = 0; i < CityData.Instance.streetDirectory.Count; i++)
        {
            StreetController r = CityData.Instance.streetDirectory[i];
            toWrite.Add("Street " + r.streetID + " " + r.name + " ended with seed " + r.seed);
        }

        StreamWriter writer = new StreamWriter(path, false);
        Game.Log("CityGen: Writing " + toWrite.Count + " lines to " + path);

        for (int i = 0; i < toWrite.Count; i++)
        {
            writer.WriteLine(toWrite[i]);
        }

        writer.Close();
    }

    private void SetLoadingText()
    {
        MainMenuController.Instance.loadingText.text = Strings.Get("ui.loading", loadState.ToString());
        displayedProgress = Mathf.RoundToInt(loadingProgress * 100f);
    }

	void SetupCityBoundary()
	{
        Game.Log("CityGen: Setup city boundary...");

        ////Set vacancy chance according to population enum
        //if (CityData.Instance.populationAmount == CityData.PopulationAmount.ultraLow)
        //{
        //    CityData.Instance.populationMultiplier = 0.5f;
        //}
        //else if (CityData.Instance.populationAmount == CityData.PopulationAmount.veryLow)
        //{
        //    CityData.Instance.populationMultiplier = 0.5f;
        //}
        //else if (CityData.Instance.populationAmount == CityData.PopulationAmount.low)
        //{
        //    CityData.Instance.populationMultiplier = 0.6f;
        //}
        //else if (CityData.Instance.populationAmount == CityData.PopulationAmount.medium)
        //{
        //    CityData.Instance.populationMultiplier = 0.7f;
        //}
        //else if (CityData.Instance.populationAmount == CityData.PopulationAmount.high)
        //{
        //    CityData.Instance.populationMultiplier = 0.9f;
        //}
        //else if (CityData.Instance.populationAmount == CityData.PopulationAmount.veryHigh)
        //{
        //    CityData.Instance.populationMultiplier = 0.95f;
        //}
        //else if (CityData.Instance.populationAmount == CityData.PopulationAmount.maximum)
        //{
        //    CityData.Instance.populationMultiplier = 1f;
        //}

        CityData.Instance.populationMultiplier = 1f;

        PathFinder.Instance.SetDimensions(); //Make sure dimensions are set

		//Set the city bounary positions
		CityData.Instance.boundaryLeft = (CityData.Instance.citySize.x * -0.5f) * CityControls.Instance.cityTileSize.x;
        CityData.Instance.boundaryRight = (CityData.Instance.citySize.x * 0.5f) * CityControls.Instance.cityTileSize.x;
        CityData.Instance.boundaryUp = (CityData.Instance.citySize.y * 0.5f) * CityControls.Instance.cityTileSize.y;
        CityData.Instance.boundaryDown = (CityData.Instance.citySize.y * -0.5f) * CityControls.Instance.cityTileSize.y;

        //Setup tile data
        if (generateNew)
        {
            for (int i = -1; i < CityData.Instance.citySize.x + 1; i++)
            {
                for (int u = -1; u < CityData.Instance.citySize.y + 1; u++)
                {
                    //Load actual city
                    if (i >= 0 && u >= 0 && i < CityData.Instance.citySize.x && u < CityData.Instance.citySize.y)
                    {
                        //Create city tile
                        GameObject newTile = Instantiate(PrefabControls.Instance.cityTile, PrefabControls.Instance.cityContainer.transform);
                        CityTile cityTile = newTile.GetComponent<CityTile>();

                        Vector2Int cityCoord = new Vector2Int(i, u);
                        cityTile.Setup(cityCoord);
                    }
                    //else
                    //{
                    //    //Do boundary stuff
                    //    GameObject newTile = Instantiate(PrefabControls.Instance.boundaryBlocker, PrefabControls.Instance.cityContainer.transform);

                    //    //Position object in game world
                    //    //thistotaltiles - totaltiles/2 + halftile
                    //    float loadPosX = i * CityControls.Instance.cityTileSize.x - ((CityData.Instance.citySize.x * 0.5f) * CityControls.Instance.cityTileSize.x) + (CityControls.Instance.cityTileSize.x * 0.5f);
                    //    float loadPosY = u * CityControls.Instance.cityTileSize.y - ((CityData.Instance.citySize.y * 0.5f) * CityControls.Instance.cityTileSize.y) + (CityControls.Instance.cityTileSize.y * 0.5f);
                    //    newTile.transform.position = new Vector3(loadPosX, 0, loadPosY);
                    //}
                }
            }
        }
        else
        {
            foreach(CitySaveData.CityTileCitySave tile in currentData.cityTiles)
            {
                //Create city tile
                GameObject newTile = Instantiate(PrefabControls.Instance.cityTile, PrefabControls.Instance.cityContainer.transform);
                CityTile cityTile = newTile.GetComponent<CityTile>();
                cityTile.LoadTileOnly(tile);
            }
        }
	}

    void GatherData()
    {
        if(generateNew)
        {
            //Gather district data
            //Sort by largest size first...
            CityData.Instance.districtDirectory.Sort();
            CityData.Instance.districtDirectory.Reverse();

            foreach (DistrictController dist in CityData.Instance.districtDirectory)
            {
                dist.PopulateData(); //This will also name the district
            }

            //Name streets
            foreach(StreetController str in CityData.Instance.streetDirectory)
            {
                str.UpdateName();
            }

            //Name buildings
            foreach(NewBuilding build in CityData.Instance.buildingDirectory)
            {
                build.UpdateName();
            }

            //Calculate room ownership
            foreach(NewAddress add in CityData.Instance.addressDirectory)
            {
                add.CalculateRoomOwnership();
            }
        }

        //Create neon signs
        foreach(NewAddress add in CityData.Instance.addressDirectory)
        {
            //Should be the same every time
            if(add.featuresNeonSignageHorizontal)
            {
                add.CreateSignageHorizontal();
            }

            if (add.featuresNeonSignageVertical)
            {
                add.CreateSignageVertical();
            }
        }

        //Spawn street calbes
        foreach(NewBuilding building in CityData.Instance.buildingDirectory)
        {
            building.SpawnStreetCables();
        }

        //Remove unused unemployment entries
        for (int i = 0; i < CityData.Instance.unemployedDirectory.Count; i++)
        {
            if (CityData.Instance.unemployedDirectory[i].employee == null)
            {
                CityData.Instance.unemployedDirectory.RemoveAt(i);
                i--;
            }
        }

        //Generate average statistics
        CityData.Instance.averageShoeSize = CityData.Instance.averageShoeSize / CityData.Instance.citizenDirectory.Count;

        //Perform open/close check on companies
        for (int i = 0; i < CityData.Instance.companyDirectory.Count; i++)
        {
            CityData.Instance.companyDirectory[i].OpenCloseCheck();
        }

        //Should street lights be off? (Lights are on by default)
        //if (SessionData.Instance.decimalClock >= CityControls.Instance.lightsOff.x && SessionData.Instance.decimalClock <= CityControls.Instance.lightsOn.x)
        //{
        //    foreach(Interactable slc in CityData.Instance.automaticLightDirectory)
        //    {
        //        slc.SetSwitchState(false, null);
        //    }
        //}

        if(!generateNew && currentData.playersApartment > -1)
        {
            NewAddress p = CityData.Instance.addressDirectory.Find(item => item.id == CityConstructor.Instance.currentData.playersApartment);

            if(p != null)
            {
                //Load player's apartment
                Game.Log("CityGen: Loading player's apartment from city data (" + currentData.playersApartment +": " + p.name + ")");
                Player.Instance.home = p;
                Player.Instance.residence = Player.Instance.home.residence;
                Player.Instance.SetResidence(Player.Instance.home.residence);
            }
        }

        //Load player skin colour materials
        FirstPersonItemController.Instance.GenerateSkinColourMaterials();
    }

    void Finalized()
    {
        Game.Log("CityGen: Set scene dirty");
        SessionData.Instance.dirtyScene = true;

        //Create city directory text
        CityData.Instance.CreateCityDirectory();

        //Pick passwords
        if (generateNew)
        {
            PipeConstructor.Instance.GeneratePipes(); //Generate pipe systems

            foreach (Citizen cit in CityData.Instance.citizenDirectory)
            {
                cit.PickPassword();
            }

            foreach(NewAddress ad in CityData.Instance.addressDirectory)
            {
                ad.PickPassword();

                foreach(NewRoom room in ad.rooms)
                {
                    room.PickPassword();

                    //Clear furntiure cache
                    room.pickFurnitureCache = null;
                }
            }

            Game.Log("CityGen: Last random key: " + CityData.Instance.seed + " = " + Toolbox.Instance.lastRandomNumberKey);
            Game.Log("CityGen: Generated " + CityData.Instance.interactableDirectory.Count + " objects");
        }
        else
        {
            //Load multi page evidence state
            foreach (CitySaveData.EvidenceStateSave ev in currentData.multiPage)
            {
                Evidence found = null;

                if (Toolbox.Instance.TryGetEvidence(ev.id, out found))
                {
                    if (ev.mpContent != null && ev.mpContent.Count > 0)
                    {
                        EvidenceMultiPage mp = found as EvidenceMultiPage;
                        //List<EvidenceMultiPage.MultiPageContent> metaObjectContent = ev.mpContent.FindAll(item => item.meta != null);
                        //Game.Log("Loading multipage content: " + ev.mpContent.Count + " (" + metaObjectContent.Count + " meta objects)");

                        if (mp != null)
                        {
                            mp.pageContent = new List<EvidenceMultiPage.MultiPageContent>(ev.mpContent);
                            mp.SetPage(ev.page, false);
                        }
                    }
                }
                else
                {
                    Game.LogError("Unable to load multi page evidence " + ev.id);
                }
            }

            //Load pipes
            PipeConstructor.Instance.generated = new List<PipeConstructor.PipeGroup>(currentData.pipes);
        }

        //Keys to the city
        if (Game.Instance.keysToTheCity)
        {
            foreach (NewAddress ad in CityData.Instance.addressDirectory)
            {
                Player.Instance.AddToKeyring(ad, false);

                if (ad.passcode.used)
                {
                    GameplayController.Instance.AddPasscode(ad.passcode, false);
                }

                foreach (NewRoom room in ad.rooms)
                {
                    if (room.passcode.used)
                    {
                        GameplayController.Instance.AddPasscode(room.passcode, false);
                    }
                }
            }

            foreach (Human h in CityData.Instance.citizenDirectory)
            {
                GameplayController.Instance.AddPasscode(h.passcode, false);
            }
        }

        //Make sure player has key for home
        if(Player.Instance.home != null)
        {
            Player.Instance.AddToKeyring(Player.Instance.home, false);
        }

        //Load pipes
        foreach (PipeConstructor.PipeGroup p in PipeConstructor.Instance.generated)
        {
            p.AddToRoomsAsReferences();
            p.Spawn();
        }

        //Setup scene recorder covered areas
        foreach(SceneRecorder r in CitizenBehaviour.Instance.sceneRecorders)
        {
            r.RefreshCoveredArea();
        }

        //Discover all
        if (Game.Instance.discoverAllRooms)
        {
            foreach (NewRoom room in CityData.Instance.roomDirectory)
            {
                room.SetExplorationLevel(2);

                //Discover ducts
                foreach (AirDuctGroup ductGrp in room.ductGroups)
                {
                    foreach (AirDuctGroup.AirDuctSection duct in ductGrp.airDucts)
                    {
                        duct.SetDiscovered(true);
                    }
                }
            }
        }

        //Setup upgrades
        UpgradesController.Instance.Setup();

        //Update matches
        GameplayController.Instance.UpdateMatches();

        //Birthday check for citizens
        foreach (Citizen cit in CityData.Instance.citizenDirectory)
        {
            cit.BirthdayCheck();
        }

        //Load broadcast schedule
        SessionData.Instance.currentBroadcastSchedule = SessionData.Instance.defaultBroadcastSchedule;

        if (ChapterController.Instance.loadedChapter != null && ChapterController.Instance.loadedChapter.broadcastSchedule != null)
        {
            SessionData.Instance.currentBroadcastSchedule = ChapterController.Instance.loadedChapter.broadcastSchedule;
        }

        //Generate fake sales data
        if (saveState == null)
        {
            MurderController.Instance.maxDifficultyLevel = 1; //Reset murder difficulty level

            foreach (Company c in CityData.Instance.companyDirectory)
            {
                c.GenerateFakeSalesRecords();
            }

            GameplayController.Instance.money = 0;
            GameplayController.Instance.lockPicks = 0;

            if(Game.Instance.sandboxMode)
            {
                GameplayController.Instance.SetMoney(Game.Instance.sandboxStartingMoney);
                GameplayController.Instance.AddLockpicks(Game.Instance.sandboxStartingLockpicks, false);
            }
            else
            {
                GameplayController.Instance.SetMoney(GameplayControls.Instance.startingMoney);
                GameplayController.Instance.SetLockpicks(0);
            }
        }

        //Set first person skin colour
        FirstPersonItemController.Instance.SetFirstPersonSkinColour();

        //Update switch states
        foreach (Interactable i in updateSwitchState)
        {
            i.SetSwitchState(i.sw0, null, true, true);
            i.SetCustomState1(i.sw1, null, true, true);
            i.SetCustomState2(i.sw2, null, true, true);
            i.SetCustomState3(i.sw3, null, true, true);
            i.SetLockedState(i.locked, null, true, true);
            i.SetPhysicsPickupState(i.phy, null, true, true);
        }

        updateSwitchState.Clear();

        //Generate dynamic text images
        GameplayController.Instance.ProcessDynamicTextImages();
    }

    void FinalizePostSave()
    {
        //Set player to homeless (must happen after save of city data)
        if (Game.Instance.sandboxMode && !Game.Instance.sandboxStartingApartment)
        {
            Game.Log("CityGen: Setting player as having no apartment...");
            Player.Instance.SetResidence(null);

            //Recreate city directory text
            CityData.Instance.CreateCityDirectory();
        }

        //Find player starting position
        if (Player.Instance.home != null)
        {
            SessionData.Instance.startingNode = Player.Instance.FindSafeTeleport(Player.Instance.home);
        }
        else
        {
            List<StreetController> validStreets = CityData.Instance.streetDirectory.FindAll(item => item.entrances.Count > 0 && item.rooms.Count > 0 && item.nodes.Count >= 8);
            SessionData.Instance.startingNode = Player.Instance.FindSafeTeleport(validStreets[Toolbox.Instance.Rand(0, validStreets.Count)]);
        }

        Player.Instance.Teleport(SessionData.Instance.startingNode, null, false);

        Game.Instance.disableCaseBoardClose = false;

        //Size basement water
        if(CityControls.Instance.basementWaterTransform != null)
        {
            float scaleX = (CityData.Instance.citySize.x - 2) * CityControls.Instance.cityTileSize.x;
            float scaleY = (CityData.Instance.citySize.y - 2) * CityControls.Instance.cityTileSize.y;

            CityControls.Instance.basementWaterTransform.localScale = new Vector3(scaleX, scaleY, CityControls.Instance.basementWaterTransform.localScale.z);
        }

        //Setup map
        MapController.Instance.Setup();
    }

    public void SetPreSim(bool val)
    {
        if(val != preSimActive)
        {
            //If turning off and this was on...
            if(preSimActive && !val)
            {
                Game.Log("CityGen: Pre Sim is complete...");
                preSimOccured = true;
                loadingProgress = 1f;
                stateComplete = true;
            }

            preSimActive = val;

            Game.Log("CityGen: Pre Simulation active: " + preSimActive);

            if(preSimActive)
            {
                //Unlock doors of open businesses...
                int unlockedDoors = 0;

                foreach(Company cc in CityData.Instance.companyDirectory)
                {
                    if(cc.placeOfBusiness.thisAsAddress != null)
                    {
                        if(cc.IsOpenAtThisTime(SessionData.Instance.gameTime))
                        {
                            if(cc.placeOfBusiness.streetAccess != null)
                            {
                                if(cc.placeOfBusiness.streetAccess.door != null)
                                {
                                    cc.placeOfBusiness.streetAccess.door.SetLocked(false, null, false);
                                    unlockedDoors++;
                                }
                            }

                            foreach(NewRoom r in cc.address.rooms)
                            {
                                r.SetMainLights(true, "Set open on presim", forceInstant: true, forceUpdate: true);
                            }
                        }
                    }
                }

                Game.Log("CityGen: Unlocked doors of " + unlockedDoors + " businesses, ready for presim...");

                //Force update of groundmap to update groundmap-based culling systems
                Player.Instance.OnCityTileChange();

                //Started game flag
                SessionData.Instance.startedGame = true;

                SessionData.Instance.ResumeGame();

                //Execute citizen behaviour
                CitizenBehaviour.Instance.StartGame();

                SessionData.Instance.SetTimeSpeed(SessionData.TimeSpeed.veryFast);
            }
            else
            {
                SessionData.Instance.SetTimeSpeed(SessionData.TimeSpeed.normal);

                SessionData.Instance.PauseGame(false);

                //Started game flag
                SessionData.Instance.startedGame = false;
            }
        }
    }

	public void StartGame()
	{
        //Copy build desc to info collector
        if (MainMenuController.Instance.feedbackPlayerInfo != null)
        {
            MainMenuController.Instance.feedbackPlayerInfo.citySeed = Toolbox.Instance.GetShareCode(CityData.Instance.cityName, (int)CityData.Instance.citySize.x, (int)CityData.Instance.citySize.y, Game.Instance.buildID, CityData.Instance.seed);
        }

        InterfaceController.Instance.caseScrollingViewRect.OnInputModeChange(); //Enable this if we are using a controller

        //Set fps mode
        CameraController.Instance.SetupFPS();

        loadingWallsReference = null;
        loadingFurnitureReference = null;

        //Force update of groundmap to update groundmap-based culling systems
        Player.Instance.OnCityTileChange();

        //Activate HUD canvas
        InterfaceController.Instance.SetInterfaceActive(true);

		//Started game flag
		SessionData.Instance.startedGame = true;

        //Toggle on play mode bool
        SessionData.Instance.ResumeGame();

		//Execute citizen behaviour
		CitizenBehaviour.Instance.StartGame();

        //Start ambience SFX
        AudioController.Instance.StartAmbienceTracks();

        //New newspaper
        if (saveState == null)
        {
            NewspaperController.Instance.GenerateNewNewspaper();
            Game.Instance.SetGameLength(MainMenuController.Instance.gameLengthDropdown.dropdown.value, true, false, false); //Set the game length

            //Set game timer
            if (Game.Instance.timeLimited)
            {
                SessionData.Instance.gameTimeLimit = Game.Instance.timeLimit * 60;
            }
        }
        else
        {
            Game.Log("Murder: Updating states for " + MurderController.Instance.activeMurders.Count + " active murders...");

            foreach (MurderController.Murder m in MurderController.Instance.activeMurders)
            {
                m.SetMurderState(m.state, true);
            }
        }

        MurderController.Instance.OnStartGame();

        //Limit limit
        if (Game.Instance.timeLimited)
        {
            SessionData.Instance.gameTimeLimit = Mathf.Min(SessionData.Instance.gameTimeLimit, Game.Instance.timeLimit * 60);
            SessionData.Instance.UpdateGameTimerText();
        }

        //Fade in
        InterfaceController.Instance.fade = 1f;
        InterfaceController.Instance.Fade(0f, 2f, true);

        //Fire game start event
        //if (OnGameStarted != null) OnGameStarted();

        //Fade out menu
        MainMenuController.Instance.EnableMainMenu(false, true, true);

        //Know lock status of apartment door
        if(Player.Instance.home != null)
        {
            NewNode.NodeAccess apartmentEntrance = Player.Instance.home.entrances.Find(item => item.accessType == NewNode.NodeAccess.AccessType.door);
            apartmentEntrance.door.SetKnowLockedStatus(true);
        }

        SessionData.Instance.skyboxGradientIndex = 0;

        //Set weather
        //SessionData.Instance.SetWeather(1f, 1f, 0f, 1f, 0.1f);

        //Force weather udpates
        SessionData.Instance.ExecuteWeatherChange();
        SessionData.Instance.ExecuteWetnessChange();
        SessionData.Instance.ExecuteWindChange();

        //Update cases
        CasePanelController.Instance.UpdateCaseControls();
        CasePanelController.Instance.UpdateCaseButtonsActive();

        //Trigger day change
        CitizenBehaviour.Instance.OnDayChange();
        CitizenBehaviour.Instance.OnHourChange();

        //Update tutorial
        SessionData.Instance.UpdateTutorialNotifications();

        AudioController.Instance.UpdateAllLoopingSoundOcclusion();

        AudioController.Instance.UpdateAmbientZonesOnEndOfFrame();

        //Update depth of field
        InterfaceController.Instance.UpdateDOF();

        //Start killer proc gen loop
        if(Game.Instance.sandboxMode)
        {
            MurderController.Instance.SetProcGenKillerLoop(Game.Instance.enableMurdererInSandbox);
            //SessionData.Instance.weatherChangeTimer = 99999f;

            if (saveState == null) InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.gameHeader, 0, Player.Instance.GetCitizenName());
            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.gameHeader, 0, SessionData.Instance.CurrentTimeString(false, true) + ", " + SessionData.Instance.LongDateString(SessionData.Instance.gameTime, true, true, true, true, true, false, false, true) + ", " + Player.Instance.currentGameLocation.name);
        }

        //Give hospital access
        if(Game.Instance.allHospitalAccess)
        {
            List<NewAddress> hospitalWards = CityData.Instance.addressDirectory.FindAll(item => item.addressPreset != null && item.addressPreset.name == "HospitalWard");

            foreach(NewAddress ad in hospitalWards)
            {
                Player.Instance.AddToKeyring(ad, false);
                if(ad.passcode != null) GameplayController.Instance.AddPasscode(ad.passcode, false);
            }
        }

        //Debug styff
        if(Game.Instance.devMode && Game.Instance.collectDebugData)
        {
            foreach(Company c in CityData.Instance.companyDirectory)
            {
                if(c.address != null)
                {
                    c.address.debugCompanyShifts = c.shifts;
                }
            }
        }

        Player.Instance.UpdateCurrentBuildingModelVisibility();

        //Start background mesh generation
        MeshPoolingController.Instance.StartCachingProcess();

        if(!Game.Instance.skipIntro && saveState == null)
        {
            CutSceneController.Instance.PlayCutScene(GameplayControls.Instance.intro);
        }
        else
        {
            //Fire game start event
            TriggerStartEvent();
        }
    }

    public void TriggerStartEvent()
    {
        //Fire game start event
        if (OnGameStarted != null) OnGameStarted();

        //Remove this script
        currentData = null;
        Destroy(this);
    }

    private void EnableTutorial()
    {
        SessionData.Instance.SetDisplayTutorialText(true);
        PopupMessageController.Instance.OnLeftButton -= DisableTutorial;
        PopupMessageController.Instance.OnRightButton -= EnableTutorial;

        loadingProgress = 1f;
        stateComplete = true;
    }

    private void DisableTutorial()
    {
        SessionData.Instance.SetDisplayTutorialText(false);
        PopupMessageController.Instance.OnLeftButton -= DisableTutorial;
        PopupMessageController.Instance.OnRightButton -= EnableTutorial;

        loadingProgress = 1f;
        stateComplete = true;
    }

    //Save city data to file
    IEnumerator SaveCityData()
    {
        //Save criminals
        foreach (Occupation cr in CityData.Instance.criminalJobDirectory)
        {
            currentData.criminals.Add(cr.GenerateSaveData());
        }

        //Save districts
        foreach (DistrictController districtController in CityData.Instance.districtDirectory)
        {
            currentData.districts.Add(districtController.GenerateSaveData());
        }

        //You should now have the data for districts and blocks, but not city tiles. This will have to be cycled through as otherwise it would take far too long!

        //Save streets
        foreach(StreetController street in CityData.Instance.streetDirectory)
        {
            currentData.streets.Add(street.GenerateSaveData());
        }

        //Save groups
        currentData.groups = new List<GroupsController.SocialGroup>(GroupsController.Instance.groups);

        //Save pipes
        currentData.pipes = new List<PipeConstructor.PipeGroup>(PipeConstructor.Instance.generated);

        //Loop city tiles to save
        int cursor = 0;

        List<CityTile> cityTiles = new List<CityTile>();

        foreach(KeyValuePair<Vector2Int, CityTile> pair in CityData.Instance.cityTiles)
        {
            cityTiles.Add(pair.Value);
        }

        while (cursor < cityTiles.Count)
        {
            for (int i = 0; i < saveChunk; i++)
            {
                //Break loop if complete
                if (cursor >= cityTiles.Count) break;

                currentData.cityTiles.Add(cityTiles[cursor].GenerateSaveData());

                cursor++;
            }

            loadingProgress = ((float)cursor / (float)cityTiles.Count);
            yield return null;
        }

        //Save citizens
        foreach(Human human in CityData.Instance.citizenDirectory)
        {
            currentData.citizens.Add(human.GenerateSaveData());
        }

        //Save meta objects
        foreach(KeyValuePair<int, MetaObject> pair in CityData.Instance.metaObjectDictionary)
        {
            currentData.metas.Add(pair.Value);
            pair.Value.cd = true;
        }

        //Save interactables
        foreach (Interactable i in CityData.Instance.interactableDirectory)
        {
            //if (i.inv > -1) Game.Log("Saving inventory item " + i.preset.name + " id " + i.id + ": " + i.save);

            if(i.save)
            {
                currentData.interactables.Add(i);
            }
        }

        //Save misc stats
        currentData.population = CityData.Instance.citizenDirectory.Count;

        //Save player's apartment
        if(Player.Instance.home != null)
        {
            Game.Log("CityGen: Saving player's apartment: " + Player.Instance.home.id);
            currentData.playersApartment = Player.Instance.home.id;
        }
        else
        {
            currentData.playersApartment = -1;
        }

        string shareCode = Toolbox.Instance.GetShareCode(ref currentData);

        string cityDataPath = Application.persistentDataPath +  "/Cities/"  + shareCode + ".cit";
        string cityInfoPath = Application.persistentDataPath +  "/Cities/"  + shareCode + ".txt";
        Game.Log("CityGen: Saving data: " + cityDataPath);

        //Save multi page evidence
        foreach (EvidenceMultiPage ev in GameplayController.Instance.multiPageEvidence)
        {
            if(ev.pageContent != null && ev.pageContent.Count > 0)
            {
                CitySaveData.EvidenceStateSave newState = new CitySaveData.EvidenceStateSave();
                newState.id = ev.evID;
                newState.page = ev.page;
                newState.mpContent = new List<EvidenceMultiPage.MultiPageContent>(ev.pageContent);
                currentData.multiPage.Add(newState);
            }
        }

        //Save info file
        CityInfoData newInfo = new CityInfoData();
        newInfo.cityName = currentData.cityName;
        newInfo.shareCode = Toolbox.Instance.GetShareCode(ref currentData);
        newInfo.citySize = currentData.citySize;
        newInfo.population = currentData.population;
        newInfo.build = Game.Instance.buildID;
        //newInfo.populationAmount = currentData.populationAmount;

        //Save to file...
        System.Diagnostics.Stopwatch stopWatch = null;

        if (Game.Instance.devMode)
        {
            stopWatch = new System.Diagnostics.Stopwatch();
            stopWatch.Start();
        }


        string writeString = JsonUtility.ToJson(newInfo, true);
        Task writeCityInfoTask = Task.Run(() =>
        {
            using (StreamWriter streamWriter = File.CreateText(cityInfoPath))
            {
                streamWriter.Write(writeString);
            }
        });
        while (!writeCityInfoTask.IsCompleted)
        {
            yield return null;
        }

        //Write to file
        if(Game.Instance.useCityDataCompression)
        {
            string compressedCityPath = cityDataPath + "b";

            bool result = false;
            Task<bool> tempCompressionTask = DataCompressionController.Instance.CompressAndSaveDataAsync<CitySaveData>(currentData, compressedCityPath, Game.Instance.cityDataCompressionQuality);
            while (!tempCompressionTask.IsCompleted)
            {
                yield return null;
            }
            result = tempCompressionTask.Result;
            if (result)
            {
                Game.Log("CityGen: Successfully compressed and saved city data: " + compressedCityPath);
            }
            else
            {
                Game.LogError("Unable to compress and save city data! Using non compressed instead...");
                string jsonString = JsonUtility.ToJson(currentData);
                Task tempTask = Task.Run(() =>
                {
                    using (StreamWriter streamWriter = File.CreateText(cityDataPath))
                    {
                        streamWriter.Write(jsonString);
                    }
                });
                while (!tempTask.IsCompleted)
                {
                    yield return null;
                }
            }
        }
        else
        {
            Game.Log("CityGen: Using JSON to write city data...");
            string jsonString = JsonUtility.ToJson(currentData);
            Task tempTask = Task.Run(() =>
            {
                using (StreamWriter streamWriter = File.CreateText(cityDataPath))
                {
                    streamWriter.Write(jsonString);
                }
            });
            while (!tempTask.IsCompleted)
            {
                yield return null;
            }
        }

        if (Game.Instance.devMode && stopWatch != null)
        {
            stopWatch.Stop();
            Game.Log("CityGen: City save data written in " + stopWatch.Elapsed.TotalSeconds);
        }

        //MainMenuController.Instance.OnGenerationComplete();

        CityConstructor.Instance.loadingProgress = 1f;
        CityConstructor.Instance.stateComplete = true;
    }

    //Cancel generation/load
    public void Cancel()
    {
        this.enabled = false;
        generateNew = false;
    }

    //Create a self employed company (street vendors etc)
    public void CreateSelfEmployed(CompanyPreset company, Human employee, Interactable workLocation)
    {
        Company newCompany = new Company();
        newCompany.Setup(company, employee.home);
        newCompany.UpdateName();

        //Give job
        employee.SetJob(newCompany.companyRoster[0]);

        //Pass work position
        if(workLocation != null)
        {
            newCompany.placeOfBusiness = workLocation.node.gameLocation; //Set place of business to elsewhere...
            newCompany.passedWorkPosition = workLocation;
            newCompany.passedWorkLocationID = workLocation.id;

            //Set furniture ownership
            if(workLocation.furnitureParent != null)
            {
                workLocation.furnitureParent.AssignOwner(employee, true);
                Game.Log("CityGen: Creating self employed " + company.name + ", Assigning owner " + employee.GetCitizenName() + " at work location: " + workLocation.id + " " + workLocation.GetWorldPosition());
            }

            employee.SetWorkFurniture(workLocation); //Set ownership
        }
        //Otherwise has no work location...
        else
        {
            newCompany.placeOfBusiness = employee.home; //Set place of business to elsewhere...
            Game.Log("CityGen: Creating self employed " + company.name + " with no work location " + employee.GetCitizenName());
        }
    }

    //Load full city save data
    public async Task LoadFullCityDataAsync()
    {
        if (RestartSafeController.Instance.loadCityFileInfo != null)
        {
            System.Diagnostics.Stopwatch stopWatch = null;

            if (Game.Instance.devMode)
            {
                stopWatch = new System.Diagnostics.Stopwatch();
                stopWatch.Start();
            }

            //Figure out if this is a compressed file or not. Compressed files should end .citb, while uncompressed is .cit
            if(RestartSafeController.Instance.loadCityFileInfo.Extension.ToLower() == ".citb")
            {
                bool result = await DataCompressionController.Instance.LoadCompressedDataAsync<CitySaveData>(RestartSafeController.Instance.loadCityFileInfo.FullName, (output) => currentData = output);

                if (result)
                {
                    Game.Log("CityGen: Successfully loaded compressed city data: " + currentData.cityName);
                }
                else
                {
                    Game.LogError("Failed to load compressed city data!");
                }
            }
            else
            {
                Game.Log("CityGen: Detected uncompressed city file, reading JSON directly from file...");
                string jsonString = null;
                await Task.Run(() =>
                {
                    //Parse to class file (JSON Utility)
                    using (StreamReader streamReader = File.OpenText(RestartSafeController.Instance.loadCityFileInfo.FullName))
                    {
                        jsonString = streamReader.ReadToEnd();
                    }
                });
                try
                {
                    CitySaveData data = JsonUtility.FromJson<CitySaveData>(jsonString);
                    currentData = data;
                }
                catch (Exception ex)
                {
                    Debug.Log(ex.ToString());
                }
            }

            if (Game.Instance.devMode && stopWatch != null)
            {
                stopWatch.Stop();
                Game.Log("CityGen: City save data parsed in " + stopWatch.Elapsed.TotalSeconds);
            }
        }
        else
        {
            Game.LogError("City file missing!");
        }
    }

    public async Task LoadSaveStateFile()
    {
        if(RestartSafeController.Instance.saveStateFileInfo != null)
        {
            Game.Log("CityGen: Loading save state " + RestartSafeController.Instance.saveStateFileInfo.Name);

            System.Diagnostics.Stopwatch stopWatch = null;

            if (Game.Instance.devMode)
            {
                stopWatch = new System.Diagnostics.Stopwatch();
                stopWatch.Start();
            }

            //Is this a compressed file? If so use brotli to uncompress...
            if(RestartSafeController.Instance.saveStateFileInfo.Extension.ToLower() == ".sodb")
            {
                bool result = await DataCompressionController.Instance.LoadCompressedDataAsync<StateSaveData>(RestartSafeController.Instance.saveStateFileInfo.FullName, (output) => saveState = output);
                if (result)
                {
                    Game.Log("CityGen: Successfully loaded compressed save game");
                }
                else
                {
                    Game.LogError("CityGen: Unable to load compressed save game");
                }
            }
            else
            {
                //Parse to class file
                string jsonString = null;
                await Task.Run(() =>
                {
                    using (StreamReader streamReader = File.OpenText(RestartSafeController.Instance.saveStateFileInfo.FullName))
                    {
                        jsonString = streamReader.ReadToEnd();
                    }
                });
                if (jsonString != null)
                {
                    saveState = JsonUtility.FromJson<StateSaveData>(jsonString);
                }
            }

            if (Game.Instance.devMode && stopWatch != null)
            {
                stopWatch.Stop();
                Game.Log("CityGen: Save state parsed in " + stopWatch.Elapsed.TotalSeconds);
            }
        }
        else
        {
            Game.Log("CityGen: Missing save state file info on RestartSafeController!");
        }
    }
}
