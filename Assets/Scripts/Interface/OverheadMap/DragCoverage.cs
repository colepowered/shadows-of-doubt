﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DragCoverage : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerEnterHandler, IPointerExitHandler
{
    //References
    public RectTransform parentRect;
    //public RectTransform otherRect;

    private Vector2 currentPointerPosition;
    private Vector2 previousPointerPosition;

    //Pivot of this point
    public Vector2 pivot;

    public Vector2 sizeRange = new Vector2(400, 1730);
    public float edgeBuffer = 10f;

    //Events
    //Triggered when this block's name is update
    public delegate void OnDragCoverage();
    public event OnDragCoverage OnDragged;

    public void OnPointerDown(PointerEventData data)
    {
        //Set the pivot point of the window so dragging works from the correct corner
        //parentRect.SetPivot(pivot);
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRect, data.position, data.pressEventCamera, out previousPointerPosition);
    }

    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        InterfaceController.Instance.AddMouseOverElement(this);
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;
        InterfaceController.Instance.RemoveMouseOverElement(this);
    }

    public void OnDrag(PointerEventData data)
    {
        Vector2 sizeDelta = parentRect.sizeDelta;

        InterfaceController.Instance.AddMouseOverElement(this);

        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRect, data.position, data.pressEventCamera, out currentPointerPosition);
        Vector2 resizeValue = currentPointerPosition - previousPointerPosition;

        //sizeDelta += new Vector2(resizeValue.x * (1 - (pivot.x * 2)), 0);
        sizeDelta += new Vector2(resizeValue.x, 0);

        //Clamp
        sizeDelta = new Vector2(
            Mathf.Clamp(sizeDelta.x, sizeRange.x, sizeRange.y),
            sizeDelta.y
            );

        SetSize(sizeDelta.x);

        previousPointerPosition = currentPointerPosition;

        //Fire event
        if (OnDragged != null) OnDragged();
    }

    void OnDestroy()
    {
        InterfaceController.Instance.RemoveMouseOverElement(this);
    }

    public void SetSize(float newSize)
    {
        parentRect.sizeDelta = new Vector2(newSize, parentRect.sizeDelta.y);

        //Set other rect to inverse
        //float inverse = InterfaceControls.Instance.hudCanvasRect.sizeDelta.x - newSize - (2f * edgeBuffer);
        //otherRect.sizeDelta = new Vector2(inverse, otherRect.sizeDelta.y);
    }
}
