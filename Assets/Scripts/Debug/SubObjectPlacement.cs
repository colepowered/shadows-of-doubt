﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class SubObjectPlacement : MonoBehaviour
{
    [Header("Setup")]
    [OnValueChanged("OnClassChanged")]
    public SubObjectClassPreset preset;
    public FurniturePreset.SubObjectOwnership belongsTo = FurniturePreset.SubObjectOwnership.nobody; //Dictates which person this location belongs to. -1 == nobody
    public int security = 0;

    [Header("Components")]
    public TextMesh text;
    public Transform spawnedObject;
    public MeshRenderer mainObject;

    public void OnClassChanged()
    {
        if (preset != null) this.name = "Spawn: " + preset.name;
        else this.name = "Null";
        text.text = preset.name;
    }

    [Button("Random Direction")]
    public void RandomDir()
    {
        this.transform.eulerAngles = new Vector3(0, UnityEngine.Random.Range(0f, 360f), 0);
    }

    [Button("Random Object")]
    public void SpawnRandomObject()
    {
        RemoveRandomObject();

        object[] interactables = AssetLoader.Instance.GetAllInteractables().ToArray();

        List<InteractablePreset> options = new List<InteractablePreset>();

        foreach(object obj in interactables)
        {
            InteractablePreset p = obj as InteractablePreset;
            if (!p.spawnable || p.prefab == null) continue;
            if(p.subObjectClasses.Contains(preset))
            {
                options.Add(p);
            }
        }

        if(options.Count > 0)
        {
            InteractablePreset chosen = options[UnityEngine.Random.Range(0, options.Count)];
            GameObject inst = Instantiate(chosen.prefab, this.transform);
            inst.transform.localPosition = Vector3.zero;
            spawnedObject = inst.transform;
            if (mainObject == null) mainObject = this.gameObject.GetComponent<MeshRenderer>();
            mainObject.enabled = false;
            text.gameObject.SetActive(false);
        }
    }

    [Button("Remove Random Object")]
    public void RemoveRandomObject()
    {
        if(spawnedObject != null) Object.DestroyImmediate(spawnedObject.gameObject);
        if (mainObject == null) mainObject = this.gameObject.GetComponent<MeshRenderer>();
        mainObject.enabled = true;
        text.gameObject.SetActive(true);
    }
}
