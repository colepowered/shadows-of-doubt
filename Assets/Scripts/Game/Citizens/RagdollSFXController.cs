﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollSFXController : MonoBehaviour
{
    public Actor actor;
    public bool playedFloorImpact = false;

    //Used to play a collapse SFX when the ragdoll hits the floor
    void OnCollisionEnter(Collision collision)
    {
        if(!playedFloorImpact)
        {
            playedFloorImpact = true;

            //Game.Log("Ragdoll collide: " + collision.gameObject.name);

            //Play SFX
            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.collapseOnFloor, actor, actor.currentNode, this.transform.position);

            //Remove this (script only)
            Destroy(this);
        }
    }
}
