﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using System.Linq;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "doorpair_data", menuName = "Database/Decor/Door Pair Preset")]

public class DoorPairPreset : ScriptableObjectIDSystem
{
    public enum WallSectionClass { wall, window, windowLarge, entrance, ventUpper, ventLower, ventTop};

    [Header("Model Options")]
    [Tooltip("The wall model of the parent wall")]
    public List<GameObject> parentWallsLong = new List<GameObject>();

    [Tooltip("The wall model of the child wall")]
    public List<GameObject> childWallsLong = new List<GameObject>();

    [Space(7)]
    [Tooltip("Short walls: There should be 3 here - left, middle and right")]
    public List<GameObject> parentWallsShort = new List<GameObject>();

    [Tooltip("Short walls: There should be 3 here - left, middle and right")]
    public List<GameObject> childWallsShort = new List<GameObject>();

    [Space(7)]
    [Tooltip("The corner model. Always on the outside walls.")]
    public List<GameObject> corners = new List<GameObject>();

    [Tooltip("The corner model for outside facing exterior walls (overrides above)")]
    public List<GameObject> quoins = new List<GameObject>();

    [Space(7)]
    [Tooltip("If true, the game will use optimization to replaces walls of 3x rows with a larger section")]
    public bool optimizeSections = false;

    [Tooltip("If true, user is able to place this in the editor")]
    public bool appearInEditor = false;

    [Tooltip("If true, this section can support lightswitches or other wall props")]
    public bool supportsWallProps = true;

    [Tooltip("If true, the game will continue to draw building corners around this")]
    public bool isFence = false;

    public bool divider = false;
    public bool dividerLeft = false;
    public bool dividerRight = false;

    [Header("Door Options")]
    [Tooltip("Door object")]
    public bool canFeatureDoor = false;

    [Tooltip("Door offset position")]
    public Vector3 doorOffset = new Vector3(0.7f, 0f, 0f);

    [Header("Procedural Overrides")]
    [Tooltip("The class of this wall section. When a procedural address is generated, it may override this with another more appropriate model with the same class.")]
    public WallSectionClass sectionClass = WallSectionClass.wall;
    [Tooltip("If true then this will force this section to ignore raycasts when generating room culling.")]
    public bool ignoreCullingRaycasts = false;
    [Tooltip("Override with this if the floor height is above 0")]
    public DoorPairPreset raisedFloorOverride;

    [Header("Material Override")]
    public MaterialGroupPreset materialOverride = null;

    [Header("Map Overrides")]
    [Tooltip("Override map graphics with this")]
    public List<Texture2D> mapOverride = new List<Texture2D>();

    [Header("Duct Overrides")]
    public bool overrideWallNormal = false;
    [EnableIf("overrideWallNormal")]
    public DoorPairPreset wallNormalOverrride;
    [Space(5)]
    public bool overrideDuctLower = false;
    [EnableIf("overrideDuctLower")]
    public DoorPairPreset ductLowerOverrride;
    [Space(5)]
    public bool overrideDuctUpper = false;
    [EnableIf("overrideDuctUpper")]
    public DoorPairPreset ductUpperOverrride;

    [Button]
    public void UpdateIDs()
    {
#if UNITY_EDITOR
        List<DoorPairPreset> all = AssetLoader.Instance.GetAllDoorPairPresets();

        int assignID = 1;

        foreach (DoorPairPreset dp in all)
        {
            int i = 1;

            if(int.TryParse(dp.id, out i))
            {
                assignID = Mathf.Max(assignID, i + 1);
            }
        }

        foreach (DoorPairPreset dp in all)
        {
            if (dp.id == null || dp.id.Length <= 0)
            {
                Debug.Log("Assigned new ID: " + dp.name + " = " + assignID);
                dp.id = assignID.ToString();
                assignID++;

                UnityEditor.EditorUtility.SetDirty(dp);
            }
        }
#endif
    }
}
