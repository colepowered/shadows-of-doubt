﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class CullingDebugController : MonoBehaviour
{
    public MeshRenderer rend;
    public NewRoom room;
    public NewNode.NodeAccess parentEntrance;
    public NewNode.NodeAccess otherEntrance;
    public List<NewDoor> dependentDoors;
    public NewRoom atriumTopOf;

    public GameObject parentObjectMarker;

    public enum CullDebugType { none, succeededNew, succeededOvr, adjacent, atriumTop};
    public CullDebugType cullType = CullDebugType.none;

    [Header("Config")]
    public Material red;
    public Material white;
    public Material yellow;
    public Material green;
    public Material blue;

    public void Setup(NewRoom newRoom, NewNode.NodeAccess newPEntrance, List<NewDoor> newDoors, CullDebugType newCullType, NewRoom newAtriumTopOf = null, NewNode.NodeAccess newOEntrance = null)
    {
        room = newRoom;
        parentEntrance = newPEntrance;
        otherEntrance = newOEntrance;
        dependentDoors = newDoors;
        cullType = newCullType;
        atriumTopOf = newAtriumTopOf;

        if (cullType == CullDebugType.none)
        {
            rend.sharedMaterial = white;
        }
        else if(cullType == CullDebugType.succeededNew)
        {
            rend.sharedMaterial = green;
        }
        else if(cullType == CullDebugType.succeededOvr)
        {
            rend.sharedMaterial = yellow;
        }
        else if(cullType == CullDebugType.adjacent)
        {
            rend.sharedMaterial = blue;
        }

        //If depedent on doors, give this a hat!
        if(dependentDoors != null && dependentDoors.Count > 0)
        {
            GameObject newObj = Toolbox.Instance.SpawnObject(PrefabControls.Instance.debugNodeDisplay, this.transform);
            newObj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            newObj.transform.localPosition = new Vector3(0, 1, 0);
            newObj.GetComponent<MeshRenderer>().sharedMaterial = red;
        }
    }

    [Button]
    public void ToggleParentsEntrance()
    {
        if(parentObjectMarker != null)
        {
            Destroy(parentObjectMarker);
        }
        else
        {
            parentObjectMarker = Toolbox.Instance.SpawnObject(PrefabControls.Instance.debugNodeDisplay, this.transform);
            parentObjectMarker.transform.position = parentEntrance.worldAccessPoint;
            parentObjectMarker.GetComponent<MeshRenderer>().sharedMaterial = white;
        }
    }

    [Button]
    public void RunDataRaycast()
    {
        List<DataRaycastController.NodeRaycastHit> output = new List<DataRaycastController.NodeRaycastHit>();
        bool pass = DataRaycastController.Instance.EntranceRaycast(parentEntrance, otherEntrance, out output, debugMode: true);

        Game.Log(pass + "...");

        for (int i = 0; i < output.Count; i++)
        {
            DataRaycastController.NodeRaycastHit h = output[i];
            Game.Log(CityData.Instance.NodeToRealpos(h.coord));

            GameObject newObj = Toolbox.Instance.SpawnObject(PrefabControls.Instance.debugNodeDisplay, this.transform);
            newObj.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            newObj.transform.position = CityData.Instance.NodeToRealpos(h.coord);
            newObj.GetComponent<MeshRenderer>().sharedMaterial = green;
            newObj.name = CityData.Instance.NodeToRealpos(h.coord).ToString();
        }
    }
}
