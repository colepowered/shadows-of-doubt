﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;

public class WalkableRecorder : MonoBehaviour
{
    [Header("Assign")]
    public FurniturePreset furniture;
    public FurnitureClass furnitureClass;
    public Transform furnitureParent;

    [Header("Components")]
    public Transform furnitureAnchor;
    public Transform walkableNodeParent;
    public GameObject walkableObject;
    public List<TileSetup> tiles = new List<TileSetup>();
    public Transform blockingParent;
    public Material nonBlockedMaterial;
    public Material blockedMaterial;
    public List<DebugBlockingSelector> blockedDisplay = new List<DebugBlockingSelector>();
    public List<Transform> walkableDisplay = new List<Transform>();
    public List<SubObjectPlacement> subObjectDisplay = new List<SubObjectPlacement>();

    [Header("Load Subobjects")]
    public GameObject subObjectPrefab;

    private Vector2[] offsetArrayX8 =
{
        new Vector2 (-1, -1),
        new Vector2 (0, -1),
        new Vector2 (1, -1),
        new Vector2 (-1, 0),
        new Vector2 (1, 0),
        new Vector2 (-1, 1),
        new Vector2 (0, 1),
        new Vector2 (1, 1)
    };

    [System.Serializable]
    public class TileSetup
    {
        public Vector2 offset;
        public Transform trans;
    }

    [Button("Load Furniture Object")]
    public void LoadFurniture()
    {
        LoadClass();
        if (furnitureParent != null) Object.DestroyImmediate(furnitureParent.gameObject);
        GameObject newFurn = Instantiate(furniture.prefab, furnitureAnchor);
        newFurn.transform.localPosition = Vector3.zero;
        newFurn.transform.localRotation = Quaternion.identity;
        furnitureParent = newFurn.transform;

        LoadWalkable();
        LoadBlockedArea();
        LoadSubObjectSetup();
    }

    [Button ("Load Class from Furniture Preset")]
    public void LoadClass()
    {
        if(furniture != null)
        {
            if(furnitureParent != null) Object.DestroyImmediate(furnitureParent.gameObject);
            furnitureClass = furniture.classes[0];
            ClearWalkable();
            ClearBlockedDisplay();

            LoadWalkable();
            LoadBlockedArea();
        }
    }

    [Button("Load Walkable Nodespace Area")]
    public void LoadWalkable()
    {
        ClearWalkable();

        foreach(FurnitureClass.FurniureWalkSubLocations loc in furnitureClass.sublocations)
        {
            TileSetup t = tiles.Find(item => item.offset == loc.offset);

            if(t != null)
            {
                foreach(Vector3 sub in loc.sublocations)
                {
                    GameObject newWalkable = Instantiate(walkableObject, t.trans);
                    newWalkable.transform.localPosition = sub;
                    walkableDisplay.Add(newWalkable.transform);
                    newWalkable.transform.SetParent(walkableNodeParent);
                }
            }
        }
    }

    [Button("Automatic Walkable Nodespace Area Generation")]
    public void AutomaticWalkable()
    {
        ClearWalkable();

        Vector3[] defaultNodeSpace = new Vector3[] { 
            new Vector3(0, 0, 0),
            new Vector3(0.594f, 0, 0.594f),
            new Vector3(-0.594f, 0 ,0.594f),
            new Vector3(0.594f, 0, -0.594f),
            new Vector3(-0.594f, 0, -0.594f),
            new Vector3(-0.594f, 0, 0),
            new Vector3(0, 0, -0.594f),
            new Vector3(0.594f, 0, 0),
            new Vector3(0, 0, 0.594f)
        };

        for (int i = 0; i < furnitureClass.objectSize.x; i++)
        {
            for (int u = 0; u < furnitureClass.objectSize.y; u++)
            {
                Vector2 offset = new Vector2(i, u);

                TileSetup t = tiles.Find(item => item.offset == offset);

                if(t != null)
                {
                    //Look to spawn walkable in default pattern on each tile
                    foreach(Vector3 v3 in defaultNodeSpace)
                    {
                        Ray ray = new Ray(t.trans.position + v3 + new Vector3(0, -0.01f, 0), Vector3.up);
                        RaycastHit hit;

                        if(!Physics.Raycast(ray, out hit, 5.4f))
                        {
                            //If no hit then add a walkable node
                            GameObject newWalkable = Instantiate(walkableObject, t.trans);
                            newWalkable.transform.localPosition = v3;
                            walkableDisplay.Add(newWalkable.transform);
                            newWalkable.transform.SetParent(walkableNodeParent);
                        }
                    }
                }
            }
        }
    }

    [Button("Save Walkable Nodespace Area")]
    public void RecordWalkable()
    {
        List<Transform> subObjects = new List<Transform>();
        furnitureClass.sublocations.Clear();

        //Loop through all children, search for placement objects
        for (int i = 0; i < walkableDisplay.Count; i++)
        {
            Transform child = walkableDisplay[i];
            if (child == null) continue;

            //Find out which tile this is on...
            Vector2 offsetThis = Vector2.zero;
            Vector3 tileFind = child.localPosition;

            while (tileFind.x < -0.9f)
            {
                tileFind.x += 1.8f;
                offsetThis.x += 1;
            }

            while (tileFind.z < -0.9f)
            {
                tileFind.z += 1.8f;
                offsetThis.y += 1;
            }

            Debug.Log("point found at tile " + offsetThis);

            if (!furnitureClass.sublocations.Exists(item => item.offset == offsetThis))
            {
                furnitureClass.sublocations.Add(new FurnitureClass.FurniureWalkSubLocations { offset = offsetThis });
            }

            FurnitureClass.FurniureWalkSubLocations loc = furnitureClass.sublocations.Find(item => item.offset == offsetThis);

            TileSetup tile = tiles.Find(item => item.offset == offsetThis);

            Vector3 pos = tile.trans.InverseTransformPoint(child.position);

            loc.sublocations.Add(new Vector3(RoundToPlaces(pos.x, 3), RoundToPlaces(pos.y, 3), RoundToPlaces(pos.z, 3)));

            furnitureClass.blockDefaultSublocations = true;
        }

        //Force save data
#if UNITY_EDITOR
        Debug.Log("Saving " + furnitureClass.name);
        UnityEditor.EditorUtility.SetDirty(furnitureClass);
#endif
    }

    [Button("Clear Walkable Nodepace Display")]
    public void ClearWalkable()
    {
        while (walkableDisplay.Count > 0)
        {
            if(walkableDisplay[0] != null) Object.DestroyImmediate(walkableDisplay[0].gameObject);
            walkableDisplay.RemoveAt(0);
        }
    }

    //Round a float to x decimal places
    public float RoundToPlaces(float input, int decimals)
    {
        int mp = 100;

        if (decimals == 1) mp = 10;
        else if (decimals == 2) mp = 100;
        else if (decimals == 3) mp = 1000;
        else if (decimals == 4) mp = 10000;

        return (float)(Mathf.RoundToInt(input * mp) / (float)mp);
    }

    [Button("Load Sub Object Placement Configuration")]
    public void LoadSubObjectSetup()
    {
        foreach(FurniturePreset.SubObject so in furniture.subObjects)
        {
            if (so == null) continue;

            Transform spawnParent = furnitureParent;
            if(so.parent != null && so.parent.Length > 0) spawnParent = SearchForTransform(furnitureParent, so.parent);

            if(spawnParent == null)
            {
                spawnParent = furnitureParent;
                Debug.Log("Unable to find parent: " + so.parent);
            }

            if (spawnParent == null) continue;

            GameObject newObj = Instantiate(subObjectPrefab, spawnParent);
            newObj.transform.localPosition = so.localPos;
            newObj.transform.localEulerAngles = so.localRot;
            SubObjectPlacement sop = newObj.GetComponent<SubObjectPlacement>();
            sop.belongsTo = so.belongsTo;
            sop.preset = so.preset;
            sop.security = so.security;
            subObjectDisplay.Add(sop);
            sop.OnClassChanged();
        }
    }

    [Button("Spawn Random Sub Object Examples")]
    public void SpawnRandomSubObjects()
    {
        subObjectDisplay.Clear();
        subObjectDisplay = furnitureParent.GetComponentsInChildren<SubObjectPlacement>().ToList();

        foreach(SubObjectPlacement so in subObjectDisplay)
        {
            so.SpawnRandomObject();
        }
    }

    [Button("Clear Random Sub Object Examples")]
    public void ClearRandomSubObjects()
    {
        subObjectDisplay.Clear();
        subObjectDisplay = furnitureParent.GetComponentsInChildren<SubObjectPlacement>().ToList();

        foreach (SubObjectPlacement so in subObjectDisplay)
        {
            so.RemoveRandomObject();
        }
    }

    [Button("Save Sub Object Placement Configuration")]
    public void RecordSubObjectPlacements()
    {
        List<FurniturePreset.SubObject> subObjects = new List<FurniturePreset.SubObject>();
        furniture.subObjects = new List<FurniturePreset.SubObject>();

        //Loop through all children, search for placement objects
        for (int i = 0; i < furnitureParent.childCount; i++)
        {
            Transform child = furnitureParent.GetChild(i);
            SubObjectPlacement sop = child.GetComponent<SubObjectPlacement>();

            if (sop != null)
            {
                FurniturePreset.SubObject newSO = new FurniturePreset.SubObject();
                newSO.preset = sop.preset;
                newSO.localPos = sop.transform.localPosition;
                newSO.localRot = sop.transform.localEulerAngles;
                newSO.belongsTo = sop.belongsTo;
                newSO.security = sop.security;

                //Add this to the preset
                furniture.subObjects.Add(newSO);
            }

            //Cycle within children
            for (int u = 0; u < child.childCount; u++)
            {
                Transform child2 = child.GetChild(u);
                SubObjectPlacement sop2 = child2.GetComponent<SubObjectPlacement>();

                if (sop2 != null)
                {
                    FurniturePreset.SubObject newSO = new FurniturePreset.SubObject();
                    newSO.preset = sop2.preset;
                    newSO.localPos = sop2.transform.localPosition;
                    newSO.localRot = sop2.transform.localEulerAngles;
                    newSO.parent = child2.transform.parent.name;
                    newSO.belongsTo = sop2.belongsTo;
                    newSO.security = sop2.security;

                    //Add this to the preset
                    furniture.subObjects.Add(newSO);
                }

                //Cycle within children
                for (int l = 0; l < child2.childCount; l++)
                {
                    Transform child3 = child2.GetChild(l);
                    SubObjectPlacement sop3 = child3.GetComponent<SubObjectPlacement>();

                    if (sop3 != null)
                    {
                        FurniturePreset.SubObject newSO = new FurniturePreset.SubObject();
                        newSO.preset = sop3.preset;
                        newSO.localPos = sop3.transform.localPosition;
                        newSO.localRot = sop3.transform.localEulerAngles;
                        newSO.parent = child3.transform.parent.name;
                        newSO.belongsTo = sop3.belongsTo;
                        newSO.security = sop3.security;

                        //Add this to the preset
                        furniture.subObjects.Add(newSO);
                    }
                }
            }
        }

        //Force save data
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(furniture);
#endif
    }

    [Button("Clear Sub Object Placement Display")]
    public void ClearSubObjectDisplay()
    {
        while (subObjectDisplay.Count > 0)
        {
            if(subObjectDisplay[0] != null) Object.DestroyImmediate(subObjectDisplay[0].gameObject);
            subObjectDisplay.RemoveAt(0);
        }
    }

    //Recursively search for a transform
    public Transform SearchForTransform(Transform parent, string search)
    {
        Transform child = null;

        // Loop through top level
        foreach (Transform t in parent)
        {
            if (t.name == search)
            {
                child = t;
                return child;
            }
            else if (t.childCount > 0)
            {
                child = SearchForTransform(t, search);

                if (child)
                {
                    return child;
                }
            }
        }

        return child;
    }

    [Button("Load Blocked Area")]
    public void LoadBlockedArea()
    {
        ClearBlockedDisplay();

        foreach(TileSetup t in tiles)
        {
            for (int i = 0; i < offsetArrayX8.Length; i++)
            {
                Vector2 offset = offsetArrayX8[i];

                CityData.BlockingDirection dir = (CityData.BlockingDirection)(i + 1);

                if (blockedDisplay.Exists(item => item.tile.offset == t.offset - offset && item.offset == -offset)) continue;

                //Create walkable objects
                GameObject newPrim = GameObject.CreatePrimitive(PrimitiveType.Cube);
                newPrim.transform.SetParent(t.trans);
                newPrim.transform.localScale = new Vector3(0.1f, 0.1f, 1.8f);

                MeshRenderer rend = newPrim.GetComponent<MeshRenderer>();
                rend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                rend.sharedMaterial = nonBlockedMaterial;

                newPrim.transform.localPosition = new Vector3(offset.x * 0.9f, 0, offset.y * 0.9f);
                Vector3 direction = newPrim.transform.localPosition;

                newPrim.transform.rotation = Quaternion.LookRotation(direction, Vector3.up);
                newPrim.transform.SetParent(blockingParent, true);

                Vector2 nodeOffset = t.offset * -1;

                newPrim.name = "Tile " + nodeOffset + " " + dir + " " + offset;

                DebugBlockingSelector blocking = newPrim.AddComponent<DebugBlockingSelector>();
                blocking.Setup(t, dir, this, offset);
                blockedDisplay.Add(blocking);

                FurnitureClass.BlockedAccess access = furnitureClass.blockedAccess.Find(item => item.nodeOffset == nodeOffset);

                if(access != null)
                {
                    if(access.blocked.Contains(dir))
                    {
                        blocking.SetB();
                    }
                }
            }
        }
    }

    [Button("Save Blocked Area")]
    public void SaveBlockedArea()
    {
        furnitureClass.blockedAccess.Clear();

        int blockingCount = 0;

        foreach(DebugBlockingSelector blocking in blockedDisplay)
        {
            if(blocking.blocked)
            {
                Vector2 nodeOffset = blocking.tile.offset * -1;

                FurnitureClass.BlockedAccess access = furnitureClass.blockedAccess.Find(item => item.nodeOffset == nodeOffset);

                if (access == null)
                {
                    access = new FurnitureClass.BlockedAccess { nodeOffset = nodeOffset };
                    furnitureClass.blockedAccess.Add(access);
                }

                if(!access.blocked.Contains(blocking.dir))
                {
                    access.blocked.Add(blocking.dir);
                }

                blockingCount++;
            }
        }

        if (blockingCount <= 0 && !furnitureClass.noPassThrough) furnitureClass.noBlocking = true;

        //Force save data
        #if UNITY_EDITOR
        Debug.Log("Saving " + furnitureClass.name);
        UnityEditor.EditorUtility.SetDirty(furnitureClass);
        #endif
    }

    [Button("Clear Blocked Display")]
    public void ClearBlockedDisplay()
    {
        while(blockedDisplay.Count > 0)
        {
            if (blockedDisplay[0] != null) Object.DestroyImmediate(blockedDisplay[0].gameObject);
            blockedDisplay.RemoveAt(0);
        }
    }

    [Button("Save All")]
    public void SaveAll()
    {
        RecordWalkable();
        SaveBlockedArea();
        RecordSubObjectPlacements();
    }

    [Button("Clear All")]
    public void ClearAll()
    {
        ClearBlockedDisplay();
        ClearWalkable();
        ClearSubObjectDisplay();
        furnitureClass = null;
        furniture = null;
        if (furnitureParent != null) Object.DestroyImmediate(furnitureParent.gameObject);
    }
}
