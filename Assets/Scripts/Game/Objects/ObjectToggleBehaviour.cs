﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class ObjectToggleBehaviour : SwitchSyncBehaviour
{
    [ReorderableList]
    public List<GameObject> objectsToToggle = new List<GameObject>();

    public override void SetOn(bool val)
    {
        base.SetOn(val);

        foreach(GameObject g in objectsToToggle)
        {
            g.SetActive(val);
        }
    }
}
