﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;
using UnityEngine.Rendering;
using UnityEngine.UI;

[System.Serializable]
public class Objective
{
    //Serializable
    public SpeechController.QueueElement queueElement = null;

    public string name = "New Objective";
    public float progress = 0f;
    public bool isComplete = false;
    public bool isCancelled = false;

    public enum DisplayPhase { preDisplay, fadeInMainText, displayMainText, removeText, displayingList, waitForComplete };
    public DisplayPhase dispPhase = DisplayPhase.preDisplay;
    private float fadeInProgress = 0f;
    private float displayProgress = 0f;
    private float displayTime = 0f; //Use the same display time as speech

    //Crouch promt
    private float crouchPromtTimer = 0f;

    //OnComplete
    public enum OnCompleteAction { nextChapterPart, nextPartWhenAllCompleted, specificChapterByString, specificChapterWhenAllCompleted, nothing, invokeFunction, triggerSideJobFunction, triggerSideJobHandIn, nextSideJobPhase, submitSideJob };

    //NonSerialized
    [System.NonSerialized]
    public Case thisCase;
    [System.NonSerialized]
    public GameObject objectiveListItem;
    [System.NonSerialized]
    public RectTransform objectiveListRect;
    [System.NonSerialized]
    public ChecklistButtonController objectiveList;

    //UI Pointer
    [System.NonSerialized]
    private bool displayPointer = false; //True if we've reached the phase where we can display a pointer
    [System.NonSerialized]
    public RectTransform pointerUIObject;
    [System.NonSerialized]
    public InterfaceController.AwarenessIcon awarenessIcon;
    [System.NonSerialized]
    public UIPointerController pointer;
    [System.NonSerialized]
    public Sprite sprite;
    [System.NonSerialized]
    public bool isSetup = false;
    [System.NonSerialized]
    public List<ObjectiveTrigger> appliedProgress;
    [System.NonSerialized]
    public bool clearedForAnimation = false; //Game must not be displaying other messages for this to start animating the screen

    [System.Serializable]
    public class ObjectiveTrigger
    {
        //Serialized
        public ObjectiveTriggerType triggerType;
        public bool forceProgressAmount = false;
        public float progressAdd;
        public bool triggered = false;
        public string name;
        public string hightlightAction;
        public bool orTrigger = false;

        public int roomID = -1;
        public int interactableID = -1;
        public string evidenceID;
        public Vector3Int nodeCoord;
        public int doorID = -1;
        public int addressID = -1;
        public int streetID = -1;
        public int jobID = -1;
        public Vector3 position;

        //Non serialized
        [System.NonSerialized]
        public NewRoom room;
        [System.NonSerialized]
        public Interactable interactable;
        [System.NonSerialized]
        public Evidence evidence;
        [System.NonSerialized]
        public NewNode node;
        [System.NonSerialized]
        public NewDoor door;
        [System.NonSerialized]
        public NewGameLocation gameLocation;
        [System.NonSerialized]
        public SideJob job;
        [System.NonSerialized]
        public List<Objective> addedToObjectives;

        public ObjectiveTrigger(
            ObjectiveTriggerType newType,
            string newName,
            bool newForceProgressAmount = false, //Automatically distribute progress based on trigger count
            float newProgressAdd = 0f,
            NewRoom newRoom = null,
            Interactable newInteractable = null,
            Evidence newEvidence = null,
            NewNode newNode = null,
            NewDoor newDoor = null,
            NewGameLocation newGameLocation = null,
            SideJob newJob = null,
            string newHighlightAction = "",
            bool newOrTrigger = false, //If true, only one of the 'or' triggers are needed for completion...
            Vector3 newPosition = new Vector3()
            )
        {
            triggerType = newType;
            name = newName;
            progressAdd = newProgressAdd;
            hightlightAction = newHighlightAction;
            forceProgressAmount = newForceProgressAmount;
            orTrigger = newOrTrigger;

            room = newRoom;
            if (room != null) roomID = room.roomID;

            interactable = newInteractable;
            if (interactable != null) interactableID = interactable.id;

            evidence = newEvidence;
            if (evidence != null) evidenceID = evidence.evID;

            node = newNode;
            if (node != null) nodeCoord = node.nodeCoord;

            door = newDoor;
            if (door != null) doorID = door.wall.id;

            job = newJob;
            if (job != null) jobID = job.jobID;

            position = newPosition;

            gameLocation = newGameLocation;

            if(gameLocation != null)
            {
                if (gameLocation.thisAsAddress != null) addressID = gameLocation.thisAsAddress.id;
                else if (gameLocation.thisAsStreet != null) streetID = gameLocation.thisAsStreet.streetID;
            }

            addedToObjectives = new List<Objective>();
        }

        public void SetupNonSerialized()
        {
            if(roomID > -1 && room == null)
            {
                if(!CityData.Instance.roomDictionary.TryGetValue(roomID, out room))
                {
                    Game.LogError("Unable to get room for objective: " + roomID);
                }
            }

            if (interactableID > 0 && interactable == null)
            {
                if(!CityData.Instance.savableInteractableDictionary.TryGetValue(interactableID, out interactable))
                {
                    Game.LogError("Unable to get interactable for objective: " + interactableID);
                }
            }

            if (evidenceID != null && evidenceID.Length > 0 && evidence == null)
            {
                if(!Toolbox.Instance.TryGetEvidence(evidenceID, out evidence))
                {
                    Game.LogError("Unable to get evidence for objective: " + evidenceID);
                }
            }

            PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out node);

            if (doorID > -1 && door == null)
            {
                if(!CityData.Instance.doorDictionary.TryGetValue(doorID, out door))
                {
                    Game.LogError("Unable to get door for objective: " + doorID);
                }
            }

            if (addressID > -1 && gameLocation == null)
            {
                NewAddress ad = null;
                
                if(!CityData.Instance.addressDictionary.TryGetValue(addressID, out ad))
                {
                    Game.LogError("Unable to get address for objective: " + addressID);
                }

                gameLocation = ad;
            }

            if (streetID > -1 && gameLocation == null)
            {
                StreetController str = CityData.Instance.streetDirectory.Find(item => item.streetID == streetID);
                gameLocation = str;

                if(str == null)
                {
                    Game.LogError("Unable to get street for objective: " + streetID);
                }
            }

            if(jobID > -1 && job == null)
            {
                if(!SideJobController.Instance.allJobsDictionary.TryGetValue(jobID, out job))
                {
                    Game.LogError("Unable to get side job for objective: " + jobID);
                }
            }
        }

        public void Trigger(bool onSetup = false)
        {
            triggered = true;

            foreach(Objective obj in addedToObjectives)
            {
                if (obj.appliedProgress == null) obj.appliedProgress = new List<ObjectiveTrigger>();

                if(!obj.appliedProgress.Contains(this))
                {
                    //This is an 'or' trigger, but another or trigger has already been applied...
                    if(orTrigger && obj.appliedProgress.Exists(item => item.orTrigger))
                    {
                        continue;
                    }

                    float addThis = progressAdd;

                    if (!forceProgressAmount)
                    {
                        List<ObjectiveTrigger> evenProgressObjectives = new List<ObjectiveTrigger>();
                        List<ObjectiveTrigger> orTriggers = new List<ObjectiveTrigger>();
                        float progressDistribute = 1f;

                        int triggerCount = 0;

                        //Setup triggers
                        foreach (ObjectiveTrigger trig in obj.queueElement.triggers)
                        {
                            if(trig.orTrigger)
                            {
                                orTriggers.Add(trig);

                                if (orTriggers.Count == 1)
                                {
                                    triggerCount++; //Treat or triggers as 1

                                    //Set trigger progress to equal...
                                    if (!trig.forceProgressAmount)
                                    {
                                        evenProgressObjectives.Add(trig);
                                    }
                                    //Otherwise remove the amount to distribute
                                    else
                                    {
                                        progressDistribute -= trig.progressAdd;
                                    }
                                }
                            }
                            else
                            {
                                triggerCount++;

                                //Set trigger progress to equal...
                                if (!trig.forceProgressAmount)
                                {
                                    evenProgressObjectives.Add(trig);
                                }
                                //Otherwise remove the amount to distribute
                                else
                                {
                                    progressDistribute -= trig.progressAdd;
                                }
                            }
                        }

                        addThis = progressDistribute / (float)triggerCount;
                    }

                    obj.progress += Mathf.CeilToInt(addThis * 100f) / 100f;
                    obj.progress = Mathf.Clamp01(obj.progress);

                    Game.Log("Objective: + " + addThis + " Objective progress to " + obj.queueElement.entryRef + " with trigger count of " + obj.queueElement.triggers.Count + ", total is " + obj.progress + "...");

                    obj.appliedProgress.Add(this);

                    //Fire event
                    if (obj.OnProgressChange != null)
                    {
                        obj.OnProgressChange();
                    }

                    if (obj.progress >= 1f)
                    {
                        //If this is done when setup, set the objective to silent...
                        if(onSetup)
                        {
                            Game.Log("Objective: Setting objective " + obj.queueElement.entryRef + " to silent (objective triggered on setup - most likely already completed)");
                            obj.queueElement.isSilent = true;
                        }
                        else
                        {
                            //Don't trigger this if being set up as saved data should be saves with the objective already
                            try
                            {
                                obj.Complete();

                                //Fire event
                                if (obj.OnComplete != null)
                                {
                                    obj.OnComplete();
                                }
                            }
                            catch
                            {
                                Game.Log("Completion trigger of an objective failed: This could be because it's triggering before loading the story configuration...");
                            }
                        }
                    }
                }
            }
        }
    }

    public enum ObjectiveTriggerType { playerAction, switchStateTrue, switchStateFalse, roomLightOn, inspectInteractable, evidencePinned, goToNode, keyInventory, knowDoorLockedStatus, goToAddress, goToRoom, playerHidden, escapeGameLocation, escapeBuilding, answerPhone, openEvidence, plotRoute, gameUnpaused, unlockDoor, goToPublicFacingAddress, answerPhoneAndEndCall, switchStateTrueForType, linkImageWithName, viewInteractable, noMoreObjectives, findFingerprints, findSurveillanceWith, findFingerprintsOnObject, accessCruncher, printVmail, successsfulSolve, makeCall, discoverParamour, onCompleteJob, identifyFinerprints, interactableRemoved, checkRecentCalls, acquireLockpicks, unlockInteractable, gamePaused, evidenceOpenAndDisplayed, collectHandIn, viewHandIn, submitCase, waitForCaseProcessing, surveillanceFlagFootage, findFingerprintsAtLocation, plotRouteToCallInvolving, notewriterWarned, exploreCrimeScene, nothing, playerHasApartment, answerLEMCall, discoverEvidence, accessApp, syncDiskInstallTutorial, onDialogSuccess, raiseFirstPersonItem, hasFPSInventory, sideMissionMeetTriggered, itemInInventory, itemIsPlacedAtSecretLocation, destroyItem, itemIsNear, playerActionNobodyHome, accessAnyCruncher, itemOfTypeIsNear};
    public ObjectiveTrigger objectiveAddOn;
    public float progressAdd;

    //Events
    public delegate void ProgressChange();
    public event ProgressChange OnProgressChange;

    public delegate void Completed();
    public event Completed OnComplete;

    //Constructors for multiple triggers
    public Objective(SpeechController.QueueElement newQueueElement)
    {
        queueElement = newQueueElement;

        thisCase = CasePanelController.Instance.activeCases.Find(item => item.id == queueElement.caseID);
        if (thisCase == null) thisCase = CasePanelController.Instance.archivedCases.Find(item => item.id == queueElement.caseID);
        if (thisCase == null) Game.LogError("Cannot find case with ID " + queueElement.caseID);

        if (!queueElement.isSilent)
        {
            if (thisCase.caseType == Case.CaseType.mainStory)
            {
                name = Strings.Get(ChapterController.Instance.loadedChapter.dictionary, queueElement.entryRef, useGenderReference: true, genderReference: Player.Instance);
                name = Strings.ComposeText(name, null); //Use player datasource for main story
            }
            else if (thisCase.caseType == Case.CaseType.sideJob || thisCase.caseType == Case.CaseType.murder || thisCase.caseType == Case.CaseType.retirement)
            {
                SideJob job = null;

                if (queueElement.jobRef > -1)
                {
                    if(!SideJobController.Instance.allJobsDictionary.TryGetValue(queueElement.jobRef, out job))
                    {
                        Game.LogError("Unable to get job from reference ID " + queueElement.jobRef);
                    }
                }
                else Game.LogError("Unable to get job from reference ID " + queueElement.jobRef);

                if(queueElement.useParsing)
                {
                    name = Strings.Get("missions.postings", queueElement.entryRef, useGenderReference: true, genderReference: Player.Instance);
                    name = Strings.ComposeText(name, job, additionalObject: job); //Use side job data source
                }
                else
                {
                    name = queueElement.entryRef;
                }
            }
        }
        else name = queueElement.entryRef;
            
        progress = 0f;

        Setup(thisCase);
    }

    public void Setup(Case newCase)
    {
        sprite = InterfaceControls.Instance.iconReference.Find(item => item.iconType == queueElement.icon).sprite; //Get sprite reference
        thisCase = newCase;

        if (thisCase.waitForObjectives == null) thisCase.waitForObjectives = new List<Objective>();

        Game.Log("Objective: Objective setup: " + queueElement.entryRef + " (Progress: " + progress + " Complete: " + isComplete + ")" );

        //Setup triggers
        foreach (ObjectiveTrigger trig in queueElement.triggers)
        {
            //Make sure triggers contain serialized data...
            trig.SetupNonSerialized();

            if (trig.addedToObjectives == null) trig.addedToObjectives = new List<Objective>();

            //Add to objective list
            if(!trig.addedToObjectives.Contains(this))
            {
                trig.addedToObjectives.Add(this);
            }

            //If already complete then trigger here: The applied progress list will ensure progress isn't applied twice
            if(trig.triggered)
            {
                trig.Trigger(true);
            }
        }

        //Add to wait for this to be completed
        if (queueElement.onComplete == OnCompleteAction.nextPartWhenAllCompleted)
        {
            if (!thisCase.waitForObjectives.Contains(this))
            {
                thisCase.waitForObjectives.Add(this);
            }
        }

        //Add to objective reference...
        if (thisCase.caseType == Case.CaseType.sideJob && thisCase.job != null)
        {
            if (!thisCase.job.objectiveReference.ContainsKey(queueElement.entryRef))
            {
                thisCase.job.objectiveReference.Add(queueElement.entryRef, new List<Objective>());
            }

            thisCase.job.objectiveReference[queueElement.entryRef].Add(this);
            Game.Log("Objective: Adding objective " + queueElement.entryRef + " to job reference (job " + thisCase.job.jobID + ", ref count " + thisCase.job.objectiveReference.Count + ")");
            thisCase.job.OnObjectiveChange();
        }

        isSetup = true;

        if(CasePanelController.Instance.activeCase == thisCase && !isCancelled && !isComplete)
        {
            Activate();
        }
        else
        {
            if(!thisCase.endedObjectives.Contains(this))
            {
                thisCase.endedObjectives.Add(this);
            }
        }
    }

    public void Activate(bool immediate = false)
    {
        Game.Log("Objective: Activate objective: " + queueElement.entryRef);
        
        if (objectiveListItem != null) Toolbox.Instance.DestroyObject(objectiveListItem);

        thisCase.inactiveCurrentObjectives.Remove(this);

        if (!thisCase.currentActiveObjectives.Contains(this))
        {
            thisCase.currentActiveObjectives.Add(this);
        }

        if (thisCase.caseType == Case.CaseType.sideJob)
        {
            thisCase.job.OnObjectiveChange();
        }

        if (immediate || (int)dispPhase >= 3 )
        {
            dispPhase = DisplayPhase.removeText;
            fadeInProgress = 0f;
            displayProgress = 0f;
            displayTime = 0f; //Use the same display time as speech
        }

        if (!InterfaceController.Instance.displayedObjectives.Contains(this))
        {
            InterfaceController.Instance.displayedObjectives.Add(this);
        }

        foreach (ObjectiveTrigger trig in queueElement.triggers)
        {
            if (trig.triggerType == ObjectiveTriggerType.playerAction || trig.triggerType == ObjectiveTriggerType.playerActionNobodyHome)
            {
                ActionController.Instance.OnPlayerAction += OnPlayerAction;
            }

            //Highlight actions
            if (trig.interactable != null && trig.hightlightAction.Length > 0)
            {
                trig.interactable.SetActionHighlight(trig.hightlightAction, true);

                //Make sure this action isn't disabled...
                trig.interactable.SetActionDisable(trig.hightlightAction, false);
            }
        }
    }

    //If there are any triggers with an action
    public void OnPlayerAction(AIActionPreset action, Interactable what, NewNode where, Actor who)
    {
        //Interactable must match
        foreach (ObjectiveTrigger trig in queueElement.triggers)
        {
            Game.Log("OnPlayerAction: " + action.name + ", " + trig.name + ", triggered: " + trig.triggered);
            if (trig.triggered) continue;

            if (trig.triggerType == ObjectiveTriggerType.playerAction)
            {
                //Object must match
                if(trig.interactable == what)
                {
                    //Action name must match (case insensitive)
                    if(trig.name.ToLower() == action.name.ToLower())
                    {
                        trig.Trigger();
                    }
                }
            }
            else if(trig.triggerType == ObjectiveTriggerType.playerActionNobodyHome)
            {
                if (trig.gameLocation.currentOccupants.Count <= 0)
                {
                    if (trig.name.ToLower() == action.name.ToLower())
                    {
                        foreach (NewNode.NodeAccess acc in trig.gameLocation.entrances)
                        {
                            if (acc.door != null)
                            {
                                if (acc.door.doorInteractable == what)
                                {
                                    trig.Trigger();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void Complete()
    {
        Game.Log("Chapter: Objective completed! " + queueElement.entryRef);
        isComplete = true;
        progress = 1f;

        //Trigger update of resolve question if it exists...
        if (thisCase.job != null)
        {
            if(ResultsController.Instance != null)
            {
                bool validationCheck = false;

                foreach(InputFieldController i in ResultsController.Instance.spawnedInputFields)
                {
                    if(i.question.inputType == Case.InputType.objective || i.question.inputType == Case.InputType.revengeObjective || i.question.inputType == Case.InputType.arrestPurp)
                    {
                        i.OnInputEdited();
                        validationCheck = true;
                    }
                }

                if(validationCheck) thisCase.ValidationCheck();
            }
        }

        //Execute action
        if (queueElement.onComplete == OnCompleteAction.nextChapterPart)
        {
            ChapterController.Instance.LoadPart(ChapterController.Instance.currentPart + 1);
        }
        else if (queueElement.onComplete == OnCompleteAction.nextPartWhenAllCompleted)
        {
            if (thisCase.waitForObjectives.Count <= 0 && Player.Instance.speechController.speechQueue.FindAll(item => item.isObjective).Count <= 0)
            {
                ChapterController.Instance.LoadPart(ChapterController.Instance.currentPart + 1);
            }
        }
        else if (queueElement.onComplete == OnCompleteAction.specificChapterByString)
        {
            ChapterController.Instance.LoadPart(queueElement.chapterString);
        }
        else if (queueElement.onComplete == OnCompleteAction.specificChapterWhenAllCompleted)
        {
            if (thisCase.waitForObjectives.Count <= 0 && Player.Instance.speechController.speechQueue.FindAll(item => item.isObjective).Count <= 0)
            {
                ChapterController.Instance.LoadPart(queueElement.chapterString);
            }
        }
        else if (queueElement.onComplete == OnCompleteAction.invokeFunction)
        {
            ChapterController.Instance.chapterScript.Invoke(queueElement.chapterString, 1f);
        }
        else if (queueElement.onComplete == OnCompleteAction.triggerSideJobFunction)
        {
            SideJobController.Instance.SideJobObjectiveComplete(thisCase.job, this);
        }
        else if (queueElement.onComplete == OnCompleteAction.triggerSideJobHandIn)
        {
            if (thisCase.job != null)
            {
                thisCase.job.Complete();
                //thisCase.job.CompleteCaseEndDialog();
            }
        }
        else if(queueElement.onComplete == OnCompleteAction.nextSideJobPhase)
        {
            if(thisCase.job != null)
            {
                int currentPhase = -1;
                int.TryParse(queueElement.chapterString, out currentPhase);

                if(currentPhase == thisCase.job.phase)
                {
                    thisCase.job.phase = currentPhase + 1;
                    Game.Log("Jobs: Side job " + thisCase.job.preset.name + " next side job phase trigger: " + thisCase.job.phase);
                }
                else
                {
                    Game.LogError("Invalid current phase detected in objective! Make sure you are passing it in the chapter string. Ignoring...");
                }
            }
        }
        else if (queueElement.onComplete == OnCompleteAction.submitSideJob)
        {
            if (thisCase.job != null)
            {
                thisCase.job.SubmitCase();
            }
        }

        //Store references of complete or cancelled objectives here...
        if (!thisCase.endedObjectives.Contains(this))
        {
            thisCase.endedObjectives.Add(this);
        }

        //Game message
        //InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, InterfaceControls.Instance.objectiveCompleteSprite, 0, StringTables.Get("ui.gamemessage", "Objective Complete") + ": " + name);

        if ((int)dispPhase <= 2) displayProgress = 99999; //If there's not been enough time to display the title text then skip it
        if (pointer != null) pointer.Remove(); //Remove world pointer
        if (awarenessIcon != null) awarenessIcon.Remove(); //Remove awareness icon
    }

    public void Cancel()
    {
        Game.Log("Chapter: Objective cancelled! " + queueElement.entryRef);
        isCancelled = true;

        //Store references of complete or cancelled objectives here...
        if(!thisCase.endedObjectives.Contains(this))
        {
            thisCase.endedObjectives.Add(this);
        }

        //Game message
        //InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, InterfaceControls.Instance.objectiveCompleteSprite, 0, StringTables.Get("ui.gamemessage", "Objective Complete") + ": " + name);

        if ((int)dispPhase <= 2) displayProgress = 99999; //If there's not been enough time to display the title text then skip it
        if (pointer != null) pointer.Remove(); //Remove world pointer
        if (awarenessIcon != null) awarenessIcon.Remove(); //Remove awareness icon
    }

    public void Remove()
    {
        //Highlight actions - clear
        foreach (ObjectiveTrigger trig in queueElement.triggers)
        {
            if (trig.triggerType == ObjectiveTriggerType.playerAction || trig.triggerType == ObjectiveTriggerType.playerActionNobodyHome)
            {
                ActionController.Instance.OnPlayerAction -= OnPlayerAction;
            }

            if (trig.interactable != null && trig.hightlightAction.Length > 0)
            {
                trig.interactable.SetActionHighlight(trig.hightlightAction, false);
            }
        }

        if (objectiveList != null)
        {
            if (isComplete) objectiveList.OnComplete();
            objectiveList.Remove();
        }

        if (thisCase.caseType == Case.CaseType.sideJob)
        {
            thisCase.job.OnObjectiveChange();
        }

        //Remove from waiting list
        thisCase.waitForObjectives.Remove(this);

        if ((int)dispPhase <= 2) displayProgress = 99999; //If there's not been enough time to display the title text then skip it
        if (pointer != null) pointer.Remove(); //Remove world pointer
        if (awarenessIcon != null) awarenessIcon.Remove(); //Remove awareness icon

        //Make sure main text is reset
        if (InterfaceController.Instance.currentlyDisplaying == this)
        {
            //Reset display text
            InterfaceController.Instance.objectiveTextBackground.gameObject.SetActive(false);
            InterfaceController.Instance.objectiveTitleText.text = string.Empty;
            InterfaceController.Instance.objectiveTitleTextRenderer.SetAlpha(1f);
            InterfaceController.Instance.objectiveBackgroundRenderer.SetAlpha(1f);
            InterfaceController.Instance.objectiveTextBackground.anchoredPosition = new Vector2(0, -180); //Reset position
            InterfaceController.Instance.currentlyDisplaying = null; //Clear the way to display the next objective
        }

        InterfaceController.Instance.displayedObjectives.Remove(this);

        //Add to inactive...
        if(!isComplete && !isCancelled && !thisCase.inactiveCurrentObjectives.Contains(this))
        {
            thisCase.inactiveCurrentObjectives.Add(this);
        }

        thisCase.currentActiveObjectives.Remove(this); //Remove from objectives list (this will stop updates)
    }

    //When active, this is run every frame
    public void CheckingLoop()
    {
        if ((isComplete || isCancelled || queueElement.isSilent) && (int)dispPhase <= 2) displayProgress = 99999; //If there's not been enough time to display the title text then skip it

        //Move to show main text only if there's nothing currently displayed
        if (dispPhase == DisplayPhase.preDisplay)
        {
            if (InterfaceController.Instance.currentlyDisplaying == null)
            {
                dispPhase = DisplayPhase.fadeInMainText;
                InterfaceController.Instance.currentlyDisplaying = this;
            }
        }
        else if (dispPhase == DisplayPhase.fadeInMainText)
        {
            if(!queueElement.isSilent)
            {
                InterfaceController.Instance.objectiveTextBackground.gameObject.SetActive(true);
                InterfaceController.Instance.objectiveTitleText.text = name;
            }

            //Calculate display time for later
            displayTime = InterfaceControls.Instance.visualTalkDisplayDestroyDelay + ((InterfaceController.Instance.objectiveTitleText.text.Length * InterfaceControls.Instance.visualTalkDisplayStringLengthModifier * 0.85f ) / Game.Instance.textSpeed);

            if (fadeInProgress < 1f)
            {
                fadeInProgress += Time.deltaTime * 2f;
                fadeInProgress = Mathf.Clamp01(fadeInProgress);
                InterfaceController.Instance.objectiveBackgroundRenderer.SetAlpha(fadeInProgress);
                InterfaceController.Instance.objectiveTitleTextRenderer.SetAlpha(fadeInProgress);
            }
            else
            {
                displayPointer = true;
                dispPhase = DisplayPhase.displayMainText;
            }
        }
        else if (dispPhase == DisplayPhase.displayMainText)
        {
            if (displayProgress < displayTime)
            {
                displayProgress += Time.deltaTime;
            }
            else
            {
                displayPointer = true;
                dispPhase = DisplayPhase.removeText;
            }
        }
        else if (dispPhase == DisplayPhase.removeText)
        {
            displayPointer = true;

            //Spawn a list item
            if (objectiveListItem == null && !queueElement.isSilent)
            {
                objectiveListItem = Toolbox.Instance.SpawnObject(PrefabControls.Instance.checklistButton, InterfaceController.Instance.objectiveSideAnchor);
                objectiveListRect = objectiveListItem.GetComponent<RectTransform>();
                objectiveList = objectiveListItem.GetComponent<ChecklistButtonController>();
                objectiveList.Setup(this);

                //Set postion to the main text
                objectiveListRect.position = InterfaceController.Instance.objectiveTextBackground.position;
            }

            //Fade in
            if(objectiveList != null)
            {
                if (objectiveList.fadeInProgress < 1f)
                {
                    objectiveList.fadeInProgress += Time.deltaTime * 2f;
                    objectiveList.fadeInProgress = Mathf.Clamp01(objectiveList.fadeInProgress);

                    objectiveList.bgRend.SetAlpha(objectiveList.fadeInProgress * InterfaceController.Instance.objectivesAlpha);
                    objectiveList.textRend.SetAlpha(objectiveList.fadeInProgress * InterfaceController.Instance.objectivesAlpha);
                    objectiveList.iconRend.SetAlpha(objectiveList.fadeInProgress * InterfaceController.Instance.objectivesAlpha);
                    objectiveList.barRend.SetAlpha(objectiveList.fadeInProgress * InterfaceController.Instance.objectivesAlpha);

                    //The text moves to the top right and fades
                    InterfaceController.Instance.objectiveTitleTextRenderer.SetAlpha(1f - objectiveList.fadeInProgress * 2.5f);
                    InterfaceController.Instance.objectiveBackgroundRenderer.SetAlpha(1f - objectiveList.fadeInProgress * 2.5f);
                    InterfaceController.Instance.objectiveTextBackground.anchoredPosition = Vector3.Lerp(InterfaceController.Instance.objectiveTextBackground.anchoredPosition, new Vector2(700, InterfaceController.Instance.objectiveTextBackground.anchoredPosition.y), Time.deltaTime * 12f);
                }
                else
                {
                    dispPhase = DisplayPhase.displayingList;
                    InterfaceController.Instance.objectiveTextBackground.gameObject.SetActive(false);
                    InterfaceController.Instance.objectiveTitleText.text = string.Empty;
                    InterfaceController.Instance.objectiveTitleTextRenderer.SetAlpha(1f);
                    InterfaceController.Instance.objectiveBackgroundRenderer.SetAlpha(1f);
                    InterfaceController.Instance.objectiveTextBackground.anchoredPosition = new Vector2(0, -180); //Reset position
                    InterfaceController.Instance.currentlyDisplaying = null; //Clear the way to display the next objective
                    dispPhase = DisplayPhase.waitForComplete;
                }
            }
            else
            {
                InterfaceController.Instance.objectiveTextBackground.gameObject.SetActive(false);
                InterfaceController.Instance.objectiveTitleText.text = string.Empty;
                InterfaceController.Instance.currentlyDisplaying = null; //Clear the way to display the next objective
                dispPhase = DisplayPhase.waitForComplete;
            }
        }
        else if(dispPhase == DisplayPhase.waitForComplete)
        {
            displayPointer = true;

            //Spawn a list item
            if (objectiveListItem == null && !queueElement.isSilent)
            {
                objectiveListItem = Toolbox.Instance.SpawnObject(PrefabControls.Instance.checklistButton, InterfaceController.Instance.objectiveSideAnchor);
                objectiveListRect = objectiveListItem.GetComponent<RectTransform>();
                objectiveList = objectiveListItem.GetComponent<ChecklistButtonController>();
                objectiveList.Setup(this);

                //Set postion to the main text
                objectiveListRect.position = InterfaceController.Instance.objectiveTextBackground.position;
            }

            if (isComplete || isCancelled)
            {
                if (objectiveList != null)
                {
                    if(isComplete) objectiveList.OnComplete();
                    objectiveList.Remove();
                }

                //Remove from waiting list
                thisCase.waitForObjectives.Remove(this);

                Remove();
            }
        }

        //Display UI Pointer
        if(displayPointer && Game.Instance.objectiveMarkers && queueElement.usePointer && progress < 1f)
        {
            if(awarenessIcon == null)
            {
                //Create material for UI awareness pointer
                Material createdMaterial = Toolbox.Instance.SpawnMaterial(InterfaceControls.Instance.speech);
                createdMaterial.SetTexture("_UnlitColorMap", sprite.texture);

                //Update position from transform
                awarenessIcon = InterfaceController.Instance.AddAwarenessIcon(InterfaceController.AwarenessType.position, InterfaceController.AwarenessBehaviour.invisibleInfront, null, null, queueElement.pointerPosition, createdMaterial, 0);
            }

            //Create UI pointer
            if(pointer == null && !isComplete && !isCancelled && !queueElement.isSilent)
            {
                pointer = InterfaceController.Instance.AddUIPointer(this);
            }
        }

        //Tell the player to crouch
        if(displayPointer && queueElement.allowCrouchPrompt)
        {
            Vector3Int nodePos = CityData.Instance.RealPosToNodeInt(queueElement.pointerPosition); //Get the node
            NewNode node = null;

            if(PathFinder.Instance.nodeMap.TryGetValue(nodePos, out node))
            {
                if(queueElement.pointerPosition.y - node.position.y < 0.8f)
                {
                    //Player is close enough to the objective
                    if (Vector3.Distance(Player.Instance.transform.position, queueElement.pointerPosition) < 1.8f)
                    {
                        if (crouchPromtTimer <= 0f)
                        {
                            crouchPromtTimer = 30f;
                            ControlsDisplayController.Instance.DisplayControlIcon(InteractablePreset.InteractionKey.crouch, "Crouch", InterfaceControls.Instance.controlIconDisplayTime, true);

                            //if (Player.Instance.speechQueue.Count <= 0)
                            //{
                            //    //Display crouch prompt
                            //    //Player.Instance.Speak("ui.interface", "crouch_prompt", false, false, null, false, 0f, true, Color.white);

                            //}
                        }
                    }
                    else crouchPromtTimer = 0f;
                }
            }

            if(crouchPromtTimer > 0f)
            {
                crouchPromtTimer -= Time.deltaTime;
            }
        }
        
        //Trigger check
        if(!isComplete && !isCancelled)
        {
            foreach (ObjectiveTrigger trig in queueElement.triggers)
            {
                if (trig.triggered) continue;

                //The following need constant updates to check the switch states. Events probably won't work very well as the object may not be spawned yet.
                if (trig.triggerType == ObjectiveTriggerType.switchStateTrue)
                {
                    if (trig.interactable != null)
                    {
                        if (trig.interactable.sw0)
                        {
                            trig.Trigger();
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.switchStateFalse)
                {
                    if (trig.interactable != null)
                    {
                        if (!trig.interactable.sw0)
                        {
                            trig.Trigger();
                        }
                    }
                }
                //Checks if switch is true across an object type in this gamelocation
                else if (trig.triggerType == ObjectiveTriggerType.switchStateTrueForType)
                {
                    if (trig.interactable != null && trig.gameLocation != null)
                    {
                        foreach (NewRoom r in trig.gameLocation.rooms)
                        {
                            foreach (NewNode n in r.nodes)
                            {
                                foreach (Interactable i in n.interactables)
                                {
                                    //Same type
                                    if (i.preset == trig.interactable.preset)
                                    {
                                        if (i.sw0)
                                        {
                                            trig.Trigger();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.roomLightOn)
                {
                    if (trig.room.mainLightStatus)
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.inspectInteractable)
                {
                    if (trig.interactable != null)
                    {
                        if (trig.interactable.ins)
                        {
                            trig.Trigger();
                        }
                    }
                    else Game.Log("Objective: No interactable for " + queueElement.entryRef + " (" + trig.interactableID + ")");
                }
                else if (trig.triggerType == ObjectiveTriggerType.itemInInventory)
                {
                    if (trig.interactable != null)
                    {
                        if (FirstPersonItemController.Instance.slots.Exists(item => item.GetInteractable() == trig.interactable))
                        {
                            Game.Log("Objective: Interactable " + trig.interactableID + " is in inventory");
                            trig.Trigger();
                        }
                        else
                        {
                            //Game.Log("Objective: Cannot find interactable " + trig.interactableID + " in inventory");
                        }
                    }
                    else Game.Log("Objective: No interactable for " + queueElement.entryRef + " (" + trig.interactableID + ")");
                }
                else if (trig.triggerType == ObjectiveTriggerType.evidencePinned)
                {
                    //Display case board prompt
                    ControlsDisplayController.Instance.DisplayControlIcon(InteractablePreset.InteractionKey.caseBoard, "Case Board", 2f);

                    foreach(Case a in CasePanelController.Instance.activeCases)
                    {
                        //Item is pinned in an active case...
                        if(trig.evidence == null)
                        {
                            Game.LogError("Trigger evidence is null: " + trig.name);
                            continue;
                        }
                        else if (a.caseElements.Exists(item => item.id == trig.evidence.evID))
                        {
                             trig.Trigger();
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.goToNode)
                {
                    if (Player.Instance.currentNode == trig.node)
                    {
                        //Also can't be in vent!
                        if (!Player.Instance.inAirVent)
                        {
                            trig.Trigger();
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.keyInventory)
                {
                    if(trig.door != null)
                    {
                        if (Player.Instance.keyring.Contains(trig.door))
                        {
                            trig.Trigger();
                        }
                    }
                    else if(trig.interactable != null)
                    {
                        if (Player.Instance.playerKeyringInt.Contains(trig.interactable))
                        {
                            trig.Trigger();
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.interactableRemoved)
                {
                    if (trig.interactable == null || trig.interactable.rem || trig.interactable.inInventory == Player.Instance)
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.knowDoorLockedStatus)
                {
                    if (trig.door.knowLockStatus)
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.unlockDoor)
                {
                    if (!trig.door.isLocked)
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.unlockInteractable)
                {
                    if (trig.interactable != null && trig.interactable.lockInteractable != null && !trig.interactable.lockInteractable.locked)
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.goToAddress)
                {
                    //Game.Log("Player: " + Player.Instance.currentGameLocation.name + " dest: " + trig.gameLocation);

                    if (Player.Instance.currentGameLocation == trig.gameLocation)
                    {
                        //Also can't be in vent!
                        if (!Player.Instance.inAirVent)
                        {
                            trig.Trigger();
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.exploreCrimeScene)
                {
                    //Game.Log("Player: " + Player.Instance.currentGameLocation.name + " dest: " + trig.gameLocation);

                    if (Player.Instance.currentGameLocation == trig.gameLocation)
                    {
                        //Also can't be in vent!
                        if (!Player.Instance.inAirVent)
                        {
                            trig.Trigger();
                        }
                    }

                    //Also complete if no longer a crime scene
                    if(MurderController.Instance.currentActiveCase == null || (!GameplayController.Instance.enforcerCalls.ContainsKey(trig.gameLocation) && !trig.gameLocation.isCrimeScene))
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.goToRoom)
                {
                    if (Player.Instance.currentRoom == trig.room)
                    {
                        //Also can't be in vent!
                        if (!Player.Instance.inAirVent)
                        {
                            trig.Trigger();
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.playerHidden)
                {
                    if (Player.Instance.isHiding || Player.Instance.inAirVent || (Player.Instance.currentRoom != null && Player.Instance.currentRoom.preset == InteriorControls.Instance.closet))
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.escapeGameLocation)
                {
                    if (Player.Instance.currentGameLocation != trig.gameLocation && !Player.Instance.inAirVent)
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.escapeBuilding)
                {
                    if (Player.Instance.currentBuilding != trig.gameLocation.building)
                    {
                        Game.Log("Current building is: " + Player.Instance.currentBuilding);
                        Game.Log("Trigger building is: " + trig.gameLocation.building);
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.answerPhone)
                {
                    if (Player.Instance.answeringPhone != null && Player.Instance.answeringPhone.interactable == trig.interactable)
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.answerPhoneAndEndCall)
                {
                    if (trig.interactable != null && trig.interactable.t.activeCall.Count <= 0)
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.openEvidence)
                {
                    if (InterfaceController.Instance.activeWindows.Exists(item => item.passedEvidence != null && item.passedEvidence.evID == trig.evidence.evID))
                    {
                        trig.Trigger();
                    }
                    else
                    {
                        foreach (Case a in CasePanelController.Instance.activeCases)
                        {
                            //Item is pinned in an active case...
                            if (trig.evidence == null)
                            {
                                Game.LogError("Trigger evidence is null: " + trig.name + " for " + queueElement.entryRef + " (completing)");
                                trig.Trigger();
                            }
                            else if (a.caseElements.Exists(item => item.id == trig.evidence.evID))
                            {
                                List<InfoWindow> matchingWindows = InterfaceController.Instance.activeWindows.FindAll(item => item.passedEvidence != null && item.passedEvidence.evID == trig.evidence.evID);

                                trig.Trigger();
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.discoverEvidence)
                {
                    if (InterfaceController.Instance.activeWindows.Exists(item => item.passedEvidence != null && item.passedEvidence.evID == trig.evidence.evID))
                    {
                        trig.Trigger();
                    }
                    else if (trig.evidence.isFound || InterfaceController.Instance.activeWindows.Exists(item => item.passedEvidence != null && item.passedEvidence.evID == trig.evidence.evID))
                    {
                        trig.Trigger();
                    }
                    else
                    {
                        //foreach(InfoWindow w in InterfaceController.Instance.activeWindows)
                        //{
                        //    if(w.passedEvidence != null) Game.Log(w.passedEvidence.evID);
                        //}

                        foreach (Case a in CasePanelController.Instance.activeCases)
                        {
                            //Item is pinned in an active case...
                            if (trig.evidence == null)
                            {
                                continue;
                            }
                            else if (a.caseElements.Exists(item => item.id == trig.evidence.evID))
                            {
                                List<InfoWindow> matchingWindows = InterfaceController.Instance.activeWindows.FindAll(item => item.passedEvidence != null && item.passedEvidence.evID == trig.evidence.evID);

                                trig.Trigger();
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.evidenceOpenAndDisplayed)
                {
                    if (InterfaceController.Instance.desktopMode && InterfaceController.Instance.activeWindows.Exists(item => item.passedEvidence == trig.evidence))
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.collectHandIn)
                {
                    if (thisCase != null && thisCase.caseStatus == Case.CaseStatus.handInCollected)
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.viewHandIn)
                {
                    if (thisCase != null && thisCase.caseStatus == Case.CaseStatus.handInCollected && CasePanelController.Instance.activeCase == thisCase)
                    {
                        //Update resolve window if there is one...
                        if (ResolveController.Instance != null)
                        {
                            trig.Trigger();
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.submitCase)
                {
                    if (thisCase != null && (thisCase.caseStatus == Case.CaseStatus.submitted || thisCase.caseStatus == Case.CaseStatus.closable || thisCase.caseStatus == Case.CaseStatus.archived) && CasePanelController.Instance.activeCase == thisCase)
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.plotRoute)
                {
                    if (MapController.Instance.playerRoute != null)
                    {
                        if (MapController.Instance.playerRoute.end.gameLocation == trig.gameLocation || MapController.Instance.playerRoute.destinationTextOverride == trig.gameLocation)
                        {
                            trig.Trigger();
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.gameUnpaused)
                {
                    if (SessionData.Instance.play)
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.gamePaused)
                {
                    if (!SessionData.Instance.play)
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.goToPublicFacingAddress)
                {
                    if (Player.Instance.currentGameLocation.thisAsAddress != null)
                    {
                        if (Player.Instance.currentGameLocation.thisAsAddress.company != null)
                        {
                            if (Player.Instance.currentGameLocation.thisAsAddress.company.preset.publicFacing)
                            {
                                if (Player.Instance.currentGameLocation.thisAsAddress.company.openForBusinessActual && Player.Instance.currentGameLocation.thisAsAddress.company.openForBusinessDesired)
                                {
                                    //Also can't be in vent!
                                    if (!Player.Instance.inAirVent)
                                    {
                                        //Must have telephones!
                                        if (Player.Instance.currentGameLocation.telephones.Count > 0)
                                        {
                                            trig.Trigger();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.linkImageWithName)
                {
                    List<Evidence.DataKey> getKeys = new List<Evidence.DataKey>();
                    getKeys.Add(Evidence.DataKey.name);

                    List<Evidence.DataKey> tiedKeys = trig.evidence.GetTiedKeys(getKeys);

                    if (tiedKeys.Contains(Evidence.DataKey.photo))
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.identifyFinerprints)
                {
                    List<Evidence.DataKey> getKeys = new List<Evidence.DataKey>();
                    getKeys.Add(Evidence.DataKey.photo);

                    List<Evidence.DataKey> tiedKeys = trig.evidence.GetTiedKeys(getKeys);

                    if (tiedKeys.Contains(Evidence.DataKey.fingerprints))
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.viewInteractable)
                {
                    if (InteractionController.Instance.currentLookingAtInteractable != null && InteractionController.Instance.currentLookingAtInteractable.interactable == trig.interactable)
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.noMoreObjectives)
                {
                    if (thisCase.currentActiveObjectives.Count == 1 && Player.Instance.speechController.speechQueue.Count <= 0)
                    {
                        if(thisCase.currentActiveObjectives[0].queueElement.triggers.Contains(trig))
                        {
                            trig.Trigger();
                        }
                    }
                }
                else if(trig.triggerType == ObjectiveTriggerType.findFingerprints)
                {
                    Human h = trig.evidence.writer as Human;

                    if(h != null)
                    {
                        if (InterfaceController.Instance.activeWindows.Exists(item => (item.passedEvidence as EvidenceFingerprint) != null && item.passedEvidence.writer == h))
                        {
                            trig.Trigger();
                        }
                        else
                        {
                            foreach (Case a in CasePanelController.Instance.activeCases)
                            {
                                foreach(Case.CaseElement el in a.caseElements)
                                {
                                    Evidence ev = null;

                                    //Spawn an evidence button
                                    if(Toolbox.Instance.TryGetEvidence(el.id, out ev))
                                    {
                                        EvidenceFingerprint fp = ev as EvidenceFingerprint;

                                        if(fp != null)
                                        {
                                            if(fp.writer == h)
                                            {
                                                trig.Trigger();
                                                break;
                                            }
                                        }
                                    }
                                }

                                if (trig.triggered) break;
                            }
                        }
                    }
                }
                else if(trig.triggerType == ObjectiveTriggerType.findFingerprintsOnObject)
                {
                    Human h = trig.evidence.writer as Human;

                    List<InfoWindow> activeWindow = InterfaceController.Instance.activeWindows.FindAll(item => (item.passedEvidence as EvidenceFingerprint) != null && item.passedEvidence.writer == h);

                    if (h != null && activeWindow != null)
                    {
                        foreach(InfoWindow w in activeWindow)
                        {
                            if(w.passedEvidence != null)
                            {
                                EvidenceFingerprint fp = w.passedEvidence as EvidenceFingerprint;

                                if(fp != null)
                                {
                                    if(fp.interactable != null && fp.interactable.print != null && fp.interactable.print.Count > 0)
                                    {
                                        if(trig.interactable != null && fp.interactable.print[0].interactableID == trig.interactableID)
                                        {
                                            trig.Trigger();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.findFingerprintsAtLocation)
                {
                    Human h = trig.evidence.writer as Human;

                    List<InfoWindow> activeWindow = InterfaceController.Instance.activeWindows.FindAll(item => (item.passedEvidence as EvidenceFingerprint) != null && item.passedEvidence.writer == h);

                    if (h != null && activeWindow != null)
                    {
                        foreach (InfoWindow w in activeWindow)
                        {
                            if (w.passedEvidence != null)
                            {
                                EvidenceFingerprint fp = w.passedEvidence as EvidenceFingerprint;

                                if (fp != null)
                                {
                                    if (fp.interactable != null && fp.interactable.print != null && fp.interactable.print.Count > 0)
                                    {
                                        if (trig.interactable != null && fp.interactable.node.gameLocation == trig.gameLocation)
                                        {
                                            trig.Trigger();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if(trig.triggerType == ObjectiveTriggerType.findSurveillanceWith)
                {
                    Human h = trig.evidence.writer;

                    if(h != null)
                    {
                        foreach (Interactable camera in trig.gameLocation.securityCameras)
                        {
                            if(SceneCapture.Instance.currrentlyViewing != null)
                            {
                                if(SceneCapture.Instance.currrentlyViewing.recorder.interactable == camera)
                                {
                                    if(SceneCapture.Instance.currrentlyViewing.aCap.Exists(item => item.id == h.humanID))
                                    {
                                        trig.Trigger();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.surveillanceFlagFootage)
                {
                    //Open evidence
                    if (Player.Instance.computerInteractable != null && trig.evidence != null)
                    {
                        if (Player.Instance.computerInteractable.controller != null)
                        {
                            if (Player.Instance.computerInteractable.controller.computer != null)
                            {
                                foreach (GameObject app in Player.Instance.computerInteractable.controller.computer.spawnedContent)
                                {
                                    SurveillanceApp getSurv = app.GetComponent<SurveillanceApp>();

                                    if (getSurv != null)
                                    {
                                        if(getSurv.flaggedActor == trig.evidence.controller)
                                        {
                                            trig.Trigger();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if(trig.triggerType == ObjectiveTriggerType.accessCruncher)
                {
                    if(trig.interactable != null && InteractionController.Instance.lockedInInteraction == trig.interactable)
                    {
                        if(trig.interactable.controller != null)
                        {
                            if(trig.interactable.controller.computer != null)
                            {
                                if(trig.interactable.controller.computer.loggedInAs != null)
                                {
                                    trig.Trigger();
                                }
                            }
                        }
                    }
                    else if(InteractionController.Instance.lockedInInteraction != null && Player.Instance.computerInteractable != null && trig.evidence != null)
                    {
                        if (InteractionController.Instance.lockedInInteraction.controller != null)
                        {
                            if (InteractionController.Instance.lockedInInteraction.controller.computer != null)
                            {
                                if (InteractionController.Instance.lockedInInteraction.controller.computer.loggedInAs == trig.evidence.controller)
                                {
                                    trig.Trigger();
                                }
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.accessAnyCruncher)
                {
                    if (trig.interactable != null && InteractionController.Instance.lockedInInteraction == trig.interactable)
                    {
                        if (trig.interactable.controller != null)
                        {
                            if (trig.interactable.controller.computer != null)
                            {
                                trig.Trigger();
                            }
                        }
                    }
                    else if (InteractionController.Instance.lockedInInteraction != null && Player.Instance.computerInteractable != null && trig.evidence != null)
                    {
                        if (InteractionController.Instance.lockedInInteraction.controller != null)
                        {
                            if (InteractionController.Instance.lockedInInteraction.controller.computer != null)
                            {
                                trig.Trigger();
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.accessApp)
                {
                    if (trig.interactable != null && InteractionController.Instance.lockedInInteraction == trig.interactable)
                    {
                        if (trig.interactable.controller != null)
                        {
                            if (trig.interactable.controller.computer != null)
                            {
                                if (trig.interactable.controller.computer.loggedInAs != null)
                                {
                                    if(trig.interactable.controller.computer.currentApp != null)
                                    {
                                        if(trig.interactable.controller.computer.currentApp.name == trig.name)
                                        {
                                            trig.Trigger();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (InteractionController.Instance.lockedInInteraction != null && Player.Instance.computerInteractable != null && trig.evidence != null)
                    {
                        if (InteractionController.Instance.lockedInInteraction.controller != null)
                        {
                            if (InteractionController.Instance.lockedInInteraction.controller.computer != null)
                            {
                                if (InteractionController.Instance.lockedInInteraction.controller.computer.loggedInAs == trig.evidence.controller)
                                {
                                    if (InteractionController.Instance.lockedInInteraction.controller.computer.currentApp != null)
                                    {
                                        if (InteractionController.Instance.lockedInInteraction.controller.computer.currentApp.name == trig.name)
                                        {
                                            trig.Trigger();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if(trig.triggerType == ObjectiveTriggerType.syncDiskInstallTutorial)
                {
                    if(UpgradesController.Instance.isOpen)
                    {
                        if(Player.Instance.hideInteractable != null && Player.Instance.hideInteractable.preset.specialCaseFlag == InteractablePreset.SpecialCase.syncBed)
                        {
                            trig.Trigger();
                        }
                    }
                }
                else if(trig.triggerType == ObjectiveTriggerType.printVmail)
                {
                    InfoWindow activeWindow = InterfaceController.Instance.activeWindows.Find(item => (item.passedEvidence as EvidencePrintedVmail) != null && (item.passedEvidence as EvidencePrintedVmail).thread.treeID == trig.name);

                    if (activeWindow != null)
                    {
                        trig.Trigger();
                    }
                }
                else if(trig.triggerType == ObjectiveTriggerType.notewriterWarned)
                {
                    if(ChapterController.Instance != null && ChapterController.Instance.chapterScript != null)
                    {
                        ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                        if(intro != null)
                        {
                            if(intro.layLowGoal != null || (intro.noteWriter.isDead && InteractionController.Instance.currentLookingAtInteractable != null && InteractionController.Instance.currentLookingAtInteractable.interactable.isActor == intro.noteWriter))
                            {
                                trig.Trigger();
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.answerLEMCall)
                {
                    //if (ChapterController.Instance != null && ChapterController.Instance.chapterScript != null)
                    //{
                    //    ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                    //    if (intro != null)
                    //    {
                    //        if (intro.LEMCallCompleted)
                    //        {
                    //            trig.Trigger();
                    //        }
                    //    }
                    //}
                }
                else if (trig.triggerType == ObjectiveTriggerType.playerHasApartment)
                {
                    if (Player.Instance.home != null)
                    {
                        trig.Trigger();
                    }
                }
                else if(trig.triggerType == ObjectiveTriggerType.successsfulSolve)
                {
                    if(trig.interactable != null && trig.interactable.isActor != null && trig.interactable.isActor.ai.isConvicted)
                    {
                        trig.Trigger();
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.acquireLockpicks)
                {
                    if (trig.interactable != null && trig.interactable.lockInteractable != null)
                    {
                        int needed = Toolbox.Instance.GetLockpicksNeeded(trig.interactable.val);
                        float newProgress = GameplayController.Instance.lockPicks / (float)needed;

                        SetProgress(newProgress);

                        if (GameplayController.Instance.lockPicks >= needed)
                        {
                            trig.Trigger();
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.plotRouteToCallInvolving)
                {
                    if (MapController.Instance.playerRoute != null && MapController.Instance.playerRoute.end.gameLocation != null)
                    {
                        if(trig.gameLocation != null && trig.gameLocation.building != null)
                        {
                            if(trig.gameLocation.building.callLog.Exists(item => (item.toNS != null && item.fromNS != null && item.toNS.interactable.node.gameLocation == trig.gameLocation && MapController.Instance.playerRoute.end.gameLocation.building == item.fromNS.interactable.node.gameLocation.building) || (item.fromNS != null && item.toNS != null && item.fromNS.interactable.node.gameLocation == trig.gameLocation && MapController.Instance.playerRoute.end.gameLocation.building == item.toNS.interactable.node.gameLocation.building)))
                            {
                                trig.Trigger();
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.makeCall)
                {
                    if(Player.Instance.answeringPhone != null)
                    {
                        //Game.Log("Jobs: Player answering phone");

                        if(Player.Instance.answeringPhone.activeCall.Count > 0)
                        {
                            //Game.Log("Jobs: Player active call: " + trig.name);
                            int toNumber = -1;

                            if(int.TryParse(trig.name, out toNumber))
                            {
                                //Game.Log("Jobs: Name parse: " + toNumber);

                                TelephoneController.CallSource source = null;

                                if (TelephoneController.Instance.fakeTelephoneDictionary.ContainsKey(toNumber))
                                {
                                    source = TelephoneController.Instance.fakeTelephoneDictionary[toNumber];
                                }

                                if ((source != null && Player.Instance.answeringPhone.activeCall[0].source == source) || Player.Instance.answeringPhone.activeCall[0].to == toNumber)
                                {
                                    if(Player.Instance.answeringPhone.activeCall[0].state == TelephoneController.CallState.started)
                                    {
                                        trig.Trigger();
                                    }
                                }
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.discoverParamour)
                {
                    if (trig.evidence != null)
                    {
                        if (trig.evidence.discoveryProgress.Contains(Evidence.Discovery.paramourDiscovery))
                        {
                            trig.Trigger();
                        }
                    }
                }
                else if(trig.triggerType == ObjectiveTriggerType.checkRecentCalls)
                {
                    if(trig.interactable != null)
                    {
                        if(trig.interactable.recentCallCheck != 0)
                        {
                            trig.Trigger();
                        }
                    }
                }
                else if(trig.triggerType == ObjectiveTriggerType.raiseFirstPersonItem)
                {
                    if(trig.interactable != null)
                    {
                        if(BioScreenController.Instance.selectedSlot != null)
                        {
                            if(BioScreenController.Instance.selectedSlot.interactableID > -1)
                            {
                                if(BioScreenController.Instance.selectedSlot.GetInteractable() == trig.interactable)
                                {
                                    if(FirstPersonItemController.Instance.isRaised)
                                    {
                                        trig.Trigger();
                                    }
                                }
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.hasFPSInventory)
                {
                    foreach(FirstPersonItemController.InventorySlot slot in FirstPersonItemController.Instance.slots)
                    {
                        FirstPersonItem fps = slot.GetFirstPersonItem();

                        if(fps != null)
                        {
                            if(fps.name.ToLower() == trig.name.ToLower())
                            {
                                trig.Trigger();
                                break;
                            }
                        }
                    }
                }
                else if(trig.triggerType == ObjectiveTriggerType.sideMissionMeetTriggered)
                {
                    if (thisCase != null && thisCase.job != null)
                    {
                        SideJobStealBriefcase briefcase = thisCase.job as SideJobStealBriefcase;

                        if(briefcase != null)
                        {
                            if(briefcase.triggeredMeet)
                            {
                                trig.Trigger();
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.itemIsPlacedAtSecretLocation)
                {
                    if (trig.interactable != null && trig.job != null && trig.job.secretLocationFurniture != 0)
                    {
                        if(trig.interactable.inInventory == null)
                        {
                            Vector3 wPosNode = CityData.Instance.NodeToRealpos(trig.job.secretLocationNode);

                            float dist = Vector3.Distance(trig.interactable.wPos, wPosNode);

                            if(dist <= 1.8f)
                            {
                                trig.Trigger();
                            }
                            else
                            {
                                Game.Log("Jobs: Item distance to postion: " + dist);
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.itemIsNear)
                {
                    if (trig.interactable != null)
                    {
                        if (trig.interactable.inInventory == null)
                        {
                            float dist = Vector3.Distance(trig.interactable.GetWorldPosition(), trig.position);

                            if (dist <= 0.5f)
                            {
                                trig.Trigger();
                            }
                        }
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.itemOfTypeIsNear)
                {
                    if (trig.interactable != null)
                    {
                        NewNode node;

                        if (PathFinder.Instance.nodeMap.TryGetValue(CityData.Instance.RealPosToNodeInt(trig.position), out node))
                        {
                            foreach (Interactable i in node.interactables)
                            {
                                if(i.preset == trig.interactable.preset)
                                {
                                    if (i.inInventory == null)
                                    {
                                        float dist = Vector3.Distance(i.GetWorldPosition(), trig.position);

                                        if (dist <= 0.5f)
                                        {
                                            trig.Trigger();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else Game.Log("Objectives: Unable to get node at " + trig.position);
                    }
                }
                else if (trig.triggerType == ObjectiveTriggerType.destroyItem)
                {
                    if (trig.interactable != null)
                    {
                        if(trig.interactable.rem || (trig.interactable.rPl && trig.interactable.inInventory == null))
                        {
                            trig.Trigger();
                        }
                    }
                }
            }

            //Shouldn't need this but there seems to be a rare case where triggered triggers aren't causing completion...
            if(progress >= 1f && !isComplete)
            {
                Game.Log("Objective: Completing objective...");
                Complete();
            }
        }
    }

    public void SetProgress(float newProgress)
    {
        if (newProgress != progress)
        {
            progress = newProgress;

            if (OnProgressChange != null)
            {
                OnProgressChange();
            }
        }
    }
}
