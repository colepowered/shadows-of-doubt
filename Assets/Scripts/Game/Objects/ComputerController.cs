﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ComputerController : MonoBehaviour
{
    [Header("Components")]
    public MeshRenderer computerRenderer;
    public MeshRenderer screenRenderer;
    public GameObject screenLightContainer;
    public InteractableController ic;
    public Material lightOffMaterial;
    public Material lightOnMaterial;
    public Canvas osCanvas;
    public GraphicRaycaster raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;
    public RectTransform cursorRect;
    public Image cursorImage;
    public Transform printerParent;
    public LightController screenLight;

    [Header("Operating System")]
    public CruncherAppPreset currentApp; //The current app running;
    public bool useCursor = false; //If true then the cursor will be displayed and updated
    public bool playerControlled = false; //True if a player is controlling this
    public bool appLoaded = false; //True once current app has loaded
    public float appLoadProgress = 0f; //Used for loading in an app
    public float appTimerProgress = 0f; //Used for tracking status of an app, eg boot sequence
    public float timedLoading = 0f; //Used to trigger loading anyway
    public float timedLoadingDemand = 0.33f;
    public List<GameObject> spawnedContent = new List<GameObject>(); //Spawned content for this app
    public Human loggedInAs; //The currently logged in person
    public float printTimer = 0f;
    public Vector3 printOutStartPos;
    public Vector3 printOutEndPos;
    AudioController.LoopingSoundInfo loadLoop;
    [System.NonSerialized]
    public Interactable printedDocument;

    [Space(7)]
    public ComputerOSUIComponent currentHover;

    public void Setup(InteractableController newController)
    {
        ic = newController;

        //Listen for switch change
        ic.interactable.OnSwitchChange += OnSwitchStateChange;
        ic.interactable.OnState1Change += OnPlayerControlChange;

        //Set to turned on/logged in etc...
        if (ic.interactable.locked)
        {
            SetLoggedIn(null);
        }
        else
        {
            Human loggedIn = null;
            CityData.Instance.GetHuman((int)ic.interactable.val, out loggedIn);
            SetLoggedIn(loggedIn);
        }

        //Disable on start
        screenLightContainer.gameObject.SetActive(false);
        OnSwitchStateChange();
        OnPlayerControlChange();

        m_EventSystem = GetComponent<EventSystem>();
    }

    private void OnDestroy()
    {
        if(ic != null && ic.interactable != null)
        {
            ic.interactable.OnSwitchChange -= OnSwitchStateChange;
            ic.interactable.OnState1Change -= OnPlayerControlChange;
        }

        //Key off on sound
        if (loadLoop != null)
        {
            AudioController.Instance.StopSound(loadLoop, AudioController.StopType.triggerCue, "Computer destroy");
        }
    }

    public void OnSwitchStateChange()
    {
        if(ic != null && ic.interactable != null)
        {
            if (ic.interactable.sw0)
            {
                if (loggedInAs != null)
                {
                    SetComputerApp(ic.interactable.preset.desktopApp, true); //Use desktop app
                }
                else
                {
                    SetComputerApp(ic.interactable.preset.bootApp, true); //Use startup sequence
                }

                if(screenLightContainer != null) screenLightContainer.gameObject.SetActive(true);
                if(this != null) this.enabled = true;

                //Start on sound
                if(loadLoop != null) AudioController.Instance.StopSound(loadLoop, AudioController.StopType.triggerCue, "Stop existing sound");
                loadLoop = AudioController.Instance.PlayWorldLooping(AudioControls.Instance.computerHDDLoading, null, ic.interactable, pauseWhenGamePaused: true);
            }
            else if(!ic.interactable.sw0)
            {
                //Key off on sound
                if(loadLoop != null) AudioController.Instance.StopSound(loadLoop, AudioController.StopType.fade, "Computer load key off");

                if (screenLightContainer != null) screenLightContainer.gameObject.SetActive(false);
                if(computerRenderer != null) computerRenderer.sharedMaterial = lightOffMaterial; //Make sure light is off
                SetComputerApp(ic.interactable.preset.bootApp, true); //Reset to boot sequence when off

                while(spawnedContent.Count > 0)
                {
                    Destroy(spawnedContent[0]);
                    spawnedContent.RemoveAt(0);
                }

                SetLoggedIn(null); //Log out

                if(this != null) this.enabled = false;
            }
        }

    }

    public void OnPlayerControlChange()
    {
        if(ic != null && ic.interactable != null && ic.interactable.sw1)
        {
            playerControlled = true;

            //Instantiate a raycaster
            if(raycaster == null && osCanvas != null)
            {
                osCanvas.enabled = true;
                osCanvas.worldCamera = Camera.main;
                raycaster = osCanvas.transform.gameObject.AddComponent<GraphicRaycaster>();
                raycaster.blockingObjects = GraphicRaycaster.BlockingObjects.ThreeD;
            }
        }
        else
        {
            playerControlled = false;

            //Remove raycaster
            if(raycaster != null)
            {
                Destroy(raycaster);
                if(osCanvas != null) osCanvas.enabled = false;
            }

            InterfaceController.Instance.SetCrosshairVisible(true);
        }

        //Enable/disable cursor
        if(currentApp != null) EnableCursor(currentApp.useCursor);
    }

    public void SetComputerApp(CruncherAppPreset newApp, bool forceUpdate = false)
    {
        if(currentApp != newApp || forceUpdate)
        {
            currentApp = newApp;

            if(computerRenderer != null) computerRenderer.sharedMaterial = lightOffMaterial; //Make sure light is off

            //Destroy existing content
            while (spawnedContent.Count > 0)
            {
                Destroy(spawnedContent[0]);
                spawnedContent.RemoveAt(0);
            }

            //Reset timer state
            appLoadProgress = 0f;
            appTimerProgress = 0f;
            appLoaded = false;

            //Change materials
            if(currentApp != null)
            {
                if(screenRenderer != null) screenRenderer.sharedMaterial = currentApp.loadBackground;

                //Enable/disable cursor
                EnableCursor(currentApp.useCursor);

                //Set light colour
                if(screenLight != null) screenLight.SetColour(currentApp.screenLightColourOnLoad);

                //Play on load sound
                if (currentApp.onStartSound != null)
                {
                    if (playerControlled)
                    {
                        AudioController.Instance.PlayWorldOneShot(currentApp.onStartSound, Player.Instance, ic.interactable.node, ic.interactable.wPos);
                    }
                    else
                    {
                        Human user = null;
                        ic.interactable.usagePoint.TryGetUserAtSlot(Interactable.UsePointSlot.defaultSlot, out user);
                        AudioController.Instance.PlayWorldOneShot(currentApp.onStartSound, user, ic.interactable.node, ic.interactable.wPos);
                    }
                }
            }
        }
    }

    public void SetLoggedIn(Human newLogIn)
    {
        loggedInAs = newLogIn;
        //Game.Log("Set logged in as " + loggedInAs);

        //Remember this using locked state
        if(loggedInAs != null)
        {
            ic.interactable.val = loggedInAs.humanID;
            ic.interactable.SetLockedState(false, loggedInAs);
        }
        else
        {
            ic.interactable.val = 0;
            ic.interactable.SetLockedState(true, null);
        }
    }

    //Triggered when an app has finished loading
    public void OnAppLoaded()
    {
        computerRenderer.sharedMaterial = lightOffMaterial; //Make sure light is off

        //Change materials
        screenRenderer.sharedMaterial = currentApp.loadedBackground;

        //Set light colour
        if(screenLight != null) screenLight.SetColour(currentApp.screenLightColourOnFinishLoad);

        //Spawn content
        foreach (GameObject go in currentApp.appContent)
        {
            GameObject newContent = Instantiate(go, osCanvas.transform);
            CruncherAppContent content = newContent.GetComponent<CruncherAppContent>();
            if(content != null) content.Setup(this);
            newContent.transform.SetAsFirstSibling();
            spawnedContent.Add(newContent);
        }

        //Play on load sound
        if(currentApp.onFinishedLoadingSound != null)
        {
            if(playerControlled)
            {
                AudioController.Instance.PlayWorldOneShot(currentApp.onFinishedLoadingSound, Player.Instance, ic.interactable.node, ic.interactable.wPos);
            }
            else
            {
                Human user = null;
                ic.interactable.usagePoint.TryGetUserAtSlot(Interactable.UsePointSlot.defaultSlot, out user);
                AudioController.Instance.PlayWorldOneShot(currentApp.onFinishedLoadingSound, user, ic.interactable.node, ic.interactable.wPos);
            }
        }

        appLoaded = true;

        EnableCursor(currentApp.useCursor);
    }

    //Triggered when an app ends
    public void OnAppExit()
    {
        computerRenderer.sharedMaterial = lightOffMaterial; //Make sure light is off

        //Destroy existing content
        while (spawnedContent.Count > 0)
        {
            Destroy(spawnedContent[0]);
            spawnedContent.RemoveAt(0);
        }

        //Play on load sound
        if (currentApp.onExitSound != null)
        {
            if (playerControlled)
            {
                AudioController.Instance.PlayWorldOneShot(currentApp.onExitSound, Player.Instance, ic.interactable.node, ic.interactable.wPos);
            }
            else
            {
                Human user = null;
                ic.interactable.usagePoint.TryGetUserAtSlot(Interactable.UsePointSlot.defaultSlot, out user);
                AudioController.Instance.PlayWorldOneShot(currentApp.onExitSound, user, ic.interactable.node, ic.interactable.wPos);
            }
        }

        //If this is the boot sequence then run the desktop
        if (ic.interactable.preset.bootApp == currentApp)
        {
            //If logged in this will be because of AI: Load the desktop...
            if(loggedInAs != null)
            {
                SetComputerApp(ic.interactable.preset.desktopApp);
            }
            else
            {
                SetComputerApp(ic.interactable.preset.logInApp);
            }
        }
        //Otherwise run this
        else if(currentApp.openOnEnd != null)
        {
            SetComputerApp(currentApp.openOnEnd);
        }
    }

    public void EnableCursor(bool val)
    {
        if (!playerControlled) val = false; //If no player control then never use cursor

        useCursor = val;

        if(useCursor)
        {
            if(cursorRect == null)
            {
                GameObject newC = Instantiate(GameplayControls.Instance.OScursor, osCanvas.transform);
                cursorRect = newC.GetComponent<RectTransform>();
                cursorImage = newC.GetComponent<Image>();
            }

            if (appLoaded)
            {
                cursorImage.sprite = currentApp.cursorSprite;
            }
            else
            {
                cursorImage.sprite = GameplayControls.Instance.loadCursor;
            }
        }
        else
        {
            if(cursorRect != null)
            {
                Destroy(cursorRect.gameObject);
            }
        }
    }

    private void Update()
    {
        if(SessionData.Instance.play)
        {
            if (ic.interactable == null) return;
            if (!ic.interactable.sw0) return; //Don't if off
            if (currentApp == null) return;

            if (appLoadProgress < 1f)
            {
                if (currentApp.loadTime <= 0f)
                {
                    appLoadProgress = 1f;
                    OnAppLoaded();
                }
                else
                {
                    //Flicker machine loading (33%)
                    if (Toolbox.Instance.Rand(0f, 1f) <= currentApp.loadDemand)
                    {
                        computerRenderer.sharedMaterial = lightOnMaterial;

                        //Pass loading param
                        if (loadLoop != null) loadLoop.audioEvent.setParameterByName("load", 1f);
                    }
                    else
                    {
                        computerRenderer.sharedMaterial = lightOffMaterial;
                        if (loadLoop != null) loadLoop.audioEvent.setParameterByName("load", 0f);
                    }

                    appLoadProgress += Time.deltaTime / currentApp.loadTime;
                    appLoadProgress = Mathf.Clamp01(appLoadProgress);

                    if (appLoadProgress >= 1f)
                    {
                        OnAppLoaded();
                    }
                }
            }
            else
            {
                //Always load
                if (currentApp.alwaysLoad)
                {
                    //Flicker machine loading (33%)
                    if (Toolbox.Instance.Rand(0f, 1f) <= currentApp.alwaysLoadDemand)
                    {
                        computerRenderer.sharedMaterial = lightOnMaterial;

                        //Pass loading param
                        if (loadLoop != null) loadLoop.audioEvent.setParameterByName("load", 1f);
                    }
                    else
                    {
                        computerRenderer.sharedMaterial = lightOffMaterial;
                        if (loadLoop != null) loadLoop.audioEvent.setParameterByName("load", 0f);
                    }
                }
                else if (timedLoading > 0f)
                {
                    timedLoading -= Time.deltaTime;

                    //Flicker machine loading (33%)
                    if (Toolbox.Instance.Rand(0f, 1f) <= timedLoadingDemand)
                    {
                        computerRenderer.sharedMaterial = lightOnMaterial;

                        //Pass loading param
                        if (loadLoop != null) loadLoop.audioEvent.setParameterByName("load", 1f);
                    }
                    else
                    {
                        computerRenderer.sharedMaterial = lightOffMaterial;
                        if (loadLoop != null) loadLoop.audioEvent.setParameterByName("load", 0f);
                    }
                }

                if (currentApp.useTimer)
                {
                    appTimerProgress += Time.deltaTime / currentApp.timerLength;
                    appTimerProgress = Mathf.Clamp01(appTimerProgress);

                    if (appTimerProgress >= 1)
                    {
                        OnAppExit();
                    }
                }
            }

            //Check for click
            if (playerControlled)
            {
                if(InputController.Instance.mouseInputMode)
                {
                    if (Input.GetMouseButtonUp(0))
                    {
                        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerCursorClick, Player.Instance, ic.interactable.node, ic.interactable.wPos);
                    }
                }
                else
                {
                    if (InputController.Instance.player.GetButtonUp("Primary"))
                    {
                        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerCursorClick, Player.Instance, ic.interactable.node, ic.interactable.wPos);
                    }
                }
            }

            //Print
            if (printTimer > 0f)
            {
                printTimer -= Time.deltaTime * 0.5f;
                printTimer = Mathf.Clamp01(printTimer);
                printerParent.localPosition = Vector3.Lerp(printOutEndPos, printOutStartPos, printTimer);

                if(printedDocument != null)
                {
                    printedDocument.MoveInteractable(printerParent.transform.position, ic.transform.eulerAngles, true);
                }
            }

            if (useCursor)
            {
                UpdateCursor();
            }
        }
    }

    //Set loading for a certain time
    public void SetTimedLoading(float forSeconds, float loadDemand = 0.33f)
    {
        timedLoading = forSeconds;
        timedLoadingDemand = loadDemand;
    }

    //Draw the screen
    private void UpdateCursor()
    {
        if (!useCursor) return;

        //Detect cursor position and write it
        if(cursorRect != null)
        {
            Ray camRay = new Ray(CameraController.Instance.cam.transform.position, CameraController.Instance.cam.transform.forward);

            int layerMask = Toolbox.Instance.CreateLayerMask(Toolbox.LayerMaskMode.onlyCast, 26); //Only detect layer 26 (computer screen)

            RaycastHit hit;

            //Player is looking at the screen
            if (Physics.Raycast(camRay, out hit, 2f, layerMask, QueryTriggerInteraction.Collide))
            {
                //Get the texture coordinates
                Vector2 pixelUV = hit.textureCoord;
                pixelUV.x *= 256f;
                pixelUV.y *= 256f;

                pixelUV.y -= 6;

                cursorRect.anchoredPosition = TexToCanvas(pixelUV);

                //Hide cursor
                if (InterfaceController.Instance.crosshairVisible)
                {
                    InterfaceController.Instance.SetCrosshairVisible(false);
                }
            }
            else
            {
                //Show cursor
                if(!InterfaceController.Instance.crosshairVisible)
                {
                    InterfaceController.Instance.SetCrosshairVisible(true);
                }
            }

            //Only allow input once loaded
            if (appLoaded && !GameplayController.Instance.activeGadgets.Exists(item => item.preset.specialCaseFlag == InteractablePreset.SpecialCase.codebreaker && item.objectRef == ic.interactable))
            {
                currentHover = null;

                //Check if the left Mouse button is clicked
                //Set up the new Pointer Event
                m_PointerEventData = new PointerEventData(m_EventSystem);

                //Set the Pointer Event Position to that of the mouse position
                m_PointerEventData.position = Input.mousePosition;

                //Create a list of Raycast Results
                List<RaycastResult> results = new List<RaycastResult>();

                //Raycast using the Graphics Raycaster and mouse click position
                raycaster.Raycast(m_PointerEventData, results);

                //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
                foreach (RaycastResult result in results)
                {
                    ComputerOSUIComponent uiComp = result.gameObject.GetComponent<ComputerOSUIComponent>();

                    if(uiComp != null)
                    {
                        currentHover = uiComp;

                        //Check for click
                        if(InputController.Instance.mouseInputMode)
                        {
                            if (Input.GetMouseButtonUp(0))
                            {
                                currentHover.OnLeftClick();
                                OnClickOnOSElement(currentHover);
                            }
                        }
                        else
                        {
                            if (InputController.Instance.player.GetButtonDown("Primary"))
                            {
                                currentHover.OnLeftClick();
                                OnClickOnOSElement(currentHover);
                            }
                        }

                        break;
                    }
                }
            }
        }
    }

    //Convert from texture coordinate to canvas coordinate
    public Vector2 TexToCanvas(Vector2 texCoord)
    {
        //Clamp to screen res
        texCoord.x = Mathf.Clamp(Mathf.RoundToInt(texCoord.x), 0, 255);
        texCoord.y = Mathf.Clamp(Mathf.RoundToInt(texCoord.y), 50, 255);

        texCoord.x = texCoord.x / 255f - 0.5f;
        texCoord.y = texCoord.y / 255f - 0.5f;

        return texCoord;
    }

    //When the player clicks on an OS button
    public void OnClickOnOSElement(ComputerOSUIComponent c)
    {
        if(c.sfx != null)
        {
            if (playerControlled)
            {
                AudioController.Instance.PlayWorldOneShot(c.sfx, Player.Instance, ic.interactable.node, ic.interactable.wPos);
            }
            else
            {
                Human user = null;
                ic.interactable.usagePoint.TryGetUserAtSlot(Interactable.UsePointSlot.defaultSlot, out user);
                AudioController.Instance.PlayWorldOneShot(c.sfx, user, ic.interactable.node, ic.interactable.wPos);
            }
        }
    }
}
