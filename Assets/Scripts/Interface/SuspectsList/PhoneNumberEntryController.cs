using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class PhoneNumberEntryController : MonoBehaviour
{
    public RectTransform rect;
    public Telephone telephone; //This field may not be populated if it's a 'fake' number
    public GameplayController.PhoneNumber number;
    public TextMeshProUGUI text;
    public ButtonController openLocationButton;
    public ButtonController openEvidenceButton;
    public ButtonController enterCodeButton;
    public Image icon;
    public string nameString;
    public string passcodeString;

    private List<Human> citizenSubscriptions = new List<Human>();

    public void Setup(GameplayController.PhoneNumber newNumber)
    {
        number = newNumber;

        openLocationButton.SetupReferences();
        openEvidenceButton.SetupReferences();
        enterCodeButton.SetupReferences();

        openLocationButton.VisualUpdate();
        openEvidenceButton.VisualUpdate();
        enterCodeButton.VisualUpdate();

        CityData.Instance.phoneDictionary.TryGetValue(number.number, out telephone);

        GameplayController.Instance.OnNewPhoneData += VisualUpdate;
        VisualUpdate();

        //Check if 'enter code' should be active
        InterfaceController.Instance.OnNewActiveCodeInput += ActiveCodeInputCheck;
        ActiveCodeInputCheck(InterfaceController.Instance.activeCodeInput);
    }

    public void VisualUpdate()
    {
        //Parse number
        text.text = Toolbox.Instance.GetTelephoneNumberString(number.number) + "\n";
        string comma = string.Empty;

        if(number.textOverride != null && number.textOverride.Length > 0)
        {
            text.text += Strings.Get("evidence.generic", number.textOverride);
        }

        //Feature location
        if(number.loc && telephone != null)
        {
            text.text += telephone.location.name;

            if(telephone.interactable != null && telephone.interactable.preset.isPayphone)
            {
                text.text += " " + Strings.Get("evidence.generic", "payphone");
            }

            openLocationButton.SetInteractable(true);
            comma = ": ";
        }
        else openLocationButton.SetInteractable(false);

        if(telephone != null && telephone.telephoneEntry != null)
        {
            openLocationButton.SetInteractable(true);
        }
        else openLocationButton.SetInteractable(false);

        //Make sure we are subscribed to owners for name changes etc...
        foreach (int i in number.p)
        {
            Human h = null;

            if(CityData.Instance.GetHuman(i, out h))
            {
                if(!citizenSubscriptions.Contains(h))
                {
                    citizenSubscriptions.Add(h);
                    h.evidenceEntry.OnNewName += VisualUpdate;
                    h.evidenceEntry.OnDataKeyChange += VisualUpdate;
                }

                text.text += comma + h.GetCitizenName();
            }
        }
    }

    public void ActiveCodeInputCheck(KeypadController keypad)
    {
        //Check if 'enter code' should be active
        if(InterfaceController.Instance.activeCodeInput != null && InterfaceController.Instance.activeCodeInput.isTelephone)
        {
            enterCodeButton.SetInteractable(true);
        }
        else enterCodeButton.SetInteractable(false);
    }

    private void OnDestroy()
    {
        foreach(Human h in citizenSubscriptions)
        {
            h.evidenceEntry.OnNewName -= VisualUpdate;
            h.evidenceEntry.OnDataKeyChange -= VisualUpdate;
        }

        GameplayController.Instance.OnNewPhoneData -= VisualUpdate;
        InterfaceController.Instance.OnNewActiveCodeInput -= ActiveCodeInputCheck;
    }

    public void OpenLocation()
    {
        if(telephone != null && telephone.locationEntry != null) InterfaceController.Instance.SpawnWindow(telephone.locationEntry, Evidence.DataKey.name);
    }

    public void OpenEvidence()
    {
        if(telephone != null && telephone.telephoneEntry != null) InterfaceController.Instance.SpawnWindow(telephone.telephoneEntry, Evidence.DataKey.name);
    }

    public void EnterCode()
    {
        if (InterfaceController.Instance.activeCodeInput != null && InterfaceController.Instance.activeCodeInput.isTelephone)
        {
            //Set input tab active
            InterfaceController.Instance.activeCodeInput.parentWindow.SetActiveContent(InterfaceController.Instance.activeCodeInput.windowContent);

            //Input code
            List<int> digits = new List<int>();
            string numStr = number.number.ToString();

            for (int i = 0; i < numStr.Length; i++)
            {
                int d = 0;

                if(int.TryParse(numStr[i].ToString(), out d))
                {
                    digits.Add(d);
                }
            }

            InterfaceController.Instance.activeCodeInput.OnInputCode(digits);

            enterCodeButton.OnDeselect();
        }
    }
}
