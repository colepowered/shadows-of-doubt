using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using TMPro;
using System.Linq;

public class BioScreenController : MonoBehaviour
{
    [Header("Components")]
    public Canvas canvasParent;
    public RectTransform socialCreditDisplayParent;
    public RectTransform levelBarBlipParent;
    public RectTransform socialLevelBarRect;
    public RectTransform barFill;
    public JuiceController barJuice;
    public List<CanvasRenderer> socialCreditRenderers = new List<CanvasRenderer>();
    [Space(7)]
    public RectTransform inventoryParentRect;
    public RectTransform solidBG;
    public ButtonController closeButton;
    public RectTransform equipmentParentRect;
    public RectTransform itemsParentRect;
    public TextMeshProUGUI inventoryTitleText;
    public TextMeshProUGUI cashText;
    public RectTransform summaryTextRect;
    public TextMeshProUGUI summaryText;
    public RectTransform buttonAreaParent;
    public ButtonController dropButton;
    public ButtonController inspectButton;
    public ButtonController equipButton;
    public ButtonController scanButton;
    public ButtonController moreOptionsButton;
    public ButtonController editDecorButton;
    public RectTransform scanProgressBar;
    public ButtonController selectNothingButton;
    public InventorySquareController nothingSquare;
    public List<CanvasRenderer> inventoryRenderers = new List<CanvasRenderer>();

    [Header("Settings")]
    public GameObject levelBlipPrefab;
    public Color clearedLevel = Color.white;
    public Color futureLevel = Color.grey;
    [Space(7)]
    public GameObject inventorySquarePrefab;
    public Sprite equipmentBGIcon;
    public Sprite itemBGIcon;

    [Header("State")]
    public int maxLevels = 10;
    public int maxPoints = 0;
    public float desiredBarFillLevel = 0f;
    public float barHeight = -1;
    public int currentLevel = 0;
    private List<ButtonController> levelBlips = new List<ButtonController>();
    public float socialCreditBarDisplayTimer = 0f;
    public float socialCreditDisplayProgress = 0f;
    private ButtonController currentLevelBlip;
    public bool openedFromPause = false;
    [Space(7)]
    public bool isOpen = false;
    public float inventoryDisplayProgress = 0f;
    [System.NonSerialized]
    public FirstPersonItemController.InventorySlot hoveredSlot;
    [System.NonSerialized]
    public FirstPersonItemController.InventorySlot selectedSlot;
    public int hoverIndex = 0;
    private string summaryTextToDisplay;
    private float summaryTextProgress = 0f;
    [System.NonSerialized]
    public Interactable scanningItem = null;
    public float scanProgress = 0f;
    AudioController.LoopingSoundInfo scannerLoop = null;
    public Dictionary<Interactable, List<Interactable>> scannedObjectsPrintsCache = new Dictionary<Interactable, List<Interactable>>();

    public delegate void InventoryOpenChange();
    public event InventoryOpenChange OnInventoryOpenChange;

    //Singleton pattern
    private static BioScreenController _instance;
    public static BioScreenController Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        foreach (CanvasRenderer rend in socialCreditRenderers)
        {
            if (rend == null) continue;
            rend.SetAlpha(socialCreditDisplayProgress);
        }
    }

    private void Start()
    {
        if(inventoryTitleText != null) inventoryTitleText.text = Strings.Get("ui.interface", "Inventory");
        if(socialLevelBarRect != null) barHeight = socialLevelBarRect.rect.height; //This does not return the correct value when inactive (like in the menu), so capture and store it once
    }

    //Sets the maximum levels needs for retirement
    public void SetMaxSocialCreditLevels(int newMax)
    {
        maxLevels = newMax;

        //Remove existing level blips
        for (int i = 0; i < levelBlips.Count; i++)
        {
            Destroy(levelBlips[i].gameObject);
        }

        //Remove renderers
        for (int i = 0; i < socialCreditRenderers.Count; i++)
        {
            if (socialCreditRenderers[i] == null)
            {
                socialCreditRenderers.RemoveAt(i);
                i--;
            }
        }

        levelBlips.Clear();

        maxPoints = GameplayController.Instance.GetSocialCreditThresholdForLevel(maxLevels); //The bar represents this many points
        Game.Log("Gameplay: Set new max social credit levels: " + maxLevels + ", needing a total of " + maxPoints + " points");

        for (int i = 2; i <= maxLevels; i++)
        {
            GameObject newBlip = Instantiate(levelBlipPrefab, levelBarBlipParent);
            ButtonController bc = newBlip.GetComponent<ButtonController>();
            levelBlips.Add(bc);

            //Position blip
            int pointsForLevel = GameplayController.Instance.GetSocialCreditThresholdForLevel(i);
            bc.rect.anchoredPosition = new Vector2(-26, (barHeight / maxPoints) * pointsForLevel - 4);

            newBlip.name = i + ": " + pointsForLevel.ToString() + " (" + ((barHeight / maxPoints) * pointsForLevel) + ")";

            //Setup text, tooltip
            bc.text.text = i.ToString();
            bc.tooltip.mainText = Strings.Get("ui.tooltips", "Social Credit Level") + " " + i.ToString();
        }

        foreach (ButtonController bc in levelBlips)
        {
            bc.juice.Pulsate(false, false);
            bc.notifications.gameObject.SetActive(false);
            bc.SetButtonBaseColour(futureLevel);

            CanvasRenderer[] rends = bc.gameObject.GetComponentsInChildren<CanvasRenderer>(true);
            socialCreditRenderers.AddRange(rends);
        }

        OnChangePoints(false);
    }

    //Opens or closes the bio/inventory display
    public void SetInventoryOpen(bool val, bool forceUpdate, bool resumeGame = true)
    {
        if (CutSceneController.Instance.cutSceneActive) val = false; //Close during cut scene
        if (Player.Instance.playerKOInProgress) val = false; //Not allowed during KO

        if(isOpen != val || forceUpdate)
        {
            isOpen = val;
            openedFromPause = !SessionData.Instance.play;
            Game.Log("Player: Inventory screen active: " + isOpen);
            InterfaceController.Instance.personButton.icon.enabled = BioScreenController.Instance.isOpen;

            InterfaceController.Instance.caseCanvasRaycaster.enabled = !isOpen; //Disabled/enable control of the case board
            InterfaceController.Instance.windowCanvas.enabled = !isOpen;

            if (isOpen)
            {
                if(SessionData.Instance.play)
                {
                    SessionData.Instance.PauseGame(true, openDesktopMode: false);
                }

                SessionData.Instance.TutorialTrigger("inventory");

                //Close the upgrades screen
                if (UpgradesController.Instance.isOpen)
                {
                    UpgradesController.Instance.CloseUpgrades();
                }

                //Check nothing selected?
                UpdateButtons();
                if (selectedSlot == null || (selectedSlot.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic && selectedSlot.GetInteractable() == null)) SelectSlot(FirstPersonItemController.Instance.slots.Find(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.holster));
                dropButton.OnDeselect();
                inspectButton.OnDeselect();
                equipButton.OnDeselect();
                scanButton.OnDeselect();
                editDecorButton.OnDeselect();

                if (cashText != null) cashText.text = InterfaceControls.Instance.cashText.text;

                //Set mouse look
                Player.Instance.EnablePlayerMouseLook(false);

                if (!InputController.Instance.mouseInputMode)
                {
                    CasePanelController.Instance.SetControllerMode(true, CasePanelController.ControllerSelectMode.topBar);
                }
            }
            else
            {
                if (resumeGame && !InterfaceController.Instance.desktopMode && !PopupMessageController.Instance.active && !MainMenuController.Instance.mainMenuActive)
                {
                    SessionData.Instance.ResumeGame();
                    Player.Instance.EnablePlayerMouseLook(true, true);
                }

                openedFromPause = false;

                HoverSlot(null);

                //Disable controller unless the case board is open
                if(!InterfaceController.Instance.desktopMode)
                {
                    CasePanelController.Instance.SetControllerMode(false, CasePanelController.Instance.currentSelectMode);
                }
            }

            //Update interaction text to display proper controls
            InteractionController.Instance.UpdateInteractionText();

            UpdateSummary();

            //Fire event
            if(OnInventoryOpenChange != null)
            {
                OnInventoryOpenChange();
            }
        }
    }

    public void HoverSlot(FirstPersonItemController.InventorySlot newSlot)
    {
        bool change = false;

        if (hoveredSlot != newSlot)
        {
            Game.Log("Hover slot: " + newSlot);
            change = true;
        }

        hoveredSlot = newSlot;
        if (hoveredSlot != null) hoverIndex = FirstPersonItemController.Instance.slots.IndexOf(hoveredSlot);

        if (hoveredSlot != null)
        {
            foreach (FirstPersonItemController.InventorySlot slot in FirstPersonItemController.Instance.slots)
            {
                if (slot == hoveredSlot)
                {
                    slot.spawnedSegment.text.fontStyle = FontStyles.Underline;
                }
                else
                {
                    slot.spawnedSegment.text.fontStyle = FontStyles.Normal;
                }
            }
        }

        //Update interaction text to display proper controls
        InteractionController.Instance.UpdateInteractionText();

        if (change) UpdateSummary();
    }

    //Select slot
    public void SelectSlot(FirstPersonItemController.InventorySlot newSlot, bool closeInventory = false, bool forceUpdate = false)
    {
        if (newSlot == null) return;
        Game.Log("Player: Select new inventory slot: " + newSlot.index);

        if (selectedSlot != newSlot || forceUpdate)
        {
            //Cancel actions
            if (FirstPersonItemController.Instance.isConsuming)
            {
                FirstPersonItemController.Instance.SetConsuming(false);
            }

            if (FirstPersonItemController.Instance.isRaised)
            {
                FirstPersonItemController.Instance.SetRaised(false);
            }

            //Cancel pick up if not nothing
            if(InteractionController.Instance.carryingObject != null && newSlot !=null && newSlot.isStatic != FirstPersonItemController.InventorySlot.StaticSlot.holster)
            {
                InteractionController.Instance.carryingObject.DropThis(false);
            }

            //Switching item with this methods cancels any force holsters
            FirstPersonItemController.Instance.forceHolstered = false;

            selectedSlot = newSlot;

            foreach(FirstPersonItemController.InventorySlot sl in FirstPersonItemController.Instance.slots)
            {
                if(sl.spawnedSegment != null)
                {
                    if(selectedSlot == sl)
                    {
                        sl.spawnedSegment.selected.gameObject.SetActive(true);
                    }
                    else sl.spawnedSegment.selected.gameObject.SetActive(false);
                }
            }

            //Select first person item to display
            if(selectedSlot != null) FirstPersonItemController.Instance.SetFirstPersonItem(selectedSlot.GetFirstPersonItem());

            UpdateButtons();
        }

        if (closeInventory)
        {
            SetInventoryOpen(false, true);
        }
        else
        {
            //Update interaction text to display proper controls
            InteractionController.Instance.UpdateInteractionText();
        }
    }

    public void UpdateButtons()
    {
        if(selectedSlot != null)
        {
            if(selectedSlot.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic)
            {
                Interactable slotInteractable = selectedSlot.GetInteractable();

                if(slotInteractable != null)
                {
                    dropButton.SetInteractable(true);

                    if(slotInteractable.evidence != null)
                    {
                        inspectButton.SetInteractable(true);
                        scanButton.SetInteractable(true);
                    }
                    else
                    {
                        inspectButton.SetInteractable(false);
                        scanButton.SetInteractable(false);
                    }
                }
                else
                {
                    dropButton.SetInteractable(false);
                    inspectButton.SetInteractable(false);
                    scanButton.SetInteractable(false);
                }
            }
            else
            {
                dropButton.SetInteractable(false);
                inspectButton.SetInteractable(false);
                scanButton.SetInteractable(false);
            }
        }
        else
        {
            dropButton.SetInteractable(false);
            inspectButton.SetInteractable(false);
            scanButton.SetInteractable(false);
        }

        if (scanningItem != null || !FirstPersonItemController.Instance.slots.Exists(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.printReader)) scanButton.SetInteractable(false);
    }

    public void UpdateDecorEditButton()
    {
        if (SessionData.Instance.isFloorEdit) return;

        if((Player.Instance.currentGameLocation == Player.Instance.home || Player.Instance.apartmentsOwned.Contains(Player.Instance.currentGameLocation)) && (Game.Instance.sandboxMode || ChapterController.Instance.currentPart >= 30))
        {
            editDecorButton.gameObject.SetActive(true);
        }
        else editDecorButton.gameObject.SetActive(false);
    }

    public void UpdateSummary()
    {
        summaryText.text = string.Empty;
        summaryTextToDisplay = string.Empty;
        summaryTextProgress = 0f;

        if (hoveredSlot != null && isOpen)
        {
            Interactable slotInteractable = hoveredSlot.GetInteractable();

            if (slotInteractable != null)
            {
                summaryTextToDisplay = Strings.GetTextForComponent(slotInteractable.preset.summaryMessageSource, slotInteractable);
            }
            else
            {
                FirstPersonItem slotItem = hoveredSlot.GetFirstPersonItem();

                if (slotItem != null && slotItem.summaryMsgID != null && slotItem.summaryMsgID.Length > 0)
                {
                    summaryTextToDisplay = Strings.GetTextForComponent(slotItem.summaryMsgID, null);
                }
            }

            summaryText.SetText(summaryTextToDisplay);
            summaryText.SetText(string.Empty);
            //summaryTextRect.sizeDelta = new Vector2(summaryTextRect.sizeDelta.x, 44f);
        }
    }

    public InventorySquareController SpawnSlotObject(FirstPersonItemController.InventorySlot slot)
    {
        RectTransform p = equipmentParentRect;
        if (slot.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic) p = itemsParentRect;

        GameObject newObj = Instantiate(inventorySquarePrefab, p);
        InventorySquareController sq = newObj.GetComponent<InventorySquareController>();
        sq.Setup(slot);

        return sq;
    }

    public void OnChangePoints(bool allowLevelChangeDisplay)
    {
        desiredBarFillLevel = Mathf.Clamp01((float)GameplayController.Instance.socialCredit / (float)maxPoints) * barHeight;

        int getLvl = GameplayController.Instance.GetCurrentSocialCreditLevel();
        InterfaceControls.Instance.socialRankText.text = getLvl.ToString();

        //Game.Log(GameplayController.Instance.socialCredit + " / " + maxPoints + " * " + barHeight + " = " + desiredBarFillLevel + " (level: " + getLvl + ", threshold: " + GameplayController.Instance.GetSocialCreditThresholdForLevel(getLvl) + ")");

        if (currentLevel != getLvl)
        {
            if(getLvl > currentLevel && allowLevelChangeDisplay)
            {
                //Trigger level change display
                InterfaceController.Instance.SocialCreditLevelUpDisplay();
            }

            currentLevel = getLvl;
            //Game.Log("Current level: " + currentLevel);

            int blipIndex = Mathf.Clamp(currentLevel - 2, -1, levelBlips.Count - 1); //Don't spawn a blip for level 1 as the player starts on this

            foreach (ButtonController bc in levelBlips)
            {
                bc.juice.Pulsate(false, false);
                bc.notifications.gameObject.SetActive(false);

                if (levelBlips.IndexOf(bc) <= blipIndex)
                {
                    bc.SetButtonBaseColour(clearedLevel);
                }
                else bc.SetButtonBaseColour(futureLevel);
            }

            if (blipIndex >= 0)
            {
                currentLevelBlip = levelBlips[blipIndex]; //Flash the current blip
            }
            else currentLevelBlip = null;

            if (currentLevelBlip != null)
            {
                currentLevelBlip.juice.Pulsate(true, false);
                currentLevelBlip.notifications.SetNotifications(1);
                currentLevelBlip.juice.Nudge();

                CanvasRenderer[] rends = currentLevelBlip.gameObject.GetComponentsInChildren<CanvasRenderer>(true);

                foreach(CanvasRenderer r in rends)
                {
                    r.SetAlpha(socialCreditDisplayProgress);
                }
            }

            //Trigger retirement
            if (getLvl >= Game.Instance.gameLengthMaxLevels[Game.Instance.gameLength] && !CasePanelController.Instance.activeCases.Exists(item => item.caseType == Case.CaseType.retirement))
            {
                Game.Log("Gameplay: Trigger retirement...");

                Case retirementCase = CasePanelController.Instance.CreateNewCase(Case.CaseType.retirement, Case.CaseStatus.handInNotCollected, true, Strings.Get("ui.interface", "Retirement"));

                foreach (Case.ResolveQuestion rq in GameplayControls.Instance.retirementResolveQuestions)
                {
                    Case.ResolveQuestion newRq = new Case.ResolveQuestion();
                    newRq.name = rq.name;
                    newRq.inputType = rq.inputType;
                    newRq.tag = rq.tag;
                    newRq.rewardRange = rq.rewardRange;
                    newRq.isOptional = rq.isOptional;
                    newRq.displayObjective = rq.displayObjective;

                    //Pick reward
                    newRq.reward = Mathf.RoundToInt((Toolbox.Instance.VectorToRandom(rq.rewardRange) * Game.Instance.jobRewardMultiplier) / 50f) * 50;
                    newRq.penalty = Mathf.RoundToInt((Toolbox.Instance.VectorToRandom(rq.penaltyRange) * Game.Instance.jobPenaltyMultiplier) / 50f) * 50;

                    Game.Log("Adding resolve question: " + newRq.name);
                    retirementCase.resolveQuestions.Add(newRq);
                }
            }
        }
    }

    private void Update()
    {
        if (SessionData.Instance.isFloorEdit) return;

        //Handle social credit display
        if (barFill.sizeDelta.y < desiredBarFillLevel)
        {
            float diff = Mathf.Max(Mathf.Abs(barFill.sizeDelta.y - desiredBarFillLevel), 1f);
            barFill.sizeDelta =  new Vector2(barFill.sizeDelta.x, Mathf.Min(desiredBarFillLevel, barFill.sizeDelta.y + diff * Time.deltaTime));
            if(!barJuice.pulsateActive) barJuice.Pulsate(true, true);
            socialCreditBarDisplayTimer = 5f;
        }
        else
        {
            if(barFill.sizeDelta.y > desiredBarFillLevel)
            {
                float diff = Mathf.Max(Mathf.Abs(barFill.sizeDelta.y - desiredBarFillLevel), 1f);
                barFill.sizeDelta = new Vector2(barFill.sizeDelta.x, Mathf.Max(desiredBarFillLevel, barFill.sizeDelta.y - diff * Time.deltaTime));
                if (!barJuice.pulsateActive) barJuice.Pulsate(true, true);
                socialCreditBarDisplayTimer = 5f;
            }
            else if (barJuice.pulsateActive) barJuice.Pulsate(false, true);
        }

        if(socialCreditBarDisplayTimer > 0f && ((!InterfaceController.Instance.gameScreenQueued && !InterfaceController.Instance.gameSceenDisplayed) || (InterfaceController.Instance.gameSceenDisplayed && InterfaceController.Instance.currentGameScreen == InterfaceController.ScreenDisplayType.socialCreditLevelUp)))
        {
            if (!socialCreditDisplayParent.gameObject.activeSelf) socialCreditDisplayParent.gameObject.SetActive(true);

            //If displayed, run down display timer
            if (socialCreditDisplayProgress >= 1f)
            {
                socialCreditBarDisplayTimer -= Time.deltaTime;
                socialCreditBarDisplayTimer = Mathf.Max(socialCreditBarDisplayTimer, 0);
            }
            //Fade in
            else
            {
                socialCreditDisplayProgress += Time.deltaTime * 3f;
                socialCreditDisplayProgress = Mathf.Clamp01(socialCreditDisplayProgress);

                foreach(CanvasRenderer rend in socialCreditRenderers)
                {
                    if (rend == null) continue;
                    rend.SetAlpha(socialCreditDisplayProgress);
                }

                //Slide out objectives display
                InterfaceController.Instance.objectiveSideAnchor.anchoredPosition = new Vector2(Mathf.Lerp(0, 600, socialCreditDisplayProgress), 0);
            }
        }
        else
        {
            //Fade out
            if (socialCreditDisplayProgress > 0f)
            {
                socialCreditDisplayProgress -= Time.deltaTime;
                socialCreditDisplayProgress = Mathf.Max(socialCreditDisplayProgress, 0);

                foreach (CanvasRenderer rend in socialCreditRenderers)
                {
                    if (rend == null) continue;
                    rend.SetAlpha(socialCreditDisplayProgress);
                }

                //Slide out objectives display
                InterfaceController.Instance.objectiveSideAnchor.anchoredPosition = new Vector2(Mathf.Lerp(0, 600, socialCreditDisplayProgress), 0);
            }
            else if (socialCreditDisplayParent.gameObject.activeSelf)
            {
                InterfaceController.Instance.objectiveSideAnchor.anchoredPosition = Vector2.zero;
                socialCreditDisplayParent.gameObject.SetActive(false);
            }
        }

        if(currentLevelBlip != null && socialCreditDisplayProgress >= 1f)
        {
            currentLevelBlip.notifications.gameObject.SetActive(true);
        }

        //Closed in paused mode
        if(isOpen)
        {
            if (!inventoryParentRect.gameObject.activeSelf)
            {
                inventoryParentRect.gameObject.SetActive(true);
            }

            if(solidBG.gameObject.activeSelf == SessionData.Instance.play)
            {
                solidBG.gameObject.SetActive(!SessionData.Instance.play);
            }

            //if (dropButton.gameObject.activeSelf == SessionData.Instance.play) dropButton.gameObject.SetActive(!SessionData.Instance.play);
            //if (inspectButton.gameObject.activeSelf == SessionData.Instance.play) inspectButton.gameObject.SetActive(!SessionData.Instance.play);
            //if (scanButton.gameObject.activeSelf == SessionData.Instance.play) scanButton.gameObject.SetActive(!SessionData.Instance.play);
            //if (equipButton.gameObject.activeSelf == SessionData.Instance.play) equipButton.gameObject.SetActive(!SessionData.Instance.play);

            //if (moreOptionsButton.gameObject.activeSelf != SessionData.Instance.play) moreOptionsButton.gameObject.SetActive(SessionData.Instance.play);
            //if (editDecorButton.gameObject.activeSelf != SessionData.Instance.play) editDecorButton.gameObject.SetActive(SessionData.Instance.play);

            socialCreditBarDisplayTimer = Mathf.Max(1, socialCreditBarDisplayTimer); //Open social credit display

            //Fade in or snap in depending on whether game is paused
            if (SessionData.Instance.play)
            {
                //Transition in
                if (inventoryDisplayProgress < 1f)
                {
                    inventoryDisplayProgress += Time.deltaTime / 0.2f;
                    inventoryDisplayProgress = Mathf.Clamp01(inventoryDisplayProgress);

                    foreach (CanvasRenderer rend in inventoryRenderers)
                    {
                        rend.SetAlpha(inventoryDisplayProgress);
                    }

                    foreach (FirstPersonItemController.InventorySlot slot in FirstPersonItemController.Instance.slots)
                    {
                        if(slot.spawnedSegment != null)
                        {
                            foreach (CanvasRenderer rend in slot.spawnedSegment.renderers)
                            {
                                rend.SetAlpha(inventoryDisplayProgress);
                            }
                        }

                    }

                    float val = InterfaceController.Instance.radialActivateScale.Evaluate(inventoryDisplayProgress);
                    inventoryParentRect.localScale = new Vector3(val, val, val);

                    if (inventoryDisplayProgress >= 1f)
                    {
                        inventoryParentRect.localScale = Vector3.one;
                    }

                    //Move tip
                    InterfaceController.Instance.helpPointerRect.anchoredPosition = new Vector2(InterfaceController.Instance.helpPointerRect.anchoredPosition.x, Mathf.Lerp(-120f, -8, inventoryDisplayProgress));
                }
            }
            else if(inventoryDisplayProgress < 1f)
            {
                inventoryDisplayProgress = 1;

                foreach (CanvasRenderer rend in inventoryRenderers)
                {
                    rend.SetAlpha(inventoryDisplayProgress);
                }

                foreach (FirstPersonItemController.InventorySlot slot in FirstPersonItemController.Instance.slots)
                {
                    if (slot.spawnedSegment != null)
                    {
                        foreach (CanvasRenderer rend in slot.spawnedSegment.renderers)
                        {
                            rend.SetAlpha(inventoryDisplayProgress);
                        }
                    }

                }

                //Move tip
                InterfaceController.Instance.helpPointerRect.gameObject.SetActive(false); //Disable if both game is paused and inventory is open
                InterfaceController.Instance.helpPointerRect.anchoredPosition = new Vector2(InterfaceController.Instance.helpPointerRect.anchoredPosition.x, Mathf.Lerp(-120f, -8, inventoryDisplayProgress));

                inventoryParentRect.localScale = Vector3.one;
            }

            //Display summary text
            if (summaryTextToDisplay != null && summaryTextToDisplay.Length > 0 && summaryTextProgress < 1f)
            {
                summaryTextProgress += Time.deltaTime / 0.25f;
                summaryTextProgress = Mathf.Clamp01(summaryTextProgress);
                summaryText.text = summaryTextToDisplay.Substring(0, Mathf.CeilToInt(summaryTextProgress * summaryTextToDisplay.Length));
            }
        }
        else
        {
            //Fade in or snap in depending on whether game is paused
            if(SessionData.Instance.play)
            {
                //Transition out
                if (inventoryDisplayProgress > 0f)
                {
                    inventoryDisplayProgress -= Time.deltaTime / 0.25f;
                    inventoryDisplayProgress = Mathf.Clamp01(inventoryDisplayProgress);

                    foreach (CanvasRenderer rend in inventoryRenderers)
                    {
                        rend.SetAlpha(inventoryDisplayProgress);
                    }

                    foreach (FirstPersonItemController.InventorySlot slot in FirstPersonItemController.Instance.slots)
                    {
                        if (slot.spawnedSegment != null)
                        {
                            foreach (CanvasRenderer rend in slot.spawnedSegment.renderers)
                            {
                                rend.SetAlpha(inventoryDisplayProgress);
                            }
                        }

                    }

                    float val = InterfaceController.Instance.radialActivateScale.Evaluate(inventoryDisplayProgress);
                    inventoryParentRect.localScale = new Vector3(val, val, val);

                    if (inventoryDisplayProgress <= 0f)
                    {
                        inventoryParentRect.localScale = Vector3.one;
                        inventoryParentRect.gameObject.SetActive(false);
                    }

                    //Move tip
                    InterfaceController.Instance.helpPointerRect.anchoredPosition = new Vector2(InterfaceController.Instance.helpPointerRect.anchoredPosition.x, Mathf.Lerp(-120f, -8, inventoryDisplayProgress));
                }
            }
            else if(inventoryDisplayProgress != 0)
            {
                inventoryDisplayProgress = 0;

                foreach (CanvasRenderer rend in inventoryRenderers)
                {
                    rend.SetAlpha(inventoryDisplayProgress);
                }

                foreach (FirstPersonItemController.InventorySlot slot in FirstPersonItemController.Instance.slots)
                {
                    if (slot.spawnedSegment != null)
                    {
                        foreach (CanvasRenderer rend in slot.spawnedSegment.renderers)
                        {
                            rend.SetAlpha(inventoryDisplayProgress);
                        }
                    }

                }

                inventoryParentRect.localScale = Vector3.one;
                inventoryParentRect.gameObject.SetActive(false);

                //Move tip
                InterfaceController.Instance.helpPointerRect.anchoredPosition = new Vector2(InterfaceController.Instance.helpPointerRect.anchoredPosition.x, Mathf.Lerp(-120f, -8, inventoryDisplayProgress));
            }
        }

        //Scan
        if(scanningItem != null)
        {
            if(scanProgress < 1f)
            {
                scanProgress += Time.deltaTime * 0.5f;
                scanProgressBar.sizeDelta = new Vector2((scanButton.rect.sizeDelta.x - 8f) * scanProgress, scanProgressBar.sizeDelta.y);
            }
            else
            {
                OnScanComplete(scanningItem);
                scanningItem = null;
                scanProgress = 0;
                scanProgressBar.sizeDelta = new Vector2(0, scanProgressBar.sizeDelta.y);
            }
        }
    }

    [Button]
    public void AddSocialCredit()
    {
        GameplayController.Instance.AddSocialCredit(50, true, "Debug button");
    }

    public void DecorEditButton()
    {
        if(Player.Instance.currentGameLocation == Player.Instance.home || Player.Instance.apartmentsOwned.Contains(Player.Instance.currentGameLocation))
        {
            if(!SessionData.Instance.isDecorEdit)
            {
                InteractionController.Instance.StartDecorEdit();
            }
        }
    }

    public void DropButton()
    {
        if (selectedSlot != null && selectedSlot.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic && selectedSlot.GetInteractable() != null)
        {
            FirstPersonItemController.Instance.EmptySlot(selectedSlot);
        }
    }

    public void InspectButton()
    {
        if (selectedSlot != null && selectedSlot.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic && selectedSlot.GetInteractable() != null)
        {
            Interactable slotInter = selectedSlot.GetInteractable();

            if(slotInter != null && slotInter.evidence != null)
            {
                SetInventoryOpen(false, true, false); //Close
                InterfaceController.Instance.SetDesktopMode(true, true);
                //SessionData.Instance.PauseGame(true);
                InterfaceController.Instance.SpawnWindow(slotInter.evidence, Evidence.DataKey.name, passedInteractable: slotInter);
            }
        }
    }

    public void ScanButton()
    {
        if(scanningItem == null)
        {
            scanningItem = selectedSlot.GetInteractable();
            scanProgress = 0;
            //if(scannerLoop != null) AudioController.Instance.StopSound(scannerLoop, AudioController.StopType.immediate, "");
            //scannerLoop = AudioController.Instance.PlayWorldLooping(AudioControls.Instance.printScannerLoop, Player.Instance, scanningItem);

            AudioController.Instance.Play2DSound(AudioControls.Instance.printScannerSelect);

            UpdateButtons();
        }
    }

    public void OnScanComplete(Interactable scanCompleteOn)
    {
        AudioController.Instance.StopSound(scannerLoop, AudioController.StopType.fade, "");
        AudioController.Instance.Play2DSound(AudioControls.Instance.printScannerHolster);

        if (scanCompleteOn == null) return;

        if(scannedObjectsPrintsCache.ContainsKey(scanCompleteOn))
        {
            if(scannedObjectsPrintsCache[scanCompleteOn].Count > 0)
            {
                SetInventoryOpen(false, true, false); //Close
                InterfaceController.Instance.SetDesktopMode(true, true);
                //SessionData.Instance.PauseGame(true, true, openDesktopMode: true);

                foreach (Interactable i in scannedObjectsPrintsCache[scanCompleteOn])
                {
                    InterfaceController.Instance.SpawnWindow(i.evidence, Evidence.DataKey.fingerprints, passedInteractable: i);
                }
            }
        }
        else
        {
            //Reveal fingerprints as windows...
            if(scanCompleteOn.preset.fingerprintsEnabled)
            {
                List<Human> owners = new List<Human>(Toolbox.Instance.GetFingerprintOwnerPool(null, null, scanCompleteOn, scanCompleteOn.preset.printsSource, Vector3.zero, false)); //List of owners to assign to picked print locations...

                if (scanCompleteOn.preset.enableDynamicFingerprints && scanCompleteOn.df.Count > 0)
                {
                    //Gather list of owners to assign
                    foreach (Interactable.DynamicFingerprint f in scanCompleteOn.df)
                    {
                        Human found = null;

                        if (CityData.Instance.GetHuman(f.id, out found))
                        {
                            if(!owners.Contains(found)) owners.Add(found);
                        }
                        else
                        {
                            Game.LogError("Cannot find citizen " + f.id);
                        }
                    }
                }

                for (int i = 0; i < owners.Count; i++)
                {
                    FingerprintScannerController.Print newPrint = new FingerprintScannerController.Print();
                    newPrint.interactable = scanCompleteOn;
                    newPrint.type = FingerprintScannerController.Print.PrintType.fingerPrint;
                    newPrint.dynamicOwner = owners[i];

                    Game.Log("Player: Discovered print belonging to " + owners[i].GetCitizenName());

                    //Get prints letter loop
                    if (owners[i].fingerprintLoop <= -1)
                    {
                        owners[i].fingerprintLoop = GameplayController.Instance.printsLetterLoop;
                        GameplayController.Instance.printsLetterLoop++;
                    }

                    Interactable printInteractable = InteractableCreator.Instance.CreateFingerprintInteractable(owners[i], Vector3.zero, Vector3.zero, newPrint);

                    if(!scannedObjectsPrintsCache.ContainsKey(scanCompleteOn))
                    {
                        scannedObjectsPrintsCache.Add(scanCompleteOn, new List<Interactable>());
                    }

                    scannedObjectsPrintsCache[scanCompleteOn].Add(printInteractable);

                    SetInventoryOpen(false, true, false); //Close
                    //SessionData.Instance.PauseGame(true, true, openDesktopMode: true);
                    InterfaceController.Instance.SetDesktopMode(true, true);
                    InterfaceController.Instance.SpawnWindow(printInteractable.evidence, Evidence.DataKey.fingerprints, passedInteractable: printInteractable);
                }
            }
        }

        UpdateButtons();
    }

    public void EquipButton()
    {
        //If this is already selected, then select empty...
        if (selectedSlot != null)
        {
            SelectSlot(selectedSlot);
        }
    }

    public void MoreOptionsButton()
    {

    }

    public void CloseButton()
    {
        SetInventoryOpen(false, false);
    }
}
