﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ViewportMouseOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public bool isOver = false;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(InputController.Instance.mouseInputMode)
        {
            isOver = true;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;

        if (InputController.Instance.mouseInputMode)
        {
            isOver = false;
        }
    }

    public void ForceMouseOver(bool val)
    {
        isOver = val;
    }
}
