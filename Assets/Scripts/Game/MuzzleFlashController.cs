﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class MuzzleFlashController : MonoBehaviour
{
    [Header("Settings")]
    public Color startColour = Color.white;
    public Color endColour = Color.yellow;
    public float maxIntensity = 100f;
    public float maxRange = 10f;
    public float duration = 1f;
    public AnimationCurve curve;

    [Header("Components")]
    public Light light;

    [Header("State")]
    public float timer = 0f;
    public float progress = 0f;

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        progress = Mathf.Clamp01(timer / duration);

        float curvedProgress = curve.Evaluate(progress);

        if (light != null)
        {
            light.intensity = curvedProgress * maxIntensity;
            light.range = curvedProgress * maxRange;
            light.color = Color.Lerp(startColour, endColour, progress);
            light.enabled = true;
        }

        if(progress >= 1f)
        {
            Destroy(this.gameObject);
        }
    }
}
