using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Displays a tv broadcast on a renderer
public class BroadcastDisplayController : MonoBehaviour
{
    public MeshRenderer rend;

    void Start()
    {
        if(rend != null) rend.sharedMaterial = SessionData.Instance.broadcastMaterialInstanced;
    }
}
