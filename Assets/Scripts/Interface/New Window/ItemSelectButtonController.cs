﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemSelectButtonController : ButtonController
{
    [System.NonSerialized]
    public Interactable obj;
    public Image photo;
    public InfoWindow thisWindow;

    public void Setup(Interactable newInteractable, InfoWindow newThisWindow)
    {
        base.SetupReferences();

        obj = newInteractable;
        thisWindow = newThisWindow;

        UpdateButtonText();
    }

    public override void UpdateButtonText()
    {
        //Set name
        if (obj != null)
        {
            photo.sprite = obj.preset.staticImage;
            text.text = obj.GetName();
        }
    }

    public override void OnLeftClick()
    {
        if(InteractionController.Instance.talkingTo != null && InteractionController.Instance.talkingTo.isActor != null)
        {
            Human human = InteractionController.Instance.talkingTo.isActor as Human;

            if(human != null)
            {
                human.TryGiveItem(obj, Player.Instance, thisWindow.dialogSuccess, true);
            }
        }

        End();
    }

    private void End()
    {
        thisWindow.CloseWindow();
        base.OnLeftClick();
    }
}
