﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactMatches : Fact
{
    public MatchPreset matchPreset;

    //Used in distance matching
    public float timeRangeDifference = 0f;
    public float travelTime = 0f;

    //Travel time
    NewNode closest1;
    NewNode closest2;

    //Constructor
    public FactMatches(FactPreset newPreset, List<Evidence> newFromEvidence, List<Evidence> newToEvidence, List<object> newPassedObjects, List<Evidence.DataKey> overrideFromKeys, List<Evidence.DataKey> overrideToKeys, bool isCustomFact) : base(newPreset, newFromEvidence, newToEvidence, newPassedObjects, overrideFromKeys, overrideToKeys, isCustomFact)
    {
        matchPreset = newPassedObjects[0] as MatchPreset;
    }

    //Match check- return bool
    public static bool MatchCheck(MatchPreset match, Evidence matchFrom, Evidence matchTo)
    {
        bool passConditions = true;

        foreach(MatchPreset.MatchCondition cond in match.matchConditions)
        {
            //Blood group match
            //if(cond == MatchPreset.MatchCondition.bloodGroup)
            //{
            //    if ((matchFrom as EvidenceBlood).medicalData.bloodType != (matchTo as EvidenceBlood).medicalData.bloodType) return false;
            //}
            //Fingerprint match
            if (cond == MatchPreset.MatchCondition.fingerprint)
            {
                //Game.Log((matchFrom as EvidenceFingerprint).writer.name + " - " + (matchTo as EvidenceFingerprint).writer.name);
                if (matchFrom.writer != matchTo.writer) return false;
            }
            //Time Match
            else if(cond == MatchPreset.MatchCondition.time)
            {
                //Is time within boundary + boundary difference
                float timeFrom1 = (matchFrom as EvidenceTime).timeFrom;
                float timeTo1 = (matchFrom as EvidenceTime).timeTo;

                float timeFrom2 = (matchTo as EvidenceTime).timeFrom;
                float timeTo2 = (matchTo as EvidenceTime).timeTo;

                if (!Toolbox.Instance.GameTimeRangeOverlap(new Vector2(timeFrom1, timeTo1), new Vector2(timeFrom2, timeTo2), true))
                {
                    return false;
                }
            }
            //Visual descriptors match
            else if(cond == MatchPreset.MatchCondition.visualDescriptors)
            {
                EvidenceCitizen fromCit = matchFrom as EvidenceCitizen;
                EvidenceCitizen toCit = matchTo as EvidenceCitizen;

                if (fromCit == null || toCit == null) return false;

                Citizen fromCC = fromCit.witnessController as Citizen;
                Citizen toCC = toCit.witnessController as Citizen;

                //Get visual descriptors in the 'from' evidence using the key in preset
                List<Evidence.DataKey> fromDescriptors = fromCit.GetTiedKeys(match.linkFromKeys);

                //Do the same for the to eveidence
                List<Evidence.DataKey> toDescriptors = toCit.GetTiedKeys(match.linkToKeys);

                //Sex
                int matchingCount = 0;

                if(fromDescriptors.Contains(Evidence.DataKey.sex) && toDescriptors.Contains(Evidence.DataKey.sex))
                {
                    if (fromCC.gender != toCC.gender) return false;
                    else matchingCount++;
                }

                //Height
                //if (fromDescriptors.Contains(Evidence.DescriptorKey.height) && toDescriptors.Contains(Evidence.DescriptorKey.height))
                //{
                //    if (fromCC.descriptors.heightEstimateFT != toCC.descriptors.heightEstimateFT) return false;
                //    else matchingCount++;
                //}

                //Build
                if (fromDescriptors.Contains(Evidence.DataKey.build) && toDescriptors.Contains(Evidence.DataKey.build))
                {
                    if (fromCC.descriptors.build != toCC.descriptors.build) return false;
                    else matchingCount++;
                }

                //Eyes
                if (fromDescriptors.Contains(Evidence.DataKey.eyes) && toDescriptors.Contains(Evidence.DataKey.eyes))
                {
                    if (fromCC.descriptors.eyeColour != toCC.descriptors.eyeColour) return false;
                    else matchingCount++;
                }

                //Age
                if (fromDescriptors.Contains(Evidence.DataKey.age) && toDescriptors.Contains(Evidence.DataKey.age))
                {
                    if (fromCC.GetAge() != toCC.GetAge()) return false;
                    else matchingCount++;
                }

                //Must have at least 1 match to count
                if (matchingCount <= 0) return false;

                //Because the other descriptors are possible to alter, ignore for now as they do not count towards making a match or not

                ////Facial features *Possible to alter
                //if (fromDescriptors.Contains(Evidence.DescriptorKey.facialFeatures) && toDescriptors.Contains(Evidence.DescriptorKey.facialFeatures))
                //{
                //    bool contains = true;

                //    foreach (Descriptors.FacialFeaturesSetting facialSetting in fromCC.descriptors.facialFeatures)
                //    {
                //        contains = toCC.descriptors.facialFeatures.Exists(item => item.feature == facialSetting.feature);
                //        if (!contains) break;
                //    }

                //    if (contains) matchingElements.Add(Evidence.DescriptorKey.facialFeatures);
                //}

                ////Hair *Possible to alter
                //if (fromDescriptors.Contains(Evidence.DescriptorKey.hair) && toDescriptors.Contains(Evidence.DescriptorKey.hair))
                //{
                //    if (fromCC.descriptors.hairColour == toCC.descriptors.hairColour)
                //    {
                //        if(fromCC.descriptors.hairLength == toCC.descriptors.hairLength)
                //        {
                //            if(fromCC.descriptors.hairType == toCC.descriptors.hairType)
                //            {
                //                matchingElements.Add(Evidence.DescriptorKey.hair);
                //            }
                //        }
                //    }
                //}
            }
            //Retail preset match
            else if(cond == MatchPreset.MatchCondition.retailPresetMatch)
            {
                EvidenceRetailItem from = matchFrom as EvidenceRetailItem;
                EvidenceRetailItem to = matchTo as EvidenceRetailItem;

                if (from.interactablePreset != to.interactablePreset) return false; //Must be same preset to match

                //If one of items is tied to information about it's sell location then make sure that matches
                if(from.interactablePreset.spawnEvidence == GameplayControls.Instance.retailItemSoldDiscovery || to.interactablePreset.spawnEvidence == GameplayControls.Instance.retailItemSoldDiscovery)
                {
                    if (from.soldHere != to.soldHere) return false;
                }
            }
            else if(cond == MatchPreset.MatchCondition.murderWeapon)
            {
                return true;
            }
        }

        return passConditions;
    }

    //Special naming
    public override string GenerateNameSuffix()
    {
        return string.Empty;
    }

    //public override void OnDiscovery()
    //{
    //    base.OnDiscovery();

    //    Game.Log("Match: DISCOVER MATCH " + name);
    //}

    //public override void ConnectFact()
    //{
    //    base.ConnectFact();

    //    Game.Log("Match: FACT CONNECTED " + fromEvidence[0].name + "=" + fromEvidence[0].isFound + " to " + toEvidence[0].name + "=" + toEvidence[0].isFound);
    //}

    //public override void OnConnectedEvidenceDiscovery(CaseComponent discovered)
    //{
    //    base.OnConnectedEvidenceDiscovery(discovered);

    //    Game.Log("Match: OnConnectedEvidenceDiscovery " + discovered.name);
    //}
}
