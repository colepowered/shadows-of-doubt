using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[System.Serializable]
public class HEXACO
{
	public int outputMin = 1;
	public int outputMax = 10;

	[Space(7)]

	public bool enableFeminineMasculine = false;
	[Range(0, 10)]
	public int feminineMasculine = 0;

	[Space(7)]

	[InfoBox("Honesty-Humility (H): sincere, honest, faithful, loyal, modest/unassuming versus sly, deceitful, greedy, pretentious, hypocritical, boastful, pompous")]
	public bool enableHumility = false;
	[Range(0, 10)]
	public int humility = 0;

	[Space(7)]

	[InfoBox("Emotionality (E): emotional, oversensitive, sentimental, fearful, anxious, vulnerable versus brave, tough, independent, self-assured, stable")]
	public bool enableEmotionality = false;
	[Range(0, 10)]
	public int emotionality = 0;

	[Space(7)]

	[InfoBox("Extraversion (X): outgoing, lively, extraverted, sociable, talkative, cheerful, active versus shy, passive, withdrawn, introverted, quiet, reserved")]
	public bool enableExtraversion = false;
	[Range(0, 10)]
	public int extraversion = 0;

	[Space(7)]

	[InfoBox("Agreeableness (A): patient, tolerant, peaceful, mild, agreeable, lenient, gentle versus ill-tempered, quarrelsome, stubborn, choleric")]
	public bool enableAgreeableness = false;
	[Range(0, 10)]
	public int agreeableness = 0;

	[Space(7)]

	[InfoBox("Conscientiousness (C): organized, disciplined, diligent, careful, thorough, precise versus sloppy, negligent, reckless, lazy, irresponsible, absent-minded")]
	public bool enableConscientiousness = false;
	[Range(0, 10)]
	public int conscientiousness = 0;

	[Space(7)]

	[InfoBox("Openness to Experience (O): intellectual, creative, unconventional, innovative, ironic versus shallow, unimaginative, conventional")]
	public bool enableCreativity = false;
	[Range(0, 10)]
	public int creativity = 0;
}
