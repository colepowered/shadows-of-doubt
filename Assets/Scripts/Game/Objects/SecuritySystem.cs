﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using NaughtyAttributes;
using System.Net;

public class SecuritySystem : Machine
{
    [Header("Security System Components")]
    public SecuritySystemType system = SecuritySystemType.camera;
    public enum SecuritySystemType { camera, sentry };
    public Animator anim;
    public GameObject laser;
    public Light laserLight;
    [Tooltip("Switch state")]
    public bool isActive = false;
    [Tooltip("Is the animation controller not at the end of an animation?")]
    public bool isAnimating = false;
    //[Tooltip("Does this searching for the player (custom state 1)")]
    //public bool isSeeking = false;
    public Actor trackingTarget;
    public bool acquiredTarget = false;
    public MeshRenderer rend;
    public Transform rotationPivotTransform;
    public Quaternion desiredPivotRotation;
    public Transform selfTransform;
    public Quaternion desiredSelfRotation;
    public Transform muzzleTransform;
    public float seekUpdateProgress = 0f;
    public float forgetProgress = 0f;
    private float pulseProgress = 0f;
    private float focusFlashCounter = 0f;
    public List<NewAIController.TrackingTarget> activeTargets = new List<NewAIController.TrackingTarget>();
    public float sweepProgress = 0.5f;
    private InterfaceController.AwarenessIcon awarenessIcon;
    private float sentryFireProgress = 0f;

    [Header("Settings")]
    public AnimationCurve cameraSweep;
    [Tooltip("How much time in seconds before this sounds an alarm/fires")]
    public float focusGraceTime = 3.5f;
    [Tooltip("How much time before this stops tracking a target that it has previously seen")]
    public float focusGiveUpTime = 5f;

    public void Setup(Interactable newInteractable, bool inheritOpenStatusFromInteractable = true)
    {
        interactable = newInteractable;
        //interactable.securitySystem = this;

        //Create evidence
        if (interactable.evidence == null)
        {
            CreateEvidence();
            interactable.evidence = evidenceEntry;
        }
        else
        {
            evidenceEntry = interactable.evidence as EvidenceWitness;
        }

        //Set open/closed
        if (inheritOpenStatusFromInteractable)
        {
            //Cameras just need power on to be active
            if(interactable.preset.specialCaseFlag == InteractablePreset.SpecialCase.securityCamera)
            {
                SetActive(interactable.sw0, true);
            }
            //Sentries must have power and switch 1 (alarm)
            else if(interactable.preset.specialCaseFlag == InteractablePreset.SpecialCase.sentryGun)
            {
                if(interactable.sw0 && interactable.sw1)
                {
                    SetActive(true, true);
                }
                else SetActive(false, true);
            }
        }
    }

    public override void CreateEvidence()
    {
        //Create citizen entry
        if (evidenceEntry == null)
        {
            string evID = "SecuritySystem" + interactable.id;
            evidenceEntry = EvidenceCreator.Instance.CreateEvidence("SecuritySystem", evID, this) as EvidenceWitness;
        }
    }

    public void SetActive(bool open, bool skipAnimation = false)
    {
        //Interupt with breaker state
        if(interactable != null && interactable.node != null && interactable.node.gameLocation.thisAsAddress != null)
        {
            Interactable breaker = interactable.node.gameLocation.thisAsAddress.GetBreakerSecurity();

            if (breaker != null && !breaker.sw0)
            {
                open = false;
            }
        }

        isActive = open;
        if (laser != null) laser.SetActive(isActive);

        //Skip animation
        if (skipAnimation || !this.gameObject.activeInHierarchy)
        {
            if(isActive)
            {
                if(anim != null) anim.SetTrigger("activeImmediate");
            }
            else
            {
                if (anim != null) anim.SetTrigger("inactiveImmediate");
            }
        }

        if (anim != null) anim.SetBool("active", isActive);

        //If this is active, make sure update loop is on.
        if (isActive)
        {
            this.enabled = true;
        }
        else
        {
            ResetFocus();
        }

        //Change lighting skin
        UpdateMaterial();
    }

    private void UpdateMaterial()
    {
        //Change lighting skin
        if (system == SecuritySystemType.camera)
        {
            if (isActive)
            {
                if(trackingTarget != null && seesIllegal.ContainsKey(trackingTarget) && seesIllegal[trackingTarget] >= 1f)
                {
                    if(rend.sharedMaterial != InteriorControls.Instance.cameraAlertMaterial) rend.sharedMaterial = InteriorControls.Instance.cameraAlertMaterial;
                }
                else
                {
                    if (rend.sharedMaterial != InteriorControls.Instance.cameraOnMaterial) rend.sharedMaterial = InteriorControls.Instance.cameraOnMaterial;
                }
            }
            else
            {
                if (rend.sharedMaterial != InteriorControls.Instance.cameraOffMaterial) rend.sharedMaterial = InteriorControls.Instance.cameraOffMaterial;
            }
        }
    }

    //Set seeking (custom state 1)
    //public void SetSeeking(bool seek)
    //{
    //    isSeeking = seek;
    //    if(!isSeeking) OnInvestigate(null, 0); //Reset tracking target

    //    UpdateMaterial();
    //}

    //Triggered when an investigation reaction is needed
    public override void OnInvestigate(Actor newTarget, int escalation)
    {
        Game.Log("Player: Security system spotted " + newTarget + " with escalation " + escalation);

        if (trackingTarget == null && newTarget != null && system == SecuritySystemType.sentry)
        {
            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.sentryGunTargetAcquire, this, currentNode, muzzleTransform.position);
        }

        trackingTarget = newTarget;
        forgetProgress = 0f;
    }

    private void Update()
    {
        //Detect if animation is playing...
        isAnimating = false;

        if (anim != null)
        {
            AnimatorStateInfo state = anim.GetCurrentAnimatorStateInfo(0);

            if(state.normalizedTime < 1f)
            {
                isAnimating = true;
            }
        }

        //If inactive, disable the update loop
        if (!isActive)
        {
            this.enabled = false;
        }

        if(!SessionData.Instance.play)
        {
            return;
        }

        //Handle laser
        if (laser != null)
        {
            Toolbox.Instance.HandleLaserBehaviour(this, laser, laserLight);
        }

        //If not actively tracking a target, perform a basic sweeping motion
        if (!isAnimating)
        {
            if (this.gameObject.activeInHierarchy)
            {
                seekUpdateProgress += Time.deltaTime;

                if (seekUpdateProgress >= 0.33f)
                {
                    seekUpdateProgress = 0f;

                    //Seek targets if camera...
                    if (system == SecuritySystemType.camera)
                    {
                        SightingCheck(GameplayControls.Instance.securityFOV, false);

                        //Pick a new tracking target
                        if (trackingTarget == null && seenIllegalThisCheck.Count > 0)
                        {
                            foreach (Actor a in seenIllegalThisCheck)
                            {
                                //For now, only track player...
                                //if (!a.isPlayer) continue;

                                OnInvestigate(a, 1);
                                break;
                            }
                        }
                    }
                    //Otherwise inherit targets from camera systems...
                    else if (system == SecuritySystemType.sentry)
                    {
                        //Copy sees illegal from cameras
                        seenIllegalThisCheck.Clear();
                        //seesIllegal.Clear();
                        //ClearSeesIllegal();

                        //Acts as it's own camera...
                        SightingCheck(1, false); //This is need to remove 'sees illegal' from lack of sighting

                        foreach (Interactable sys in interactable.node.gameLocation.securityCameras)
                        {
                            if (sys.controller == null) continue;
                            if (sys.controller.securitySystem == null) continue;

                            //Acquire cam's target
                            if (trackingTarget == null && sys.controller.securitySystem.trackingTarget != null)
                            {
                                OnInvestigate(sys.controller.securitySystem.trackingTarget, 2);
                            }

                            foreach (Human seen in sys.controller.securitySystem.seenIllegalThisCheck)
                            {
                                if (!seenIllegalThisCheck.Contains(seen))
                                {
                                    seenIllegalThisCheck.Add(seen);
                                }
                            }

                            foreach (KeyValuePair<Actor, float> pair in sys.controller.securitySystem.seesIllegal)
                            {
                                if (!seesIllegal.ContainsKey(pair.Key))
                                {
                                    seesIllegal.Add(pair.Key, pair.Value);
                                }
                                else
                                {
                                    seesIllegal[pair.Key] = Mathf.Max(seesIllegal[pair.Key], pair.Value);
                                }
                            }
                        }
                    }
                }
            }

            if (trackingTarget == null)
            {
                //Reset focus progress
                if (seenIllegalThisCheck.Count > 0)
                {
                    ResetFocus();
                }

                //If camera or active sentry, do a sweep motion
                if (system == SecuritySystemType.camera || (system == SecuritySystemType.sentry && isActive))
                {
                    sweepProgress += Time.deltaTime / 10f;

                    //Set the camera tracking height to 20 on the x axis...
                    float xAxis = Mathf.Lerp(selfTransform.localEulerAngles.x, 20f, GameplayControls.Instance.securityTrackSpeed * Time.deltaTime);

                    //Clamp
                    selfTransform.localEulerAngles = new Vector3(Toolbox.Instance.ClampAngle(xAxis, 0f, 80f), 0, 0);

                    //Get the Y rotation from the sweep curve...
                    Quaternion newQ = Quaternion.Euler(0, cameraSweep.Evaluate(sweepProgress), 0);
                    rotationPivotTransform.localRotation = Quaternion.Slerp(rotationPivotTransform.localRotation, newQ, GameplayControls.Instance.securityTrackSpeed * Time.deltaTime);

                    //float yAxis = Mathf.Lerp(rotationPivotTransform.localEulerAngles.y, cameraSweep.Evaluate(sweepProgress), GameplayControls.Instance.securityTrackSpeed * Time.deltaTime);

                    if (sweepProgress > 1f) sweepProgress -= 1f;
                    sweepProgress = Mathf.Clamp01(sweepProgress);
                }
            }
            else
            {
                //Game.Log("Security tracking target: " + trackingTarget + "...");

                //Build or loose focus
                if (seesIllegal.ContainsKey(trackingTarget) && !trackingTarget.isDead && !trackingTarget.isStunned)
                {
                    //Setup awareness
                    if (awarenessIcon == null && trackingTarget.isPlayer)
                    {
                        awarenessIcon = InterfaceController.Instance.AddAwarenessIcon(InterfaceController.AwarenessType.actor, InterfaceController.AwarenessBehaviour.alwaysVisible, this, selfTransform, Vector3.zero, InterfaceControls.Instance.spotted, 10, true, InterfaceControls.Instance.maxIndicatorDistance);
                    }
                    else if (awarenessIcon != null)
                    {
                        awarenessIcon.removalFlag = false;
                    }

                    bool alarmSysTarget = interactable.node.gameLocation.IsAlarmSystemTarget(trackingTarget as Human);

                    int esc = trackingTarget.trespassingEscalation;
                    if (trackingTarget.illegalActionActive) esc = 2;
                    if(trackingTarget.currentRoom != null) esc += trackingTarget.currentRoom.gameLocation.GetAdditionalEscalation(trackingTarget);
                    esc = Mathf.Clamp(esc, 1, 2);

                    //Seen target doing illegal stuff!
                    if (seesIllegal[trackingTarget] < 1f || (esc == 1 && !alarmSysTarget))
                    {
                        Game.Log("Security system Sees illegal contains " + trackingTarget + ": " + seesIllegal[trackingTarget] + " esc " + esc);

                        //Flash material when escalation requires
                        if (system == SecuritySystemType.camera)
                        {
                            focusFlashCounter += Time.deltaTime * 4.5f;

                            if (focusFlashCounter >= 1f)
                            {
                                focusFlashCounter = 0f;

                                if ((esc >= 2 || alarmSysTarget) && rend.sharedMaterial != InteriorControls.Instance.cameraAlertMaterial)
                                {
                                    rend.sharedMaterial = InteriorControls.Instance.cameraAlertMaterial;

                                    //Only play alert sound if alarm inactive
                                    if (!interactable.node.gameLocation.IsAlarmActive(out _, out _, out _))
                                    {
                                        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.securityCameraAlert, this, currentNode, this.transform.position);
                                    }
                                }
                                else if (esc == 1 && rend.sharedMaterial != InteriorControls.Instance.cameraFocusMaterial)
                                {
                                    rend.sharedMaterial = InteriorControls.Instance.cameraFocusMaterial;
                                }
                                else if(esc == 1 || seesIllegal[trackingTarget] <= 0.5f)
                                {
                                    rend.sharedMaterial = InteriorControls.Instance.cameraOnMaterial;
                                }
                                else
                                {
                                    rend.sharedMaterial = InteriorControls.Instance.cameraFocusMaterial;
                                }
                            }
                        }

                        //Add escalation if on level 1
                        if (esc == 1 && !alarmSysTarget)
                        {
                            //Update escalation
                            if (trackingTarget.currentRoom != null)
                            {
                                trackingTarget.currentRoom.gameLocation.AddEscalation(trackingTarget);
                            }
                        }
                    }
                    else if (seesIllegal[trackingTarget] >= 1f)
                    {
                        if (!acquiredTarget)
                        {
                            if(esc >= 2 || alarmSysTarget)
                            {
                                Game.Log("...Acquire target " + trackingTarget);

                                acquiredTarget = true;

                                //Confirm trespassing
                                if (trackingTarget.isPlayer && Player.Instance.currentGameLocation.thisAsAddress != null && Player.Instance.illegalActionActive)
                                {
                                    StatusController.Instance.ConfirmFine(Player.Instance.currentGameLocation.thisAsAddress, null, StatusController.CrimeType.trespassing);
                                }

                                UpdateMaterial();
                            }
                        }
                    }

                    //Fire if target is aquired...
                    if (acquiredTarget)
                    {
                        //If camera, sound the alarm on this floor
                        if (system == SecuritySystemType.camera)
                        {
                            if (interactable.node.gameLocation.thisAsAddress != null) interactable.node.gameLocation.thisAsAddress.SetAlarm(true, trackingTarget as Human);
                        }

                        //If sentry gun fire!
                        if (system == SecuritySystemType.sentry)
                        {
                            if (interactable.node.gameLocation.thisAsAddress != null) interactable.node.gameLocation.thisAsAddress.SetAlarm(true, trackingTarget as Human);
                            sentryFireProgress += Time.deltaTime * GameplayControls.Instance.sentryGunROF;

                            if (sentryFireProgress >= 1f)
                            {
                                sentryFireProgress -= 1f;
                                Toolbox.Instance.Shoot(this, muzzleTransform.position, trackingTarget.aimTransform.position, 12f, GameplayControls.Instance.sentryGunAccuracy, GameplayControls.Instance.sentryGunDamage, GameplayControls.Instance.sentryGunWeapon, false, Vector3.zero, false);
                            }
                        }
                    }
                }
                else
                {
                    if (trackingTarget != null && (!seenIllegalThisCheck.Contains(trackingTarget) || trackingTarget.isDead || trackingTarget.isStunned))
                    {
                        forgetProgress += Time.deltaTime / focusGiveUpTime;

                        //Game.Log("...Camera forget target " + forgetProgress);

                        if (forgetProgress >= 1f)
                        {
                            OnInvestigate(null, 0);
                            ResetFocus();
                            return;
                        }
                        else if (system == SecuritySystemType.sentry)
                        {
                            pulseProgress += Time.deltaTime / 0.9f;

                            if (pulseProgress >= 1f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.sentryGunSearchPulse, this, currentNode, muzzleTransform.position);
                                pulseProgress = 0f;
                            }
                        }
                    }
                }

                //Tracking is the target's location
                if (trackingTarget != null)
                {
                    Vector3 dirToTarget = trackingTarget.transform.position - selfTransform.position;

                    if (dirToTarget != Vector3.zero)
                    {
                        desiredSelfRotation = Quaternion.LookRotation(dirToTarget, Vector3.up);
                        desiredPivotRotation = Quaternion.LookRotation(dirToTarget, Vector3.up);
                    }
                }

                //Follow desired directions
                if (selfTransform.rotation != desiredSelfRotation)
                {
                    selfTransform.rotation = Quaternion.Slerp(selfTransform.rotation, desiredSelfRotation, GameplayControls.Instance.securityTrackSpeed * Time.deltaTime);

                    //Clamp
                    Vector3 lEuler = selfTransform.localEulerAngles;

                    selfTransform.localEulerAngles = new Vector3(
                        Toolbox.Instance.ClampAngle(lEuler.x, 0f, 80f),
                        0,
                        0
                        );
                }

                if (rotationPivotTransform.rotation != desiredPivotRotation)
                {
                    rotationPivotTransform.rotation = Quaternion.Slerp(rotationPivotTransform.rotation, desiredPivotRotation, GameplayControls.Instance.securityTrackSpeed * Time.deltaTime);

                    //Clamp
                    Vector3 lEuler = rotationPivotTransform.localEulerAngles;

                    rotationPivotTransform.localEulerAngles = new Vector3(
                        0,
                        lEuler.y,
                        0
                        );
                }
            }
        }
    }

    private void OnDestroy()
    {
        ResetFocus(); //For now remove all focus once destroyed
    }

    private void OnDisable()
    {
        ResetFocus(); //For now remove all focus once destroyed
    }

    public void ResetFocus()
    {
        //Game.Log("Camera reset focus");
        ClearSeesIllegal();
        focusFlashCounter = 0f;
        acquiredTarget = false;
        trackingTarget = null;
        forgetProgress = 0f;
        sentryFireProgress = 0f;
        pulseProgress = 0f;
        UpdateMaterial();
        if (awarenessIcon != null) awarenessIcon.Remove();
    }
}
