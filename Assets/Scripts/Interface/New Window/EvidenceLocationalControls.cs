﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class EvidenceLocationalControls : MonoBehaviour
{
    public InfoWindow parentWindow;

    [Header("Location Controls")]
    public ButtonController locateOnMapButton;
    public ButtonController plotRouteButton;

    [Header("Job Posting Controls")]
    public ButtonController acceptJobButton;
    public TextMeshProUGUI acceptJobText;

    [Header("Take Item Controls")]
    public ButtonController takeItemButton;
    public TextMeshProUGUI takeItemText;

    private void OnEnable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange += CheckEnabled;
        parentWindow.OnWindowRefresh += CheckEnabled;

        if(acceptJobText != null)
        {
            //Remove button if the job is accepted.
            if (parentWindow.passedEvidence.interactable.jobParent.accepted)
            {
                Destroy(this.gameObject);
                return;
            }

            acceptJobText.text = Strings.Get("ui.interface", "Accept Job");
        }

        if (takeItemText != null)
        {
            takeItemText.text = Strings.Get("ui.interface", "Take Item");

            if(parentWindow.pinned)
            {
                this.gameObject.SetActive(false);
            }
            else
            {
                this.gameObject.SetActive(true);
            }
        }

        CheckEnabled();
    }

    private void OnDisable()
    {
        if(parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange -= CheckEnabled;
        parentWindow.OnWindowRefresh -= CheckEnabled;
    }

    public void CheckEnabled()
    {
        Game.Log("Interface: Check for location key in parent window...");

        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();

        if(locateOnMapButton != null && plotRouteButton != null)
        {
            if (parentWindow.evidenceKeys.Contains(Evidence.DataKey.name))
            {
                Game.Log("Interface: Location key found");

                locateOnMapButton.SetInteractable(true);
                plotRouteButton.SetInteractable(true);
                //autoTravelButton.SetInteractable(true);
            }
            else
            {
                Game.Log("Window: Location key not found");

                locateOnMapButton.SetInteractable(false);
                plotRouteButton.SetInteractable(false);
                //autoTravelButton.SetInteractable(false);
            }
        }
    }

    public void OnLocateOnMap()
    {
        MapController.Instance.LocateEvidenceOnMap(parentWindow.passedEvidence);
    }

    public void OnPlotRoute()
    {
        //Evidence is location
        EvidenceLocation loc = parentWindow.passedEvidence as EvidenceLocation;
        EvidenceBuilding build = parentWindow.passedEvidence as EvidenceBuilding;

        if (loc != null)
        {
            //Remove existing route
            if(MapController.Instance.playerRoute != null)
            {
                MapController.Instance.playerRoute.Remove();

                //Return to normal colour
                if(plotRouteButton != null) plotRouteButton.button.image.color = plotRouteButton.baseColour;

                return;
            }
        }
        else if(build != null)
        {
            //Remove existing route
            if (MapController.Instance.playerRoute != null)
            {
                MapController.Instance.playerRoute.Remove();

                //Return to normal colour
                if (plotRouteButton != null) plotRouteButton.button.image.color = plotRouteButton.baseColour;

                return;
            }
        }

        //Plot route
        MapController.Instance.PlotPlayerRoute(parentWindow.passedEvidence);
    }

    public void OnAutoTravel()
    {
        //EvidenceLocation loc = parentWindow.passedEvidence as EvidenceLocation;
        //if (loc != null) FastTravel.Instance.ExecuteFastTravel(loc.locationController.thisAsAddress.anchorNode);
    }

    public void OnAcceptJob()
    {
        if(CasePanelController.Instance.activeCases.Count < GameplayControls.Instance.maxCases)
        {
            //Accept job on side mission
            parentWindow.passedEvidence.interactable.jobParent.AcceptJob();

            //Cancel world interaction as the player will probably want to examine the evidence...
            parentWindow.SetWorldInteraction(false);

            //Remove button
            Destroy(this.gameObject);

            //Remove job posting
            parentWindow.passedEvidence.interactable.RemoveFromPlacement();
        }
        else
        {
            PopupMessageController.Instance.PopupMessage("TooManyCases", true);
        }
    }

    public void OnTakeItem()
    {
        //Is this item a key? In which case we don't need to pin it to
        Interactable interactable = parentWindow.passedEvidence.interactable;

        if(interactable.preset.name == "Key")
        {
            ActionController.Instance.TakeKey(interactable, null, Player.Instance);
        }
        else
        {
            //Add to inventory
            interactable.SetInInventory(Player.Instance);

            //Pin elements
            CasePanelController.Instance.PinToCasePanel(CasePanelController.Instance.activeCase, parentWindow.passedEvidence, parentWindow.passedKeys, forceAutoPin: true);
        }

        parentWindow.CloseWindow(); //Close window

        if (parentWindow.pinned)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            this.gameObject.SetActive(true);
        }
    }
}
