﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactFavourite : Fact
{
    public Company company;

    //Constructor
    public FactFavourite(FactPreset newPreset, List<Evidence> newFromEvidence, List<Evidence> newToEvidence, List<object> newPassedObjects, List<Evidence.DataKey> overrideFromKeys, List<Evidence.DataKey> overrideToKeys, bool isCustomFact) : base(newPreset, newFromEvidence, newToEvidence, newPassedObjects, overrideFromKeys, overrideToKeys, isCustomFact)
    {
        company = (toEvidence[0] as EvidenceLocation).locationController.thisAsAddress.company;
    }

    public override void OnDiscovery()
    {
        base.OnDiscovery();

        company = (toEvidence[0] as EvidenceLocation).locationController.thisAsAddress.company;

        //if (company.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.bank)) toEvidence[0].AddDescriptorKey(Evidence.DescriptorKey.favBank, Evidence.DataKey.name);
        //else if (company.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.caffeine)) toEvidence[0].AddDescriptorKey(Evidence.DescriptorKey.favCaffeine, Evidence.DataKey.name);
        //else if (company.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.dentist)) toEvidence[0].AddDescriptorKey(Evidence.DescriptorKey.favDentist, Evidence.DataKey.name);
        //else if (company.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.doctor)) toEvidence[0].AddDescriptorKey(Evidence.DescriptorKey.favDoctor, Evidence.DataKey.name);
        //else if (company.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.entertainment)) toEvidence[0].AddDescriptorKey(Evidence.DescriptorKey.favEntertainment, Evidence.DataKey.name);
        //else if (company.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.food)) toEvidence[0].AddDescriptorKey(Evidence.DescriptorKey.favFoodDining, Evidence.DataKey.name);
        //else if (company.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.foodTakeaway)) toEvidence[0].AddDescriptorKey(Evidence.DescriptorKey.favFoodQuick, Evidence.DataKey.name);
        //else if (company.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.hairStylist)) toEvidence[0].AddDescriptorKey(Evidence.DescriptorKey.favHairStylist, Evidence.DataKey.name);
        //else if (company.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.recreational)) toEvidence[0].AddDescriptorKey(Evidence.DescriptorKey.favRecreational, Evidence.DataKey.name);
        //else if (company.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.retail)) toEvidence[0].AddDescriptorKey(Evidence.DescriptorKey.favRetail, Evidence.DataKey.name);
        //else if (company.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.bar)) toEvidence[0].AddDescriptorKey(Evidence.DescriptorKey.favSocial, Evidence.DataKey.name);

        UpdateName();
    }

    //Get name from the connecting evidence. Optional: Get name for a specific link
    public override string GetName(Evidence.FactLink specificLink = null)
    {
        company = (toEvidence[0] as EvidenceLocation).locationController.thisAsAddress.company;

        string ret = string.Empty;

        //Get from names
        for (int i = 0; i < fromEvidence.Count; i++)
        {
            Evidence fromEv = fromEvidence[i];

            if (i > 0) ret += " & ";
            ret += fromEv.GetNameForDataKey(Evidence.DataKey.name);
        }

        ret += " " + Strings.Get("evidence.generic", preset.name);
        //ret += " " + Strings.Get("evidence.generic", company.preset.companyCategory.ToString());
        ret += " " + Strings.Get("evidence.generic", "is");

        //Get to names
        for (int i = 0; i < toEvidence.Count; i++)
        {
            Evidence toEv = toEvidence[i];

            if (i > 0) ret += " & ";
            ret += toEv.GetNameForDataKey(Evidence.DataKey.name);
        }

        Game.Log(ret.Trim());

        return ret.Trim();
    }
}
