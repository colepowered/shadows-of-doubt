using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrecipitationParticleSystemController : MonoBehaviour
{
    [Header("Components")]
    public ParticleSystem snowSystem;
    public ParticleSystem rainSystem;

    [Header("Settings")]
    public int snowMaxEmissionRate = 2500;
    public int rainMaxEmissionRate = 18000;

    [Header("State")]
    public bool snowMode = false;

    //Singleton pattern
    private static PrecipitationParticleSystemController _instance;
    public static PrecipitationParticleSystemController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        SetSnowMode(false, true);
    }

    public void SetSnowMode(bool val, bool forceUpdate = false)
    {
        if (val != snowMode || forceUpdate)
        {
            snowMode = val;

            if(this.enabled)
            {
                snowSystem.gameObject.SetActive(snowMode);
                rainSystem.gameObject.SetActive(!snowMode);
            }
        }
    }

    public void SetEnabled(bool val)
    {
        if(val)
        {
            snowSystem.gameObject.SetActive(snowMode);
            rainSystem.gameObject.SetActive(!snowMode);
        }
        else
        {
            snowSystem.gameObject.SetActive(false);
            rainSystem.gameObject.SetActive(false);
        }

        this.enabled = val;
    }

    public void AddAreaTrigger(Collider coll)
    {
        snowSystem.trigger.AddCollider(coll);
        rainSystem.trigger.AddCollider(coll);
    }

    public void RemoveAreaTrigger(Collider coll)
    {
        snowSystem.trigger.RemoveCollider(coll);
        rainSystem.trigger.RemoveCollider(coll);
    }

    // Update is called once per frame
    void Update()
    {
        if (SessionData.Instance.currentRain > 0f || SessionData.Instance.currentSnow > 0f)
        {
            //Always position on player
            this.transform.position = new Vector3(Player.Instance.transform.position.x, 6f, Player.Instance.transform.position.z);

            if(snowMode)
            {
                //Set emission rate
                var emission = snowSystem.emission;
                emission.rateOverTime = SessionData.Instance.currentSnow * snowMaxEmissionRate;

                if (Player.Instance.currentNode != null)
                {
                    //Stop/start completely if inside or outside
                    if(Player.Instance.currentNode.isOutside && snowSystem.isStopped)
                    {
                        snowSystem.Play();
                    }
                    else if (!Player.Instance.currentNode.isOutside && snowSystem.isPlaying)
                    {
                        snowSystem.Stop();
                    }
                }
            }
            else
            {
                //Set emission rate
                var emission = rainSystem.emission;
                emission.rateOverTime = SessionData.Instance.currentRain * rainMaxEmissionRate;

                if (Player.Instance.currentNode != null)
                {
                    //Stop/start completely if inside or outside
                    if (Player.Instance.currentNode.isOutside && rainSystem.isStopped)
                    {
                        rainSystem.Play();
                    }
                    else if (!Player.Instance.currentNode.isOutside && rainSystem.isPlaying)
                    {
                        rainSystem.Stop();
                    }
                }
            }
        }
    }
}
