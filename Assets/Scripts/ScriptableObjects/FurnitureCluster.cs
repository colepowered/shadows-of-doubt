﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "furniturecluster_data", menuName = "Database/Decor/Furniture Cluster")]

public class FurnitureCluster : SoCustomComparison
{
    public bool disable = false;

    public enum FurnitureRuleOption { mustFeature, cantFeature, canFeature};
    public enum WallRule { nothing, wallNoDoor, onlyWall, doorway, door, bannister, window};
    public enum FurnitureFacing { down, up, left, right};
    public enum AllowedOpenPlan { yes, no, openPlanOnly};

    [System.Serializable]
    public class FurnitureClusterRule
    {
        [Tooltip("Only consider this if the last object got placed")]
        public bool onlyValidIfPreviousObjectPlaced = false;
        [Tooltip("If not able to be placed at the above, scan list of alternates at random to find a valid position")]
        public List<Vector2> placements = new List<Vector2>();
        [Tooltip("What should be found at this node?")]
        public FurnitureClass furnitureClass;
        public FurnitureFacing facing = FurnitureFacing.down;
        [Tooltip("If cannot be place here, this cluster placement is invalid")]
        public bool importantToCluster = false;
        [DisableIf("importantToCluster")]
        [Range(0f, 1f)]
        [Tooltip("Chance this placement will be attempted: Done on a per-location basis, so often a lower number will result in a much lower placement count...")]
        public float chanceOfPlacementAttempt = 1f;
        public int placementScoreBoost = 0;
        [Tooltip("Block objects in this path")]
        public bool useFovBlock = false;
        [Tooltip("The FOV block will continue in this direction, this is before direction is applied, so 0,1 is infront for example.")]
        public Vector2 blockDirection = new Vector2(0, 1);
        public int maxFOVBlockDistance = 5;
        [Tooltip("Local scale")]
        public Vector3 localScale = Vector3.one;
        [Tooltip("Offset")]
        public Vector3 positionOffset = Vector3.zero;
    }

    [Header("Rules")]
    [Tooltip("List of rules this furniture must follow")]
    public List<FurnitureClusterRule> clusterElements = new List<FurnitureClusterRule>();

    [Header("Suitability")]
    [Tooltip("Chance for skipping this cluster altogether")]
    [Range(0f, 1f)]
    public float placementChance = 1f;
    [Tooltip("The ranking given to this item when choosing what to place.")]
    [Range(0, 11)]
    public float roomPriority = 5;
    [Tooltip("Modify priority with traits present. The base chance here is x10 and added to the above room priority.")]
    public List<CharacterTrait.TraitPickRule> modifyPriorityTraits = new List<CharacterTrait.TraitPickRule>();
    [Tooltip("Modify placement chance with traits present. The base chance here is x10 and added to the above placement chance.")]
    public List<CharacterTrait.TraitPickRule> modifyPlacementChanceTraits = new List<CharacterTrait.TraitPickRule>();
    [Space(7)]
    [Tooltip("If true this will override priority with 10 unless one already exists")]
    public bool essentialFurniture = false;

    [Header("PreCalculated Limits/Optimization")]
    [OnValueChanged("UpdatePreCalculatedLimits")]
    public bool updatePreCalculated = false;
    [ReadOnly]
    public int calculatedMinRoomSize = 1;
    [ReadOnly]
    public int minimumZeroNodeWallCount = 0;
    [ReadOnly]
    public int maximumZeroNodeWallCount = 4;
    [ReadOnly]
    public List<FurnitureClass> zeroNodeClasses = new List<FurnitureClass>();

    [Header("Custom Limits/Optimization")]
    [Tooltip("Room must be at least this size")]
    public int minimumRoomSize = 1;
    public bool useMaximumRoomSize = true;
    [EnableIf("useMaximumRoomSize")]
    public int maximumRoomSize = 99;
    [Space(7)]
    public bool useCustomZeroNodeMinWallCount = false;
    [EnableIf("useCustomZeroNodeMinWallCount")]
    [Range(0, 4)]
    public int customZeroNodeMinWallCount = 0;
    public bool useCustomZeroNodeMaxWallCount = false;
    [EnableIf("useCustomZeroNodeMaxWallCount")]
    [Range(0, 4)]
    public int customZeroNodeMaxWallCount = 0;
    public List<FurnitureClass.FurnitureWallRule> zeroNodeWallRules = new List<FurnitureClass.FurnitureWallRule>();

    [Space(7)]
    [Tooltip("Is this allowed in open plan rooms?")]
    public AllowedOpenPlan allowedInOpenPlan = AllowedOpenPlan.yes;
    [Space(7)]
    public bool allowInResidential = true;
    public bool allowInCompanies = true;
    public bool allowOnStreets = true;
    [Tooltip("This is only to be placed on coastal streets")]
    public bool coastalOnly = false;
    [Space(7)]
    [Tooltip("Only allow this in certain districts")]
    public bool limitToDistricts = false;
    [EnableIf("limitToDistricts")]
    public List<DistrictPreset> allowedInDistricts = new List<DistrictPreset>();
    public bool banFromDistricts = false;
    [EnableIf("banFromDistricts")]
    public List<DistrictPreset> notAllowedInDistricts = new List<DistrictPreset>();
    [Space(7)]
    [Tooltip("Skip placement of this if there are no address owners")]
    public bool skipIfNoAddressInhabitants = false;
    [EnableIf("skipIfNoAddressInhabitants")]
    [Tooltip("If the above is true, will only be skipped if this is a residence or company")]
    public bool onlySkipNoInhabitantsIfResidenceOrCompany = true;
    [EnableIf("skipIfNoAddressInhabitants")]
    [Tooltip("If the above is true, don't skip if within addresses of this type")]
    public List<RoomClassPreset> dontSkipNoInhabitantsIfIn = new List<RoomClassPreset>();
    [Space(7)]
    public List<RoomTypeFilter> allowedRoomFilters = new List<RoomTypeFilter>();

    [Space(7)]
    [Tooltip("Maximum number per room")]
    public bool limitPerRoom = true;
    [EnableIf("limitPerRoom")]
    [Range(1, 20)]
    public int maximumPerRoom = 2;

    [Tooltip("Maximum number per address")]
    public bool limitPerAddress = false;
    [EnableIf("limitPerAddress")]
    [Range(1, 20)]
    public int maximumPerAddress = 1;

    [Tooltip("Allow only on this floor")]
    public bool limitToFloor = false;
    [EnableIf("limitToFloor")]
    public int allowedOnFloor = 0;

    [Tooltip("Allow only between these floors")]
    [DisableIf("limitToFloor")]
    public bool limitToFloorRange = false;
    [EnableIf("limitToFloorRange")]
    public Vector2Int allowedOnFloorRange = new Vector2Int(-2, 99);

    //public bool limitPerBuilding = false;
    //[Tooltip("Maximum number per building")]
    //[EnableIf("limitPerBuilding")]
    //[Range(1, 20)]
    //public int maximumPerBuilding = 1;

    //public bool limitPerCity = false;
    //[Tooltip("Maximum number per city")]
    //[EnableIf("limitPerCity")]
    //[Range(1, 20)]
    //public int maximumPerCity = 1;

    public bool wealthLimit = false;
    [EnableIf("wealthLimit")]
    [Range(0f, 1f)]
    public float minimumWealth = 0f;
    [EnableIf("wealthLimit")]
    [Range(0f, 1f)]
    public float maximumWealth = 1f;

    public bool useRoomGrub = false;
    [EnableIf("useRoomGrub")]
    [Range(0f, 1f)]
    public float minimumGrub = 0f;
    [EnableIf("useRoomGrub")]
    [Range(0f, 1f)]
    public float maximumGrub = 1f;

    public bool useBuildingResidences = false;
    [EnableIf("useBuildingResidences")]
    public int minimumResidences = 0;
    [EnableIf("useBuildingResidences")]
    public int maximumResidences = 40;

    [Header("Optimizations")]
    [Tooltip("If this cluster is successfully placed, add the following cluster presets from trying to be placed")]
    public List<FurnitureCluster> addClustersOnSuccess = new List<FurnitureCluster>();
    [Tooltip("If this cluster is successfully placed, remove the following cluster presets from trying to be placed")]
    public List<FurnitureCluster> removeClustersOnSuccess = new List<FurnitureCluster>();
    [Tooltip("If this cluster fails to be placed, remove the following cluster presets from trying to be placed")]
    public List<FurnitureCluster> removeClustersOnFail = new List<FurnitureCluster>();

    [Header("Misc.")]
    [Tooltip("Is this a security door?")]
    public bool securityDoor = false;
    [Tooltip("Is this a breaker box?")]
    public bool isBreakerBox = false;

    [Header("Debugging")]
    public bool enableDebug = false;

    [Button]
    public void UpdatePreCalculatedLimits()
    {
        calculatedMinRoomSize = 0;
        minimumZeroNodeWallCount = 0;
        maximumZeroNodeWallCount = 4;

        List<FurnitureClusterRule> zeroNodePlacements = new List<FurnitureClusterRule>();
        zeroNodeClasses = new List<FurnitureClass>();

        //Run through setup and count room size needed
        foreach (FurnitureClusterRule cl in clusterElements)
        {
            if(cl.importantToCluster)
            {
                if(cl.furnitureClass != null)
                {
                    if (cl.placements.Exists(item => Mathf.RoundToInt(item.x) == 0 && Mathf.RoundToInt(item.y) == 0))
                    {
                        zeroNodePlacements.Add(cl);
                        zeroNodeClasses.Add(cl.furnitureClass);
                    }

                    int objectSpace = (int)cl.furnitureClass.objectSize.x * (int)cl.furnitureClass.objectSize.y;

                    //Add physical space needed by object itself
                    if (cl.furnitureClass.occupiesTile)
                    {
                        calculatedMinRoomSize += objectSpace;
                    }
                }
            }
        }

        foreach(FurnitureClusterRule cl in zeroNodePlacements)
        {
            if(cl.furnitureClass != null)
            {
                cl.furnitureClass.UpdatePreCalculatedLimits();
                minimumZeroNodeWallCount = Mathf.Max(minimumZeroNodeWallCount, cl.furnitureClass.minimumZeroNodeWallCount);
                maximumZeroNodeWallCount = Mathf.Min(maximumZeroNodeWallCount, cl.furnitureClass.maximumZeroNodeWallCount);
            }
        }

        calculatedMinRoomSize = Mathf.Max(1, calculatedMinRoomSize);
        minimumRoomSize = Mathf.Max(minimumRoomSize, calculatedMinRoomSize);

        //Save changes
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(this);
#endif
    }
}
