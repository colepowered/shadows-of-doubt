﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class CriminalControls : MonoBehaviour
{
    [Header("Blood Patterns")]
    public SpatterPatternPreset punchSpatter;

    //Singleton pattern
    private static CriminalControls _instance;
    public static CriminalControls Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
}
