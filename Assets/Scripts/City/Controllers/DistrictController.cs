﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

//A controlelr give to each district in the city. Not very visible to the player, but will crop up from time to time.
//Script pass 1
public class DistrictController : Controller, IComparable<DistrictController>
{
    [Header("ID")]
    public int districtID = 0;
    public static int assignID = 1;
    public string seed;

    [Header("Location")]
    public List<BlockController> blocks = new List<BlockController>();
    public List<CityTile> cityTiles = new List<CityTile>();

    [Header("Details")]
    public DistrictPreset preset;
    public float averageLandValue = 0f;
    public List<SocialStatistics.EthnicityFrequency> dominantEthnicities = new List<SocialStatistics.EthnicityFrequency>(); //List of dominant ethnicities

    public void Setup(DistrictPreset newPreset)
    {
        //Assign ID
        districtID = assignID;
        assignID++;
        seed = districtID + newPreset.name;
        name = "District " + districtID;
        this.transform.name = name;
        preset = newPreset;

        CityData.Instance.districtDirectory.Add(this);
    }

    public void Load(CitySaveData.DistrictCitySave data)
    {
        districtID = data.districtID;
        assignID = Mathf.Max(assignID, districtID + 1); //Make sure others won't overwrite this ID
        name = data.name;
        this.transform.name = name;
        averageLandValue = data.averageLandValue;
        dominantEthnicities = data.dominantEthnicities;

        Toolbox.Instance.LoadDataFromResources<DistrictPreset>(data.preset, out preset);

        CityData.Instance.districtDirectory.Add(this);

        //Load blocks
        foreach(CitySaveData.BlockCitySave block in data.blocks)
        {
            GameObject newBlockObj = Instantiate(PrefabControls.Instance.block, PrefabControls.Instance.cityContainer.transform);
            BlockController newBlock = newBlockObj.GetComponent<BlockController>();
            newBlock.Load(block, this);
        }
    }

    //Add a city tile to this district
    public void AddCityTile(CityTile newCityTile)
    {
        if (!cityTiles.Contains(newCityTile))
        {
            //Unassign previous district
            if (newCityTile.district != null)
            {
                newCityTile.district.cityTiles.Remove(newCityTile);
            }

            newCityTile.SetDensity((BuildingPreset.Density)Mathf.Lerp((int)preset.minimumDensity, (int)preset.maximumDensity, Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed)));
            newCityTile.SetLandVlaue((BuildingPreset.LandValue)Mathf.Lerp((int)preset.minimumLandValue, (int)preset.maximumLandValue, Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed)));

            newCityTile.district = this;
            cityTiles.Add(newCityTile);

            newCityTile.transform.SetParent(this.transform, true);
        }
    }

    //Add a block to this district
    public void AddBlock(BlockController newBlock)
    {
        if (!blocks.Contains(newBlock))
        {
            //Unassign previous district
            //if (newBlock.district != null)
            //{
            //    newBlock.district.blocks.Remove(newBlock);
            //}

            //newBlock.district = this;
            newBlock.transform.SetParent(this.transform, true); //Set parent
            blocks.Add(newBlock);

            //Assign tiles to this district
            //foreach(CityTile tile in newBlock.cityTiles)
            //{
            //    AddCityTile(tile);
            //}
        }
    }

    //Called when game map has been generated.
    public void PopulateData()
	{
		//Calculate average land value
		foreach(CityTile b in cityTiles)
		{
			averageLandValue += (int)b.landValue;
		}

		averageLandValue /= (float)cityTiles.Count;

		if (averageLandValue < 0f) averageLandValue = 0f;

        //Assign dominant ehtnicities
        int totalRatio = 0;
        dominantEthnicities.Clear();

        //Unlike citizens, this isn't random but based on the city settings ie the largest district must be dominant to the largest ethnicity %
        for (int i = 0; i < SocialStatistics.Instance.ethnicityFrequencies.Count; i++)
        {
            SocialStatistics.EthnicityFrequency freq = SocialStatistics.Instance.ethnicityFrequencies[i]; //Get city wide ethnicities

            SocialStatistics.EthnicityFrequency newFreq = new SocialStatistics.EthnicityFrequency();
            newFreq.ethnicity = freq.ethnicity;
            newFreq.frequency = freq.frequency;

            if (preset.affectEthnicity)
            {
                SocialStatistics.EthnicityFrequency modifier = preset.ethnicityFrequencyModifiers.Find(item => item.ethnicity == freq.ethnicity);

                if(modifier != null)
                {
                    newFreq.frequency += modifier.frequency;
                }
            }

            totalRatio += newFreq.frequency;
            dominantEthnicities.Add(newFreq);
        }

        //Adjust ratios
        foreach(SocialStatistics.EthnicityFrequency f in dominantEthnicities)
        {
            f.frequency = Mathf.RoundToInt(f.frequency / (float)totalRatio);
        }

        //Generate name
        UpdateName();
	}

	//Generate name
	public void UpdateName()
	{
        //Make sure name is unique
        int safetyAttempts = 99;
        bool first = true;
        string seed = CityData.Instance.seed + averageLandValue + blocks.Count + cityTiles.Count + preset.name + districtID;

        while (first || (CityData.Instance.districtDirectory.Exists(item => item != this && item.name == name) && safetyAttempts > 0))
        {
            Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed);

            if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) < preset.prefixOrSuffixChance)
            {
                //Prefix
                if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) < 0.5f)
                {
                    name = NameGenerator.Instance.GenerateName(preset.prefixList[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.prefixList.Count, seed, out seed)], 1f, preset.mainNamingList[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.mainNamingList.Count, seed, out seed)], 1f, "", 0f, seed);
                }
                //Suffix
                else
                {
                    name = NameGenerator.Instance.GenerateName("", 0f, preset.mainNamingList[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.mainNamingList.Count, seed, out seed)], 1f, preset.suffixList[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.suffixList.Count, seed, out seed)], 1f, seed);
                }
            }
            else
            {
                name = NameGenerator.Instance.GenerateName("", 0f, preset.mainNamingList[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.mainNamingList.Count, seed, out seed)], 1f, "", 0f, seed);
            }

            first = false;
            safetyAttempts--;
        }

        this.transform.name = name;
    }

    //Get ethnicity based on dominance
    public Descriptors.EthnicGroup EthnictiyBasedOnDominance()
    {
        if(dominantEthnicities.Count > 0)
        {
            float val = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed);

            float cursor = 0;

            for (int i = 0; i < dominantEthnicities.Count; i++)
            {
                SocialStatistics.EthnicityFrequency f = dominantEthnicities[i];

                if(val >= cursor && val < cursor + f.frequency)
                {
                    return f.ethnicity;
                }
            }
        }

        //Otherwise return random
        return Toolbox.Instance.RandomEthnicGroup(seed);
    }

    //Default comparer (size)
    public int CompareTo(DistrictController otherObject)
    {
        return this.cityTiles.Count.CompareTo(otherObject.cityTiles.Count);
    }

    //Create save data
    public CitySaveData.DistrictCitySave GenerateSaveData()
    {
        CitySaveData.DistrictCitySave output = new CitySaveData.DistrictCitySave();
        output.name = name;
        output.districtID = districtID;
        output.averageLandValue = averageLandValue;
        output.dominantEthnicities = dominantEthnicities;
        output.preset = preset.name;

        //Get block data as part of this
        foreach(BlockController bc in blocks)
        {
            output.blocks.Add(bc.GenerateSaveData());
        }

        return output;
    }
}
