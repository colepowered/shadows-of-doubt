﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
//using UnityEditor.PackageManager.UI;
using UnityEngine;
using UnityEngine.Animations;
using NaughtyAttributes;

public class NewFloor : Controller
{
    [Header("ID")]
    public int floorID = 0;
    public static int assignID = 0;

    [Header("Location")]
    public NewBuilding building; //Reference to the building
    public int floor = 0; //Which floor is this within the building?
    public int assignResidence = 1;

    [Header("Floor Contents")]
    public List<NewAddress> addresses = new List<NewAddress>(); //List of addresses
    public NewAddress lobbyAddress; //Default lobby address. This isn't always valid: Use GetLobbyAddress() instead!
    public NewAddress outsideAddress; //Null/outside
    public Dictionary<Vector2Int, NewTile> tileMap = new Dictionary<Vector2Int, NewTile>(); //Map of all tiles on the floor
    public Dictionary<Vector2Int, NewNode> nodeMap = new Dictionary<Vector2Int, NewNode>(); //Map of all nodes on the floor
    public List<NewWall> buildingEntrances = new List<NewWall>();
    public List<Interactable> securityDoors = new List<Interactable>();
    public bool alarmLockdown = false; //True if this floor is locked-down from alarm
    public int layoutIndex = 0; //Corresponds to the layout index in the building preset

    public int breakerSecurityID = -1;
    public int breakerDoorsID = -1;
    public int breakerLightsID = -1;
    [System.NonSerialized]
    public Interactable breakerSecurity;
    [System.NonSerialized]
    public Interactable breakerDoors;
    [System.NonSerialized]
    public Interactable breakerLights;
    public float breakerSecurityState = 1; //Breaker for security state
    public float breakerLightsState = 1; //Breaker for security state
    public float breakerDoorsState = 1; //Breaker for security state

    [Header("Details")]
    [Tooltip("The name of the floor data")]
    public string floorName = "newFloor";

    [Tooltip("The size of this configuration in 7x7 cells")]
    public Vector2 size = new Vector2(1, 1);

    [Tooltip("The default floor height (voxel units == 0.1)")]
    public int defaultFloorHeight = 0;

    [Tooltip("The default ceiling height (voxel units == 0.1)")]
    public int defaultCeilingHeight = 42; //The max ceiling height for roof ducts is 48

    [Header("Map")]
    public MapDuctsButtonController mapDucts;

    [Header("Save Data")]
    public int maxDuctExtrusion = 0; //Only accurate on generation as this is the only time it should be needed.
    private FloorSaveData saveData;

    [Header("Debug")]
    public List<NewRoom> frontWindowDebug = new List<NewRoom>();
    public List<NewRoom> rearWindowDebug = new List<NewRoom>();
    public List<NewRoom> leftWindowDebug = new List<NewRoom>();
    public List<NewRoom> rightWindowDebug = new List<NewRoom>();

    //Events
    //Case status change; open a new active case or close one
    public delegate void SaveDataComplete(NewFloor floor, FloorSaveData data);
    public event SaveDataComplete OnSaveDataComplete;

    //Setup
    public void Setup(int newFloor, NewBuilding newBuilding, string newName, Vector2 newSize, int newFloorHeight, int newCeilingHeight)
    {
        floorID = assignID;
        assignID++;
        assignResidence = 1; //Assign residence from 1...
        floor = newFloor;

        newBuilding.AddNewFloor(this);
        this.transform.SetParent(building.gameObject.transform);
        this.transform.localPosition = new Vector3(0, floor * (float)PathFinder.Instance.tileSize.z, 0);

        size = newSize;
        defaultFloorHeight = newFloorHeight;
        defaultCeilingHeight = newCeilingHeight;
        CityData.Instance.floorRange = new Vector2(Mathf.Min(CityData.Instance.floorRange.x, floor), Mathf.Max(CityData.Instance.floorRange.y, floor));
        floorName = newName;

        if (SessionData.Instance.isFloorEdit)
        {
            this.transform.name = newName;
        }
        else
        {
            this.transform.name = floorName + " (Floor " + floor + ")";
        }

        //Create default lobby & outside
        GameObject newAd = Instantiate(PrefabControls.Instance.address, this.transform);
        outsideAddress = newAd.GetComponent<NewAddress>();
        outsideAddress.Setup(this, CityControls.Instance.outsideLayoutConfig, CityControls.Instance.defaultStyle);
        outsideAddress.isOutsideAddress = true;

        GameObject newAd2 = Instantiate(PrefabControls.Instance.address, this.transform);
        lobbyAddress = newAd2.GetComponent<NewAddress>();
        lobbyAddress.Setup(this, CityControls.Instance.lobbyLayoutConfig, CityControls.Instance.defaultStyle);
        lobbyAddress.isLobbyAddress = true;

        if(SessionData.Instance.isFloorEdit)
        {
            FloorEditController.Instance.addressSelection = lobbyAddress; //Set default assign selection
        }

        //Load floor area
        //Point of origin (0, 0) is always bottom left
        for (int i = 0; i < size.x; i++)
        {
            for (int u = 0; u < size.y; u++)
            {
                //Load tile size == path map multiplier
                for (int l = 0; l < CityControls.Instance.tileMultiplier; l++)
                {
                    for (int k = 0; k < CityControls.Instance.tileMultiplier; k++)
                    {
                        Vector2Int loc = new Vector2Int((i * CityControls.Instance.tileMultiplier) + l, (u * CityControls.Instance.tileMultiplier) + k);

                        //Detect edge tile
                        bool edgeTile = false;

                        if (loc.x <= 0 || loc.y <= 0)
                        {
                            edgeTile = true;
                        }
                        else if (loc.x >= size.x * CityControls.Instance.tileMultiplier - 1 || loc.y >= size.y * CityControls.Instance.tileMultiplier - 1)
                        {
                            edgeTile = true;
                        }

                        //Does this tile exist already?
                        NewTile existingTile = null;

                        //Check against the global coord
                        Vector3Int globalCheck = new Vector3Int(building.globalTileCoords.x - Mathf.FloorToInt(CityControls.Instance.tileMultiplier * 0.5f) + loc.x, building.globalTileCoords.y - Mathf.FloorToInt(CityControls.Instance.tileMultiplier * 0.5f) + loc.y, floor);

                        //Because edge tiles already exist in-game as street tiles (on the street level at least).
                        if (PathFinder.Instance.tileMap.TryGetValue(globalCheck, out existingTile))
                        {
                            //This will have been created by the pathfinder in order to create alleys and inter-building obstacles.

                            ////Add to floor's tile map
                            //tileMap.Add(loc, existingTile);

                            //Add to floor's node map
                            foreach (NewNode node in existingTile.nodes)
                            {
                                //Get the would-be local coordinate for this node and add to the building's node map
                                Vector2Int fc = new Vector2Int(Mathf.RoundToInt(loc.x * CityControls.Instance.nodeMultiplier) + node.localTileCoord.x, Mathf.RoundToInt(loc.y * CityControls.Instance.nodeMultiplier) + node.localTileCoord.y);
                                nodeMap.Add(fc, node);
                            }
                        }
                        else
                        {
                            //Spawn tile
                            //GameObject newTileObj = Instantiate(PrefabControls.Instance.tile, this.transform);
                            //NewTile newTile = newTileObj.GetComponent<NewTile>();
                            NewTile newTile = new NewTile();

                            newTile.SetupInterior(this, loc, edgeTile);
                        }
                    }
                }
            }
        }

        if(!SessionData.Instance.isFloorEdit && !CityData.Instance.floorDirectory.Contains(this))
        {
            CityData.Instance.floorDirectory.Add(this);
        }
    }

    //Load
    public void Load(CitySaveData.FloorCitySave data, NewBuilding newBuilding)
    {
        floorID = data.floorID;
        floor = data.floor;
        building = newBuilding;
        newBuilding.AddNewFloor(this);

        this.transform.SetParent(building.gameObject.transform);
        this.transform.localPosition = new Vector3(0, floor * (float)PathFinder.Instance.tileSize.z, 0);

        size = data.size;
        defaultFloorHeight = data.defaultFloorHeight;
        defaultCeilingHeight = data.defaultCeilingHeight;
        CityData.Instance.floorRange = new Vector2(Mathf.Min(CityData.Instance.floorRange.x, floor), Mathf.Max(CityData.Instance.floorRange.y, floor));
        floorName = data.name;
        layoutIndex = data.layoutIndex;
        breakerLightsID = data.breakerLights;
        breakerSecurityID = data.breakerSec;
        breakerDoorsID = data.breakerDoors;

        //Create default lobby & outside
        GameObject newAd = Instantiate(PrefabControls.Instance.address, this.transform);
        outsideAddress = newAd.GetComponent<NewAddress>();
        outsideAddress.Setup(this, CityControls.Instance.outsideLayoutConfig, CityControls.Instance.defaultStyle);
        outsideAddress.isOutsideAddress = true;

        GameObject newAd2 = Instantiate(PrefabControls.Instance.address, this.transform);
        lobbyAddress = newAd2.GetComponent<NewAddress>();
        lobbyAddress.Setup(this, CityControls.Instance.lobbyLayoutConfig, CityControls.Instance.defaultStyle);
        lobbyAddress.isLobbyAddress = true;

        //Load floor area
        //Point of origin (0, 0) is always bottom left
        for (int i = 0; i < size.x; i++)
        {
            for (int u = 0; u < size.y; u++)
            {
                //Load tile size == path map multiplier
                for (int l = 0; l < CityControls.Instance.tileMultiplier; l++)
                {
                    for (int k = 0; k < CityControls.Instance.tileMultiplier; k++)
                    {
                        Vector2Int loc = new Vector2Int((i * CityControls.Instance.tileMultiplier) + l, (u * CityControls.Instance.tileMultiplier) + k);

                        //Detect edge tile
                        bool edgeTile = false;

                        if (loc.x <= 0 || loc.y <= 0)
                        {
                            edgeTile = true;
                        }
                        else if (loc.x >= size.x * CityControls.Instance.tileMultiplier - 1 || loc.y >= size.y * CityControls.Instance.tileMultiplier - 1)
                        {
                            edgeTile = true;
                        }

                        //Does this tile exist already?
                        NewTile existingTile = null;

                        //Check against the global coord
                        Vector3Int globalCheck = new Vector3Int(building.globalTileCoords.x - Mathf.FloorToInt(CityControls.Instance.tileMultiplier * 0.5f) + loc.x, building.globalTileCoords.y - Mathf.FloorToInt(CityControls.Instance.tileMultiplier * 0.5f) + loc.y, floor);

                        //Because edge tiles already exist in-game as street tiles (on the street level at least).
                        if (PathFinder.Instance.tileMap.TryGetValue(globalCheck, out existingTile))
                        {
                            //Add to floor's node map
                            foreach (NewNode node in existingTile.nodes)
                            {
                                //Get the would-be local coordinate for this node and add to the building's node map
                                Vector2Int fc = new Vector2Int((loc.x * CityControls.Instance.nodeMultiplier) + node.localTileCoord.x, (loc.y * CityControls.Instance.nodeMultiplier) + node.localTileCoord.y);
                                nodeMap.Add(fc, node);
                            }
                        }
                        else
                        {
                            //Spawn tile
                            //GameObject newTileObj = Instantiate(PrefabControls.Instance.tile, this.transform);
                            //NewTile newTile = newTileObj.GetComponent<NewTile>();
                            NewTile newTile = new NewTile();

                            newTile.SetupInterior(this, loc, edgeTile);
                        }
                    }
                }
            }
        }

        //This will create a new floor and load it's variables.
        //Next: Update tile data
        for (int i = 0; i < data.tiles.Count; i++)
        {
            CitySaveData.TileCitySave tile = data.tiles[i];

            NewTile currentTile = null;

            if (PathFinder.Instance.tileMap.TryGetValue(tile.globalTileCoord, out currentTile))
            {
                currentTile.LoadInterior(tile);
            }
        }

        //Next: Create addresses and nodes within them
        for (int i = 0; i < data.addresses.Count; i++)
        {
            CitySaveData.AddressCitySave add = data.addresses[i];
            NewAddress newAddress = null;

            //If this isn't a lobby or outside address, create it as an object
            if (!add.isLobbyAddress && !add.isOutsideAddress)
            {
                GameObject newAdObj = Instantiate(PrefabControls.Instance.address, this.transform);
                newAddress = newAdObj.GetComponent<NewAddress>();
            }
            else if(add.isOutsideAddress)
            {
                newAddress = outsideAddress;
            }
            else if(add.isLobbyAddress)
            {
                newAddress = lobbyAddress;
            }

            newAddress.Load(add, this);
        }

        this.transform.name = floorName + " (Floor " + floor + ")";

        if (!SessionData.Instance.isFloorEdit && !CityData.Instance.floorDirectory.Contains(this))
        {
            CityData.Instance.floorDirectory.Add(this);
        }
    }

    //Add an address
    public void AddNewAddress(NewAddress newAddress)
    {
        if (!addresses.Contains(newAddress))
        {
            //Remove existing
            if (newAddress.floor != null) newAddress.floor.RemoveAddress(newAddress);

            addresses.Add(newAddress);

            //Set location info
            newAddress.floor = this;
            newAddress.building = building;

            //Tree update
            foreach (NewRoom room in newAddress.rooms)
            {
                room.floor = this;

                foreach (NewNode node in room.nodes)
                {
                    node.floor = this;
                }
            }
        }
    }

    //Remove address
    public void RemoveAddress(NewAddress newAddress)
    {
        if (addresses.Contains(newAddress))
        {
            addresses.Remove(newAddress);

            //Set location info
            newAddress.floor = null;
            newAddress.building = null;
        }
    }

    //Generate save data for this floor and it's contents
    public void GetSaveData()
    {
        StartCoroutine(GenerateFloorSaveData());
    }

    IEnumerator GenerateFloorSaveData()
    {
        Game.Log("Gathering floor save data...");
        saveData = new FloorSaveData();
        saveData.floorName = floorName;
        saveData.size = size;
        saveData.defaultCeilingHeight = defaultCeilingHeight;
        saveData.defaultFloorHeight = defaultFloorHeight;

        //Create address data
        foreach (NewAddress ad in addresses)
        {
            //Make sure preset and editor colour are correct
            ad.saveData.p_n = ad.preset.name;
            ad.saveData.e_c = ad.editorColour;

            //Create variations (make sure we have at least one)
            while(ad.saveData.vs.Count < 0)
            {
                Game.Log("Generating new variation for " + ad.name + " as there is currently 0...");
                GenerationController.Instance.GenerateAddressLayout(ad);
                ConnectNodesOnFloor();//Connect nodes and generate cull trees

                //Create new variation
                AddressLayoutVariation newVariation = new AddressLayoutVariation();

                //Create room data
                foreach (NewRoom room in ad.rooms)
                {
                    //Skip null space room
                    //if (room == ad.nullRoom) continue;

                    RoomSaveData newRoomData = new RoomSaveData();
                    newRoomData.id = room.roomFloorID;
                    newRoomData.l = room.roomType.name;

                    //newRoomData.p = room.preset.name;

                    //for (int u = 0; u < room.openPlanElements.Count; u++)
                    //{
                    //    int index = ad.addressPreset.roomConfig.FindIndex(item => item == room.openPlanElements[u]); //Set room conifg by using index reference
                    //    newRoomData.op_e.Add(index);
                    //}

                    //Create node data
                    foreach (NewNode node in room.nodes)
                    {
                        NodeSaveData newNodeData = new NodeSaveData();
                        newNodeData.f_c = node.floorCoord;
                        //newNodeData.c_h = node.ceilingHeight;
                        newNodeData.f_h = node.floorHeight;
                        newNodeData.f_t = node.floorType;

                        if(node.forcedRoom != null)
                        {
                            newNodeData.f_r = node.forcedRoomRef;
                        }

                        //Create wall data
                        foreach (NewWall wall in node.walls)
                        {
                            WallSaveData newWallData = new WallSaveData();
                            newWallData.w_o = wall.wallOffset;
                            newWallData.p_n = wall.preset.id;

                            newNodeData.w_d.Add(newWallData);
                        }

                        //Add to base class container
                        newRoomData.n_d.Add(newNodeData);
                    }

                    newVariation.r_d.Add(newRoomData);
                }

                ad.saveData.vs.Add(newVariation);

                GenerationController.Instance.LoadGeometryFloor(this); //Load geometry

                yield return null;
            }

            //Debug: List non-standard walls saved in each var
            for (int i = 0; i < ad.saveData.vs.Count; i++)
            {
                AddressLayoutVariation asd = ad.saveData.vs[i];

                int nonDefaultWalls = 0;

                foreach(RoomSaveData rsd in asd.r_d)
                {
                    foreach (NodeSaveData nsd in rsd.n_d)
                    {
                        foreach (WallSaveData wsd in nsd.w_d)
                        {
                            if(wsd.p_n != "0")
                            {
                                nonDefaultWalls++;
                            }
                        }
                    }
                }

                Game.Log(nonDefaultWalls + " non default walls saved in address " + ad.name + " variation " + i + "...");
            }

            //Add to base class container
            saveData.a_d.Add(ad.saveData);
        }

        //Create tile data
        foreach (KeyValuePair<Vector2Int, NewTile> pair in tileMap)
        {
            TileSaveData newTileSaveData = new TileSaveData();
            newTileSaveData.f_c = pair.Value.floorCoord;
            newTileSaveData.e_r = pair.Value.elevatorRotation;
            newTileSaveData.e_l = pair.Value.isInvertedStairwell;
            newTileSaveData.i_e = pair.Value.isEntrance;
            newTileSaveData.m_e = pair.Value.isMainEntrance;
            newTileSaveData.s_t = pair.Value.isStairwell;
            newTileSaveData.s_r = pair.Value.stairwellRotation;

            saveData.t_d.Add(newTileSaveData);
        }

        //Fire event
        if(OnSaveDataComplete != null)
        {
            OnSaveDataComplete(this, saveData);
        }
    }

    //Load data from this save file to this floor
    public void LoadDataToFloor(FloorSaveData savedData)
    {
        //This will create a new floor and load it's variables.
        //Next: Update tile data
        for (int i = 0; i < savedData.t_d.Count; i++)
        {
            TileSaveData tile = savedData.t_d[i];

            //Rotate vectors
            Vector2Int facedFloorCoord = building.FaceLocalTileVector(tile.f_c);

            NewTile currentTile = null;

            if (tileMap.TryGetValue(facedFloorCoord, out currentTile))
            {
                currentTile.SetAsEntrance(tile.i_e, tile.m_e);

                currentTile.SetAsStairwell(tile.s_t, false, tile.e_l);
                currentTile.SetStairwellRotation((tile.s_r + building.rotations * -90));
            }
        }

        //Next: Create addresses and nodes within them
        for (int i = 0; i < savedData.a_d.Count; i++)
        {
            AddressSaveData add = savedData.a_d[i];
            NewAddress currentAdd = null;

            //The first one will be outside
            if (i == 0)
            {
                currentAdd = outsideAddress;
            }
            //The second will be the default address (lobby)
            else if (i == 1)
            {
                //Remove old lobby
                NewAddress oldLobby = lobbyAddress;

                //Get the preset
                LayoutConfiguration layoutPreset = null;

                if (Toolbox.Instance.LoadDataFromResources<LayoutConfiguration>(add.p_n, out layoutPreset))
                {
                    lobbyAddress = CreateNewAddress(layoutPreset, CityControls.Instance.defaultStyle);
                }

                currentAdd = lobbyAddress;

                if(oldLobby != null && SessionData.Instance.isFloorEdit)
                {
                    int oldIndex = FloorEditController.Instance.editFloor.addresses.IndexOf(oldLobby);

                    //Set all nodes in this address to default address
                    for (int u = 0; u < oldLobby.nodes.Count; u++)
                    {
                        lobbyAddress.AddNewNode(oldLobby.nodes[u]);
                        u--;
                    }

                    //Remove the address object completely
                    Destroy(oldLobby.gameObject);

                    //Once the nodes are removed, remove the address entry
                    FloorEditController.Instance.editFloor.addresses.RemoveAt(oldIndex);
                }
            }
            //Others will need to be created...
            else
            {
                //Get the preset
                LayoutConfiguration layoutPreset = null;

                if (Toolbox.Instance.LoadDataFromResources<LayoutConfiguration>(add.p_n, out layoutPreset))
                {
                    currentAdd = CreateNewAddress(layoutPreset, CityControls.Instance.defaultStyle);
                }
            }

            if(SessionData.Instance.isFloorEdit)
            {
                currentAdd.editorColour = add.e_c;
            }

            currentAdd.saveData = add;

            //Load nodes
            //Cycle saved node data inside address...
            //Pick a variation
            if (add.vs.Count > 0)
            {
                int varIndex = 0;
                if (!SessionData.Instance.isFloorEdit) varIndex = Toolbox.Instance.GetPsuedoRandomNumber(0, add.vs.Count, currentAdd.seed);

                LoadVariation(currentAdd, add.vs[varIndex]);
            }
        }

        FinalizeLoadingIn();

        floorName = savedData.floorName;
        this.transform.name = floorName;
    }

    public void LoadVariation(NewAddress currentAdd, AddressLayoutVariation newVar)
    {
        currentAdd.loadedVarIndex = currentAdd.saveData.vs.IndexOf(newVar);

        if(currentAdd.loadedVarIndex > -1)
        {
            foreach (RoomSaveData room in newVar.r_d)
            {
                //-2 means use the null space room
                NewRoom newRoom = currentAdd.nullRoom;

                if (room.l != null && room.l.Length > 0 && currentAdd.preset != CityControls.Instance.outsideLayoutConfig && room.n_d.Count > 0)
                {
                    //Get the preset
                    RoomTypePreset config = null;

                    Toolbox.Instance.LoadDataFromResources<RoomTypePreset>(room.l, out config);

                    if (config != null)
                    {
                        //Create new room
                        GameObject newObj = Instantiate(PrefabControls.Instance.room, currentAdd.transform);
                        newRoom = newObj.GetComponent<NewRoom>();

                        newRoom.SetupLayoutOnly(currentAdd, config, room.id);

                        //Game.Log("Created new room: " + newRoom.name + " " + room.id);
                    }
                }

                //Add open plan elements
                //for (int u = 0; u < room.op_e.Count; u++)
                //{
                //    RoomConfiguration openPlanElement = currentAdd.addressPreset.roomConfig[room.op_e[u]];
                //    newRoom.AddOpenPlanElement(openPlanElement);
                //}

                //Add nodes
                foreach (NodeSaveData node in room.n_d)
                {
                    //Find the existing node data
                    NewNode existingNode = null;

                    //Rotate vectors
                    Vector2Int facedNodeCoord = building.FaceLocalNodeVector(node.f_c);

                    if (nodeMap.TryGetValue(facedNodeCoord, out existingNode))
                    {
                        //Skip if edge node
                        if (existingNode.tile.isEdge) continue;

                        bool forcedRoom = false;
                        if (node.f_r != null && node.f_r.Length > 0) forcedRoom = true;

                        //Set node data (this will be refreshed later with geometry update)
                        existingNode.floorHeight = node.f_h;
                        //existingNode.ceilingHeight = node.c_h;

                        //Set floor type
                        existingNode.SetFloorType(node.f_t);

                        //Add this node to the address
                        currentAdd.AddNewNode(existingNode);

                        //Add this node to the room
                        newRoom.AddNewNode(existingNode);

                        //Load forced room
                        if (forcedRoom)
                        {
                            existingNode.forcedRoomRef = node.f_r;

                            string[] layoutNames = existingNode.forcedRoomRef.Split('.');

                            string lastElement = layoutNames[layoutNames.Length - 1];

                            //Get the preset
                            RoomConfiguration forcedRoomPreset = null;

                            if (Toolbox.Instance.LoadDataFromResources<RoomConfiguration>(lastElement, out forcedRoomPreset))
                            {
                                existingNode.SetForcedRoom(forcedRoomPreset);
                            }

                            //Game.Log("Loading a forced room from floordata " + existingNode.gameLocation.name + " with preset " + forcedRoomPreset.name + " and preset id " + room.p_i + " adding to room of " + newRoom.preset.name + " id: " + newRoom.roomID);
                        }
                    }
                    //else Game.Log("Cannot find existing node");
                }
            }
        }
        else
        {
            Game.Log("Unable to find this variation in floor save data: " + currentAdd.loadedVarIndex + "/" + currentAdd.saveData.vs.Count);
        }
    }

    public void FinalizeLoadingIn()
    {
        //Walls will need to be in place before updaing their saved preferences...
        GenerationController.Instance.UpdateFloorCeilingFloor(this);
        GenerationController.Instance.UpdateWallsFloor(this);

        //Next: load saved door pairings: This should be compatible with outside the address
        foreach (NewAddress add in addresses)
        {
            if(add.loadedVarIndex > -1)
            {
                AddressLayoutVariation getVar = null;

                try
                {
                    getVar = add.saveData.vs[add.loadedVarIndex];
                }
                catch
                {
                    Game.Log(add.name + ": Unable to find this variation in floor save data: " + add.loadedVarIndex);
                }

                if(getVar != null)
                {
                    int nonDefaultWalls = 0;

                    foreach (RoomSaveData room in getVar.r_d)
                    {
                        foreach (NodeSaveData node in room.n_d)
                        {
                            //Rotate vectors
                            Vector2Int facedNodeCoord = building.FaceLocalNodeVector(node.f_c);

                            //Find the existing node data
                            NewNode existingNode = null;

                            if (nodeMap.TryGetValue(facedNodeCoord, out existingNode))
                            {
                                if (existingNode.tile.isEdge) continue;

                                foreach (WallSaveData wall in node.w_d)
                                {
                                    //Faced wall offset
                                    Vector2 facedWallOffset = building.FaceWallOffsetVector(wall.w_o);

                                    //Find the wall with the correct offset
                                    NewWall existingWall = existingNode.walls.Find(item => item.wallOffset == facedWallOffset);

                                    if (existingWall != null)
                                    {
                                        //Get the preset
                                        DoorPairPreset doorPreset = null;

                                        if (Toolbox.Instance.LoadDataFromResources<DoorPairPreset>(wall.p_n, out doorPreset))
                                        {
                                            existingWall.SetDoorPairPreset(doorPreset, enableUpdate: false);

                                            //Is this an entrance?
                                            if (doorPreset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
                                            {
                                                //Add entrances references
                                                if (existingWall.parentWall != null && existingWall.parentWall.node.gameLocation != null)
                                                {
                                                    existingWall.parentWall.node.gameLocation.AddEntrance(existingWall.parentWall.node, existingWall.childWall.node);
                                                    existingWall.childWall.node.gameLocation.AddEntrance(existingWall.childWall.node, existingWall.parentWall.node);
                                                }

                                                //Is this an external entrance? Outside should be child...
                                                if (existingWall.otherWall != null)
                                                {
                                                    //Inside facing out...
                                                    if (existingWall.node.building != null && existingWall.otherWall.node.building == null)
                                                    {
                                                        if (existingWall.node.tile.isMainEntrance)
                                                        {
                                                            existingWall.node.building.AddBuildingEntrance(existingWall, true);
                                                        }
                                                        else if (existingWall.node.tile.isEntrance)
                                                        {
                                                            existingWall.node.building.AddBuildingEntrance(existingWall, false);
                                                        }
                                                    }
                                                    //Outside facing in...
                                                    else if (existingWall.node.building == null && existingWall.otherWall.node.building != null)
                                                    {
                                                        if (existingWall.otherWall.node.tile.isMainEntrance)
                                                        {
                                                            existingWall.otherWall.node.building.AddBuildingEntrance(existingWall.otherWall, true);
                                                        }
                                                        else if (existingWall.otherWall.node.tile.isEntrance)
                                                        {
                                                            existingWall.otherWall.node.building.AddBuildingEntrance(existingWall.otherWall, false);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else Game.Log("Cannot find door pair " + wall.p_n);
                                    }
                                    else Game.Log("Cannot find existing wall for wall pair");

                                    if(SessionData.Instance.isFloorEdit)
                                    {
                                        if(wall.p_n != "0")
                                        {
                                            nonDefaultWalls++;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //Game.Log(nonDefaultWalls + " non default walls loaded for variation " + add.loadedVarIndex + " of " + add.name);

                    //Remove save data reference
                    if(!SessionData.Instance.isFloorEdit)
                    {
                        add.saveData = null;
                    }
                }
            }
            else
            {
                if(add.nodes.Count > 0) Game.Log(add.name + " (" + add.id + ") Unable to find this variation in floor save data: " + add.loadedVarIndex);
            }
        }

        //All relevent data should be loaded now!
        GenerationController.Instance.UpdateGeometryFloor(this, "NewFloor");
    }

    //Create a new address for this floor
    public NewAddress CreateNewAddress(LayoutConfiguration newRoomConfig, DesignStylePreset newDesign)
    {
        GameObject newAd = Instantiate(PrefabControls.Instance.address, this.transform);
        NewAddress newAddress = newAd.GetComponent<NewAddress>();
        newAddress.Setup(this, newRoomConfig, newDesign);

        //Force refresh
        if (SessionData.Instance.isFloorEdit)
        {
            FloorEditController.Instance.SetTool(FloorEditController.FloorEditTool.addressDesignation, true);

            //Select newly created address
            FloorEditController.Instance.addressDropdown.value = FloorEditController.Instance.addressDropdown.options.Count - 1;
        }

        return newAddress;
    }

    //Connect nodes on the floor and generate cull trees: Editor only
    public void ConnectNodesOnFloor()
    {
        if (!SessionData.Instance.isFloorEdit) return;

        //Clear address entrances (start afresh)
        foreach (NewAddress add in addresses)
        {
            add.entrances.Clear();
        }

        //Connect nodes inside room (& entrances)
        foreach (NewAddress add in addresses)
        {
            if (add == outsideAddress) continue;

            foreach(NewRoom room in add.rooms)
            {
                room.ConnectNodes();
            }
        }

        //Generate culling tree
        if(!SessionData.Instance.isFloorEdit)
        {
            foreach (NewAddress add in addresses)
            {
                foreach (NewRoom room in add.rooms)
                {
                    room.GenerateCullingTree();
                }
            }
        }
    }

    //Assign window UV data to windows on this floor: Must be done after all walls are spawned and presets set
    public void AssignWindowUVData(bool debug = false)
    {
        frontWindowDebug.Clear();
        rearWindowDebug.Clear();
        leftWindowDebug.Clear();
        rightWindowDebug.Clear();

        List<NewWall> windowedWalls = new List<NewWall>();

        //Get all windowed walls on this floor...
        foreach(NewAddress add in addresses)
        {
            if (add.isOutside) continue;

            if (!add.generatedRoomConfigs) add.GenerateRoomConfigs();

            foreach(NewRoom room in add.rooms)
            {
                if(room.preset == null)
                {
                    Game.LogError("Room " + room.roomType.name + " in " + room.building.preset.name + " has no preset...");
                    continue;
                }

                if (room.IsOutside()) continue;

                foreach(NewNode node in room.nodes)
                {
                    if (node.floorType == NewNode.FloorTileType.none) continue;

                    foreach(NewWall w in node.walls)
                    {
                        if(w.preset.sectionClass == DoorPairPreset.WallSectionClass.window || w.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge)
                        {
                            //Detect outside window only
                            if(w.node.room.IsOutside() != w.otherWall.node.room.IsOutside())
                            {
                                room.windows.Add(w);
                            }
                            else if(w.node.gameLocation.isOutside != w.otherWall.node.gameLocation.isOutside)
                            {
                                room.windows.Add(w);
                            }
                            else if (w.otherWall.node.room.preset != null && w.otherWall.node.room.preset.forceOutside == RoomConfiguration.OutsideSetting.forceOutside)
                            {
                                room.windows.Add(w);
                            }

                            //Used for culling checks...
                            if (floor > 0 && !w.node.room.IsOutside() && !w.node.room.isNullRoom && (w.otherWall.node.room.IsOutside() || (w.otherWall.node.room.preset != null && w.otherWall.node.room.preset.forceOutside == RoomConfiguration.OutsideSetting.forceOutside)))
                            {
                                if(!windowedWalls.Contains(w))
                                {
                                    windowedWalls.Add(w);
                                }
                            }
                        }
                    }
                }

                if (floor > 0)
                {
                    room.UpdateEmissionEndOfFrame(); //Force update of emission textures
                }
            }
        }

        //Dont do this for ground floor as we don't need emission data...
        if (floor <= 0) return;

        //Get window faces...
        List<NewWall> frontWindows = new List<NewWall>();
        List<NewWall> rearWindows = new List<NewWall>();
        List<NewWall> leftWindows = new List<NewWall>();
        List<NewWall> rightWindows = new List<NewWall>();

        foreach(NewWall ww in windowedWalls)
        {
            Vector2 rotatedOffset = ww.node.building.GetOriginalWallOffset(ww.wallOffset);
            //ww.debugRotatedOffset = rotatedOffset;

            if(debug)
            {
                Game.Log(ww.wallOffset + " = " + rotatedOffset + " (" + ww.node.building.rotations + ")");
            }

            if (rotatedOffset.x < -0.1f)
            {
                rightWindows.Add(ww);
                rightWindowDebug.Add(ww.node.room);
            }
            else if (rotatedOffset.x > 0.1f)
            {
                leftWindows.Add(ww);
                leftWindowDebug.Add(ww.node.room);
            }
            else if (rotatedOffset.y < -0.1f)
            {
                frontWindows.Add(ww);
                frontWindowDebug.Add(ww.node.room);
            }
            else if (rotatedOffset.y > 0.1f)
            {
                rearWindows.Add(ww);
                rearWindowDebug.Add(ww.node.room);
            }
        }

        //Order window by from left side: (p2 first = highest first)
        frontWindows.Sort((p1, p2) => building.GetOriginalWallOffset(p1.node.floorCoord).x.CompareTo(building.GetOriginalWallOffset(p2.node.floorCoord).x));

        rearWindows.Sort((p1, p2) => building.GetOriginalWallOffset(p2.node.floorCoord).x.CompareTo(building.GetOriginalWallOffset(p1.node.floorCoord).x));

        rightWindows.Sort((p1, p2) => building.GetOriginalWallOffset(p2.node.floorCoord).y.CompareTo(building.GetOriginalWallOffset(p1.node.floorCoord).y));

        leftWindows.Sort((p1, p2) => building.GetOriginalWallOffset(p1.node.floorCoord).y.CompareTo(building.GetOriginalWallOffset(p2.node.floorCoord).y));

        if(debug)
        {
            foreach(NewWall ww in frontWindows)
            {
                Game.Log("Sort front X highest first: " + ww.node.building.GetOriginalWallOffset(ww.node.floorCoord));
            }

            foreach (NewWall ww in rearWindows)
            {
                Game.Log("Sort rear X lowest first: " + ww.node.building.GetOriginalWallOffset(ww.node.floorCoord));
            }

            foreach (NewWall ww in rightWindows)
            {
                Game.Log("Sort right Y lowest first: " + ww.node.building.GetOriginalWallOffset(ww.node.floorCoord));
            }

            foreach (NewWall ww in leftWindows)
            {
                Game.Log("Sort left Y highest first: " + ww.node.building.GetOriginalWallOffset(ww.node.floorCoord));
            }
        }

        //These should now be sorted and ready to assign...
        BuildingPreset.WindowUVFloor correctFloor = null;

        try
        {
            correctFloor = building.preset.sortedWindows[floor - 1];
        }
        catch
        {
            //Game.Log("CityGen: Unable to find UV data for floor " + (floor - 1) + " on building " + building.preset.name);
            return;
        }

        for (int i = 0; i < frontWindows.Count; i++)
        {
            frontWindows[i].windowUVHorizonalPosition = i;
            frontWindows[i].windowUV = correctFloor.front.Find(item => item.horizonal == i);
            if (frontWindows[i].windowUV != null)
            {
                frontWindows[i].node.room.windowsWithUVData.Add(frontWindows[i]);
            }
            //frontWindows[i].debugUVHorziontal = frontWindows;
        }

        for (int i = 0; i < rearWindows.Count; i++)
        {
            rearWindows[i].windowUVHorizonalPosition = i;
            rearWindows[i].windowUV = correctFloor.back.Find(item => item.horizonal == i);
            if (rearWindows[i].windowUV != null) rearWindows[i].node.room.windowsWithUVData.Add(rearWindows[i]);
            //rearWindows[i].debugUVHorziontal = rearWindows;
        }

        for (int i = 0; i < leftWindows.Count; i++)
        {
            leftWindows[i].windowUVHorizonalPosition = i;
            leftWindows[i].windowUV = correctFloor.left.Find(item => item.horizonal == i);
            if (leftWindows[i].windowUV != null) leftWindows[i].node.room.windowsWithUVData.Add(leftWindows[i]);
            //leftWindows[i].debugUVHorziontal = leftWindows;
        }

        for (int i = 0; i < rightWindows.Count; i++)
        {
            rightWindows[i].windowUVHorizonalPosition = i;
            rightWindows[i].windowUV = correctFloor.right.Find(item => item.horizonal == i);
            if (rightWindows[i].windowUV != null) rightWindows[i].node.room.windowsWithUVData.Add(rightWindows[i]);
            //rightWindows[i].debugUVHorziontal = rightWindows;
        }

        //Window UV data should now be assigned correctly
    }

    //Generate air duct system
    public void GenerateAirDucts()
    {
        //For each address, pick the air vent locations. This will give us points that need linking to...
        foreach(NewAddress ad in addresses)
        {
            ad.SelectAirVentLocations();
        }
    }

    //Add security door
    public void AddSecurityDoor(Interactable newInteractable)
    {
        securityDoors.Add(newInteractable);
    }

    //Get valid lobby address
    public NewAddress GetLobbyAddress()
    {
        List<NewAddress> lobbies = addresses.FindAll(item => item.isLobby && item.nodes.Count > 0);
        return lobbies[Toolbox.Instance.Rand(0, lobbies.Count)];
    }

    //Set alarm lockdown
    public void SetAlarmLockdown(bool newVal, NewAddress addressOnly = null)
    {
        if(newVal != alarmLockdown)
        {
            alarmLockdown = newVal;

            Game.Log("Debug: Locking down floor: " + alarmLockdown + " " + floor + " by closing/opening " + securityDoors.Count + " doors...");

            foreach(Interactable i in securityDoors)
            {
                Game.Log("Debug: Door at " + i.node.gameLocation.name + " (address only: " + addressOnly + ")");

                if (addressOnly == null || addressOnly == i.node.gameLocation)
                {
                    Game.Log("Debug: Door " + i.id + ": " + (!alarmLockdown));
                    i.SetSwitchState(!alarmLockdown, null);

                    if (i.node.gameLocation.thisAsAddress != null)
                    {
                        Interactable breaker = i.node.gameLocation.thisAsAddress.GetBreakerDoors();

                        if (breaker != null)
                        {
                            breaker.SetSwitchState(!alarmLockdown, null); //Set to the opposite of alarm lockdown: closed = doors locked down
                            Game.Log("Debug: Set breaker: " + alarmLockdown);
                        }
                    }
                }
            }
        }
    }

    //Create save data
    public CitySaveData.FloorCitySave GenerateSaveData()
    {
        CitySaveData.FloorCitySave output = new CitySaveData.FloorCitySave();

        output.name = floorName;
        output.floorID = floorID;
        output.floor = floor;
        output.size = size;
        output.defaultFloorHeight = defaultFloorHeight;
        output.defaultCeilingHeight = defaultCeilingHeight;
        output.layoutIndex = layoutIndex;

        output.breakerSec = breakerSecurityID;
        output.breakerLights = breakerLightsID;
        output.breakerDoors = breakerDoorsID;

        //Get addresses
        foreach (NewAddress ad in addresses)
        {
            output.addresses.Add(ad.GenerateSaveData());
        }

        //Get tiles
        foreach (KeyValuePair<Vector2Int, NewTile> pair in tileMap)
        {
            output.tiles.Add(pair.Value.GenerateSaveData());
        }

        return output;
    }

    [Button]
    public void DebugWindowUVAssign()
    {
        AssignWindowUVData(true);
    }

    public void SetBreakerSecurity(Interactable newObject)
    {
        breakerSecurity = newObject;
        if (breakerSecurity != null) breakerSecurityID = breakerSecurity.id;
    }

    public void SetBreakerLights(Interactable newObject)
    {
        breakerLights = newObject;
        if (breakerLights != null) breakerLightsID = breakerLights.id;
    }

    public void SetBreakerDoors(Interactable newObject)
    {
        breakerDoors = newObject;
        if (breakerDoors != null) breakerDoorsID = breakerDoors.id;
    }

    public Interactable GetBreakerSecurity()
    {
        if (breakerSecurity != null) return breakerSecurity;
        else if (breakerSecurityID > -1)
        {
            breakerSecurity = CityData.Instance.interactableDirectory.Find(item => item.id == breakerSecurityID);
        }

        if (breakerSecurity != null) return breakerSecurity;

        return null;
    }

    public Interactable GetBreakerLights()
    {
        if (breakerLights != null) return breakerLights;
        else if (breakerLightsID > -1)
        {
            breakerLights = CityData.Instance.interactableDirectory.Find(item => item.id == breakerLightsID);
        }

        if (breakerLights != null) return breakerLights;

        return null;
    }

    public Interactable GetBreakerDoors()
    {
        if (breakerDoors != null) return breakerDoors;
        else if (breakerDoorsID > -1)
        {
            breakerDoors = CityData.Instance.interactableDirectory.Find(item => item.id == breakerDoorsID);
        }

        if (breakerDoors != null) return breakerDoors;

        return null;
    }
}