﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactWorkHours : Fact
{
    //Constructor
    public FactWorkHours(FactPreset newPreset, List<Evidence> newFromEvidence, List<Evidence> newToEvidence, List<object> newPassedObjects, List<Evidence.DataKey> overrideFromKeys, List<Evidence.DataKey> overrideToKeys, bool isCustomFact) : base(newPreset, newFromEvidence, newToEvidence, newPassedObjects, overrideFromKeys, overrideToKeys, isCustomFact)
    {

    }

    //Get name from the connecting evidence. Optional: Get name for a specific link
    public override string GetName(Evidence.FactLink specificLink = null)
    {
        string ret = string.Empty;

        //Get from names
        if (specificLink == null || !specificLink.thisIsTheFromEvidence)
        {
            for (int i = 0; i < fromEvidence.Count; i++)
            {
                Evidence fromEv = fromEvidence[i];

                if (i > 0) ret += " & ";
                ret += fromEv.GetNameForDataKey(fromDataKeys);
            }
        }
        else
        {
            ret += specificLink.thisEvidence.GetNameForDataKey(fromDataKeys);
        }

        ret += " " + Strings.Get("evidence.generic", preset.name) + " ";

        //Get hours
        ret += (fromEvidence[0] as EvidenceCitizen).witnessController.job.GetWorkingHoursString() + FoundAtName() + GenerateNameSuffix();

        //Trim extra whitespace
        ret = ret.Trim();

        return ret;
    }
}
