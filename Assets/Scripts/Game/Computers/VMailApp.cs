﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Text;

public class VMailApp : CruncherAppContent
{
    public ComputerOSMultiSelect vmailList;
    public ComputerOSMultiSelectElement selectedVmail;
    public TextMeshProUGUI vmailHeaderText;
    public TextMeshProUGUI vmailBodyText;
    public Button nextPageButton;
    public Button prevPageButton;

    public Human emailSender;
    public Human emailReciever;
    public string emailTextContent;
    DDSSaveClasses.DDSTreeSave tree;
    StateSaveData.MessageThreadSave thread;
    int msgIndex;

    //Used for properly parsing vmails
    public class VmailParsingData
    {
        public StateSaveData.MessageThreadSave thread;
        public int messageIndex;
    }

    public override void OnSetup()
    {
        base.OnSetup();

        //Grab emails
        if(controller.loggedInAs != null)
        {
            Game.Log("Player: Retrieving " + controller.loggedInAs.messageThreadFeatures.Count + " message threads...");

            List<StateSaveData.MessageThreadSave> featured = new List<StateSaveData.MessageThreadSave>();

            //Make sure we have 1 copy of each thread...
            foreach(StateSaveData.MessageThreadSave msg in controller.loggedInAs.messageThreadFeatures)
            {
                if(!featured.Contains(msg))
                {
                    //Check for disabled inbox
                    DDSSaveClasses.DDSTreeSave tree = Toolbox.Instance.allDDSTrees[msg.treeID];

                    if (msg.participantA == controller.loggedInAs.humanID && tree.participantA != null && tree.participantA.disableInbox) continue;
                    if (msg.participantB == controller.loggedInAs.humanID && tree.participantB != null && tree.participantB.disableInbox) continue;
                    if (msg.participantC == controller.loggedInAs.humanID && tree.participantC != null && tree.participantC.disableInbox) continue;
                    if (msg.participantD == controller.loggedInAs.humanID && tree.participantD != null && tree.participantD.disableInbox) continue;

                    featured.Add(msg);
                }
            }

            foreach (StateSaveData.MessageThreadSave msg in controller.loggedInAs.messageThreadCCd)
            {
                if (!featured.Contains(msg))
                {
                    //Check for disabled inbox
                    DDSSaveClasses.DDSTreeSave tree = Toolbox.Instance.allDDSTrees[msg.treeID];

                    if (msg.participantA == controller.loggedInAs.humanID && tree.participantA != null && tree.participantA.disableInbox) continue;
                    if (msg.participantB == controller.loggedInAs.humanID && tree.participantB != null && tree.participantB.disableInbox) continue;
                    if (msg.participantC == controller.loggedInAs.humanID && tree.participantC != null && tree.participantC.disableInbox) continue;
                    if (msg.participantD == controller.loggedInAs.humanID && tree.participantD != null && tree.participantD.disableInbox) continue;

                    featured.Add(msg);
                }
            }

            try
            {
                featured.Sort((p2, p1) => p1.timestamps[p1.timestamps.Count - 1].CompareTo(p2.timestamps[p2.timestamps.Count - 1])); //Rank by latest email first
            }
            catch
            {

            }

            //Refresh multi selection
            List<ComputerOSMultiSelect.OSMultiOption> newOptions = new List<ComputerOSMultiSelect.OSMultiOption>();

            //Users are owners of the furniture
            foreach (StateSaveData.MessageThreadSave msg in featured)
            {
                //The vmail title is the first block of the first message
                DDSSaveClasses.DDSTreeSave tree = Toolbox.Instance.allDDSTrees[msg.treeID];

                //Get AQ
                Acquaintance aq = null;
                Human reciever = null;
                if (msg.recievers == null) msg.recievers = new List<int>();

                if(msg.recievers.Count > 0)
                {
                    CityData.Instance.GetHuman(msg.recievers[0], out reciever);
                }

                Human partA = null;

                if(CityData.Instance.GetHuman(msg.participantA, out partA))
                {
                    partA.FindAcquaintanceExists(reciever, out aq);
                }

                if (msg.messages == null || msg.messages.Count <= 0)
                {
                    Game.Log("Trying to retrieve vmail thread with no messages...");
                    continue;
                }

                DDSSaveClasses.DDSMessageSettings messageSetting = tree.messageRef[msg.messages[0]];
                Game.Log("Player: Retrieving " + messageSetting.msgID + "...");

                //Gather list of things to say from this message
                List<string> say = controller.loggedInAs.ParseDDSMessage(messageSetting.msgID, aq, out _);

                //Create messages by newest first
                for (int i = msg.messages.Count - 1; i >= 0; i--)
                {
                    string fromName = string.Empty;
                    Human from = Strings.GetVmailSender(msg, i, out fromName);

                    string title = Strings.Get("dds.blocks", say[0], useGenderReference: true, genderReference: from);
                    if (i > 0) title = Strings.Get("computer", "RE") + ": " + title; //Add Reply if this is not the original message
                    if (i != msg.messages.Count - 1) title = ">" + title;

                    //Add user and timestamp on second row

                    //if (from == controller.loggedInAs)
                    //{
                    //    title += "\n<alpha=#AA>" + controller.loggedInAs.initialledName;
                    //}
                    //else
                    //{
                    //    title += "\n<alpha=#AA>" + fromName;
                    //}

                    title += "\n<alpha=#AA>" + fromName;

                    ComputerOSMultiSelect.OSMultiOption newOption = new ComputerOSMultiSelect.OSMultiOption { text = title, msgThread = msg, msgIndex = i };
                    newOptions.Add(newOption);
                }
            }

            vmailList.UpdateElements(newOptions);
        }
        else
        {
            Game.Log("Player: Nobody is logged in on this machine.");
        }

        SetSelectedVmail(null);

        vmailList.OnNewSelection += OnUpdatedSelection;
    }

    private void OnDestroy()
    {
        vmailList.OnNewSelection -= OnUpdatedSelection;
    }

    public void OnUpdatedSelection()
    {
        SetSelectedVmail(vmailList.selected);
    }

    public void SetSelectedVmail(ComputerOSMultiSelectElement newSelection)
    {
        selectedVmail = newSelection;
        vmailBodyText.pageToDisplay = 0;

        if (selectedVmail == null)
        {
            vmailHeaderText.text = string.Empty;
            vmailBodyText.text = string.Empty;
        }
        else
        {
            thread = newSelection.option.msgThread;
            msgIndex = newSelection.option.msgIndex;

            string reciverString = string.Empty;
            string senderString = string.Empty;

            emailSender = Strings.GetVmailSender(thread, msgIndex, out senderString);
            emailReciever = Strings.GetVmailReciever(thread, msgIndex, out reciverString);

            //if (emailSender == null)
            //{
            //    emailSender = Player.Instance;
            //}

            //if (emailReciever == null)
            //{
            //    emailReciever = Player.Instance;
            //}

            vmailHeaderText.text = Strings.Get("computer", "To") + ": " + reciverString
                + "\n" + Strings.Get("computer", "From") + ": " + senderString
                + "\n" + SessionData.Instance.GameTimeToClock12String(thread.timestamps[msgIndex], false) + "\n" + SessionData.Instance.LongDateString(thread.timestamps[msgIndex], true, true, true, true, true, true, false, false);

            //Comile message
            //The vmail title is the first block of the first message
            tree = Toolbox.Instance.allDDSTrees[selectedVmail.option.msgThread.treeID];

            ////Get AQ
            //Acquaintance aq = null;
            //if(emailSender != null) emailSender.FindAcquaintanceExists(emailReciever, out aq);

            ////Gather list of things to say from this message
            //List<string> say = emailSender.ParseDDSMessage(tree.messageRef[selectedVmail.option.msgThread.messages[msgIndex]], aq);

            //StringBuilder sb = new StringBuilder();

            ////The first message should skip the first block (subject)
            //int startBlock = 1;
            //if (msgIndex > 0) startBlock = 0;

            //for (int i = startBlock; i < say.Count; i++)
            //{
            //    Dictionary<string, EvidenceLinkData> source = new Dictionary<string, EvidenceLinkData>();

            //    if(emailReciever != null) source = emailReciever.dataSource;

            //    //Override the data source...
            //    if(selectedVmail.option.msgThread.ds == StateSaveData.CustomDataSource.groupID)
            //    {
            //        GroupsController.SocialGroup group = GroupsController.Instance.groups.Find(item => item.id == selectedVmail.option.msgThread.dsID);
            //        if (group != null && group.dataSource != null) source = group.dataSource;
            //    }

            //    string getText = Strings.ComposeText(Strings.Get("dds.blocks", say[i]), ref source, null, useLinks: false, talking: emailSender, talkingTo: emailReciever);

            //    //Add linebreak
            //    if(i < say.Count - 1)
            //    {
            //        getText += "\n\n";
            //    }

            //    sb.Append(getText);
            //}

            //emailTextContent = sb.ToString();

            Human a = null;
            Human b = null;
            Human c = null;
            Human d = null;

            CityData.Instance.GetHuman(thread.participantA, out a);
            CityData.Instance.GetHuman(thread.participantB, out b);
            CityData.Instance.GetHuman(thread.participantC, out c);
            CityData.Instance.GetHuman(thread.participantD, out d);

            StateSaveData.MessageThreadSave threadSave = selectedVmail.option.msgThread;

            vmailBodyText.text = Strings.GetTextForComponent(tree.messageRef[threadSave.messages[msgIndex]].msgID, new VmailParsingData { thread = thread, messageIndex = msgIndex }, emailSender, emailReciever, lineBreaks: "\n\n", skipFirstBlock: true);
        }
    }

    public void NextButton()
    {
        vmailBodyText.pageToDisplay++;
    }

    public void PrevButton()
    {
        vmailBodyText.pageToDisplay--;
    }

    public void ExitButton()
    {
        controller.OnAppExit();
    }

    public override void PrintButton()
    {
        if (controller.printedDocument == null && selectedVmail != null && controller.printTimer <= 0f && tree != null)
        {
            controller.SetTimedLoading(Toolbox.Instance.Rand(0.5f, 1f));

            controller.printTimer = 1f;
            controller.printerParent.localPosition = new Vector3(controller.printerParent.localPosition.x, controller.printerParent.localPosition.y, -0.05f); //Move printer paren back

            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerPrint, Player.Instance, controller.ic.interactable.node, controller.ic.interactable.wPos);

            Human from = null;
            CityData.Instance.GetHuman(selectedVmail.option.msgThread.participantA, out from);
            Human to = null;
            CityData.Instance.GetHuman(selectedVmail.option.msgThread.participantB, out to);

            //We need to pass the message thread ID...
            try
            {
                List<Interactable.Passed> newPassed = new List<Interactable.Passed>();

                Interactable.Passed passedID = new Interactable.Passed(Interactable.PassedVarType.vmailThreadID, selectedVmail.option.msgThread.threadID);
                newPassed.Add(passedID);

                Interactable.Passed passedInex = new Interactable.Passed(Interactable.PassedVarType.vmailThreadMsgIndex, msgIndex);
                newPassed.Add(passedInex);

                Game.Log("Print vmail: " + selectedVmail.option.msgThread.threadID);

                controller.printedDocument = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.vmailPrintout, Player.Instance, to, from, controller.printerParent.position, controller.ic.transform.eulerAngles, newPassed, null, ddsOverride: selectedVmail.option.msgThread.treeID);
                if (controller.printedDocument != null) controller.printedDocument.MarkAsTrash(true);

                //Listen for on remove...
                controller.printedDocument.OnRemovedFromWorld += OnPlayerTakePrint;
            }
            catch
            {
                Game.LogError("Unable to print message index");
            }
        }
        else AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerInvalidPasscode, Player.Instance, controller.ic.interactable.node, controller.ic.interactable.wPos);
    }
}
