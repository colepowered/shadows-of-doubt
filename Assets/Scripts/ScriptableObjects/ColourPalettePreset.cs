﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "colourpalette_data", menuName = "Database/Colour Palette")]

public class ColourPalettePreset : SoCustomComparison
{
	[System.Serializable]
	public class MaterialSettings
	{
		public Color colour;
		[Range(1, 5)]
		public int weighting = 3;
	}

	[Header("Colours")]
	[ReorderableList]
	public List<MaterialSettings> colours = new List<MaterialSettings>();

	[Header("Suited Personality")]
	public HEXACO hexaco;
}
