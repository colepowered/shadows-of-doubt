﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StateSaveData
{
    [Header("Session Data")]
    public string build;
    public string cityShare; //Share code for the city

    public List<string> compositionData; //Used for debugging save game sizes
    public int dynamicPrintsCount; //Used for debugging save game sizes
    public int sceneCaptureCount; //Used for debugging save game sizes
    public int sceneCapMax; //Used for debugging save game sizes; the object with the most captures

    public string saveTime; //Timestamp of save game
    public float gameTime; //Time of day
    public float timeLimit;
    public int leapCycle; //Leap year cycle
    public int fingerprintLoop = 0; //Used in discovered of fingerprint sets
    public int assignCaptureID = 0;
    public int assignMessageThreadID = 0;
    public int assignGroupID = 0;
    public int assignStickNote = 1;
    public int assignInteractableID = 1;
    public int assignCaseID = 1;
    public int assignMurderID = 1;
    public int gameLength = 2;

    //Weather
    public float currentRain = 0f;
    public float desiredRain = 0f;
    public float currentWind = 0f;
    public float desiredWind = 0f;
    public float currentSnow = 0f;
    public float desiredSnow = 0f;
    public float currentLightning = 0f;
    public float desiredLightning = 0f;
    public float cityWetness = 0f;
    public float citySnow = 0f;
    public float weatherChange = 0f;

    //Side Jobs
    public List<SideJob> basicJobs = new List<SideJob>();
    public List<SideJobAffair> affairJobs = new List<SideJobAffair>();
    public List<SideJobSabotage> sabotageJobs = new List<SideJobSabotage>();
    public List<SideJobStolenItem> stolenItemJobs = new List<SideJobStolenItem>();
    public List<SideJobMissingPerson> missingPersonJobs = new List<SideJobMissingPerson>();
    public List<SideJobRevenge> revengeJobs = new List<SideJobRevenge>();
    public List<SideJobStealBriefcase> briefcaseJobs = new List<SideJobStealBriefcase>();
    public int jobDiffLevel = 0; //Job difficulty level

    //Chapter
    public int chapter;
    public int chapterPart;
    public ChaperStateSave chapterSaveState;

    //Map path
    public bool mapPathActive = false;
    public bool mapPathNodeSpecific = false;
    public Vector3Int mapPathNode = Vector3Int.zero;

    //Cases
    public List<Case> activeCases = new List<Case>();
    public List<Case> archivedCases = new List<Case>();
    public int activeCase = -1;

    //Footprints
    public List<GameplayController.Footprint> footprints = new List<GameplayController.Footprint>();

    //History
    public List<GameplayController.History> history = new List<GameplayController.History>();

    //Passcodes
    public List<GameplayController.Passcode> passcodes = new List<GameplayController.Passcode>();

    //Numbers
    public List<GameplayController.PhoneNumber> numbers = new List<GameplayController.PhoneNumber>();

    //Enforcer calls
    public List<GameplayController.EnforcerCall> enforcerCalls = new List<GameplayController.EnforcerCall>();

    //Crime scene cleanup pool
    public List<CrimeSceneCleanup> crimeSceneCleanup = new List<CrimeSceneCleanup>();

    [System.Serializable]
    public class CrimeSceneCleanup
    {
        public bool isStreet = false;
        public int id;
    }

    [System.Serializable]
    public class BrokenWindowSave
    {
        public Vector3 pos;
        public float brokenAt;
    }

    //Hotel guests
    public List<GameplayController.HotelGuest> hotelGuests = new List<GameplayController.HotelGuest>();

    public List<BrokenWindowSave> brokenWindows = new List<BrokenWindowSave>();


    //Newspaper
    public NewspaperController.NewspaperState newspaperState = null;

    //Player
    public string playerFirstName = "Fred";
    public string playerSurname = "Melrose";
    public Human.Gender playerGender = Human.Gender.male;
    public Human.Gender partnerGender = Human.Gender.male;
    public Color playerSkinColour;
    public int playerBirthDay = 19;
    public int playerBirthMonth = 12;
    public int playerBirthYear = 1947;
    public int residence = -1;
    public List<int> apartmentsOwned = new List<int>();
    public bool accidentCover = false;
    public List<int> foodH = new List<int>();
    public List<int> sanitary = new List<int>();
    public List<int> ops = new List<int>();
    public List<int> knowsPasswords = new List<int>(); //Know location passwords
    public List<GameplayController.LoanDebt> debt = new List<GameplayController.LoanDebt>();
    public int carried = -1; //Carried by player

    //Tutorial
    public bool tutorial = false;
    //public List<string> tutTextRead = new List<string>();
    public List<string> tutTextTriggered = new List<string>();

    //Items
    public List<FirstPersonItemController.InventorySlot> firstPersonItems = new List<FirstPersonItemController.InventorySlot>();
    public List<ScannedObjPrint> scannedPrints = new List<ScannedObjPrint>();

    public Vector3 playerPos;
    public Quaternion playerRot;
    public int money;
    public int lockpicks;
    public int socCredit;

    public float health;
    public float nourishment;
    public float hydration;
    public float alertness;
    public float energy;
    public float hygiene;
    public float heat;
    public float drunk;
    public float sick;
    public float headache;
    public float wet;
    public float brokenLeg;
    public float bruised;
    public float blackEye;
    public float blackedOut;
    public float numb;
    public float poisoned;
    public float bleeding;
    public float wellRested;
    public float starchAddiction;
    public float syncDiskInstall;
    public float blinded;

    public bool crouched = false;
    public List<UpgradesController.Upgrades> upgrades = new List<UpgradesController.Upgrades>();
    public List<string> sabotaged = new List<string>();
    public List<string> booksRead = new List<string>();

    public List<SceneRecorder.SceneCapture> playerSavedCaptures = new List<SceneRecorder.SceneCapture>();

    //Speech
    public List<SpeechController.QueueElement> speech = new List<SpeechController.QueueElement>();

    //Keyring
    public List<int> keyring = new List<int>();
    public List<int> keyringInt = new List<int>();

    //Fake telephone numbers
    public List<FakeTelephone> fakeTelephone = new List<FakeTelephone>();

    //States
    public int hideInteractable = -1;
    public int hideRef = -1;
    public int phoneInteractable = -1;
    public int computerInteractable = -1;
    public int duct = -1;
    public Vector3 storedTransPos;

    //Buildings
    public List<BuildingStateSav> buildings = new List<BuildingStateSav>();

    //Companies
    public List<CompanyStateSave> companies = new List<CompanyStateSave>();

    //Vmail Threads
    public List<MessageThreadSave> messageThreads = new List<MessageThreadSave>();

    //Murders
    public bool pgLoop = false; //Proc gen loop state
    public int currentMurderer = -1;
    public int currentVictim = -1;
    public int currentActiveCase = -1;
    public string murderPreset;
    public string chosenMO;
    public List<int> previousMurderers = new List<int>();
    public float pauseBetweenMurders = 0f;
    public bool murderRoutineActive = false;
    public int maxMurderDiffLevel = 1;

    public List<MurderController.Murder> murders = new List<MurderController.Murder>();
    public List<MurderController.Murder> iaMurders = new List<MurderController.Murder>();

    //Evidence
    public List<EvidenceStateSave> evidence = new List<EvidenceStateSave>();

    //Below: Experimenting with saving evidence as an already-serialized string that can also act as an ID to avoid duplicates.
    public List<string> timeEvidence = new List<string>();
    public List<string> dateEvidence = new List<string>();
    public List<string> customStrings = new List<string>();

    //Spatter
    public List<SpatterSimulation> spatter = new List<SpatterSimulation>();

    //Furniture storage
    public List<CitySaveData.FurnitureClusterObjectCitySave> furnitureStorage = new List<CitySaveData.FurnitureClusterObjectCitySave>();

    [System.Serializable]
    public class ScannedObjPrint
    {
        public int objID;
        public List<int> prints;
    }

    [System.Serializable]
    public class ChaperStateSave
    {
        public List<ChapterSaveData> data = new List<ChapterSaveData>();

        public void AddData(string reference, int integer)
        {
            data.Add(new ChapterSaveData { reference = reference, data = integer.ToString() });
        }

        public void AddData(string reference, float floatP)
        {
            data.Add(new ChapterSaveData { reference = reference, data = floatP.ToString() });
        }

        public void AddData(string reference, string str)
        {
            data.Add(new ChapterSaveData { reference = reference, data = str });
        }

        public void AddData(string reference, bool b)
        {
            if(b)
            {
                AddData(reference, 1);
            }
            else
            {
                AddData(reference, 0);
            }
        }

        public bool GetDataBool(string reference)
        {
            bool ret = false;
            int getBoolInt = GetDataInt(reference);
            if (getBoolInt >= 1) ret = true;

            return ret;
        }

        public int GetDataInt(string reference)
        {
            int ret = -1;

            ChapterSaveData getData = data.Find(item => item.reference == reference);

            if(getData != null)
            {
                int.TryParse(getData.data, out ret);
            }

            return ret;
        }

        public float GetDataFloat(string reference)
        {
            float ret = -1;

            ChapterSaveData getData = data.Find(item => item.reference == reference);

            if (getData != null)
            {
                float.TryParse(getData.data, out ret);
            }

            return ret;
        }

        public string GetDataString(string reference)
        {
            string ret = string.Empty;

            ChapterSaveData getData = data.Find(item => item.reference == reference);

            if (getData != null)
            {
                ret = getData.data;
            }

            return ret;
        }
    }

    [System.Serializable]
    public class ChapterSaveData
    {
        public string reference;
        public string data;
    }

    [System.Serializable]
    public class EvidenceStateSave
    {
        public string id;
        public string dds;
        public bool found = false;
        public List<EvidenceDataKeyTie> keyTies = new List<EvidenceDataKeyTie>();
        public List<Evidence.Discovery> discovery = new List<Evidence.Discovery>();
        public bool fs;
        public string n; //Saved note
        public List<Evidence.CustomName> customName = new List<Evidence.CustomName>();

        //Specific evidence data
        public List<EvidenceMultiPage.MultiPageContent> mpContent = new List<EvidenceMultiPage.MultiPageContent>();
    }

    [System.Serializable]
    public class EvidenceDataKeyTie
    {
        public Evidence.DataKey key;
        public List<Evidence.DataKey> tied = new List<Evidence.DataKey>();
    }

    [System.Serializable]
    public class FakeTelephone
    {
        public int number;
        public TelephoneController.CallSource source;
    }

    [System.Serializable]
    public class BuildingStateSav
    {
        public int id;

        //Alarm state
        public bool alarmActive = false;
        public float alarmTimer = 0f;
        public NewBuilding.AlarmTargetMode targetMode;
        public float targetModeSetAt;
        public List<int> targets = new List<int>();
        public float wanted = 0f;

        public List<ElevatorStateSave> elevators = new List<ElevatorStateSave>();//Elevator positions

        public List<TelephoneController.PhoneCall> callLog;

        public List<GameplayController.LostAndFound> lostAndFound = new List<GameplayController.LostAndFound>();
    }

    [System.Serializable]
    public class ElevatorStateSave
    {
        public int tileID;
        public float yPos;
        public int floor; //Current floor
    }

    //Floors
    public List<FloorStateSave> floors = new List<FloorStateSave>();

    [System.Serializable]
    public class FloorStateSave
    {
        public int id;
        public bool alarmLockdown = false; //True if this floor is locked-down from alarm
    }

    //Addresses
    public List<AddressStateSave> addresses = new List<AddressStateSave>();

    [System.Serializable]
    public class AddressStateSave
    {
        public int id;
        public int sale = -1; //Sale note id

        public List<NewAddress.Vandalism> vandalism = new List<NewAddress.Vandalism>(); //Vandalism

        //Alarm state
        public bool alarmActive = false;
        public float alarmTimer = 0f;
        public NewBuilding.AlarmTargetMode targetMode;
        public float targetModeSetAt;
        public List<int> targets = new List<int>();

        public List<NewGameLocation.TrespassEscalation> escalation = new List<NewGameLocation.TrespassEscalation>();
    }

    [System.Serializable]
    public class CompanyStateSave
    {
        //Sales records
        public int id;
        public List<Company.SalesRecord> sales = new List<Company.SalesRecord>();
    }
     
    //GameLocations
    public List<GuestPassStateSave> guestPasses = new List<GuestPassStateSave>();

    [System.Serializable]
    public class GuestPassStateSave
    {
        public int id;
        public Vector2 guestPassUntil;
    }

    //Rooms
    public List<RoomStateSave> rooms = new List<RoomStateSave>();

    [System.Serializable]
    public class RoomStateSave
    {
        public int id;
        public int ex = 0;
        public bool ml = true; //Main light status
        public float gl = 0; //Gas level
        public int fID = 1; //Furniture assign ID
        public int iID = 1; //Interactable assign ID

        //If the decor has been edited, save version of city data file
        public List<CitySaveData.RoomCitySave> decorOverride; //Use a list here as by default the save class here is serialized, and would take extra save data space
    }

    //Meta objects
    public List<MetaObject> metas = new List<MetaObject>();

    //Interactables
    public List<Interactable> interactables = new List<Interactable>();

    //Removed city data items
    public List<int> removedCityData = new List<int>();

    //Citizens
    public List<CitizenStateSave> citizens = new List<CitizenStateSave>();

    [System.Serializable]
    public class CitizenStateSave
    {
        public int id;

        //Position
        public Vector3 pos;
        public Quaternion rot;

        public int trespassingEscalation = 0; //How severe trespassing is
        public ClothesPreset.OutfitCategory currentOutfit = ClothesPreset.OutfitCategory.casual;

        //Status
        public float nourishment = 0f;
        public float hydration = 0f;
        public float alertness = 0f;
        public float energy = 0f;
        public float excitement = 0f;
        public float chores = 0f;
        public float hygiene = 0f;
        public float bladder = 0f;
        public float heat = 0f;
        public float drunk = 0f;
        public float breath = 0f;
        public float poisoned = 0f;
        public float blinded = 0f;

        public int poisoner = -1;

        public bool remFromWorld = false; //Has been removed from the game world

        public float currentHealth = 1f;
        public float currentNerve = 0.1f; //When an AI will switch tactics to running away (when at 0)

        public float fsDirt = 0f; //Footprint dirt
        public float fsBlood = 0f; //Footprint blood

        public List<Human.Wound> wounds = new List<Human.Wound>();

        //AI
        public Vector3Int investigateLocation; //Last known node of player
        public Vector3 investigatePosition; //Last known position of player
        public Vector3 investigatePositionProjection; //Projection of where the current target will be (2m ahead of now)
        public float lastInvestigate = 0f;
        public bool persuit = false; //True if chasing player (with a certain amount of grace period tracking)
        public bool seesPlayerOnPersuit = false; //Only true when actively seeing the player
        public float persuitChaseLogicUses = 0; //Each time a player is spotted, it gets one lead on to where the player has disappeared to...
        public int persuitTarget;
        public bool persuitPlayer = false;
        public int escalationLevel = 0;
        public float minimumInvestigationTimeMultiplier = 1f; //Can be increased depending on the severety of the crime (remember to max this value)
        public NewAIController.ReactionState reactionState = NewAIController.ReactionState.none;
        public List<int> atHome; //At home interactables
        public bool convicted = false;
        public bool unreportable = false;

        public bool ko = false; //True if AI is knocked-out
        public float koTime = 0f;

        public bool res = false; //Restrained
        public float resTime = 0f;

        public float spooked = 0;
        public int spookCount = 0;

        public Human.Death death;
        public List<CitizenAnimationController.RagdollSnapshot> ragdollSnapshot = null;

        public List<Human.WalletItem> wallet = new List<Human.WalletItem>();

        public CurrentGoalStateSave currentGoal;

        public int fingerprintLoop = -1;

        public List<string> currentConsumable;
        public List<int> trash;

        public List<int> putDown;

        public List<int> sightingCit = new List<int>();
        public List<Human.Sighting> sightings = new List<Human.Sighting>();

        public AvoidConfineStateSave confine = null;
        public List<AvoidConfineStateSave> avoid = new List<AvoidConfineStateSave>();
    }

    [System.Serializable]
    public class AvoidConfineStateSave
    {
        public int id = -1;
        public bool st = false; //Is street?
    }


    [System.Serializable]
    public class CurrentGoalStateSave
    {
        public string preset;
        public float priority;
        public float trigerTime;
        public float duration;
        public Vector3Int passedNode;
        public int passedInteractable = -1;
        public int gameLocation = -1;
        public int room = -1;
        public bool isAddress;
        public int passedGroup = -1; //Optionally passed to form group meetups
        public int jobID = -1;
        public int var = -2;
        public float activeTime;

        public List<AIActionStateSave> actions = new List<AIActionStateSave>();
    }

    [System.Serializable]
    public class AIActionStateSave
    {
        public string preset;
        public Vector3 node;
        public int interactable = -1;
        public int passedInteractable; //Optional passed to the constructor to force and interactables
        public int passedRoom = -1; //Optionally passed to the constructor to force a room
        public int passedGroup = -1; //Optionally passed to form group meetups
        public Vector3Int forcedNode; //Pass this to force a node upon which to perform the action
        public bool repeat = false;
    }

    //Doors
    public List<DoorStateSave> doors = new List<DoorStateSave>();

    [System.Serializable]
    public class DoorStateSave
    {
        public int id;
        //public bool isClosed = true;
        public bool l = false;
        public float ds = 0f;
        public float ls = 0f;
        public float ajar;
        public bool cs = false;
    }

    //Misc
    [System.Serializable]
    public class MessageThreadSave
    {
        public int threadID = 0;
        public DDSSaveClasses.TreeType msgType = DDSSaveClasses.TreeType.vmail;
        public string treeID;

        //This save class uses a message list to save conversation trees
        public int participantA = -1;
        public int participantB = -1;
        public int participantC = -1;
        public int participantD = -1;

        public List<int> cc = new List<int>(); //CC'd citizens not in the above list...

        //List of message IDs that correspond to who said what and refer to DDS messages
        public List<string> messages = new List<string>(); //This is the INSTANCE Id
        public List<int> senders = new List<int>(); //Citizen IDs corresponding to above
        public List<int> recievers = new List<int>(); //Citizen IDs corresponding to above
        public List<float> timestamps = new List<float>(); //Game times corresponding to above

        //Override sender's data source...
        public CustomDataSource ds = CustomDataSource.sender;
        public int dsID = -1;
    }

    public enum CustomDataSource { sender, groupID};
}


















