using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Text;
using System;

public class SecurityApp : CruncherAppContent
{
    [Header("Components")]
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI cameraSelectionText;
    public TextMeshProUGUI targetSelectionText;
    public TextMeshProUGUI locationstampText;
    public TextMeshProUGUI locationstampTextShadow;
    public RenderTexture renderTexturePrefab;
    public RawImage captureDisplay;
    public RectTransform captureRect;
    public Button camOnButton;
    public Button camOffButton;
    public Button alarmOnButton;
    public Button alarmOffButton;
    public RectTransform camDisplayPageRect;
    [Space(5)]
    public TextMeshProUGUI camOnText;
    public TextMeshProUGUI camOffText;
    public TextMeshProUGUI alarmOnText;
    public TextMeshProUGUI alarmOffText;

    [Header("State")]
    public List<Interactable> cameras = new List<Interactable>();
    [System.NonSerialized]
    public Interactable selectedCamera;
    public List<Interactable> selectedSentries = new List<Interactable>();
    private float camUpdateTimer = 0f;

    public override void OnSetup()
    {
        base.OnSetup();

        titleText.text = Strings.Get("computer", "Security Systems");

        camOnText.text = Strings.Get("computer", "On", Strings.Casing.upperCase);
        camOffText.text = Strings.Get("computer", "Off", Strings.Casing.upperCase);
        alarmOnText.text = Strings.Get("computer", "On", Strings.Casing.upperCase);
        alarmOffText.text = Strings.Get("computer", "Off", Strings.Casing.upperCase);

        //Get cameras
        cameras.AddRange(controller.ic.interactable.node.gameLocation.securityCameras);

        //Check for adding building surveillance...
        if (controller.ic.interactable.node.gameLocation.thisAsAddress != null)
        {
            if (controller.ic.interactable.node.gameLocation.thisAsAddress.company != null)
            {
                if (controller.ic.interactable.node.gameLocation.thisAsAddress.company.preset.controlsBuildingSurveillance)
                {
                    foreach (NewAddress lob in controller.ic.interactable.node.building.lobbies)
                    {
                        cameras.AddRange(lob.securityCameras);
                    }
                }
            }
        }

        locationstampText.text = string.Empty;
        locationstampTextShadow.text = locationstampText.text;

        if (cameras.Count > 0)
        {
            SetCamera(cameras[0]);
        }
    }

    public void SetCamera(Interactable newSelection)
    {
        selectedCamera = newSelection;

        if (selectedCamera != null)
        {
            Game.Log("Gameplay: Security set new camera: " + selectedCamera.node.room.name);

            //Find sentry gun in same room
            selectedSentries.Clear();

            if (selectedCamera.node.gameLocation.thisAsAddress != null)
            {
                selectedSentries.AddRange(selectedCamera.node.gameLocation.thisAsAddress.sentryGuns.FindAll(item => item.node.room == selectedCamera.node.room));
            }

            //Load cam
            cameraSelectionText.text = Strings.Get("names.rooms", selectedCamera.node.room.preset.name);

            locationstampText.text = selectedCamera.node.room.GetName();
            locationstampTextShadow.text = locationstampText.text;

            camOnButton.interactable = true;
            camOffButton.interactable = true;
            camUpdateTimer = 0f;

            UpdateCamStatus();
        }
        else
        {
            Game.Log("Gameplay: Security set null camera");
            selectedSentries.Clear();

            locationstampText.text = string.Empty;
            locationstampTextShadow.text = locationstampText.text;

            camOnButton.interactable = false;
            camOffButton.interactable = false;
        }

        if(selectedCamera != null && selectedCamera.sw0)
        {
            captureDisplay.color = Color.white;
        }
        else
        {
            captureDisplay.color = Color.black;
        }
    }

    private void Update()
    {
        if (SessionData.Instance.play)
        {
            if (Player.Instance.computerInteractable == controller.ic.interactable)
            {
                if (selectedCamera != null && selectedCamera.sw0)
                {
                    if (camUpdateTimer <= 0)
                    {
                        SceneRecorder.SceneCapture cap = selectedCamera.sceneRecorder.ExecuteCapture(false, false, false);
                        captureDisplay.texture = SceneCapture.Instance.GetSurveillanceScene(cap, saveToCache: false);
                        camUpdateTimer = 1f;
                    }

                    camUpdateTimer -= Time.deltaTime;
                }
            }
        }
    }

    public void CameraSelection(int addSelection)
    {
        int currentIndex = cameras.IndexOf(selectedCamera);

        currentIndex += addSelection;

        if (currentIndex < 0)
        {
            currentIndex += cameras.Count;
        }
        else if (currentIndex >= cameras.Count)
        {
            currentIndex -= cameras.Count;
        }

        if (cameras.Count > 0)
        {
            SetCamera(cameras[currentIndex]);
        }
    }

    public void AlarmTargetSelection(int addSelection)
    {
        if(selectedCamera != null)
        {
            if(selectedCamera.node.gameLocation.thisAsAddress != null)
            {
                int currentIndex = 0;

                if (selectedCamera.node.gameLocation.thisAsAddress.addressPreset != null && selectedCamera.node.gameLocation.thisAsAddress.addressPreset.useOwnSecuritySystem)
                {
                    currentIndex = (int)selectedCamera.node.gameLocation.thisAsAddress.targetMode;
                }
                else if (selectedCamera.node.gameLocation.building != null)
                {
                    currentIndex = (int)selectedCamera.node.building.targetMode;
                }

                currentIndex += addSelection;

                if (currentIndex < 0)
                {
                    currentIndex = 4;
                }
                else if (currentIndex > 4)
                {
                    currentIndex = 0;
                }

                //Change target mode
                if (selectedCamera.node.gameLocation.thisAsAddress.addressPreset != null && selectedCamera.node.gameLocation.thisAsAddress.addressPreset.useOwnSecuritySystem)
                {
                    selectedCamera.node.gameLocation.thisAsAddress.SetTargetMode((NewBuilding.AlarmTargetMode)currentIndex);
                }
                else if(selectedCamera.node.gameLocation.building != null)
                {
                    selectedCamera.node.building.SetTargetMode((NewBuilding.AlarmTargetMode)currentIndex);
                }
            }
        }

        UpdateCamStatus();
    }

    public void ExitButton()
    {
        controller.OnAppExit();
    }

    public void SetCamActiveButton(bool val)
    {
        if (selectedCamera != null)
        {
            selectedCamera.SetSwitchState(val, Player.Instance);

            if (val)
            {
                selectedCamera.SetValue(selectedCamera.GetSecurityStrength());
            }
            else selectedCamera.SetValue(0f);
        }

        UpdateCamStatus();
    }

    public void SetAlarmActiveButton(bool val)
    {
        if (selectedCamera != null && selectedCamera.node.gameLocation.thisAsAddress != null)
        {
            if(selectedCamera.node.gameLocation.IsAlarmActive(out _, out _, out _))
            {
                if(!val)
                {
                    selectedCamera.node.gameLocation.thisAsAddress.SetAlarm(false, null);
                }
            }
            else
            {
                if (val)
                {
                    selectedCamera.node.gameLocation.thisAsAddress.SetAlarm(true, null);
                }
            }
        }

        UpdateCamStatus();
    }

    private void UpdateCamStatus()
    {
        if (selectedCamera != null)
        {
            if (selectedCamera.sw0)
            {
                camOnText.text = ">" + Strings.Get("computer", "On", Strings.Casing.upperCase) + "<";
                camOffText.text = Strings.Get("computer", "Off", Strings.Casing.upperCase);
            }
            else
            {
                camOnText.text = Strings.Get("computer", "On", Strings.Casing.upperCase);
                camOffText.text = ">" + Strings.Get("computer", "Off", Strings.Casing.upperCase) + "<";
            }

            if(selectedCamera.node.gameLocation.IsAlarmActive(out _, out _, out _))
            {
                alarmOnText.text = ">" + Strings.Get("computer", "On", Strings.Casing.upperCase) + "<";
                alarmOffText.text = Strings.Get("computer", "Off", Strings.Casing.upperCase);
            }
            else
            {
                alarmOnText.text = Strings.Get("computer", "On", Strings.Casing.upperCase);
                alarmOffText.text = ">" + Strings.Get("computer", "Off", Strings.Casing.upperCase) + "<";
            }

            if (selectedCamera.node.gameLocation.thisAsAddress != null && selectedCamera.node.gameLocation.thisAsAddress.addressPreset != null && selectedCamera.node.gameLocation.thisAsAddress.addressPreset.useOwnSecuritySystem)
            {
                targetSelectionText.text = Strings.Get("computer", selectedCamera.node.gameLocation.thisAsAddress.targetMode.ToString());
            }
            else if(selectedCamera.node.gameLocation.building != null)
            {
                targetSelectionText.text = Strings.Get("computer", selectedCamera.node.gameLocation.building.targetMode.ToString());
            }
            else
            {
                targetSelectionText.text = string.Empty;
            }
        }

        if (selectedCamera != null && selectedCamera.sw0)
        {
            captureDisplay.color = Color.white;
        }
        else
        {
            captureDisplay.color = Color.black;
        }
    }
}
