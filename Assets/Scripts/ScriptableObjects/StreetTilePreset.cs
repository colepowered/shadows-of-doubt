﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "streettile_data", menuName = "Database/Street Tile Preset")]

public class StreetTilePreset : SoCustomComparison
{
	[Header("Setup")]
	public StreetSection sectionType = StreetSection.streetLong;
	[ReorderableList]
	public List<StreetSectionModel> prefabList = new List<StreetSectionModel>();

	[System.Serializable]
	public class StreetSectionModel
	{
		public GameObject prefab;
		[Tooltip("When raindrops are disabled, use this material...")]
		public Material normalMaterial;
		[Tooltip("When raindrops are enabled, use this material...")]
		public Material rainMaterial;
	}

	public enum StreetSection { streetLong, streetShort, streetInsideCorner, streetJunctionCorner, streetOutsideCorner, joinerLong, joinerShort};
}
