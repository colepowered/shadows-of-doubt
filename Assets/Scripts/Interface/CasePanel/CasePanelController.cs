﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.EventSystems;
using NaughtyAttributes;
using System;
using System.IO.IsolatedStorage;
using System.Text.RegularExpressions;

//Script that controls the case board.
//Script pass 1
public class CasePanelController : PanelController
{
	[Header("References")]
	public RectTransform corkBoard;
    public RectTransform pinnedContainer;
    public RectTransform stringContainer;
    public RectTransform caseButtonParent;
    public ButtonController newCaseButton;
    public ButtonController closeCaseButton;
    public RectTransform caseDisplayArea;
    public RectTransform closeCaseDisplayArea;
    public Sprite resolveSprite;
    public Sprite archiveSprite;
    public Sprite collectHandInSprite;

    [Header("Cases")]
    [NonSerialized]
    public Case activeCase;
    public List<Case> activeCases = new List<Case>();
    public List<Case> archivedCases = new List<Case>();
    public List<CaseButtonController> spawnedCaseButtons = new List<CaseButtonController>();

    [Header("Pick Evidence Mode")]
    public bool pickModeActive = false;
    public InputFieldController pickForField;

    [Header("String Link")]
    public bool customLinkSelectionMode = false;
    public PinnedItemController customStringLinkSelection = null;
    public RectTransform customString;
    private FactCustom newestCreatedFact;

    //String connection
    public class StringConnection
    {
        public PinnedItemController from;
        public PinnedItemController to;
        public List<Evidence.FactLink> links = new List<Evidence.FactLink>();
        public List<Fact> facts = new List<Fact>();

        public StringConnection(PinnedItemController fromPinned, PinnedItemController toPinned)
        {
            from = fromPinned;
            to = toPinned;
        }
    }

    [Header("Spawned")]
    public List<PinnedItemController> spawnedPins = new List<PinnedItemController>();
    public List<StringController> spawnedStrings = new List<StringController>();
    private float caseCloseTransition = 0f;

    [Header("Controller Mode")]
    public bool controllerMode = false;
    public InfoWindow selectedWindow;
    public PinnedItemController selectedPinned;
    public ButtonController selectedTopBarButton;
    public ControllerSelectMode currentSelectMode = ControllerSelectMode.caseBoard;
    public RectTransform topMenuSelect;
    public JuiceController topMenuSelectJuice;
    public RectTransform upgradesSelect;
    public JuiceController upgradesSelectJuice;
    public RectTransform boardSelect;
    public JuiceController boardSelectJuice;
    public RectTransform mapSelect;
    public JuiceController mapSelectJuice;
    public ButtonController notebookButton;
    public ButtonController stickNoteButton;
    public ButtonController selectNoCaseButton;

    public ViewportMouseOver caseboardMO;
    public ControllerViewRectScroll caseboardScroll;

    public ViewportMouseOver mapMO;
    public ControllerViewRectScroll mapScroll;

    public ViewportMouseOver upgradesMO;
    public ControllerViewRectScroll upgradesScroll;

    public enum ControllerSelectMode { topBar, caseBoard, windows };

    //Events
    public delegate void PinnedChange();
    public event PinnedChange OnPinnedChange;

    public delegate void PinEvidence(Evidence evidence);
    public event PinEvidence OnPinEvidence;

    public delegate void UnpinEvidence(Evidence evidence);
    public event UnpinEvidence OnUnpinEvidence;

    //Singleton pattern
    private static CasePanelController _instance;
    public static CasePanelController Instance { get { return _instance; } }

	void Awake()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
	}

    private void Start()
    {
        UpdateCloseCaseButton();
    }

    //Update case controls
    public void UpdateCaseControls()
    {
        List<Case> required = new List<Case>(activeCases);

        for (int i = 0; i < spawnedCaseButtons.Count; i++)
        {
            CaseButtonController cbc = spawnedCaseButtons[i];

            if(required.Contains(cbc.thisCase))
            {
                required.Remove(cbc.thisCase);
                cbc.UpdateVisuals();
            }
            else
            {
                Destroy(cbc.gameObject);
                spawnedCaseButtons.RemoveAt(i);
                i--;
            }
        }

        foreach(Case req in required)
        {
            GameObject newObj = Instantiate(PrefabControls.Instance.caseButton, caseButtonParent);
            CaseButtonController cbc = newObj.GetComponent<CaseButtonController>();
            cbc.Setup(req);
            spawnedCaseButtons.Add(cbc);
        }

        //Position new case button at end
        newCaseButton.transform.SetAsLastSibling();

        //Configure controller access
        if(spawnedCaseButtons.Count <= 0)
        {
            stickNoteButton.RefreshAutomaticNavigation();
            newCaseButton.RefreshAutomaticNavigation();
        }
        else
        {
            stickNoteButton.RefreshAutomaticNavigation();
            newCaseButton.RefreshAutomaticNavigation();
        }

        for (int i = 0; i < spawnedCaseButtons.Count; i++)
        {
            CaseButtonController cbc = spawnedCaseButtons[i];

            cbc.RefreshAutomaticNavigation();
        }
    }

    public void SelectNoCaseButton()
    {
        //Disable this if in tutorial section
        int chapt = 0;

        if(Toolbox.Instance.IsStoryMissionActive(out _, out chapt))
        {
            if (chapt < 31) return;
        }

        SetActiveCase(null);
    }

    public void NewCustomCaseButton()
    {
        //Disable this if in tutorial section
        int chapt = 0;

        if (Toolbox.Instance.IsStoryMissionActive(out _, out chapt))
        {
            if (chapt < 31) return;
        }

        CreateNewCase(Case.CaseType.custom, Case.CaseStatus.closable, caseName: Strings.Get("ui.interface", "New Case"));
    }

    public Case CreateNewCase(Case.CaseType newType, Case.CaseStatus newStatus, bool isSilent = false, string caseName = "New Case")
    {
        //Case limit
        if(activeCases.Count >= GameplayControls.Instance.maxCases && newType != Case.CaseType.mainStory && newType != Case.CaseType.murder)
        {
            Game.Log("Player: Case limit reached!");
            return null;
        }

        if(newType == Case.CaseType.murder)
        {
            InterfaceController.Instance.NewMurderCaseDisplay();
        }

        Case newCase = null;

        if(!isSilent)
        {
            //Launch create case popup
            PopupMessageController.Instance.PopupMessage("CreateCase", true, true, RButton: "Confirm", anyButtonClosesMsg: true, enableInputField: true, inputFieldDefault: caseName);
            PopupMessageController.Instance.OnLeftButton += OnCancelNewCustomCase;
            PopupMessageController.Instance.OnRightButton += OnCreateNewCustomCase;
        }
        else
        {
            newCase = new Case(caseName, newType, newStatus);
        }

        if(newCase != null)
        {
            activeCases.Add(newCase);
            UpdateCaseControls();
            SetActiveCase(newCase); //Set active immediately

            //Set hand-in (case trays for murder case)
            if (newCase.caseType == Case.CaseType.mainStory || newCase.caseType == Case.CaseType.murder || newCase.caseType == Case.CaseType.retirement)
            {
                newCase.handIn.Clear();

                foreach(Interactable i in CityData.Instance.caseTrays)
                {
                    newCase.handIn.Add(i.id);
                }
            }
        }

        return newCase;
    }

    //Called after popup controller
    public void OnCreateNewCustomCase()
    {
        PopupMessageController.Instance.OnLeftButton -= OnCancelNewCustomCase;
        PopupMessageController.Instance.OnRightButton -= OnCreateNewCustomCase;

        Case newCase = new Case(PopupMessageController.Instance.inputField.text, Case.CaseType.custom, Case.CaseStatus.closable);

        if (newCase != null)
        {
            activeCases.Add(newCase);
            UpdateCaseControls();
            SetActiveCase(newCase); //Set active immediately
        }
    }

    public void OnCancelNewCustomCase()
    {
        PopupMessageController.Instance.OnLeftButton -= OnCancelNewCustomCase;
        PopupMessageController.Instance.OnRightButton -= OnCreateNewCustomCase;
    }

    public void UpdateCloseCaseButton()
    {
        try
        {
            if(pickModeActive)
            {
                closeCaseButton.icon.sprite = resolveSprite;
                closeCaseButton.textReference = "Cancel Pick";
                closeCaseButton.UpdateButtonText();

                closeCaseButton.tooltip.mainDictionaryKey = "cancelpick";
                this.enabled = true;
                return;
            }

            //Should this button be displayed?
            if(activeCase != null && (activeCase.caseStatus == Case.CaseStatus.handInCollected || activeCase.caseStatus == Case.CaseStatus.handInNotCollected || activeCase.caseStatus == Case.CaseStatus.closable))
            {
                if (activeCase.caseStatus == Case.CaseStatus.handInCollected)
                {
                    closeCaseButton.icon.sprite = resolveSprite;

                    if(activeCase.caseType == Case.CaseType.retirement)
                    {
                        closeCaseButton.textReference = "Retire";
                    }
                    else
                    {
                        closeCaseButton.textReference = "Resolve";
                    }
                
                    closeCaseButton.UpdateButtonText();

                    closeCaseButton.tooltip.mainDictionaryKey = "resolvecase";
                    closeCaseButton.tooltip.detailDictionaryKey = "resolvecase_detail";
                }
                else if(activeCase.caseStatus == Case.CaseStatus.closable)
                {
                    closeCaseButton.icon.sprite = archiveSprite;
                    closeCaseButton.textReference = "Archive/Close";
                    closeCaseButton.UpdateButtonText();

                    closeCaseButton.tooltip.mainDictionaryKey = "closecase";
                    closeCaseButton.tooltip.detailDictionaryKey = "closecase_detail";
                }
                else if (activeCase.caseStatus == Case.CaseStatus.handInNotCollected)
                {
                    closeCaseButton.icon.sprite = collectHandInSprite;

                    if (activeCase.caseType == Case.CaseType.sideJob)
                    {
                        closeCaseButton.textReference = "Get Info";
                        closeCaseButton.UpdateButtonText();

                        closeCaseButton.tooltip.mainDictionaryKey = "getinfo";
                        closeCaseButton.tooltip.detailDictionaryKey = "getinfo_detail";
                    }
                    else
                    {
                        closeCaseButton.textReference = "Get Form";
                        closeCaseButton.UpdateButtonText();

                        closeCaseButton.tooltip.mainDictionaryKey = "getform";
                        closeCaseButton.tooltip.detailDictionaryKey = "getform_detail";
                    }
                
                    closeCaseButton.UpdateButtonText();

                    closeCaseButton.tooltip.mainDictionaryKey = "collectHandIn";
                    closeCaseButton.tooltip.detailDictionaryKey = "collectHandIn_detail";
                }

                if(closeCaseDisplayArea != null) caseCloseTransition = closeCaseDisplayArea.sizeDelta.x / 354;
                closeCaseButton.SetInteractable(true);
            
                try
                {
                    this.enabled = true;
                    newCaseButton.RefreshAutomaticNavigation();
                    //newCaseButton.button.navigation = new Navigation { mode = Navigation.Mode.Explicit, selectOnLeft = newCaseButton.button.navigation.selectOnLeft, selectOnRight = closeCaseButton.button };
                }
                catch
                {

                }
            }
            else
            {
                if(closeCaseButton != null)
                {
                    closeCaseButton.SetInteractable(false);
                    closeCaseButton.text.gameObject.SetActive(false);
                    closeCaseButton.icon.gameObject.SetActive(false);
                }

                if(closeCaseDisplayArea != null) caseCloseTransition = closeCaseDisplayArea.sizeDelta.x / 354;
                this.enabled = true;

                if(InterfaceController.Instance.selectedElement == closeCaseButton && controllerMode && currentSelectMode == ControllerSelectMode.topBar)
                {
                    closeCaseButton.OnDeselect();
                    notebookButton.OnSelect();
                }

                newCaseButton.RefreshAutomaticNavigation();
                //newCaseButton.button.navigation = new Navigation { mode = Navigation.Mode.Explicit, selectOnLeft = newCaseButton.button.navigation.selectOnLeft };
            }

            UpdateResolveNotifications();
        }
        catch
        {
            //Thsis is causing a rare null exception; I don't know where. Come back and find out where at some point?
        }
    }

    private void Update()
    {
        if(closeCaseButton.interactable)
        {
            if(caseCloseTransition < 1f)
            {
                caseCloseTransition += Time.deltaTime / 0.1f;
                caseCloseTransition = Mathf.Clamp01(caseCloseTransition);

                closeCaseDisplayArea.sizeDelta = new Vector2(Mathf.Lerp(0f, 354, caseCloseTransition), closeCaseDisplayArea.sizeDelta.y);
                //caseDisplayArea.offsetMax = new Vector2(-Mathf.Lerp(6f, 168f, caseCloseTransition), caseDisplayArea.offsetMax.y);

                if (caseCloseTransition >= 1f)
                {
                    closeCaseButton.text.gameObject.SetActive(true);
                    closeCaseButton.icon.gameObject.SetActive(true);
                    closeCaseButton.SetInteractable(true);
                    this.enabled = false;
                }
            }
        }
        else
        {
            if (caseCloseTransition > 0f)
            {
                caseCloseTransition -= Time.deltaTime / 0.1f;
                caseCloseTransition = Mathf.Clamp01(caseCloseTransition);

                closeCaseDisplayArea.sizeDelta = new Vector2(Mathf.Lerp(0f, 354, caseCloseTransition), closeCaseDisplayArea.sizeDelta.y);
                //caseDisplayArea.offsetMax = new Vector2(-Mathf.Lerp(6f, 168f, caseCloseTransition), caseDisplayArea.offsetMax.y);

                if (caseCloseTransition <= 0f) this.enabled = false;
            }
        }
    }

    //This can be resolve or archive depending on context
    public void CloseCaseButton()
    {
        if (pickModeActive) SetPickModeActive(false, null);
        else if(activeCase != null)
        {
            //Display questions screen
            if (ResolveController.Instance == null)
            {
                InterfaceController.Instance.SpawnWindow(null, presetName: "CaseResolve", autoPosition: false, forcePosition: InterfaceControls.Instance.handbookWindowPosition, worldInteraction: false);
            }

            //if (activeCase.caseStatus == Case.CaseStatus.closable)
            //{
            //    if(activeCase.caseType == Case.CaseType.mainStory)
            //    {
            //        PopupMessageController.Instance.PopupMessage("CloseCaseStory", true, false);
            //    }
            //    else
            //    {
            //        PopupMessageController.Instance.PopupMessage("CloseCase", true, true, RButton: "Confirm");
            //        PopupMessageController.Instance.OnLeftButton += CancelCloseCase;
            //        PopupMessageController.Instance.OnRightButton += ConfirmCloseCurrentCase;
            //    }
            //}
            //else if(activeCase.caseStatus == Case.CaseStatus.handInCollected)
            //{
            //    if(ResolveController.Instance == null)
            //    {
            //        InterfaceController.Instance.SpawnWindow(null, presetName: "CaseResolve", autoPosition: false, forcePosition: InterfaceControls.Instance.handbookWindowPosition, worldInteraction: false);
            //    }
            //}
        }
    }

    public void CloseCase(Case closeThisCase)
    {
        //If the case has no content then remove completely
        if (closeThisCase.caseElements.Count > 0)
        {
            closeThisCase.isActive = false;

            //if (!archivedCases.Contains(closeThisCase))
            //{
            //    archivedCases.Add(closeThisCase);
            //}
        }

        activeCases.Remove(closeThisCase);
        if(closeThisCase == activeCase) SetActiveCase(null);

        UpdateCaseControls();
    }

    public void SetActiveCase(Case newCase)
    {
        if(activeCase != newCase)
        {
            Case previousCase = activeCase;
            activeCase = newCase;

            if(activeCase != null)
            {
                Game.Log("Player: New active case: " + activeCase.name);
            }

            //Update status of all open windows
            foreach (InfoWindow window in InterfaceController.Instance.activeWindows)
            {
                window.PinnedUpdateCheck();
            }

            UpdatePinned();

            //Update string colours
            foreach(StringController str in spawnedStrings)
            {
                str.UpdateStringColour();
                str.UpdateHidden();
            }

            //Update the resolve window if open...
            if(ResolveController.Instance != null)
            {
                ResolveController.Instance.UpdateResolveFields();
            }
        }

        UpdateCaseButtonsActive();

        if (activeCase != null)
        {
            closeCaseButton.SetInteractable(true);
        }
        else
        {
            closeCaseButton.SetInteractable(false);
        }

        UpdateCloseCaseButton();
    }

    public void UpdateCaseButtonsActive()
    {
        foreach (CaseButtonController cbc in spawnedCaseButtons)
        {
            if (cbc.thisCase == activeCase)
            {
                cbc.thisCase.isActive = true;
                cbc.icon.enabled = true;
            }
            else
            {
                cbc.thisCase.isActive = false;
                cbc.icon.enabled = false;
            }
        }
    }

    public void NewStickyNoteButton()
    {
        NewStickyNote();
    }

    public InfoWindow NewStickyNote()
    {
        if (CasePanelController.Instance.activeCase == null)
        {
            PopupMessageController.Instance.PopupMessage("NoActiveCase", true, true, RButton: "Confirm", anyButtonClosesMsg: true, enableInputField: true, inputFieldDefault: Strings.Get("ui.interface", "New Case"));
            PopupMessageController.Instance.OnLeftButton += onCreateNewCasePopupCancel;
            PopupMessageController.Instance.OnRightButton += OnCreateNewCasePopup;

            return null;
        }
        else
        {
            //Create a sticky note evidence
            EvidenceStickyNote newNoteEvidence = EvidenceCreator.Instance.CreateEvidence("PlayerStickyNote", "StickyNote" + InterfaceController.assignStickyNoteID, forceDiscoveryOnCreate: true) as EvidenceStickyNote;
            InterfaceController.assignStickyNoteID++;

            return InterfaceController.Instance.SpawnWindow(newNoteEvidence, Evidence.DataKey.name);
        }
    }

    public void OnCreateNewCasePopup()
    {
        PopupMessageController.Instance.OnLeftButton -= onCreateNewCasePopupCancel;
        PopupMessageController.Instance.OnRightButton -= OnCreateNewCasePopup;

        CasePanelController.Instance.OnCreateNewCustomCase();
    }

    public void onCreateNewCasePopupCancel()
    {
        PopupMessageController.Instance.OnLeftButton -= onCreateNewCasePopupCancel;
        PopupMessageController.Instance.OnRightButton -= OnCreateNewCasePopup;
    }

    //Pin to cork board
    public void PinToCasePanel(Case toCase, Evidence ev, Evidence.DataKey evKey, bool forceAutoPin = false, Vector2 localPostion = new Vector2(), bool debugFlag = false)
    {
        List<Evidence.DataKey> keys = new List<Evidence.DataKey>();
        keys.Add(evKey);
        PinToCasePanel(toCase, ev, keys, forceAutoPin, localPostion, debugFlag);
    }

    public void PinToCasePanel(Case toCase, Evidence ev, List<Evidence.DataKey> evKeys, bool forceAutoPin = false, Vector2 localPostion = new Vector2(), bool debugFlag = false)
    {
        //Return if null
        if (ev == null)
        {
            Game.Log("Evidence: Null evidence for pinning to case board.");
            return;
        }

        Game.Log("Evidence: Pinning evidence " + ev.evID);
        //foreach (Evidence.DataKey k in evKeys) Game.Log(k.ToString());

        //Is this already pinned with these evidence keys
        foreach(Case.CaseElement element in toCase.caseElements)
        {
            if(element.id == ev.evID)
            {
                //Get existing keys, do they match?
                List<Evidence.DataKey> keys = ev.GetTiedKeys(element.dk);

                bool foundExisting = false;

                foreach(Evidence.DataKey k in evKeys)
                {
                    if (!ev.preset.IsKeyUnique(k)) continue;

                    if(keys.Contains(k))
                    {
                        //Found matching data key; don't spawn another copy of this window...
                        Game.Log("Evidence: Found existing data key " + k.ToString() + " matches previously spawned evidence tied data keys...");
                        foundExisting = true;
                    }
                }

                if (foundExisting) return;
            }
        }

        //Find existing window
        InfoWindow existingWindow = InterfaceController.Instance.GetWindow(ev, evKeys);

        //Create a new pin
        Case.CaseElement newElement = new Case.CaseElement();
        newElement.caseID = toCase.id;
        newElement.n = ev.GetNameForDataKey(Evidence.DataKey.name) + "(" + ev.evID + ")";
        newElement.id = ev.evID;
        newElement.dk = evKeys;
        newElement.v = localPostion;
        newElement.ap = forceAutoPin;

        toCase.caseElements.Add(newElement);

        //Listen for evidence changes...
        ev.OnDataKeyChange += UpdatePinned;
        ev.OnDiscoverConnectedFact += UpdateStrings;
        ev.OnDiscoverChild += UpdateStrings;

        if (existingWindow != null)
        {
            newElement.w = true; //Existing window is open

            //Enable custom string creation
            if (existingWindow.pinButton.contextMenu.disabledItems.Contains("CreateCustomString"))
            {
                existingWindow.pinButton.contextMenu.disabledItems.Remove("CreateCustomString");
            }

            //If pinned, this is no longer a world interaction
            existingWindow.SetWorldInteraction(false);

            //Fire event (window)
            existingWindow.OnWindowPinnedChange(true, newElement);

            //Set to the window's evidence colour
            newElement.SetColour(existingWindow.evColour);

            //Fire event
            if (OnPinnedChange != null)
            {
                OnPinnedChange();
                OnPinEvidence(ev);
            }
        }

        //Run update
        UpdatePinned();

        if(controllerMode)
        {
            PinnedItemController pic = spawnedPins.Find(item => item.caseElement == newElement);

            if(pic != null)
            {
                SetSelectedPinned(pic);
            }
        }

        //Fire pinned event
        ev.OnPinnedChange();
    }

    //Unpin from corkboard (by evidence)
    public void UnPinFromCasePanel(Case thisCase, Evidence ev, List<Evidence.DataKey> evKeys, bool uniqueKeysOnly = false, Case.CaseElement forceElement = null)
    {
        //Return if null
        if (ev == null) return;

        List<Case.CaseElement> elements = thisCase.caseElements.FindAll(item => item.id == ev.evID);

        List<Case.CaseElement> toRemove = new List<Case.CaseElement>();

        foreach(Case.CaseElement el in elements)
        {
            List<Evidence.DataKey> keys = ev.GetTiedKeys(el.dk);

            foreach(Evidence.DataKey k in evKeys)
            {
                if (uniqueKeysOnly && !ev.preset.IsKeyUnique(k)) continue; //Ignore keys that aren't unique

                if (keys.Contains(k))
                {
                    toRemove.Add(el);
                }
            }
        }

        if(forceElement != null && !toRemove.Contains(forceElement))
        {
            toRemove.Add(forceElement);
        }

        while(toRemove.Count > 0)
        {
            Game.Log("Interface: Removing case element " + ev.evID);
            thisCase.caseElements.Remove(toRemove[0]);
            toRemove.RemoveAt(0);
        }

        //Listen for evidence changes...
        ev.OnDataKeyChange -= UpdatePinned;
        ev.OnDiscoverConnectedFact -= UpdateStrings;
        ev.OnDiscoverChild -= UpdateStrings;

        //Find existing window
        InfoWindow existingWindow = InterfaceController.Instance.GetWindow(ev, evKeys);

        if(existingWindow != null)
        {
            //Disable custom string creation
            if (!existingWindow.pinButton.contextMenu.disabledItems.Contains("CreateCustomString"))
            {
                existingWindow.pinButton.contextMenu.disabledItems.Add("CreateCustomString");
            }

            //Fire event (window)
            existingWindow.OnWindowPinnedChange(false, null);
        }

        UpdatePinned();

        //Fire event
        if (OnPinnedChange != null)
        {
            OnPinnedChange();
            OnUnpinEvidence(ev);
        }

        //Fire pinned event
        ev.OnPinnedChange();
    }

    //Update pinned- spawn or destroy
    public void UpdatePinned()
    {
        //Spawn pins
        List<Case.CaseElement> required = new List<Case.CaseElement>();
        if (activeCase != null) required.AddRange(activeCase.caseElements);

        for (int i = 0; i < spawnedPins.Count; i++)
        {
            PinnedItemController spawned = spawnedPins[i];

            if(required.Contains(spawned.caseElement))
            {
                //Update the tooltip text
                spawned.UpdateTooltipText();

                required.Remove(spawned.caseElement);
            }
            else
            {
                Destroy(spawned.gameObject);
                spawnedPins.RemoveAt(i);
                i--;
                continue;
            }
        }

        //Spawn required
        foreach (Case.CaseElement req in required)
        {
            GameObject newObj = Instantiate(PrefabControls.Instance.casePanelObject, pinnedContainer);
            PinnedItemController pic = newObj.GetComponent<PinnedItemController>();

            pic.Setup(req);
            req.pinnedController = pic;

            spawnedPins.Add(pic);

            pic.rect.localPosition = req.v;
            Game.Log("Set pinned local pos from case element (spawn): " + req.v);
        }

        //Add/remove strings
        UpdateStrings();

        //Pulsate evidence for pick mode
        if(pickModeActive && pickForField != null)
        {
            foreach (PinnedItemController pic in spawnedPins)
            {
                if (pic.evidenceButton == null) continue;
                EvidenceCitizen cit = pic.evidence as EvidenceCitizen;

                if(cit != null && pickForField.question.inputType == Case.InputType.citizen && pic.caseElement.dk.Contains(Evidence.DataKey.name))
                {
                    pic.evidenceButton.juice.Pulsate(true, false);
                }
                else
                {
                    EvidenceLocation loc = pic.evidence as EvidenceLocation;

                    if(loc != null && pickForField.question.inputType == Case.InputType.location)
                    {
                        pic.evidenceButton.juice.Pulsate(true, false);
                    }
                    else if(pic.evidence != null && pic.evidence.interactable != null && pickForField.question.inputType == Case.InputType.item)
                    {
                        pic.evidenceButton.juice.Pulsate(true, false);
                    }
                    else
                    {
                        pic.evidenceButton.juice.Pulsate(false, false);
                    }
                }
            }
        }
        else
        {
            foreach(PinnedItemController pic in spawnedPins)
            {
                if(pic.evidenceButton != null) pic.evidenceButton.juice.Pulsate(false, false);
            }
        }
    }

    //Update strings- spawn or destroy
    public void UpdateStrings()
    {
       List<StringConnection> required = new List<StringConnection>();

        //Look through spawned items for discovered facts
        foreach(PinnedItemController pic in spawnedPins)
        {
            if (pic == null) continue;
            if (pic.evidence == null) continue;
            if (pic.caseElement == null) continue;

            List<Evidence.FactLink> facts = pic.evidence.GetFactsForDataKey(pic.caseElement.dk);
            //Game.Log("Evidence: String update; found " + facts.Count + " facts for " + pic.evidence.GetNameForDataKey(Evidence.DataKey.name) + "...");

            foreach(Evidence.FactLink link in facts)
            {
                if (link == null) continue;

                //fact is found
                if(link.fact != null && link.fact.isFound)
                {
                    //Game.Log("Evidence: ... Fact is found: " + link.fact.GetName());

                    List<Evidence> otherEv;
                    List<Evidence.DataKey> otherKeys;

                    if (link.thisIsTheFromEvidence)
                    {
                        //Game.Log("Evidence: ... This is the from evidence (" + pic.evidence.GetNameForDataKey(Evidence.DataKey.name) + ")");
                        otherEv = link.fact.toEvidence;
                        otherKeys = link.fact.toDataKeys;
                    }
                    else continue; //If this isn't the from evidence, this loop will cycle around to the link where it is.

                    //Search other evidence
                    foreach (Evidence ev in otherEv)
                    {
                        if (ev == null) continue;

                        //Is found
                        if(ev.isFound)
                        {
                            PinnedItemController otherPinned = ev.GetPinned(otherKeys);

                            //Is pinned
                            if (otherPinned != null)
                            {
                                //Game.Log("Evidence: ... Found other evidence (" + otherPinned.evidence.GetNameForDataKey(Evidence.DataKey.name) + ")");

                                //Find existing
                                int index = required.FindIndex(item => (item.from == pic && item.to == otherPinned) || (item.from == otherPinned && item.to == pic));

                                if(index > -1)
                                {
                                    //Add to links reference
                                    required[index].links.Add(link);
                                    if (!required[index].facts.Contains(link.fact)) required[index].facts.Add(link.fact);
                                }
                                else
                                {
                                    StringConnection str = new StringConnection(pic, otherPinned);
                                    str.links.Add(link);

                                    if (!str.facts.Contains(link.fact)) str.facts.Add(link.fact);

                                    required.Add(str);
                                }
                            }
                            else
                            {
                                //Game.Log("Evidence: ... Other evidence (" + ev.GetNameForDataKey(Evidence.DataKey.name) + ") is not pinned");
                            }
                        }
                        else
                        {
                            //Game.Log("Evidence: ... Other evidence (" + ev.GetNameForDataKey(Evidence.DataKey.name) + ") is not found");
                        }
                    }
                }
            }
        }

        //Game.Log("Required: " + required.Count);

        for (int i = 0; i < spawnedStrings.Count; i++)
        {
            StringController spawned = spawnedStrings[i];

            if (spawned == null)
            {
                spawnedStrings.RemoveAt(i);
                i--;
                continue;
            }

            int index = required.FindIndex(item => item == spawned.connection);

            if(index > -1)
            {
                //Update connection reference
                spawned.connection = required[index];

                //Update the tooltip text
                //spawned.UpdateTooltipText();

                //Update the string colour
                //spawned.UpdateStringColour();

                required.Remove(spawned.connection);
            }
            else
            {
                spawned.connection.from.OnMoved -= spawned.UpdatePosition;
                spawned.connection.to.OnMoved -= spawned.UpdatePosition;

                Destroy(spawned.gameObject);
                spawnedStrings.RemoveAt(i);
                i--;
                continue;
            }
        }

        //Spawn required
        foreach (StringConnection inst in required)
        {
            GameObject newObj = Instantiate(PrefabControls.Instance.stringLink, stringContainer);
            StringController str = newObj.GetComponent<StringController>();

            str.Setup(inst);

            spawnedStrings.Add(str);
        }
    }

    //Set custom string link selection
    public void CustomStringLinkSelection(PinnedItemController pinnedItem, bool holdButtonMode = false)
    {
        if(customStringLinkSelection == pinnedItem)
        {
            return;
        }
        else if(customLinkSelectionMode)
        {
            CancelCustomStringLinkSelection();
        }
        else
        {
            customStringLinkSelection = pinnedItem;
            StartCoroutine(CustomStringLink(holdButtonMode));
        }
    }

    IEnumerator CustomStringLink(bool holdButtonMode = false)
    {
        Game.Log("Interface: Start custom string link selection...");
        customLinkSelectionMode = true;

        RectTransform fromRect = null;

        if (customString == null)
        {
            Game.Log("Interface: Creating new selection linking string...");
            GameObject customLink = Instantiate(PrefabControls.Instance.customStringLinkSelect, stringContainer);
            customString = customLink.GetComponent<RectTransform>();
            InterfaceController.Instance.UpdateCursorSprite();

            fromRect = customStringLinkSelection.pinButtonController.rect;
        }

        int waitFrames = 0;

        while(customStringLinkSelection != null && customLinkSelectionMode && fromRect != null)
        {
            //Cancel if something other than a click on another pin happens
            if(!InterfaceController.Instance.desktopMode)
            {
                CancelCustomStringLinkSelection();
            }

            if(!holdButtonMode)
            {
                //Cancel with mosue 2
                if ((Input.GetMouseButtonDown(1) && InputController.Instance.mouseInputMode))
                {
                    CancelCustomStringLinkSelection();
                }

                //Cancel with create string control
                if (InputController.Instance.player.GetButtonDown("CreateString"))
                {
                    CancelCustomStringLinkSelection();
                }
            }

            if((Input.GetMouseButtonDown(0) && InputController.Instance.mouseInputMode && !holdButtonMode) || (!InputController.Instance.player.GetButton("CreateString") && holdButtonMode) ||(waitFrames > 2 && InputController.Instance.player.GetButtonDown("Select") && !InputController.Instance.mouseInputMode))
            {
                GameObject selected = null;

                if (InterfaceController.Instance.selectedPinned.Count > 0)
                {
                    selected = InterfaceController.Instance.selectedPinned[InterfaceController.Instance.selectedPinned.Count - 1].gameObject;
                }

                //GameObject selected = EventSystem.current.currentSelectedGameObject;
                Game.Log("Interface: String link target: " + selected);

                if(selected == null)
                {
                    CancelCustomStringLinkSelection();
                }
                //Can be linked to: Pin pinned, pin folder, folder, pinned
                else
                {
                    PinnedItemController pinnedFolder = selected.GetComponentInParent<PinnedItemController>();

                    if(pinnedFolder != null && pinnedFolder != customStringLinkSelection)
                    {
                        Game.Log("Interface: Found pin folder: " + pinnedFolder);
                        FinishCustomStringLinkSelection(pinnedFolder);
                    }
                }
            }

            //Set position to objects - offset
            Vector3 differenceVector = InterfaceControls.Instance.caseBoardCursorRBContainer.position - fromRect.position;

            //Calculate with based on whether this is a file link
            customString.sizeDelta = new Vector2(differenceVector.magnitude, customString.sizeDelta.y);

            //Set angle
            float angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
            customString.rotation = Quaternion.Euler(0, 0, angle);

            customString.position = fromRect.position;
            waitFrames++;

            yield return null;
        }

        if(customLinkSelectionMode)
        {
            CancelCustomStringLinkSelection();
        }
    }

    private void OnDisable()
    {
        CancelCustomStringLinkSelection();
        if(customString != null) Destroy(customString.gameObject);
        SetPickModeActive(false, null);
    }

    public void CancelCustomStringLinkSelection()
    {
        Game.Log("Interface: End string link selection mode");
        customStringLinkSelection = null;
        customLinkSelectionMode = false;
        if (customString != null) Destroy(customString.gameObject);
        InterfaceController.Instance.UpdateCursorSprite();
    }

    public void FinishCustomStringLinkSelection(PinnedItemController target)
    {
        Game.Log("Interface: Finish string link: " + target);

        newestCreatedFact = EvidenceCreator.Instance.CreateFact("CustomLink", customStringLinkSelection.evidence, target.evidence, overrideFromKeys: customStringLinkSelection.caseElement.dk, overrideToKeys: target.caseElement.dk, isCustomFact: true) as FactCustom;

        if (customString != null) Destroy(customString.gameObject);
        customStringLinkSelection = null; //Cancel placement
        customLinkSelectionMode = false;
        InterfaceController.Instance.UpdateCursorSprite();

        //Name link
        PopupMessageController.Instance.PopupMessage("CustomFactRename", true, true, "Cancel", "Continue", anyButtonClosesMsg: true, newPauseState: PopupMessageController.AffectPauseState.no, true, Strings.Get("evidence.generic", "customlink"), enableColourPicker: true);
        PopupMessageController.Instance.OnLeftButton += OnCancelCustomFact;
        PopupMessageController.Instance.OnRightButton += OnContinueFactName;
    }

    public void OnContinueFactName()
    {
        try
        {
            if (newestCreatedFact != null)
            {
                 newestCreatedFact.SetCustomName(PopupMessageController.Instance.inputField.text);

                //Set colour
                UpdateStrings();

                StringController foundString = spawnedStrings.Find(item => item != null && item.connection != null && item.connection.facts.Exists(item2 => item2 == newestCreatedFact));
                foundString.SetColour(PopupMessageController.Instance.colourPicker.GetCurrentSelectedEvidenceColourValue());
            }

            PopupMessageController.Instance.OnLeftButton -= OnCancelCustomFact;
            PopupMessageController.Instance.OnRightButton -= OnContinueFactName;
        }
        catch
        {
            //Error here for some reason???
        }
    }

    //Remove fact
    public void OnCancelCustomFact()
    {
        for (int i = 0; i < newestCreatedFact.fromEvidence.Count; i++)
        {
            newestCreatedFact.fromEvidence[i].RemoveFactLink(newestCreatedFact);
        }

        for (int i = 0; i < newestCreatedFact.toEvidence.Count; i++)
        {
            newestCreatedFact.toEvidence[i].RemoveFactLink(newestCreatedFact);
        }

        GameplayController.Instance.factList.Remove(newestCreatedFact); //Remove from fact list

        PopupMessageController.Instance.OnLeftButton -= OnCancelCustomFact;
        PopupMessageController.Instance.OnRightButton -= OnContinueFactName;
    }

    public void UpdateResolveNotifications()
    {
        if (activeCase == null || !activeCase.isActive)
        {
            closeCaseButton.notifications.SetNotifications(0);
        }
        else
        {
            if(activeCase.caseStatus == Case.CaseStatus.handInCollected)
            {
                int invalidEntries = 0;

                foreach(Case.ResolveQuestion q in activeCase.resolveQuestions)
                {
                    if (!q.isValid || q.progress < 1f) invalidEntries++;
                }

                closeCaseButton.notifications.SetNotifications(invalidEntries);
            }
            else closeCaseButton.notifications.SetNotifications(0);
        }
    }

    public void SetPickModeActive(bool val, InputFieldController forField)
    {
        pickModeActive = val;
        //Game.Log("Pick mode: " + pickModeActive);
        pickForField = forField;

        InterfaceController.Instance.UpdateCursorSprite();

        //Make resolve controller active/inactive
        if (ResolveController.Instance != null)
        {
            if (pickModeActive)
            {
                ResolveController.Instance.wcc.window.windowCanvasGroup.alpha = 0.02f;
                ResolveController.Instance.wcc.window.contentCanvasGroup.alpha = 0.02f;
                ResolveController.Instance.wcc.window.windowCanvasGroup.blocksRaycasts = false;
                ResolveController.Instance.wcc.window.contentCanvasGroup.blocksRaycasts = false;

                if(!InputController.Instance.mouseInputMode)
                {
                    SetControllerMode(true, ControllerSelectMode.caseBoard);
                }
            }
            else
            {
                ResolveController.Instance.wcc.window.windowCanvasGroup.alpha = 1;
                ResolveController.Instance.wcc.window.contentCanvasGroup.alpha = 1;
                ResolveController.Instance.wcc.window.windowCanvasGroup.blocksRaycasts = true;
                ResolveController.Instance.wcc.window.contentCanvasGroup.blocksRaycasts = true;

                if (!InputController.Instance.mouseInputMode)
                {
                    SetControllerMode(true, ControllerSelectMode.windows);
                }
            }
        }

        UpdatePinned(); //Update this to highlight correct pinned items
        UpdateCloseCaseButton();
    }

    public void OnShowCaseBoard()
    {
        SetControllerMode(!InputController.Instance.mouseInputMode, currentSelectMode);
    }

    public void OnHideCaseBoard()
    {
        SetControllerMode(false, currentSelectMode);

        //Deselect element
        if (InterfaceController.Instance.selectedElement != null)
        {
            InterfaceController.Instance.selectedElement.OnDeselect(); //Select the notebook button
        }
    }

    public void SetControllerMode(bool isActive, ControllerSelectMode newMode)
    {
        if(isActive != controllerMode)
        {
            Game.Log("Menu: Set caseboard controller section active: " + isActive);
            controllerMode = isActive;
        }

        if(isActive)
        {
            //Must have active windows to switch to window mode
            if(newMode == ControllerSelectMode.windows)
            {
                if (InterfaceController.Instance.activeWindows.Count <= 0) return;
            }
            else
            {
                InterfaceController.Instance.RemoveWindowFocus();
            }

            if(newMode != ControllerSelectMode.caseBoard)
            {
                //Close quick menu
                if (PinnedItemController.activeQuickMenu != null)
                {
                    PinnedItemController.activeQuickMenu.Remove();
                }
            }

            //Disallow case and window mode if in upgrades screen
            if((UpgradesController.Instance.isOpen || BioScreenController.Instance.isOpen) && newMode != ControllerSelectMode.topBar)
            {
                Game.Log("Menu: Cancelling caseboard mode as upgrades/inventory is open...");
                return;
            }

            if(newMode != currentSelectMode)
            {
                if (ContextMenuController.activeMenu != null)
                {
                    ContextMenuController.activeMenu.ForceClose();
                }

                //Deselect element
                if (InterfaceController.Instance.selectedElement != null)
                {
                    InterfaceController.Instance.selectedElement.OnDeselect(); //Select the notebook button
                }
            }

            currentSelectMode = newMode;

            Game.Log("Menu: Set caseboard controller section mode: " + currentSelectMode);

            if (currentSelectMode == ControllerSelectMode.topBar)
            {
                topMenuSelect.gameObject.SetActive(true);
                upgradesSelect.gameObject.SetActive(true);
                boardSelect.gameObject.SetActive(false);
                mapSelect.gameObject.SetActive(true);

                //If this is selected, find another...
                if (InterfaceController.Instance.selectedElement == null || InterfaceController.Instance.selectedElementTag != "TopPanelButtons")
                {
                    //If inventory is open, select the holster button
                    if(BioScreenController.Instance.isOpen)
                    {
                        BioScreenController.Instance.selectNothingButton.OnSelect();
                    }
                    else
                    {
                        if (selectedTopBarButton != null)
                        {
                            selectedTopBarButton.OnSelect();
                        }
                        else notebookButton.OnSelect(); //Select the notebook button
                    }
                }

                caseboardMO.ForceMouseOver(false);
                caseboardScroll.controlEnabled = false;

                mapMO.ForceMouseOver(false);
                mapScroll.controlEnabled = false;

                upgradesMO.ForceMouseOver(UpgradesController.Instance.isOpen);
                upgradesScroll.controlEnabled = UpgradesController.Instance.isOpen;
            }
            else if(currentSelectMode == ControllerSelectMode.caseBoard)
            {
                topMenuSelect.gameObject.SetActive(false);
                upgradesSelect.gameObject.SetActive(false);
                boardSelect.gameObject.SetActive(true);
                mapSelect.gameObject.SetActive(false);

                caseboardMO.ForceMouseOver(true);
                caseboardScroll.controlEnabled = true;

                mapMO.ForceMouseOver(false);
                mapScroll.controlEnabled = false;

                upgradesMO.ForceMouseOver(false);
                upgradesScroll.controlEnabled = false;

                //Select a pinned item
                if (selectedPinned == null)
                {
                    if (spawnedPins.Count > 0)
                    {
                        SetSelectedPinned(GetClosestPinnedToCentre());
                    }
                }
            }
            else if(currentSelectMode == ControllerSelectMode.windows)
            {
                topMenuSelect.gameObject.SetActive(false);
                upgradesSelect.gameObject.SetActive(false);
                boardSelect.gameObject.SetActive(false);
                mapSelect.gameObject.SetActive(false);

                //Select a window
                if (selectedWindow == null)
                {
                    if(InterfaceController.Instance.activeWindows.Count > 0)
                    {
                        SetSelectedWindow(InterfaceController.Instance.activeWindows[0]);
                    }
                    else
                    {
                        SetControllerMode(true, ControllerSelectMode.caseBoard);
                    }
                }

                mapMO.ForceMouseOver(false);
                mapScroll.controlEnabled = false;

                caseboardMO.ForceMouseOver(false);
                caseboardScroll.controlEnabled = false;

                upgradesMO.ForceMouseOver(false);
                upgradesScroll.controlEnabled = false;
            }
        }
        else
        {
            topMenuSelect.gameObject.SetActive(false);
            upgradesSelect.gameObject.SetActive(false);
            boardSelect.gameObject.SetActive(false);
            mapSelect.gameObject.SetActive(false);

            caseboardScroll.controlEnabled = false;
            mapScroll.controlEnabled = false;
            upgradesScroll.controlEnabled = false;
        }

        //Update all windows regardless
        foreach (InfoWindow w in InterfaceController.Instance.activeWindows)
        {
            w.UpdateControllerSelected();
        }

        InteractionController.Instance.UpdateInteractionText();
    }

    public void SetSelectedWindow(InfoWindow newWindow, bool forceUpdate = false)
    {
        if(selectedWindow != newWindow || forceUpdate)
        {
            selectedWindow = newWindow;

            if (ContextMenuController.activeMenu != null)
            {
                ContextMenuController.activeMenu.ForceClose();
            }

            Game.Log("Menu: Set new controller selected window: " + newWindow.name);

            foreach(InfoWindow w in InterfaceController.Instance.activeWindows)
            {
                if(selectedWindow == w)
                {
                    w.SetSelected(true);
                }
                else
                {
                    w.SetSelected(false);
                }
            }

            if (selectedWindow != null && currentSelectMode == ControllerSelectMode.windows)
            {
                //Select pin button
                if (InterfaceController.Instance.selectedElement != null)
                {
                    InterfaceController.Instance.selectedElement.OnDeselect();
                }

                if (selectedWindow.pinButton != null)
                {
                    selectedWindow.pinButton.OnSelect();
                }
                else if (selectedWindow.activeContent != null)
                {
                    //Select top button
                    List<ButtonController> contentButtons = selectedWindow.activeContent.GetComponentsInChildren<ButtonController>(true).ToList();

                    float top = -99999999999;
                    ButtonController selectThis = null;

                    foreach (ButtonController b in contentButtons)
                    {
                        if (selectThis == null || b.transform.position.y > top)
                        {
                            top = b.transform.position.y;
                            selectThis = b;
                        }
                    }

                    if (selectThis != null)
                    {
                        selectThis.OnSelect();
                    }
                    else if (selectedWindow.closable && selectedWindow.closeButton != null)
                    {
                        selectedWindow.closeButton.OnSelect();
                    }
                    else if (selectedWindow.tabs.Count > 0)
                    {
                        selectedWindow.tabs[0].tabButton.gameObject.GetComponent<ButtonController>().OnSelect();
                    }
                }
            }
        }
    }

    public void SetSelectedPinned(PinnedItemController newPinned, bool forceUpdate = false)
    {
        if (selectedPinned != newPinned || forceUpdate)
        {
            selectedPinned = newPinned;

            if(ContextMenuController.activeMenu != null)
            {
                ContextMenuController.activeMenu.ForceClose();
            }

            Game.Log("Menu: Set new controller selected pinned: " + newPinned.name);

            if (selectedPinned != null && currentSelectMode == ControllerSelectMode.caseBoard)
            {
                //Select pin button
                if (InterfaceController.Instance.selectedElement != null)
                {
                    InterfaceController.Instance.selectedElement.OnDeselect();
                }

                //Close quick menu
                if(PinnedItemController.activeQuickMenu != null && PinnedItemController.activeQuickMenu.parentPinned != selectedPinned)
                {
                    PinnedItemController.activeQuickMenu.Remove();
                }

                foreach(PinnedItemController pic in spawnedPins)
                {
                    if(pic == selectedPinned)
                    {
                        pic.SetSelected(true, false);
                    }
                    else
                    {
                        pic.SetSelected(false, false);
                    }
                }

                selectedPinned.evidenceButton.OnSelect();
            }

            InteractionController.Instance.UpdateInteractionText(); //Update text to show custom string action
        }
    }

    public void ControllerNavigate(Vector2 direction)
    {
        //Window select
        if(ContextMenuController.activeMenu == null)
        {
            if (currentSelectMode == ControllerSelectMode.caseBoard)
            {
                if (InterfaceController.Instance.selectedElement != null)
                {
                    //Look for appropriate windows in this onscreen direction...
                    PinnedItemController bestPinned = null;
                    ButtonController bestButton = null;
                    float closest = Mathf.Infinity;

                    foreach (PinnedItemController w in spawnedPins)
                    {
                        if (w == selectedPinned) continue; //Don't include currently selected...

                        Vector2 difference = w.rect.position - InterfaceController.Instance.selectedElement.transform.position;
                        float dist = Vector2.Distance(w.rect.position, InterfaceController.Instance.selectedElement.transform.position);

                        if (direction.x > 0 && difference.x > 0)
                        {
                            if (dist < closest)
                            {
                                bestPinned = w;
                                closest = dist;
                            }
                        }
                        else if (direction.x < 0 && difference.x < 0)
                        {
                            if (dist < closest)
                            {
                                bestPinned = w;
                                closest = dist;
                            }
                        }
                        else if (direction.y > 0 && difference.y > 0)
                        {
                            if (dist < closest)
                            {
                                bestPinned = w;
                                closest = dist;
                            }
                        }
                        else if (direction.y < 0 && difference.y < 0)
                        {
                            if (dist < closest)
                            {
                                bestPinned = w;
                                closest = dist;
                            }
                        }
                    }

                    //Look for quick menu icons...
                    if (PinnedItemController.activeQuickMenu != null)
                    {
                        foreach(ButtonController bc in PinnedItemController.activeQuickMenu.activeButtons)
                        {
                            if (InterfaceController.Instance.selectedElement == bc.button) continue;
                            if (bc == null) continue;

                            Vector2 difference = bc.rect.position - InterfaceController.Instance.selectedElement.transform.position;
                            float dist = Vector2.Distance(bc.rect.position, InterfaceController.Instance.selectedElement.transform.position);

                            if (direction.x > 0 && difference.x > 0)
                            {
                                if (dist < closest)
                                {
                                    bestButton = bc;
                                    closest = dist;
                                }
                            }
                            else if (direction.x < 0 && difference.x < 0)
                            {
                                if (dist < closest)
                                {
                                    bestButton = bc;
                                    closest = dist;
                                }
                            }
                            else if (direction.y > 0 && difference.y > 0)
                            {
                                if (dist < closest)
                                {
                                    bestButton = bc;
                                    closest = dist;
                                }
                            }
                            else if (direction.y < 0 && difference.y < 0)
                            {
                                if (dist < closest)
                                {
                                    bestButton = bc;
                                    closest = dist;
                                }
                            }
                        }
                    }

                    if(bestButton != null)
                    {
                        InterfaceController.Instance.selectedElement.OnDeselect();
                        bestButton.OnSelect();
                        bestButton.AutoScroll();
                    }
                    else if (bestPinned != null)
                    {
                        SetSelectedPinned(bestPinned);
                        bestPinned.evidenceButton.AutoScroll(); //Manually autoscroll; automatic messes up movement
                    }
                }
                else if(spawnedPins.Count > 0)
                {
                    SetSelectedPinned(GetClosestPinnedToCentre());
                }
            }
        }
    }

    public void ShoulderNavigate(bool right)
    {
        if (ContextMenuController.activeMenu == null)
        {
            if (currentSelectMode == ControllerSelectMode.topBar && !mapScroll.controlEnabled)
            {
                if (InterfaceController.Instance.selectedElement != null)
                {
                    Selectable sel = InterfaceController.Instance.selectedElement.button.FindSelectableOnLeft();
                    if (right) sel = InterfaceController.Instance.selectedElement.button.FindSelectableOnRight();

                    if (sel != null)
                    {
                        InterfaceController.Instance.selectedElement.OnDeselect(); //Fire exit event
                        ButtonController nextButton = sel.gameObject.GetComponent<ButtonController>();
                        if (nextButton != null) nextButton.OnSelect();
                    }
                }
            }
            else if (currentSelectMode == ControllerSelectMode.windows)
            {
                if (selectedWindow != null)
                {
                    InfoWindow bWindow = null;
                    float lowestDiff = Mathf.Infinity;

                    foreach (InfoWindow w in InterfaceController.Instance.activeWindows)
                    {
                        if (w == selectedWindow) continue;

                        float xDiff = selectedWindow.transform.position.x - w.transform.position.x;

                        if ((right && xDiff <= 0f) || (!right && xDiff > 0f))
                        {
                            if (Mathf.Abs(xDiff) < lowestDiff)
                            {
                                bWindow = w;
                                lowestDiff = Mathf.Abs(xDiff);
                            }
                        }
                    }

                    if (bWindow != null)
                    {
                        InterfaceController.Instance.RemoveWindowFocus();
                        SetSelectedWindow(bWindow);
                    }
                }
            }
        }
    }

    public PinnedItemController GetClosestPinnedToCentre()
    {
        PinnedItemController ret = null;
        float closest = Mathf.Infinity;

        foreach(PinnedItemController pic in spawnedPins)
        {
            float dist = Vector3.Distance(pic.transform.position, boardSelect.transform.position);

            if(dist < closest)
            {
                ret = pic;
                closest = dist;
            }
        }

        return ret;
    }
}
