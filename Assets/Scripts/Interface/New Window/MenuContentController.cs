﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MenuContentController : MonoBehaviour
{
    public WindowContentController windowContent;
    public InfoWindow parentWindow;
    public TextMeshProUGUI descriptionText;

    private void OnEnable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        if (windowContent == null) windowContent = this.gameObject.GetComponentInParent<WindowContentController>();
        parentWindow.passedEvidence.OnDataKeyChange += CheckEnabled;
        parentWindow.OnWindowRefresh += CheckEnabled;

        CheckEnabled();
    }

    private void OnDisable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange -= CheckEnabled;
        parentWindow.OnWindowRefresh -= CheckEnabled;
    }

    public void CheckEnabled()
    {
        Company comp = (parentWindow.passedEvidence.controller as NewAddress).company;

        string str = "\n\n" + "<align=center><b><cspace=0.3em><smallcaps>" + comp.name + " " + Strings.Get("evidence.names", "Menu") + "</smallcaps><align=left></b></cspace>";

        //Add opening hours
        if (comp.retailOpenHours.x == 0f && comp.retailOpenHours.y == 24f)
        {
            str += "\n\n" + Strings.Get("evidence.generic", "Open") + " " + Strings.Get("evidence.generic", "24 hours") + ", ";
        }
        else
        {
            str += "\n\n" + Strings.Get("evidence.generic", "Open") + " " + SessionData.Instance.DecimalToClockString(comp.retailOpenHours.x, false) + " - " + SessionData.Instance.DecimalToClockString(comp.retailOpenHours.y, false) + ", ";
        }

        //Add opening days
        //Use syntax, open all week except x...
        if (comp.daysOpen.Count >= 7)
        {
            str += Strings.Get("evidence.generic", "every day");
        }
        else if (comp.daysOpen.Count >= 5)
        {
            str += Strings.Get("evidence.generic", "every day") + " " + Strings.Get("evidence.generic", "except") + " ";

            for (int i = 0; i < comp.daysClosed.Count; i++)
            {
                str += Strings.Get("ui.interface", comp.daysClosed[i].ToString());
                if (i < comp.daysClosed.Count - 1) str += "& ";
            }
        }
        else
        {
            for (int i = 0; i < comp.daysClosed.Count; i++)
            {
                str += Strings.Get("ui.interface", comp.daysClosed[i].ToString());
                if (i < comp.daysClosed.Count - 1) str += ", ";
            }
        }

        str += ".\n";

        //Add items for sale
        Dictionary<RetailItemPreset, float> food = new Dictionary<RetailItemPreset, float>();
        Dictionary<RetailItemPreset, float> snacks = new Dictionary<RetailItemPreset, float>();
        Dictionary<RetailItemPreset, float> drinks = new Dictionary<RetailItemPreset, float>();

        foreach (KeyValuePair<InteractablePreset, int> pair in comp.prices)
        {
            if(pair.Key.retailItem != null)
            {
                if (pair.Key.retailItem.menuCategory == RetailItemPreset.MenuCategory.food)
                {
                    food.Add(pair.Key.retailItem, pair.Value);
                }
                else if (pair.Key.retailItem.menuCategory == RetailItemPreset.MenuCategory.snacks)
                {
                    snacks.Add(pair.Key.retailItem, pair.Value);
                }
                else if (pair.Key.retailItem.menuCategory == RetailItemPreset.MenuCategory.drinks)
                {
                    drinks.Add(pair.Key.retailItem, pair.Value);
                }
            }
        }

        if (food.Count > 0)
        {
            str += "\n\n" + "<align=center><b><cspace=0.3em><smallcaps>" + Strings.Get("evidence.generic", "Food") + "</smallcaps><align=left></b></cspace>";

            foreach (KeyValuePair<RetailItemPreset, float> pair in food)
            {
                str += "\n" + Strings.Get("evidence.names", pair.Key.name) + "<pos=70%>" + CityControls.Instance.cityCurrency + Toolbox.Instance.AddZeros(Toolbox.Instance.RoundToPlaces(pair.Value, 2), 2);
            }
        }

        if (snacks.Count > 0)
        {
            str += "\n\n" + "<align=center><b><cspace=0.3em><smallcaps>" + Strings.Get("evidence.generic", "Snacks") + "</smallcaps><align=left></b></cspace>";

            foreach (KeyValuePair<RetailItemPreset, float> pair in snacks)
            {
                str += "\n" + Strings.Get("evidence.names", pair.Key.name) + "<pos=70%>" + CityControls.Instance.cityCurrency + Toolbox.Instance.AddZeros(Toolbox.Instance.RoundToPlaces(pair.Value, 2), 2);
            }
        }

        if (drinks.Count > 0)
        {
            str += "\n\n" + "<align=center><b><cspace=0.3em><smallcaps>" + Strings.Get("evidence.generic", "Drink") + "</smallcaps><align=left></b></cspace>";

            foreach (KeyValuePair<RetailItemPreset, float> pair in drinks)
            {
                str += "\n" + Strings.Get("evidence.names", pair.Key.name) + "<pos=70%>" + CityControls.Instance.cityCurrency + Toolbox.Instance.AddZeros(Toolbox.Instance.RoundToPlaces(pair.Value, 2), 2);
            }
        }

        descriptionText.text = str.Trim();
    }
}
