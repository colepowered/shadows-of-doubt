﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ToggleController : MonoBehaviour
{
    [Header("Components")]
    public ButtonController onButton;
    public ButtonController offButton;

    [Header("State")]
    public bool isOn = true;

    [Header("Configuration")]
    public string playerPrefsID;

    private void Start()
    {
        //Assign button events
        onButton.button.onClick.AddListener(delegate { SetOn(); });
        offButton.button.onClick.AddListener(delegate { SetOff(); });
    }

    public void SetIsOnWithoutNotify(bool val)
    {
        isOn = val;
        ButtonsVisualUpdate();
    }

    public void SetOn()
    {
        isOn = true;
        OnValueChange();
        ButtonsVisualUpdate();
    }

    public void SetOff()
    {
        isOn = false;
        OnValueChange();
        ButtonsVisualUpdate();
    }

    public void ButtonsVisualUpdate()
    {
        if (isOn)
        {
            onButton.icon.enabled = true;
            offButton.icon.enabled = false;

            onButton.text.fontStyle = FontStyles.Underline;
            offButton.text.fontStyle = FontStyles.Normal;
        }
        else
        {
            onButton.icon.enabled = false;
            offButton.icon.enabled = true;

            onButton.text.fontStyle = FontStyles.Normal;
            offButton.text.fontStyle = FontStyles.Underline;
        }
    }

    public void OnValueChange()
    {
        //Game.Log("Value changed: " + isOn);

        if (playerPrefsID != null && playerPrefsID.Length > 0)
        {
            PlayerPrefsController.Instance.OnToggleChanged(playerPrefsID, true, this);
        }
    }
}
