﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Data;

public class ComputerLogin : CruncherAppContent
{
    public ComputerOSMultiSelect loginSelection;

    public TextMeshProUGUI inputText;
    public TextMeshProUGUI instructionText;
    public List<int> input = new List<int>();
    public Color defaultTextColour;
    public GameObject numPadParent;

    public bool checking = false;
    public bool correct = false;
    public float checkCounter = 0f;

    public bool inputCodeActive = false;

    public override void OnSetup()
    {
        base.OnSetup();

        loginSelection.Setup(controller);

        //Refresh multi selection
        List<ComputerOSMultiSelect.OSMultiOption> newOptions = new List<ComputerOSMultiSelect.OSMultiOption>();

        //Accounts depend on 1) inhabitants if placed at residence, or 2) owners of furniture + admin if at company
        if (controller.ic.interactable.node.gameLocation.thisAsAddress != null)
        {
            if (controller.ic.interactable.node.gameLocation.thisAsAddress.residence != null)
            {
                foreach (Human h in controller.ic.interactable.node.gameLocation.thisAsAddress.inhabitants)
                {
                    ComputerOSMultiSelect.OSMultiOption newOption = new ComputerOSMultiSelect.OSMultiOption { text = h.GetInitialledName(), human = h };
                    newOptions.Add(newOption);
                }
            }
            else
            {
                //Users are owners of the furniture
                foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> pair in controller.ic.interactable.furnitureParent.ownerMap)
                {
                    if(pair.Key.human != null)
                    {
                        ComputerOSMultiSelect.OSMultiOption newOption = new ComputerOSMultiSelect.OSMultiOption { text = pair.Key.human.GetInitialledName(), human = pair.Key.human };
                        newOptions.Add(newOption);
                    }
                }

                //Add company admin (boss)
                if (controller.ic.interactable.node != null && controller.ic.interactable.node.gameLocation.thisAsAddress != null && controller.ic.interactable.node.gameLocation.thisAsAddress.company != null)
                {
                    if (controller.ic.interactable.node.gameLocation.thisAsAddress.company.director != null)
                    {
                        ComputerOSMultiSelect.OSMultiOption newOption = new ComputerOSMultiSelect.OSMultiOption { text = controller.ic.interactable.node.gameLocation.thisAsAddress.company.shortName + "_Admin", human = controller.ic.interactable.node.gameLocation.thisAsAddress.company.director };
                        newOptions.Add(newOption);
                    }
                }
            }
        }

        //Update login with new users
        loginSelection.UpdateElements(newOptions);

        loginSelection.OnNewSelection += OnNewUserSelected;
        OnNewUserSelected();

        //Clear current code
        ClearCode(false); //Clear on start to set display
    }

    private void OnDestroy()
    {
        loginSelection.OnNewSelection -= OnNewUserSelected;
    }

    public void OnNewUserSelected()
    {
        if(loginSelection.selected != null)
        {
            numPadParent.SetActive(true);
            instructionText.text = Strings.Get("computer", "Enter Passcode");

            //Display passcode in debug
            List<int> password = loginSelection.selected.option.human.passcode.GetDigits();
            Game.Log("Player: Correct code is " + password[0] + password[1] + password[2] + password[3]);

            //Auto submit
            if(GameplayController.Instance.acquiredPasscodes.Contains(loginSelection.selected.option.human.passcode))
            {
                OnInputCode(loginSelection.selected.option.human.passcode.GetDigits());
            }
        }
        else
        {
            numPadParent.SetActive(false);
            instructionText.text = Strings.Get("computer", "Select User");
        }
    }

    public void PressNumberButton(int newInt)
    {
        if (checking) return; //No input during grace period

        input.Add(newInt);
        string inputString = string.Empty;

        for (int i = 0; i < input.Count; i++)
        {
            inputString += input[i].ToString();
        }

        while (inputString.Length < 4)
        {
            inputString += "_";
        }

        inputText.text = inputString;

        //Play button audio
        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerKeyboardKey, Player.Instance, Player.Instance.currentNode, this.transform.position);

        if (input.Count >= 4)
        {
            SubmitCode();
        }
    }

    public void ClearCode(bool press = true)
    {
        checkCounter = 0f;
        checking = false;
        correct = false;
        inputText.color = defaultTextColour;

        input.Clear();
        inputText.text = "____";

        //Play button audio
        if (press)
        {
            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerKeyboardKey, Player.Instance, Player.Instance.currentNode, this.transform.position);
        }
    }

    //Called when 4 characters are submitted
    public void SubmitCode()
    {
        if(loginSelection.selected == null)
        {
            ClearCode();
            return;
        }

        //Is this correct?
        List<int> password = loginSelection.selected.option.human.passcode.GetDigits();

        if (password == null || password.Count <= 0)
        {
            Game.Log("Wrong code: Unable to get password from human.");

            //Wrong!
            inputText.color = Color.red;
            correct = false;
            checking = true;
            return;
        }

        for (int i = 0; i < input.Count; i++)
        {
            if (input[i] != password[i])
            {
                //Wrong!
                Game.Log("Player: Correct code is " + password[0] + password[1] + password[2] + password[3]);

                //Play sound here as it's not tied to an action
                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerInvalidPasscode, Player.Instance, Player.Instance.currentNode, this.transform.position);

                inputText.color = Color.red;
                correct = false;
                checking = true;
                return;
            }
        }

        //Add to bypassed locks
        //if (inter.thisDoor != null)
        //{
        //    if (!Player.Instance.bypassedLocks.Contains(inter.thisDoor.identifier))
        //    {
        //        Player.Instance.bypassedLocks.Add(inter.thisDoor.identifier);
        //    }
        //}

        //Set text to green
        inputText.color = Color.green;
        correct = true;
        checking = true;

        //Add passcode
        GameplayController.Instance.AddPasscode(loginSelection.selected.option.human.passcode);

        //Play sound here as it's not tied to an action
        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerValidPasscode, Player.Instance, Player.Instance.currentNode, this.transform.position);
    }

    //Detect input
    private void Update()
    {
        if(Player.Instance.computerInteractable == controller.ic.interactable)
        {
            if(!InterfaceController.Instance.playerTextInputActive)
            {
                if (InputController.Instance.player.GetButtonDown("0"))
                {
                    PressNumberButton(0);
                }
                else if (InputController.Instance.player.GetButtonDown("1"))
                {
                    PressNumberButton(1);
                }
                else if (InputController.Instance.player.GetButtonDown("2"))
                {
                    PressNumberButton(2);
                }
                else if (InputController.Instance.player.GetButtonDown("3"))
                {
                    PressNumberButton(3);
                }
                else if (InputController.Instance.player.GetButtonDown("4"))
                {
                    PressNumberButton(4);
                }
                else if (InputController.Instance.player.GetButtonDown("5"))
                {
                    PressNumberButton(5);
                }
                else if (InputController.Instance.player.GetButtonDown("6"))
                {
                    PressNumberButton(6);
                }
                else if (InputController.Instance.player.GetButtonDown("7"))
                {
                    PressNumberButton(7);
                }
                else if (InputController.Instance.player.GetButtonDown("8"))
                {
                    PressNumberButton(8);
                }
                else if (InputController.Instance.player.GetButtonDown("9"))
                {
                    PressNumberButton(9);
                }
            }
        }


        if (checking)
        {
            checkCounter += Time.deltaTime;

            if (checkCounter >= 0.3f)
            {
                //Close this evidence
                if (correct)
                {
                    //Log in
                    controller.SetLoggedIn(loginSelection.selected.option.human);

                    //If we've reached here the password is correct
                    //Exit the login app to go to desktop
                    controller.OnAppExit();
                }
                else
                {
                    ClearCode(false);
                }

                checkCounter = 0f;
                checking = false;
                correct = false;
            }
        }
    }

    public void OnInputCode(List<int> code, float keyDelay = 0.15f)
    {
        if (!inputCodeActive)
        {
            ClearCode(false);
            StartCoroutine(InputCode(code, keyDelay));
        }
    }

    IEnumerator InputCode(List<int> code, float keyDelay = 0.15f)
    {
        inputCodeActive = true;
        int codeCursor = 0;

        while (codeCursor < 4)
        {
            if (!checking)
            {
                PressNumberButton(code[codeCursor]);
                codeCursor++;
            }

            yield return new WaitForSeconds(keyDelay);
        }

        inputCodeActive = false;
    }
}
