using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlsDisplayController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public RectTransform anchor;
    public GameObject controlDisplayPrefab;

    [Header("Settings")]
    public Vector2 padding = new Vector2(24f, 24f);
    public float animationSelectTime = 0.5f;
    public AnimationCurve controlSelectAnimation;
    public float controlSelectScaleLerp = 2f;
    public Color controlSelectColorLerp = Color.white;
    public Color audioFullColor = Color.white;
    public Color audioEmptyColor = Color.white;
    float posChangeProgress = 1f;
    float desiredYPos = 0f;
    float desiredHeight = 0f;
    float desiredRectFromLeft = 0f;
    float desiredRectFromRight = 0f;
    [Tooltip("Minimum display time in gametime")]
    public float minimumCustomControlDisplayTimeInterval = 0.15f;

    [Header("State")]
    public List<ControlDisplayController> spawned = new List<ControlDisplayController>();
    public List<CustomActionsDisplayed> customActionsDisplayed = new List<CustomActionsDisplayed>();
    public List<InteractablePreset.InteractionKey> disableControlDisplay = new List<InteractablePreset.InteractionKey>();

    public class CustomActionsDisplayed
    {
        public InteractablePreset.InteractionKey key;
        public string interactionName;
        public float displayTime = 0f;
        public float lastDisplayedAt = 0f;
        public Interactable.InteractableCurrentAction action;
    }


    //Singleton pattern
    private static ControlsDisplayController _instance;
    public static ControlsDisplayController Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void UpdateControlDisplay()
    {
        //What controls are required?
        List<InteractablePreset.InteractionKey> requiredKeys = new List<InteractablePreset.InteractionKey>();
        List<InteractionController.InteractionSetting> requiredActions = new List<InteractionController.InteractionSetting>();

        foreach(KeyValuePair<InteractablePreset.InteractionKey, InteractionController.InteractionSetting> pair in InteractionController.Instance.currentInteractions)
        {
            if (pair.Key == InteractablePreset.InteractionKey.none) continue;
            if (pair.Value.currentAction != null && pair.Value.currentAction.action != null && pair.Value.currentAction.action.disableUIDisplay) continue;

            //If cannot find assigned control...
            bool assignedControl = true;
            ControlsDisplayController.Instance.GetControlIcon(pair.Key, out _, out assignedControl);
            if (!assignedControl) continue;

            if (pair.Value.currentSetting != null && pair.Value.currentSetting.enabled && pair.Value.currentSetting.display)
            {
                requiredKeys.Add(pair.Key);
                requiredActions.Add(pair.Value);
            }
        }

        for (int i = 0; i < spawned.Count; i++)
        {
            ControlDisplayController cdc = spawned[i];

            int keyIndex = requiredKeys.IndexOf(cdc.key);

            if(keyIndex > -1)
            {
                //Update because action name could be different
                cdc.UpdateDisplay(cdc.key, requiredActions[keyIndex]);

                requiredKeys.RemoveAt(keyIndex);
                requiredActions.RemoveAt(keyIndex);
            }
            else
            {
                cdc.Remove();
                i--;
            }
        }

        //Spawn remaining
        while(requiredKeys.Count > 0)
        {
            GameObject newControlDisp = Instantiate(controlDisplayPrefab, anchor);
            ControlDisplayController cdc = newControlDisp.GetComponent<ControlDisplayController>();

            foreach(CanvasRenderer r in cdc.renderers)
            {
                r.SetAlpha(0f);
            }

            cdc.UpdateDisplay(requiredKeys[0], requiredActions[0]);
            spawned.Add(cdc);

            requiredKeys.RemoveAt(0);
            requiredActions.RemoveAt(0);
        }

        //Figure out desired positions...
        Dictionary<ControlDisplayController.ControlPositioning, List<ControlDisplayController>> positioning = new Dictionary<ControlDisplayController.ControlPositioning, List<ControlDisplayController>>();

        foreach(ControlDisplayController cdc in spawned)
        {
            if(!positioning.ContainsKey(cdc.positioning))
            {
                positioning.Add(cdc.positioning, new List<ControlDisplayController>());
            }

            positioning[cdc.positioning].Add(cdc);
        }

        //Sort each category
        foreach(KeyValuePair<ControlDisplayController.ControlPositioning, List<ControlDisplayController>> pair in positioning)
        {
            pair.Value.Sort((p2, p1) => p1.interactionSetting.priority.CompareTo(p2.interactionSetting.priority)); //Highest first
        }

        //Arrange Y
        List<ControlDisplayController> top = new List<ControlDisplayController>();
        List<ControlDisplayController> middle = new List<ControlDisplayController>();
        List<ControlDisplayController> neutral = new List<ControlDisplayController>();
        List<ControlDisplayController> bottom = new List<ControlDisplayController>();

        if (positioning.ContainsKey(ControlDisplayController.ControlPositioning.up))
        {
            top.AddRange(positioning[ControlDisplayController.ControlPositioning.up]);
        }

        if (positioning.ContainsKey(ControlDisplayController.ControlPositioning.down))
        {
            bottom.AddRange(positioning[ControlDisplayController.ControlPositioning.down]);
        }

        if (positioning.ContainsKey(ControlDisplayController.ControlPositioning.left))
        {
            middle.InsertRange(0, positioning[ControlDisplayController.ControlPositioning.left]);
        }

        if (positioning.ContainsKey(ControlDisplayController.ControlPositioning.right))
        {
            middle.AddRange(positioning[ControlDisplayController.ControlPositioning.right]);
        }

        if (positioning.ContainsKey(ControlDisplayController.ControlPositioning.neutral))
        {
            neutral.AddRange(positioning[ControlDisplayController.ControlPositioning.neutral]);
        }

        //Now position from bottom up
        Vector2 pos = Vector2.zero;

        if(bottom.Count > 0)
        {
            float width = Mathf.Min(rect.rect.width / (float)bottom.Count - ((float)(bottom.Count - 1) * padding.x), 330);

            float contentWidth = width * (float)bottom.Count + ((float)(bottom.Count - 1) * padding.x);
            pos.x = contentWidth * -0.5f + width * 0.5f;

            for (int i = 0; i < bottom.Count; i++)
            {
                ControlDisplayController cdc = bottom[i];
                cdc.desiredPosition = new Vector2(pos.x, pos.y);

                cdc.rect.sizeDelta = new Vector2(width, cdc.rect.sizeDelta.y);
                pos.x += width + padding.x;

                if(!cdc.assignedSpawnPosition)
                {
                    cdc.spawnPosition = cdc.desiredPosition + new Vector2(64, 0);
                    if(cdc.desiredPosition.x < 0) cdc.spawnPosition = cdc.desiredPosition - new Vector2(64, 0);
                    cdc.rect.anchoredPosition = cdc.spawnPosition;
                    cdc.assignedSpawnPosition = true;
                }
            }

            pos.y += 56 + padding.y;
        }

        if (neutral.Count > 0)
        {
            float width = Mathf.Min(rect.rect.width / (float)neutral.Count - ((float)(neutral.Count - 1) * padding.x), 330);

            float contentWidth = width * (float)neutral.Count + ((float)(neutral.Count - 1) * padding.x);
            pos.x = contentWidth * -0.5f + width * 0.5f;

            for (int i = 0; i < neutral.Count; i++)
            {
                ControlDisplayController cdc = neutral[i];
                cdc.desiredPosition = new Vector2(pos.x, pos.y);

                cdc.rect.sizeDelta = new Vector2(width, cdc.rect.sizeDelta.y);
                pos.x += width + padding.x;

                if (!cdc.assignedSpawnPosition)
                {
                    cdc.spawnPosition = cdc.desiredPosition + new Vector2(64, 0);
                    if (cdc.desiredPosition.x < 0) cdc.spawnPosition = cdc.desiredPosition - new Vector2(64, 0);
                    cdc.rect.anchoredPosition = cdc.spawnPosition;
                    cdc.assignedSpawnPosition = true;
                }
            }

            pos.y += 56 + padding.y;
        }

        if (middle.Count > 0)
        {
            float width = Mathf.Min(rect.rect.width / (float)middle.Count - ((float)(middle.Count - 1) * padding.x), 330);

            float contentWidth = width * (float)middle.Count + ((float)(middle.Count - 1) * padding.x);
            pos.x = contentWidth * -0.5f + width * 0.5f;

            for (int i = 0; i < middle.Count; i++)
            {
                ControlDisplayController cdc = middle[i];
                cdc.desiredPosition = new Vector2(pos.x, pos.y);

                cdc.rect.sizeDelta = new Vector2(width, cdc.rect.sizeDelta.y);
                pos.x += width + padding.x;

                if (!cdc.assignedSpawnPosition)
                {
                    cdc.spawnPosition = cdc.desiredPosition + new Vector2(64, 0);
                    if (cdc.desiredPosition.x < 0) cdc.spawnPosition = cdc.desiredPosition - new Vector2(64, 0);
                    cdc.rect.anchoredPosition = cdc.spawnPosition;
                    cdc.assignedSpawnPosition = true;
                }
            }

            pos.y += 56 + padding.y;
        }

        if (top.Count > 0)
        {
            float width = Mathf.Min(rect.rect.width / (float)top.Count - ((float)(top.Count - 1) * padding.x), 330);

            float contentWidth = width * (float)top.Count + ((float)(top.Count - 1) * padding.x);
            pos.x = contentWidth * -0.5f + width * 0.5f;

            for (int i = 0; i < top.Count; i++)
            {
                ControlDisplayController cdc = top[i];
                cdc.desiredPosition = new Vector2(pos.x, pos.y);

                cdc.rect.sizeDelta = new Vector2(width, cdc.rect.sizeDelta.y);
                pos.x += width + padding.x;

                if (!cdc.assignedSpawnPosition)
                {
                    cdc.spawnPosition = cdc.desiredPosition + new Vector2(64, 0);
                    if (cdc.desiredPosition.x < 0) cdc.spawnPosition = cdc.desiredPosition - new Vector2(64, 0);
                    cdc.rect.anchoredPosition = cdc.spawnPosition;
                    cdc.assignedSpawnPosition = true;
                }
            }

            pos.y += 56 + padding.y;
        }

        if (spawned.Count > 0)
        {
            this.enabled = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach(ControlDisplayController cdc in spawned)
        {
            if (cdc == null) continue;

            //Animate position
            if(cdc.rect.anchoredPosition != cdc.desiredPosition)
            {
                cdc.rect.anchoredPosition = Vector2.Lerp(cdc.rect.anchoredPosition, cdc.desiredPosition, Time.deltaTime/0.1f);
            }
        }

        if(posChangeProgress < 1f)
        {
            posChangeProgress += Time.deltaTime / 0.1f;
            posChangeProgress = Mathf.Clamp01(posChangeProgress);

            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, Mathf.Lerp(rect.anchoredPosition.y, desiredYPos, posChangeProgress));
            rect.sizeDelta = new Vector2(rect.sizeDelta.x, Mathf.Lerp(rect.sizeDelta.y, desiredHeight, posChangeProgress));
            rect.offsetMin = new Vector2(Mathf.Lerp(rect.offsetMin.x, desiredRectFromLeft, posChangeProgress), rect.offsetMin.y);
            rect.offsetMax = new Vector2(Mathf.Lerp(rect.offsetMax.x, -desiredRectFromRight, posChangeProgress), rect.offsetMax.y);
        }

        if(spawned.Count <= 0 && posChangeProgress >= 1f)
        {
            this.enabled = false;
        }
    }

    //Restore the default display area
    public void RestoreDefaultDisplayArea()
    {
        Game.Log("Interface: Restore default control display area...");
        SetControlDisplayArea(144, 220, 300, 300);
    }

    //Update the display area
    public void SetControlDisplayArea(float yPos, float height, float rectFromLeft, float rectFromRight)
    {
        desiredYPos = yPos;
        desiredHeight = height;
        desiredRectFromLeft = rectFromLeft;
        desiredRectFromRight = rectFromRight;

        posChangeProgress = 0f;
        this.enabled = true;
    }

    //Display a control icon after a delay of x seconds
    public void DisplayControlIconAfterDelay(float afterSeconds, InteractablePreset.InteractionKey key, string interactionName, float forTime, bool overrideMinDisplayTime = false)
    {
        StartCoroutine(DisplayControlIconAfter(afterSeconds, key, interactionName, forTime, overrideMinDisplayTime));
    }

    IEnumerator DisplayControlIconAfter(float afterSeconds, InteractablePreset.InteractionKey key, string interactionName, float forTime, bool overrideMinDisplayTime)
    {
        while(afterSeconds > 0f)
        {
            afterSeconds -= Time.deltaTime;
            yield return null;
        }

        DisplayControlIcon(key, interactionName, forTime, overrideMinDisplayTime);
    }

    //Display a control icon for a certain amount of time
    public void DisplayControlIcon(InteractablePreset.InteractionKey key, string interactionName, float forTime, bool overrideMinDisplayTime = false)
    {
        if (!SessionData.Instance.startedGame || (CityConstructor.Instance != null && CityConstructor.Instance.preSimActive)) return;

        if (disableControlDisplay.Contains(key)) return;

        //Game.Log("Player: Display control icon " + key.ToString() + "...");

        CustomActionsDisplayed existingAction = customActionsDisplayed.Find(item => item.key == key);

        if (existingAction == null || (overrideMinDisplayTime || SessionData.Instance.gameTime > existingAction.lastDisplayedAt + minimumCustomControlDisplayTimeInterval))
        {
            if(existingAction == null)
            {
                CustomActionsDisplayed customAction = new CustomActionsDisplayed();
                customAction.key = key;
                customAction.interactionName = interactionName;
                customAction.displayTime = forTime;
                customAction.lastDisplayedAt = SessionData.Instance.gameTime;
                customAction.action = new Interactable.InteractableCurrentAction { display = true, enabled = true };
                customAction.action.overrideInteractionName = interactionName;
                customActionsDisplayed.Add(customAction);
            }
            else
            {
                existingAction.interactionName = interactionName;
                existingAction.displayTime = forTime;
                existingAction.lastDisplayedAt = SessionData.Instance.gameTime;
                existingAction.action.overrideInteractionName = interactionName;
            }

            InteractionController.Instance.UpdateInteractionText();
        }
        else
        {
            //Game.Log("Player: Didn't display control icon because minimum time hasn't elapsed: " + SessionData.Instance.gameTime + "/" + (existingAction.lastDisplayedAt + minimumCustomControlDisplayTimeInterval));
        }

        ////Find the control reference
        //InterfaceControls.ControlDisplayReferences reference = InterfaceControls.Instance.controlDisplay.Find(item => item.key == key);

        //if(reference != null)
        //{
        //    if(overrideMinDisplayTime || SessionData.Instance.gameTime > reference.lastDisplayedAt + reference.minimumTimeBetweenDisplay)
        //    {
        //        reference.displayTimer = forTime;
        //        reference.displayed = true;
        //        reference.lastDisplayedAt = SessionData.Instance.gameTime;
        //    }
        //    else
        //    {
        //        Game.Log("Player: Didn't display control icon because minimum time hasn't elapsed: " + SessionData.Instance.gameTime +"/" + (reference.lastDisplayedAt + reference.minimumTimeBetweenDisplay));
        //    }
        //}
    }

    public string GetControlIcon(InteractablePreset.InteractionKey key, out ControlDisplayController.ControlPositioning positioning, out bool foundControl)
    {
        foundControl = true;
        positioning = ControlDisplayController.ControlPositioning.neutral;

        if (key == InteractablePreset.InteractionKey.scrollAxisUp)
        {
            positioning = ControlDisplayController.ControlPositioning.up;
        }
        else if (key == InteractablePreset.InteractionKey.scrollAxisDown)
        {
            positioning = ControlDisplayController.ControlPositioning.down;
        }

        string searchStr = key.ToString();

        Rewired.ActionElementMap map = null;

        if (InputController.Instance.player !=  null)
        {
            map = InputController.Instance.player.controllers.maps.GetFirstElementMapWithAction(searchStr, true);
        }

        if (map != null)
        {
            string spriteCategory = "desktop";
            string spriteName = map.elementIdentifierName;
            string extraKeyText = string.Empty;

            if (map.controllerMap.controllerType == Rewired.ControllerType.Keyboard)
            {
                spriteCategory = "desktop";

                //Display a keyboard key
                if (spriteName.Length <= 1)
                {
                    spriteName = "Keyboard Key";
                    extraKeyText = map.elementIdentifierName.ToUpper() + "  ";
                }
                //Detect F keys
                else if (spriteName.Length <= 3)
                {
                    if(spriteName.Length > 0)
                    {
                        string f = spriteName.Substring(0, 1);

                        if (f == "F")
                        {
                            string number = spriteName.Substring(1, spriteName.Length - 1);

                            if (int.TryParse(number, out _))
                            {
                                spriteName = "Keyboard Key";
                                extraKeyText = map.elementIdentifierName.ToUpper() + "  ";
                            }
                        }
                    }
                }
                //Remove keypad references
                else if(spriteName.Length >= 7)
                {
                    string keypad = spriteName.Substring(0, 6);

                    if(keypad == "Keypad")
                    {
                        spriteName = "Keyboard Key";
                        extraKeyText = map.elementIdentifierName.ToUpper().Substring(7) + "  ";
                    }
                }
            }
            else if (map.controllerMap.controllerType == Rewired.ControllerType.Mouse)
            {
                spriteCategory = "desktop";

                if (spriteName == "Left Mouse Button")
                {
                    positioning = ControlDisplayController.ControlPositioning.left;
                }
                else if (spriteName == "Right Mouse Button")
                {
                    positioning = ControlDisplayController.ControlPositioning.right;
                }
            }
            else
            {
                //Game.Log("Controller name: " + map.controllerMap.controller.name + " type " + map.controllerMap.controllerType);

                //Default to xbox controller icons
                spriteCategory = "controller";

                //Convert switch stick button reference...
                if (spriteName == "Left Stick" || spriteName == "Right Stick")
                {
                    spriteName += " Button";
                }

                //Convert axis to display a singular icon
                if (spriteName == "Left Stick X" || spriteName == "Left Stick Y")
                {
                    spriteName = "Left Stick";
                }
                else if (spriteName == "Right Stick X" || spriteName == "Right Stick Y")
                {
                    spriteName = "Right Stick";
                }

                //For nintendo controllers use a different version of A, B, X, Y with different arrows, as they are in different positions...
                //Switch pro controller
                if(map.controllerMap.controller.name == "Pro Controller")
                {
                    if(spriteName == "X" || spriteName == "Y" || spriteName == "A" || spriteName == "B")
                    {
                        spriteName += "_Nintendo";
                    }
                }
            }

            return "<sprite=\"" + spriteCategory + "\" name=\"" + spriteName + "\">" + extraKeyText;
        }
        else
        {
            //Game.LogError("Unable to find action for " + key.ToString() + ", check your rewired actions config...");
            foundControl = false;
            return string.Empty;
        }
    }
}
