﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockpickProgressController : MonoBehaviour
{
    public float amount = 0f;
    public float barMax = 1f;
    public float progress = 0f;
    public RectTransform rect;
    public RectTransform bar;
    public JuiceController juice;
    public Color depletedColor;
    bool completed = false;

    public void SetBarMax(float val)
    {
        barMax = val;
        UpdateBar();
    }

    public void SetAmount(float val)
    {
        if(amount != val)
        {
            amount = val;
            UpdateBar();

            //Flash on progress
            if(progress >= 1f && !completed)
            {
                juice.Pulsate(false);
                juice.elements[0].originalColour = depletedColor;
                juice.Flash(1, false, speed: 5f);
                completed = true;
            }
        }
    }

    public void UpdateBar()
    {
        //Calculate progress
        progress = Mathf.Clamp01(amount / barMax);

        bar.sizeDelta = new Vector2(progress * (rect.sizeDelta.x - 31f), bar.sizeDelta.y);
        //Game.Log("Bar progress: " + progress + " / " + barMax + " amount: " + amount);
    }
}
