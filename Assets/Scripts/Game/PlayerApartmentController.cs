using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;
using UnityEngine.Rendering.HighDefinition;

public class PlayerApartmentController : MonoBehaviour
{
    [Header("Components")]
    public Transform placementCursor;

    [Header("Settings")]
    public Color placementValidLerpColour = Color.gray;
    public Color placementInvalidLerpColour = Color.red;
    public FirstPersonItem furnitureFPSItem;
    public FurnitureCluster nullCluster;

    [Header("State")]
    public List<Color> swatches = new List<Color>();
    public List<FurnitureLocation> furnitureStorage = new List<FurnitureLocation>();
    public List<Interactable> itemStorage = new List<Interactable>();
    [Space(7)]
    public bool furniturePlacementMode = false;
    public bool placeExistingRoomObject = false; //Are we placing something that exists in the room?
    public FurniturePlacement furnPlacement;
    public NewRoom furnPlacementRoom;
    public GameObject spawnedPlacementObj;
    public MeshRenderer spawnedPlacementMesh;
    public int furnitureRotation = 0;
    public List<Material> cloneMaterials = new List<Material>();
    public List<Collider> placementColliders = new List<Collider>();
    public float materialPulse = 0f;
    public Color lerpColour = Color.black;
    [Space(7)]
    public NewNode placementNode = null;
    public bool isPlacementValid = false;
    [Space(7)]
    public bool decoratingMode = false;
    public MaterialGroupPreset decoratingMaterial;
    public MaterialGroupPreset.MaterialType decoratingType = MaterialGroupPreset.MaterialType.walls;
    public Toolbox.MaterialKey decoratingKey;
    public NewRoom decoratingRoom;
    [Space(7)]
    public InfoWindow materialKeyWindow;

    [Space(7)]
    public WindowTabPreset.TabContentType rememberContent = WindowTabPreset.TabContentType.decor;
    public MaterialGroupPreset.MaterialType rememberDecorType = MaterialGroupPreset.MaterialType.walls;
    public FurnishingsController.TabState rememberRoomStorageShop = FurnishingsController.TabState.inRoom;
    public List<FurniturePreset.DecorClass> rememberDisplayClasses = new List<FurniturePreset.DecorClass>();
    public List<InteractablePreset.ItemClass> rememberItemDisplayClasses = new List<InteractablePreset.ItemClass>();

    [System.Serializable]
    public class PlayerFurniture
    {
        //Serialized
        public Toolbox.MaterialKey matKey;
        public string presetStr;

        //Non Serialized
        [System.NonSerialized]
        public FurniturePreset preset;
        [System.NonSerialized]
        public FurnitureLocation placement;
    }

    [System.Serializable]
    public class FurniturePlacement
    {
        public FurniturePreset preset; //Preset, whether new or existing
        public FurnitureLocation existing; //Existing furniture
        public Toolbox.MaterialKey materialKey; //Colour info
        public ArtPreset art;
        public NewNode anchorNode; //Node
        public List<NewNode> coversNodes = new List<NewNode>();
        public int angle;
        public Vector3 offset = Vector3.zero;
    }

    public delegate void FurnitureChange();
    public event FurnitureChange OnFurnitureChange;


    //Singleton pattern
    private static PlayerApartmentController _instance;
    public static PlayerApartmentController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        this.enabled = false;

        //Sort colour swatches
        SortSwatches();
    }

    [Button]
    public void SortSwatches()
    {
        //Sort colour swatches
        swatches.Sort((p1, p2) => Step(p1).CompareTo(Step(p2)));
    }

    public int Step(Color colour)
    {
        //float lum = Mathf.Sqrt(0.241f * colour.r + 0.691f * colour.g + 0.068f * colour.b);

        float h = 0;
        float s = 0;
        float v = 0;

        Color.RGBToHSV(colour, out h, out s, out v);
        //int h2 = Mathf.RoundToInt(h * repetitions);
        //int lum2 = Mathf.RoundToInt(lum * repetitions);
        //int v2 = Mathf.RoundToInt(v * repetitions);

        //if (h2 % 2 == 1)
        //{
        //    v2 = repetitions - v2;
        //    lum = repetitions - lum;
        //}

        return Mathf.RoundToInt(h * 1000) + Mathf.RoundToInt(s * 10) + Mathf.RoundToInt(v * 100);
    }

    public void BuyNewResidence(ResidenceController newHome, bool removePreviousResidence = false)
    {
        ResidenceController oldResidence = null;

        //Remove all furniture from previous and fit it in new if possible
        if (Player.Instance.residence != null && removePreviousResidence)
        {
            oldResidence = Player.Instance.residence;
            Player.Instance.residence.address.RemoveAllInhabitantFurniture(true, FurnitureClusterLocation.RemoveInteractablesOption.moveToStorage);
        }

        if(!Player.Instance.apartmentsOwned.Contains(newHome.address))
        {
            Player.Instance.apartmentsOwned.Add(newHome.address);
        }

        Player.Instance.SetResidence(newHome, removePreviousResidence);

        //Redecorate new
        if (Player.Instance.residence != null)
        {
            Player.Instance.residence.address.RemoveAllInhabitantFurniture(false, FurnitureClusterLocation.RemoveInteractablesOption.remove);

            //Reset doors
            foreach(NewNode.NodeAccess acc in Player.Instance.residence.address.entrances)
            {
                if(acc.door != null)
                {
                    if(acc.door.isJammed)
                    {
                        acc.door.SetJammed(false);
                    }

                    if (acc.door.policeTape != null)
                    {
                        Destroy(acc.door.policeTape);
                        Game.Log("Removing police tape at " + acc.door.name);
                    }
                }
            }
        }

        //Give key
        Player.Instance.AddToKeyring(newHome.address, true);

        //Update city directory text
        CityData.Instance.CreateCityDirectory();

        //Update for sale
        CitizenBehaviour.Instance.UpdateForSale();
    }

    //Set furniture placement mode
    public void SetFurniturePlacementMode(bool val, FurniturePlacement newPlacement, NewRoom forRoom, bool newPlaceExistingRoomObject = false, bool forceUpdate = false)
    {
        if (furniturePlacementMode != val || forceUpdate)
        {
            furniturePlacementMode = val;

            Game.Log("Decor: Set furniture placement mode: " + furniturePlacementMode);

            if (furniturePlacementMode)
            {
                furnitureRotation = 0; //Reset rotation
                placeExistingRoomObject = newPlaceExistingRoomObject;

                if(decoratingMode)
                {
                    SetDecoratingMode(false, null);
                }

                //Delete existing
                if(GetExistingFurniture() != null)
                {
                    Game.Log("Decor: Removing existing furniture " + GetExistingFurniture());
                    GetExistingFurniture().Delete(true, FurnitureClusterLocation.RemoveInteractablesOption.moveToStorage);
                }

                furnPlacement = newPlacement;
                furnPlacementRoom = forRoom;
                this.enabled = true;

                if (BioScreenController.Instance.isOpen) BioScreenController.Instance.SetInventoryOpen(false, true);
                FirstPersonItemController.Instance.SetFirstPersonItem(furnitureFPSItem, true);
            }
            else
            {
                placeExistingRoomObject = false;
                ResetExisting();

                if (materialKeyWindow != null)
                {
                    Game.Log("Decor: Closing material key window");
                    materialKeyWindow.CloseWindow(false);
                }

                if (BioScreenController.Instance.isOpen) BioScreenController.Instance.SetInventoryOpen(false, true);

                //Find an empty slot...
                FirstPersonItemController.InventorySlot emptySlot = FirstPersonItemController.Instance.slots.Find(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.holster);
                if(emptySlot != null) BioScreenController.Instance.SelectSlot(emptySlot, false, true);
            }

            FirstPersonItemController.Instance.UpdateCurrentActions();
            InterfaceController.Instance.UpdateDOF();
        }
    }

    //Open a new material window, or update an existing one with new settings
    public InfoWindow OpenOrUpdateMaterialWindow(FurniturePreset furn, Toolbox.MaterialKey useKey, MaterialGroupPreset newSelection)
    {
        if (materialKeyWindow != null)
        {
            Game.Log("Decor: Found an existing new material window...");
        }
        else
        {
            Game.Log("Decor: Opening a new material window...");
            materialKeyWindow = InterfaceController.Instance.SpawnWindow(null, presetName: "MaterialKey");
        }

        MaterialKeyController keyController = materialKeyWindow.GetComponentInChildren<MaterialKeyController>();

        if(furn != null)
        {
            keyController.UpdateButtonsBasedOnFurniture(furn);
        }
        else
        {
            keyController.UpdateButtonsBasedOnMaterial(newSelection.material, false, MaterialKeyController.SliderPickerType.grub);
            keyController.SetButtonsToKey(useKey);
        }

        keyController.UpdatePlacementText();

        return materialKeyWindow;
    }

    public void SetDecoratingMode(bool val, MaterialGroupPreset materialPreset, MaterialGroupPreset.MaterialType editType = MaterialGroupPreset.MaterialType.walls, Toolbox.MaterialKey editKey = new Toolbox.MaterialKey(), NewRoom forRoom = null)
    {
        if(decoratingMode != val)
        {
            decoratingMode = val;

            Game.Log("Decor: Set decorating mode: " + decoratingMode);

            if (decoratingMode)
            {
                if(furniturePlacementMode)
                {
                    SetFurniturePlacementMode(false, null, null);
                }

                decoratingMaterial = materialPreset;
                decoratingType = editType;
                decoratingRoom = forRoom;
                this.enabled = true;

                if(BioScreenController.Instance.isOpen) BioScreenController.Instance.SetInventoryOpen(false, true);
                FirstPersonItemController.Instance.SetFirstPersonItem(furnitureFPSItem, true);

                ApplyDecor(decoratingType, decoratingMaterial, decoratingKey, false);
            }
            else
            {
                //Reset to original material
                RevertDecor(decoratingType);

                if(materialKeyWindow != null)
                {
                    Game.Log("Decor: Closing material key window");
                    materialKeyWindow.CloseWindow(false);
                }

                if (BioScreenController.Instance.isOpen) BioScreenController.Instance.SetInventoryOpen(false, true);

                //Select empty
                BioScreenController.Instance.SelectSlot(FirstPersonItemController.Instance.slots.Find(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.holster), forceUpdate: true);
            }

            InteractionController.Instance.UpdateInteractionText();
            InterfaceController.Instance.UpdateDOF();
        }
    }

    private void Update()
    {
        if (furniturePlacementMode)
        {
            bool createdThisFrame = false;

            if (spawnedPlacementObj == null && furnPlacement.preset != null)
            {
                Game.Log("Decor: Creating furniture spawn object " + furnPlacement.preset.name);
                spawnedPlacementObj = Instantiate(furnPlacement.preset.prefab, Player.Instance.transform);
                spawnedPlacementObj.transform.localPosition = Vector3.zero;
                spawnedPlacementMesh = spawnedPlacementObj.GetComponent<MeshRenderer>();

                //Remove collider
                placementColliders = spawnedPlacementObj.GetComponentsInChildren<Collider>().ToList();

                for (int i = 0; i < placementColliders.Count; i++)
                {
                    //Remove triggers from detecting placement obstacles
                    if(placementColliders[i].isTrigger)
                    {
                        placementColliders.RemoveAt(i);
                        i--;
                        continue;
                    }

                    //Set triggers to trigger
                    placementColliders[i].isTrigger = true;
                }

                UpdatePlacementColourKey();

                createdThisFrame = true;
            }

            isPlacementValid = true;

            placementNode = UpdateFurnitureDesiredPosition();
            furnPlacement.angle = furnitureRotation;

            //Can only move node-to-node
            if (placementNode != null && spawnedPlacementObj != null)
            {
                furnPlacement.anchorNode = placementNode;
                furnPlacement.coversNodes = new List<NewNode>();

                float cursorY = placementNode.position.y;

                //Calculate Y pos; ceiling furniture hangs down and is positioned @ top
                if (furnPlacement.preset.classes[0].ceilingPiece)
                {
                    cursorY += (placementNode.floor.defaultCeilingHeight * 0.1f) - (placementNode.floorHeight * 0.1f);
                }
                //else if(furnPlacement.preset.classes[0].wallPiece && isPlacementValid)
                //{
                //    cursorY = Mathf.RoundToInt(placementCursor.transform.position.y / 0.3f) * 0.3f;
                //}

                placementCursor.position = new Vector3(placementCursor.position.x, cursorY, placementCursor.position.z);
                furnPlacement.offset = placementCursor.position - placementNode.position; //Set offset
                furnPlacement.offset = new Vector3(furnPlacement.offset.x, 0, furnPlacement.offset.z);

                Toolbox.Instance.SetLightLayer(spawnedPlacementObj, Player.Instance.currentBuilding);

                //Is this node valid?
                //All nodes are within gamelocation
                //Rotate the offset using the angle we are checking...
                //Loop object's size and find the placement nodes.
                if(isPlacementValid)
                {
                    for (int i = 0; i < furnPlacement.preset.classes[0].objectSize.x; i++)
                    {
                        for (int u = 0; u < furnPlacement.preset.classes[0].objectSize.y; u++)
                        {
                            //Next get the furniture tile offset that's been rotated by both the angle and the facing
                            Vector2 furniturePos = Toolbox.Instance.RotateVector2CW(new Vector2(i, u), furnitureRotation);

                            Vector3Int offsetCoord = placementNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(furniturePos.x), Mathf.RoundToInt(furniturePos.y), 0);
                            NewNode foundNode = null;

                            //This node must be present in the room...
                            if (PathFinder.Instance.nodeMap.TryGetValue(offsetCoord, out foundNode))
                            {
                                furnPlacement.coversNodes.Add(foundNode);

                                //Must be in same room...
                                if (foundNode.gameLocation != furnPlacementRoom.gameLocation)
                                {
                                    Game.Log("Decor: Different room");
                                    isPlacementValid = false;
                                    break;
                                }

                                //Not allowed on stairwell
                                if (!furnPlacement.preset.classes[0].allowedOnStairwell)
                                {
                                    if (foundNode.tile.isStairwell)
                                    {
                                        Game.Log("Decor: Is Stairwell");
                                        isPlacementValid = false;
                                        break;
                                    }
                                    //else
                                    //{
                                    //    //Also search adjacent for stairwell nodes
                                    //    foreach (Vector2 sv2 in CityData.Instance.offsetArrayX4)
                                    //    {
                                    //        Vector3 nodeC = foundNode.nodeCoord + new Vector3(sv2.x, sv2.y, 0);

                                    //        NewNode sNode = null;

                                    //        //This node must be present in the room...
                                    //        if (PathFinder.Instance.nodeMap.TryGetValue(nodeC, out sNode))
                                    //        {
                                    //            if (sNode.tile.isStairwell)
                                    //            {
                                    //                Game.Log("Decor: Is Stairwell");
                                    //                isPlacementValid = false;
                                    //                break;
                                    //            }
                                    //        }
                                    //    }

                                    //    if (!isPlacementValid) break;
                                    //}
                                }

                                //Not allowed if no floor
                                if(foundNode.gameLocation.thisAsStreet == null)
                                {
                                    if (!furnPlacement.preset.classes[0].allowIfNoFloor && (foundNode.floorType == NewNode.FloorTileType.CeilingOnly || foundNode.floorType == NewNode.FloorTileType.none || foundNode.floorType == NewNode.FloorTileType.noneButIndoors))
                                    {
                                        Game.Log("Decor: no floor");
                                        isPlacementValid = false;
                                        break;
                                    }
                                }

                                //Ceiling piece
                                if (furnPlacement.preset.classes[0].ceilingPiece)
                                {
                                    if (foundNode.floorType == NewNode.FloorTileType.floorOnly || foundNode.floorType == NewNode.FloorTileType.none || foundNode.floorType == NewNode.FloorTileType.noneButIndoors)
                                    {
                                        Game.Log("Decor: Ceiling piece");
                                        isPlacementValid = false;
                                        break;
                                    }

                                    //Don't allow a light to spawn on ducts...
                                    if (foundNode.airDucts.Exists(item => item.level == 1) || (foundNode.floor.defaultCeilingHeight > 42 && foundNode.airDucts.Exists(item => item.level == 2)))
                                    {
                                        Game.Log("Decor: Ceiling piece");
                                        isPlacementValid = false;
                                        break;
                                    }

                                    //Can't be placed here if other ceiling items are here...
                                    if (foundNode.individualFurniture.Exists(item => item.furniture.classes[0].ceilingPiece && item.furniture.classes[0].blocksCeiling))
                                    {
                                        Game.Log("Decor: Ceiling piece");
                                        isPlacementValid = false;
                                        break;
                                    }
                                }
                                else if (furnPlacement.preset.classes[0].requiresCeiling)
                                {
                                    if (foundNode.floorType == NewNode.FloorTileType.floorOnly || foundNode.floorType == NewNode.FloorTileType.none || foundNode.floorType == NewNode.FloorTileType.noneButIndoors)
                                    {
                                        Game.Log("Decor: Requires ceiling");
                                        isPlacementValid = false;
                                        break;
                                    }
                                }

                                //Check for physically colliding objects
                                int layerMask = Toolbox.Instance.CreateLayerMask(Toolbox.LayerMaskMode.onlyCast, 0, 29);

                                foreach(Collider c in placementColliders)
                                {
                                    List<Collider> overlap = Physics.OverlapBox(c.bounds.center, c.bounds.extents * 0.9f, Quaternion.identity, layerMask, QueryTriggerInteraction.Ignore).ToList();

                                    foreach(Collider overlapping in overlap)
                                    {
                                        bool hitFloorOrCeiling = false;

                                        foreach (NewAddress ad in Player.Instance.apartmentsOwned)
                                        {
                                            foreach (NewRoom r in ad.rooms)
                                            {
                                                if (r.combinedFloor != null && overlapping.gameObject == r.combinedFloor)
                                                {
                                                    //Game.Log("Decor: Overlapping hits floor. This is ok.");
                                                    hitFloorOrCeiling = true;
                                                    break;
                                                }
                                                else if (r.combinedCeiling != null && overlapping.gameObject == r.combinedCeiling)
                                                {
                                                    //Game.Log("Decor: Overlapping hits ceiling. This is ok.");
                                                    hitFloorOrCeiling = true;
                                                    break;
                                                }

                                                if (hitFloorOrCeiling) break;
                                            }

                                            if (hitFloorOrCeiling) break;
                                        }

                                        if (hitFloorOrCeiling) continue;

                                        if (!placementColliders.Contains(overlapping))
                                        {
                                            Game.Log("Decor: Overlapping fail: " + c.name + " overlaps " + overlapping.name);
                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    if(!isPlacementValid) break;
                                }
                            }
                            else
                            {
                                Game.Log("Decor: Invalid node");
                                isPlacementValid = false;
                            }

                            //Invalid location for this furniture, escape size loop...
                            if (!isPlacementValid)
                            {
                                break;
                            }
                        }

                        //Invalid location for this furniture, escape size loop...
                        if (!isPlacementValid)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    Game.Log("Decor: Invalid node");
                    isPlacementValid = false;
                }
            }
            else
            {
                Game.Log("Decor: Invalid node");
                isPlacementValid = false;
            }

            //Lerp object to cursor
            if (createdThisFrame)
            {
                //Snap to position immediately
                spawnedPlacementObj.transform.position = placementCursor.transform.position;
                spawnedPlacementObj.transform.rotation = placementCursor.transform.rotation;
            }
            else
            {
                float lerp = Time.deltaTime * 20f;

                if (Vector3.Distance(spawnedPlacementObj.transform.position, placementCursor.transform.position) < 0.1f) lerp = 1f;

                spawnedPlacementObj.transform.position = Vector3.Lerp(spawnedPlacementObj.transform.position, placementCursor.transform.position, lerp);
                spawnedPlacementObj.transform.rotation = Quaternion.Slerp(spawnedPlacementObj.transform.rotation, placementCursor.transform.rotation, lerp);
            }

            //Lerp placement colour
            Color lerpMat = placementValidLerpColour;

            if (!isPlacementValid)
            {
                lerpMat = placementInvalidLerpColour;
            }

            materialPulse += Time.deltaTime;

            if (materialPulse > 2f)
            {
                materialPulse = 0f;
            }

            if (materialPulse <= 1f)
            {
                lerpColour = Color.Lerp(Color.black, lerpMat, materialPulse);

                foreach (Material mat in cloneMaterials)
                {
                    mat.SetColor("_EmissionColour", lerpColour);
                    mat.SetColor("_EmissiveColor", lerpColour);
                }
            }
            else
            {
                lerpColour = Color.Lerp(lerpMat, Color.black, materialPulse - 1f);

                foreach (Material mat in cloneMaterials)
                {
                    mat.SetColor("_EmissionColour", lerpColour);
                    mat.SetColor("_EmissiveColor", lerpColour);
                }
            }

            //Detect out of area
            if(furnPlacementRoom == null || Player.Instance.currentGameLocation != furnPlacementRoom.gameLocation)
            {
                SetFurniturePlacementMode(false, null, null);
            }
        }
        else if(decoratingMode)
        {
            //Detect out of area
            if (decoratingRoom == null || Player.Instance.currentGameLocation != decoratingRoom.gameLocation)
            {
                SetDecoratingMode(false, null);
            }
        }
        else
        {
            RemoveBeingPlaced();

            this.enabled = false;
        }
    }

    public void RemoveBeingPlaced()
    {
        if (spawnedPlacementObj != null)
        {
            Destroy(spawnedPlacementObj);
        }

        foreach (Material mat in cloneMaterials)
        {
            Destroy(mat);
        }

        cloneMaterials.Clear();
    }

    public FurnitureLocation GetExistingFurniture()
    {
        if (furnPlacement != null && furnPlacement.existing != null && furnPlacement.existing.id != 0)
        {
            //Game.Log("Decor: Found existing furniture: " + furnPlacement);
            return furnPlacement.existing;
        }
        else return null;
    }

    public void UpdatePlacementColourKey()
    {
        if(spawnedPlacementObj != null && furniturePlacementMode)
        {
            Game.Log("Decor: Updating placement colour key...");
            MeshRenderer[] rends = spawnedPlacementObj.GetComponentsInChildren<MeshRenderer>();

            foreach (MeshRenderer rend in rends)
            {
                MaterialsController.Instance.ApplyMaterialKey(rend, furnPlacement.materialKey);
            }

            while(cloneMaterials.Count > 0)
            {
                Destroy(cloneMaterials[0]);
                cloneMaterials.RemoveAt(0);
            }

            foreach (MeshRenderer r in rends)
            {
                Material mat = r.sharedMaterial;

                if(mat != null)
                {
                    Material cloneMat = Instantiate(mat) as Material;
                    cloneMaterials.Add(cloneMat);
                    r.sharedMaterial = cloneMat;

                    //Art material
                    if(furnPlacement.art != null && r.transform.tag == "Art")
                    {
                        Material artMat = furnPlacement.art.material;
                        Game.Log("Decor: Found art material, applying " + artMat.name);

                        //Try to load dynamic text image
                        if (furnPlacement.art.useDynamicText)
                        {
                            if (!GameplayController.Instance.dynamicTextImages.TryGetValue(furnPlacement.art, out mat))
                            {
                                artMat = furnPlacement.art.material;
                            }
                        }

                        r.sharedMaterial = artMat;
                    }
                }
            }

            //Is this a decal projector?
            if(furnPlacement.art != null)
            {
                DecalProjector dec = spawnedPlacementObj.GetComponentInChildren<DecalProjector>();

                if (dec != null)
                {
                    dec.material = furnPlacement.art.material;

                    //Set size to tex size * 0.02
                    Texture tex = dec.material.GetTexture("_BaseColorMap");
                    dec.size = new Vector3(tex.width * furnPlacement.art.pixelScaleMultiplier, tex.height * furnPlacement.art.pixelScaleMultiplier, 0.13f);
                }
            }
        }
    }

    private NewNode UpdateFurnitureDesiredPosition()
    {
        NewNode ret = null;

        if (spawnedPlacementMesh != null)
        {
            placementCursor.localPosition = new Vector3(0, 0, spawnedPlacementMesh.bounds.size.z * 0.5f + 1f);

            //Position centre by bounds
            //Vector3 boundsLocal = Player.Instance.transform.InverseTransformPoint(spawnedPlacementMesh.bounds.center);

            float altX = 0f;

            if(furnPlacement.preset.classes[0].objectSize.x > 1f)
            {
                altX = (spawnedPlacementMesh.bounds.size.x - 1) * -0.5f;
            }

            placementCursor.localPosition = new Vector3(altX, placementCursor.transform.localPosition.y, spawnedPlacementMesh.bounds.size.z * 0.5f + 1f);
        }
        else placementCursor.localPosition = new Vector3(0, 0, 1);

        //Rotate by 90 degree angles only
        placementCursor.eulerAngles = new Vector3(0, furnitureRotation, 0);

        //Wall piece; snap to nearest wall
        if (furnPlacement.preset.classes[0].wallPiece || furnPlacement.preset.classes[0].useWallSnappingInDecorMode)
        {
            //Raycast to wall
            Ray wallRay = new Ray(CameraController.Instance.cam.transform.position, CameraController.Instance.cam.transform.forward);

            int wallMask = Toolbox.Instance.CreateLayerMask(Toolbox.LayerMaskMode.onlyCast, 29);

            RaycastHit hit;

            if(Physics.Raycast(wallRay, out hit, 10f, wallMask))
            {
                //Choose the closest wall to snap to
                NewWall closestWall = null;
                float closestDist = 9999f;

                foreach (NewRoom r in Player.Instance.currentGameLocation.rooms)
                {
                    foreach (NewNode n in r.nodes)
                    {
                        foreach (NewWall w in n.walls)
                        {
                            if (w.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance) continue; //Skip entrances

                            float d = Vector3.Distance(w.position, hit.point);

                            if (d < closestDist)
                            {
                                closestWall = w;
                                closestDist = d;
                            }
                        }
                    }
                }

                if (closestWall != null)
                {
                    furnitureRotation = Mathf.RoundToInt(closestWall.localEulerAngles.y);
                    placementCursor.position = closestWall.node.position;
                    ret = closestWall.node; //Set node to this wall
                }
                else
                {
                    isPlacementValid = false;
                    Game.Log("Decor: No valid wall placement");
                }
            }
            else
            {
                isPlacementValid = false;
                Game.Log("Decor: Raycast does not hit a wall");
            }
        }

        if(ret == null)
        {
            Vector3Int nodePos = CityData.Instance.RealPosToNodeInt(placementCursor.position);
            PathFinder.Instance.nodeMap.TryGetValue(nodePos, out ret);
        }

        //Snap to grid
        placementCursor.transform.position = new Vector3(Mathf.RoundToInt(placementCursor.transform.position.x / 0.3f) * 0.3f, placementCursor.transform.position.y, Mathf.RoundToInt(placementCursor.transform.position.z / 0.3f) * 0.3f);
        return ret;
    }

    public void RotateFurn(bool right)
    {
        if(PlayerApartmentController.Instance.furniturePlacementMode && furnPlacement != null)
        {
            if(right)
            {
                if (furnPlacement.preset.classes[0].objectSize.x + furnPlacement.preset.classes[0].objectSize.y > 2)
                {
                    AddFurnitureRotation(90);
                }
                else AddFurnitureRotation(45);
            }
            else
            {
                if (furnPlacement.preset.classes[0].objectSize.x + furnPlacement.preset.classes[0].objectSize.y > 2)
                {
                    AddFurnitureRotation(-90);
                }
                else AddFurnitureRotation(-45);
            }
        }
    }

    public void AddFurnitureRotation(int angle)
    {
        furnitureRotation += angle;

        while (furnitureRotation >= 360)
        {
            furnitureRotation -= 360;
        }

        while (furnitureRotation < 0)
        {
            furnitureRotation += 360;
        }
    }

    public void ExecutePlacement()
    {
        if(furnPlacement != null && furniturePlacementMode && isPlacementValid)
        {
            Game.Log("Decor: Execute furniture placement: " + isPlacementValid + ": " + placementNode);
            FurnitureLocation existingFurniture = GetExistingFurniture();

            if (existingFurniture != null || furnPlacement.preset.cost <= GameplayController.Instance.money && placementNode != null)
            {
                AudioController.Instance.Play2DSound(AudioControls.Instance.furniturePlacement);

                //Purchase a new piece of furniture...
                if (existingFurniture == null)
                {
                    Game.Log("Decor: Creating new furniture...");
                    FurnitureLocation newObj = placementNode.room.AddFurnitureCustom(furnPlacement); //Add a new piece of furniture here
                    AudioController.Instance.Play2DSound(AudioControls.Instance.purchaseItem);
                    GameplayController.Instance.AddMoney(-furnPlacement.preset.cost, true, "furniture purchase");
                }
                //Place existing furniture...
                else
                {
                    //If this already exists in the room (but not in storage) move it to storage to remove it from it's original position
                    if(!furnitureStorage.Contains(existingFurniture))
                    {
                        MoveFurnitureToStorage(existingFurniture);
                    }

                    Game.Log("Decor: Placing existing furniture...");

                    //Copy new positional values to the existing furniture
                    existingFurniture.anchorNode = furnPlacement.anchorNode;
                    existingFurniture.coversNodes = new List<NewNode>();
                    existingFurniture.coversNodes.Add(existingFurniture.anchorNode); //For now, treat as only covering one node; this should not affect much as it is mostly relevent to AI actions
                    existingFurniture.offset = furnPlacement.offset;
                    existingFurniture.angle = furnPlacement.angle;
                    existingFurniture.scaleMultiplier = Vector3.one;

                    placementNode.room.AddFurnitureCustom(existingFurniture);

                    //Now remove from storage
                    RemoveFromStorage(existingFurniture);

                    ResetExisting();

                    SetFurniturePlacementMode(false, null, null);
                }

                //Fire event
                if (OnFurnitureChange != null)
                {
                    OnFurnitureChange();
                }

                ObjectPoolingController.Instance.UpdateObjectRanges();
                InteractionController.Instance.StartDecorEdit();
            }
        }
        else if (decoratingMode)
        {
            Game.Log("Decor: Execute decoration change");

            int price = GetCurrentCost();

            if (price <= GameplayController.Instance.money)
            {
                //Save change
                ApplyDecor(decoratingType, decoratingMaterial, decoratingKey, true);

                AudioController.Instance.Play2DSound(AudioControls.Instance.furniturePlacement);
                AudioController.Instance.Play2DSound(AudioControls.Instance.purchaseItem);
                GameplayController.Instance.AddMoney(-price, true, "decor purchase");

                SetDecoratingMode(false, null);

                InteractionController.Instance.StartDecorEdit();
            }
        }
    }

    //Reset a reference to an existing piece of furniture
    public void ResetExisting()
    {
        Game.Log("Decor: Resetting existing furniture reference");
        if(furnPlacement !=null) furnPlacement.existing = null;
    }

    public int GetCurrentCost()
    {
        int cost = 0;

        if (furnPlacement != null && furniturePlacementMode && isPlacementValid && GetExistingFurniture() == null)
        {
            cost = furnPlacement.preset.cost;
        }
        else if (decoratingMode)
        {
            if(decoratingType == MaterialGroupPreset.MaterialType.walls)
            {
                cost = decoratingMaterial.price * decoratingRoom.GetWallCount();
            }
            else
            {
                cost = decoratingMaterial.price * decoratingRoom.nodes.Count;
            }
        }

        return cost;
    }

    public void CancelPlacement(bool restoreExistingPosition)
    {
        //Reset the previous placement
        if(furniturePlacementMode)
        {
            Game.Log("Decor: Cancel furniture placement...");

            //Recreate existing element
            FurnitureLocation existingFurn = GetExistingFurniture();

            if (existingFurn != null && restoreExistingPosition)
            {
                Game.Log("Decor: ... Restoring existing furniture...");

                FurnitureLocation newObj = placementNode.room.AddFurnitureCustom(existingFurn);

                //Now remove from storage
                RemoveFromStorage(existingFurn);

                //Fire event
                if (OnFurnitureChange != null)
                {
                    OnFurnitureChange();
                }

                ObjectPoolingController.Instance.UpdateObjectRanges();
            }

            SetFurniturePlacementMode(false, null, null);
        }
        else if(decoratingMode)
        {
            Game.Log("Decor: Cancel decorating mode...");
            SetDecoratingMode(false, null);
        }

        InteractionController.Instance.StartDecorEdit(); //Do we want the window to pop up again?
    }

    public void MoveFurnitureToStorage(FurnitureLocation newStorage)
    {
        if (newStorage == null) return;

        if(!furnitureStorage.Contains(newStorage))
        {
            Game.Log("Decor: Moving " + newStorage.furniture.name + " to storage...");

            //Cancel existing placement
            if (furniturePlacementMode && GetExistingFurniture() == newStorage)
            {
                SetFurniturePlacementMode(false, null, null);
            }

            if (Player.Instance.currentRoom != null) Player.Instance.currentRoom.decorEdit = true; //We need to save this to state save data if anything has been changed; this flag does that

            newStorage.Delete(true, FurnitureClusterLocation.RemoveInteractablesOption.moveToStorage); //Delete from world

            //newStorage.anchorNode = null;
            //newStorage.coversNodes.Clear();
            //newStorage.angle = 0;
            //newStorage.diagonalAngle = 0;

            newStorage.fovDirection = Vector2.zero;
            newStorage.fovMaxDistance = 0;
            newStorage.useFOVBLock = false;
            newStorage.usage.Clear();
            newStorage.sublocations.Clear();
            newStorage.scaleMultiplier = Vector3.one;

            furnitureStorage.Add(newStorage);

            //Fire event
            if (OnFurnitureChange != null)
            {
                OnFurnitureChange();
            }
        }
    }

    public void RemoveFromStorage(FurnitureLocation newStorage)
    {
        if (newStorage == null) return;

        if(furnitureStorage.Contains(newStorage))
        {
            Game.Log("Decor: Removing " + newStorage.furniture.name + " from storage...");

            if (Player.Instance.currentRoom != null) Player.Instance.currentRoom.decorEdit = true; //We need to save this to state save data if anything has been changed; this flag does that

            furnitureStorage.Remove(newStorage);

            //Fire event
            if (OnFurnitureChange != null)
            {
                OnFurnitureChange();
            }
        }
    }

    public void SellFurniture(FurnitureLocation newSell)
    {
        if (newSell == null) return;
        Game.Log("Decor: Selling " + newSell.furniture.name + "...");

        if (Player.Instance.currentRoom != null) Player.Instance.currentRoom.decorEdit = true; //We need to save this to state save data if anything has been changed; this flag does that

        RemoveFromStorage(newSell);

        //Cancel existing placement
        if (furniturePlacementMode && GetExistingFurniture() == newSell)
        {
            SetFurniturePlacementMode(false, null, null);
        }

        AudioController.Instance.Play2DSound(AudioControls.Instance.pickUpMoney);
        GameplayController.Instance.AddMoney(Mathf.RoundToInt(newSell.furniture.cost * 0.5f), true, "Sell furniture");

        newSell.Delete(true, FurnitureClusterLocation.RemoveInteractablesOption.moveToStorage); //Delete from world

        //Fire event
        if (OnFurnitureChange != null)
        {
            OnFurnitureChange();
        }
    }

    public void MoveItemToStorage(Interactable newStorage)
    {
        if (newStorage == null) return;

        if(!itemStorage.Contains(newStorage))
        {
            if(!newStorage.preset.allowInApartmentStorage)
            {
                Game.Log("Decor: " + newStorage.GetName() + " is not allowed in apartment storage, deleting...");
                newStorage.SafeDelete();
                return;
            }

            Game.Log("Decor: Moving " + newStorage.GetName() + " to storage...");

            if (Player.Instance.currentRoom != null) Player.Instance.currentRoom.decorEdit = true; //We need to save this to state save data if anything has been changed; this flag does that

            //Drop item
            if (InteractionController.Instance.carryingObject != null)
            {
                if(InteractionController.Instance.carryingObject.interactable == newStorage)
                {
                    InteractionController.Instance.carryingObject.DropThis(false);
                }
            }

            if (newStorage.phy) newStorage.SetPhysicsPickupState(false, null);

            if (newStorage.inInventory != null)
            {
                newStorage.SetAsNotInventory(Player.Instance.currentNode);
            }

            newStorage.MarkAsTrash(false); //Make sure this isn't deleted as trash while in storage
            itemStorage.Add(newStorage);
            newStorage.RemoveFromPlacement();

            //Fire event
            if (OnFurnitureChange != null)
            {
                OnFurnitureChange();
            }
        }
    }

    public void RemoveItemFromStorage(Interactable newStorage)
    {
        if (newStorage == null) return;

        if(itemStorage.Contains(newStorage))
        {
            Game.Log("Decor: Removing " + newStorage.GetName() + " from storage...");

            itemStorage.Remove(newStorage);

            newStorage.rPl = false; //Reset removed from world

            //Fire event
            if (OnFurnitureChange != null)
            {
                OnFurnitureChange();
            }
        }
    }

    public void SellItem(Interactable newSell)
    {
        if (newSell == null) return;
        Game.Log("Decor: Selling " + newSell.GetName() + "...");

        itemStorage.Remove(newSell);

        AudioController.Instance.Play2DSound(AudioControls.Instance.pickUpMoney);
        GameplayController.Instance.AddMoney(Mathf.RoundToInt(newSell.val * 0.5f), true, "Sell furniture");

        newSell.SafeDelete(true); //Delete from world

        //Fire event
        if (OnFurnitureChange != null)
        {
            OnFurnitureChange();
        }
    }

    public void UpdateDecorColourKey()
    {
        if(decoratingMode)
        {
            ApplyDecor(decoratingType, decoratingMaterial, decoratingKey, false);
        }
    }

    public void ApplyDecor(MaterialGroupPreset.MaterialType decorType, MaterialGroupPreset material, Toolbox.MaterialKey key, bool saveChanges)
    {
        if (material != null)
        {
            Game.Log("Decor: Apply decor " + decorType.ToString() + " using " + material.name + ", save changes: " + saveChanges);

            //Set existing walls
            if (decorType == MaterialGroupPreset.MaterialType.walls)
            {
                if (saveChanges)
                {
                    decoratingRoom.defaultWallKey = key;
                    decoratingRoom.defaultWallMaterial = material;
                    decoratingRoom.wallMat = MaterialsController.Instance.SetMaterialGroup(decoratingRoom.combinedWalls, material, key, true);

                    //Set frontage keys
                    //foreach (NewNode n in decoratingRoom.nodes)
                    //{
                    //    foreach (NewWall w in n.walls)
                    //    {
                    //        foreach (NewWall.FrontageSetting f in w.frontagePresets)
                    //        {
                    //            f.matKey = newDetailKey;
                    //        }

                    //        w.SpawnFrontage(false);

                    //        //Set doors
                    //        w.doorMatKey = newDoorsKey;
                    //        if (w.door != null) w.door.SelectColouring(false); //Refresh colours
                    //    }
                    //}
                }
                else
                {
                    MaterialsController.Instance.SetMaterialGroup(decoratingRoom.combinedWalls, material, key);

                    //Preview frontage keys
                    //foreach (NewNode n in decoratingRoom.nodes)
                    //{
                    //    foreach (NewWall w in n.walls)
                    //    {
                    //        w.SpawnFrontage(true, newDetailKey);
                    //        if (w.door != null) w.door.SelectColouring(true, newDoorsKey); //Refresh colours
                    //    }
                    //}
                }
            }
            else if (decorType == MaterialGroupPreset.MaterialType.floor)
            {
                if (saveChanges)
                {
                    decoratingRoom.floorMatKey = key;
                    decoratingRoom.floorMaterial = material;
                    decoratingRoom.floorMat = MaterialsController.Instance.SetMaterialGroup(decoratingRoom.combinedFloor, material, key, true);
                }
                else
                {
                    MaterialsController.Instance.SetMaterialGroup(decoratingRoom.combinedFloor, material, key);
                }
            }
            else if (decorType == MaterialGroupPreset.MaterialType.ceiling)
            {
                if (saveChanges)
                {
                    decoratingRoom.ceilingMatKey = key;
                    decoratingRoom.ceilingMaterial = material;
                    decoratingRoom.ceilingMat = MaterialsController.Instance.SetMaterialGroup(decoratingRoom.combinedCeiling, material, key, true);
                }
                else
                {
                    MaterialsController.Instance.SetMaterialGroup(decoratingRoom.combinedCeiling, material, key);
                }
            }

            if (saveChanges)
            {
                decoratingRoom.decorEdit = true; //This flag will save the new decor with the save state
            }
        }
    }

    public void RevertDecor(MaterialGroupPreset.MaterialType decorType)
    {
        Game.Log("Decor: Reverting decor for " + decoratingRoom.name + " " + decorType.ToString());

        Material savedMaterial = null;

        if (decorType == MaterialGroupPreset.MaterialType.walls)
        {
            savedMaterial = decoratingRoom.wallMat;

            if (decoratingRoom.combinedWallRend != null)
            {
                decoratingRoom.combinedWallRend.material = decoratingRoom.wallMat;
            }

            //Preview frontage keys
            //foreach (NewNode n in decoratingRoom.nodes)
            //{
            //    foreach (NewWall w in n.walls)
            //    {
            //        w.SpawnFrontage(false);
            //        if (w.door != null) w.door.SelectColouring(false);
            //    }
            //}
        }
        else if (decorType == MaterialGroupPreset.MaterialType.floor)
        {
            savedMaterial = decoratingRoom.floorMat;

            if (decoratingRoom.combinedFloorRend != null)
            {
                decoratingRoom.combinedFloorRend.material = decoratingRoom.floorMat;
            }
        }
        else if (decorType == MaterialGroupPreset.MaterialType.ceiling)
        {
            savedMaterial = decoratingRoom.ceilingMat;

            if (decoratingRoom.combinedCeilingRend != null)
            {
                decoratingRoom.combinedCeilingRend.material = decoratingRoom.ceilingMat;
            }
        }

        //Load saved material
        //if (savedMaterial != null)
        //{
        //    Game.Log("Decor: Cancelling preview, reverting to room's set material: " + savedMaterial.name);
        //    UpdateButtonsBasedOnMaterial(savedMaterial, true);
        //}
    }

    public void PlaceIndividualCluster(FurnitureCluster cluster, NewAddress address)
    {
        Dictionary<FurnitureClusterLocation, NewRoom> possibleLocations = new Dictionary<FurnitureClusterLocation, NewRoom>();

        foreach(NewRoom r in address.rooms)
        {
            List<FurnitureClusterLocation> l = GenerationController.Instance.GetPossibleFurnitureClusterLocations(r, cluster, cluster.enableDebug);

            foreach(FurnitureClusterLocation f in l)
            {
                possibleLocations.Add(f, r);
            }
        }

        if(possibleLocations.Count > 0)
        {
            FurnitureClusterLocation best = null;
            NewRoom bestRoom = null;

            foreach(KeyValuePair<FurnitureClusterLocation, NewRoom> pair in possibleLocations)
            {
                if(best == null || pair.Key.ranking > best.ranking)
                {
                    best = pair.Key;
                    bestRoom = pair.Value;
                }
            }

            if(best != null)
            {
                bestRoom.decorEdit = true;
                bestRoom.AddFurniture(best, true);
                Game.Log("Added " + cluster.name + " to room " + bestRoom.name);
            }
        }
    }
}
