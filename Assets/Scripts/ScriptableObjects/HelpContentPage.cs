﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using TMPro;
using UnityEngine.Video;
using UnityEditor;
using UnityEngine.UI;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "helpcontent_data", menuName = "Database/Help Content Page")]

public class HelpContentPage : SoCustomComparison
{
    public bool disabled = false;
    public string messageID;
    public List<HelpContentDisplay> contentDisplay = new List<HelpContentDisplay>();

    [System.Serializable]
    public class HelpContentDisplay
    {
        public DisplaySetting helpDisplaySetting = DisplaySetting.dontDisplay;
        public VideoClip clip;
        public Texture2D image;
    }

    public enum DisplaySetting { dontDisplay, displayBeforeText, displayAfterText};
}
