﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ElevatorControlsController : MonoBehaviour
{
    public InfoWindow parentWindow;
    public Evidence evidence;
    public WindowContentController windowContent;
    public List<RectTransform> buttons = new List<RectTransform>();

    public TextMeshProUGUI inputText;

    private void OnEnable()
    {
        parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        evidence = parentWindow.passedEvidence;

        foreach(RectTransform rect in buttons)
        {
            rect.gameObject.SetActive(false);
        }

        Interactable inter = InteractionController.Instance.currentLookingAtInteractable.interactable;

        if(inter != null)
        {
            //Get elevator class
            Elevator elevator = inter.objectRef as Elevator;

            if(elevator != null)
            {
                for (int i = -2; i < buttons.Count - 2; i++)
                {
                    //Does this floor exist?
                    if(inter.node.building.floors.ContainsKey(i))
                    {
                        //Does the elevator go to this floor?
                        if(elevator.elevatorFloors.ContainsKey(i))
                        {
                            buttons[i + 2].gameObject.SetActive(true);
                        }
                        else
                        {
                            //Disable button
                            Game.Log("Elevator does not visit floor " + i);
                            buttons[i + 2].gameObject.SetActive(false);
                        }
                    }
                    else
                    {
                        //Disable button
                        Game.Log("Elevator does not visit floor " + i);
                        buttons[i + 2].gameObject.SetActive(false);
                    }
                }
            }
        }
    }

    public void PressNumberButton(int newInt)
    {
        //Is this correct?
        Interactable inter = InteractionController.Instance.currentLookingAtInteractable.interactable;

        if (inter == null) return;

        //Get elevator class
        Elevator elevator = inter.objectRef as Elevator;

        if (elevator == null) return;

        bool goingUp = false;

        if(newInt > elevator.currentFloor)
        {
            goingUp = true;
        }

        elevator.CallElevator(newInt, goingUp);

        AudioEvent buttonSound = AudioControls.Instance.keypadButtons[0];

        try
        {
            buttonSound = AudioControls.Instance.keypadButtons[newInt % 10];
        }
        catch
        {

        }

        //Play button audio
        AudioController.Instance.PlayWorldOneShot(buttonSound, Player.Instance, null, inter.wPos);

        //Close
        parentWindow.CloseWindow();
    }
}
