﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Assets.TextureChannelPacker.Editor
{
	public class TextureChannelPacker : EditorWindow
	{
		private int WindowWidth { get { return (int)position.width; } }
		private int ElementWidth { get { return (int)position.width - (TCPStyles.WINDOW_BORDER_SPACING << 1); } }
		private int WindowHeight { get { return (int)position.height; } }

		private Rect _outputPreviewRect = new Rect(0, 0, 0, 0);

		private ChannelToggles _channelToggles = (ChannelToggles.Red | ChannelToggles.Green | ChannelToggles.Blue | ChannelToggles.Alpha);

		private const int ChannelCount = 4;
		private readonly List<ChannelTexture> _inputTextureList = new List<ChannelTexture>(ChannelCount);
		private Texture2D _previewTexture = null;           // the ouput texture with the actual data
		private Texture2D _previewTextureDisplay = null;    // the output texture with the diplay date (e.g.: with only red channel if other channels are hidden by options)

		private bool _shouldUpdatePreview = false;
		private bool _openImageOnExport = true;

		[MenuItem(("Window/Texture Channel Packer"))]
		public static void ShowWindow()
		{
			TextureChannelPacker window = GetWindow<TextureChannelPacker>();
			window.InitializeWindow();
			window.InitializeVariables();
		}

		private void InitializeWindow()
		{
			// window tab title and icon
			Texture2D icon = Resources.Load(TCPStyles.WINDOW_ICON) as Texture2D;
			if (icon == null)
			{
				TCPStyles.LogSimpleError("The editor window icon failed to load! Initializing window without icon ...");
				titleContent = new GUIContent(TCPStyles.WINDOW_TITLE);
			}
			else
			{
				titleContent = new GUIContent(TCPStyles.WINDOW_TITLE, icon);
			}

			// minimum size
			int minX = (TCPStyles.WINDOW_BORDER_SPACING << 1) + (TCPStyles.TEXTURE_PREVIEW_SIZE << 2) + (TCPStyles.UNITY_ELEMENT_SPACING * 3);
			int minY = (TCPStyles.WINDOW_BORDER_SPACING << 1) + (TCPStyles.TEXTURE_PREVIEW_SIZE << 1) + TCPStyles.INPUT_OUTPUT_VERTICALSPACING + (TCPStyles.UNITY_BUTTON_HEIGHT << 2);
			minSize = new Vector2(minX, minY);
		}

		private void InitializeVariables()
		{
			for (int i = 0; i < ChannelCount; ++i)
			{
				_inputTextureList.Add(new ChannelTexture());
			}

			_previewTextureDisplay = null;
		}

		private void OnGUI()
		{
			_outputPreviewRect.position = new Vector2((WindowWidth >> 1) - (TCPStyles.TEXTURE_PREVIEW_SIZE >> 1), TCPStyles.WINDOW_BORDER_SPACING);
			_outputPreviewRect.size = Vector2.one * TCPStyles.TEXTURE_PREVIEW_SIZE;

			DrawOutputTexturePreview();
			GUILayout.Space(TCPStyles.INPUT_OUTPUT_VERTICALSPACING);
			DrawInputTextureFields();

			// Drag & drop fields
			{
				float safeWidthX = WindowWidth - TCPStyles.WINDOW_BORDER_SPACING - TCPStyles.WINDOW_BORDER_SPACING;
				float spacingX = (safeWidthX - (4 * TCPStyles.TEXTURE_PREVIEW_SIZE)) / (_inputTextureList.Count - 1);
				for (int i = 0; i < _inputTextureList.Count; i++)
				{
					float x = TCPStyles.WINDOW_BORDER_SPACING + ((spacingX + TCPStyles.TEXTURE_PREVIEW_SIZE) * i) - 1;
					float y = TCPStyles.WINDOW_BORDER_SPACING + TCPStyles.INPUT_OUTPUT_VERTICALSPACING + TCPStyles.TEXTURE_PREVIEW_SIZE + 2;
					Rect r = new Rect(x, y, TCPStyles.TEXTURE_PREVIEW_SIZE, TCPStyles.TEXTURE_PREVIEW_SIZE);
					DropAreaGUI(r, i);
				}
			}

			DrawMessagRect();
		}

		private void DrawMessagRect()
		{
			bool allTexturesNull = AreAllTexturesNull();
			bool anyDisplayToggleDisabled = IsAnyDisplayFlagDisabled();
			bool allDisplayTogglesDisabled = AreAllDisplayFlagDisabled();
			bool differentImageSizes = !AreAllTexturesSameSize();

			if (!_shouldUpdatePreview && !allTexturesNull && !anyDisplayToggleDisabled &&
				!allDisplayTogglesDisabled && !differentImageSizes)
				return;

			Rect msgRect = new Rect(TCPStyles.WINDOW_BORDER_SPACING, _outputPreviewRect.position.y + (TCPStyles.TEXTURE_PREVIEW_SIZE >> 2),
				_outputPreviewRect.position.x - (TCPStyles.WINDOW_BORDER_SPACING << 1), (TCPStyles.TEXTURE_PREVIEW_SIZE >> 1));

			if (allTexturesNull)
			{
				EditorGUI.HelpBox(msgRect, "All input textures are currently empty.\nYou cannot export whitout atleast one input texture.", MessageType.Warning);
			}
			else if (differentImageSizes)
			{
				EditorGUI.HelpBox(msgRect, "Your input textures have different dimensions.\nThis could result in undesired results.", MessageType.Warning);
			}
			else if (_shouldUpdatePreview)
			{
				EditorGUI.HelpBox(msgRect, "New changed are still not updated to the output texture.\nPress the 'Update preview'-button before exporting or changes might get lost!", MessageType.Warning);
			}
			else if (allDisplayTogglesDisabled)
			{
				EditorGUI.HelpBox(msgRect, "All channel display toggles are disabled.\nHowever, this does not affect your export image.", MessageType.Info);
			}
			else if (anyDisplayToggleDisabled)
			{
				EditorGUI.HelpBox(msgRect, "One or more preview channel display toggles are deactivated.\nHowever, this does not affect your export image.", MessageType.Info);
			}
		}

		private void DrawOutputTexturePreview()
		{
			GUILayout.Space(_outputPreviewRect.position.y);
			GUILayout.BeginHorizontal();
			{
				GUILayout.Space(_outputPreviewRect.position.x);
				if (_previewTextureDisplay != null)
				{
					GUILayout.Label(_previewTextureDisplay, GUILayout.Width(TCPStyles.TEXTURE_PREVIEW_SIZE), GUILayout.Height(TCPStyles.TEXTURE_PREVIEW_SIZE));
				}
				else
				{
					GUILayout.Label("", GUILayout.Width(TCPStyles.TEXTURE_PREVIEW_SIZE), GUILayout.Height(TCPStyles.TEXTURE_PREVIEW_SIZE)); // for GUI spacing purposes
					EditorGUI.HelpBox(_outputPreviewRect, "no texture to preview", MessageType.None);
				}

				GUILayout.BeginVertical();
				{
					GUI.enabled = !AreAllTexturesNull();
					if (GUILayout.Button("Update preview", GUILayout.Width(TCPStyles.PREVIEW_BUTTON_WIDHT)))
					{
						UpdatePreviewTexture();
					}

					GUI.enabled = !_shouldUpdatePreview && !AreAllTexturesNull();
					if (GUILayout.Button("Export to Assets", GUILayout.Width(TCPStyles.PREVIEW_BUTTON_WIDHT)))
					{
						ExportPreviewImage(false);
					}

					if (GUILayout.Button("Export to Disk", GUILayout.Width(TCPStyles.PREVIEW_BUTTON_WIDHT)))
					{
						ExportPreviewImage(true);
					}
					GUI.enabled = true;

					_openImageOnExport = GUILayout.Toggle(_openImageOnExport, "Open image on export (disk only)");

					DrawToggleChannelViewButtons();
				}
				GUILayout.EndVertical();
			}
			GUILayout.EndHorizontal();
		}

		private void DrawToggleChannelViewButtons()
		{
			GUI.backgroundColor = TCPStyles.RED_CHANNEL_COLOR;
			bool oldToggle = (_channelToggles & ChannelToggles.Red) != 0;
			bool newToggle = GUILayout.Toggle(oldToggle, "Show red channel", GUILayout.Width(TCPStyles.PREVIEW_BUTTON_WIDHT << 1));
			bool changed = oldToggle != newToggle;
			if (changed)
			{
				if (newToggle)
				{
					_channelToggles |= ChannelToggles.Red;
				}
				else
				{
					_channelToggles &= ~ChannelToggles.Red;
				}
				UpdatePreviewDisplayTextureData();
			}

			GUI.backgroundColor = TCPStyles.GREEN_CHANNEL_COLOR;
			oldToggle = (_channelToggles & ChannelToggles.Green) != 0;
			newToggle = GUILayout.Toggle(oldToggle, "Show green channel", GUILayout.Width(TCPStyles.PREVIEW_BUTTON_WIDHT << 1));
			changed = oldToggle != newToggle;
			if (changed)
			{
				if (newToggle)
				{
					_channelToggles |= ChannelToggles.Green;
				}
				else
				{
					_channelToggles &= ~ChannelToggles.Green;
				}
				UpdatePreviewDisplayTextureData();
			}

			GUI.backgroundColor = TCPStyles.BLUE_CHANNEL_COLOR;
			oldToggle = (_channelToggles & ChannelToggles.Blue) != 0;
			newToggle = GUILayout.Toggle(oldToggle, "Show blue channel", GUILayout.Width(TCPStyles.PREVIEW_BUTTON_WIDHT << 1));
			changed = oldToggle != newToggle;
			if (changed)
			{
				if (newToggle)
				{
					_channelToggles |= ChannelToggles.Blue;
				}
				else
				{
					_channelToggles &= ~ChannelToggles.Blue;
				}
				UpdatePreviewDisplayTextureData();
			}

			GUI.backgroundColor = TCPStyles.ALPHA_CHANNEL_COLOR;
			oldToggle = (_channelToggles & ChannelToggles.Alpha) != 0;
			newToggle = GUILayout.Toggle(oldToggle, "Show alpha channel", GUILayout.Width(TCPStyles.PREVIEW_BUTTON_WIDHT << 1));
			changed = oldToggle != newToggle;
			if (changed)
			{
				if (newToggle)
				{
					_channelToggles |= ChannelToggles.Alpha;
				}
				else
				{
					_channelToggles &= ~ChannelToggles.Alpha;
				}
				UpdatePreviewDisplayTextureData();
			}

			GUI.backgroundColor = TCPStyles.DEFAULT_COLOR;
		}

		private void DrawInputTextureFields()
		{
			GUILayout.BeginHorizontal();
			{
				GUILayout.Space(TCPStyles.WINDOW_BORDER_SPACING);
				for (int i = 0; i < ChannelCount; ++i)
				{
					if (i != 0)
					{
						GUILayout.FlexibleSpace();
					}

					ChannelTexture texture = _inputTextureList[i];

					GUILayout.BeginVertical();
					{
						// draw tex preview
						if (texture.Texture == null)
						{
							GUI.backgroundColor = TCPStyles.GetChannelColor(i);
							if (GUILayout.Button(string.Format("to {0} channel", ((Channel)i).ToString().ToUpper()), GUILayout.Width(TCPStyles.TEXTURE_PREVIEW_SIZE), GUILayout.Height(TCPStyles.TEXTURE_PREVIEW_SIZE)))
							{
								BrowseForTexture(texture);
							}
							GUI.backgroundColor = TCPStyles.DEFAULT_COLOR;
						}
						else
						{
							GUILayout.Label(texture.Texture, GUILayout.Width(TCPStyles.TEXTURE_PREVIEW_SIZE), GUILayout.Height(TCPStyles.TEXTURE_PREVIEW_SIZE));
						}

						GUILayout.BeginHorizontal();
						{
							GUI.enabled = _inputTextureList[i].Texture != null;
							{
								GUI.backgroundColor = TCPStyles.DELETE_BUTTON_COLOR;
								if (GUILayout.Button("X", GUILayout.Width(TCPStyles.SQUARE_BUTTON_SIZE)))
								{
									_inputTextureList[i].Texture = null;
									_shouldUpdatePreview = true;
								}
								GUI.backgroundColor = TCPStyles.DEFAULT_COLOR;
							}
							GUI.enabled = true;

							if (GUILayout.Button("Browse", GUILayout.Width(TCPStyles.TEXTURE_PREVIEW_SIZE - TCPStyles.SQUARE_BUTTON_SIZE - TCPStyles.UNITY_ELEMENT_SPACING)))
							{
								BrowseForTexture(_inputTextureList[i]);
							}
						}
						GUILayout.EndHorizontal();

						GUI.enabled = _inputTextureList[i].Texture != null;
						{
							GUILayout.Label(string.Format("Dimensions: {0} x {1}", texture.Width, texture.Height), GUILayout.Width(TCPStyles.TEXTURE_PREVIEW_SIZE));

							GUILayout.BeginHorizontal();
							{
								GUILayout.Label("Extract from:", GUILayout.Width((TCPStyles.TEXTURE_PREVIEW_SIZE * (1f - TCPStyles.TEXTURE_ENUM_WIDTH_SCALE)) - TCPStyles.UNITY_ELEMENT_SPACING));
								Channel newValue = DrawChannelPopup(texture.ExtractionChannel);
								if (newValue != texture.ExtractionChannel)
								{
									texture.ExtractionChannel = newValue;
									_shouldUpdatePreview = true;
								}
							}
							GUILayout.EndHorizontal();

							bool newInverted = GUILayout.Toggle(_inputTextureList[i].Inverted, "Invert texture", GUILayout.Width(TCPStyles.TEXTURE_PREVIEW_SIZE));
							if (newInverted != _inputTextureList[i].Inverted)
							{
								for (int y = 0; y < _inputTextureList[i].Height; y++)
								{
									for (int x = 0; x < _inputTextureList[i].Width; x++)
									{
										Color c = _inputTextureList[i].Texture.GetPixel(x, y);
										_inputTextureList[i].Texture.SetPixel(x, y, new Color(1f - c.r, 1f - c.g, 1f - c.b, c.a));
									}
								}
								_inputTextureList[i].Texture.Apply(false);
								_inputTextureList[i].Inverted = newInverted;
								_shouldUpdatePreview = true;
							}
						}
						GUI.enabled = true;
					}
					GUILayout.EndVertical();

					if (i == ChannelCount - 1)
					{
						GUILayout.Space(TCPStyles.WINDOW_BORDER_SPACING);
					}
				}
			}
			GUILayout.EndHorizontal();
		}

		private Channel DrawChannelPopup(Channel channel)
		{
			GUI.backgroundColor = TCPStyles.GetChannelColor(channel);
			Channel value = (Channel)EditorGUILayout.EnumPopup(channel, GUILayout.Width(TCPStyles.TEXTURE_PREVIEW_SIZE * TCPStyles.TEXTURE_ENUM_WIDTH_SCALE));

			GUI.backgroundColor = TCPStyles.DEFAULT_COLOR;
			return value;
		}

		private void BrowseForTexture(ChannelTexture texture)
		{
			string[] filter = { "Image files", "png" };
			string searchResult = EditorUtility.OpenFilePanelWithFilters("", Application.dataPath, filter);

			if (File.Exists(searchResult))
			{
				texture.FilePath = searchResult;

				Texture2D newTexture = new Texture2D(2, 2, TCPStyles.PNG_FORMAT, false);
				byte[] bytes = File.ReadAllBytes(texture.FilePath);
				newTexture.LoadImage(bytes);
				newTexture.Apply();

				texture.Texture = newTexture;
				texture.Inverted = false;
				_shouldUpdatePreview = true;
			}
		}

		private void UpdatePreviewTexture()
		{
			_previewTexture = null;

			int maxWidth = _inputTextureList[0].Width;
			int maxHeight = _inputTextureList[0].Height;
			for (int i = 1; i < ChannelCount; ++i)
			{
				maxWidth = Mathf.Max(maxWidth, _inputTextureList[i].Width);
				maxHeight = Mathf.Max(maxHeight, _inputTextureList[i].Height);
			}

			if (maxWidth <= 0 || maxHeight <= 0)
			{
				Debug.LogWarning("The width or height of the preview texture would result in zero (0). Check if atleast one texture is not null.");
				_previewTextureDisplay = null;
				_shouldUpdatePreview = false;
				return;
			}

			_previewTexture = new Texture2D(maxWidth, maxHeight, TCPStyles.PNG_FORMAT, false);

			for (int i = 0; i < ChannelCount; ++i)
			{
				ChannelTexture input = _inputTextureList[i];
				if (input == null || input.Texture == null)
				{
					for (int y = 0; y < _previewTexture.height; ++y)
					{
						for (int x = 0; x < _previewTexture.width; ++x)
						{
							// set value to color
							Color c = _previewTexture.GetPixel(x, y);
							switch ((Channel)i)
							{
								case Channel.Red:
								c.r = 0;
								break;

								case Channel.Green:
								c.g = 0;
								break;

								case Channel.Blue:
								c.b = 0;
								break;

								case Channel.Alpha:
								c.a = 1;
								break;

								default:
								TCPStyles.LogSimpleError("An invalid channel index is given to set the pixel information!");
								break;
							}
							_previewTexture.SetPixel(x, y, c);
						}
					}
					continue;
				}

				for (int y = 0; y < maxHeight; ++y)
				{
					for (int x = 0; x < maxWidth; ++x)
					{
						Color c = _previewTexture.GetPixel(x, y);

						if (y >= input.Height || x >= input.Width)
						{
							switch ((Channel)i)
							{
								case Channel.Red:
								c.r = 0;
								break;

								case Channel.Green:
								c.g = 0;
								break;

								case Channel.Blue:
								c.b = 0;
								break;

								case Channel.Alpha:
								c.a = 1;
								break;

								default:
								TCPStyles.LogSimpleError("An invalid channel index is given to set the pixel information!");
								break;
							}
							continue;
						}

						float value = 0;
						// get value of input texture's extraction channel
						switch (input.ExtractionChannel)
						{
							case Channel.Red:
							value = input.Texture.GetPixel(x, y).r;
							break;

							case Channel.Green:
							value = input.Texture.GetPixel(x, y).g;
							break;

							case Channel.Blue:
							value = input.Texture.GetPixel(x, y).b;
							break;

							case Channel.Alpha:
							value = input.Texture.GetPixel(x, y).a;
							if (input.Inverted)
							{
								value = 1f - value;
							}
							break;

							default:
							TCPStyles.LogSimpleError("An invalid channel index is given to retreive the pixel information!");
							break;
						}

						// set value to color
						switch ((Channel)i)
						{
							case Channel.Red:
							c.r = value;
							break;

							case Channel.Green:
							c.g = value;
							break;

							case Channel.Blue:
							c.b = value;
							break;

							case Channel.Alpha:
							c.a = value;
							break;

							default:
							TCPStyles.LogSimpleError("An invalid channel index is given to set the pixel information!");
							break;
						}

						_previewTexture.SetPixel(x, y, c);
					}
				}
			}
			_previewTexture.Apply(false);
			UpdatePreviewDisplayTextureData();
			_shouldUpdatePreview = false;
		}

		private void ExportPreviewImage(bool toDesktop = true)
		{
			// get save path
			string savePath;
			if (toDesktop)
				savePath = EditorUtility.SaveFilePanel(TCPStyles.EXPORT_TITLE, "", TCPStyles.EXPORT_DEFAULT_NAME, TCPStyles.EXPORT_EXTENSION);
			else
				savePath = EditorUtility.SaveFilePanelInProject(TCPStyles.EXPORT_TITLE, TCPStyles.EXPORT_DEFAULT_NAME, TCPStyles.EXPORT_EXTENSION, "");

			// check if user cancelled
			if (string.IsNullOrEmpty(savePath))
			{
				Debug.LogWarning("Failed to export the image!");
				return;
			}

			// save image
			byte[] bytes = _previewTexture.EncodeToPNG();
			FileStream stream = File.Open(savePath, FileMode.Create);
			BinaryWriter binary = new BinaryWriter(stream);
			binary.Write(bytes);
			stream.Close();

			if (File.Exists(savePath))
			{
				Debug.Log(string.Format("TCP texture successfully exported to: {0}", savePath));

				// open image
				if (_openImageOnExport && toDesktop)
				{
					System.Diagnostics.Process.Start(savePath);
				}
			}
			else
			{
				Debug.LogError(string.Format("TCP texture failed to export to: {0}", savePath));
			}

			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		private void UpdatePreviewDisplayTextureData()
		{
			if (_previewTexture == null)
			{
				_previewTextureDisplay = null;
				return;
			}

			_previewTextureDisplay = new Texture2D(_previewTexture.width, _previewTexture.height, TCPStyles.PNG_FORMAT, false);

			bool r = (_channelToggles & ChannelToggles.Red) != 0;
			bool g = (_channelToggles & ChannelToggles.Green) != 0;
			bool b = (_channelToggles & ChannelToggles.Blue) != 0;
			bool a = (_channelToggles & ChannelToggles.Alpha) != 0;

			int enableCount = 0;
			if (r) ++enableCount;
			if (g) ++enableCount;
			if (b) ++enableCount;
			if (a) ++enableCount;

			for (int y = 0; y < _previewTexture.height; ++y)
			{
				for (int x = 0; x < _previewTexture.width; ++x)
				{
					Color c = _previewTexture.GetPixel(x, y);
					Color final = new Color(0, 0, 0, 1);

					if (r)
					{
						final.r = c.r;
					}
					if (g)
					{
						final.g = c.g;
					}
					if (b)
					{
						final.b = c.b;
					}
					if (a)
					{
						final.a = c.a;
					}

					if (enableCount <= 0)
					{
						_previewTextureDisplay = null;
						return;
					}
					else if (enableCount == 1)
					{
						float gray = (a) ? final.a : final.maxColorComponent;
						_previewTextureDisplay.SetPixel(x, y, new Color(gray, gray, gray, 1));
					}
					else
					{
						_previewTextureDisplay.SetPixel(x, y, final);
					}
				}
			}
			_previewTextureDisplay.Apply(false);
		}

		private bool AreAllTexturesNull()
		{
			for (int i = 0; i < _inputTextureList.Count; ++i)
			{
				if (_inputTextureList[i].Texture != null)
					return false;
			}
			return true;
		}

		private bool IsAnyDisplayFlagDisabled()
		{
			if ((_channelToggles & ChannelToggles.Red) == 0)
				return true;
			if ((_channelToggles & ChannelToggles.Green) == 0)
				return true;
			if ((_channelToggles & ChannelToggles.Blue) == 0)
				return true;
			if ((_channelToggles & ChannelToggles.Alpha) == 0)
				return true;

			return false;
		}

		private bool AreAllDisplayFlagDisabled()
		{
			if ((_channelToggles & ChannelToggles.Red) != 0)
				return false;
			if ((_channelToggles & ChannelToggles.Green) != 0)
				return false;
			if ((_channelToggles & ChannelToggles.Blue) != 0)
				return false;
			if ((_channelToggles & ChannelToggles.Alpha) != 0)
				return false;

			return true;
		}

		private bool AreAllTexturesSameSize()
		{
			if (AreAllTexturesNull())
				return true;

			int x = -1;
			int y = -1;
			for (int i = 0; i < _inputTextureList.Count; ++i)
			{
				if (_inputTextureList[i].Texture == null)
					continue;

				if (x < 0 || y < 0)
				{
					x = _inputTextureList[i].Width;
					y = _inputTextureList[i].Height;
				}
				else
				{
					if (_inputTextureList[i].Width != x)
						return false;
					if (_inputTextureList[i].Height != y)
						return false;
				}
			}
			return true;
		}

		public void DropAreaGUI(Rect dropRect, int textureIndex)
		{
			Event evt = Event.current;
			//GUI.Box(dropRect, "Add Trigger"); // draw for debugging

			switch (evt.type)
			{
				case EventType.DragUpdated:
				case EventType.DragPerform:
				if (!dropRect.Contains(evt.mousePosition))
					return;

				DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

				if (evt.type == EventType.DragPerform)
				{
					DragAndDrop.AcceptDrag();

					foreach (Object draggedObject in DragAndDrop.objectReferences)
					{
						if (draggedObject != null)
						{
							Texture2D droppedTexture = draggedObject as Texture2D;
							if (droppedTexture != null)
							{
								ChannelTexture texture = _inputTextureList[textureIndex];

								texture.FilePath = AssetDatabase.GetAssetPath(droppedTexture);
								Texture2D newTexture = new Texture2D(2, 2, TCPStyles.PNG_FORMAT, false);
								byte[] bytes = File.ReadAllBytes(texture.FilePath);
								newTexture.LoadImage(bytes);
								newTexture.Apply();

								texture.Texture = newTexture;
								texture.Inverted = false;
								_shouldUpdatePreview = true;
								break;
							}
						}
					}
				}
				break;
			}
		}
	}
}