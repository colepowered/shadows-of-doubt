﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceTime : Evidence
{
    public float timeFrom;
    public float timeTo;
    public string duration;

    //Constructor
    public EvidenceTime(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        //Parse data from ID key...
        string[] keyParse = evID.Split('|');

        if(keyParse.Length > 1)
        {
            float.TryParse(keyParse[1], out timeFrom);
        }

        if (keyParse.Length > 2)
        {
            float.TryParse(keyParse[2], out timeTo);
        }

        if (keyParse.Length > 4)
        {
            int from = -1;
            int.TryParse(keyParse[4], out from);

            CityData.Instance.GetHuman(from, out writer);
        }

        if (keyParse.Length > 5)
        {
            int from = -1;
            int.TryParse(keyParse[5], out from);

            CityData.Instance.GetHuman(from, out reciever);
        }

        float d = timeTo - timeFrom;
        duration = SessionData.Instance.DecimalToTimeLengthString(d);

        //Add to evidence date
        GameplayController.Instance.timeEvidence.Add(this);
    }

    //Override data sources: This needs it's own, not the owners like in EvidenceItem
    public override void BuildDataSources()
    {
        //if (dataSource == null) dataSource = new Dictionary<string, EvidenceLinkData>();

        //string time = string.Empty;

        //if (timeFrom == timeTo)
        //{
        //    time += SessionData.Instance.TimeStringOnDay(timeFrom, false, true);
        //}
        //else
        //{
        //    time += SessionData.Instance.TimeStringOnDay(timeFrom, false, true) + " — " + SessionData.Instance.TimeStringOnDay(timeTo, false, true);
        //}

        //dataSource.Add("time", new EvidenceLinkData(time));

        //if (!dataSource.ContainsKey("summary"))
        //{
        //    string summary = Strings.ComposeText(Strings.Get("evidence.body", preset.name, Strings.Casing.asIs), ref dataSource, this, true);
        //    dataSource.Add("summary", new EvidenceLinkData(summary));
        //}
    }

    //Name
    public override string GenerateName()
    {
        string ret = string.Empty;

        if (timeFrom == timeTo)
        {
            ret = SessionData.Instance.TimeAndDate(timeFrom, false, true, true);
        }
        else
        {
            ret = SessionData.Instance.TimeAndDate(timeFrom, false, true, true) + " — " + SessionData.Instance.TimeAndDate(timeTo, false, true, true);
        }

        SetNote(Toolbox.Instance.allDataKeys, ret);

        return ret;
    }

    //Set ineractable name to evidence name
    public override void OnDiscovery()
    {
        base.OnDiscovery();

        UpdateName();
    }
}
