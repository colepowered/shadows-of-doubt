﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Subclass of evidence/item , this holds data of documents
//Script pass 1
//[System.Serializable]
public class DBWitnessStatement //: DBFile
{
 //   //Belongs to
 //   public CitizenController belongsTo;

	////A list of witness information
	//public List<WitnessStatementInfo> witnessElements = new List<WitnessStatementInfo>();

 //   //Case reference
 //   public CaseReference caseRef;

 //   //List of memories from 12 hours prior to 12 hours after
 //   public List<MemoryEvent> memoryPool = new List<MemoryEvent>();
 //   public List<MemoryEvent> alibiMemoryPool = new List<MemoryEvent>();
 //   public List<MemoryEvent> sightingMemoryPool = new List<MemoryEvent>();

 //   //List of memories that have been called upon
 //   public List<MemoryEvent> calledUponMemories = new List<MemoryEvent>();

 //   //Time of statement
 //   public float timeOfStatement = 0f;

 //   //Location of statement
 //   public LocationController locOfStatement;

 //   //Routine of statement
 //   public RoutineAction routineOfStatement;

 //   //List of processed timeline entries
 //   public List<ProcessedTimelineEntry> processedMemories = new List<ProcessedTimelineEntry>();
 //   public List<ProcessedTimelineEntry> processedLinks = new List<ProcessedTimelineEntry>();

 //   public List<MemoryEvent> suspectSightings = new List<MemoryEvent>();

 //   //Percent of called upon memories
 //   float alibiRating = 0f;
 //   float sightingRating = 0f;
 //   float lieDetectionRating = 0f;

 //   //Lying
 //   public List<LieGroup> lieGroups = new List<LieGroup>();
 //   public float calculatedLieWorryChance = 0f;
 //   public float calculatedLieRealChance = 0f;

 //   public DBWitnessStatement(EntryPreset ev, Dictionary<string, EvidenceDataSource> newSources, DBItem newParent, CaseReference newCaseRef) : base(ev, newSources, newParent)
 //   {
 //       Game.Log(SessionData.Instance.gameTime + ": Construct witness statement");
	//	caseRef = newCaseRef;
 //       belongsTo = (parent as DBCitizen).citizenController;
 //       timeOfStatement = SessionData.Instance.gameTime;

 //       //Add to case statement list
 //       if (!caseRef.statements.ContainsKey(belongsTo)) caseRef.statements.Add(belongsTo, this);
 //       else if (caseRef.statements[belongsTo] == null) caseRef.statements[belongsTo] = this;
 //   }

 //   public override void OnPreComp()
 //   {
 //       base.OnPreComp();

 //       if(belongsTo == null) belongsTo = (parent as DBCitizen).citizenController;

 //       EvidenceDataSource statementData = sources["statement_info"];
 //       UpdateStatement(statementData.customValue1, statementData.customValue2, statementData.customValue3);
 //   }

 //   //Called on creation and update
 //   public void UpdateStatement(float newAlibiRating, float newSightingRating, float newLieDetectionRating)
 //   {
 //       alibiRating = newAlibiRating;
 //       sightingRating = newSightingRating;
 //       lieDetectionRating = newLieDetectionRating;

 //       //Add peaved amount to citizen
 //       belongsTo.AddPeaved(SocialControls.Instance.basePeavedAmountOnQuestioning);

 //       //Calculate lie threshold worry; (80% base, 20% distress) * globalLieMultiplier
 //       calculatedLieWorryChance = ((belongsTo.liklinessToWorryLieFixed * 0.8f) + (belongsTo.GetDistress() * 0.2f)) * LieControls.Instance.globalLieMultiplierWorry;

 //       //Calculate lie threshold real; (95% base, 5% distress) * globalLieMultiplier
 //       calculatedLieRealChance = ((belongsTo.liklinessToRealLieFixed * 0.95f) + (belongsTo.GetDistress() * 0.05f)) * LieControls.Instance.globalLieMultiplierReal;

 //       Game.Log("Lie: Update statement; peaved: " + belongsTo.GetPeaved().ToString() + ", distress: " + belongsTo.GetDistress().ToString() + ", cooperation: "+ belongsTo.GetCooperation().ToString());

 //       //All previously called upon memories are no longer 'recent'
 //       foreach(MemoryEvent mem in calledUponMemories)
 //       {
 //           mem.recentlyCalledUpon = false;
 //       }

 //       //All previous lie groups are no longer 'recent'
 //       foreach (LieGroup lieGroup in lieGroups)
 //       {
 //           lieGroup.recentlyAltered = false;
 //       }

 //       //Set creation refs
 //       timeOfStatement = SessionData.Instance.gameTime;
 //       locOfStatement = belongsTo.movement.currentLocation;
 //       routineOfStatement = belongsTo.movement.routine;

 //       //If this citizen found the body, bind likeness to location
 //       foreach(CaseReference caseRef in PoliceDatabase.Instance.activeCases)
 //       {
 //           if(caseRef.murderDataSource.ContainsKey("reported_by"))
 //           {
 //               if(caseRef.murderDataSource["reported_by"].evidence == belongsTo.evidenceEntry)
 //               {
 //                   caseRef.murdered.evidenceEntry.AddInstanceBinding(DBItem.MysteryKey.likeness, DBItem.MysteryKey.location);
 //               }
 //           }

 //           //Make sure witness data is created and assign statement
 //           ItemInstance self = belongsTo.evidenceEntry.RetrieveInstance(DBItem.MysteryKey.likeness);
 //           WitnessData newWit = caseRef.AddWitnessData(self);
 //           newWit.SetWitnessStatement(this);
 //       }

 //       //Make sure the citizen is acquainted with the victim, along with suspects
 //       for (int i = 0; i < CasePanelController.Instance.spawnedEvidence.Count; i++)
 //       {
 //           ItemInstance inst = CasePanelController.Instance.spawnedEvidence[i] as ItemInstance;
 //           if (inst == null) continue;

 //           //Add if known name or likeness
 //           if (instance.mysteryKeys.Contains(DBItem.MysteryKey.likeness) || instance.mysteryKeys.Contains(DBItem.MysteryKey.name))
 //           {
 //               if (inst.parent.isItem)
 //               {
 //                   //Add known citizens
 //                   DBCitizen cit = inst.parent as DBCitizen;
 //                   CitizenController cc = null;

 //                   if (cit != null)
 //                   {
 //                       cc = cit.citizenController;

 //                       Acquaintance aq = null;

 //                       //If not acquainted with suspect, add acquainance as police investiagation
 //                       if (!belongsTo.FindAcquaintanceExists(cc, out aq))
 //                       {
 //                           belongsTo.AddAcquaintance(cc, 0f, Acquaintance.ConnectionType.policeInvestigation, false);
 //                       }
 //                       //else reveal connections here
 //                       else
 //                       {
 //                           foreach (DBDetail det in aq.connectionDetails)
 //                           {
 //                               if (!det.isFound)
 //                               {
 //                                   det.OnDiscoverThis();
 //                               }
 //                           }
 //                       }
 //                   }
 //               }
 //           }
 //       }

 //       //Get list of possible memories
 //       GetMemoryEvents(caseRef.murdered.timeOfDeath.timeOfDeath5Mins);

 //       //Calculate how many memories to call upon...
 //       //Use floor of cooperation * 0.5f
 //       float alibiRatingMod = Mathf.Max(alibiRating, belongsTo.GetCooperation() * 0.5f);
 //       float sightingRatingMod = Mathf.Max(alibiRating, belongsTo.GetCooperation() * 0.5f);

 //       int callUponAlibi = Mathf.RoundToInt(alibiRatingMod * alibiMemoryPool.Count);
 //       int callUponSightings = Mathf.RoundToInt(sightingRatingMod * sightingMemoryPool.Count);

 //       //Call upon memories
 //       CallUponMemories(callUponAlibi, callUponSightings);
 //   }

 //   //Compile a list of memory events from 12 hours before to 12 hours after
 //   private void GetMemoryEvents(float gameTime)
 //   {
 //       //Game.Log(SessionData.Instance.clock + ": Get memory events");
 //       float getEventsFrom = gameTime - 12f;
 //       float getEventsTo = gameTime + 12f;

 //       //Update memory events
 //       belongsTo.UpdateMemoryEvents();

 //       //Sort memories by time
 //       belongsTo.memoryEvents.Sort();

 //       for (int i = 0; i < belongsTo.memoryEvents.Count; i++)
 //       {
 //           MemoryEvent mem = belongsTo.memoryEvents[i];

 //           //If before start, continue
 //           if (mem.happenedAt < getEventsFrom) continue;

 //           //If after end, break
 //           if (mem.happenedAt > getEventsTo) continue;

 //           //Add all self related memories to pool
 //           if (mem.isSelfRelated)
 //           {
 //               //Add to memory pool
 //               AddToMemoryPool(mem, gameTime);
 //               continue;
 //           }
 //           //Add a sighting if it's an aquaintance or matches a given descriptor
 //           else
 //           {
 //               Acquaintance aq = null;
 //               bool isAquaintance = belongsTo.FindAcquaintanceExists(mem.witnessed, out aq);
                
 //               if (isAquaintance)
 //               {
 //                   //Update memory aqcaintance ref
 //                   mem.witnessedAcquaintanceRef = aq;

 //                   //Add to memory pool
 //                   AddToMemoryPool(mem, gameTime);
 //                   continue;
 //               }
 //               else
 //               {
 //                   //If not an acquaintance, add to the memory pool if citizen acting suspiciously.
 //                   if(mem.suspiciousBehaviour != RoutineAction.SuspiciousBehaviour.none)
 //                   {
 //                       if(mem.suspiciousValueReal > 0f)
 //                       {
 //                           //Add to memory pool
 //                           AddToMemoryPool(mem, gameTime);
 //                           continue;
 //                       }
 //                   }

 //                   //Add partial appearance matches

 //                   continue;
 //               }
 //           }
 //       }

 //       //Now there's a collection of memories, use a custom sort to arrange which ones to discover first
 //       memoryPool.Sort(MemoryEvent.MemoryComparison);

 //       //Reverse (highest first)
 //       memoryPool.Reverse();
 //   }

 //   //Adds a memory to pool of possible memories to be recalled- additive
 //   private void AddToMemoryPool(MemoryEvent mem, float gameTime)
 //   {
 //       if (!memoryPool.Contains(mem))
 //       {
 //           float diff = mem.happenedAt - gameTime;
 //           float rec = mem.witnessTimeAccuracy * 12f;

 //           //Set comparison variable: (12 - difference from time) + (recall accuracy * 12) + (random 0 - 3)
 //           mem.memoryComparison = (12f - Mathf.Abs(diff)) + rec + Random.Range(0f, 3f);

 //           //Add to pools
 //           memoryPool.Add(mem);

 //           if (mem.isSelfLocational) alibiMemoryPool.Add(mem);
 //           if (!mem.isSelfRelated) sightingMemoryPool.Add(mem);
 //       }
 //   }

 //   private void CallUponMemories(int numberOfAlibi, int numberOfSightings)
 //   {
 //       numberOfAlibi = Mathf.Min(numberOfAlibi, alibiMemoryPool.Count);
 //       numberOfSightings = Mathf.Min(numberOfSightings, sightingMemoryPool.Count);

 //       Game.Log("Lie: Calling upon " + numberOfAlibi + "/" + alibiMemoryPool.Count + " alibi memories (" + Mathf.RoundToInt(((float)numberOfAlibi / (float)alibiMemoryPool.Count) * 100f) + "%)");
 //       Game.Log("Lie: Calling upon " + numberOfSightings + "/" + sightingMemoryPool.Count + " sihgting memories (" + Mathf.RoundToInt(((float)numberOfSightings / (float)sightingMemoryPool.Count) * 100f) + "%)");

 //       int cursor = 0;

 //       //Call alibis first
 //       for (int i = 0; i < numberOfAlibi; i++)
 //       {
 //           for (int u = 0; u < alibiMemoryPool.Count; u++)
 //           {
 //               //Loop cursor
 //               if (cursor >= alibiMemoryPool.Count) cursor = 0;

 //               //If not called upon for this statement
 //               if (!alibiMemoryPool[cursor].calledForStatements.Contains(this))
 //               {
 //                   CallUponMemory(alibiMemoryPool[cursor]);

 //                   //Add arrive/depart events for free
 //                   if(alibiMemoryPool[cursor].arriveEvent != null && !alibiMemoryPool[cursor].arriveEvent.calledUpon)
 //                   {
 //                       CallUponMemory(alibiMemoryPool[cursor].arriveEvent);
 //                   }

 //                   if (alibiMemoryPool[cursor].departEvent != null && !alibiMemoryPool[cursor].departEvent.calledUpon)
 //                   {
 //                       CallUponMemory(alibiMemoryPool[cursor].departEvent);
 //                   }

 //                   cursor++;
 //                   break;
 //               }
 //               else
 //               {
 //                   //If already called upon for this statement, skip but count this as one of the recalled memories
 //                   cursor++;
 //                   break;
 //               }
 //           }
 //       }

 //       //Call sightings
 //       for (int i = 0; i < numberOfSightings; i++)
 //       {
 //           for (int u = 0; u < sightingMemoryPool.Count; u++)
 //           {
 //               //Loop cursor
 //               if (cursor >= sightingMemoryPool.Count) cursor = 0;

 //               //If not called upon for this statement
 //               if (!sightingMemoryPool[cursor].calledForStatements.Contains(this))
 //               {
 //                   CallUponMemory(sightingMemoryPool[cursor]);

 //                   //Add arrive/depart events for free
 //                   if (sightingMemoryPool[cursor].arriveEvent != null && !sightingMemoryPool[cursor].arriveEvent.calledUpon)
 //                   {
 //                       CallUponMemory(sightingMemoryPool[cursor].arriveEvent);
 //                   }

 //                   if (sightingMemoryPool[cursor].departEvent != null && !sightingMemoryPool[cursor].departEvent.calledUpon)
 //                   {
 //                       CallUponMemory(sightingMemoryPool[cursor].departEvent);
 //                   }

 //                   cursor++;
 //                   break;
 //               }
 //               else
 //               {
 //                   //If already called upon for this statement, skip but count this as one of the recalled memories
 //                   cursor++;
 //                   break;
 //               }
 //           }
 //       }

 //       //Game.Log("Called upon " + calledUponMemories.Count + " new memories");
 //       //Apply secrets & lies
 //       SecretsAndLiesCheck();

 //       //Arrange called upon memories
 //       ArrangeCalledUponMemories();

 //       //Process memories for timeline
 //       ProcessMemories();
 //   }

 //   //Call upon new memory
 //   public void CallUponMemory(MemoryEvent mem)
 //   {
 //        //Reveal connection details pertaining to sighting upon calling upon this memory
 //       if (!mem.isSelfRelated)
 //       {
 //           Acquaintance aq = null;
 //           mem.witness.FindAcquaintanceExists(mem.witnessed, out aq);

 //           //Acquaintance
 //           if (aq != null)
 //           {
 //               foreach (DBDetail det in aq.connectionDetails)
 //               {
 //                   if (!det.isFound)
 //                   {
 //                       det.OnDiscoverThis();
 //                   }
 //               }
 //           }

 //           //Make sure the locational memory relating to this sighting is also revealed
 //           //Search for arrival event by seeking backwards
 //           try
 //           {
 //               int poolCursorPos = memoryPool.IndexOf(mem);

 //               for (int u = poolCursorPos; u >= 0; u--)
 //               {
 //                   if (memoryPool[u].isSelfLocational)
 //                   {
 //                       //Add arrive/depart events for free
 //                       if (memoryPool[u].arriveEvent != null && !memoryPool[u].arriveEvent.calledUpon)
 //                       {
 //                           CallUponMemory(memoryPool[u].arriveEvent);
 //                       }

 //                       if (memoryPool[u].departEvent != null && !memoryPool[u].departEvent.calledUpon)
 //                       {
 //                           CallUponMemory(memoryPool[u].departEvent);
 //                       }
 //                   }
 //               }
 //           }
 //           catch
 //           {
 //               //
 //           }
 //       }

 //       mem.calledForStatements.Add(this);

 //       //Return if already called upon
 //       if (calledUponMemories.Contains(mem)) return;

 //       //Called upon in this statement update
 //       mem.recentlyCalledUpon = true;

 //       //Call upon memory normal
 //       if (!mem.isFabrication)
 //       {
 //           mem.CallUponMemory(false, 0f);
 //       }
 //       //call upon lie
 //       else
 //       {
 //           mem.CallUponMemory(true, Random.Range(0f, 0.5f));
 //       }
        
 //       calledUponMemories.Add(mem);
 //   }

 //   //Lie Scan - search for memories to cover up
 //   public void SecretsAndLiesCheck()
 //   {
 //       Game.Log("Update: DBWitnessStatement Secrets & lies check...");

 //       //Lie detection must beat a range set by the lier's; intelligence 35% + inverse emotionality 10% + conscientousness 45% + interaction 10%
 //       float lieSkill = (belongsTo.intelligence * 0.35f) + ((1f - belongsTo.emotionality) * 0.1f) + (belongsTo.conscientiousness * 0.45f) + (belongsTo.interactionSkill * 0.1f);

 //       //Chance of false positive lie detection is based on this range and scaled by the lie detection skill
 //       float chanceOfFalsePositive = Mathf.Lerp(DepartmentControls.Instance.lieDetectionFalsePositiveRange.x, DepartmentControls.Instance.lieDetectionFalsePositiveRange.y, lieDetectionRating);

 //       //Sort by time
 //       calledUponMemories.Sort();

 //       //Seek for core suspicious actions & assign 'worry' suspicion
 //       for (int i = 0; i < calledUponMemories.Count; i++)
 //       {
 //           MemoryEvent called = calledUponMemories[i];

 //           //Assign worry- based on proimity and time of death
 //           if (called.isSelfLocational)
 //           {
 //               called.suspiciousValueWorry = CalculateWorry(called.memoryLocation, called.happenedAt, called.witness);
 //           }

 //           //A new lie will always start with an arrive event
 //           if (called.encounterType == MemoryEvent.MemoryType.selfArrive && called.lieGroup == null)
 //           {
 //               //If suspicious, attempt to cover it up with lies!
 //               if (called.suspiciousValueReal > 0f || called.suspiciousValueWorry > 0f)
 //               {
 //                   Game.Log("Lie: Suspicious memory found: " + called.name + " real: " + called.suspiciousValueReal + ", worry: " + called.suspiciousValueWorry);

 //                   //To determin a lie or not, run through 2 seperate checks.
 //                   bool chooseToLie = false;

 //                   //0: Chances test
 //                   //First if this activity is actually suspicious, use the 'real' check value. A suspicious value of 1 will always get covered with a lie.
 //                   if (Random.Range(0f, calculatedLieRealChance) >= 1f - called.suspiciousValueReal)
 //                   {
 //                       called.debugLieChanceRecord = "Real lie: " + calculatedLieRealChance.ToString() + " beat " + (1f - called.suspiciousValueReal).ToString();
 //                       chooseToLie = true;
 //                   }

 //                   //Secondly, if this activity garners worry (based on location/time of memory)
 //                   if (!chooseToLie)
 //                   {
 //                       if (Random.Range(0f, calculatedLieWorryChance) >= 1f - called.suspiciousValueWorry)
 //                       {
 //                           called.debugLieChanceRecord = "Worry lie: " + calculatedLieWorryChance.ToString() + " beat " + (1f - called.suspiciousValueWorry).ToString();
 //                           chooseToLie = true;
 //                       }
 //                   }

 //                   //chooseToLie = true;

 //                   if (chooseToLie)
 //                   {
 //                       //Get the departure index
 //                       int nextDepartIndex = FindFollowingDepart(i);

 //                       //First check if a lie is possible...
 //                       //1: Is there a departure event?
 //                       if (nextDepartIndex > -1)
 //                       {
 //                           MemoryEvent nextDepart = calledUponMemories[nextDepartIndex];

 //                           //2: Does my time at this location include any unconcealable memories? If so choose not to lie...
 //                           bool hasUnconcealable = false;

 //                           for (int u = i; u <= nextDepartIndex; u++)
 //                           {
 //                               if (calledUponMemories[u].unconcealable)
 //                               {
 //                                   hasUnconcealable = true;
 //                                   break;
 //                               }
 //                           }

 //                           if (!hasUnconcealable)
 //                           {
 //                               //3: For a valid addition, it also needs the arrival following that (that's not concealed)
 //                               Game.Log("Lie: ... Next depart found: " + nextDepart.name + ". Checking for following arrival.");
 //                               int followingArrivalIndex = FindFollowingArrive(nextDepartIndex);

 //                               if (followingArrivalIndex > -1)
 //                               {
 //                                   //If found event is concealed by a lie already, add current to lie group
 //                                   if (nextDepart.lieGroup != null)
 //                                   {
 //                                       nextDepart.lieGroup.AddMemory(called);
 //                                       nextDepart.lieGroup.UpdatePeripharyData();
 //                                   }
 //                                   //Else create new lie group and assign both
 //                                   else
 //                                   {
 //                                       LieGroup newLie = new LieGroup(this);
 //                                       lieGroups.Add(newLie);
 //                                       newLie.AddMemory(called);
 //                                       newLie.AddMemory(nextDepart);
 //                                       newLie.UpdatePeripharyData();
 //                                   }
 //                               }
 //                               else
 //                               {
 //                                   called.debugLieChanceRecord += " (Lie failed: Following next departure arrival event could not be found)";
 //                                   Game.Log("Lie: ...Following arrival could not be found- " + called.name + " & " + nextDepart.name + " will not be added to the lie group");
 //                               }
 //                           }
 //                           else
 //                           {
 //                               called.debugLieChanceRecord += " (Lie failed: Location includes an unoncealable event)";
 //                               Game.Log("Lie: Location includes an unconcealable event. Skipping...");
 //                           }
 //                       }
 //                       else
 //                       {
 //                           called.debugLieChanceRecord += " (Lie failed: No next depart event)";
 //                           Game.Log("Lie: No next depart event. Skipping...");
 //                       }
 //                   }
 //                   else
 //                   {
 //                       called.debugLieChanceRecord += " (Lie failed: Test chances failed)";
 //                       Game.Log("Lie: Test chances resulted in no lie. Skipping...");
 //                   }
 //               }
 //           }
 //       }

 //       //Test for merging of lies; it's best to keep things simple!
 //       lieGroups.Sort();
 //       Game.Log("Lie: " + lieGroups.Count + " Lie groups created.");

 //       //Merge lie groups with no other destination inbetween
 //       for (int i = 0; i < lieGroups.Count - 1; i++)
 //       {
 //           LieGroup group = lieGroups[i];

 //           //Sort memories by time
 //           group.memoryContents.Sort();

 //           //Get index of the truth's end
 //           int endIndex = calledUponMemories.IndexOf(group.peripharyLieEnd);

 //           //Keep track of memories inbetween the two groups, as these will also need to be added if merged.
 //           List<MemoryEvent> inbetweenConceal = new List<MemoryEvent>();

 //           for (int u = endIndex + 1; u < calledUponMemories.Count; u++)
 //           {
 //               MemoryEvent mem = calledUponMemories[u];

 //               //Add this memory(depart event) as it will need to be concealed if we find a group to merge with
 //               inbetweenConceal.Add(mem);

 //               if (mem.encounterType == MemoryEvent.MemoryType.selfDepart && mem.lieGroup != null)
 //               {
 //                   //Merge with this...
 //                   if (mem.lieGroup != group)
 //                   {
 //                       LieGroup mergeTarget = mem.lieGroup;
 //                       List<MemoryEvent> mergeMemories = new List<MemoryEvent>(mergeTarget.memoryContents);

 //                       //Merge group
 //                       foreach (MemoryEvent memory in mergeMemories)
 //                       {
 //                           group.AddMemory(memory);
 //                       }

 //                       //Merge inbetween events
 //                       foreach (MemoryEvent memory in inbetweenConceal)
 //                       {
 //                           group.AddMemory(memory);
 //                       }

 //                       //Remove target group
 //                       lieGroups.Remove(mergeTarget);

 //                       group.UpdatePeripharyData();

 //                       Game.Log("Lie: Lie groups merged. There are now " + lieGroups.Count + " groups");

 //                       break;
 //                   }
 //                   //Within this group
 //                   else
 //                   {
 //                       group.UpdatePeripharyData();
 //                       break;
 //                   }
 //               }
 //               //If this is another self locational, but with no lie group, then do not merge.
 //               else if (mem.isSelfLocational)
 //               {
 //                   break;
 //               }
 //           }
 //       }

 //       //Pass over to the lie group script to setup finer details. Remove null /empty groups.
 //       for (int i = 0; i < lieGroups.Count; i++)
 //       {
 //           LieGroup group = lieGroups[i];

 //           //Remove null and empty
 //           if (group == null || group.memoryContents.Count <= 0)
 //           {
 //               lieGroups.RemoveAt(i);
 //               Game.Log("Lie: Empty lie group removed. There are now " + lieGroups.Count + " groups.");
 //               i--;
 //               continue;
 //           }

 //           //Calculate and execute lie
 //           if (group.recentlyAltered)
 //           {
 //               group.CalculateLie();

 //               if (!DebugControls.Instance.disableLieExecution)
 //               {
 //                   group.ExecuteLie();
 //               }
 //           }
 //       }

 //       calledUponMemories.Sort();
 //       lieGroups.Sort();

 //       //Match up depart-arrive motives, add motive deceptions if needed, implement lie detection
 //       if (!DebugControls.Instance.disableLieExecution)
 //       {
 //           for (int i = 0; i < calledUponMemories.Count; i++)
 //           {
 //               MemoryEvent mem = calledUponMemories[i];

 //               //Detect lies- only lies that have been changed in this statement update have a chance of being detected
 //               if (mem.recentlyCalledUpon)
 //               {
 //                   //Use a chance based system with 1/3 of lie skill as the floor
 //                   if (mem.bentTruth || mem.isFabrication || mem.motiveDeception)
 //                   {
 //                       float lieValue = Random.Range(lieSkill * 0.33f, lieSkill);

 //                       if (lieDetectionRating >= lieValue)
 //                       {
 //                           Game.Log("Lie: Detection rating " + lieDetectionRating + " beats " + lieValue + "; lie detected!");
 //                           mem.SuspectAsLie(false);
 //                       }
 //                       else
 //                       {
 //                           Game.Log("Lie: Detection rating " + lieDetectionRating + " did not beat " + lieValue + "; lie not detected!");
 //                       }
 //                   }

 //                   //Assign false positive lie detection
 //                   if (chanceOfFalsePositive >= Random.Range(0f, 1f))
 //                   {
 //                       Game.Log("Lie: False positive lie! Chance: " + chanceOfFalsePositive.ToString());
 //                       mem.SuspectAsLie(true);
 //                   }
 //               }

 //               //Match up depart-arrive motives, add motive deceptions if needed
 //               if (mem.encounterType == MemoryEvent.MemoryType.selfDepart)
 //               {
 //                   //Find the following arrive event
 //                   int nextArriveIndex = FindFollowingArrive(i);
 //                   if (nextArriveIndex > -1)
 //                   {
 //                       MemoryEvent arrive = calledUponMemories[nextArriveIndex];

 //                       //Does the routine from the depart match the routine from this arrive?
 //                       if (mem.witnessRoutine != null && arrive.witnessRoutine != null)
 //                       {
 //                           if (mem.witnessRoutine == arrive.witnessRoutine)
 //                           {
 //                               continue;
 //                           }
 //                           //If this doesn't match, it could be that the citizen changed their mind while travelling (legal)... Or one is a lie/concealed.
 //                           //For simplicity's sake, always match the motive with the arrive event, but never assign deception to a changed motive.
 //                           else
 //                           {
 //                               Game.Log("Lie: Mismatching routine found for " + mem.name);

 //                               mem.SetMotiveDeception(arrive.witnessRoutine);

 //                               ////Depart contains suspicious behaviour
 //                               //if (mem.witnessRoutine.suspicious != RoutineAction.SuspiciousBehaviour.none)
 //                               //{
 //                               //    Game.Log("Lie: Depart event " + mem.name + " was found to be suspicious...");

 //                               //    //Arive does not contain suspicious behaviour
 //                               //    if (arrive.witnessRoutine.suspicious == RoutineAction.SuspiciousBehaviour.none)
 //                               //    {
 //                               //        Game.Log("Lie: Copying non-supicious routine from arrive event.");

 //                               //        //The depart copies the routine from arive
 //                               //        mem.SetMotiveDeception(arrive.witnessRoutine);
 //                               //    }
 //                               //    //Else both are suspicious- this should not happen
 //                               //    else
 //                               //    {
 //                               //        Game.Log("Lie: Both depart and arrival are suspicious! Force a non reveal of routine for this memory.");
 //                               //        //TODO: Force the routine details of this memory to not be shared.
 //                               //    }
 //                               //}

 //                               ////Arrive contains suspicious behaviour
 //                               //if (arrive.witnessRoutine.suspicious != RoutineAction.SuspiciousBehaviour.none)
 //                               //{
 //                               //    Game.Log("Lie: Arrive event " + arrive.name + " was found to be suspicious...");

 //                               //    //Depart does not contain suspicious behaviour
 //                               //    if (mem.witnessRoutine.suspicious == RoutineAction.SuspiciousBehaviour.none)
 //                               //    {
 //                               //        Game.Log("Lie: Copying non-supicious routine from depart event.");

 //                               //        //The arrive copies the routine from depart
 //                               //        arrive.SetMotiveDeception(mem.witnessRoutine);
 //                               //    }
 //                               //    //Else both are suspicious- this should not happen
 //                               //    else
 //                               //    {
 //                               //        Game.Log("Lie: Both depart and arrival are suspicious! Force a non reveal of routine for this memory.");
 //                               //        //TODO: Force the routine details of this memory to not be shared.
 //                               //    }
 //                               //}
 //                           }
 //                       }
 //                   }
 //               }
 //               else if (mem.encounterType == MemoryEvent.MemoryType.selfArrive)
 //               {
 //                   if (mem.witnessRoutine != null)
 //                   {
 //                       //A single arrive event is found with suspicious behaviour
 //                       if (mem.witnessRoutine.suspicious != RoutineAction.SuspiciousBehaviour.none)
 //                       {
 //                           Game.Log("Lie: Single arrive event found. Force a non reveal of routine for this memory.");
 //                           //TODO: Force the routine details of this memory to not be shared.
 //                       }
 //                   }
 //               }
 //           }
 //       }
 //   }

 //   //Arrange called upon memories
 //   public void ArrangeCalledUponMemories()
 //   {
 //       //Sort by time
 //       calledUponMemories.Sort();

 //       for (int i = 0; i < calledUponMemories.Count; i++)
 //       {
 //           MemoryEvent called = calledUponMemories[i];

 //           called.debugListPosition = i;

 //           MemoryEvent previousSelfLocational = null;

 //           for (int u = i - 1; u >= 0; u--)
 //           {
 //               if(calledUponMemories[u].isSelfLocational)
 //               {
 //                   previousSelfLocational = calledUponMemories[u];
 //                   break;
 //               }
 //           }

 //           MemoryEvent nextSelfLocational = null;

 //           for (int u = i + 1; u < calledUponMemories.Count; u++)
 //           {
 //               if (calledUponMemories[u].isSelfLocational)
 //               {
 //                   nextSelfLocational = calledUponMemories[u];
 //                   break;
 //               }
 //           }

 //           //Remove illegal timing scenarios
 //           if (called.isSelfLocational)
 //           {
 //               if (nextSelfLocational != null)
 //               {
 //                   int correctionAttempts = 0;

 //                   while ((called.calledUponTimeStart >= nextSelfLocational.calledUponTimeStart ||
 //                       called.calledUponTimeEnd >= nextSelfLocational.calledUponTimeEnd)
 //                       && correctionAttempts < 10)
 //                   {
 //                       Game.Log("Timeline: Illegal timing detected. Correction attempts: " + correctionAttempts);

 //                       //Attempt to correct by increasing accuracy of called
 //                       if (called.witnessTimeAccuracy <= 0.9f)
 //                       {
 //                           Game.Log("Timeline: Increasing accuracy of current memory...");
 //                           called.originalAccuracy = Mathf.Max(called.witnessTimeAccuracy, called.originalAccuracy);
 //                           called.memoryOrderingCorrectionAttempts++;
 //                           called.CallUponMemory(true, Mathf.Clamp01(called.witnessTimeAccuracy + 0.1f));
 //                       }

 //                       //If still illegal, increase accuracy of next memory
 //                       if (called.calledUponTimeStart >= nextSelfLocational.calledUponTimeStart ||
 //                       called.calledUponTimeEnd >= nextSelfLocational.calledUponTimeEnd)
 //                       {
 //                           if(nextSelfLocational.witnessTimeAccuracy <= 0.9f)
 //                           {
 //                               Game.Log("Timeline: Increasing accuracy of next memory...");
 //                               nextSelfLocational.originalAccuracy = Mathf.Max(nextSelfLocational.witnessTimeAccuracy, nextSelfLocational.originalAccuracy);
 //                               nextSelfLocational.memoryOrderingCorrectionAttempts++;
 //                               nextSelfLocational.CallUponMemory(true, Mathf.Clamp01(nextSelfLocational.witnessTimeAccuracy + 0.1f));
 //                           }
 //                       }
 //                       else break;

 //                       correctionAttempts++;
 //                   }
 //               }
 //           }
 //           //Make sure sightings inside buildings are tied to arrival/departure events if needed
 //           if (called.encounterType == MemoryEvent.MemoryType.sightingHere)
 //           {
 //               //Tie to previous
 //               if(previousSelfLocational != null)
 //               {
 //                   if (called.calledUponTimeStart < previousSelfLocational.calledUponTimeStart)
 //                   {
 //                       Game.Log("Timeline: " + called.name + ": Tie sighting to previous self locational.");
 //                       previousSelfLocational.AddTiedMemoryToThis(called);
 //                   }
 //               }

 //               //Tie to next
 //               if(nextSelfLocational != null)
 //               {
 //                   if (called.calledUponTimeEnd > nextSelfLocational.calledUponTimeEnd)
 //                   {
 //                       Game.Log("Timeline: " + called.name +  ": Tie sighting to next self locational.");
 //                       nextSelfLocational.AddTiedMemoryToThis(called);
 //                   }
 //               }
 //           }
 //           //Make sure sightings on street are tied to arrival/departure events if needed
 //           else if (called.encounterType == MemoryEvent.MemoryType.sightingStreet)
 //           {
 //               //Tie to previous
 //               if (previousSelfLocational != null)
 //               {
 //                   if(previousSelfLocational.encounterType == MemoryEvent.MemoryType.selfDepart)
 //                   {
 //                       if (called.calledUponTimeStart < previousSelfLocational.calledUponTimeStart)
 //                       {
 //                           Game.Log("Timeline: " + called.name + ": Tie sighting to previous self locational.");
 //                           previousSelfLocational.AddTiedMemoryToThis(called);
 //                       }
 //                   }
 //               }

 //               //Tie to next
 //               if (nextSelfLocational != null)
 //               {
 //                   if (nextSelfLocational.encounterType == MemoryEvent.MemoryType.selfArrive)
 //                   {
 //                       if (called.calledUponTimeEnd > nextSelfLocational.calledUponTimeEnd)
 //                       {
 //                           Game.Log("Timeline: " + called.name + ": Tie sighting to next self locational.");
 //                           nextSelfLocational.AddTiedMemoryToThis(called);
 //                       }
 //                   }
 //               }
 //           }
 //       }
 //   }

 //   //Find following arrive event using index
 //   public int FindFollowingArrive(int fromThisIndex)
 //   {
 //       //Make sure list is sorted by time
 //       calledUponMemories.Sort();

 //       //Look for next arrive
 //       for (int u = fromThisIndex + 1; u < calledUponMemories.Count; u++)
 //       {
 //           //Found next depart
 //           if (calledUponMemories[u].encounterType == MemoryEvent.MemoryType.selfArrive)
 //           {
 //               return u;
 //           }
 //       }

 //       return -1;
 //   }

 //   //Find following depart event using index
 //   public int FindFollowingDepart(int fromThisIndex)
 //   {
 //       //Make sure list is sorted by time
 //       calledUponMemories.Sort();

 //       //Look for next depart
 //       for (int u = fromThisIndex + 1; u < calledUponMemories.Count; u++)
 //       {
 //           //Found next depart
 //           if (calledUponMemories[u].encounterType == MemoryEvent.MemoryType.selfDepart)
 //           {
 //               return u;
 //           }
 //       }

 //       return -1;
 //   }

 //   //Calculate a 'worry' value- how worried I am about this memory considering crime scene location and time of death
 //   float CalculateWorry(LocationController location, float atTime, CitizenController citizen)
 //   {
 //       float worryScore = 0f;

 //       foreach(KeyValuePair<LocationController, float> pair in caseRef.reportedCrimeScenes)
 //       {
 //           //How far from crime scene in time
 //           float timeAwayFromCrimeScene = Toolbox.Instance.TravelTimeEstimate(belongsTo, location, pair.Key, true, true);

 //           //Score 0-1 with 0 being 30 mins from crime scene
 //           float worryProximity = Mathf.Clamp01(0.5f - timeAwayFromCrimeScene) * 2f;

 //           //Score 0-1 with 0 being 8 hours from time of death range midpoint
 //           float worryTiming = 1f - (Mathf.Clamp(Mathf.Abs(caseRef.murdered.timeOfDeath.timeOfDeathMidpoint - atTime), 0f, 8f) / 8f);

 //           //* together above for maximum worry score
 //           worryScore = Mathf.Max(worryScore, worryProximity * worryTiming);
 //       }

 //       return worryScore;
 //   }

 //   //Processed memories for timeline & relating evidence
 //   //Update memories
 //   public void ProcessMemories()
 //   {
 //       Game.Log("Update: DBWitnessStatement Process Memories: " + calledUponMemories.Count);

 //       //Build a list of required entries
 //       List<MemoryEvent> required = new List<MemoryEvent>();

 //       //Update arrive/depart on each memory to be consistent with lies told.
 //       calledUponMemories.Sort();

 //       for (int i = 0; i < calledUponMemories.Count; i++)
 //       {
 //           MemoryEvent mem1 = calledUponMemories[i];

 //           //Found arrive event
 //           if (mem1.encounterType == MemoryEvent.MemoryType.selfArrive)
 //           {
 //               mem1.arriveEvent = mem1;
 //               mem1.departEvent = null;

 //               //Find next depart
 //               for (int u = i + 1; u < calledUponMemories.Count; u++)
 //               {
 //                   MemoryEvent mem2 = calledUponMemories[u];

 //                   if (mem2.encounterType == MemoryEvent.MemoryType.selfDepart)
 //                   {
 //                       mem1.departEvent = mem2;

 //                       mem2.arriveEvent = mem1;
 //                       mem2.departEvent = mem2;
 //                       break;
 //                   }
 //               }
 //           }

 //           //Add to required.
 //           if (!required.Contains(mem1)) required.Add(mem1);
 //       }

 //       //Add an artifical memory for creation of statement
 //       if (timeOfStatement < caseRef.caseZeroTime + caseRef.timelineStartingHoursPerSide)
 //       {
 //           float crowdedness = 0f;
 //           if (locOfStatement != null) crowdedness = locOfStatement.GetCrowdedness();

 //           MemoryEvent statementCreation = new MemoryEvent(belongsTo, null, timeOfStatement, MemoryEvent.MemoryType.witnessStatementTime, locOfStatement, routineOfStatement, null, null, RoutineAction.SuspiciousBehaviour.none, 0f, false, crowdedness);
 //           statementCreation.SetUnconcealable();
 //           statementCreation.CallUponMemory(true, 1f);
 //           required.Add(statementCreation);
 //       }

 //       //Go through existing list
 //       for (int i = 0; i < processedMemories.Count; i++)
 //       {
 //           ProcessedTimelineEntry spawned = processedMemories[i];

 //           //If a null entry, this has obviously been destroyed- remove entry
 //           if (spawned == null)
 //           {
 //               processedMemories.RemoveAt(i);
 //               i--;
 //               continue;
 //           }

 //           MemoryEvent mem = spawned.fromMemory;

 //           //If this is supposed to exist, remove from found files list
 //           if (required.Contains(mem))
 //           {
 //               //Memories are not changed after creation, but if in the future they are, update created entries here...
 //               required.Remove(mem);
 //               continue;
 //           }
 //           //If it's not found in the list, it's safe to remove
 //           else
 //           {
 //               processedMemories.RemoveAt(i);
 //               i--;
 //               continue;
 //           }

 //           //On update statement
 //           spawned.OnUpdateStatement();
 //       }

 //       //The ones left in the found files list are memories to process
 //       foreach (MemoryEvent mem in required)
 //       {
 //           processedMemories.Add(new ProcessedTimelineEntry(mem, null, ProcessedTimelineEntry.TimelineEntryType.memory, caseRef, this));
 //       }

 //       //Spawn details for any processed memories
 //       foreach(ProcessedTimelineEntry pte in processedMemories)
 //       {
 //           if (pte.createdFact != null) continue;

 //           //Create alibis for links
 //           else if (pte.entryType == ProcessedTimelineEntry.TimelineEntryType.link)
 //           {
 //               //Was this same address
 //               if(pte.fromMemory.memoryLocation == caseRef.murdered.locationOfDeath)
 //               {
 //                   pte.createdFact = EvidenceCreator.Instance.CreateEvidence("AlibiSame", belongsTo.evidenceEntry, pte.fromMemory.memoryLocation.evidenceEntry, pte) as DBDetail;
 //               }
 //               //Was this close
 //               else if(pte.minutesFromCrimeScene <= 5)
 //               {
 //                   pte.createdFact = EvidenceCreator.Instance.CreateEvidence("AlibiClose", belongsTo.evidenceEntry, pte.fromMemory.memoryLocation.evidenceEntry, pte) as DBDetail;
 //               }
 //           }
 //           //Create sightings
 //           else if (pte.entryType == ProcessedTimelineEntry.TimelineEntryType.memory)
 //           {
 //               if(!pte.fromMemory.isSelfRelated)
 //               {
 //                   //Was this same address
 //                   if (pte.fromMemory.memoryLocation == caseRef.murdered.locationOfDeath)
 //                   {
 //                       pte.createdFact = EvidenceCreator.Instance.CreateEvidence("SeenAtSame", pte.fromMemory.witnessed.evidenceEntry, belongsTo.evidenceEntry, pte) as DBDetail;

 //                       if (pte.createdFact != null)
 //                       {
 //                           if (pte.fromMemory != null)
 //                           {
 //                               if (pte.fromMemory.memoryLocation != null)
 //                               {
 //                                   if (pte.fromMemory.memoryLocation.evidenceEntry != null)
 //                                   {
 //                                       pte.fromMemory.memoryLocation.evidenceEntry.AddToDetails(pte.createdFact);
 //                                   }
 //                               }
 //                           }
 //                       }
 //                   }
 //                   //Was this close
 //                   else if (pte.minutesFromCrimeScene <= 5)
 //                   {
 //                       pte.createdFact = EvidenceCreator.Instance.CreateEvidence("SeenAtClose", pte.fromMemory.witnessed.evidenceEntry, belongsTo.evidenceEntry, pte) as DBDetail;

 //                       if (pte.createdFact != null)
 //                       {
 //                           if (pte.fromMemory != null)
 //                           {
 //                               if(pte.fromMemory.memoryLocation != null)
 //                               {
 //                                   if(pte.fromMemory.memoryLocation.evidenceEntry != null)
 //                                   {
 //                                       pte.fromMemory.memoryLocation.evidenceEntry.AddToDetails(pte.createdFact);
 //                                   }
 //                               }
 //                           }
 //                       }
 //                   }
 //               }
 //           }
 //       }

 //       //Update memory links
 //       UpdateMemoryLinks();
 //   }

 //   //Update links: Do this after updating memories as it uses them to create the links
 //   public void UpdateMemoryLinks()
 //   {
 //       //Build a list of required entries
 //       List<MemoryEvent> required = new List<MemoryEvent>();

 //       foreach (ProcessedTimelineEntry mem in processedMemories)
 //       {
 //           if (mem.fromMemory.isSelfLocational)
 //           {
 //               //If arrive exists (depart event can not exist)
 //               if (mem.fromMemory.arriveEvent != null && mem.fromMemory.arriveEvent.intialised)
 //               {
 //                   if (!required.Contains(mem.fromMemory.arriveEvent))
 //                   {
 //                       required.Add(mem.fromMemory.arriveEvent);
 //                   }
 //               }
 //           }
 //       }

 //       //Go through existing list
 //       for (int i = 0; i < processedLinks.Count; i++)
 //       {
 //           ProcessedTimelineEntry spawned = processedLinks[i];

 //           //If a null entry, this has obviously been destroyed- remove entry
 //           if (spawned == null)
 //           {
 //               processedLinks.RemoveAt(i);
 //               i--;
 //               continue;
 //           }

 //           MemoryEvent mem = spawned.fromMemory;

 //           //If this is supposed to exist, remove from found files list
 //           if (required.Contains(mem))
 //           {
 //               //Memories are not changed after creation, but if in the future they are, update created entries here...

 //               required.Remove(mem);
 //           }
 //           //If it's not found in the list, it's safe to remove
 //           else
 //           {
 //               processedLinks.RemoveAt(i);
 //               i--;
 //           }

 //           //On update statement
 //           spawned.OnUpdateStatement();
 //       }

 //       //The ones left in the found files list are buttons to spawn
 //       foreach (MemoryEvent mem in required)
 //       {
 //           processedLinks.Add(new ProcessedTimelineEntry(mem, mem.departEvent, ProcessedTimelineEntry.TimelineEntryType.link, caseRef, this));
 //       }
 //   }
}
