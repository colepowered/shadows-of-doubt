﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SideJobSabotage : SideJob
{
    public NewAddress chosenAddress;
    public float callTime;
    bool callTriggered = false;
    TelephoneController.PhoneCall call;
    Objective.ObjectiveTrigger getToPhone;

    public SideJobSabotage(JobPreset newPreset, SideJobController.JobPickData newData, bool immediatePost) : base(newPreset, newData, immediatePost)
    {
        GenerateFakeNumber();
    }

    public override void GameWorldLoop()
    {
        base.GameWorldLoop();

        //Objectives state loops
        if (thisCase != null && thisCase.isActive)
        {
            //Phase 0: Call the fake number
            if (phase == 0)
            {
                //Update fake call details
                if (Player.Instance.phoneInteractable != null)
                {
                    callTime = SessionData.Instance.gameTime + 0.1f;

                    //Pick a restuarant or public place with a phone that's open
                    if(chosenAddress == null)
                    {
                        List<NewAddress> payphoneAddressPool = new List<NewAddress>();

                        foreach (NewAddress ad in CityData.Instance.addressDirectory)
                        {
                            if (ad == Player.Instance.currentGameLocation) continue; //Somewhere different to where I am

                            //Not a residence
                            if (ad.residence == null)
                            {
                                //With telephones
                                if (ad.telephones.Count > 0)
                                {
                                    //Public facing company
                                    if (ad.company != null && ad.company.publicFacing)
                                    {
                                        //Open when I get there...
                                        if (ad.company.IsOpenAtThisTime(callTime))
                                        {
                                            payphoneAddressPool.Add(ad);
                                        }
                                    }
                                }
                            }
                        }

                        chosenAddress = payphoneAddressPool[Toolbox.Instance.Rand(0, payphoneAddressPool.Count)];
                    }

                    Game.Log("Job: Update data source details: " + chosenAddress.name + " and " + callTime);
                }

                //Only run this once
                if (phase != phaseChange)
                {
                    phaseChange = phase;
                    callTriggered = false;

                    if (!TelephoneController.Instance.fakeTelephoneDictionary.ContainsKey(fakeNumber))
                    {
                        //TelephoneController.Instance.AddFakeNumber(fakeNumber, new TelephoneController.CallSource(TelephoneController.CallType.player, preset.additionalDialog[0], this));
                    }

                    //TelephoneController.Instance.fakeTelephoneDictionary[fakeNumber].dialogGreeting = preset.additionalDialog[0];
                    //TelephoneController.Instance.fakeTelephoneDictionary[fakeNumber].dialog = preset.additionalDialog[0].name;

                    //Add turn on light objective
                    Objective.ObjectiveTrigger callFake = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.makeCall, fakeNumber.ToString());
                    AddObjective("CallFake", callFake, usePointer: false, onCompleteAction: Objective.OnCompleteAction.triggerSideJobFunction, useIcon: InterfaceControls.Icon.hand);
                }
            }
            //Phase 1: On fake number call
            else if (phase == 1 && Player.Instance.answeringPhone == null)
            {
                //Only run this once
                if (phase != phaseChange)
                {
                    phaseChange = phase;

                    Game.Log("Jobs: Player called fake number...");
                    callTriggered = false;

                    getToPhone = null;
                }

                //Trigger ringing after x time
                if (!callTriggered && SessionData.Instance.gameTime >= callTime)
                {
                    Game.Log("Jobs: Trigger the telephone call at " + chosenAddress.name);
                    Telephone payPhone = chosenAddress.telephones.Find(item => item.interactable.preset.isPayphone);
                    if (payPhone == null) Game.Log("Jobs: Cannot find payphone at " + chosenAddress.name + "...");

                    //Pick a payphone...
                    List<Telephone> allPhones = new List<Telephone>(CityData.Instance.phoneDictionary.Values);
                    List<Telephone> payPhones = allPhones.FindAll(item => item.interactable != null && item.interactable.preset.isPayphone && item.interactable.node.gameLocation.thisAsStreet != null);
                    Telephone chosenPhone = allPhones[Toolbox.Instance.Rand(0, allPhones.Count)];

                    if (payPhones.Count > 0)
                    {
                        chosenPhone = payPhones[Toolbox.Instance.Rand(0, payPhones.Count)];
                    }
                    else
                    {
                        payPhones = allPhones.FindAll(item => item.interactable != null && item.interactable.preset.isPayphone);

                        if (payPhones.Count > 0)
                        {
                            chosenPhone = payPhones[Toolbox.Instance.Rand(0, payPhones.Count)];
                        }
                    }

                    //call = TelephoneController.Instance.CreateNewCall(chosenPhone, payPhone, null, Player.Instance, new TelephoneController.CallSource(TelephoneController.CallType.player, preset.additionalDialog[1], this), 0.1f, true);
                    callTriggered = true;
                }
                else if(!callTriggered && Player.Instance.phoneInteractable == null && getToPhone == null)
                {
                    //Add get to phone objective
                    getToPhone = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: chosenAddress);
                    AddObjective("GetToPhone", getToPhone, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.run);
                }
                else if (callTriggered)
                {
                    //Check if player didn't catch the call...
                    if (call != null && call.state == TelephoneController.CallState.ended)
                    {
                        if(call.recevierNS == Player.Instance)
                        {
                            phase = 2;
                        }
                        else
                        {
                            phase = 3;
                        }
                    }
                }
            }
            //Phase 2: Call success
            else if (phase == 2)
            {
                //Only run this once
                if (phase != phaseChange)
                {
                    phaseChange = phase;

                    //OnAcquireJobInfo(preset.additionalDialog[1]);

                    TelephoneController.Instance.RemoveFakeNumber(fakeNumber);

                    //Add get to phone objective
                    Objective.ObjectiveTrigger sabotageRecover = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.evidencePinned, "", newEvidence: activeJobItems[JobPreset.JobTag.A].evidence);
                    AddObjective("SabotageRecoverInfo", sabotageRecover, usePointer: false, onCompleteAction: Objective.OnCompleteAction.triggerSideJobFunction, useIcon: InterfaceControls.Icon.pin);

                    Game.Log("Jobs: The player got the call. Setting up objectives...");
                }
            }
            //Phase 3: Call fail
            else if (phase == 3)
            {
                //Only run this once
                if (phase != phaseChange)
                {
                    phaseChange = phase;

                    getToPhone.Trigger();

                    //Set back to phase 1
                    Game.Log("Jobs: The player missed the call, sending them back to phase 0...");
                    phase = 0;
                }
            }
            ////Phase 4: Hand in
            //else if (phase == 4)
            //{
            //    //Only run this once
            //    if (phase != phaseChange)
            //    {
            //        phaseChange = phase;

            //        //Change fake number call to hand in...
            //        if (TelephoneController.Instance.fakeTelephoneDictionary.ContainsKey(fakeNumber))
            //        {
            //            TelephoneController.Instance.fakeTelephoneDictionary[fakeNumber].dialogGreeting = preset.additionalDialog[0];
            //            TelephoneController.Instance.fakeTelephoneDictionary[fakeNumber].dialog = preset.additionalDialog[0].name;
            //        }
            //        else
            //        {
            //            TelephoneController.Instance.AddFakeNumber(fakeNumber, new TelephoneController.CallSource(TelephoneController.CallType.player, preset.additionalDialog[2], this));
            //        }

            //        //Add hand in objectives...
            //        Objective.ObjectiveTrigger handIn = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.makeCall, fakeNumber.ToString());
            //        AddObjective("HandInSabotage", handIn, usePointer: false, onCompleteAction: Objective.OnCompleteAction.triggerSideJobHandIn, useIcon: InterfaceControls.Icon.hand);
            //    }
            //}
        }
    }

    public override void UpdateResolveAnswers()
    {
        foreach (Case.ResolveQuestion r in thisCase.resolveQuestions)
        {
            r.correctAnswers.Clear();
        }

        Case.ResolveQuestion doc = thisCase.resolveQuestions.Find(item => item.tag == JobPreset.JobTag.A);
        if (activeJobItems.ContainsKey(JobPreset.JobTag.A)) doc.correctAnswers.Add(activeJobItems[JobPreset.JobTag.A].id.ToString());

        base.UpdateResolveAnswers();
    }
}
