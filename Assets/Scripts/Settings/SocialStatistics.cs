﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using NaughtyAttributes;

public class SocialStatistics : MonoBehaviour
{
    [System.Serializable]
    public class EthnicityFrequency : IComparable<EthnicityFrequency>
    {
        public Descriptors.EthnicGroup ethnicity;
        public int frequency;

        //Default comparer (frequency)
        public int CompareTo(EthnicityFrequency otherObject)
        {
            return this.frequency.CompareTo(otherObject.frequency);
        }
    }

    [System.Serializable]
    public class HairSetting
    {
        public Descriptors.HairColour colour;
        public Color hairColourRange1 = Color.white;
        public Color hairColourRange2 = Color.black;
    }

    [Header("Gender/Sexuality")]
    [Tooltip("The scale in the centre of Female:Male float that applies to citizens that identify as non-binary")]
    public float genderNonBinaryThreshold = 0.01f;
    [Tooltip("How many citizens identify as something other than their birth gender?")]
    public float transThreshold = 0.01f;
    [Tooltip("Sexuality threshold for being attracted to opposite sex (straight)")]
    public float sexualityStraightThreshold = 0.4f;
    [Tooltip("Sexuality threshold for being attracted to same sex (gay)")]
    public float sexualityGayThreshold = 0.9f;
    [Tooltip("Chance of being asexual if attracted to neither sex")]
    public float asexualChance = 0.7f;

    [Space(7)]
    public CharacterTrait maleTrait;
    public CharacterTrait femaleTrait;
    public CharacterTrait nbTrait;
    public CharacterTrait AttractedToMaleTrait;
    public CharacterTrait AttractedToFemaleTrait;
    public CharacterTrait AttractedToNBTrait;
    public CharacterTrait relationshipTrait;

    [Space(7)]
    public List<Color> lipstickColours = new List<Color>();

    [Header("Demographics")]
    [Tooltip("The higher the rank, the more likely it is that the person is older: 19-21, 22-26, 27-31, 32-36, 37-41, 42-46, 47-51, 52-56, 57-61, 62-66, 67+")]
    public int[] ageRanges = { 19, 22, 27, 32, 37, 42, 47, 52, 57, 62 };

    [Header("Ethnicity")]
    public List<EthnicityFrequency> ethnicityFrequencies = new List<EthnicityFrequency>();
    public int chanceOf2ndEthnicity = 10;
    public float districtEthnictiyDominanceMultiplier = 0.82f;

    [System.Serializable]
    public class EthnicityStats
    {
        public Descriptors.EthnicGroup group;

        [Header("Skin")]
        public Color skinColourRange1 = Color.white;
        public Color skinColourRange2 = Color.black;

        [Header("Hair Colour")]
        public int blackHairRatio = 22;
        public int brownHairRatio = 50;
        public int blondeHairRatio = 22;
        public int gingerHairRatio = 1;
        public int RedHairRatio = 0;
        public int blueHairRatio = 0;
        public int greenHairRatio = 0;
        public int purpleHairRatio = 0;
        public int pinkHairRatio = 0;
        public int greyHairRatio = 0;
        public int whiteHairRatio = 0;

        [Header("Hair Type")]
        //Seperated into male/female groups
        public int baldHairRatioMale = 3;
        public int shortHairRatioMale = 60;
        public int longHairRatioMale = 5;

        public int baldHairRatioFemale = 0;
        public int shortHairRatioFemale = 20;
        public int longHairRatioFemale = 35;

        [Header("Hair Type")]
        //straight, curly, balding, messy, styled, mohawk, afro
        public int straightHairRatioMale = 40;
        public int curlyHairRatioMale = 10;
        public int balingHairRatioMale = 10;
        public int messyHairRatioMale = 10;
        public int styledHairRatioMale = 20;
        public int mohawkHairRatioMale = 1;
        public int afroHairRatioMale = 1;

        public int straightHairRatioFemale = 50;
        public int curlyHairRatioFemale = 20;
        public int balingHairRatioFemale = 0;
        public int messyHairRatioFemale = 10;
        public int styledHairRatioFemale = 5;
        public int mohawkHairRatioFemale = 1;
        public int afroHairRatioFemale = 1;

        [Header("Eye Colour")]
        //blue, brown, green, grey
        public int blueEyesRatio = 35;
        public int brownEyesRatio = 30;
        public int greenEyesRatio = 30;
        public int greyEyesRatio = 5;

        [Header("Naming")]
        public bool overrideFirst = false;
        public Descriptors.EthnicGroup overrideNameFirst;
        public bool overrideSur = false;
        public Descriptors.EthnicGroup overrideNameSur;

        [Header("Cultural Similiarities")]
        public List<Descriptors.EthnicGroup> culturalSimilarities = new List<Descriptors.EthnicGroup>();

        [Header("Traits")]
        public List<CharacterTrait> ethTraits = new List<CharacterTrait>();
    }

    [Header("Ethnicity Classes")]
    public List<EthnicityStats> ethnicityStats = new List<EthnicityStats>();

    [Header("Physical Build")]
    [Tooltip("Real-world average height in cm")]
    public float averageHeight = 175.4f;
    [Tooltip("Real-world average weight in kg")]
    public float averageWeight = 70.8f;
    [Tooltip("Height min/max thresholds in cm")]
    public Vector2 heightMinMax = Vector2.one;
    [Space(7)]
    //public int verySkinnyRatio = 5;
    public int skinnyRatio = 15;
    public int averageRatio = 35;
    public int overweightRatio = 20;
    //public int obeseRatio = 5;
    //public int athleticRatio = 10;
    public int muscleyRatio = 10;

    [Header("Blood Group")]
    public float bloodOPosRatio = 38f;
    public float bloodAPosRatio = 34f;
    public float bloodBPosRatio = 9f;
    public float bloodONegRatio = 7f;
    public float bloodANegRatio = 6f;
    public float bloodABPosRatio = 3f;
    public float bloodBNegRatio = 2f;
    public float bloodABNegRatio = 1f;

    [Header("Hair")]
    [ReorderableList]
    public List<HairSetting> hairColourSettings = new List<HairSetting>();

    [Space(7)]
    public int RedHairRatio = 10;
    public int blueHairRatio = 1;
    public int greenHairRatio = 1;
    public int purpleHairRatio = 1;
    public int pinkHairRatio = 2;

    [Header("Facial Features")]
    public int scaringRatio = 25; //Selection is also affected by other factors
    public int menWithBeards = 36;
    public int menWithMoustaches = 11;
    public int piercingRatio = 25; //Selection is also affected by other factors
    public int TattooRatio = 25; //Selection is also affected by other factors
    public int glassesRatio = 50;
    public int moleRatio = 2;
    public int frecklesRatio = 5; //Selection is also affected by other factors

    [Header("Society")]
    public float seriousRelationshipsRatio = 0.5f;

    [Header("Slang Defaults")]
    [ReorderableList]
    [Tooltip("A default slang greeting to be used on anyone in a casual manor")]
    public List<string> slangGreetingDefault;
    [ReorderableList]
    [Tooltip("Similar to above, but male specific (eg. 'bro')")]
    public List<string> slangGreetingMale;
    [ReorderableList]
    [Tooltip("Similar to above, but female specific")]
    public List<string> slangGreetingFemale;
    [ReorderableList]
    [Tooltip("Slang greeting for a lover")]
    public List<string> slangGreetingLover;
    [ReorderableList]
    [Tooltip("Slang curse word")]
    public List<string> slangCurse;
    [ReorderableList]
    [Tooltip("Slang curse noun word")]
    public List<string> slangCurseNoun;
    [ReorderableList]
    [Tooltip("Slang praise noun word")]
    public List<string> slangPraiseNoun;

    [Header("Fav Colours Pool")]
    [ReorderableList]
    public List<Color> favouriteColoursPool = new List<Color>();

    //Singleton pattern
    private static SocialStatistics _instance;
    public static SocialStatistics Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
}
