﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class LightningStrikeController : MonoBehaviour
{
    public Vector2 aliveTime = Vector2.one;
    [ReadOnly]
    public float timer = 0f;
    public Transform startPoint;
    public Transform endPoint;

    // Start is called before the first frame update
    void Start()
    {
        timer = Toolbox.Instance.Rand(aliveTime.x, aliveTime.y);
    }

    // Update is called once per frame
    void Update()
    {
        if(SessionData.Instance.play)
        {
            //Alive time
            timer -= Time.deltaTime;

            if (timer <= 0f)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
