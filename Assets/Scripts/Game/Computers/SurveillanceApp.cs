﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Text;
using System;

public class SurveillanceApp : CruncherAppContent
{
    [Header("Components")]
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI cameraSelectionText;
    public RectTransform timelineRect;
    public RectTransform timelineScrub;
    public ComputerOSUIComponent scrubUI;
    public GameObject timelineEntryPrefab;
    public TextMeshProUGUI timestampText;
    public TextMeshProUGUI timestampTextShadow;
    public TextMeshProUGUI locationstampText;
    public TextMeshProUGUI locationstampTextShadow;
    public RenderTexture renderTexturePrefab;
    public RawImage captureDisplay;
    public RectTransform captureRect;
    public Button yesterdayButton;
    public Button todayButton;
    public Button actorNextButton;
    public Button actorPrevButton;
    public TextMeshProUGUI printButtonText;
    public RectTransform actorPageRect;
    public RectTransform camDisplayPageRect;
    public RawImage actorImage;
    public TextMeshProUGUI actorNameText;
    public Button acquireNameButton;
    public Button actorBackButton;
    public TextMeshProUGUI actorPageText;
    public TextMeshProUGUI yesterdayText;
    public TextMeshProUGUI todayText;
    [Space(5)]
    public RectTransform actorListRect;
    public GameObject actorListPrefab;
    public RectTransform locator;

    [Space(7)]
    public Color timelineColor;
    public Color timelineMOColor;
    public Color timelineFlagColor;

    [Header("State")]
    public List<Interactable> cameras = new List<Interactable>();
    [System.NonSerialized]
    public Interactable selectedCamera;
    public List<Interactable> selectedSentries = new List<Interactable>();
    public List<SceneRecorder.SceneCapture> loadedCaptures = new List<SceneRecorder.SceneCapture>();
    private List<CruncherTimelineEntry> spawnedTimelineEntries = new List<CruncherTimelineEntry>();
    public List<CruncherSurveillanceActorEntry> spawnedActorEntries = new List<CruncherSurveillanceActorEntry>();
    public CruncherSurveillanceActorEntry hoveredActor;
    public CruncherSurveillanceActorEntry selectedActor;
    public Human flaggedActor;

    public bool dispayYesterday = false;
    public Vector2 timelineScale; //X is low, Y is high
    public float timelineCovers; //Time the timeline covers
    public SceneRecorder.SceneCapture currentScene;
    //public bool currentForcedGameTime = false;
    public float currentSceneGametime = 0f;
    public float scrubTime = 0f;
    public int actorPage = 0;
    public float loadInHeadshots = 0f;

    public bool scrubMove = false;
    private Vector3 scrubOffset;

    public bool actorPageActive = false;

    public override void OnSetup()
    {
        base.OnSetup();

        titleText.text = Strings.Get("computer", "Surveillance");

        //Setup default text
        printButtonText.text = Strings.Get("computer", "Print");

        yesterdayText.text = Strings.Get("computer", "Yesterday", Strings.Casing.upperCase);
        todayText.text = Strings.Get("computer", "Today", Strings.Casing.upperCase);

        //Get cameras
        cameras.AddRange(controller.ic.interactable.node.gameLocation.securityCameras);

        //Check for adding building surveillance...
        if(controller.ic.interactable.node.gameLocation.thisAsAddress != null)
        {
            if(controller.ic.interactable.node.gameLocation.thisAsAddress.company != null)
            {
                if(controller.ic.interactable.node.gameLocation.thisAsAddress.company.preset.controlsBuildingSurveillance)
                {
                    foreach(NewAddress lob in controller.ic.interactable.node.building.lobbies)
                    {
                        cameras.AddRange(lob.securityCameras);
                    }
                }
            }
        }

        timestampText.text = string.Empty;
        timestampTextShadow.text = timestampText.text;

        locationstampText.text = string.Empty;
        locationstampTextShadow.text = locationstampText.text;

        SetScene(null);

        if (cameras.Count > 0)
        {
            SetCamera(cameras[0]);
        }
    }

    public void SetCamera(Interactable newSelection)
    {
        if(selectedCamera != null)
        {
            selectedCamera.sceneRecorder.OnNewCapture -= OnSelectedCameraNewCapture;
        }

        selectedCamera = newSelection;

        if(dispayYesterday)
        {
            //yesterdayText.text = ">" + Strings.Get("computer", "Yesterday", Strings.Casing.upperCase) + "<";
            //todayText.text = Strings.Get("computer", "Today", Strings.Casing.upperCase);

            int dayInt = SessionData.Instance.dayInt - 1;
            if (dayInt < 0) dayInt = 6;
            dayInt = Mathf.Clamp(dayInt, 0, 6);

            yesterdayText.text = ">" + Strings.Get("ui.interface", ((SessionData.WeekDay)dayInt).ToString()) + "<";
            todayText.text = Strings.Get("ui.interface", SessionData.Instance.day.ToString());
        }
        else
        {
            //yesterdayText.text = Strings.Get("computer", "Yesterday", Strings.Casing.upperCase);
            //todayText.text = ">" + Strings.Get("computer", "Today", Strings.Casing.upperCase) + "<";

            int dayInt = SessionData.Instance.dayInt - 1;
            if (dayInt < 0) dayInt = 6;
            dayInt = Mathf.Clamp(dayInt, 0, 6);

            yesterdayText.text = Strings.Get("ui.interface", ((SessionData.WeekDay)dayInt).ToString());
            todayText.text = ">" + Strings.Get("ui.interface", SessionData.Instance.day.ToString()) + "<";
        }

        //Remove all existing entries
        while(spawnedTimelineEntries.Count > 0)
        {
            Destroy(spawnedTimelineEntries[0].gameObject);
            spawnedTimelineEntries.RemoveAt(0);
        }

        loadedCaptures.Clear();

        if (selectedCamera != null)
        {
            Game.Log("Gameplay: Surveillance set new camera: " + selectedCamera.node.room.name);

            //Listen for new captures from camera
            selectedCamera.sceneRecorder.OnNewCapture += OnSelectedCameraNewCapture;

            //Find sentry gun in same room
            selectedSentries.Clear();

            if(selectedCamera.node.gameLocation.thisAsAddress != null)
            {
                selectedSentries.AddRange(selectedCamera.node.gameLocation.thisAsAddress.sentryGuns.FindAll(item => item.node.room == selectedCamera.node.room));
            }

            //Load cam
            string fl = string.Empty;
            if (selectedCamera.node.floor != null) fl = Strings.Get("names.rooms", "floor_" + selectedCamera.node.floor.floor);
            cameraSelectionText.text = Strings.Get("names.rooms", selectedCamera.node.room.preset.name) + " " + fl;
            titleText.text = Strings.Get("computer", "Surveillance");

            //What is the last midnight time?
            float lastMidnight = SessionData.Instance.gameTime - SessionData.Instance.decimalClock;

            //Load timeline
            for (int i = 0; i < selectedCamera.sceneRecorder.interactable.cap.Count; i++)
            {
                if(selectedCamera.sceneRecorder.interactable.cap[i].t >= lastMidnight)
                {
                    if(!dispayYesterday)
                    {
                        loadedCaptures.Add(selectedCamera.sceneRecorder.interactable.cap[i]);
                    }
                }
                else
                {
                    if (dispayYesterday)
                    {
                        loadedCaptures.Add(selectedCamera.sceneRecorder.interactable.cap[i]);
                    }
                }
            }

            //Calculate timeline scale
            if(loadedCaptures.Count > 0)
            {
                timelineScale = new Vector2(loadedCaptures[0].t, SessionData.Instance.gameTime);
            }

            timelineCovers = timelineScale.y - timelineScale.x;

            foreach (SceneRecorder.SceneCapture s in loadedCaptures)
            {
                GameObject newT = Instantiate(timelineEntryPrefab, timelineRect);
                RectTransform r = newT.GetComponent<RectTransform>();

                float posX = (s.t - timelineScale.x) / timelineCovers; //Calculate as if 0 x pos was oldest

                //Position on timeline
                r.anchoredPosition = new Vector2((posX * timelineRect.rect.width), 0f);

                CruncherTimelineEntry te = newT.GetComponent<CruncherTimelineEntry>();
                te.Setup(this, s);

                spawnedTimelineEntries.Add(te);
            }

            //Set the scrub position to account for timeline range changes...
            timelineScrub.anchoredPosition = new Vector2((((scrubTime - timelineScale.x) / timelineCovers) * timelineRect.rect.width) - (timelineRect.rect.width * 0.5f), 0f);
            timelineScrub.anchoredPosition = new Vector2(Mathf.Clamp(timelineScrub.anchoredPosition.x, timelineRect.rect.width * -0.5f, timelineRect.rect.width * 0.5f), 0f);
            timelineScrub.SetAsLastSibling(); //Move scrub infront

            yesterdayButton.interactable = true;
            todayButton.interactable = true;

            UpdateCamStatus();
            UpdateTimelineFlagging();
        }
        else
        {
            Game.Log("Gameplay: Surveillance set null camera");

            SetScene(null);
            cameraSelectionText.text = "-";
            selectedSentries.Clear();

            yesterdayButton.interactable = false;
            todayButton.interactable = false;
        }

        UpdateScrub(true, currentSceneGametime);
    }

    //Triggered when a selected camera captures
    private void OnSelectedCameraNewCapture()
    {
        Game.Log("Gameplay: Selected camera has a new capture...");
        controller.SetTimedLoading(Toolbox.Instance.Rand(0.1f, 0.3f));
        SetCamera(selectedCamera);
    }

    private void OnDestroy()
    {
        SceneCapture.Instance.currrentlyViewing = null;

        if (selectedCamera != null)
        {
            selectedCamera.sceneRecorder.OnNewCapture -= OnSelectedCameraNewCapture;
        }
    }

    private void Update()
    {
        if(SessionData.Instance.play)
        {
            if (Player.Instance.computerInteractable == controller.ic.interactable)
            {
                //Detect timeline scrub drag
                if (controller.currentHover == scrubUI)
                {
                    //Check for mouse down
                    if (!scrubMove)
                    {
                        if (Input.GetMouseButtonDown(0))
                        {
                            scrubOffset = controller.cursorRect.position - timelineScrub.position;
                            scrubMove = true;
                        }
                    }
                }

                if (scrubMove)
                {
                    if (!Input.GetMouseButton(0))
                    {
                        scrubMove = false;
                    }
                    else
                    {
                        timelineScrub.position = controller.cursorRect.position - scrubOffset;

                        //Clamp
                        timelineScrub.anchoredPosition = new Vector2(Mathf.Clamp(timelineScrub.anchoredPosition.x, timelineRect.rect.width * -0.5f, timelineRect.rect.width * 0.5f), 0f);

                        UpdateScrub();
                    }
                }
                else
                {
                    if(!InterfaceController.Instance.playerTextInputActive)
                    {
                        //Use arrow keys to alter timeline...
                        if (InputController.Instance.player.GetButtonDown("NavigateLeft"))
                        {
                            NextCaptureButtion(-1);

                            //Play button audio
                            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerKeyboardKey, Player.Instance, Player.Instance.currentNode, this.transform.position);
                        }
                        else if (InputController.Instance.player.GetButtonDown("NavigateRight"))
                        {
                            NextCaptureButtion(1);

                            //Play button audio
                            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerKeyboardKey, Player.Instance, Player.Instance.currentNode, this.transform.position);
                        }
                        else if (InputController.Instance.player.GetButtonDown("NavigateUp"))
                        {
                            CameraSelection(-1);

                            //Play button audio
                            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerKeyboardKey, Player.Instance, Player.Instance.currentNode, this.transform.position);
                        }
                        else if (InputController.Instance.player.GetButtonDown("NavigateDown"))
                        {
                            CameraSelection(1);

                            //Play button audio
                            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerKeyboardKey, Player.Instance, Player.Instance.currentNode, this.transform.position);
                        }
                    }

                }
            }

            //Load in headshots
            //if(loadInHeadshots < spawnedActorEntries.Count)
            //{
            //    loadInHeadshots += Time.deltaTime / 0.125f;

            //    for (int i = 0; i < Mathf.Min(loadInHeadshots, spawnedActorEntries.Count); i++)
            //    {
            //        if(i < spawnedActorEntries.Count)
            //        {
            //            spawnedActorEntries[i].LoadHeadshot();
            //        }
            //    }
            //}

            //Hover
            hoveredActor = null;

            foreach(CruncherSurveillanceActorEntry h in spawnedActorEntries)
            {
                if (controller.currentHover != null && h.component == controller.currentHover)
                {
                    hoveredActor = h;
                    h.SetOnOver(true);
                }
                else h.SetOnOver(false);
            }

            if ((hoveredActor != null && currentScene != null) || actorPageActive)
            {
                //Turn on/off custom switch state to enable inspect command
                if(!controller.ic.interactable.sw3)
                {
                    controller.ic.interactable.SetCustomState3(true, hoveredActor.human, false);
                }
            }
            else
            {
                if (controller.ic.interactable.sw3)
                {
                    controller.ic.interactable.SetCustomState3(false, null, false);
                }

                if (locator.gameObject.activeSelf)
                {
                    locator.gameObject.SetActive(false);
                }
            }
        }
    }

    public void UpdateScrub(bool forceTime = false, float newScrub = 0f)
    {
        //Get normalized
        float norm = Mathf.Clamp01((timelineScrub.anchoredPosition.x + timelineRect.rect.width * 0.5f) / timelineRect.rect.width);
        scrubTime = Mathf.Lerp(timelineScale.x, timelineScale.y, norm);

        if(forceTime)
        {
            scrubTime = Mathf.Clamp(newScrub, timelineScale.x, timelineScale.y);
            Game.Log("Gameplay: Force scrub time of " + newScrub + " (game time: " + SessionData.Instance.gameTime + ")");
            float inverseLerp = Mathf.InverseLerp(timelineScale.x, timelineScale.y, scrubTime);
            Game.Log("Gameplay: Force scrub inverse lerp: " + inverseLerp);
            timelineScrub.anchoredPosition = new Vector2(inverseLerp * timelineRect.rect.width - (0.5f * timelineRect.rect.width), 0f);
        }

        //Find the last capture
        SceneRecorder.SceneCapture lastRealCapture = null;

        foreach(SceneRecorder.SceneCapture c in loadedCaptures)
        {
            if(lastRealCapture == null || (c.t >= lastRealCapture.t && c.t <= scrubTime))
            {
                lastRealCapture = c;
            }
        }

        SetScene(lastRealCapture);
    }

    public void SetScene(SceneRecorder.SceneCapture newScene)
    {
        if(currentScene != newScene && Player.Instance.computerInteractable == controller.ic.interactable)
        {
            currentScene = newScene;
            controller.SetTimedLoading(Toolbox.Instance.Rand(0.2f, 0.5f));

            if (currentScene != null)
            {
                captureDisplay.color = Color.white;
                currentSceneGametime = newScene.t;
                //currentForcedGameTime = forceTime;
                //if (forceTime) currentSceneGametime = forceGameTime;

                timestampText.text = SessionData.Instance.TimeAndDate(currentSceneGametime, true, true, true);
                timestampTextShadow.text = timestampText.text;

                if(selectedCamera.node.room.isNullRoom)
                {
                    locationstampText.text = selectedCamera.node.gameLocation.name;
                }
                else
                {
                    locationstampText.text = selectedCamera.node.room.GetName();
                }

                locationstampTextShadow.text = locationstampText.text;

                Game.Log("Gameplay: Set scene " + selectedCamera.node.room.name + " scene time: " + SessionData.Instance.TimeStringOnDay(currentScene.t, true, true) + " (time: " + timestampText.text + ")");

                captureDisplay.texture = SceneCapture.Instance.GetSurveillanceScene(currentScene);
                captureDisplay.color = Color.white;

                SceneCapture.Instance.currrentlyViewing = currentScene;
            }
            else
            {
                Game.Log("Gameplay: Set scene null");
                captureDisplay.color = Color.black;
                SceneCapture.Instance.currrentlyViewing = null;

                timestampText.text = string.Empty;
                timestampTextShadow.text = timestampText.text;
                //currentForcedGameTime = false;

                locationstampText.text = string.Empty;
                locationstampTextShadow.text = locationstampText.text;

                currentSceneGametime = -9999f;
            }

            actorPage = 0; //Reset actor page with each scroll through
            UpdateActorList();
        }
    }

    public void CameraSelection(int addSelection)
    {
        int currentIndex = cameras.IndexOf(selectedCamera);

        currentIndex += addSelection;

        if(currentIndex < 0)
        {
            currentIndex += cameras.Count;
        }
        else if(currentIndex >= cameras.Count)
        {
            currentIndex -= cameras.Count;
        }

        if(cameras.Count > 0)
        {
            SetCamera(cameras[currentIndex]);
        }
    }

    public void ExitButton()
    {
        controller.OnAppExit();
    }

    public void NextCaptureButtion(int val)
    {
        if(loadedCaptures.Count > 1)
        {
            //Find the last capture
            int currentIndex = loadedCaptures.IndexOf(currentScene);

            currentIndex += val;
            currentIndex = Mathf.Clamp(currentIndex, 0, loadedCaptures.Count - 1);

            //Set the scrub position
            timelineScrub.anchoredPosition = new Vector2((((loadedCaptures[currentIndex].t - timelineScale.x) / timelineCovers) * timelineRect.rect.width) - (timelineRect.rect.width * 0.5f), 0f);
            timelineScrub.anchoredPosition = new Vector2(Mathf.Clamp(timelineScrub.anchoredPosition.x, timelineRect.rect.width * -0.5f, timelineRect.rect.width * 0.5f), 0f);

            SetScene(loadedCaptures[currentIndex]);
        }
    }

    public override void PrintButton()
    {
        controller.SetTimedLoading(Toolbox.Instance.Rand(0.5f, 1f));

        if (controller.printedDocument == null && currentScene != null && controller.printTimer <= 0f)
        {
            controller.printTimer = 1f;
            controller.printerParent.localPosition = new Vector3(controller.printerParent.localPosition.x, controller.printerParent.localPosition.y, -0.05f); //Move printer parent back

            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerPrint, Player.Instance, controller.ic.interactable.node, controller.ic.interactable.wPos);

            //Save the surveillance capture
            currentScene.recorder.interactable.sCap.Add(currentScene);

            //Get passed vars
            Interactable.Passed passedID = new Interactable.Passed(Interactable.PassedVarType.savedSceneCapID, currentScene.id);
            List<Interactable.Passed> newPassed = new List<Interactable.Passed>();
            newPassed.Add(passedID);

            controller.printedDocument = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.surveillancePrintout, Player.Instance, Player.Instance, null, controller.printerParent.position, controller.ic.transform.eulerAngles, newPassed, null);
            if (controller.printedDocument != null) controller.printedDocument.MarkAsTrash(true);

            //Listen for on remove...
            controller.printedDocument.OnRemovedFromWorld += OnPlayerTakePrint;
        }
        else AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerInvalidPasscode, Player.Instance, controller.ic.interactable.node, controller.ic.interactable.wPos);
    }

    public void SetCamActiveButton(bool val)
    {
        if (selectedCamera != null)
        {
            selectedCamera.SetSwitchState(val, Player.Instance);

            if (val)
            {
                selectedCamera.SetValue(selectedCamera.GetSecurityStrength());
            }
            else selectedCamera.SetValue(0f);
        }

        UpdateCamStatus();
    }

    private void UpdateCamStatus()
    {
        if(selectedCamera != null)
        {

        }
    }

    private void UpdateActorList()
    {
        while(spawnedActorEntries.Count > 0)
        {
            Destroy(spawnedActorEntries[0].gameObject);
            spawnedActorEntries.RemoveAt(0);
        }

        loadInHeadshots = 0f;
        controller.SetTimedLoading(1f);

        List<Human> required = new List<Human>();

        if(currentScene != null)
        {
            foreach (SceneRecorder.ActorCapture a in currentScene.aCap)
            {
                Human found = a.GetHuman();

                if (found.isPlayer) continue;

                if (!required.Contains(found))
                {
                    required.Add(found);
                }
            }
        }

        float xPos = 0.002f;

        //Limit actor page
        int limitPage = Mathf.Max(Mathf.FloorToInt((required.Count - 1) / 5f), 0);
        actorPage = Mathf.Clamp(actorPage, 0, limitPage);

        int displayFrom = actorPage * 5;
        int actorLimit = actorPage * 5 + 5;

        actorPageText.text = (actorPage + 1) + "/" + (limitPage + 1);

        //Game.Log("Display from " + Mathf.Min(displayFrom, required.Count) + " to " + Mathf.Min(required.Count, actorLimit) + " required: " + required.Count);

        for (int i = Mathf.Min(displayFrom, required.Count); i < Mathf.Min(required.Count, actorLimit); i++)
        {
            Human req = required[i];
            GameObject newAct = Instantiate(actorListPrefab, actorListRect);
            CruncherSurveillanceActorEntry ae = newAct.GetComponent<CruncherSurveillanceActorEntry>();
            ae.Setup(this, req);
            spawnedActorEntries.Add(ae);
            ae.rect.anchoredPosition = new Vector2(xPos, 0);
            xPos += ae.rect.sizeDelta.x + 0.002f;
        }

        if (actorPage > 0)
        {
            actorPrevButton.interactable = true;
        }
        else actorPrevButton.interactable = false;

        if (actorPage < limitPage)
        {
            actorNextButton.interactable = true;
        }
        else actorNextButton.interactable = false;
    }

    public void SetActorPage(int val)
    {
        actorPage += val;
        UpdateActorList();
    }

    public void SelectActor(CruncherSurveillanceActorEntry actorButton)
    {
        selectedActor = actorButton;
        SetActorPage(true, true);
    }

    public void SetActorPage(bool val, bool forceUpdate = false)
    {
        if(val != actorPageActive || forceUpdate)
        {
            actorPageActive = val;

            selectedActor.UpdateText();

            actorImage.texture = selectedActor.headshotImg.texture;
            actorNameText.text = selectedActor.popupText.text;

            actorPageRect.gameObject.SetActive(actorPageActive);
            camDisplayPageRect.gameObject.SetActive(!actorPageActive);

            //Enable/disable acquire name
            List<Evidence.DataKey> tied = selectedActor.human.evidenceEntry.GetTiedKeys(Evidence.DataKey.photo);

            if (tied.Contains(Evidence.DataKey.name) || GameplayController.Instance.money < 50)
            {
                acquireNameButton.interactable = false;
            }
            else if(GameplayController.Instance.money >= 50)
            {
                acquireNameButton.interactable = true;
            }
        }
    }

    public void ActorBackButton()
    {
        SetActorPage(false);
    }

    public void AcquireNameButton()
    {
        if (GameplayController.Instance.money >= 50)
        {
            selectedActor.human.evidenceEntry.MergeDataKeys(Evidence.DataKey.photo, Evidence.DataKey.name);
            GameplayController.Instance.AddMoney(-50, true, "Acquire name");
            SetActorPage(true, true);
        }
    }

    public void ToggleFlagOnFootage()
    {
        if (selectedActor != null && selectedActor != flaggedActor)
        {
            SetFlaggedActor(selectedActor.human);
        }
        else SetFlaggedActor(null);
    }

    public void SetFlaggedActor(Human h)
    {
        if(flaggedActor != h)
        {
            flaggedActor = h;

            UpdateTimelineFlagging();
        }
    }

    public void UpdateTimelineFlagging()
    {
        foreach(CruncherTimelineEntry te in spawnedTimelineEntries)
        {
            if (hoveredActor != null)
            {
                if (te.sceneReference.aCap.Exists(item => item.id == hoveredActor.human.humanID))
                {
                    te.SetMouseOver(true);
                }
                else te.SetMouseOver(false);
            }
            else te.SetMouseOver(false);

            if (flaggedActor != null)
            {
                if (te.sceneReference.aCap.Exists(item => item.id == flaggedActor.humanID))
                {
                    te.SetFlagged(true);
                }
                else te.SetFlagged(false);
            }
            else te.SetFlagged(false);
        }
    }

    public void SaveToTapeButton()
    {

    }

    public void YesterdayButton()
    {
        if(!dispayYesterday)
        {
            dispayYesterday = true;
            SetCamera(selectedCamera);
        }
    }

    public void TodayButton()
    {
        if(dispayYesterday)
        {
            dispayYesterday = false;
            SetCamera(selectedCamera);
        }
    }
}
