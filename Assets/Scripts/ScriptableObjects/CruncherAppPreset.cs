﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "cruncherapp_data", menuName = "Database/Cruncher App")]

public class CruncherAppPreset : SoCustomComparison
{
	[Header("App Settings")]
	[Tooltip("The background image to display on load")]
	public Material loadBackground;
	[Tooltip("The background image to display once app has loaded")]
	public Material loadedBackground;
	[Tooltip("Use the cursor if the player is controlling the computer")]
	public bool useCursor = true;
	[Tooltip("The cursor to use")]
	public Sprite cursorSprite;
	[Tooltip("Use timer to exit: The app will exit on this timer")]
	public bool useTimer = false;
	[EnableIf("useTimer")]
	[Tooltip("Timer length in seconds")]
	public float timerLength = 3f;
	[Tooltip("Take this time to load in")]
	public float loadTime = 1f;
	[Tooltip("How heavy this is on loading the machine (1 = constant)")]
	[Range(0f, 1f)]
	public float loadDemand = 0.33f;
	[Tooltip("Always load during the duration of this app")]
	public bool alwaysLoad = false;
	[Tooltip("How heavy this is on loading the machine (1 = constant)")]
	[Range(0f, 1f)]
	public float alwaysLoadDemand = 0.33f;
	[Tooltip("App Icon displayed on desktop")]
	public Sprite desktopIcon;
	[Tooltip("Computer light emmits this colour")]
	public Color screenLightColourOnLoad = Color.white;
	[Tooltip("Computer light emmits this colour")]
	public Color screenLightColourOnFinishLoad = Color.white;

	[Header("Access")]
	public bool alwaysInstalled = false;
	[DisableIf("alwaysInstalled")]
	public bool onlyIfCorporateSabotageSkill = false;
	[DisableIf("alwaysInstalled")]
	public bool companyOnly = false;
	[DisableIf("alwaysInstalled")]
	public bool salesRecordsOnly = false;
	[DisableIf("alwaysInstalled")]
	[Tooltip("Only installed if the login is an owner of the address")]
	public bool onlyIfOwner = false;
	[DisableIf("alwaysInstalled")]
	[ReorderableList]
	public List<AppAccess> installationConditions = new List<AppAccess>();
	[DisableIf("alwaysInstalled")]
	public List<AddressPreset> onlyInAddresses = new List<AddressPreset>();
	[DisableIf("alwaysInstalled")]
	public bool onlyIfResidential = false;

	[Header("Content")]
	[ReorderableList]
	public List<GameObject> appContent = new List<GameObject>();

	[Header("Audio")]
	[Tooltip("Played when the app is started")]
	public AudioEvent onStartSound;
	[Tooltip("Played when the app is ended")]
	public AudioEvent onExitSound;
	[Tooltip("Played when the app has finished loading")]
	public AudioEvent onFinishedLoadingSound;

	[Header("On Exit")]
	[Tooltip("Open this app on end")]
	public CruncherAppPreset openOnEnd;

	[System.Serializable]
	public class AppAccess
	{
		public CharacterTrait.RuleType rule = CharacterTrait.RuleType.ifAnyOfThese;
		public List<CharacterTrait> traitList = new List<CharacterTrait>();
	}
}
