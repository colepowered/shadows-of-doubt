﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelationshipCreator : Creator
{
    //Chunks to load per frame
    public int loadChunk = 10;

    //Singleton pattern
    private static RelationshipCreator _instance;
    public static RelationshipCreator Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public override void StartLoading()
    {
        Game.Log("CityGen: Generating relationships...");
        StartCoroutine("Relationships");
    }

    IEnumerator Relationships()
    {
        int citizenCursor = 0;

        while (citizenCursor < CityData.Instance.citizenDirectory.Count)
        {
            for (int u = 0; u < loadChunk; u++)
            {
                //Break loop if complete
                if (citizenCursor >= CityData.Instance.citizenDirectory.Count) break;

                Citizen cc = CityData.Instance.citizenDirectory[citizenCursor];

                cc.GenerateItemFavs(); //Generate favourite items
                cc.CreateAcquaintances(); //Create acquaintances (all citizens need to be already created for this to be accurate)

                citizenCursor++;
            }

            //Calculate ~progress
            CityConstructor.Instance.loadingProgress = (float)citizenCursor / (float)CityData.Instance.citizenDirectory.Count;

            yield return null;
        }

        //Complete!
        SetComplete();
    }
}
