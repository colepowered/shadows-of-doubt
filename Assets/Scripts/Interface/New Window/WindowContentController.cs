﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using NaughtyAttributes;
using System;
using System.Text;

public class WindowContentController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public Button tabButton;
    public WindowTabController tabController;
    public InfoWindow window;
    public ZoomContent zoomController;

    public RectTransform pageRect;
    public Image pageImg;

    public Vector2 normalSize;
    public bool centred = false;
    public bool alwaysCentred = false;
    public float fitScale = 1f;
    public bool clearTextMode = false;

    StateSaveData.MessageThreadSave thread = null;
    int msgIndex = 0;

    [Header("Content")]
    [NonSerialized]
    public DDSSaveClasses.DDSTreeSave content;
    public Dictionary<DDSSaveClasses.DDSMessageSettings, TextMeshProUGUI> spawnedText = new Dictionary<DDSSaveClasses.DDSMessageSettings, TextMeshProUGUI>();
    private List<GameObject> spawnedContent = new List<GameObject>();

    //Text content (used for overflow checking)
    public TextMeshProUGUI elementText;
    private GameObject pageControls;
    public List<Image> pagePips = new List<Image>();
    public int page = -1;
    public ButtonController nextPage;
    public ButtonController prevPage;

    // Use this for initialization
    void Awake()
    {
        GetReferences();
    }

    private void GetReferences()
    {
        if(rect == null) rect = this.gameObject.GetComponent<RectTransform>();
        if(pageImg == null) pageImg = this.gameObject.GetComponentInChildren<Image>();
        normalSize = rect.sizeDelta;
    }

    private void OnEnable()
    {
        //Update icon positions on enable
        if (tabController != null)
        {
            if (tabController.preset.contentType == WindowTabPreset.TabContentType.facts)
            {
                window.item.PositionSpawnedFacts();
            }
        }
    }

    public void SetAlwaysCentred(bool newVal)
    {
        alwaysCentred = newVal;

        if(alwaysCentred)
        {
            centred = true;
            CentrePage();
        }
    }

    public void CentrePage()
    {
        if(rect == null) GetReferences();

        rect.anchoredPosition = Vector2.zero;

        ZoomContent zoomController = this.gameObject.GetComponent<ZoomContent>();

        if (zoomController != null)
        {
            zoomController.ResetPivot();
            UpdateFitScale();
            zoomController.SetZoom(fitScale);
        }

        centred = true;
    }

    //Update the ideal size of the content to fit, centred inside window
    public void UpdateFitScale()
    {
        //Get the viewport size
        Vector2 viewportSize = window.scrollRect.GetComponent<RectTransform>().rect.size;

        //Get the content size
        float idealX = viewportSize.x / (normalSize.x + (window.centringTollerance * 0.5f));
        float idealY = viewportSize.y / (normalSize.y + (window.centringTollerance * 0.5f));

        //Choose the lowest so it fits onscreen
        if(tabController.preset.fitToScaleX && tabController.preset.fitToScaleY)
        {
            fitScale = Mathf.RoundToInt(Mathf.Min(idealX, idealY) * 100f) / 100f; //Fit scale to 2dp
        }
        else if(tabController.preset.fitToScaleX && !tabController.preset.fitToScaleY)
        {
            fitScale = Mathf.RoundToInt(idealX * 100f) / 100f; //Fit scale to 2dp
        }
        else if(!tabController.preset.fitToScaleX && tabController.preset.fitToScaleY)
        {
            fitScale = Mathf.RoundToInt(idealY * 100f) / 100f; //Fit scale to 2dp
        }
    }

    //Load preset content
    public void LoadContent()
    {
        while(spawnedContent.Count > 0)
        {
            Destroy(spawnedContent[0]);
            spawnedContent.RemoveAt(0);
        }

        spawnedText.Clear();

        string ddsTree = window.passedEvidence.preset.ddsDocumentID;
        if (window.passedEvidence.overrideDDS != null && window.passedEvidence.overrideDDS.Length > 0) ddsTree = window.passedEvidence.overrideDDS;

        //Get DDS content
        if (!Toolbox.Instance.allDDSTrees.TryGetValue(ddsTree, out content))
        {
            Game.LogError("Evidence: Cannot find content for " + window.name + " tree " + ddsTree + " (" + window.passedEvidence.preset.name +"). Original doc ID: " + window.passedEvidence.preset.ddsDocumentID);
            return;
        }

        //If this is a printed vmail, get document data from separate...
        if(content.treeType == DDSSaveClasses.TreeType.vmail)
        {
            DDSSaveClasses.DDSTreeSave printedVmailDoc = Toolbox.Instance.allDDSTrees["04f9b47f-e525-4aef-82b2-3cffe52cc061"]; //Default printed vmail document

            //Set size
            rect.sizeDelta = printedVmailDoc.document.size;
            pageRect.sizeDelta = printedVmailDoc.document.size;
            normalSize = printedVmailDoc.document.size;
            rect.localPosition = Vector3.zero;
            rect.localScale = Vector3.one;
            pageRect.localPosition = Vector3.zero;
            pageRect.localScale = Vector3.one;

            pageImg.sprite = DDSControls.Instance.backgroundSprites.Find(item => item.name == printedVmailDoc.document.background);
            pageImg.type = printedVmailDoc.document.fill;
            pageImg.color = printedVmailDoc.document.colour;

            Game.Log("Evidence: Loaded document from printed vmail doc...");

            //Sort content by order
            printedVmailDoc.messages.Sort((p1, p2) => p1.order.CompareTo(p2.order));

            //Spawn objects
            foreach (DDSSaveClasses.DDSMessageSettings msg in printedVmailDoc.messages)
            {
                ConstructContent(msg);
            }

            //Get thread...
            EvidencePrintedVmail vmailEv = window.passedEvidence as EvidencePrintedVmail;

            if(vmailEv != null)
            {
                thread = vmailEv.thread;
                msgIndex = vmailEv.msgIndexID;
            }

            UpdateNoteText();
        }
        else
        {
            //Set size
            rect.sizeDelta = content.document.size;
            pageRect.sizeDelta = content.document.size;
            normalSize = content.document.size;
            rect.localPosition = Vector3.zero;
            rect.localScale = Vector3.one;
            pageRect.localPosition = Vector3.zero;
            pageRect.localScale = Vector3.one;

            pageImg.sprite = DDSControls.Instance.backgroundSprites.Find(item => item.name == content.document.background);
            pageImg.type = content.document.fill;
            pageImg.color = content.document.colour;

            Game.Log("Evidence: Set document fill type to " + content.document.fill);

            //Sort content by order
            content.messages.Sort((p1, p2) => p1.order.CompareTo(p2.order));

            //Spawn objects
            foreach (DDSSaveClasses.DDSMessageSettings msg in content.messages)
            {
                ConstructContent(msg);
            }
        }

        //Centres and updates normal size
        CentrePage();

        //Check for text overflow
        TextOverflowCheck();
    }

    //Construct Content
    public void ConstructContent(DDSSaveClasses.DDSMessageSettings msg)
    {
        window.clearTextButton.gameObject.SetActive(false);

        //Create a null object for this
        GameObject elementParent = Instantiate(DDSControls.Instance.elementPrefab, pageRect);
        spawnedContent.Add(elementParent);
        RectTransform elementRect = elementParent.GetComponent<RectTransform>();

        elementRect.sizeDelta = msg.size;
        elementRect.localPosition = msg.pos;
        elementRect.localEulerAngles = new Vector3(0, 0, msg.rot);

        //Toolbox.Instance.SetPivot(elementRect, new Vector2(0.5f, 1f)); //Set pivot to top
        Toolbox.Instance.SetAnchor(elementRect, new Vector2(0.5f, 1f), new Vector2(0.5f, 1f)); //Set anchor to top

        GameObject newElementComponent = null;

        //Find element: Is this text or image?
        if (msg.msgID != null && msg.msgID.Length > 0)
        {
            newElementComponent = Instantiate(DDSControls.Instance.textComponent, elementRect);

            TextMeshProUGUI compText = newElementComponent.GetComponent<TextMeshProUGUI>();

            //Override message text
            object passed = window.passedEvidence;
            if (window.passedInteractable != null) passed = window.passedInteractable;
            if (window.passedEvidence.interactable != null) passed = window.passedEvidence.interactable;
            compText.text = Strings.GetTextForComponent(msg.msgID, passed, window.passedEvidence.writer, window.passedEvidence.reciever);

            if(msg.isHandwriting)
            {
                if (window.passedEvidence.writer != null && window.passedEvidence.writer.handwriting != null)
                {
                    compText.font = window.passedEvidence.writer.handwriting.fontAsset;
                }
                else compText.font = DDSControls.Instance.defaultHandwritingFont;

                window.clearTextButton.gameObject.SetActive(true);
            }
            else
            {
                compText.font = DDSControls.Instance.fonts.Find(item => item.name == msg.font);
            }

            compText.color = msg.col;
            compText.fontSize = msg.fontSize;
            compText.characterSpacing = msg.charSpace;
            compText.wordSpacing = msg.wordSpace;
            compText.lineSpacing = msg.lineSpace;
            compText.paragraphSpacing = msg.paraSpace;

            //Alignment bitmask
            TMPro.TextAlignmentOptions newAlign = TextAlignmentOptions.Left;

            if (msg.alignH == 0)
            {
                if (msg.alignV == 0)
                {
                    newAlign = TextAlignmentOptions.TopLeft;
                }
                else if (msg.alignV == 1)
                {
                    newAlign = TextAlignmentOptions.MidlineLeft;
                }
                else if (msg.alignV == 2)
                {
                    newAlign = TextAlignmentOptions.BottomLeft;
                }
            }
            else if (msg.alignH == 1)
            {
                if (msg.alignV == 0)
                {
                    newAlign = TextAlignmentOptions.Top;
                }
                else if (msg.alignV == 1)
                {
                    newAlign = TextAlignmentOptions.Center;
                }
                else if (msg.alignV == 2)
                {
                    newAlign = TextAlignmentOptions.Bottom;
                }
            }
            else if (msg.alignH == 2)
            {
                if (msg.alignV == 0)
                {
                    newAlign = TextAlignmentOptions.TopRight;
                }
                else if (msg.alignV == 1)
                {
                    newAlign = TextAlignmentOptions.MidlineRight;
                }
                else if (msg.alignV == 2)
                {
                    newAlign = TextAlignmentOptions.BottomRight;
                }
            }

            compText.alignment = newAlign;

            spawnedText.Add(msg, compText);
        }
        //This is an image
        else
        {
            GameObject getPrefab = DDSControls.Instance.elementPrefabs.Find(item => item.name == msg.elementName);

            if(getPrefab != null)
            {
                newElementComponent = Instantiate(getPrefab, elementRect);

                Image elementImg = newElementComponent.GetComponentInChildren<Image>();

                if(elementImg != null)
                {
                    elementImg.color = msg.col;
                }
                else
                {
                    TextMeshProUGUI compText = newElementComponent.GetComponentInChildren<TextMeshProUGUI>();

                    if(compText != null)
                    {
                        compText.color = msg.col;
                    }
                }
            }
        }

        //Is this used as overflow text?
        if(msg.usePages)
        {
            elementText = elementParent.GetComponentInChildren<TextMeshProUGUI>();
        }
    }

    public void UpdateNoteText()
    {
        bool firstMessage = true;

        foreach (KeyValuePair<DDSSaveClasses.DDSMessageSettings, TextMeshProUGUI> pair in spawnedText)
        {
            if (content.treeType == DDSSaveClasses.TreeType.vmail)
            {
                string getVmailText = string.Empty;

                //Loop through messages in vmail chain...
                if(thread != null)
                {
                    if(msgIndex < thread.messages.Count)
                    {
                        string instanceID = thread.messages[msgIndex];
                        DDSSaveClasses.DDSMessageSettings msg = null;

                        if (content.messageRef.TryGetValue(instanceID, out msg))
                        {
                            //If previous emails exists, add line breaks...
                            if (!firstMessage)
                            {
                                getVmailText += "\n\n";
                            }

                            Human emailSender = null;
                            Human emailReciever = null;

                            CityData.Instance.GetHuman(thread.senders[msgIndex], out emailSender);
                            CityData.Instance.GetHuman(thread.recievers[msgIndex], out emailReciever);

                            string reciverString = string.Empty;
                            string senderString = string.Empty;

                            emailSender = Strings.GetVmailSender(thread, msgIndex, out senderString);
                            emailReciever = Strings.GetVmailReciever(thread, msgIndex, out reciverString);

                            if (emailSender != null)
                            {
                                //Manually insert link
                                Strings.LinkData link = Strings.AddOrGetLink(emailSender.evidenceEntry);
                                if (link != null) senderString = "<link=" + link.id.ToString() + ">" + senderString + "</link>";
                            }

                            if(emailReciever != null)
                            {
                                //Manually insert link
                                Strings.LinkData link = Strings.AddOrGetLink(emailReciever.evidenceEntry);
                                if (link != null) reciverString = "<link=" + link.id.ToString() + ">" + reciverString + "</link>";
                            }

                            getVmailText = Strings.Get("computer", "To") + ": " + reciverString
                                + "\n" + Strings.Get("computer", "From") + ": " + senderString
                                + "\n" + SessionData.Instance.GameTimeToClock12String(thread.timestamps[msgIndex], false) + "\n" + SessionData.Instance.LongDateString(thread.timestamps[msgIndex], true, true, true, true, true, true, false, false);

                            Human a = null;
                            Human b = null;
                            Human c = null;
                            Human d = null;

                            CityData.Instance.GetHuman(thread.participantA, out a);
                            CityData.Instance.GetHuman(thread.participantB, out b);
                            CityData.Instance.GetHuman(thread.participantC, out c);
                            CityData.Instance.GetHuman(thread.participantD, out d);

                            getVmailText += "\n\n" + Strings.GetTextForComponent(msg.msgID, new VMailApp.VmailParsingData { thread = thread, messageIndex = msgIndex }, emailSender, emailReciever, lineBreaks: "\n\n");
                            //getVmailText += "\n\n" + Strings.GetTextForComponent(msg.msgID, window.passedInteractable, emailSender, emailReciever, lineBreaks: "\n\n");

                            firstMessage = false;
                        }
                    }
                    else
                    {
                        Game.LogError("Message index " + msgIndex + " is out of scope for " + thread.messages.Count + " messages");
                    }
                }
                else
                {
                    Game.LogError("Thread is null for vmail!");
                }

                pair.Value.text = getVmailText;
            }
            else
            {
                //Override message text
                string msgID = pair.Key.msgID;

                if (window.passedEvidence != null && window.passedEvidence.interactable != null && window.passedEvidence.interactable.preset.summaryMessageSource != null && window.passedEvidence.interactable.preset.summaryMessageSource.Length > 0)
                {
                    DDSSaveClasses.DDSMessageSave ovrMsg = null;

                    if (Toolbox.Instance.allDDSMessages.TryGetValue(window.passedEvidence.interactable.preset.summaryMessageSource, out ovrMsg))
                    {
                        Game.Log("Override with message id " + window.passedEvidence.interactable.preset.summaryMessageSource);
                        msgID = ovrMsg.id;
                    }
                }

                pair.Value.text = Strings.GetTextForComponent(msgID, window.passedInteractable, window.passedEvidence.writer, window.passedEvidence.reciever);
            }
        }
    }

    //Check to see if pages need to be set up
    public void TextOverflowCheck()
    {
        if (elementText != null)
        {
            RectTransform textRect = elementText.gameObject.GetComponent<RectTransform>();
            Vector2 preferred = elementText.GetPreferredValues();

            Game.Log("Interface: Overflow check for page " + tabController.preset.tabName + ": " + elementText.isTextOverflowing);
            Game.Log("Interface: Overflowcheck preffered height = " + preferred.y);
            Game.Log("Interface: Overflowcheck actual height = " + textRect.rect.height);

            if (preferred.y > textRect.rect.height && elementText.overflowMode != TextOverflowModes.Page)
            {
                Game.Log("Interface: Setting overflow mode to page " + tabController.preset.tabName);

                //Set overflow mode to page
                elementText.overflowMode = TextOverflowModes.Page;
                elementText.ForceMeshUpdate();
                elementText.pageToDisplay = page;

                //Spawn page controls
                pageControls = Instantiate(PrefabControls.Instance.evidenceContentPageControls, this.transform);
                pageControls.transform.SetAsLastSibling();

                //Get buttons
                foreach (Transform child in pageControls.transform)
                {
                    if (child.CompareTag("ContentButton1") && nextPage == null)
                    {
                        nextPage = child.gameObject.GetComponent<ButtonController>();
                    }
                    else if (child.CompareTag("ContentButton2") && prevPage == null)
                    {
                        prevPage = child.gameObject.GetComponent<ButtonController>();
                    }
                    else if(child.CompareTag("PagePipDisplay"))
                    {
                        for (int i = 0; i < pagePips.Count; i++)
                        {
                            Destroy(pagePips[i].gameObject);
                        }

                        //Game.Log("Spawning " + elementText.textInfo.pageCount + " pips...");

                        for (int i = 0; i < elementText.textInfo.pageCount; i++)
                        {
                            GameObject newPip = Instantiate(PrefabControls.Instance.pagePip, child);
                            pagePips.Add(newPip.GetComponent<Image>());
                        }
                    }
                }

                SetPage(1, true);
            }
            else
            {
                if (pageControls != null) Destroy(pageControls);
            }
        }
        else
        {
            if (pageControls != null) Destroy(pageControls);
        }
    }

    public void SetPage(int newPage, bool forceUpdate = false)
    {
        newPage = Mathf.Clamp(newPage, 1, elementText.textInfo.pageCount);

        if(newPage != page || forceUpdate)
        {
            page = newPage;
            elementText.pageToDisplay = page;

            if (page > 1)
            {
                prevPage.SetInteractable(true);
            }
            else
            {
                prevPage.SetInteractable(false);
            }

            if (page < elementText.textInfo.pageCount)
            {
                nextPage.SetInteractable(true);
            }
            else
            {
                nextPage.SetInteractable(false);
            }

            //Game.Log("Element page to display: " + elementText.pageToDisplay + "/" + elementText.textInfo.pageCount);

            window.UpdateControllerNavigationEndOfFrame(); //Update controller navigation; accounts for inactive objects...
            UpdatePips();
        }
    }

    public void NextPage()
    {
        SetPage(page + 1);

        //Play sound
        AudioController.Instance.Play2DSound(AudioControls.Instance.pageForward);
    }

    public void PrevPage()
    {
        SetPage(page - 1);

        //Play sound
        AudioController.Instance.Play2DSound(AudioControls.Instance.pageBack);
    }

    private void UpdatePips()
    {
        //Colour the page pips
        for (int i = 0; i < pagePips.Count; i++)
        {
            if (i == page - 1)
            {
                pagePips[i].color = Color.white;
            }
            else pagePips[i].color = Color.grey;
        }
    }
}
