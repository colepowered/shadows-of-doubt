﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "group_data", menuName = "Database/Group Preset")]

public class GroupPreset : SoCustomComparison
{
    public enum GroupType { interestGroup, couples, cheaters, work};

    [Header("Setup")]
    public GroupType groupType = GroupType.interestGroup;
    [Tooltip("Chance of existance on a per instance basis.")]
    [Range(0f, 1f)]
    public float chance = 1f;
    [Tooltip("Minimum members")]
    public int minMembers = 2;
    [Tooltip("Maximum members")]
    public int maxMembers = 2;

    [Header("Requirements")]
    [Tooltip("Members must have these traits")]
    public List<CharacterTrait> requiredTraits = new List<CharacterTrait>();
    [Tooltip("Members must have this extraversion value")]
    [Range(0f, 1f)]
    public float minimumExtraversion = 0f;

    [Header("Meet Ups")]
    public bool enableMeetUps = true;
    [Tooltip("How many times a week this group meets")]
    [EnableIf("enableMeetUps")]
    public int daysPerWeek = 2;
    [Tooltip("The time range for the meet up time. If set to something other that special interest, this is driven by when both are free (after work etc).")]
    [EnableIf("enableMeetUps")]
    public Vector2 timeRange = new Vector2(18f, 20.5f);
    [Tooltip("Meet up length")]
    public float meetUpLength = 1.5f;
    [Tooltip("Possible meeting place address types")]
    [EnableIf("enableMeetUps")]
    [ReorderableList]
    public List<CompanyPreset> meetUpLocations = new List<CompanyPreset>();
    [Tooltip("Meet up goal")]
    [EnableIf("enableMeetUps")]
    public AIGoalPreset meetUpGoal;
    [Tooltip("The first person will reserve up to 4 seats on arrival...")]
    [EnableIf("enableMeetUps")]
    public bool reserveSeats = true;
    [Tooltip("Add this distance multiplier when choosing a seat")]
    public float useDistanceMultiplierModifier = 0f;

    [Header("Evidence")]
    [ReorderableList]
    public List<ClubClue> clues = new List<ClubClue>();

    [Header("Vmails")]
    [ReorderableList]
    public List<MeetUpVmailThread> vmails = new List<MeetUpVmailThread>();

    [System.Serializable]
    public class MeetUpVmailThread
    {
        public string name;
        public string treeID;
        public MeetUpVmailSender sender;
        public MeetUpVmailSender recevier;
    }

    [System.Serializable]
    public class ClubClue
    {
        public string name;
        public InteractablePreset preset;
        public SpawnAt spawnAt;
    }

    public enum SpawnAt { meetingPlace, leadersApartment, entireGroupsApartments};

    public enum MeetUpVmailSender { groupLeader, groupRandom, meetupPlace, entireGroup, prioritiseFaithful };
}
