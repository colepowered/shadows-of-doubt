﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using TMPro;
using UnityEngine.UI;

public class ApartmentItemsController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public WindowContentController wcc;
    public RectTransform entryParent;
    public ButtonController inRoomButton;
    public ButtonController inStorageButton;
    public ButtonController inShopButton;
    public GameObject itemElementPrefab;

    public ButtonController consumableButton;
    public ButtonController medicalButton;
    public ButtonController equipmentButton;
    public ButtonController electronicsButton;
    public ButtonController documentsButton;
    public ButtonController miscButton;

    [Header("Settings")]
    public Sprite uncheckedSprite;
    public Sprite checkedSprite;

    [Header("State")]
    public bool isSetup = false;
    public FurnishingsController.TabState tabState = FurnishingsController.TabState.inRoom;
    public List<InteractablePreset.ItemClass> displayClasses = new List<InteractablePreset.ItemClass>();
    public NewRoom room;
    //public MaterialKeyController keyController;

    public TMP_InputField searchInputField;
    public List<ApartmentItemElementController> spawnedEntries = new List<ApartmentItemElementController>();

    public void Setup(WindowContentController newContentController)
    {
        rect = this.gameObject.GetComponent<RectTransform>();

        wcc = newContentController;
        room = Player.Instance.currentRoom;

        SetPageSize(new Vector2(640, 748));

        displayClasses = new List<InteractablePreset.ItemClass>(PlayerApartmentController.Instance.rememberItemDisplayClasses);

        //Update list
        SetTabState(PlayerApartmentController.Instance.rememberRoomStorageShop, true);

        isSetup = true;

        PlayerApartmentController.Instance.OnFurnitureChange += OnFurnitureChange;
    }

    public void SetPageSize(Vector2 newSize)
    {
        rect.sizeDelta = newSize;
        wcc.normalSize = rect.sizeDelta;
    }

    public void SetTabState(int newState)
    {
        SetTabState((FurnishingsController.TabState)newState);
    }

    public void SetTabState(FurnishingsController.TabState newState, bool forceUpdate = false)
    {
        if(newState != tabState || forceUpdate)
        {
            tabState = newState;

            //Remove all if switched tabs
            while(spawnedEntries.Count > 0)
            {
                Destroy(spawnedEntries[0].gameObject);
                spawnedEntries.RemoveAt(0);
            }

            UpdateListDisplay();
        }
    }

    //Update facts list
    public void UpdateListDisplay()
    {
        Game.Log("Decor: Update item list display...");

        //Force deselect
        if (InterfaceController.Instance.selectedElement != null) InterfaceController.Instance.selectedElement.OnDeselect();

        if(tabState == FurnishingsController.TabState.inRoom)
        {
            inRoomButton.SetInteractable(false);
            inShopButton.SetInteractable(true);
            inStorageButton.SetInteractable(true);
        }
        else if(tabState == FurnishingsController.TabState.inShop)
        {
            inRoomButton.SetInteractable(true);
            inShopButton.SetInteractable(false);
            inStorageButton.SetInteractable(true);
        }
        else if(tabState == FurnishingsController.TabState.inStorage)
        {
            inRoomButton.SetInteractable(true);
            inShopButton.SetInteractable(true);
            inStorageButton.SetInteractable(false);
        }

        //Remove all if switched tabs
        while (spawnedEntries.Count > 0)
        {
            Destroy(spawnedEntries[0].gameObject);
            spawnedEntries.RemoveAt(0);
        }

        //Set checked/unchecked
        if (displayClasses.Contains(InteractablePreset.ItemClass.consumable)) consumableButton.icon.sprite = checkedSprite;
        else consumableButton.icon.sprite = uncheckedSprite;

        if (displayClasses.Contains(InteractablePreset.ItemClass.medical)) medicalButton.icon.sprite = checkedSprite;
        else medicalButton.icon.sprite = uncheckedSprite;

        if (displayClasses.Contains(InteractablePreset.ItemClass.equipment)) equipmentButton.icon.sprite = checkedSprite;
        else equipmentButton.icon.sprite = uncheckedSprite;

        if (displayClasses.Contains(InteractablePreset.ItemClass.document)) documentsButton.icon.sprite = checkedSprite;
        else documentsButton.icon.sprite = uncheckedSprite;

        if (displayClasses.Contains(InteractablePreset.ItemClass.electronics)) electronicsButton.icon.sprite = checkedSprite;
        else electronicsButton.icon.sprite = uncheckedSprite;

        if (displayClasses.Contains(InteractablePreset.ItemClass.misc)) miscButton.icon.sprite = checkedSprite;
        else miscButton.icon.sprite = uncheckedSprite;

        rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 0);

        //Compile a list of file icons that should exist
        List<InteractablePreset> allRelevant = new List<InteractablePreset>();
        List<Interactable> allExisting = new List<Interactable>();

        List<InteractablePreset> required = new List<InteractablePreset>();
        List<Interactable> requiredExisting = new List<Interactable>();

        if (tabState == FurnishingsController.TabState.inRoom)
        {
            allExisting = new List<Interactable>();

            foreach (NewNode n in room.nodes)
            {
                foreach (Interactable i in n.interactables)
                {
                    if (!i.preset.spawnable || i.preset.prefab == null) continue;
                    if (i.rem || i.rPl) continue; //Don't count if removed from world or deleted
                    if (!i.preset.allowInApartmentStorage) continue;
                    if (PlayerApartmentController.Instance.itemStorage.Contains(i)) continue; //Don't show items in storage

                    if(!allExisting.Contains(i))
                    {
                        allExisting.Add(i);
                    }
                }
            }
        }
        else if(tabState == FurnishingsController.TabState.inStorage)
        {
            allExisting = new List<Interactable>(PlayerApartmentController.Instance.itemStorage);
        }
        else if(tabState == FurnishingsController.TabState.inShop)
        {
            allRelevant = new List<InteractablePreset>();

            foreach(KeyValuePair<string, InteractablePreset> pair in Toolbox.Instance.objectPresetDictionary)
            {
                if(pair.Value.spawnable && pair.Value.prefab != null && pair.Value.allowInApartmentShop)
                {
                    allRelevant.Add(pair.Value);
                }
            }

            foreach (InteractablePreset h in allRelevant)
            {
                if (required.Contains(h)) continue;

                if (searchInputField.text.Length <= 0)
                {
                    required.Add(h);
                }
                else
                {
                    string realText = Strings.Get("evidence.names", h.presetName);

                    if (realText.ToLower().Contains(searchInputField.text.ToLower()))
                    {
                        required.Add(h);
                    }
                }
            }
        }

        if(tabState == FurnishingsController.TabState.inRoom || tabState == FurnishingsController.TabState.inStorage)
        {
            foreach (Interactable f in allExisting)
            {
                if (requiredExisting.Contains(f)) continue;

                if (!displayClasses.Contains(f.preset.itemClass)) continue;

                if (searchInputField.text.Length <= 0)
                {
                    requiredExisting.Add(f);
                }
                else
                {
                    string realText = f.GetName();

                    if (realText.ToLower().Contains(searchInputField.text.ToLower()))
                    {
                        requiredExisting.Add(f);
                    }
                }
            }
        }

        //Sort required by price
        required.Sort((p1, p2) => p1.value.x.CompareTo(p2.value.x));
        requiredExisting.Sort((p1, p2) => p1.preset.value.x.CompareTo(p2.preset.value.x));

        //Go through existing list
        for (int i = 0; i < spawnedEntries.Count; i++)
        {
            ApartmentItemElementController sec = spawnedEntries[i];

            //If a null entry, this has obviously been destroyed- remove entry
            if (sec == null)
            {
                spawnedEntries.RemoveAt(i);
                i--;
                continue;
            }

            if (required.Contains(sec.itemPreset))
            {
                required.Remove(sec.itemPreset);
                sec.VisualUpdate();
            }
            else if (requiredExisting.Contains(sec.worldItemReference))
            {
                required.Remove(sec.itemPreset);
                sec.VisualUpdate();
            }
            else
            {
                Destroy(sec.gameObject);
                spawnedEntries.RemoveAt(i);
                i--;
                continue;
            }
        }

        //The ones left in the found files list are buttons to spawn
        foreach (InteractablePreset sec in required)
        {
            GameObject newButton = null;

            newButton = Instantiate(itemElementPrefab, entryParent);

            ApartmentItemElementController newEntry = newButton.GetComponent<ApartmentItemElementController>();

            newEntry.SetupItem(sec, this, wcc.window, null);
            spawnedEntries.Add(newEntry);
        }

        foreach (Interactable sec in requiredExisting)
        {
            GameObject newButton = null;

            newButton = Instantiate(itemElementPrefab, entryParent);

            ApartmentItemElementController newEntry = newButton.GetComponent<ApartmentItemElementController>();

            newEntry.SetupItem(sec.preset, this, wcc.window, sec);
            spawnedEntries.Add(newEntry);
        }

        for (int i = 0; i < spawnedEntries.Count; i++)
        {
            spawnedEntries[i].transform.SetAsLastSibling();
        }

        //Update rect sizes...
        entryParent.sizeDelta = new Vector2(entryParent.sizeDelta.x, Mathf.Max(spawnedEntries.Count * 130f + 24f, 466));
        //SetPageSize(new Vector2(rect.sizeDelta.x, entryParent.sizeDelta.y + 500f));
    }

    public void ToggleDisplayClass(int classInt)
    {
        InteractablePreset.ItemClass decorClass = (InteractablePreset.ItemClass)classInt;

        if(displayClasses.Contains(decorClass))
        {
            displayClasses.Remove(decorClass);
        }
        else
        {
            displayClasses.Add(decorClass);
        }

        UpdateListDisplay();
    }

    public void PlaceObject(Interactable existingObject)
    {
        //Pick up lockpicks
        if(existingObject.preset.actionsPreset.Exists(item => item.actions.Exists(item => item.interactionName.ToLower() == "take (lockpick)")))
        {
            if(existingObject.preset.presetName == "LockpickKit")
            {
                ActionController.Instance.TakeLockpickKit(existingObject, Player.Instance.currentNode, Player.Instance);
            }
            else
            {
                ActionController.Instance.TakeLockpick(existingObject, Player.Instance.currentNode, Player.Instance);
            }

            UpdateListDisplay();
            return;
        }

        //Placement mode
        Game.Log("Decor: Set selected item: " + existingObject.GetName());
        existingObject.rPl = false; //Reset removed from world
        ActionController.Instance.PickUp(existingObject, Player.Instance.currentNode, Player.Instance);

        if (existingObject.spawnedObject == null)
        {
            Game.Log("Decor: Unable to spawn object to world! " + existingObject.inInventory);
        }

        existingObject.SetPhysicsPickupState(true, Player.Instance);

        //wcc.window.CloseWindow(false); //Close this
        SessionData.Instance.ResumeGame();
    }

    public void ClearSearchButton()
    {
        searchInputField.text = string.Empty;
    }

    public void OnFurnitureChange()
    {
        UpdateListDisplay();
    }

    private void OnDestroy()
    {
        PlayerApartmentController.Instance.OnFurnitureChange -= OnFurnitureChange;

        PlayerApartmentController.Instance.rememberRoomStorageShop = tabState;
        PlayerApartmentController.Instance.rememberItemDisplayClasses = new List<InteractablePreset.ItemClass>(displayClasses);
    }

    public void MoveAllToStorageButton()
    {
        PopupMessageController.Instance.PopupMessage("Move to Storage", true, true, RButton: "Confirm");
        PopupMessageController.Instance.OnRightButton += ConfirmMoveToStorage;
        PopupMessageController.Instance.OnLeftButton += CancelMoveToStorage;
    }

    public void ConfirmMoveToStorage()
    {
        PopupMessageController.Instance.OnRightButton -= ConfirmMoveToStorage;
        PopupMessageController.Instance.OnLeftButton -= CancelMoveToStorage;

        Game.Log("Decor: Move all items to storage...");
        List<Interactable> allObjects = new List<Interactable>();

        foreach (NewNode n in room.nodes)
        {
            foreach(Interactable i in n.interactables)
            {
                if (i.furnitureParent != null && i.furnitureParent.integratedInteractables.Contains(i)) continue; //Skip integrated
                if (!i.preset.spawnable) continue; //Skip non spawnable
                if (i.preset.disableMoveToStorage) continue;

                if(!allObjects.Contains(i))
                {
                    allObjects.Add(i);
                }
            }
        }

        foreach (Interactable i in allObjects)
        {
            PlayerApartmentController.Instance.MoveItemToStorage(i);
        }

        UpdateListDisplay();
    }

    public void CancelMoveToStorage()
    {
        PopupMessageController.Instance.OnRightButton -= ConfirmMoveToStorage;
        PopupMessageController.Instance.OnLeftButton -= CancelMoveToStorage;
    }
}
