using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

public class DebugGraffitiScaler : MonoBehaviour
{
    public ArtPreset art;
    public DecalProjector decal;
    public float pixelScaleMultiplier = 0.05f;

    [Button]
    public void LoadArt()
    {
        if(art != null && decal != null)
        {
            decal.material = art.material;
            pixelScaleMultiplier = art.pixelScaleMultiplier;
            SetScale();
        }
    }
    [Button]
    public void SetScale()
    {
        if (decal != null)
        {
            //Set size to tex size * 0.02
            Texture tex = decal.material.GetTexture("_BaseColorMap");
            decal.size = new Vector3(tex.width * pixelScaleMultiplier, tex.height * pixelScaleMultiplier, 0.13f);
        }
    }

    [Button]
    public void SavePixelScale()
    {
#if UNITY_EDITOR
        if (art != null && decal != null)
        {
            art.pixelScaleMultiplier = pixelScaleMultiplier;
            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
    }
}
