﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

[System.Serializable]
public class CitySaveData
{
    public string build;
    public string cityName = "New City";
    public string seed = "Abaos93rmnisf932";
    public Vector2 citySize = new Vector2(5, 5);
    //public CityData.PopulationAmount populationAmount = CityData.PopulationAmount.high;
    public int population = 0;
    public int playersApartment = -1;

    //Districts: This should be able to hold all map data
    public List<DistrictCitySave> districts = new List<DistrictCitySave>();
    public List<StreetCitySave> streets = new List<StreetCitySave>();
    public List<CityTileCitySave> cityTiles = new List<CityTileCitySave>();
    public List<HumanCitySave> citizens = new List<HumanCitySave>();
    public List<Interactable> interactables = new List<Interactable>();
    public List<GroupsController.SocialGroup> groups = new List<GroupsController.SocialGroup>();
    public List<PipeConstructor.PipeGroup> pipes = new List<PipeConstructor.PipeGroup>();

    //Criminal Jobs
    public List<OccupationCitySave> criminals = new List<OccupationCitySave>();

    //Saved multi page evidence
    public List<EvidenceStateSave> multiPage = new List<EvidenceStateSave>();

    //Meta objects
    public List<MetaObject> metas = new List<MetaObject>();

    //IDs
    //public int interactableID = 0;

    [System.Serializable]
    public class DistrictCitySave
    {
        public string name;
        public string preset;
        public int districtID = 0;
        public List<BlockCitySave> blocks = new List<BlockCitySave>();
        public float averageLandValue = 0f;

        public List<SocialStatistics.EthnicityFrequency> dominantEthnicities = new List<SocialStatistics.EthnicityFrequency>(); //List of dominant ethnicities
    }

    [System.Serializable]
    public class BlockCitySave
    {
        public string name;
        public int blockID = 0;
        public float averageDensity = 0f;
        public float averageLandValue = 0f;
    }

    [System.Serializable]
    public class CityTileCitySave
    {
        public string name;
        public int blockID;
        public int districtID;

        public Vector2Int cityCoord; //Coordinate relative to other city tiles
        public BuildingCitySave building; //Building present on the tile
        public List<TileCitySave> outsideTiles = new List<TileCitySave>(); //List of outside street tiles

        public BuildingPreset.Density density = BuildingPreset.Density.medium; //Density (how tall are the buildings)
        public BuildingPreset.LandValue landValue = BuildingPreset.LandValue.medium; //Land value
    }

    [System.Serializable]
    public class BuildingCitySave
    {
        public int buildingID = 0;
        public string name;
        public List<FloorCitySave> floors = new List<FloorCitySave>();

        public string preset;
        public NewBuilding.Direction facing = NewBuilding.Direction.North;
        public bool isInaccessible = false;

        public List<NewBuilding.SideSign> sideSigns;

        public List<AirDuctGroupCitySave> airDucts = new List<AirDuctGroupCitySave>();

        //public int mainEntranceID; //Entrances will be added when walls are loaded
        //public List<int> additionalEntrances = new List<int>(); //Entrances will be added when walls are loaded

        public string designStyle; //Design style chosen for the address
        public Color wood; //Colour of the wood for this address

        public string floorMaterial;
        public Toolbox.MaterialKey floorMatKey;

        public string ceilingMaterial;
        public Toolbox.MaterialKey ceilingMatKey;

        public string defaultWallMaterial;
        public Toolbox.MaterialKey defaultWallKey;

        public string extWallMaterial;
        public Toolbox.MaterialKey extWallKey;

        public string colourScheme;

        //Overrides
        public string floorMatOverride;
        public string ceilingMatOverride;
        public string wallMatOverride;

        public string floorMatOverrideB;
        public string ceilingMatOverrideB;
        public string wallMatOverrideB;
    }

    [System.Serializable]
    public class AirDuctGroupCitySave
    {
        public int id;
        public bool ext;
        public List<int> airVents = new List<int>(); //Air vents by ID reference
        public List<AirDuctSegmentCitySave> airDucts = new List<AirDuctSegmentCitySave>();
        public List<int> ventRooms = new List<int>();
        public List<int> adjoining = new List<int>();
    }

    [System.Serializable]
    public class AirDuctSegmentCitySave
    {
        public int level = 0;
        public int index = 0;
        public Vector3Int duct;
        public Vector3Int previous;
        public Vector3Int next;
        public Vector3Int node;
        public bool peek;
        public Vector3Int addRot;
    }

    [System.Serializable]
    public class AirVentSave
    {
        public int id;
        public NewAddress.AirVent ventType;
        public int wall = -1; //If wall vent, this is the wall
        public Vector3Int node; //If ceiling vent, this is the node
        public Vector3Int rNode; //Room side node
    }

    [System.Serializable]
    public class FloorCitySave
    {
        public string name;
        public int floorID = 0;
        public int floor = 0;
        public List<AddressCitySave> addresses = new List<AddressCitySave>(); //List of addresses
        public List<TileCitySave> tiles = new List<TileCitySave>();
        public Vector2 size = new Vector2(1, 1);
        public int defaultFloorHeight = 0;
        public int defaultCeilingHeight = 42;
        public int layoutIndex = 0;

        public int breakerSec = -1;
        public int breakerLights = -1;
        public int breakerDoors = -1;
    }

    [System.Serializable]
    public class TileCitySave
    {
        public int tileID = 0;
        public Vector2Int floorCoord; //Coordinate within floor
        public Vector3Int globalTileCoord; //Coordinate within all tiles
        public bool isOutside = false; //True if this tile is outside, false if it's indoors
        public bool isObstacle = false; //True if the whole tile is considered an obstacle
        //public bool isAlley = false; //True if the all nodes are considered an alley
        //public bool isBackstreet = false; //True if this is street space behind a building
        public bool isEdge = false; //True if this reperesents the edge of a groundmap tile (eg road space)
        public int rotation = 0; //Rotation of this tile relative to the address
        public bool isEntrance = false; //True if this is an entrance
        public bool isMainEntrance = false; //True if this is a main entrance
        public bool isStairwell = false; //True if this is a stairwell
        public int stairwellRotation = 0; //Rotation of the stairwell
        public bool isElevator = false; //True if this is an elevator
        public int elevatorRotation = 0; //Rotation of the elevator
        public bool isTop = false; //If true this is the top of the elevator/stairs
        public bool isBottom = false; //If true this is the bottom of the elevator/stairs
        //public bool useOptimizedFloor = false;
        //public bool useOptimizedCeiling = false;
    }

    [System.Serializable]
    public class StreetCitySave
    {
        public string name;
        public int residenceNumber = 0; //Residence number relative to this floor (set in addresses only)
        public bool isLobby = false; //Game Only: True if this is a building lobby (set in addresses only)
        public bool isMainLobby = false; //Game Only: True if this lobby features the main entrance
        public bool isOutside = false; //Is this location outside?
        public AddressPreset.AccessType access = AddressPreset.AccessType.allPublic;
        public List<RoomCitySave> rooms = new List<RoomCitySave>(); //List of rooms
        public string designStyle; //Design style chosen for the address

        public int streetID = 0;
        public int district;
        public List<Vector3Int> tiles = new List<Vector3Int>();
        public string streetSuffix = string.Empty;
        public bool isAlley = false; //True if this road is an alleyway
        public bool isBackstreet = false; //True if this is street space behind a building
        public List<int> sharedGround;
        public List<StreetController.StreetTile> streetTiles = null;

       // public List<int> visibleStreets = new List<int>(); //Visible streets from this
    }

    [System.Serializable]
    public class AddressCitySave
    {
        public string name;
        public int residenceNumber = 0; //Residence number relative to this floor (set in addresses only)
        public bool isLobby = false; //Game Only: True if this is a building lobby (set in addresses only)
        //public bool isMainLobby = false; //Game Only: True if this lobby features the main entrance
        public bool isOutside = false; //Is this location outside?
        public AddressPreset.AccessType access = AddressPreset.AccessType.allPublic;
        public List<RoomCitySave> rooms = new List<RoomCitySave>(); //List of rooms
        public string designStyle; //Design style chosen for the address
        public bool neonHor = false;
        public bool neonVer = false;
        public int neonVerticalIndex = -1;
        public int neonColour;
        public string neonFont;
        public float landValue = 0;
        public GameplayController.Passcode passcode;

        public List<Vector3> protectedNodes = new List<Vector3>(); //List of nodes that can't be overwritten: Reset these upon another generation cycle
        public int id = 0; //Editor IDs used for naming inside the floor editor
        public string address; //Address details preset
        public string preset; //Preset used to determin interior generation
        public Color wood; //Colour of the wood for this address
        public ResidenceCitySave residence; //Residence controller
        public CompanyCitySave company; //Company controller
        public bool isOutsideAddress;
        public bool isLobbyAddress;

        public bool hkey = false; //Hidden key

        public int breakerSec = -1;
        public int breakerLights = -1;
        public int breakerDoors = -1;

        //public List<string> noteDebug;
    }

    [System.Serializable]
    public class ResidenceCitySave
    {
        public string preset;
        //public int bedroomsTaken = 0;
        public int mail = 0;
    }

    [System.Serializable]
    public class CompanyCitySave
    {
        public string preset;
        public int id;
        public List<OccupationCitySave> companyRoster = new List<OccupationCitySave>();
        public float topSalary;
        public float minimumSalary;
        public bool publicFacing = true;
        public string shortName;
        public List<string> nameAltTags;
        public bool monday = true;
        public bool tuesday = true;
        public bool wednesday = true;
        public bool thursday = true;
        public bool friday = true;
        public bool saturday = true;
        public bool sunday = false;
        public Vector2 retailOpenHours = new Vector2(8, 17);
        public int passedWorkLocation = -1;

        //Menus
        public List<string> menuItems = new List<string>();
        public List<int> itemCosts = new List<int>();
    }

    [System.Serializable]
    public class OccupationCitySave
    {
        public int id;
        public string preset;
        public string name = "worker";
        public bool teamLeader = false;
        public int boss;
        public float paygrade; //0-1 float to pass through pay curve
        public int teamID;
        public bool isOwner = false;
        public OccupationPreset.workCollar collar;
        public OccupationPreset.workType work;
        public List<OccupationPreset.workTags> tags = new List<OccupationPreset.workTags>();
        public int shift;
        public float startTime = 9f;
        public float endTime = 17f;
        public List<SessionData.WeekDay> workDaysList = new List<SessionData.WeekDay>();
        public float salary = 0f;
        public string salaryString = string.Empty;
    }

    [System.Serializable]
    public class RoomCitySave
    {
        public string name;
        public List<NodeCitySave> nodes = new List<NodeCitySave>();
        public List<string> openPlanElements = new List<string>();
        public List<LightZoneSave> lightZones = new List<LightZoneSave>(); //List of lists of the room divided into rectangles for ceiling lights.
        public List<int> commonRooms = new List<int>(); //List of rooms that share the same light system (assigned when decor is generated).
        public int floorID = -1;
        public int id = -1; //This must be saved as in some cases it can determin parent
        public int fID = 1; //The furniture assign id for this room (needed for accurate furniture)
        public int iID = 1; //The interactable assign id for this room (needed for accruate object ids)
        public string preset;
        public bool reachableFromEntrance = false; //Used in address generation to heck if this room can be accessed
        public bool isOutsideWindow = false; //If true this room is the outside area outside a building
        public bool allowCoving = false; //If true this room will feature coving
        public string floorMaterial;
        public Toolbox.MaterialKey floorMatKey;
        public string ceilingMaterial;
        public Toolbox.MaterialKey ceilingMatKey;
        public string defaultWallMaterial;
        public Toolbox.MaterialKey defaultWallKey;
        public Toolbox.MaterialKey miscKey;
        public string colourScheme;
        public string mainLightPreset; //Chosen main light preset
        public bool isBaseNullRoom;
        public Vector3 middle;
        public List<FurnitureClusterCitySave> f = new List<FurnitureClusterCitySave>(); //furniture
        public List<int> owners = new List<int>();
        public List<AirVentSave> airVents = new List<AirVentSave>(); //Air vents by ID reference
        public GameplayController.Passcode password;
        public int cf = -1; //Ceiling fans

        public List<CullTreeSave> cullTree = new List<CullTreeSave>(); //Save culling tress

        //Above, below and adjacent
        public List<int> above = new List<int>();
        public List<int> below = new List<int>();
        public List<int> adj = new List<int>();

        //Non audio-occluded rooms
        public List<int> occ = new List<int>();
    }

    [System.Serializable]
    public class CullTreeSave
    {
        public int r;
        public List<int> d;
    }

    [System.Serializable]
    public class LightZoneSave
    {
        public List<Vector3Int> n = new List<Vector3Int>(); //FLOOR Node coordinates
        public Color areaLightColour;
        public float areaLightBright;
    }

    [System.Serializable]
    public class NodeCitySave
    {
        public Vector2Int fc; //floorCoord
        public Vector2Int ltc; //localTileCoord
        public Vector3Int nc; //nodeCoord
        public List<WallCitySave> w = new List<WallCitySave>(); //walls
        public int fh = 0; //floorHeight
        //public int ch = 42; //ceilingHeight
        public NewNode.FloorTileType ft = NewNode.FloorTileType.floorAndCeiling; //floorType
        //public List<Vector2> pe = new List<Vector2>(); //preventEntrances
        //public float nwm = 1f; //nodeWeightMultiplier
        public bool io = false; //isObstacle
        //public bool ia = false; //isAlley
        //public bool ib = false; //isBackstreet
        public bool ios = false; //isOutside
        //public bool ic = false; //isConnected
        public bool sll = false; //stairwellLowerLink
        public bool sul = false; //stairwellUpperLink
        public string fr; //forcedRoom
        public string frr; //forcedRoomRef
        public bool anf = true; //allowNewFurniture
        //public bool wt = true; //walkableTile
        public bool cav = false; //Ceiling air vent
        public bool fav = false; //Floor air vent
    }

    [System.Serializable]
    public class WallCitySave
    {
        public Vector2 wo; //wallOffset
        //public string wm; //wallMaterial
        //public Toolbox.MaterialKey wmk; //wallMatKey
        public int id = -1; //id
        public string p; //preset
        public int ow; //otherWall
        public int pw; //parentWall
        public int cw; //childWall
        public bool oo = false; //optimizationOverride
        public bool oa = false; //optimizationAnchor
        public int nos = 0; //nonOptimizedSegment
        public bool isw = false; //isShortWall
        public int cl = -1; //containsLightswitch
        public bool sw = false; //separate wall
        //public List<int> vfw = new List<int>(); //Visible from window
        public List<WallFrontageSave> fr = new List<WallFrontageSave>(); //Frontage

        public bool dm = false; //Uses door material key
        public Toolbox.MaterialKey dmk; //Door material key

        public float ds = 0f; //Door strength
        public float ls = 0f; //Lock Strength
    }

    [System.Serializable]
    public class WallFrontageSave
    {
        public string str;
        public Toolbox.MaterialKey matKey; //Chosen when added to a room
        public Vector3 o;
    }

    [System.Serializable]
    public class FurnitureClusterCitySave
    {
        public string cluster; //Cluster preset
        public Vector3Int anchorNode; //Reference to the anchor node
        public int angle; //The angle this object should be facing
        public float ranking = 0f;

        public List<FurnitureClusterObjectCitySave> objs = new List<FurnitureClusterObjectCitySave>(); //Cluster objects
    }

    [System.Serializable]
    public class FurnitureClusterObjectCitySave
    {
        public int id;
        public List<string> furnitureClasses;
        public int angle;
        public Vector3Int anchorNode;
        public List<Vector3Int> coversNodes;
        public Vector3 offset;
        public string furniture; //Chosen furniture
        public string art; //Chosen art

        public bool useFOVBLock = false;
        public Vector2 fovDirection;
        public int fovMaxDistance = 5;

        public bool up = false; //User placed

        public Vector3 scale;

        public Toolbox.MaterialKey matKey; //Chosen when added to a room
        public Toolbox.MaterialKey artMatKet;
        public List<int> owners = new List<int>(); //List of owners (human = + and address = -)
    }

    [System.Serializable]
    public class HumanCitySave
    {
        public int humanID = -1;
        public int home;
        public string debugHome;
        public float speedModifier = 0.12f;
        public int job;
        public string birthday;
        public float societalClass = 0f; //Rank from 0 - 1, 0 being working class, 1 being upper class. Based on job paygrade
        public Descriptors descriptors;
        public Human.BloodType blood;
        public string citizenName = string.Empty;
        public string firstName = string.Empty;
        public string casualName = string.Empty;
        public string surName = string.Empty;
        //public string initialledName = string.Empty;
        public bool homeless = false;

        public float slangUsage = 0.5f;
        //public string slangDefault;
        //public string slangMale;
        //public string slangFemale;
        //public string slangLover;
        //public string slangCurse;
        //public string slangCurseNoun;
        //public string slandPraiseNoun;

        public float genderScale = 0.5f;
        public Human.Gender gender = Human.Gender.male;
        public Human.Gender bGender = Human.Gender.male;
        public float sexuality = 0.5f;
        public float homosexuality = 0.5f;
        public List<Human.Gender> attractedTo = new List<Human.Gender>();
        public int partner = -1;
        public int paramour = -1;
        public string anniversary;
        public float sleepNeedMultiplier = 1f; //Criminals will need more spare time, so need less sleep
        public float snoring = 1f; //Amount this person snores in their sleep
        public float snoreDelay = 2f; //Time between snores in seconds
        public float humility = 0f;
        public float emotionality = 0f;
        public float extraversion = 0f;
        public float agreeableness = 0f;
        public float conscientiousness = 0f;
        public float creativity = 0f;
        public List<AcquaintanceCitySave> acquaintances = new List<AcquaintanceCitySave>();
        public List<CharTraitSave> traits = new List<CharTraitSave>();
        public GameplayController.Passcode password;
        public float maxHealth = 1f;
        public float recoveryRate = 0.1f;
        public float combatSkill = 1f;
        public float combatHeft = 0.25f;
        public float maxNerve = 1f;
        public float breathRecovery = 1f;

        public string handwriting;

        public int sightingMemory = 100;

        //Favourites
        public List<string> favItems = new List<string>();
        public List<int> favItemRanks = new List<int>();

        public List<CompanyPreset.CompanyCategory> favCat = new List<CompanyPreset.CompanyCategory>();
        public List<int> favAddresses = new List<int>();

        //Outfits
        public List<CitizenOutfitController.Outfit> outfits = new List<CitizenOutfitController.Outfit>();

        //public List<string> noteDebug;

        public int favCol; //Fav colour index
    }

    [System.Serializable]
    public class CharTraitSave
    {
        public int traitID = 0;
        public string trait;
        public int reason = -1;
        public string date;
    }

    [System.Serializable]
    public class AcquaintanceCitySave
    {
        public int from;
        public int with;
        public List<Acquaintance.ConnectionType> connections = new List<Acquaintance.ConnectionType>();
        public Acquaintance.ConnectionType secret = Acquaintance.ConnectionType.friend;
        public float compatible = 0f;
        public float known = 0.1f;
        public float like = 0f;
        public List<Evidence.DataKey> dataKeys = new List<Evidence.DataKey>();
    }

    [System.Serializable]
    public class EvidenceStateSave
    {
        public string id;
        public int page;

        //Specific evidence data
        public List<EvidenceMultiPage.MultiPageContent> mpContent = new List<EvidenceMultiPage.MultiPageContent>();
    }
}
