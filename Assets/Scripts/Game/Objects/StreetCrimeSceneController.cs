using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using NaughtyAttributes;

public class StreetCrimeSceneController : MonoBehaviour
{
    [Header("Components")]
    public Transform middle;
    public List<GameObject> elements = new List<GameObject>();

    private void Start()
    {
        int mask = Toolbox.Instance.CreateLayerMask(Toolbox.LayerMaskMode.castAllExcept, 2, 18, 30, 31, 24);

        //Raycast to elements to see what we can see...
        foreach (GameObject element in elements)
        {
            Vector3 elPos = element.transform.position + new Vector3(0, 1, 0);

            Vector3 dir = elPos - middle.position;

            Ray r = new Ray(middle.position, dir);

            RaycastHit hit;

            if (Physics.Raycast(r, out hit, Vector3.Distance(elPos, middle.position), mask, QueryTriggerInteraction.Collide))
            {
                //If hit element then allow...
                if (hit.collider.gameObject != element)
                {
                    Destroy(element);
                }
            }

            if(element != null)
            {
                MeshRenderer rend = element.GetComponent<MeshRenderer>();
                rend.enabled = true;
            }
        }

    }
}
