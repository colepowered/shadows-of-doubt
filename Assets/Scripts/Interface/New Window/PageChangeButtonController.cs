using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageChangeButtonController : ButtonController
{
    public bool nextPage = false;

    public override void OnLeftClick()
    {
        base.OnLeftClick();

        WindowContentController wcc = this.gameObject.GetComponentInParent<WindowContentController>();

        if(wcc != null)
        {
            if(nextPage)
            {
                wcc.NextPage();
            }
            else
            {
                wcc.PrevPage();
            }
        }
    }
}
