using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class CleanupController : MonoBehaviour
{
    [Header("Interactables")]
    [InfoBox("Breakdown of active interactables", EInfoBoxType.Normal)]
    [ReadOnly]
    public int totalInteractables = 0;
    [ReadOnly]
    public int removedCityDataInteractables = 0;

    [System.NonSerialized]
    public List<int> removedCityDataItems = new List<int>();

    [ReadOnly]
    public int savableCount = 0;
    [ProgressBar("Savable %", 100, EColor.Green)]
    public int savablePercent = 0;

    [ReadOnly]
    public int trashCount = 0;
    [ProgressBar("Trash %", 100, EColor.Blue)]
    public int trashPercent = 0;

    [ReadOnly]
    public int trashThreshold = 0;
    [ProgressBar("Trash Threshold %", 100, EColor.Red)]
    public int trashThresholdPercent = 0;

    [ReadOnly]
    public int trashRemovedLastUpdate = 0;

    [Space(7)]
    public List<DebugInteractable> breakdownSavable = new List<DebugInteractable>();
    public List<DebugInteractable> breakdownNonSavable = new List<DebugInteractable>();
    public List<DebugInteractable> breakdownTrash = new List<DebugInteractable>();

    [System.Serializable]
    public class DebugInteractable
    {
        public string name;
        public int count;
        [ProgressBar("Savable", 100, EColor.Green)]
        public int savablePercent;
        [ProgressBar("Trash", 100, EColor.Blue)]
        public int trashPercent;

        [Space(7)]
        public List<SaveableBecause> savableDetails = new List<SaveableBecause>();
    }

    [System.Serializable]
    public class SaveableBecause
    {
        public string reason;
        public int count;
    }

    [Header("Trash")]
    [ReadOnly]
    public int currentTrash = 0;
    [System.NonSerialized]
    public List<Interactable> trash = new List<Interactable>();
    [ReadOnly]
    public int binTrash = 0;

    //Singleton pattern
    private static CleanupController _instance;
    public static CleanupController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    //Check to remove unwanted objects
    public void TrashUpdate()
    {
        //Check and remove trash
        if (trash.Count > GameplayControls.Instance.globalCreatedTrashLimit)
        {
            List<Interactable> toRemove = new List<Interactable>();

            for (int i = 0; i < trash.Count; i++)
            {
                Interactable picked = trash[i];

                if (picked.IsSafeToDelete())
                {
                    //Is not in storage or owned apartment
                    if(!PlayerApartmentController.Instance.itemStorage.Contains(picked) && (picked.node == null || picked.node.gameLocation == null || picked.node.gameLocation.thisAsAddress == null || (!Player.Instance.apartmentsOwned.Contains(picked.node.gameLocation.thisAsAddress) && picked.node.gameLocation != Player.Instance.home)))
                    {
                        toRemove.Add(picked);
                    }
                }

                if (trash.Count - toRemove.Count <= GameplayControls.Instance.globalCreatedTrashLimit) break; //This will take us under the limit
            }

            trashRemovedLastUpdate = toRemove.Count;

            foreach (Interactable t in toRemove)
            {
                t.Delete(); //Remove completely (Removes from trash)
            }

            currentTrash = trash.Count;
        }
    }

    [Button]
    public void UpdateData()
    {
        totalInteractables = 0;
        savableCount = 0;
        trashCount = 0;
        trashThreshold = GameplayControls.Instance.globalCreatedTrashLimit;
        removedCityDataInteractables = removedCityDataItems.Count;

        Dictionary<InteractablePreset, List<Interactable>> breakdownDictSavable = new Dictionary<InteractablePreset, List<Interactable>>();
        Dictionary<InteractablePreset, List<Interactable>> breakdownDictNonSavable = new Dictionary<InteractablePreset, List<Interactable>>();
        Dictionary<InteractablePreset, List<Interactable>> breakdownDictTrash = new Dictionary<InteractablePreset, List<Interactable>>();

        foreach (Interactable i in CityData.Instance.interactableDirectory)
        {
            if (i.IsSaveStateEligable())
            {
                if (!breakdownDictSavable.ContainsKey(i.preset))
                {
                    breakdownDictSavable.Add(i.preset, new List<Interactable>());
                }

                breakdownDictSavable[i.preset].Add(i);

                savableCount++;
            }
            else
            {
                if (!breakdownDictNonSavable.ContainsKey(i.preset))
                {
                    breakdownDictNonSavable.Add(i.preset, new List<Interactable>());
                }

                breakdownDictNonSavable[i.preset].Add(i);
            }

            if (trash.Contains(i))
            {
                if (!breakdownDictTrash.ContainsKey(i.preset))
                {
                    breakdownDictTrash.Add(i.preset, new List<Interactable>());
                }

                breakdownDictTrash[i.preset].Add(i);

                trashCount++;
            }

            totalInteractables++;
        }

        savablePercent = Mathf.RoundToInt((float)savableCount / (float)totalInteractables * 100f);
        trashPercent = Mathf.RoundToInt((float)trashCount / (float)totalInteractables * 100f);
        trashThresholdPercent = Mathf.RoundToInt((float)trashCount / (float)trashThreshold * 100f);

        breakdownSavable.Clear();
        breakdownNonSavable.Clear();
        breakdownTrash.Clear();

        List<InteractablePreset> keys = new List<InteractablePreset>(breakdownDictSavable.Keys);

        foreach (InteractablePreset ip in keys)
        {
            DebugInteractable newDebug = new DebugInteractable();
            newDebug.count = breakdownDictSavable[ip].Count;
            newDebug.name = ip.name + " (" + newDebug.count + ")";

            int savableCount = 0;
            int trashCount = 0;

            foreach(Interactable i in breakdownDictSavable[ip])
            {
                if (i.IsSaveStateEligable())
                {
                    savableCount++;

                    string reason = i.GetReasonForSaveStateEligable();

                    SaveableBecause r = newDebug.savableDetails.Find(item => item.reason == reason);

                    if(r != null)
                    {
                        r.count++;
                    }
                    else
                    {
                        newDebug.savableDetails.Add(new SaveableBecause { reason = reason, count = 1 });
                    }
                }

                if (trash.Contains(i)) trashCount++;
            }

            newDebug.savableDetails.Sort((p2, p1) => p1.count.CompareTo(p2.count));

            newDebug.savablePercent = Mathf.RoundToInt(((float)savableCount / (float)newDebug.count) * 100f);
            newDebug.trashPercent = Mathf.RoundToInt(((float)trashCount / (float)newDebug.count) * 100f);

            breakdownSavable.Add(newDebug);
        }

        breakdownSavable.Sort((p2, p1) => p1.count.CompareTo(p2.count));

        keys = new List<InteractablePreset>(breakdownDictNonSavable.Keys);

        foreach (InteractablePreset ip in keys)
        {
            DebugInteractable newDebug = new DebugInteractable();
            newDebug.count = breakdownDictNonSavable[ip].Count;
            newDebug.name = ip.name + " (" + newDebug.count + ")";

            int savableCount = 0;
            int trashCount = 0;

            foreach (Interactable i in breakdownDictNonSavable[ip])
            {
                if (i.IsSaveStateEligable()) savableCount++;
                if (trash.Contains(i)) trashCount++;
            }

            newDebug.savablePercent = Mathf.RoundToInt(((float)savableCount / (float)newDebug.count) * 100f);
            newDebug.trashPercent = Mathf.RoundToInt(((float)trashCount / (float)newDebug.count) * 100f);

            breakdownNonSavable.Add(newDebug);
        }

        breakdownNonSavable.Sort((p2, p1) => p1.count.CompareTo(p2.count));

        keys = new List<InteractablePreset>(breakdownDictTrash.Keys);

        foreach (InteractablePreset ip in keys)
        {
            DebugInteractable newDebug = new DebugInteractable();
            newDebug.count = breakdownDictTrash[ip].Count;
            newDebug.name = ip.name + " (" + newDebug.count + ")";

            int savableCount = 0;
            int trashCount = 0;

            foreach (Interactable i in breakdownDictTrash[ip])
            {
                if (i.IsSaveStateEligable()) savableCount++;
                if (trash.Contains(i)) trashCount++;
            }

            newDebug.savablePercent = Mathf.RoundToInt(((float)savableCount / (float)newDebug.count) * 100f);
            newDebug.trashPercent = Mathf.RoundToInt(((float)trashCount / (float)newDebug.count) * 100f);

            breakdownTrash.Add(newDebug);
        }

        breakdownTrash.Sort((p2, p1) => p1.count.CompareTo(p2.count));
    }

}
