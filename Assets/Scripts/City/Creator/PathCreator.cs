﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

//Generates paths for citizen routines
//Script pass 1
public class PathCreator : Creator
{
    //Chunks to load per frame
    public int loadChunk = 10;

    //List of paths to calculate
    public Dictionary<NewNode, List<NewNode>> pathsNeededWalking = new Dictionary<NewNode, List<NewNode>>();

    //Singleton pattern
    private static PathCreator _instance;
    public static PathCreator Instance { get { return _instance; } }

	void Awake ()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
	}

    //Cycle through all paths and generate them
    public override void StartLoading()
    {
        //Debug: Scan for invalid access locations
        bool foundErrors = false;

        foreach (NewAddress ac in CityData.Instance.addressDirectory)
        {
            foreach (NewNode lc in ac.nodes)
            {
                if (lc.accessToOtherNodes.Count <= 0)
                {
                    //Game.Log("Pathfinder: Building: " + lc.building.name);
                    //Game.Log("Pathfinder: Coord: " + lc.pathCoord);
                    //Game.Log("Pathfinder: Lobby: " + lc.isLobby);
                    //Game.Log("Pathfinder: Elevator: " + lc.isElevator);
                    //Game.Log("Pathfinder: Tile location: " + lc.floorTile.tileLocation);
                    //Game.Log("Pathfinder: Location has been connected (this): " + lc.debugLocHasBeenConnectedThis);
                    //Game.Log("Pathfinder: Location has been connected (via other): " + lc.debugLocHasBeenConnectedViaOther);
                    //Game.Log("Pathfinder: Tile access list: " + lc.floorTile.tileAccessList.Count);

                    Game.LogError("Pathfinder: Node with no access found!");
                    foundErrors = true;
                }
            }
        }

        if (foundErrors) return;

		StartCoroutine("GenChunk");
	}

	IEnumerator GenChunk()
	{
        //Dictionary of paths-to-find (walking)
        //Dictionaries hold a list of routine references, so driving can be changed/vice vers
		int pathsProgress = 0;

		while (pathsProgress < pathsNeededWalking.Count)
		{
			for (int i = 0; i < loadChunk; i++)
			{
				//Break loop if complete
				if (pathsProgress >= pathsNeededWalking.Count) break;

				//Walking
				if (pathsProgress < pathsNeededWalking.Count)
				{
                    KeyValuePair<NewNode, List<NewNode>> pair = pathsNeededWalking.ElementAt(pathsProgress);

                    foreach(NewNode dest in pair.Value)
                    {
                        //PathFinder.Instance.Pathfind(pair.Key, dest);
                    }
				}

				pathsProgress++;
			}

			CityConstructor.Instance.loadingProgress = Mathf.Min((float)pathsProgress / (float)pathsNeededWalking.Count, 0.99f);
            yield return null;
		}

        //Complete!
        SetComplete();
    }
}
