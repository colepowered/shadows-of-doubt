﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EvidencePhotoController : MonoBehaviour
{
    public InfoWindow parentWindow;
    public RawImage photoRaw;

    private void OnEnable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange += CheckEnabled;
        //parentWindow.OnWindowRefresh += CheckEnabled; //Don't think this is needed...

        CheckEnabled();
    }

    private void OnDisable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange -= CheckEnabled;
        //parentWindow.OnWindowRefresh -= CheckEnabled;
    }

    public void CheckEnabled()
    {
        Game.Log("Interface: Check for photo key in parent window...");
        photoRaw.texture = parentWindow.passedEvidence.GetPhoto(parentWindow.evidenceKeys);
        photoRaw.color = Color.white;
    }
}
