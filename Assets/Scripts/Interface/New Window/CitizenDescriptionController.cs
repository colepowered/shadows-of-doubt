﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CitizenDescriptionController : MonoBehaviour
{
    public InfoWindow parentWindow;
    public TextMeshProUGUI descriptionText;

    private void OnEnable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange += CheckEnabled;
        parentWindow.OnWindowRefresh += CheckEnabled;

        CheckEnabled();
    }

    private void OnDisable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange -= CheckEnabled;
        parentWindow.OnWindowRefresh -= CheckEnabled;
    }

    public void CheckEnabled()
    {
        descriptionText.text = parentWindow.passedEvidence.GetNoteComposed(parentWindow.evidenceKeys, true);
        //Game.Log("Description text for " + parentWindow.passedEvidence.GetNameForDataKey(Evidence.DataKey.name) + ": " + descriptionText.text);
    }
}
