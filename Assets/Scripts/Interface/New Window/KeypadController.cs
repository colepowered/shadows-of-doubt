﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class KeypadController : MonoBehaviour
{
    public InfoWindow parentWindow;
    public Evidence evidence;
    public WindowContentController windowContent;

    public TextMeshProUGUI inputText;
    public List<int> input = new List<int>();
    public Color defaultTextColour;

    public bool checking = false;
    public bool correct = false;
    public float checkCounter = 0f;

    public bool inputCodeActive = false;
    public bool isTelephone = false;
    public int digits = 4;

    private void OnEnable()
    {
        parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        windowContent = this.gameObject.GetComponentInParent<WindowContentController>();
        evidence = parentWindow.passedEvidence;
        defaultTextColour = inputText.color;

        //Is this a telephone?
        if (evidence == CityData.Instance.telephone)
        {
            isTelephone = true;
            digits = 7;
        }
        else digits = 4;

        ClearCode(false); //Clear on start to set display

        //Listen for input via buttons
        InterfaceController.Instance.OnInputCode += OnInputCode;

        //Do I have the right code for this already?
        if(!isTelephone)
        {
            //Set code input active
            InterfaceController.Instance.SetActiveCodeInput(this);

            Interactable inter = InteractionController.Instance.currentLookingAtInteractable.interactable;

            GameplayController.Passcode code = inter.GetPasswordSource();

            if(GameplayController.Instance.acquiredPasscodes.Contains(code))
            {
                OnInputCode(code.GetDigits());
            }

            SessionData.Instance.TutorialTrigger("passwords");
        }
        else
        {
            //Set code input active
            InterfaceController.Instance.SetActiveCodeInput(this);

            SessionData.Instance.TutorialTrigger("phones");
        }
    }

    private void OnDisable()
    {
        InterfaceController.Instance.OnInputCode -= OnInputCode;
        inputCodeActive = false;
    }

    private void OnDestroy()
    {
        if (InterfaceController.Instance.activeCodeInput == this)
        {
            InterfaceController.Instance.SetActiveCodeInput(null);
        }
    }

    public void PressNumberButton(int newInt)
    {
        if (checking) return; //No input during grace period

        //Stop dial tone
        if (isTelephone && parentWindow.passedInteractable.t != null)
        {
            if (parentWindow.passedInteractable.t.dialTone != null)
            {
                AudioController.Instance.StopSound(parentWindow.passedInteractable.t.dialTone, AudioController.StopType.immediate, "phone hang up"); //Stop dial tone
                parentWindow.passedInteractable.t.dialTone = null;
            }
        }

        input.Add(newInt);
        string inputString = string.Empty;

        for (int i = 0; i < input.Count; i++)
        {
            inputString += input[i].ToString();
        }

        while(inputString.Length < digits)
        {
            inputString += "_";
        }

        inputText.text = inputString;

        //Play button audio
        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.keypadButtons[newInt], Player.Instance, Player.Instance.currentNode, parentWindow.passedInteractable.controller.transform.position);

        if(input.Count >= digits)
        {
            SubmitCode();
        }
    }

    public void OnKeypadButtonDown()
    {
        //Play button audio
        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.keypadPress, Player.Instance, Player.Instance.currentNode, parentWindow.passedInteractable.controller.transform.position);
    }

    public void ClearCode(bool press = true)
    {
        checkCounter = 0f;
        checking = false;
        correct = false;
        inputText.color = defaultTextColour;

        input.Clear();
        inputText.text = string.Empty;

        for (int i = 0; i < digits; i++)
        {
            inputText.text += "_";
        }

        //Play button audio
        if(press)
        {
            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.keypadClear, Player.Instance, Player.Instance.currentNode, InteractionController.Instance.currentLookingAtInteractable.interactable.spawnedObject.transform.position);
        }
    }

    //Called when 4 characters are submitted
    public void SubmitCode()
    {
        if(!isTelephone)
        {
            //Is this correct?
            Interactable inter = InteractionController.Instance.currentLookingAtInteractable.interactable;

            List<string> notePlacements = null;
            List<int> password = inter.GetPasswordFromSource(out notePlacements);

            Game.Log("Password contains " + password.Count + " digits...");

            for (int i = 0; i < input.Count; i++)
            {
                if(password.Count > i && input[i] != password[i])
                {
                    //Wrong!
                    if (password.Count >= 4) Game.Log("Wrong code: Correct code is " + password[0] + password[1] + password[2] + password[3]);
                    else Game.Log("Invalid passcode length: " + password.Count);

                    if((Game.Instance.devMode && Game.Instance.collectDebugData))
                    {
                        Game.Log("Displaying note placements...");

                        if(notePlacements != null)
                        {
                            foreach (string str in notePlacements)
                            {
                                Game.Log(str);
                            }
                        }
                    }

                    //Play sound here as it's not tied to an action
                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.keypadDeny, Player.Instance, Player.Instance.currentNode, InteractionController.Instance.currentLookingAtInteractable.interactable.spawnedObject.transform.position);

                    inputText.color = Color.red;
                    correct = false;
                    checking = true;
                    return;
                }
            }

            //Unarm the lock here so it plays the sfx
            InteractionController.Instance.currentLookingAtInteractable.interactable.thisDoor.SetLockedState(false, Player.Instance);
        }
        else
        {
            if(parentWindow.passedInteractable != null && parentWindow.passedInteractable.preset.isPayphone)
            {
                if(GameplayController.Instance.money >= 1)
                {
                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.payphoneMoneyIn, Player.Instance, Player.Instance.currentNode, parentWindow.passedInteractable.GetWorldPosition());
                    GameplayController.Instance.AddMoney(-1, true, "payphone");
                }
                else
                {
                    InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "not_enough_money"), InterfaceControls.Icon.money, null, colourOverride: true, col: InterfaceControls.Instance.messageRed, ping: GameMessageController.PingOnComplete.money);
                    inputText.color = Color.red;
                    correct = false;
                    checking = true;
                    return;
                }
            }

            string code = string.Empty;

            for (int i = 0; i < input.Count; i++)
            {
                code += input[i].ToString();
            }

            int numCode = 0;
            int.TryParse(code, out numCode);

            //Check for valid phone number...
            //Dev cheat: Phone someone at home...
            if (numCode == 0000451)
            {
                NewAddress ad = CityData.Instance.addressDirectory.Find(item => item.currentOccupants.Count > 0 && item.telephones.Count > 0 && item.residence != null);

                if (ad != null)
                {
                    input.Clear();
                    string num = ad.telephones[0].number.ToString();

                    for (int i = 0; i < num.Length; i++)
                    {
                        int ch = 0;
                        int.TryParse(num[i].ToString(), out ch);
                        input.Add(ch);
                    }

                }
            }
            else if (!CityData.Instance.phoneDictionary.ContainsKey(numCode) && !TelephoneController.Instance.fakeTelephoneDictionary.ContainsKey(numCode))
            {
                //Play sound here as it's not tied to an action
                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.keypadDeny, Player.Instance, Player.Instance.currentNode, InteractionController.Instance.currentLookingAtInteractable.interactable.spawnedObject.transform.position);

                inputText.color = Color.red;
                correct = false;
                checking = true;
                return;
            }
        }

        //Set text to green
        inputText.color = Color.green;
        correct = true;
        checking = true;

        if(!isTelephone)
        {
            Interactable inter = parentWindow.passedInteractable;
            inter.AddPasswordSourceToAcquired();
        }
        else
        {
            //Enter dialog mode
            InteractionController.Instance.SetDialog(true, parentWindow.passedInteractable);
        }
    }

    //Detect input
    private void Update()
    {
        if(!InterfaceController.Instance.playerTextInputActive)
        {
            if (InputController.Instance.player.GetButtonDown("0"))
            {
                PressNumberButton(0);
            }
            else if (InputController.Instance.player.GetButtonDown("1"))
            {
                PressNumberButton(1);
            }
            else if (InputController.Instance.player.GetButtonDown("2"))
            {
                PressNumberButton(2);
            }
            else if (InputController.Instance.player.GetButtonDown("3"))
            {
                PressNumberButton(3);
            }
            else if (InputController.Instance.player.GetButtonDown("4"))
            {
                PressNumberButton(4);
            }
            else if (InputController.Instance.player.GetButtonDown("5"))
            {
                PressNumberButton(5);
            }
            else if (InputController.Instance.player.GetButtonDown("6"))
            {
                PressNumberButton(6);
            }
            else if (InputController.Instance.player.GetButtonDown("7"))
            {
                PressNumberButton(7);
            }
            else if (InputController.Instance.player.GetButtonDown("8"))
            {
                PressNumberButton(8);
            }
            else if (InputController.Instance.player.GetButtonDown("9"))
            {
                PressNumberButton(9);
            }
        }

        if(checking)
        {
            checkCounter += Time.deltaTime;

            if(checkCounter >= 0.3f)
            {
                //Close this evidence
                if(correct)
                {
                    if(isTelephone)
                    {
                        string code = string.Empty;

                        for (int i = 0; i < input.Count; i++)
                        {
                            code += input[i].ToString();
                        }

                        int numCode = 0;
                        int.TryParse(code, out numCode);

                        if(CityData.Instance.phoneDictionary.ContainsKey(numCode))
                        {
                            TelephoneController.Instance.CreateNewCall(parentWindow.passedInteractable.t, CityData.Instance.phoneDictionary[numCode], Player.Instance, null, new TelephoneController.CallSource(TelephoneController.CallType.player, CitizenControls.Instance.telephoneGreeting), maxRingTime: 99f);
                        }
                        else if(TelephoneController.Instance.fakeTelephoneDictionary.ContainsKey(numCode))
                        {
                            TelephoneController.Instance.CreateNewCall(parentWindow.passedInteractable.t, null, Player.Instance, null, TelephoneController.Instance.fakeTelephoneDictionary[numCode], maxRingTime: 99f);
                        }

                        InteractionController.Instance.RefreshDialogOptions();
                    }
                    else
                    {
                        InteractablePreset.InteractionAction act = InteractionController.Instance.currentLookingAtInteractable.interactable.thisDoor.preset.GetActions().Find(item => item.effectSwitchStates.Exists(item2 => item2.switchState == InteractablePreset.Switch.switchState && item2.boolIs)); //Find the open action
                        InteractionController.Instance.currentLookingAtInteractable.interactable.thisDoor.OnInteraction(act, Player.Instance); //Trigger open
                    }

                    parentWindow.CloseWindow();
                }
                else
                {
                    ClearCode(false);
                }

                checkCounter = 0f;
                checking = false;
                correct = false;
            }
        }
    }

    public void OnInputCode(List<int> code)
    {
        if(!inputCodeActive)
        {
            Game.Log("Interface: Inputting code with " + code.Count + " digits...");
            StartCoroutine(InputCode(code));
        }
    }

    IEnumerator InputCode(List<int> code)
    {
        inputCodeActive = true;
        int codeCursor = 0;

        while(code != null && codeCursor < digits && codeCursor < code.Count)
        {
            if(!checking)
            {
                int inputDigit = 0;

                inputDigit = code[codeCursor];
                Game.Log(inputDigit);

                try
                {
                    OnKeypadButtonDown();
                    PressNumberButton(inputDigit);
                }
                catch
                {
                    Game.LogError("Error inputting code!");
                }

                codeCursor++;
            }

            yield return new WaitForSeconds(0.15f);
        }

        inputCodeActive = false;
    }
}
