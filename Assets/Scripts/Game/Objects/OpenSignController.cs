﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenSignController : SwitchSyncBehaviour
{
    public MeshRenderer signRend;
    public Material onMat;
    public Material offMat;

    public override void SetOn(bool val)
    {
        base.SetOn(val);

        if(!isOn)
        {
            signRend.sharedMaterial = onMat;
        }
        else
        {
            signRend.sharedMaterial = offMat;
        }
    }
}
