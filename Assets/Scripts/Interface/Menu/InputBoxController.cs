﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InputBoxController : MonoBehaviour
{
    [Header("Components")]
    public TMP_InputField input;
    public ButtonController editButton;

    public void OnButtonSelect()
    {
        input.interactable = true;
        input.ActivateInputField();
    }
}
