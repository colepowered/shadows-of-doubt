﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "particleeffect_data", menuName = "Database/Particle Effect")]

public class ParticleEffect : SoCustomComparison
{
	[Header("Breakage")]
	[Tooltip("The relative velocity this has to be travelling at on collision to break")]
	public float damageBreakPoint = 0.1f;
	[Tooltip("Deletes the object completely")]
	public bool deleteObject = true;

	[Header("VFX")]
	public GameObject effectPrefab;

	[Header("Shatter")]
	[EnableIf("deleteObject")]
	public bool shatter = true;
	[Tooltip("The size of the shards created")]
	public Vector3 shardSize = new Vector3(0.025f, 0.025f, 0.025f);
	[Tooltip("Create a shard every this amount of pixels on the texture")]
	public int shardEveryXPixels = 64;
	public float shatterForceMultiplier = 2.4f;
	[Tooltip("Use a glass shard material")]
	public bool isGlass = false;

	[Header("Spatter")]
	public SpatterTrigger spatterTrigger = SpatterTrigger.off;
	public SpatterPatternPreset spatter;
	public float countMultiplier = 1f;
	public bool stickToActors = true;
	public bool spatterIsVandalism = false;
	[EnableIf("spatterIsVandalism")]
	public int vandalismFine = 20;

	[Header("Object Creation")]
	public SpatterTrigger creationTrigger = SpatterTrigger.off;
	public List<GameObject> objectPool = new List<GameObject>();
	public int instances = 1;
	public bool useRandomRotation = false;
	public Vector3 localEuler = Vector3.zero;

	public enum SpatterTrigger { off, onBreak, onAnyImpact, whileInAirOrAnyImpact };

	[Header("Audio")]
	public List<AudioEvent> impactEvents = new List<AudioEvent>();
	public List<AudioEvent> breakEvents = new List<AudioEvent>();
}
