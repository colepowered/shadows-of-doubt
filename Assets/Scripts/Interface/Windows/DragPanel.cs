﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

//Controls dragging of a window.
//Script pass 1
public class DragPanel : MonoBehaviour, IPointerDownHandler, IEndDragHandler, IDragHandler, IBeginDragHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
	//References
	public Vector2 pointerOffset;
    public RectTransform parentRect;
    public InfoWindow parentWindow;

	//Act as a draggable component
	public bool draggableComponent = false;
	public string dragTag = string.Empty;
	public bool isDragging = false;

    //Click Settings
    private float lastLeftClick = 0f;
    private float lastRightClick = 0f;

    //Events
    public delegate void DragEnd(GameObject dragObj, string tag);
    public event DragEnd OnDragEnd;

    //Raycast targets to enable/disable
   	private List<Image> rayTargets = new List<Image>();

    void Start()
    {
        if (parentWindow == null)
        {
            parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        }

		//Collect all images with raycast targets
		Transform parentTrans = this.gameObject.transform.parent;
		Image[] images = parentTrans.GetComponentsInChildren<Image>();
		foreach(Image i in images)
		{
			if (i.raycastTarget)
			{
				rayTargets.Add(i);
			}
		}
    }

    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        InterfaceController.Instance.AddMouseOverElement(this);
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;
        InterfaceController.Instance.RemoveMouseOverElement(this);
    }

    void OnDestroy()
    {
        InterfaceController.Instance.RemoveMouseOverElement(this);
    }

    public virtual void OnPointerDown(PointerEventData data)
	{
        parentRect.SetAsLastSibling();
        RectTransformUtility.ScreenPointToLocalPointInRectangle (parentRect, data.position, data.pressEventCamera, out pointerOffset);
	}

	public virtual void OnBeginDrag(PointerEventData data)
	{
        if (draggableComponent)
		{
			EndDrag();

			//New Drag
			StartCoroutine("Drag", data);
		}
	}

    public virtual void OnEndDrag(PointerEventData data)
    {
        InterfaceController.Instance.RemoveMouseOverElement(this);
    }

    public virtual void OnDrag (PointerEventData data)
	{
        if (InterfaceController.Instance.desktopMode)
        {
            if (parentRect == null) return;

            InterfaceController.Instance.AddMouseOverElement(this);

            Vector2 pointerPostion = ClampToWindow(data);

            Vector2 localPointerPosition;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
                InterfaceControls.Instance.hudCanvasRect, pointerPostion, data.pressEventCamera, out localPointerPosition
            ))
            {
                //Convert to anchored
                if (parentWindow != null)
                {
                    Vector2 anchored = new Vector2((localPointerPosition.x - pointerOffset.x) + InterfaceControls.Instance.hudCanvasRect.rect.width * 0.5f, (localPointerPosition.y - pointerOffset.y) - InterfaceControls.Instance.hudCanvasRect.rect.height * 0.5f);
                    parentWindow.SetAnchoredPosition(anchored);
                }
                else
                {
                    Vector2 anchored = new Vector2((localPointerPosition.x - pointerOffset.x), (localPointerPosition.y - pointerOffset.y));
                    parentRect.anchoredPosition = anchored;
                }
            }
        }
	}

	//Seperate methods are needed for dragging onto input fields, as otherwise the object is destroyed/nullified before it can be registered
	IEnumerator Drag(PointerEventData data)
	{
        if(InterfaceController.Instance.desktopMode)
        {
            isDragging = true;

            InterfaceController.Instance.AddMouseOverElement(this);

            //Set the dragging object
            GameObject draggingObj = this.gameObject;

            Vector2 pointerOut = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRect, Input.mousePosition, null, out pointerOut);

            InterfaceController.Instance.SetDragged(draggingObj, dragTag, pointerOut);

            foreach (Image im in rayTargets)
            {
                try
                {
                    im.raycastTarget = false;
                }
                catch
                {

                }
            }

            while (Input.GetMouseButton(0))
            {
                yield return null;
            }
        }

		EndDrag();
	}

	public virtual void EndDrag()
	{
		//Fire Event   	
	    if(OnDragEnd != null)
		OnDragEnd(InterfaceController.Instance.dragged, InterfaceController.Instance.draggedTag);

        InterfaceController.Instance.RemoveMouseOverElement(this);

        InterfaceController.Instance.SetDragged(null, "", Vector2.zero);
		StopCoroutine("Drag");
		isDragging = false;

		foreach(Image im in rayTargets)
		{
            if (im == null) continue;
			im.raycastTarget = true;
		}
	}

	public Vector2 ClampToWindow (PointerEventData data)
	{
		Vector2 rawPointerPosition = data.position;

		Vector3[] canvasCorners = new Vector3[4];
        InterfaceControls.Instance.hudCanvasRect.GetWorldCorners (canvasCorners);

		float clampedX = Mathf.Clamp (rawPointerPosition.x, canvasCorners[0].x, canvasCorners[2].x);
		float clampedY = Mathf.Clamp (rawPointerPosition.y, canvasCorners[0].y, canvasCorners[2].y);

		Vector2 newPointerPosition = new Vector2 (clampedX, clampedY);
		return newPointerPosition;
	}

    public virtual void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if ((Time.time - lastLeftClick) <= InterfaceControls.Instance.doubleClickDelay)
            {
                OnLeftClick();
                OnLeftDoubleClick();
                //Game.Log("Double Left Click: " + name);
            }
            else
            {
                OnLeftClick();
                //Game.Log("Left Click: " + name);
            }

            lastLeftClick = Time.time; // save the current time
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            if ((Time.time - lastRightClick) <= InterfaceControls.Instance.doubleClickDelay)
            {
                OnRightClick();
                OnRightDoubleClick();
                //Game.Log("Double Right Click: " + name);
            }
            else
            {
                OnRightClick();
                //Game.Log("Right Click: " + name);
            }

            lastRightClick = Time.time; // save the current time
        }
    }

    public virtual void OnLeftClick()
    {
        return;
    }

    public virtual void OnRightClick()
    {
        return;
    }

    public virtual void OnLeftDoubleClick()
    {
        if(parentWindow != null)
        {
            parentWindow.Rename();
        }
    }

    public virtual void OnRightDoubleClick()
    {
        return;
    }
}