﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.PlayerLoop;

public class EvidenceFootprint : Evidence
{
    //bool ownerFound = false;

    //Constructor
    public EvidenceFootprint(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {

    }

    public override void OnDiscovery()
    {
        base.OnDiscovery();

        Game.Log("Evidence: Footprint creating 'found at' evidence...");
        EvidenceCreator.Instance.CreateFact("FoundAt", this, parent, forceDiscoveryOnCreate: true);
    }

    //Override data sources: This needs it's own, not the owners like in EvidenceItem
    public override void BuildDataSources()
    {
        //base.BuildDataSources();
        UpdateSummary();
    }

    public void UpdateSummary()
    {
        //Refresh window content to update summary text
        List<InfoWindow> windows = InterfaceController.Instance.activeWindows.FindAll(item => item.passedEvidence == this);

        foreach(InfoWindow w in windows)
        {
            foreach(WindowTabController t in w.tabs)
            {
                if(w.passedEvidence != null && t.preset.contentType == WindowTabPreset.TabContentType.generated)
                {
                    t.content.LoadContent();
                }
            }
        }
    }

    //Override name variants
    public override string GetNameForDataKey(List<DataKey> inputKeys)
    {
        string ret = string.Empty;

        try
        {
            if (inputKeys == null) return Strings.Get("evidence.names", preset.name);

            //Return custom names first
            if (customNames.Count > 0)
            {
                List<DataKey> keys = GetTiedKeys(inputKeys);

                foreach (CustomName n in customNames)
                {
                    if (keys.Contains(n.key))
                    {
                        return n.name;
                    }
                }
            }

            ret = Strings.Get("evidence.names", preset.name) + " (" + Strings.Get("evidence.generic", interactable.belongsTo.descriptors.footwear.ToString(), Strings.Casing.firstLetterCaptial) + ", " + Strings.Get("evidence.generic", "size") + " " + interactable.belongsTo.descriptors.shoeSize +")";

            return ret;
        }
        catch
        {
            return ret;
        }
    }
}
