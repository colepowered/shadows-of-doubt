﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using NaughtyAttributes;
using TMPro;

public class ButtonController : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    [Header("References")]
    [Tooltip("Reference to the rect transform component (assigned if not given)")]
    public RectTransform rect;
    [Tooltip("Reference to the button component (assigned if not given)")]
    public Button button;
    [Tooltip("Reference to the canvas renderer component (assigned if not given)")]
    public CanvasRenderer rend;
    [Tooltip("The background image for this button")]
    public Image background;
    [Tooltip("The icon image for this button")]
    public Image icon;
    [Tooltip("Text for this button")]
    public TextMeshProUGUI text;
    [Tooltip("Tooltip control component")]
    public TooltipController tooltip;
    [Tooltip("Juice controller for effects")]
    public JuiceController juice;
    [Tooltip("Used for notifications")]
    public NotificationController notifications;
    [System.NonSerialized]
    public object genericReference; //Used for misc references

    [Space(5)]
    [ReadOnly]
    public InfoWindow parentWindow;
    [System.NonSerialized]
    public Evidence windowOf;
    [ReadOnly]
    public WindowTabController tabOf;
    [ReadOnly]
    public RectTransform additionalHighlightRect;

    [Space(5)]
    [Header("State")]
    [ReadOnly]
    [Tooltip("Is this currently moused-over?")]
    public bool isOver = false;
    [ReadOnly]
    [Tooltip("Force the additional/manual highlight")]
    public bool forceAdditionalHighlighted = false;
    [ReadOnly]
    [Tooltip("Addition/manual highlight active")]
    public bool additionalHighlighted = false;
    [ReadOnly]
    [Tooltip("Interactable")]
    public bool interactable = true;

    public bool setupReferences = false;

    //Click Settings
    private float lastLeftClick = 0f;
    private float lastRightClick = 0f;

    [Space(7)]
    [BoxGroup("Button Setup")]
    [Tooltip("Base colour used for built-in flash functionality")]
    public Color baseColour = Color.white;
    [BoxGroup("Button Setup")]
    [Tooltip("If enabled, this will detect if it's in a scroll rect, then adjust scroll accordingly when selected")]
    public bool scrollRectAutoScroll = true;
    [BoxGroup("Button Setup")]
    [EnableIf("scrollRectAutoScroll")]
    public bool scrollVertical = true;
    [BoxGroup("Button Setup")]
    [EnableIf("scrollRectAutoScroll")]
    public bool scrollHorizontal = false;

    [BoxGroup("Auto Navigation")]
    [Tooltip("Can this button be found using automatic navigation of other buttons?")]
    public bool findableForAutoNavigation = true;
    [BoxGroup("Auto Navigation")]
    [Tooltip("Automatically refresh the controller navigation when set up")]
    public bool refreshControllerNavigationOnSetup = false;
    //[BoxGroup("Auto Navigation")]
    //[Tooltip("Automatically refresh the controller navigation when enabled")]
    //public bool refreshControllerNavigationOnEnable = false;
    [BoxGroup("Auto Navigation")]
    [Tooltip("Automatically refresh the controller navigation when selected")]
    public bool refreshControllerNavigationOnSelect = false;
    [BoxGroup("Auto Navigation")]
    [Tooltip("Automatically refresh the controller navigation when pressed")]
    public bool refreshControllerNavigationOnPress = false;
    [BoxGroup("Auto Navigation")]
    [Tooltip("When looking for navigation selectables, include inactive objects")]
    public bool includeInactiveSelectables = false;
    [BoxGroup("Auto Navigation")]
    [Tooltip("When looking for navigation selectables, how many transform parents up should it search for other buttons?")]
    [Range(1, 10)]
    [DisableIf("isEvidenceWindowButton")]
    public int selectableSearchParentHierarchyThreshold = 1;
    [BoxGroup("Auto Navigation")]
    [Tooltip("A shortcut instead of above; search for components within this window...")]
    public bool isEvidenceWindowButton = false;
    [BoxGroup("Auto Navigation")]
    [Tooltip("When auto navigation is setup, should we allow a search for selectables on the left?")]
    public bool allowLeftNavigation = true;
    [BoxGroup("Auto Navigation")]
    [Tooltip("When auto navigation is setup, should we allow a search for selectables on the right?")]
    public bool allowRightNavigation = true;
    [BoxGroup("Auto Navigation")]
    [Tooltip("When auto navigation is setup, should we allow a search for selectables up?")]
    public bool allowUpNavigation = true;
    [BoxGroup("Auto Navigation")]
    [Tooltip("When auto navigation is setup, should we allow a search for selectables down?")]
    public bool allowDownNavigation = true;
    [BoxGroup("Auto Navigation")]
    [Tooltip("When measuring distances to to other nav buttons, use this point of the rect")]
    public NavRectPoint thisNavRectPoint = NavRectPoint.center;
    [BoxGroup("Auto Navigation")]
    [Tooltip("When measuring distances to to other nav buttons, use this point of the other rect")]
    public NavRectPoint otherNavRectPoint = NavRectPoint.center;
    [BoxGroup("Auto Navigation")]
    [Tooltip("When selecting from auto navigation, ignore the following objects if they are parents")]
    public List<string> ignoreParentsNamed = new List<string>();
    [BoxGroup("Auto Navigation")]
    [Tooltip("When using a controller, the secondary action button is classed as a right click")]
    public bool secondaryIsRightClick = false;

    public enum NavRectPoint { center, min, max };

    [Space(5)]
    [BoxGroup("Text")]
    [Tooltip("Automatically set the button text")]
    public bool useAutomaticText = false;
    [BoxGroup("Text")]
    [EnableIf("useAutomaticText")]
    [Tooltip("Use this dictionary reference to get the text")]
    public string textDictionary = "ui.interface";
    [BoxGroup("Text")]
    [EnableIf("useAutomaticText")]
    [Tooltip("Use this reference to get the text")]
    public string textReference;
    [BoxGroup("Text")]
    [EnableIf("useAutomaticText")]
    public Strings.Casing casing = Strings.Casing.asIs;
    [BoxGroup("Text")]
    public string menuMouseoverReference;

    [Space(5)]
    [BoxGroup("Interactability")]
    [Range(0f, 1f)]
    public float uninteractableTextAlpha = 0.25f;
    [BoxGroup("Interactability")]
    [Range(0f, 1f)]
    public float interactableTextAlpha = 1f;
    [BoxGroup("Interactability")]
    [Tooltip("If nothing is selected, the priority of this button being the defauly selection vs others")]
    [Range(-2, 2)]
    public int defaultSelectionPriority = 0;

    [Space(5)]
    [BoxGroup("Additional Highlighter")]
    [Tooltip("Use the additional/manual highlight functionality")]
    public bool useAdditionalHighlight = true;
    [BoxGroup("Additional Highlighter")]
    [Tooltip("Prefab that acts as the additional/manual highlighter. If empty a default highlighter will be used.")]
    public GameObject additionalHighlightPrefab;
    [BoxGroup("Additional Highlighter")]
    [Tooltip("Colour applied to the additional/manual highlighter")]
    public Color additionalHighlightColour = Color.white;
    [BoxGroup("Additional Highlighter")]
    [Tooltip("Colour applied to the additional/manual highlighter when uninteractable")]
    public Color additionalHighlightUninteractableColour = Color.gray;
    [BoxGroup("Additional Highlighter")]
    [Tooltip("Set the additional/manual highlight to the front")]
    public bool additionalHighlightAtFront = false;
    [BoxGroup("Additional Highlighter")]
    [Tooltip("Add to the rect size of the highlighter")]
    public Vector4 additionalHighlightRectModifier = Vector4.zero;

    private Image additionalHImage;

    [Space(5)]
    [BoxGroup("Juice")]
    public bool nudgeOnClick = true;
    [BoxGroup("Juice")]
    public bool glowOnHighlight = true;

    [Space(5)]
    [BoxGroup("Audio")]
    [Tooltip("Use generic button sounds")]
    public bool useGenericAudioSounds = true;

    public enum ButtonAudioType { normal, forward, back, tickBox};
    [BoxGroup("Audio")]
    [EnableIf("useGenericAudioSounds")]
    public ButtonAudioType buttonType = ButtonAudioType.normal;
    [BoxGroup("Audio")]
    [DisableIf("useGenericAudioSounds")]
    public AudioEvent buttonDown;
    [BoxGroup("Audio")]
    [DisableIf("useGenericAudioSounds")]
    public AudioEvent clickPrimary;
    [BoxGroup("Audio")]
    [DisableIf("useGenericAudioSounds")]
    public AudioEvent clickSecondary;
    [BoxGroup("Audio")]
    [DisableIf("useGenericAudioSounds")]
    public AudioEvent rightClick;

    public struct NavRanking
    {
        public ButtonController button;
        public float score;
        public int dir; //Direction; 0 = up, 1 = down, 2 = left, 3 = right
    }

    //Events
    public delegate void Press(ButtonController thisButton);
    public event Press OnPress;

    public delegate void HoverChange(ButtonController thisButton, bool mouseOver);
    public event HoverChange OnHoverChange;

    [Button("Setup References")]
    //Set rect, parent window etc.
    public virtual void SetupReferences()
    {
        if (setupReferences) return;
        if(rect == null) rect = this.gameObject.GetComponent<RectTransform>();
        if(button == null) button = this.gameObject.GetComponent<Button>();
        if (background == null) background = this.gameObject.GetComponent<Image>();
        if (rend == null) rend = this.gameObject.GetComponent<CanvasRenderer>();
        if (tooltip == null) tooltip = this.gameObject.GetComponent<TooltipController>();
        if (juice == null) juice = this.gameObject.GetComponent<JuiceController>();

        if(Application.isPlaying)
        {
            if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
            if (parentWindow != null) windowOf = parentWindow.passedEvidence;

            if (windowOf != null)
            {
                WindowContentController wcc = this.gameObject.GetComponentInParent<WindowContentController>();

                if (wcc != null)
                {
                    tabOf = wcc.tabController;
                }
            }

            //Listen to my own tooltip, set it just before it's about to spawn
            if (tooltip != null)
            {
                tooltip.OnBeforeTooltipSpawn += UpdateTooltipText;
            }

            //Execute OnPointerClick if the button is pressed...
            if(button != null)
            {
                //Turn off listening states- instead these are triggered through OnPointerClick
                int listeners = button.onClick.GetPersistentEventCount();

                for (int i = 0; i < listeners; i++)
                {
                    button.onClick.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.Off);
                }
            }

            UpdateButtonText();

            if (refreshControllerNavigationOnSetup && this.isActiveAndEnabled && !InputController.Instance.mouseInputMode) StartCoroutine(RefreshNavEndOfFrame());
        }

        setupReferences = true;
    }

    void Start()
    {
        if(!setupReferences) SetupReferences();
    }

    public virtual void VisualUpdate()
    {
        UpdateButtonText();
    }

    public virtual void UpdateButtonText()
    {
        if(useAutomaticText && text != null)
        {
            text.text = Strings.Get(textDictionary, textReference, casing);
        }
    }

    public virtual void UpdateTooltipText()
    {
        return;
    }

    public virtual void SetInteractable(bool val)
    {
        if(interactable != val)
        {
            interactable = val;

            if(button != null)
            {
                button.interactable = interactable;
            }

            if(!interactable)
            {
                if (glowOnHighlight && juice != null)
                {
                    juice.Pulsate(false);
                    background.color = baseColour;
                }

                //If this is selected, find another...
                if(InterfaceController.Instance.selectedElement == this)
                {
                    if(!InputController.Instance.mouseInputMode)
                    {
                        OnDeselect();
                    }
                }
            }

            if(text != null)
            {
                Color textCol = text.color;

                if(interactable)
                {
                    textCol.a = interactableTextAlpha;
                }
                else
                {
                    textCol.a = uninteractableTextAlpha;
                }

                text.color = textCol;
            }

            if (tooltip != null) tooltip.ForceClose();
            UpdateAdditionalHighlight();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(button == null) button = this.gameObject.GetComponent<Button>();
        if (button != null && !button.interactable) return;

        if (eventData.button == PointerEventData.InputButton.Left)
        {
            Game.Log("Menu: Button OnPointerClick: " + this.gameObject.name);

            //If this is NOT in mouse mode, this click has been called manually and therefore we need to execute the button's submission events manually...
            if(button != null)
            {
                //Turn on events and manually invoke them, then turn them off again
                int listeners = button.onClick.GetPersistentEventCount();

                for (int i = 0; i < listeners; i++)
                {
                    button.onClick.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
                }

                button.onClick.Invoke();

                for (int i = 0; i < listeners; i++)
                {
                    button.onClick.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.Off);
                }
            }

            if ((Time.time - lastLeftClick) <= InterfaceControls.Instance.doubleClickDelay)
            {
                OnLeftClick();
                OnLeftDoubleClick();
                //Game.Log("Double Left Click: " + name);
            }
            else
            {
                OnLeftClick();
                //Game.Log("Left Click: " + name);
            }

            if(nudgeOnClick && juice != null)
            {
                juice.Nudge();
            }

            if(useGenericAudioSounds)
            {
                if(buttonType == ButtonAudioType.normal)
                {
                    AudioController.Instance.Play2DSound(AudioControls.Instance.mainButton);
                }
                else if(buttonType == ButtonAudioType.forward)
                {
                    AudioController.Instance.Play2DSound(AudioControls.Instance.mainButtonForward);
                }
                else if(buttonType == ButtonAudioType.back)
                {
                    AudioController.Instance.Play2DSound(AudioControls.Instance.mainButtonBack);
                }
                else if(buttonType == ButtonAudioType.tickBox)
                {
                    AudioController.Instance.Play2DSound(AudioControls.Instance.tickbox);
                }
            }
            else
            {
                if (clickPrimary != null) AudioController.Instance.Play2DSound(clickPrimary);
                if (clickSecondary != null) AudioController.Instance.Play2DSound(clickSecondary);
            }

            lastLeftClick = Time.time; // save the current time

            ////If this is selected, find another...
            //if (!InputController.Instance.mouseInputMode)
            //{
            //    Game.Log("Deselect button " + this.gameObject.name + " through selection");
            //    OnDeselect();
            //}
        } 
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            if ((Time.time - lastRightClick) <= InterfaceControls.Instance.doubleClickDelay)
            {
                OnRightClick();
                OnRightDoubleClick();
                //Game.Log("Double Right Click: " + name);
            }
            else
            {
                OnRightClick();
                //Game.Log("Right Click: " + name);
            }

            if (rightClick != null) AudioController.Instance.Play2DSound(rightClick);

            lastRightClick = Time.time; // save the current time
        }    
    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        if (!useGenericAudioSounds)
        {
            if (buttonDown != null) AudioController.Instance.Play2DSound(buttonDown);
        }
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {

    }

    public virtual void OnLeftClick()
    {
        //Fire event
        if(OnPress != null)
        {
            OnPress(this);
        }

        if (refreshControllerNavigationOnPress && !InputController.Instance.mouseInputMode) StartCoroutine(RefreshNavEndOfFrame());

        return;
    }

    public virtual void OnRightClick()
    {
        if (refreshControllerNavigationOnPress && !InputController.Instance.mouseInputMode) StartCoroutine(RefreshNavEndOfFrame());
        return;
    }

    public virtual void OnLeftDoubleClick()
    {
        if (refreshControllerNavigationOnPress && !InputController.Instance.mouseInputMode) StartCoroutine(RefreshNavEndOfFrame());
        return;
    }

    public virtual void OnRightDoubleClick()
    {
        if (refreshControllerNavigationOnPress && !InputController.Instance.mouseInputMode) StartCoroutine(RefreshNavEndOfFrame());
        return;
    }

    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        //Game.Log("OnPointerEnter: " + this.gameObject.name);

        if (InterfaceController.Instance != null)
        {
            if (InputController.Instance.mouseInputMode)
            {
                InterfaceController.Instance.AddMouseOverElement(this);
                OnSelect();
            }
        }
    }

    public virtual void OnSelect()
    {
        if (InterfaceController.Instance.selectedElement == this) return;
        if (this == null || this.gameObject == null) return;
        Game.Log("Menu: OnSelect: " + this.gameObject.name);

        if (refreshControllerNavigationOnSelect) RefreshAutomaticNavigation();

        if (InterfaceController.Instance != null)
        {
            if (button != null)
            {
                button.Select();
            }

            InterfaceController.Instance.selectedElement = this;
            InterfaceController.Instance.selectedElementTag = this.tag;

            if (MainMenuController.Instance != null)
            {
                MainMenuController.Instance.OnNewMouseOver();
            }

            if(CasePanelController.Instance.controllerMode && CasePanelController.Instance.currentSelectMode == CasePanelController.ControllerSelectMode.topBar && transform.tag == "TopPanelButtons")
            {
                CasePanelController.Instance.selectedTopBarButton = this;
            }
        }

        //Fire event
        if (OnHoverChange != null)
        {
            OnHoverChange(this, true);
        }

        isOver = true;
        OnHoverStart();

        if (useAdditionalHighlight)
        {
            UpdateAdditionalHighlight();
        }

        if (glowOnHighlight && juice != null && interactable)
        {
            juice.Pulsate(true);
        }

        if(scrollRectAutoScroll && !InputController.Instance.mouseInputMode)
        {
            AutoScroll();
        }

        //Activate tooltip
        if(tooltip != null)
        {
            tooltip.OnButtonHover();
        }
    }

    //Centre the scroll rect on this
    public void AutoScroll()
    {
        CustomScrollRect scrollRect = this.gameObject.GetComponentInParent<CustomScrollRect>();

        if (scrollRect != null)
        {
            //Toolbox.Instance.ScrollScrollRect(scrollRect, this.transform.position, scrollHorizontal, scrollVertical, 0f);
            Toolbox.Instance.ScrollRectPosition(scrollRect, rect, scrollHorizontal, scrollVertical);
        }
    }

    void OnEnable()
    {
        if(button != null) button.interactable = interactable;
        if(InputController.Instance != null && !InputController.Instance.mouseInputMode) StartCoroutine(RefreshNavEndOfFrame());
    }

    void OnDisable()
    {
        if (button != null) button.interactable = false;

        if(tooltip != null)
        {
            if(tooltip.isOver)
            {
                tooltip.OnButtonExitHover();
            }
        }

        if (InterfaceController.Instance != null)
        {
            if (InputController.Instance.mouseInputMode)
            {
                InterfaceController.Instance.RemoveMouseOverElement(this);
            }
        }
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        if (InterfaceController.Instance != null)
        {
            if (InputController.Instance.mouseInputMode)
            {
                if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;

                InterfaceController.Instance.RemoveMouseOverElement(this);
                OnDeselect();
            }
        }
    }

    public virtual void OnDeselect()
    {
        //Game.Log("OnDeselect: " + this.gameObject.name);

        if (InterfaceController.Instance != null)
        {
            if(InterfaceController.Instance.selectedElement == this)
            {
                InterfaceController.Instance.selectedElement = null;
                InterfaceController.Instance.selectedElementTag = string.Empty;
            }

            if (MainMenuController.Instance != null)
            {
                MainMenuController.Instance.OnNewMouseOver();
            }
        }

        //Fire event
        if (OnHoverChange != null)
        {
            OnHoverChange(this, false);
        }

        isOver = false;
        OnHoverEnd();

        if (useAdditionalHighlight)
        {
            UpdateAdditionalHighlight();
        }

        if (glowOnHighlight && juice != null)
        {
            juice.Pulsate(false);
            background.color = baseColour;
        }

        //Deactivate tooltip
        if (tooltip != null)
        {
            tooltip.OnButtonExitHover();
        }
    }

    public virtual void OnHoverStart()
    {
        return;
    }

    public virtual void OnHoverEnd()
    {
        return;
    }

    public virtual void SetButtonBaseColour(Color col)
    {
        baseColour = col;

        if (button == null) return;
        button.image.color = col;

        if (juice != null)
        {
            foreach (JuiceController.JuiceElement j in juice.elements)
            {
                if (j.imageElement != null)
                {
                    j.originalColour = j.imageElement.color;
                }
                else if (j.rawImageElement != null)
                {
                    j.originalColour = j.rawImageElement.color;
                }
            }
        }
    }

    public void SetupAdditionalHighlight()
    {
        //If additional highlight is null, use the default
        if (additionalHighlightPrefab == null)
        {
            additionalHighlightPrefab = PrefabControls.Instance.buttonAdditionalHighlight;
        }

        if (rect == null) rect = this.gameObject.GetComponent<RectTransform>();

        GameObject newObj = Instantiate(additionalHighlightPrefab, rect);
        additionalHighlightRect = newObj.GetComponent<RectTransform>();
        additionalHImage = newObj.GetComponent<Image>();
        additionalHImage.color = additionalHighlightColour;

        Toolbox.Instance.SetRectSize(additionalHighlightRect, additionalHighlightRectModifier.x, additionalHighlightRectModifier.y, additionalHighlightRectModifier.z, additionalHighlightRectModifier.w);

        if(!additionalHighlightAtFront)
        {
            additionalHighlightRect.SetAsFirstSibling();
        }
        else
        {
            additionalHighlightRect.SetAsLastSibling();
        }
        
        additionalHighlightRect.gameObject.SetActive(false);

        UpdateAdditionalHighlight();
    }

    public virtual void UpdateAdditionalHighlight()
    {
        if (button == null) button = this.gameObject.GetComponent<Button>();
        if (additionalHighlightRect == null) SetupAdditionalHighlight();

        bool newBool = false;

        //Highlight from mouse over?
        if(useAdditionalHighlight && isOver)
        {
            newBool = true;
        }

        //Forced highlight
        if(forceAdditionalHighlighted)
        {
            newBool = true;
        }
        else if(!forceAdditionalHighlighted && !useAdditionalHighlight)
        {
            newBool = false;
        }

        if(!additionalHighlighted && newBool)
        {
            additionalHighlighted = true;
            additionalHighlightRect.gameObject.SetActive(true);
        }
        else if (additionalHighlighted && !newBool)
        {
            additionalHighlighted = false;
            additionalHighlightRect.gameObject.SetActive(false);
        }

        //Set colour
        if (interactable)
        {
            additionalHImage.color = additionalHighlightColour;
        }
        else
        {
            additionalHImage.color = additionalHighlightUninteractableColour;
        }
    }

    public void SetForceAdditionalHighlight(bool newVal)
    {
        forceAdditionalHighlighted = newVal;
        UpdateAdditionalHighlight();
    }

    //Flash
    public void Flash(int repeat, Color flashColour)
    {
        StartCoroutine(FlashColour(repeat, flashColour));
    }

    public IEnumerator FlashColour(int repeat, Color flashColour)
    {
        int cycle = 0;
        float progress = 0f;
        float flashF = 0f;
        float speed = 10f;

        while (cycle < repeat && progress < 2f)
        {
            progress += speed * Time.deltaTime;

            if (progress <= 1f) flashF = progress;
            else flashF = 2f - progress;

            background.color = Color.Lerp(baseColour, flashColour, flashF);

            if (progress >= 2f)
            {
                cycle++;
                progress = 0f;
            }

            yield return null;
        }

        //Return to original colors
        background.color = baseColour;
    }

    public IEnumerator RefreshNavEndOfFrame()
    {
        bool waited = false;

        while(!waited)
        {
            waited = true;
            yield return null; //Wait a frame; this might help this be more correct for reasons I'm not really sure of
            //yield return new WaitForEndOfFrame();
        }

        RefreshAutomaticNavigation();
    }

    void OnDestroy()
    {
        try
        {
            if (additionalHighlightRect != null)
            {
                Destroy(additionalHighlightRect.gameObject);
            }

            InterfaceController.Instance.RemoveMouseOverElement(this);
        }
        catch
        {
            //Don't know how this could be causing a null exception??
        }
    }

    //Refreshes this button's automatic navigation by manually finding the nearest buttons and setting the mode to explicit
    public void RefreshAutomaticNavigation()
    {
        RefreshAutomaticNavigation(allowLeftNavigation, allowRightNavigation, allowUpNavigation, allowDownNavigation, includeInactiveSelectables);
    }

    public void RefreshAutomaticNavigation(bool enableLeft, bool enableRight, bool enableUp, bool enableDown, bool includeInactive)
    {
        if (InputController.Instance.mouseInputMode) return; //Don't bother doing this if using mouse & keyboard

        allowLeftNavigation = enableLeft;
        allowRightNavigation = enableRight;
        allowUpNavigation = enableUp;
        allowDownNavigation = enableDown;
        includeInactiveSelectables = includeInactive;

        if(button != null)
        {
            //Game.Log("Refreshing auto navigation for " + name);

            Selectable r = button.navigation.selectOnRight;
            Selectable l = button.navigation.selectOnLeft;
            Selectable u = button.navigation.selectOnUp;
            Selectable d = button.navigation.selectOnDown;

            if(!includeInactiveSelectables)
            {
                if (r !=null && !r.gameObject.activeInHierarchy) r = null;
                if (l != null && !l.gameObject.activeInHierarchy) l = null;
                if (u != null && !u.gameObject.activeInHierarchy) u = null;
                if (d != null && !d.gameObject.activeInHierarchy) d = null;
            }

            //This has to be custom code as the default button function include inactive objects
            Transform searchParent = this.transform;

            //With this shortcut, search until you hit the window class
            if(isEvidenceWindowButton)
            {
                InfoWindow win = this.gameObject.GetComponentInParent<InfoWindow>();

                if(win != null)
                {
                    searchParent = win.transform;
                }
                else
                {
                    for (int i = 0; i < selectableSearchParentHierarchyThreshold; i++)
                    {
                        if (searchParent.parent != null) searchParent = searchParent.parent;
                    }
                }
            }
            else
            {
                for (int i = 0; i < selectableSearchParentHierarchyThreshold; i++)
                {
                    if (searchParent.parent != null) searchParent = searchParent.parent;
                }
            }

            ButtonController[] foundButtons = searchParent.GetComponentsInChildren<ButtonController>(includeInactiveSelectables);

            Vector3 screenPointThis = rect.TransformPoint(rect.rect.center);

            if(thisNavRectPoint == NavRectPoint.min) screenPointThis = rect.TransformPoint(rect.rect.min);
            else if (thisNavRectPoint == NavRectPoint.max) screenPointThis = rect.TransformPoint(rect.rect.max);

            Game.Log("Found " + foundButtons.Length + " in " + searchParent.name + " (screen point: " + screenPointThis + ")");

            //Give biases to directions
            Vector3 xBias = new Vector3(0.5f, 1f, 1f);
            Vector3 yBias = new Vector3(1f, 0.5f, 1f);

            List<NavRanking> navRanks = new List<NavRanking>();

            foreach(ButtonController bc in foundButtons)
            {
                if (!bc.findableForAutoNavigation) continue;
                if (bc.button == null) continue;
                if (!includeInactiveSelectables && !bc.gameObject.activeInHierarchy) continue;
                if (bc == this) continue;

                //Check the graphic raycaster is enabled
                GraphicRaycaster grR = bc.GetComponentInParent<GraphicRaycaster>();

                if (grR != null)
                {
                    if (!grR.enabled) continue;
                }
                else continue;

                //If we are ignoring certain objects, trace the parent/child hierarcy back and scan for names...
                if(ignoreParentsNamed.Count > 0)
                {
                    Transform scanParent = bc.transform;

                    bool foundIgnore = false;

                    for (int i = 0; i < 100; i++)
                    {
                        if (scanParent != null)
                        {
                            foreach (string str in ignoreParentsNamed)
                            {
                                if (scanParent.name.ToLower() == str.ToLower())
                                {
                                    foundIgnore = true;
                                    break;
                                }
                            }

                            if (scanParent.parent != null)
                            {
                                scanParent = scanParent.parent;
                            }
                            else break;
                        }
                        else break;

                        if (foundIgnore) break;
                    }

                    if (foundIgnore) continue;
                }

                Vector3 screenPoint = bc.rect.TransformPoint(bc.rect.rect.center);

                if (otherNavRectPoint == NavRectPoint.min) screenPoint = bc.rect.TransformPoint(bc.rect.rect.min);
                else if (otherNavRectPoint == NavRectPoint.max) screenPoint = bc.rect.TransformPoint(bc.rect.rect.max);

                if (allowLeftNavigation)
                {
                    if(screenPoint.x < screenPointThis.x - 6f)
                    {
                        float dist = Vector3.Distance(new Vector3(screenPoint.x * xBias.x, screenPoint.y * xBias.y, screenPoint.z * xBias.z), new Vector3(screenPointThis.x * xBias.x, screenPointThis.y * xBias.y, screenPointThis.z * xBias.z));
                        navRanks.Add(new NavRanking { button = bc, score = dist, dir = 2 });
                    }
                }

                if (allowRightNavigation)
                {
                    if (screenPoint.x > screenPointThis.x + 6f)
                    {
                        float dist = Vector3.Distance(new Vector3(screenPoint.x * xBias.x, screenPoint.y * xBias.y, screenPoint.z * xBias.z), new Vector3(screenPointThis.x * xBias.x, screenPointThis.y * xBias.y, screenPointThis.z * xBias.z));
                        navRanks.Add(new NavRanking { button = bc, score = dist, dir = 3 });
                    }
                }

                if (allowUpNavigation)
                {
                    if (screenPoint.y > screenPointThis.y + 6f)
                    {
                        float dist = Vector3.Distance(new Vector3(screenPoint.x * yBias.x, screenPoint.y * yBias.y, screenPoint.z * yBias.z), new Vector3(screenPointThis.x * yBias.x, screenPointThis.y * yBias.y, screenPointThis.z * yBias.z));
                        navRanks.Add(new NavRanking { button = bc, score = dist, dir = 0 });
                    }
                }

                if (allowDownNavigation)
                {
                    if (screenPoint.y < screenPointThis.y - 6f)
                    {
                        float dist = Vector3.Distance(new Vector3(screenPoint.x * yBias.x, screenPoint.y * yBias.y, screenPoint.z * yBias.z), new Vector3(screenPointThis.x * yBias.x, screenPointThis.y * yBias.y, screenPointThis.z * yBias.z));
                        navRanks.Add(new NavRanking { button = bc, score = dist, dir = 1 });
                    }
                }
            }

            //Sort by scores
            navRanks.Sort((p1, p2) => p1.score.CompareTo(p2.score)); //Lowest first

            for (int i = 0; i < 4; i++)
            {
                for (int y = 0; y < navRanks.Count; y++)
                {
                    NavRanking n = navRanks[y];

                    //Found the best direction score for this
                    if (n.dir == i)
                    {
                        if (i == 0) u = n.button.button;
                        else if (i == 1) d = n.button.button;
                        else if (i == 2) l = n.button.button;
                        else if (i == 3) r = n.button.button;

                        Game.Log("Selected " + n.button.name + " for " + i);

                        //Remove other references to this direction or button
                        for (int z = 0; z < navRanks.Count; z++)
                        {
                            if(navRanks[z].dir == i || navRanks[z].button == n.button)
                            {
                                navRanks.RemoveAt(z);
                                z--;
                            }
                        }

                        break;
                    }
                }
            }


            button.navigation = new Navigation { mode = Navigation.Mode.Explicit, selectOnLeft = l, selectOnRight = r, selectOnUp = u, selectOnDown = d };
        }
    }
}
