﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "newspaper_data", menuName = "Database/Newspaper Article")]

public class NewspaperArticle : SoCustomComparison
{
	[Header("Debug")]
	public bool disabled = false;

	[Header("Setup")]
	public string ddsReference;
	public Category category = Category.general;
	[Tooltip("The next generated newspaper will try to feature one of the following")]
	public List<NewspaperArticle> followupStories = new List<NewspaperArticle>();
	public List<Sprite> possibleImages = new List<Sprite>();
	public ContextSource context = ContextSource.nothing;

	public enum Category { general, murder, ad, foreignAffairs, murderSecond };
	public enum ContextSource { nothing, lastMurder, player, randomCitizen, randomCriminal, randomGroup };
}
