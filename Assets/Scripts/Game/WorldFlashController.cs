﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldFlashController : MonoBehaviour
{
    public InteractableController controller;
    public MeshRenderer rend;
    public Material offMaterial;
    public Material onMaterial;

    public float speed = 10f;

    public bool flashActive = false;
    private int repeat = 0;

    //Flash
    public void Flash(int newRepeat)
    {
        //Game.Log("World flash");
        if (!this.isActiveAndEnabled) return;

        //If already active, add repeats to current coroutine
        if (flashActive)
        {
            repeat += newRepeat;
        }
        else
        {
            StartCoroutine("FlashColour", newRepeat);
        }
    }

    public IEnumerator FlashColour(int newRepeat)
    {
        flashActive = true;

        repeat = newRepeat;
        int cycle = 0;
        float progress = 0f;

        while (cycle < repeat && progress < 2f)
        {
            progress += speed * Time.deltaTime;

            if(progress < 1f)
            {
                rend.material = onMaterial;
            }
            else
            {
                rend.material = offMaterial;
            }

            if(progress >= 2f)
            {
                cycle++;
                progress = 0f;
            }

            yield return null;
        }

        if(controller.interactable.locked && controller.interactable.preset.lockOnMaterial != null)
        {
            rend.material = controller.interactable.preset.lockOnMaterial;
        }
        else if (!controller.interactable.locked && controller.interactable.preset.lockOffMaterial != null)
        {
            rend.material = controller.interactable.preset.lockOffMaterial;
        }

        flashActive = false;
    }

    private void OnDisable()
    {
        if(flashActive)
        {
            StopCoroutine("FlashColour");
            flashActive = false;
            repeat = 0;

            if (controller.interactable.locked && controller.interactable.preset.lockOnMaterial != null)
            {
                rend.material = controller.interactable.preset.lockOnMaterial;
            }
            else if (!controller.interactable.locked && controller.interactable.preset.lockOffMaterial != null)
            {
                rend.material = controller.interactable.preset.lockOffMaterial;
            }
        }
    }
}
