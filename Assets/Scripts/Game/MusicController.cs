﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using NaughtyAttributes;

public class MusicController : MonoBehaviour
{
    [System.NonSerialized]
    public List<MusicCue> cues;

    [Header("Settings")]
    public bool enableMusic = true;

    [Header("Seconds Between Tracks")]
    public Vector2 silenceBetweenTracks = new Vector2(120f, 360f);

    [Header("State")]
    [InfoBox("Current valid cues that the game will select from")]
    public List<MusicCue> currentValidCues = new List<MusicCue>();
    [InfoBox("List of tracks that have already been played and are marked to play only once")]
    public List<MusicCue> playedOnceTracks = new List<MusicCue>();
    public bool isPlaying = false;
    public float nextTrackTriggerTime = 0f;

    [Space(7)]
    public MusicCue.MusicTriggerGameState currentGameState = MusicCue.MusicTriggerGameState.any;
    public MusicCue.MusicTriggerPlayerState currentPlayerSate = MusicCue.MusicTriggerPlayerState.any;
    public MusicCue.MusicTriggerPlayerLocation currentPlayerLocation = MusicCue.MusicTriggerPlayerLocation.any;

    [Space(7)]
    [InfoBox("Used to determine priorities when avoiding repeating tracks")]
    public List<MusicCue> previousTracks = new List<MusicCue>();

    private Dictionary<MusicCue, FMOD.Studio.EventInstance> activeTracks = new Dictionary<MusicCue, FMOD.Studio.EventInstance>();
    public List<MusicCue> activeCuePresets = new List<MusicCue>();

    //Singleton pattern
    private static MusicController _instance;
    public static MusicController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        //string musicBusString = "Bus:/Music";
        //musicBus = FMODUnity.RuntimeManager.GetBus(musicBusString);
        //musicBus.setVolume(desiredBusFaderLevel);

        //Load cue data
        cues = AssetLoader.Instance.GetAllMusicCues();

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

    public void SetGameState(MusicCue.MusicTriggerGameState newGameState)
    {
        if(currentGameState != newGameState)
        {
            Game.Log("Audio: Music: Switch game state to " + newGameState);
            currentGameState = newGameState;
            MusicTriggerCheck();
        }
    }

    public void SetPlayerState(MusicCue.MusicTriggerPlayerState newPlayerState)
    {
        if (currentPlayerSate != newPlayerState)
        {
            Game.Log("Audio: Music: Switch player state to " + newPlayerState);
            currentPlayerSate = newPlayerState;
            MusicTriggerCheck();
        }
    }

    public void SetPlayerLocation(MusicCue.MusicTriggerPlayerLocation newPlayerLocation)
    {
        if (currentPlayerLocation != newPlayerLocation)
        {
            Game.Log("Audio: Music: Switch player location to " + newPlayerLocation);
            currentPlayerLocation = newPlayerLocation;
            MusicTriggerCheck();
        }
    }

    public void MusicTriggerCheck(MusicCue.MusicTriggerEvent passEvent = MusicCue.MusicTriggerEvent.none)
    {
        if(Game.Instance != null) Game.Log("Audio: Music: Executing music check...");
        currentValidCues = new List<MusicCue>();

        foreach(MusicCue cue in cues)
        {
            if (cue.disabled) continue;
            if (cue.playOnce && playedOnceTracks.Contains(cue)) continue;

            //Each trigger here can count as a chance to play...
            foreach(MusicCue.MusicTrigger trigger in cue.triggers)
            {
                if (cue.debug && Game.Instance.collectDebugData) Game.Log("Audio: Checking trigger for " + cue.presetName + "...");

                if (IsTriggerValid(trigger, passEvent, cue.debug))
                {
                    if(!currentValidCues.Contains(cue)) currentValidCues.Add(cue);
                }
            }
        }

        //Sort valid by priority; include randomness and position on previously played list
        currentValidCues.Sort((p1, p2) => (p2.
            ambientPriority + Toolbox.Instance.Rand(-0.05f, 0.05f) + GetPreviouslyPlayedBias(p2) * 0.5f)
            .CompareTo(
            (p1.ambientPriority + Toolbox.Instance.Rand(-0.05f, 0.05f) + GetPreviouslyPlayedBias(p1) * 0.5f)
            )); //Highest first with shuffle variation for equal values

        //Is the current track still compatible, and if not, is the option set to stop the track?
        List<MusicCue> toRemove = new List<MusicCue>();

        foreach (KeyValuePair<MusicCue, FMOD.Studio.EventInstance> pair in activeTracks)
        {
            FMOD.Studio.PLAYBACK_STATE playback = FMOD.Studio.PLAYBACK_STATE.STOPPED;
            pair.Value.getPlaybackState(out playback);

            Game.Log("Audio: Playback state of " + pair.Key + " is " + playback.ToString());

            if (playback == FMOD.Studio.PLAYBACK_STATE.PLAYING || playback == FMOD.Studio.PLAYBACK_STATE.STARTING || playback == FMOD.Studio.PLAYBACK_STATE.SUSTAINING)
            {
                if (pair.Key.stopOnIncompatibleStateSwitch && !currentValidCues.Contains(pair.Key))
                {
                    Game.Log("Audio: " + pair.Key.name + " is incompatible, stopping...");

                    pair.Value.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                    pair.Value.release();
                    toRemove.Add(pair.Key);
                    isPlaying = false;
                }
            }
        }

        foreach (MusicCue rem in toRemove)
        {
            activeTracks.Remove(rem);
            activeCuePresets.Remove(rem);
        }

        for (int i = 0; i < currentValidCues.Count; i++)
        {
            MusicCue c = currentValidCues[i];

            if(isPlaying)
            {
                if (!c.interrupt) continue;
            }

            PlayNewTrack(c, true);
            break;
        }
    }

    //Test conditions of a trigger class
    public bool IsTriggerValid(MusicCue.MusicTrigger trigger, MusicCue.MusicTriggerEvent passEvent, bool debug)
    {
        if (passEvent != MusicCue.MusicTriggerEvent.none)
        {
            if (passEvent == trigger.onEvent)
            {
                //Event matches!
                if (Toolbox.Instance.Rand(0f, 1f) > trigger.eventTriggerChance)
                {
                    if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Event chance failed");
                    return false;
                }
            }
            else
            {
                if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Music cue not valid: Wrong event");
                return false;
            }
        }
        else if (trigger.triggerOnlyOnEvents)
        {
            if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Cue will only trigger on events");
            return false;
        }

        if(!trigger.ignoreSilentTimeBetweenTracks)
        {
            if (nextTrackTriggerTime > 0f)
            {
                if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Waiting for delay between tracks");
                return false;
            }
        }

        if(trigger.onGameState != MusicCue.MusicTriggerGameState.any)
        {
            if (trigger.onGameState != currentGameState)
            {
                if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Game state " + currentGameState.ToString() + " does not match " + trigger.onGameState.ToString());
                return false;
            }
        }

        if (trigger.onPlayerSate != MusicCue.MusicTriggerPlayerState.any)
        {
            if (trigger.onPlayerSate != currentPlayerSate)
            {
                if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Player state " + currentPlayerSate.ToString() + " does not match " + trigger.onPlayerSate.ToString());
                return false;
            }
        }

        if (trigger.onPlayerLocation != MusicCue.MusicTriggerPlayerLocation.any)
        {
            if (trigger.onPlayerLocation != currentPlayerLocation)
            {
                if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Player location state " + currentPlayerLocation.ToString() + " does not match " + trigger.onPlayerLocation.ToString());
                return false;
            }
        }

        if(trigger.onlyInDistricts)
        {
            if (Player.Instance.currentGameLocation == null || Player.Instance.currentGameLocation.district == null || !trigger.compatibleDistricts.Contains(Player.Instance.currentGameLocation.district.preset))
            {
                if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Invalid district");
                return false;
            }
        }
        else if (trigger.excludeDistricts)
        {
            if (Player.Instance.currentGameLocation != null && Player.Instance.currentGameLocation.district != null && trigger.excludedDistricts.Contains(Player.Instance.currentGameLocation.district.preset))
            {
                if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Invalid district");
                return false;
            }
        }

        if (trigger.onlyInBuildings)
        {
            if (Player.Instance.currentGameLocation == null || Player.Instance.currentGameLocation.building == null || !trigger.compatibleBuildings.Contains(Player.Instance.currentGameLocation.building.preset))
            {
                if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Invalid building");
                return false;
            }
        }
        else if (trigger.excludeBuildings)
        {
            if (Player.Instance.currentGameLocation != null && Player.Instance.currentGameLocation.building != null && trigger.excludedBuildings.Contains(Player.Instance.currentGameLocation.building.preset))
            {
                if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Invalid building");
                return false;
            }
        }

        if (trigger.onlyInLocations)
        {
            if (Player.Instance.currentGameLocation == null || Player.Instance.currentGameLocation.thisAsAddress == null || Player.Instance.currentGameLocation.thisAsAddress.addressPreset == null || !trigger.compatibleAddressTypes.Contains(Player.Instance.currentGameLocation.thisAsAddress.addressPreset))
            {
                if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Invalid location");
                return false;
            }
        }
        else if (trigger.excludeLocations)
        {
            if (Player.Instance.currentGameLocation != null && Player.Instance.currentGameLocation.thisAsAddress != null && Player.Instance.currentGameLocation.thisAsAddress.addressPreset != null && trigger.excludedAddressTypes.Contains(Player.Instance.currentGameLocation.thisAsAddress.addressPreset))
            {
                if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Invalid location");
                return false;
            }
        }

        if (trigger.onlyDuringStatuses)
        {
            bool statusPass = false;

            foreach(StatusPreset sp in StatusController.Instance.activeStatuses)
            {
                if(trigger.compatibleStatuses.Contains(sp))
                {
                    statusPass = true;
                    break;
                }
            }

            if (!statusPass)
            {
                if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Invalid status");
                return false;
            }
        }
        else if (trigger.excludeStatuses)
        {
            bool statusPass = true;

            foreach (StatusPreset sp in StatusController.Instance.activeStatuses)
            {
                if (trigger.compatibleStatuses.Contains(sp))
                {
                    statusPass = false;
                    break;
                }
            }

            if (!statusPass)
            {
                if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Invalid status");
                return false;
            }
        }

        if(trigger.floorRanges.Count > 0)
        {
            if(Player.Instance.currentNode != null)
            {
                bool floorPass = false;

                foreach(Vector2 v2 in trigger.floorRanges)
                {
                    if (Player.Instance.currentNode.nodeCoord.z >= Mathf.RoundToInt(v2.x) && Player.Instance.currentNode.nodeCoord.z <= Mathf.RoundToInt(v2.y))
                    {
                        floorPass = true;
                        break;
                    }
                }

                if (!floorPass)
                {
                    if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Invalid floor range");
                    return false;
                }
            }
        }

        if(trigger.timeRanges.Count > 0)
        {
            bool timePass = false;

            foreach(Vector2 v2 in trigger.timeRanges)
            {
                if (SessionData.Instance.decimalClock >= v2.x && SessionData.Instance.decimalClock <= v2.y)
                {
                    timePass = true;
                    break;
                }
            }

            if (!timePass)
            {
                if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Invalid time range");
                return false;
            }
        }

        if(trigger.useDecorGrimeRange)
        {
            if(Player.Instance.currentRoom != null && !Player.Instance.currentRoom.isNullRoom && Player.Instance.currentRoom.wallMat != null)
            {
                if(Player.Instance.currentRoom.defaultWallKey.grubiness < trigger.grimeRange.x || Player.Instance.currentRoom.defaultWallKey.grubiness > trigger.grimeRange.y)
                {
                    if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue not valid: Invalid grime range");
                    return false;
                }
            }
        }

        if (debug && Game.Instance.collectDebugData) Game.Log("Audio: Music cue is valid");
        return true;
    }

    //Get a value from 0 - 1 with 1 being not played before or played a while ago on the previously played list...
    private float GetPreviouslyPlayedBias(MusicCue cue)
    {
        float ret = 1;

        if(previousTracks.Contains(cue))
        {
            int index = previousTracks.IndexOf(cue);
            if(index > 0) ret = 1f - ((float)index / (float)(previousTracks.Count - 1));
        }

        return ret;
    }

    //Play a new track, will crossfade with an existing one if needed.
    public void PlayNewTrack(MusicCue newTrack, bool interupt = false)
    {
        //Don't do this is music is disabled
        if (!enableMusic)
        {
            return;
        }

        nextTrackTriggerTime = Toolbox.Instance.Rand(silenceBetweenTracks.x, silenceBetweenTracks.y);

        //Interupt 
        if(interupt)
        {
            StopCurrentTrack();
        }

        FMOD.GUID fmodEvent = FMOD.GUID.Parse(newTrack.fmodGUID);

        //Setup event instance
        FMOD.Studio.EventInstance music = FMODUnity.RuntimeManager.CreateInstance(fmodEvent);

        //Play
        music.start();
        activeTracks.Add(newTrack, music);
        activeCuePresets.Add(newTrack);

        //Add to previously played list
        previousTracks.Remove(newTrack);
        previousTracks.Add(newTrack);

        Game.Log("Audio: Play new ambient track: " + newTrack.name);

        //If set to only play once, remove from reference dictionary
        if(newTrack.playOnce)
        {
            playedOnceTracks.Add(newTrack);
        }
    }

    private void Update()
    {
        //Don't do this is music is disabled
        if(!enableMusic)
        {
            return;
        }

        //Check if there's anything playing...
        isPlaying = false;

        List<MusicCue> toRemove = new List<MusicCue>();

        foreach (KeyValuePair<MusicCue, FMOD.Studio.EventInstance> pair in activeTracks)
        {
            FMOD.Studio.EventInstance active = pair.Value;

            FMOD.Studio.PLAYBACK_STATE playback = FMOD.Studio.PLAYBACK_STATE.STOPPED;
            active.getPlaybackState(out playback);

            if (playback == FMOD.Studio.PLAYBACK_STATE.STOPPED)
            {
                pair.Value.release();
                toRemove.Add(pair.Key);
            }
            else
            {
                isPlaying = true;
                break;
            }
        }

        foreach (MusicCue rem in toRemove)
        {
            activeTracks.Remove(rem);
            activeCuePresets.Remove(rem);
        }

        //Pause between ambient tracks
        if (!isPlaying)
        {
            if(nextTrackTriggerTime > 0f)
            {
                //If we're not playing anything, count time between tracks
                nextTrackTriggerTime -= Time.deltaTime;
            }
            else
            {
                MusicTriggerCheck();
            }
        }
    }

    [Button]
    public void StopCurrentTrack()
    {
        List<MusicCue> toRemove = new List<MusicCue>();

        foreach(KeyValuePair<MusicCue, FMOD.Studio.EventInstance> pair in activeTracks)
        {
            pair.Value.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            pair.Value.release();
            toRemove.Add(pair.Key);
        }

        foreach(MusicCue rem in toRemove)
        {
            activeTracks.Remove(rem);
            activeCuePresets.Remove(rem);
        }
    }

    [Button]
    public void ForceNextTrack()
    {
        nextTrackTriggerTime = 0f;
    }
}
