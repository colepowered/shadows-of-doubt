﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ComputerOSUIComponent : MonoBehaviour
{
    public Button button;
    public AudioEvent sfx;

    public virtual void OnLeftClick()
    {
        Game.Log("Player: Computer clicked on " + name);

        if (button != null) button.onClick.Invoke();
    }
}
