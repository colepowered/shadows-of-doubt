﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;
using UnityEngine.UI;
using UnityEditor;
using NaughtyAttributes;

public class ChapterIntro : Chapter
{
    [Header("Character")]
    //public Human partner; //Player's fictional partner
    public Human noteWriter;
    public Human kidnapper;
    public Human killer;
    public Human slophouseOwner;

    [Header("Presim")]
    //Luckily, this doesn't need to be saved...
    GroupsController.SocialGroup meetGroup;
    NewAIGoal meetFoodNotewriter;
    NewAIGoal meetFoodKidnapper;
    NewAIGoal postNote;
    NewAIGoal kidnapperGoHome;
    NewAIGoal kidnapperRunAway;
    NewAIGoal returnToApartment;
    MurderController.Murder murder;
    MurderController.Murder murder2;
    bool handlePreSim = false;
    bool murderPreSimPass = false;
    public int preSimPhase = 0;

    //End diner
    TelephoneController.PhoneCall lemCall = null;
    public float timeSinceCallObjective = 0f;

    [Header("Saved variables")]
    public int noteWriterID = -1;
    public int kidnapperID = -1;
    public int killerID = -1;
    public int playersAparment = -1;
    public int eatery = -1;
    public int slopHouseOwnerID = -1;
    public int slopHouseID = -1;
    public int addressBookID = -1;

    public float meetTime = -1f;
    //public int kidnapperSeat = -1;
    public bool enforcerEventsTrigger = false;
    public bool findNotewriter = false;
    public bool notewriterDialogAdded = false;
    public bool lastCallPlaced = false;
    public float notewriterMurderTimer = -1f;
    public bool notewriterManualMurderTrigger = false;
    public bool notewriterMurderTriggered = false;
    public bool receiptSearchPromt = false;
    public bool addressBookSearchPrompt = false;
    public bool fingerprintPrompt = false;
    public float receiptSearchTimer = 0f;
    public float addressBookSearchTimer = 0f;
    public float printSearchTimer = 0f;
    public bool receiptSearchActivated = false;
    public bool addressBookSearchActivated = false;
    public bool printSearchActivated = false;
    public int killerBarID = -1;
    public int redGumMeetID = -1;
    public int chosenRouterAddressID = -1;
    public int weaponSellerID = -1;
    public bool discoveredWeaponsDealer = false;
    public bool completed = false;

    [Header("Locations")]
    public NewAddress apartment;
    NewRoom playerBedroom;
    NewRoom playerLounge;
    NewRoom playerKitchen;
    NewRoom kidnappersBedroom;
    NewRoom noteWritersBedroom;
    public NewAddress restaurant;
    NewRoom restaurantBackroom;
    public NewAddress killerBar;
    public NewAddress redGumMeet;
    public NewAddress chosenRouterAddress;
    public NewAddress weaponSeller;
    public NewAddress slophouse;

    [Header("Objects: Spawned")]
    Interactable note;
    Interactable key;

    Interactable detectiveStuff;
    //Interactable handcuffs;
    Interactable policeBadge;
    //Interactable lockpickKit;

    Interactable hairpin;
    Interactable paperclip;
    Interactable spareKeyDoormat;
    Interactable workID;
    Interactable safePasscode;

    Interactable rewardSyncDisk;

    [System.NonSerialized]
    public Interactable murderWeapon;

    Interactable kidnapperDiary;
    //Interactable suitcase;
    Interactable envelopeWithCredits;
    Interactable corpLetter;
    Interactable crumpledFlyer;
    Interactable printedVmail;
    Interactable meetingNote;
    Interactable noteOnNapkin;
    Interactable tornPhotograph;
    Interactable travelreceipt;
    List<Interactable> playerApartmentLockpicks = new List<Interactable>();
    [System.NonSerialized]
    public Evidence restaurantReceipt;
    public bool receiptInBin = false;
    Interactable noteWriterDiary;
    public Interactable playersStorageBox;
    //Interactable partnerEmploymentContract;
    //Interactable partnerConferenceReference;
    Interactable policeCertificate;
    Interactable fieldsAdvert;
    Interactable scientificPaper;
    //Interactable partnerBusinessCard;
    Interactable playersPasscodeReminder;
    //Interactable partnersPasscodeReminder;
    //Interactable playerAddressNote;
    Interactable killerPropaganda;
    Interactable killerNotewriterDetails;
    Interactable killerPoliceFines;
    Interactable killerBusinessCard;
    Interactable killerCorpSponsorship;
    Interactable killerBarTab;
    Interactable robItem;

    Interactable workplaceReceipt;
    Interactable workplaceMessageNote;

    Interactable dinerFlyer;

    Interactable finalNoticeBill;
    Interactable evictionNotice;
    Interactable flophouseWelcomeLetter;
    Interactable flophouseSyncDiskNote;
    Interactable flophouseJobNote;
    Interactable flophouseSyncDisk;

    [Header("Objects: Reference")]
    FurnitureLocation kidnappersSafe;
    FurnitureLocation bed;
    Interactable closestSleep;
    Interactable closestLight;
    NewNode.NodeAccess apartmentEntrance;
    NewNode interiorDoorNode;
    NewNode exteriorDoorNode;
    Interactable playerCalendar;
    Interactable cityDir;
    FurnitureLocation noteWritersBed;
    Interactable dinerCruncher;
    NewNode.NodeAccess kidnappersEntrance;
    NewNode kidnappersDoorNode;
    NewDoor kidnappersDoor;
    Interactable kidnappersCalendar;
    Interactable kidnappersAddressBook;
    Interactable kidnapperBin;
    Interactable kidnapperPhone;
    Interactable weaponsSalesLedger;
    Interactable kidnapperRouter;
    Interactable kidnapperRouterDoor;
    EvidenceTime meetingTimeEvidence;
    //Fact meetingTimeFact;
    [System.NonSerialized]
    public NewAIGoal layLowGoal;

    //UI Pointers
    private RectTransform pointer;
    private PulseGlowController glow;

    public class IntoCharacterPick
    {
        public Human noteWriter;
        public Human kidnapper;
        public float score = 0f;
    }

    //Misc references
    float nextLeadDelay = 0f;
    bool notewriterOnCam = false;
    bool kidnapperOnCam = false;
    public int lockpicksNeeded = 0;
    float endDelayTimer = 0f;
    float passcodeNoteTimer = 0f;
    bool triggeredPasscodeNoteHint = false;
    private float triggeredTutorialSkip = 0;
    //private float positionalUpdateTimer = 0;

    //Triggered on chapter load

    public override void OnLoaded()
    {
        base.OnLoaded();

        SetUpMission();
    }

    public override void OnGameStart()
    {
        //Pick usage node cloest to bedside light
        if (bed != null)
        {
            Interactable useInteractable = bed.integratedInteractables[Toolbox.Instance.Rand(0, bed.integratedInteractables.Count)];
            if (!loadedFromSave) Player.Instance.Teleport(useInteractable.node, null);
        }

        base.OnGameStart();
    }

    private void SetUpMission()
    {
        ControlsDisplayController.Instance.disableControlDisplay.Add(InteractablePreset.InteractionKey.flashlight);

        //Pick characters
        if (!loadedFromSave)
        {
            apartment = Player.Instance.home;
            playersAparment = apartment.id;

            PickCharacters();
        }
        //else GeneratePartner(); //Otherwise just generate the partner

        (noteWriter as Citizen).alwaysPassDialogSuccess = true;

        //Remove kidnapper's partner
        if (kidnapper.partner != null)
        {
            kidnapper.partner.RemoveFromWorld(true);
        }

        kidnapper.unreportable = true; //Stop kidnapper being found for now

        //Find locations
        playerLounge = apartment.rooms.Find(item => item.preset == InteriorControls.Instance.lounge);
        playerKitchen = apartment.rooms.Find(item => item.preset == InteriorControls.Instance.kitchen);

        //Teleport player to bed
        bed = Toolbox.Instance.FindFurnitureWithinGameLocation(apartment, InteriorControls.Instance.bed, out playerBedroom); //Bedroom is assigned on out
        Interactable useInteractable = null;

        //Pick usage node cloest to bedside light
        if (bed != null)
        {
            useInteractable = bed.integratedInteractables[Toolbox.Instance.Rand(0, bed.integratedInteractables.Count)];
            if(!loadedFromSave) Player.Instance.Teleport(useInteractable.node, null);
        }

        apartmentEntrance = apartment.entrances.Find(item => item.accessType == NewNode.NodeAccess.AccessType.door);

        interiorDoorNode = apartmentEntrance.fromNode;
        exteriorDoorNode = apartmentEntrance.toNode;

        if (apartmentEntrance.toNode.gameLocation == apartment)
        {
            interiorDoorNode = apartmentEntrance.toNode;
            exteriorDoorNode = apartmentEntrance.fromNode;
        }

        //Pick slophouse
        //Update for sale
        if(slophouse == null)
        {
            CitizenBehaviour.Instance.UpdateForSale();

            foreach(NewAddress r in GameplayController.Instance.forSale)
            {
                r.CalculateLandValue();

                if(slophouse == null || r.normalizedLandValue < slophouse.normalizedLandValue)
                {
                    slophouse = r;
                    slopHouseID = r.id;
                }
            }

            if (slophouse != null) Game.Log("Chapter: Picked new flophouse is at " + slophouse.name);
            else Game.Log("Chapter: Unable to find flophouse for story mission!");
        }

        //City dir
        if(useInteractable != null) cityDir = Toolbox.Instance.FindClosestObjectTo(InteriorControls.Instance.cityDirectory, useInteractable.GetWorldPosition(), null, apartment, null, out _);

        //Get note writer entrance details
        kidnappersEntrance = kidnapper.home.entrances.Find(item => item.accessType == NewNode.NodeAccess.AccessType.door);
        kidnappersDoorNode = kidnappersEntrance.toNode;
        if (kidnappersEntrance.toNode.gameLocation == kidnapper.home) kidnappersDoorNode = kidnappersEntrance.fromNode;
        kidnappersDoor = kidnappersEntrance.wall.door;

        Toolbox.Instance.FindFurnitureWithinGameLocation(kidnapper.home, InteriorControls.Instance.bed, out kidnappersBedroom);
        kidnappersSafe = Toolbox.Instance.FindFurnitureWithinGameLocation(kidnapper.home, InteriorControls.Instance.safe, out _);
        noteWritersBed = Toolbox.Instance.FindFurnitureWithinGameLocation(noteWriter.home, InteriorControls.Instance.bed, out noteWritersBedroom);

        //Is there a bin?
        NewRoom lookForBin = kidnapper.home.entrances[0].fromNode.room;
        NewRoom kidnapperKitchen = kidnapper.home.rooms.Find(item => item.preset == InteriorControls.Instance.kitchen);
        if (kidnapperKitchen != null) lookForBin = kidnapperKitchen;

         kidnapperBin = Toolbox.Instance.FindNearestWithAction(RoutineControls.Instance.disposal, lookForBin, kidnapper, AIActionPreset.FindSetting.homeOnly, enforcersAllowedEverywhere: false);
        if (kidnapperBin == null) Game.Log("Chapter: Unable to find kidnapper's bin");

        kidnapperPhone = Toolbox.Instance.FindNearestWithAction(RoutineControls.Instance.makeCall, kidnapper.home.entrances[0].fromNode.room, kidnapper, AIActionPreset.FindSetting.homeOnly, enforcersAllowedEverywhere: false);
        if (kidnapperPhone == null) Game.Log("Chapter: Unable to find kidnapper's phone");

        kidnapperRouter = Toolbox.Instance.FindClosestObjectTo(InteriorControls.Instance.telephoneRouter, kidnapper.home.entrances[0].fromNode.position, kidnapper.home.building, null, null, out _);
        if (kidnapperRouter == null) Game.Log("Chapter: Unable to find kidnapper's telephone router");

        if(kidnapperRouter != null)
        {
            kidnapperRouterDoor = Toolbox.Instance.FindClosestObjectTo(InteriorControls.Instance.telephoneRouterDoor, kidnapperRouter.GetWorldPosition(), kidnapper.home.building, null, kidnapperRouter.node.room, out _);
            if (kidnapperRouterDoor == null) Game.Log("Chapter: Unable to find kidnapper's telephone router door");
        }

        //Remove the default give key to apartment
        if (!loadedFromSave)
        {
            //Remove existsing keys in the player's apartment
            foreach (NewRoom room in apartment.rooms)
            {
                foreach (FurnitureLocation furn in room.individualFurniture)
                {
                    for (int i = 0; i < furn.spawnedInteractables.Count; i++)
                    {
                        Interactable subObj = furn.spawnedInteractables[i];

                        if(!loadedFromSave)
                        {
                            //Also remove all food as we'll be spawning more...
                            if (subObj.preset.name == "Key")
                            {
                                subObj.Delete();
                                i--;
                                continue;
                            }
                            //Remove address book
                            else if(subObj.preset.name == "AddressBook")
                            {
                                subObj.Delete();
                                i--;
                                continue;
                            }
                        }

                        //Find calendar
                        if (playerCalendar == null && subObj.preset.name == "Calendar")
                        {
                            playerCalendar = subObj;
                        }
                    }
                }
            }

            Player.Instance.RemoveFromKeyring(apartmentEntrance.door);

            //Remove clues from person other than the kidnapper
            foreach (NewRoom room in kidnapper.home.rooms)
            {
                foreach (FurnitureLocation furn in room.individualFurniture)
                {
                    for (int i = 0; i < furn.spawnedInteractables.Count; i++)
                    {
                        Interactable subObj = furn.spawnedInteractables[i];

                        if (!loadedFromSave)
                        {
                            //Remove everything not belonging to note writer that is spawned
                            if (subObj.belongsTo != null && subObj.belongsTo != kidnapper && subObj.preset.spawnable)
                            {
                                subObj.Delete();
                                i--;
                                continue;
                            }
                        }

                        //Remove notewriter's cruncher to stop player from finding vmails early...
                        if (subObj.preset.name == "MicroCruncher")
                        {
                            subObj.Delete();
                            i--;
                            continue;
                        }

                        //Also remove all food as we'll be spawning more...
                        //if (subObj.preset.spawnEvidence == GameplayControls.Instance.retailItemSoldDiscovery || subObj.preset.spawnEvidence == GameplayControls.Instance.retailItemNoSoldDiscovery)
                        //{
                        //    subObj.Remove();
                        //    i--;
                        //    continue;
                        //}

                        //Find work ID, food items
                        if (workID == null && subObj.preset.name == "WorkID")
                        {
                            workID = subObj;
                        }

                        //Find calendar
                        if (kidnappersCalendar == null && subObj.preset.name == "Calendar")
                        {
                            kidnappersCalendar = subObj;
                        }

                        //Find address book
                        if (kidnappersAddressBook == null && subObj.preset.name == "AddressBook")
                        {
                            kidnappersAddressBook = subObj;
                            addressBookID = kidnappersAddressBook.id;
                        }
                    }
                }

                //All lights are on
                if(!loadedFromSave)
                {
                    room.SetMainLights(true, "Chapter");

                    foreach (NewNode.NodeAccess acc in room.entrances)
                    {
                        if (acc.door != null)
                        {
                            if (!acc.door.isLocked)
                            {
                                acc.door.SetOpen(1f, null, true);
                            }
                        }
                    }
                }
            }

            if(restaurant == null)
            {
                float rank = -99999;

                foreach(Company c in CityData.Instance.companyDirectory)
                {
                    if(c.publicFacing)
                    {
                        if(c.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.caffeine))
                        {
                            if(c.preset.name == "AmericanDiner")
                            {
                                float thisRank = 1f;

                                if(noteWriter.favouritePlaces[CompanyPreset.CompanyCategory.caffeine] == c.address)
                                {
                                    thisRank++;
                                }

                                if (kidnapper.favouritePlaces[CompanyPreset.CompanyCategory.caffeine] == c.address)
                                {
                                    thisRank++;
                                }

                                if(thisRank > rank)
                                {
                                    rank = thisRank;
                                    restaurant = c.address;
                                    eatery = restaurant.id;
                                }
                            }
                        }
                    }
                }
            }

            //Add sales record to weapons dealer...
            List<InteractablePreset> weaponsPurchase = new List<InteractablePreset>();
            weaponsPurchase.Add(InteriorControls.Instance.handgun);
            weaponsPurchase.Add(InteriorControls.Instance.silencer);
            weaponsPurchase.Add(InteriorControls.Instance.ammo1);

            weaponSeller.company.AddSalesRecord(killer, weaponsPurchase, SessionData.Instance.gameTime - 12f);

            if (weaponsSalesLedger == null)
            {
                foreach (NewRoom r in weaponSeller.rooms)
                {
                    foreach(NewNode n in r.nodes)
                    {
                        if(weaponsSalesLedger == null) weaponsSalesLedger = n.interactables.Find(item => item.preset == InteriorControls.Instance.salesLedger);

                        if (weaponsSalesLedger != null)
                        {
                            Game.Log("Chapter: Found weapon seller sales ledger");
                            break;
                        }
                    }

                    if (weaponsSalesLedger != null)
                    {
                        break;
                    }
                }
            }
        }

        //restaurant = noteWriter.favouritePlaces[CompanyPreset.CompanyCategory.caffeine]; //It's important to the tutorial mission this is open 24 hours.

        //Get keycode lock
        if(restaurant != null)
        {
            foreach (NewRoom room in restaurant.rooms)
            {
                foreach (FurnitureLocation furn in room.individualFurniture)
                {
                    for (int i = 0; i < furn.spawnedInteractables.Count; i++)
                    {
                        Interactable subObj = furn.spawnedInteractables[i];

                        //Also remove all food as we'll be spawning more...
                        if (subObj.preset.name == "MicroCruncher")
                        {
                            dinerCruncher = subObj;
                            restaurantBackroom = room;
                        }
                    }
                }
            }

            if (dinerCruncher == null)
            {
                //Game.LogError("Chaptter: Cannot find sales records in restaurant rooms! Dumping debug data...");

                foreach (NewRoom room in restaurant.rooms)
                {
                    //Game.Log("Chapter: Searching in room " + room.name + "...");

                    foreach (FurnitureLocation furn in room.individualFurniture)
                    {
                        //Game.Log("Chapter: Searching in furniture " + furn.furniture.name + "... Created interactables?" + furn.createdInteractables);

                        for (int i = 0; i < furn.spawnedInteractables.Count; i++)
                        {
                            Interactable subObj = furn.spawnedInteractables[i];

                            //Game.Log("Chapter: Found interactable " + subObj.name + "...");

                            //Also remove all food as we'll be spawning more...
                            if (subObj.preset.name == "MicroCruncher")
                            {
                                dinerCruncher = subObj;
                                restaurantBackroom = room;
                            }
                        }
                    }
                }
            }
        }

        //Start player vmails (NOT saved)
        //Toolbox.Instance.NewVmailThread(Player.Instance, partner, null, null, null, "f23b5f7c-9d83-4680-be84-a364cff1f814", SessionData.Instance.gameTime - 24);

        //Create objects...
        OnObjectsCreated();

        //Execute presim
        if(!loadedFromSave)
        {
            ExecutePreSim();
        }
    }

    //Get chapter state save data
    public override StateSaveData.ChaperStateSave GetChapterSaveData()
    {
        StateSaveData.ChaperStateSave newData = new StateSaveData.ChaperStateSave();

        //Characters
        newData.AddData("notewriter", noteWriterID);
        newData.AddData("kidnapper", kidnapperID);
        newData.AddData("killer", killerID);
        newData.AddData("slophouseowner", slopHouseOwnerID);

        //Places
        newData.AddData("apartment", apartment.id);
        newData.AddData("killerbar", killerBarID);
        newData.AddData("redgummeet", redGumMeetID);
        newData.AddData("routeraddress", chosenRouterAddressID);
        newData.AddData("weaponseller", weaponSellerID);
        newData.AddData("restaurant", eatery);
        newData.AddData("slophouse", slopHouseID);
        newData.AddData("addressbook", addressBookID);

        //Objects
        if (murderWeapon != null) newData.AddData("murderweapon", murderWeapon.id);
        if (note != null) newData.AddData("note", note.id);
        if (key != null) newData.AddData("key", key.id);
        if (detectiveStuff != null) newData.AddData("printReader", detectiveStuff.id);
        //if (handcuffs != null) newData.AddData("handcuffs", handcuffs.id);
        if (policeBadge != null) newData.AddData("policebadge", policeBadge.id);
        //if (lockpickKit != null) newData.AddData("lockpickkit", lockpickKit.id);
        if (playersStorageBox != null) newData.AddData("box", playersStorageBox.id);
        if (hairpin != null) newData.AddData("hairpin", hairpin.id);
        if (paperclip != null) newData.AddData("paperclip", paperclip.id);

        if (robItem != null) newData.AddData("robItem", robItem.id);
        if (workID != null) newData.AddData("workID", workID.id);
        if (meetingNote != null) newData.AddData("meetingNote", meetingNote.id);
        if (noteOnNapkin != null) newData.AddData("noteOnNapkin", noteOnNapkin.id);
        if (dinerFlyer != null) newData.AddData("dinerFlyer", dinerFlyer.id);
        if (workplaceMessageNote != null) newData.AddData("workplaceMessageNote", workplaceMessageNote.id);
        if (workplaceReceipt != null) newData.AddData("workplaceReceipt", workplaceReceipt.id);
        //if (playerAddressNote != null) newData.AddData("playerAddressNote", playerAddressNote.id);
        if (rewardSyncDisk != null) newData.AddData("rewardDisk", rewardSyncDisk.id);

        if (finalNoticeBill != null) newData.AddData("finalNoticeBill", finalNoticeBill.id);
        if (evictionNotice != null) newData.AddData("evictionNotice", evictionNotice.id);
        if (flophouseWelcomeLetter != null) newData.AddData("flophouseWelcome", flophouseWelcomeLetter.id);
        if (flophouseSyncDiskNote != null) newData.AddData("flophouseSyncDiskNote", flophouseSyncDiskNote.id);
        if (flophouseJobNote != null) newData.AddData("flophouseJob", flophouseJobNote.id);
        if (flophouseSyncDisk != null) newData.AddData("flophouseSyncDisk", flophouseSyncDisk.id);

        //Flags
        newData.AddData("meettime", meetTime);
        //newData.AddData("kidnapperSeat", kidnapperSeat);
        newData.AddData("enforcerEventsTrigger", enforcerEventsTrigger);
        newData.AddData("lastCallPlaced", lastCallPlaced);
        newData.AddData("notewriterMurderTimer", notewriterMurderTimer);
        newData.AddData("notewriterManualMurderTrigger", notewriterManualMurderTrigger);
        newData.AddData("notewriterMurderTriggered", notewriterMurderTriggered);

        newData.AddData("findNoteWriter", findNotewriter);
        newData.AddData("notewriterDialogAdded", notewriterDialogAdded);

        newData.AddData("receiptSearchPromt", receiptSearchPromt);
        newData.AddData("fingerprintSearchPrompt", fingerprintPrompt);
        newData.AddData("addressBookSearchPromt", addressBookSearchPrompt);

        newData.AddData("receiptSearchTimer", receiptSearchTimer);
        newData.AddData("printsSearchTimer", printSearchTimer);
        newData.AddData("addressBookSearchTimer", addressBookSearchTimer);

        newData.AddData("receiptSearchActivated", receiptSearchActivated);
        newData.AddData("printsSearchActivated", printSearchActivated);
        newData.AddData("addressBookSearchActivated", addressBookSearchActivated);

        newData.AddData("discoveredWeaponsDealer", discoveredWeaponsDealer);
        newData.AddData("completed", completed);

        return newData;
    }

    public override void LoadStateSaveData(StateSaveData.ChaperStateSave newData)
    {
        //Characters
        noteWriterID = newData.GetDataInt("notewriter");
        kidnapperID = newData.GetDataInt("kidnapper");
        killerID = newData.GetDataInt("killer");
        slopHouseOwnerID = newData.GetDataInt("slophouseowner");

        CityData.Instance.GetHuman(noteWriterID, out noteWriter);
        CityData.Instance.GetHuman(kidnapperID, out kidnapper);
        CityData.Instance.GetHuman(killerID, out killer);
        CityData.Instance.GetHuman(slopHouseOwnerID, out slophouseOwner);

        //Places
        killerBarID = newData.GetDataInt("killerbar");
        redGumMeetID = newData.GetDataInt("redgummeet");
        chosenRouterAddressID = newData.GetDataInt("routeraddress");
        playersAparment = newData.GetDataInt("apartment");
        weaponSellerID = newData.GetDataInt("weaponseller");
        eatery = newData.GetDataInt("restaurant");
        slopHouseID = newData.GetDataInt("slophouse");
        addressBookID = newData.GetDataInt("addressbook");

        CityData.Instance.addressDictionary.TryGetValue(eatery, out restaurant);
        CityData.Instance.addressDictionary.TryGetValue(killerBarID, out killerBar);
        CityData.Instance.addressDictionary.TryGetValue(redGumMeetID, out redGumMeet);
        CityData.Instance.addressDictionary.TryGetValue(chosenRouterAddressID, out chosenRouterAddress);
        CityData.Instance.addressDictionary.TryGetValue(playersAparment, out apartment);
        CityData.Instance.addressDictionary.TryGetValue(weaponSellerID, out weaponSeller);
        CityData.Instance.addressDictionary.TryGetValue(slopHouseID, out slophouse);

        //Objects
        murderWeapon = LoadInteractableFromData("murderweapon", ref newData);
        note = LoadInteractableFromData("note", ref newData);
        key = LoadInteractableFromData("key", ref newData);
        detectiveStuff = LoadInteractableFromData("printReader", ref newData);
        //handcuffs = LoadInteractableFromData("handcuffs", ref newData);
        policeBadge = LoadInteractableFromData("policebadge", ref newData);
        //lockpickKit = LoadInteractableFromData("lockpickkit", ref newData);
        playersStorageBox = LoadInteractableFromData("box", ref newData);

        hairpin = LoadInteractableFromData("hairpin", ref newData);
        paperclip = LoadInteractableFromData("paperclip", ref newData);

        kidnappersAddressBook = LoadInteractableFromData("addressbook", ref newData);

        robItem = LoadInteractableFromData("robItem", ref newData);
        workID = LoadInteractableFromData("workID", ref newData);
        meetingNote = LoadInteractableFromData("meetingNote", ref newData);
        noteOnNapkin = LoadInteractableFromData("noteOnNapkin", ref newData);

        dinerFlyer = LoadInteractableFromData("dinerFlyer", ref newData);
        workplaceMessageNote = LoadInteractableFromData("workplaceMessageNote", ref newData);
        workplaceReceipt = LoadInteractableFromData("workplaceReceipt", ref newData);
        //playerAddressNote = LoadInteractableFromData("playerAddressNote", ref newData);
        rewardSyncDisk = LoadInteractableFromData("rewardDisk", ref newData);

        finalNoticeBill = LoadInteractableFromData("finalNoticeBill", ref newData);
        evictionNotice = LoadInteractableFromData("evictionNotice", ref newData);
        flophouseWelcomeLetter = LoadInteractableFromData("flophouseWelcome", ref newData);
        flophouseSyncDiskNote = LoadInteractableFromData("flophouseSyncDiskNote", ref newData);
        flophouseSyncDisk = LoadInteractableFromData("flophouseSyncDisk", ref newData);
        flophouseJobNote = LoadInteractableFromData("flophouseJob", ref newData);

        //Flags
        meetTime = newData.GetDataFloat("meettime");
        //kidnapperSeat = newData.GetDataInt("kidnapperSeat");

        string timeStr = SessionData.Instance.GameTimeToClock12String(meetTime, false);
        string dateStr = SessionData.Instance.ShortDateString(meetTime, true);

        meetingTimeEvidence = EvidenceCreator.Instance.GetTimeEvidence(meetTime, meetTime) as EvidenceTime;

        enforcerEventsTrigger = newData.GetDataBool("enforcerEventsTrigger");
        lastCallPlaced = newData.GetDataBool("lastCallPlaced");
        notewriterMurderTimer = newData.GetDataFloat("notewriterMurderTimer");
        notewriterManualMurderTrigger = newData.GetDataBool("notewriterManualMurderTrigger");
        notewriterMurderTriggered = newData.GetDataBool("notewriterMurderTriggered");
        receiptSearchPromt = newData.GetDataBool("receiptSearchPromt");
        fingerprintPrompt = newData.GetDataBool("fingerprintSearchPrompt");
        addressBookSearchPrompt = newData.GetDataBool("addressBookSearchPrompt");
        receiptSearchTimer = newData.GetDataFloat("receiptSearchTimer");
        receiptSearchActivated = newData.GetDataBool("receiptSearchActivated");
        printSearchTimer = newData.GetDataFloat("printsSearchTimer");
        printSearchActivated = newData.GetDataBool("printsSearchActivated");
        addressBookSearchTimer = newData.GetDataFloat("addressBookSearchTimer");
        addressBookSearchActivated = newData.GetDataBool("addressBookSearchActivated");

        findNotewriter = newData.GetDataBool("findNoteWriter");
        notewriterDialogAdded = newData.GetDataBool("notewriterDialogAdded");

        //Make sure the notewrite dialog is available
        if (findNotewriter || notewriterDialogAdded)
        {
            noteWriter.evidenceEntry.AddDialogOption(Evidence.DataKey.name, ChapterController.Instance.loadedChapter.dialogEvents[1], allowPresetDuplicates: false);
        }

        discoveredWeaponsDealer = newData.GetDataBool("discoveredWeaponsDealer");
        completed = newData.GetDataBool("completed");
    }

    //Triggered when creating objects
    public override void OnObjectsCreated()
    {
        base.OnObjectsCreated();

        if(!loadedFromSave)
        {
            //Create all objects needed for mission...
            SpawnPlayerApartmentClues();
            SpawnKidnapperClues();
            SpawnNotewriterClues();
            SpawnKillerClues();
            SpawnMiscClues();
        }
    }

    private void SpawnPlayerApartmentClues()
    {
        //Place a key in the player's apartment
        if (key == null)
        {
            key = apartment.PlaceObject(InteriorControls.Instance.keyTabletopOnly, Player.Instance, Player.Instance, null, out _, true, Interactable.PassedVarType.roomID, apartmentEntrance.door.passwordDoorsRoom.roomID);
        }

        //Handcuffs
        playersStorageBox = Toolbox.Instance.FindClosestObjectTo(InteriorControls.Instance.storageBox, Player.Instance.transform.position, null, apartment, null, out _);

        if(playersStorageBox == null)
        {
            playersStorageBox = apartment.PlaceObject(InteriorControls.Instance.storageBox, Player.Instance, Player.Instance, null, out _, false, ignoreLimits: true);
        }
        
        playersStorageBox.SetValue(0.07f); //Make it so we only need one or two lockpicks...
        //handcuffs = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.detectiveStuff, Player.Instance, Player.Instance, null, playersStorageBox.GetWorldPosition() + new Vector3(0.1f, 0, -0.05f), playersStorageBox.wEuler + new Vector3(0, 56f, 0), null, null);
        detectiveStuff = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.detectiveStuff, Player.Instance, Player.Instance, null, playersStorageBox.GetWorldPosition(false) + new Vector3(0f, 0.0315f, 0f), playersStorageBox.wEuler, null, null);
        Game.Log("Chapter: Detective stuff cr: " + detectiveStuff.cr + " WPOS: " + playersStorageBox.GetWorldPosition(false));
        
        //lockpickKit = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.lockpickKit, Player.Instance, Player.Instance, null, playersStorageBox.GetWorldPosition() + new Vector3(-0.087f, 0.037f, -0.215f), playersStorageBox.wEuler + new Vector3(0, 3.4f, 0), null, null);
        GameplayController.Instance.SetLockpicks(0);

        //Spawn police badge
        //policeBadge = apartment.PlaceObject(InteriorControls.Instance.policeBadge, Player.Instance, Player.Instance, null, out _, false, ignoreLimits: true);

        //Spawn some hairpins and paperclips...
        hairpin = apartment.PlaceObject(InteriorControls.Instance.hairpin, null, null, null, out _, false, ignoreLimits: true);
        paperclip = apartment.PlaceObject(InteriorControls.Instance.paperclip, null, null, null, out _, false, ignoreLimits: true);

        //Spawn extra
        apartment.PlaceObject(InteriorControls.Instance.paperclip, null, null, null, out _, false, ignoreLimits: true);
        apartment.PlaceObject(InteriorControls.Instance.hairpin, null, null, null, out _, false, ignoreLimits: true);

        //Spawn final bill
        finalNoticeBill = apartment.PlaceObject(InteriorControls.Instance.letter, Player.Instance, Player.Instance, null, out _, null, forcedSecurity: 0, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseNonOwned, ddsOverride: "ea869950-7590-4ca2-b472-9811dd583e57");

        //Partner employment contract OLD
        //partnerEmploymentContract = apartment.PlaceObject(InteriorControls.Instance.letter, Player.Instance, Player.Instance, null, out _, null, forcedSecurity: 1, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseNonOwned, ddsOverride: "26837641-bcc4-4051-99d5-59d4cdbec0b8");

        //Police certificate
        policeCertificate = apartment.PlaceObject(InteriorControls.Instance.document, Player.Instance, Player.Instance, null, out _, null, forcedSecurity: 3, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseOwned, ddsOverride: "581dcd40-f88f-4c44-b52c-4f354807e881");

        //Conference invitation OLD
        //partnerConferenceReference = apartment.PlaceObject(InteriorControls.Instance.letter, Player.Instance, Player.Instance, null, out _, null, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseNonOwned, ddsOverride: "66e32ac9-c3a4-4197-9052-f00c492d165a");

        //Fields advert
        fieldsAdvert = apartment.PlaceObject(InteriorControls.Instance.fieldsAd, Player.Instance, Player.Instance, null, out _, null);

        //Science paper
        scientificPaper = apartment.PlaceObject(InteriorControls.Instance.document, Player.Instance, Player.Instance, null, out _, null, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseNonOwned, ddsOverride: "e5e08419-8c20-4aa1-8939-a39c1cb3fc07");

        //Support group flyer
        apartment.PlaceObject(InteriorControls.Instance.policeSupportFlyer, Player.Instance, Player.Instance, null, out _, null, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseOwned);

        //Partner business card OLD
        //partnerBusinessCard = apartment.PlaceObject(InteriorControls.Instance.businessCard, Player.Instance, Player.Instance, null, out _, null, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseNonOwned, ddsOverride: "7673eaa5-9eb9-470a-9cfd-c78be1bf4e79");

        //Passcode reminders
        playersPasscodeReminder = apartment.PlaceObject(InteriorControls.Instance.note, Player.Instance, Player.Instance, null, out _, null, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseNonOwned, ddsOverride: "443a1a96-3309-493a-8137-24c9b427af90");

        //partnersPasscodeReminder = apartment.PlaceObject(InteriorControls.Instance.note, Player.Instance, Player.Instance, null, out _, null, ddsOverride: "3ed88278-3cbb-41ff-a8ed-219b6bb6951b");

        //Toothbrush and painkillers
        apartment.PlaceObject(InteriorControls.Instance.toothbrush, Player.Instance, Player.Instance, null, out _, null);
        apartment.PlaceObject(InteriorControls.Instance.painkillers, Player.Instance, Player.Instance, null, out _, null);
        apartment.PlaceObject(InteriorControls.Instance.bandage, Player.Instance, Player.Instance, null, out _, null);
        apartment.PlaceObject(InteriorControls.Instance.splint, Player.Instance, Player.Instance, null, out _, null);

        //Add codebreaker to safe
        apartment.PlaceObject(InteriorControls.Instance.codebreaker, Player.Instance, Player.Instance, null, out _, null, forceSecuritySettings: true, forcedOwnership: InteractablePreset.OwnedPlacementRule.both, forcedSecurity: 3 );

        //Partner's vmails
        Toolbox.Instance.NewVmailThread(Player.Instance, Player.Instance, null, null, null, "4e8b5f98-c543-409e-ae34-e692a5ce753f", SessionData.Instance.gameTime - 48f);

        //FPOWA vmail
        Toolbox.Instance.NewVmailThread(slophouseOwner, Player.Instance, null, null, null, "bd550805-e75f-41d1-b41d-a58e852a7ffa", SessionData.Instance.gameTime - 8f);
    }

    private void SpawnKidnapperClues()
    {
        //Place money inside the kidnapper's apartment for something to rob (and leave prints on)
        robItem = kidnapper.home.PlaceObject(InteriorControls.Instance.valuableItems[Toolbox.Instance.Rand(0, InteriorControls.Instance.valuableItems.Count)], kidnapper, kidnapper, null, out _, false);
        robItem.force = true;

        //Spare key to kidnapper apartment...
        if (spareKeyDoormat == null)
        {
            spareKeyDoormat = Toolbox.Instance.SpawnSpareKey(kidnapper.home);
        }

        //Place kidnappers work ID @ home
        if (workID == null)
        {
            workID = kidnapper.home.PlaceObject(InteriorControls.Instance.workID, kidnapper, kidnapper, null, out _, passVariable: false);
        }

        //Safe passcode
        if (safePasscode == null)
        {
            safePasscode = kidnapper.WriteNote(Human.NoteObject.note, "88098251-db09-4c6b-a4db-a1284502e11b", null, kidnapper.home, 0, InteractablePreset.OwnedPlacementRule.both, 2);
            
            //if(safePasscode != null)
            //{
            //    safePasscode.AddNewDynamicFingerprint(killer, Interactable.PrintLife.manualRemoval);
            //    safePasscode.AddNewDynamicFingerprint(killer, Interactable.PrintLife.manualRemoval);
            //}
        }

        //Player address note
        //playerAddressNote = kidnapper.home.PlaceObject(InteriorControls.Instance.note, kidnapper, kidnapper, Player.Instance, out _, null, ddsOverride: "9635fa61-9320-4dfc-94f1-9d6874c0d78b");

        //Meeting note
        meetingNote = kidnapper.home.PlaceObject(InteriorControls.Instance.note, kidnapper, killer, kidnapper, out _, false, ddsOverride: "4d51f137-9e56-4e93-8434-fccf3fc3d2f6");

        //Vmail exchange between kidnapper and notewriter
        Toolbox.Instance.NewVmailThread(kidnapper, noteWriter, null, null, null, "63c2e755-2840-44f6-b4da-f72e9a84d890", SessionData.Instance.gameTime);

        //Vmail exchange between killer and kidnapper
        Toolbox.Instance.NewVmailThread(killer, kidnapper, null, null, null, "37d3cbd8-9450-430f-8bf2-9e3efbf7f3f7", SessionData.Instance.gameTime - 24f);

        //Life insurance vmail
        StateSaveData.MessageThreadSave thread = Toolbox.Instance.NewVmailThread(kidnapper, kidnapper, null, null, null, "ddccb668-d79b-4a92-b106-a896ced366d5", SessionData.Instance.gameTime);

        //Create a printed version of the above...
        List<Interactable.Passed> newPassed = new List<Interactable.Passed>();

        Interactable.Passed passedID = new Interactable.Passed(Interactable.PassedVarType.vmailThreadID, thread.threadID);
        //Don't bother passing a message index here as there is only one vmail in the chain

        newPassed.Add(passedID);

        printedVmail = kidnapper.home.PlaceObject(InteriorControls.Instance.vmailPrintoutStatic, kidnapper, kidnapper, kidnapper, out _, newPassed, ddsOverride: thread.treeID);

        //Crumpled flyer
        crumpledFlyer = kidnapper.home.PlaceObject(InteriorControls.Instance.crumpledPaper, kidnapper, kidnapper, null, out _, false, ddsOverride: "1d64e27b-9992-4688-90ce-eab60bf88dd9");

        //Note on napkin
        noteOnNapkin = kidnapper.home.PlaceObject(InteriorControls.Instance.crumpledPaper, kidnapper, killer, kidnapper, out _, false, ddsOverride: "48e44a7b-86be-4d0e-8ed8-cd87d76cb64d");

        //Turn photograph
        tornPhotograph = kidnapper.home.PlaceObject(InteriorControls.Instance.crumpledPaper, kidnapper, killer, kidnapper, out _, false, ddsOverride: "8cf8c5d6-8407-4c50-90e5-c88dd8b60af8", forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseOwned);

        //Place a codebreaker gadget
        kidnapper.home.PlaceObject(InteriorControls.Instance.codebreaker, kidnapper, kidnapper, null, out _, false);

        //Workplace personal letter
        kidnapper.job.employer.address.PlaceObject(InteriorControls.Instance.letter, kidnapper, kidnapper, null, out _, true, forcedOwnership: InteractablePreset.OwnedPlacementRule.ownedOnly, forcedPriority: 2, ddsOverride: "b340345c-af9f-4694-a743-adaf78035c8b");

        //workplace dealer note
        workplaceMessageNote = kidnapper.job.employer.address.PlaceObject(InteriorControls.Instance.crumpledPaper, kidnapper, kidnapper, kidnapper, out _, false, ddsOverride: "2dff3b2a-501b-4e4d-afa2-a8eddd3fd3ee", forcedOwnership: InteractablePreset.OwnedPlacementRule.ownedOnly);

        //Workplace receipt
        List<Interactable.Passed> receiptPassed = new List<Interactable.Passed>();
        receiptPassed.Add(new Interactable.Passed(Interactable.PassedVarType.companyID, killerBar.company.companyID));
        receiptPassed.Add(new Interactable.Passed(Interactable.PassedVarType.time, SessionData.Instance.gameTime - 24f));
        receiptPassed.Add(new Interactable.Passed(Interactable.PassedVarType.stringInteractablePreset, -1, "Gemsteader"));
        receiptPassed.Add(new Interactable.Passed(Interactable.PassedVarType.stringInteractablePreset, -1, "Vodka"));

        workplaceReceipt = kidnapper.job.employer.address.PlaceObject(InteriorControls.Instance.receipt, kidnapper, kidnapper, kidnapper, out _, receiptPassed, forcedOwnership: InteractablePreset.OwnedPlacementRule.ownedOnly);
    }

    private void SpawnNotewriterClues()
    {
        //Spawn suitcase on bed
        if(noteWritersBed != null)
        {
            //NewNode bedNode = noteWritersBed.coversNodes[0];

            //Use the bed's rotation
            //Vector2 suitcaseRot = Toolbox.Instance.RotateVector2CW(new Vector3(0.41f, -0.645f), noteWritersBed.angle);

            //Interactable suitcase = new Interactable(InteriorControls.Instance.suitcase, bedNode.transform, new Vector3(suitcaseRot.x, 0.7f, suitcaseRot.y), new Vector3(0, UnityEngine.Random.Range(-20f, 20f), 0));
            //Change below interatable type to something else...
            //suitcase = InteractableCreator.Instance.CreateFurnitureSpawnedInteractable(InteriorControls.Instance.suitcase, noteWritersBed, noteWriter, noteWriter, null, new Vector3(-0.95f, 0.75f, -0.742f), new Vector3(0, Toolbox.Instance.Rand(-20f, 20f), 0), InteractableController.InteractableID.A, FurniturePreset.SubObjectOwnership.nobody, null, null);
        }

        //Reward sync disk; place in note writer's mailbox
        if(rewardSyncDisk == null && noteWriter != null)
        {
            Interactable mailboxDoor = Toolbox.Instance.GetMailbox(noteWriter);

            if (mailboxDoor != null)
            {
                rewardSyncDisk = mailboxDoor.node.gameLocation.PlaceObject(InteriorControls.Instance.chapterRewardSyncDisk.interactable, noteWriter, noteWriter, Player.Instance, out _, passedObject: InteriorControls.Instance.chapterRewardSyncDisk, forceSecuritySettings: true, forcedOwnership: InteractablePreset.OwnedPlacementRule.ownedOnly, forcedPriority: 3, ignoreLimits: true);
            }
        }

        //Vmail Exchange between notewriter and a friend
        Acquaintance aqFriend = null;
        float like = -9999999f;

        foreach(Acquaintance aq in noteWriter.acquaintances)
        {
            if(aq.with != killer && aq.with != kidnapper)
            {
                if(aq.like + aq.known > like)
                {
                    aqFriend = aq;
                    like = aq.like + aq.known;
                }
            }
        }

        Toolbox.Instance.NewVmailThread(noteWriter, aqFriend.with, null, null, null, "4053b6b3-d28d-4286-bc3a-40b00eabda7f", SessionData.Instance.gameTime - 7f);

        //Travel receipt
        travelreceipt = noteWriter.home.PlaceObject(InteriorControls.Instance.note, noteWriter, noteWriter, null, out _, false, ddsOverride: "00c130a7-d733-403a-b7fe-302041a5dc23");

        //Letter with credits
        envelopeWithCredits = noteWriter.home.PlaceObject(InteriorControls.Instance.letter, noteWriter, kidnapper, noteWriter, out _, false, forceSecuritySettings: true, forcedSecurity: 3, ddsOverride: "b67d4478-2229-47a0-9f41-3d266d6b542a");

        //Play money
        noteWriter.home.PlaceObject(InteriorControls.Instance.moneyLots, noteWriter, noteWriter, null, out _, true, forcedSecurity: 3, forcedOwnership: InteractablePreset.OwnedPlacementRule.ownedOnly);
    }

    private void SpawnKillerClues()
    {
        //Killer propaganda
        killerPropaganda = killer.home.PlaceObject(InteriorControls.Instance.letter, killer, killer, null, out _, null, ddsOverride: "8ab3445f-a6c2-46ec-993c-f4dda9a47a0b");

        //Killer police fines
        killerPoliceFines = killer.home.PlaceObject(InteriorControls.Instance.letter, killer, killer, null, out _, null, ddsOverride: "af25d949-bce6-4ebf-9d94-0ced43a69aab");

        //Killer corp sponsorship
        killerCorpSponsorship = killer.home.PlaceObject(InteriorControls.Instance.letter, killer, killer, null, out _, null, ddsOverride: "a174ed64-3297-4fed-bcc5-0ff939168f69");

        //Killer notewriter details
        killerNotewriterDetails = killer.home.PlaceObject(InteriorControls.Instance.note, killer, killer, noteWriter, out _, null, ddsOverride: "3327d0c3-c8ae-4ca8-bcb9-285118273a05");

        //Killer business card
        killerBusinessCard = killer.home.PlaceObject(InteriorControls.Instance.businessCard, killer, killer, null, out _, null, ddsOverride: "3d63c9fc-3c1d-4d65-abca-2029db278305");

        //Target Vmails
        Toolbox.Instance.NewVmailThread(killer, killer, null, null, null, "cc972db9-1f6e-440e-b276-0bbacf0a8e73", SessionData.Instance.gameTime + 2f, 999);
        Toolbox.Instance.NewVmailThread(killer, killer, null, null, null, "c821e6b0-8901-4381-a149-d15105e32aa8", SessionData.Instance.gameTime + 2.25f, 999);

        //Bar tab should be placed where the poker tournament is held
        if(killerBar != null)
        {
            Game.Log("Chapter: Killer belongs to poker club, so placing bar tab evidence at " + killerBar);

            Human boss = killerBar.company.director;

            killerBarTab = killerBar.PlaceObject(InteriorControls.Instance.note, boss, boss, killer, out _, null, forceSecuritySettings: true, 0, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseOwned, ddsOverride: "860a4b02-79a8-4998-afd6-fe1629fd9a0b");
        }
    }

    private void SpawnMiscClues()
    {
        //Add calendar birthdays
        if (playerCalendar != null)
        {
            EvidenceMultiPage calendar = playerCalendar.evidence as EvidenceMultiPage;

            //Add player's birthday
            calendar.AddStringContentToPage(Game.Instance.playerBirthMonth, Toolbox.Instance.GetNumbericalStringReference(Game.Instance.playerBirthDay) + " — " + Strings.ComposeText(Strings.Get("chapter.introduction", "playersbirthday"), playerCalendar), order: preset.startingDate);

            //Add partners's birthday
            calendar.AddStringContentToPage(8, Toolbox.Instance.GetNumbericalStringReference(9) + " — " + Strings.ComposeText(Strings.Get("chapter.introduction", "partnersbirthday"), playerCalendar), order: preset.startingDate);
        }

        //Add calendar events
        if (kidnappersCalendar != null)
        {

        }

        //If notewriter is the CEO/owner, then write a note with the password
        if (noteWriter.job.employer.address.owners.Contains(noteWriter))
        {
        }
        else if (noteWriter.job.employer.address.owners.Contains(kidnapper))
        {
            HashSet<NewRoom> dontPlaceIn = new HashSet<NewRoom>(kidnapper.job.employer.address.rooms.FindAll(item => item.preset.name == "BusinessBackroom"));
        }

        //Set note writer diary
        //noteWriterDiary = Toolbox.Instance.FindClosestObjectTo(InteriorControls.Instance.diary, noteWriter.home.entrances[0].fromNode.position, noteWriter.home, null, out _);

        //if (noteWriterDiary == null)
        //{
        //    noteWriterDiary = noteWriter.home.PlaceObject(InteriorControls.Instance.diary, noteWriter, null, out _, null, forcedPriority: 3, ddsOverride: "451ad347-5a12-4e88-86c8-0bd4baa34f66");
        //}

        //Kindapper diary
        kidnapperDiary = Toolbox.Instance.FindClosestObjectTo(InteriorControls.Instance.diary, kidnapper.home.entrances[0].fromNode.position, null, kidnapper.home, null, out _);

        if (kidnapperDiary == null)
        {
            kidnapperDiary = kidnapper.home.PlaceObject(InteriorControls.Instance.diary, kidnapper, kidnapper, null, out _, null, forcedPriority: 3, ddsOverride: "3bc4ab3c-c6e7-4bf7-8c2d-51c4fbf7cc1b");
        }
    }

    public void ExecutePreSim()
    {
        //Force restaurant to be open
        //restaurant.company.SetOpen(true);
        //restaurant.company.OnActualOpen();

        //positionalUpdateTimer++;

        //foreach(NewNode.NodeAccess acc in restaurant.entrances)
        //{
        //    if(acc.door != null)
        //    {
        //        acc.door.SetLocked(false, null);
        //    }
        //}

        ////Teleport both notewriter and kidnapper to somewhere near the restaurant
        //if(restaurant.building.street != null)
        //{
        //    Game.Log("Chapter: Teleporting notewriter and kidnapper to " + restaurant.name);
        //    restaurant.company.SetOpen(true, true);
            
        //    foreach(NewRoom r in restaurant.rooms)
        //    {
        //        r.SetMainLights(true, "Chapter", null, forceInstant: true, forceUpdate: true);
        //    }

        //    noteWriter.Teleport(noteWriter.FindSafeTeleport(restaurant.rooms.Find(item => item.roomType.name == "Diner" || item.roomType.name == "Eatery")), null);
        //    kidnapper.Teleport(kidnapper.FindSafeTeleport(restaurant.rooms.Find(item => item.roomType.name == "Diner" || item.roomType.name == "Eatery")), null);
        //}

        //Make sure tick rate is at maximum
        //noteWriter.ai.disableTickRateUpdate = true;
        //noteWriter.ai.SetDesiredTickRate(NewAIController.AITickRate.veryHigh, true);
        //kidnapper.ai.disableTickRateUpdate = true;
        //kidnapper.ai.SetDesiredTickRate(NewAIController.AITickRate.veryHigh, true);

        //Meet kidnapper for food at restaurant
        //meetGroup = new GroupsController.SocialGroup();
        //meetGroup.decimalStartTime = SessionData.Instance.decimalClock;
        //meetGroup.preset = RoutineControls.Instance.meetUpFoodMission.name;
        //meetGroup.members = new List<int>();
        //meetGroup.weekDays = new List<SessionData.WeekDay>();
        //meetGroup.weekDays.Add(SessionData.Instance.day);
        //meetGroup.meetingPlace = restaurant.id;
        //meetGroup.members.Add(noteWriter.humanID);
        //meetGroup.members.Add(kidnapper.humanID);

        //List<Human> meetGroup = new List<Human>();
        //meetGroup.Add(noteWriter);
        //meetGroup.Add(kidnapper);

        //Pass other person and restaurant
        //meetFoodNotewriter = noteWriter.ai.CreateNewGoal(RoutineControls.Instance.meetFood, 0f, 0f, newPassedGameLocation: restaurant, newPassedGroup: meetGroup);
        //meetFoodKidnapper = kidnapper.ai.CreateNewGoal(RoutineControls.Instance.meetFood, 0f, 0f, newPassedGameLocation: restaurant, newPassedGroup: meetGroup);

        //NEW SYSTEM
        preSimPhase = 0;

        //Make sure restaurant is open
        Game.Log("Chapter: Setting restaurant open: " + restaurant.name);
        restaurant.company.SetOpen(true, true);
        restaurant.company.OnActualOpen();

        foreach (NewRoom r in restaurant.rooms)
        {
            r.SetMainLights(true, "Chapter", null, forceInstant: true, forceUpdate: true);
        }

        foreach (NewNode.NodeAccess acc in restaurant.entrances)
        {
            if (acc.door != null)
            {
                acc.door.SetLocked(false, null);
            }
        }

        //Disable AI
        noteWriter.ai.EnableAI(false);
        kidnapper.ai.EnableAI(false);
        killer.ai.EnableAI(false);

        StartCoroutine(PreSimHandling());
    }

    IEnumerator PreSimHandling()
    {
        Game.Log("Chapter: Handling pre sim...");
        handlePreSim = true;
        float preSimTimeElapsed = 0f;
        float prevTime = SessionData.Instance.gameTime;

        //Phase 0
        bool findRestaurantSeat = false;
        NewNode boothSeatNode = null;
        Interactable boothSeat1 = null;
        Interactable boothSeat2 = null;
        float sittingTime = 0f;
        float preMeetTime = 0f;
        bool findMeetTime = false;

        float preMeetingLength = 0.2f; //SETTING: Record before meeting
        float meetingTimeLength = 0.6f; //SETTING: How long for them to meet for
        float phase0LoadRatio = 0.5f; //SETTING: How much load time does this represent?

        //Phase 1
        bool noteWriterPathingComplete = false;
        bool nWpathsCalc = false;
        int nWcaptureCursor = 0;
        PathFinder.PathData nWcapturePath = null;
        Dictionary<NewRoom, List<NewNode>> nWnodesPool = null;
        Dictionary<NewNode, List<Interactable>> nWcameraCoverage = null;
        HashSet<NewRoom> nWroutesCovered = null;
        bool nWwaitingForCapture = false;
        float nWwaitForCaptureTime = 0f;
        Interactable nWcam = null;

        bool kidnapperPathingComplete = false;
        bool kpathsCalc = false;
        int kcaptureCursor = 0;
        PathFinder.PathData kcapturePath = null;
        Dictionary<NewRoom, List<NewNode>> knodesPool = null;
        Dictionary<NewNode, List<Interactable>> kcameraCoverage = null;
        HashSet<NewRoom> kroutesCovered = null;
        bool kwaitingForCapture = false;
        float kwaitForCaptureTime = 0f;
        Interactable kcam = null;

        float nwAv = 0f;
        float kAv = 0f;
        float recordedDistance2 = 999;
        float startDistance2 = 999;

        float phase1LoadRatio = 0.33f; //SETTING: How much load time does this represent?

        //Phase 2
        bool setupFinalPhase = false;
        float recordedDistance = 999f;
        float startDistance = 999f;

        float phase2LoadRatio = 0.33f; //SETTING: How much load time does this represent?
        float murderTime = -1f;

        /* NEW SYSTEM: Confine notewriter and kidnapper to home; we will fake them being out and about...
         * 1: Position notewriter and kidnapper in cafe within clear shot of cctv
         *      Make sure cctv shots are permenant, and not erased until after the tutorial phase
         *      Generate receipt and make sure they are not erased until after the tutorial phase
         * 2: Position notewriter along route to player apartment
         * 3: Position kidnapper along route to home
         * 4: Execute murder
        */

        while (handlePreSim)
        {
            preSimTimeElapsed = SessionData.Instance.gameTime - prevTime;
            prevTime = SessionData.Instance.gameTime;

            //Phase 0: At the restaurant...
            if(preSimPhase == 0)
            {
                if(preMeetTime < preMeetingLength)
                {
                    preMeetTime += preSimTimeElapsed;
                }
                else
                {
                    //Find a good seat in the restaurant, in view of CCTV
                    if(!findRestaurantSeat)
                    {
                        NewRoom sittingRoom = restaurant.rooms.Find(item => item.roomType.name == "Diner" || item.roomType.name == "Eatery" || item.roomType.name == "Bar");

                        if (sittingRoom != null)
                        {
                            //Setup purchases
                            if(sittingRoom.actionReference.ContainsKey(RoutineControls.Instance.takeConsumable))
                            {
                                foreach(Interactable i in sittingRoom.actionReference[RoutineControls.Instance.takeConsumable])
                                {
                                    if(i.preset.specialCaseFlag != InteractablePreset.SpecialCase.fridge)
                                    {
                                        ActionController.Instance.TakeConsumable(i, i.node, noteWriter);
                                        ActionController.Instance.TakeConsumable(i, i.node, kidnapper);
                                        break;
                                    }
                                }
                            }

                            //Set seating
                            Dictionary<NewNode, float> boothSeatScoreDict = new Dictionary<NewNode, float>();
                            boothSeatNode = null;
                            float bestBoothScore = Mathf.Infinity; //Lowest best

                            foreach (Interactable cctv in restaurant.securityCameras)
                            {
                                if (cctv.node.room == sittingRoom)
                                {
                                    foreach (NewNode n in cctv.sceneRecorder.primaryNodes)
                                    {
                                        if (n.individualFurniture.Exists(item => item.furniture.classes.Exists(item => item.name == "1x1DinerBoothTable")))
                                        {
                                            float distanceToCam = Vector3.Distance(n.position, cctv.wPos);
                                            if (distanceToCam <= 5f) distanceToCam += 5f; //Don't choose too close?

                                            distanceToCam -= Toolbox.Instance.Rand(0f, 1.8f); //Randomize a bit...

                                            if (!boothSeatScoreDict.ContainsKey(n))
                                            {
                                                boothSeatScoreDict.Add(n, distanceToCam);
                                            }
                                            else boothSeatScoreDict[n] += distanceToCam;

                                            if (boothSeatScoreDict[n] < bestBoothScore)
                                            {
                                                boothSeatNode = n;
                                                bestBoothScore = boothSeatScoreDict[n];
                                            }
                                        }
                                    }
                                }
                            }

                            //Have we found the best seat?
                            if (boothSeatNode != null)
                            {
                                //Get seats...
                                if (sittingRoom.actionReference.ContainsKey(RoutineControls.Instance.sit))
                                {
                                    foreach(Interactable s in sittingRoom.actionReference[RoutineControls.Instance.sit])
                                    {
                                        if(s.node == boothSeatNode)
                                        {
                                            //Try and pick opposite facing seats
                                            if(boothSeat1 == null)
                                            {
                                                boothSeat1 = s;
                                            }
                                            else
                                            {
                                                if(boothSeat2 == null)
                                                {
                                                    boothSeat2 = s;
                                                }

                                                if(s.furnitureParent != boothSeat1.furnitureParent)
                                                {
                                                    boothSeat2 = s;
                                                }
                                            }
                                        }
                                    }
                                }

                                if (boothSeat1 != null && boothSeat2 != null)
                                {
                                    noteWriter.Teleport(boothSeatNode, boothSeat1.usagePoint);
                                    noteWriter.animationController.SetIdleAnimationState(CitizenAnimationController.IdleAnimationState.sitting);

                                    kidnapper.Teleport(boothSeatNode, boothSeat2.usagePoint);
                                    kidnapper.animationController.SetIdleAnimationState(CitizenAnimationController.IdleAnimationState.sitting);

                                    findRestaurantSeat = true;
                                    Game.Log("Chapter: Found and teleported kidnapper, notewriter to booth seats: " + boothSeat1.id + " and " + boothSeat2.id);
                                }

                                else Game.LogError("Chapter: Unable to find 2 booth seats");
                            }
                            else
                            {
                                Game.LogError("Chapter: Unable to find a valid booth seat at " + sittingRoom.name + " " + sittingRoom.roomID + " (" + restaurant.securityCameras.Count + " cams)");
                            }
                        }
                        else Game.LogError("Chapter: Unable to find restaurant sitting room");
                    }
                    else
                    {
                        sittingTime += preSimTimeElapsed;

                        //Wait time
                        if (sittingTime < meetingTimeLength)
                        {
                            CityConstructor.Instance.loadingProgress = phase0LoadRatio * (sittingTime / meetingTimeLength);

                            if(!findMeetTime)
                            {
                                Game.Log("Chapter: Notewriter and kidnapper are both sitting at the restaurant " + SessionData.Instance.DecimalToClockString(SessionData.Instance.decimalClock, false));
                                meetTime = SessionData.Instance.gameTime;
                                meetTime = Mathf.Round(meetTime * 4f) / 4f;

                                string timeStr = SessionData.Instance.GameTimeToClock12String(meetTime, false);
                                string dateStr = SessionData.Instance.ShortDateString(meetTime, true);

                                meetingTimeEvidence = EvidenceCreator.Instance.GetTimeEvidence(meetTime, meetTime) as EvidenceTime;

                                //Setup calendar event
                                if (kidnappersCalendar != null)
                                {
                                    //meetingTimeFact = EvidenceCreator.Instance.CreateFact("FoundOn", meetingTimeEvidence, kidnappersCalendar.evidence);
                                    //Fact restaurantFact = EvidenceCreator.Instance.CreateFact("FoundOn", restaurant.evidenceEntry, kidnappersCalendar.evidence);
                                    //Fact kidnapperFact = EvidenceCreator.Instance.CreateFact("FoundOn", restaurant.evidenceEntry, kidnapper.evidenceEntry);

                                    //Add meeting note
                                    EvidenceMultiPage kidnapperCalendar = kidnappersCalendar.evidence as EvidenceMultiPage;

                                    kidnapperCalendar.AddStringContentToPage(preset.startingMonth + 1, Toolbox.Instance.GetNumbericalStringReference(preset.startingDate) + " — " + Strings.ComposeText(Strings.Get("chapter.introduction", "kidnappercalendar"), kidnappersCalendar), order: preset.startingDate);
                                    //kidnapperCalendar.AddEvidenceDiscoveryToPage(meetingTimeFact, preset.startingMonth + 1, meetingTimeFact);
                                    //kidnapperCalendar.AddEvidenceDiscoveryToPage(restaurantFact, preset.startingMonth + 1);
                                    //kidnapperCalendar.AddEvidenceDiscoveryToPage(restaurantFact, preset.startingMonth + 1);
                                }

                                //Spawn diner flyer clue
                                if (dinerFlyer == null)
                                {
                                    Vector3 pos = boothSeatNode.position + new Vector3(0f, 1f, 0f);

                                    List<FurniturePreset.SubObject> so = new List<FurniturePreset.SubObject>();
                                    List<FurnitureLocation> furnSo = new List<FurnitureLocation>();

                                    foreach(FurnitureLocation furn in boothSeatNode.individualFurniture)
                                    {
                                        if (furn.furniture.classes[0].name == "1x1WindowShelf") continue; //Don't spawn on the window shelf!

                                        foreach(FurniturePreset.SubObject sub in furn.furniture.subObjects)
                                        {
                                            if(!furn.spawnedInteractables.Exists(item => item.subObject == sub))
                                            {
                                                so.Add(sub);
                                                furnSo.Add(furn);
                                            }
                                        }
                                    }

                                    if(so.Count > 0)
                                    {
                                        int chosenIndex = Toolbox.Instance.Rand(0, so.Count);
                                        FurniturePreset.SubObject chosen = so[chosenIndex];
                                        FurnitureLocation chosenFurn = furnSo[chosenIndex];

                                        dinerFlyer = InteractableCreator.Instance.CreateFurnitureSpawnedInteractable(InteriorControls.Instance.crumpledPaper, chosenFurn, chosen, kidnapper, noteWriter, kidnapper, null, null, null, ddsOverride: "f40252ed-0254-41da-98a1-a3f5d1d09673");

                                    }
                                    else dinerFlyer = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.crumpledPaper, kidnapper, noteWriter, kidnapper, pos, Vector3.zero, null, null, ddsOverride: "f40252ed-0254-41da-98a1-a3f5d1d09673");
                                }

                                findMeetTime = true;
                            }
                        }
                        else
                        {
                            //Teleport killer to their home, make a call to kidnapper
                            Game.Log("Chapter: Teleporting killer to their home so they can make a call to the kidnapper...");
                            killer.Teleport(killer.FindSafeTeleport(killer.home), null);

                            //Make a call from killer to kidnapper
                            foreach (Telephone t in killer.home.telephones)
                            {
                                if (t != null && kidnapperPhone != null)
                                {
                                    Game.Log("Chapter: Killer making call to kidnapper...");
                                    TelephoneController.Instance.CreateNewCall(t, kidnapperPhone.t, killer, kidnapper, new TelephoneController.CallSource(TelephoneController.CallType.dds, "8a0d56c3-629a-40d2-9f7a-22aa07a18f48"), 0.1f, true);
                                    break;
                                }
                            }

                            Game.Log("Chapter: Notewriter heading to player's apartment...");

                            preSimPhase = 1;
                        }
                    }
                }
            }
            //Phase 1: Notewriter goes to post note, kidnapper travels home...
            else if(preSimPhase == 1)
            {
                if(!noteWriterPathingComplete)
                {
                    if(!nWpathsCalc)
                    {
                        nWcapturePath = PathFinder.Instance.GetPath(noteWriter.FindSafeTeleport(restaurant.streetAccess.GetOtherGameLocation(restaurant)), exteriorDoorNode, noteWriter);

                        if(nWcapturePath != null)
                        {
                            //Scan route for cctv coverage, and record it here...
                            nWnodesPool = new Dictionary<NewRoom, List<NewNode>>();
                            nWcameraCoverage = new Dictionary<NewNode, List<Interactable>>();

                            foreach(NewNode.NodeAccess acc in nWcapturePath.accessList)
                            {
                                if(acc.toNode.gameLocation.securityCameras.Count > 0)
                                {
                                    foreach(Interactable cctv in acc.toNode.gameLocation.securityCameras)
                                    {
                                        //This node is covered by a camera, add to pool
                                        if(cctv.sceneRecorder.coveredNodes.ContainsKey(acc.toNode))
                                        {
                                            if(!nWnodesPool.ContainsKey(acc.toNode.room))
                                            {
                                                nWnodesPool.Add(acc.toNode.room, new List<NewNode>());
                                            }

                                            nWnodesPool[acc.toNode.room].Add(acc.toNode);

                                            if(!nWcameraCoverage.ContainsKey(acc.toNode))
                                            {
                                                nWcameraCoverage.Add(acc.toNode, new List<Interactable>());
                                            }

                                            nWcameraCoverage[acc.toNode].Add(cctv);
                                        }
                                    }
                                }
                            }

                            nWroutesCovered = new HashSet<NewRoom>();

                            //We don't need any rooms from the restaurant to be covered...
                            foreach(NewRoom r in restaurant.rooms)
                            {
                                nWroutesCovered.Add(r);
                            }

                            nWpathsCalc = true;
                            nWwaitingForCapture = false;
                            nWcaptureCursor = 0;
                        }
                    }
                    else
                    {
                        //Skip to next node for capture
                        if (!nWwaitingForCapture)
                        {
                            if(nWcaptureCursor < nWcapturePath.accessList.Count - 1)
                            {
                                while(nWcaptureCursor < nWcapturePath.accessList.Count - 1 && (nWroutesCovered.Contains(nWcapturePath.accessList[nWcaptureCursor].toNode.room) || !nWnodesPool.ContainsKey(nWcapturePath.accessList[nWcaptureCursor].toNode.room)))
                                {
                                    nWcaptureCursor++;
                                }

                                if (nWcaptureCursor < nWcapturePath.accessList.Count - 1)
                                {
                                    NewNode.NodeAccess acc = nWcapturePath.accessList[nWcaptureCursor];
                                    Game.Log("Chapter: Notewriter waiting for cctv capture at " + acc.toNode.room.name + " " + nWcaptureCursor + "/" + nWcapturePath.accessList.Count);

                                    //Choose a random node to position character on...
                                    NewNode chosenNode = nWnodesPool[acc.toNode.room][Toolbox.Instance.Rand(0, nWnodesPool[acc.toNode.room].Count)];
                                    noteWriter.Teleport(chosenNode, null);
                                    noteWriter.animationController.SetIdleAnimationState(CitizenAnimationController.IdleAnimationState.none);

                                    //Face correct direction
                                    if(nWcaptureCursor < nWcapturePath.accessList.Count - 2)
                                    {
                                        Vector3 facingDirection = nWcapturePath.accessList[nWcaptureCursor + 1].toNode.position - noteWriter.transform.position;

                                        //Update quaternion
                                        facingDirection.y = 0; // keep only the horizontal direction
                                        if (facingDirection != Vector3.zero) noteWriter.transform.rotation = Quaternion.LookRotation(facingDirection);
                                    }

                                    //Set camera
                                    nWcam = nWcameraCoverage[chosenNode][0];

                                    //Set waiting for capture
                                    nWwaitForCaptureTime = SessionData.Instance.gameTime;
                                    nWwaitingForCapture = true;
                                }
                            }
                            else
                            {
                                //Complete!
                                noteWriterPathingComplete = true;
                            }
                        }
                        else
                        {
                            //Capture happens, we can move on now!
                            if (nWcam != null && nWcam.sceneRecorder.lastCaptureAt >= nWwaitForCaptureTime)
                            {
                                Game.Log("Chapter: Notewriter cctv capture at " + nWcam.node.room.name + " " + nWcaptureCursor + "/" + nWcapturePath.accessList.Count);
                                nWroutesCovered.Add(nWcapturePath.accessList[nWcaptureCursor].toNode.room); //Add to path
                            }

                            nWwaitingForCapture = false;
                        }
                    }
                }

                if (!kidnapperPathingComplete)
                {
                    if (!kpathsCalc)
                    {
                        kcapturePath = PathFinder.Instance.GetPath(kidnapper.FindSafeTeleport(restaurant.streetAccess.GetOtherGameLocation(restaurant)), kidnappersDoorNode, kidnapper);

                        if (kcapturePath != null)
                        {
                            //Scan route for cctv coverage, and record it here...
                            knodesPool = new Dictionary<NewRoom, List<NewNode>>();
                            kcameraCoverage = new Dictionary<NewNode, List<Interactable>>();

                            foreach (NewNode.NodeAccess acc in kcapturePath.accessList)
                            {
                                if (acc.toNode.gameLocation.securityCameras.Count > 0)
                                {
                                    foreach (Interactable cctv in acc.toNode.gameLocation.securityCameras)
                                    {
                                        //This node is covered by a camera, add to pool
                                        if (cctv.sceneRecorder.coveredNodes.ContainsKey(acc.toNode))
                                        {
                                            if (!knodesPool.ContainsKey(acc.toNode.room))
                                            {
                                                knodesPool.Add(acc.toNode.room, new List<NewNode>());
                                            }

                                            knodesPool[acc.toNode.room].Add(acc.toNode);

                                            if (!kcameraCoverage.ContainsKey(acc.toNode))
                                            {
                                                kcameraCoverage.Add(acc.toNode, new List<Interactable>());
                                            }

                                            kcameraCoverage[acc.toNode].Add(cctv);
                                        }
                                    }
                                }
                            }

                            kroutesCovered = new HashSet<NewRoom>();

                            //We don't need any rooms from the restaurant to be covered...
                            foreach (NewRoom r in restaurant.rooms)
                            {
                                kroutesCovered.Add(r);
                            }

                            kpathsCalc = true;
                            kwaitingForCapture = false;
                            kcaptureCursor = 0;
                        }
                    }
                    else
                    {
                        //Skip to next node for capture
                        if (!kwaitingForCapture)
                        {
                            if (kcaptureCursor < kcapturePath.accessList.Count - 1)
                            {
                                while (kcaptureCursor < kcapturePath.accessList.Count - 1 && (kroutesCovered.Contains(kcapturePath.accessList[kcaptureCursor].toNode.room) || !knodesPool.ContainsKey(kcapturePath.accessList[kcaptureCursor].toNode.room)))
                                {
                                    kcaptureCursor++;
                                }

                                if (kcaptureCursor < kcapturePath.accessList.Count - 1)
                                {
                                    NewNode.NodeAccess acc = kcapturePath.accessList[kcaptureCursor];
                                    Game.Log("Chapter: Kidnapper waiting for cctv capture at " + acc.toNode.room.name + " " + kcaptureCursor + "/" + kcapturePath.accessList.Count);

                                    //Choose a random node to position character on...
                                    NewNode chosenNode = knodesPool[acc.toNode.room][Toolbox.Instance.Rand(0, knodesPool[acc.toNode.room].Count)];
                                    kidnapper.Teleport(chosenNode, null);
                                    kidnapper.animationController.SetIdleAnimationState(CitizenAnimationController.IdleAnimationState.none);

                                    //Face correct direction
                                    if (kcaptureCursor < kcapturePath.accessList.Count - 2)
                                    {
                                        Vector3 facingDirection = kcapturePath.accessList[kcaptureCursor + 1].toNode.position - kidnapper.transform.position;

                                        //Update quaternion
                                        facingDirection.y = 0; // keep only the horizontal direction
                                        if (facingDirection != Vector3.zero) kidnapper.transform.rotation = Quaternion.LookRotation(facingDirection);
                                    }

                                    //Set camera
                                    kcam = kcameraCoverage[chosenNode][0];

                                    //Set waiting for capture
                                    kwaitForCaptureTime = SessionData.Instance.gameTime;
                                    kwaitingForCapture = true;
                                }
                            }
                            else
                            {
                                //Complete!
                                kidnapperPathingComplete = true;
                            }
                        }
                        else
                        {
                            //Capture happens, we can move on now!
                            if (kcam != null && kcam.sceneRecorder.lastCaptureAt >= kwaitForCaptureTime)
                            {
                                Game.Log("Chapter: Kidnapper cctv capture at " + kcam.node.room.name + " " + kcaptureCursor + "/" + kcapturePath.accessList.Count);
                                kroutesCovered.Add(kcapturePath.accessList[kcaptureCursor].toNode.room); //Add to path
                            }

                            kwaitingForCapture = false;
                        }
                    }
                }

                if (!noteWriterPathingComplete && !kidnapperPathingComplete)
                {
                    float dist = Vector3.Distance(noteWriter.transform.position, exteriorDoorNode.position);

                    if (dist < recordedDistance)
                    {
                        recordedDistance = dist;
                        if (startDistance >= 999f) startDistance = recordedDistance;
                        nwAv = 1f - (recordedDistance / startDistance);
                    }

                    dist = Vector3.Distance(kidnapper.transform.position, kidnappersDoorNode.position);

                    if (dist < recordedDistance2)
                    {
                        recordedDistance2 = dist;
                        if (startDistance2 >= 999f) startDistance2 = recordedDistance2;
                        kAv = 1f - (recordedDistance2 / startDistance2);
                    }

                    CityConstructor.Instance.loadingProgress = phase0LoadRatio + phase1LoadRatio * ((nwAv + kAv)/2f);
                }
                else
                {
                    recordedDistance = 999;
                    startDistance = 999;
                    preSimPhase = 2;
                }
            }
            //Phase 2: Execute...
            else if(preSimPhase == 2)
            {
                if(!setupFinalPhase)
                {
                    //Make sure diner CCTV records aren't removed over time
                    foreach (Interactable cctv in restaurant.securityCameras)
                    {
                        foreach(SceneRecorder.SceneCapture cap in cctv.cap)
                        {
                            cap.k = true;
                        }
                    }

                    //Make sure kidnapper is home
                    kidnapper.Teleport(kidnapper.FindSafeTeleport(kidnapper.home), null);
                    kidnapper.animationController.SetIdleAnimationState(CitizenAnimationController.IdleAnimationState.none);

                    //Make sure kidnapper's other housemates stay away
                    foreach(Human housemate in kidnapper.home.inhabitants)
                    {
                        if (housemate == kidnapper) continue;
                        housemate.Teleport(housemate.FindSafeTeleport(restaurant), null);
                        housemate.ai.AddAvoidLocation(kidnapper.home);
                    }

                    //Make sure notewriter is home & confine to location
                    noteWriter.Teleport(noteWriter.FindSafeTeleport(noteWriter.home), null);
                    noteWriter.ai.SetConfineLocation(noteWriter.home);

                    //Make sure notewriter's other housemates are away
                    foreach (Human housemate in noteWriter.home.inhabitants)
                    {
                        if (housemate == noteWriter) continue;
                        housemate.Teleport(housemate.FindSafeTeleport(restaurant), null);
                        housemate.ai.AddAvoidLocation(noteWriter.home);
                    }

                    //Enable AI for everyone...
                    noteWriter.ai.EnableAI(true);
                    kidnapper.ai.EnableAI(true);

                    //Get rid of all binnable trash...
                    if (kidnapperBin != null)
                    {
                        Game.Log("Chapter: Dispose of " + kidnapper.trash.Count + " trash objects in bin...");
                        ActionController.Instance.Dispose(kidnapperBin, kidnapperBin.node, kidnapper);
                    }

                    //Warning VMail to notewriter
                    Toolbox.Instance.NewVmailThread(kidnapper, noteWriter, null, null, null, "0b71ed7f-88e4-4887-9fd4-dd371253a30d", SessionData.Instance.gameTime);

                    //Get rid of all trash here...
                    for (int i = 0; i < kidnapper.trash.Count; i++)
                    {
                        MetaObject t = CityData.Instance.FindMetaObject(kidnapper.trash[i]);

                        if(t != null)
                        {
                            InteractablePreset newPreset = null;

                            if (Toolbox.Instance.objectPresetDictionary.TryGetValue(t.preset, out newPreset))
                            {
                                //Add created time and trash flag to passed...
                                t.passed.Add(new Interactable.Passed(Interactable.PassedVarType.isTrash, SessionData.Instance.gameTime)); //Pass trash variable and when it was turned into an interactable...

                                kidnapper.home.PlaceObject(newPreset, kidnapper, kidnapper, null, out _, passedVars: t.passed);
                                Game.Log("Chapter: Dispose of " + newPreset.name + " in apartment...");

                                if (newPreset.disposal == Human.DisposalType.anywhere) kidnapper.anywhereTrash--;
                                kidnapper.trash.RemoveAt(i);
                                i--;
                                break;
                            }
                            else
                            {
                                Game.LogError("Could not find preset for " + t.preset);
                            }
                        }

                        kidnapper.trash.RemoveAt(i);
                        i--;
                    }

                    Game.Log("Chapter: Ready to execute murder...");

                    //Teleport murderer to street outside notewriter residence...
                    if (killer.currentBuilding != kidnapper.home.building && kidnapper.home.building.floors.ContainsKey(0))
                    {
                        Game.Log("Chapter: Teleporting killer " + killer.GetCitizenName() + " to notewriter's building ground floor lobby...");
                        killer.Teleport(killer.FindSafeTeleport(kidnapper.home.building.floors[0].GetLobbyAddress()), null);
                        killer.footstepDirt = 1f;
                    }

                    //Make sure killer has weapon...
                    if (murderWeapon.inInventory != killer)
                    {
                        murderWeapon.SetInInventory(killer);
                    }

                    killer.ai.EnableAI(true); //Enable killer AI

                    //Time for murder!
                    murder = MurderController.Instance.ExecuteNewMurder(killer, kidnapper, preset.crimePool[Toolbox.Instance.Rand(0, preset.crimePool.Count)], preset.MOPool[Toolbox.Instance.Rand(0, preset.MOPool.Count)]);

                    //Listen for murder complete...
                    murder.OnStateChanged += OnMurderStateChange;

                    setupFinalPhase = true;
                }

                if (killer.ai.currentGoal != null)
                {
                    float dist = Vector3.Distance(killer.transform.position, kidnapper.home.GetMainEntrance().worldAccessPoint);

                    if(dist < recordedDistance)
                    {
                        recordedDistance = dist;
                        if (startDistance >= 999f) startDistance = recordedDistance;
                        CityConstructor.Instance.loadingProgress = phase0LoadRatio + phase1LoadRatio + phase2LoadRatio * (1f - (recordedDistance / startDistance));
                    }
                }
            }

            //Exit the presim after this time if murder is ready
            if (murderPreSimPass /*&& (Game.Instance.devMode || preSimTimeElapsed >= ChapterController.Instance.loadedChapter.minimumPreSimLength)*/)
            {
                if(murderTime <= 0f)
                {
                    murderTime = SessionData.Instance.gameTime;
                }
                //Wait for 15 mins after murder time
                else if(SessionData.Instance.gameTime >= murderTime + 0.25f)
                {
                    Game.Log("Chapter: Pre sim complete! The time is " + SessionData.Instance.decimalClock);
                    CityConstructor.Instance.SetPreSim(false);
                    handlePreSim = false; //Stop coroutine loop
                }
            }

            yield return null;
        }
    }

    public override void OnGameWorldLoop()
    {
        base.OnGameWorldLoop();

        float timePassed = SessionData.Instance.gameTime - CitizenBehaviour.Instance.timeOnLastGameWorldUpdate;

        //Trigger movement tutorial
        if(ChapterController.Instance.currentPart == 1 && InteractionController.Instance.lockedInInteraction == null)
        {
            //Trigger tutorial
            SessionData.Instance.TutorialTrigger("movement");
        }

        //Spawn extra lockpicks if needed...
        if(playersStorageBox != null && playersStorageBox.locked)
        {
            int lockpickCount = GameplayController.Instance.lockPicks;
            lockpicksNeeded = Toolbox.Instance.GetLockpicksNeeded(playersStorageBox.val) + 2; //Add extra for safety

            if (lockpickCount < lockpicksNeeded)
            {
                foreach (NewNode n in apartment.nodes)
                {
                    List<Interactable> lp = n.interactables.FindAll(item => item.preset.name == "Paperclip" || item.preset.name == "Hairpin");
                    lockpickCount += lp.Count;
                    if (lockpickCount >= lockpicksNeeded) break;
                }

                if (lockpickCount < lockpicksNeeded)
                {
                    Game.Log("Chapter: Spawned extra paperclip...");

                    //Spawn extra paperclip...
                    Player.Instance.home.PlaceObject(InteriorControls.Instance.paperclip, null, null, null, out _, false, ignoreLimits: true, forceSecuritySettings: true, forcedSecurity: 0, forcedOwnership: InteractablePreset.OwnedPlacementRule.nonOwnedOnly);
                }
            }
        }

        //TODO: Notewriter calls kidnapper after they die...
        if(!lastCallPlaced && kidnapper.isDead)
        {
            //noteWriter.ai.CreateNewGoal(RoutineControls.Instance.makeSpecificCall, 0f, 0f, newPassedVar: kidnapper.humanID);
            lastCallPlaced = true;
        }

        //Skip apartment tutorial section if player is out of apartment
        if(ChapterController.Instance.currentPart >= 1 && ChapterController.Instance.currentPart <= 20 && !Player.Instance.playerKOInProgress)
        {
            if(Player.Instance.currentGameLocation != Player.Instance.home && Player.Instance.previousGameLocation != Player.Instance.home && triggeredTutorialSkip <= 0f)
            {
                triggeredTutorialSkip += 0.15f;

                Game.Log("Chapter: Asking to skip apartment tutorial section...");
                PopupMessageController.Instance.OnLeftButton += OnReturnToApartmentOption;
                PopupMessageController.Instance.OnRightButton += OnSkipAheadOption;
                PopupMessageController.Instance.OnOptionButton += OnCancelOption;
                PopupMessageController.Instance.PopupMessage("Tutorial skip", true, true, LButton: "Return to Apartment", RButton: "Skip to Next Tutorial Section", enableOptionButton: true, OButton: "Cancel");
            }
            else if(triggeredTutorialSkip > 0f)
            {
                triggeredTutorialSkip -= timePassed;
            }
        }

        //Make sure there are fingerprints to find
        if (Player.Instance.currentGameLocation == kidnapper.home)
        {
            foreach (NewRoom r in kidnapper.home.rooms)
            {
                foreach (FurnitureLocation f in r.individualFurniture)
                {
                    foreach (Interactable i in f.integratedInteractables)
                    {
                        //This can have prints
                        if (i.preset.fingerprintsEnabled && i.preset.enableDynamicFingerprints)
                        {
                            //Features an open or close action
                            if (i.preset.actionsPreset.Exists(item => item.actions.Exists(item => item.interactionName.ToLower() == "open" || item.interactionName.ToLower() == "close")))
                            {
                                if (!i.df.Exists(item => item.id == killer.humanID))
                                {
                                    Game.Log("Chapter: Adding 2x killer's prints to " + i.name + " on " + f.furniture.name);
                                    i.AddNewDynamicFingerprint(killer, Interactable.PrintLife.timed);
                                    i.AddNewDynamicFingerprint(killer, Interactable.PrintLife.timed);
                                }
                            }
                        }
                    }
                }
            }
        }

        //Player is in apartment when police arrive
        if(Player.Instance.currentGameLocation == kidnapper.home && !enforcerEventsTrigger)
        {
            if(GameplayController.Instance.enforcerCalls.ContainsKey(kidnapper.home))
            {
                if(GameplayController.Instance.enforcerCalls[kidnapper.home].state == GameplayController.EnforcerCallState.responding || GameplayController.Instance.enforcerCalls[kidnapper.home].state == GameplayController.EnforcerCallState.arrived)
                {
                    //Is a door being barged?
                    foreach (NewNode.NodeAccess acc in kidnapper.home.entrances)
                    {
                        foreach(NewNode.NodeSpace h in acc.fromNode.occupiedSpace)
                        {
                            Human human = h.occupier as Human;

                            if(human != null)
                            {
                                if(GameplayController.Instance.enforcerCalls[kidnapper.home].response.Contains(human.humanID))
                                {
                                    TriggerEscapeEvents();
                                    break;
                                }
                            }

                            if (enforcerEventsTrigger) break;
                        }

                        if (enforcerEventsTrigger) break;
                    }

                    if (!enforcerEventsTrigger)
                    {
                        foreach (NewNode.NodeAccess acc in kidnapper.home.entrances)
                        {
                            foreach (NewNode.NodeSpace h in acc.toNode.occupiedSpace)
                            {
                                Human human = h.occupier as Human;

                                if (human != null)
                                {
                                    if (GameplayController.Instance.enforcerCalls[kidnapper.home].response.Contains(human.humanID))
                                    {
                                        TriggerEscapeEvents();
                                        break;
                                    }
                                }

                                if (enforcerEventsTrigger) break;
                            }

                            if (enforcerEventsTrigger) break;
                        }
                    }
                }
            }
        }

        //Search the bin clue prompt
        if(ChapterController.Instance.currentPartName == "CrimeSceneClues")
        {
            if(restaurantReceipt != null && receiptSearchPromt)
            {
                if(!receiptSearchActivated)
                {
                    receiptSearchTimer += timePassed;

                    if(receiptSearchTimer > 0.37f)
                    {
                        receiptSearchActivated = true;

                        Objective.ObjectiveTrigger findReceipt = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", newEvidence: restaurantReceipt, newOrTrigger: true);

                        //Add delayed promt
                        if (receiptInBin)
                        {
                            AddObjective("Sometimes it's useful to search the trash!", findReceipt, true, pointerPosition: kidnapperBin.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.hand);
                        }
                        else
                        {
                            AddObjective("Thought I saw something over here...", findReceipt, true, pointerPosition: restaurantReceipt.interactable.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.hand);
                        }
                    }
                }
            }

            //Address book prompt
            if (kidnappersAddressBook != null && addressBookSearchPrompt)
            {
                if (!addressBookSearchActivated)
                {
                    addressBookSearchTimer += timePassed;

                    if (addressBookSearchTimer > 0.28f)
                    {
                        addressBookSearchActivated = true;

                        Objective.ObjectiveTrigger findAddressBook = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.inspectInteractable, "", newInteractable: kidnappersAddressBook, newOrTrigger: true);

                        AddObjective("It's worth checking out the address book...", findAddressBook, true, pointerPosition: kidnappersAddressBook.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.hand);
                    }
                }
            }

            //Scan for prints
            if (fingerprintPrompt)
            {
                if (!printSearchActivated)
                {
                    printSearchTimer += timePassed;

                    if (printSearchTimer > 0.5f)
                    {
                        printSearchActivated = true;

                        Objective.ObjectiveTrigger scanForPrints = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.findFingerprints, "", newEvidence: killer.evidenceEntry);

                        List<Interactable> killerPrints = new List<Interactable>();

                        foreach(NewNode n in kidnapper.home.nodes)
                        {
                            foreach(Interactable i in n.interactables)
                            {
                                if(i.df != null)
                                {
                                    if(i.df.Exists(item => item.id == killer.humanID))
                                    {
                                        killerPrints.Add(i);
                                    }
                                }
                            }
                        }

                        SessionData.Instance.TutorialTrigger("fingerprints");

                        for (int i = 0; i < Mathf.Min(4, killerPrints.Count); i++)
                        {
                            AddObjective("Scan for prints " + i.ToString(), scanForPrints, true, pointerPosition: killerPrints[i].GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.printScanner);
                        }

                        ControlsDisplayController.Instance.DisplayControlIconAfterDelay(1f, InteractablePreset.InteractionKey.WeaponSelect, "Inventory", InterfaceControls.Instance.controlIconDisplayTime + 2f);

                        PlayerVO("7c0dcfe5-f974-4ab8-bb0e-e3a5b526bad8", 0f);
                    }
                }
            }
        }

        //Restaurant pointer to passcode note
        if(ChapterController.Instance.currentPartName == "ArrivalDiner")
        {
            if(!triggeredPasscodeNoteHint)
            {
                if (passcodeNoteTimer < 0.4f)
                {
                    passcodeNoteTimer += timePassed;
                }
                else
                {
                    //Add other access pointers
                    if (restaurantBackroom.passcode != null)
                    {
                        foreach (int i in restaurantBackroom.passcode.notes)
                        {
                            Interactable note = null;

                            if (CityData.Instance.savableInteractableDictionary.TryGetValue(i, out note))
                            {
                                if (note.node.gameLocation == restaurant)
                                {
                                    if (note.node.room != restaurantBackroom)
                                    {
                                        Objective.ObjectiveTrigger findNote = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.inspectInteractable, "", newInteractable: note);
                                        AddObjective("You may be able to find the passcode on a note somewhere...", findNote, usePointer: true, pointerPosition: note.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.lookingGlass, delay: 0f);
                                        triggeredPasscodeNoteHint = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //Trigger murder of notewriter
        if((ChapterController.Instance.currentPart >= 31 || notewriterManualMurderTrigger) && !notewriterMurderTriggered && !killer.ai.isConvicted)
        {
            notewriterMurderTimer += timePassed;

            if(notewriterMurderTimer >= 2.5f || notewriterManualMurderTrigger)
            {
                Game.Log("Chapter: Notewriter murder triggered....");

                //Time for murder!
                murder2 = MurderController.Instance.ExecuteNewMurder(killer, noteWriter, preset.crimePool[Toolbox.Instance.Rand(0, preset.crimePool.Count)], preset.MOPool[Toolbox.Instance.Rand(0, preset.MOPool.Count)]);
                notewriterMurderTriggered = true;
            }
        }

        //Trigger asking player about next leads...
        if(ChapterController.Instance.currentPartName == "DisplayLeads" && !PopupMessageController.Instance.active && Player.Instance.speechController.speechQueue.Count <= 0 && thisCase != null && thisCase.caseStatus == Case.CaseStatus.handInCollected)
        {
            if(nextLeadDelay <= 0f)
            {
                ExecuteChangeLeadsManual();
                nextLeadDelay = 0.005f;
            }
            else
            {
                nextLeadDelay -= timePassed;
            }
        }

        if(killer.ai.isConvicted && layLowGoal != null)
        {
            layLowGoal.Remove(); //Finish laying low
        }

        //Finish when killer is convicted
        if(killer.ai.isConvicted && ChapterController.Instance.currentPart < 58)
        {
            if(endDelayTimer < 0.01f)
            {
                endDelayTimer += timePassed;
            }
            else
            {
                //End tutorial message
                //PopupMessageController.Instance.PopupMessage("tutorialend");

                ChapterController.Instance.LoadPart("ReturnHome");
            }
        }
    }

    public void OnReturnToApartmentOption()
    {
        PopupMessageController.Instance.OnLeftButton -= OnReturnToApartmentOption;
        PopupMessageController.Instance.OnRightButton -= OnSkipAheadOption;
        PopupMessageController.Instance.OnOptionButton -= OnCancelOption;

        Game.Log("Chapter: Teleporting back to apartment...");
        Player.Instance.Teleport(Player.Instance.FindSafeTeleport(playerBedroom), null);
        triggeredTutorialSkip = 0;
    }

    public void OnSkipAheadOption()
    {
        PopupMessageController.Instance.OnLeftButton -= OnReturnToApartmentOption;
        PopupMessageController.Instance.OnRightButton -= OnSkipAheadOption;
        PopupMessageController.Instance.OnOptionButton -= OnCancelOption;

        ClearAllObjectives();
        triggeredTutorialSkip = 0;

        Game.Log("Chapter: Skipping ahead...");
        ChapterController.Instance.LoadPart(21);
    }

    public void OnCancelOption()
    {
        PopupMessageController.Instance.OnLeftButton -= OnReturnToApartmentOption;
        PopupMessageController.Instance.OnRightButton -= OnSkipAheadOption;
        PopupMessageController.Instance.OnOptionButton -= OnCancelOption;

        Player.Instance.ClearAllDisabledActions();

        Game.Log("Chapter: Cancel...");
    }

    void ChooseInvestigatePhone()
    {
        PopupMessageController.Instance.OnLeftButton -= ChooseInvestigateCCTV;
        PopupMessageController.Instance.OnRightButton -= ChooseInvestigatePhone;
        PopupMessageController.Instance.OnLeftButton2 -= ChooseInvestigateVmails;
        PopupMessageController.Instance.OnRightButton2 -= ChooseInvestigateMurderWeapon;
        PopupMessageController.Instance.OnOptionButton -= ChooseCancelLeads;

        //Clear other objectives not on this lead
        bool cancelExisting = true;

        if(ChapterController.Instance.currentPartName == "AccessCabinet"
            || ChapterController.Instance.currentPartName == "TraceCall"
            || ChapterController.Instance.currentPartName == "SearchCallSource"
            || ChapterController.Instance.currentPartName == "AccessOtherAddress"
            || ChapterController.Instance.currentPartName == "InvesitgatePhone"
            )
        {
            cancelExisting = false;
        }

        ClearLeads(true, true, cancelExisting, true);

        ChapterController.Instance.LoadPart("InvesitgatePhone");

        Invoke("ChangeLeadTip", 4f);
    }

    void ChooseInvestigateCCTV()
    {
        PopupMessageController.Instance.OnLeftButton -= ChooseInvestigateCCTV;
        PopupMessageController.Instance.OnRightButton -= ChooseInvestigatePhone;
        PopupMessageController.Instance.OnLeftButton2 -= ChooseInvestigateVmails;
        PopupMessageController.Instance.OnRightButton2 -= ChooseInvestigateMurderWeapon;
        PopupMessageController.Instance.OnOptionButton -= ChooseCancelLeads;

        //Clear other objectives not on this lead
        bool cancelExisting = true;

        if (ChapterController.Instance.currentPartName == "InvestigateCCTV"
            || ChapterController.Instance.currentPartName == "ArrivalDiner"
            || ChapterController.Instance.currentPartName == "AccessBackroom"
            || ChapterController.Instance.currentPartName == "LaunchSurveillance"
            || ChapterController.Instance.currentPartName == "FoundRecords"
            || ChapterController.Instance.currentPartName == "KidnapperOnCam"
            || ChapterController.Instance.currentPartName == "OpenNotewirterEvidence"
            || ChapterController.Instance.currentPartName == "FindFlyer"
            )
        {
            cancelExisting = false;
        }

        ClearLeads(cancelExisting, true, true, true);

        ChapterController.Instance.LoadPart("InvestigateCCTV");

        Invoke("ChangeLeadTip", 4f);
    }

    void ChooseInvestigateVmails()
    {
        PopupMessageController.Instance.OnLeftButton -= ChooseInvestigateCCTV;
        PopupMessageController.Instance.OnRightButton -= ChooseInvestigatePhone;
        PopupMessageController.Instance.OnLeftButton2 -= ChooseInvestigateVmails;
        PopupMessageController.Instance.OnRightButton2 -= ChooseInvestigateMurderWeapon;
        PopupMessageController.Instance.OnOptionButton -= ChooseCancelLeads;

        //Clear other objectives not on this lead
        bool cancelExisting = true;

        if (ChapterController.Instance.currentPartName == "InvestigateVmails"
            || ChapterController.Instance.currentPartName == "ArrivalWorkplace"
            || ChapterController.Instance.currentPartName == "LaunchVmail"
            || ChapterController.Instance.currentPartName == "AccessKidnapperCruncher"
            || ChapterController.Instance.currentPartName == "FoundNotewriterID"
            || ChapterController.Instance.currentPartName == "AccessNotewriterCruncher"
            )
        {
            cancelExisting = false;
        }

        ClearLeads(true, cancelExisting, true, true);

        ChapterController.Instance.LoadPart("InvestigateVmails");

        Invoke("ChangeLeadTip", 4f);
    }

    void ChooseInvestigateMurderWeapon()
    {
        PopupMessageController.Instance.OnLeftButton -= ChooseInvestigateCCTV;
        PopupMessageController.Instance.OnRightButton -= ChooseInvestigatePhone;
        PopupMessageController.Instance.OnLeftButton2 -= ChooseInvestigateVmails;
        PopupMessageController.Instance.OnRightButton2 -= ChooseInvestigateMurderWeapon;
        PopupMessageController.Instance.OnOptionButton -= ChooseCancelLeads;

        //Clear other objectives not on this lead
        bool cancelExisting = true;

        if (ChapterController.Instance.currentPartName == "InvestigateMurderWeapon"
            || ChapterController.Instance.currentPartName == "SearchWeaponsDealer"
            || ChapterController.Instance.currentPartName == "SearchOtherAddress"
            )
        {
            cancelExisting = false;
        }

        ClearLeads(true, true, true, cancelExisting);

        ChapterController.Instance.LoadPart("InvestigateMurderWeapon");

        Invoke("ChangeLeadTip", 4f);
    }

    void ChooseCancelLeads()
    {
        PopupMessageController.Instance.OnLeftButton -= ChooseInvestigateCCTV;
        PopupMessageController.Instance.OnRightButton -= ChooseInvestigatePhone;
        PopupMessageController.Instance.OnLeftButton2 -= ChooseInvestigateVmails;
        PopupMessageController.Instance.OnRightButton2 -= ChooseInvestigateMurderWeapon;
        PopupMessageController.Instance.OnOptionButton -= ChooseCancelLeads;

        ChapterController.Instance.LoadPart("CancelLeads");

        Invoke("ChangeLeadTip", 4f);
    }

    public void ChangeLeadTip()
    {
        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "tip-changelead"), newIcon: InterfaceControls.Icon.questionMark);
    }

    private void OnDestroy()
    {
        PopupMessageController.Instance.OnLeftButton -= ChooseInvestigateCCTV;
        PopupMessageController.Instance.OnRightButton -= ChooseInvestigatePhone;
        PopupMessageController.Instance.OnLeftButton2 -= ChooseInvestigateVmails;
        PopupMessageController.Instance.OnRightButton2 -= ChooseInvestigateMurderWeapon;
    }

    public void OnMurderStateChange(MurderController.MurderState newState)
    {
        //Stop pre sim on murder execute
        if(newState == MurderController.MurderState.executing)
        {
            Game.Log("Chapter: Murder is complete...");
            murderPreSimPass = true;

            //Reset tick rates
            noteWriter.ai.disableTickRateUpdate = false;
            kidnapper.ai.disableTickRateUpdate = false;
            killer.ai.disableTickRateUpdate = false;

            murder.OnStateChanged -= OnMurderStateChange;
        }
    }

    private void PickCharacters()
    {
        //GeneratePartner();

        //We need two work friends, one to be the kidnapper, the other to be the worried friend who writes the note...
        //We also need a killer associate who will go on to track down and kill both
        List<IntoCharacterPick> charPicks = new List<IntoCharacterPick>();

        //Pick killer from preset criminals
        killer = CityData.Instance.criminalJobDirectory.Find(item => item.preset.name == "HiredKiller").employee;
        killerID = killer.humanID;

        foreach(GroupsController.SocialGroup socGroup in GroupsController.Instance.groups)
        {
            if (socGroup.preset == "PokerClub")
            {
                if(!socGroup.members.Contains(killer.humanID))
                {
                    socGroup.members.Add(killer.humanID);
                    killer.groups.Add(socGroup);
                }

                killerBar = socGroup.GetMeetingPlace();
                killerBarID = killerBar.id;
            }
            else if (socGroup.preset == "RedGumsMeetup")
            {
                if (!socGroup.members.Contains(killer.humanID))
                {
                    socGroup.members.Add(killer.humanID);
                    killer.groups.Add(socGroup);
                }

                redGumMeet = socGroup.GetMeetingPlace();

                if (redGumMeet != null)
                {
                    redGumMeetID = redGumMeet.id;
                }

                if(killerBar == null)
                {
                    killerBar = socGroup.GetMeetingPlace();
                    killerBarID = killerBar.id;
                }
            }
            else if (socGroup.preset == "GunClub")
            {
                if (!socGroup.members.Contains(killer.humanID))
                {
                    socGroup.members.Add(killer.humanID);
                    killer.groups.Add(socGroup);
                }

                if (killerBar == null)
                {
                    killerBar = socGroup.GetMeetingPlace();
                    killerBarID = killerBar.id;
                }
            }
        }

        if(killerBar == null)
        {
            Company rBar = CityData.Instance.companyDirectory.Find(item => item.preset.name == "Bar");

            if(rBar != null)
            {
                killerBar = rBar.placeOfBusiness.thisAsAddress;
                killerBarID = killerBar.id;
            }
        }

        if(weaponSeller == null)
        {
            Company weap = CityData.Instance.companyDirectory.Find(item => item.preset.name == "WeaponsDealer");

            if(weap != null)
            {
                weaponSeller = weap.placeOfBusiness.thisAsAddress;
                weaponSellerID = weaponSeller.id;
            }
        }

        if(murderWeapon == null)
        {
            murderWeapon = killer.inventory.Find(item => item.preset == InteriorControls.Instance.handgun);

            if (murderWeapon == null)
            {
                foreach (NewRoom r in killer.home.rooms)
                {
                    foreach (NewNode n in r.nodes)
                    {
                        foreach (Interactable i in n.interactables)
                        {
                            if (i.preset == InteriorControls.Instance.handgun)
                            {
                                Game.Log("Chapter: Murder weapon found in apartment of killer...");
                                murderWeapon = i;
                                break;
                            }
                        }

                        if (murderWeapon != null) break;
                    }
                    if (murderWeapon != null) break;
                }

                if (murderWeapon == null)
                {
                    Game.LogError("Unable to locate murder weapon!");
                }
            }
            else Game.Log("Chapter: Murder weapon found in inventory of killer...");
        }

        //Bar tab should be placed where the poker tournament is held
        Game.Log("Chapter: Lopping killer social groups...");

        foreach (GroupsController.SocialGroup socGroup in killer.groups)
        {
            Game.Log("Chapter: ..." + socGroup.preset);
        }

        foreach (Citizen cit in CityData.Instance.citizenDirectory)
        {
            //Must have a job
            if (cit.job == null || cit.job.employer == null) continue;
            if (cit.job.employer.preset.name != "MediumOffice") continue;
            if (cit.home == null) continue;

            //Must work at a non public-facing location & own a desk
            if (cit.job.employer.preset.publicFacing && cit.job.preset.ownsWorkPosition && cit.job.preset.jobPostion == InteractablePreset.SpecialCase.workDesk) continue;

            //Skip if chosen for a side job...
            if (SideJobController.Instance.exemptFromPosters.Contains(cit)) continue;
            if (SideJobController.Instance.exemptFromPurps.Contains(cit)) continue;

            List<Citizen> workFriends = new List<Citizen>();

            //Acquaintances
            List<Acquaintance> validAcq = cit.acquaintances.FindAll(item => item.connections.Contains(Acquaintance.ConnectionType.boss) || item.connections.Contains(Acquaintance.ConnectionType.workOther) || item.connections.Contains(Acquaintance.ConnectionType.workTeam) || item.connections.Contains(Acquaintance.ConnectionType.familiarWork));
            if (validAcq.Count <= 0) continue;

            //Find one of these I most know/like
            float knowLike = -99999;
            Acquaintance other = null;

            foreach(Acquaintance aq in validAcq)
            {
                if (SideJobController.Instance.exemptFromPosters.Contains(aq.with)) continue;
                if (SideJobController.Instance.exemptFromPurps.Contains(aq.with)) continue;

                //Must also have a job with a desk
                if (aq.with.job.preset == null) continue;
                if (!aq.with.job.preset.ownsWorkPosition || aq.with.job.preset.jobPostion != InteractablePreset.SpecialCase.workDesk) continue;
                if (aq.with.home == null) continue;

                float evaluation = aq.known + aq.like - aq.with.humility - aq.with.emotionality;

                if (evaluation > knowLike)
                {
                    knowLike = evaluation;
                    other = aq;
                }
            }

            if (other == null) continue; //We couldn't find a work friend that isn't featured in jobs...
            else
            {
                //Boost known/like
                other.known = Mathf.Max(other.known, SocialControls.Instance.telephoneBookInclusionThreshold);
                other.like = Mathf.Max(other.known, 0.5f);
            }

            //We have our couple...
            IntoCharacterPick newPick = new IntoCharacterPick();

            //Which one is the kidnapper, which one is the worried friend: Use lowest humility and emotionality
            float eval1 = cit.humility + cit.emotionality;
            float eval2 = other.with.humility + other.with.emotionality;

            if(eval1 <= eval2)
            {
                newPick.kidnapper = cit;
                newPick.noteWriter = other.with;
            }
            else
            {
                newPick.noteWriter = cit;
                newPick.kidnapper = other.with;
            }

            newPick.score = (1f - newPick.kidnapper.humility) + (1f - newPick.kidnapper.emotionality) + newPick.noteWriter.humility; //The best score is one with one person being like the above, but the other having more honesty
            if (newPick.kidnapper.partner == null) newPick.score += 0.5f; //Bonus if the kidnapper is single as we don't have to deal with their partner

            //Live at different location to player
            if (newPick.kidnapper.home.building != apartment.building)
            {
                newPick.score += 3f;
            }

            if (newPick.kidnapper.home.building != newPick.noteWriter.home.building)
            {
                newPick.score += 1f;
            }

            if(newPick.kidnapper.home.rooms.Count >= 4 && newPick.kidnapper.home.rooms.Count <= 6)
            {
                newPick.score += 2f;
            }

            if(newPick.kidnapper.home.rooms.Exists(item => item.individualFurniture.Exists(item => item.furnitureClasses[0].name == "1x1Safe")))
            {
                newPick.score += 2f;
            }

            if(newPick.kidnapper.partner == null)
            {
                newPick.score += 2f;
            }

            if(newPick.noteWriter.partner == null)
            {
                newPick.score += 0.5f;
            }

            charPicks.Add(newPick);
        }

        //Hopefully we have some good picks now, sort them.
        charPicks.Sort((p1, p2) => p2.score.CompareTo(p1.score)); //Using P2 first gives highest first

        noteWriter = charPicks[0].noteWriter;
        noteWriterID = noteWriter.humanID;

        kidnapper = charPicks[0].kidnapper;
        kidnapperID = kidnapper.humanID;

        //Make sure notewriter and kidnapper know killer
        if(!kidnapper.FindAcquaintanceExists(killer, out _))
        {
            kidnapper.AddAcquaintance(killer, 0.6f, Acquaintance.ConnectionType.friend, true, false);
        }

        //Pick slophouse owner
        List<Citizen> slophouseOwnerPicks = CityData.Instance.citizenDirectory.FindAll(item => item.home != null && item != noteWriter && item != kidnapper && item != killer && !item.isPlayer && item.home != kidnapper.home && item.home != killer.home && item.societalClass > 0.1f && item.societalClass < 0.8f);
        slophouseOwner = slophouseOwnerPicks[Toolbox.Instance.GetPsuedoRandomNumber(0, slophouseOwnerPicks.Count, noteWriter.citizenName + kidnapper.citizenName)];
        slopHouseOwnerID = slophouseOwner.humanID;

        //if (!noteWriter.FindAcquaintanceExists(killer, out _))
        //{
        //    noteWriter.AddAcquaintance(killer, 0.6f, Acquaintance.ConnectionType.friend, true, false);
        //}

        Game.Log("Chapter: Kidnapper is " + kidnapper.GetCitizenName());
        Game.Log("Chapter: Note Writer is " + noteWriter.GetCitizenName());
        Game.Log("Chapter: Killer is " + killer.GetCitizenName());
        Game.Log("Chapter: Slophouse owner is " + slophouseOwner.GetCitizenName());
    }

    //private void GeneratePartner()
    //{
    //    //Create partner
    //    partner = this.gameObject.AddComponent<Citizen>();
    //    partner.SetResidence(apartment.residence);
    //    partner.citizenName = "Sam" + " " + "Merriweather";
    //    partner.name = partner.citizenName;
    //    partner.firstName = "Sam";
    //    partner.surName = "Merriweather";
    //    partner.humanID = 999999999;
    //    //partner.casualName = "Sam";
    //    //if(partner.firstName.Length > 1) partner.initialledName = partner.firstName.Substring(0, 1) + ". " + partner.surName;
    //    //else partner.initialledName = partner.firstName + ". " + partner.surName;

    //    //Set password
    //    partner.passcode = new GameplayController.Passcode(GameplayController.PasscodeType.citizen);
    //    partner.passcode.digits = new List<int>();
    //    string birthMonth = Game.Instance.playerBirthMonth.ToString();

    //    foreach (char c in birthMonth)
    //    {
    //        int pass = 0;
    //        int.TryParse(c.ToString(), out pass);
    //        partner.passcode.digits.Add(pass);
    //    }

    //    string birthDay = Game.Instance.playerBirthDay.ToString();

    //    foreach (char c in birthDay)
    //    {
    //        int pass = 0;
    //        int.TryParse(c.ToString(), out pass);
    //        partner.passcode.digits.Add(pass);
    //    }
    //}

    //These part functions are automatically invoked when parts are loaded
    //Part 0
    public void TurnOnLight(int passedVar)
    {
        Player.Instance.energy = 0.7f;
        Player.Instance.hydration = 0.7f;
        Player.Instance.nourishment = 0.7f;

        InterfaceController.Instance.fade = 1f;
        InterfaceController.Instance.Fade(0f, 12f, true);

        AudioController.Instance.PassWeatherParams();

        //Remove print scanners
        FirstPersonItemController.Instance.RemoveSpecificStaticSlot(FirstPersonItemController.InventorySlot.StaticSlot.printReader);

        //Disable get up, sleep etc
        Player.Instance.SetActionDisable("Get In", true);
        Player.Instance.SetActionDisable("Get Up", true);
        //Player.Instance.SetActionDisable("Enter", true);
        Player.Instance.SetActionDisable("Sleep...", true);
        Player.Instance.SetActionDisable("Call", true);

        //Disable city directory tutorial on look...
        SessionData.Instance.tutorialTextTriggered.Add("citydirectory");
        SessionData.Instance.tutorialTextTriggered.Add("evidence");

        //Forced locked in interaction with clost sleep
        List<Interactable> sleep = new List<Interactable>();

        foreach (Interactable inter in bed.integratedInteractables)
        {
            //inter.SetActionDisable("Sleep", true); //Disable get up for now
            //inter.SetActionDisable("Get Up", true); //Disable get up for now

            if (inter.pt == (int)InteractableController.InteractableID.B || inter.pt == (int)InteractableController.InteractableID.C)
            {
                sleep.Add(inter);
            }
        }

        //Put player in bed
        closestSleep = sleep[0];
        float closest = 9999f;

        foreach (Interactable inter in sleep)
        {
            float dist = Vector3.Distance(inter.node.position, Player.Instance.transform.position);

            if (dist < closest)
            {
                closest = dist;
                closestSleep = inter;
            }
        }

        //Set part location
        SetCurrentPartLocation(closestSleep.node);

        InteractablePreset.InteractionAction action = closestSleep.preset.GetActions().Find(item => item.action == RoutineControls.Instance.hide);
        InteractionController.Instance.currentLookingAtInteractable = closestSleep.controller;
        closestSleep.OnInteraction(action, Player.Instance); //Activate by simulating input

        //Chapter message
        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.gameHeader, 0, Strings.Get("ui.chapters", "Chapter I"), newMessageDelay: 3);
        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.gameHeader, 0, SessionData.Instance.CurrentTimeString(false, true) + ", " + SessionData.Instance.LongDateString(SessionData.Instance.gameTime, true, true, true, true, true, false, false, true) + ", " + Strings.Get("ui.interface", "Your Apartment"));

        PlayerVO("9992d16f-2d69-4821-bd31-2e8333f0d043", 16f);
        PlayerVO("2bd1b9db-eefb-4cc5-ab0a-7e629c99b33d", 1f);

        //Add light objective
        Interactable closestLight = null;
        List<Interactable> otherLights = new List<Interactable>();
        float bestDist = 999999f;

        foreach(NewNode n in bed.anchorNode.room.nodes)
        {
            foreach(Interactable i in n.interactables)
            {
                if(i.preset == InteriorControls.Instance.bedsideLamp)
                {
                    float dist = Vector3.Distance(i.GetWorldPosition(), CameraController.Instance.cam.transform.position);

                    if(dist < bestDist && closestLight != i)
                    {
                        if(closestLight != null && !otherLights.Contains(closestLight))
                        {
                            otherLights.Add(closestLight); //Add previous lights to this list
                        }

                        closestLight = i;
                        bestDist = dist;
                    }
                }
            }
        }

        //Find the closest bedside light to the player...
        //Bedside cabinets in the bedroom
        //losestLight = Toolbox.Instance.FindClosestObjectTo(InteriorControls.Instance.bedsideLamp, Player.Instance.cam.transform.position, null, null, playerBedroom, out _);

        if (closestLight != null)
        {
            Vector3 lightPointer = closestLight.GetWorldPosition() + new Vector3(0f, 0.25f, 0);
            //if (closestLight.controller != null) lightPointer = closestLight.controller.transform.position + new Vector3(0f, 0.25f, 0);

            //Add turn on light objective
            Objective.ObjectiveTrigger turnOnLight = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.switchStateTrueForType, "", newInteractable: closestLight, newGameLocation: apartment, newHighlightAction: "Turn On");
            AddObjective("Turn on the bedside light", turnOnLight, usePointer: true, pointerPosition: lightPointer, onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.hand);

            //Add silent objectives for other lights
            for (int i = 0; i < otherLights.Count; i++)
            {
                Interactable otherLight = otherLights[i];

                Objective.ObjectiveTrigger turnOnLightOther = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.switchStateTrueForType, "", newInteractable: otherLight, newGameLocation: apartment);
                AddObjective("Turn on the bedside light " + i, turnOnLightOther, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, isSilent: true, chapterString: "FindPartner");
            }
        }
        else
        {
            ChapterController.Instance.SkipToNextPart();
        }
    }

    public void FindPartner(int passedVar)
    {
        ClearAllObjectives();
        Player.Instance.SetActionDisable("Get Up", false);

        //Get up
        if(closestSleep != null)
        {
            closestSleep.SetActionHighlight("Get Up", true);
        }

        //Objective.ObjectiveTrigger getUp = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.playerAction, "Get Up", true, 0, newInteractable: closestSleep, newHighlightAction: "Get Up");
        List<Objective.ObjectiveTrigger> trigList = new List<Objective.ObjectiveTrigger>();

        PlayerVO("42c76822-6efe-4510-a70d-28b9f07c6fa8", 0f, true);
        PlayerVO("e4b4c96c-e858-4197-a31f-afedf5b3ca87", 0f, true);

        SetCurrentPartLocation(playerBedroom.GetRandomEntranceNode());

        if (playerLounge != null)
        {
            //Set part location
            SetCurrentPartLocation(playerLounge.GetRandomEntranceNode());

            NewWall closestLightswitch = null;
            float closest = 9999f;

            foreach(NewWall wall in playerLounge.lightswitches)
            {
                float dist = Vector3.Distance(playerLounge.GetRandomEntranceNode().position, wall.position);

                if(dist < closest)
                {
                    closest = dist;
                    closestLightswitch = wall;
                }
            }

            if(closestLightswitch != null)
            {
                Objective.ObjectiveTrigger loungeLight = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.roomLightOn, "", newRoom: playerLounge, newInteractable: closestLightswitch.lightswitchInteractable, newHighlightAction: "Turn On");
                trigList.Add(loungeLight);

                AddObjective("Turn on the light switch in the lounge", loungeLight, usePointer: true, pointerPosition: closestLightswitch.lightswitchInteractable.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nothing, delay: 1f, useIcon: InterfaceControls.Icon.hand);
            }
        }

        if (playerKitchen != null)
        {
            //Set part location
            SetCurrentPartLocation(playerKitchen.GetRandomEntranceNode());

            NewWall closestLightswitch = null;
            float closest = 9999f;
            
            foreach (NewWall wall in playerKitchen.lightswitches)
            {
                float dist = Vector3.Distance(playerKitchen.GetRandomEntranceNode().position, wall.position);
                if (dist < closest)
                {
                    closest = dist;
                    closestLightswitch = wall;
                }
            }

            if(closestLightswitch != null)
            {
                Objective.ObjectiveTrigger kitchenLight = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.roomLightOn, "", newRoom: playerKitchen, newInteractable: closestLightswitch.lightswitchInteractable, newHighlightAction: "Turn On");
                trigList.Add(kitchenLight);

                AddObjective("Turn on the light switch in the kitchen", kitchenLight, usePointer: true, pointerPosition: closestLightswitch.lightswitchInteractable.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nothing, delay: 1f, useIcon: InterfaceControls.Icon.hand);
            }
        }

        AddObjective("Turned on lights", trigList, false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "TelephoneRing", isSilent: true);

        PlayerVO("3d5bc60d-a4de-42a6-bf8f-b71c15b9aa5c", 4f, true);
        //PlayerVO("|partnerheshe| didn't say she was working late...",0f, true);
        //PlayerVO("Something's wrong...", 0f);
    }

    public void TelephoneRing(int passedVar)
    {
        if (closestSleep != null)
        {
            closestSleep.SetActionHighlight("Get Up", false);
        }

        ControlsDisplayController.Instance.disableControlDisplay.Remove(InteractablePreset.InteractionKey.flashlight);

        //Set part location
        if (playerLounge != null)
        {
            SetCurrentPartLocation(playerLounge.GetRandomEntranceNode());
        }
        else if(playerKitchen != null)
        {
            SetCurrentPartLocation(playerKitchen.GetRandomEntranceNode());
        }
        else SetCurrentPartLocation(playerBedroom.GetRandomEntranceNode());

        //ControlsDisplayController.Instance.DisplayControlIcon(InteractablePreset.InteractionKey.notebook, "Notebook", 4.5f);

        //Slight delay until phone is rung
        float delay = 0f;
        if (!loadedFromSave) delay = Toolbox.Instance.Rand(1.5f, 2.5f);

        InvokeAfterDelay("RingPhone", delay);
    }

    //Delay until this happens
    private void RingPhone()
    {
        Game.Log("Chapter: Ring player's apartment...");

        //Ring the player's flat
        //Pick a payphone...
        List<Telephone> allPhones = new List<Telephone>(CityData.Instance.phoneDictionary.Values);
        List<Telephone> payPhones = allPhones.FindAll(item => item.interactable != null && item.interactable.preset.isPayphone && item.interactable.node.gameLocation.thisAsStreet != null);
        Telephone chosenPhone = allPhones[Toolbox.Instance.Rand(0, allPhones.Count)];

        if(payPhones.Count > 0)
        {
            chosenPhone = payPhones[Toolbox.Instance.Rand(0, payPhones.Count)];
        }
        else
        {
            payPhones = allPhones.FindAll(item => item.interactable != null && item.interactable.preset.isPayphone);

            if (payPhones.Count > 0)
            {
                chosenPhone = payPhones[Toolbox.Instance.Rand(0, payPhones.Count)];
            }
        }

        TelephoneController.Instance.CreateNewCall(chosenPhone, apartment.telephones[0], null, Player.Instance, new TelephoneController.CallSource(TelephoneController.CallType.audioEvent, preset.audioEvents[0]), 999999f);

        Objective.ObjectiveTrigger answerTelephone = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.answerPhone, "", newGameLocation: apartment, newInteractable: apartment.telephones.FirstOrDefault().interactable, newHighlightAction: "Pick Up");
        AddObjective("Answer the telephone", answerTelephone, usePointer: true, pointerPosition: apartment.telephones.FirstOrDefault().interactable.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.hand, delay: 1.5f);
    }

    public void AnswerCall(int passedVar)
    {
        //Door note
        if (note == null)
        {
            Vector3 worldPos = Vector3.Lerp(interiorDoorNode.position, apartmentEntrance.worldAccessPoint, 0.6f);
            note = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.note, noteWriter, noteWriter, Player.Instance, worldPos, new Vector3(0, Toolbox.Instance.Rand(0f, 360f), 0), null, null, ddsOverride: "bc6fabb0-8ae4-44a1-b1e0-c8305a575b89");

            //Add fingerprints
            note.AddNewDynamicFingerprint(noteWriter, Interactable.PrintLife.manualRemoval);
            note.AddNewDynamicFingerprint(noteWriter, Interactable.PrintLife.manualRemoval);
            note.AddNewDynamicFingerprint(noteWriter, Interactable.PrintLife.manualRemoval);
        }

        PlayerVO("71c93b28-2bce-4915-afff-0635df4811c9", 0f, useParsing: true, interupt: true);
        PlayerVO("5f4b466c-b4e0-40aa-961c-fb168a0606be", 0f, useParsing: true, interupt: true);

        //Set part location
        if (playerLounge != null)
        {
            SetCurrentPartLocation(playerLounge.GetRandomEntranceNode());
        }
        else if (playerKitchen != null)
        {
            SetCurrentPartLocation(playerKitchen.GetRandomEntranceNode());
        }
        else SetCurrentPartLocation(playerBedroom.GetRandomEntranceNode());

        Objective.ObjectiveTrigger answerAndEnd = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.answerPhoneAndEndCall, "", newGameLocation: Player.Instance.home, newInteractable: Player.Instance.home.telephones.FirstOrDefault().interactable);
        AddObjective("End Telephone Call", answerAndEnd, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextChapterPart, isSilent: true);
    }

    public void SomethingWrong(int passedVar)
    {
        //SessionData.Instance.tutorialTextTriggered.Remove("evidence");
        //Game.Instance.disableCaseBoardClose = false;

        //Set part location
        if (playerLounge != null)
        {
            SetCurrentPartLocation(playerLounge.GetRandomEntranceNode());
        }
        else if (playerKitchen != null)
        {
            SetCurrentPartLocation(playerKitchen.GetRandomEntranceNode());
        }
        else SetCurrentPartLocation(playerBedroom.GetRandomEntranceNode());

        //Check note
        Objective.ObjectiveTrigger findNote = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.inspectInteractable, "", newInteractable: note, newHighlightAction: "Inspect");
        AddObjective("Search the apartment for clues", findNote, usePointer: false, pointerPosition: note.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "OpenName");

        AddObjective("Check the front door", findNote, usePointer: true, pointerPosition: note.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "OpenName", delay: 20f, removePrevious: true, allowCrouchPromt: true);
    }

    public void OpenName(int passedVar)
    {
        ClearAllObjectives();

        //Set part location
        if (playerLounge != null)
        {
            SetCurrentPartLocation(playerLounge.GetRandomEntranceNode());
        }
        else if (playerKitchen != null)
        {
            SetCurrentPartLocation(playerKitchen.GetRandomEntranceNode());
        }
        else SetCurrentPartLocation(playerBedroom.GetRandomEntranceNode());

        List<string> skipBlocks = new List<string>();
        skipBlocks.Add("35aac0af-338b-4c63-8843-1f987a68e336");
        PopupMessageController.Instance.TutorialMessage("caseboard", newSkipBlocks: skipBlocks); //Display popup tutorial section: Case board

        //Add pointer to the name on the note
        InfoWindow noteWindow = InterfaceController.Instance.activeWindows.Find(item => item.passedEvidence == note.evidence);

        if (noteWindow != null)
        {
            //Game.Instance.disableCaseBoardClose = true;

            //Disable closing and pinning in this instance
            noteWindow.forceDisableClose = true;
            noteWindow.forceDisablePin = true;

            LinkButtonController lbc = noteWindow.GetComponentInChildren<LinkButtonController>();

            GameObject newPointer = Instantiate(PrefabControls.Instance.tutorialPointer, lbc.rect);
            pointer = newPointer.GetComponent<RectTransform>();

            pointer.sizeDelta = lbc.rect.sizeDelta;
        }
        //else Game.Instance.disableCaseBoardClose = false;

        Objective.ObjectiveTrigger pinCitizen = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.evidenceOpenAndDisplayed, "", newEvidence: kidnapper.evidenceEntry);
        AddObjective("Select the name '|kidnappername|' on the note", pinCitizen, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.pin);
    }

    public void PinCitizen(int passedVar)
    {
        if (pointer != null) Destroy(pointer.gameObject); //Remove pointer
        ClearAllObjectives();

        //Set part location
        if (playerLounge != null)
        {
            SetCurrentPartLocation(playerLounge.GetRandomEntranceNode());
        }
        else if (playerKitchen != null)
        {
            SetCurrentPartLocation(playerKitchen.GetRandomEntranceNode());
        }
        else SetCurrentPartLocation(playerBedroom.GetRandomEntranceNode());

        //Remove pointer to the name on the note
        InfoWindow noteWindow = InterfaceController.Instance.activeWindows.Find(item => item.passedEvidence == note.evidence);

        if (noteWindow != null)
        {
            //Game.Instance.disableCaseBoardClose = true;

            //Disable closing and pinning in this instance
            noteWindow.forceDisableClose = true;
            noteWindow.forceDisablePin = true;

            LinkButtonController lbc = noteWindow.GetComponentInChildren<LinkButtonController>();

            if(lbc != null)
            {
                JuiceController juice = lbc.GetComponentInChildren<JuiceController>();

                if(juice != null)
                {
                    Destroy(juice.gameObject);
                }
            }
        }

        //Add pointer to note's window pin
        InfoWindow citizenWindow = InterfaceController.Instance.activeWindows.Find(item => item.passedEvidence == kidnapper.evidenceEntry);

        if (citizenWindow != null)
        {
            //Game.Instance.disableCaseBoardClose = true;
            citizenWindow.forceDisableClose = true;

            PinFolderButtonController pbc = citizenWindow.pinButton;

            GameObject newPointer = Instantiate(PrefabControls.Instance.tutorialPointer, pbc.rect);
            pointer = newPointer.GetComponent<RectTransform>();

            pointer.sizeDelta = pbc.rect.sizeDelta;
        }

        if(noteWindow == null && citizenWindow)
        {
            //Game.Instance.disableCaseBoardClose = false;
        }

        Objective.ObjectiveTrigger pinCitizen = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.evidencePinned, "", newEvidence: kidnapper.evidenceEntry);
        AddObjective("Pin |kidnappername| to your case board", pinCitizen, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.pin);
    }

    public void PinNote(int passedVar)
    {
        if (pointer != null) Destroy(pointer.gameObject); //Remove pointer
        ClearAllObjectives();

        //Set part location
        if (playerLounge != null)
        {
            SetCurrentPartLocation(playerLounge.GetRandomEntranceNode());
        }
        else if (playerKitchen != null)
        {
            SetCurrentPartLocation(playerKitchen.GetRandomEntranceNode());
        }
        else SetCurrentPartLocation(playerBedroom.GetRandomEntranceNode());

        //Add pointer to note's window pin
        InfoWindow noteWindow = InterfaceController.Instance.activeWindows.Find(item => item.passedEvidence == note.evidence);

        if(noteWindow != null)
        {
            //Game.Instance.disableCaseBoardClose = true;

            noteWindow.forceDisableClose = true;
            noteWindow.forceDisablePin = false;

            PinFolderButtonController pbc = noteWindow.pinButton;

            GameObject newPointer = Instantiate(PrefabControls.Instance.tutorialPointer, pbc.rect);
            pointer = newPointer.GetComponent<RectTransform>();

            pointer.sizeDelta = pbc.rect.sizeDelta;
        }
        //else Game.Instance.disableCaseBoardClose = false;

        Objective.ObjectiveTrigger pinNote = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.evidencePinned, "", newEvidence: note.evidence);
        AddObjective("Pin the note to your case board", pinNote, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.pin);
    }

    public void CloseCaseBoard1(int passedVar)
    {
        if(pointer != null) Destroy(pointer.gameObject); //Remove pointer

        //Game.Instance.disableCaseBoardClose = false;

        //Set part location
        if (playerLounge != null)
        {
            SetCurrentPartLocation(playerLounge.GetRandomEntranceNode());
        }
        else if (playerKitchen != null)
        {
            SetCurrentPartLocation(playerKitchen.GetRandomEntranceNode());
        }
        else SetCurrentPartLocation(playerBedroom.GetRandomEntranceNode());

        //Reset close of all windows
        foreach(InfoWindow w in InterfaceController.Instance.activeWindows)
        {
            w.forceDisableClose = false;
            w.forceDisablePin = false;
        }

        //Lookup
        Objective.ObjectiveTrigger closeCaseBoard = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.gameUnpaused, "");
        AddObjective("Close the case board", closeCaseBoard, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.pin);

        //SessionData.Instance.TutorialTrigger("caseboard");
    }

    public void InspectCityDirectory(int passedVar)
    {
        if(pointer != null)
        {
            Destroy(pointer.gameObject); //Remove pointer
        }

        //Disable city directory tutorial on look...
        //SessionData.Instance.tutorialTextTriggered.Remove("citydirectory");

        PlayerVO("cec04341-f8a0-4878-9b56-f2d7c54ad773", 0f, useParsing: true);
        PlayerVO("917078a7-1fc6-4414-8274-bcac940c55fc", 0f, useParsing: true);

        SetCurrentPartLocation(cityDir.node);

        //Lookup
        Objective.ObjectiveTrigger openDirectory = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.inspectInteractable, "", newInteractable: cityDir, newHighlightAction: "Inspect");
        AddObjective("Inspect the City Directory", openDirectory, usePointer: true, pointerPosition: cityDir.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nextChapterPart, allowCrouchPromt: true, useIcon: InterfaceControls.Icon.hand);
    }

    public void AddressLookup(int passedVar)
    {
        SetCurrentPartLocation(cityDir.node);

        //Add a glow to the correct tab
        InfoWindow cd = InterfaceController.Instance.activeWindows.Find(item => item.preset == InteriorControls.Instance.cityDirectory.useSingleton.windowStyle);

        if (cd != null)
        {
            string noteWriterLastInitial = kidnapper.GetSurName();
            if(noteWriterLastInitial.Length > 1) noteWriterLastInitial = noteWriterLastInitial.Substring(0, 1);

            bool found = false;

            foreach (WindowTabController tab in cd.tabs)
            {
                for (int i = 0; i < tab.name.Length; i++)
                {
                    string letter = tab.name;

                    try
                    {
                        letter = tab.name.Substring(i, 1);
                    }
                    catch
                    {

                    }

                    if (noteWriterLastInitial.ToLower() == letter.ToLower())
                    {
                        glow = tab.rect.gameObject.AddComponent<PulseGlowController>();
                        glow.originalColour = tab.rect.gameObject.GetComponent<Image>().color;
                        glow.imageToGlow = glow.gameObject.GetComponentInChildren<Image>();
                        glow.lerpColour = Color.white;
                        glow.SetGlow(true);
                        glow.glowActive = true;

                        found = true;
                        break;
                    }
                }

                if (found) break;
            }
        }

        //Lookup
        Objective.ObjectiveTrigger findAddress = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.evidenceOpenAndDisplayed, "", newEvidence: kidnapper.home.evidenceEntry);
        AddObjective("Look up |notewritername| in your City Directory", findAddress, usePointer: true, pointerPosition: cityDir.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nextChapterPart);
    }

    public void PinAddress(int passedVar)
    {
        if (pointer != null && pointer.gameObject != null) Destroy(pointer.gameObject);
        if(glow != null) Destroy(glow); //Remove tab glow

        SetCurrentPartLocation(cityDir.node);

        //Add pointer to note's window pin
        InfoWindow cd = InterfaceController.Instance.activeWindows.Find(item => item.passedEvidence == kidnapper.home.evidenceEntry);

        if (cd != null)
        {
            PinFolderButtonController pbc = cd.pinButton;

            GameObject newPointer = Instantiate(PrefabControls.Instance.tutorialPointer, pbc.rect);
            pointer = newPointer.GetComponent<RectTransform>();

            pointer.sizeDelta = pbc.rect.sizeDelta;
        }

        List<Objective.ObjectiveTrigger> trigList = new List<Objective.ObjectiveTrigger>();

        //Lookup
        Objective.ObjectiveTrigger pinAddress = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.evidencePinned, "", newEvidence: kidnapper.home.evidenceEntry);
        AddObjective("Pin |notewriteraddress| to your case board", pinAddress, usePointer: true, pointerPosition: cityDir.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.pin);
        trigList.Add(pinAddress);

        //Lookup
        Objective.ObjectiveTrigger closeCaseBoard = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.gameUnpaused, "");
        AddObjective("Close the case board", closeCaseBoard, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.pin);
        trigList.Add(closeCaseBoard);

        if (trigList.Count > 0)
        {
            AddObjective("Close the case board 2", trigList, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextChapterPart, isSilent: true);
        }
        else ChapterController.Instance.LoadPart(ChapterController.Instance.currentPart + 1);

        SessionData.Instance.TutorialTrigger("citydirectory");
    }

    public void Pickup(int passedVar)
    {
        ClearAllObjectives();

        //Enable all actions at this point
        Player.Instance.ClearAllDisabledActions();

        if (key != null)
        {
            SetCurrentPartLocation(playersStorageBox.node);
        }

        PlayerVO("baf5a368-15fe-429a-b1e4-3c8118e2ebf9", 0f, useParsing: false);

        //Go to the locked box
        try
        {
            Objective.ObjectiveTrigger proxyTrigger = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToNode, "", newNode: playersStorageBox.node);
            AddObjective("Find old detective equipment", proxyTrigger, usePointer: true, pointerPosition: detectiveStuff.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.hand);
        }
        catch
        {

        }
    }

    public void AcquireLockpicks(int passedVar)
    {
        //Enable all actions at this point
        Player.Instance.ClearAllDisabledActions();

        if (playersStorageBox != null)
        {
            SetCurrentPartLocation(playersStorageBox.node);
        }

        //Flashlight prompt
        ControlsDisplayController.Instance.DisplayControlIcon(InteractablePreset.InteractionKey.flashlight, "Flashlight", InterfaceControls.Instance.controlIconDisplayTime);

        PlayerVO("84e52f42-6ef8-4734-9ae6-56e50023666c", 0f, useParsing: true);
        PlayerVO("166933ae-6cf4-484f-942c-1e2efdd1f99f", 0f, useParsing: true);

        Objective.ObjectiveTrigger acquireLockpicks = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.acquireLockpicks, "", newInteractable: playersStorageBox);
        AddObjective("Look for hairpins or paperclips to pick the lock", acquireLockpicks, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.lockpick);

        if(hairpin == null) hairpin = Toolbox.Instance.FindClosestObjectTo(InteriorControls.Instance.hairpin, Player.Instance.transform.position, null, apartment, null, out _);
        if (paperclip == null) paperclip = Toolbox.Instance.FindClosestObjectTo(InteriorControls.Instance.paperclip, Player.Instance.transform.position, null, apartment, null, out _);

        if (hairpin != null)
        {
            Objective.ObjectiveTrigger pickupHairpin = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.interactableRemoved, "", newInteractable: hairpin);
            AddObjective("Pick up hairpin", pickupHairpin, usePointer: true, pointerPosition: hairpin.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nothing, delay: 40f, useIcon: InterfaceControls.Icon.hand);
        }

        if(paperclip != null)
        {
            Objective.ObjectiveTrigger pickupPaperclip = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.interactableRemoved, "", newInteractable: paperclip);
            AddObjective("Pick up paperclip", pickupPaperclip, usePointer: true, pointerPosition: paperclip.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nothing, delay: 60f, useIcon: InterfaceControls.Icon.hand);
        }
    }

    public void UnlockBox(int passedVar)
    {
        ClearAllObjectives();

        //Enable all actions at this point
        Player.Instance.ClearAllDisabledActions();

        if (playersStorageBox != null)
        {
            SetCurrentPartLocation(playersStorageBox.node);
        }

        List<string> skipBlocks = new List<string>();
        skipBlocks.Add("143e3260-8728-47a0-b623-223be3bcb855");
        PopupMessageController.Instance.TutorialMessage("lockpicking", newSkipBlocks: skipBlocks); //Display popup tutorial section: Lockpicking

        Objective.ObjectiveTrigger unlockBox = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.unlockInteractable, "", newInteractable: playersStorageBox);
        AddObjective("Unlock the box", unlockBox, usePointer: true, pointerPosition: playersStorageBox.lockInteractable.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.lockpick);
    }

    public void GatherItems(int passedVar)
    {
        ClearAllObjectives();

        //Enable all actions at this point
        Player.Instance.ClearAllDisabledActions();

        if (playersStorageBox != null)
        {
            SetCurrentPartLocation(playersStorageBox.node);
        }

        //Pickup handcuffs
        //if (handcuffs != null)
        //{
        //    Objective.ObjectiveTrigger pickupHandcuffs = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.interactableRemoved, "", newInteractable: handcuffs, newHighlightAction: "Take");
        //    AddObjective("Pick up Handcuffs", pickupHandcuffs, usePointer: true, pointerPosition: handcuffs.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.hand);
        //    trigList.Add(pickupHandcuffs);
        //}

        //Pickup print scanner
        Objective.ObjectiveTrigger pickupPrintReader = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.interactableRemoved, "", newInteractable: detectiveStuff, newHighlightAction: "Take");
        AddObjective("Pick up Equipment", pickupPrintReader, usePointer: true, pointerPosition: detectiveStuff.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.hand);

        ////Pickup lockpick kit
        //if (lockpickKit != null)
        //{
        //    Objective.ObjectiveTrigger pickupLockpickKit = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.interactableRemoved, "", newInteractable: lockpickKit, newHighlightAction: "Take");
        //    AddObjective("Pick up Lockpick Kit", pickupLockpickKit, usePointer: true, pointerPosition: lockpickKit.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.hand);
        //    trigList.Add(pickupLockpickKit);
        //}

        //AddObjective("Pick up Equipment", pickupPrintReader, usePointer: true, onCompleteAction: Objective.OnCompleteAction.nextChapterPart, isSilent: true);
    }

    public void TakeKey(int passedVar)
    {
        SetCurrentPartLocation(cityDir.node);

        PopupMessageController.Instance.TutorialMessage("inventory");

        if (key != null)
        {
            Objective.ObjectiveTrigger pickupKey = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.keyInventory, "", newDoor: apartmentEntrance.door, newInteractable: key, newHighlightAction: "Take");
            AddObjective("Pick up the key to your apartment", pickupKey, usePointer: true, pointerPosition: key.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.hand);
        }
        else ChapterController.Instance.LoadPart(ChapterController.Instance.currentPart + 1);
    }

    public void SetRouteOpenCaseBoard(int passedVar)
    {
        SetCurrentPartLocation(cityDir.node);

        Objective.ObjectiveTrigger openDestination = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.evidenceOpenAndDisplayed, "", newEvidence: kidnapper.home.evidenceEntry);
        AddObjective("Open the case board and select the pinned |kidnapperaddress|", openDestination, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.hand);
    }

    public void SetRoute(int passedVar)
    {
        if(pointer != null) Destroy(pointer.gameObject);
        if (glow != null) Destroy(glow); //Remove tab glow

        SetCurrentPartLocation(cityDir.node);

        //Add pointer to the plot route button
        InfoWindow cd = InterfaceController.Instance.activeWindows.Find(item => item.passedEvidence == kidnapper.home.evidenceEntry);

        if (cd != null)
        {
            EvidenceLocationalControls locControls = cd.GetComponentInChildren<EvidenceLocationalControls>();

            if(locControls != null && locControls.plotRouteButton != null)
            {
                GameObject newPointer = Instantiate(PrefabControls.Instance.tutorialPointer, locControls.plotRouteButton.rect);
                pointer = newPointer.GetComponent<RectTransform>();

                pointer.anchoredPosition = new Vector2(-4, -4);
                pointer.sizeDelta = locControls.plotRouteButton.rect.sizeDelta + new Vector2(8, 8);
            }
        }

        //Lookup
        Objective.ObjectiveTrigger setRoute = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.plotRoute, "", newGameLocation: kidnapper.home);
        AddObjective("Plot a route to |notewriteraddress| on your map", setRoute, usePointer: false, pointerPosition: cityDir.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.location);
    }

    public void CloseCaseBoard2(int passedVar)
    {
        if(pointer != null) Destroy(pointer.gameObject); //Remove pointer
        if(glow != null) Destroy(glow); //Remove tab glow

        SetCurrentPartLocation(cityDir.node);

        //Lookup
        Objective.ObjectiveTrigger closeCaseBoard = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.gameUnpaused, "");
        AddObjective("Close the case board by pressing space", closeCaseBoard, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.pin);
    }

    public void UnlockFrontDoor(int passedVar)
    {
        SetCurrentPartLocation(apartmentEntrance.fromNode);

        //Pickup key
        Objective.ObjectiveTrigger unlockDoor = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.unlockDoor, "", newDoor: apartmentEntrance.door, newInteractable: apartmentEntrance.door.handleInteractable, newHighlightAction: "Unlock");

        //Door handle world position
        Vector3 handlePos = apartmentEntrance.door.transform.TransformPoint(new Vector3(-0.5f, 1f, 0.05f));

        AddObjective("Unlock the door to your apartment", unlockDoor, usePointer: true, pointerPosition: handlePos, onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.hand);
    }

    public void FindNoteWriter(int passedVar)
    {
        //Enable all actions at this point
        Player.Instance.ClearAllDisabledActions();

        //Set part location
        SetCurrentPartLocation(apartmentEntrance.toNode);

        //Make sure detective stuff has been picked up...
        if(detectiveStuff != null && !detectiveStuff.rem)
        {
            ActionController.Instance.TakeDetectiveStuff(detectiveStuff, detectiveStuff.node, Player.Instance);
        }

        if(Game.Instance.demoMode && Game.Instance.demoChapterSkip)
        {
            //Give items
            GameplayController.Instance.AddMoney(100, false, "demo mode");
            GameplayController.Instance.AddLockpicks(30, false);
            //if(handcuffs != null) FirstPersonItemController.Instance.PickUpItem(handcuffs);
            if (detectiveStuff != null) FirstPersonItemController.Instance.PickUpItem(detectiveStuff);
            Player.Instance.AddToKeyring(Player.Instance.home, false);

            //Door note
            if (note == null)
            {
                Vector3 worldPos = Vector3.Lerp(interiorDoorNode.position, apartmentEntrance.worldAccessPoint, 0.6f);
                note = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.note, noteWriter, noteWriter, Player.Instance, worldPos, new Vector3(0, Toolbox.Instance.Rand(0f, 360f), 0), null, null, ddsOverride: "bc6fabb0-8ae4-44a1-b1e0-c8305a575b89");

                //Add fingerprints
                note.AddNewDynamicFingerprint(noteWriter, Interactable.PrintLife.manualRemoval);
                note.AddNewDynamicFingerprint(noteWriter, Interactable.PrintLife.manualRemoval);
                note.AddNewDynamicFingerprint(noteWriter, Interactable.PrintLife.manualRemoval);
            }

            //Pin evidence
            CasePanelController.Instance.PinToCasePanel(CasePanelController.Instance.activeCase, note.evidence, Evidence.DataKey.name, true);
            CasePanelController.Instance.PinToCasePanel(CasePanelController.Instance.activeCase, kidnapper.evidenceEntry, Toolbox.Instance.GetList<Evidence.DataKey>(Evidence.DataKey.name, Evidence.DataKey.address), true);
            CasePanelController.Instance.PinToCasePanel(CasePanelController.Instance.activeCase, kidnapper.home.evidenceEntry, Evidence.DataKey.name, true);

            //Set route
            MapController.Instance.PlotPlayerRoute(kidnapper.home);
        }

        //Start timer msg
        if(Game.Instance.timeLimited && Game.Instance.startTimerAfterApartmentExit)
        {
            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "The demo timer starts now..."));
        }

        Objective.ObjectiveTrigger goToAddress = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToNode, "", newNode: kidnappersDoorNode);
        AddObjective("Go to |notewriteraddress|", goToAddress, usePointer: true, pointerPosition: kidnappersEntrance.wall.position + new Vector3(0, 1.3f, 0), onCompleteAction: Objective.OnCompleteAction.nextChapterPart, useIcon: InterfaceControls.Icon.run);

        //Skip adead to inside address
        Objective.ObjectiveTrigger getInside = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: kidnapper.home);
        AddObjective("Find a way inside...", getInside, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterWhenAllCompleted, chapterString: "InvestigateWriterAddress", isSilent: true);

        //Spawn key under door...
        Interactable findKey = kidnappersDoorNode.interactables.Find(item => item.name == "Key");

        if(findKey == null)
        {
            List<Interactable.Passed> passed = new List<Interactable.Passed>();
            passed.Add(new Interactable.Passed(Interactable.PassedVarType.roomID, kidnappersEntrance.fromNode.room.roomID));
            findKey = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.key, kidnapper, null, null, kidnappersDoorNode.position, Vector3.zero, passed, null);
        }

        //Find and turn on television
        if(Toolbox.Instance.Rand(0f, 1f) < 0.7f)
        {
            FurnitureLocation kidnapperTV = Toolbox.Instance.FindFurnitureWithinGameLocation(kidnapper.home, InteriorControls.Instance.television, out _);

            if(kidnapperTV != null)
            {
                if(kidnapperTV.integratedInteractables.Count > 0)
                {
                    kidnapperTV.integratedInteractables[0].SetSwitchState(true, null);
                }
            }
        }

        //Find and turn on phone table lamp
        FurnitureLocation kidnapperTelephoneTable = Toolbox.Instance.FindFurnitureWithinGameLocation(kidnapper.home, InteriorControls.Instance.telephoneTable, out _);

        if(kidnapperTelephoneTable != null)
        {
            Interactable deskLamp = kidnapperTelephoneTable.spawnedInteractables.Find(item => item.preset == InteriorControls.Instance.deskLamp);

            if(deskLamp != null)
            {
                deskLamp.SetSwitchState(true, null);
            }
        }
    }

    public void Knock(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(kidnappersDoorNode);

        //Make sure it's locked
        kidnappersDoor.SetOpen(0f, null, true);
        kidnappersDoor.SetLocked(true, null, false);

        Objective.ObjectiveTrigger knock = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.playerAction, "KnockOnDoor", newInteractable: kidnappersDoor.doorInteractable, newDoor: kidnappersDoor, newHighlightAction: "Knock");
        AddObjective("Knock on the door", knock, usePointer: true, pointerPosition: kidnappersDoor.doorInteractable.GetWorldPosition() + new Vector3(0, 1.3f, 0), onCompleteAction: Objective.OnCompleteAction.nextChapterPart);

        Objective.ObjectiveTrigger getInside = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: kidnapper.home);
        AddObjective("Find a way inside...", getInside, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterWhenAllCompleted, chapterString: "InvestigateWriterAddress", isSilent: true);
    }

    public void FindWayInside(int passedVar)
    {
        ClearAllObjectives();

        //Set part location
        SetCurrentPartLocation(kidnappersDoorNode);

        Objective.ObjectiveTrigger getInside = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: kidnapper.home);
        AddObjective("Find a way inside...", getInside, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "InvestigateWriterAddress");

        PopupMessageController.Instance.TutorialMessage("breakingandentering"); //Display popup tutorial section: Breaking and entering

        //Add options
        AddObjective("Hint: You can pick the lock by looking at the handle", getInside, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "InvestigateWriterAddress");
        //AddObjective("Option 2: Find a ventilation duct", getInside, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "InvestigateWriterAddress");
        //AddObjective("Hint: Barge the door open", getInside, usePointer: false, pointerPosition: kidnappersDoor.wall.door.handleInteractable.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "InvestigateWriterAddress");
    }

    public void InvestigateWriterAddress(int passedVar)
    {
        ClearAllObjectives();

        //Set part location
        SetCurrentPartLocation(kidnappersDoorNode);

        Invoke("EscapeTutorial", 3f);

        Objective.ObjectiveTrigger findBody = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToRoom, "", newRoom: kidnapper.currentRoom);
        AddObjective("Investigate the property", findBody, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "FoundBody");

        //Setup objective to find escape routes
        List<AirDuctGroup.AirVent> vents = new List<AirDuctGroup.AirVent>();

        foreach(NewRoom r in kidnapper.home.rooms)
        {
            vents.AddRange(r.airVents);
        }

        Game.Log("Chapter: Found " + vents.Count + " vents...");

        if(vents.Count > 0)
        {
            //NewNode getNode = vents[0].node;
            //if (vents[0].ventType == NewAddress.AirVent.wallLower || vents[0].ventType == NewAddress.AirVent.wallUpper) getNode = vents[0].roomNode;
            for (int i = 0; i < Mathf.Min(vents.Count, 3); i++)
            {
                if(vents[i].spawned != null)
                {
                    Interactable v = vents[i].spawned.interactable;

                    Objective.ObjectiveTrigger findVent = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToNode, "", newNode: v.node);
                    AddObjective("Look around for vents for a potential quick exit " + i, findVent, usePointer: true, pointerPosition: v.GetWorldPosition(), useIcon: InterfaceControls.Icon.lookingGlass, onCompleteAction: Objective.OnCompleteAction.nothing);
                }
            }
        }
    }

    void EscapeTutorial()
    {
        PopupMessageController.Instance.TutorialMessage("escaping"); //Display popup tutorial section: Escaping
    }

    public void FoundBody(int passedVar)
    {
        PlayerVO("fb466dcb-e473-426e-8219-0c7734c56dd6", 0f);
        //PlayerVO("What happened here?!", 0f);
        //PlayerVO("Looks like a hitjob. Six shots when one would do. Whoever did this sure wasn’t taking any chances.", 0f);
        //PlayerVO("This could be |Notewriterfirstname|? No way to be sure without an ID.", 0f, useParsing: true);

        //Invoke("FoundBodyTutorial", 5f);

        //Set part location
        SetCurrentPartLocation(noteWriter.currentNode);

        //Crime scene checklist
        List<Objective.ObjectiveTrigger> trigList = new List<Objective.ObjectiveTrigger>();

        Objective.ObjectiveTrigger findID = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.linkImageWithName, "", newEvidence: kidnapper.evidenceEntry);
        trigList.Add(findID);
        AddObjective("Identify the body", findID, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nothing);

        //Get time of death...
        Objective.ObjectiveTrigger time = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.discoverEvidence, "", newEvidence: kidnapper.death.GetTimeOfDeathEvidence());
        trigList.Add(time);
        AddObjective("Find time of death", time, usePointer: true, pointerPosition: kidnapper.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.upperTorso).position, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.skull);

        //if(noteWriter.murderWeapon != null)
        //{
        //    Objective.ObjectiveTrigger cause = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", 1f, newEvidence: noteWriter.murderWeapon);
        //    trigList.Add(cause);
        //    AddObjective("Find cause of death", cause, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Instance.pin);
        //}

        //Invisible objective with trigger for next part
        Objective.ObjectiveTrigger nextPhase = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.noMoreObjectives, "");
        AddObjective("NextPhase", nextPhase, false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "CrimeSceneClues", isSilent: true);

        //Add warn notewriter dialog
        Game.Log("Chapter: Attempting to add notewriter dialog...");
        noteWriter.evidenceEntry.AddDialogOption(Evidence.DataKey.name, ChapterController.Instance.loadedChapter.dialogEvents[1], allowPresetDuplicates: false);
        notewriterDialogAdded = true;
    }

    public void CrimeSceneClues(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(kidnapper.currentNode);

        //Send enforcers to player's apartment
        //if (apartment != null)
        //{
        //    GameplayController.Instance.CallEnforcers(apartment, true); //Force a crime scene here
        //}

        //Make sure player has a fingerprint scanner
        //if(!FirstPersonItemController.Instance.firstPersonItems.Contains(InteriorControls.Instance.printReader.fpsItem))
        //{
        //    FirstPersonItemController.Instance.AddFirstPersonItem(InteriorControls.Instance.printReader.fpsItem, switchToNew: false);
        //}

        PlayerVO("93f06c61-37dd-4061-a31e-c63a6ead82b3", 0f, true);
        PlayerVO("a1d82a1b-2137-4fb8-8d04-7cd41fcaa29d", 0f, true);
        //PlayerVO("And I should scan for fingerprints that don't belong to the victim...", 0f, true);
        Invoke("PrintsTutorial", 8f);

        //Find evidence list
        List<Objective.ObjectiveTrigger> findEvidenceTriggers = new List<Objective.ObjectiveTrigger>();
        List<Objective.ObjectiveTrigger> nextChapterTriggers = new List<Objective.ObjectiveTrigger>();

        //This is required
        if (restaurantReceipt == null)
        {
            foreach (NewRoom r in kidnapper.home.rooms)
            {
                foreach(NewNode n in r.nodes)
                {
                    foreach(Interactable i in n.interactables)
                    {
                        //Set time on the receipt to the meet up time...
                        if(i.pv != null)
                        {
                            Interactable.Passed pv = i.pv.Find(item => item.varType == Interactable.PassedVarType.time);
                            if (pv != null) pv.value = meetTime;
                        }

                        if (i.preset == InteriorControls.Instance.receipt)
                        {
                            EvidenceReceipt ev = i.evidence as EvidenceReceipt;

                            if(ev != null)
                            {
                                if(ev.soldHere == restaurant.company)
                                {
                                    Game.Log("Chapter: Found restaurant receipt!");
                                    receiptInBin = false;
                                    restaurantReceipt = ev;

                                    //Set time on the receipt to the meet up time...
                                    ev.purchasedTime = meetTime;

                                    break;
                                }
                            }
                        }
                        else
                        {
                            EvidenceMultiPage ev = i.evidence as EvidenceMultiPage;

                            if(ev != null)
                            {
                                foreach(EvidenceMultiPage.MultiPageContent content in ev.pageContent)
                                {
                                    if(content.meta > 0)
                                    {
                                        MetaObject meta = CityData.Instance.FindMetaObject(content.meta);

                                        if(meta != null)
                                        {
                                            if (meta.preset == InteriorControls.Instance.receipt.name)
                                            {
                                                //Set time on the receipt to the meet up time...
                                                if(meta.passed != null)
                                                {
                                                    Interactable.Passed mpv = meta.passed.Find(item => item.varType == Interactable.PassedVarType.time);
                                                    if (mpv != null) mpv.value = meetTime;
                                                }

                                                foreach (Interactable.Passed p in meta.passed)
                                                {
                                                    if(p.varType == Interactable.PassedVarType.companyID)
                                                    {
                                                        if ((int)p.value == restaurant.company.companyID)
                                                        {
                                                            Game.Log("Chapter: Found restaurant receipt!");
                                                            restaurantReceipt = meta.GetEvidence(true, i.node.nodeCoord);
                                                            receiptInBin = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (restaurantReceipt != null) break;
                                }
                            }
                        }
                    }

                    if (restaurantReceipt != null) break;
                }

                if (restaurantReceipt != null) break;
            }
        }

        //Find restaurant receipt OR calendar entry
        if(restaurantReceipt != null)
        {
            Objective.ObjectiveTrigger findReceipt = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", newEvidence: restaurantReceipt, newOrTrigger: true);
            findEvidenceTriggers.Add(findReceipt);
            nextChapterTriggers.Add(findReceipt);

            AddObjective("FindReceipt", findReceipt, false, onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "FindReceipt", isSilent: true);

            receiptSearchPromt = true;
        }

        //Find address book
        if (kidnappersAddressBook != null)
        {
            Objective.ObjectiveTrigger findAddressBook = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.inspectInteractable, "", newInteractable: kidnappersAddressBook);
            findEvidenceTriggers.Add(findAddressBook);
            nextChapterTriggers.Add(findAddressBook);

            AddObjective("FindAddressBook", findAddressBook, false, onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "FindAddressBook", isSilent: true);

            addressBookSearchPrompt = true;
        }

        //Find restaurant receipt OR calendar entry
        if (kidnappersCalendar != null && meetingTimeEvidence != null)
        {
            Objective.ObjectiveTrigger findCalendar = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", newEvidence: meetingTimeEvidence, newOrTrigger: true);
            findEvidenceTriggers.Add(findCalendar);
            nextChapterTriggers.Add(findCalendar);

            AddObjective("FindCalendar", findCalendar, false, onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "FindCalendar", isSilent: true);
        }

        //Find work ID
        //if (workID != null)
        //{
        //    Game.Log("Chapter: Work ID placed in apartment: " + workID.name + " placed in " + workID.node.room.name);

        //    Objective.ObjectiveTrigger findWorkID = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", newEvidence: workID.evidence);
        //    findEvidenceTriggers.Add(findWorkID);
        //    nextChapterTriggers.Add(findWorkID);

        //    AddObjective("FindWorkID", findWorkID, usePointer: false, onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "FindWorkID", isSilent: true);
        //}

        //A silent objective that triggers a player comment but it otherwise unessential
        if(meetingNote != null)
        {
            if(meetingNote.name != null && meetingNote.node != null && meetingNote.node.room != null) Game.Log("Chapter: Meeting note: " + meetingNote.name + " placed in " + meetingNote.node.room.GetName());
            Objective.ObjectiveTrigger meetingNoteObj = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", newEvidence: meetingNote.evidence);
            AddObjective("FindMeetingNote", meetingNoteObj, usePointer: false, onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "FindMeetingNote", isSilent: true);
        }

        //A silent objective that triggers a player comment but it otherwise unessential OLD
        //if (playerAddressNote != null)
        //{
        //    Game.Log("Chapter: Player address note: " + playerAddressNote.name + " placed in " + playerAddressNote.node.room.name);
        //    Objective.ObjectiveTrigger addressNote = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", newEvidence: playerAddressNote.evidence);
        //    AddObjective("FindAddressNote", addressNote, usePointer: false, onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "FindAddressNote", isSilent: true);
        //}

        //Clue search: Find all of above
        AddObjective("Search for clues at the crime scene", findEvidenceTriggers, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nothing);

        //Try for most recent number
        if (kidnapperPhone != null)
        {
            Objective.ObjectiveTrigger getRecentCaller = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.checkRecentCalls, "", newInteractable: kidnapperPhone);
            nextChapterTriggers.Add(getRecentCaller);

            AddObjective("Check for recent calls", getRecentCaller, usePointer: true, pointerPosition: kidnapperPhone.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.telephone);
        }

        //Get fingerprints from the victim
        Objective.ObjectiveTrigger getVictimPrints = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.identifyFinerprints, "", newEvidence: kidnapper.evidenceEntry);
        nextChapterTriggers.Add(getVictimPrints);

        AddObjective("Get victim's prints", getVictimPrints, usePointer: true, pointerPosition: kidnapper.rightHandInteractable.controller.transform.position, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.hand);

        //Scan for fingerprints
        Objective.ObjectiveTrigger scanForPrints = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.findFingerprints, "", newEvidence: killer.evidenceEntry);
        nextChapterTriggers.Add(scanForPrints);
        fingerprintPrompt = true;

        //Prioritise safe...
        bool showPrintsLocation = false;
        Vector3 printPos = Vector3.zero;

        foreach (NewNode n in kidnapper.home.nodes)
        {
            foreach (Interactable i in n.interactables)
            {
                if (i.df != null && i.preset.name == "Safe")
                {
                    if (i.df.Exists(item => item.id == killer.humanID))
                    {
                        printPos = i.GetWorldPosition() + new Vector3(0, 0.1f, 0);
                        showPrintsLocation = true;
                    }
                }
            }
        }

        NewNode printNode = null;

        if(!showPrintsLocation)
        {
            foreach (NewNode n in kidnapper.home.nodes)
            {
                foreach (Interactable i in n.interactables)
                {
                    if (i.df != null)
                    {
                        if (i.df.Exists(item => item.id == killer.humanID))
                        {
                            if(printNode == null || i.sw0)
                            {
                                printPos = i.GetWorldPosition() + new Vector3(0, 0.1f, 0);
                                printNode = i.node;
                                showPrintsLocation = true;
                            }
                        }
                    }
                }
            }
        }

        AddObjective("Scan for fingerprints", scanForPrints, usePointer: showPrintsLocation, pointerPosition: printPos + new Vector3(0f, 0.25f, 0f), onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "FindPrints", useIcon: InterfaceControls.Icon.printScanner);

        //Display inventory prompt when close to fingerprints
        Objective.ObjectiveTrigger nearPrints = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToNode, "", newNode: printNode);
        AddObjective("InventoryPrompt", nearPrints, false, onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "InventoryPrompt", isSilent: true);

        //Next chapter when all above are complete
        AddObjective("Police Knock", nextChapterTriggers, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "PoliceCall", isSilent: true);
    }

    public void PrintsTutorial()
    {
        PopupMessageController.Instance.TutorialMessage("fingerprints"); //Display popup tutorial section: Fingerprints
    }

    private void InventoryPrompt()
    {
        ControlsDisplayController.Instance.DisplayControlIconAfterDelay(1f, InteractablePreset.InteractionKey.WeaponSelect, "Inventory", InterfaceControls.Instance.controlIconDisplayTime + 2f);
    }

    public void FindMeetingNote()
    {
        PlayerVO("1de86551-31de-4439-9ab5-24f2341eb33a", 0f, true);
        discoveredWeaponsDealer = true;
    }

    public void FindAddressNote()
    {
        PlayerVO("d199791d-4ebb-4af4-bdce-39174d791d51", 0f, true);
    }

    public void FindAddressBook()
    {
        //TODO: Invoke tutorial
    }

    public void FindWorkID()
    {
        PlayerVO("84f8c4a0-45eb-442a-90d7-20bb440f42c6", 0f, true);
    }

    public void FindReceipt()
    {
        PlayerVO("62c546bc-f8f3-47f8-b141-baba4ec2ebcc", 0f, true);
        receiptSearchActivated = true;
        //PlayerVO("Too much food for one person. |notewriterfirstname| likely met someone here...", 0f, true);
    }

    public void FindCalendar()
    {
        PlayerVO("77e7055c-cf8d-42fe-a41a-8675377cff27", 0f, true);
        //PlayerVO("Too much food for one person. |notewriterfirstname| likely met someone here...", 0f, true);
    }

    public void FindPrints()
    {
        printSearchActivated = true;
        PlayerVO("add6b43c-d21f-4bda-a020-f3883fd5f9e0", 0f, true);
    }

    public void PoliceCall(int passedVar)
    {
        ClearAllObjectives();

        //Set part location
        SetCurrentPartLocation(kidnapper.currentNode);

        //Set reportable
        kidnapper.unreportable = false;

        //Auto lock door
        kidnappersDoor.SetOpen(0, null, true);
        kidnappersDoor.SetLocked(true, null, false);
        Game.Log("Chapter: Locking kidnappers door for enforcer sequence...");

        //Send police and immediately teleport them outside
        GameplayController.Instance.CallEnforcers(kidnapper.home, immediateTeleport: true);

        //kidnapper.home.building.SetAlarm(true, Player.Instance, kidnapper.home.floor);

        //Set player wanted in building
        //StatusController.Instance.SetWantedInBuilding(kidnapper.home.building, GameplayControls.Instance.buildingWantedTime);

        PlayerVO("24c59b68-e667-4d37-ba6a-c84ca103e620", 1f);
        PlayerVO("a9015c83-9133-4dc8-b219-b6500a8c7e66", 1f);

        //Optional objective to search for more clues
        //Objective.ObjectiveTrigger searchMore = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.escapeGameLocation, "", newGameLocation: noteWriter.home);
        //AddObjective("Search for more clues", searchMore, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "DisplayLeads");

        //Give choice of staying or going...
        //PlayerVO("659b84c4-062a-4751-9540-dd560ad45c03", 3f);
        //PlayerVO("2303ad0e-6bb0-4d59-9137-2503ec8a4a63", 3f);

        Invoke("StealthTutorial", 12f);

        Game.Log("The kidnapper home is " + kidnapper.home.name);
        Objective.ObjectiveTrigger escape = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.escapeBuilding, "", newGameLocation: kidnapper.home);
        AddObjective("Escape the apartment", escape, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "CollectHandIn", delay: 1f, useIcon: InterfaceControls.Icon.run);

        //SessionData.Instance.TutorialTrigger("enforcers");
        //Objective.ObjectiveTrigger silentAlarm = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: kidnapper.home.floor.GetLobbyAddress());
        //AddObjective("SilentAlarm", silentAlarm, usePointer: false, onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "SilentAlarm", isSilent: true);
    }

    void StealthTutorial()
    {
        PopupMessageController.Instance.TutorialMessage("custom_stealthcombat"); //Display popup tutorial section: Stealth/combat
    }

    //void SilentAlarm()
    //{
    //    Player.Instance.currentBuilding.SetAlarm(true, Player.Instance, Player.Instance.currentGameLocation.floor);

    //    PlayerVO("24c59b68-e667-4d37-ba6a-c84ca103e620", 0f);
    //    PlayerVO("a5a85ff9-4a94-423e-825c-7ef81ee4d963", 0f);

    //    //Set reportable
    //    kidnapper.unreportable = false;

    //    Objective.ObjectiveTrigger escape = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.escapeBuilding, "", newGameLocation: kidnapper.home);
    //    AddObjective("Use venilation ducts to escape, or wait for the enforcers to leave.", escape, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "CollectHandIn", delay: 3f, useIcon: InterfaceControls.Icon.run);

    //    SessionData.Instance.TutorialTrigger("enforcers");
    //}

    private void TriggerEscapeEvents()
    {
        //For this objective, point towards under the bed but any hiding place is valid
        //NewRoom kidnapperBedroom = kidnapper.home.rooms.Find(item => item.preset == InteriorControls.Instance.bedroom);
        //if (kidnapperBedroom == null) kidnapperBedroom = kidnapper.home.rooms.Find(item => item.preset == InteriorControls.Instance.lounge);
        //FurnitureLocation kidnapperBed = kidnapperBedroom.individualFurniture.Find(item => item.furnitureClasses.Contains(InteriorControls.Instance.bed));

        //Objective.ObjectiveTrigger findHidingPlace = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.playerHidden, "");
        //AddObjective("Find a hiding place! Under the bed might work.", findHidingPlace, usePointer: true, pointerPosition: kidnapperBed.GetWorldAveragePosition() + new Vector3(0, 0.3f, 0), onCompleteAction: Objective.OnCompleteAction.nothing, allowCrouchPromt: true, useIcon: InterfaceControls.Icon.run);

        //PlayerVO("a9015c83-9133-4dc8-b219-b6500a8c7e66", 0f);

        //enforcerEventsTrigger = true;
    }

    public void CollectHandIn(int passedVar)
    {
        ClearAllObjectives();

        //Set part location
        SetCurrentPartLocation(kidnappersDoorNode);

        //From this point, this is classed as a 'normal' murder case; prompting the player to pick up the hand in form from city hall...
        MurderController.Instance.AssignActiveCase(thisCase);
        thisCase.SetStatus(Case.CaseStatus.handInNotCollected);

        //Triggered when form is collected...
        Objective.ObjectiveTrigger collectHandIn = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.collectHandIn, "");
        AddObjective("Hand In", collectHandIn, false, isSilent: true, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "ViewHandIn");

        //Plot route to city call
        if (MapController.Instance.playerRoute == null && thisCase.handIn.Count > 0)
        {
            Interactable handIn = null;

            if(CityData.Instance.savableInteractableDictionary.TryGetValue(thisCase.handIn[0], out handIn))
            {
                MapController.Instance.PlotPlayerRoute(handIn.node.gameLocation);
            }
        }
    }

    public void ViewHandIn(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(kidnappersDoorNode);

        //Triggered when form is collected...
        Objective.ObjectiveTrigger viewHandIn = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.viewHandIn, "View Hand In", newProgressAdd: 1f);
        AddObjective("View Hand In", viewHandIn, false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "ViewedHandIn");
    }

    public void ViewedHandIn(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(kidnappersDoorNode);

        PlayerVO("8ba8c86a-8874-4195-a619-ca64f09f660a", 0f, true);
        PlayerVO("be076959-6241-4dae-bbc1-55059a7947d9", 0f, true);

        //Complete form objective
        Objective.ObjectiveTrigger catchKiller = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.submitCase, "");
        AddObjective("complete the hand in form", catchKiller, false, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.resolve);

        //Skip straight to next part: Picking lead...
        ChapterController.Instance.LoadPart("DisplayLeads");
    }

    public void DisplayLeads(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(kidnappersDoorNode);

        //Set reportable
        kidnapper.unreportable = false;

        //Complete form objective
        Objective.ObjectiveTrigger catchKiller = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.submitCase, "");
        AddObjective("complete the hand in form", catchKiller, false, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.resolve);

        nextLeadDelay = 0.005f; //Game loop method will bring up lead selection after this time delay...
    }

    public void ExecuteChangeLeadsManual()
    {
        //Let the player choose which lead to follow up on...
        bool investigateCCTV = true;
        bool investigatePhoneCalls = true;
        bool invesigateVmails = true;
        bool investigateMurderWeapon = discoveredWeaponsDealer;

        PopupMessageController.Instance.PopupMessage("Choose Next Lead", enableLeftButton: investigateCCTV, LButton: "Investigate CCTV Records", enableRightButton: investigatePhoneCalls, RButton: "Investigate Phone Calls", enableSecondaryLeftButton: invesigateVmails, LButton2: "Investigate VMails", enableSecondaryRightButton: investigateMurderWeapon, RButton2: "Investigate Murder Weapon", anyButtonClosesMsg: true, enableOptionButton: true, OButton: "Disable Leads");

        PopupMessageController.Instance.OnLeftButton += ChooseInvestigateCCTV;
        PopupMessageController.Instance.OnRightButton += ChooseInvestigatePhone;
        PopupMessageController.Instance.OnLeftButton2 += ChooseInvestigateVmails;
        PopupMessageController.Instance.OnRightButton2 += ChooseInvestigateMurderWeapon;
        PopupMessageController.Instance.OnOptionButton += ChooseCancelLeads;
    }

    public void ClearLeads(bool clearDiner, bool clearOffice, bool clearPhone, bool clearWeaponsDealer)
    {
        if (clearDiner)
        {
            ClearObjective("Restaurant");
            ClearObjective("Access the back office");
            ClearObjective("Disable security by tracing the red wires to the breaker box (optional)");
            ClearObjective("Bribe staff for access (optional)");
            ClearObjective("Disable security systems by using the lever (optional)");
            ClearObjective("Access the cruncher");
            ClearObjective("Open the 'Surveillance' application");
            ClearObjective("Look for surveillance entries from around |meettime|");
            ClearObjective("|story.kidnapper.name| is sitting with somebody. Open an evidence window by inspecting their mugshot");
            ClearObjective("Check the table where |story.kidnapper.name| was sitting");
            ClearObjective("FindMeetingNote");
        }

        if(clearOffice)
        {
            ClearObjective("Office");
            ClearObjective("Access kidnapper cruncher");
            ClearObjective("Find employee record for |story.kidnapper.name| for more info (optional)");
            ClearObjective("Disable security systems by using the lever (optional)");
            ClearObjective("Open the 'V Mail' application");
            ClearObjective("Print any relevent vmail threads");
        }

        if(clearPhone)
        {
            ClearObjective("Find cabinet");
            ClearObjective("Unlock cabinet");
            ClearObjective("Access cabinet");
            ClearObjective("Trace Call");
            ClearObjective("Access other cabinet");
            ClearObjective("Find other call");
            ClearObjective("Go to other address");
        }

        if (clearWeaponsDealer)
        {
            ClearObjective("Weapons Dealer");
            ClearObjective("Search Weapons Dealer");
        }
    }

    public void InvestigateCCTV(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(kidnappersDoorNode);

        //Trigger restaurant
        Objective.ObjectiveTrigger goToRestaurant = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: restaurant);
        AddObjective("Restaurant", goToRestaurant, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "ArrivalDiner");

        //City directory hint
        Objective.ObjectiveTrigger goToRestaurantHint = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: restaurant);
        AddObjective("Hint: You can always find a city directory next to a public telelphone", goToRestaurantHint, false, useIcon: InterfaceControls.Icon.questionMark, onCompleteAction: Objective.OnCompleteAction.nothing);

        AddObjective("Hint: You also search for public or visited places by using |controls.notebook|", goToRestaurantHint, false, useIcon: InterfaceControls.Icon.questionMark, onCompleteAction: Objective.OnCompleteAction.nothing);
    }

    public void InvestigateVmails(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(kidnappersDoorNode);

        //Trigger office
        Objective.ObjectiveTrigger goToOffice = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: kidnapper.job.employer.address);
        AddObjective("Office", goToOffice, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "ArrivalWorkplace");

        //City directory hint
        Objective.ObjectiveTrigger goToOfficeHint = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: kidnapper.job.employer.address);
        AddObjective("Hint: You can always find a city directory next to a public telelphone", goToOfficeHint, false, useIcon: InterfaceControls.Icon.questionMark, onCompleteAction: Objective.OnCompleteAction.nothing);

        AddObjective("Hint: You also search for public or visited places by using |controls.notebook|", goToOfficeHint, false, useIcon: InterfaceControls.Icon.questionMark, onCompleteAction: Objective.OnCompleteAction.nothing);
    }

    public void InvesitgatePhone(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(kidnappersDoorNode);

        //Trigger weapons dealer
        Objective.ObjectiveTrigger accessCabinet = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToNode, "", newNode: kidnapperRouter.node);
        Vector3 pointerPos = Vector3.zero;
        if (kidnapperRouter != null) pointerPos = kidnapperRouter.GetWorldPosition();

        AddObjective("Find cabinet", accessCabinet, usePointer: true, pointerPosition: pointerPos, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "AccessCabinet", useIcon: InterfaceControls.Icon.hand);
    }

    public void InvestigateMurderWeapon(int passedVar)
    {
        //Set part location
        NewRoom entranceRoom = weaponSeller.rooms.Find(item => item.entrances.Count > 0);
        SetCurrentPartLocation(entranceRoom.entrances[0].fromNode);

        //Access the cabinet
        Objective.ObjectiveTrigger goToWeaponsDealer = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: weaponSeller);
        AddObjective("Weapons Dealer", goToWeaponsDealer, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "SearchWeaponsDealer");

        //City directory hint
        Objective.ObjectiveTrigger goToWeaponsDealerHint = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: weaponSeller);
        AddObjective("Hint: You can always find a city directory next to a public telelphone", goToWeaponsDealerHint, false, useIcon: InterfaceControls.Icon.questionMark, onCompleteAction: Objective.OnCompleteAction.nothing);

        AddObjective("Hint: You also search for public or visited places by using |controls.notebook|", goToWeaponsDealerHint, false, useIcon: InterfaceControls.Icon.questionMark, onCompleteAction: Objective.OnCompleteAction.nothing);
    }

    //public void BackToApartment(int passedVar)
    //{
    //    PlayerVO("Oh no! The police are here!", 0f);
    //    PlayerVO("This is bad, I need to keep my head down!", 0f);

    //    NewNode playerDoorNode = apartmentEntrance.toNode;
    //    if (apartmentEntrance.toNode.gameLocation == Player.Instance.home) playerDoorNode = apartmentEntrance.fromNode;

    //    SetCurrentPartLocation(playerDoorNode);

    //    //Cancel route
    //    if (MapController.Instance.playerRoute != null) MapController.Instance.playerRoute.Remove();

    //    //LEM will try to phone the player when they pass by a payphone...
    //    triggerLEMCall = true;

    //    //Go to nearby diner
    //    //Objective.ObjectiveTrigger diner = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToPublicFacingAddress, "");
    //    //AddObjective("Find a Diner, Restaurant or Bar to lay low", diner, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextChapterPart);
    //}

    public void ArrivalDiner(int passedVar)
    {
        //Set part location
        NewRoom eateryRoom = restaurant.rooms.Find(item => item.roomType.name == "Diner" || item.roomType.name == "Eatery" || item.roomType.name == "Bar");
        SetCurrentPartLocation(eateryRoom.entrances[0].fromNode);

        PlayerVO("a5e5e68f-4a2c-4db9-8910-de678d41b919", 0f);
        PlayerVO("79fe960e-6ae9-4e02-a8d9-1ecdbe0caca8", 0f);

        Vector3 pointerPos = dinerCruncher.GetWorldPosition();
        NewNode.NodeAccess backroomDoor = dinerCruncher.node.room.entrances.Find(item => item.GetOtherRoom(dinerCruncher.node.room) == eateryRoom);
        if (backroomDoor != null) pointerPos = backroomDoor.worldAccessPoint + new Vector3(0, 1.5f, 0);

        PopupMessageController.Instance.TutorialMessage("commercialproperty"); //Display popup tutorial section: Escaping

        Objective.ObjectiveTrigger accessBack = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToRoom, "", newRoom: restaurantBackroom);
        AddObjective("Access the back office", accessBack, usePointer: true, pointerPosition: pointerPos, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "AccessBackroom", useIcon: InterfaceControls.Icon.location);

        //Breaker box objective
        Interactable restaurantBreaker = restaurant.GetBreakerSecurity();

        if(restaurantBreaker != null)
        {
            Objective.ObjectiveTrigger goToBreaker = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToNode, "", newNode: restaurantBreaker.node);
            AddObjective("Disable security by tracing the red wires to the breaker box (optional)", goToBreaker, usePointer: true, pointerPosition: restaurantBreaker.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "BreakerBox", useIcon: InterfaceControls.Icon.lockpick);
        }

        //Talk
        Objective.ObjectiveTrigger talkToStaff = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToRoom, "", newRoom: restaurantBackroom);
        AddObjective("Bribe staff for access (optional)", talkToStaff, usePointer: false, pointerPosition: Vector3.zero, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.citizen);

        //Setup event so that the player can skip this sequence
        if (dinerFlyer != null)
        {
            Objective.ObjectiveTrigger checkTable = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", newEvidence: dinerFlyer.evidence);
            AddObjective("FlyerFind", checkTable, usePointer: false, isSilent: true, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "FindFlyer");
        }
    }

    void BreakerBox()
    {
        Interactable restaurantBreaker = restaurant.GetBreakerSecurity();

        if (restaurantBreaker != null)
        {
            Objective.ObjectiveTrigger disableSecurity = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.switchStateFalse, "", newInteractable: restaurantBreaker);
            AddObjective("Disable security systems by using the lever (optional)", disableSecurity, usePointer: true, pointerPosition: restaurantBreaker.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "BreakerTip", useIcon: InterfaceControls.Icon.hand);
        }
    }

    void BreakerTip()
    {
        SessionData.Instance.TutorialTrigger("breakingandentering");
    }

    public void AccessBackroom(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(dinerCruncher.node);

        PlayerVO("e3be726f-53fc-48c5-b6fb-01a7516a60e2", 0f);

        PopupMessageController.Instance.TutorialMessage("computers");

        Objective.ObjectiveTrigger findRecords = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.accessCruncher, "", newInteractable: dinerCruncher);
        AddObjective("Access the cruncher", findRecords, usePointer: true, pointerPosition: dinerCruncher.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "LaunchSurveillance", useIcon: InterfaceControls.Icon.hand);
    }

    public void LaunchSurveillance(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(dinerCruncher.node);

        Objective.ObjectiveTrigger openSurveillance = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.accessApp, "Surveillance", newInteractable: dinerCruncher);
        AddObjective("Open the 'Surveillance' application", openSurveillance, usePointer: false, pointerPosition: dinerCruncher.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "FoundRecords", useIcon: InterfaceControls.Icon.hand);
    }

    public void FoundRecords(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(dinerCruncher.node);

        PopupMessageController.Instance.TutorialMessage("surveillance"); //Display popup tutorial section: Fingerprints

        UpdateCamReferences();

        if(kidnapperOnCam)
        {
            Objective.ObjectiveTrigger findCorrect = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.findSurveillanceWith, "", newEvidence: kidnapper.evidenceEntry, newGameLocation: restaurant);
            AddObjective("Look for surveillance entries from around |meettime|", findCorrect, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "KidnapperOnCam");
        }
        else
        {
            PopupMessageController.Instance.PopupMessage("dud_lead", true);

            PlayerVO("5d31cc22-5924-48c6-a117-2ada1b5c6074", 0f);
        }
    }

    //Updates whether notewriter and kidnapper are on camera at the same time...
    void UpdateCamReferences()
    {
        //Different objectives depending on what surveillance captured...
        notewriterOnCam = false;
        kidnapperOnCam = false;

        foreach (Interactable cam in restaurant.securityCameras)
        {
            if (cam.sceneRecorder != null)
            {
                foreach (SceneRecorder.SceneCapture cap in cam.sceneRecorder.interactable.cap)
                {
                    if (cap.aCap.Exists(item => item.id == kidnapper.humanID))
                    {
                        Game.Log("Chapter: Kidnapper " + kidnapper.GetCitizenName() + " is on cam at " + SessionData.Instance.TimeStringOnDay(cap.t, true, true));
                        kidnapperOnCam = true;

                        if (cap.aCap.Exists(item => item.id == noteWriter.humanID))
                        {
                            Game.Log("Chapter: Notewriter " + noteWriter.GetCitizenName() + " is on also cam at " + SessionData.Instance.TimeStringOnDay(cap.t, true, true));
                            notewriterOnCam = true;
                        }
                    }

                    if (notewriterOnCam && kidnapperOnCam) break;
                }
            }

            if (notewriterOnCam && kidnapperOnCam) break;
        }
    }

    public void KidnapperOnCam(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(dinerCruncher.node);

        UpdateCamReferences();

        Objective.ObjectiveTrigger selectNotewriter = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", newEvidence: noteWriter.evidenceEntry);
        AddObjective("|story.kidnapper.name| is sitting with somebody. Open an evidence window by inspecting their mugshot", selectNotewriter, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "OpenNotewirterEvidence", useIcon: InterfaceControls.Icon.hand);

        PlayerVO("ff3d774e-0309-4521-b255-31acb2eacaed");
    }

    public void OpenNotewirterEvidence(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(dinerCruncher.node);

        if (dinerFlyer != null)
        {
            Vector3 pointerPos = dinerFlyer.GetWorldPosition();

            Objective.ObjectiveTrigger checkTable = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", newEvidence: dinerFlyer.evidence);
            AddObjective("Check the table where |story.kidnapper.name| was sitting", checkTable, usePointer: true, pointerPosition: pointerPos, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "FindFlyer");
        }
        else
        {
            PopupMessageController.Instance.PopupMessage("dud_lead", true);
            ChapterController.Instance.LoadPart("DisplayLeads");
        }
    }

    public void FindFlyer(int passedVar)
    {
        PlayerVO("7bb0e329-6311-49cf-8d7b-99e6a3d63993", 0f, true);

        ClearObjective("Disable security by tracing the red wires to the breaker box (optional)");
        ClearObjective("Bribe staff for access (optional)");

        WarnNotewriter(); //Trigger find notewriter objective
        //ChapterController.Instance.LoadPart("DisplayLeads");
    }

    public void ArrivalWorkplace(int passedVar)
    {
        //Set part location
        NewRoom entranceRoom = kidnapper.job.employer.address.rooms.Find(item => item.entrances.Count > 0 && !item.isNullRoom);
        SetCurrentPartLocation(entranceRoom.entrances[0].fromNode);

        //Breaker box objective
        Interactable workplaceBreaker = kidnapper.job.employer.address.GetBreakerSecurity();

        if (workplaceBreaker != null)
        {
            Objective.ObjectiveTrigger goToBreaker = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToNode, "", newNode: workplaceBreaker.node);
            AddObjective("Disable security by tracing the red wires to the breaker box (optional)", goToBreaker, usePointer: true, pointerPosition: workplaceBreaker.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "BreakerBoxWorkplace", useIcon: InterfaceControls.Icon.lockpick);
        }

        //A silent objective that triggers a player comment but it otherwise unessential
        if (workplaceMessageNote != null)
        {
            Objective.ObjectiveTrigger meetingNoteObj = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", newEvidence: workplaceMessageNote.evidence);
            AddObjective("FindMeetingNote", meetingNoteObj, usePointer: false, onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "FindMeetingNote", isSilent: true);
        }

        PlayerVO("6c3006c1-ce37-430d-b8ee-9feb73ecf196", 0f, true);

        if(kidnapper.job.employer.IsOpenAtThisTime(SessionData.Instance.gameTime, SessionData.Instance.decimalClock, SessionData.Instance.day))
        {
            PlayerVO("3b444a2f-3eb5-484e-8bb0-08636f7fd16e", 0f, true);
        }
        else
        {
            PlayerVO("8bd617a9-9d4c-479e-8e51-01de26ec9a74", 0f, true);
        }

        //Change passcode note to be birth year
        foreach (int i in kidnapper.passcode.notes)
        {
            Interactable pNote = null;

            if(CityData.Instance.savableInteractableDictionary.TryGetValue(i, out pNote))
            {
                if(pNote != null && pNote.node != null && kidnapper.job != null && kidnapper.job.employer != null && pNote.node.gameLocation == kidnapper.job.employer.address)
                {
                    Game.Log("Chapter: Found kidnapper's passcode note at work!");

                    //TODO: Line about needing to find birth year etc?
                }
            }
        }

        //Computers tutorial
        Objective.ObjectiveTrigger accessPC = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.accessAnyCruncher, "");
        AddObjective("Access Cruncher", accessPC, false, onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "ComputerTutorial", isSilent: true);

        Objective.ObjectiveTrigger findnote1 = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.accessCruncher, "", newEvidence: kidnapper.evidenceEntry);
        AddObjective("Access kidnapper cruncher", findnote1, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "LaunchVmail", useIcon: InterfaceControls.Icon.hand);

        if (kidnapper.job.employer.address.company.employeeRoster != null)
        {
            EvidenceMultiPage.MultiPageContent mp = kidnapper.job.employer.address.company.employeeRoster.pageContent.Find(item => item.evID == "EmployeeRecord" + kidnapper.humanID);

            if(mp != null)
            {
                Objective.ObjectiveTrigger findEmployeeDets = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", newEvidence: mp.GetEvidence());
                AddObjective("Find employee record for |story.kidnapper.name| for more info (optional)", findEmployeeDets, usePointer: true, pointerPosition: kidnapper.job.employer.address.company.employeeRoster.interactable.GetWorldPosition() + new Vector3(0, 0.25f, 0), onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.lookingGlass);
            }
        }
    }

    void BreakerBoxWorkplace()
    {
        Interactable workpalceBreaker = kidnapper.job.employer.address.GetBreakerSecurity();

        if (workpalceBreaker != null)
        {
            Objective.ObjectiveTrigger disableSecurity = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.switchStateFalse, "", newInteractable: workpalceBreaker);
            AddObjective("Disable security systems by using the lever (optional)", disableSecurity, usePointer: true, pointerPosition: workpalceBreaker.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "BreakerTip", useIcon: InterfaceControls.Icon.hand);
        }
    }

    void ComputerTutorial()
    {
        PopupMessageController.Instance.TutorialMessage("computers");
    }

    public void LaunchVmail(int passedVar)
    {
        //Set part location
        NewRoom entranceRoom = kidnapper.job.employer.address.rooms.Find(item => item.entrances.Count > 0 && !item.isNullRoom);
        SetCurrentPartLocation(entranceRoom.entrances[0].fromNode);

        Objective.ObjectiveTrigger openVmail = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.accessApp, "VMail", newEvidence: kidnapper.evidenceEntry);
        AddObjective("Open the 'V Mail' application", openVmail, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "AccessKidnapperCruncher", useIcon: InterfaceControls.Icon.hand);
    }

    public void AccessKidnapperCruncher(int passedVar)
    {
        //Set part location
        NewRoom entranceRoom = kidnapper.job.employer.address.rooms.Find(item => item.entrances.Count > 0);
        SetCurrentPartLocation(entranceRoom.entrances[0].fromNode);

        Objective.ObjectiveTrigger openVmail = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.printVmail, "63c2e755-2840-44f6-b4da-f72e9a84d890");
        AddObjective("Print any relevent vmail threads", openVmail, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "FoundNotewriterID");

        Objective.ObjectiveTrigger weaponsVmail = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.printVmail, "37d3cbd8-9450-430f-8bf2-9e3efbf7f3f7");
        AddObjective("Print any relevent vmail threads", weaponsVmail, isSilent: true, usePointer: false, onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "FindMeetingNote");
    }

    public void FoundNotewriterID(int passedVar)
    {
        PlayerVO("7bb0e329-6311-49cf-8d7b-99e6a3d63993", 0f, true);

        //Add warn notewriter dialog
        findNotewriter = true;

        //Set part location
        NewRoom entranceRoom = noteWriter.job.employer.address.rooms.Find(item => item.entrances.Count > 0);
        SetCurrentPartLocation(entranceRoom.entrances[0].fromNode);

        WarnNotewriter(); //Trigger find notewriter objective
        //ChapterController.Instance.LoadPart("DisplayLeads");
    }

    public void WarnNotewriter()
    {
        //Notewriter pointer...
        if (!killer.ai.isConvicted)
        {
            findNotewriter = true;

            //Add warn notewriter dialog
            Game.Log("Chapter: Attempting to add notewriter dialog...");
            noteWriter.evidenceEntry.AddDialogOption(Evidence.DataKey.name, ChapterController.Instance.loadedChapter.dialogEvents[1], allowPresetDuplicates: false);
            notewriterDialogAdded = true;

            Objective.ObjectiveTrigger warnNotewriter = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.notewriterWarned, "");
            AddObjective("Find |notewritername|", warnNotewriter, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.citizen);

            //Prompt if player knocks on the door and nobody is home...
            Objective.ObjectiveTrigger nobodyHome2 = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.playerActionNobodyHome, "KnockOnDoor", newGameLocation: noteWriter.home);
            AddObjective("Nobody home", nobodyHome2, false, isSilent: true, onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "NobodyHome");
        }
    }

    public void AccessCabinet(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(kidnapperRouter.node);

        if(kidnapperRouterDoor.lockInteractable != null && kidnapperRouterDoor.lockInteractable.locked)
        {
            //Unlock the cabinet
            Objective.ObjectiveTrigger unlockCabinet = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.unlockInteractable, "", newInteractable: kidnapperRouterDoor);
            AddObjective("Unlock cabinet", unlockCabinet, usePointer: true, pointerPosition: kidnapperRouterDoor.lockInteractable.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.lockpick);
        }

        Vector3 pointerPos = Vector3.zero;
        if (kidnapperRouter != null) pointerPos = kidnapperRouter.GetWorldPosition();

        //Search for calls to/fom the kidnapper address
        Objective.ObjectiveTrigger accessInterface = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", newEvidence: kidnapperRouter.evidence);
        AddObjective("Access cabinet", accessInterface, usePointer: true, pointerPosition: pointerPos, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "TraceCall", useIcon: InterfaceControls.Icon.hand);
    }

    public void TraceCall(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(kidnapperRouter.node);

        Vector3 pointerPos = Vector3.zero;
        if (kidnapperRouter != null) pointerPos = kidnapperRouter.GetWorldPosition();

        //Search for calls to/fom the kidnapper address
        Objective.ObjectiveTrigger pickAddress = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.plotRouteToCallInvolving, "", newGameLocation: kidnapper.home);
        AddObjective("Trace Call", pickAddress, usePointer: true, pointerPosition: pointerPos, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "SearchCallSource");
    }

    public void SearchCallSource(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(kidnapperRouter.node);

        Interactable otherCabinet = null;
        Vector3 cursorPos = Vector3.zero;

        try
        {
            if (kidnapper.home != null)
            {
                TelephoneController.PhoneCall validCall = kidnapper.home.building.callLog.Find(item => (item.toNS != null && item.fromNS != null && item.toNS.interactable.node.gameLocation == kidnapper.home && MapController.Instance.playerRoute.end.gameLocation.building == item.fromNS.interactable.node.gameLocation.building) || (item.fromNS != null && item.toNS != null && item.fromNS.interactable.node.gameLocation == kidnapper.home && MapController.Instance.playerRoute.end.gameLocation.building == item.toNS.interactable.node.gameLocation.building));

                if (validCall != null)
                {
                    if (validCall.toNS.interactable.node.gameLocation == kidnapper.home)
                    {
                        chosenRouterAddress = validCall.fromNS.interactable.node.gameLocation.thisAsAddress;
                    }
                    else if (validCall.fromNS.interactable.node.gameLocation == kidnapper.home)
                    {
                        chosenRouterAddress = validCall.toNS.interactable.node.gameLocation.thisAsAddress;
                    }

                    if (chosenRouterAddress != null)
                    {
                        if (chosenRouterAddress.building != Player.Instance.currentBuilding)
                        {
                            otherCabinet = Toolbox.Instance.FindClosestObjectTo(InteriorControls.Instance.telephoneRouter, Player.Instance.transform.position, chosenRouterAddress.building, null, null, out _);

                            if (otherCabinet != null)
                            {
                                cursorPos = otherCabinet.GetWorldPosition();

                                Objective.ObjectiveTrigger accessOtherCabinet = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", newEvidence: otherCabinet.evidence);
                                AddObjective("Access other cabinet", accessOtherCabinet, usePointer: true, pointerPosition: cursorPos, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "AccessOtherAddress", useIcon: InterfaceControls.Icon.hand);
                            }
                        }
                        else
                        {
                            ChapterController.Instance.LoadPart("SearchOtherAddress");
                        }
                    }
                }
            }
            else Game.Log("Chapter: The kidnapper no longer features a home...");
        }
        catch
        {
            Game.Log("Chapter: Unable to load call source for chapter");
        }
    }

    public void AccessOtherAddress(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(kidnapperRouter.node);

        //Find the corresponding call
        Objective.ObjectiveTrigger findCorrespondingCall = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.openEvidence, "", newEvidence: chosenRouterAddress.evidenceEntry);
        AddObjective("Find other call", findCorrespondingCall, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "SearchOtherAddress");
    }

    public void SearchOtherAddress(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(kidnapperRouter.node);

        //Go to other address
        Objective.ObjectiveTrigger goToOther = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: chosenRouterAddress);
        AddObjective("Go to other address", goToOther, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "SearchFail");
    }

    public void SearchFail(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(kidnapperRouter.node);

        //Find the corresponding call
        Objective.ObjectiveTrigger leaveOther = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.escapeGameLocation, "", newGameLocation: chosenRouterAddress);
        AddObjective("Search other address", leaveOther, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "DisplayLeads");

        Objective.ObjectiveTrigger dudLead = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.escapeGameLocation, "", newGameLocation: chosenRouterAddress);
        AddObjective("Dud lead", dudLead, usePointer: false, isSilent: true, onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "DudLead");
    }

    void DudLead()
    {
        PopupMessageController.Instance.PopupMessage("dud_lead", true);
    }

    public void SearchWeaponsDealer(int passedVar)
    {
        //Set part location
        NewRoom entranceRoom = weaponSeller.rooms.Find(item => item.entrances.Count > 0);
        SetCurrentPartLocation(entranceRoom.entrances[0].fromNode);

        //Searcg weapons dealer
        Objective.ObjectiveTrigger searchWeaponsDealer = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.inspectInteractable, "", newInteractable: weaponsSalesLedger);
        AddObjective("Search Weapons Dealer", searchWeaponsDealer, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "FoundKillerID");
    }

    public void FoundKillerID(int passedVar)
    {
        //Set part location
        NewRoom entranceRoom = killer.home.rooms.Find(item => item.entrances.Count > 0);
        SetCurrentPartLocation(entranceRoom.entrances[0].fromNode);

        //Trigger killer find
        Objective.ObjectiveTrigger goTokiller = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: killer.home);
        AddObjective("Find Killer", goTokiller, usePointer: false, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "ProveKiller");
    }

    public void ProveKiller(int passedVar)
    {
        //Set part location
        NewRoom entranceRoom = killer.home.rooms.Find(item => item.entrances.Count > 0);
        SetCurrentPartLocation(entranceRoom.entrances[0].fromNode);

        //Trigger killer find
        Objective.ObjectiveTrigger findPrints = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.findFingerprintsAtLocation, "", newEvidence: killer.evidenceEntry, newGameLocation: killer.home);
        AddObjective("Find Proof", findPrints, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nothing);
    }

    //Player returns home to discover eviction notice
    public void ReturnHome(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(apartmentEntrance.door.wall.node);

        ClearAllObjectives();

        //Optional return home objective
        apartmentEntrance.door.SetLocked(false, null, false);
        apartmentEntrance.door.OpenByActor(null);
        apartmentEntrance.door.SetForbidden(true); //Enable police tape

        //Call enforcers
        //GameplayController.Instance.CallEnforcers(apartment, true, true);

        //Remove all furniture
        if (Player.Instance.residence != null)
        {
            Player.Instance.residence.address.RemoveAllInhabitantFurniture(false, FurnitureClusterLocation.RemoveInteractablesOption.remove);
        }

        //Place eviction notice
        if(evictionNotice == null)
        {
            Vector3 wPos = apartmentEntrance.door.transform.TransformPoint(new Vector3(-1.2f, 1.6f, -0.2f));
            Vector3 wEuler = apartmentEntrance.door.transform.eulerAngles + new Vector3(-90, 0, 0);
            evictionNotice = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.letter, null, null, null, wPos, wEuler, null, null, ddsOverride: "fdf6930b-0c20-434f-a181-dd4975944331");
        }

        Objective.ObjectiveTrigger returnHome = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToRoom, "", newRoom: exteriorDoorNode.room);
        AddObjective("Return to Apartment", returnHome, true, pointerPosition: apartmentEntrance.worldAccessPoint + new Vector3(0, 1, 0), onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "VistSlophouseOwner", useIcon: InterfaceControls.Icon.run, delay: 6f);

        MapController.Instance.PlotPlayerRoute(exteriorDoorNode, false); //Auto plot this one to encourage player to travel instead of fast-travel
    }

    //Visits half way house owner
    public void VistSlophouseOwner(int passedVar)
    {
        //Set part location
        SetCurrentPartLocation(apartmentEntrance.door.wall.node);

        //Go home
        apartment.inhabitants.Clear();
        apartment.owners.Clear();
        Player.Instance.SetResidence(null);

        apartmentEntrance.door.SetLocked(false, null, false);
        apartmentEntrance.door.OpenByActor(null);
        apartmentEntrance.door.SetForbidden(true); //Enable police tape

        //Add dialog to slophouse owner and anyone who lives there...
        foreach (Human h in slophouseOwner.home.inhabitants)
        {
            //Add warn notewriter dialog
            h.evidenceEntry.AddDialogOption(Evidence.DataKey.name, ChapterController.Instance.loadedChapter.dialogEvents[2], allowPresetDuplicates: false);
            (h as Citizen).alwaysPassDialogSuccess = true;
        }

        //Always give name
        if (slophouseOwner != null)
        {
            (slophouseOwner as Citizen).alwaysPassDialogSuccess = true;
        }

        //Repossessed
        PlayerVO("9112f9a1-d46d-4167-954d-a3ec74670b15", 0f, true);
        PlayerVO("9c738588-19b2-495c-9968-e6458ff5ecb6", 0f, true);
        PlayerVO("70e61f16-1db2-4269-a53f-11dba118bcac", 0f, true);
        PlayerVO("2919fe0b-38da-4680-9705-1d4440e76273", 0f, true);

        Objective.ObjectiveTrigger talkToOwner = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.onDialogSuccess, ChapterController.Instance.loadedChapter.dialogEvents[2].name);
        AddObjective("Find and talk to |story.slophouseowner.name| about accommodation", talkToOwner, false, useIcon: InterfaceControls.Icon.citizen, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "GoToSlophouse");

        //Prompt if player knocks on the door and nobody is home...
        Objective.ObjectiveTrigger nobodyHome = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.playerActionNobodyHome, "KnockOnDoor", newGameLocation: slophouseOwner.home);
        AddObjective("Nobody home", nobodyHome, false, isSilent: true, onCompleteAction: Objective.OnCompleteAction.invokeFunction, chapterString: "NobodyHome");
    }

    void NobodyHome()
    {
        //Display hint
        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "nobody_home"), InterfaceControls.Icon.questionMark);
    }

    //Player visits slophouse
    public void GoToSlophouse(int passedVar)
    {
        //Set part location
        NewRoom entranceRoom = slophouse.rooms.Find(item => item.entrances.Count > 0 && !item.isNullRoom);
        SetCurrentPartLocation(entranceRoom.entrances[0].fromNode);

        ClearAllObjectives();

        //Remove eviction notice
        if(evictionNotice != null)
        {
            evictionNotice.MarkAsTrash(true);
            evictionNotice.RemoveFromPlacement();
        }

        //Remove dialog from slophouse owner and anyone who lives there...
        foreach (Human h in slophouseOwner.home.inhabitants)
        {
            //Add warn notewriter dialog
            h.evidenceEntry.RemoveDialogOption(Evidence.DataKey.name, ChapterController.Instance.loadedChapter.dialogEvents[2]);
            (h as Citizen).alwaysPassDialogSuccess = false;
        }

        //Buys residence
        if(Player.Instance.home != slophouse)
        {
            Game.Log("Chapter: Setting flophouse " + slophouse.name + " " + slophouse.id + " as player home...");
            PlayerApartmentController.Instance.BuyNewResidence(slophouse.residence, true);

            //Place some basic furniture; a bed and notice board
            PlayerApartmentController.Instance.PlaceIndividualCluster(InteriorControls.Instance.bedCluster, slophouse);
            PlayerApartmentController.Instance.PlaceIndividualCluster(InteriorControls.Instance.noticeBoardCluster, slophouse);
        }

        //Place instruction notes
        if (flophouseWelcomeLetter == null) flophouseWelcomeLetter = slophouse.PlaceObject(InteriorControls.Instance.note, Player.Instance, Player.Instance, null, out _, null, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseNonOwned, ddsOverride: "a35d5d04-fcb3-4302-902b-4e9c10bb101a");
        if (flophouseSyncDiskNote == null) flophouseSyncDiskNote = slophouse.PlaceObject(InteriorControls.Instance.note, Player.Instance, Player.Instance, null, out _, null, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseNonOwned, ddsOverride: "97523ad4-eac0-49c1-a21f-006cf06bd5b2");
        if (flophouseJobNote == null) flophouseJobNote = slophouse.PlaceObject(InteriorControls.Instance.note, Player.Instance, Player.Instance, null, out _, null, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseNonOwned, ddsOverride: "0ab4cf29-d643-438f-832c-5443d8667f6f");

        //Place starch loan sync disk
        if (slophouse != null && slophouse.residence != null && slophouse.residence.mailbox != null && flophouseSyncDisk == null)
        {
            Interactable mailboxDoor = Toolbox.Instance.GetMailbox(Player.Instance);

            if (mailboxDoor != null)
            {
                flophouseSyncDisk = mailboxDoor.node.gameLocation.PlaceObject(InteriorControls.Instance.chapterFlophouseSyncDisk.interactable, Player.Instance, Player.Instance, Player.Instance, out _, passedObject: InteriorControls.Instance.chapterFlophouseSyncDisk, forceSecuritySettings: true, forcedOwnership: InteractablePreset.OwnedPlacementRule.ownedOnly, forcedPriority: 3, ignoreLimits: true);
                if (flophouseSyncDisk == null) Game.Log("Chapter: Unable to place sync disk");
            }
            else Game.Log("Chapter: Unable to get mailbox...");
        }

        Objective.ObjectiveTrigger goToSlophouse = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: slophouse);
        AddObjective("Visit |story.flophouse.name|", goToSlophouse, false, useIcon: InterfaceControls.Icon.building, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "ArrivedAtSlophouse");

        MapController.Instance.PlotPlayerRoute(slophouse); //Auto plot this route
    }

    //Player arrives at slophouse
    public void ArrivedAtSlophouse(int passedVar)
    {
        //Set part location
        NewRoom entranceRoom = slophouse.rooms.Find(item => item.entrances.Count > 0 && !item.isNullRoom);
        SetCurrentPartLocation(entranceRoom.entrances[0].fromNode);

        //TODO: Dialog "It's not much, but it will have to make do for now"
        PlayerVO("da76dfd0-e337-4305-9afc-61b79291aa62");

        FurnitureLocation nBoard = null;

        foreach(NewRoom r in slophouse.rooms)
        {
            nBoard = r.individualFurniture.Find(item => item.furniture.name == "PublicCorkboard");
            if (nBoard != null) break;
        }

        if (nBoard != null)
        {
            Objective.ObjectiveTrigger checkNoticeBoard = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToNode, "", newNode: nBoard.anchorNode);
            AddObjective("Check notice board", checkNoticeBoard, true, nBoard.anchorNode.position + new Vector3(0, 1.5f, 0), useIcon: InterfaceControls.Icon.questionMark, onCompleteAction: Objective.OnCompleteAction.specificChapterByString, chapterString: "End");
        }
        else
        {
            Game.Log("Chapter: Unable to find corkboard in flophouse...");

            //Skip to end
            ChapterController.Instance.SkipToNextPart();
        }
    }

    public void CancelLeads(int passedVar)
    {
        //Literally do nothing
    }

    public void End(int passedVar)
    {
        //TODO: Dialog "Need to get out of this hell hole"
        PlayerVO("c0c6baab-3617-4708-9281-9868740bc684");

        //Display optional hint/objectives
        Objective.ObjectiveTrigger nothing = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.nothing, "");
        AddObjective("You can change the decor of your apartment using the inventory menu (|controls.weaponselect|) (optional)", nothing, false, useIcon: InterfaceControls.Icon.questionMark);
        AddObjective("Work towards retirement by advancing social ranks (optional)", nothing, false, useIcon: InterfaceControls.Icon.questionMark);
        AddObjective("You can close this case when ready by using the resolve button", nothing, false, useIcon: InterfaceControls.Icon.questionMark);

        Game.Log("Chapter: End has been reached!");

        if(noteWriter != null)
        {
            (noteWriter as Citizen).alwaysPassDialogSuccess = false;
        }

        if(slophouseOwner != null)
        {
            (slophouseOwner as Citizen).alwaysPassDialogSuccess = false;
        }

        Game.Instance.sandboxMode = true; //Set game to be sandobx mode from now on

        if(thisCase != null)
        {
            thisCase.SetStatus(Case.CaseStatus.closable, false);
        }

        //Below needs to trigger once only
        if (!completed)
        {
            completed = true;

            MurderController.Instance.currentActiveCase = null;
            killer.RemoveFromWorld(true);

            if(note != null)
            {
                note.MarkAsTrash(true);
                note.RemoveManuallyCreatedFingerprints();
            }

            //Murder is cancelled
            for (int i = 0; i < MurderController.Instance.activeMurders.Count; i++)
            {
                MurderController.Murder m = MurderController.Instance.activeMurders[i];
                m.CancelCurrentMurder();
                m.murderer.RemoveFromWorld(true); //Convict murderers...
                m.SetMurderState(MurderController.MurderState.solved, true);
                i--;
            }

            //Make the diner cctv records deletable
            foreach (Interactable cctv in restaurant.securityCameras)
            {
                foreach (SceneRecorder.SceneCapture cap in cctv.cap)
                {
                    cap.k = false;
                }
            }

            //Reset confined statuses
            if(kidnapper != null && kidnapper.home != null && kidnapper.home.inhabitants != null)
            {
                foreach (Human housemate in kidnapper.home.inhabitants)
                {
                    housemate.ai.RemoveAvoidLocation(kidnapper.home);
                }
            }

            noteWriter.ai.SetConfineLocation(null);

            if(noteWriter != null && noteWriter.home != null && noteWriter.home.inhabitants != null)
            {
                foreach (Human housemate in noteWriter.home.inhabitants)
                {
                    housemate.ai.RemoveAvoidLocation(noteWriter.home);
                }
            }

            //Convert to sandbox experience by starting the proc gen loop
            MurderController.Instance.SetProcGenKillerLoop(true);
        }
    }

    public void NotewriterLayLow()
    {
        if(layLowGoal == null)
        {
            noteWriter.ai.SetConfineLocation(null);
            (noteWriter as Citizen).alwaysPassDialogSuccess = false;
            findNotewriter = true;

            Game.Log("Chapter: The notewriter will lay low!");
            layLowGoal = noteWriter.ai.CreateNewGoal(RoutineControls.Instance.layLow, 0f, 0f, newPassedGameLocation: restaurant);

            //Murder is cancelled
            for (int i = 0; i < MurderController.Instance.activeMurders.Count; i++)
            {
                MurderController.Instance.activeMurders[i].CancelCurrentMurder();
            }

            Invoke("NotewritersLeads", 35);
        }
    }

    void NotewritersLeads()
    {
        SessionData.Instance.PauseGame(true);

        InterfaceController.Instance.SpawnWindow(killer.evidenceEntry, Evidence.DataKey.name);
        InterfaceController.Instance.SpawnWindow(killerBar.evidenceEntry, Evidence.DataKey.name);
        GameplayController.Instance.AddMoney(200, true, "warn notewriter");

        ChapterController.Instance.LoadPart("FoundKillerID"); //Found killer ID!

        if (rewardSyncDisk != null)
        {
            rewardSyncDisk.SetOwner(Player.Instance); //Set ownership to player so this isn't stealing...

            //Unlock mailbox for player to get sync disk
            Interactable mailboxDoor = Toolbox.Instance.GetMailbox(noteWriter);

            if (mailboxDoor != null)
            {
                //Unlock the door
                mailboxDoor.SetLockedState(false, null, false, true);

                Objective.ObjectiveTrigger pickUpReward = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.itemInInventory, "", newInteractable: rewardSyncDisk);
                AddObjective("Pick up the Sync Disk in the mailbox", pickUpReward, true, pointerPosition: rewardSyncDisk.GetWorldPosition(), useIcon: InterfaceControls.Icon.star, onCompleteAction: Objective.OnCompleteAction.nothing);
            }

            //Trigger killer find
            foreach(Company c in CityData.Instance.companyDirectory)
            {
                if(c.preset.name == "SyncClinic")
                {
                    if(c.address != null)
                    {
                        bool foundSync = false;

                        foreach(NewRoom r in c.address.rooms)
                        {
                            foreach(NewNode n in r.nodes)
                            {
                                Interactable syncInterface = n.interactables.Find(item => item.preset.specialCaseFlag == InteractablePreset.SpecialCase.syncBed);

                                if(syncInterface != null)
                                {
                                    Vector3 pointer = syncInterface.GetWorldPosition();

                                    Objective.ObjectiveTrigger installSyncDisk = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.syncDiskInstallTutorial, "");
                                    AddObjective("Install sync disk", installSyncDisk, usePointer: true, pointerPosition: pointer, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.hand);
                                    foundSync = true;
                                    break;
                                }
                            }

                            if (foundSync) break;
                        }

                        break;
                    }
                }
            }
        }
    }

    [Button]
    public void ManualTriggerNotewriterMurder()
    {
        notewriterManualMurderTrigger = true;
    }

    [Button]
    public void SkipPreSim()
    {
        murderPreSimPass = true;
    }

    [Button]
    public void TriggerNotewriterLeads()
    {
        NotewritersLeads();
    }
}
 