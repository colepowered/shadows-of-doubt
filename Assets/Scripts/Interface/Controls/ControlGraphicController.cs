﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ControlGraphicController : MonoBehaviour
{
    public Image img;
    public TextMeshProUGUI controlText;
    public ControlGraphicType controlType = ControlGraphicType.keyboard;

    public enum ControlGraphicType { keyboard, mouse, controller};

    public string trackControl;
    public string buttonStr;

    private void OnEnable()
    {
        
    }
}
