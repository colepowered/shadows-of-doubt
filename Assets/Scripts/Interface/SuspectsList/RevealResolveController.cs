using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using NaughtyAttributes;

public class RevealResolveController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public TextMeshProUGUI questionText;
    public GameObject tick;
    public GameObject cross;
    public JuiceController tickJuice;
    public JuiceController crossJuice;
    public List<CanvasRenderer> fadeInRenderers = new List<CanvasRenderer>();

    [Header("State")]
    public bool isCorrect = false;
    public float revealAfterTimer = 0;
    public float fadeIn = 0;
    public float revealCorrectTimer = 0.75f;
    public float waitTimer = 2.6f;
    public float removeTimer = 0.33f;
    public string qText;

    public int revealPhase = 0;
    Case.ResolveQuestion question;

    public void Setup(Case.ResolveQuestion newQuestion, Case newCase, float newRevealAfter)
    {
        question = newQuestion;
        qText = question.GetText(newCase, false, false);
        questionText.text = string.Empty;
        revealAfterTimer = newRevealAfter;

        isCorrect = question.isCorrect;

        tick.SetActive(false);
        cross.SetActive(false);

        //Make things invisible to start
        foreach(CanvasRenderer rend in fadeInRenderers)
        {
            rend.SetAlpha(0f);
        }
    }

    private void Update()
    {
        //Fade in phase
        if(revealPhase == 0)
        {
            if(revealAfterTimer > 0f)
            {
                revealAfterTimer -= Time.deltaTime;
            }
            else
            {
                fadeIn += Time.deltaTime;
                fadeIn = Mathf.Clamp01(fadeIn);

                foreach (CanvasRenderer rend in fadeInRenderers)
                {
                    rend.SetAlpha(fadeIn);
                }

                int revealChars = Mathf.CeilToInt(qText.Length * fadeIn);

                try
                {
                    questionText.text = qText.Substring(0, revealChars);
                }
                catch
                {

                }

                if (fadeIn >= 1f)
                {
                    revealPhase = 1;
                }
            }
        }
        //Reveal correct phase
        else if(revealPhase == 1)
        {
            if(revealCorrectTimer > 0f)
            {
                revealCorrectTimer -= Time.deltaTime;
            }
            else
            {
                if (isCorrect)
                {
                    tick.SetActive(true);
                    tickJuice.Nudge();
                }
                else
                {
                    cross.SetActive(true);
                    crossJuice.Nudge();
                }

                revealPhase = 2;
            }
        }
        //Wait/display
        else if(revealPhase == 2)
        {
            if(waitTimer > 0f)
            {
                waitTimer -= Time.deltaTime;
            }
            else
            {
                revealPhase = 3;
            }
        }
        //Remove
        else if(revealPhase == 3)
        {
            if(removeTimer > 0f)
            {
                rect.anchoredPosition = new Vector2(rect.anchoredPosition.x + (60f / removeTimer), rect.anchoredPosition.y);
                removeTimer -= Time.deltaTime;
            }
            else
            {
                Destroy(this.gameObject); //Remove
            }
        }
    }
}
