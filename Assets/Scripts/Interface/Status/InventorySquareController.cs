using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using TMPro;

public class InventorySquareController : ButtonController
{
    public List<CanvasRenderer> renderers = new List<CanvasRenderer>();
    public FirstPersonItemController.InventorySlot slot;
    public RectTransform stolenIcon;
    public RectTransform equipmentIcon;
    public RectTransform selected;

    public void Setup(FirstPersonItemController.InventorySlot newSlot)
    {
        slot = newSlot;

        if(slot.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic)
        {
            background.sprite = BioScreenController.Instance.itemBGIcon;
            if (equipmentIcon != null) equipmentIcon.gameObject.SetActive(false);
        }
        else if (equipmentIcon != null) equipmentIcon.gameObject.SetActive(true);

        SetupReferences();
        OnUpdateContent();

        UpdateHotkeyDisplay();
    }

    public void OnUpdateContent()
    {
        if(stolenIcon != null) stolenIcon.gameObject.SetActive(false); //Stolen icon disabled by default
        if(icon != null) icon.gameObject.SetActive(false);

        text.text = string.Empty;

        if (slot.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic)
        {
            Interactable slotInteractable = slot.GetInteractable();

            if (slotInteractable == null)
            {
                text.text = Strings.Get("ui.interface", "Empty", Strings.Casing.firstLetterCaptial);
                slot.interactableID = -1; //Reset square so this can be filled
            }
            else
            {
                text.text = slotInteractable.GetName();

                if (icon != null)
                {
                    icon.sprite = slotInteractable.preset.iconOverride; //For now use icons, update later (?)
                    icon.gameObject.SetActive(true);
                }

                //Is stolen?
                if (StatusController.Instance.activeFineRecords.Exists(item => item.crime == StatusController.CrimeType.theft && item.objectID == slotInteractable.id))
                {
                    if (stolenIcon != null) stolenIcon.gameObject.SetActive(true);
                }
            }
        }
        else
        {
            FirstPersonItem slotFPSItem = slot.GetFirstPersonItem();

            if (slotFPSItem != null)
            {
                text.text = Strings.Get("evidence.names", slotFPSItem.name, Strings.Casing.firstLetterCaptial);

                if(icon != null)
                {
                    icon.sprite = slotFPSItem.selectionIcon;
                    icon.gameObject.SetActive(true);
                }
            }
            else
            {
                text.text = Strings.Get("ui.interface", "nothing", Strings.Casing.firstLetterCaptial);
                //img.sprite = InterfaceControls.Instance.iconReference.Find(item => item.iconType == InterfaceControls.Icon.empty).sprite;
            }
        }

        if (slot.hotkey != null && slot.hotkey.Length > 0)
        {
            text.text += " [" + slot.hotkey + "]";
        }
    }

    public void UpdateHotkeyDisplay()
    {
        OnUpdateContent();
    }

    public override void OnHoverStart()
    {
        base.OnHoverStart();

        BioScreenController.Instance.HoverSlot(slot);
    }

    public override void OnHoverEnd()
    {
        base.OnHoverEnd();

        if(BioScreenController.Instance.hoveredSlot == slot)
        {
            BioScreenController.Instance.HoverSlot(null);
        }
    }

    public override void OnLeftClick()
    {
        base.OnLeftClick();

        if(BioScreenController.Instance.selectedSlot != slot)
        {
            BioScreenController.Instance.SelectSlot(slot, closeInventory: false);
        }
    }

    public override void OnRightClick()
    {
        if (InterfaceController.Instance.desktopMode) return; //Don't do this when in desktop mode

        base.OnRightClick();

        if (BioScreenController.Instance.selectedSlot != slot)
        {
            BioScreenController.Instance.SelectSlot(slot, closeInventory: true);
        }
    }
}
