using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;

public class ScriptableObjectIDSystem : ScriptableObject
{
    [Header("ID System")]
    [Tooltip("Used as a replacement for names for smaller save data sizes")]
    public string id;

    //Custom comparisions: This is needed for comparing addressable versions to non-addressable versions of the same class
    public bool Equals(DoorPairPreset other)
    {
        return Equals(other, this);
    }

    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        var objectToCompareWith = (DoorPairPreset)obj;

        return objectToCompareWith.id == id;
    }

    public override int GetHashCode()
    {
        System.HashCode hc = new System.HashCode();
        hc.Add(id);
        hc.Add(GetType());
        return hc.ToHashCode();
    }

    public static bool operator ==(ScriptableObjectIDSystem c1, ScriptableObjectIDSystem c2)
    {
        if (c1 is null)
        {
            return c2 is null;
        }

        return c1.Equals(c2);
    }

    public static bool operator !=(ScriptableObjectIDSystem c1, ScriptableObjectIDSystem c2)
    {
        return !(c1 == c2);
    }
}
