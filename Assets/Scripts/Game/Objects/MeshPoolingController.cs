using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using System.Threading;

public class MeshPoolingController : MonoBehaviour
{
    [Header("State")]
    [ReadOnly]
    public int generatedRoomMeshes = 0;

    [Header("Background Caching")]
    [ReadOnly]
    public bool backgroundCachingEnabled = false;
    //public int threadsToUse = 4; //Threading doesn't work for this (see below) :(
    public int cacheRoomPerXFrames = 2;
    private int frameCounter = 0;
    [InfoBox("How many room meshes this will cache; possibly make this a game settings option?")]
    public int maxCache = 2000;
    [ReadOnly]
    public int uncachedRooms = 0;
    private List<NewRoom> toCache = new List<NewRoom>(); //List of rooms to cache in the background
    [System.NonSerialized]
    public List<LoaderThread> threads = new List<LoaderThread>();

    public Dictionary<NewRoom, RoomMeshCache> roomMeshes = new Dictionary<NewRoom, RoomMeshCache>();

    [System.Serializable]
    public class RoomMeshCache
    {
        public Mesh floorMesh = null;
        public Mesh wallMesh = null;
        public Dictionary<NewBuilding, Mesh> additionalWallMesh = null;
        public Mesh ceilingMesh = null;
    }

    public class LoaderThread
    {
        public Coroutine thread;
        public NewRoom location;
        public bool isDone = false;
    }

    //Singleton pattern
    private static MeshPoolingController _instance;
    public static MeshPoolingController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    //Generate all required meshes for a room
    public void SpawnMeshesForRoom(NewRoom room)
    {
        bool combineMeshes = Game.Instance.combineRoomMeshes;
        if (SessionData.Instance.isFloorEdit) combineMeshes = false;

        //If we're not combing meshes here, simply spawn the modular elements as separate objects
        if (!combineMeshes)
        {
            SpawnModularRoomElements(room, combineMeshes, out _, out _, out _, out _);
        }
        else
        {
            GetCombinedRoomMeshes(room, out room.combinedFloor, out room.combinedWalls, out room.additionalWalls, out room.combinedCeiling, out room.combinedFloorRend, out room.combinedWallRend, out room.combinedCeilingRend); //If these are not cached it will build them...
        }

        SpawnExtraRoomElements(room); //Spawn extra stuff that is not combined (frontages, stairs)

        //This is now loaded
        room.geometryLoaded = true;
        room.gameObject.SetActive(true);
    }

    //Spawn the individual building blocks for each room
    public void SpawnModularRoomElements(
                                            NewRoom room,
                                            bool prepForCombineMeshes,
                                            out List<MeshFilter> wallChildMeshes,
                                            out Dictionary<NewBuilding, List<MeshFilter>> separateWallChildMeshes,
                                            out List<MeshFilter> floorChildMeshes,
                                            out List<MeshFilter> ceilingChildMeshes
        )
    {
        wallChildMeshes = null;
        separateWallChildMeshes = null;
        floorChildMeshes = null;
        ceilingChildMeshes = null;

        if (prepForCombineMeshes)
        {
            wallChildMeshes = new List<MeshFilter>();
            separateWallChildMeshes = new Dictionary<NewBuilding, List<MeshFilter>>();
            floorChildMeshes = new List<MeshFilter>();
            ceilingChildMeshes = new List<MeshFilter>();
        }

        //Spawn walls & corners
        foreach (NewNode nod in room.nodes)
        {
            //Spawn floor/ceiling modular elements
            nod.SpawnFloor(prepForCombineMeshes);
            nod.SpawnCeiling(prepForCombineMeshes);

            if (prepForCombineMeshes)
            {
                if (nod.spawnedFloor != null)
                {
                    floorChildMeshes.Add(nod.spawnedFloor.GetComponent<MeshFilter>());
                }

                if (nod.spawnedCeiling != null)
                {
                    ceilingChildMeshes.Add(nod.spawnedCeiling.GetComponent<MeshFilter>());
                }
            }

            foreach (NewWall wal in nod.walls)
            {
                wal.SpawnWall(prepForCombineMeshes);
                wal.SpawnCorner(prepForCombineMeshes);
                //wal.SpawnFrontage(); //Frontage meshes are never combined - Now done in SpawnExtraRoomElements()

                //Add all wall meshes (if part of same material)
                if (prepForCombineMeshes && wal.preset.materialOverride == null)
                {
                    if (wal.spawnedWall != null)
                    {
                        if (wal.separateWall && wal.otherWall != null && wal.otherWall.node.building != null)
                        {
                            if (!separateWallChildMeshes.ContainsKey(wal.otherWall.node.building))
                            {
                                separateWallChildMeshes.Add(wal.otherWall.node.building, new List<MeshFilter>());
                            }

                            separateWallChildMeshes[wal.otherWall.node.building].Add(wal.spawnedWall.GetComponent<MeshFilter>());
                        }
                        else
                        {
                            wallChildMeshes.Add(wal.spawnedWall.GetComponent<MeshFilter>());
                        }
                    }

                    if (wal.spawnedCorner != null)
                    {
                        if (wal.separateWall && wal.otherWall != null && wal.otherWall.node.building != null)
                        {
                            if (!separateWallChildMeshes.ContainsKey(wal.otherWall.node.building))
                            {
                                separateWallChildMeshes.Add(wal.otherWall.node.building, new List<MeshFilter>());
                            }

                            separateWallChildMeshes[wal.otherWall.node.building].Add(wal.spawnedCorner.GetComponent<MeshFilter>());
                        }
                        else
                        {
                            wallChildMeshes.Add(wal.spawnedCorner.GetComponent<MeshFilter>());
                        }
                    }

                    if (wal.spawnedCoving != null)
                    {
                        if (wal.separateWall && wal.otherWall != null && wal.otherWall.node.building != null)
                        {
                            if (!separateWallChildMeshes.ContainsKey(wal.otherWall.node.building))
                            {
                                separateWallChildMeshes.Add(wal.otherWall.node.building, new List<MeshFilter>());
                            }

                            separateWallChildMeshes[wal.otherWall.node.building].Add(wal.spawnedCoving.GetComponent<MeshFilter>());
                        }
                        else
                        {
                            wallChildMeshes.Add(wal.spawnedCoving.GetComponent<MeshFilter>());
                        }
                    }

                    if (wal.spawnedCornerCoving != null && wal.otherWall != null && wal.otherWall.node.building != null)
                    {
                        if (wal.separateWall)
                        {
                            if (!separateWallChildMeshes.ContainsKey(wal.otherWall.node.building))
                            {
                                separateWallChildMeshes.Add(wal.otherWall.node.building, new List<MeshFilter>());
                            }

                            separateWallChildMeshes[wal.otherWall.node.building].Add(wal.spawnedCornerCoving.GetComponent<MeshFilter>());
                        }
                        else
                        {
                            wallChildMeshes.Add(wal.spawnedCornerCoving.GetComponent<MeshFilter>());
                        }
                    }
                }
            }
        }
    }

    //Spawn room elements that cannot be combined (but we don't want as part of the caching process)
    public void SpawnExtraRoomElements(NewRoom room)
    {
        //Spawn walls & corners
        foreach (NewNode nod in room.nodes)
        {
            foreach (NewWall wal in nod.walls)
            {
                wal.SpawnFrontage(); //Frontage meshes are never combined
            }

            //Spawn stairs/elevators
            if (nod.tile.isInvertedStairwell && nod.tile.elevator == null)
            {
                nod.tile.SetAsStairwell(nod.tile.isInvertedStairwell, true, true);
            }

            if (nod.tile.isStairwell && nod.tile.stairwell == null)
            {
                nod.tile.SetAsStairwell(nod.tile.isStairwell, true, false);
            }
        }
    }

    //Get meshes from either the cache, or build them immediately...
    public void GetCombinedRoomMeshes(NewRoom room, out GameObject floor, out GameObject walls, out Dictionary<NewBuilding, GameObject> additionalWalls, out GameObject ceiling, out MeshRenderer floorRend, out MeshRenderer wallsRend, out MeshRenderer ceilingRend)
    {
        floor = null;
        floorRend = null;

        walls = null;
        wallsRend = null;

        additionalWalls = new Dictionary<NewBuilding, GameObject>();

        ceiling = null;
        ceilingRend = null;

        RoomMeshCache cached = null;

        //Retrieve mesh data from the cache and construct game objects...
        if (roomMeshes.TryGetValue(room, out cached))
        {
            floor = CreateGameObjectFromMesh(cached.floorMesh, room, "Default Floor Mesh", Game.Instance.roomFloorShadowMode, out floorRend);
            ProcessFloor(floor, room);

            walls = CreateGameObjectFromMesh(cached.wallMesh, room, "Default Walls Mesh", Game.Instance.roomWallShadowMode, out wallsRend);
            ProcessWall(walls, room);

            ceiling = CreateGameObjectFromMesh(cached.ceilingMesh, room, "Default Ceiling Mesh", Game.Instance.roomCeilingShadowMode, out ceilingRend);
            ProcessCeiling(ceiling, room);

            if (cached.additionalWallMesh != null)
            {
                int sep = 1;

                foreach (KeyValuePair<NewBuilding, Mesh> pair in cached.additionalWallMesh)
                {
                    GameObject addW = CreateGameObjectFromMesh(pair.Value, room, "Separate Wall Mesh " + sep, Game.Instance.roomWallShadowMode, out _);
                    ProcessWall(addW, room);

                    if (!additionalWalls.ContainsKey(pair.Key))
                    {
                        additionalWalls.Add(pair.Key, null);
                    }

                    additionalWalls[pair.Key] = addW;

                    sep++;
                }
            }
        }
        else
        {
            BuildCombinedMeshesForRoom(room, out _, out _, out _, out _, true, out floor, out walls, out additionalWalls, out ceiling, out floorRend, out wallsRend, out ceilingRend);
        }
    }

    //Build meshes that can be combined for a room...
    public void BuildCombinedMeshesForRoom(NewRoom room, out Mesh floorMesh, out Mesh wallMesh, out Dictionary<NewBuilding, Mesh> additionalWallMeshes, out Mesh ceilingMesh, bool returnGameObjects, out GameObject floor, out GameObject walls, out Dictionary<NewBuilding, GameObject> additionalWalls, out GameObject ceiling, out MeshRenderer floorRend, out MeshRenderer wallsRend, out MeshRenderer ceilingRend)
    {
        floorMesh = null;
        wallMesh = null;
        additionalWallMeshes = null;
        ceilingMesh = null;

        floor = null;
        walls = null;
        additionalWalls = new Dictionary<NewBuilding, GameObject>();
        ceiling = null;

        floorRend = null;
        wallsRend = null;
        ceilingRend = null;

        List<MeshFilter> wallChildMeshes = null;
        Dictionary<NewBuilding, List<MeshFilter>> separateWallChildMeshes = null;
        List<MeshFilter> floorChildMeshes = null;
        List<MeshFilter> ceilingChildMeshes = null;

        //Spawn all combinable bits
        SpawnModularRoomElements(room, true, out wallChildMeshes, out separateWallChildMeshes, out floorChildMeshes, out ceilingChildMeshes);

        //Combine wall meshes
        if (wallChildMeshes.Count > 0 || separateWallChildMeshes.Count > 0)
        {
            //You should now have a list of default room walls and a list of separate walls...
            wallMesh = CombineMeshes(ref wallChildMeshes);

            //Clean up objects
            while (wallChildMeshes.Count > 0)
            {
                wallChildMeshes[0].gameObject.SetActive(false);
                Destroy(wallChildMeshes[0].gameObject);
                wallChildMeshes.RemoveAt(0);
            }

            if (returnGameObjects)
            {
                walls = CreateGameObjectFromMesh(wallMesh, room, "Default Walls Mesh", Game.Instance.roomWallShadowMode, out wallsRend);
                ProcessWall(walls, room);
            }
        }

        //Combine additional wall meshes
        if (separateWallChildMeshes.Count > 0)
        {
            if (additionalWallMeshes == null) additionalWallMeshes = new Dictionary<NewBuilding, Mesh>();

            int sepCount = 1;

            foreach (KeyValuePair<NewBuilding, List<MeshFilter>> pair in separateWallChildMeshes)
            {
                List<MeshFilter> childMeshes = pair.Value;

                //Game.Log("Creating separate wall mesh for " + pair.Key.name + " (" + room.name + ")");
                Mesh sepMesh = CombineMeshes(ref childMeshes);

                if (!additionalWallMeshes.ContainsKey(pair.Key))
                {
                    additionalWallMeshes.Add(pair.Key, null);
                }

                additionalWallMeshes[pair.Key] = sepMesh;

                if (returnGameObjects)
                {
                    if (additionalWalls == null) additionalWalls = new Dictionary<NewBuilding, GameObject>();
                    GameObject newSepWall = CreateGameObjectFromMesh(sepMesh, room, "Separate Wall Mesh " + sepCount, Game.Instance.roomWallShadowMode, out _);
                    ProcessWall(newSepWall, room, pair.Key);

                    if (!additionalWalls.ContainsKey(pair.Key))
                    {
                        additionalWalls.Add(pair.Key, null);
                    }

                    additionalWalls[pair.Key] = newSepWall;
                }

                sepCount++;
            }

            List<MeshFilter> cleanUp = new List<MeshFilter>();

            foreach (KeyValuePair<NewBuilding, List<MeshFilter>> pair in separateWallChildMeshes)
            {
                cleanUp.AddRange(pair.Value);
            }

            while (cleanUp.Count > 0)
            {
                if (cleanUp[0] != null)
                {
                    cleanUp[0].gameObject.SetActive(false);
                    Destroy(cleanUp[0].gameObject);
                }

                cleanUp.RemoveAt(0);
            }
        }

        //Combine all floor meshes
        if (floorChildMeshes.Count > 0)
        {
            //You should now have a list of default room walls and a list of separate walls...
            floorMesh = CombineMeshes(ref floorChildMeshes);

            //Clean up objects
            while (floorChildMeshes.Count > 0)
            {
                floorChildMeshes[0].gameObject.SetActive(false);
                Destroy(floorChildMeshes[0].gameObject);
                floorChildMeshes.RemoveAt(0);
            }

            if (returnGameObjects)
            {
                floor = CreateGameObjectFromMesh(floorMesh, room, "Default Floor Mesh", Game.Instance.roomFloorShadowMode, out floorRend);
                ProcessFloor(floor, room);
            }
        }

        //Combine all ceiling meshes
        if (ceilingChildMeshes.Count > 0)
        {
            //You should now have a list of default room walls and a list of separate walls...
            ceilingMesh = CombineMeshes(ref ceilingChildMeshes);

            //Clean up objects
            while (ceilingChildMeshes.Count > 0)
            {
                ceilingChildMeshes[0].gameObject.SetActive(false);
                Destroy(ceilingChildMeshes[0].gameObject);
                ceilingChildMeshes.RemoveAt(0);
            }

            if (returnGameObjects)
            {
                ceiling = CreateGameObjectFromMesh(ceilingMesh, room, "Default Ceiling Mesh", Game.Instance.roomCeilingShadowMode, out ceilingRend);
                ProcessCeiling(ceiling, room);
            }
        }

        //Add to cache
        if(roomMeshes.Count < maxCache)
        {
            //Game.Log("Meshes: Adding new room to mesh cache: " + room.roomID + ", " + room.nodes.Count + " nodes");
            RoomMeshCache newCache = new RoomMeshCache();
            newCache.floorMesh = floorMesh;
            newCache.wallMesh = wallMesh;
            newCache.additionalWallMesh = additionalWallMeshes;
            newCache.ceilingMesh = ceilingMesh;

            if (!roomMeshes.ContainsKey(room))
            {
                roomMeshes.Add(room, newCache);
                generatedRoomMeshes = roomMeshes.Keys.Count;
            }
            else
            {
                roomMeshes[room] = newCache;
            }
        }
    }

    //Combine a list of meshes
    private Mesh CombineMeshes(ref List<MeshFilter> childMeshes)
    {
        //Create a mesh instance for combined meshes
        CombineInstance[] combine = new CombineInstance[childMeshes.Count];

        //Cycle through meshes to combine them
        int i = 0;

        while (i < childMeshes.Count)
        {
            combine[i].mesh = childMeshes[i].sharedMesh;
            combine[i].transform = childMeshes[i].transform.localToWorldMatrix;

            //As we still need colliders, only remove mesh filters and renderers from existing components
            Destroy(childMeshes[i].gameObject);

            i++;
        }

        //New mesh
        Mesh mesh = new Mesh();
        mesh.CombineMeshes(combine, true, true);

        return mesh;
    }

    public GameObject CreateGameObjectFromMesh(Mesh mesh, NewRoom room, string newName, UnityEngine.Rendering.ShadowCastingMode shadowMode, out MeshRenderer meshRenderer)
    {
        GameObject ret = new GameObject();
        ret.name = newName;

        //Add mesh components
        MeshFilter meshFilter = ret.AddComponent<MeshFilter>();
        meshRenderer = ret.AddComponent<MeshRenderer>();
        MeshCollider meshColl = ret.AddComponent<MeshCollider>();

        if(meshRenderer != null)
        {
            meshRenderer.shadowCastingMode = shadowMode;
            meshRenderer.staticShadowCaster = true;
        }

        //Set collision mesh
        meshColl.sharedMesh = mesh;

        //New mesh
        meshFilter.sharedMesh = mesh;
        ret.SetActive(true);

        //Parent container to building
        ret.transform.SetParent(room.transform);
        ret.transform.position = Vector3.zero;
        ret.transform.eulerAngles = Vector3.zero;

        //Set to block rain
        ret.layer = 29;

        return ret;
    }

    public void ProcessWall(GameObject wallObject, NewRoom room, NewBuilding building = null)
    {
        //Set light layer: This should include the vents
        bool ext = false;
        if (room.IsOutside() || room.gameLocation.IsOutside() || room.preset.forceStreetLightLayer || room.preset.forceOutside == RoomConfiguration.OutsideSetting.forceOutside) ext = true;

        if (ext && (building != null || room.building != null))
        {
            if (building != null)
            {
                if (building.extMat != null)
                {
                    MaterialsController.Instance.SetMaterialGroup(wallObject, building.extWallMaterial, building.preset.exteriorKey);
                }
                else
                {
                    building.extMat = MaterialsController.Instance.SetMaterialGroup(wallObject, building.extWallMaterial, building.preset.exteriorKey);
                }
            }
            else if (room.building != null)
            {
                if (room.building.extMat != null)
                {
                    MaterialsController.Instance.SetMaterialGroup(wallObject, room.building.extWallMaterial, room.building.preset.exteriorKey);
                }
                else
                {
                    room.building.extMat = MaterialsController.Instance.SetMaterialGroup(wallObject, room.building.extWallMaterial, room.building.preset.exteriorKey);
                }
            }
        }
        //No floor replacement
        else
        {
            MaterialGroupPreset wallMat = room.defaultWallMaterial;

            if (room.nodes.Where(item => item.floorType == NewNode.FloorTileType.CeilingOnly || item.floorType == NewNode.FloorTileType.noneButIndoors || item.tile.isStairwell || item.tile.isInvertedStairwell).FirstOrDefault() != null)
            {
                if (room.defaultWallMaterial != null && room.defaultWallMaterial.noFloorReplacement != null)
                {
                    wallMat = room.defaultWallMaterial.noFloorReplacement;
                }
            }

            room.wallMat = MaterialsController.Instance.SetMaterialGroup(wallObject, wallMat, room.defaultWallKey);
        }

        //Set light layer
        MeshRenderer meshRenderer = wallObject.GetComponent<MeshRenderer>();

        Toolbox.Instance.SetLightLayer(meshRenderer, room.building, ext);

        meshRenderer.shadowCastingMode = Game.Instance.roomWallShadowMode;
        meshRenderer.staticShadowCaster = true;

        //Static object has no benefit as it's not created in the editor
        wallObject.tag = "WallsMesh";
    }

    public void ProcessFloor(GameObject floorObject, NewRoom room)
    {
        //Set light layer: This should include the vents
        bool ext = false;
        if (room.IsOutside() || room.gameLocation.IsOutside() || room.preset.forceStreetLightLayer || room.preset.forceOutside == RoomConfiguration.OutsideSetting.forceOutside) ext = true;

        MeshRenderer meshRenderer = floorObject.GetComponent<MeshRenderer>();

        //Apply material
        room.floorMat = MaterialsController.Instance.SetMaterialGroup(room.combinedFloor, room.floorMaterial, room.floorMatKey);

        //Set light layer
        Toolbox.Instance.SetLightLayer(meshRenderer, room.building, ext);

        meshRenderer.shadowCastingMode = Game.Instance.roomFloorShadowMode;

        //Static object has no benefit as it's not created in the editor
        floorObject.tag = "FloorMesh";
    }

    public void ProcessCeiling(GameObject ceilingObject, NewRoom room)
    {
        //Set light layer: This should include the vents
        bool ext = false;
        if (room.IsOutside() || room.gameLocation.IsOutside() || room.preset.forceStreetLightLayer || room.preset.forceOutside == RoomConfiguration.OutsideSetting.forceOutside) ext = true;

        MeshRenderer meshRenderer = ceilingObject.GetComponent<MeshRenderer>();

        if (room.preset.boostCeilingEmission && room.ceilingMat != null) Destroy(room.ceilingMat);
        room.ceilingMat = MaterialsController.Instance.SetMaterialGroup(room.combinedCeiling, room.ceilingMaterial, room.ceilingMatKey, room.preset.boostCeilingEmission);
        room.uniqueCeilingMaterial = room.preset.boostCeilingEmission;

        //Boost ceiling emission
        if (room.preset.boostCeilingEmission && room.ceilingMat != null && room.uniqueCeilingMaterial)
        {
            if (room.mainLightStatus)
            {
                Color areaLightColour = Color.white;
                if (room.lightZones.Count > 0) areaLightColour = room.lightZones[0].areaLightColour;
                room.ceilingMat.SetColor("_EmissiveColor", room.preset.ceilingEmissionBoost * areaLightColour);
            }
            else
            {
                room.ceilingMat.SetColor("_EmissiveColor", Color.black);
            }
        }

        //Set light layer
        Toolbox.Instance.SetLightLayer(meshRenderer, room.building, ext);

        meshRenderer.shadowCastingMode = Game.Instance.roomCeilingShadowMode;

        //Static object has no benefit as it's not created in the editor
        ceilingObject.tag = "CeilingMesh";
    }

    //Start caching meshes in the background
    private void Update()
    {
        if (!backgroundCachingEnabled)
        {
            Game.Log("Meshes: Disabling mesh caching process");
            this.enabled = false;
            return;
        }
        else
        {
            uncachedRooms = toCache.Count;

            //Start a new thread
            //if(toCache.Count > 0 && threads.Count < Mathf.Min(threadsToUse, Game.Instance.maxThreads))
            //{
            //    if (!roomMeshes.ContainsKey(toCache[0]))
            //    {
            //        LoaderThread newThread = new LoaderThread { location = toCache[0] };
            //        newThread.thread = StartCoroutine(ThreadedMeshGeneration(newThread));
            //    }
            //    else toCache.RemoveAt(0);
            //}

            if (toCache.Count > 0 && roomMeshes.Count < maxCache && IsBackgroundCachingAllowed())
            {
                if(frameCounter <= 1)
                {
                    if (!roomMeshes.ContainsKey(toCache[0]))
                    {
                        BuildCombinedMeshesForRoom(toCache[0], out _, out _, out _, out _, false, out _, out _, out _, out _, out _, out _, out _);
                        toCache.RemoveAt(0);
                    }
                    else toCache.RemoveAt(0);

                    frameCounter = cacheRoomPerXFrames;
                }
                else
                {
                    frameCounter--;
                }
            }

            if (toCache.Count <= 0 || roomMeshes.Count >= maxCache)
            {
                Game.Log("Meshes: Stopping mesh caching process: " + toCache.Count + " left to cache");
                uncachedRooms = 0;
                backgroundCachingEnabled = false;
                this.enabled = false;
            }
        }
    }

    private bool IsBackgroundCachingAllowed()
    {
        if (SessionData.Instance.startedGame)
        {
            if(SessionData.Instance.play)
            {
                if(!CutSceneController.Instance.cutSceneActive)
                {
                    if (!Player.Instance.isOnStreet) return true;
                }
            }
        }

        return false;
    }

    //Threaded generation: This doesn't work as we need to access transforms not on the main thread :(
    IEnumerator ThreadedMeshGeneration(LoaderThread loaderReference)
    {
        loaderReference.isDone = false;

        threads.Add(loaderReference);
        Game.Log("Meshes: Mesh creator threads: " + threads.Count);

        Thread thread = new Thread(() =>
        {
            BuildCombinedMeshesForRoom(loaderReference.location, out _, out _, out _, out _, false, out _, out _, out _, out _, out _, out _, out _);

            loaderReference.isDone = true;
        });

        thread.Start();

        //Do nothing on each frame until the thread is done
        while (!loaderReference.isDone || thread.IsAlive)
        {
            yield return null;
        }

        threads.Remove(loaderReference); //Remove thread reference
                                         //Game.Log("CityGen: Finished thread for room cursor " + roomCursor);
    }

    [Button]
    public void StartCachingProcess()
    {
        Game.Log("Meshes: Starting mesh caching process...");
        backgroundCachingEnabled = true;

        toCache = new List<NewRoom>();

        //Put together a list of rooms to cache
        foreach (NewRoom r in CityData.Instance.roomDirectory)
        {
            if(!roomMeshes.ContainsKey(r))
            {
                if(r.nodes.Count >= 2)
                {
                    toCache.Add(r);
                }
            }
        }

        //Order by node count
        toCache.Sort((p2, p1) => p1.nodes.Count.CompareTo(p2.nodes.Count)); //Highest first

        this.enabled = backgroundCachingEnabled;
    }
}
