﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "neonsign_data", menuName = "Database/Neon Sign Characters")]

public class NeonSignCharacters : SoCustomComparison
{
    [System.Serializable]
    public class NeonCharacter
    {
        public string character;
        public GameObject prefab;
    }

    public List<NeonCharacter> characterList = new List<NeonCharacter>();
}
