﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using TMPro;
using UnityEngine.UI;

public class ColourPickerController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public WindowContentController wcc;
    public RectTransform entryParent;
    public RectTransform spawnParent;
    public GameObject swatchPrefab;

    [Header("State")]
    public bool isSetup = false;
    public Color selectedColor = Color.white;
    public List<SwatchController> spawnedEntries = new List<SwatchController>();

    //Triggered when the window is closed
    public delegate void NewColour(Color newColour);
    public event NewColour OnNewColour;

    public void Setup(WindowContentController newContentController)
    {
        rect = this.gameObject.GetComponent<RectTransform>();

        wcc = newContentController;

        SetPageSize(new Vector2(338, 300));

        //Update list
        UpdateListDisplay();

        isSetup = true;
    }

    public void SetPageSize(Vector2 newSize)
    {
        rect.sizeDelta = newSize;
        wcc.normalSize = rect.sizeDelta;
    }

    //Update facts list
    public void UpdateListDisplay()
    {
        //Force deselect
        if (InterfaceController.Instance.selectedElement != null) InterfaceController.Instance.selectedElement.OnDeselect();

        rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 0);

        //Compile a list of file icons that should exist
        List<Color> required = new List<Color>(PlayerApartmentController.Instance.swatches);

        //Go through existing list
        for (int i = 0; i < spawnedEntries.Count; i++)
        {
            SwatchController sec = spawnedEntries[i];

            //If a null entry, this has obviously been destroyed- remove entry
            if (sec == null)
            {
                spawnedEntries.RemoveAt(i);
                i--;
                continue;
            }

            if(required.Contains(sec.background.color))
            {
                required.Remove(sec.background.color);
                sec.VisualUpdate();
            }
            else
            {
                Destroy(sec.gameObject);
                spawnedEntries.RemoveAt(i);
                i--;
                continue;
            }
        }

        //The ones left in the found files list are buttons to spawn
        foreach (Color sec in required)
        {
            GameObject newButton = null;

            newButton = Instantiate(swatchPrefab, spawnParent);

            SwatchController newEntry = newButton.GetComponent<SwatchController>();

            newEntry.Setup(sec, this);
            spawnedEntries.Add(newEntry);
        }

        for (int i = 0; i < spawnedEntries.Count; i++)
        {
            spawnedEntries[i].transform.SetAsLastSibling();
        }

        //Update rect sizes...
        entryParent.sizeDelta = new Vector2(entryParent.sizeDelta.x, ((spawnedEntries.Count / 6f) * 48f) + 16f);
        SetPageSize(new Vector2(rect.sizeDelta.x, entryParent.sizeDelta.y + 34f));
    }

    public void OnPickNewColour(SwatchController swatch)
    {
        selectedColor = swatch.baseColour;

        if(OnNewColour != null)
        {
            OnNewColour(selectedColor);
        }

        wcc.window.CloseWindow(false);
    }
}
