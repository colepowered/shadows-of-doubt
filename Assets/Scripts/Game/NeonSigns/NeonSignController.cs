﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEditor;

public class NeonSignController : MonoBehaviour
{
    public List<MeshRenderer> meshRenderers = new List<MeshRenderer>();
    public Light lightComponent;

    [ReorderableList]
    public List<Material> materialAnimations = new List<Material>();
    [ReorderableList]
    public List<bool> lightBools = new List<bool>();

    public int frameCursor = 0;
    public int frameDelay = 1;
    private float frameCounter = 0f;

    [Header("Materials")]
    public bool useAddressColours = true;
    [EnableIf("useAddressColours")]
    public bool changeBaseColour = true;
    [EnableIf("useAddressColours")]
    public bool changeAltColour1 = false;
    [EnableIf("useAddressColours")]
    public bool changeAltColour2 = false;
    [EnableIf("useAddressColours")]
    public bool changeAltColour3 = false;

    [Header("Audio")]
    public AudioEvent audioLoop;
    public Vector3 localSoundOffset;
    private AudioController.LoopingSoundInfo loop;
    private NewNode closestStreetNode;

    private void OnEnable()
    {
        if(Toolbox.Instance != null) frameCounter = Toolbox.Instance.Rand(0f, (float)frameDelay);

        //Play sound
        if(audioLoop != null && loop == null)
        {
            //Find the closest street node
            if(closestStreetNode == null)
            {
                closestStreetNode = Toolbox.Instance.FindClosestValidNodeToWorldPosition(this.transform.TransformPoint(localSoundOffset), limitToDirection: true, limitedDirection: new Vector3Int(0, 0, -1));
            }

            loop = AudioController.Instance.PlayWorldLoopingStatic(audioLoop, null, closestStreetNode, this.transform.TransformPoint(localSoundOffset), pauseWhenGamePaused: true);
        }
    }

    private void OnDisable()
    {
        if(loop != null)
        {
            AudioController.Instance.StopSound(loop, AudioController.StopType.fade, "Neon disabled");
            loop = null;
        }
    }

    //Update is called once per frame
    void Update()
    {
        if(SessionData.Instance.play && materialAnimations.Count > 0)
        {
            frameCounter += Time.deltaTime;

            if (frameCounter >= frameDelay)
            {
                frameCounter -= frameDelay;

                frameCursor++;

                if (frameCursor >= materialAnimations.Count)
                {
                    frameCursor = 0;
                }

                foreach(MeshRenderer r in meshRenderers)
                {
                    if (r == null) continue;

                    try
                    {
                        r.sharedMaterial = materialAnimations[frameCursor];
                    }
                    catch
                    {

                    }
                }

                try
                {
                    if (lightBools[frameCursor])
                    {
                        if(lightComponent != null) lightComponent.enabled = true;

                        //Also pause/enable sfx
                        if (loop != null)
                        {
                            loop.audioEvent.setPaused(false);
                        }
                    }
                    else
                    {
                        if (lightComponent != null) lightComponent.enabled = false;

                        //Also pause/enable sfx
                        if (loop != null)
                        {
                            loop.audioEvent.setPaused(true);
                        }
                    }
                }
                catch
                {

                }
            }
        }
    }
}
