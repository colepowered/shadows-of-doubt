﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleController : MonoBehaviour
{
    public Transform vehicle;

    void OnTriggerEnter(Collider other)
    {
        if(Player.Instance.currentVehicle == null && !Player.Instance.playerKOInProgress)
        {
            if (other.tag == "Player")
            {
                Player.Instance.SetVehicle(vehicle);
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(Player.Instance.currentVehicle != null && !Player.Instance.playerKOInProgress)
        {
            if (other.tag == "Player")
            {
                Player.Instance.SetVehicle(null);
            }
        }
    }
}
