﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using NaughtyAttributes;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Data;
using System.Linq;

public class UpgradesController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform mainContentRect;
    public RectTransform mainViewport;
    public CustomScrollRect mainScrollRect;
    public RectTransform listContentRect;
    public RectTransform listRect;
    [Space(7)]
    public ButtonController closeButton;

    public TextMeshProUGUI installedDisksText;
    public TextMeshProUGUI syncClinicPromptText;
    public TextMeshProUGUI configText;
    public TextMeshProUGUI upgradesText;
    public TextMeshProUGUI sideEffectsText;
    public TextMeshProUGUI descriptionText;
    public TextMeshProUGUI optionsText;
    public GameObject syncDiskElementPrefab;

    [Header("State")]
    [NonSerialized]
    public float openProgress = 0f;
    public bool isOpen = false;
    public bool installedAllowed = false;
    public int notInstalled = 0;
    public bool playSyncDiskInstallAudio = false; //Play audio when resumed game

    public List<Upgrades> upgrades = new List<Upgrades>();
    public List<SyncDiskElementController> spawnedDisks = new List<SyncDiskElementController>();
    public Dictionary<string, SyncDiskPreset> upgradesQuickRef = new Dictionary<string, SyncDiskPreset>();
    public List<Interactable> upgradeVials = new List<Interactable>();

    public enum SyncDiskState { notInstalled, option1, option2, option3 };

    [System.Serializable]
    public class Upgrades
    {
        public string upgrade;
        public SyncDiskState state = SyncDiskState.notInstalled;
        public int list; //List position
        public int level = 0; //Upgraded level
        public int objId = -1; //If this is installed, reference to the object id
        public int uninstallCost = 0;

        [NonSerialized]
        public SyncDiskPreset preset;

        public SyncDiskPreset GetPreset()
        {
            if (preset != null) return preset;

            Toolbox.Instance.LoadDataFromResources<SyncDiskPreset>(upgrade, out preset);
            return preset;
        }

        public Interactable GetObject()
        {
            Interactable ret = null;
            CityData.Instance.savableInteractableDictionary.TryGetValue(objId, out ret);
            return ret;
        }

        public List<UpgradeEffectController.AppliedEffect> GetAllEffects()
        {
            GetPreset();
            Game.Log("Gameplay: Getting all effects for " + preset.name + "...");

            List<UpgradeEffectController.AppliedEffect> ret = new List<UpgradeEffectController.AppliedEffect>(GetMainEffects());

            //Add upgrade effects
            ret.AddRange(GetUpgradeEffects());

            //Add side effects
            ret.AddRange(GetSideEffects());

            Game.Log("Gameplay: " + ret.Count +" total effects for " + preset.name + "...");

            return ret;
        }

        public List<UpgradeEffectController.AppliedEffect> GetMainEffects()
        {
            GetPreset();
            Game.Log("Gameplay: Getting main effects for " + preset.name + "...");

            List<UpgradeEffectController.AppliedEffect> ret = new List<UpgradeEffectController.AppliedEffect>();

            //Get main effects
            if (state == SyncDiskState.option1)
            {
                ret.Add(new UpgradeEffectController.AppliedEffect { effect = preset.mainEffect1, value = GetEffectiveness(), disk = this });
            }
            else if (state == SyncDiskState.option2)
            {
                ret.Add(new UpgradeEffectController.AppliedEffect { effect = preset.mainEffect2, value = GetEffectiveness(), disk = this });
            }
            else if (state == SyncDiskState.option3)
            {
                ret.Add(new UpgradeEffectController.AppliedEffect { effect = preset.mainEffect3, value = GetEffectiveness(), disk = this });
            }

            Game.Log("Gameplay: Returning " + ret.Count + " main effects for " + preset.name + "...");

            return ret;
        }

        public List<UpgradeEffectController.AppliedEffect> GetUpgradeEffects()
        {
            GetPreset();
            Game.Log("Gameplay: Getting upgrade effects for " + preset.name + ": " + state.ToString() + " level " + level + "...");

            List<SyncDiskPreset.UpgradeEffect> upgradeFX = new List<SyncDiskPreset.UpgradeEffect>();
            List<float> upgradeValues = new List<float>();

            if(level > 0)
            {
                if (state == SyncDiskState.option1)
                {
                    if (level >= 1 && preset.option1UpgradeEffects.Count > 0)
                    {
                        upgradeFX.Add(preset.option1UpgradeEffects[0]);
                        upgradeValues.Add(preset.option1UpgradeValues[0]);
                    }

                    if (level >= 2 && preset.option1UpgradeEffects.Count > 1)
                    {
                        upgradeFX.Add(preset.option1UpgradeEffects[1]);
                        upgradeValues.Add(preset.option1UpgradeValues[1]);
                    }

                    if (level >= 3 && preset.option1UpgradeEffects.Count > 2)
                    {
                        upgradeFX.Add(preset.option1UpgradeEffects[2]);
                        upgradeValues.Add(preset.option1UpgradeValues[2]);
                    }
                }
                else if (state == SyncDiskState.option2)
                {
                    if (level >= 1 && preset.option2UpgradeEffects.Count > 0)
                    {
                        upgradeFX.Add(preset.option2UpgradeEffects[0]);
                        upgradeValues.Add(preset.option2UpgradeValues[0]);
                    }

                    if (level >= 2 && preset.option2UpgradeEffects.Count > 1)
                    {
                        upgradeFX.Add(preset.option2UpgradeEffects[1]);
                        upgradeValues.Add(preset.option2UpgradeValues[1]);
                    }

                    if (level >= 3 && preset.option2UpgradeEffects.Count > 2)
                    {
                        upgradeFX.Add(preset.option2UpgradeEffects[2]);
                        upgradeValues.Add(preset.option2UpgradeValues[2]);
                    }
                }
                else if (state == SyncDiskState.option3)
                {
                    if (level >= 1 && preset.option3UpgradeEffects.Count > 0)
                    {
                        upgradeFX.Add(preset.option3UpgradeEffects[0]);
                        upgradeValues.Add(preset.option3UpgradeValues[0]);
                    }

                    if (level >= 2 && preset.option3UpgradeEffects.Count > 1)
                    {
                        upgradeFX.Add(preset.option3UpgradeEffects[1]);
                        upgradeValues.Add(preset.option3UpgradeValues[1]);
                    }

                    if (level >= 3 && preset.option3UpgradeEffects.Count > 2)
                    {
                        upgradeFX.Add(preset.option3UpgradeEffects[2]);
                        upgradeValues.Add(preset.option3UpgradeValues[2]);
                    }
                }
            }

            Game.Log("Gameplay: Parsing from " + upgradeFX.Count + " effects...");

            List<UpgradeEffectController.AppliedEffect> ret = new List<UpgradeEffectController.AppliedEffect>();

            for (int i = 0; i < upgradeFX.Count; i++)
            {
                SyncDiskPreset.UpgradeEffect effect = upgradeFX[i];
                float value = upgradeValues[i];

                //APPLY upgrade special case effects here...
                if (effect == SyncDiskPreset.UpgradeEffect.readingSeriesBonus)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.readingSeriesBonus, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.bothConfigurations)
                {
                    List<UpgradeEffectController.AppliedEffect> main = GetMainEffects();

                    if(!main.Exists(item => item.effect == preset.mainEffect1) && preset.mainEffect1 != SyncDiskPreset.Effect.none) ret.Add(new UpgradeEffectController.AppliedEffect { effect = preset.mainEffect1, value = preset.mainEffect1Value, disk = this });
                    if (!main.Exists(item => item.effect == preset.mainEffect2) && preset.mainEffect2 != SyncDiskPreset.Effect.none) ret.Add(new UpgradeEffectController.AppliedEffect { effect = preset.mainEffect2, value = preset.mainEffect2Value, disk = this });
                    if (!main.Exists(item => item.effect == preset.mainEffect3) && preset.mainEffect3 != SyncDiskPreset.Effect.none) ret.Add(new UpgradeEffectController.AppliedEffect { effect = preset.mainEffect3, value = preset.mainEffect3Value, disk = this });
                }
                else if(effect == SyncDiskPreset.UpgradeEffect.reduceMedicalCosts)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.reduceMedicalCosts, value = value });
                }
                else if(effect == SyncDiskPreset.UpgradeEffect.accidentCover)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.accidentCover, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.legalInsurance)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.legalInsurance, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.awakenAtHome)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.awakenAtHome, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.increaseHealth)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.increaseHealth, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.increaseInventory)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.increaseInventory, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.increaseRegeneration)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.increaseRegeneration, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.priceModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.priceModifier, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.dialogChanceModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.dialogChanceModifier, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.doorBargeModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.doorBargeModifier, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.fallDamageModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.fallDamageModifier, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.sideJobPayModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.sideJobPayModifier, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.throwPowerModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.throwPowerModifier, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.punchPowerModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.punchPowerModifier, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.blockIncoming)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.blockIncoming, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.focusFromDamage)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.focusFromDamage, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.noBrokenBones)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.noBrokenBones, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.reachModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.reachModifier, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.holdingBlocksBullets)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.holdingBlocksBullets, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.fistsThreatModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.fistsThreatModifier, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.noBleeding)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.noBleeding, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.incomingDamageModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.incomingDamageModifier, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.passiveIncome)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.passiveIncome, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.installMalware)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.installMalware, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.malwareOwnerBonus)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.malwareOwnerBonus, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.agePerception)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.agePerception, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.footSizePerception)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.footSizePerception, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.heightPerception)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.heightPerception, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.salaryPerception)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.salaryPerception, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.singlePerception)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.singlePerception, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.wealthPerception)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.wealthPerception, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.starchAmbassador)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.starchAmbassador, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.starchGive)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.starchGive, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.lockpickingEfficiencyModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.lockpickingEfficiencyModifier, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.lockpickingSpeedModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.lockpickingSpeedModifier, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.triggerIllegalOnPick)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.triggerIllegalOnPick, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.securityBreakerModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.securityBreakerModifier, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.KOTimeModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.KOTimeModifier, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.noSmelly)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.noSmelly, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.noCold)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.noCold, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.noTired)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.noTired, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.kitchenPhotos)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.kitchenPhotos, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.bathroomPhotos)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.bathroomPhotos, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.illegalOpsPhotos)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.illegalOpsPhotos, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.removeSideEffect)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.removeSideEffect, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.moneyForLocations)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.moneyForLocations, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.moneyForDucts)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.moneyForDucts, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.moneyForPasscodes)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.moneyForPasscodes, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.moneyForAddresses)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.moneyForAddresses, value = value });
                }
                else if (effect == SyncDiskPreset.UpgradeEffect.playerHeightModifier)
                {
                    ret.Add(new UpgradeEffectController.AppliedEffect { effect = SyncDiskPreset.Effect.playerHeightModifier, value = value });
                }
            }

            Game.Log("Gameplay: Returning " + ret.Count + " upgrade effects for " + preset.name + "...");

            return ret;
        }

        public List<UpgradeEffectController.AppliedEffect> GetSideEffects()
        {
            GetPreset();
            Game.Log("Gameplay: Getting side effects for " + preset.name + "...");

            List<UpgradeEffectController.AppliedEffect> ret = new List<UpgradeEffectController.AppliedEffect>();

            if(preset.sideEffect != SyncDiskPreset.Effect.none)
            {
                ret.Add(new UpgradeEffectController.AppliedEffect { effect = preset.sideEffect, value = GetSideEffectValue(), disk = this });
            }

            Game.Log("Gameplay: Returning " + ret.Count + " side effects for " + preset.name + "...");

            return ret;
        }

        public float GetEffectiveness()
        {
            float ret = 0f;
            GetPreset();

            if (state == SyncDiskState.option1)
            {
                ret = preset.mainEffect1Value;

                if (level >= 1 && preset.option1UpgradeEffects.Count > 0 && preset.option1UpgradeValues.Count > 0 && preset.option1UpgradeEffects[0] == SyncDiskPreset.UpgradeEffect.modifyEffect)
                {
                    ret += preset.option1UpgradeValues[0];
                }

                if (level >= 2 && preset.option1UpgradeEffects.Count > 1 && preset.option1UpgradeValues.Count > 1 && preset.option1UpgradeEffects[1] == SyncDiskPreset.UpgradeEffect.modifyEffect)
                {
                    ret += preset.option1UpgradeValues[1];
                }

                if (level >= 3 && preset.option1UpgradeEffects.Count > 2 && preset.option1UpgradeValues.Count > 2 && preset.option1UpgradeEffects[2] == SyncDiskPreset.UpgradeEffect.modifyEffect)
                {
                    ret += preset.option1UpgradeValues[2];
                }
            }
            else if (state == SyncDiskState.option2)
            {
                ret = preset.mainEffect2Value;

                if (level >= 1 && preset.option2UpgradeEffects.Count > 0 && preset.option2UpgradeValues.Count > 0 && preset.option2UpgradeEffects[0] == SyncDiskPreset.UpgradeEffect.modifyEffect)
                {
                    ret += preset.option2UpgradeValues[0];
                }

                if (level >= 2 && preset.option2UpgradeEffects.Count > 1 && preset.option2UpgradeValues.Count > 1 && preset.option2UpgradeEffects[1] == SyncDiskPreset.UpgradeEffect.modifyEffect)
                {
                    ret += preset.option2UpgradeValues[1];
                }

                if (level >= 3 && preset.option2UpgradeEffects.Count > 2 && preset.option2UpgradeValues.Count > 2 && preset.option2UpgradeEffects[2] == SyncDiskPreset.UpgradeEffect.modifyEffect)
                {
                    ret += preset.option2UpgradeValues[2];
                }
            }
            else if (state == SyncDiskState.option3)
            {
                ret = preset.mainEffect3Value;

                if (level >= 1 && preset.option3UpgradeEffects.Count > 0 && preset.option3UpgradeValues.Count > 0 && preset.option3UpgradeEffects[0] == SyncDiskPreset.UpgradeEffect.modifyEffect)
                {
                    ret += preset.option3UpgradeValues[0];
                }

                if (level >= 2 && preset.option3UpgradeEffects.Count > 1 && preset.option3UpgradeValues.Count > 1 && preset.option3UpgradeEffects[1] == SyncDiskPreset.UpgradeEffect.modifyEffect)
                {
                    ret += preset.option3UpgradeValues[1];
                }

                if (level >= 3 && preset.option3UpgradeEffects.Count > 2 && preset.option3UpgradeValues.Count > 2 && preset.option3UpgradeEffects[2] == SyncDiskPreset.UpgradeEffect.modifyEffect)
                {
                    ret += preset.option3UpgradeValues[2];
                }
            }

            return ret;
        }

        public float GetSideEffectValue()
        {
            GetPreset();

            if(GetUpgradeEffects().Exists(item => item.effect == SyncDiskPreset.Effect.removeSideEffect))
            {
                return 0f;
            }

            return preset.sideEffectValue;
        }
    }

    //Singleton pattern
    private static UpgradesController _instance;
    public static UpgradesController Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void SetupQuickRef()
    {
        foreach(SyncDiskPreset p in Toolbox.Instance.allSyncDisks)
        {
            upgradesQuickRef.Add(p.name, p);
        }
    }

    public void Setup()
    {
        if (upgradesQuickRef.Count <= 0) SetupQuickRef();

        upgrades.Clear();

        Game.Log("Gameplay: Setup upgrades, give all: " + Game.Instance.giveAllUpgrades);

        syncClinicPromptText.text = "*" + Strings.Get("ui.interface", "Find a sync clinic to install, uninstall or upgrade sync disks", useGenderReference: true, genderReference: Player.Instance);

        configText.text = Strings.Get("ui.interface", "Configuration");
        upgradesText.text = Strings.Get("ui.interface", "Upgrades");
        sideEffectsText.text = Strings.Get("ui.interface", "Side Effect");
        descriptionText.text = Strings.Get("ui.interface", "Description");
        optionsText.text = Strings.Get("ui.interface", "Options");

        UpdateUpgrades();
    }

    public void UpdateUpgrades()
    {
        Game.Log("Interface: Update upgrades...");

        List<Upgrades> required = new List<Upgrades>(upgrades);
        upgradeVials.Clear();
        notInstalled = 0;

        //Add inventory to required
        foreach(FirstPersonItemController.InventorySlot slot in FirstPersonItemController.Instance.slots)
        {
            Interactable i = slot.GetInteractable();

            if(i != null)
            {
                if(i.syncDisk != null)
                {
                    Upgrades newUpgrade = new Upgrades { upgrade = i.syncDisk.name, state = SyncDiskState.notInstalled, objId = i.id, uninstallCost = i.syncDisk.uninstallCost };
                    required.Add(newUpgrade);
                    notInstalled++;
                }
                else if(i.preset.specialCaseFlag == InteractablePreset.SpecialCase.syncDiskUpgrade)
                {
                    if(!upgradeVials.Contains(i))
                    {
                        upgradeVials.Add(i);
                    }
                }
            }
        }

        for (int i = 0; i < spawnedDisks.Count; i++)
        {
            SyncDiskElementController spawned = spawnedDisks[i];

            if(required.Contains(spawned.upgrade))
            {
                spawned.VisualUpdate();
                required.Remove(spawned.upgrade);
            }
            else
            {
                Destroy(spawned.gameObject);
                spawnedDisks.RemoveAt(i);
                i--;
                continue;
            }
        }

        foreach(Upgrades up in required)
        {
            GameObject newObj = Instantiate(syncDiskElementPrefab, listRect);
            SyncDiskElementController usd = newObj.GetComponent<SyncDiskElementController>();

            if (upgradesQuickRef.Count <= 0) SetupQuickRef();
            usd.Setup(up);

            spawnedDisks.Add(usd);
        }

        //Set list contents
        spawnedDisks.Sort((p2, p1) => (p1.upgrade.list - (Mathf.Min((int)p1.upgrade.state, 1) * 10000)).CompareTo((p2.upgrade.list - (Mathf.Min((int)p2.upgrade.state, 1) * 10000)))); //Lowest number first

        float listY = -16;

        for (int i = 0; i < spawnedDisks.Count; i++)
        {
            SyncDiskElementController spawned = spawnedDisks[i];

            spawned.rect.localPosition = new Vector2(spawned.rect.localPosition.x, listY);

            listY -= 180; //Content
            listY -= 0; //Padding
        }

        //Bottom padding
        listY -= 16;

        listRect.sizeDelta = new Vector2(listRect.sizeDelta.x, -listY);
        listContentRect.sizeDelta = new Vector2(listContentRect.sizeDelta.x, Mathf.Max(736, listRect.sizeDelta.y + 120));

        UpdateInstalledAvailableText();
        UpdateNavigation();
        UpdateActivation();

        InterfaceController.Instance.upgradesButton.notifications.SetNotifications(notInstalled + upgradeVials.Count);
    }

    public void InstallSyncDisk(Upgrades application, int option)
    {
        if (upgrades.Exists(item => item.upgrade == application.upgrade)) return; //Don't do this if this type is already installed.

        //Give slot # etc
        application.state = (SyncDiskState)option;
        application.list = 0;
        application.level = 0; //Start at level 0 always

        //Bump list down for each
        foreach(Upgrades u in upgrades)
        {
            u.list++;
        }

        upgrades.Insert(0, application);

        //Remove physical object
        Interactable obj = application.GetObject();

        if(obj != null)
        {
            obj.SafeDelete(true);
            application.objId = -1;
        }
        else
        {
            Game.LogError("Unable to get sync disk object! Cannot delete...");
        }

        AudioController.Instance.Play2DSound(AudioControls.Instance.syncDiskInstall);
        Player.Instance.AddSyncDiskInstall(1f);
        playSyncDiskInstallAudio = true;
        StatusController.Instance.ForceStatusCheck();

        UpdateUpgrades();

        //Perform installation one-off
        List<UpgradeEffectController.AppliedEffect> main = application.GetMainEffects();

        foreach(UpgradeEffectController.AppliedEffect effect in main)
        {
            UpgradeEffectController.Instance.OnInstall(application, effect.effect, effect.value);
        }
    }

    public void UninstallSyncDisk(Upgrades removal)
    {
        upgrades.Remove(removal);
        UpdateUpgrades();

        AudioController.Instance.Play2DSound(AudioControls.Instance.syncDiskUninstall);

        //Perform installation one-off
        List<UpgradeEffectController.AppliedEffect> main = removal.GetMainEffects();

        foreach (UpgradeEffectController.AppliedEffect effect in main)
        {
            UpgradeEffectController.Instance.OnUninstall(removal, effect.effect, effect.value);
        }
    }

    public void UpgradeSyncDisk(Upgrades upgradeThis)
    {
        if(upgradeThis != null && upgradeVials.Count > 0)
        {
            upgradeThis.level++;

            upgradeVials[0].SafeDelete(true);

            UpdateUpgrades();

            AudioController.Instance.Play2DSound(AudioControls.Instance.syncDiskUpgrade);

            upgradeThis.GetPreset();

            //Perform installation one-off
            if(upgradeThis.level == 1)
            {
                try
                {
                    if(upgradeThis.state == SyncDiskState.option1) UpgradeEffectController.Instance.OnUpgrade(upgradeThis, upgradeThis.preset.option1UpgradeEffects[0], upgradeThis.preset.option1UpgradeValues[0], 1);
                    else if (upgradeThis.state == SyncDiskState.option2) UpgradeEffectController.Instance.OnUpgrade(upgradeThis, upgradeThis.preset.option2UpgradeEffects[0], upgradeThis.preset.option2UpgradeValues[0], 1);
                    else if (upgradeThis.state == SyncDiskState.option3) UpgradeEffectController.Instance.OnUpgrade(upgradeThis, upgradeThis.preset.option3UpgradeEffects[0], upgradeThis.preset.option3UpgradeValues[0], 1);
                }
                catch
                {
                    Game.LogError("Could not execute one-shot upgrade effect for " + upgradeThis.preset.name);
                }
            }
            else if(upgradeThis.level == 2)
            {
                try
                {
                    if (upgradeThis.state == SyncDiskState.option1) UpgradeEffectController.Instance.OnUpgrade(upgradeThis, upgradeThis.preset.option1UpgradeEffects[1], upgradeThis.preset.option1UpgradeValues[1], 2);
                    else if (upgradeThis.state == SyncDiskState.option2) UpgradeEffectController.Instance.OnUpgrade(upgradeThis, upgradeThis.preset.option2UpgradeEffects[1], upgradeThis.preset.option2UpgradeValues[1], 2);
                    else if (upgradeThis.state == SyncDiskState.option3) UpgradeEffectController.Instance.OnUpgrade(upgradeThis, upgradeThis.preset.option3UpgradeEffects[1], upgradeThis.preset.option3UpgradeValues[1], 2);
                }
                catch
                {
                    Game.LogError("Could not execute one-shot upgrade effect for " + upgradeThis.preset.name);
                }
            }
            else if(upgradeThis.level == 3)
            {
                try
                {
                    if (upgradeThis.state == SyncDiskState.option1) UpgradeEffectController.Instance.OnUpgrade(upgradeThis, upgradeThis.preset.option1UpgradeEffects[2], upgradeThis.preset.option1UpgradeValues[2], 3);
                    else if (upgradeThis.state == SyncDiskState.option2) UpgradeEffectController.Instance.OnUpgrade(upgradeThis, upgradeThis.preset.option2UpgradeEffects[2], upgradeThis.preset.option2UpgradeValues[2], 3);
                    else if (upgradeThis.state == SyncDiskState.option3) UpgradeEffectController.Instance.OnUpgrade(upgradeThis, upgradeThis.preset.option3UpgradeEffects[2], upgradeThis.preset.option3UpgradeValues[2], 3);
                }
                catch
                {
                    Game.LogError("Could not execute one-shot upgrade effect for " + upgradeThis.preset.name);
                }
            }
        }
    }

    //Update when sync disks can be installed
    public void UpdateInstallButton(bool newInstallAllowed)
    {
        installedAllowed = newInstallAllowed;

        foreach (SyncDiskElementController element in spawnedDisks)
        {
            element.SetInstallAllowed(installedAllowed);
        }
    }

    public void UpdateInstalledAvailableText()
    {
        installedDisksText.text = Strings.Get("ui.interface", "Installed Disks") + ": " + upgrades.FindAll(item => item.state != SyncDiskState.notInstalled).Count + "/" + Toolbox.Instance.allSyncDisks.Count + ", " + Strings.Get("ui.interface", "Available Disks") + ": " + notInstalled + ", " + Strings.Get("ui.interface", "Available Upgrades") + ": " + upgradeVials.Count;
    }

    public void OpenUpgrades(bool playSound = true)
    {
        InterfaceController.Instance.upgradesCanvas.gameObject.SetActive(true);
        this.enabled = true;
        isOpen = true;

        //Close inventory
        if(BioScreenController.Instance.isOpen)
        {
            BioScreenController.Instance.SetInventoryOpen(false, false);
        }

        StopCoroutine("Open");
        StopCoroutine("Close");
        StartCoroutine("Open");

        //Play SFX
        if (playSound) AudioController.Instance.Play2DSound(AudioControls.Instance.mapSlideIn);

        //Enable top bar navigation
        //Toolbox.Instance.AddNavigationInput(InterfaceController.Instance.notebookButton.button, newDown: availableButton.button);
        //Toolbox.Instance.AddNavigationInput(InterfaceController.Instance.upgradesButton.button, newDown: availableButton.button);
        //Toolbox.Instance.AddNavigationInput(InterfaceController.Instance.mapButton.button, newDown: availableButton.button);
        //Toolbox.Instance.AddNavigationInput(CasePanelController.Instance.selectNoCaseButton.button, newDown: sideEffectsButton.button);
        //Toolbox.Instance.AddNavigationInput(CasePanelController.Instance.stickNoteButton.button, newDown: sideEffectsButton.button);

        UpdateNavigation();

        if(!InputController.Instance.mouseInputMode)
        {
            CasePanelController.Instance.SetControllerMode(true, CasePanelController.ControllerSelectMode.topBar);
        }

        InterfaceController.Instance.upgradesButton.icon.enabled = isOpen;
    }

    IEnumerator Open()
    {
        while (openProgress < 1f)
        {
            openProgress += Time.deltaTime * 20f;
            openProgress = Mathf.Clamp01(openProgress);
            InterfaceController.Instance.upgradesCanvasGroup.alpha = openProgress;

            yield return null;
        }

        //Enable drag
        mainScrollRect.enabled = true;
    }

    public void CloseUpgrades(bool playSound = true)
    {
        //Game.Log("CloseMap");
        isOpen = false;

        StopCoroutine("Open");
        StopCoroutine("Close");
        StartCoroutine("Close");

        foreach(SyncDiskElementController element in spawnedDisks)
        {
            element.SelectOptionButton(-1); //Deselect all options
        }

        //Play SFX
        if (playSound) AudioController.Instance.Play2DSound(AudioControls.Instance.mapSlideOut);

        //Disable top bar navigation
        Navigation nav = new Navigation { mode = Navigation.Mode.Explicit };
        nav.selectOnLeft = InterfaceController.Instance.notebookButton.button.FindSelectableOnLeft();
        nav.selectOnRight = InterfaceController.Instance.notebookButton.button.FindSelectableOnRight();
        InterfaceController.Instance.notebookButton.button.navigation = nav;

        Navigation nav2 = new Navigation { mode = Navigation.Mode.Explicit };
        nav2.selectOnLeft = InterfaceController.Instance.upgradesButton.button.FindSelectableOnLeft();
        nav2.selectOnRight = InterfaceController.Instance.upgradesButton.button.FindSelectableOnRight();
        InterfaceController.Instance.upgradesButton.button.navigation = nav2;

        Navigation nav3 = new Navigation { mode = Navigation.Mode.Explicit };
        nav3.selectOnLeft = InterfaceController.Instance.mapButton.button.FindSelectableOnLeft();
        nav3.selectOnRight = InterfaceController.Instance.mapButton.button.FindSelectableOnRight();
        InterfaceController.Instance.mapButton.button.navigation = nav3;

        Navigation nav4 = new Navigation { mode = Navigation.Mode.Explicit };
        nav4.selectOnLeft = CasePanelController.Instance.selectNoCaseButton.button.FindSelectableOnLeft();
        nav4.selectOnRight = CasePanelController.Instance.selectNoCaseButton.button.FindSelectableOnRight();
        CasePanelController.Instance.selectNoCaseButton.button.navigation = nav4;

        Navigation nav5 = new Navigation { mode = Navigation.Mode.Explicit };
        nav5.selectOnLeft = CasePanelController.Instance.stickNoteButton.button.FindSelectableOnLeft();
        nav5.selectOnRight = CasePanelController.Instance.stickNoteButton.button.FindSelectableOnRight();
        CasePanelController.Instance.stickNoteButton.button.navigation = nav5;

        InterfaceController.Instance.upgradesButton.icon.enabled = isOpen;
    }

    IEnumerator Close()
    {
        mainScrollRect.enabled = false;

        while (openProgress > 0f)
        {
            openProgress -= Time.deltaTime * 20f;
            openProgress = Mathf.Clamp01(openProgress);

            InterfaceController.Instance.upgradesCanvasGroup.alpha = openProgress;

            yield return null;
        }

        this.enabled = false;
        InterfaceController.Instance.upgradesCanvas.gameObject.SetActive(false);
    }

    public void UpdateActivation()
    {
        UpgradeEffectController.Instance.appliedEffects.Clear();
        Game.Log("Gameplay: Updating sync disk activation...");

        foreach (Upgrades upgrade in upgrades)
        {
            if(upgrade.state != SyncDiskState.notInstalled)
            {
                UpgradeEffectController.Instance.appliedEffects.AddRange(upgrade.GetAllEffects());
            }
        }

        UpgradeEffectController.Instance.OnSyncDiskChange();
    }

    public void UpdateNavigation()
    {
        for (int i = 0; i < spawnedDisks.Count; i++)
        {
            SyncDiskElementController spawned = spawnedDisks[i];
            if (spawned == null) continue;

        //    Button leftDisk = null;
        //    float closestL = Mathf.Infinity;

        //    Button rightDisk = null;
        //    float closestR = Mathf.Infinity;

        //    Button upDisk = null;
        //    float closestU = Mathf.Infinity;

        //    Button downDisk = null;
        //    float closestD = Mathf.Infinity;

        //    foreach (UpgradesSyncDisk d in spawnedDisks)
        //    {
        //        float dist = Vector3.Distance(d.transform.position, spawned.transform.position);

        //        if (d.transform.position.x < spawned.transform.position.x)
        //        {
        //            if(dist < closestL)
        //            {
        //                leftDisk = d.button;
        //                closestL = dist;
        //            }
        //        }
        //        else if (d.transform.position.x > spawned.transform.position.x)
        //        {
        //            if (dist < closestR)
        //            {
        //                rightDisk = d.button;
        //                closestR = dist;
        //            }
        //        }
        //        else if (d.transform.position.y < spawned.transform.position.y)
        //        {
        //            if (dist < closestD)
        //            {
        //                downDisk = d.button;
        //                closestD = dist;
        //            }
        //        }
        //        else if(d.transform.position.y > spawned.transform.position.y)
        //        {
        //            if (dist < closestU)
        //            {
        //                upDisk = d.button;
        //                closestU = dist;
        //            }
        //        }
        //    }

        //    if(upDisk == null)
        //    {
        //        if(spawned.upgrade.slot >= 0)
        //        {
        //            upDisk = installButton.button;
        //        }
        //    }

        //    Toolbox.Instance.AddNavigationInput(spawned.button, leftDisk, rightDisk, upDisk, downDisk, true);
        //}

        ////Configure top buttons
        //if(spawnedDisks.Count > 0)
        //{
        //    List<UpgradesSyncDisk> av = spawnedDisks.FindAll(item => item.upgrade.slot < 0);

        //    if (av.Count > 0)
        //    {
        //        Toolbox.Instance.AddNavigationInput(availableButton.button, newDown: av[0].button);
        //        Toolbox.Instance.AddNavigationInput(av[0].button, newUp: availableButton.button);

        //        if (av.Count > 1)
        //        {
        //            Toolbox.Instance.AddNavigationInput(sideEffectsButton.button, newDown: av[1].button);
        //            Toolbox.Instance.AddNavigationInput(av[1].button, newUp: sideEffectsButton.button);
        //        }
        //        else
        //        {
        //            Toolbox.Instance.AddNavigationInput(sideEffectsButton.button, newDown: av[0].button);
        //            Toolbox.Instance.AddNavigationInput(av[0].button, newUp: sideEffectsButton.button);
        //        }
        //    }
        //    else
        //    {
        //        Toolbox.Instance.AddNavigationInput(availableButton.button, newDown: spawnedDisks[0].button);
        //        Toolbox.Instance.AddNavigationInput(spawnedDisks[0].button, newUp: availableButton.button);

        //        Toolbox.Instance.AddNavigationInput(sideEffectsButton.button, newDown: spawnedDisks[0].button);
        //        Toolbox.Instance.AddNavigationInput(spawnedDisks[0].button, newUp: sideEffectsButton.button);
        //    }

        //    UpgradesSyncDisk slotted = spawnedDisks.Find(item => item.upgrade.slot >= 0);

        //    if(slotted != null)
        //    {
        //        Toolbox.Instance.AddNavigationInput(installButton.button, newDown: slotted.button);
        //    }
        //    else if(spawnedDisks.Count > 1)
        //    {
        //        Toolbox.Instance.AddNavigationInput(installButton.button, newDown: spawnedDisks[1].button);
        //    }
        //    else
        //    {
        //        Toolbox.Instance.AddNavigationInput(installButton.button, newDown: spawnedDisks[0].button);
        //    }
        }
    }
}
