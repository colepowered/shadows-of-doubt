﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using NaughtyAttributes;
using System;
using System.Linq;
using FMOD.Studio;
using UnityEngine.UI;

public class Human : Actor, IComparable<Human>
{
    [Header("ID")]
    public int humanID = -1;
    [System.NonSerialized]
    public static int assignID = 2;
    [System.NonSerialized]
    public static int assignTraitID = 1;
    [System.NonSerialized]
    public string seed;

    [Header("Current Variables")]
    public ShoeType footwear = ShoeType.normal;
    [System.NonSerialized]
    public AudioEvent footstepEvent;
    public enum ShoeType { normal, boots, heel, barefoot };
    public float footstepDirt = 0f;
    public float footstepBlood = 0f;
    public Transform leftFoot;
    public Transform rightFoot;
    public bool removedFromWorld = false;

    [Header("Human Attributes")]
    public NewAddress home;
    [System.NonSerialized]
    public ResidenceController residence;

    [Header("Movement")]
    [Tooltip("Used for variation of the base values")]
    public float speedMultiplier = 1f; //Used for variation of the base values
    [Tooltip("Calculated walking speed")]
    public float movementWalkSpeed = 1.85f; //Movement speed without time applied
    [Tooltip("Calculated running speed")]
    public float movementRunSpeed = 2f; //Movement speed without time applied
    [System.NonSerialized]
    [Tooltip("The calculated walking speed ratio")]
    public float walkingSpeedRatio = 0.4625f;
    [Space(5)]
    [Tooltip("Speed as a ratio of maximum (0 - 1)")]
    public float currentNormalizedSpeed = 0f; //Speed as a ratio of maximum (0 - 1)
    [Tooltip("The desired movement speed")]
    public float desiredNormalizedSpeed = 0f;
    [Tooltip("The actual movement speed")]
    public float currentMovementSpeed = 1.85f;
    [Tooltip("Recovery rate")]
    public float breathRecoveryRate = 1f;

    public enum MovementSpeed { stopped, walking, running };

    [Header("Job")]
    public Occupation job;
    public Company director;
    [ReadOnly]
    public float societalClass = 0f; //Rank from 0 - 1, 0 being working class, 1 being upper class. Based on job paygrade

    [Header("Personal Data")]
    public Descriptors descriptors;
    public CitizenOutfitController outfitController;
    public HandwritingPreset handwriting;
    [ReadOnly]
    public string birthday;//Format = month//year);

    [Space(7)]
    public string citizenName = string.Empty;
    [System.NonSerialized]
    public string firstName = string.Empty;
    [System.NonSerialized]
    public string casualName = string.Empty;
    [System.NonSerialized]
    public string surName = string.Empty;
    //[System.NonSerialized]
    //public string initialledName = string.Empty; //Moved this to be generated on a get method

    [Space(7)]
    //Gender scale 0: Female, 1: Male
    [System.NonSerialized]
    public float genderScale = 0.5f;
    public enum Gender { male, female, nonBinary };
    public Gender gender = Gender.male;
    public Gender birthGender = Gender.male;

    [Space(7)]
    [Tooltip("How often this person uses the below slang terms")]
    public float slangUsage = 0.5f;
    //[Tooltip("A default slang greeting to be used on anyone in a casual manor")]
    //public string slangGreetingDefault;
    //[Tooltip("Similar to above, but male specific (eg. 'bro')")]
    //public string slangGreetingMale;
    //[Tooltip("Similar to above, but female specific")]
    //public string slangGreetingFemale;
    //[Tooltip("Slang greeting for a lover")]
    //public string slangGreetingLover;
    //[Tooltip("Preferred curse words")]
    //public string slangCurse;
    //[Tooltip("Preferred curse noun word")]
    //public string slangCurseNoun;
    //[Tooltip("Preferred praise noun word")]
    //public string slangPraiseNoun;

    [Space(7)]
    public float sexuality = 0.5f; //How attracted to opposite sex
    public float homosexuality = 0.5f; //Float range from 0-1f to determin attracted to same sex
    [System.NonSerialized]
    public List<Gender> attractedTo = new List<Gender>();

    [Space(7)]
    public Citizen partner;
    public string anniversary; //Anniversary in same format as birthday string
    public Citizen paramour;

    [Space(7)]
    [System.NonSerialized]
    public int fingerprintLoop = -1; //Assigned when fingerprints are discovered
    public enum BloodType { unassigned, Apos, Aneg, Bpos, Bneg, Opos, Oneg, ABpos, ABneg };
    public BloodType bloodType = BloodType.Apos; //The blood type in enum form

    [Space(7)]
    [System.NonSerialized]
    public int favColourIndex = 0;

    [Header("Human Traits & Personality")]
    //Personality
    [ProgressBar("Humility", 1f, EColor.Blue)]
    //Honesty-Humility (H): sincere, honest, faithful, loyal, modest/unassuming versus sly, deceitful, greedy, pretentious, hypocritical, boastful, pompous
    public float humility = 0f;
    [ProgressBar("Emotionality", 1f, EColor.Blue)]
    //Emotionality (E): emotional, oversensitive, sentimental, fearful, anxious, vulnerable versus brave, tough, independent, self-assured, stable
    public float emotionality = 0f;
    [ProgressBar("Extraversion", 1f, EColor.Blue)]
    //Extraversion (X): outgoing, lively, extraverted, sociable, talkative, cheerful, active versus shy, passive, withdrawn, introverted, quiet, reserved
    public float extraversion = 0f;
    [ProgressBar("Agreeableness", 1f, EColor.Blue)]
    //Agreeableness (A): patient, tolerant, peaceful, mild, agreeable, lenient, gentle versus ill-tempered, quarrelsome, stubborn, choleric
    public float agreeableness = 0f;
    [ProgressBar("Conscientiousness", 1f, EColor.Blue)]
    //Conscientiousness (C): organized, disciplined, diligent, careful, thorough, precise versus sloppy, negligent, reckless, lazy, irresponsible, absent-minded
    public float conscientiousness = 0f;
    [ProgressBar("Creativity", 1f, EColor.Blue)]
    //Openness to Experience (O): intellectual, creative, unconventional, innovative, ironic versus shallow, unimaginative, conventional
    public float creativity = 0f;

    //Other stat based variables
    [System.NonSerialized]
    public float sleepNeedMultiplier = 1f; //TODO: Criminals will need more spare time, so need less sleep
    [System.NonSerialized]
    public float snoring = 1f; //Amount this person snores in their sleep (>0.6f uses heavy snore)
    [System.NonSerialized]
    public float snoreDelay = 1.5f; //Time between snores in seconds

    [Space(7)]
    [System.NonSerialized]
    public Vector2 limitHumility = new Vector2(0f, 1f);
    [System.NonSerialized]
    public Vector2 limitEmotionality = new Vector2(0f, 1f);
    [System.NonSerialized]
    public Vector2 limitExtraversion = new Vector2(0f, 1f);
    [System.NonSerialized]
    public Vector2 limitAgreeableness = new Vector2(0f, 1f);
    [System.NonSerialized]
    public Vector2 limitConscientiousness = new Vector2(0f, 1f);
    [System.NonSerialized]
    public Vector2 limitCreativity = new Vector2(0f, 1f);

    [System.Serializable]
    public class Trait
    {
        public string name; //Use this for ease in-editor
        public int traitID = 0;
        public CharacterTrait trait;
        public Trait reason;
        public string date;
    }

    public List<Trait> characterTraits = new List<Trait>();

    [Header("Groups")]
    public List<GroupsController.SocialGroup> groups = new List<GroupsController.SocialGroup>();

    [Header("Status Stats")]
    //[ProgressBar("Nourishment", 1f, EColor.Yellow)]
    public float nourishment = 0f;
    //[ProgressBar("Hydration", 1f, EColor.Yellow)]
    public float hydration = 0f;
    //[ProgressBar("Alertness", 1f, EColor.Yellow)]
    public float alertness = 0f;
    //[ProgressBar("Energy", 1f, EColor.Yellow)]
    public float energy = 0f;
    [ProgressBar("Excitement", 1f, EColor.Yellow)]
    public float excitement = 0f;
    [ProgressBar("Chores", 1f, EColor.Yellow)]
    public float chores = 0f;
    [ProgressBar("Hygiene", 1f, EColor.Yellow)]
    public float hygiene = 0f;
    [ProgressBar("Bladder", 1f, EColor.Yellow)]
    public float bladder = 0f;
    [ProgressBar("Breath", 1f, EColor.Yellow)]
    public float breath = 1f;
    //[ProgressBar("Heat", 1f, EColor.Yellow)]
    public float heat = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float drunk = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float sick = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float headache = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float wet = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float brokenLeg = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float bruised = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float blackEye = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float blackedOut = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float numb = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float poisoned = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float bleeding = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float wellRested = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float starchAddiction = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float syncDiskInstall = 0f;
    //[ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float blinded = 0f;

    public Human poisoner; //The person who poisioned...

    //[System.NonSerialized]
    //private float lastUpdateOfRealtimeStats = 0f;

    [Header("Acquaintances")]
    [System.NonSerialized]
    public List<Acquaintance> acquaintances = new List<Acquaintance>();

    [Header("Vocab")]
    //New DDS system
    public Dictionary<DDSSaveClasses.TriggerPoint, List<DDSSaveClasses.DDSTreeSave>> dds = new Dictionary<DDSSaveClasses.TriggerPoint, List<DDSSaveClasses.DDSTreeSave>>();

    [System.NonSerialized]
    public ConversationInstance currentConversation;
    public float nextCasualSpeechValidAt; //Acts as a conversation general delay

    public Dictionary<Human, Sighting> lastSightings = new Dictionary<Human, Sighting>();
    public int sightingMemoryLimit = 100;

    [System.Serializable]
    public class Sighting
    {
        public float time; //Timestamp
        public Vector3 node; //Node location
        public bool mov = false; //Are they moving?
        public Vector3 dest; //Destination; 2 nodes ahead of current postion
        public bool run = false; //Running
        public int exp; //Expression
        public bool drunk = false; //drunk
        public bool phone = false; //Was phone call
        public bool poi = false; //Person of interest
        public int sound = 0; //Is this a sound? 0 = no, 1 = gunshot, 2 = scream
    }

    [System.Serializable]
    public class ConversationInstance
    {
        [Tooltip("True if active")]
        public bool active = false;

        public NewRoom room; //The room this is in

        [System.NonSerialized]
        public DDSSaveClasses.DDSTreeSave tree; //The current conversation tree

        //Participants of conversation
        public Human participantA;
        public Human participantB;
        public Human participantC;
        public Human participantD;

        [Space(7)]
        public Human previouslyTalking;
        public Human currentlyTalking;

        public Human currentlyTalkingTo;

        public bool speechTriggered = false; //Flag for knowing when to parse new

        //Current point
        [System.NonSerialized]
        public DDSSaveClasses.DDSMessageSettings currentMessage;
        [System.NonSerialized]
        public DDSSaveClasses.DDSMessageLink currentLink;

        public float linkDelay = 0f; //Link delay time in gametime

        [Header("Debug")]
        public float timeUntilNextSpeech = 0;
        public int currentlyTalkingSpeechQueue = 0;
        public string treeName;

        public void EndConversation()
        {
            active = false;

            if (participantA != null)
            {
                participantA.SetInConversation(null);
                if (participantA.ai != null) participantA.ai.faceTransform = null; //Reset facing
            }

            if (participantB != null)
            {
                participantB.SetInConversation(null);
                if (participantB.ai != null) participantB.ai.faceTransform = null; //Reset facing
            }

            if (participantC != null)
            {
                participantC.SetInConversation(null);
                if (participantC.ai != null) participantC.ai.faceTransform = null; //Reset facing
            }

            if (participantD != null)
            {
                participantD.SetInConversation(null);
                if (participantD.ai != null) participantD.ai.faceTransform = null; //Reset facing
            }

            //Remove from room
            room.activeConversations.Remove(this);
        }

        public void SetCurrentMessage(string instanceID)
        {
            if (tree.messageRef.TryGetValue(instanceID, out currentMessage))
            {
                previouslyTalking = currentlyTalking;

                //Set currently talking
                currentlyTalking = participantA;
                if (currentMessage.saidBy == 1) currentlyTalking = participantB;
                else if (currentMessage.saidBy == 2) currentlyTalking = participantC;
                else if (currentMessage.saidBy == 3) currentlyTalking = participantD;

                currentlyTalkingTo = participantA;
                if (currentMessage.saidTo == 1) currentlyTalkingTo = participantB;
                else if (currentMessage.saidTo == 2) currentlyTalkingTo = participantC;
                else if (currentMessage.saidTo == 3) currentlyTalkingTo = participantD;

                speechTriggered = false; //Reset this flag to detect when to parse new
            }
            else
            {
                if (!tree.messages.Exists(item => item.instanceID == instanceID))
                {
                    Game.LogError("...Message " + instanceID + " does not exist within tree " + tree.name + " messages (msg count: " + tree.messages.Count + ")");
                }

                Game.LogError(tree.name + ": Failed to set current conversation message instance ID: " + instanceID);

                EndConversation();
            }
        }
    }

    public class DDSRank
    {
        public string id;
        public DDSSaveClasses.DDSMessageLink linkRef;
        public float rankRef;
    }

    public class SpeechHistory
    {
        public float timeStamp;
        public List<Human> participants = new List<Human>();
    }

    //Speech history can be recorded to stop repeating speech execution
    private Dictionary<DDSSaveClasses.DDSTreeSave, List<SpeechHistory>> speechHistory = new Dictionary<DDSSaveClasses.DDSTreeSave, List<SpeechHistory>>();

    //Message chains
    //Message threads started by this character
    [System.NonSerialized]
    public List<StateSaveData.MessageThreadSave> messageThreadsStarted = new List<StateSaveData.MessageThreadSave>();

    //Message chains featuring this character (including started)
    [System.NonSerialized]
    public List<StateSaveData.MessageThreadSave> messageThreadFeatures = new List<StateSaveData.MessageThreadSave>();

    //Message chains ccing this character (including started)
    [System.NonSerialized]
    public List<StateSaveData.MessageThreadSave> messageThreadCCd = new List<StateSaveData.MessageThreadSave>();

    [Header("Possessions")]
    [System.NonSerialized]
    public Evidence addressBook;
    [System.NonSerialized]
    public bool setupAddressBook = false;
    //[System.NonSerialized]
    //public EvidenceItem businessCard;
    [System.NonSerialized]
    public Evidence workID;
    //[System.NonSerialized]
    //public Evidence idCard;
    //[System.NonSerialized]
    //public EvidenceItem namePlacard;
    [System.NonSerialized]
    public List<Interactable> birthdayCards = new List<Interactable>();
    public List<InteractablePreset> currentConsumables = new List<InteractablePreset>();

    public List<int> trash = new List<int>();
    public enum DisposalType { anywhere, homeOnly, workOnly, homeOrWork };
    public int anywhereTrash = 0;

    public Death death;

    public List<Wound> currentWounds = new List<Wound>();

    [System.Serializable]
    public class Wound
    {
        public int humanID = -1;
        public Interactable interactable;
        public CitizenOutfitController.CharacterAnchor anchor;
        public float timestamp;

        [System.NonSerialized]
        public Human human;

        public void Load()
        {
            if(CityData.Instance.GetHuman(humanID, out human))
            {
                Transform closestBodyPart = human.outfitController.GetBodyAnchor(anchor);
                interactable.parentTransform = closestBodyPart;

                //Load interactable
                interactable.MainSetupStart();
                interactable.OnLoad();
                interactable.MarkAsTrash(true);

                interactable.objectRef = this;
            }
        }
    }

    [System.Serializable]
    public class Death
    {
        [Header("Death")]
        public bool isDead; //Use this to check intialization
        public Vector3 location; //Node reference
        public float time; //Time of death
        public Vector2 timeOfDeathRange;
        public int weapon;
        public int murder = -1;
        public int victim;
        public int killer;
        public int discoveredBy;
        public float discoveredAt;
        public bool reported = false;
        public ReportType reportType;
        public float smell = 0f;

        public enum ReportType { visual, smell, audio };

        //Initialize a new death class
        public Death(Human newVictim, MurderController.Murder newMurder, Human newKiller, Interactable newWeapon)
        {
            isDead = true;
            if(newMurder != null) murder = newMurder.murderID;
            newMurder.death = this;
            victim = newVictim.humanID;
            location = newVictim.currentNodeCoord;
            time = SessionData.Instance.gameTime;
            newMurder.time = time;
            timeOfDeathRange = Toolbox.Instance.CreateTimeRange(time, GameplayControls.Instance.timeOfDeathAccuracy, false, true, 15);
            if (newWeapon != null) weapon = newWeapon.id;

            killer = newKiller.humanID;

            if (newVictim.currentRoom != null) newVictim.currentRoom.containsDead = true;

            newVictim.death = this;
            CityData.Instance.deadCitizensDirectory.Add(newVictim);
        }

        public void SetReported(Human newFoundBy, ReportType newReportType)
        {
            if (!reported)
            {
                reported = true;
                discoveredAt = SessionData.Instance.gameTime;
                discoveredBy = newFoundBy.humanID;
                reportType = newReportType;

                //Add mourn
                Human h = null;

                if (CityData.Instance.GetHuman(victim, out h))
                {
                    if (h.ai != null)
                    {
                        foreach (Acquaintance aq in h.acquaintances)
                        {
                            if (aq.with.humanID == killer) continue;
                            if (aq.with.ai == null) continue;

                            if (aq.known > SocialControls.Instance.knowMournThreshold || aq.connections[0] == Acquaintance.ConnectionType.lover || aq.connections[0] == Acquaintance.ConnectionType.paramour || aq.connections[0] == Acquaintance.ConnectionType.housemate)
                            {
                                NewAIGoal mournGoal = aq.with.ai.goals.Find(item => item.preset == RoutineControls.Instance.mourn);

                                if (mournGoal == null)
                                {
                                    Game.Log("Create mourn goal for " + aq.with.GetCitizenName());
                                    aq.with.ai.CreateNewGoal(RoutineControls.Instance.mourn, 0f, 0f);
                                }
                                //Otherwise reprioritize mourn
                                else
                                {
                                    mournGoal.activeTime = 0f;
                                }
                            }
                        }
                    }
                }


            }
        }

        public Human GetVictim()
        {
            Human ret = null;
            CityData.Instance.GetHuman(victim, out ret);
            return ret;
        }

        public Human GetKiller()
        {
            Human ret = null;
            CityData.Instance.GetHuman(killer, out ret);
            return ret;
        }

        public Human GetDiscoverer()
        {
            Human ret = null;
            CityData.Instance.GetHuman(discoveredBy, out ret);
            return ret;
        }

        public EvidenceTime GetTimeOfDeathEvidence()
        {
            timeOfDeathRange.y = Mathf.Min(timeOfDeathRange.y, SessionData.Instance.gameTime); //Make sure range doesn't extend past now
            return EvidenceCreator.Instance.GetTimeEvidence(timeOfDeathRange.x, timeOfDeathRange.y, "TimeOfDeath", writer: victim);
        }

        public NewGameLocation GetDeathLocation()
        {
            NewNode n = null;

            if (PathFinder.Instance.nodeMap.TryGetValue(location, out n))
            {
                return n.gameLocation;
            }
            else
            {
                Game.LogError("Cannot get death location from node coord " + location);
                return null;
            }
        }

        public MurderController.Murder GetMurder()
        {
            MurderController.Murder ret = MurderController.Instance.activeMurders.Find(item => item.murderID == murder);
            if(ret == null) ret = MurderController.Instance.inactiveMurders.Find(item => item.murderID == murder);

            return ret;
        }
    }

    [System.Serializable]
    public class WalletItem
    {
        public WalletItemType itemType = WalletItemType.nothing;
        public int meta = -1;
        public int money = 0;
    }

    public List<WalletItem> walletItems = new List<WalletItem>();

    public enum WalletItemType { nothing, money, evidence, key };

    //Detail dictionary
    public Dictionary<string, Fact> factDictionary = new Dictionary<string, Fact>();

    [Header("Personal Affects")]
    public List<InteractablePreset> personalAffects = new List<InteractablePreset>();
    public List<InteractablePreset> workAffects = new List<InteractablePreset>();
    //[System.NonSerialized]
    //public FurnitureLocation ownedJobFurniture;
    [System.NonSerialized]
    public Interactable workPosition;
    //[System.NonSerialized]
    //public FurnitureLocation bed;
    [System.NonSerialized]
    public Interactable sleepPosition;

    private int preferredBookCount = 3;

    public struct BookChoice
    {
        public BookPreset p;
        public float rank;
    }

    public List<BookPreset> library = new List<BookPreset>(); //Full pook of owned books
    public List<BookPreset> nonShelfBooks = new List<BookPreset>(); //Pool of books that can be placed away from a shelf
    [System.NonSerialized]
    public int booksAwayFromShelf = 0; //Books to place away from shelf

    public enum NoteObject { note, letter, travelReceipt, vmailLetter };

    //Routine preferences
    public Dictionary<RetailItemPreset, int> itemRanking = new Dictionary<RetailItemPreset, int>(); //Give a favourite ranking to every retail item; pick near the top to choose what to buy in favourite place
    public Dictionary<CompanyPreset.CompanyCategory, NewAddress> favouritePlaces = new Dictionary<CompanyPreset.CompanyCategory, NewAddress>(); //Dictionary of fav places close to work
    public Dictionary<RetailItemPreset, float> recentPurchases = new Dictionary<RetailItemPreset, float>(); //Record recent purchases to weight against recent purchases

    [Header("Passwords")]
    public GameplayController.Passcode passcode;
    public CharacterTrait passwordTrait;

    [Header("Simulated Behaviour")]
    private List<float> simulatedPreviousBehaviour = new List<float>(); //Used for simulating behaviour on game load

    [Header("Misc")]
    public Vector2 lastUsedCCTVScreenPoint;
    public bool updateMeshList = false; //Update this when active (limits multiple updating)

    [Header("Debug")]
    public ConversationInstance debugConversation;

    //Events

    //Give this citizen a job!
    public void SetJob(Occupation newJob)
    {
        if (newJob == null) Game.Log("CityGen: Trying to give null job to " + humanID);

        //Remove from previous
        if (job != null)
        {
            job.employee = null;

            //Is director?
            if (job.isOwner)
            {
                job.employer.director = null;
                director = null;
                job.employer.address.RemoveOwner(this);
            }

            if (job.preset.receptionist)
            {
                job.employer.receptionist = this;
            }

            if (job.preset.janitor)
            {
                job.employer.janitor = this;
            }

            if (job.preset.security)
            {
                job.employer.security = this;
            }

            CityData.Instance.assignedJobsDirectory.Remove(job);

            if (job.employer != null && job.employer.address != home)
            {
                RemoveFromKeyring(job.employer.address);
                job.employer.address.RemoveInhabitant(this);
            }

            if (job.preset.work == OccupationPreset.workType.Enforcer)
            {
                GameplayController.Instance.enforcers.Remove(this);
                isEnforcer = false;
            }

            //Add the routine
            NewAIGoal jobGoal = ai.goals.Find(item => item.preset == RoutineControls.Instance.workGoal);
            if (jobGoal != null) jobGoal.Remove();

            //Add business card to personal and work affects
            RemovePersonalAffect(InteriorControls.Instance.businessCard, false);
            RemovePersonalAffect(InteriorControls.Instance.businessCard, true);
            RemovePersonalAffect(InteriorControls.Instance.namePlacard, true);
            //RemovePersonalAffect(InteriorControls.Instance.workID, false);
            //RemovePersonalAffect(InteriorControls.Instance.workID, true);

            workAffects.Clear();

            job = null;
        }

        job = newJob;
        job.employee = this;

        //Societal class is the same as paygrade + a little random
        societalClass = Mathf.Clamp01(job.paygrade + Toolbox.Instance.GetPsuedoRandomNumber(-0.1f, 0.1f, humanID.ToString()));

        //Is director?
        if (job.isOwner && job.employer != null)
        {
            SetAsDirector(job.employer);
        }

        if (job.preset.work == OccupationPreset.workType.Enforcer)
        {
            if (!GameplayController.Instance.enforcers.Contains(this))
            {
                //Game.Log("Adding enforcer " + humanID);
                GameplayController.Instance.enforcers.Add(this);
                isEnforcer = true;
            }
        }

        CityData.Instance.assignedJobsDirectory.Add(job);

        //Add business card to personal and work affects
        if (!SessionData.Instance.isFloorEdit && CityConstructor.Instance.generateNew)
        {
            if (job.preset.businessCards)
            {
                AddPersonalAffect(InteriorControls.Instance.businessCard, false);
                AddPersonalAffect(InteriorControls.Instance.businessCard, true);
            }

            if (job.preset.namePlacard)
            {
                AddPersonalAffect(InteriorControls.Instance.namePlacard, true);
            }

            if (job.preset.employeePhoto)
            {
                AddPersonalAffect(InteriorControls.Instance.employeePhoto, true);
            }

            if (job.preset.workRota)
            {
                AddPersonalAffect(InteriorControls.Instance.workRota, false);
            }

            if (job.preset.employmentContract)
            {
                AddPersonalAffect(InteriorControls.Instance.employmentContractHome, false);
                AddPersonalAffect(InteriorControls.Instance.employmentContractWork, true);
            }

            foreach (InteractablePreset p in job.preset.jobItems)
            {
                AddPersonalAffect(p, true);
            }
        }

        //Add as inhabitant of company
        if (job.employer != null && job.employer.address != null)
        {
            //Give keys
            AddToKeyring(job.employer.address);

            job.employer.address.AddInhabitant(this);

            //Add the routine
            if (!ai.goals.Exists(item => item.preset == RoutineControls.Instance.workGoal))
            {
                NewAIGoal jobGoal = ai.CreateNewGoal(RoutineControls.Instance.workGoal, 0f, 0f);
            }
        }
    }

    //Give this citizen a sexuality & gender!
    public void SetSexualityAndGender()
    {
        //Set ID
        if (SessionData.Instance.isFloorEdit || CityConstructor.Instance.generateNew)
        {
            humanID = assignID;
            assignID++;

            seed = Toolbox.Instance.SeedRand(0, 999999999).ToString();
        }

        //Determin sexuality/gender here as it will effect couples being created...
        //Assign sex as float 0.49 - 0.51 is enough for the person to belong to a gender 0(female) - 1(male).
        genderScale = Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, seed, out seed) * 0.01f;

        //Use bools for quick references generic gender.
        if (genderScale < 0.5f - SocialStatistics.Instance.genderNonBinaryThreshold)
        {
            gender = Gender.female;
            AddCharacterTrait(SocialStatistics.Instance.femaleTrait);
        }
        else if (genderScale > 0.5f + SocialStatistics.Instance.genderNonBinaryThreshold)
        {
            gender = Gender.male;
            AddCharacterTrait(SocialStatistics.Instance.maleTrait);
        }
        else
        {
            gender = Gender.nonBinary;
            AddCharacterTrait(SocialStatistics.Instance.nbTrait);
        }

        //Game.Log("Set gender for " + name + " " + humanID + ": " + gender);

        //Assign sexuality as a float 0(straight) - 1(gay). 0.95 is the threshold for identifying as gay.
        sexuality = Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, seed, out seed) * 0.01f;
        homosexuality = Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, seed, out seed) * 0.01f;

        if (sexuality >= SocialStatistics.Instance.sexualityStraightThreshold)
        {
            if (gender == Gender.male)
            {
                attractedTo.Add(Gender.female);
                AddCharacterTrait(SocialStatistics.Instance.AttractedToFemaleTrait);
            }
            else if (gender == Gender.female)
            {
                attractedTo.Add(Gender.male);
                AddCharacterTrait(SocialStatistics.Instance.AttractedToMaleTrait);
            }
        }

        if (homosexuality >= SocialStatistics.Instance.sexualityStraightThreshold)
        {
            if (gender == Gender.male)
            {
                attractedTo.Add(Gender.male);
                AddCharacterTrait(SocialStatistics.Instance.AttractedToMaleTrait);
            }
            else if (gender == Gender.female)
            {
                attractedTo.Add(Gender.female);
                AddCharacterTrait(SocialStatistics.Instance.AttractedToFemaleTrait);
            }
        }

        //If both or neither, chance to be attracted to non-binary
        if (attractedTo.Count >= 2)
        {
            attractedTo.Add(Gender.nonBinary);
            AddCharacterTrait(SocialStatistics.Instance.AttractedToNBTrait);
        }
        else if (attractedTo.Count <= 0 && Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) > SocialStatistics.Instance.asexualChance)
        {
            attractedTo.Add(Gender.nonBinary);
            AddCharacterTrait(SocialStatistics.Instance.AttractedToNBTrait);
        }

        SetBirthGender();
    }

    private void SetBirthGender()
    {
        birthGender = gender;

        //If non binary, choose a random birth gender. Also small chance they are trans...
        if (birthGender == Gender.nonBinary)
        {
            if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) > 0.5f)
            {
                birthGender = Gender.female;
            }
            else
            {
                birthGender = Gender.male;
            }
        }
        else if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) <= SocialStatistics.Instance.transThreshold)
        {
            if (birthGender == Gender.male) birthGender = Gender.female;
            else if (birthGender == Gender.female) birthGender = Gender.male;
            else
            {
                if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) > 0.5f)
                {
                    birthGender = Gender.female;
                }
                else
                {
                    birthGender = Gender.male;
                }
            }
        }
    }

    //Give this citizen a sexuality and gender based on partnering to this person
    public void GenerateSuitableGenderAndSexualityForParnter(Citizen newPartner)
    {
        //Set ID
        if (SessionData.Instance.isFloorEdit || CityConstructor.Instance.generateNew)
        {
            humanID = assignID;
            assignID++;

            seed = Toolbox.Instance.SeedRand(0, 999999999).ToString();
        }

        //Gender must be one that partner is attracted to...
        List<Gender> possibleGenders = new List<Gender>(newPartner.attractedTo);

        if (possibleGenders.Count <= 0)
        {
            possibleGenders.Add(Gender.female);
            possibleGenders.Add(Gender.male);
            possibleGenders.Add(Gender.nonBinary);
        }

        gender = possibleGenders[Toolbox.Instance.GetPsuedoRandomNumberContained(0, possibleGenders.Count, seed, out seed)];

        if (gender == Gender.male)
        {
            genderScale = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 0.5f + SocialStatistics.Instance.genderNonBinaryThreshold, seed, out seed);
        }
        else if (gender == Gender.female)
        {
            genderScale = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 0.5f - SocialStatistics.Instance.genderNonBinaryThreshold, seed, out seed);
        }
        else
        {
            genderScale = Toolbox.Instance.GetPsuedoRandomNumberContained(0.5f - SocialStatistics.Instance.genderNonBinaryThreshold, 0.5f + SocialStatistics.Instance.genderNonBinaryThreshold, seed, out seed);
        }

        genderScale = Mathf.RoundToInt(genderScale * 100f) / 100f;

        //Sexuality must also be compatible with partner
        sexuality = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed);
        homosexuality = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed);

        //Sexuality must be within the matching 15%
        if (newPartner.sexuality >= SocialStatistics.Instance.sexualityStraightThreshold)
        {
            sexuality = Toolbox.Instance.GetPsuedoRandomNumberContained(SocialStatistics.Instance.sexualityStraightThreshold, 1f, seed, out seed);
        }

        if (newPartner.sexuality >= SocialStatistics.Instance.sexualityGayThreshold)
        {
            homosexuality = Toolbox.Instance.GetPsuedoRandomNumberContained(SocialStatistics.Instance.sexualityGayThreshold, 1f, seed, out seed);
        }

        sexuality = Mathf.RoundToInt(sexuality * 100f) / 100f;
        homosexuality = Mathf.RoundToInt(homosexuality * 100f) / 100f;

        if (sexuality >= SocialStatistics.Instance.sexualityStraightThreshold)
        {
            if (gender == Gender.male)
            {
                attractedTo.Add(Gender.female);
            }
            else if (gender == Gender.female)
            {
                attractedTo.Add(Gender.male);
            }
        }

        if (homosexuality >= SocialStatistics.Instance.sexualityStraightThreshold)
        {
            if (gender == Gender.male)
            {
                attractedTo.Add(Gender.male);
            }
            else if (gender == Gender.female)
            {
                attractedTo.Add(Gender.female);
            }
        }

        //If both or neither, chance to be attracted to non-binary
        if (attractedTo.Count >= 2)
        {
            attractedTo.Add(Gender.nonBinary);
        }
        else if (attractedTo.Count <= 0 && Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) > SocialStatistics.Instance.asexualChance)
        {
            attractedTo.Add(Gender.nonBinary);
        }

        //Use bools for quick references generic gender.
        if (genderScale < 0.5f - SocialStatistics.Instance.genderNonBinaryThreshold)
        {
            gender = Gender.female;
            AddCharacterTrait(SocialStatistics.Instance.femaleTrait);
        }
        else if (genderScale > 0.5f + SocialStatistics.Instance.genderNonBinaryThreshold)
        {
            gender = Gender.male;
            AddCharacterTrait(SocialStatistics.Instance.maleTrait);
        }
        else
        {
            gender = Gender.nonBinary;
            AddCharacterTrait(SocialStatistics.Instance.nbTrait);
        }

        //Game.Log("Set gender for " + name + " " + humanID + ": " + gender);

        SetBirthGender();
    }

    //Give this citizen a personality!
    public void SetPersonality()
    {
        preferredBookCount = SocialControls.Instance.basePreferredBookCount;
        sightingMemoryLimit = CitizenControls.Instance.defaultMemoryLimit;

        //Set personality HEXACO
        //Honesty-Humility (H): sincere, honest, faithful, loyal, modest/unassuming versus sly, deceitful, greedy, pretentious, hypocritical, boastful, pompous
        humility = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed);
        //Emotionality (E): emotional, oversensitive, sentimental, fearful, anxious, vulnerable versus brave, tough, independent, self-assured, stable
        emotionality = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed);
        //Extraversion (X): outgoing, lively, extraverted, sociable, talkative, cheerful, active versus shy, passive, withdrawn, introverted, quiet, reserved
        extraversion = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed);
        //Agreeableness (A): patient, tolerant, peaceful, mild, agreeable, lenient, gentle versus ill-tempered, quarrelsome, stubborn, choleric
        agreeableness = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed);
        //Conscientiousness (C): organized, disciplined, diligent, careful, thorough, precise versus sloppy, negligent, reckless, lazy, irresponsible, absent-minded
        conscientiousness = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed);
        //Openness to Experience (O): intellectual, creative, unconventional, innovative, ironic versus shallow, unimaginative, conventional
        creativity = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed);

        //Scew personality towards job fit...
        if (job != null)
        {
            if (job.preset != null && job.preset.skewPersonalityTowardsJobFit > 0f)
            {
                if (job.preset.skewHumility) humility = Mathf.Lerp(humility, job.preset.humility, job.preset.skewPersonalityTowardsJobFit);
                if (job.preset.skewEmotionality) emotionality = Mathf.Lerp(emotionality, job.preset.emotionality, job.preset.skewPersonalityTowardsJobFit);
                if (job.preset.skewExtraversion) extraversion = Mathf.Lerp(extraversion, job.preset.extraversion, job.preset.skewPersonalityTowardsJobFit);
                if (job.preset.skewAgreeableness) agreeableness = Mathf.Lerp(agreeableness, job.preset.agreeableness, job.preset.skewPersonalityTowardsJobFit);
                if (job.preset.skewConscientiousness) conscientiousness = Mathf.Lerp(conscientiousness, job.preset.conscientiousness, job.preset.skewPersonalityTowardsJobFit);
                if (job.preset.skewHumility) creativity = Mathf.Lerp(conscientiousness, job.preset.conscientiousness, job.preset.skewPersonalityTowardsJobFit);
            }
        }

        //Continued...
        snoring = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed);
        snoreDelay = 1.5f + Mathf.Clamp01(descriptors.heightCM - 122f / 91f) * 1.5f;

        //Pick character traits...
        //Do this in 4 stages...
        for (int i = 0; i < 4; i++)
        {
            List<CharacterTrait> traitList = null;

            if (i == 0)
            {
                traitList = Toolbox.Instance.stage0Traits;
            }
            else if (i == 1)
            {
                traitList = Toolbox.Instance.stage1Traits;
            }
            else if (i == 2)
            {
                traitList = Toolbox.Instance.stage2Traits;
            }
            else
            {
                traitList = Toolbox.Instance.stage3Traits;
            }

            //Randomize list
            if(traitList != null && traitList.Count > 0) Toolbox.Instance.ShuffleListSeedContained(ref traitList, seed, out seed);

            foreach (CharacterTrait trait in traitList)
            {
                if (characterTraits.Exists(item => item.trait == trait)) continue; //Skip duplicate traits
                if (trait.requiresHome && home == null) continue; //Requires home
                if (trait.requiresPartner && partner == null) continue; //Requires partner
                if (trait.requiresSingle && partner != null) continue; //Requires single
                if (trait.requiresEmployment && (job == null || job.employer == null || job.preset.selfEmployed || job.employer.address == null)) continue; //Requires job
                if (trait.disabled) continue;

                if (trait.isPassword && passwordTrait != null) continue;

                //Calculate base chance...
                float baseChance = GetTraitChance(trait);

                if (baseChance <= 0f) continue;

                if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) <= baseChance)
                {
                    AddCharacterTrait(trait);
                }
            }
        }

        //Build possessions...
        preferredBookCount = Mathf.Max(0, preferredBookCount);
        List<BookChoice> randomRejected = new List<BookChoice>();

        sightingMemoryLimit = Mathf.Max(1, sightingMemoryLimit);

        //Shuffle book list
        for (int i = 0; i < Toolbox.Instance.allBooks.Count; i++)
        {
            BookPreset temp = Toolbox.Instance.allBooks[i];
            int randomIndex = Toolbox.Instance.GetPsuedoRandomNumberContained(i, Toolbox.Instance.allBooks.Count, seed, out seed);
            Toolbox.Instance.allBooks[i] = Toolbox.Instance.allBooks[randomIndex];
            Toolbox.Instance.allBooks[randomIndex] = temp;
        }

        //Add likely books first...
        foreach (BookPreset b in Toolbox.Instance.allBooks)
        {
            if (library.Contains(b)) continue; //Skip duplicate books

            //If parnter has book then skip
            if (b.spawnRule != BookPreset.SpawnRules.onlyAtWork && b.spawnRule != BookPreset.SpawnRules.secret)
            {
                if (partner != null)
                {
                    if (partner.library.Contains(b)) continue;
                }
            }

            //Calculate base chance...
            float chance = GetChance(ref b.pickRules, b.baseChance);

            if (chance <= 0f) continue; //If no chance then quit here...

            //Factor in common books here...
            chance *= b.common;

            if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) <= (chance))
            {
                library.Add(b);
            }
            else
            {
                randomRejected.Add(new BookChoice { p = b, rank = chance });
            }
        }

        //Sort rejected by chance
        randomRejected.Sort((p1, p2) => p2.rank.CompareTo(p1.rank)); //Highest chance first

        //Continue to fill library until we run out of books or 
        while (randomRejected.Count > 0 && library.Count < preferredBookCount)
        {
            library.Add(randomRejected[0].p);
            randomRejected.RemoveAt(0);
        }

        //We now have a library of books we should spawn. How many should be on non-shelves? Certain traits will dictate this
        booksAwayFromShelf = Mathf.RoundToInt(library.Count * ((1f - conscientiousness) * 0.25f)); //As many as half of books won't be on shelves...
        booksAwayFromShelf = Mathf.Max(1, booksAwayFromShelf);

        //Create list (must contain a blurb to spawn not on bookcase)
        nonShelfBooks.Clear();

        foreach(BookPreset bp in library)
        {
            DDSSaveClasses.DDSMessageSave msg = null;

            if(Toolbox.Instance.allDDSMessages.TryGetValue(bp.ddsMessage, out msg))
            {
                if(msg.blocks.Exists(item => item.group == 2))
                {
                    nonShelfBooks.Add(bp);
                }
            }
        }

        booksAwayFromShelf = Mathf.Min(booksAwayFromShelf, nonShelfBooks.Count);

        //Select handwriting
        List<HandwritingPreset> handwritingPool = new List<HandwritingPreset>();

        foreach(HandwritingPreset cp in Toolbox.Instance.allHandwriting)
        {
            float traitScore = GetChance(ref cp.characterTraits, cp.baseChance);

            int totalScore = Mathf.CeilToInt(traitScore * 10);

            for (int u = 0; u < totalScore; u++)
            {
                handwritingPool.Add(cp);
            }
        }

        if (handwritingPool.Count > 0)
        {
            handwriting = handwritingPool[Toolbox.Instance.GetPsuedoRandomNumberContained(0, handwritingPool.Count, seed, out seed)];
        }
        else handwriting = Toolbox.Instance.allHandwriting[Toolbox.Instance.GetPsuedoRandomNumberContained(0, Toolbox.Instance.allHandwriting.Count, seed, out seed)];
    }

    //Get the chance of picking this trait
    private float GetTraitChance(CharacterTrait trait)
    {
        //Calculate base chance...
        float baseChance = trait.primeBaseChance;

        //Check picking rules
        bool passRules = true;

        foreach (CharacterTrait.TraitPickRule rule in trait.pickRules)
        {
            bool pass = false;

            if (rule.rule == CharacterTrait.RuleType.ifAnyOfThese)
            {
                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (TraitExists(searchTrait))
                    {
                        pass = true;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifAllOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (!TraitExists(searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifNoneOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (TraitExists(searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifPartnerAnyOfThese)
            {
                if (partner != null)
                {
                    foreach (CharacterTrait searchTrait in rule.traitList)
                    {
                        if (partner.TraitExists(searchTrait))
                        {
                            pass = true;
                            break;
                        }
                    }
                }
                else pass = false;
            }

            if (pass)
            {
                baseChance += rule.baseChance;
            }
            else if (rule.mustPassForApplication)
            {
                passRules = false;
            }
        }

        if (!passRules) return 0f;

        //Match to existing personality...
        int matchDivider = 0;
        float add = 0f;

        if (trait.useHumilityMatch)
        {
            add += (1f - Mathf.Abs(humility - trait.matchHumility));
            matchDivider++;
        }

        if (trait.useExtraversionMatch)
        {
            add += (1f - Mathf.Abs(extraversion - trait.matchExtraversion));
            matchDivider++;
        }

        if (trait.useEmotionalityMatch)
        {
            add += (1f - Mathf.Abs(emotionality - trait.matchEmotionality));
            matchDivider++;
        }

        if (trait.useAgreeablenessMatch)
        {
            add += (1f - Mathf.Abs(agreeableness - trait.matchAgreeableness));
            matchDivider++;
        }

        if (trait.useConscientiousnessMatch)
        {
            add += (1f - Mathf.Abs(conscientiousness - trait.matchConscientiousness));
            matchDivider++;
        }

        if (trait.useCreativityMatch)
        {
            add += (1f - Mathf.Abs(creativity - trait.matchCreativity));
            matchDivider++;
        }

        if (trait.useSocietalClassMatch)
        {
            add += (1f - Mathf.Abs(societalClass - trait.matchSocietalClass));
            matchDivider++;
        }

        if (add != 0f && matchDivider != 0f) add /= (float)matchDivider;

        if (!float.IsNaN(add))
        {
            baseChance += add * trait.matchChance;
        }

        return Mathf.Clamp01(baseChance);
    }

    //Does this trait exist in this human's trait list?
    public bool TraitExists(CharacterTrait searchTrait)
    {
        if (searchTrait is null) return false;

        if (characterTraits.Exists(item => item != null && item.trait != null && item.trait == searchTrait))
        {
            return true;
        }
        else return false;
    }

    //Get the chance of picking this book
    public float GetChance(ref List<CharacterTrait.TraitPickRule> pickRules, float baseChance)
    {
        //Check picking rules
        bool passRules = true;

        foreach (CharacterTrait.TraitPickRule rule in pickRules)
        {
            bool pass = false;

            if (rule.rule == CharacterTrait.RuleType.ifAnyOfThese)
            {
                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = true;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifAllOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (!characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifNoneOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifPartnerAnyOfThese)
            {
                if (partner != null)
                {
                    foreach (CharacterTrait searchTrait in rule.traitList)
                    {
                        if (partner.characterTraits.Exists(item => item.trait == searchTrait))
                        {
                            pass = true;
                            break;
                        }
                    }
                }
            }

            if (pass)
            {
                baseChance += rule.baseChance;
            }
            else if (rule.mustPassForApplication)
            {
                passRules = false;
            }
        }

        if (!passRules) return 0f;

        return Mathf.Clamp01(baseChance);
    }

    //Add a character trait and apply it's automatic effects
    public Trait AddCharacterTrait(CharacterTrait newTrait)
    {
        if (newTrait == null) return null;
        //Game.Log("Add trait for for " + name + " " + humanID + ": " + newTrait.name);

        Trait newT = new Trait();
        newT.name = newTrait.name;
        newT.traitID = assignTraitID;
        assignTraitID++;

        newT.trait = newTrait;

        humility += newTrait.effectHumility;
        emotionality += newTrait.effectEmotionality;
        extraversion += newTrait.effectExtraversion;
        agreeableness += newTrait.effectAgreeableness;
        conscientiousness += newTrait.effectConscientiousness;
        creativity += newTrait.effectCreativity;

        SetMaxHealth(maximumHealth + newTrait.maxHealthModifier);
        SetRecoveryRate(recoveryRate + newTrait.recoveryRateModifier);
        SetCombatSkill(combatSkill + newTrait.combatSkillModifier);
        SetCombatHeft(combatHeft + newTrait.combatHeftModifier);
        SetMaxNerve(maxNerve + newTrait.maxNerveModifier);

        breathRecoveryRate += newTrait.breathRecoveryModifier;

        slangUsage += newTrait.slangUsageModifier;

        preferredBookCount += newTrait.preferredBookCountModifier;
        sightingMemoryLimit += newTrait.sightingLimitMemoryModifier;

        if (newTrait.isPassword)
        {
            passwordTrait = newTrait;
        }

        //Choose date
        if (newTrait.needsDate)
        {
            if (newTrait.useCouplesAnniversary)
            {
                newT.date = anniversary;
            }
            else
            {
                int whenAge = Toolbox.Instance.GetPsuedoRandomNumberContained((int)newTrait.ageDateRange.x, (int)newTrait.ageDateRange.y, seed, out seed);
                whenAge = Mathf.Min(whenAge, GetAge() - 1);

                //Pick a month
                int annMonth = Toolbox.Instance.GetPsuedoRandomNumberContained(0, 12, seed, out seed) + 1;
                int annDate = Toolbox.Instance.GetPsuedoRandomNumberContained(1, SessionData.Instance.daysInMonths[annMonth - 1] + 1, seed, out seed);

                //Now calculate the year based on age.
                int minYear = SessionData.Instance.yearInt - GetAge() + whenAge + SessionData.Instance.publicYear;

                int annYear = Toolbox.Instance.GetPsuedoRandomNumberContained(minYear, SessionData.Instance.publicYear, seed, out seed);

                //Get string
                newT.date = annMonth + "/" + annDate + "/" + annYear;
            }
        }

        //Apply limits
        limitHumility = new Vector2(Mathf.Max(limitHumility.x, newTrait.limitHumility.x), Mathf.Min(limitHumility.y, newTrait.limitHumility.y));
        limitEmotionality = new Vector2(Mathf.Max(limitEmotionality.x, newTrait.limitEmotionality.x), Mathf.Min(limitEmotionality.y, newTrait.limitEmotionality.y));
        limitExtraversion = new Vector2(Mathf.Max(limitExtraversion.x, newTrait.limitExtraversion.x), Mathf.Min(limitExtraversion.y, newTrait.limitExtraversion.y));
        limitAgreeableness = new Vector2(Mathf.Max(limitAgreeableness.x, newTrait.limitAgreeableness.x), Mathf.Min(limitAgreeableness.y, newTrait.limitAgreeableness.y));
        limitConscientiousness = new Vector2(Mathf.Max(limitConscientiousness.x, newTrait.limitConscientiousness.x), Mathf.Min(limitConscientiousness.y, newTrait.limitConscientiousness.y));
        limitCreativity = new Vector2(Mathf.Max(limitCreativity.x, newTrait.limitCreativity.x), Mathf.Min(limitCreativity.y, newTrait.limitCreativity.y));

        humility = Mathf.Clamp(humility, limitHumility.x, limitHumility.y);
        emotionality = Mathf.Clamp(emotionality, limitEmotionality.x, limitEmotionality.y);
        extraversion = Mathf.Clamp(extraversion, limitExtraversion.x, limitExtraversion.y);
        agreeableness = Mathf.Clamp(agreeableness, limitAgreeableness.x, limitAgreeableness.y);
        conscientiousness = Mathf.Clamp(conscientiousness, limitConscientiousness.x, limitConscientiousness.y);
        creativity = Mathf.Clamp(creativity, limitCreativity.x, limitCreativity.y);

        if (newTrait == CitizenControls.Instance.litterBug)
        {
            isLitterBug = true;
        }

        if (newTrait == CitizenControls.Instance.likesTheRain)
        {
            likesTheRain = true;
        }

        //Make sure to add before searching for a reason...
        characterTraits.Add(newT);

        if (newTrait.needsReson)
        {
            List<CharacterTrait> reasonPool = new List<CharacterTrait>();

            foreach (CharacterTrait reason in Toolbox.Instance.reasons)
            {
                if (reason.requiresHome && home == null) continue; //Requires home
                if (reason.requiresPartner && partner == null) continue; //Requires partner
                if (reason.requiresSingle && partner != null) continue; //Requires single
                if (reason.requiresEmployment && (job == null || job.employer == null || job.preset.selfEmployed || job.employer.address == null)) continue; //Requires job
                if (reason.disabled) continue;

                //Check picking rules
                foreach (CharacterTrait.TraitPickRule rule in reason.pickRules)
                {
                    bool pass = false;

                    if (rule.rule == CharacterTrait.RuleType.ifAnyOfThese)
                    {
                        foreach (CharacterTrait searchTrait in rule.traitList)
                        {
                            if (characterTraits.Exists(item => item.trait == searchTrait))
                            {
                                pass = true;
                                break;
                            }
                        }
                    }
                    else if (rule.rule == CharacterTrait.RuleType.ifAllOfThese)
                    {
                        pass = true;

                        foreach (CharacterTrait searchTrait in rule.traitList)
                        {
                            if (!characterTraits.Exists(item => item.trait == searchTrait))
                            {
                                pass = false;
                                break;
                            }
                        }
                    }
                    else if (rule.rule == CharacterTrait.RuleType.ifNoneOfThese)
                    {
                        pass = true;

                        foreach (CharacterTrait searchTrait in rule.traitList)
                        {
                            if (characterTraits.Exists(item => item.trait == searchTrait))
                            {
                                pass = false;
                                break;
                            }
                        }
                    }
                    else if (rule.rule == CharacterTrait.RuleType.ifPartnerAnyOfThese)
                    {
                        if (partner != null)
                        {
                            foreach (CharacterTrait searchTrait in rule.traitList)
                            {
                                if (partner.characterTraits.Exists(item => item.trait == searchTrait))
                                {
                                    pass = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (pass)
                    {
                        for (int i = 0; i < rule.reasonChance; i++)
                        {
                            reasonPool.Add(reason);
                        }
                    }
                }
            }

            if (reasonPool.Count > 0)
            {
                newT.reason = AddCharacterTrait(reasonPool[Toolbox.Instance.GetPsuedoRandomNumberContained(0, reasonPool.Count, seed, out seed)]);
            }
        }

        return newT;
    }

    //Give this citizen a serious relationship!
    public void SetPartner(Citizen newLover)
    {
        partner = newLover;
        newLover.partner = this as Citizen;

        //Share address book
        if (newLover.addressBook == null)
        {
            addressBook = newLover.addressBook;
        }
    }

    //Give this citizen a home! 5th
    public virtual void SetResidence(ResidenceController newHome, bool removePreviousResidence = true)
    {
        //Remove previous residence
        if (residence != null && removePreviousResidence)
        {
            if (residence != newHome)
            {
                //Add locations of authority
                RemoveLocationOfAuthority(residence.address);

                residence.address.RemoveOwner(this);
                residence.address.RemoveInhabitant(this);

                //Remove toothbrush!
                RemovePersonalAffect(CitizenControls.Instance.toothbrush, false);
            }
        }

        residence = newHome;

        if (residence != null)
        {
            home = residence.address;
        }
        else home = null;

        //Add locations of authority
        if (home != null)
        {
            AddLocationOfAuthorty(home);

            //If self employed, set as company address...
            if (!isPlayer && job != null && job.employer != null)
            {
                if (job.employer.preset.isSelfEmployed && job.employer.address == null)
                {
                    job.employer.SetAddress(home);
                    if (job.employer.placeOfBusiness == null) job.employer.SetPlaceOfBusiness(home);
                }
            }

            //Give keys (there should probably be a faster way of doing this)
            AddToKeyring(home, false);

            foreach (NewRoom room in home.rooms)
            {
                foreach (NewNode node in room.nodes)
                {
                    foreach (NewWall wall in node.walls)
                    {
                        if (wall.door != null)
                        {
                            AddToKeyring(wall.door, false);
                        }
                    }
                }
            }

            home.AddOwner(this as Human);
            home.AddInhabitant(this as Human);

            //If self employed assign this address to company...
            if (job != null && job.employer != null && job.preset.selfEmployed)
            {
                job.employer.SetAddress(home);
                if (job.employer.placeOfBusiness == null) job.employer.SetPlaceOfBusiness(home);
            }
        }
    }

    //Adjust AI tick rate based on proximity to player
    public void UpdateTickRateOnProx()
    {
        if (ai != null)
        {
            //Highest if in the same room
            if (currentRoom == Player.Instance.currentRoom)
            {
                ai.SetDesiredTickRate(NewAIController.AITickRate.veryHigh);
            }
            //High if same game loc
            else if (currentGameLocation == Player.Instance.currentGameLocation)
            {
                ai.SetDesiredTickRate(NewAIController.AITickRate.high);
            }
            //Medium if same floor
            else if (currentBuilding != null && currentBuilding == Player.Instance.currentBuilding && currentGameLocation.floor == Player.Instance.currentGameLocation.floor)
            {
                ai.SetDesiredTickRate(NewAIController.AITickRate.medium);
            }
            //Low if same building
            else if (currentBuilding == Player.Instance.currentBuilding)
            {
                ai.SetDesiredTickRate(NewAIController.AITickRate.low);
            }
            //Very low if none of these
            else
            {
                ai.SetDesiredTickRate(NewAIController.AITickRate.veryLow);
            }
        }
    }

    //Generate stats: Name, Medical etc.
    public void SetupGeneral()
    {
        passcode = new GameplayController.Passcode(GameplayController.PasscodeType.citizen);
        passcode.id = humanID;

        CreateEvidence();

        //Pick fav colour
        favColourIndex = Toolbox.Instance.GetPsuedoRandomNumberContained(0, SocialStatistics.Instance.favouriteColoursPool.Count, seed, out seed);

        SetupInteractables();

        //Descriptions (generate most within this class)
        descriptors = new Descriptors(this);

        //Clamp slang usage
        slangUsage = Mathf.Clamp01(slangUsage);

        //Generate slang
        GenerateSlang();

        SetFootwear(ShoeType.barefoot);

        SetPhysicalModelParams();

        //Relationship anniversary for both people
        if ((SessionData.Instance.isFloorEdit || CityConstructor.Instance.generateNew) && partner != null)
        {
            //Pick a month
            if (anniversary.Length <= 0)
            {
                int annMonth = Toolbox.Instance.GetPsuedoRandomNumberContained(0, 12, seed, out seed) + 1;
                int annDate = Toolbox.Instance.GetPsuedoRandomNumberContained(1, SessionData.Instance.daysInMonths[annMonth - 1] + 1, seed, out seed);

                //Now calculate the year based on age.
                int minYear = SessionData.Instance.yearInt - GetAge() + 19 + SessionData.Instance.publicYear;
                minYear = Mathf.Max(minYear, SessionData.Instance.yearInt - GetAge() + 19 + SessionData.Instance.publicYear);

                int annYear = Toolbox.Instance.GetPsuedoRandomNumberContained(minYear, SessionData.Instance.publicYear, seed, out seed);

                //Get string
                anniversary = annMonth + "/" + annDate + "/" + annYear;
                partner.anniversary = anniversary;

                AddCharacterTrait(SocialStatistics.Instance.relationshipTrait);
                partner.AddCharacterTrait(SocialStatistics.Instance.relationshipTrait);
            }
        }

        ////Create name password
        //if (firstName.Length == 4)
        //{
        //    AddPossiblePassword(PasswordType.firstName, Toolbox.Instance.GetKeyCodeFromString(casualName));
        //}

        //if(surName.Length == 4)
        //{
        //    AddPossiblePassword(PasswordType.surName, Toolbox.Instance.GetKeyCodeFromString(surName));
        //}

        //if (nickName.Length == 4)
        //{
        //    AddPossiblePassword(PasswordType.nickName, Toolbox.Instance.GetKeyCodeFromString(nickName));
        //}

        GenerateBloodType();
    }

    private void GenerateBloodType()
    {
        //Generate blood type details
        int bloodPick = Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, seed, out seed);

        if (bloodPick < SocialStatistics.Instance.bloodOPosRatio)
        {
            bloodType = BloodType.Opos;
        }
        else if (bloodPick < SocialStatistics.Instance.bloodOPosRatio + SocialStatistics.Instance.bloodAPosRatio)
        {
            bloodType = BloodType.Apos;
        }
        else if (bloodPick < SocialStatistics.Instance.bloodOPosRatio + SocialStatistics.Instance.bloodAPosRatio + SocialStatistics.Instance.bloodBPosRatio)
        {
            bloodType = BloodType.Bpos;
        }
        else if (bloodPick < SocialStatistics.Instance.bloodOPosRatio + SocialStatistics.Instance.bloodAPosRatio + SocialStatistics.Instance.bloodBPosRatio + SocialStatistics.Instance.bloodONegRatio)
        {
            bloodType = BloodType.Oneg;
        }
        else if (bloodPick < SocialStatistics.Instance.bloodOPosRatio + SocialStatistics.Instance.bloodAPosRatio + SocialStatistics.Instance.bloodBPosRatio + SocialStatistics.Instance.bloodONegRatio + SocialStatistics.Instance.bloodANegRatio)
        {
            bloodType = BloodType.Aneg;
        }
        else if (bloodPick < SocialStatistics.Instance.bloodOPosRatio + SocialStatistics.Instance.bloodAPosRatio + SocialStatistics.Instance.bloodBPosRatio + SocialStatistics.Instance.bloodONegRatio + SocialStatistics.Instance.bloodANegRatio + SocialStatistics.Instance.bloodABPosRatio)
        {
            bloodType = BloodType.ABpos;
        }
        else if (bloodPick < SocialStatistics.Instance.bloodOPosRatio + SocialStatistics.Instance.bloodAPosRatio + SocialStatistics.Instance.bloodBPosRatio + SocialStatistics.Instance.bloodONegRatio + SocialStatistics.Instance.bloodANegRatio + SocialStatistics.Instance.bloodABPosRatio + SocialStatistics.Instance.bloodBNegRatio)
        {
            bloodType = BloodType.Bneg;
        }
        else
        {
            bloodType = BloodType.ABneg;
        }
    }

    public string GetBloodTypeString()
    {
        return Strings.Get("descriptors", "bloodtype_" + bloodType.ToString());
    }

    //Generate slang
    public void GenerateSlang()
    {
        if (SessionData.Instance.isFloorEdit) return;

        //Build lists of slang greetings which are specific to this personality only (defaults will be added after)
        List<string> greetingDefault = new List<string>(SocialStatistics.Instance.slangGreetingDefault);
        List<string> greetingMale = new List<string>(SocialStatistics.Instance.slangGreetingMale);
        List<string> greetingFemale = new List<string>(SocialStatistics.Instance.slangGreetingFemale);
        List<string> greetingLover = new List<string>(SocialStatistics.Instance.slangGreetingLover);
        List<string> curse = new List<string>(SocialStatistics.Instance.slangCurse);

        List<string> curseNoun = new List<string>(SocialStatistics.Instance.slangCurseNoun);
        List<string> praiseNoun = new List<string>(SocialStatistics.Instance.slangPraiseNoun);

        foreach (Trait t in characterTraits)
        {
            //Add slang
            greetingDefault.AddRange(t.trait.slangGreetingDefault);
            greetingMale.AddRange(t.trait.slangGreetingMale);
            greetingFemale.AddRange(t.trait.slangGreetingFemale);
            greetingLover.AddRange(t.trait.slangGreetingLover);
            curse.AddRange(t.trait.slangCurse);
            curseNoun.AddRange(t.trait.slangPraiseNoun);
            praiseNoun.AddRange(t.trait.slangPraiseNoun);
        }

        //Slang lists should be complete now, choose slang terms
        //slangGreetingDefault = greetingDefault[Toolbox.Instance.Rand(0, greetingDefault.Count, true)];
        //slangGreetingMale = greetingMale[Toolbox.Instance.Rand(0, greetingMale.Count, true)];
        //slangGreetingFemale = greetingFemale[Toolbox.Instance.Rand(0, greetingFemale.Count, true)];
        //slangGreetingLover = greetingLover[Toolbox.Instance.Rand(0, greetingLover.Count, true)];
        //slangCurse = curse[Toolbox.Instance.Rand(0, curse.Count, true)];
        //slangCurseNoun = curseNoun[Toolbox.Instance.Rand(0, curseNoun.Count, true)];
        //slangPraiseNoun = praiseNoun[Toolbox.Instance.Rand(0, praiseNoun.Count, true)];
    }

    //Set physical model scale depening on height etc
    public void SetPhysicalModelParams()
    {
        //Set physical model scale depening on height...
        //Use the average height as just under 6ft
        float baseScale = (descriptors.heightCM / SocialStatistics.Instance.averageHeight) * CitizenControls.Instance.baseScale;
        modelParent.transform.localScale = new Vector3(baseScale, baseScale, baseScale);

        //Weight head 70% towards a uniform scale across all models
        //float headScale = (SocialStatistics.Instance.averageHeight / descriptors.heightCM) * CitizenControls.Instance.baseScale;
        //headScale = Mathf.Lerp(1f, headScale, 0.7f);
        //animationController.headMesh.transform.localScale = new Vector3(headScale, headScale, headScale);
    }

    public override void CreateEvidence()
    {
        //Create citizen entry
        if (evidenceEntry == null)
        {
            string newID = "Human" + humanID;
            evidenceEntry = EvidenceCreator.Instance.CreateEvidence("citizen", newID, this, this, this) as EvidenceWitness;
        }
    }

    public void CreateDetails()
    {
        if (evidenceEntry == null)
        {
            Game.LogError(GetCitizenName() + " has no evidence file!");
        }

        if (residence != null && residence.address.evidenceEntry == null)
        {
            Game.LogError(residence.address.name + " has no evidence file!");
        }

        //Birthday
        Evidence birthdayDate = EvidenceCreator.Instance.GetDateEvidence(birthday);
        factDictionary.Add("Birthday", EvidenceCreator.Instance.CreateFact("Birthday", evidenceEntry, birthdayDate) as Fact);

        //Lives at
        if (residence != null)
        {
            factDictionary.Add("LivesAt", EvidenceCreator.Instance.CreateFact("LivesAt", evidenceEntry, residence.address.evidenceEntry) as Fact);
            factDictionary.Add("LivesAtBuilding", EvidenceCreator.Instance.CreateFact("LivesAtBuilding", evidenceEntry, residence.address.building.evidenceEntry) as Fact);
            factDictionary.Add("LivesOnFloor", EvidenceCreator.Instance.CreateFact("LivesOnFloor", evidenceEntry, residence.address.building.evidenceEntry) as Fact);

            //Telephone number
            if (home.telephones.Count > 0)
            {
                factDictionary.Add("TelephoneNumber", EvidenceCreator.Instance.CreateFact("IsTelephoneNumber", home.telephones[0].telephoneEntry, evidenceEntry) as Fact);
            }

            //Address book
            if (addressBook == null)
            {
                string evID = "AddressBook" + humanID;
                addressBook = EvidenceCreator.Instance.CreateEvidence("AddressBook", evID, this, this, this, newParent: home.evidenceEntry) as Evidence;

                foreach (Human h in home.inhabitants)
                {
                    h.addressBook = addressBook;
                }

                if (partner != null) partner.addressBook = addressBook; //Share address books

                setupAddressBook = false;
            }
        }

        //Documents
        ////Resident's contract
        //if (partner != null && partner.residentsContract != null) residentsContract = partner.residentsContract;
        //else
        //{
        //    //residentsContract = EvidenceCreator.Instance.CreateEvidence("ResidentsContract", this, this, newParent: home.evidenceEntry) as EvidenceItem;
        //    //homeFile.AddOrAppendContentToPage(homeFile.pageContent.Count + 1, Strings.Get("evidence.names", InteriorControls.Instance.residentsContract.name));
        //    //homeFile.AddEvidencePage(residentsContract, homeFile.pageContent.Count);
        //}

        //Birth certificate
        //birthCertificate = EvidenceCreator.Instance.CreateEvidence("BirthCertificate", this, this, newParent: home.evidenceEntry) as EvidenceItem;
        //homeFile.AddOrAppendContentToPage(homeFile.pageContent.Count + 1, Strings.Get("evidence.names", InteriorControls.Instance.birthCertificate.name));
        //homeFile.AddEvidencePage(birthCertificate, homeFile.pageContent.Count);

        //Bank statement
        //bankStatement = EvidenceCreator.Instance.CreateEvidence("BankStatement", this, this, newParent: home.evidenceEntry) as EvidenceItem;
        //homeFile.AddOrAppendContentToPage(homeFile.pageContent.Count + 1, Strings.Get("evidence.names", InteriorControls.Instance.bankStatement.name));
        //homeFile.AddEvidencePage(bankStatement, homeFile.pageContent.Count);

        //Medical details
        //medicalDetails = EvidenceCreator.Instance.CreateEvidence("MedicalDetails", this, this, newParent: home.evidenceEntry) as EvidenceItem;
        //homeFile.AddOrAppendContentToPage(homeFile.pageContent.Count + 1, Strings.Get("evidence.names", InteriorControls.Instance.medicalDetails.name));
        //homeFile.AddEvidencePage(medicalDetails, homeFile.pageContent.Count);

        //Add to resident's contract (address copy)
        //residentsContract.AddFactLink(factDictionary["LivesAt"], Evidence.DataKey.name, false);

        //ID Card
        //string newID = "IDCard" + humanID;
        //idCard = EvidenceCreator.Instance.CreateEvidence("IDCard", newID, this, this, newParent: evidenceEntry) as Evidence;

        //Works at
        if (job != null && job.employer != null)
        {
            if(job.employer.address != null) factDictionary.Add("WorksAt", EvidenceCreator.Instance.CreateFact("WorksAt", evidenceEntry, job.employer.address.evidenceEntry) as Fact);
            if(job.employer.address.building != null) factDictionary.Add("WorksAtBuilding", EvidenceCreator.Instance.CreateFact("WorksAtBuilding", evidenceEntry, job.employer.address.building.evidenceEntry) as Fact);
            factDictionary.Add("WorksHours", EvidenceCreator.Instance.CreateFact("WorksHours", evidenceEntry, job.employer.employeeRoster) as Fact);

            //Add to company roster
            int page = job.employer.employeeRoster.AddStringContentToNewPage(GetCitizenName() + " — " + Strings.Get("jobs", job.preset.name));
            job.employer.employeeRoster.AddEvidenceDiscoveryToPage(page, evidenceEntry, Evidence.Discovery.jobDiscovery);

            string evID = "EmployeeRecord" + humanID;
            Evidence employeeRecord = EvidenceCreator.Instance.CreateEvidence("EmployeeRecord", evID, this, this, this, newParent: job.employer.address.evidenceEntry);
            job.employer.employeeRoster.AddEvidenceToPage(page, employeeRecord);

            employeeRecord.AddFactLink(factDictionary["WorksAt"], Evidence.DataKey.name, false);
            employeeRecord.AddFactLink(factDictionary["WorksHours"], Evidence.DataKey.name, false);

            //Work ID
            evID = "WorkID" + humanID;
            workID = EvidenceCreator.Instance.CreateEvidence("WorkID", evID, this, this, this, newParent: home.evidenceEntry) as Evidence;
        }

        //Add to resident roster
        if (home != null)
        {
            int page = home.building.residentRoster.AddStringContentToNewPage(GetCitizenName());
            home.building.residentRoster.AddEvidenceDiscoveryToPage(page, evidenceEntry, Evidence.Discovery.livesAt);

            string evID = "ResidentFile" + humanID;
            Evidence resFile = EvidenceCreator.Instance.CreateEvidence("ResidentFile", evID, this, this, this, newParent: home.building.evidenceEntry);
            home.building.residentRoster.AddEvidenceToPage(page, resFile);

            home.building.residentRoster.AddFactLink(factDictionary["LivesAt"], Evidence.DataKey.name, false);
        }

        //Favourites
        foreach (KeyValuePair<CompanyPreset.CompanyCategory, NewAddress> pair in favouritePlaces)
        {
            //Create favourite fact
            factDictionary.Add("Fav" + pair.Key.ToString(), EvidenceCreator.Instance.CreateFact("Favourite", evidenceEntry, pair.Value.evidenceEntry) as Fact);
        }

        //Diary
        //diary = EvidenceCreator.Instance.CreateEvidence("Diary", this, this, newParent: home.evidenceEntry) as EvidenceItem;
        //AddPersonalAffect(InteriorControls.Instance.diary, false);
    }

    //Calculate age: Based on occupation
    public void CalculateAge()
    {
        //Choose age based on rank at work
        //Weight age ranges according to job rank.
        //Because couples are chosen based on their residence land value, they are likely to end up in a similar age range as they are likely to have similar jobs.
        int[] ageWeight = {/*19-21*/6, /*22-26*/8, /*27-31*/6, /*32-36*/5, /*37-41*/4, /*42-46*/3, /*47-51*/2, /*52-56*/2, /*57-61*/1, /*62-66*/1, /*67+*/0 };

        if (job != null)
        {
            if (job.paygrade <= 0.25f)
            {
                ageWeight = new int[] {/*19-21*/3, /*22-26*/6, /*27-31*/7, /*32-36*/6, /*37-41*/5, /*42-46*/4, /*47-51*/3, /*52-56*/2, /*57-61*/2, /*62-66*/1, /*67+*/0 };

            }
            else if (job.paygrade > 0.25f && job.paygrade <= 0.5f)
            {
                ageWeight = new int[] {/*19-21*/1, /*22-26*/5, /*27-31*/6, /*32-36*/7, /*37-41*/7, /*42-46*/6, /*47-51*/4, /*52-56*/3, /*57-61*/2, /*62-66*/1, /*67+*/0 };

            }
            else if (job.paygrade > 0.5f && job.paygrade <= 0.75f)
            {
                ageWeight = new int[] {/*19-21*/0, /*22-26*/1, /*27-31*/4, /*32-36*/6, /*37-41*/7, /*42-46*/6, /*47-51*/5, /*52-56*/3, /*57-61*/2, /*62-66*/1, /*67+*/0 };

            }
            else if (job.paygrade > 0.75f)
            {
                ageWeight = new int[] {/*19-21*/0, /*22-26*/0, /*27-31*/3, /*32-36*/5, /*37-41*/7, /*42-46*/7, /*47-51*/6, /*52-56*/4, /*57-61*/3, /*62-66*/2, /*67+*/1 };
            }

            if (job.work == OccupationPreset.workType.Unemployed)
            {
                ageWeight = new int[] {/*19-21*/6, /*22-26*/7, /*27-31*/5, /*32-36*/4, /*37-41*/3, /*42-46*/2, /*47-51*/1, /*52-56*/1, /*57-61*/1, /*62-66*/0, /*67+*/0 };

            }
            else if (job.work == OccupationPreset.workType.Retired)
            {
                ageWeight = new int[] {/*19-21*/0, /*22-26*/0, /*27-31*/0, /*32-36*/1, /*37-41*/2, /*42-46*/3, /*47-51*/4, /*52-56*/5, /*57-61*/6, /*62-66*/8, /*67+*/12 };
            }
        }
        else
        {
            ageWeight = new int[] {/*19-21*/6, /*22-26*/7, /*27-31*/5, /*32-36*/4, /*37-41*/3, /*42-46*/2, /*47-51*/1, /*52-56*/1, /*57-61*/1, /*62-66*/0, /*67+*/0 };
        }

        //Create a list of possible age ranges.
        List<int> ageProbability = new List<int>();
        List<int> ageGroupProb = new List<int>();

        for (int n = 0; n < SocialStatistics.Instance.ageRanges.Length; n++)
        {
            for (int i = 0; i < ageWeight[n]; i++)
            {
                ageProbability.Add(SocialStatistics.Instance.ageRanges[n]);
                ageGroupProb.Add(n);
            }
        }

        //Choose an age range from list.
        int rangeSelection = Toolbox.Instance.GetPsuedoRandomNumberContained(0, ageProbability.Count - 1, seed, out seed);

        //Create random age within this age range to assign.
        int ageGroup = ageGroupProb[rangeSelection];
        int age = 25;

        if (ageGroup != 10)
        {
            age = Toolbox.Instance.GetPsuedoRandomNumberContained(ageProbability[rangeSelection], ageProbability[rangeSelection] + 5, seed, out seed);
        }
        else
        {
            //oap
            age = Toolbox.Instance.GetPsuedoRandomNumberContained(ageProbability[rangeSelection], 110, seed, out seed);
        }

        //Pick a birthday
        //Pick a month
        int birthdayMonth = Toolbox.Instance.GetPsuedoRandomNumberContained(0, 12, seed, out seed) + 1;
        int birthdayDate = Toolbox.Instance.GetPsuedoRandomNumberContained(1, SessionData.Instance.daysInMonths[birthdayMonth - 1] + 1, seed, out seed);

        //Now calculate the year based on age.
        int birthdayYear = SessionData.Instance.yearInt - age - 1 + SessionData.Instance.publicYear;

        //If we've passed their birthday this year add 1
        if (birthdayMonth > SessionData.Instance.monthInt)
        {
            birthdayYear += 1;
        }
        else if (birthdayMonth == SessionData.Instance.monthInt)
        {
            if (birthdayDate >= SessionData.Instance.dateInt)
            {
                birthdayYear += 1;
            }
        }

        //Get string
        birthday = birthdayMonth + "/" + birthdayDate + "/" + birthdayYear;

        string password1 = birthdayMonth.ToString();
        if (password1.Length < 2) password1 = "0" + password1;

        string password2 = birthdayDate.ToString();
        if (password2.Length < 2) password2 = "0" + password2;

        //Calculate speed
        speedMultiplier = Toolbox.Instance.GetPsuedoRandomNumberContained(CitizenControls.Instance.movementSpeedMultiplierRange.x, CitizenControls.Instance.movementSpeedMultiplierRange.y, seed, out seed);
        movementWalkSpeed = speedMultiplier * CitizenControls.Instance.baseCitizenWalkSpeed;
        movementRunSpeed = speedMultiplier * CitizenControls.Instance.baseCitizenRunSpeed;
        walkingSpeedRatio = movementWalkSpeed / movementRunSpeed;
    }

    public int GetAge()
    {
        string[] parseBday = birthday.Split('/');

        if (parseBday.Length >= 3)
        {
            int birthdayMonth = 0;
            if(!int.TryParse(parseBday[0], out birthdayMonth))
            {
                Game.LogError("Unable to parse " + parseBday[0] + " to int");
            }

            int birthdayDate = 0;
            if(!int.TryParse(parseBday[1], out birthdayDate))
            {
                Game.LogError("Unable to parse " + parseBday[1] + " to int");
            }

            int birthdayYear = 0;
            if(!int.TryParse(parseBday[2], out birthdayYear))
            {
                Game.LogError("Unable to parse " + parseBday[2] + " to int");
            }

            //Have we gone past the date this year?
            int ageAddition = -1;

            if(SessionData.Instance.monthInt > (birthdayMonth - 1))
            {
                ageAddition = 0;
            }
            else if(SessionData.Instance.monthInt == (birthdayMonth - 1))
            {
                if(SessionData.Instance.dateInt >= (birthdayDate - 1))
                {
                    ageAddition = 0;
                }
            }

            return SessionData.Instance.yearInt - birthdayYear + ageAddition + SessionData.Instance.publicYear;
        }
        else return 25;
    }

    public Descriptors.Age GetAgeGroup()
    {
        int age = GetAge();

        //Set age enum
        if (age < 30)
        {
            return Descriptors.Age.youngAdult;
        }
        else if (age > 60)
        {
            return Descriptors.Age.old;
        }
        else
        {
            return Descriptors.Age.adult;
        }
    }

    //Pick password: Pick a password, after all possible ones completed
    public void PickPassword()
    {
        //Is a trait password has not been picked, use a random one
        if (passwordTrait == null)
        {
            AddCharacterTrait(CitizenControls.Instance.randomPassword);
        }

        passcode.digits.Clear();

        if (passwordTrait.name == "Secret-Password-Random")
        {
            passcode.digits.Add(Toolbox.Instance.GetPsuedoRandomNumberContained(0, 10, seed, out seed));
            passcode.digits.Add(Toolbox.Instance.GetPsuedoRandomNumberContained(0, 10, seed, out seed));
            passcode.digits.Add(Toolbox.Instance.GetPsuedoRandomNumberContained(0, 10, seed, out seed));
            passcode.digits.Add(Toolbox.Instance.GetPsuedoRandomNumberContained(0, 10, seed, out seed));
        }
        else if (passwordTrait.name == "Secret-Password-0451")
        {
            passcode.digits.Add(0);
            passcode.digits.Add(4);
            passcode.digits.Add(5);
            passcode.digits.Add(1);
        }
        else if (passwordTrait.name == "Secret-Password-Address")
        {
            //Parse residence number
            if (home != null && home.floor.floor >= 0)
            {
                string numStr = home.residence.GetResidenceString();

                //Create residence string
                if (numStr.Length < 4) numStr = "0" + numStr;

                for (int i = 0; i < 4; i++)
                {
                    int p = 0;

                    int.TryParse(numStr.Substring(i, 1), out p);
                    passcode.digits.Add(p);
                }
            }
            else
            {
                passcode.digits.Add(0);
                passcode.digits.Add(0);
                passcode.digits.Add(0);
                passcode.digits.Add(1);
            }
        }
        else if (passwordTrait.name == "Secret-Password-AnniversaryDeadFather")
        {
            //Find trait and use date
            Trait findTrait = characterTraits.Find(item => item.trait.name == "Event-FatherDiedWhenYoung");
            string[] dateComp = findTrait.date.Split('/');

            string month = dateComp[0];
            string day = dateComp[1];

            if (month.Length < 2)
            {
                month = "0" + month;
            }

            if (day.Length < 2)
            {
                day = "0" + day;
            }

            string dateStr = month + day; //Compose without year

            for (int i = 0; i < 4; i++)
            {
                int p = 0;
                int.TryParse(dateStr.Substring(i, 1), out p);
                passcode.digits.Add(p);
            }
        }
        else if (passwordTrait.name == "Secret-Password-AnniversaryDeadMother")
        {
            //Find trait and use date
            Trait findTrait = characterTraits.Find(item => item.trait.name == "Event-MotherDiedWhenYoung");
            string[] dateComp = findTrait.date.Split('/');

            string month = dateComp[0];
            string day = dateComp[1];

            if (month.Length < 2)
            {
                month = "0" + month;
            }

            if (day.Length < 2)
            {
                day = "0" + day;
            }

            string dateStr = month + day; //Compose without year

            for (int i = 0; i < 4; i++)
            {
                int p = 0;
                int.TryParse(dateStr.Substring(i, 1), out p);
                passcode.digits.Add(p);
            }
        }
        else if (passwordTrait.name == "Secret-Password-AnniversaryDeadSibling")
        {
            //Find trait and use date
            Trait findTrait = characterTraits.Find(item => item.trait.name == "Event-SiblingDiedWhenYoung");
            string[] dateComp = findTrait.date.Split('/');

            string month = dateComp[0];
            string day = dateComp[1];

            if (month.Length < 2)
            {
                month = "0" + month;
            }

            if (day.Length < 2)
            {
                day = "0" + day;
            }

            string dateStr = month + day; //Compose without year

            for (int i = 0; i < 4; i++)
            {
                int p = 0;
                int.TryParse(dateStr.Substring(i, 1), out p);
                passcode.digits.Add(p);
            }
        }
        else if (passwordTrait.name == "Secret-Password-CountDown")
        {
            int countDownFrom = Toolbox.Instance.GetPsuedoRandomNumberContained(4, 10, seed, out seed);
            passcode.digits.Add(countDownFrom);
            passcode.digits.Add(countDownFrom - 1);
            passcode.digits.Add(countDownFrom - 2);
            passcode.digits.Add(countDownFrom - 3);
        }
        else if (passwordTrait.name == "Secret-Password-CountUp")
        {
            int countUpFrom = Toolbox.Instance.GetPsuedoRandomNumberContained(1, 7, seed, out seed);
            passcode.digits.Add(countUpFrom);
            passcode.digits.Add(countUpFrom + 1);
            passcode.digits.Add(countUpFrom + 2);
            passcode.digits.Add(countUpFrom + 3);
        }
        else if (passwordTrait.name == "Secret-Password-DateOfAnniversary")
        {
            string[] dateComp = anniversary.Split('/');

            string month = dateComp[0];
            string day = dateComp[1];

            if (month.Length < 2)
            {
                month = "0" + month;
            }

            if (day.Length < 2)
            {
                day = "0" + day;
            }

            string dateStr = month + day; //Compose without year

            for (int i = 0; i < 4; i++)
            {
                int p = 0;
                int.TryParse(dateStr.Substring(i, 1), out p);
                passcode.digits.Add(p);
            }
        }
        else if (passwordTrait.name == "Secret-Password-DateOfBirth")
        {
            string[] dateComp = birthday.Split('/');

            string month = dateComp[0];
            string day = dateComp[1];

            if (month.Length < 2)
            {
                month = "0" + month;
            }

            if (day.Length < 2)
            {
                day = "0" + day;
            }

            string dateStr = month + day; //Compose without year

            for (int i = 0; i < 4; i++)
            {
                int p = 0;
                int.TryParse(dateStr.Substring(i, 1), out p);
                passcode.digits.Add(p);
            }
        }
        else if (passwordTrait.name == "Secret-Password-DateOfBirthPartner")
        {
            string[] dateComp = partner.birthday.Split('/');

            string month = dateComp[0];
            string day = dateComp[1];

            if (month.Length < 2)
            {
                month = "0" + month;
            }

            if (day.Length < 2)
            {
                day = "0" + day;
            }

            string dateStr = month + day; //Compose without year

            for (int i = 0; i < 4; i++)
            {
                int p = 0;
                int.TryParse(dateStr.Substring(i, 1), out p);
                passcode.digits.Add(p);
            }
        }
        else if (passwordTrait.name == "Secret-Password-EndWork")
        {
            //Add job start time...
            float formatted = SessionData.Instance.FloatMinutes24H(job.endTimeDecialHour);
            //Get hours only
            int hours = Mathf.FloorToInt(formatted);
            string output = hours.ToString();
            //If single digit hours, shove another 0 infront!
            if (output.Length == 1) output = "0" + output;
            //Add the minutes
            string minutes = Mathf.RoundToInt((formatted - (float)hours) * 100f).ToString();
            if (minutes.Length == 1) minutes = "0" + minutes;
            string timeStr = output + minutes;

            for (int i = 0; i < 4; i++)
            {
                int p = 0;
                int.TryParse(timeStr.Substring(i, 1), out p);
                passcode.digits.Add(p);
            }
        }
        else if (passwordTrait.name == "Secret-Password-ShoeSize")
        {
            string shoeStr = descriptors.shoeSize.ToString();

            while (shoeStr.Length < 4)
            {
                shoeStr = "0" + shoeStr;
            }

            for (int i = 0; i < 4; i++)
            {
                int p = 0;
                int.TryParse(shoeStr.Substring(i, 1), out p);
                passcode.digits.Add(p);
            }
        }
        else if (passwordTrait.name == "Secret-Password-StairsOnWayToWork")
        {
            string floorStr = (home.floor.floor + job.employer.placeOfBusiness.floor.floor).ToString();

            while (floorStr.Length < 4)
            {
                floorStr = "0" + floorStr;
            }

            for (int i = 0; i < 4; i++)
            {
                int p = 0;
                int.TryParse(floorStr.Substring(i, 1), out p);
                passcode.digits.Add(p);
            }
        }
        else if (passwordTrait.name == "Secret-Password-StartWork")
        {
            //Add job start time...
            float formatted = SessionData.Instance.FloatMinutes24H(job.startTimeDecimalHour);
            //Get hours only
            int hours = Mathf.FloorToInt(formatted);
            string output = hours.ToString();
            //If single digit hours, shove another 0 infront!
            if (output.Length == 1) output = "0" + output;
            //Add the minutes
            string minutes = Mathf.RoundToInt((formatted - (float)hours) * 100f).ToString();
            if (minutes.Length == 1) minutes = "0" + minutes;
            string timeStr = output + minutes;

            for (int i = 0; i < 4; i++)
            {
                int p = 0;
                int.TryParse(timeStr.Substring(i, 1), out p);
                passcode.digits.Add(p);
            }
        }
        else if (passwordTrait.name == "Secret-Password-YearOfBirth")
        {
            string[] dateComp = birthday.Split('/');

            for (int i = 0; i < 4; i++)
            {
                int p = 0;
                int.TryParse(dateComp[2].Substring(i, 1), out p);
                passcode.digits.Add(p);
            }
        }
        else if (passwordTrait.name == "Secret-Password-YearOfBirthPartner")
        {
            string[] dateComp = partner.birthday.Split('/');

            for (int i = 0; i < 4; i++)
            {
                int p = 0;
                int.TryParse(dateComp[2].Substring(i, 1), out p);
                passcode.digits.Add(p);
            }
        }

        if (home != null)
        {
            Interactable newNote = WriteNote(NoteObject.note, "970c4114-def0-4e04-8982-da36e01f4905", this, home, 1, InteractablePreset.OwnedPlacementRule.ownedOnly, 3); //Place note @ home.

            if(newNote != null)
            {
                passcode.notes.Add(newNote.id);
            }

            //Small chance this also features at work (must also have their own computer/desk at work)
            if (job != null && job.employer != null && job.preset.ownsWorkPosition && job.preset.jobPostion == InteractablePreset.SpecialCase.workDesk)
            {
                //Base 50% chance of there being a password note @ work.
                float chanceOfWorkCopy = 0.5f;

                //If forgetful, 100%
                if (characterTraits.Exists(item => item.trait.name == "Quirk-Forgetful"))
                {
                    chanceOfWorkCopy = 1f;
                }

                if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) <= chanceOfWorkCopy)
                {
                    Interactable newNote2 = WriteNote(NoteObject.note, "970c4114-def0-4e04-8982-da36e01f4905", this, job.employer.address, 1, InteractablePreset.OwnedPlacementRule.ownedOnly, 0); //Place note @ home.

                    if (newNote2 != null)
                    {
                        passcode.notes.Add(newNote2.id);
                    }
                }
            }
        }
    }

    //Setup starting stats and position etc
    public virtual void PrepForStart()
    {
        //Reset to maximum health
        ResetHealthToMaximum();
        ResetNerveToMaximum();

        //Add locations of authority
        if (home != null)
        {
            AddLocationOfAuthorty(home);

            //Setup address book
            if (!setupAddressBook)
            {
                foreach (Human h in home.inhabitants)
                {
                    h.addressBook = addressBook;
                }

                if (partner != null) partner.addressBook = addressBook; //Share address books

                setupAddressBook = true;
            }

            //Add places where residents are allowed
            foreach (KeyValuePair<int, NewFloor> pair in home.building.floors)
            {
                foreach (NewAddress ad in pair.Value.addresses)
                {
                    if (ad.addressPreset != null && ad.addressPreset.sameBuildingResidentsAuthority)
                    {
                        AddLocationOfAuthorty(ad);
                    }
                }
            }
        }
        else
        {
            //Citizen must have authority to use bed
            if (sleepPosition != null)
            {
                if (sleepPosition.node != null)
                {
                    AddLocationOfAuthorty(sleepPosition.node.gameLocation);
                }
            }
        }

        //Set job debug
        if (ai != null && job != null)
        {
            ai.jobDebug = job.name;

            if(job.employer != null && job.employer.placeOfBusiness != null)
            {
                AddLocationOfAuthorty(job.employer.placeOfBusiness);

                //Add places where residents are allowed
                if(job.employer.placeOfBusiness.building != null)
                {
                    foreach (KeyValuePair<int, NewFloor> pair in job.employer.placeOfBusiness.building.floors)
                    {
                        foreach (NewAddress ad in pair.Value.addresses)
                        {
                            if (ad.addressPreset != null && ad.addressPreset.sameBuildingEmployeesAuthority)
                            {
                                AddLocationOfAuthorty(ad);
                            }
                        }
                    }
                }
            }

            //Add dialog options for job
            //Add any follow ups
            foreach (DialogPreset dp in job.preset.addDialog)
            {
                if (dp == null) continue;
                evidenceEntry.AddDialogOption(dp.tiedToKey, dp);
            }
        }

        //Make sure I have a bed...
        if (sleepPosition == null && home != null)
        {
            Game.LogError("AI: Citizen has no sleep position at their home: " + this.GetCitizenName() + " (" + home.name + ")");
        }

        //Place favourite items @ home and @ work
        if (CityConstructor.Instance != null && CityConstructor.Instance.generateNew)
        {
            PlaceFavouriteItems();
            SpawnInventoryItems();
        }

        //How much do I like the rain?
        if (likesTheRain)
        {
            ownsUmbrella = false;
        }
        else
        {
            float chanceToOwnUmbrella = 0.25f + conscientiousness;
            if (isHomeless) chanceToOwnUmbrella -= 0.4f;

            if (Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, GetCitizenName()) < chanceToOwnUmbrella)
            {
                ownsUmbrella = true;
            }
            else ownsUmbrella = false;
        }

        //Am I working today?
        float nextPrevWork = 0f;
        if (job != null && job.employer != null) nextPrevWork = SessionData.Instance.GetNextOrPreviousGameTimeForThisHour(job.workDaysList, job.startTimeDecimalHour, job.endTimeDecialHour);

        float timeUntilWork = nextPrevWork - SessionData.Instance.gameTime;

        //In bed
        if (home != null && !home.actionReference.ContainsKey(RoutineControls.Instance.sleep))
        {
            Game.LogError("Object: No bed present at " + home.name + ", floor " + home.floor.floorName + ", building " + home.building.buildingID);
        }

        if (!isDead)
        {
            if (home != null && job != null && job.employer != null)
            {
                if (timeUntilWork >= 0.25f)
                {
                    if (home.actionReference.ContainsKey(RoutineControls.Instance.sleep))
                    {
                        Teleport(FindSafeTeleport(home), null);
                    }
                    else Teleport(FindSafeTeleport(home), null);
                }
                else
                {
                    //In work
                    if (job.employer.placeOfBusiness != null && job.employer.placeOfBusiness.rooms.Count > 1)
                    {
                        Teleport(FindSafeTeleport(job.employer.placeOfBusiness), null);
                        ai.timeAtCurrentAddress = Mathf.Abs(timeUntilWork);
                    }
                    else
                    {
                        Teleport(FindSafeTeleport(home), null);
                    }
                }
            }
            else
            {
                //In bed
                if (home != null && home.actionReference.ContainsKey(RoutineControls.Instance.sleep))
                {
                    Teleport(FindSafeTeleport(home), null);
                }
                //In street
                else
                {
                    Teleport(FindSafeTeleport(CityData.Instance.streetDirectory[Toolbox.Instance.Rand(0, CityData.Instance.streetDirectory.Count)]), null);
                }
            }
        }

        float sleepMin = 0f;
        float sleepMax = 1f;

        if (timeUntilWork <= 0f)
        {
            sleepMin = 0.5f;
        }

        //Calculate likely starting status stats based on how long I've likely been asleep...
        AddNourishment(Toolbox.Instance.Rand(sleepMin, sleepMax));
        AddEnergy(Toolbox.Instance.Rand(sleepMin, sleepMax));
        AddHydration(Toolbox.Instance.Rand(0.1f, 1f));
        AddAlertness(Toolbox.Instance.Rand(0.1f, 1f));
        AddExcitement(Toolbox.Instance.Rand(0.1f, 1f));
        AddChores(Toolbox.Instance.Rand(0.1f, 1f));
        AddHygiene(Toolbox.Instance.Rand(sleepMin, sleepMax));
        AddBladder(Toolbox.Instance.Rand(0.1f, 1f));

        //Set idle sound
        if (ai != null)
        {
            ai.lastUpdated = SessionData.Instance.gameTime;
            ai.idleSound = Toolbox.Instance.Rand(0f, 1f);

            ai.nourishment = nourishment;
            ai.hydration = hydration;
            ai.alertness = alertness;
            ai.excitement = excitement;
            ai.energy = energy;
            ai.chores = chores;
            ai.hygiene = hygiene;
            ai.bladder = bladder;
            ai.heat = heat;
            ai.drunk = drunk;
            ai.breath = breath;
        }

        //Update location data
        UpdateGameLocation();

        //Spawn inventory items
        //if(CityConstructor.Instance.generateNew)
        //{
        //    Interactable interactableIDCard = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.IDCard, this, null, Vector3.zero, Vector3.zero, null, null);
        //    interactableIDCard.SetInInventory(this);
        //}

        //Update time spent here already
        //ai.timeAtCurrentAddress = workEndedThisTimeAgo;

        GenerateVocab();

        if (CityConstructor.Instance != null && CityConstructor.Instance.saveState == null)
        {
            GeneratePastVmails();
            WalletItemCheck(4);

            //Game.Log("Citizen " + name + " has started " + messageThreadsStarted.Count + " vmail threads...");
        }

        //Force-load outfit
        outfitController.SetCurrentOutfit(ClothesPreset.OutfitCategory.casual, true);
    }

    //Generate vocab from presets
    public void GenerateVocab()
    {
        //Scan through all trees and pick relevent
        foreach (KeyValuePair<string, DDSSaveClasses.DDSTreeSave> pair in Toolbox.Instance.allDDSTrees)
        {
            if (pair.Value.treeType == DDSSaveClasses.TreeType.conversation || pair.Value.treeType == DDSSaveClasses.TreeType.vmail)
            {
                //Scan for correct job...
                if (pair.Value.participantA.useJobs)
                {
                    if (job == null || job.preset == null) continue;

                    if (!pair.Value.participantA.jobs.Contains(job.preset.name))
                    {
                        //Game.Log(name + " doesn't contain job " + job.preset.name);
                        continue;
                    }
                }

                //Check against traits
                if (pair.Value.participantA.useTraits)
                {
                    bool pass = Toolbox.Instance.DDSTraitConditionLogic(this, null, pair.Value.participantA.traitConditions, ref pair.Value.participantA.traits);

                    if (!pass) continue;
                }

                //If at this point, add to vocab
                AddDDSVocab(pair.Value);
            }
        }

        //int count = 0;

        //foreach(KeyValuePair<DDSSaveClasses.TriggerPoint, List<DDSSaveClasses.DDSTreeSave>> pair in dds)
        //{
        //    count += pair.Value.Count;

        //    foreach(DDSSaveClasses.DDSTreeSave t in pair.Value)
        //    {
        //        Game.Log("..." + t.name);
        //    }
        //}

        //Game.Log("Added " + count + " vocab entries for " + name + "...");
    }

    public virtual void AddDDSVocab(DDSSaveClasses.DDSTreeSave newTree)
    {
        if (!dds.ContainsKey(newTree.triggerPoint))
        {
            dds.Add(newTree.triggerPoint, new List<DDSSaveClasses.DDSTreeSave>());
        }

        dds[newTree.triggerPoint].Add(newTree);
    }

    //Generate already-sent vmails
    public void GeneratePastVmails()
    {
        //Speech trigger point
        if (dds.ContainsKey(DDSSaveClasses.TriggerPoint.vmail))
        {
            string seed = humanID.ToString() + CityData.Instance.seed + humanID.ToString();

            int threadsToGenerate = Toolbox.Instance.RandContained(7, 15, seed, out seed);
            int safety = dds.Count * 2;

            while(threadsToGenerate > 0 && safety > 0)
            {
                DDSSaveClasses.DDSTreeSave sp = dds[DDSSaveClasses.TriggerPoint.vmail][Toolbox.Instance.RandContained(0, dds[DDSSaveClasses.TriggerPoint.vmail].Count, seed, out seed)];

                //Overall chance
                if (Toolbox.Instance.RandContained(0f, 1f, seed, out seed) > sp.treeChance)
                {
                    safety--;
                    continue;
                }

                //This person can't already be featured in the 'same' tree thread by someone else.
                if (messageThreadFeatures.Exists(item => item.treeID == sp.id))
                {
                    safety--;
                    continue;
                }

                if (messageThreadsStarted.Exists(item => item.treeID == sp.id))
                {
                    safety--;
                    continue;
                }

                bool pass = true;

                //Attempt to match conditions for participants
                List<Human> otherParticipants = new List<Human>();

                for (int i = 0; i < 4; i++)
                {
                    //Participant A is always the initiator, aka this human
                    if (i == 0)
                    {
                        bool self = DDSParticipantConditionCheck(this, sp.participantA, DDSSaveClasses.TreeType.vmail);

                        if (!self)
                        {
                            safety--;
                            pass = false;
                            break;
                        }
                    }
                    else
                    {
                        DDSSaveClasses.DDSParticipant participant = sp.participantB;
                        if (i == 2) participant = sp.participantC;
                        if (i == 3) participant = sp.participantD;

                        if (participant.required)
                        {
                            Human part = null;

                            if (Toolbox.Instance.GetVmailParticipant(this, participant, otherParticipants, out part))
                            {
                                otherParticipants.Add(part);
                            }
                            else
                            {
                                pass = false;
                                break;
                            }
                        }
                        else continue; //Continued if we don't need this participant
                    }
                }

                if (!pass)
                {
                    //If invalid participants, continue scanning for usable trees...
                    safety--;
                    continue;
                }

                //If this point is reached, the speech preset is valid so do the vmail thread. Use random progress
                Toolbox.Instance.NewVmailThread(this, otherParticipants, sp.id, SessionData.Instance.gameTime + Toolbox.Instance.GetPsuedoRandomNumberContained(-48f, -12f, this.seed, out this.seed), Toolbox.Instance.GetPsuedoRandomNumberContained(0, sp.messageRef.Count + 1, this.seed, out this.seed));

                threadsToGenerate--;
            }
        }
    }

    public void SetupInteractables()
    {
        List<InteractableController> controllers = this.gameObject.GetComponentsInChildren<InteractableController>().ToList();

        if (interactableController == null)
        {
            interactableController = controllers.Find(item => item.id == InteractableController.InteractableID.A);
        }

        //Setup interaction controller
        interactable = InteractableCreator.Instance.CreateCitizenInteractable(CitizenControls.Instance.citizenInteractable, this, interactableController.transform, evidenceEntry);
        interactableController.Setup(interactable);

        //Setup hand interactables
        if (!SessionData.Instance.isFloorEdit)
        {
            InteractableController rightHandController = controllers.Find(item => item.id == InteractableController.InteractableID.B);
            rightHandInteractable = InteractableCreator.Instance.CreateCitizenInteractable(CitizenControls.Instance.handInteractable, this, rightHandController.transform, null);
            rightHandController.Setup(rightHandInteractable);

            InteractableController leftHandController = controllers.Find(item => item.id == InteractableController.InteractableID.C);
            leftHandInteractable = InteractableCreator.Instance.CreateCitizenInteractable(CitizenControls.Instance.handInteractable, this, leftHandController.transform, null);
            leftHandController.Setup(leftHandInteractable);
        }
    }

    //Load data from file
    public void Load(CitySaveData.HumanCitySave data)
    {
        humanID = data.humanID;
        seed = Toolbox.Instance.SeedRand(0, 999999999).ToString();

        //For now, create evidence
        CreateEvidence();

        //Setup interaction controller
        SetupInteractables();

        if (!data.homeless)
        {
            home = CityData.Instance.addressDirectory.Find(item => item.id == data.home);
            residence = home.residence;
            SetResidence(residence);
        }

        slangUsage = data.slangUsage;
        favColourIndex = data.favCol;

        anniversary = data.anniversary;

        speedMultiplier = data.speedModifier;
        movementWalkSpeed = speedMultiplier * CitizenControls.Instance.baseCitizenWalkSpeed;
        movementRunSpeed = speedMultiplier * CitizenControls.Instance.baseCitizenRunSpeed;
        walkingSpeedRatio = movementWalkSpeed / movementRunSpeed;

        if (data.job <= 0)
        {
            SetJob(CitizenCreator.Instance.CreateUnemployed());
        }
        else
        {
            job = CityData.Instance.jobsDirectory.Find(item => item.id == data.job);

            if (job == null)
            {
                Game.LogError("Cant find job " + data.job + " for " + data.citizenName + " using employed. Setting as unemployed.");
                SetJob(CitizenCreator.Instance.CreateUnemployed());
            }
            else if (job.employee != null && job.employee != this)
            {
                Game.LogError("employee already set for job " + data.job + " for " + data.citizenName + ". Setting as unemployed.");
                SetJob(CitizenCreator.Instance.CreateUnemployed());
            }
            else SetJob(job);
        }

        societalClass = data.societalClass; //Rank from 0 - 1, 0 being working class, 1 being upper class. Based on job paygrade

        descriptors = data.descriptors;
        descriptors.citizen = this;
        bloodType = data.blood;

        citizenName = data.citizenName;
        name = citizenName;
        this.transform.name = name;
        firstName = data.firstName;
        casualName = data.casualName;
        surName = data.surName;
        //initialledName = data.initialledName;
        genderScale = data.genderScale;
        gender = data.gender;
        birthGender = data.bGender;
        attractedTo = data.attractedTo;
        homosexuality = data.homosexuality;
        sexuality = data.sexuality;
        passcode = data.password;
        isHomeless = data.homeless;
        if (isHomeless) CityData.Instance.homelessDirectory.Add(this as Citizen);
        else CityData.Instance.homedDirectory.Add(this as Citizen);

        if(data.handwriting != null && data.handwriting.Length > 0) Toolbox.Instance.LoadDataFromResources<HandwritingPreset>(data.handwriting, out handwriting);

        birthday = data.birthday;

        //Add possible passwords
        string[] bday = birthday.Split('/');

        string password1 = bday[0].ToString();
        if (password1.Length < 2) password1 = "0" + password1;

        string password2 = bday[1].ToString();
        if (password2.Length < 2) password2 = "0" + password2;

        sleepNeedMultiplier = data.sleepNeedMultiplier; //Criminals will need more spare time, so need less sleep
        snoring = data.snoring; //Amount this person snores in their sleep
        snoreDelay = data.snoreDelay; //Time between snores in seconds
        humility = data.humility;
        emotionality = data.emotionality;
        extraversion = data.extraversion;
        agreeableness = data.agreeableness;
        conscientiousness = data.conscientiousness;
        creativity = data.creativity;

        //Load traits
        foreach (CitySaveData.CharTraitSave traitSave in data.traits)
        {
            Trait newT = new Trait();
            newT.traitID = traitSave.traitID;
            newT.trait = Toolbox.Instance.allCharacterTraits.Find(item => item.name == traitSave.trait);
            newT.name = newT.trait.name;
            newT.date = traitSave.date;

            if (newT.trait == CitizenControls.Instance.litterBug)
            {
                isLitterBug = true;
            }

            if (newT.trait == CitizenControls.Instance.likesTheRain)
            {
                likesTheRain = true;
            }

            characterTraits.Add(newT);
            if (newT.trait.isPassword) passwordTrait = newT.trait;
        }

        //Find reasons
        foreach (CitySaveData.CharTraitSave traitSave in data.traits)
        {
            if (traitSave.reason > -1)
            {
                Trait existingTrait = characterTraits.Find(item => item.traitID == traitSave.traitID);
                existingTrait.reason = characterTraits.Find(item => item.traitID == traitSave.reason);
            }
        }

        //Load outfits
        outfitController.outfits = new List<CitizenOutfitController.Outfit>(data.outfits);

        //passcode.debugNotePlacement = data.noteDebug;

        SetPhysicalModelParams();

        SetMaxHealth(data.maxHealth, true); //Set max health
        SetRecoveryRate(data.recoveryRate); //Set recovery
        SetCombatHeft(data.combatHeft);
        SetCombatSkill(data.combatSkill);
        SetMaxNerve(data.maxNerve, true);

        breathRecoveryRate = data.breathRecovery;
        sightingMemoryLimit = data.sightingMemory;
    }

    //Load acquaintances: This must be done after all citizens are loaded
    public void LoadAcquaintances(CitySaveData.HumanCitySave data)
    {
        //Load acquaintances
        foreach (CitySaveData.AcquaintanceCitySave aq in data.acquaintances)
        {
            Acquaintance newAq = new Acquaintance(aq);
        }
    }

    //Load favourites: This must be done after all addresses are created
    public void LoadFavourites(CitySaveData.HumanCitySave data)
    {
        //Load favourites
        for (int i = 0; i < data.favItems.Count; i++)
        {
            RetailItemPreset retailItem = Toolbox.Instance.allItems.Find(item => item.name == data.favItems[i]);
            itemRanking.Add(retailItem, data.favItemRanks[i]);
        }

        for (int i = 0; i < data.favCat.Count; i++)
        {
            NewAddress add = CityData.Instance.addressDirectory.Find(item => item.id == data.favAddresses[i]);
            favouritePlaces.Add(data.favCat[i], add);

            if (!add.favouredCustomers.Contains(this))
            {
                add.favouredCustomers.Add(this);
            }
        }

        GenerateRoutineGoals();
    }

    //Generate routine goals
    public void GenerateRoutineGoals()
    {
        List<AIGoalPreset> startingPresets = Toolbox.Instance.allGoals.FindAll(item => item.startingGoal && (item.appliesTo == AIGoalPreset.StartingGoal.all || (item.appliesTo == AIGoalPreset.StartingGoal.homelessOnly && isHomeless) || (item.appliesTo == AIGoalPreset.StartingGoal.nonHomelessOnly && !isHomeless) && (item.appliedToTheseJobs.Count <= 0 || (job != null && item.appliedToTheseJobs.Contains(job.preset)))));

        foreach (AIGoalPreset goal in startingPresets)
        {
            if (goal.onlyIfFeaturesItemsAtHome.Count > 0 && home != null)
            {
                bool pass = false;

                if(home.entrances.Count <= 0)
                {
                    Game.LogError(home.name + " features no entrances!");
                }
                else
                {
                    foreach (InteractablePreset ip in goal.onlyIfFeaturesItemsAtHome)
                    {
                        Interactable foundObj = Toolbox.Instance.FindClosestObjectTo(ip, home.entrances[0].worldAccessPoint, null, home, null, out _);

                        if (foundObj != null)
                        {
                            pass = true;
                            break;
                        }
                    }
                }

                if (!pass) continue; //Skip if none of these found...
            }

            ai.CreateNewGoal(goal, 0f, 0f);
        }

        //Create group events
        foreach (GroupsController.SocialGroup group in groups)
        {
            GroupPreset grp = null;

            if (Toolbox.Instance.groupsDictionary.TryGetValue(group.preset, out grp))
            {
                if (grp.meetUpGoal != null)
                {
                    NewAddress meetUp = null;

                    if (group.meetingPlace > -1)
                    {
                        meetUp = CityData.Instance.addressDirectory[group.meetingPlace];
                    }

                    NewAIGoal goal = ai.CreateNewGoal(grp.meetUpGoal, 0f, grp.meetUpLength, newPassedGameLocation: meetUp, newPassedGroup: group);
                    goal.name = grp.meetUpGoal.name + ": " + grp.name; //Name goal
                }
            }
        }
    }

    //Returns bool: Whether this should be added; output float priority multiplier;
    public bool TraitGoalTest(AIGoalPreset goalPreset, out float priorityMultiplier)
    {
        priorityMultiplier = 1f;

        //Check picking rules
        bool passRules = true;

        foreach (AIGoalPreset.GoalModifierRule rule in goalPreset.goalModifiers)
        {
            bool pass = false;

            if (rule.rule == CharacterTrait.RuleType.ifAnyOfThese)
            {
                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = true;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifAllOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (!characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifNoneOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifPartnerAnyOfThese)
            {
                if (partner != null)
                {
                    foreach (CharacterTrait searchTrait in rule.traitList)
                    {
                        if (partner.characterTraits.Exists(item => item.trait == searchTrait))
                        {
                            pass = true;
                            break;
                        }
                    }
                }
                else pass = false;
            }

            if (pass)
            {
                priorityMultiplier += rule.priorityMultiplier;
            }
            else if (rule.mustPassForApplication)
            {
                passRules = false;
            }
        }

        return passRules;
    }

    public override void SetVisible(bool vis, bool force = false)
    {
        if (Player.Instance == this) return; //Ignore player
        if (vis == visible) return;

        //On visible, load current outfit
        if (vis)
        {
            outfitController.LoadCurrentOutfit();

            if (updateMeshList) UpdateMeshList();
        }
        else
        {
            if(ai != null)
            {
                ai.isTripping = false;
                if(animationController != null) animationController.CancelTrip();
            }
        }

        //Do culling stuff...
        base.SetVisible(vis, force);
    }

    //Override location change
    public override void OnGameLocationChange(bool enableSocialSightings = true, bool forceDisableLocationMemory = false)
    {
        base.OnGameLocationChange();

        //Is this home?
        if (home != null && currentGameLocation == home)
        {
            isHome = true;
        }
        else isHome = false;

        //Is this at work?
        if (job != null && job.employer != null)
        {
            if (!job.preset.selfEmployed || workPosition == null)
            {
                if (currentGameLocation == job.employer.address)
                {
                    isAtWork = true;
                }
                else isAtWork = false;
            }
        }

        //Reset normal door behaviour
        if (ai != null && ai.closeDoorsNormallyAfterLeaving != null)
        {
            if (ai.closeDoorsNormallyAfterLeaving != currentGameLocation && ai.closeDoorsNormallyAfterLeaving != previousGameLocation)
            {
                ai.dontEverCloseDoors = false;
                ai.closeDoorsNormallyAfterLeaving = null;
            }
        }
    }

    public override void OnRoomChange()
    {
        base.OnRoomChange();

        //Is this home?
        if (home != null && currentRoom.gameLocation == home)
        {
            isHome = true;
        }
        else isHome = false;

        ////Is this at work?
        //if (job != null && job.employer != null)
        //{
        //    if (currentRoom.gameLocation == job.employer.address)
        //    {
        //        isAtWork = true;
        //    }
        //    else isAtWork = false;
        //}

        //Check for status flags
        bool allowEnforcersEverywhere = false;
        if (ai != null && ai.currentGoal != null) allowEnforcersEverywhere = ai.currentGoal.preset.allowEnforcersEverywhere;

        UpdateTrespassing(allowEnforcersEverywhere);

        //Adjust AI tick rate based on proximity to player
        UpdateTickRateOnProx();
    }

    //Pass on new location to AI controller
    public override void OnNodeChange()
    {
        //if(previousNode != null) previousNode.RemoveHumanTraveller(this); //Remove traveller

        //Is this at work?
        if (job != null && job.employer != null)
        {
            if (job.preset.selfEmployed && workPosition != null)
            {
                if (currentNode == workPosition.node)
                {
                    isAtWork = true;
                }
                else isAtWork = false;
            }
        }

        //Umbrella check
        if (currentGameLocation != null && animationController != null && !likesTheRain && ownsUmbrella)
        {
            if (currentGameLocation.isOutside && SessionData.Instance.currentRain > 0.1f)
            {
                animationController.SetUmbrella(true);
            }
            else if (animationController.umbrella)
            {
                animationController.SetUmbrella(false);
            }
        }

        //AI instant persuit check
        if(ai != null)
        {
            if(ai.persuitTarget != null)
            {
                ai.InstantPersuitCheck(ai.persuitTarget);
            }
        }

        base.OnNodeChange();

        //Motion trackers
        if(GameplayController.Instance.trackedNodes.Contains(currentNode))
        {
            Descriptors.BuildType build = Descriptors.BuildType.average;
            if (descriptors != null) build = descriptors.build;

            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, currentNode.gameLocation.name + ": " + Strings.Get("ui.gamemessage", "Movement Alert") + " [" + Strings.Get("descriptors", "Build", Strings.Casing.firstLetterCaptial) + ": " + Strings.Get("descriptors", build.ToString(), Strings.Casing.firstLetterCaptial) + "]", InterfaceControls.Icon.footprint, additionalSFX: AudioControls.Instance.motionTrackerPing);
        }
    }

    //Would I be trespassing in this room?
    public override bool IsTrespassing(NewRoom room, out int trespassEscalation, out string debugOutput, bool enforcersAllowedEverywhere = true)
    {
        trespassEscalation = 0;
        bool ret = false;
        debugOutput = string.Empty;
        if (room == null) return false;
        if (SessionData.Instance.isFloorEdit) return false;

        //Single out enforcers for being allowed everywhere
        if (enforcersAllowedEverywhere)
        {
            if (isEnforcer && (outfitController.currentOutfit == ClothesPreset.OutfitCategory.work || outfitController.currentOutfit == ClothesPreset.OutfitCategory.outdoorsWork))
            {
                if(Game.Instance.collectDebugData) debugOutput = "Enforcer allowed everywhere";
                return false; //Enforcers on duty
            }
        }

        if (inAirVent) return true;

        //Guest passes
        if (isPlayer && room.gameLocation.thisAsAddress != null)
        {
            if (GameplayController.Instance.guestPasses.ContainsKey(room.gameLocation.thisAsAddress))
            {
                ////Still respsect closing hours with a company...
                //if (room.gameLocation.thisAsAddress.company == null || room.gameLocation.thisAsAddress.company.openForBusinessDesired)
                //{
                    if (Game.Instance.collectDebugData) debugOutput = "Guest Pass";
                    return false;
                //}
            }

            //Player has password
            try
            {
                if (room.preset.allowedIfGivenCorrectPassword && room.gameLocation.thisAsAddress.addressPreset != null && room.gameLocation.thisAsAddress.addressPreset.needsPassword && GameplayController.Instance.playerKnowsPasswords.Contains(room.gameLocation.thisAsAddress.id))
                {
                    if (Game.Instance.collectDebugData) debugOutput = "Knows password";
                    return false;
                }
            }
            catch
            {

            }
        }

        //Trespassing @ crime scene
        //I've changed this is correspondence with being at a crime scene
        if (room.gameLocation.isCrimeScene && room.gameLocation.thisAsAddress != null)
        {
            if (isEnforcer && isOnDuty)
            {
                if (Game.Instance.collectDebugData) debugOutput = "Enforcer on duty";
                return false; //Enforcers on duty
            }

            trespassEscalation = 0;
            if (isPlayer) trespassEscalation = 2;
            if (Game.Instance.collectDebugData) debugOutput = "CrimeScene";
            return true; //Not allowed in crime scenes
        }

        //If passworded and this is AI then allow
        try
        {
            if (room.preset.AIknowPassword && !isPlayer && room.gameLocation != null && room.gameLocation.thisAsAddress != null && room.gameLocation.thisAsAddress.addressPreset != null && room.gameLocation.thisAsAddress.addressPreset.needsPassword)
            {
                if (Game.Instance.collectDebugData) debugOutput = "Allow AI with password";
                return false;
            }

        }
        catch
        {

        }

        try
        {
            if (!locationsOfAuthority.Contains(room.gameLocation))
            {
                if (room.preset.forbidden == RoomConfiguration.Forbidden.alwaysForbidden)
                {
                    if (Game.Instance.collectDebugData) debugOutput = "Always forbidden";
                    ret = true;

                    trespassEscalation = room.preset.escalationLevelNormal;

                    //Change escalation level after hours
                    if (room.preset.escalationLevelAfterHours != room.preset.escalationLevelNormal)
                    {
                        if (room.gameLocation != null && room.gameLocation.thisAsAddress != null && room.gameLocation.thisAsAddress.company != null)
                        {
                            if (!room.gameLocation.thisAsAddress.company.openForBusinessActual)
                            {
                                trespassEscalation = room.preset.escalationLevelAfterHours;
                            }
                        }
                    }

                    trespassEscalation += room.gameLocation.GetAdditionalEscalation(this);
                }
                else if (room.preset.forbidden == RoomConfiguration.Forbidden.allowedDuringOpenHours)
                {
                    if (room.gameLocation != null && room.gameLocation.thisAsAddress != null)
                    {
                        if (room.gameLocation.thisAsAddress.company != null)
                        {
                            if (job != null && job.employer == room.gameLocation.thisAsAddress.company)
                            {
                                if (Game.Instance.collectDebugData) debugOutput = "Always allow employees";
                                ret = false; //Allow employees always
                            }
                            else
                            {
                                if (room.gameLocation.thisAsAddress.company.publicFacing && room.gameLocation.thisAsAddress.company.openForBusinessActual && room.gameLocation.thisAsAddress.company.openForBusinessDesired)
                                {
                                    if (Game.Instance.collectDebugData) debugOutput = "Public facing & open";
                                    ret = false;
                                }
                                else
                                {
                                    if (Game.Instance.collectDebugData) debugOutput = "Not public facing or closed for business";
                                    ret = true;
                                }
                            }
                        }

                        //Are the open hours here dictated by an adjacent company?
                        if (room.gameLocation.thisAsAddress.addressPreset != null && room.gameLocation.thisAsAddress.addressPreset.openHoursDicatedByAdjoiningCompany)
                        {
                            if (Game.Instance.collectDebugData) debugOutput = "Open hours dictated by adjacent...";
                            ret = false; //Default to open

                            foreach (NewNode.NodeAccess acc in room.gameLocation.entrances)
                            {
                                if (!acc.walkingAccess) continue;

                                if (acc.toNode.gameLocation != room.gameLocation)
                                {
                                    if (acc.toNode.gameLocation.thisAsAddress != null && acc.toNode.gameLocation.thisAsAddress.company != null)
                                    {
                                        if (job != null && job.employer == acc.toNode.gameLocation.thisAsAddress.company)
                                        {
                                            if (Game.Instance.collectDebugData) debugOutput = "Adjacent: Always allow employees";
                                            ret = false; //Allow employees always
                                            break;
                                        }
                                        else
                                        {
                                            if (acc.toNode.gameLocation.thisAsAddress.company.publicFacing && acc.toNode.gameLocation.thisAsAddress.company.openForBusinessActual && acc.toNode.gameLocation.thisAsAddress.company.openForBusinessDesired)
                                            {
                                                if (Game.Instance.collectDebugData) debugOutput = "Adjacent: Public facing & open";
                                                ret = false;
                                                break;
                                            }
                                            else
                                            {
                                                if (Game.Instance.collectDebugData) debugOutput = "Adjacent: Not public facing or closed for business";
                                                ret = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                else if (acc.fromNode.gameLocation != room.gameLocation)
                                {
                                    if (acc.fromNode.gameLocation.thisAsAddress != null && acc.fromNode.gameLocation.thisAsAddress.company != null)
                                    {
                                        if (job != null && job.employer == acc.fromNode.gameLocation.thisAsAddress.company)
                                        {
                                            if (Game.Instance.collectDebugData) debugOutput = "Adjacent: Always allow employees";
                                            ret = false; //Allow employees always
                                            break;
                                        }
                                        else
                                        {
                                            if (acc.fromNode.gameLocation.thisAsAddress.company.publicFacing && acc.fromNode.gameLocation.thisAsAddress.company.openForBusinessActual && acc.fromNode.gameLocation.thisAsAddress.company.openForBusinessDesired)
                                            {
                                                if (Game.Instance.collectDebugData) debugOutput = "Adjacent: Public facing & open";
                                                ret = false;
                                                break;
                                            }
                                            else
                                            {
                                                if (Game.Instance.collectDebugData) debugOutput = "Adjacent: Not public facing or closed for business";
                                                ret = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else ret = true;
                    }

                    trespassEscalation = room.preset.escalationLevelNormal;

                    //Change escalation level after hours
                    if (ret)
                    {
                        if (room.preset.escalationLevelAfterHours != room.preset.escalationLevelNormal)
                        {
                            if (room.gameLocation.thisAsAddress != null && room.gameLocation.thisAsAddress.company != null)
                            {
                                ret = !room.gameLocation.thisAsAddress.company.openForBusinessActual;

                                if (ret)
                                {
                                    trespassEscalation = room.preset.escalationLevelAfterHours;
                                }
                            }
                        }

                        trespassEscalation += room.gameLocation.GetAdditionalEscalation(this);
                    }
                }
            }
            else
            {
                if (Game.Instance.collectDebugData) debugOutput = "Location of authority";
            }
        }
        catch
        {

        }

        trespassEscalation = Mathf.Clamp(trespassEscalation, 0, 2);

        return ret;
    }

    //Create acquaintances
    public void CreateAcquaintances()
    {
        //Living with lover
        if (partner != null)
        {
            AddAcquaintance(partner, Toolbox.Instance.VectorToRandomSeedContained(SocialControls.Instance.knowLoverRange, seed, out seed), Acquaintance.ConnectionType.lover, true);
        }

        //Neighbours in building
        if (home != null)
        {
            foreach (KeyValuePair<int, NewFloor> pair in home.building.floors)
            {
                foreach (NewAddress ac in pair.Value.addresses)
                {
                    if (ac.residence != null)
                    {
                        //If has residents
                        if (ac.residence.address.owners.Count > 0)
                        {
                            foreach (Human cc in ac.residence.address.owners)
                            {
                                //Skip null & this
                                if (cc == null) continue;
                                if (cc == this) continue;
                                if (cc == partner) continue;

                                //If same addres, this is a housemate
                                if (cc.home == home)
                                {
                                    AddAcquaintance(cc, Toolbox.Instance.VectorToRandomSeedContained(SocialControls.Instance.knowHousemateRange, seed, out seed), Acquaintance.ConnectionType.housemate, true);
                                    continue;
                                }
                                //Knows neighbor on same floor
                                else if (ac.floor == home.floor)
                                {
                                    AddAcquaintance(cc, Toolbox.Instance.VectorToRandomSeedContained(SocialControls.Instance.knowNeighborRange, seed, out seed), Acquaintance.ConnectionType.neighbor, true);
                                    continue;
                                }

                                //Familiar building residents: Any building residents can be familiar.Influencing factors include:
                                //Depart for work at a similar time
                                //Random
                                float familiar = (extraversion + cc.extraversion) * 0.1f;

                                if (cc.job != null)
                                {
                                    //similar start time for jobs
                                    if (Mathf.Abs(cc.job.startTimeDecimalHour - job.startTimeDecimalHour) < 1f)
                                    {
                                        for (int i = 0; i < cc.job.workDaysList.Count; i++)
                                        {
                                            //Matching work day
                                            if (job.workDaysList.Contains(cc.job.workDaysList[i]))
                                            {
                                                familiar += 0.05f;
                                            }
                                        }
                                    }
                                }
                                //Both unemployed
                                else if (cc.job == null && job == null)
                                {
                                    familiar += 0.02f;
                                }

                                if (familiar >= Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed))
                                {
                                    AddAcquaintance(cc, familiar, Acquaintance.ConnectionType.familiarResidence, false);
                                    continue;
                                }
                            }
                        }
                    }
                }
            }
        }
        //Homeless
        else
        {
            foreach (Citizen c in CityData.Instance.homelessDirectory)
            {
                AddAcquaintance(c, Toolbox.Instance.VectorToRandomSeedContained(SocialControls.Instance.knowNeighborRange, seed, out seed), Acquaintance.ConnectionType.neighbor, true);
            }
        }

        //Add work colleages
        if (job != null)
        {
            if (job.employer != null)
            {
                foreach (Occupation oc in job.employer.companyRoster)
                {
                    if (oc.employee != null)
                    {
                        if (oc.employee == this) continue;

                        float rankDifference = Mathf.Abs(job.paygrade - oc.paygrade);

                        //Will know all employees in the same team
                        if (oc == job.boss)
                        {
                            AddAcquaintance(job.boss.employee, Toolbox.Instance.VectorToRandomSeedContained(SocialControls.Instance.knowBossRange, seed, out seed), Acquaintance.ConnectionType.boss, true);
                        }
                        else if (oc.teamID == job.teamID)
                        {
                            AddAcquaintance(oc.employee, Toolbox.Instance.VectorToRandomSeedContained(SocialControls.Instance.knowWorkTeamRange, seed, out seed), Acquaintance.ConnectionType.workTeam, true);
                        }
                        else if (rankDifference < 1)
                        {
                            AddAcquaintance(oc.employee, Toolbox.Instance.VectorToRandomSeedContained(SocialControls.Instance.knowWorkRange, seed, out seed), Acquaintance.ConnectionType.workOther, true);
                        }
                        else
                        {
                            AddAcquaintance(oc.employee, Toolbox.Instance.VectorToRandomSeedContained(SocialControls.Instance.knowWorkOtherRange, seed, out seed), Acquaintance.ConnectionType.familiarWork, false);
                        }
                    }
                }

                //Add service industry regulars
                if (job.employer.preset.publicFacing && job.preset.isPublicFacing)
                {
                    foreach (KeyValuePair<CompanyPreset.CompanyCategory, NewAddress> pair in favouritePlaces)
                    {
                        if (pair.Value.company != null && pair.Value.company.publicFacing)
                        {
                            List<Occupation> publicFacingJobs = pair.Value.company.companyRoster.FindAll(item => item.preset.isPublicFacing && item.employee != null);

                            int knownCount = 0;

                            foreach (Occupation j in publicFacingJobs)
                            {
                                //Chances of knowing this person: Increased as a sum of extraversion
                                if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, (extraversion + j.employee.extraversion) * 0.5f, seed, out seed) <= 0.33f)
                                {
                                    knownCount++;
                                    AddAcquaintance(j.employee, Toolbox.Instance.VectorToRandomSeedContained(SocialControls.Instance.knowRegularCustomerRange, seed, out seed), Acquaintance.ConnectionType.regularCustomer, true);
                                }

                                //Hard limit this to knowing 16 customers...
                                if (knownCount >= 16) break;
                            }

                            Game.Log("CityGen: Public facing job " + job.preset.name + " for " + GetCitizenName() + " created " + knownCount + " customer connections");
                        }
                    }
                }
            }
        }

        //Pick 0-4 people from their past as friends
        int pastAcq = Toolbox.Instance.GetPsuedoRandomNumberContained(0, Mathf.RoundToInt(extraversion * 5), seed, out seed);

        //Minus existing friends
        List<Acquaintance> existing = acquaintances.FindAll(item => item.connections.Contains(Acquaintance.ConnectionType.friend));
        if (existing != null) pastAcq -= existing.Count;

        int failsafe = 100;

        while (pastAcq > 0 && failsafe > 0)
        {
            Citizen chosenC = CityData.Instance.citizenDirectory[Toolbox.Instance.GetPsuedoRandomNumberContained(0, CityData.Instance.citizenDirectory.Count, seed, out seed)];

            if (chosenC != this)
            {
                //Must not already be an acquaintance...
                Acquaintance randomAq;

                if (!FindAcquaintanceExists(chosenC, out randomAq))
                {
                    AddAcquaintance(chosenC, Toolbox.Instance.VectorToRandomSeedContained(SocialControls.Instance.knowFriendRange, seed, out seed), Acquaintance.ConnectionType.friend, true);
                    pastAcq--;
                }
            }

            failsafe--;
        }

        //Paramour
        //If this person has a secret lover, make it someone I already know...
        if (partner != null)
        {
            float likliness = (1f - humility + emotionality) / 2f; //Normalized

            if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) < likliness)
            {
                Acquaintance highestRank = null;
                float r = -99999f;

                //Pick someone I already know...
                foreach (Acquaintance aq in acquaintances)
                {
                    if (aq.connections.Contains(Acquaintance.ConnectionType.lover)) continue; //Can't be with existing lover!

                    //Can't have existing paramour...
                    if (aq.with.paramour != null) continue;

                    if (!attractedTo.Contains(aq.with.gender)) continue; //Must be attracted to gender...
                    if (!aq.with.attractedTo.Contains(gender)) continue; //Vice versa...

                    //Check compatilbility...
                    float rank = (1f - aq.with.humility + aq.with.emotionality) / 2f; //Normalized

                    //Add a little if this is their boss
                    if (aq.connections.Contains(Acquaintance.ConnectionType.boss)) rank += 0.1f;

                    if (rank > r)
                    {
                        highestRank = aq;
                        r = rank;
                    }
                }

                //Add paramour
                if (highestRank != null)
                {
                    Acquaintance inv = null;

                    if (highestRank.with.FindAcquaintanceExists(this, out inv))
                    {
                        //Change connection...
                        paramour = highestRank.with as Citizen;
                        highestRank.secretConnection = Acquaintance.ConnectionType.paramour;
                        highestRank.known = Toolbox.Instance.VectorToRandomSeedContained(SocialControls.Instance.knowParamourRange, seed, out seed);
                        AddCharacterTrait(SocialControls.Instance.paramour);

                        highestRank.with.paramour = this as Citizen;
                        inv.secretConnection = Acquaintance.ConnectionType.paramour;
                        inv.known = highestRank.known;
                        highestRank.with.AddCharacterTrait(SocialControls.Instance.paramour);
                    }
                }
            }
        }

        //Sort acquaintances
        acquaintances.Sort();
        acquaintances.Reverse();
    }

    //Find if an acquaintances already exists
    public bool FindAcquaintanceExists(Human findC, out Acquaintance returnAcq)
    {
        returnAcq = null;

        if (findC == null) return false;

        for (int i = 0; i < acquaintances.Count; i++)
        {
            if (acquaintances[i].from == findC || acquaintances[i].with == findC)
            {
                returnAcq = acquaintances[i];
                return true;
            }
        }

        return false;
    }

    //Add an acquaintance if it isn't in the list already...
    public void AddAcquaintance(Human addC, float known, Acquaintance.ConnectionType newConnection, bool addInverse = true, bool secretConnection = false, Acquaintance.ConnectionType newSecretConnection = Acquaintance.ConnectionType.friend, GroupsController.SocialGroup group = null)
    {
        if (addC == this)
        {
            return;
        }

        if (addC == null)
        {
            return;
        }

        if (addC.evidenceEntry == null)
        {
            return;
        }

        Acquaintance.ConnectionType secret = newConnection;

        if (secretConnection)
        {
            secret = newSecretConnection;
        }

        //Add acquaintance
        Acquaintance existing = null;

        if (!FindAcquaintanceExists(addC, out existing))
        {
            Acquaintance aq = new Acquaintance(this, addC, known, newConnection, secret, group);
        }
        else
        {
            existing.AddConnection(known, newConnection);

            if (group != null)
            {
                existing.group = group;
            }
        }

        if (addInverse)
        {
            existing = null;

            //Search for inverse
            if (!addC.FindAcquaintanceExists(this, out existing))
            {
                Acquaintance aqInv = new Acquaintance(addC, this, known, newConnection, secret, group);
            }
            else
            {
                existing.AddConnection(known, newConnection);

                if (group != null)
                {
                    existing.group = group;
                }
            }
        }
    }

    //Add detail to dictionary
    public void AddDetailToDict(string key, Fact det)
    {
        if (det == null) return;

        if (det.toEvidence == null) Game.LogError("Detail: Detail " + det.name + " has no toItem (" + det.preset.name + ")");

        if (!factDictionary.ContainsKey(key))
        {
            factDictionary.Add(key, det);
        }
    }

    //Murder!
    public void Murder(Human killer, bool setTimeOfDeath, MurderController.Murder murder, Interactable weapon, float chanceToScream = 1f)
    {
        if (isDead) return;

        if (ai != null) ai.ResetInvestigate();

        //Remove interactable
        interactableController.gameObject.tag = "Untagged";
        Destroy(interactableController);

        //Dead :(
        isDead = true;
        ai.deadRagdollTimer = 0f;
        WakeUp(true); //Not asleep any more!
        ai.SetDesiredTickRate(NewAIController.AITickRate.veryLow);

        if(Toolbox.Instance.Rand(0f, 1f) <= chanceToScream)
        {
            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.screamEvent, this, currentNode, lookAtThisTransform.position);
            speechController.Speak("38de9e2f-0d2c-4f10-ba3b-040ef3b18037", shout: true, interupt: true);
        }

        if(murder != null && weapon != null)
        {
            murder.SetMurderWeaponActual(weapon);
        }

        //Create new death class
        if (setTimeOfDeath)
        {
            death = new Death(this, murder, killer, weapon);
        }

        outfitController.SetCurrentOutfit(ClothesPreset.OutfitCategory.casual);

        //Set local position of Y level of eyes to cover body
        interactableController.lookAtTarget.localPosition = new Vector3(interactableController.lookAtTarget.localPosition.x, interactableController.lookAtTarget.localPosition.y - 1.08f, interactableController.lookAtTarget.localPosition.z);

        //Remove collider
        Destroy(interactableController.coll);

        //Add an interactable to upper torso enabling the player to check pockets
        interactableController.enabled = false;

        Transform upperTorso = outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.upperTorso);
        Interactable iUpperTorso = InteractableCreator.Instance.CreateCitizenInteractable(CitizenControls.Instance.deadBodySearchInteractable, this, upperTorso, evidenceEntry);
        InteractableController icUpperTorso = upperTorso.gameObject.AddComponent<InteractableController>();
        MeshCollider icUpperTorsoBox = upperTorso.gameObject.AddComponent<MeshCollider>();

        if (!icUpperTorsoBox.convex) icUpperTorsoBox.convex = true;

        if (icUpperTorsoBox.convex)
        {
            icUpperTorsoBox.isTrigger = true;
            icUpperTorsoBox.convex = true;
        }
        else Game.LogError("Unsupported convex collider in " + icUpperTorsoBox.name);

        icUpperTorso.coll = icUpperTorsoBox;

        icUpperTorso.Setup(iUpperTorso);
        iUpperTorso.SetPolymorphicReference(this);

        interactable = iUpperTorso;

        iUpperTorso.UpdateCurrentActions();

        //Remove from update
        if (ai != null)
        {
            ai.EnableAI(false);
            ai.enabled = false;
        }
    }

    //Remove from game world
    public void RemoveFromWorld(bool val)
    {
        if (ai != null)
        {
            ai.queuedActions.Clear();
        }

        removedFromWorld = val;
        WakeUp(true); //Not asleep any more!
        isDead = val;

        if (val)
        {
            //Move to an inacessable location
            this.transform.position = new Vector3(0, -99, 0);

            ai.EnableAI(false);
            ai.enabled = false;

            //Remove from update
            if (ai != null)
            {
                CitizenBehaviour.Instance.updateList.Remove(ai);
                CitizenBehaviour.Instance.veryHighTickRate.Remove(ai);
                CitizenBehaviour.Instance.highTickRate.Remove(ai);
                CitizenBehaviour.Instance.mediumTickRate.Remove(ai);
                CitizenBehaviour.Instance.lowTickRate.Remove(ai);
                CitizenBehaviour.Instance.veryLowTickRate.Remove(ai);
            }

            if (animationController != null) animationController.SetPauseAnimation(true);
        }
        else
        {
            //Reset to maximum health
            ResetHealthToMaximum();
            ResetNerveToMaximum();

            if (home != null)
            {
                Teleport(FindSafeTeleport(home), null);
            }
            else
            {
                Teleport(FindSafeTeleport(CityData.Instance.streetDirectory[Toolbox.Instance.Rand(0, CityData.Instance.streetDirectory.Count)]), null);
            }

            if (ai != null)
            {
                ai.EnableAI(true);
                ai.enabled = true;

                ai.SetRestrained(false, 0f);
                ai.UpdateTickRate(true);
            }

            if(animationController != null) animationController.SetPauseAnimation(false);
        }
    }

    //On wake/sleep
    public override void GoToSleep()
    {
        if (isAsleep) return;

        awakenPromt = 0; //Reset awaken promt

        //Only sleep if at home
        if (currentGameLocation != null)
        {
            if (currentGameLocation.thisAsAddress == home)
            {
                isAsleep = true;

                //Reset to maximum nerve
                ResetNerveToMaximum();

                return;
            }
        }
    }

    //If force immediate is true, it will reset the 'don't update priorities for x' on the current sleep action
    public override void WakeUp(bool forceImmediate = false)
    {
        base.WakeUp(forceImmediate);

        awakenPromt = 0; //Reset awaken promt

        //Insert the awaken goal
        if (ai != null)
        {
            //Is there an existing goal with this already? If not, create one...
            if (!ai.goals.Exists(item => item.preset == RoutineControls.Instance.awakenGoal))
            {
                NewAIGoal newAwaken = ai.CreateNewGoal(RoutineControls.Instance.awakenGoal, 0f, 0f);

                if (forceImmediate)
                {
                    NewAIGoal sleepGoal = ai.goals.Find(item => item.preset == RoutineControls.Instance.sleepGoal);

                    if (sleepGoal != null)
                    {
                        NewAIAction sleepAction = sleepGoal.actions.Find(item => item.preset == RoutineControls.Instance.sleep);

                        if (sleepAction != null)
                        {
                            sleepAction.dontUpdateGoalPriorityForExtraTime = 0f;
                        }
                    }
                }

                ai.AITick();
            }
        }

        //Speak
        if (ai != null && !isDead)
        {
            if (visible)
            {
                if(genderScale >= 0.5f)
                {
                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.maleYawn, this, currentNode, lookAtThisTransform.position);
                }
                else
                {
                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.femaleYawn, this, currentNode, lookAtThisTransform.position);
                }
            }

            //Yawn
            speechController.TriggerBark(SpeechController.Bark.yawn);
        }
    }

    //Add status stats
    public virtual void AddNourishment(float addVal)
    {
        nourishment += addVal;
        nourishment = Mathf.Clamp01(nourishment);
    }

    public virtual void AddHydration(float addVal)
    {
        hydration += addVal;
        hydration = Mathf.Clamp01(hydration);
    }

    public virtual void AddAlertness(float addVal)
    {
        alertness += addVal;
        alertness = Mathf.Clamp01(alertness);
    }

    public virtual void AddEnergy(float addVal)
    {
        energy += addVal;
        energy = Mathf.Clamp01(energy);
    }

    public virtual void AddExcitement(float addVal)
    {
        excitement += addVal;
        excitement = Mathf.Clamp01(excitement);
    }

    public virtual void AddChores(float addVal)
    {
        chores += addVal;
        chores = Mathf.Clamp01(chores);
    }

    public virtual void AddHygiene(float addVal)
    {
        hygiene += addVal;
        hygiene = Mathf.Clamp01(hygiene);
    }

    public void AddBladder(float addVal)
    {
        bladder += addVal;
        bladder = Mathf.Clamp01(bladder);
    }

    public void AddBreath(float addVal)
    {
        breath += addVal;
        breath = Mathf.Clamp01(breath);

        if(ai != null)
        {
            if(breath >= 1f && ai.outOfBreath)
            {
                ai.SetOutOfBreath(false);
            }
            else if(breath <= 0f && !ai.outOfBreath)
            {
                ai.SetOutOfBreath(true);
            }
        }
    }

    public virtual void AddHeat(float addVal)
    {
        heat += addVal;
        heat = Mathf.Clamp01(heat);
    }

    public virtual void AddDrunk(float addVal)
    {
        drunk += addVal;
        drunk = Mathf.Clamp01(drunk);
    }

    public virtual void AddSick(float addVal)
    {
        sick += addVal;
        sick = Mathf.Clamp01(sick);
    }

    public virtual void AddHeadache(float addVal)
    {
        headache += addVal;
        headache = Mathf.Clamp01(headache);
    }

    public virtual void AddWet(float addVal)
    {
        wet += addVal;
        wet = Mathf.Clamp01(wet);
    }

    public virtual void AddBrokenLeg(float addVal)
    {
        brokenLeg += addVal;
        brokenLeg = Mathf.Clamp01(brokenLeg);
    }

    public virtual void AddBruised(float addVal)
    {
        bruised += addVal;
        bruised = Mathf.Clamp01(bruised);
    }

    public virtual void AddBlackEye(float addVal)
    {
        blackEye += addVal;
        blackEye = Mathf.Clamp01(blackEye);
    }

    public virtual void AddBlackedOut(float addVal)
    {
        blackedOut += addVal;
        blackedOut = Mathf.Clamp01(blackedOut);
    }

    public virtual void AddNumb(float addVal)
    {
        numb += addVal;
        numb = Mathf.Clamp01(numb);
    }

    public virtual void AddPoisoned(float addVal, Human byWho)
    {
        if (addVal > 0f)
        {
            poisoner = byWho;

            //before applying poison, do a stat update so we don't land a giant chunk of damage here in one go...
            if (ai != null) ai.StatusStatUpdate();
        }

        poisoned += addVal;
        poisoned = Mathf.Clamp01(poisoned);
       
        if(poisoned <= 0f)
        {
            poisoner = null; //Reset if reaches 0
        }
    }

    public virtual void AddBleeding(float addVal)
    {
        bleeding += addVal;
        bleeding = Mathf.Clamp01(bleeding);
    }

    public virtual void AddBlinded(float addVal)
    {
        blinded += addVal;
        blinded = Mathf.Clamp01(blinded);
    }

    public virtual void AddStarchAddiction(float addVal)
    {
        starchAddiction += addVal;
        starchAddiction = Mathf.Clamp01(starchAddiction);
    }

    public virtual void AddWellRested(float addVal)
    {
        wellRested += addVal;
        wellRested = Mathf.Clamp01(wellRested);
    }

    public virtual void AddSyncDiskInstall(float addVal)
    {
        syncDiskInstall += addVal;
        syncDiskInstall = Mathf.Clamp01(syncDiskInstall);
    }

    public void SetAsDirector(Company newComp)
    {
        newComp.director = this;
        director = newComp;

        if (newComp.address != null)
        {
            newComp.address.AddOwner(this);
        }
    }

    public virtual void SetFootwear(ShoeType newType)
    {
        footwear = newType;

        if (footwear == ShoeType.normal)
        {
            footstepEvent = AudioControls.Instance.footstepShoe;
        }
        else if (footwear == ShoeType.boots)
        {
            footstepEvent = AudioControls.Instance.footstepBoot;
        }
        else if (footwear == ShoeType.heel)
        {
            footstepEvent = AudioControls.Instance.footstepHeel;
        }

        if (isPlayer)
        {
            //Update the footstep sound indicator
            InterfaceController.Instance.footstepAudioIndicator.SetSoundEvent(footstepEvent, true);
        }
    }

    //Called on footstep (on or offscreen)
    public void OnFootstep(bool isRight)
    {
        if (!SessionData.Instance.play || !SessionData.Instance.startedGame) return;

        AudioController.Instance.PlayWorldFootstep(footstepEvent, this, false); //For now only play if visible

        if (isOnStreet || currentGameLocation.isOutside)
        {
            footstepDirt += GameplayControls.Instance.outdoorStepDirtAccumulation;
        }

        //Apply default modifiers
        footstepDirt += GameplayControls.Instance.stepDirtRemoval;
        footstepBlood += GameplayControls.Instance.stepBloodRemoval;

        if (currentRoom != null)
        {
            //Add blood from bodies
            if (currentRoom.containsDead)
            {
                foreach (Actor a in currentRoom.currentOccupants)
                {
                    if (a.isDead && a.currentNode == currentNode)
                    {
                        float dist = Vector3.Distance(a.transform.position, this.transform.position);
                        footstepBlood += Mathf.Lerp(1f, 0.25f, dist);
                    }
                }
            }

            footstepDirt += currentRoom.floorMaterial.affectFootprintDirt; //Affect the dirt on this
            footstepDirt += currentRoom.floorMatKey.grubiness * currentRoom.floorMaterial.grubFootprintDirtMultiplier; //If dirty, affect dirt

            footstepDirt = Mathf.Clamp01(footstepDirt);
            footstepBlood = Mathf.Clamp01(footstepBlood);
            if (!currentRoom.floorMaterial.allowFootprints) return;
        }
        else
        {
            footstepDirt = Mathf.Clamp01(footstepDirt);
            footstepBlood = Mathf.Clamp01(footstepBlood);
            return;
        }

        if (footwear == ShoeType.barefoot) return;
        if (isOnStreet) return; //Don't leave footprints on street

        float strength = Mathf.Max(footstepDirt, footstepBlood);
        if (strength <= 0.1f) return;

        Vector3 pos = leftFoot.transform.position;
        if (isRight) pos = rightFoot.transform.position;

        //Stick to floor
        if (currentNode != null && !currentNode.tile.stairwell) pos.y = currentNode.position.y;

        //Game.Log("Create footprint for " + name);

        //Remove previous prints
        if (GameplayController.Instance.activeFootprints.ContainsKey(currentRoom))
        {
            //Remove when max per room is reached...
            while (GameplayController.Instance.activeFootprints[currentRoom].Count > GameplayControls.Instance.maximumFootprintsPerRoom)
            {
                GameplayController.Instance.footprintsList.Remove(GameplayController.Instance.activeFootprints[currentRoom][0]);
                GameplayController.Instance.activeFootprints[currentRoom].RemoveAt(0);
            }
        }

        GameplayController.Footprint newPrint = new GameplayController.Footprint(this, pos, this.transform.eulerAngles, footstepDirt, footstepBlood);
    }

    //These must be added before their room is decorated, otherwise they will not be placed
    public void AddPersonalAffect(InteractablePreset interactable, bool isWork = false)
    {
        if (SessionData.Instance.isFloorEdit || !CityConstructor.Instance.generateNew) return;

        if (interactable == null)
        {
            Game.LogError("Tried to add null interactable affect for " + GetCitizenName());
            return;
        }

        if (isWork)
        {
            //Make sure this citizen has a job!
            if (job != null && job.employer != null && job.employer.address != null && job.employer.address != home)
            {
                workAffects.Add(interactable);
            }
            else if (job != null && job.employer != null && (job.employer.address == null || job.employer.address == home))
            {
                personalAffects.Add(interactable);
            }
        }
        else
        {
            personalAffects.Add(interactable);
        }
    }

    //Remove personal affect
    public void RemovePersonalAffect(InteractablePreset interactable, bool isWork = false)
    {
        if (SessionData.Instance.isFloorEdit || !CityConstructor.Instance.generateNew) return;

        if (interactable == null)
        {
            Game.LogError("Tried to remove null interactable affect for " + GetCitizenName());
            return;
        }

        if (isWork)
        {
            workAffects.Remove(interactable);
        }
        else
        {
            personalAffects.Remove(interactable);
        }
    }

    //Find a safe position to teleport within this address
    public NewNode FindSafeTeleport(NewGameLocation gameLoc)
    {
        NewNode bestNode = null;
        float bestScore = -999999f;

        foreach (NewRoom room in gameLoc.rooms)
        {
            //Skip scenarios if indoors...
            if (gameLoc.thisAsStreet == null)
            {
                if (room.isNullRoom) continue; //Ignore null rooms
                if (room.preset == CityControls.Instance.outsideLayoutConfig) continue; //Ignore outside rooms
                if (room.isBaseNullRoom) continue;
                if (room.entrances.Count <= 0) continue; //Ignore rooms with no entrances
                if (!room.entrances.Exists(item => item.accessType == NewNode.NodeAccess.AccessType.door || item.accessType == NewNode.NodeAccess.AccessType.openDoorway || item.accessType == NewNode.NodeAccess.AccessType.adjacent)) continue;
            }

            if (room.nodes.Count <= 1) continue;

            float thisScore = 0f;
            NewNode bestNodeInRoom = FindSafeTeleport(room, out thisScore);

            if(thisScore > bestScore || bestNode == null)
            {
                thisScore = bestScore;
                bestNode = bestNodeInRoom;
            }
        }

        if (bestNode != null)
        {
            return bestNode;
        }
        else
        {
            if (gameLoc.nodes.Count > 0)
            {
                Game.LogError("Unable to find safe teleport for " + gameLoc + " (no valid nodes exist, so picking random from a pool of " + gameLoc.nodes.Count + ")");
                return gameLoc.nodes[Toolbox.Instance.Rand(0, gameLoc.nodes.Count)];
            }

            Game.LogError("Unable to find safe teleport for " + gameLoc + " (no nodes exist at this location!)");
            return null;
        }
    }

    //A version below for if we don't need the score
    public NewNode FindSafeTeleport(NewRoom room)
    {
        return FindSafeTeleport(room, out _);
    }

    //Find a safe position to teleport within this room
    public NewNode FindSafeTeleport(NewRoom room, out float bestScore)
    {
        NewNode bestNode = null;
        bestScore = -9999;

        foreach (NewNode node in room.nodes)
        {
            if (node.noPassThrough) continue;
            if (node.noAccess) continue;
            if (node.accessToOtherNodes.Count <= 0) continue;

            //If no floor
            if (node.room.gameLocation.thisAsStreet == null)
            {
                if (node.floorType != NewNode.FloorTileType.floorOnly && node.floorType != NewNode.FloorTileType.floorAndCeiling) continue;
            }

            float scoreThis = Toolbox.Instance.Rand(0f, 2f, true);
            bool walkingAccess = false;

            foreach (KeyValuePair<NewNode, NewNode.NodeAccess> pair in node.accessToOtherNodes)
            {
                if (pair.Value.walkingAccess)
                {
                    scoreThis += 0.1f;
                    walkingAccess = true;
                    break;
                }
            }

            if (!walkingAccess) continue; //Ignore this is there's no walking access

            //Knock points off for having a door here
            if (node.walls.Exists(item => item.door != null))
            {
                scoreThis -= 1.2f;
            }

            //Knock points off for having existing blocking furniture here
            foreach(FurnitureLocation f in node.individualFurniture)
            {
                if(f.furnitureClasses.Count > 0 && f.furnitureClasses[0].occupiesTile)
                {
                    scoreThis -= 0.5f;
                }
            }

            //Knock off points for having citizens here already
            scoreThis -= node.occupiedSpace.Count * 0.1f;

            if (scoreThis > bestScore)
            {
                bestScore = scoreThis;
                bestNode = node;
            }
        }

        if (bestNode != null)
        {
            return bestNode;
        }
        else
        {
            Game.LogError("Unable to find safe teleport for " + room + " (no nodes exist at this location!)");
            return null;
        }
    }

    //Generate item favourites: Only run on new creation
    public void GenerateItemFavs()
    {
        List<NewAddress> favCreated = new List<NewAddress>();

        //Loop through all products to rank them
        foreach (RetailItemPreset preset in Toolbox.Instance.allItems)
        {
            if (!preset.canBeFavourite) continue;
            if (preset.minimumWealth > societalClass) continue; //Item is too expensive for me

            bool passTrait = true;

            //Block these traits
            foreach (CharacterTrait blockedTrait in preset.cantFeatureTrait)
            {
                if (characterTraits.Exists(item => item.trait == blockedTrait))
                {
                    passTrait = false;
                    break;
                }
            }

            if (!passTrait) continue;

            //Must have these traits
            foreach (CharacterTrait blockedTrait in preset.mustFeatureTraits)
            {
                if (!characterTraits.Exists(item => item.trait == blockedTrait))
                {
                    passTrait = false;
                    break;
                }
            }

            if (!passTrait) continue;

            //Create ranking (higher is better)
            //Start with random preference
            int ranking = Toolbox.Instance.GetPsuedoRandomNumberContained(0, 50, seed, out seed);

            //Give a bonus for featuring these traits +20
            foreach (CharacterTrait blockedTrait in preset.preferredTraits)
            {
                if (characterTraits.Exists(item => item.trait == blockedTrait))
                {
                    ranking += 20;
                }
            }

            //Ethnicity (+10 for perfect match)
            foreach (Descriptors.EthnicitySetting group in descriptors.ethnicities)
            {
                if (preset.ethnicity.Contains(group.group))
                {
                    ranking += Mathf.RoundToInt(group.ratio * 10f);
                }
            }

            //Create dictionary entry
            itemRanking.Add(preset, ranking);
        }

        //Create favourite locations for company categories
        for (int i = 0; i < Toolbox.Instance.allCompanyCategories.Count; i++)
        {
            CompanyPreset.CompanyCategory cat = Toolbox.Instance.allCompanyCategories[i];

            //Don't create favourites for these classes:
            //if (cat == CompanyPreset.CompanyCategory. || cat == CompanyPreset.CompanyCategory.misc || cat == CompanyPreset.CompanyCategory.office || cat == CompanyPreset.CompanyCategory.chore) continue;

            //Get a list of all companies that fit this category
            List<Company> companies = CityData.Instance.companyDirectory.FindAll(item => item.preset != null && item.preset.companyCategories.Contains(cat));
            if (companies.Count <= 0) continue; //Skip if no valid companies

            Company fav = null;
            int favRank = -99999;

            foreach (Company company in companies)
            {
                //Company must be non-street
                if (company.placeOfBusiness.thisAsAddress == null) continue;
                if (!company.publicFacing) continue;

                //Rank items sold here
                int rankItems = 0;

                foreach (MenuPreset menu in company.preset.menus)
                {
                    foreach (InteractablePreset itemSold in menu.itemsSold)
                    {
                        if (itemSold.retailItem == null)
                        {
                            Game.LogError(itemSold.name + " has no retail item config...");
                        }
                        else if (itemRanking.ContainsKey(itemSold.retailItem))
                        {
                            rankItems += itemRanking[itemSold.retailItem];
                        }
                    }
                }

                if (rankItems == 0 || company.preset.menus.Count == 0) continue;

                //Make this an average so more menu items != fav
                rankItems /= company.preset.menus.Count;

                //Multiply by the index of the category so first categories carry more weight
                rankItems = Mathf.RoundToInt(rankItems * Mathf.Lerp(0.33f, 1f, (3 - company.preset.companyCategories.IndexOf(cat)) / 3f));

                //Close to home
                int distanceRankToHome = 0;

                if (home != null && company != null && company.placeOfBusiness != null && company.placeOfBusiness.nodes.Count > 0 && home.nodes.Count > 0)
                {
                    distanceRankToHome = 100 - Mathf.RoundToInt(Vector3.Distance(company.placeOfBusiness.nodes[0].position, home.nodes[0].position) * 7);
                }

                int distanceRankToWork = 0;

                if (job.employer != null && home != null && company.placeOfBusiness != null && company.placeOfBusiness.nodes.Count > 0 && home.nodes.Count > 0)
                {
                    distanceRankToWork = 100 - Mathf.RoundToInt(Vector3.Distance(company.placeOfBusiness.nodes[0].position, home.nodes[0].position) * 7);
                }

                int total = (rankItems * 2) + (distanceRankToHome * 2) + (distanceRankToWork * 2);

                //Penalty if this is my work!
                if (job.employer == company)
                {
                    total -= 1000;
                }

                //Is this a new favourite?
                if (total > favRank)
                {
                    fav = company;
                    favRank = total;
                }
            }

            //You should now have the correct favourites for the category
            if (fav != null)
            {
                favouritePlaces.Add(cat, fav.address);

                if (!fav.address.favouredCustomers.Contains(this))
                {
                    fav.address.favouredCustomers.Add(this);
                }
            }
        }
    }

    public void SpawnInventoryItems()
    {
        //Spawn starting items
        foreach (CitizenControls.StartingInventory si in CitizenControls.Instance.citizenStartingInventory)
        {
            float chance = si.baseChance;

            float traitModifier = 0f;

            if (MurderController.Instance.TraitTest(this as Citizen, ref si.modifiers, out traitModifier))
            {
                chance += traitModifier;

                if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) <= chance)
                {
                    InteractablePreset chosenItem = si.presets[Toolbox.Instance.GetPsuedoRandomNumberContained(0, si.presets.Count, seed, out seed)];
                    Interactable newItem = InteractableCreator.Instance.CreateWorldInteractable(chosenItem, this, this, null, Vector2.zero, Vector2.zero, null, null);
                    newItem.SetInInventory(this);
                }
            }
        }

        bool givenWeapon = false;

        if (job != null)
        {
            foreach (InteractablePreset p in job.preset.inventoryItems)
            {
                Interactable newItem = InteractableCreator.Instance.CreateWorldInteractable(p, this, this, null, Vector2.zero, Vector2.zero, null, null);
                newItem.SetInInventory(this);
                if (p.weapon != null) givenWeapon = true;
                //Game.Log("Creating job inventory item " + p.name + " for " + name + " job " + job.preset.name + " id " + newItem.id);
            }
        }

        //Spawn & add weapon to inventory
        //Fists aren't classed as a weapon here, so keep the chance of just having fists
        if (!givenWeapon)
        {
            List<InteractablePreset> weaponPool = new List<InteractablePreset>();

            //This represetns the chance to have no weapon
            for (int i = 0; i < 2; i++)
            {
                weaponPool.Add(null);
            }

            foreach (InteractablePreset p in Toolbox.Instance.allWeapons)
            {
                if (!p.weapon.usedInPersonalDefence || p.weapon.disabled) continue;

                if (societalClass < p.weapon.socialClassRange.x || societalClass > p.weapon.socialClassRange.y) continue;

                float chance = 0;

                if (p.weapon.personalDefenceTraitModifiers.Count > 0 && !WeaponTraitTest(this as Citizen, ref p.weapon.personalDefenceTraitModifiers, out chance))
                {
                    continue;
                }

                chance += p.weapon.citizenSpawningWithScore;

                //Add job modifier
                if (job != null && job.employer != null)
                {
                    if(p.weapon.jobModifierList.Contains(job.preset))
                    {
                        chance += p.weapon.jobScoreModifier;
                    }
                }

                for (int i = 0; i < Mathf.RoundToInt(chance); i++)
                {
                    weaponPool.Add(p);
                }
            }

            if (weaponPool.Count > 0)
            {
                InteractablePreset chosen = weaponPool[Toolbox.Instance.GetPsuedoRandomNumber(0, weaponPool.Count, GetCitizenName() + CityData.Instance.seed)];

                if(chosen != null)
                {
                    Interactable newWeapon = InteractableCreator.Instance.CreateWorldInteractable(chosen, this, this, null, Vector2.zero, Vector2.zero, null, null);
                    newWeapon.SetInInventory(this);

                    if (Game.Instance.devMode && Game.Instance.collectDebugData)
                    {
                        Game.DebugCitizenWeapons existing = Game.Instance.debugWeaponsSurvey.Find(item => item.weapon == chosen.weapon);
                        if (existing != null) existing.count++;
                        else Game.Instance.debugWeaponsSurvey.Add(new Game.DebugCitizenWeapons { weapon = chosen.weapon, count = 1 });

                        foreach(Game.DebugCitizenWeapons w in Game.Instance.debugWeaponsSurvey)
                        {
                            w.percentage = ((float)w.count / (float)CityData.Instance.citizenDirectory.Count) * 100f;
                        }
                    }
                }
                else if (Game.Instance.devMode && Game.Instance.collectDebugData)
                {
                    Game.DebugCitizenWeapons existing = Game.Instance.debugWeaponsSurvey.Find(item => item.weapon == null);
                    if (existing != null) existing.count++;
                    else Game.Instance.debugWeaponsSurvey.Add(new Game.DebugCitizenWeapons { weapon = null, count = 1 });

                    foreach (Game.DebugCitizenWeapons w in Game.Instance.debugWeaponsSurvey)
                    {
                        w.percentage = ((float)w.count / (float)CityData.Instance.citizenDirectory.Count) * 100f;
                    }
                }
            }
        }
    }

    //Returns bool: Whether this should be added; output float modifier;
    public bool WeaponTraitTest(Citizen cit, ref List<MurderPreset.MurdererModifierRule> rules, out float output)
    {
        output = 1f;

        //Check picking rules
        bool passRules = true;

        foreach (MurderPreset.MurdererModifierRule rule in rules)
        {
            bool pass = false;

            if (rule.rule == CharacterTrait.RuleType.ifAnyOfThese)
            {
                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (cit.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = true;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifAllOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (!cit.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifNoneOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (cit.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifPartnerAnyOfThese)
            {
                if (cit.partner != null)
                {
                    foreach (CharacterTrait searchTrait in rule.traitList)
                    {
                        if (cit.partner.characterTraits.Exists(item => item.trait == searchTrait))
                        {
                            pass = true;
                            break;
                        }
                    }
                }
                else pass = false;
            }

            if (pass)
            {
                output += rule.scoreModifier;
            }
            else if (rule.mustPassForApplication)
            {
                passRules = false;
            }
        }

        return passRules;
    }

    //Place favourite items at home/work
    public void PlaceFavouriteItems()
    {
        if (home == null) return;

        ////Place at least 1 clue to favourite quick food (takeaway)
        //int foodItemsAtHome = Toolbox.Instance.Rand(1, 3);

        //bool mustLinkToRestaurant = true; //At least one item must link to restaurant

        //if(!favouritePlaces.ContainsKey(CompanyPreset.CompanyCategory.foodTakeaway))
        //{
        //    Game.LogError("CityGen: Citizen " + name + " has no favourite takeaway food (" + favouritePlaces.Count + " other fav categories)");
        //    return;
        //}

        ////When did I buy this? Find a likely time range
        //float foodPurchaseTime = GetSimulatedTimeRange(favouritePlaces[CompanyPreset.CompanyCategory.foodTakeaway], 48f); //Maximum time ago for food is 48 hours.
        //List<RetailItemPreset> purchasedFood = new List<RetailItemPreset>();

        //for (int i = 0; i < foodItemsAtHome; i++)
        //{
        //    //At least 1 item must be able to be traced to that restaurant
        //    NewAddress favPlace = favouritePlaces[CompanyPreset.CompanyCategory.foodTakeaway]; //It's important to the tutorial mission this is open 24 hours.
        //    RetailItemPreset chooseFood = ChooseItemFrom(favPlace, takeawayOnly: true, mustLinkToRestaurant: mustLinkToRestaurant, tryToIgnore: purchasedFood);
        //    purchasedFood.Add(chooseFood);

        //    //Pass place of purcahse, and purchase time
        //    List<Interactable.Passed> passed = new List<Interactable.Passed>();
        //    passed.Add(new Interactable.Passed(Interactable.PassedVarType.addressID, favPlace.id));
        //    passed.Add(new Interactable.Passed(Interactable.PassedVarType.time, foodPurchaseTime));

        //    //Spawn item
        //    home.PlaceObject(chooseFood.itemPreset, this, null, out _, passed, passedObject: chooseFood);

        //    mustLinkToRestaurant = false; //Don't need to be linked to restaurant after first item is placed
        //}

        //Create sales record for the above food
        //favouritePlaces[CompanyPreset.CompanyCategory.foodTakeaway].company.AddSalesRecord(this, purchasedFood, foodPurchaseTime);
    }

    //Get a realistic time range for when a purchase could have happened
    public float GetSimulatedTimeRange(NewGameLocation where, float maxTimeAgo)
    {
        float timeAgo = Toolbox.Instance.TravelTimeEstimate(this, home.nodes[0], where.nodes[0]); //Minimum time ago = travel time to destination
        List<float> validTimes = new List<float>();

        //Use hour steps to go backwards in time. Stop when reached the max time ago, or there is no valid entries.
        while (timeAgo < maxTimeAgo || validTimes.Count <= 0)
        {
            timeAgo += 1f; //Step back an hour

            float thisTime = SessionData.Instance.gameTime - timeAgo + Toolbox.Instance.Rand(0f, 1f); //Add random variance here so it's included in recorded times

            bool pass = true;

            //Must also not already be chosen
            for (int i = 0; i < simulatedPreviousBehaviour.Count; i++)
            {
                if (Mathf.Abs(simulatedPreviousBehaviour[i] - thisTime) < 1f)
                {
                    pass = false;
                    break;
                }
            }

            if (!pass) continue;

            //Was I at work?
            bool atWork = job.IsAtWork(thisTime);

            validTimes.Add(thisTime);
        }

        float chosenEntry = validTimes[Toolbox.Instance.Rand(0, validTimes.Count)];
        simulatedPreviousBehaviour.Add(chosenEntry);
        return chosenEntry;
    }

    //Write a note...
    public Interactable WriteNote(NoteObject newPresetType, string treeID, Human reciever, NewGameLocation placement, int security = 0, InteractablePreset.OwnedPlacementRule ownershipPlacement = InteractablePreset.OwnedPlacementRule.both, int priority = 1, HashSet<NewRoom> dontPlaceInRooms = null, bool printDebug = false, int toneFriendly = 0, int toneFormal = 0, string loadGUID = null)
    {
        List<NoteObject> newPresets = (new NoteObject[] { newPresetType }).ToList();
        return WriteNote(newPresets, treeID, reciever, placement, security, ownershipPlacement, priority, dontPlaceInRooms, printDebug, toneFriendly, toneFormal, loadGUID);
    }

    public Interactable WriteNote(List<NoteObject> newPresetType, string treeID, Human reciever, NewGameLocation placement, int security = 0, InteractablePreset.OwnedPlacementRule ownershipPlacement = InteractablePreset.OwnedPlacementRule.both, int priority = 1, HashSet<NewRoom> dontPlaceInRooms = null, bool printDebug = false, int toneFriendly = 0, int toneFormal = 0, string loadGUID = null)
    {
        Interactable created = null;

        //Run through all possible types, so if one fails then another should succeed...
        while (created == null && newPresetType.Count > 0)
        {
            NoteObject presetType = newPresetType[Toolbox.Instance.Rand(0, newPresetType.Count)];
            InteractablePreset presetT = InteriorControls.Instance.note;

            if (presetType == NoteObject.letter)
            {
                presetT = InteriorControls.Instance.letter;
            }
            else if (presetType == NoteObject.travelReceipt)
            {
                presetT = InteriorControls.Instance.travelReceipt;
            }
            else if (presetType == NoteObject.vmailLetter)
            {
                presetT = InteriorControls.Instance.vmailLetter;
            }

            //Attempt to place object of this type...
            created = placement.PlaceObject(presetT, reciever, this, reciever, out _, forceSecuritySettings: true, forcedSecurity: security, forcedOwnership: ownershipPlacement, passedVars: null, forcedPriority: priority, printDebug: printDebug, dontPlaceInRooms: dontPlaceInRooms, loadGUID: loadGUID, ddsOverride: treeID);

            newPresetType.Remove(presetType);
        }

        //Override tree ID with this
        //if (created != null)
        //{
        //    created.SetDDSOverride(treeID);
        //}

        return created;
    }

    public CitySaveData.HumanCitySave GenerateSaveData()
    {
        CitySaveData.HumanCitySave output = new CitySaveData.HumanCitySave();

        output.humanID = humanID;

        if (home != null)
        {
            output.home = home.id;
            output.debugHome = home.name;
        }

        output.speedModifier = speedMultiplier;

        output.job = job.id;
        if (job.employer == null && !job.preset.isCriminal) output.job = 0;

        output.societalClass = societalClass; //Rank from 0 - 1, 0 being working class, 1 being upper class. Based on job paygrade
        output.blood = bloodType;
        output.descriptors = descriptors;
        output.citizenName = citizenName;
        output.casualName = casualName;
        output.firstName = firstName;
        output.surName = surName;
        //output.initialledName = initialledName;
        output.genderScale = genderScale;
        output.gender = gender;
        output.bGender = birthGender;
        output.attractedTo = attractedTo;
        output.homosexuality = homosexuality;
        output.sexuality = sexuality;
        if (partner != null) output.partner = partner.humanID;
        if (paramour != null) output.paramour = paramour.humanID;
        output.sleepNeedMultiplier = sleepNeedMultiplier; //Criminals will need more spare time, so need less sleep
        output.snoring = snoring; //Amount this person snores in their sleep
        output.snoreDelay = snoreDelay; //Time between snores in seconds
        output.humility = humility;
        output.emotionality = emotionality;
        output.extraversion = extraversion;
        output.agreeableness = agreeableness;
        output.conscientiousness = conscientiousness;
        output.creativity = creativity;
        output.birthday = birthday;
        output.password = passcode;
        output.maxHealth = maximumHealth;
        output.recoveryRate = recoveryRate;
        output.combatHeft = combatHeft;
        output.combatSkill = combatSkill;
        output.maxNerve = maxNerve;
        output.homeless = isHomeless;
        output.breathRecovery = breathRecoveryRate;
        output.sightingMemory = sightingMemoryLimit;

        if (handwriting != null) output.handwriting = handwriting.name;

        output.slangUsage = slangUsage;
        //output.slangDefault = slangGreetingDefault;
        //output.slangMale = slangGreetingMale;
        //output.slangFemale = slangGreetingFemale;
        //output.slangLover = slangGreetingLover;
        //output.slangCurse = slangCurse;
        //output.slangCurseNoun = slangCurseNoun;
        //output.slandPraiseNoun = slangPraiseNoun;

        output.anniversary = anniversary;

        //Add acquaintance to save state
        foreach (Acquaintance aq in acquaintances)
        {
            output.acquaintances.Add(aq.GenerateSaveData()); //Save id reference
        }

        //Add traits to save state
        foreach (Trait tr in characterTraits)
        {
            CitySaveData.CharTraitSave newTr = new CitySaveData.CharTraitSave();
            newTr.traitID = tr.traitID;
            newTr.trait = tr.trait.name;
            newTr.date = tr.date;
            if (tr.reason != null) newTr.reason = tr.reason.traitID;
            output.traits.Add(newTr);
        }

        //Save favourites
        foreach (KeyValuePair<RetailItemPreset, int> pair in itemRanking)
        {
            output.favItems.Add(pair.Key.name);
            output.favItemRanks.Add(pair.Value);
        }

        foreach (KeyValuePair<CompanyPreset.CompanyCategory, NewAddress> pair in favouritePlaces)
        {
            output.favCat.Add(pair.Key);
            output.favAddresses.Add(pair.Value.id);
        }

        //Save outfits
        output.outfits = new List<CitizenOutfitController.Outfit>(outfitController.outfits);
        output.favCol = favColourIndex;

        return output;
    }

    //Compare by job then society class
    public int CompareTo(Human comp)
    {
        float thisRank = societalClass;

        if (job != null)
        {
            if (job.employer != null)
            {
                thisRank += job.preset.jobFillPriority;
            }
        }

        float otherRank = comp.societalClass;

        if (comp.job != null)
        {
            if (comp.job.employer != null)
            {
                otherRank += comp.job.preset.jobFillPriority;
            }
        }

        return thisRank.CompareTo(otherRank);
    }

    //Execute speech from within vocab
    public void SpeechTriggerPoint(DDSSaveClasses.TriggerPoint triggerPoint, Actor trackedTarget, AIActionPreset onAction = null)
    {
        //SelectedDebug("Speech trigger point: " + triggerPoint);
        if (currentCityTile == null) return;

        if ((CityConstructor.Instance != null && CityConstructor.Instance.preSimActive) || !SessionData.Instance.play) return;

        //Only do this if in player's vicinity...
        if (!currentCityTile.isInPlayerVicinity)
        {
            if (Game.Instance.collectDebugData) SelectedDebug("... Not in player's vicinity", HumanDebug.misc);
            return;
        }

        //If already in conversation, this is null
        if (inConversation)
        {
            if (Game.Instance.collectDebugData) SelectedDebug("... In existing conversation", HumanDebug.misc);
            return;
        }

        ////Casual speech delay
        if (nextCasualSpeechValidAt > SessionData.Instance.gameTime)
        {
            //SelectedDebug("... Next casual speech at " + nextCasualSpeechValidAt + " it's currently " + SessionData.Instance.gameTime);
            return;
        }

        ////Only 1 conversation per room at a time
        if (currentRoom.activeConversations.Count > 0)
        {
            //SelectedDebug("... Conversation active in room already");
            return;
        }

        if (InterfaceController.Instance.activeSpeechBubbles.Count > CitizenControls.Instance.maxSpeechBubbles)
        {
            return;
        }

        if (InteractionController.Instance.talkingTo == interactable) return;

        if (currentGameLocation != null && currentGameLocation.telephones.Count > 0)
        {
            if (currentGameLocation.telephones.Exists(item => item.activeReceiver == this)) return; //Citizen is on the phone
        }

        if (ai != null)
        {
            if (ai.inCombat) return;
            if (isRunning) return;

            //Disable this during action
            if (ai.currentAction != null)
            {
                if (ai.currentAction.preset.disableConversationTriggers) return;
                if (ai.currentAction.preset == RoutineControls.Instance.answerTelephone) return; //Citizen is answering the phone
            }
        }

        //Speech trigger point
        if (dds.ContainsKey(triggerPoint))
        {
            //SelectedDebug("Checking vocab for trigger point " + triggerPoint +"...");

            //Game.Log("Vocab trigger...");
            List<DDSSaveClasses.DDSTreeSave> validSpeech = new List<DDSSaveClasses.DDSTreeSave>();
            List<List<Human>> validParticipants = new List<List<Human>>();

            foreach (DDSSaveClasses.DDSTreeSave sp in dds[triggerPoint])
            {
                //Global delay
                if(!sp.ignoreGlobalRepeat)
                {
                    if (GameplayController.Instance.globalConversationDelay.ContainsKey(sp.id)) continue;
                }

                //Overall chance
                if (Toolbox.Instance.Rand(0f, 1f) > sp.treeChance * 0.1f) continue; //Use multiplier to control general chatter amount

                bool pass = true;

                //Check history...
                List<SpeechHistory> existingHistory = null;

                if(sp.repeat != DDSSaveClasses.RepeatSetting.noLimit)
                {
                    if (speechHistory.TryGetValue(sp, out existingHistory))
                    {
                        //Only allow repeats with different other participants
                        if (sp.repeat == DDSSaveClasses.RepeatSetting.never)
                        {
                            continue; //Automatic fail if set to never repeat
                        }

                        float repeatAllowedAfter = 1f;
                        if (sp.repeat == DDSSaveClasses.RepeatSetting.sixHours) repeatAllowedAfter = 6;
                        else if (sp.repeat == DDSSaveClasses.RepeatSetting.twelveHours) repeatAllowedAfter = 12;
                        else if (sp.repeat == DDSSaveClasses.RepeatSetting.oneDay) repeatAllowedAfter = 24;
                        else if (sp.repeat == DDSSaveClasses.RepeatSetting.twoDays) repeatAllowedAfter = 48;
                        else if (sp.repeat == DDSSaveClasses.RepeatSetting.threeDays) repeatAllowedAfter = 72;
                        else if (sp.repeat == DDSSaveClasses.RepeatSetting.oneWeek) repeatAllowedAfter = 168;

                        //Check timestamps
                        for (int i = 0; i < existingHistory.Count; i++)
                        {
                            SpeechHistory h = existingHistory[i];

                            if (sp.repeat != DDSSaveClasses.RepeatSetting.never)
                            {
                                if (SessionData.Instance.gameTime > h.timeStamp + repeatAllowedAfter)
                                {
                                    existingHistory.RemoveAt(i);
                                    i--;
                                    continue;
                                }
                            }
                        }

                        //If history still exists, deny this
                        if (existingHistory.Count > 0)
                        {
                            pass = false;
                        }
                    }
                }


                if (!pass) continue; //Invalid history

                //Attempt to match conditions for participants
                List<Human> otherParticipants = new List<Human>();

                for (int i = 0; i < 4; i++)
                {
                    //Participant A is always the initiator, aka this human
                    if (i == 0)
                    {
                        bool self = DDSParticipantConditionCheck(this, sp.participantA, sp.treeType);
                        if (!self)
                        {
                            pass = false;
                            break;
                        }
                    }
                    else
                    {
                        DDSSaveClasses.DDSParticipant participant = sp.participantB;
                        if (i == 2) participant = sp.participantC;
                        if (i == 3) participant = sp.participantD;

                        if (participant.required)
                        {
                            if (trackedTarget != null)
                            {
                                Human humanTarget = trackedTarget as Human;

                                if (humanTarget != null && !otherParticipants.Contains(humanTarget) && humanTarget != this)
                                {
                                    if (humanTarget.DDSParticipantConditionCheck(this, participant, sp.treeType))
                                    {
                                        otherParticipants.Add(humanTarget);
                                        continue;
                                    }
                                }
                            }

                            bool foundInRoom = false;

                            //Search the room for participants
                            foreach (Actor act in currentRoom.currentOccupants)
                            {
                                Human humanRoom = act as Human;

                                if (humanRoom != null)
                                {
                                    //Can't already be in the conversation, or myself
                                    if (!otherParticipants.Contains(humanRoom) && humanRoom != this)
                                    {
                                        if (humanRoom.DDSParticipantConditionCheck(this, participant, sp.treeType))
                                        {
                                            otherParticipants.Add(humanRoom);
                                            foundInRoom = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            //If this point is reached, we can't find actors to complete the required for the tree...
                            if (!foundInRoom)
                            {
                                pass = false;
                                break;
                            }
                        }
                        else continue; //Continued if we don't need this participant
                    }
                }

                if (!pass) continue; //If invalid participants, continue scanning for usable trees...

                //If this point is reached, the speech preset is valid...
                validSpeech.Add(sp);
                validParticipants.Add(otherParticipants);
            }

            if (validSpeech.Count > 0)
            {
                //SelectedDebug("... " + validSpeech.Count + " valid speech entries...");

                //Rank by priority
                validSpeech.Sort((p1, p2) => p2.priority.CompareTo(p1.priority)); //Using P2 first gives highest first

                //Execute
                ExecuteConversationTree(validSpeech[0], validParticipants[0]);
            }
            else
            {
                //SelectedDebug("... No valid speech in DDS Vocab");
            }
        }
    }

    public bool DDSParticipantConditionCheck(Human initiator, DDSSaveClasses.DDSParticipant conditions, DDSSaveClasses.TreeType treeType)
    {
        if (ai != null)
        {
            if (isRunning) return false;
            if (ai.inCombat) return false;

            if (ai.currentAction != null)
            {
                if (ai.currentAction.preset.disableConversationTriggers) return false;
            }
        }

        if (InteractionController.Instance.talkingTo == interactable) return false;

        if (currentGameLocation != null && currentGameLocation.telephones.Count > 0)
        {
            if (currentGameLocation.telephones.Exists(item => item.activeReceiver == this)) return false; //Citizen is on the phone
        }

        if (ai != null)
        {
            if (isRunning) return false;
            if (ai.currentAction != null && ai.currentAction.preset == RoutineControls.Instance.answerTelephone) return false; //Citizen is answering the phone
            if (ai.inCombat) return false;
        }

        //Already in conversation?
        if (treeType == DDSSaveClasses.TreeType.conversation)
        {
            if (inConversation)
            {
                return false;
            }

            //Don't do this if standing in doorway
            if (currentNode == null || currentNode.isIndoorsEntrance)
            {
                return false;
            }

            //Don't do this if fleeing
            if (ai != null)
            {
                if (ai.currentGoal != null)
                {
                    if (ai.currentGoal.preset == RoutineControls.Instance.fleeGoal)
                    {
                        return false;
                    }
                }
            }

            //Same room?
            if (currentRoom != initiator.currentRoom)
            {
                return false;
            }

            //Within range?
            if (initiator != this)
            {
                float distance = Vector3.Distance(initiator.transform.position, this.transform.position);

                if (distance > 3.5f || distance < 1.25f)
                {
                    return false;
                }
            }
        }

        //This will be needed later
        Acquaintance aq = null;

        bool pass = false;

        //Only do connection requirement if this is not the initiator
        if (initiator != this)
        {
            //Connection requirement
            //If anyone, then automatic pass
            if (conditions.connection == Acquaintance.ConnectionType.anyone)
            {
                pass = true;
            }
            else if (conditions.connection == Acquaintance.ConnectionType.anyoneNotPlayer && !isPlayer)
            {
                pass = true;
            }
            //If player
            else if (conditions.connection == Acquaintance.ConnectionType.player && isPlayer)
            {
                pass = true;
            }
            //Otherwise have some kind of connection
            else
            {
                //Do I know this person?
                if (initiator.FindAcquaintanceExists(this, out aq))
                {
                    //Does connection match?
                    if(aq.with.isPlayer)
                    {
                        pass = false;
                    }
                    else if (aq.connections.Contains(conditions.connection) || conditions.connection == aq.secretConnection)
                    {
                        pass = true;
                    }
                    else if (conditions.connection == Acquaintance.ConnectionType.anyAcquaintance)
                    {
                        pass = true;
                    }
                    else if (conditions.connection == Acquaintance.ConnectionType.friendOrWork)
                    {
                        if (aq.connections.Contains(Acquaintance.ConnectionType.friend) || aq.connections.Contains(Acquaintance.ConnectionType.workTeam) || aq.connections.Contains(Acquaintance.ConnectionType.workOther) || aq.connections.Contains(Acquaintance.ConnectionType.familiarWork) || aq.connections.Contains(Acquaintance.ConnectionType.paramour))
                        {
                            pass = true;
                        }
                    }
                    else if (conditions.connection == Acquaintance.ConnectionType.knowsName)
                    {
                        if (aq.dataKeys.Contains(Evidence.DataKey.name))
                        {
                            pass = true;
                        }
                    }
                }
                else
                {
                    //Only pass in this case if we're looking for a stranger
                    if (conditions.connection == Acquaintance.ConnectionType.stranger)
                    {
                        pass = true;
                    }
                }
            }

            if (!pass) return false;

            //Now check against jobs and traits...
            if (conditions.useJobs)
            {
                if (job.preset == null || !conditions.jobs.Contains(job.preset.name))
                {
                    return false;
                }
            }

            //Check against traits
            if (conditions.useTraits)
            {
                //Get aq if null
                if (aq == null)
                {
                    initiator.FindAcquaintanceExists(this, out aq);
                }

                pass = Toolbox.Instance.DDSTraitConditionLogicAcquaintance(this, aq, conditions.traitConditions, ref conditions.traits);
            }

            if (!pass) return false;
        }
        //If initiator, do a pass for this section
        else pass = true;

        //Now check triggers: Do this for all participants...
        foreach (DDSSaveClasses.TreeTriggers trigger in conditions.triggers)
        {
            if (trigger == DDSSaveClasses.TreeTriggers.lightOnAny)
            {
                if (!currentRoom.mainLightStatus && !currentRoom.secondaryLightStatus) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.lightOnMain)
            {
                if (!currentRoom.mainLightStatus) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.allLightsOff)
            {
                if (currentRoom.mainLightStatus || currentRoom.secondaryLightStatus) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.asleep)
            {
                if (!isAsleep) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.awake)
            {
                if (isAsleep || isDead || isStunned) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.dead)
            {
                if (!isDead) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.atHome)
            {
                if (!isHome) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.atWork)
            {
                if (!isAtWork) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.investigatingSound)
            {
                if (ai != null)
                {
                    if (ai.reactionState != NewAIController.ReactionState.investigatingSound) return false;
                }
                else if (!isPlayer) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.persuing)
            {
                if (ai != null)
                {
                    if (ai.reactionState != NewAIController.ReactionState.persuing) return false;
                }
                else if (!isPlayer) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.searching)
            {
                if (ai != null)
                {
                    if (ai.reactionState != NewAIController.ReactionState.searching) return false;
                }
                else if (!isPlayer) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.travelling)
            {
                if (!isMoving) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.carrying)
            {
                if (isPlayer)
                {
                    if (InteractionController.Instance.carryingObject == null)
                    {
                        return false;
                    }
                }
                else return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.notCarrying)
            {
                if (isPlayer)
                {
                    if (InteractionController.Instance.carryingObject != null)
                    {
                        return false;
                    }
                }
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.employee)
            {
                //Match initiator's employment
                if (job.employer != initiator.job.employer)
                {
                    return false;
                }
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.nonEmployee)
            {
                if (job.employer == initiator.job.employer)
                {
                    return false;
                }
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.illegal)
            {
                if (isPlayer)
                {
                    if (!Player.Instance.illegalStatus)
                    {
                        return false;
                    }
                }
                else if (!isTrespassing)
                {
                    return false;
                }
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.legal)
            {
                if (isPlayer)
                {
                    if (Player.Instance.illegalStatus)
                    {
                        return false;
                    }
                }
                else if (isTrespassing)
                {
                    return false;
                }
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.inCombat)
            {
                if (ai != null)
                {
                    if (!ai.inCombat) return false;
                }
                else return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.notInCombat)
            {
                if (ai != null)
                {
                    if (ai.inCombat) return false;
                }
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.trespassing)
            {
                return IsTrespassing(currentRoom, out _, out _);
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.locationOfAuthority)
            {
                return locationsOfAuthority.Contains(currentGameLocation);
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.drunk)
            {
                if (drunk > 0.75f) return true;
                else return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.sober)
            {
                if (drunk <= 0.75f) return true;
                else return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.onStreet)
            {
                if (currentGameLocation.thisAsStreet == null) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.indoors)
            {
                if (currentGameLocation.thisAsAddress == null) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.privateLocation)
            {
                if (currentRoom.preset.forbidden == RoomConfiguration.Forbidden.alwaysAllowed) return false;
                else if (currentRoom.preset.forbidden == RoomConfiguration.Forbidden.allowedDuringOpenHours)
                {
                    return !currentRoom.IsAccessAllowed(this);
                }
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.publicLocation)
            {
                if (currentRoom.preset.forbidden == RoomConfiguration.Forbidden.alwaysForbidden) return false;
                else if (currentRoom.preset.forbidden == RoomConfiguration.Forbidden.allowedDuringOpenHours)
                {
                    return currentRoom.IsAccessAllowed(this);
                }
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.sat)
            {
                if (animationController.idleAnimationState != CitizenAnimationController.IdleAnimationState.sitting) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.noReactionState)
            {
                if (ai != null)
                {
                    if (ai.reactionState != NewAIController.ReactionState.none) return false;
                    if (ai.restrained) return false;
                }
                else if (!isPlayer) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.investigating)
            {
                if (ai != null)
                {
                    if (ai.reactionState != NewAIController.ReactionState.investigatingSight && ai.reactionState != NewAIController.ReactionState.investigatingSound) return false;
                }
                else if (!isPlayer) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.investigatingVisual)
            {
                if (ai != null)
                {
                    if (ai.reactionState != NewAIController.ReactionState.investigatingSight) return false;
                }
                else if (!isPlayer) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.unconscious)
            {
                if (!isStunned) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.rain)
            {
                if (SessionData.Instance.currentRain < 0.5f) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.brokenSign)
            {
                if (currentRoom.gameLocation.thisAsAddress != null)
                {
                    if (!currentRoom.gameLocation.thisAsAddress.featuresBrokenSign)
                    {
                        return false;
                    }
                }
                else return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.travellingToWork)
            {
                if (ai != null)
                {
                    if (ai.currentGoal.preset == RoutineControls.Instance.workGoal)
                    {
                        if (currentGameLocation == job.employer.placeOfBusiness)
                        {
                            return false;
                        }
                    }
                    else return false;
                }
                else if (!isPlayer) return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.atEatery)
            {
                if (currentRoom.gameLocation.thisAsAddress != null)
                {
                    if (currentRoom.gameLocation.thisAsAddress.company != null)
                    {
                        if (currentRoom.gameLocation.thisAsAddress.company.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.meal) || currentRoom.gameLocation.thisAsAddress.company.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.snack) || currentRoom.gameLocation.thisAsAddress.company.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.caffeine))
                        {
                            if (isAtWork)
                            {
                                return false;
                            }
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.enforcerOnDuty)
            {
                if (!isEnforcer || !isOnDuty)
                {
                    return false;
                }
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.notEnforcerOnDuty)
            {
                if (isEnforcer && isOnDuty)
                {
                    return false;
                }
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.hasJob)
            {
                if (job == null || job.employer == null)
                {
                    return false;
                }
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.unemployed)
            {
                if (job != null && job.employer != null)
                {
                    return false;
                }
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.homeIntenseWallpaper)
            {
                if (home != null)
                {
                    bool found = false;

                    foreach (NewRoom r in home.rooms)
                    {
                        try
                        {
                            if (r.colourScheme.loudness > 8)
                            {
                                found = true;
                                break;
                            }
                        }
                        catch
                        {

                        }
                    }

                    if (!found) return false;
                }
            }
            else if(trigger == DDSSaveClasses.TreeTriggers.restrained)
            {
                if (ai != null)
                {
                    return ai.restrained;
                }
                else return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.hasRoomAtHotel)
            {
                if(currentGameLocation != null && currentGameLocation.thisAsAddress != null && currentGameLocation.thisAsAddress.company != null && currentGameLocation.thisAsAddress.company.preset != null && currentGameLocation.thisAsAddress.company.preset.isHotel)
                {
                    GameplayController.HotelGuest guest = Toolbox.Instance.GetHotelRoom(this);

                    if(guest != null)
                    {
                        if (SessionData.Instance.gameTime < guest.nextPayment)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.hasNoRoomAtHotel)
            {
                if (currentGameLocation != null && currentGameLocation.thisAsAddress != null && currentGameLocation.thisAsAddress.company != null && currentGameLocation.thisAsAddress.company.preset != null && currentGameLocation.thisAsAddress.company.preset.isHotel)
                {
                    GameplayController.HotelGuest guest = Toolbox.Instance.GetHotelRoom(this);

                    if (guest != null)
                    {
                        return false;
                    }
                }

                return true;
            }
            else if (trigger == DDSSaveClasses.TreeTriggers.hotelPaymentDue)
            {
                if (currentGameLocation != null && currentGameLocation.thisAsAddress != null && currentGameLocation.thisAsAddress.company != null && currentGameLocation.thisAsAddress.company.preset != null && currentGameLocation.thisAsAddress.company.preset.isHotel)
                {
                    GameplayController.HotelGuest guest = Toolbox.Instance.GetHotelRoom(this);

                    if (guest != null)
                    {
                        if(SessionData.Instance.gameTime >= guest.lastPayment + 24)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        }

        //If we've reached here, this is true
        return true;
    }

    public void ExecuteConversationTree(DDSSaveClasses.DDSTreeSave newTree, List<Human> otherParticipants)
    {
        if (newTree == null || otherParticipants == null || otherParticipants.Count <= 0)
        {
            Game.LogError("Invalid conversation tree!");
        }

        if(!newTree.ignoreGlobalRepeat && !GameplayController.Instance.globalConversationDelay.ContainsKey(newTree.id))
        {
            GameplayController.Instance.globalConversationDelay.Add(newTree.id, SessionData.Instance.gameTime);
        }

        if (Game.Instance.collectDebugData) SelectedDebug("Execute speech " + newTree.name, HumanDebug.misc);

        //Casual speech delay
        nextCasualSpeechValidAt = SessionData.Instance.gameTime + 0.1f;

        //Add to history
        if (!speechHistory.ContainsKey(newTree))
        {
            speechHistory.Add(newTree, new List<SpeechHistory>());
        }

        SpeechHistory newHistoryElement = new SpeechHistory { timeStamp = SessionData.Instance.gameTime };
        newHistoryElement.participants.Add(this);
        newHistoryElement.participants.AddRange(otherParticipants);

        speechHistory[newTree].Add(newHistoryElement);

        //Also add this to other participants...
        foreach (Human p in otherParticipants)
        {
            if (p == null)
            {
                Game.LogError("Null conversation participant!");
            }

            if (!p.speechHistory.ContainsKey(newTree))
            {
                p.speechHistory.Add(newTree, new List<SpeechHistory>());
            }

            p.speechHistory[newTree].Add(newHistoryElement);
        }

        //Create conversation instance
        ConversationInstance newConversation = new ConversationInstance { tree = newTree, participantA = this };
        if (otherParticipants.Count >= 1) newConversation.participantB = otherParticipants[0];
        if (otherParticipants.Count >= 2) newConversation.participantC = otherParticipants[1];
        if (otherParticipants.Count >= 3) newConversation.participantD = otherParticipants[2];
        newConversation.active = true;

        //Add to active conversations in this room
        newConversation.room = currentRoom;
        currentRoom.activeConversations.Add(newConversation);

        //For now use initiator as person talking so it is not null
        newConversation.currentlyTalking = this;

        SetInConversation(newConversation);

        foreach (Human oth in otherParticipants)
        {
            oth.SetInConversation(newConversation);
        }
    }

    public virtual void SetInConversation(ConversationInstance newInstance, bool endCall = true)
    {
        //Game.Log(name + ": Set in conversation: " + newInstance + " " + currentGameLocation.name + " end call: " + endCall);

        if (newInstance != null)
        {
            if (Game.Instance.collectDebugData) SelectedDebug("Set in conversation: " + newInstance.tree.name + " " + newInstance.tree.id, HumanDebug.misc);

            if(isPlayer)
            {
                Game.Log("Player: Player is in conversation! Should this be happening: " + newInstance.tree.name);
            }

            newInstance.treeName = newInstance.tree.name;

            currentConversation = newInstance;
            inConversation = true;

            if (ai != null)
            {
                ai.SetUpdateEnabled(true); //Make sure AI is enabled for conversation

                //Cancel any dumb hand movements
                if (animationController != null)
                {
                    if (animationController.armsBoolAnimationState == CitizenAnimationController.ArmsBoolSate.armsUse || animationController.armsBoolAnimationState == CitizenAnimationController.ArmsBoolSate.armsLocking)
                    {
                        animationController.SetArmsBoolState(CitizenAnimationController.ArmsBoolSate.none);
                    }
                }
            }
        }
        else
        {
            currentConversation = null;
            inConversation = false;

            //End telephone call...
            if (currentGameLocation.telephones.Count > 0 && endCall)
            {
                Telephone currentPhone = currentGameLocation.telephones.Find(item => item.activeReceiver == this);

                if (currentPhone != null)
                {
                    if (currentPhone.activeCall.Count > 0)
                    {
                        currentPhone.activeCall[0].EndCall();
                    }
                }
            }
        }

        debugConversation = newInstance;

        //Update node space pos
        UpdateCurrentNodeSpace();
    }

    public List<string> ParseDDSMessage(DDSSaveClasses.DDSMessageSettings settings, Acquaintance aq, object passedObject = null)
    {
        return ParseDDSMessage(settings.msgID, aq, out _, passedObject: passedObject);
    }

    //Same as below but input values for informal/forman and for hostile/friendly
    public List<string> ParseDDSMessage(string msgID, Acquaintance aq, out List<int> outputDisplayGroups, bool forceRealRandom = false, object passedObject = null, bool debug = false)
    {
        List<string> output = new List<string>(); //Text output
        outputDisplayGroups = new List<int>(); //Output which groups the above text was assigned to in the DDS editor

        //Get the actual message...
        DDSSaveClasses.DDSMessageSave msg = null;

        string seed;

        if (forceRealRandom)
        {
            seed = Toolbox.Instance.GenerateSeed(16);
        }
        else seed = humanID + msgID;

        if (Toolbox.Instance.allDDSMessages.TryGetValue(msgID, out msg))
        {
            List<DDSSaveClasses.DDSBlockSave> validBlocks = new List<DDSSaveClasses.DDSBlockSave>();

            List<int> displayedGroup = new List<int>(); //Display only one of a group

            //Run through blocks
            for (int i = 0; i < msg.blocks.Count; i++)
            {
                DDSSaveClasses.DDSBlockCondition conditions = msg.blocks[i];

                if (debug) Game.Log("DDS: Parse debug: Checking validity of block " + conditions.blockID + "...");

                if (!Toolbox.Instance.allDDSBlocks.ContainsKey(conditions.blockID))
                {
                    Game.LogError("Missing Block! " + conditions.blockID + " for message " + msgID + " (" + msg.name + ")");
                }
                else
                {
                    if (conditions.alwaysDisplay)
                    {
                        if (debug) Game.Log("DDS: Parse debug: .... Always display block");
                        validBlocks.Add(Toolbox.Instance.allDDSBlocks[conditions.blockID]);
                        outputDisplayGroups.Add((int)conditions.group);
                    }
                    //Else check block conditions
                    else
                    {
                        //Display one of a group...
                        if (conditions.group > 0)
                        {
                            if (debug) Game.Log("DDS: Parse debug: .... Block uses display group " + conditions.group);

                            if (displayedGroup.Contains(conditions.group))
                            {
                                if (debug) Game.Log("DDS: Parse debug: .... Display group already contains entry");
                                continue; //Already displayed this group...
                            }

                            //Groups 11+ have special conditions...
                            if (conditions.group >= 21)
                            {
                                SideJob jobRef = passedObject as SideJob;

                                if(jobRef == null)
                                {
                                    Interactable obj = passedObject as Interactable;
                                    if (obj != null) jobRef = obj.jobParent;
                                }

                                if (jobRef == null && conditions.group <= 49) continue; //The following needs a job reference passed...

                                //Game.Log("Condition " + conditions.group + " L " + jobRef.intro);

                                if (conditions.group >= 37 && conditions.group <= 41)
                                {
                                    if ((int)jobRef.preset.difficultyTag < 2) continue; //Requires target ID
                                }
                                else if (conditions.group <= 31 && conditions.group != 21 + (int)jobRef.preset.difficultyTag) continue; //Display block based on side mission difficulty
                                else if(conditions.group >= 32 && conditions.group < 42)
                                {
                                    //Replace this text with info chosen through the lead pool
                                    int leadPoolIndex = conditions.group - 32;
                                    if (jobRef.appliedBasicLeads.Count <= leadPoolIndex) continue; //Invalid index
                                    Game.Log("Group + " + conditions.group + " LeadPoolIndex: " + leadPoolIndex + ", applied lead count: " + jobRef.appliedBasicLeads.Count);
                                    int displayGroupFromLeadPool = (int)jobRef.appliedBasicLeads[leadPoolIndex] + 1;

                                    //Get info pool message...
                                    DDSSaveClasses.DDSMessageSave infoPool = null;

                                    if (Toolbox.Instance.allDDSMessages.TryGetValue("8d6a4bf8-2f99-46de-ac7a-1b1e628e45cb", out infoPool))
                                    {
                                        List<DDSSaveClasses.DDSBlockCondition> groupBlocks = infoPool.blocks.FindAll(item => item.group == displayGroupFromLeadPool);

                                        foreach (DDSSaveClasses.DDSBlockCondition c in groupBlocks)
                                        {
                                            //Special case; appearance blocks
                                            if (displayGroupFromLeadPool == 15)
                                            {
                                                if (jobRef.purp.characterTraits.Exists(item => item.name == "Affliction-ShortSighted" || item.name == "Affliction-FarSighted"))
                                                {
                                                    if (jobRef.purp.characterTraits.Exists(item => item.name == "Quirk-FacialHair"))
                                                    {
                                                        validBlocks.Add(Toolbox.Instance.allDDSBlocks["37f565a8-3b50-4812-81de-dda0eae00f67"]); //Add to valid
                                                    }
                                                    else
                                                    {
                                                        validBlocks.Add(Toolbox.Instance.allDDSBlocks["a8986a53-80e5-4084-8cdd-10f2100eeb53"]); //Add to valid
                                                    }
                                                }
                                                else
                                                {
                                                    if (jobRef.purp.characterTraits.Exists(item => item.name == "Quirk-FacialHair"))
                                                    {
                                                        validBlocks.Add(Toolbox.Instance.allDDSBlocks["80d87458-1f4e-4f3c-bf82-8843d1371820"]); //Add to valid
                                                    }
                                                    else
                                                    {
                                                        validBlocks.Add(Toolbox.Instance.allDDSBlocks["22274aea-20b9-4e6f-9128-76ddb5c7369f"]); //Add to valid
                                                    }
                                                }
                                            }
                                            else validBlocks.Add(Toolbox.Instance.allDDSBlocks[c.blockID]); //Add to valid

                                            break;
                                        }

                                        continue;
                                    }
                                    else continue;
                                }
                                else if(conditions.group >= 42 && conditions.group <= 47)
                                {
                                    if (jobRef.resolveQuestions.Exists(item => item.GetRevengeObjective() != null && (int)item.GetRevengeObjective().tag == conditions.group - 42))
                                    {

                                    }
                                    else continue;
                                }
                                else if(conditions.group == 48)
                                {
                                    if (jobRef.intro.Contains("Staff"))
                                    {
                                        continue;
                                    }
                                }
                                else if(conditions.group == 49)
                                {
                                    if (!jobRef.intro.Contains("Staff"))
                                    {
                                        continue;
                                    }
                                }
                                //If murder calling card
                                else if (conditions.group == 50)
                                {
                                    MurderController.Murder m = passedObject as MurderController.Murder;

                                    if (m == null || m.callingCard == null)
                                    {
                                        continue;
                                    }
                                }
                                //If murder graffiti
                                else if (conditions.group == 51)
                                {
                                    MurderController.Murder m = passedObject as MurderController.Murder;

                                    if (m == null || m.graffitiMsg == null || m.graffitiMsg.Length <= 0)
                                    {
                                        continue;
                                    }
                                }
                                //Must be employed
                                else if(conditions.group == 52)
                                {
                                    if (job == null || job.employer == null)
                                    {
                                        if (debug) Game.Log("DDS: Parse debug: .... No job found for " + citizenName);
                                        continue;
                                    }
                                }
                                //Lost item is in tall building
                                else if(conditions.group == 53)
                                {
                                    Interactable obj = passedObject as Interactable;

                                    if (obj != null)
                                    {
                                        if(obj.pv != null)
                                        {
                                            Interactable.Passed passedLost = obj.pv.Find(item => item.varType == Interactable.PassedVarType.lostItemBuilding);

                                            if(passedLost != null)
                                            {
                                                NewBuilding foundBuilding = CityData.Instance.buildingDirectory.Find(item => item.buildingID == (int)passedLost.value);

                                                if (foundBuilding != null)
                                                {
                                                    if (foundBuilding.floors.Count < 4)
                                                    {
                                                        continue;
                                                    }
                                                }
                                                else continue;
                                            }
                                            else continue;
                                        }
                                        else continue;
                                    }
                                    else continue;
                                }
                                //Lost item is in short building
                                else if (conditions.group == 54)
                                {
                                    Interactable obj = passedObject as Interactable;

                                    if (obj != null)
                                    {
                                        if (obj.pv != null)
                                        {
                                            Interactable.Passed passedLost = obj.pv.Find(item => item.varType == Interactable.PassedVarType.lostItemBuilding);

                                            if (passedLost != null)
                                            {
                                                NewBuilding foundBuilding = CityData.Instance.buildingDirectory.Find(item => item.buildingID == (int)passedLost.value);

                                                if (foundBuilding != null)
                                                {
                                                    if (foundBuilding.floors.Count >= 4)
                                                    {
                                                        continue;
                                                    }
                                                }
                                                else continue;
                                            }
                                            else continue;
                                        }
                                        else continue;
                                    }
                                    else continue;
                                }
                            }

                            List<DDSSaveClasses.DDSBlockCondition> matchingGroupBlocks = new List<DDSSaveClasses.DDSBlockCondition>();

                            //Gather all messages in a group and pick one at random
                            foreach (DDSSaveClasses.DDSBlockCondition matching in msg.blocks)
                            {
                                if (matching.group == conditions.group)
                                {
                                    //Check against traits
                                    if (matching.useTraits)
                                    {
                                        bool pass = Toolbox.Instance.DDSTraitConditionLogicAcquaintance(this, aq, matching.traitConditions, ref matching.traits);

                                        if (!pass) continue;
                                    }

                                    //Add to matching
                                    matchingGroupBlocks.Add(matching);
                                }
                            }

                            if (matchingGroupBlocks.Count > 0)
                            {
                                conditions = matchingGroupBlocks[Toolbox.Instance.RandContained(0, matchingGroupBlocks.Count, seed, out seed)]; //Swap the current for a random, the rest will be skipped.
                            }
                            else continue;

                            displayedGroup.Add(conditions.group);
                        }
                        //Check against traits
                        else if (conditions.useTraits)
                        {
                            if (debug) Game.Log("DDS: Parse debug: .... Block uses trait conditions");

                            bool pass = Toolbox.Instance.DDSTraitConditionLogicAcquaintance(this, aq, conditions.traitConditions, ref conditions.traits);

                            if (!pass)
                            {
                                if (debug) Game.Log("DDS: Parse debug: .... Block does not pass trait conditions");
                                continue;
                            }
                        }

                        validBlocks.Add(Toolbox.Instance.allDDSBlocks[conditions.blockID]); //Add to valid
                        outputDisplayGroups.Add((int)conditions.group);
                    }
                }
            }

            //We now have our valid blocks, scan them each for replacements...
            for (int i = 0; i < validBlocks.Count; i++)
            {
                DDSSaveClasses.DDSBlockSave block = validBlocks[i];

                //Scan and rank all valid replacements...
                List<DDSRank> validRepIDs = new List<DDSRank>();

                //If no relationship, default stats are 0 know and 0.5 like, resulting in -1
                float knowLikeDiff = 1f;

                if (aq != null)
                {
                    knowLikeDiff = Mathf.Abs(aq.known - 0.5f) + Mathf.Abs(aq.like - 0.5f);
                }

                //Add the default text...
                validRepIDs.Add(new DDSRank { id = block.id, rankRef = knowLikeDiff });

                foreach (DDSSaveClasses.DDSReplacement rep in block.replacements)
                {
                    float rank = Toolbox.Instance.RandContained(-0.02f, -0.01f, seed + rep.replaceWithID, out seed); //Random for variation

                    if (rep.useTraits)
                    {
                        bool pass = Toolbox.Instance.DDSTraitConditionLogicAcquaintance(this, aq, rep.traitCondition, ref rep.traits);

                        if (!pass) continue;
                    }

                    if (rep.useConnection)
                    {
                        bool pass = false;

                        //Connection requirement
                        //If anyone, then automatic pass
                        if (rep.connection == Acquaintance.ConnectionType.anyoneNotPlayer && !isPlayer)
                        {
                            pass = true;
                        }
                        //If player
                        else if (rep.connection == Acquaintance.ConnectionType.player && isPlayer)
                        {
                            pass = true;
                        }
                        //Otherwise have some kind of connection
                        else
                        {
                            //Do I know this person?
                            if (aq != null)
                            {
                                //Does connection match?
                                if (aq.connections.Contains(rep.connection))
                                {
                                    pass = true;
                                }
                                else if (rep.connection == Acquaintance.ConnectionType.anyAcquaintance)
                                {
                                    pass = true;
                                }
                                else if (rep.connection == Acquaintance.ConnectionType.friendOrWork)
                                {
                                    if (aq.connections.Contains(Acquaintance.ConnectionType.friend) || aq.connections.Contains(Acquaintance.ConnectionType.workTeam) || aq.connections.Contains(Acquaintance.ConnectionType.workOther) || aq.connections.Contains(Acquaintance.ConnectionType.familiarWork))
                                    {
                                        pass = true;
                                    }
                                }
                                else if (rep.connection == Acquaintance.ConnectionType.knowsName)
                                {
                                    if (aq.dataKeys.Contains(Evidence.DataKey.name))
                                    {
                                        pass = true;
                                    }
                                }
                            }
                            else
                            {
                                //Only pass in this case if we're looking for a stranger
                                if (rep.connection == Acquaintance.ConnectionType.stranger)
                                {
                                    pass = true;
                                }
                            }
                        }

                        if (!pass) continue;
                    }

                    if (rep.useDislikeLike)
                    {
                        if (aq != null)
                        {
                            rank += Mathf.Abs(aq.known - rep.strangerKnown) + Mathf.Abs(aq.like - rep.dislikeLike);
                        }
                        else
                        {
                            rank += 1f;
                        }
                    }
                    else rank += knowLikeDiff;

                    validRepIDs.Add(new DDSRank { id = rep.replaceWithID, rankRef = rank });
                }

                //Rank: LOWER = less difference = better
                validRepIDs.Sort((p1, p2) => p1.rankRef.CompareTo(p2.rankRef)); //Lower is better

                //Now add to the output
                output.Add(validRepIDs[0].id);
            }
        }

        return output;
    }

    //Set the desired speed
    public virtual void SetDesiredSpeed(float newSpeedRatio)
    {
        desiredNormalizedSpeed = Mathf.Clamp01(newSpeedRatio - (drunk * CitizenControls.Instance.drunkMovementPenalty));

        //If not visible, snap to this
        if (!visible || (CityConstructor.Instance != null && CityConstructor.Instance.preSimActive))
        {
            currentNormalizedSpeed = desiredNormalizedSpeed;
            currentMovementSpeed = movementRunSpeed * currentNormalizedSpeed;
            UpdateMovementSpeed();
        }
        else if (ai != null)
        {
            ai.SetUpdateEnabled(true); //Set enabled in order to allow speed lerping
        }
    }

    public virtual void SetDesiredSpeed(MovementSpeed newMovement)
    {
        SelectedDebug("Set movement speed: " + newMovement, HumanDebug.movement);

        if (newMovement == MovementSpeed.stopped)
        {
            //Reset footsteps
            //if(ai != null)
            //{
            //    ai.footStepDistanceCounter = 0;
            //    ai.rightFootNext = true;
            //}

            SetDesiredSpeed(0f);
        }
        else if (newMovement == MovementSpeed.walking)
        {
            SetDesiredSpeed(walkingSpeedRatio);
        }
        else if (newMovement == MovementSpeed.running)
        {
            SetDesiredSpeed(1f);
        }

        //If not visible, snap to this
        if (!visible)
        {
            currentNormalizedSpeed = desiredNormalizedSpeed;
            currentMovementSpeed = movementRunSpeed * currentNormalizedSpeed;
            UpdateMovementSpeed();
        }
        else if (ai != null)
        {
            ai.SetUpdateEnabled(true); //Set enabled in order to allow speed lerping
        }
    }

    //Called when the speed ratio has been changed...
    public virtual void UpdateMovementSpeed()
    {
        currentMovementSpeed = movementRunSpeed * currentNormalizedSpeed;
        animationController.UpdateMovementSpeed();

        //Set moving/running flags
        if (currentNormalizedSpeed <= 0f)
        {
            isMoving = false;
            isRunning = false;
        }
        else
        {
            isMoving = true;
            isRunning = false;

            if (currentNormalizedSpeed >= 1f)
            {
                isRunning = true;
            }
        }
    }

    //Set bed/work/special location reference
    public virtual void SetBed(Interactable passSpecificInteractable)
    {
        sleepPosition = passSpecificInteractable;
    }

    public virtual void SetWorkFurniture(Interactable passSpecificInteractable)
    {
        workPosition = passSpecificInteractable;
    }

    //Drive conversation
    public virtual void UpdateConversation()
    {
        if (currentConversation == null)
        {
            if (Game.Instance.collectDebugData) SelectedDebug("Conversation is null!", HumanDebug.misc);
            return;
        }

        //SelectedDebug("Update conversation");

        //Starting message
        if (currentConversation.currentMessage == null)
        {
            if (Game.Instance.collectDebugData) SelectedDebug("Conversation: Set starting message", HumanDebug.misc);
            currentConversation.SetCurrentMessage(currentConversation.tree.startingMessage);
        }

        //Is the current message said by me?
        if (currentConversation != null && currentConversation.currentlyTalking == this)
        {
            //Are we waiting for a link delay
            if (currentConversation.currentLink != null)
            {
                if (SessionData.Instance.gameTime < currentConversation.linkDelay)
                {
                    //Wait for delay
                    currentConversation.timeUntilNextSpeech = currentConversation.linkDelay - SessionData.Instance.gameTime;
                    //SelectedDebug("Conversation: Waiting for msg delay: " + currentConversation.timeUntilNextSpeech);
                }
                else
                {
                    currentConversation.SetCurrentMessage(currentConversation.currentLink.to);
                    //SelectedDebug("Conversation: Set current message");

                    //Delay is up: Set new current message
                    currentConversation.speechTriggered = false;
                    currentConversation.currentLink = null;
                    currentConversation.linkDelay = 0;
                }
            }
            //Do we need speech to be triggered?
            else if (!currentConversation.speechTriggered)
            {
                if (Game.Instance.collectDebugData) SelectedDebug("Conversation: Triggering speech", HumanDebug.misc);

                //Get relationship of talking to...
                Acquaintance aq = null;

                if (currentConversation.currentlyTalking != null && currentConversation.currentlyTalkingTo != null)
                {
                    currentConversation.currentlyTalking.FindAcquaintanceExists(currentConversation.currentlyTalkingTo, out aq);
                }

                //Gather list of things to say from this message
                List<string> say = ParseDDSMessage(currentConversation.currentMessage.msgID, aq, out _);

                //We can add all these to the citizen's speak queue at once
                foreach (string str in say)
                {
                    speechController.Speak("dds.blocks", str, true);
                }

                currentConversation.speechTriggered = true;
            }
            //Outside of the above, we're waiting for the speech to end...
            else if (speechController.speechQueue.Count <= 0 && !isSpeaking)
            {
                List<DDSRank> possibleLinks = GetConversationTreeLinkRankings(currentConversation.currentMessage);
                if (Game.Instance.collectDebugData) SelectedDebug("Conversation: Choose links from: " + possibleLinks.Count, HumanDebug.misc);

                //If there are no possible links, the conversation is ended
                if (possibleLinks.Count <= 0)
                {
                    //Game.Log("Ending conversation (" + name + ") as no conversation tree links exist...");
                    currentConversation.EndConversation();
                }
                else
                {
                    //We now have our chosen link, activate the delay
                    currentConversation.currentLink = possibleLinks[0].linkRef;
                    currentConversation.linkDelay = SessionData.Instance.gameTime + Toolbox.Instance.Rand(possibleLinks[0].linkRef.delayInterval.x, possibleLinks[0].linkRef.delayInterval.y);
                }
            }
            else
            {
                //SelectedDebug("Waiting for speech queue: " + speechController.speechQueue.Count);

                currentConversation.currentlyTalkingSpeechQueue = speechController.speechQueue.Count;
            }
        }
    }

    //Returns possible tree links from this message (ranked highest first)
    public List<DDSRank> GetConversationTreeLinkRankings(DDSSaveClasses.DDSMessageSettings thisMsg)
    {
        List<DDSRank> possibleLinks = new List<DDSRank>();

        string seed = thisMsg.instanceID + CityData.Instance.seed;

        //We now need to process the next step of the conversation...
        foreach (DDSSaveClasses.DDSMessageLink link in thisMsg.links)
        {
            //Attempt to match the response...
            //Add a small random number to help break up ties...
            float rank = Toolbox.Instance.RandContained(-0.01f, 0.01f, seed, out seed);

            if (link.useWeights)
            {
                rank += link.choiceWeight;
            }

            if (link.useKnowLike || link.useTraits)
            {
                //Grab the acquaintance of the corresponding message talker...
                DDSSaveClasses.DDSMessageSettings nextMessage = currentConversation.tree.messageRef[link.to];

                Human nextSaidBy = currentConversation.participantA;
                if (nextMessage.saidBy == 1) nextSaidBy = currentConversation.participantB;
                else if (nextMessage.saidBy == 2) nextSaidBy = currentConversation.participantC;
                else if (nextMessage.saidBy == 3) nextSaidBy = currentConversation.participantD;

                if (link.useKnowLike)
                {
                    Acquaintance nextAq = null;

                    if (currentConversation.currentlyTalking.FindAcquaintanceExists(nextSaidBy, out nextAq))
                    {
                        float matchDifference = Mathf.Abs(nextAq.like - link.like) + Mathf.Abs(nextAq.known - link.know); //The lower this is the better the fit 0 - 2

                        //Add 0 - 1 to rank for better matches
                        rank += (2f - matchDifference) / 2f;
                    }
                }

                //Check against traits
                if (link.useTraits)
                {
                    bool pass = Toolbox.Instance.DDSTraitConditionLogic(currentConversation.currentlyTalking, nextSaidBy, link.traitConditions, ref link.traits);

                    if (pass)
                    {
                        rank += 1f;
                    }
                }
            }

            possibleLinks.Add(new DDSRank { linkRef = link, rankRef = rank });
        }

        possibleLinks.Sort((p1, p2) => p2.rankRef.CompareTo(p1.rankRef)); //Using P2 first gives highest first

        return possibleLinks;
    }

    //Set the current consumable
    public void AddCurrentConsumable(InteractablePreset newPreset)
    {
        if (newPreset == null) return;
        currentConsumables.Add(newPreset);
        if (ai != null) ai.UpdateHeldItems(AIActionPreset.ActionStateFlag.none);
    }

    public void RemoveCurrentConsumable(InteractablePreset newPreset)
    {
        if (newPreset == null) return;
        currentConsumables.Remove(newPreset);

        //Create trash
        if (!newPreset.destroyWhenAllConsumed)
        {
            List<Interactable.Passed> passed = new List<Interactable.Passed>();
            passed.Add(new Interactable.Passed(Interactable.PassedVarType.time, SessionData.Instance.gameTime));

            if (newPreset.useSameModelAsTrash)
            {
                AddTrash(newPreset, this, passed);
            }
            else if (newPreset.trashItem != null)
            {
                AddTrash(newPreset.trashItem, this, passed);
            }
        }

        if (ai != null) ai.UpdateHeldItems(AIActionPreset.ActionStateFlag.none);
    }

    //Add trash
    public void AddTrash(InteractablePreset trashItem, Human writer, List<Interactable.Passed> passedVars = null)
    {
        MetaObject newMeta = new MetaObject(trashItem, this, writer, this, passedVars);
        trash.Add(newMeta.id);
        if (trashItem.disposal == DisposalType.anywhere) anywhereTrash++;

        //Game.Log(name + ": Add trash " + trashItem);
    }

    //Pick a consumable to buy...
    public InteractablePreset PickConsumable(ref Dictionary<InteractablePreset, int> prices, out int price, List<InteractablePreset> ignore = null)
    {
        InteractablePreset toBuy = null;
        float bestRank = -9999f;
        price = 0;

        foreach (KeyValuePair<InteractablePreset, int> pair in prices)
        {
            if (ignore != null && ignore.Contains(pair.Key)) continue; //Skip ignore

            float rank = Toolbox.Instance.Rand(0f, 2f); //Random base

            //Is cost a concern? Increasingly less so with class...
            rank -= pair.Value * (1f - societalClass);

            int pref = 0;

            //How much do I like this?
            if (pair.Key.retailItem != null)
            {
                if (!pair.Key.retailItem.isConsumable) continue;

                if (itemRanking.TryGetValue(pair.Key.retailItem, out pref))
                {
                    rank += pref;
                }

                //Does it suit my needs?
                float needs = 0f;
                needs += Mathf.Max(pair.Key.retailItem.nourishment - nourishment, 0);
                needs += Mathf.Max(pair.Key.retailItem.hydration - hydration, 0);
                needs += Mathf.Max(pair.Key.retailItem.alertness - alertness, 0);
                needs += Mathf.Max(pair.Key.retailItem.energy - energy, 0);
                needs += Mathf.Max(pair.Key.retailItem.excitement - excitement, 0);
                needs += Mathf.Max(pair.Key.retailItem.chores - chores, 0);
                needs += Mathf.Max(pair.Key.retailItem.hygiene - hygiene, 0);
                needs += Mathf.Max(pair.Key.retailItem.bladder - bladder, 0);
                needs += Mathf.Max(pair.Key.retailItem.heat - heat, 0);
                needs += Mathf.Max(pair.Key.retailItem.drunk - drunk, 0);
                needs += Mathf.Max(pair.Key.retailItem.sick - sick, 0);
                needs += Mathf.Max(pair.Key.retailItem.headache - headache, 0);
                needs += Mathf.Max(pair.Key.retailItem.wet - wet, 0);
                needs += Mathf.Max(pair.Key.retailItem.brokenLeg - brokenLeg, 0);
                needs += Mathf.Max(pair.Key.retailItem.bruised - bruised, 0);
                needs += Mathf.Max(pair.Key.retailItem.blackEye - blackEye, 0);
                needs += Mathf.Max(pair.Key.retailItem.blackedOut - blackedOut, 0);
                needs += Mathf.Max(pair.Key.retailItem.numb - numb, 0);
                needs += Mathf.Max(pair.Key.retailItem.bleeding - bleeding, 0);
                needs += Mathf.Max(pair.Key.retailItem.wellRested - wellRested, 0);
                needs += Mathf.Max(pair.Key.retailItem.breath - breath, 0);
                needs += Mathf.Max(pair.Key.retailItem.poisoned - poisoned, 0);
                needs += Mathf.Max(pair.Key.retailItem.health - currentHealth, 0);

                rank += needs * 10f;
            }
            else continue;

            if (rank > bestRank)
            {
                toBuy = pair.Key;
                bestRank = rank;
                price = pair.Value;
            }
        }

        return toBuy;
    }

    //Find doctor based on predictable seed
    public Human GetDoctor()
    {
        string seed = citizenName + humanID.ToString();

        List<Occupation> doctors = CityData.Instance.jobsDirectory.FindAll(item => item.employee != null && item.employee != this && (item.preset.name == "MedicalOfficer" || item.preset.name == "Nurse" || item.preset.name == "ChiefMedicalOfficer"));

        //Sort so we get a predictable outcome
        doctors.Sort((p1, p2) => p1.employee.humanID.CompareTo(p2.employee.humanID));

        if (doctors.Count > 0)
        {
            return doctors[Toolbox.Instance.GetPsuedoRandomNumber(0, doctors.Count, seed)].employee;
        }
        else return null;
    }

    //Find landlord based on predictable seed
    public Human GetLandlord()
    {
        if (home == null) return null;
        string seed = home.id.ToString();

        List<Occupation> landlords = CityData.Instance.jobsDirectory.FindAll(item => item.employee != null && item.employee != this && (item.preset.name == "SelfEmployedLandlord"));

        //Sort so we get a predictable outcome
        landlords.Sort((p1, p2) => p1.employee.humanID.CompareTo(p2.employee.humanID));

        if (landlords.Count > 0)
        {
            return landlords[Toolbox.Instance.GetPsuedoRandomNumber(0, landlords.Count, seed)].employee;
        }
        else return null;
    }

    public virtual void AddMeshes(List<MeshRenderer> renderers, bool addToOutline = true, bool forceMeshListUpdate = false)
    {
        foreach (MeshRenderer r in renderers)
        {
            AddMesh(r, addToOutline, false);
        }

        if (forceMeshListUpdate) UpdateMeshList();
        else updateMeshList = true;
    }

    public virtual void AddMesh(GameObject newObject, bool addToOutline = true, bool forceMeshListUpdate = false)
    {
        //Search for a LOD group on this model...
        LODGroup lod = newObject.GetComponent<LODGroup>();

        if(lod != null)
        {
            LOD[] lods = lod.GetLODs();

            if(lods.Length > 0)
            {
                foreach(Renderer r in lods[0].renderers)
                {
                    AddMesh(r as MeshRenderer, addToOutline, false, false, false);
                }
            }

            if(lods.Length > 1)
            {
                foreach (Renderer r in lods[1].renderers)
                {
                    AddMesh(r as MeshRenderer, addToOutline, false, true, false);
                }
            }

            //Get rid of LOD
            lod.enabled = false;
            Destroy(lod); //Immediate sometimes does not allow this because this can be triggered inside a physics step...
            //DestroyImmediate(lod);
        }
        else
        {
            MeshRenderer[] rends = newObject.GetComponentsInChildren<MeshRenderer>();

            foreach (MeshRenderer r in rends)
            {
                AddMesh(r, addToOutline, false, addToLOD1: true, addToBoth: true);
            }
        }

        if (forceMeshListUpdate) UpdateMeshList();
        else updateMeshList = true;
    }

    public virtual void AddMesh(MeshRenderer newMesh, bool addToOutline = true, bool forceMeshListUpdate = false, bool addToLOD1 = false, bool addToBoth = false)
    {
        if (newMesh == null) return;

        if(addToLOD1)
        {
            if(!meshesLOD1.Contains(newMesh))
            {
                meshesLOD1.Add(newMesh);
                Toolbox.Instance.SetLightLayer(newMesh, currentBuilding);
            }
        }

        if(!addToLOD1 || addToBoth)
        {
            if(!meshes.Contains(newMesh))
            {
                meshes.Add(newMesh);
                Toolbox.Instance.SetLightLayer(newMesh, currentBuilding);
            }
        }

        if (addToOutline && outline != null)
        {
            if (!outline.meshesToOutline.Contains(newMesh))
            {
                outline.meshesToOutline.Add(newMesh);
            }

            if (outline.outlineActive)
            {
                newMesh.gameObject.layer = 30;
            }
        }

        if (forceMeshListUpdate)
        {
            UpdateMeshList();
        }
        else updateMeshList = true;
    }

    public virtual void RemoveMesh(MeshRenderer newMesh, bool removeFromOutline = true, bool forceMeshListUpdate = false)
    {
        if (newMesh == null) return;

        meshes.Remove(newMesh);
        meshesLOD1.Remove(newMesh);

        if (removeFromOutline && outline != null)
        {
            outline.meshesToOutline.Remove(newMesh);

            if (outline.outlineActive)
            {
                newMesh.gameObject.layer = outline.normalLayer;
            }
        }

        if (forceMeshListUpdate)
        {
            UpdateMeshList();
        }
        else updateMeshList = true;
    }

    public virtual void UpdateMeshList()
    {
        updateMeshList = false;

        for (int i = 0; i < meshes.Count; i++)
        {
            //Remove null entries
            if(meshes[i] == null)
            {
                meshes.RemoveAt(i);
                i--;
            }
        }

        for (int i = 0; i < meshesLOD1.Count; i++)
        {
            //Remove null entries
            if (meshesLOD1[i] == null)
            {
                meshesLOD1.RemoveAt(i);
                i--;
            }
        }

        if (outline != null)
        {
            for (int i = 0; i < outline.meshesToOutline.Count; i++)
            {
                //Remove null entries
                if (outline.meshesToOutline[i] == null)
                {
                    outline.meshesToOutline.RemoveAt(i);
                    i--;
                }
            }
        }

        UpdateLODs();
    }

    public override void AddNerve(float amount, Actor scaredBy = null)
    {
        //Drunkeness weakens effects
        if(amount < 0f)
        {
            amount *= 1f - drunk;
        }

        if (isEnforcer && isOnDuty)
        {
            amount = Mathf.Max(0, amount);
        }

        if(ai != null)
        {
            if(ai.currentWeaponPreset != null)
            {
                if(amount < 0f)
                {
                    amount *= ai.currentWeaponPreset.incomingNerveDamageMultiplier;
                }
            }
        }

        base.AddNerve(amount, scaredBy);
    }

    public void UpdateLODs()
    {
        //Update LOD renderers
        if (outfitController != null)
        {
            LOD[] newLods = new LOD[] { new LOD(0.16f, meshes.ToArray()), new LOD(0.03f, meshesLOD1.ToArray()), new LOD(0.001f, new MeshRenderer[] { outfitController.distantLOD }) };
            outfitController.lod.SetLODs(newLods);
        }
    }

    public int GetHexacoScore(ref HEXACO hex)
    {
        //Get matching stats...
        int count = 0;
        int total = 0;

        if(hex.enableFeminineMasculine)
        {
            total += 10 - Mathf.RoundToInt(Mathf.Abs(hex.feminineMasculine - (genderScale * 10f)));
            count++;
        }

        if (hex.enableHumility)
        {
            total += 10 - Mathf.RoundToInt(Mathf.Abs(hex.humility - (humility * 10f)));
            count++;
        }

        if (hex.enableEmotionality)
        {
            total += 10 - Mathf.RoundToInt(Mathf.Abs(hex.emotionality - (emotionality * 10f)));
            count++;
        }

        if (hex.enableExtraversion)
        {
            total += 10 - Mathf.RoundToInt(Mathf.Abs(hex.extraversion - (extraversion * 10f)));
            count++;
        }

        if (hex.enableAgreeableness)
        {
            total += 10 - Mathf.RoundToInt(Mathf.Abs(hex.agreeableness - (agreeableness * 10f)));
            count++;
        }

        if (hex.enableConscientiousness)
        {
            total += 10 - Mathf.RoundToInt(Mathf.Abs(hex.conscientiousness - (conscientiousness * 10f)));
            count++;
        }

        if (hex.enableCreativity)
        {
            total += 10 - Mathf.RoundToInt(Mathf.Abs(hex.creativity - (creativity * 10f)));
            count++;
        }

        if (total <= 0 || count <= 0) return Mathf.CeilToInt(Mathf.Lerp(hex.outputMin, hex.outputMax, 0.5f));

        float average = ((float)total / (float)count); //Average out of 10

        return Mathf.CeilToInt(Mathf.Lerp(hex.outputMin, hex.outputMax, average / 10f));
    }

    //Check and add an item to the wallet 
    public void WalletItemCheck(int maxNewItems)
    {
        int walletItemCount = Mathf.Clamp(Mathf.CeilToInt(societalClass * 4), 2, 4); //Maximum number of entries is based on soc class

        for (int i = 0; i < maxNewItems; i++)
        {
            if (walletItems.Count >= walletItemCount) break; //Hit max items

            //Create donor card
            if (characterTraits.Exists(item => item.trait == GameplayControls.Instance.donorCardTrait) && !walletItems.Exists(item => item.meta > 0 && CityData.Instance.FindMetaObject(item.meta) != null && CityData.Instance.FindMetaObject(item.meta).preset == InteriorControls.Instance.donorCard.name))
            {
                WalletItem newItem = new WalletItem();
                newItem.itemType = WalletItemType.evidence;
                MetaObject newMeta = new MetaObject(InteriorControls.Instance.donorCard, this, this, this, null);
                newItem.meta = newMeta.id;
                walletItems.Add(newItem);
            }
            //Create credit card
            else if (characterTraits.Exists(item => item.trait == GameplayControls.Instance.creditCardTrait) && !walletItems.Exists(item => item.meta > 0 && CityData.Instance.FindMetaObject(item.meta) != null && CityData.Instance.FindMetaObject(item.meta).preset == InteriorControls.Instance.creditCard.name))
            {
                WalletItem newItem = new WalletItem();
                newItem.itemType = WalletItemType.evidence;
                MetaObject newMeta = new MetaObject(InteriorControls.Instance.creditCard, this, this, this, null);
                newItem.meta = newMeta.id;
                walletItems.Add(newItem);
            }
            //Create key
            else if (Toolbox.Instance.Rand(0f, 1f) > 0.5f && !walletItems.Exists(item => item.itemType == WalletItemType.key) && home != null)
            {
                WalletItem newItem = new WalletItem();
                newItem.itemType = WalletItemType.key;
                walletItems.Add(newItem);
            }
            //Create money
            else if (Toolbox.Instance.Rand(0f, 1f) < societalClass && !walletItems.Exists(item => item.itemType == WalletItemType.money))
            {
                WalletItem newItem = new WalletItem();
                newItem.itemType = WalletItemType.money;
                newItem.money = Mathf.Max(Mathf.RoundToInt(GameplayControls.Instance.walletCashAmountBasedOnWealth.Evaluate(societalClass) * Toolbox.Instance.Rand(0.7f, 1.3f)), 1);
                walletItems.Add(newItem);
            }
        }

        //Shuffle items
        Toolbox.Instance.ShuffleList(ref walletItems);

        //Remove nothing items
        for (int i = 0; i < walletItems.Count; i++)
        {
            if(walletItems[i].itemType == WalletItemType.nothing)
            {
                walletItems.RemoveAt(i);
                i--;
            }
        }
    }

    //Update last sighting of citizen
    public void UpdateLastSighting(Human citizen, bool phoneCall = false, int isSound = 0)
    {
        if (citizen == null) return;

        bool poi = false;
        if (MurderController.Instance.activeMurders.Exists(item => item.murderer == citizen && (item.state == MurderController.MurderState.travellingTo || item.state == MurderController.MurderState.executing || item.state == MurderController.MurderState.post || item.state == MurderController.MurderState.escaping))) poi = true;

        if (lastSightings.ContainsKey(citizen))
        {
            lastSightings[citizen].time = SessionData.Instance.gameTime;
            lastSightings[citizen].node = citizen.currentNodeCoord;
            lastSightings[citizen].run = citizen.isRunning;
            if (citizen.ai != null && citizen.ai.currentExpression != null) lastSightings[citizen].exp = (int)citizen.ai.currentExpression.expression;
            else lastSightings[citizen].exp = 0;
            lastSightings[citizen].phone = phoneCall;
            if(citizen.drunk > 0.2f) lastSightings[citizen].drunk = true;

            if (citizen.ai != null && citizen.currentGameLocation.thisAsStreet != null && citizen.ai.currentAction != null && citizen.isMoving && citizen.ai.currentAction.path != null && citizen.ai.pathCursor + 2 < citizen.ai.currentAction.path.accessList.Count)
            {
                lastSightings[citizen].mov = true;
                lastSightings[citizen].dest = citizen.ai.currentAction.path.accessList[citizen.ai.pathCursor + 2].toNode.nodeCoord;
            }
            else lastSightings[citizen].mov = false;

            lastSightings[citizen].poi = poi;
            lastSightings[citizen].sound = isSound;
        }
        else
        {
            //Remove oldest sighting if memory limit reached
            if(lastSightings.Keys.Count >= sightingMemoryLimit)
            {
                float lastS = 999999;
                Human toDelete = null;

                foreach(Human k in lastSightings.Keys)
                {
                    if(lastSightings[k].time < lastS)
                    {
                        lastS = lastSightings[k].time;
                        toDelete = k;
                    }
                }

                if(toDelete != null)
                {
                    lastSightings.Remove(toDelete);
                }
            }

            Sighting newSighting = new Sighting();
            newSighting.time = SessionData.Instance.gameTime;
            newSighting.node = citizen.currentNodeCoord;
            newSighting.run = citizen.isRunning;
            if (citizen.ai != null && citizen.ai.currentExpression != null) newSighting.exp = (int)citizen.ai.currentExpression.expression;
            newSighting.phone = phoneCall;
            if (citizen.drunk > 0.2f) newSighting.drunk = true;

            if (citizen.ai != null && citizen.currentGameLocation.thisAsStreet != null && citizen.ai.currentAction != null && citizen.isMoving && citizen.ai.currentAction.path != null && citizen.ai.pathCursor + 2 < citizen.ai.currentAction.path.accessList.Count)
            {
                newSighting.mov = true;
                newSighting.dest = citizen.ai.currentAction.path.accessList[citizen.ai.pathCursor + 2].toNode.nodeCoord;
            }
            else newSighting.mov = false;

            newSighting.poi = poi;
            newSighting.sound = isSound;

            lastSightings.Add(citizen, newSighting);
        }
    }

    //Get the direction of the sighting
    public Vector2 GetSightingDirection(Sighting sighting, out NewGameLocation newDestination)
    {
        newDestination = null;

        NewNode pos = null;
        NewNode dest = null;
        Vector2 ret = Vector2.zero;

        if (sighting.mov)
        {
            if (PathFinder.Instance.nodeMap.TryGetValue(sighting.dest, out dest))
            {
                if (PathFinder.Instance.nodeMap.TryGetValue(sighting.node, out pos))
                {
                    //Reveal next location
                    if (dest.gameLocation != pos.gameLocation)
                    {
                        newDestination = dest.gameLocation;
                    }
                    //Reveal direction
                    else
                    {
                        Vector3 dir = dest.nodeCoord - pos.nodeCoord;
                        ret = dir.normalized;
                    }
                }
            }
        }

        return ret;
    }

    public void RevealSighting(Human prospectCitizen, bool allowCalls, bool allowSounds, bool allowGeneralClue = true)
    {
        //Are they here?
        if (currentGameLocation.currentOccupants.Contains(prospectCitizen))
        {
            speechController.Speak("a81a7552-f196-45de-8da3-c6fe1254cfeb", speakAbout: prospectCitizen);
        }
        else
        {
            //I've seen them...
            if (lastSightings.ContainsKey(prospectCitizen) && (allowCalls || !lastSightings[prospectCitizen].phone) && (allowSounds || lastSightings[prospectCitizen].sound == 0))
            {
                Human.Sighting sighting = lastSightings[prospectCitizen];
                RevealSighting(prospectCitizen, sighting);
            }
            else if(allowGeneralClue)
            {
                if (Toolbox.Instance.GetPsuedoRandomNumber(0, 1f, citizenName) > 0.5f && prospectCitizen.job != null && prospectCitizen.job.employer != null)
                {
                    //Hanging around job building
                    speechController.Speak("7099af81-6c9b-411d-af8b-df6a5f6d6520", speakAbout: prospectCitizen);
                }
                else if (prospectCitizen.home != null)
                {
                    //Hanging around home building
                    speechController.Speak("f916152e-3a58-4f75-ba03-fca880a7a340", speakAbout: prospectCitizen);
                }
                else
                {
                    //No sighting
                    speechController.Speak("aeba5683-cb14-4df7-a95c-04025dfcd5d0", speakAbout: prospectCitizen);
                }
            }
            else
            {
                //No sighting
                speechController.Speak("aeba5683-cb14-4df7-a95c-04025dfcd5d0", speakAbout: prospectCitizen);
            }
        }
    }

    public void RevealSighting(Human prospectCitizen, Human.Sighting sighting)
    {
        float hour = 0f;
        int dayInt = 0;

        SessionData.Instance.ParseTimeData(sighting.time, out hour, out dayInt, out _, out _, out _);

        if (dayInt == SessionData.Instance.dayInt)
        {
            //Gunshot
            if(sighting.sound == 1)
            {
                speechController.Speak("79b84ed9-5c40-4927-9b57-f096cea0930e", speakAbout: prospectCitizen);
            }
            //Scream
            else if(sighting.sound == 2)
            {
                speechController.Speak("d2ac8d3d-1a62-410a-8a73-fd69965679e5", speakAbout: prospectCitizen);
            }
            else
            {
                //Sighting today
                if (sighting.phone)
                {
                    speechController.Speak("d6f5edea-422f-4d8b-b31f-7780ac9f2760", speakAbout: prospectCitizen);
                }
                else
                {
                    speechController.Speak("3a4fc598-8523-4b91-9ced-9fd4e288cf61", speakAbout: prospectCitizen);
                }
            }
        }
        else
        {
            //Gunshot
            if (sighting.sound == 1)
            {
                speechController.Speak("772b36f5-b8b6-4f4b-9b2f-5e8ad188e5d3", speakAbout: prospectCitizen);
            }
            //Scream
            else if (sighting.sound == 2)
            {
                speechController.Speak("be09a225-d2aa-40bd-b69a-bc04505cec48", speakAbout: prospectCitizen);
            }
            else
            {
                //Sighting other day
                if (sighting.phone)
                {
                    speechController.Speak("19946aed-4866-4035-838d-fad6b4c5f20b", speakAbout: prospectCitizen);
                }
                else
                {
                    speechController.Speak("22068015-912e-43bf-be30-c8bf312a2592", speakAbout: prospectCitizen);
                }
            }
        }

        if(sighting.sound <= 0)
        {
            //Location clarification
            NewNode sightLocation = null;

            if (PathFinder.Instance.nodeMap.TryGetValue(sighting.node, out sightLocation))
            {
                //Sighted in the street, give clarification...
                if (sightLocation.gameLocation.thisAsStreet != null)
                {
                    //"They were nearby x"
                    speechController.Speak("915bba5b-f242-42b4-8f9e-3315a767ad58", speakAbout: prospectCitizen);

                    NewGameLocation newLoc = null;
                    Vector2 dir = GetSightingDirection(sighting, out newLoc);

                    if (newLoc != null)
                    {
                        //"They were heading towards"
                        speechController.Speak("79beb68a-30a6-46e5-bf79-dfeb4c9d9241", speakAbout: prospectCitizen);
                    }
                    //Needs to be moving
                    else if(sighting.mov)
                    {
                        //"They were heading north"
                        speechController.Speak("79beb68a-30a6-46e5-bf79-dfeb4c9d9241", speakAbout: prospectCitizen);
                    }
                }
            }

            if (sighting.run && !sighting.phone)
            {
                speechController.Speak("860362f6-c03a-4300-9c6d-9be6d679d9b7", speakAbout: prospectCitizen);
            }

            if (sighting.drunk)
            {
                speechController.Speak("193eaf1d-4ad4-461a-a60f-d3f25057a4c5", speakAbout: prospectCitizen);
            }

            if (!sighting.phone)
            {
                if ((CitizenOutfitController.Expression)sighting.exp == CitizenOutfitController.Expression.angry)
                {
                    speechController.Speak("c4621de3-8513-48e8-803e-39a1b2558046", speakAbout: prospectCitizen);
                }
                else if ((CitizenOutfitController.Expression)sighting.exp == CitizenOutfitController.Expression.sad)
                {
                    speechController.Speak("b3d8492a-962b-4525-a672-599870327d7e", speakAbout: prospectCitizen);
                }
                else if ((CitizenOutfitController.Expression)sighting.exp == CitizenOutfitController.Expression.happy)
                {
                    speechController.Speak("fa2ced7b-81e3-42fb-a412-3e41df9417ab", speakAbout: prospectCitizen);
                }
            }
        }
        else
        {
            speechController.Speak("d073a4c0-319c-4ee7-bf99-6a112c234070", speakAbout: prospectCitizen);
        }
    }

    //Returns the nearest body part transform and local position within it
    public Vector3 GetNearestVert(Vector3 worldPosition, out CitizenOutfitController.CharacterAnchor nearestBodyPart)
    {
        nearestBodyPart = CitizenOutfitController.CharacterAnchor.upperTorso;

        if (outfitController != null)
        {
            //Scan through anchor reference...
            float minDistanceSqr = Mathf.Infinity;
            Vector3 nearestVertex = Vector3.zero;
            bool closeEnough = false;

            foreach (MeshFilter mesh in outfitController.allCurrentMeshFilters)
            {
                if (mesh == null) continue;

                //Find nearest anchor
                if (outfitController != null)
                {
                    bool foundAnchor = false;

                    foreach (KeyValuePair<CitizenOutfitController.CharacterAnchor, Transform> pair in outfitController.anchorReference)
                    {
                        if (pair.Value == mesh.transform || pair.Value == mesh.transform.parent)
                        {
                            nearestBodyPart = pair.Key;
                            foundAnchor = true;
                            break;
                        }
                    }

                    if (!foundAnchor) continue;
                }

                //convert point to local space
                Vector3 point = mesh.transform.InverseTransformPoint(worldPosition);

                if(mesh.mesh.isReadable)
                {
                    foreach (Vector3 vertex in mesh.mesh.vertices)
                    {
                        Vector3 diff = point - vertex;
                        float distSqr = diff.sqrMagnitude;

                        if (distSqr < minDistanceSqr)
                        {
                            minDistanceSqr = distSqr;
                            nearestVertex = vertex;

                            //Is this close enough to stop the search?
                            if (minDistanceSqr <= 0.1f)
                            {
                                closeEnough = true;
                                break;
                            }
                        }
                    }
                }

                if (closeEnough) break;
            }

            return nearestVertex;
        }

        return Vector3.zero;
    }

    public string GetCitizenName()
    {
        string ret = citizenName;

        if(StreamingOptionsController.Instance != null && StreamingOptionsController.Instance.customNames.Count > 0 && StreamingOptionsController.Instance.enableTwitchAudienceCitizens && !isPlayer)
        {
            try
            {
                if(humanID < StreamingOptionsController.Instance.customNames.Count)
                {
                    ret = GetFirstName() + " " + GetSurName();
                }
            }
            catch
            {
                Game.Log("Streaming: Failed to get twitch audience name for " + name);
            }
        }

        return ret;
    }

    public string GetFirstName()
    {
        string ret = firstName;

        if (StreamingOptionsController.Instance != null && StreamingOptionsController.Instance.customNames.Count > 0 && StreamingOptionsController.Instance.enableTwitchAudienceCitizens && !isPlayer)
        {
            if (StreamingOptionsController.Instance.customNames.Count > 0 && humanID < StreamingOptionsController.Instance.customNames.Count)
            {
                try
                {
                    if(StreamingOptionsController.Instance.customNames[humanID].firstName != null && StreamingOptionsController.Instance.customNames[humanID].firstName.Length > 0)
                    {
                        ret = StreamingOptionsController.Instance.customNames[humanID].firstName;
                    }
                }
                catch
                {
                    Game.Log("Streaming: Failed to get twitch audience name for " + name);
                }
            }
        }

        return ret;
    }

    public string GetCasualName()
    {
        string ret = GetFirstName();

        if(StreamingOptionsController.Instance != null && !StreamingOptionsController.Instance.enableTwitchAudienceCitizens)
        {
            if (casualName != null && casualName.Length > 0) ret = casualName;
        }

        return ret;
    }

    public string GetSurName()
    {
        string ret = surName;

        if (StreamingOptionsController.Instance != null && StreamingOptionsController.Instance.enableTwitchAudienceCitizens && !isPlayer)
        {
            //Game.Log("Streaming: Attempting to get twitch audenice surname for " + name + " (" + humanID + ")...");

            if (StreamingOptionsController.Instance.customNames.Count > 0 && humanID < StreamingOptionsController.Instance.customNames.Count)
            {
                try
                {
                    if (StreamingOptionsController.Instance.customNames[humanID].surName != null && StreamingOptionsController.Instance.customNames[humanID].surName.Length > 0)
                    {
                        //Game.Log("Streaming: ...Success " + StreamingOptionsController.Instance.streamerAudience[humanID].surName);
                        ret = StreamingOptionsController.Instance.customNames[humanID].surName;
                    }
                }
                catch
                {
                    Game.Log("Streaming: ...Failed to get twitch audience name for " + name);
                }
            }
        }

        return ret;
    }

    public string GetInitialledName()
    {
        string initialledName = string.Empty;

        string first = GetFirstName();
        if (first.Length > 0) initialledName = first.Substring(0, 1) + ". " + GetSurName();
        else initialledName = first + ". " + GetSurName();

        return initialledName;
    }

    public string GetInitials()
    {
        string ret = string.Empty;

        string first = GetFirstName();
        string sur = GetSurName();

        if (first.Length > 0 && sur.Length > 0)
        {
            ret = first.Substring(0, 1) + sur.Substring(0, 1);
        }

        return ret;
    }

    [Button]
    public void DebugGetAge()
    {
        Game.Log(GetAge());
        Game.Log(GetAgeGroup());
        Game.Log(birthday);
        Game.Log(SessionData.Instance.yearInt);
    }

    public bool TryGiveItem(Interactable givenItem, Human givenBy, bool defaultSuccess, bool enableSpeech = true)
    {
        if (givenItem == null) return false;

        bool isMissionSuccess = false;

        //Is this an auto success? IE is this a lost item?
        foreach (NewBuilding b in CityData.Instance.buildingDirectory)
        {
            for (int i = 0; i < b.lostAndFound.Count; i++)
            {
                GameplayController.LostAndFound lf = b.lostAndFound[i];

                //Item type matches...
                if (lf.preset == givenItem.preset.presetName)
                {
                    Human owner = null;
                    CityData.Instance.citizenDictionary.TryGetValue(lf.ownerID, out owner);

                    //I am the person who lost this...
                    if (lf.ownerID == humanID)
                    {
                        if(enableSpeech && speechController != null) speechController.Speak("8b018440-f220-4d57-a9c9-9b97adfd46d3", speakAbout: owner);
                        b.CompleteLostAndFound(owner as Citizen, givenItem.preset, true);
                        isMissionSuccess = true;
                        break;
                    }
                    //I live with the person who lost this...
                    else if (owner != null && home != null && home.inhabitants.Exists(item => item.humanID == lf.ownerID))
                    {
                        if (enableSpeech && speechController != null) speechController.Speak("3caae4b3-3c6a-4c38-b1d2-fe981cb5654e", speakAbout: owner);
                        b.CompleteLostAndFound(owner as Citizen, givenItem.preset, true);

                        if (owner.isDead)
                        {
                            isMissionSuccess = true;
                        }
                        else
                        {
                            owner.TryGiveItem(givenItem, givenBy, true, enableSpeech: false);
                            return true;
                        }

                        break;
                    }
                    //I work with the person who lost this...
                    else if (job != null && job.employer != null && owner != null && owner.job != null && owner.job.employer == job.employer)
                    {
                        if (enableSpeech && speechController != null) speechController.Speak("3caae4b3-3c6a-4c38-b1d2-fe981cb5654e", speakAbout: owner);
                        b.CompleteLostAndFound(owner as Citizen, givenItem.preset, true);

                        if (owner.isDead)
                        {
                            isMissionSuccess = true;
                        }
                        else
                        {
                            owner.TryGiveItem(givenItem, givenBy, true, enableSpeech: false);
                            return true;
                        }

                        break;
                    }
                }
            }

            if (isMissionSuccess) break;
        }

        //Is this a regular success? IE the person will accept anything...
        if(!isMissionSuccess)
        {
            if (defaultSuccess)
            {
                //Choose between regular and confused responses...
                if (givenItem.val <= 1)
                {
                    //Confused response
                    if (enableSpeech && speechController != null) speechController.Speak("1d7fd151-6ab6-426f-9a26-b0e38d9ddb44");
                }
                else
                {
                    //Generic response
                    if (enableSpeech && speechController != null) speechController.Speak("af828eb2-57b9-4af2-a287-15530fab21ce");
                }
            }
            else
            {
                //A fail; meaning the citizen will accept not just anything (it has to be valuable to them)

                //Are they hungry/thirsty?
                if (givenItem.preset.retailItem != null)
                {
                    if (givenItem.preset.retailItem.nourishment > 0.1f && nourishment < 0.5f)
                    {
                        defaultSuccess = true;
                        if (enableSpeech && speechController != null) speechController.Speak("48bc129e-0841-4e10-9ac5-00f0acc56ec0");
                    }
                    else if (givenItem.preset.retailItem.hydration > 0.1f && hydration < 0.5f)
                    {
                        defaultSuccess = true;
                        if (enableSpeech && speechController != null) speechController.Speak("bf120304-ed08-4e6d-b93b-49b081ca9e7d");
                    }
                }

                //Otherwise only accept something of value
                if (givenItem.val >= 10 && !defaultSuccess)
                {
                    defaultSuccess = true;
                    if (enableSpeech && speechController != null) speechController.Speak("af828eb2-57b9-4af2-a287-15530fab21ce");
                }

                //Reject
                if(!defaultSuccess)
                {
                    if (enableSpeech && speechController != null) speechController.Speak("4e2f4c3e-99b9-4e1a-a9d6-ad8098d5901e");
                }
            }
        }

        if (isMissionSuccess || defaultSuccess)
        {
            //Is this a consumable?
            if (givenItem.preset.retailItem != null && givenItem.preset.consumableAmount > 0f && givenItem.cs > 0f) AddCurrentConsumable(givenItem.preset);

            //This is given by a player
            if (givenBy != null && givenBy.isPlayer)
            {
                FirstPersonItemController.InventorySlot slot = FirstPersonItemController.Instance.slots.Find(item => item.GetInteractable() == givenItem);

                if (slot != null)
                {
                    FirstPersonItemController.Instance.EmptySlot(slot, false, false, playSound: true);
                    givenItem.SetInInventory(this);
                }

                //Starch product
                if (givenItem.preset.retailItem != null && givenItem.preset.retailItem.tags.Contains(RetailItemPreset.Tags.starchProduct))
                {
                    int pay = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.starchGive));

                    if (pay > 0)
                    {
                        GameplayController.Instance.AddMoney(pay, true, "Starch give");
                    }
                }
            }

            return true;
        }
        else return false;
    }
}
