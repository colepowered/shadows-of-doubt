﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;

[System.Serializable]
public class TimelineEvent : IComparable<TimelineEvent>
{
    public string name = "TimelineEvent";
    public string detail = "TimelieDetail";
    [NonSerialized]
    public bool intialised = false;

    public enum EventType { sightingStreet, sightingWindow, sightingHere, sightingArrive, sightingDepart, selfArrive, selfDepart, wakeUp, goToBed, heardSound, nonPersonSighting, smell, questioned, delayBegin, delayEnd, timeOfDeath, sightingWentToBed, sightingWokeUp, forcedEntryInvestigate };
    public EventType eventType; //The type of memory
    public bool isSelfLocational = false; //This event is self locational, acts as an alibi
    public bool isGlobalEvent = false; //This event is a global and doesn't 'belong' to anyone.

    //IDs: Can be required for determining which memory is created first
    public int eventID = 0;
    public static int assignEventID = 0;

    public NewNode location; //Recorded location of this memor

    public float happenedAt = 0f; //Accurate recording of when this memory happened
    public float timeAccuracy = 1f; //The time accuracy of recall for this (dictates time rounding)

    [NonSerialized]
    public float totalSuspicion = 0f;

    public bool calledUpon = false; //Has this memory been called upon by a witness statement order
    [NonSerialized]
    public EventTime eventTime; //Class that hold called upon timing data

    //Tie to memory- tie to another specific memory. Most memories will be tied to an arrival event.
    [NonSerialized]
    public List<TimelineEvent> childEvents = new List<TimelineEvent>();
    [NonSerialized]
    public TimelineEvent parentEvent;

    //Timeline inclusion
    public bool discoveredByQuestioned = false; //Flag for knowing when to display on the timeline. Becuase the questioned process takes time, the events must be revealed over a progress period using this.
    //public List<CaseReference> releventForCases = new List<CaseReference>(); //This event will appear in timelines for these cases

    //Events
    //Triggered when this block's name is update
    public delegate void OnNameChange();
    public event OnNameChange OnNameChanged;

    //Triggered when recall accuracy is changed, so time will trigger an update
    public delegate void RecallAccuracyChange();
    public event RecallAccuracyChange OnRecallAccuracyChange;

    //Triggered when timing is updated
    public delegate void OnCalledUponTimeUpdate();
    public event OnCalledUponTimeUpdate OnCalledUponTimeUpdated;

    //Debug
    public int debugLocationID = -1;
    public string debugLocationName;

    //Constructor- truth
    public TimelineEvent(EventType newType, NewNode newLocation, TimelineEvent newParentEvent, bool autoCallUpon, bool overrideHappenedAt = false, float happenedOverride = 0f)
    {
        intialised = true;
        eventType = newType;

        //Parse self locational
        if (eventType == EventType.selfArrive || eventType == EventType.selfDepart || eventType == EventType.delayBegin || eventType == EventType.delayEnd)
        {
            isSelfLocational = true;

            //Assign id here
            eventID = assignEventID;
            assignEventID++;
        }
        else
        {
            isSelfLocational = false;
        }

        if (!overrideHappenedAt) happenedAt = SessionData.Instance.gameTime;
        else happenedAt = happenedOverride;

        location = newLocation;

        if (location != null)
        {
            debugLocationName = location.name;
        }

        parentEvent = newParentEvent;

        //Tie to another memory
        if (parentEvent != null)
        {
            if (parentEvent != this)
            {
                parentEvent.AddChildEventToThis(this);
            }
        }

        if (autoCallUpon) CallUpon();
    }

    //Update name
    public virtual void UpdateName()
    {
        //Event name from - to
        name = Strings.Get("evidence.generic", eventType.ToString());
        if (eventTime != null)
        {
            name += " " + eventTime.startString + " - " + eventTime.endString;

            //Debug
            name += " (Accurate:" + eventTime.accurateString + ")";
        }

        //Fire event
        if (OnNameChanged != null) OnNameChanged();
    }

    //Add a memory that's tied to this
    public void AddChildEventToThis(TimelineEvent newTied)
    {
        if (!childEvents.Contains(newTied))
        {
            childEvents.Add(newTied);
            newTied.parentEvent = this;
            newTied.eventTime = eventTime;
        }
    }

    //Call upon/re-call upon this memory
    public virtual void CallUpon(bool forceAccuracy = false, int forceAccuracyToMinutes = 0)
    {
        //Ignore if previously called upon
        if (calledUpon) return;

        //Create a called upon time class
        if (eventTime == null)
        {
            eventTime = new EventTime(this, forceAccuracy, forceAccuracyToMinutes); //Create time class with optional forced accuracy

            //Listen for name change
            eventTime.OnCalledUponTimeUpdated += UpdateName;
            eventTime.OnCalledUponTimeUpdated += OnTimeUpdated;

            OnTimeUpdated();
        }

        //If called upon, this memory will stay longer
        calledUpon = true;

        //Valid time to set the name
        UpdateName();

        ////Call tied memories
        //foreach (MemoryEvent tie in eventsTiedToThis)
        //{
        //    tie.CallUpon();
        //}
    }

    //Set time accuracy: Set the accuracy for this event
    public void SetTimeRecallAccuracy(float newVal)
    {
        timeAccuracy = newVal;

        //Fire event
        if(OnRecallAccuracyChange != null)
        {
            OnRecallAccuracyChange();
        }
    }

    //Triggered when timing is updated
    public void OnTimeUpdated()
    {
        //Fire event
        if (OnCalledUponTimeUpdated != null) OnCalledUponTimeUpdated();
    }

    //On appear in timeline
    public virtual void OnAppearInTimeline()
    {
        discoveredByQuestioned = true;
    }

    //Default comparer (time)
    public int CompareTo(TimelineEvent otherObject)
    {
        //If exactly the same time, use the memory id to determin which was created first
        if (this.happenedAt == otherObject.happenedAt)
        {
            return this.eventID.CompareTo(otherObject.eventID);
        }
        else return this.happenedAt.CompareTo(otherObject.happenedAt);
    }
}
