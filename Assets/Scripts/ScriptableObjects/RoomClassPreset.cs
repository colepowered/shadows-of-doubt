﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEditor;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "roomclass_data", menuName = "Database/Room Class Preset")]

public class RoomClassPreset : SoCustomComparison
{

}
