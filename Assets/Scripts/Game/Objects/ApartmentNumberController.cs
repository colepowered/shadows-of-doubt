using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApartmentNumberController : MonoBehaviour
{
    public List<GameObject> numberPrefabs = new List<GameObject>();
    public List<Transform> spawned = new List<Transform>();

    public void Setup(NewNode behindNode, NewDoor door)
    {
        if (behindNode.gameLocation.thisAsAddress != null && behindNode.gameLocation.thisAsAddress.residence != null && behindNode.gameLocation.thisAsAddress.floor.floor >= 0)
        {
            if(door.transform.localScale.x < 0f)
            {
                this.transform.localScale = new Vector3(-this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z);
            }

            //Create residence string
            string numStr = behindNode.gameLocation.thisAsAddress.residence.GetResidenceString();
            //if (numStr.Length < 2) numStr = "0" + numStr;

            for (int i = 0; i < numStr.Length; i++)
            {
                int number = 0;

                if(int.TryParse(numStr[i].ToString(), out number))
                {
                    GameObject newObject = Instantiate(numberPrefabs[number], this.transform);

                    spawned.Add(newObject.transform);
                }
            }

            float xCur = 0.2f * ((spawned.Count - 1) / 2f);

            for (int i = 0; i < spawned.Count; i++)
            {
                spawned[i].localPosition = new Vector3(xCur, 0f, 0f);
                xCur -= 0.2f;
            }
        }
        else
        {
            //Game.LogError("Destroying apartment number sign: " + behindNode.gameLocation.thisAsAddress + ", " + behindNode.gameLocation.thisAsAddress.residence + ", " + behindNode.gameLocation.thisAsAddress.floor.floor);
            Destroy(this.gameObject);
        }
    }
}
