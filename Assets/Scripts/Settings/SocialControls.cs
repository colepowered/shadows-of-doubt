﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocialControls : MonoBehaviour
{
    //public enum ConnectionType {friend, neighbor, housemate, lover, boss, workTeam, workOther, familiarResidence, familiarWork, descriptionOnly};

    [Header("Relationships/Acquaintances")]
    [Tooltip("Random ranges for knowning different acquaintances")]
    public Vector2 knowLoverRange = new Vector2(0.9f, 1f);
    public Vector2 knowHousemateRange = new Vector2(0.4f, 0.88f);
    public Vector2 knowFriendRange = new Vector2(0.5f, 0.93f);
    public Vector2 knowNeighborRange = new Vector2(0f, 0.66f);
    public Vector2 knowBossRange = new Vector2(0.1f, 0.72f);
    public Vector2 knowWorkTeamRange = new Vector2(0.1f, 0.8f);
    public Vector2 knowWorkRange = new Vector2(0f, 0.7f);
    public Vector2 knowWorkOtherRange = new Vector2(0.1f, 0.2f);
    public Vector2 knowRegularCustomerRange = new Vector2(0.1f, 0.5f);
    public Vector2 knowParamourRange = new Vector2(0.4f, 1f);
    public Vector2 knowGroupRange = new Vector2(0f, 0.8f);

    [Header("Traits Reference")]
    public CharacterTrait paramour;

    [Header("Culture")]
    public int basePreferredBookCount = 3;

    [Header("Businesses")]
    [Tooltip("Paygrades (see company preset wage enum")]
    public List<float> wageRanges = new List<float>();
    [Tooltip("Overtime ranges (see occupation preset enum")]
    public List<Vector2> overtimeRanges = new List<Vector2>();

    [Space(7)]
    [Header("Memory Accuracy Steps")]
    [Tooltip("0.8 - 1 accuracy (minutes)")]
    public float accuracy1 = 5f;
    [Tooltip("0.6 - 0.8 accuracy (minutes)")]
    public float accuracy2 = 10f;
    [Tooltip("0.4 - 0.6 accuracy (minutes)")]
    public float accuracy3 = 15f;
    [Tooltip("0.2 - 0.4 accuracy (minutes)")]
    public float accuracy4 = 30f;
    [Tooltip("0.0 - 0.2 accuracy (minutes)")]
    public float accuracy5 = 60f;

    [Space(7)]
    [Header("Know Thresholds")]
    [Tooltip("How well known a connection has to be before they are included in a citizen's telephone book")]
    [Range(0f, 1f)]
    public float telephoneBookInclusionThreshold = 0.35f;
    [Tooltip("How well known a connection has to be before they know the others' place of work")]
    [Range(0f, 1f)]
    public float knowPlaceOfWorkThreshold = 0.4f;
    [Tooltip("How well known a connection has to be before they know the others' address")]
    [Range(0f, 1f)]
    public float knowAddressThreshold = 0.75f;
    [Tooltip("How well known a connection has to be before a citizen mourn's another's death")]
    [Range(0f, 1f)]
    public float knowMournThreshold = 0.35f;
    [Tooltip("How well known a connection has to be before a citizen sends the other birthday cards or has their birthday listed on the calendar")]
    [Range(0f, 1f)]
    public float knowBirthdayThreshold = 0.7f;
    [Tooltip("How well known a connection before they can reveal their immediate location")]
    [Range(0f, 1f)]
    public float knowImmediateLocationThreshold = 0.8f;

    //Singleton pattern
    private static SocialControls _instance;
    public static SocialControls Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
}
