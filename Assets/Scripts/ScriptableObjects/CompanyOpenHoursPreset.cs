﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make company presets.
//Script pass 1
[CreateAssetMenu(fileName = "company_open_hours_data", menuName = "Database/Company/Open Hours Preset")]

public class CompanyOpenHoursPreset : SoCustomComparison
{
    [System.Serializable]
    public class CompanyShift
    {
        public string name = "shift";
        public OccupationPreset.ShiftType shiftType = OccupationPreset.ShiftType.dayShift;
        public Vector2 decimalHours = new Vector2(9, 17);
        public bool monday = true;
        public bool tuesday = true;
        public bool wednesday = true;
        public bool thursday = true;
        public bool friday = true;
        public bool saturday = false;
        public bool sunday = false;

        [System.NonSerialized]
        public List<Occupation> assigned = new List<Occupation>();

        [ReadOnly]
        public int debugAssigned = 0;
    }

    [Header("Opening Hours")]
    [Tooltip("Hours of retail opening hours")]
    public Vector2 retailOpenHours = new Vector2(8, 17);

    [Header("Days Open")]
    public bool monday = true;
    public bool tuesday = true;
    public bool wednesday = true;
    public bool thursday = true;
    public bool friday = true;
    public bool saturday = true;
    public bool sunday = false;

    [Header("Work Hours")]
    [ReorderableList]
    public List<CompanyShift> shifts = new List<CompanyShift>();
}
