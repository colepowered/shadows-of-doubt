﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PinFolderButtonController : ButtonController, IPointerDownHandler, IPointerUpHandler
{
    public Sprite pinnedImage;
    public Sprite pinnedMO;
    public Sprite unpinnedImage;
    public Sprite unpinnedMO;

    public Sprite pinnedColour;
    public Sprite unpinnedColour;

    public ContextMenuController contextMenu;

    public bool placementActive = false;
    public bool pointerDown = false;

    void Start()
    {
        SetupReferences();
        VisualUpdate();
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        if(parentWindow != null && parentWindow.forceDisablePin)
        {
            return;
        }

        //This must be the left button
        if (eventData.button != PointerEventData.InputButton.Left)
        {
            Game.Log("Pointer button: " + eventData.button.ToString());
            return;
        }

        if (CasePanelController.Instance.activeCase == null)
        {
            PopupMessageController.Instance.PopupMessage("NoActiveCase", true, true, RButton: "Confirm", anyButtonClosesMsg: true, enableInputField: true, inputFieldDefault: Strings.Get("ui.interface", "New Case"));
            PopupMessageController.Instance.OnLeftButton += onCreateNewCasePopupCancel;
            PopupMessageController.Instance.OnRightButton += OnCreateNewCasePopup;

            Game.Log("No active case...");
            return;
        }

        //Is the custom string select active?
        if (CasePanelController.Instance.customLinkSelectionMode)
        {
            Game.Log("Custom string active...");
            return;
        }

        //Pin this to effectively create a click-drag to position
        if (parentWindow != null && !parentWindow.pinned && parentWindow.currentPinnedCaseElement == null)
        {
            Game.Log("Toggling pinned...");

            pointerDown = true;

            parentWindow.pinColour.enabled = false;
            parentWindow.pinOverlay.enabled = false;

            parentWindow.TogglePinned();

            //We need to force/mimic the drag action on the pinned drag script
            try
            {
                parentWindow.currentPinnedCaseElement.pinnedController.dragController.OnPointerDown(eventData);
                parentWindow.currentPinnedCaseElement.pinnedController.OnPointerDown(eventData);

                //Nudge size
                parentWindow.currentPinnedCaseElement.pinnedController.juice.Nudge(new Vector2(2.5f, 2.5f), Vector2.zero, affectRotation: false);

                if (!placementActive)
                {
                    StartCoroutine(PlacementFade());
                }
            }
            catch
            {

            }
        }
        else
        {
            //Game.Log(parentWindow);

            //if (parentWindow != null)
            //{
            //    Game.Log(parentWindow.pinned);
            //    Game.Log(parentWindow.currentPinnedCaseElement);
            //}

            //If unpinning then show unpin icon
            icon.gameObject.SetActive(true);

            if(parentWindow != null)
            {
                parentWindow.pinColour.enabled = false;
                parentWindow.pinOverlay.enabled = false;
            }
        }
    }

    public void OnCreateNewCasePopup()
    {
        PopupMessageController.Instance.OnLeftButton -= onCreateNewCasePopupCancel;
        PopupMessageController.Instance.OnRightButton -= OnCreateNewCasePopup;

        CasePanelController.Instance.OnCreateNewCustomCase();
    }

    public void onCreateNewCasePopupCancel()
    {
        PopupMessageController.Instance.OnLeftButton -= onCreateNewCasePopupCancel;
        PopupMessageController.Instance.OnRightButton -= OnCreateNewCasePopup;
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        //Must be in desktop mode
        if (!InterfaceController.Instance.desktopMode)
        {
            return;
        }

        //This must be the left button
        if (eventData.button != PointerEventData.InputButton.Left)
        {
            return;
        }

        ForcePointerUp();
    }

    public void ForcePointerUp()
    {
        Game.Log("Force pointer up");
        pointerDown = false;

        //Is the custom string select active?
        if (CasePanelController.Instance.customLinkSelectionMode)
        {
            return;
        }

        //Mimic pointer up
        if (parentWindow.currentPinnedCaseElement != null && parentWindow.currentPinnedCaseElement.pinnedController != null)
        {
            parentWindow.currentPinnedCaseElement.pinnedController.ForcePointerUp();
        }

        if (placementActive)
        {
            placementActive = false;

            //Minimize on pinned
            if (InterfaceControls.Instance.minimizeEvidenceOnPinned)
            {
                parentWindow.CloseWindow();
            }
        }
        //Unpin
        else if (parentWindow.pinned && parentWindow.currentPinnedCaseElement != null && parentWindow.currentPinnedCaseElement.pinnedController != null)
        {
            parentWindow.TogglePinned();
        }

        //Hide icon
        parentWindow.pinColour.enabled = true;
        parentWindow.pinOverlay.enabled = true;

        //button.image.color = parentWindow.pinColourActual;
        icon.gameObject.SetActive(false);
    }

    IEnumerator PlacementFade()
    {
        placementActive = true;

        if (!InputController.Instance.mouseInputMode)
        {
            parentWindow.currentPinnedCaseElement.pinnedController.rect.transform.position = rect.transform.position;
        }

        while (placementActive)
        {
            //Force drag move the pinned object
            if (parentWindow.currentPinnedCaseElement != null && parentWindow.currentPinnedCaseElement.pinnedController != null)
            {
                parentWindow.currentPinnedCaseElement.pinnedController.pinPlaceActive = true;

                if (InputController.Instance.mouseInputMode)
                {
                    parentWindow.currentPinnedCaseElement.pinnedController.ForceDrag();
                    parentWindow.currentPinnedCaseElement.pinnedController.dragController.ForceDrag(Input.mousePosition);
                }
                else
                {
                    Vector2 moveWindow = new Vector2(InputController.Instance.GetAxisRelative("MoveEvidenceAxisX"), InputController.Instance.GetAxisRelative("MoveEvidenceAxisY"));

                    if (moveWindow.magnitude > 0.15f)
                    {
                        moveWindow *= 12f;
                        parentWindow.currentPinnedCaseElement.pinnedController.ForceDrag();
                        parentWindow.currentPinnedCaseElement.pinnedController.dragController.ForceDragController(new Vector2(parentWindow.currentPinnedCaseElement.pinnedController.rect.localPosition.x, parentWindow.currentPinnedCaseElement.pinnedController.rect.localPosition.y) + moveWindow);
                    }
                }
            }

            yield return null;
        }

        parentWindow.currentPinnedCaseElement.pinnedController.pinPlaceActive = false;
    }

    public override void OnLeftClick()
    {
        //Is the custom string select active?
        if (CasePanelController.Instance.customLinkSelectionMode && parentWindow.currentPinnedCaseElement != null && parentWindow.currentPinnedCaseElement.pinnedController != null)
        {
            CasePanelController.Instance.FinishCustomStringLinkSelection(parentWindow.currentPinnedCaseElement.pinnedController);
            return;
        }
    }

    public override void OnHoverStart()
    {
        VisualUpdate();
    }

    public override void OnHoverEnd()
    {
        VisualUpdate();
    }

    public override void VisualUpdate()
    {
        if(parentWindow != null && parentWindow.pinnable)
        {
            if (parentWindow.pinned)
            {
                parentWindow.pinColour.sprite = pinnedColour;

                if (isOver)
                {
                    parentWindow.pinOverlay.sprite = pinnedMO;

                }
                else
                {
                    parentWindow.pinOverlay.sprite = pinnedImage;
                }

                rect.sizeDelta = new Vector2(64, 64);

                RectTransform pin = parentWindow.pinColour.GetComponent<RectTransform>();
                if(pin != null) pin.sizeDelta = rect.sizeDelta;
            }
            else
            {
                parentWindow.pinColour.sprite = unpinnedColour;

                if (isOver)
                {
                    parentWindow.pinOverlay.sprite = unpinnedMO;
                }
                else
                {
                    parentWindow.pinOverlay.sprite = unpinnedImage;
                }

                if(rect != null) rect.sizeDelta = new Vector2(128, 64);

                RectTransform pin = parentWindow.pinColour.GetComponent<RectTransform>();
                if (pin != null) pin.sizeDelta = rect.sizeDelta;
            }
        }
    }
}
