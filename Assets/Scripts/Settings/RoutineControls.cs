﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoutineControls : MonoBehaviour
{

    //Singleton pattern
    private static RoutineControls _instance;
    public static RoutineControls Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    [Header("Stat Multipliers")]
    [Tooltip("Based on 3 meals a day + snack, the hunger rate should be ~+0.125 an hour")]
    public float hungerRate = 0.125f;
    [Tooltip("...Raise it slightly for thirst")]
    public float thirstRate = 0.2f;
    [Tooltip("Alertness- similar to meals")]
    public float tirednessRate = 0.12f;
    [Tooltip("Energy- a 17 hour day depleats energy completely")]
    public float energyRate = 0.058f;
    [Tooltip("Boredem rate")]
    public float boredemRate = 0.058f;
    [Tooltip("Chores rate - 48 hours")]
    public float choresRate = 0.02f;
    [Tooltip("Hygiene rate - 24 hours")]
    public float hygeieneRate = 0.04f;
    [Tooltip("Bladder rate - 4 hours")]
    public float bladderRate = 0.25f;
    [Tooltip("Drunk rate - 2 hours")]
    public float drunkRate = 0.5f;
    [Tooltip("Breath rate - 5 mins")]
    public float breathRate = 0.083f;
    [Tooltip("Idle sound rate - 1 hour")]
    public float idleSoundRate = 1f;
    [Tooltip("Poison remove rate - 1 hours")]
    public float poisonRate = 1f;
    [Tooltip("Blinded remove rate - 5 mins")]
    public float blindedRate = 0.083f;

    [Header("Routine")]
    [Tooltip("Citizen decisions on whether to go out to get certain things like food depend on 1) how much time they've spent somewhere, this many hours = 100% decision to go somewhere else")]
    public float commericalDecisionMPTimeSpent = 3f;
    [Tooltip("How likely a citizen will choose to go out to get certain things (eg. Food) when the player is in the same building.")]
    [Range(0f, 1f)]
    public float commericalDecisionMPlayerSameBuilding = 0.66f;
    [Tooltip("How likely a citizen will choose to go out to get certain things (eg. Food) when the player is in the same gamelocation.")]
    [Range(0f, 1f)]
    public float commericalDecisionMPlayerSameLocation = 0.33f;
    [Tooltip("How likely a citizen will choose to go out to get certain things (eg. Food) when the player is not in the above.")]
    [Range(0f, 2f)]
    public float commericalDecisionMPlayerElsewhere = 1f;

    [Header("Action Reference")]
    public AIGoalPreset workGoal;
    public AIGoalPreset answerDoorGoal;
    public AIGoalPreset awakenGoal;
    public AIGoalPreset sleepGoal;
    public AIGoalPreset patrolGoal;
    public AIGoalPreset fleeGoal;
    public AIGoalPreset investigateGoal;
    public AIGoalPreset postJob;
    public AIGoalPreset enforcerResponse;
    public AIGoalPreset enforcerGuardDuty;
    public AIGoalPreset makeSpecificCall;
    public AIGoalPreset layLow;
    public AIActionPreset searchArea;
    public AIActionPreset searchAreaEnforcer;
    public AIActionPreset hangUp;
    public AIActionPreset raiseAlarm;
    public AIActionPreset sleep;
    public AIActionPreset audioFocus;
    public AIActionPreset mainLightOn;
    public AIActionPreset mainLightOff;
    public AIActionPreset secondaryLightOn;
    public AIActionPreset secondaryLightOff;
    public AIActionPreset lockDoor;
    public AIActionPreset unlockDoor;
    public AIActionPreset openDoor;
    public AIActionPreset closeDoor;
    public AIActionPreset knockOnDoor;
    public AIActionPreset openLocker;
    public AIActionPreset closeLocker;
    public AIActionPreset hide;
    public AIActionPreset pullPlayerFromHiding;
    public AIActionPreset answerTelephone;
    public AIActionPreset makeCall;
    public AIActionPreset takeMoney;
    public AIActionPreset pickupFromFloor;
    public AIActionPreset putBack;
    public AIActionPreset turnOnMusic;
    public AIActionPreset disposal;
    public AIActionPreset bargeDoor;
    public AIActionPreset standAgainstWall;
    public AIActionPreset standGuard;
    public AIActionPreset putUpPoliceTape;
    public AIActionPreset putUpStreetCrimeScene;
    public AIActionPreset getHandIn;
    public AIActionPreset AIPutDownItem;
    public AIActionPreset AIPickUpItem;
    public AIActionPreset purchaseItem;
    public AIActionPreset takeConsumable;
    public AIActionPreset sit;
    public AIActionPreset lookBehindSpooked;
    public AIActionPreset mugging;
    public AIActionPreset takeFirstPersonItem;
    public AIGoalPreset findDeadBody;
    public AIGoalPreset smellDeadBody;
    public AIGoalPreset mourn;
    public AIGoalPreset stealItem;
    public AIGoalPreset exitBuilding;
    public AIGoalPreset missionMeetUpSpecific;
    public AIGoalPreset giveSelfUp;


    public AIGoalPreset meetFood;
    public GroupPreset meetUpFoodMission;
    public AIGoalPreset toGoGoal;
    public AIGoalPreset toGoWalkGoal;

    public BuildingPreset cityHall;

    [Header("Sales records")]
    [Tooltip("How many sales records are kept")]
    public int salesRecordsThreshold = 100;
}
