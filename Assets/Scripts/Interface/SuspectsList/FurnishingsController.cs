﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using TMPro;
using UnityEngine.UI;

public class FurnishingsController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public WindowContentController wcc;
    public RectTransform entryParent;
    public ButtonController inRoomButton;
    public ButtonController inStorageButton;
    public ButtonController inShopButton;
    public GameObject furnitureElementPrefab;

    public ButtonController chairsButton;
    public ButtonController tablesButton;
    public ButtonController unitsButton;
    public ButtonController electronicsButton;
    public ButtonController structuralButton;
    public ButtonController decorationButton;
    public ButtonController miscButton;

    public enum TabState { inRoom, inStorage, inShop };

    [Header("Settings")]
    public Sprite uncheckedSprite;
    public Sprite checkedSprite;

    [Header("State")]
    public bool isSetup = false;
    public TabState tabState = TabState.inRoom;
    public List<FurniturePreset.DecorClass> displayClasses = new List<FurniturePreset.DecorClass>();
    public NewRoom room;
    //public InfoWindow materialKeyWindow;
    public MaterialKeyController keyController;
    //public PlayerApartmentController.FurniturePlacement placement;

    //public FurniturePreset selectedFurniture;

    public TMP_InputField searchInputField;
    public List<DecorElementController> spawnedEntries = new List<DecorElementController>();

    public void Setup(WindowContentController newContentController)
    {
        rect = this.gameObject.GetComponent<RectTransform>();

        wcc = newContentController;
        room = Player.Instance.currentRoom;

        SetPageSize(new Vector2(640, 748));

        displayClasses = new List<FurniturePreset.DecorClass>(PlayerApartmentController.Instance.rememberDisplayClasses);

        //Update list
        SetTabState(PlayerApartmentController.Instance.rememberRoomStorageShop, true);

        isSetup = true;

        PlayerApartmentController.Instance.OnFurnitureChange += OnFurnitureChange;
    }

    public void SetPageSize(Vector2 newSize)
    {
        rect.sizeDelta = newSize;
        wcc.normalSize = rect.sizeDelta;
    }

    public void SetTabState(int newState)
    {
        SetTabState((TabState)newState);
    }

    public void SetTabState(TabState newState, bool forceUpdate = false)
    {
        if(newState != tabState || forceUpdate)
        {
            tabState = newState;

            //Remove all if switched tabs
            while(spawnedEntries.Count > 0)
            {
                Destroy(spawnedEntries[0].gameObject);
                spawnedEntries.RemoveAt(0);
            }

            UpdateListDisplay();
        }
    }

    //Update facts list
    public void UpdateListDisplay()
    {
        //Force deselect
        if (InterfaceController.Instance.selectedElement != null) InterfaceController.Instance.selectedElement.OnDeselect();

        if(tabState == TabState.inRoom)
        {
            inRoomButton.SetInteractable(false);
            inShopButton.SetInteractable(true);
            inStorageButton.SetInteractable(true);
        }
        else if(tabState == TabState.inShop)
        {
            inRoomButton.SetInteractable(true);
            inShopButton.SetInteractable(false);
            inStorageButton.SetInteractable(true);
        }
        else if(tabState == TabState.inStorage)
        {
            inRoomButton.SetInteractable(true);
            inShopButton.SetInteractable(true);
            inStorageButton.SetInteractable(false);
        }

        //Remove all if switched tabs
        while (spawnedEntries.Count > 0)
        {
            Destroy(spawnedEntries[0].gameObject);
            spawnedEntries.RemoveAt(0);
        }

        //Set checked/unchecked
        if (displayClasses.Contains(FurniturePreset.DecorClass.chairs)) chairsButton.icon.sprite = checkedSprite;
        else chairsButton.icon.sprite = uncheckedSprite;

        if (displayClasses.Contains(FurniturePreset.DecorClass.tables)) tablesButton.icon.sprite = checkedSprite;
        else tablesButton.icon.sprite = uncheckedSprite;

        if (displayClasses.Contains(FurniturePreset.DecorClass.units)) unitsButton.icon.sprite = checkedSprite;
        else unitsButton.icon.sprite = uncheckedSprite;

        if (displayClasses.Contains(FurniturePreset.DecorClass.electronics)) electronicsButton.icon.sprite = checkedSprite;
        else electronicsButton.icon.sprite = uncheckedSprite;

        if (displayClasses.Contains(FurniturePreset.DecorClass.structural)) structuralButton.icon.sprite = checkedSprite;
        else structuralButton.icon.sprite = uncheckedSprite;

        if (displayClasses.Contains(FurniturePreset.DecorClass.decoration)) decorationButton.icon.sprite = checkedSprite;
        else decorationButton.icon.sprite = uncheckedSprite;

        if (displayClasses.Contains(FurniturePreset.DecorClass.misc)) miscButton.icon.sprite = checkedSprite;
        else miscButton.icon.sprite = uncheckedSprite;

        rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 0);

        //Compile a list of file icons that should exist
        List<FurniturePreset> allRelevant = new List<FurniturePreset>();
        List<FurnitureLocation> allExisting = new List<FurnitureLocation>();

        List<FurniturePreset> required = new List<FurniturePreset>();
        List<FurnitureLocation> requiredExisting = new List<FurnitureLocation>();

        if (tabState == TabState.inRoom)
        {
            allExisting = new List<FurnitureLocation>(room.individualFurniture);
        }
        else if(tabState == TabState.inStorage)
        {
            allExisting = new List<FurnitureLocation>(PlayerApartmentController.Instance.furnitureStorage);
        }
        else if(tabState == TabState.inShop)
        {
            allRelevant = Toolbox.Instance.allFurniture.FindAll(item => item.purchasable && displayClasses.Contains(item.decorClass));

            foreach (FurniturePreset h in allRelevant)
            {
                if (required.Contains(h)) continue;

                if (searchInputField.text.Length <= 0)
                {
                    required.Add(h);
                }
                else
                {
                    string realText = Strings.Get("evidence.names", h.name);

                    if (realText.ToLower().Contains(searchInputField.text.ToLower()))
                    {
                        required.Add(h);
                    }
                }
            }
        }

        if(tabState == TabState.inRoom || tabState == TabState.inStorage)
        {
            foreach (FurnitureLocation f in allExisting)
            {
                if (requiredExisting.Contains(f)) continue;

                if (!displayClasses.Contains(f.furniture.decorClass)) continue;

                if (searchInputField.text.Length <= 0)
                {
                    requiredExisting.Add(f);
                }
                else
                {
                    string realText = Strings.Get("evidence.names", f.furniture.name);

                    if (realText.ToLower().Contains(searchInputField.text.ToLower()))
                    {
                        requiredExisting.Add(f);
                    }
                }
            }
        }

        //Sort required by price
        required.Sort((p1, p2) => p1.cost.CompareTo(p2.cost));
        requiredExisting.Sort((p1, p2) => p1.furniture.cost.CompareTo(p2.furniture.cost));

        //Go through existing list
        for (int i = 0; i < spawnedEntries.Count; i++)
        {
            DecorElementController sec = spawnedEntries[i];

            //If a null entry, this has obviously been destroyed- remove entry
            if (sec == null)
            {
                spawnedEntries.RemoveAt(i);
                i--;
                continue;
            }

            if (required.Contains(sec.furniture))
            {
                required.Remove(sec.furniture);
                sec.VisualUpdate();
            }
            else if(requiredExisting.Contains(sec.worldFurnitureReference))
            {
                required.Remove(sec.furniture);
                sec.VisualUpdate();
            }
            else
            {
                Destroy(sec.gameObject);
                spawnedEntries.RemoveAt(i);
                i--;
                continue;
            }
        }

        //The ones left in the found files list are buttons to spawn
        foreach (FurniturePreset sec in required)
        {
            GameObject newButton = null;

            newButton = Instantiate(furnitureElementPrefab, entryParent);

            DecorElementController newEntry = newButton.GetComponent<DecorElementController>();

            newEntry.SetupFurniture(sec, this, wcc.window, null);
            spawnedEntries.Add(newEntry);
        }

        foreach (FurnitureLocation sec in requiredExisting)
        {
            GameObject newButton = null;

            newButton = Instantiate(furnitureElementPrefab, entryParent);

            DecorElementController newEntry = newButton.GetComponent<DecorElementController>();

            newEntry.SetupFurniture(sec.furniture, this, wcc.window, sec);
            spawnedEntries.Add(newEntry);
        }

        for (int i = 0; i < spawnedEntries.Count; i++)
        {
            spawnedEntries[i].transform.SetAsLastSibling();
        }

        //Update rect sizes...
        entryParent.sizeDelta = new Vector2(entryParent.sizeDelta.x, Mathf.Max(spawnedEntries.Count * 130f + 24f, 466));
        //SetPageSize(new Vector2(rect.sizeDelta.x, entryParent.sizeDelta.y + 500f));
    }

    public void ToggleDisplayClass(int classInt)
    {
        FurniturePreset.DecorClass decorClass = (FurniturePreset.DecorClass)classInt;

        if(displayClasses.Contains(decorClass))
        {
            displayClasses.Remove(decorClass);
        }
        else
        {
            displayClasses.Add(decorClass);
        }

        UpdateListDisplay();
    }

    public void SetSelected(FurniturePreset newSelection, FurnitureLocation existingLocation, bool newPlaceExistingRoomObject)
    {
        Game.Log("Decor: Set selected furniture: " + newSelection);

        if(PlayerApartmentController.Instance.furniturePlacementMode)
        {
            Game.Log("Decor: Already in furniture placement mode, removing the furniture currently being placed...");
            PlayerApartmentController.Instance.RemoveBeingPlaced();
        }

        PlayerApartmentController.Instance.ResetExisting();

        if (newSelection != null)
        {
            Game.Log("Decor: Set selected furniture: " + newSelection);

            PlayerApartmentController.FurniturePlacement placement = new PlayerApartmentController.FurniturePlacement();
            placement.preset = newSelection;
            placement.existing = existingLocation;

            if (placement.existing != null && placement.existing.id != 0)
            {
                placement.materialKey = existingLocation.matKey;
                placement.art = existingLocation.art;

                //If existing furniture is selected, move it to storage
                PlayerApartmentController.Instance.MoveFurnitureToStorage(existingLocation);
            }

            PlayerApartmentController.Instance.SetFurniturePlacementMode(true, placement, room, newPlaceExistingRoomObject, forceUpdate: true);

            //You can only pick the colour on new purchases
            if (placement.existing == null || placement.existing.id <= 0)
            {
                //Open a material key window so we can pick the material
                PlayerApartmentController.Instance.OpenOrUpdateMaterialWindow(newSelection, new Toolbox.MaterialKey(), null);
            }
            else
            {
                SessionData.Instance.ResumeGame();
            }

            wcc.window.CloseWindow(false); //Close this
        }
        else if(PlayerApartmentController.Instance.furniturePlacementMode)
        {
            Game.Log("Decor: Cancelling furniture placement mode as selection is null");
            PlayerApartmentController.Instance.SetFurniturePlacementMode(false, null, null);
        }
    }

    public void ClearSearchButton()
    {
        searchInputField.text = string.Empty;
    }

    public void OnFurnitureChange()
    {
        UpdateListDisplay();
    }

    private void OnDestroy()
    {
        PlayerApartmentController.Instance.OnFurnitureChange -= OnFurnitureChange;

        PlayerApartmentController.Instance.rememberRoomStorageShop = tabState;
        PlayerApartmentController.Instance.rememberDisplayClasses = new List<FurniturePreset.DecorClass>(displayClasses);
    }

    public void MoveAllToStorageButton()
    {
        PopupMessageController.Instance.PopupMessage("Move to Storage", true, true, RButton: "Confirm");
        PopupMessageController.Instance.OnRightButton += ConfirmMoveToStorage;
        PopupMessageController.Instance.OnLeftButton += CancelMoveToStorage;
    }

    public void ConfirmMoveToStorage()
    {
        PopupMessageController.Instance.OnRightButton -= ConfirmMoveToStorage;
        PopupMessageController.Instance.OnLeftButton -= CancelMoveToStorage;

        Game.Log("Decor: Move all furniture to storage...");

        List<FurnitureLocation> allFurn = new List<FurnitureLocation>(room.individualFurniture);

        foreach (FurnitureLocation f in allFurn)
        {
            PlayerApartmentController.Instance.MoveFurnitureToStorage(f);
        }

        UpdateListDisplay();
    }

    public void CancelMoveToStorage()
    {
        PopupMessageController.Instance.OnRightButton -= ConfirmMoveToStorage;
        PopupMessageController.Instance.OnLeftButton -= CancelMoveToStorage;
    }
}
