﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    //string name = "controller";

    //Base class for controllers
    public virtual void SetupEvidence()
    {

    }

    public virtual void CreateEvidence()
    {
        return;
    }
}
