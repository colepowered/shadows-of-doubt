using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RainBlocker : MonoBehaviour
{
    public Collider rainCollider;

    private void OnEnable()
    {
        if(rainCollider != null)
        {
            rainCollider.isTrigger = true;
            this.gameObject.layer = 2;

            if(PrecipitationParticleSystemController.Instance != null) PrecipitationParticleSystemController.Instance.AddAreaTrigger(rainCollider);
        }
    }

    private void OnDestroy()
    {
        if (PrecipitationParticleSystemController.Instance != null) PrecipitationParticleSystemController.Instance.RemoveAreaTrigger(rainCollider);
    }
}
