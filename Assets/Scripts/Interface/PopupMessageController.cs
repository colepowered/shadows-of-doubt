﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using TMPro;
using UnityEngine.EventSystems;
using System.Linq;
using UnityEngine.Video;

public class PopupMessageController : MonoBehaviour
{
    [Header("References")]
    public RectTransform rect;
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI bodyText;
    public ButtonController leftButton;
    public ButtonController rightButton;
    public ButtonController leftButton2;
    public ButtonController rightButton2;
    public ButtonController optionButton;
    public TMP_InputField inputField;
    public MultiSelectController colourPicker;
    public List<LayoutGroup> buttonLayouts = new List<LayoutGroup>();

    public CanvasGroup canvasGroup;
    public CanvasRenderer vignetteRenderer;
    public GameObject vignetteObject;
    public List<GraphicRaycaster> otherCanvasRaycasters = new List<GraphicRaycaster>();

    [Space(7)]
    public RectTransform tutorialRect;
    public TextMeshProUGUI tutorialTitleText;
    public TextMeshProUGUI tutorialBodyText;
    public ButtonController tutorialLeftButton;
    public ButtonController tutorialRightButton;
    public InterfaceVideoController tutorialVideoPlayer;
    public List<LayoutGroup> tutorialButtonLayouts = new List<LayoutGroup>();
    public HelpContentPage helpPage;
    public int helpPageNumber = 0;
    public int maxHelpPageNumber = 0;
    public List<string> skipBlocks = new List<string>();
    public CanvasGroup tutorialCanvasGroup;

    [Header("State")]
    public bool active = false;
    public bool tutorialActive = false; //Used in combo with above for tutorial message
    public float appearProgress = 0f;
    public List<string> buttonActions = new List<string>();
    public bool anyButtonPressCloses = true;

    //Affect pause state: Automatic will attempt to detect the pause state from when the message was called
    public enum AffectPauseState { automatic, yes, no};
    public bool affectPauseState = true;
    private bool setupNav = false;

    //Event for location
    public delegate void LeftButton();
    public event LeftButton OnLeftButton;

    public delegate void RightButton();
    public event RightButton OnRightButton;

    public delegate void LeftButton2();
    public event LeftButton2 OnLeftButton2;

    public delegate void RightButton2();
    public event RightButton2 OnRightButton2;

    public delegate void OptionButton();
    public event OptionButton OnOptionButton;

    //Singleton pattern
    private static PopupMessageController _instance;
    public static PopupMessageController Instance { get { return _instance; } }

    public void Setup()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        colourPicker.SetChosen(0);
    }

    private void Update()
    {
        if(active && appearProgress < 1f)
        {
            appearProgress += 3.66f * Time.deltaTime;
            appearProgress = Mathf.Clamp01(appearProgress);
        }
        else if(!active && appearProgress > 0f)
        {
            appearProgress -= 10f * Time.deltaTime;
            appearProgress = Mathf.Clamp01(appearProgress);
        }
        else if(!active && appearProgress <= 0f)
        {
            vignetteObject.SetActive(false);
            rect.gameObject.SetActive(false); //set inactive
            tutorialRect.gameObject.SetActive(false); //set inactive
            return;
        }

        //If game isn't paused, pause it
        if(active)
        {
            if (affectPauseState && SessionData.Instance.play)
            {
                SessionData.Instance.PauseGame(true);
            }
        }

        float scale = Mathf.Max(appearProgress * appearProgress * appearProgress, 0.2f);

        if(!tutorialActive)
        {
            canvasGroup.alpha = scale;
            rect.localScale = new Vector3(scale, scale, scale);
        }
        else
        {
            tutorialCanvasGroup.alpha = scale;
            tutorialRect.localScale = new Vector3(scale, scale, scale);
        }

        vignetteRenderer.SetAlpha(scale);

        if(active && appearProgress >= 1f && !setupNav)
        {
            //List<Button> selectables = new List<Button>();

            //if(tutorialActive)
            //{
            //    if (tutorialLeftButton.gameObject.activeSelf) selectables.Add(tutorialLeftButton.button);
            //    if (tutorialRightButton.gameObject.activeSelf) selectables.Add(tutorialRightButton.button);
            //}
            //else
            //{
            //    if (leftButton.gameObject.activeSelf) selectables.Add(leftButton.button);
            //    if (rightButton.gameObject.activeSelf) selectables.Add(rightButton.button);
            //    if (leftButton2.gameObject.activeSelf) selectables.Add(leftButton2.button);
            //    if (rightButton2.gameObject.activeSelf) selectables.Add(rightButton2.button);
            //    if (optionButton.gameObject.activeSelf) selectables.Add(optionButton.button);
            //}

            //Toolbox.Instance.AutomaticNavigationSetup(ref selectables);

            InterfaceController.Instance.UpdateCursorSprite();

            //Colour picker
            if (!tutorialActive)
            {
                //if(colourPicker.gameObject.activeSelf)
                //{
                //    if(leftButton.gameObject.activeSelf)
                //    {
                //        Toolbox.Instance.AddNavigationInput(leftButton.button, newUp: colourPicker.optionButtons[0].button.button);
                //    }

                //    if (rightButton.gameObject.activeSelf)
                //    {
                //        Toolbox.Instance.AddNavigationInput(rightButton.button, newUp: colourPicker.optionButtons[colourPicker.optionButtons.Count - 1].button.button);
                //    }

                //    foreach(MultiSelectController.MultiSelectValue val in colourPicker.optionButtons)
                //    {
                //        if (rightButton.gameObject.activeSelf)
                //        {
                //            Toolbox.Instance.AddNavigationInput(val.button.button, newDown: rightButton.button);
                //        }
                //        else if(leftButton.gameObject.activeSelf)
                //        {
                //            Toolbox.Instance.AddNavigationInput(val.button.button, newDown: leftButton.button);
                //        }

                //        if(inputField.gameObject.activeSelf)
                //        {
                //            Toolbox.Instance.AddNavigationInput(val.button.button, newUp: inputField);
                //        }
                //    }

                //    if(inputField.gameObject.activeSelf)
                //    {
                //        Toolbox.Instance.AddNavigationInput(inputField, newDown: colourPicker.optionButtons[0].button.button);
                //    }
                //}
                ////Input field
                //else if (inputField.gameObject.activeSelf)
                //{
                //    if (leftButton.gameObject.activeSelf)
                //    {
                //        Toolbox.Instance.AddNavigationInput(leftButton.button, newUp: inputField);
                //    }

                //    if (rightButton.gameObject.activeSelf)
                //    {
                //        Toolbox.Instance.AddNavigationInput(rightButton.button, newUp: inputField);
                //    }

                //    if (rightButton.gameObject.activeSelf)
                //    {
                //        Toolbox.Instance.AddNavigationInput(inputField, newDown: rightButton.button);
                //    }
                //    else if (leftButton.gameObject.activeSelf)
                //    {
                //        Toolbox.Instance.AddNavigationInput(inputField, newDown: leftButton.button);
                //    }
                //}

                //Enable layout groups here for correct positions
                foreach (LayoutGroup lg in buttonLayouts)
                {
                    lg.enabled = true;
                }
            }
            else
            {
                //Enable layout groups here for correct positions
                foreach(LayoutGroup lg in tutorialButtonLayouts)
                {
                    lg.enabled = true;
                }
            }

            ButtonController[] allButtons = canvasGroup.gameObject.GetComponentsInChildren<ButtonController>(true);

            foreach(ButtonController bc in allButtons)
            {
                bc.RefreshAutomaticNavigation();
            }

            setupNav = true;
        }
    }

    public void PopupMessage(string newMsgString, bool enableLeftButton = true, bool enableRightButton = false, string LButton = "Cancel", string RButton = "", bool anyButtonClosesMsg = true, AffectPauseState newPauseState = AffectPauseState.automatic, bool enableInputField = false, string inputFieldDefault = "", bool closeMap = false, bool enableColourPicker = false, bool enableSecondaryLeftButton = false, bool enableSecondaryRightButton = false, string LButton2 = "", string RButton2 = "", bool enableOptionButton = false, string OButton = "")
    {
        if (active) return; //Return if already active
        setupNav = false;

        foreach (LayoutGroup lg in buttonLayouts)
        {
            lg.enabled = true;
        }

        //If this is selected, find another...
        if (InterfaceController.Instance.selectedElement != null)
        {
            Game.Log("Menu: Deselect button " + InterfaceController.Instance.selectedElement.name + " through selection");
            InterfaceController.Instance.selectedElement.OnDeselect();
        }

        //Reset current button down
        InputController.Instance.ResetCurrentButtonDown();

        //Camera needs to be active for this to work properly...
        if (!CameraController.Instance.cam.enabled)
        {
            CameraController.Instance.cam.enabled = true;
        }

        //Pause the game if needed
        if((newPauseState == AffectPauseState.automatic && SessionData.Instance.play) || newPauseState == AffectPauseState.yes)
        {
            affectPauseState = true;
        }
        else
        {
            affectPauseState = false;
        }

        //The game must be paused
        if(SessionData.Instance.play)
        {
            SessionData.Instance.PauseGame(true);
        }

        //Close the map
        if(closeMap)
        {
            MapController.Instance.CloseMap();
        }

        //Enable/disable input
        if(enableInputField)
        {
            inputField.text = inputFieldDefault;
            inputField.gameObject.SetActive(true);
        }
        else
        {
            inputField.gameObject.SetActive(false);
        }

        //Enable/disable colour picker
        if (enableColourPicker)
        {
            colourPicker.gameObject.SetActive(true);
        }
        else
        {
            colourPicker.gameObject.SetActive(false);
        }

        anyButtonPressCloses = anyButtonClosesMsg;

        buttonActions.Clear(); //Clear existing button actions

        //Set text
        titleText.text = Strings.Get("ui.popups", newMsgString + "_title");
        bodyText.text = Strings.ComposeText(Strings.Get("ui.popups", newMsgString + "_body"), null, linkSetting: Strings.LinkSetting.forceNoLinks);

        buttonActions.Add(LButton);
        buttonActions.Add(RButton);
        buttonActions.Add(LButton2);
        buttonActions.Add(RButton2);
        buttonActions.Add(OButton);

        if (enableRightButton)
        {
            rightButton.gameObject.SetActive(true);
            rightButton.SetInteractable(true);
            rightButton.text.text = Strings.Get("ui.popups", RButton);

            if (InterfaceController.Instance.selectedElement == null && !InputController.Instance.mouseInputMode)
            {
                rightButton.OnSelect();
            }
        }
        else
        {
            rightButton.SetInteractable(false);
            rightButton.gameObject.SetActive(false);
        }

        if (enableLeftButton)
        {
            leftButton.gameObject.SetActive(true);
            leftButton.SetInteractable(true);
            leftButton.text.text = Strings.Get("ui.popups", LButton);

            if (InterfaceController.Instance.selectedElement == null && !InputController.Instance.mouseInputMode)
            {
                leftButton.OnSelect();
            }
        }
        else
        {
            leftButton.SetInteractable(false);
            leftButton.gameObject.SetActive(false);
        }

        if (enableSecondaryRightButton)
        {
            rightButton2.gameObject.SetActive(true);
            rightButton2.SetInteractable(true);
            rightButton2.text.text = Strings.Get("ui.popups", RButton2);

            if (InterfaceController.Instance.selectedElement == null && !InputController.Instance.mouseInputMode)
            {
                rightButton2.OnSelect();
            }
        }
        else
        {
            rightButton2.SetInteractable(false);
            rightButton2.gameObject.SetActive(false);
        }


        if (enableSecondaryLeftButton)
        {
            leftButton2.gameObject.SetActive(true);
            leftButton2.SetInteractable(true);
            leftButton2.text.text = Strings.Get("ui.popups", LButton2);

            if (InterfaceController.Instance.selectedElement == null && !InputController.Instance.mouseInputMode)
            {
                leftButton2.OnSelect();
            }
        }
        else
        {
            leftButton2.SetInteractable(false);
            leftButton2.gameObject.SetActive(false);
        }

        if (enableOptionButton)
        {
            optionButton.gameObject.SetActive(true);
            optionButton.SetInteractable(true);
            optionButton.text.text = Strings.Get("ui.popups", OButton);

            if (InterfaceController.Instance.selectedElement == null && !InputController.Instance.mouseInputMode)
            {
                optionButton.OnSelect();
            }
        }
        else
        {
            optionButton.SetInteractable(false);
            optionButton.gameObject.SetActive(false);
        }

        rect.gameObject.SetActive(true);
        vignetteObject.SetActive(true);
        appearProgress = 0f; //Reset appear progress

        active = true;

        //Add graphic raycasters of all active windows to list
        foreach(InfoWindow win in InterfaceController.Instance.activeWindows)
        {
            GraphicRaycaster[] gr = win.GetComponentsInChildren<GraphicRaycaster>();

            if(gr != null)
            {
                otherCanvasRaycasters.AddRange(gr);
            }

            //Add pinned items
            if(win.currentPinnedCaseElement != null && win.currentPinnedCaseElement.pinnedController != null)
            {
                GraphicRaycaster[] gr2 = win.currentPinnedCaseElement.pinnedController.GetComponentsInChildren<GraphicRaycaster>();

                if (gr2 != null)
                {
                    otherCanvasRaycasters.AddRange(gr2);
                }
            }
        }

        //Set other raycasters to inactive
        foreach (GraphicRaycaster gr in otherCanvasRaycasters)
        {
            if(gr != null) gr.enabled = false;
        }

        foreach(LayoutGroup lg in buttonLayouts)
        {
            lg.enabled = false;
        }

        InterfaceController.Instance.UpdateCursorSprite();
    }

    public void TutorialMessage(string newHelpSection, AffectPauseState newPauseState = AffectPauseState.automatic, bool closeMap = false, List<string> newSkipBlocks = null)
    {
        if (active) return; //Return if already active
        if (!SessionData.Instance.startedGame) return; //Don't display until we're playing
        if (!SessionData.Instance.enableTutorialText) return;
        if (CityConstructor.Instance != null && CityConstructor.Instance.preSimActive) return;
        if (CityConstructor.Instance != null && CityConstructor.Instance.loadingOperationActive) return;

        helpPage = null;

        if (!Toolbox.Instance.allHelpContent.TryGetValue(newHelpSection, out helpPage))
        {
            Game.Log("Unable to find help content: " + newHelpSection);
            return;
        }

        setupNav = false;
        tutorialActive = true;
        helpPageNumber = 0;
        maxHelpPageNumber = 0;
        skipBlocks = newSkipBlocks;
        anyButtonPressCloses = false;

        foreach (LayoutGroup lg in tutorialButtonLayouts)
        {
            lg.enabled = true;
        }

        //If this is selected, find another...
        if (InterfaceController.Instance.selectedElement != null)
        {
            Game.Log("Menu: Deselect button " + InterfaceController.Instance.selectedElement.name + " through selection");
            InterfaceController.Instance.selectedElement.OnDeselect();
        }

        //Reset current button down
        InputController.Instance.ResetCurrentButtonDown();

        //Camera needs to be active for this to work properly...
        if (!CameraController.Instance.cam.enabled)
        {
            CameraController.Instance.cam.enabled = true;
        }

        //Pause the game if needed
        if ((newPauseState == AffectPauseState.automatic && SessionData.Instance.play) || newPauseState == AffectPauseState.yes)
        {
            affectPauseState = true;
        }
        else
        {
            affectPauseState = false;
        }

        //The game must be paused
        if (SessionData.Instance.play)
        {
            SessionData.Instance.PauseGame(true);
        }

        //Close the map
        if (closeMap)
        {
            MapController.Instance.CloseMap();
        }

        anyButtonPressCloses = false;

        //The first message should skip the first block (subject)
        SetHelpPage(0);

        tutorialRect.gameObject.SetActive(true);
        vignetteObject.SetActive(true);
        appearProgress = 0f; //Reset appear progress

        active = true;

        //Add graphic raycasters of all active windows to list
        foreach (InfoWindow win in InterfaceController.Instance.activeWindows)
        {
            GraphicRaycaster[] gr = win.GetComponentsInChildren<GraphicRaycaster>();

            if (gr != null)
            {
                otherCanvasRaycasters.AddRange(gr);
            }

            //Add pinned items
            if (win.currentPinnedCaseElement != null && win.currentPinnedCaseElement.pinnedController != null)
            {
                GraphicRaycaster[] gr2 = win.currentPinnedCaseElement.pinnedController.GetComponentsInChildren<GraphicRaycaster>();

                if (gr2 != null)
                {
                    otherCanvasRaycasters.AddRange(gr2);
                }
            }
        }

        //Set other raycasters to inactive
        foreach (GraphicRaycaster gr in otherCanvasRaycasters)
        {
            if (gr != null) gr.enabled = false;
        }

        foreach (LayoutGroup lg in tutorialButtonLayouts)
        {
            lg.enabled = false;
        }

        InterfaceController.Instance.UpdateCursorSprite();
    }

    public void SetHelpPage(int newNumber)
    {
        List<string> say = Player.Instance.ParseDDSMessage(helpPage.messageID, null, out _);
        if (say == null) return;

        maxHelpPageNumber = say.Count - 1;

        if (skipBlocks != null)
        {
            maxHelpPageNumber = say.FindAll(item => !skipBlocks.Contains(item)).Count - 1;
        }

        helpPageNumber = Mathf.Clamp(newNumber, 0, maxHelpPageNumber);
        Game.Log("Set help page: " + helpPageNumber + "/" + maxHelpPageNumber);

        tutorialTitleText.text = Strings.Get("ui.handbook", helpPage.name) + " (" + (helpPageNumber + 1) + ")";//" + "|" + (maxHelpPageNumber + 1) + ")";

        //Get text
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        int pageCursor = 0;

        for (int i = 0; i < say.Count; i++)
        {
            if (skipBlocks != null && skipBlocks.Contains(say[i])) continue;

            if(helpPageNumber == pageCursor)
            {
                tutorialBodyText.text = Strings.ComposeText(Strings.Get("dds.blocks", say[i]), null, Strings.LinkSetting.forceLinks);

                //Load clip
                if (helpPage.contentDisplay.Count >  i)
                {
                    tutorialVideoPlayer.Setup(helpPage.contentDisplay[i].clip, helpPage.contentDisplay[i].image);
                }

                break;
            }
            else
            {
                pageCursor++;
            }
        }

        buttonActions.Clear(); //Clear existing button actions
        buttonActions.Add("Previous");

        bool enableRightButton = true;
        bool enableLeftButton = true;
        string RButton = "Next";

        if (pageCursor <= 0)
        {
            enableLeftButton = false;
            buttonActions.Add("Next");
        }
        else if(pageCursor >= maxHelpPageNumber)
        {
            RButton = "Continue";
            buttonActions.Add("Continue");
        }
        else
        {
            buttonActions.Add("Next");
        }

        if (enableRightButton)
        {
            tutorialRightButton.gameObject.SetActive(true);
            tutorialRightButton.SetInteractable(true);
            tutorialRightButton.text.text = Strings.Get("ui.popups", RButton);

            if (InterfaceController.Instance.selectedElement == null && !InputController.Instance.mouseInputMode)
            {
                tutorialRightButton.OnSelect();
            }
        }
        else
        {
            tutorialRightButton.SetInteractable(false);
            tutorialRightButton.gameObject.SetActive(false);
        }

        if (enableLeftButton)
        {
            tutorialLeftButton.gameObject.SetActive(true);
            tutorialLeftButton.SetInteractable(true);
            tutorialLeftButton.text.text = Strings.Get("ui.popups", "Previous");

            if (InterfaceController.Instance.selectedElement == null && !InputController.Instance.mouseInputMode)
            {
                tutorialLeftButton.OnSelect();
            }
        }
        else
        {
            tutorialLeftButton.SetInteractable(false);
            tutorialLeftButton.gameObject.SetActive(false);
        }
    }

    public void RemoveMessage()
    {
        active = false;
        tutorialActive = false;
        buttonActions.Clear(); //Clear existing button actions

        InterfaceController.Instance.SetPlayerTextInput(false); //Make sure text input is cancelled

        //Set other raycasters to active
        foreach (GraphicRaycaster gr in otherCanvasRaycasters)
        {
            if (gr != null) gr.enabled = true;
        }

        if (affectPauseState)
        {
            SessionData.Instance.ResumeGame();
        }

        //If this is selected, find another...
        if (InterfaceController.Instance.selectedElement != null)
        {
            Game.Log("Menu: Deselect button " + InterfaceController.Instance.selectedElement.name + " through selection");
            InterfaceController.Instance.selectedElement.OnDeselect();
        }
    }

    public void OnButtonPress(int buttonVal)
    {
        if (!active || appearProgress < 1f) return; //Ignore if already inactive

        //Game.Log("Popup button: " + buttonVal);
        string action = buttonActions[buttonVal];

        //Remove message
        if(action == "Cancel")
        {
            RemoveMessage();
        }
        else if(tutorialActive)
        {
            if (action == "Continue") RemoveMessage();
            else if (action == "Next") SetHelpPage(helpPageNumber + 1);
            else if (action == "Previous") SetHelpPage(helpPageNumber - 1);
        }

        //Fire events
        if(buttonVal == 0)
        {
            if(OnLeftButton != null)
            {
                OnLeftButton();
            }
        }
        else if (buttonVal == 1)
        {
            if (OnRightButton != null)
            {
                OnRightButton();
            }
        }
        else if (buttonVal == 2)
        {
            if (OnLeftButton2 != null)
            {
                OnLeftButton2();
            }
        }
        else if (buttonVal == 3)
        {
            if (OnRightButton2 != null)
            {
                OnRightButton2();
            }
        }
        else if (buttonVal == 4)
        {
            if (OnOptionButton != null)
            {
                OnOptionButton();
            }
        }

        if (anyButtonPressCloses)
        {
            RemoveMessage();
        }
    }
}
