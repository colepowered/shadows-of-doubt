﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamController : MonoBehaviour
{
    public NewRoom room;
    [Tooltip("The steam level previously existing in this room")]
    private float existingSteamLevel = 0f;
    [Tooltip("The steam level in this room.")]
    public float steamLevel = 0;
    [Tooltip("The scale to blur glass panels.")]
    public Vector2 blurScale = new Vector2(0.05f, 0.75f);
    [Tooltip("The time it takes to completely steam up a room")]
    public float steamTime = 0.2f;
    [Tooltip("The time it takes to completely de-steam a room")]
    public float desteamTime = 0.6f;

    public List<MeshRenderer> glassPanels;
    public Material glassMaterialOriginal;
    public Material glassMaterial;

    public void Setup(NewRoom newRoom)
    {
        room = newRoom;
        room.steamController = this;

        //Create an instance of the material and apply it to the panels
        glassMaterial = Instantiate(glassMaterialOriginal);

        //Set blur to the steam level
        glassMaterial.SetFloat("_DistortionBlurScale", Mathf.Lerp(blurScale.x, blurScale.y, steamLevel));

        foreach(MeshRenderer m in glassPanels)
        {
            m.sharedMaterial = glassMaterial;
        }

        //Force an update of the steam state
        SteamStateChanged();
    }

    //Triggered when the steam state is changed in the room
    public void SteamStateChanged()
    {
        //Determin steam level
        existingSteamLevel = steamLevel;
        this.enabled = true;
    }

    private void Update()
    {
        if (room == null) return;

        //Determin steam level based on last switch
        if(room.steamOn)
        {
            steamLevel = Mathf.Clamp01(existingSteamLevel + ((SessionData.Instance.gameTime - room.steamLastSwitched) / steamTime));
        }
        else
        {
            steamLevel = Mathf.Clamp01(existingSteamLevel - ((SessionData.Instance.gameTime - room.steamLastSwitched) / desteamTime));
        }

        //Set glass blur the to steam level
        glassMaterial.SetFloat("_DistortionBlurScale", Mathf.Lerp(blurScale.x, blurScale.y, steamLevel));

        //Update main lights to set correct atmosphere
        foreach(Interactable lc in room.mainLights)
        {
            //Use normal value as the 'baseline' to add to
            if(lc.lightController != null)
            {
                lc.lightController.SetVolumentricAtmosphere(room.preset.baseRoomAtmosphere * lc.lightController.preset.atmosphereMultiplier + steamLevel);
            }
        }

        //Disable update
        if(room.steamOn && steamLevel >= 1f)
        {
            this.enabled = false;
        }
        else if(!room.steamOn && steamLevel <= 0f)
        {
            this.enabled = false;
        }
    }
}
