﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSelectController : MonoBehaviour
{
    [Header("References")]
    public RectTransform pageRect;
    public WindowContentController wcc;

    [Header("Prefabs")]
    public GameObject selectPrefab;

    private List<ItemSelectButtonController> spawned = new List<ItemSelectButtonController>();

    public void Setup(WindowContentController newWcc)
    {
        Game.Log("Interface: Setting up item select controller...");
        wcc = newWcc;

        //Remove existing
        while(spawned.Count > 0)
        {
            Destroy(spawned[0].gameObject);
            spawned.RemoveAt(0);
        }

        //Get items from inventory

        float xPos = 16;
        float yPos = -16;

        foreach(FirstPersonItemController.InventorySlot slot in FirstPersonItemController.Instance.slots)
        {
            if (slot == null) continue;
            if (slot.isStatic != FirstPersonItemController.InventorySlot.StaticSlot.nonStatic) continue;

            Interactable obj = slot.GetInteractable();

            if(obj != null)
            {
                GameObject newObject = Instantiate(selectPrefab, pageRect);
                ItemSelectButtonController psbc = newObject.GetComponent<ItemSelectButtonController>();
                //spawned.Add(psbc);

                psbc.rect.anchoredPosition = new Vector2(xPos, yPos);

                psbc.Setup(obj, wcc.window);

                xPos += psbc.rect.sizeDelta.x + 10f;

                if(xPos > 500)
                {
                    xPos = 12;
                    yPos -= psbc.rect.sizeDelta.y + 10f;
                }

                pageRect.sizeDelta = new Vector2(pageRect.sizeDelta.x, -yPos + psbc.rect.sizeDelta.y + 32f);
            }
        }
    }
}
