﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "spatter_data", menuName = "Database/Spatter Pattern")]

public class SpatterPatternPreset : SoCustomComparison
{
    [Header("Configuration")]
    public int spatterCount = 2000;
    public float maxAngleX = 1f;
    public float maxAngleY = 1f;
    [MinMaxSlider(0f, 10f)]
    public Vector2 rayLength;
    public AnimationCurve spreadCurve;
    public Material heavyMaterial;
    public Material mediumMaterial;
    public Material lightMaterial;
}
