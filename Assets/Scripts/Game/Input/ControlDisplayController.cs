using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using NaughtyAttributes;
using UnityEngine.UI;

public class ControlDisplayController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public TextMeshProUGUI controlText;
    public List<CanvasRenderer> renderers = new List<CanvasRenderer>();
    public Image background;
    public SoundIndicatorController soundIndicator;
    public JuiceController juiceController;

    [Header("State")]
    public InteractablePreset.InteractionKey key;
    public InteractionController.InteractionSetting interactionSetting;
    public float fadeIn = 0f;
    public bool remove = false;
    public ControlPositioning positioning = ControlPositioning.neutral;
    public Vector2 desiredPosition = Vector2.zero;
    public Vector2 spawnPosition = Vector2.zero;
    public bool assignedSpawnPosition = false;
    public bool execute = false;
    public float executeProgress = 0f;

    [Header("Debug")]
    public string actionName = "Jump";

    public enum ControlPositioning { neutral, up, down, left, right };

    public bool UpdateDisplay(InteractablePreset.InteractionKey newKey, InteractionController.InteractionSetting newAction)
    {
        bool ret = true;
        key = newKey;
        interactionSetting = newAction;

        interactionSetting.newUIRef = this;

        //Use item context?
        bool useContext = false;

        if(FirstPersonItemController.Instance.currentItem != null && FirstPersonItemController.Instance.currentItem != GameplayControls.Instance.nothingItem)
        {
            useContext = true;
        }

        ret = SetControlText(key, interactionSetting.actionText, useContext);

        if(interactionSetting.interactable != null && interactionSetting.currentAction != null)
        {
            //Toggle glow
            if (interactionSetting.interactable.highlightActions.Contains(interactionSetting.currentAction))
            {
                juiceController.Pulsate(true);
            }
            else juiceController.Pulsate(false);
        }

        if(interactionSetting.currentSetting != null)
        {
            juiceController.Pulsate(interactionSetting.currentSetting.highlight);
        }

        return ret;
    }

    private void Update()
    {
        //Handle fade in/remove
        if(fadeIn < 1f && !remove)
        {
            fadeIn += Time.deltaTime / 0.1f;
            fadeIn = Mathf.Clamp01(fadeIn);

            foreach(CanvasRenderer r in renderers)
            {
                if (r == null) continue;
                r.SetAlpha(fadeIn);
            }

            if(fadeIn >= 1f && !execute)
            {
                this.enabled = false;
            }
        }
        else if(remove)
        {
            fadeIn -= Time.deltaTime / 0.1f;
            fadeIn = Mathf.Clamp01(fadeIn);

            foreach (CanvasRenderer r in renderers)
            {
                if (r == null) continue;
                r.SetAlpha(fadeIn);
            }

            rect.anchoredPosition = Vector2.Lerp(rect.anchoredPosition, spawnPosition, Time.deltaTime / 0.1f);

            if(fadeIn <= 0f)
            {
                Destroy(this.gameObject);
            }
        }

        if(execute)
        {
            executeProgress += Time.deltaTime / ControlsDisplayController.Instance.animationSelectTime;
            executeProgress = Mathf.Clamp01(executeProgress);

            float anim = ControlsDisplayController.Instance.controlSelectAnimation.Evaluate(executeProgress);
            float s = 1f + anim * ControlsDisplayController.Instance.controlSelectScaleLerp;
            rect.localScale = new Vector3(s, s, s);

            background.color = Color.Lerp(Color.black, ControlsDisplayController.Instance.controlSelectColorLerp, anim);

            if (executeProgress >= 1f)
            {
                execute = false;
                executeProgress = 0f;
            }
        }
    }

    private void OnEnable()
    {
        InputController.Instance.OnInputModeChange += RefreshIcon;
    }

    private void OnDisable()
    {
        InputController.Instance.OnInputModeChange -= RefreshIcon;
    }

    public void RefreshIcon()
    {
        //Use item context?
        bool useContext = false;

        if (FirstPersonItemController.Instance.currentItem != null && FirstPersonItemController.Instance.currentItem != GameplayControls.Instance.nothingItem)
        {
            useContext = true;
        }

        SetControlText(key, interactionSetting.actionText, useContext);
    }

    public void Remove()
    {
        remove = true;
        ControlsDisplayController.Instance.spawned.Remove(this);

        try
        {
            this.enabled = true;
        }
        catch
        {

        }
    }

    public void Execute()
    {
        if(!execute)
        {
            juiceController.Pulsate(false, false);
            execute = true;
            executeProgress = 0f;

            try
            {
                this.enabled = true;
            }
            catch
            {

            }
        }
    }

    public bool SetControlText(InteractablePreset.InteractionKey key, string newText, bool useContext = false)
    {
        bool ret = true;
        string context = string.Empty;

        if(useContext)
        {
            string contextName = string.Empty;

            if (interactionSetting.isFPSItem && FirstPersonItemController.Instance.currentItem != null)
            {
                if(!FirstPersonItemController.Instance.currentItem.disableBracketDisplayName)
                {
                    Interactable slotInteractable = BioScreenController.Instance.selectedSlot.GetInteractable();
                    if (slotInteractable != null) contextName = slotInteractable.GetName();
                    else
                    {
                        FirstPersonItem slotItem = BioScreenController.Instance.selectedSlot.GetFirstPersonItem();
                        if(slotItem != null) contextName = Strings.Get("ui.interface", slotItem.name, Strings.Casing.firstLetterCaptial);
                    }
                }
            }
            else if (interactionSetting.interactable != null)
            {
                contextName = interactionSetting.interactable.GetName();
            }

            if (contextName.Length > 0) context = " [" + contextName + "]";
        }

        string actionCost = string.Empty;

        int cost = interactionSetting.GetActionCost();

        if(cost != 0)
        {
            actionCost = " [" + CityControls.Instance.cityCurrency + cost + "]";
        }

        controlText.SetText(ControlsDisplayController.Instance.GetControlIcon(key, out positioning, out ret) + " " + newText + actionCost + context);

        //if(controlText.textInfo.characterInfo[0].elementType != TMP_TextElementType.Sprite)
        //{
        //    Game.Log("Missing sprite in control text!");

        //    string spriteCategory = "desktop";
        //    string spriteName = "Keyboard Key";
        //    string extraKeyText = string.Empty;

        //    controlText.text = "<sprite=\"" + spriteCategory + "\" name=\"" + spriteName + "\">" + extraKeyText;
        //}

        this.name = controlText.text;

        controlText.ForceMeshUpdate();
        renderers.Clear();
        renderers.AddRange(this.gameObject.GetComponentsInChildren<CanvasRenderer>());

        if(interactionSetting.currentSetting.forcePositioning)
        {
            positioning = interactionSetting.currentSetting.forcePosition;
        }

        return ret;
    }
}
