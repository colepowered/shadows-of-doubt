﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidencePrintedVmail : Evidence
{
    public int threadID;
    public int msgIndexID;

    public StateSaveData.MessageThreadSave thread;

    //Constructor
    public EvidencePrintedVmail(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        for (int i = 0; i < newPassedObjects.Count; i++)
        {
            object obj = newPassedObjects[i];

            Interactable.Passed passed = obj as Interactable.Passed; //Company is passed

            if (passed != null)
            {
                if(passed.varType == Interactable.PassedVarType.vmailThreadID)
                {
                    threadID = (int)passed.value;

                    if(!GameplayController.Instance.messageThreads.TryGetValue((int)passed.value, out thread))
                    {
                        Game.LogError("Unable to find message thread " + (int)passed.value + " (message threads: " + GameplayController.Instance.messageThreads.Count + ")");
                    }
                }
                else if (passed.varType == Interactable.PassedVarType.vmailThreadMsgIndex)
                {
                    msgIndexID = (int)passed.value;
                }
            }

            Interactable inter = obj as Interactable;

            if (inter != null)
            {
                interactable = inter;
                interactablePreset = interactable.preset;
            }
        }
    }
}
