﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//Generates districts within the city
//Script pass 1
public class DistrictsCreator : Creator
{
	//Singleton pattern
	private static DistrictsCreator _instance;
    public static DistrictsCreator Instance { get { return _instance; } }

    public class DistrictPlacement
    {
        public float score = 0;
        public List<CityTile> tiles = new List<CityTile>();
        public List<CityTile> innerTiles = new List<CityTile>();
        public List<CityTile> edgeTiles = new List<CityTile>();
    }

	void Awake ()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
	}

    public override void StartLoading()
    {
        if(CityConstructor.Instance.generateNew)
        {
            Game.Log("CityGen: Generating districts...");

            //Set the cursor to the beginning
            Vector2 districtCursor = Vector2.zero;

            string seed = CityData.Instance.seed;

            //Used to calculate progress
            int citySquaresProgress = 0;
            int citySquaresTotal = Mathf.RoundToInt(CityData.Instance.citySize.x * CityData.Instance.citySize.y);
            int loopFailSafe = Toolbox.Instance.allDistricts.Count;

            List<DistrictPreset> districtsOpenSet = new List<DistrictPreset>(Toolbox.Instance.allDistricts);

            while (citySquaresProgress < citySquaresTotal && loopFailSafe > 0)
            {
                //If no district presets left, repopulate the list
                if (districtsOpenSet.Count <= 0) districtsOpenSet.AddRange(Toolbox.Instance.allDistricts.FindAll(item => !item.limitToOne));

                //Sort open set by priority- we may get some randomization from this...
                districtsOpenSet.Sort((p1, p2) => Toolbox.Instance.GetPsuedoRandomNumberContained(p2.generationPriority.x, p2.generationPriority.y, seed, out seed).CompareTo(Toolbox.Instance.GetPsuedoRandomNumberContained(p1.generationPriority.x, p1.generationPriority.y, seed, out seed)));

                //Pick district
                DistrictPreset newDistrict = districtsOpenSet[0];
                districtsOpenSet.RemoveAt(0);

                //Determin size
                int cityTiles = Mathf.RoundToInt(newDistrict.cityRatio * (CityData.Instance.citySize.x - 2) * (CityData.Instance.citySize.y - 2));
                cityTiles = Mathf.Clamp(cityTiles, newDistrict.minimumSize, newDistrict.maximumSize);

                List<DistrictPlacement> possiblePlacements = new List<DistrictPlacement>();

                for (int i = 1; i < CityData.Instance.citySize.x - 1; i++)
                {
                    for (int n = 1; n < CityData.Instance.citySize.y - 1; n++)
                    {
                        //Select new cursor pos on tile found with no assigned district.
                        Vector2Int thisVector = new Vector2Int(i, n);
                        CityTile tile = null;

                        if (CityData.Instance.cityTiles.TryGetValue(thisVector, out tile))
                        {
                            if (tile.district != null) continue;

                            //Flood fill method...
                            DistrictPlacement newPlacement = new DistrictPlacement { score = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 0.2f, seed, out seed) };

                            bool coastPass = true;
                            if (newDistrict.mustBeOnCoast) coastPass = false;

                            HashSet<CityTile> openSet = new HashSet<CityTile>();
                            openSet.Add(tile);

                            HashSet<CityTile> closedSet = new HashSet<CityTile>();

                            //Continue while there are open spaces and the maximum tiles count is not met...
                            int failsafe = 999;

                            while(openSet.Count > 0 && newPlacement.innerTiles.Count < cityTiles && failsafe > 0)
                            {
                                CityTile[] asArray = openSet.ToArray();
                                CityTile current = asArray[Toolbox.Instance.GetPsuedoRandomNumberContained(0, asArray.Length, seed, out seed)]; //Pick random as a backup

                                //Choose the next current tile (to add) based on how many existing assigned tiles are around it...
                                float existingScore = 0;

                                foreach(CityTile t in openSet)
                                {
                                    float thisScore = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 0.2f, seed, out seed);

                                    //Add score based on surrounding picked tiles to create a more uniform shape...
                                    foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                                    {
                                        Vector2 searchVector = t.cityCoord + v2;

                                        if(newPlacement.tiles.Exists(item => item.cityCoord == searchVector))
                                        {
                                            thisScore++;
                                        }
                                    }

                                    if(thisScore > existingScore)
                                    {
                                        existingScore = thisScore;
                                        current = t;
                                    }
                                }

                                bool currentIsCoast = false;

                                //Add this tile to possible placement...
                                if (current.district == null)
                                {
                                    newPlacement.tiles.Add(current);

                                    if (current.cityCoord.x == 0 || current.cityCoord.y == 0 || current.cityCoord.x == CityData.Instance.citySize.x - 1 || current.cityCoord.y == CityData.Instance.citySize.y - 1)
                                    {
                                        newPlacement.edgeTiles.Add(current);
                                        coastPass = true;
                                        currentIsCoast = true;
                                    }
                                    else
                                    {
                                        float newScore = 1f;
                                        newScore += Vector2.Distance(current.cityCoord, new Vector2((CityData.Instance.citySize.x - 1) * 0.5f, (CityData.Instance.citySize.y - 1) * 0.5f)) * newDistrict.centreWeighting;
                                        newPlacement.score += newScore;
                                        newPlacement.innerTiles.Add(current);
                                    }
                                }

                                //Expand search in 4 directions
                                if(!currentIsCoast)
                                {
                                    foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                                    {
                                        Vector2Int searchVector = current.cityCoord + v2;
                                        CityTile expandTile = null;

                                        if (CityData.Instance.cityTiles.TryGetValue(searchVector, out expandTile))
                                        {
                                            if (expandTile.district != null) continue;
                                            if (openSet.Contains(expandTile)) continue;
                                            if (closedSet.Contains(expandTile)) continue;

                                            openSet.Add(expandTile);
                                        }
                                    }
                                }

                                closedSet.Add(current);
                                openSet.Remove(current);
                                failsafe--;

                                if(failsafe <= 0)
                                {
                                    Game.LogError("District failsafe reached! Check your generation code...");
                                }
                            }

                            //Must pass coast test and be above minimum size
                            if(coastPass && newPlacement.innerTiles.Count >= newDistrict.minimumSize)
                            {
                                possiblePlacements.Add(newPlacement);
                            }
                        }
                    }
                }

                if (possiblePlacements.Count > 0)
                {
                    possiblePlacements.Sort((p1, p2) => p2.score.CompareTo(p1.score));
                    DistrictPlacement chosen = possiblePlacements[0];

                    //Spawn district object
                    GameObject newDist = Instantiate(PrefabControls.Instance.district, PrefabControls.Instance.cityContainer.transform);
                    DistrictController d = newDist.GetComponent<DistrictController>();
                    d.Setup(newDistrict);

                    foreach (CityTile ct in chosen.tiles)
                    {
                        //Assign the district to these tiles
                        d.AddCityTile(ct);
                    }

                    citySquaresProgress += chosen.tiles.Count;

                    Game.Log("CityGen: District " + newDistrict.name + " contains " + chosen.innerTiles.Count + " iiner tiles and " + chosen.edgeTiles.Count + " edge tiles, remaining city tiles: " + (citySquaresTotal - citySquaresProgress));
                }
                else
                {
                    //If we've hit this then there are possible isolated tiles
                    loopFailSafe--;
                }

                //Calculate ~progress
                CityConstructor.Instance.loadingProgress = (float)citySquaresProgress / (float)citySquaresTotal;
            }

            //Clean up any tiles that aren't assign to a district by expanding existing districts into them using flood fill
            int failSafe = 999;

            while(citySquaresProgress < citySquaresTotal && failSafe > 0)
            {
                List<DistrictController> districts = new List<DistrictController>(CityData.Instance.districtDirectory);

                while(districts.Count > 0)
                {
                    DistrictController dist = districts[Toolbox.Instance.GetPsuedoRandomNumberContained(0, districts.Count, seed, out seed)]; //Pick random as a backup

                    //Chose a district that's furthest away from it's maximum tiles...
                    int maxDiff = 0;

                    foreach(DistrictController d in districts)
                    {
                        int thisDiff = d.preset.maximumSize - d.cityTiles.Count;

                        if(thisDiff > maxDiff)
                        {
                            maxDiff = thisDiff;
                            dist = d;
                        }
                    }

                    HashSet<CityTile> openSet = new HashSet<CityTile>(dist.cityTiles);
                    HashSet<CityTile> closedSet = new HashSet<CityTile>();

                    while(openSet.Count > 0)
                    {
                        CityTile[] asArray = openSet.ToArray();
                        CityTile current = asArray[Toolbox.Instance.GetPsuedoRandomNumberContained(0, asArray.Length, seed, out seed)]; //Pick random as a backup

                        //Add tile
                        if(current.district == null && !dist.cityTiles.Contains(current))
                        {
                            dist.AddCityTile(current);
                            citySquaresProgress++;

                            Game.Log("CityGen: Cleaned up isolated tile " + current.cityCoord + " by adding it to " + dist.preset.name + ", remaining city tiles: " + (citySquaresTotal - citySquaresProgress));
                            break;
                        }

                        //Expand search in 4 directions
                        foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                        {
                            Vector2Int searchVector = current.cityCoord + v2;
                            CityTile expandTile = null;

                            if (CityData.Instance.cityTiles.TryGetValue(searchVector, out expandTile))
                            {
                                if (expandTile.district != null) continue;
                                if (openSet.Contains(expandTile)) continue;
                                if (closedSet.Contains(expandTile)) continue;

                                openSet.Add(expandTile);
                            }
                        }

                        if(!closedSet.Contains(current)) closedSet.Add(current);
                        openSet.Remove(current);
                    }

                    districts.Remove(dist);
                }

                failSafe--;

                if(failSafe <= 0)
                {
                    Game.LogError("District failsafe triggered! Check your code...");
                }

                //Calculate ~progress
                CityConstructor.Instance.loadingProgress = (float)citySquaresProgress / (float)citySquaresTotal;
            }
        }
        else
        {
            Game.Log("CityGen: Loading districts...");

            //Load criminals
            foreach (CitySaveData.OccupationCitySave cr in CityConstructor.Instance.currentData.criminals)
            {
                //OccupationController addJob = CitizenCreator.Instance.criminalHolder.AddComponent<OccupationController>();
                Occupation addJob = new Occupation();
                addJob.Load(cr, null);
                CityData.Instance.jobsDirectory.Add(addJob);
                CityData.Instance.criminalJobDirectory.Add(addJob);
            }

            //Create districts & blocks
            foreach (CitySaveData.DistrictCitySave dist in CityConstructor.Instance.currentData.districts)
            {
                //Spawn district object
                GameObject newDist = Instantiate(PrefabControls.Instance.district, PrefabControls.Instance.cityContainer.transform);
                DistrictController d = newDist.GetComponent<DistrictController>();
                d.Load(dist);
            }

            //Assign tiles to district & blocks
            foreach (KeyValuePair<Vector2Int, CityTile> pair in CityData.Instance.cityTiles)
            {
                DistrictController dist = CityData.Instance.districtDirectory.Find(item => item.districtID == pair.Value.districtID);
                dist.AddCityTile(pair.Value);

                BlockController block = CityData.Instance.blocksDirectory.Find(item => item.blockID == pair.Value.blockID);
                block.AddCityTile(pair.Value);

                //Is this the border block?
                if (pair.Key.x == 0 || pair.Key.x == CityData.Instance.citySize.x - 1 || pair.Key.y == 0 || pair.Key.y == CityData.Instance.citySize.y - 1)
                {
                    CityData.Instance.borderBlock = block;
                }
            }
        }

        Game.Log("CityGen: " + CityData.Instance.districtDirectory.Count + " districts generated...");

        //Complete!
        SetComplete();
	}
}
