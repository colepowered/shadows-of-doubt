﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Machine : Actor
{
    //[Header("Machine Attributes")]

    private void Awake()
    {
        isMachine = true;
        isSeenByOthers = false;
    }
}
