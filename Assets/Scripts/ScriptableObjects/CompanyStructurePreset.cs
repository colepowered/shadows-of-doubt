﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make company presets.
//Script pass 1
[CreateAssetMenu(fileName = "company_structure_data", menuName = "Database/Company/Structure Preset")]

public class CompanyStructurePreset : SoCustomComparison
{
    [System.Serializable]
    public class OccupationSettings
    {
        public OccupationPreset occupation;
        public int positionsMinimum = 1;
        public int positionsMaximum = 1;
        [Range(0f, 1f)]
        public float payGrade = 0.5f;
    }

    [System.Serializable]
    public class BossConfig : OccupationSettings
    {
        [Header("Is Boss Of...")]
        public List<Hierarchy1Config> subordinates = new List<Hierarchy1Config>();
    }

    [System.Serializable]
    public class Hierarchy1Config : OccupationSettings
    {
        [Header("Is Boss Of...")]
        public List<Hierarchy2Config> subordinates = new List<Hierarchy2Config>();
    }

    [System.Serializable]
    public class Hierarchy2Config : OccupationSettings
    {
        [Header("Is Boss Of...")]
        public List<Hierarchy3Config> subordinates = new List<Hierarchy3Config>();
    }

    [System.Serializable]
    public class Hierarchy3Config : OccupationSettings
    {
        [Header("Is Boss Of...")]
        public List<OccupationSettings> subordinates = new List<OccupationSettings>();
    }

    [System.Serializable]
    public class Hierarchy4Config : OccupationSettings
    {

    }

    [Header("Company Structure")]
    public BossConfig companyStructure;
}
