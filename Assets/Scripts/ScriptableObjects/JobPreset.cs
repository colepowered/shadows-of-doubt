﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "sidejob_data", menuName = "Database/Job Preset")]

public class JobPreset : SoCustomComparison
{
    public enum JobTag { A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z };

    [System.Serializable]
    public class StartingScenario
    {
        public string name;
        public string dds;
        [Space(5)]
        public List<StartingLead> leads = new List<StartingLead>();
    }

    [System.Serializable]
    public class StartingLead
    {
        public LeadEvidence leadEvidence;
        [Space(5)]

        [HideIf("useKeyFromLeadPool")]
        public List<Evidence.DataKey> keys = new List<Evidence.DataKey>();
        [Tooltip("Add to the above with keys from the lead pool (chosen first)")]
        public bool useKeyFromLeadPool = false;
        public bool autoPin = false;
        [Space(5)]
        [Tooltip("Add these dialog options to the above person")]
        public List<DialogPreset> addDialogOptions = new List<DialogPreset>();
        [Tooltip("Add this fact link to the post facts section")]
        public List<string> factsReveal = new List<string>();
        public List<Evidence.DataKey> mergeKeys = new List<Evidence.DataKey>();
        public List<Evidence.Discovery> discoveryApplication = new List<Evidence.Discovery>();
    }

    [System.Serializable]
    public class FactCreation
    {
        public FactPreset factPreset;
        public LeadEvidence from;
        public LeadEvidence to;

        [Space(5)]
        public bool overrideFromKeys = false;
        public List<Evidence.DataKey> fromKeys = new List<Evidence.DataKey>();
        public bool featureKeysFromLeadPool = true;

        [Space(5)]
        public bool overrideToKeys = false;
        public List<Evidence.DataKey> toKeys = new List<Evidence.DataKey>();
        public bool featureKeysFromLeadPoolTo = false;
    }

    public enum LeadEvidence { none, poster, purp, purpsParamour, postersHome, purpsHome, purpsParamourHome, postersWorkplace, purpsWorkplace, purpsParamourWorkplace, postersBuilding, purpsBuilding, purpsParamourBuilding, post, posterTelephone, purpsTelephone, purpsParamourTelephone, postersWorkplaceBuilding, purpsWorkplaceBuilding, purpsParamourWorkplaceBuilding, extraPerson1, itemA, itemB, itemC, itemD, itemE }

    public enum BasicLeadPool { hair, eyeColour, shoeSize, build, height, fingerprint, age, jobTitle, randomInterest, partnerFirstName, partnerJobTitle, firstNameInitial, socialClub, partnerSocialClub, notableFeatures, salary, bloodType, randomAffliction, handwriting };

    public enum LeadCitizen { nobody, poster, purp, purpsParamour };
    public enum JobSpawnWhere { posterHome, posterWork, purpHome, purpWork, purpsParamourHome, purpsParamourWork, hiddenItemPlace, nearbyGooseChase };
    public enum DifficultyTag { D0, D1, D2A, D2B, D3, D4A, D4B, D4C, D5, D6 };

    [System.Serializable]
    public class JobModifierRule
    {
        public LeadCitizen who;
        public CharacterTrait.RuleType rule = CharacterTrait.RuleType.ifAnyOfThese;

        public List<CharacterTrait> traitList = new List<CharacterTrait>();
        [ShowIf("isTrait")]
        [Tooltip("If this isn't true then it won't be picked for application at all.")]
        public bool mustPassForApplication = true;
        [Tooltip("Add this to a default priority multiplier of 1.")]
        public float chanceModifier = 0f;
    }

    [System.Serializable]
    public class StartingSpawnItem
    {
        public string name;
        [Tooltip("Try and find an existing interactable that matches this criteria...")]
        public bool findExisting = false;
        public List<MotivePreset> compatibleWithMotives = new List<MotivePreset>();
        public bool compatibleWithAllMotives = false;

        [Space(7)]
        [DisableIf("useOrGroup")]
        [Range(0f, 1f)]
        public float chance = 1f;

        [Space(7)]
        public bool useTraits = false;
        [EnableIf("useTraits")]
        public List<JobModifierRule> traitModifiers = new List<JobModifierRule>();

        [Space(7)]
        public bool useIf = false;
        [EnableIf("useIf")]
        [Tooltip("Only spawn if a previous object of this letter is spawned...")]
        public JobTag ifTag;

        [Space(7)]
        public bool useOrGroup = false;
        [EnableIf("useOrGroup")]
        [Tooltip("If enabled, only one chosen item from this group will be spawned...")]
        public JobTag orGroup;
        [EnableIf("useOrGroup")]
        [Range(0, 10)]
        public int chanceRatio = 4;

        [Space(7)]
        public List<DifficultyTag> disableOnDifficulties = new List<DifficultyTag>();

        [Space(7)]
        public JobTag itemTag;
        [Tooltip("What?")]
        public InteractablePreset spawnItem; //Spawn an item
        [Space(7)]
        public string vmailThread; //Spawn a vmailThread
        public Vector2 vmailProgressThreshold;

        [Tooltip("Where?")]
        public JobSpawnWhere where = JobSpawnWhere.posterHome;
        public LeadCitizen belongsTo = LeadCitizen.poster;
        public LeadCitizen writer = LeadCitizen.nobody;
        public LeadCitizen receiver = LeadCitizen.nobody;
        public int security = 3;
        public int priority = 1;
        public InteractablePreset.OwnedPlacementRule ownershipRule;
    }

    [System.Serializable]
    public class HandInLocation
    {
        public LeadCitizen who;
    }

    [System.Serializable]
    public class IntroConfig
    {
        public SideMissionIntroPreset preset;
        [Range(0, 10)]
        public int frequency = 5;
    }

    [System.Serializable]
    public class HandInConfig
    {
        public SideMissionHandInPreset preset;
        [Range(0, 10)]
        public int frequency = 5;
    }

    [BoxGroup("Disable")]
    [Tooltip("Disable this in-game completely.")]
    public bool disabled = false;

    [Header("Setup")]
    public string caseName;
    public InteractablePreset jobPosting;
    [Tooltip("Spawn this subclass. If left empty it will use the base class.")]
    public string subClass = string.Empty;
    public bool allowSyncDiskRewards = true;
    [EnableIf("allowSyncDiskRewards")]
    public bool allowBlackMarketSyncDiskRewards = true;
    public RewardLocation physicalRewardLocation = RewardLocation.postersMailbox;
    [Tooltip("Generates an item hiden location on acceptance")]
    public bool generateHidingLocation = false;

    public enum RewardLocation { none, postersMailbox, cityHallDesk, playersMailbox };

    [Header("Frequency")]
    [InfoBox("The frequency uses the below graph multiplied by the active per citizen value to calculate how many jobs should be spawned...")]
    [Tooltip("Spawn this job according to social credit level")]
    public AnimationCurve socialCreditLevelMinSpawnFrequency;
    [Tooltip("The number of these jobs that should be active at one time, per citizen.")]
    public float activePerCitizen = 0.01f;
    [Tooltip("Hard limit on maximum jobs spawned")]
    public int maxJobs = 8;
    [Tooltip("If posted jobs count is below this, then spawn them immediately")]
    public int immediatePostCountThreshold = 2;

    [Header("Difficulty")]
    public DifficultyTag difficultyTag;

    [Header("Characters")]
    public ParticipantCompliancy changePosterDialogCompliancy = ParticipantCompliancy.noChange;
    public ParticipantCompliancy changePerpDialogCompliancy = ParticipantCompliancy.alwaysFail;

    public enum ParticipantCompliancy { noChange, alwaysSuccess, alwaysFail};

    [Header("Motives")]
    public List<MotivePreset> purpetratorMotives= new List<MotivePreset>();

    [Space(7)]
    [Tooltip("Minus this from the score if the purp and poster live in the same building")]
    public int penaltyForPurpAndPosterSameBuilding = 5;

    [Header("Starting Scenarios")]
    [Tooltip("Possible starting scenarios for this job")]
    public List<StartingScenario> startingScenarios = new List<StartingScenario>();

    [Header("Intros")]
    [Tooltip("Scenarios that will reveal the required information for the task")]
    public List<IntroConfig> compatibleIntros = new List<IntroConfig>();

    [Header("On Info Acquisition")]
    [Tooltip("How many entries from the general lead pool should we add?")]
    [Range(0, 5)]
    public int leadPoolData = 0;
    [InfoBox("Created facts here are automatically also discovered on creation")]
    public List<FactCreation> createFactsOnInformationAcquisition = new List<FactCreation>();
    public List<StartingLead> informationAcquisitionLeads = new List<StartingLead>();

    [Header("Revenge Objectives")]
    public List<RevengeObjective> revengeObjectives = new List<RevengeObjective>();

    [Header("Spawn Items")]
    public List<StartingSpawnItem> spawnItems = new List<StartingSpawnItem>();

    [Header("Objectives")]
    public List<Case.ResolveQuestion> resolveQuestions = new List<Case.ResolveQuestion>();

    [Header("Additonal Main Elements")]
    public List<SideMissionIntroPreset.SideMissionObjectiveBlock> additional = new List<SideMissionIntroPreset.SideMissionObjectiveBlock>();

    [Header("Hand-Ins")]
    [Tooltip("Scenarios that will reveal the required information for the task")]
    public List<HandInConfig> compatibleHandIns = new List<HandInConfig>();

    [Header("Misc References")]
    public List<DialogReference> dialogReferences = new List<DialogReference>();

    [System.Serializable]
    public class DialogReference
    {
        public string name;
        public DialogPreset dialog;
    }

    [Header("Debug")]
    public JobPreset debugCopyFrom;

    [Button]
    public void CopyAcquisitionData()
    {
#if UNITY_EDITOR

        if (debugCopyFrom != null)
        {
            difficultyTag = debugCopyFrom.difficultyTag;
            leadPoolData = debugCopyFrom.leadPoolData;
            createFactsOnInformationAcquisition = new List<FactCreation>(debugCopyFrom.createFactsOnInformationAcquisition);
            informationAcquisitionLeads = new List<StartingLead>(debugCopyFrom.informationAcquisitionLeads);

            //Force save data
            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
    }

    [Button]
    public void CopyFrequencyData()
    {
#if UNITY_EDITOR

        if (debugCopyFrom != null)
        {
            socialCreditLevelMinSpawnFrequency = debugCopyFrom.socialCreditLevelMinSpawnFrequency;

            //Force save data
            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
    }

    [Button]
    public void CopyStartingScenarios()
    {
#if UNITY_EDITOR

        if (debugCopyFrom != null)
        {
            startingScenarios = new List<StartingScenario>(debugCopyFrom.startingScenarios);

            //Force save data
            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
    }

    [Button]
    public void CopyItemSpawns()
    {
#if UNITY_EDITOR

        if (debugCopyFrom != null)
        {
            spawnItems = new List<StartingSpawnItem>(debugCopyFrom.spawnItems);

            //Force save data
            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
    }

    [Button]
    public void CopyResolveQuestions()
    {
#if UNITY_EDITOR

        if (debugCopyFrom != null)
        {
            resolveQuestions = new List<Case.ResolveQuestion>(debugCopyFrom.resolveQuestions);

            //Force save data
            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
    }

    [Button]
    public void CopyIntros()
    {
#if UNITY_EDITOR

        if (debugCopyFrom != null)
        {
            compatibleIntros = new List<IntroConfig>(debugCopyFrom.compatibleIntros);

            //Force save data
            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
    }

    [Button]
    public void CopyHandIns()
    {
#if UNITY_EDITOR

        if (debugCopyFrom != null)
        {
            compatibleHandIns = new List<HandInConfig>(debugCopyFrom.compatibleHandIns);

            //Force save data
            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
    }

    [Button]
    public void CopyAdditionalMainElements()
    {
#if UNITY_EDITOR

        if (debugCopyFrom != null)
        {
            additional = new List<SideMissionIntroPreset.SideMissionObjectiveBlock>(debugCopyFrom.additional);

            //Force save data
            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
    }

    [Button]
    public void CopyDialogReferences()
    {
#if UNITY_EDITOR

        if (debugCopyFrom != null)
        {
            dialogReferences = new List<DialogReference>(debugCopyFrom.dialogReferences);

            //Force save data
            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
    }

    public int GetDifficultyValue()
    {
        if (difficultyTag == DifficultyTag.D0) return 0;
        else if (difficultyTag == DifficultyTag.D1) return 1;
        else if (difficultyTag == DifficultyTag.D2A || difficultyTag == DifficultyTag.D2B) return 2;
        else if (difficultyTag == DifficultyTag.D3) return 3;
        else if (difficultyTag == DifficultyTag.D4A || difficultyTag == DifficultyTag.D4B || difficultyTag == DifficultyTag.D4C) return 4;
        else if (difficultyTag == DifficultyTag.D5) return 5;
        else if (difficultyTag == DifficultyTag.D6) return 6;

        return 0;
    }

    public int GetFrequencyForSocialCreditLevel()
    {
        int socCreditLevel = GameplayController.Instance.GetCurrentSocialCreditLevel();

        float graphValue = socialCreditLevelMinSpawnFrequency.Evaluate(socCreditLevel);

        int idealNumber = Mathf.RoundToInt(graphValue * Mathf.CeilToInt(CityData.Instance.citizenDirectory.Count * activePerCitizen));

        int returnVal = Mathf.Min(idealNumber, maxJobs);
        //Game.Log("Jobs: Desired number of " + name + " jobs: " + idealNumber + " (" + returnVal + ") based on social credit level: " + GameplayController.Instance.socialCredit);

        return returnVal;
    }
}
