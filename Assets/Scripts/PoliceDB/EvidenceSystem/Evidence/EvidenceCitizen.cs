﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Linq;
using System.Text;

public class EvidenceCitizen : EvidenceWitness
{
    public Human witnessController; //Witness controller; replace with cc

    //Constructor
    public EvidenceCitizen(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        //Set citizen controller
        witnessController = newController as Human;
    }

    //Return a list of know info 
    public override string GetNote(List<DataKey> keys)
    {
        if (keys == null) keys = new List<DataKey>();

        StringBuilder sb = new StringBuilder();

        List<DataKey> tiedKeys = GetTiedKeys(keys);
        if (tiedKeys == null) tiedKeys = new List<DataKey>();

        string checkboxEmpty = "<sprite=\"icons\" name=\"Checkbox Empty\">";
        string checkboxChecked = "<sprite=\"icons\" name=\"Checkbox Checked\">";

        string uniqueEmpty = "<sprite=\"icons\" name=\"Name Empty\">";
        string uniqueChecked = "<sprite=\"icons\" name=\"Name Checked\">";

        string handwritingFont = "<font=\"PapaManAOE SDF\">";
        string fontEnd = "</font>";

        string checkbox = string.Empty;
        bool knownGender = false;

        //Unique keys
        if (tiedKeys.Contains(DataKey.name)) checkbox = uniqueChecked;
        else checkbox = uniqueEmpty;

        sb.Append(checkbox + Strings.Get("descriptors", "Name", Strings.Casing.firstLetterCaptial) + ": ");
        if (tiedKeys.Contains(DataKey.name))
        {
            sb.Append(handwritingFont + GetNameForDataKey(keys) + fontEnd);
            knownGender = true; //Don't know how else to tie gender to anything? Maybe photo/name is the best way?
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //uniqueEmpty = "<sprite=\"icons\" name=\"Photo Empty\">";
        //uniqueChecked = "<sprite=\"icons\" name=\"Photo Checked\">";

        //Photo
        if (tiedKeys.Contains(DataKey.photo))
        {
            checkbox = uniqueChecked;
            knownGender = true; //Don't know how else to tie gender to anything? Maybe photo/name is the best way?
        }
        else checkbox = uniqueEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Photo", Strings.Casing.firstLetterCaptial) + ": ");
        if (tiedKeys.Contains(DataKey.photo)) sb.Append(handwritingFont + Strings.Get("descriptors", "Yes", Strings.Casing.firstLetterCaptial) + fontEnd);
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //uniqueEmpty = "<sprite=\"icons\" name=\"Voice Empty\">";
        //uniqueChecked = "<sprite=\"icons\" name=\"Voice Checked\">";

        //Voice
        if (tiedKeys.Contains(DataKey.voice)) checkbox = uniqueChecked;
        else checkbox = uniqueEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Voice ID", Strings.Casing.firstLetterCaptial) + ": ");
        if (tiedKeys.Contains(DataKey.voice)) sb.Append(handwritingFont + Strings.Get("descriptors", "Yes", Strings.Casing.firstLetterCaptial) + fontEnd);
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Fingerprint
        //uniqueEmpty = "<sprite=\"icons\" name=\"Fingerprint Empty\">";
        //uniqueChecked = "<sprite=\"icons\" name=\"Fingerprint Checked\">";

        if (tiedKeys.Contains(DataKey.fingerprints)) checkbox = uniqueChecked;
        else checkbox = uniqueEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Fingerprint", Strings.Casing.firstLetterCaptial) + ": ");
        if (tiedKeys.Contains(DataKey.fingerprints)) sb.Append(handwritingFont + Strings.Get("descriptors", "Yes", Strings.Casing.firstLetterCaptial) + fontEnd);
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        sb.Append("\n" + "<sprite=\"icons\" name=\"Link\">");

        //Age
        if (tiedKeys.Contains(DataKey.age)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Age", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + ": ");
        if (tiedKeys.Contains(DataKey.age))
        {
            sb.Append(handwritingFont + witnessController.GetAge().ToString() + " (" + Strings.Get("descriptors", witnessController.GetAgeGroup().ToString()) + ")" + fontEnd);
        }
        else if (tiedKeys.Contains(DataKey.ageGroup))
        {
            sb.Append(handwritingFont + Strings.Get("descriptors", witnessController.GetAgeGroup().ToString(), Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + fontEnd);
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Date of birth
        if (tiedKeys.Contains(DataKey.dateOfBirth)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Date of Birth", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + ": ");
        if (tiedKeys.Contains(DataKey.dateOfBirth)) sb.Append(handwritingFont + witnessController.birthday + fontEnd);
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Gender
        if (tiedKeys.Contains(DataKey.sex)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Gender", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + ": ");
        if (tiedKeys.Contains(DataKey.sex)) sb.Append(handwritingFont + Strings.Get("descriptors", witnessController.gender.ToString(), Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + fontEnd);
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Height
        if (tiedKeys.Contains(DataKey.height)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Height", Strings.Casing.firstLetterCaptial) + ": ");
        if(tiedKeys.Contains(DataKey.height)) sb.Append(handwritingFont + Mathf.RoundToInt(witnessController.descriptors.heightCM).ToString() + " (" + Strings.Get("descriptors", witnessController.descriptors.height.ToString()) + ")" + fontEnd);
        else if(tiedKeys.Contains(DataKey.heightEstimate)) sb.Append(handwritingFont + Strings.Get("descriptors", witnessController.descriptors.height.ToString(), useGenderReference: knownGender, genderReference: witnessController) + fontEnd);
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Build
        if (tiedKeys.Contains(DataKey.build)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Build", Strings.Casing.firstLetterCaptial) + ": ");
        if (tiedKeys.Contains(DataKey.build)) sb.Append(handwritingFont + Strings.Get("descriptors", witnessController.descriptors.build.ToString(), Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + fontEnd);
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Hair
        if (tiedKeys.Contains(DataKey.hair)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Hair", Strings.Casing.firstLetterCaptial) + ": ");
        if (tiedKeys.Contains(DataKey.hair)) sb.Append(handwritingFont + Strings.Get("descriptors", witnessController.descriptors.hairType.ToString(), Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + ", " + Strings.Get("descriptors", witnessController.descriptors.hairColourCategory.ToString(), Strings.Casing.lowerCase) + fontEnd);
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Eyes
        if (tiedKeys.Contains(DataKey.eyes)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Eyes", Strings.Casing.firstLetterCaptial) + ": ");
        if (tiedKeys.Contains(DataKey.eyes)) sb.Append(handwritingFont + Strings.Get("descriptors", witnessController.descriptors.eyeColour.ToString(), Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + fontEnd);
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Shoes
        if (tiedKeys.Contains(DataKey.shoeSize)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Shoe Size", Strings.Casing.firstLetterCaptial) + ": ");
        if (tiedKeys.Contains(DataKey.shoeSize) || (tiedKeys.Contains(DataKey.shoeSizeEstimate) && UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.footSizePerception) > 0f))
        {
            sb.Append(handwritingFont + witnessController.descriptors.shoeSize.ToString() + fontEnd);
        }
        else if (tiedKeys.Contains(DataKey.shoeSizeEstimate) && !tiedKeys.Contains(DataKey.shoeSize) && UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.footSizePerception) <= 0f)
        {
            Vector2 sizeRange = Toolbox.Instance.CreateTimeRange(witnessController.descriptors.shoeSize, 2f, false, true, 60);
            sb.Append(handwritingFont + Mathf.FloorToInt(sizeRange.x) + " - " + Mathf.CeilToInt(sizeRange.y) + " (" + Strings.Get("descriptors", "shoe size estimate", useGenderReference: knownGender, genderReference: witnessController) + ")" + fontEnd);
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Glasses
        if (tiedKeys.Contains(DataKey.glasses)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Glasses", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + ": ");
        if (tiedKeys.Contains(DataKey.glasses))
        {
            //Glasses
            if (witnessController.characterTraits.Exists(item => item.name == "Affliction-ShortSighted" || item.name == "Affliction-FarSighted"))
            {
                sb.Append(handwritingFont + Strings.Get("descriptors", "Yes", Strings.Casing.firstLetterCaptial) + fontEnd);
            }
            else sb.Append(handwritingFont + Strings.Get("descriptors", "No", Strings.Casing.firstLetterCaptial) + fontEnd);
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Facial hair
        if (tiedKeys.Contains(DataKey.facialHair)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Facial Hair", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + ": ");
        if(tiedKeys.Contains(DataKey.facialHair))
        {
            //Glasses
            if(witnessController.characterTraits.Exists(item => item.name == "Quirk-FacialHair"))
            {
                sb.Append(handwritingFont + Strings.Get("descriptors", "Yes", Strings.Casing.firstLetterCaptial) + fontEnd);
            }
            else sb.Append(handwritingFont + Strings.Get("descriptors", "No", Strings.Casing.firstLetterCaptial) + fontEnd);
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Blood Type
        if (tiedKeys.Contains(DataKey.bloodType)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Blood Type", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + ": ");
        if (tiedKeys.Contains(DataKey.bloodType)) sb.Append(handwritingFont + "|bloodtype|" + fontEnd);
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Break
        //sb.Append("\n");

        //Address
        if (tiedKeys.Contains(DataKey.address)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Address", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + ": ");
        if (tiedKeys.Contains(DataKey.address))
        {
            if(witnessController.home != null) sb.Append(handwritingFont + "|home.name|" + fontEnd);
            else sb.Append(handwritingFont + Strings.Get("descriptors", "None", Strings.Casing.firstLetterCaptial) + fontEnd);
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Telephone
        if (tiedKeys.Contains(DataKey.telephoneNumber)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Telephone", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + ": ");
        if (tiedKeys.Contains(DataKey.telephoneNumber))
        {
            if (witnessController.home != null && witnessController.home.telephones != null && witnessController.home.telephones.Count > 0) sb.Append(handwritingFont + "|home.telephone|" + fontEnd);
            else sb.Append(handwritingFont + Strings.Get("descriptors", "None", Strings.Casing.firstLetterCaptial) + fontEnd);
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Workplace
        if (tiedKeys.Contains(DataKey.work)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Workplace", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + ": ");
        if (tiedKeys.Contains(DataKey.work))
        {
            if(witnessController.job != null && witnessController.job.employer != null && witnessController.job.employer.address != null) sb.Append(handwritingFont + "|job.employer.location.name|" + fontEnd);
            else sb.Append(handwritingFont + Strings.Get("descriptors", "None", Strings.Casing.firstLetterCaptial) + fontEnd);
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Job Title
        if (tiedKeys.Contains(DataKey.jobTitle)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Job Title", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + ": ");
        if (tiedKeys.Contains(DataKey.jobTitle))
        {
            if(witnessController.job != null && witnessController.job.employer != null) sb.Append(handwritingFont + "|job.title|" + fontEnd);
            else sb.Append(handwritingFont + Strings.Get("descriptors", "None", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + fontEnd);
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Job Hours
        if (tiedKeys.Contains(DataKey.workHours)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Work Hours", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + ": ");
        if (tiedKeys.Contains(DataKey.workHours))
        {
            if(witnessController.job != null && witnessController.job.employer != null) sb.Append(handwritingFont + "|job.hours|" + fontEnd);
            else sb.Append(handwritingFont + Strings.Get("descriptors", "None", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + fontEnd);
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Salary
        if (tiedKeys.Contains(DataKey.salary)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Salary", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + ": ");
        if (tiedKeys.Contains(DataKey.salary))
        {
            if(witnessController.job != null && witnessController.job.employer != null) sb.Append(handwritingFont + "|job.salary|" + fontEnd);
            else sb.Append(handwritingFont + Strings.Get("descriptors", "None", Strings.Casing.firstLetterCaptial) + fontEnd);
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Handwriting
        if (tiedKeys.Contains(DataKey.handwriting)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        string citHandwriting = string.Empty;
        if(witnessController.handwriting != null) citHandwriting = "<font=\"" + witnessController.handwriting.fontAsset.name + "\">";

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Handwriting", Strings.Casing.firstLetterCaptial) + ": ");

        if (tiedKeys.Contains(DataKey.handwriting))
        {
            int hwIndex = Toolbox.Instance.allHandwriting.IndexOf(witnessController.handwriting);
            sb.Append(citHandwriting + Strings.Get("descriptors", "Sample") + fontEnd + "(" + Strings.Get("evidence.generic", "Type", Strings.Casing.firstLetterCaptial) + " " + Toolbox.Instance.ToBase26(hwIndex) + ")");
        }

        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Passcode
        if (tiedKeys.Contains(DataKey.code)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Passcode", Strings.Casing.firstLetterCaptial) + ": ");
        if (tiedKeys.Contains(DataKey.code)) sb.Append(citHandwriting + "|passcode|" + fontEnd);
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Break
        sb.Append("\n");

        //Other notes
        sb.Append("\n" + Strings.Get("descriptors", "Additional Notes", Strings.Casing.firstLetterCaptial) + ": \n" + handwritingFont);

        bool previous = false;

        if(tiedKeys.Contains(DataKey.randomInterest))
        {
            sb.Append(Strings.Get("descriptors", "Interest", Strings.Casing.firstLetterCaptial) + ": |Interest|");
            previous = true;
        }

        if (tiedKeys.Contains(DataKey.randomAffliction))
        {
            sb.Append(Strings.Get("descriptors", "Affliction", Strings.Casing.firstLetterCaptial) + ": |Affliction|");
            previous = true;
        }

        if (tiedKeys.Contains(DataKey.randomSocialClub))
        {
                if (previous) sb.Append("\n");
                sb.Append(Strings.Get("descriptors", "member", Strings.Casing.firstLetterCaptial) + ": |group1.name|");
                previous = true;
        }

        if (tiedKeys.Contains(DataKey.firstNameInitial))
        {
            if (previous) sb.Append("\n");

            sb.Append(Strings.Get("descriptors", "First name initial", Strings.Casing.firstLetterCaptial) + ": |initial|");
            previous = true;
        }

        if (tiedKeys.Contains(DataKey.partnerFirstName))
        {
            if(witnessController.partner != null)
            {
                if (previous) sb.Append("\n");

                sb.Append(Strings.Get("descriptors", "Partner first name", Strings.Casing.firstLetterCaptial) + ": " + witnessController.partner.GetFirstName());
                previous = true;
            }
        }

        if (tiedKeys.Contains(DataKey.partnerJobTitle))
        {
            if (witnessController.partner != null && witnessController.partner != null && witnessController.partner.job != null && witnessController.partner.job.employer != null)
            {
                if (previous) sb.Append("\n");

                sb.Append(Strings.Get("descriptors", "Partner job", Strings.Casing.firstLetterCaptial) + ": |partner.job.title|");
                previous = true;
            }
        }

        if (tiedKeys.Contains(DataKey.partnerSocialClub))
        {
            if(witnessController.partner != null)
            {
                if (previous) sb.Append("\n");

                sb.Append(Strings.Get("descriptors", "Partner is member", Strings.Casing.firstLetterCaptial) + ": |partner.group1.name|");
                previous = true;
            }

        }

        if (tiedKeys.Contains(DataKey.livesOnFloor))
        {
            if (witnessController.home != null)
            {
                if (previous) sb.Append("\n");

                sb.Append(Strings.Get("descriptors", "Lives on the ", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + " |home.floor| of |home.building|");
                previous = true;
            }        
        }

        if (tiedKeys.Contains(DataKey.livesInBuilding))
        {
            if (witnessController.home != null)
            {
                if (previous) sb.Append("\n");

                sb.Append(Strings.Get("descriptors", "Lives in ", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + " |home.building|");
                previous = true;
            }
        }

        if (tiedKeys.Contains(DataKey.worksInBuilding))
        {
            if (witnessController.job != null && witnessController.job.employer != null && witnessController.job.employer.address != null)
            {
                if (previous) sb.Append("\n");

                sb.Append(Strings.Get("descriptors", "Works in ", Strings.Casing.firstLetterCaptial, useGenderReference: knownGender, genderReference: witnessController) + " |job.employer.location.building|");
                previous = true;
            }
        }

        return sb.ToString();
    }

    //Name
    public override string GenerateName()
    {
        return witnessController.GetCitizenName();
    }

    //Override name variants
    public override string GetNameForDataKey(List<DataKey> inputKeys)
    {
        string ret = string.Empty;

        try
        {
            if (inputKeys == null) return Strings.Get("evidence.names", preset.name);

            List<DataKey> keys = GetTiedKeys(inputKeys);

            //Return custom names first
            if (customNames.Count > 0)
            {
                foreach (CustomName n in customNames)
                {
                    if (keys.Contains(n.key))
                    {
                        return n.name;
                    }
                }
            }

            ret = string.Empty;

            //Return name
            if (keys.Contains(DataKey.name))
            {
                return witnessController.GetCitizenName();
            }
            else if(keys.Contains(DataKey.firstName))
            {
                return witnessController.GetFirstName() + " " + Strings.Get("evidence.generic", "?", Strings.Casing.firstLetterCaptial);
            }
            else if (keys.Contains(DataKey.surname))
            {
                return Strings.Get("evidence.generic", "?", Strings.Casing.firstLetterCaptial) + " " + witnessController.GetSurName();
            }
            else if (keys.Contains(DataKey.initialedName))
            {
                return witnessController.GetInitialledName();
            }
            else if (keys.Contains(DataKey.initials))
            {
                return witnessController.GetInitials();
            }
            else
            {
                //New code: This may need changing: Check against name key for input keys...
                List<DataKey> nameKeys = GetTiedKeys(DataKey.name);

                if(nameKeys.Exists(item => inputKeys.Contains(item)))
                {
                    return witnessController.GetCitizenName();
                }
                else
                {
                    List<DataKey> firstNameKeys = GetTiedKeys(DataKey.firstName);

                    if (firstNameKeys.Exists(item => inputKeys.Contains(item)))
                    {
                        return witnessController.GetFirstName() + " " + Strings.Get("evidence.generic", "?", Strings.Casing.firstLetterCaptial);
                    }
                    else
                    {
                        List<DataKey> surnameKeys = GetTiedKeys(DataKey.surname);

                        if (surnameKeys.Exists(item => inputKeys.Contains(item)))
                        {
                            return Strings.Get("evidence.generic", "?", Strings.Casing.firstLetterCaptial) + " " + witnessController.GetSurName();
                        }
                        else
                        {
                            List<DataKey> initialedNameKeys = GetTiedKeys(DataKey.initialedName);

                            if (initialedNameKeys.Exists(item => inputKeys.Contains(item)))
                            {
                                return witnessController.GetInitialledName();
                            }
                            else
                            {
                                List<DataKey> initialsNameKeys = GetTiedKeys(DataKey.initials);

                                if (initialsNameKeys.Exists(item => inputKeys.Contains(item)))
                                {
                                    return witnessController.GetInitials();
                                }
                            }
                        }
                    }
                }

                //'Unknown'
                ret = Strings.Get("evidence.generic", "Unknown", Strings.Casing.firstLetterCaptial) + " " + Strings.Get("evidence.generic", preset.name, Strings.Casing.firstLetterCaptial);
            }

            return ret;
        }
        catch
        {
            return ret;
        }
    }

    public override void NamePhotoMerge()
    {
        //Update interactable name
        witnessController.interactable.UpdateName(true, DataKey.name);

        //Activate reading mode on interactable
        witnessController.interactable.drm = true;
    }

    //Get note with text composed with the data reference from this evidence
    public override string GetNoteComposed(List<Evidence.DataKey> keys, bool useLinks = true)
    {
        Strings.LinkSetting links = Strings.LinkSetting.forceNoLinks;
        if (useLinks) links = Strings.LinkSetting.forceLinks;
        return Strings.ComposeText(GetNote(keys), witnessController, links, evidenceKeys: keys);
    }
}
