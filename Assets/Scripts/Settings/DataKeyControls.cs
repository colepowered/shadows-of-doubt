﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataKeyControls : MonoBehaviour
{
    [System.Serializable]
    public class DataKeySettings
    {
        public Evidence.DataKey key;
        [Tooltip("Is this a unique identifier?")]
        public bool uniqueKey = false;
        public bool countTowardsProfile = true;
    }

    //Singleton pattern
    private static DataKeyControls _instance;
    public static DataKeyControls Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
}
