﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using NaughtyAttributes;

//Controls a pinned item of evidence
//Script pass 1
public class PinnedItemController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, IDragHandler, IEndDragHandler
{
    //References
    public Case.CaseElement caseElement;
    public Evidence evidence;
    public EvidenceButtonController evidenceButton;
    public RectTransform newInfoIcon;

    public Image background;
    public TextMeshProUGUI titleText;
	public RectTransform rect;
    public RectTransform pinnedRect;
    public PinnedPinButtonController pinButtonController;
    public DragCasePanel dragController;
    public RectTransform crossedOut;

    public Rigidbody2D rb;
    public HingeJoint2D joint;
    public JuiceController juice;

    public ContextMenuController contextMenu;
    public TooltipController tooltip;

    public List<StringController> connectedStrings = new List<StringController>();

    public static PinnedQuickMenuController activeQuickMenu;

    public bool hideConnections = false;

    public Vector2 originalSize = Vector2.zero;

	//Monitor to see if mouse is over window.
    public bool isOver = false; //Mouse is over
    public bool isDragging = false; //Being dragged
    public bool isSelected = false; //Considered selected: Either by clicking/box OR mouse over
    public bool permSelected = false; //A more permenant selection that is achieved by clicking on or box-selecting.
    public bool pinPlaceActive = false; //True while first pinning and moving.
    public float scalingSpeed = 8f;
    public Vector3 mouseOverScale = new Vector3 (1.05f, 1.05f, 1f);
    public Vector3 prevLocalPos;

    public List<string> debug = new List<string>();

    //Auto-pinning
    public static List<float> angleSteps;

    //Minimized
    public bool minimized = false;

    //Events
    //Triggered when this is moved
    public delegate void OnMove();
    public event OnMove OnMoved;

    public void Setup(Case.CaseElement newElement)
	{
        caseElement = newElement;

        //Get rect
        rect = this.gameObject.GetComponent<RectTransform>();
        rect.localScale = Vector3.one;

        //Get tooltip
        //tooltip = this.gameObject.GetComponent<TooltipController>();

        //Setup angle steps
        if(angleSteps == null)
        {
            angleSteps = new List<float>();

            for (int i = 0; i < InterfaceControls.Instance.angleStepsCount; i++)
            {
                angleSteps.Add(((float)360 / (float)InterfaceControls.Instance.angleStepsCount) * (float)i);
            }
        }

        //Spawn an evidence button
        Toolbox.Instance.TryGetEvidence(caseElement.id, out evidence);

        if(evidence != null)
        {
            GameObject newButtonObj = Instantiate(PrefabControls.Instance.evidenceButton, this.gameObject.transform, false);
            evidenceButton = newButtonObj.GetComponent<EvidenceButtonController>();
            evidenceButton.Setup(evidence, caseElement.dk, this);

            tooltip = evidenceButton.tooltip;
            if(evidenceButton.tooltip != null) evidenceButton.tooltip.tooltipEnabled = true;

            //Display text infront
            titleText.transform.SetAsLastSibling();
            newInfoIcon.SetAsLastSibling();
            pinnedRect.SetAsLastSibling(); //Display pin on top

            //Disable button tooltip
            //evidenceButton.gameObject.GetComponent<TooltipController>().tooltipEnabled = false;

            //Configure style
            if (evidence.preset.pinnedStyle == EvidencePreset.PinnedStyle.stickNote)
            {
                //Set size
                rect.sizeDelta = new Vector2(150f, 150f);
                titleText.rectTransform.sizeDelta = new Vector2(titleText.rectTransform.sizeDelta.x, 130f); //Text covers the whole item
                evidenceButton.rect.sizeDelta = new Vector2(140f, 140f); //Evidence button covers whole thing

                //Remove physics
                Destroy(joint);
                Destroy(rb);

                evidence.OnNoteAdded += VisualUpdate;
            }

            //Set background colour
            background.color = evidence.preset.pinnedBackgroundColour;

            //Position and size button
            RectTransform brt = newButtonObj.GetComponent<RectTransform>();

            evidence.OnDataKeyChange += VisualUpdate;
            name = evidenceButton.evidence.preset.name;
        }
        else
        {
            //Set name to this for better debugging if this isn't loading correctly
            titleText.text = caseElement.n;
        }

        originalSize = rect.sizeDelta;

        dragController = this.gameObject.GetComponentInChildren<DragCasePanel>();
        dragController.Setup(this);

        VisualUpdate();

        //Auto-pin position
        if(caseElement.ap)
        {
            AutoPinPostion();
            caseElement.ap = false;
        }

        //Run pin setup
        pinButtonController.Setup(this);
    }

    void OnDestroy()
    {
        SetSelected(false, false);

        if(InterfaceController.Instance.pinnedBeingDragged == this)
        {
            InterfaceController.Instance.pinnedBeingDragged = null;
        }

        if(activeQuickMenu != null)
        {
            activeQuickMenu.Remove(true);
        }

        if (evidence != null)
        {
            evidence.OnDataKeyChange -= VisualUpdate;

            if (evidence.preset.pinnedStyle == EvidencePreset.PinnedStyle.stickNote)
            {
                evidence.OnNoteAdded -= VisualUpdate;
            }
        }
    }

    //Set position of pinned
    public void SetPostion(Vector2 newPos)
    {
        rect.localPosition = newPos;
        //rb.MovePosition(rect.TransformPoint(newPos));

        OnMoveThis();
    }

    //Autopin position
    public void AutoPinPostion()
    {
        Vector2 originPoint = Vector2.zero; //Parent point which to seek out from- if this has a parent, use that

        int level = 0;
        float angle = 0f;
        List<float> possibleAngles = new List<float>();

        bool foundPos = false;
        int loopFailSafe = 9999;

        while(!foundPos && loopFailSafe > 0)
        {
            float levelDistanceFromOrigin = (float)level * InterfaceControls.Instance.autoPinDistance; //Calculate the distance from 0,0 using level and distance
            Vector2 searchPoint = new Vector2(originPoint.x + (Mathf.Cos(angle) * levelDistanceFromOrigin), originPoint.y + (Mathf.Sin(angle) * levelDistanceFromOrigin)); //Calculate the search point using above and angle
            foundPos = true;

            //Search radius by looping distance of all pinned
            foreach (PinnedItemController pic in CasePanelController.Instance.spawnedPins)
            {
                float distance = Vector2.Distance(pic.rect.localPosition, searchPoint);

                //If any pic is below distance threshold, search for new spot
                if (distance <= InterfaceControls.Instance.pinnedEvidenceRadius)
                {
                    foundPos = false;
                    break;
                }
                else continue;
            }

            if (!foundPos)
            {
                loopFailSafe--;

                //If the level is 0, add a level as there's no point in changing angles
                if (level <= 0)
                {
                    //Game.Log("Not found position, level is 0, so level++");
                    level++;
                    possibleAngles.AddRange(angleSteps); //Re-new the possible angles to check
                    continue;
                }
                //Else change the angle to a new point that shouldn't feature any evidence
                else
                {
                    //If no angles exist to check, change level
                    if(possibleAngles.Count <= 0)
                    {
                        //Game.Log("Level is " + level + ", no angles left! Level ++");
                        level++;
                        possibleAngles.AddRange(angleSteps); //Re-new the possible angles to check
                        continue;
                    }
                    //Pick a new angle
                    else
                    {
                        int anglePick = Toolbox.Instance.Rand(0, possibleAngles.Count);
                        //Game.Log("Level is " + level + ", pick angle " + anglePick + "/" + possibleAngles.Count);
                        angle = possibleAngles[anglePick];
                        possibleAngles.RemoveAt(anglePick);
                        continue; //Continue to next loop for analysis...
                    }
                }
            }
            //Found a position, so position here
            else
            {
                //Game.Log("Found position at level " + level + " and angle " + angle);
                SetPostion(searchPoint);
                return;
            }
        }
    }

    //Triggered when the pinned item is moved
    public void OnMoveThis()
    {
        //Game.Log("Interface: Move pin " + name + " saved at " + rect.localPosition);
        caseElement.v = rect.localPosition;
        //Game.Log("Set save local pos of case element (OnMoveThis): " + caseElement.v);

        if (juice != null) juice.elements[0].originalLocalPos = rect.localPosition;

        if (OnMoved != null) OnMoved();
    }

    public void VisualUpdate()
    {
        //Set text- copy title of window
        if (evidence != null && evidence.preset.pinnedStyle == EvidencePreset.PinnedStyle.polaroid)
        {
            titleText.text = evidence.GetNameForDataKey(caseElement.dk);
        }

        //Update new info icon
        UpdateNewInfoIcon();

        //Set image
        if(evidenceButton != null)
        {
            evidenceButton.VisualUpdate();
        }

        //Spawn/despawn cross out
        if(caseElement.co)
        {
            if(crossedOut == null)
            {
                GameObject nc = Instantiate(PrefabControls.Instance.crossOut, this.transform);
                crossedOut = nc.GetComponent<RectTransform>();
                titleText.rectTransform.SetAsLastSibling(); //Position behind text
            }
        }
        else
        {
            if(crossedOut != null)
            {
                Destroy(crossedOut.gameObject);
            }
        }

        if (caseElement.m)
        {
            Minimize();
        }
        else
        {
            Restore();
        }

        UpdateTooltipText();
    }

    public void UpdateNewInfoIcon()
    {
        //Update new info icon
        if (evidence != null && evidence.preset != null && evidence.preset.useDataKeys && caseElement != null)
        {
            bool newInfo = false;

            if (caseElement.sdk != null)
            {
                List<Evidence.DataKey> knownKeys = evidence.GetTiedKeys(caseElement.dk);

                if (knownKeys.Count > caseElement.sdk.Count)
                {
                    //Game.Log("...Known keys " + knownKeys.Count + " > seen keys " + caseElement.sdk.Count);
                    newInfo = true;
                }
            }
            else
            {
                //Game.Log("...SDK is null");
                newInfo = true;
            }

            if(newInfoIcon != null) newInfoIcon.gameObject.SetActive(newInfo);
        }
        else
        {
            if (newInfoIcon != null) newInfoIcon.gameObject.SetActive(false);
        }
    }

	public void OnPointerEnter(PointerEventData eventData)
    {
        if(InputController.Instance.mouseInputMode)
        {
            SetHovered(true);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (InputController.Instance.mouseInputMode)
        {
            if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;
            SetHovered(false);
        }
    }

    public void SetHovered(bool val)
    {
        if(isOver != val)
        {
            isOver = val;

            if(isOver)
            {
                StartCoroutine(IsOver());
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //This must be the left button
        if (eventData.button != PointerEventData.InputButton.Left)
        {
            return;
        }

        //Is the custom string select active?
        if (CasePanelController.Instance.customLinkSelectionMode)
        {
            return;
        }

        //Is current box selection active? If so deselect them...
        if (!InterfaceController.Instance.boxSelectActive && !permSelected)
        {
            for (int i = 0; i < InterfaceController.Instance.selectedPinned.Count; i++)
            {
                PinnedItemController pbc = InterfaceController.Instance.selectedPinned[i];

                if (pbc != this && pbc.permSelected)
                {
                    pbc.SetSelected(false, false);
                    i--;
                }
            }
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        //This must be the left button
        if (eventData.button != PointerEventData.InputButton.Left)
        {
            return;
        }

        ForcePointerUp();
    }

    public void OnDrag(PointerEventData data)
    {
        //This must be the left button
        if (data.button != PointerEventData.InputButton.Left)
        {
            return;
        }

        ForceDrag();
    }

    public void ForceDrag()
    {
        InterfaceController.Instance.AddMouseOverElement(this);

        if (!isDragging)
        {
            //Show icon
            pinButtonController.mainColour.enabled = false;
            pinButtonController.mainOverlay.enabled = false;
            pinButtonController.icon.gameObject.SetActive(true);

            prevLocalPos = rect.localPosition;

            //Attach joint to the cursor
            if (joint != null) joint.connectedBody = InterfaceControls.Instance.caseBoardCursorRigidbody;
            if (rb != null) rb.drag = InterfaceControls.Instance.movingLinearDrag;

            //Play SFX depending on whether this is a sticky note or not...
            if (evidence != null && (evidence as EvidenceStickyNote) != null)
            {
                AudioController.Instance.Play2DSound(AudioControls.Instance.stickyNotePickUp);
            }
            else
            {
                AudioController.Instance.Play2DSound(AudioControls.Instance.folderPickUp);
            }

            isDragging = true;
            StartCoroutine(IsDragging());
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //This must be the left button
        if (eventData.button != PointerEventData.InputButton.Left)
        {
            return;
        }

        ForcePointerUp();
    }

    public void ForcePointerUp()
    {
        Game.Log("Interface: Folder put down");

        //InterfaceController.Instance.currentMouseOverElement = null;
        //InterfaceController.Instance.UpdateCursorSprite();

        //Is the custom string select active?
        if (CasePanelController.Instance.customLinkSelectionMode)
        {
            return;
        }

        isDragging = false;
    }

    //Active while mouse is over
    IEnumerator IsOver()
    {
        if (rect == null) rect = this.gameObject.GetComponent<RectTransform>();
        rect.SetAsLastSibling();

        bool selected = false;
        float quickMenuTimer = 0f;

        //Don't deselect if a context menu is active
        while ((isOver && ContextMenuController.activeMenu == null) || (ContextMenuController.activeMenu != null && ContextMenuController.activeMenu.flag == ContextMenuController.MenuFlag.pinnedSelected) || (activeQuickMenu != null && activeQuickMenu.parentPinned == this && activeQuickMenu.isOver))
        {
            if(!selected && ContextMenuController.activeMenu == null && !InterfaceController.Instance.boxSelectActive && InterfaceController.Instance.pinnedBeingDragged == null)
            {
                //Update tooltip (easier than listening to all connecting facts!)
                //UpdateTooltipText(); //Dont think we need this here?

                SetSelected(true, false); //Add to selection

                selected = true;
            }

            //Handle quick menu
            if (InterfaceController.Instance.selectedPinned.Count == 1 && activeQuickMenu == null && !MainMenuController.Instance.mainMenuActive && InterfaceController.Instance.pinnedBeingDragged == null && ContextMenuController.activeMenu == null && (!InputController.Instance.mouseInputMode || evidenceButton == null || !evidenceButton.isOver))
            {
                if (InputController.Instance.mouseInputMode)
                {
                    quickMenuTimer += Time.deltaTime;

                    if (quickMenuTimer >= 0.75f)
                    {
                        GameObject newObj = Instantiate(PrefabControls.Instance.quickMenu, PrefabControls.Instance.contextMenuContainer);
                        activeQuickMenu = newObj.GetComponent<PinnedQuickMenuController>();
                        activeQuickMenu.Setup(this);
                    }
                }
                else if (InputController.Instance.player.GetButtonDown("Secondary"))
                {
                    GameObject newObj = Instantiate(PrefabControls.Instance.quickMenu, PrefabControls.Instance.contextMenuContainer);
                    activeQuickMenu = newObj.GetComponent<PinnedQuickMenuController>();
                    activeQuickMenu.Setup(this);
                }
            }
            else if (activeQuickMenu != null)
            {
                if (!InputController.Instance.mouseInputMode && InputController.Instance.player.GetButtonDown("Secondary"))
                {
                    activeQuickMenu.Remove();
                }
            }

            if (InterfaceController.Instance.boxSelectActive || InterfaceController.Instance.pinnedBeingDragged != null || ContextMenuController.activeMenu != null || MainMenuController.Instance.mainMenuActive)
            {
                if (activeQuickMenu != null) activeQuickMenu.Remove();
                quickMenuTimer = 0f;
            }

            yield return null;
        }

        if (activeQuickMenu != null)
        {
            activeQuickMenu.Remove();
        }

        //Wait an extra frame before deselecting
        bool deselectTimer = false;

        while(!deselectTimer)
        {
            deselectTimer = true;
            yield return null;
        }

        if (selected && !permSelected)
        {
            SetSelected(false, false); //Deselect if not permenant
        }
    }

    //Active while dragging
    IEnumerator IsDragging()
    {
        if(evidenceButton != null) evidenceButton.SetInteractable(false);

        while (isDragging)
        {
            InterfaceController.Instance.pinnedBeingDragged = this;

            if(activeQuickMenu != null)
            {
                activeQuickMenu.Remove();
            }

            //Do this for all selected
            foreach (PinnedItemController pbc in InterfaceController.Instance.selectedPinned)
            {
                //if (pbc == this) continue; //Don't need to do this for itself

                //Add mouse inertia as force
                if (pbc.rb != null)
                {
                    Vector3 moveInertia = rect.localPosition - pbc.prevLocalPos;
                    pbc.rb.AddForce(moveInertia * InterfaceControls.Instance.pinnedMovementIntertiaMultiplier);
                }

                pbc.prevLocalPos = rect.localPosition;
            }

            //Force disable tooltip
            if(tooltip != null)
            {
                tooltip.ForceClose();
                tooltip.enabled = false;
            }

            //Detect mouse up manually- for some reason this isn't registering when draggin with the evidence button...
            if(pinPlaceActive)
            {
                if (!InputController.Instance.mouseInputMode && !InputController.Instance.player.GetButton("Select"))
                {
                    ForcePointerUp();
                }
                else if(InputController.Instance.mouseInputMode && !Input.GetMouseButton(0))
                {
                    ForcePointerUp();
                }
            }
            else if(!pinPlaceActive && !InputController.Instance.mouseInputMode)
            {
                Vector2 movePinned = new Vector2(InputController.Instance.GetAxisRelative("MoveEvidenceAxisX"), InputController.Instance.GetAxisRelative("MoveEvidenceAxisY"));

                if (movePinned.magnitude <= 0.15f)
                {
                    ForcePointerUp();
                }
            }

            yield return null;
        }

        if (InterfaceController.Instance.pinnedBeingDragged == this)
        {
            InterfaceController.Instance.pinnedBeingDragged = null;
        }

        //Re-enable tooltip
        if (tooltip != null)
        {
            if (!tooltip.enabled)
            {
                tooltip.enabled = true;
            }
        }

        if (evidenceButton != null) evidenceButton.SetInteractable(true);

        //Play SFX depending on whether this is a sticky note or not...
        if (evidence != null && (evidence as EvidenceStickyNote) != null)
        {
            AudioController.Instance.Play2DSound(AudioControls.Instance.stickyNotePutDown);
        }
        else
        {
            AudioController.Instance.Play2DSound(AudioControls.Instance.folderPutDown);
        }

        //Attach joint to the cursor
        if (joint != null) joint.connectedBody = null;
        if (rb != null) rb.drag = InterfaceControls.Instance.pinnedLinearDrag;

        InterfaceController.Instance.RemoveMouseOverElement(this);

        //Hide icon
        pinButtonController.mainColour.enabled = true;
        pinButtonController.mainOverlay.enabled = true;
        pinButtonController.icon.gameObject.SetActive(false);
    }

    IEnumerator Rescale(Vector3 size)
    {
    	float len = 0f;

    	while (len < 1f)
    	{
    		len += scalingSpeed * Time.deltaTime;

			rect.localScale = Vector3.Lerp(rect.localScale, size, Mathf.Clamp(len, 0f, 1f));

    		yield return null;
    	}
    }

    public void SetSelected(bool val, bool permenantSelected)
    {
        if (isSelected != val)
        {
            isSelected = val;
            permSelected = permenantSelected;

            SetHovered(isSelected);

            if (isSelected)
            {
                StartCoroutine(Rescale(mouseOverScale));

                //Is current box selection active? If so deselect them...
                //if (!InterfaceController.Instance.boxSelectActive && ContextMenuController.activeMenu == null)
                //{
                //    List<PinnedItemController> currentSelected = new List<PinnedItemController>(InterfaceController.Instance.selectedPinned);

                //    for (int i = 0; i < currentSelected.Count; i++)
                //    {
                //        PinnedItemController pbc = currentSelected[i];

                //        if (pbc != null && pbc != this)
                //        {
                //            SetSelected(false, false);
                //        }
                //    }
                //}

                if (!InterfaceController.Instance.selectedPinned.Contains(this))
                {
                    InterfaceController.Instance.selectedPinned.Add(this);
                    InteractionController.Instance.UpdateInteractionText(); //Update text to show custom string action
                }

                //Set sprite to outline
                pinButtonController.mainOverlay.sprite = pinButtonController.pinnedOverlayMO;
                pinButtonController.mainMOOverlay.gameObject.SetActive(true);
            }
            else
            {
                if (activeQuickMenu != null)
                {
                    activeQuickMenu.Remove();
                }

                StartCoroutine(Rescale(new Vector3(1f, 1f, 1f)));

                permSelected = false;

                if(InterfaceController.Instance.selectedPinned.Contains(this))
                {
                    Game.Log("Interface: Remove " + name + " from selected pinned (deselect)");
                    InterfaceController.Instance.selectedPinned.Remove(this);
                }

                InteractionController.Instance.UpdateInteractionText(); //Update text to show custom string action

                //Set sprite
                if (!isOver)
                {
                    pinButtonController.mainOverlay.sprite = pinButtonController.pinnedOverlay;
                    pinButtonController.mainMOOverlay.gameObject.SetActive(false);

                    //Attach joint to the cursor
                    if (joint != null) joint.connectedBody = null;
                    if (rb != null) rb.drag = InterfaceControls.Instance.pinnedLinearDrag;
                }
            }

            UpdateContextMenuOptions();
            Game.Log("Interface: Pin set selected: " + name + ": " + val + " permenant: " + permSelected);
        }
    }

    //Change base colour from white
    public void ChangeBaseColour(Color newBaseColour)
    {
        Image img = this.gameObject.GetComponent<Image>();
        img.color = newBaseColour;
    }

    public void UpdateTooltipText()
    {
        if(evidence != null && tooltip != null)
        {
            tooltip.mainText = evidence.GetNameForDataKey(caseElement.dk);

            string det = string.Empty;
            string originalColourHex = "<color=#" + ColorUtility.ToHtmlStringRGB(InterfaceControls.Instance.defaultTextColour) + ">";

            //Use identifier
            det += evidence.GetNoteComposed(caseElement.dk, false);

            tooltip.detailText = det;

            if (evidence.preset.pinnedStyle == EvidencePreset.PinnedStyle.stickNote)
            {
                titleText.text = det; //For sticky notes, display the detail text
            }
        }
    }

    //Context commands
    public void ToggleHideChildren()
    {
        if(hideConnections)
        {
            ShowConnections();
        }
        else
        {
            HideConnections();
        }
    }

    public void HideConnections()
    {
        hideConnections = true;
        Game.Log("Interface: Set hide linked evidence connections for " + name + ": " + hideConnections);

        List<Evidence.FactLink> links = evidence.GetFactsForDataKey(caseElement.dk);

        foreach(Evidence.FactLink link in links)
        {
            if(link.fact.isFound)
            {
                if(link.fact.fromEvidence.Contains(evidence))
                {
                    foreach(Evidence ev in link.fact.toEvidence)
                    {
                        if(ev.isFound)
                        {
                            Case findCase = CasePanelController.Instance.activeCases.Find(item => item.id == caseElement.caseID);

                            if(findCase != null)
                            {
                                CasePanelController.Instance.UnPinFromCasePanel(findCase, ev, link.fact.toDataKeys);
                            }
                        }
                    }
                }
                else if(link.fact.toEvidence.Contains(evidence))
                {
                    foreach (Evidence ev in link.fact.fromEvidence)
                    {
                        if (ev.isFound)
                        {
                            Case findCase = CasePanelController.Instance.activeCases.Find(item => item.id == caseElement.caseID);

                            if (findCase != null)
                            {
                                CasePanelController.Instance.UnPinFromCasePanel(findCase, ev, link.fact.fromDataKeys);
                            }
                        }
                    }
                }
            }
        }
    }

    public void ShowConnections()
    {
        hideConnections = false;
        Game.Log("Interface: Set show linked evidence connections for " + name + ": " + hideConnections);

        List<Evidence.FactLink> links = evidence.GetFactsForDataKey(caseElement.dk);

        foreach (Evidence.FactLink link in links)
        {
            if (link.fact.isFound)
            {
                if (link.fact.fromEvidence.Contains(evidence))
                {
                    foreach (Evidence ev in link.fact.toEvidence)
                    {
                        if (ev == null) continue;

                        if (ev.isFound)
                        {
                            Case findCase = CasePanelController.Instance.activeCases.Find(item => item.id == caseElement.caseID);

                            if (findCase != null)
                            {
                                //Pin if not already pinned
                                CasePanelController.Instance.PinToCasePanel(findCase, ev, link.fact.toDataKeys, forceAutoPin: true);
                            }
                        }
                    }
                }
                else if (link.fact.toEvidence.Contains(evidence))
                {
                    foreach (Evidence ev in link.fact.fromEvidence)
                    {
                        if (ev == null) continue;

                        if (ev.isFound)
                        {
                            Case findCase = CasePanelController.Instance.activeCases.Find(item => item.id == caseElement.caseID);

                            if (findCase != null)
                            {
                                //Pin if not already pinned
                                CasePanelController.Instance.PinToCasePanel(findCase, ev, link.fact.fromDataKeys, forceAutoPin: true);
                            }
                        }
                    }
                }
            }
        }
    }

    //Toggle minimize
    public void ToggleMinimize()
    {
        if (caseElement.m)
        {
            Restore();
        }
        else
        {
            Minimize();
        }
    }

    public void Minimize()
    {
        caseElement.m = true;

        if(evidenceButton != null) evidenceButton.gameObject.SetActive(false);
        titleText.gameObject.SetActive(false);
        rect.sizeDelta = Vector2.zero;
        if (crossedOut != null) crossedOut.gameObject.GetComponent<CanvasRenderer>().SetAlpha(0f);
    }

    public void Restore()
    {
        caseElement.m = false;

        if(evidenceButton != null) evidenceButton.gameObject.SetActive(true);
        if(titleText != null) titleText.gameObject.SetActive(true);
        rect.sizeDelta = originalSize;
        if(crossedOut != null) crossedOut.gameObject.GetComponent<CanvasRenderer>().SetAlpha(1f);
    }

    public void OpenEvidence() //Open evidence
    {
        if (evidenceButton != null) evidenceButton.OnLeftClick();
    }

    public void Unpin()
    {
        Case findCase = CasePanelController.Instance.activeCases.Find(item => item.id == caseElement.caseID);

        if (findCase != null)
        {
            CasePanelController.Instance.UnPinFromCasePanel(findCase, evidence, caseElement.dk, true, caseElement);
        }
    }

    public void Cancel()
    {
        if(contextMenu != null)
        {
            contextMenu.ForceClose();
        }
    }

    //Don't rotate the pin
    void LateUpdate()
    {
        pinnedRect.rotation = Quaternion.identity;
    }

    public void UpdateContextMenuOptions()
    {
        contextMenu.disabledItems.Clear();

        //Incompatible with multiple...
        if (InterfaceController.Instance.selectedPinned.Count > 1)
        {
            contextMenu.disabledItems.Add("LocateOnMap");
            contextMenu.disabledItems.Add("PlotRoute");
            contextMenu.disabledItems.Add("CreateCustomString");
        }

        //Incompatible with non locations
        if(evidence == null || (evidence as EvidenceLocation) == null)
        {
            contextMenu.disabledItems.Add("LocateOnMap");
            contextMenu.disabledItems.Add("PlotRoute");
        }
    }

    //Referenced through context menu (cannot feature bool input)
    public void CreateCustomString()
    {
        CasePanelController.Instance.CustomStringLinkSelection(this, false);
    }

    //Force cancel a drag
    public void ForceCancelDrag()
    {
        Game.Log("Cancel pinned drag");
        ForcePointerUp();
    }

    public void ToggleCrossedOut()
    {
        foreach (PinnedItemController pic in InterfaceController.Instance.selectedPinned)
        {
            pic.caseElement.co = !pic.caseElement.co;
            pic.VisualUpdate();
            pic.pinButtonController.VisualUpdate();
        }
    }

    public void PlotRoute()
    {
        MapController.Instance.PlotPlayerRoute(evidence);
    }

    public void LocateOnMap()
    {
        MapController.Instance.LocateEvidenceOnMap(evidence);
    }

    public void ToggleCollapse()
    {
        foreach (PinnedItemController pic in InterfaceController.Instance.selectedPinned)
        {
            pic.ToggleMinimize();
        }
    }

    public void NewStickyNote()
    {
        Case findCase = CasePanelController.Instance.activeCases.Find(item => item.id == caseElement.caseID);

        if (findCase != null)
        {
            InfoWindow stickyNote = CasePanelController.Instance.NewStickyNote();

            if(stickyNote != null)
            {
                //Create custom sticky note link
                List<Evidence> linkEv = new List<Evidence>();

                foreach(PinnedItemController p in InterfaceController.Instance.selectedPinned)
                {
                    linkEv.Add(p.evidence);
                }

                Fact newestCreatedFact = EvidenceCreator.Instance.CreateFact("CustomLink", null, stickyNote.passedEvidence, fromEvidence: linkEv, overrideFromKeys: caseElement.dk, overrideToKeys: caseElement.dk, isCustomFact: true) as FactCustom;

                //Name link
                //PopupMessageController.Instance.PopupMessage("CustomFactRename", true, true, "Cancel", "Continue", anyButtonClosesMsg: true, newPauseState: PopupMessageController.AffectPauseState.no, true, Strings.Get("evidence.generic", "customlink"), enableColourPicker: true);
                //PopupMessageController.Instance.OnLeftButton += OnCancelCustomFact;
                //PopupMessageController.Instance.OnRightButton += OnContinueFactName;

                //Pin sticky note
                CasePanelController.Instance.PinToCasePanel(findCase, stickyNote.passedEvidence, caseElement.dk, true);
            }
        }
    }

    public void MinimizeAll()
    {
        InterfaceController.Instance.MinimizeAll();
    }

    public void PinAllLinks()
    {
        foreach (PinnedItemController pic in InterfaceController.Instance.selectedPinned)
        {
            if (pic.evidence == null) continue;

            //Cycle through available facts...
            //Compile a list of file icons that should exist
            List<Evidence.FactLink> required = new List<Evidence.FactLink>();

            List<Evidence.FactLink> getKeys = pic.evidence.GetFactsForDataKey(pic.caseElement.dk);

            foreach (Evidence.FactLink link in getKeys)
            {
                if (link.fact.isFound)
                {
                    if (!required.Contains(link))
                    {
                        bool duplicate = false;

                        foreach (Evidence.FactLink comp in required)
                        {
                            //Same fact
                            if (comp.fact == link.fact)
                            {
                                //Same 'this' evidence
                                if (comp.thisEvidence == link.thisEvidence)
                                {
                                    //Same to evidence
                                    bool containsAll = true;

                                    foreach (Evidence dest in comp.destinationEvidence)
                                    {
                                        if (!link.destinationEvidence.Contains(dest))
                                        {
                                            containsAll = false;
                                            break;
                                        }
                                    }

                                    if (containsAll)
                                    {
                                        duplicate = true;
                                        break;
                                    }
                                }
                            }
                        }

                        if (!duplicate)
                        {
                            required.Add(link);
                        }
                    }
                }
            }

            Case findCase = CasePanelController.Instance.activeCases.Find(item => item.id == pic.caseElement.caseID);

            if (findCase != null)
            {
                foreach (Evidence.FactLink fl in required)
                {
                    if (fl.thisEvidence != null && fl.thisEvidence != evidence) CasePanelController.Instance.PinToCasePanel(findCase, fl.thisEvidence, fl.thisKeys, true);

                    if (fl.destinationEvidence != null)
                    {
                        foreach(Evidence ev in fl.destinationEvidence)
                        {
                            if(ev != evidence) CasePanelController.Instance.PinToCasePanel(findCase, ev, fl.destinationKeys, true);
                        }
                    }
                }
            }
        }
    }

    public void UnpinAllLinks()
    {
        foreach (PinnedItemController pic in InterfaceController.Instance.selectedPinned)
        {
            if (pic.evidence == null) continue;

            //Cycle through available facts...
            //Compile a list of file icons that should exist
            List<Evidence.FactLink> required = new List<Evidence.FactLink>();

            List<Evidence.FactLink> getKeys = pic.evidence.GetFactsForDataKey(pic.caseElement.dk);

            foreach (Evidence.FactLink link in getKeys)
            {
                if (link.fact.isFound)
                {
                    if (!required.Contains(link))
                    {
                        bool duplicate = false;

                        foreach (Evidence.FactLink comp in required)
                        {
                            //Same fact
                            if (comp.fact == link.fact)
                            {
                                //Same 'this' evidence
                                if (comp.thisEvidence == link.thisEvidence)
                                {
                                    //Same to evidence
                                    bool containsAll = true;

                                    foreach (Evidence dest in comp.destinationEvidence)
                                    {
                                        if (!link.destinationEvidence.Contains(dest))
                                        {
                                            containsAll = false;
                                            break;
                                        }
                                    }

                                    if (containsAll)
                                    {
                                        duplicate = true;
                                        break;
                                    }
                                }
                            }
                        }

                        if (!duplicate)
                        {
                            required.Add(link);
                        }
                    }
                }
            }

            Case findCase = CasePanelController.Instance.activeCases.Find(item => item.id == pic.caseElement.caseID);

            if (findCase != null)
            {
                foreach (Evidence.FactLink fl in required)
                {
                    if (fl.thisEvidence != null && fl.thisEvidence != evidence) CasePanelController.Instance.UnPinFromCasePanel(findCase, fl.thisEvidence, fl.thisKeys);

                    if (fl.destinationEvidence != null)
                    {
                        foreach (Evidence ev in fl.destinationEvidence)
                        {
                            if (ev != evidence) CasePanelController.Instance.UnPinFromCasePanel(findCase, ev, fl.destinationKeys);
                        }
                    }
                }
            }
        }
    }

    //Context menu
    public void SetColourRed()
    {
        foreach (PinnedItemController pic in InterfaceController.Instance.selectedPinned)
        {
            pic.caseElement.SetColour(InterfaceControls.EvidenceColours.red);
        }
    }

    public void SetColourBlue()
    {
        foreach (PinnedItemController pic in InterfaceController.Instance.selectedPinned)
        {
            pic.caseElement.SetColour(InterfaceControls.EvidenceColours.blue);
        }
    }

    public void SetColourYellow()
    {
        foreach (PinnedItemController pic in InterfaceController.Instance.selectedPinned)
        {
            pic.caseElement.SetColour(InterfaceControls.EvidenceColours.yellow);
        }
    }

    public void SetColourGreen()
    {
        foreach (PinnedItemController pic in InterfaceController.Instance.selectedPinned)
        {
            pic.caseElement.SetColour(InterfaceControls.EvidenceColours.green);
        }
    }

    public void SetColourPurple()
    {
        foreach (PinnedItemController pic in InterfaceController.Instance.selectedPinned)
        {
            pic.caseElement.SetColour(InterfaceControls.EvidenceColours.purple);
        }
    }

    public void SetColourWhite()
    {
        foreach (PinnedItemController pic in InterfaceController.Instance.selectedPinned)
        {
            pic.caseElement.SetColour(InterfaceControls.EvidenceColours.white);
        }
    }

    public void SetColourBlack()
    {
        foreach (PinnedItemController pic in InterfaceController.Instance.selectedPinned)
        {
            pic.caseElement.SetColour(InterfaceControls.EvidenceColours.black);
        }
    }

    public void UpdatePulsate()
    {
        bool pulsate = false;

        if(EventSystem.current != null && EventSystem.current.IsPointerOverGameObject())
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

            foreach (InfoWindow w in InterfaceController.Instance.activeWindows)
            {
                //Mouse over window
                if (w.currentPinnedCaseElement == caseElement && results.Exists(item => item.gameObject == w.background.gameObject))
                {
                    pulsate = true;
                    break;
                }
            }
        }

        juice.Pulsate(pulsate, true);
    }

    [Button]
    public void DisplayEvidenceIdentifier()
    {
        if(evidence != null)
        {
            Game.Log(evidence.GetIdentifier());
        }
    }
}
