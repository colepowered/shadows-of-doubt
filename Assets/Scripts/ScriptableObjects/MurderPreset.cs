﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "murder_data", menuName = "Database/Murder Preset")]

public class MurderPreset : SoCustomComparison
{
	[Header("Preset Picking")]
	[Tooltip("How often this is picked compared to others...")]
	[Range(0, 10)]
	public int frequency = 3;

	[Header("Murderer Picking")]
	public Vector2 murdererRandomScoreRange = new Vector2(0f, 1f);
	public List<MurdererModifierRule> murdererTraitModifiers = new List<MurdererModifierRule>();

	[System.Serializable]
	public class MurdererModifierRule
	{
		public CharacterTrait.RuleType rule = CharacterTrait.RuleType.ifAnyOfThese;

		public List<CharacterTrait> traitList = new List<CharacterTrait>();
		[ShowIf("isTrait")]
		[Tooltip("If this isn't true then it won't be picked for application at all.")]
		public bool mustPassForApplication = true;
		[Tooltip("Add this to a default priority multiplier of 1.")]
		public float scoreModifier = 0.5f;
	}

	public bool useHexaco = false;
	[ShowIf("useHexaco")]
	public HEXACO hexaco;

	[Header("Other")]
	public float minimumTimeBetweenMurders = 5f;

	[Space(5)]
	[Tooltip("When not at home, how many occupants are allowed here at maximum for the murder to trigger at this location")]
	public int nonHomeMaximumOccupantsTrigger = 2;
	[Tooltip("When not at home, how many occupants are allowed here at maximum for the triggered murder to be cancelled")]
	public int nonHomeMaximumOccupantsCancel = 4;

	[Header("Phase 1: Acquire Murder Weapon/Ammo")]
	public bool requiresAcquirePhase = true;
	public bool acquirePassInteractable = false;
	public bool acquirePassRoom = true;
	public List<AIGoalPreset.GoalActionSetup> acquireActionSetup = new List<AIGoalPreset.GoalActionSetup>();

	[Header("Phase 2: Research")]
	[Tooltip("Does this require a research state?")]
	public bool requiresResearchPhase = false;

	[EnableIf("requiresResearchPhase")]
	public bool researchPassInteractable = true;
	[EnableIf("requiresResearchPhase")]
	public bool researchPassRoom = true;
	[EnableIf("requiresResearchPhase")]
	public List<AIGoalPreset.GoalActionSetup> researchActionSetup = new List<AIGoalPreset.GoalActionSetup>();

	[Header("Phase 3: Travel To")]
	[Tooltip("Once the murderer has started travelling, block the victim from leaving their location...")]
	public bool blockVictimFromLeavingLocation = true;

	public bool travelPassInteractable = true;
	public bool travelPassRoom = true;
	public List<AIGoalPreset.GoalActionSetup> travelActionSetup = new List<AIGoalPreset.GoalActionSetup>();

	[Header("Phase 4: Execution")]
	public bool executePassInteractable = true;
	public bool executePassRoom = true;
	public List<AIGoalPreset.GoalActionSetup> executionActionSetup = new List<AIGoalPreset.GoalActionSetup>();

	[Header("Phase 5: Post")]
	public bool postPassInteractable = true;
	public bool postPassRoom = true;
	public List<AIGoalPreset.GoalActionSetup> postActionSetup = new List<AIGoalPreset.GoalActionSetup>();

	[Header("Phase 6: Escape")]
	public bool escapePassInteractable = true;
	public bool escapePassRoom = true;
	public List<AIGoalPreset.GoalActionSetup> escapeActionSetup = new List<AIGoalPreset.GoalActionSetup>();

	[Header("Leads")]
	public List<MurderLeadItem> leads = new List<MurderLeadItem>();

	public enum LeadCitizen { nobody, victim, killer, victimsClosest, killersClosest, victimsDoctor, killersDoctor };
	public enum LeadSpawnWhere { victimHome, victimWork, killerHome, killerWork };

	[System.Serializable]
	public class MurderModifierRule
	{
		public LeadCitizen who;
		public CharacterTrait.RuleType rule = CharacterTrait.RuleType.ifAnyOfThese;

		public List<CharacterTrait> traitList = new List<CharacterTrait>();
		[ShowIf("isTrait")]
		[Tooltip("If this isn't true then it won't be picked for application at all.")]
		public bool mustPassForApplication = true;
		[Tooltip("Add this to a default priority multiplier of 1.")]
		public float chanceModifier = 0f;
	}

	[System.Serializable]
	public class MurderLeadItem
	{
		public string name;
		public List<MurderMO> compatibleWithMotives = new List<MurderMO>();
		public MurderController.MurderState spawnOnPhase = MurderController.MurderState.acquireEuipment;
		public LeadCitizen belongsTo = LeadCitizen.victim;

		[Space(7)]
		[DisableIf("useOrGroup")]
		[Range(0f, 1f)]
		public float chance = 1f;

		[Space(7)]
		public bool useTraits = false;
		[EnableIf("useTraits")]
		public List<MurderModifierRule> traitModifiers = new List<MurderModifierRule>();

		[Space(7)]
		public bool useIf = false;
		[EnableIf("useIf")]
		[Tooltip("Only spawn if a previous object of this letter is spawned...")]
		public JobPreset.JobTag ifTag;

		[Space(7)]
		public bool useOrGroup = false;
		[EnableIf("useOrGroup")]
		[Tooltip("If enabled, only one chosen item from this group will be spawned...")]
		public JobPreset.JobTag orGroup;
		[EnableIf("useOrGroup")]
		[Range(0, 10)]
		public int chanceRatio = 4;

		[Space(7)]
		public JobPreset.JobTag itemTag;
		[Tooltip("What?")]
		public InteractablePreset spawnItem; //Spawn an item
		[Space(7)]
		public string vmailThread; //Spawn a vmailThread
		public Vector2 vmailProgressThreshold;

		[Tooltip("Where?")]
		public LeadSpawnWhere where = LeadSpawnWhere.victimHome;
		public LeadCitizen writer = LeadCitizen.nobody;
		public LeadCitizen receiver = LeadCitizen.nobody;
		public int security = 3;
		public int priority = 1;
		public InteractablePreset.OwnedPlacementRule ownershipRule;
	}
}
