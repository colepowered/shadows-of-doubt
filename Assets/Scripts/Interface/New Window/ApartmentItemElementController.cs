﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ApartmentItemElementController : MonoBehaviour
{
    [Header("Components")]
    public InteractablePreset itemPreset;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI priceText;
    public TextMeshProUGUI sellText;
    public InfoWindow thisWindow;
    public DecorController decorController;
    public ApartmentItemsController itemsController;
    public Image mainImage;
    [System.NonSerialized]
    public Interactable worldItemReference;
    public ButtonController placeButton;
    public ButtonController storageButton;
    public ButtonController sellButton;
    public Image icon;

    [Header("State")]
    public int price = 0;

    public void SetupItem(InteractablePreset newItemPreset, ApartmentItemsController newDecorController, InfoWindow newThisWindow, Interactable newWorldItemReference)
    {
        //Game.Log("Setup shop button for " + newFurniture);

        itemPreset = newItemPreset;
        thisWindow = newThisWindow;
        itemsController = newDecorController;
        worldItemReference = newWorldItemReference;

        if(storageButton != null)
        {
            storageButton.SetInteractable(false);
            storageButton.gameObject.SetActive(false);
        }

        if (itemPreset != null)
        {
            if (worldItemReference == null)
            {
                price = (int)itemPreset.value.y;
            }
            else
            {
                price = Mathf.RoundToInt(worldItemReference.val);

                if (storageButton != null)
                {
                    storageButton.SetInteractable(true);
                    storageButton.gameObject.SetActive(true);
                }
            }

            mainImage.sprite = itemPreset.staticImage;
            mainImage.color = Color.white;
        }

        VisualUpdate();
    }

    public void VisualUpdate()
    {
        UpdateButtonText();
        UpdatePurchaseAbility();
    }

    public void UpdateButtonText()
    {
        if(itemPreset != null)
        {
            if(worldItemReference != null)
            {
                //Can this be picked up into inventory?
                if(itemPreset.isInventoryItem || itemPreset.isMoney)
                {
                    priceText.text = Strings.Get("ui.interface", "Take");
                }
                else if (itemPreset.actionsPreset.Exists(item => item.actions.Exists(item => item.interactionName.ToLower() == "take (lockpick)")))
                {
                    priceText.text = Strings.Get("ui.interface", "Take");
                }
                else
                {
                    priceText.text = Strings.Get("ui.interface", "Place");
                }

                nameText.text = worldItemReference.GetName();

                if(sellButton != null)
                {
                    if(!itemPreset.isMoney)
                    {
                        sellText.text = Strings.Get("ui.interface", "Sell") + " " + CityControls.Instance.cityCurrency + Mathf.RoundToInt(worldItemReference.val * 0.5f);
                        sellButton.gameObject.SetActive(true);
                        sellButton.SetInteractable(true);
                    }
                    else
                    {
                        sellText.text = Strings.Get("ui.interface", "Sell");
                        sellButton.gameObject.SetActive(true);
                        sellButton.SetInteractable(false);
                    }
                }
            }
            else
            {
                nameText.text = Strings.Get("evidence.names", itemPreset.name);
                priceText.text = Strings.Get("ui.interface", "Select") + " " + CityControls.Instance.cityCurrency + price;

                if (sellButton != null)
                {
                    sellButton.gameObject.SetActive(false);
                    sellButton.SetInteractable(false);
                }

                Toolbox.Instance.SetRectSize(placeButton.rect, 120, 0, 8, 0);
                placeButton.rect.sizeDelta = new Vector2(placeButton.rect.sizeDelta.x, 52);
                placeButton.rect.anchoredPosition = new Vector2(placeButton.rect.anchoredPosition.x, 6);
            }
        }

        UpdatePurchaseAbility();
    }

    public void OnPlaceButton()
    {
        //Purchase a new item
        if (itemsController != null && itemsController.tabState == FurnishingsController.TabState.inShop)
        {
            if (price <= GameplayController.Instance.money)
            {
                //Ask
                PopupMessageController.Instance.PopupMessage("purchaseask", true, true, "Cancel", "Confirm", anyButtonClosesMsg: true);
                PopupMessageController.Instance.OnLeftButton += PurchaseCancel;
                PopupMessageController.Instance.OnRightButton += PurchaseConfirm;
            }
            else
            {
                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "not_enough_money"), InterfaceControls.Icon.money, null, colourOverride: true, col: InterfaceControls.Instance.messageRed, ping: GameMessageController.PingOnComplete.money);
            }
        }
        //Place an existing item
        else if (itemsController != null && worldItemReference != null)
        {
            //Cancel furniture placement
            if(PlayerApartmentController.Instance.furniturePlacementMode)
            {
                PlayerApartmentController.Instance.SetFurniturePlacementMode(false, null, null);
            }

            PlayerApartmentController.Instance.RemoveItemFromStorage(worldItemReference);

            if(worldItemReference.preset.isMoney)
            {
                ActionController.Instance.TakeMoney(worldItemReference, Player.Instance.currentNode, Player.Instance);
            }
            else if (worldItemReference.preset.isInventoryItem)
            {
                FirstPersonItemController.Instance.PickUpItem(worldItemReference, true, false, playSound: false);
            }
            else
            {
                itemsController.PlaceObject(worldItemReference);
            }

            itemsController.UpdateListDisplay();
            UpdatePurchaseAbility();
        }
    }

    public void PurchaseConfirm()
    {
        PopupMessageController.Instance.OnLeftButton -= PurchaseCancel;
        PopupMessageController.Instance.OnRightButton -= PurchaseConfirm;

        //Cancel furniture placement
        if (PlayerApartmentController.Instance.furniturePlacementMode)
        {
            PlayerApartmentController.Instance.SetFurniturePlacementMode(false, null, null);
        }

        //Create object
        worldItemReference = InteractableCreator.Instance.CreateWorldInteractable(itemPreset, Player.Instance, Player.Instance, null, Player.Instance.transform.position + new Vector3(0, 3.5f, 0), Vector3.zero, null, null);

        //Execute purchase
        if (worldItemReference != null)
        {
            if(worldItemReference.preset.isInventoryItem)
            {
                if (FirstPersonItemController.Instance.PickUpItem(worldItemReference, true, false, playSound: false))
                {
                    Game.Log("Successfully created newObject " + worldItemReference.name + " id " + worldItemReference.id);

                    AudioController.Instance.Play2DSound(AudioControls.Instance.purchaseItem);

                    worldItemReference.MarkAsTrash(true); //Remove this once we can
                    GameplayController.Instance.AddMoney(-price, true, "purchase");
                }
                else
                {
                    Game.LogError("Decor: Unable to create world item " + itemPreset.name);
                    worldItemReference.Delete();
                    worldItemReference = null;
                }
            }
            else
            {
                Game.Log("Decor: Successfully created newObject " + worldItemReference.name + " id " + worldItemReference.id);

                AudioController.Instance.Play2DSound(AudioControls.Instance.purchaseItem);

                GameplayController.Instance.AddMoney(-price, true, "purchase");

                itemsController.PlaceObject(worldItemReference);
            }
        }
        else
        {
            Game.LogError("Decor: Unable to create world item " + itemPreset.name);
        }

        UpdatePurchaseAbility();
    }

    public void PurchaseCancel()
    {
        PopupMessageController.Instance.OnLeftButton -= PurchaseCancel;
        PopupMessageController.Instance.OnRightButton -= PurchaseConfirm;
    }

    public void OnStorageButton()
    {
        if (worldItemReference != null)
        {
            PlayerApartmentController.Instance.MoveItemToStorage(worldItemReference);
        }
    }

    public void OnSellButton()
    {
        if(worldItemReference != null)
        {
            PlayerApartmentController.Instance.SellItem(worldItemReference);
        }
    }

    public void UpdatePurchaseAbility()
    {
        if (itemsController == null || itemsController.tabState == FurnishingsController.TabState.inShop)
        {
            if (price > GameplayController.Instance.money)
            {
                placeButton.SetInteractable(false);
            }
            else
            {
                placeButton.SetInteractable(true);
            }
        }
        else placeButton.SetInteractable(true);

        if (storageButton != null && itemsController != null)
        {
            if (worldItemReference == null || PlayerApartmentController.Instance.itemStorage.Contains(worldItemReference))
            {
                storageButton.gameObject.SetActive(false);
                storageButton.SetInteractable(false);
            }
        }
    }
}
