﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "wallfrontage_data", menuName = "Database/Decor/Wall Frontage Preset")]

public class WallFrontagePreset : SoCustomComparison
{
    [Header("Visuals")]
    public GameObject gameObject;
    [Tooltip("If true this will seach for identical furniture in a room to batch with")]
    public bool allowStaticBatching = true;
    [Tooltip("Can this feature a rainy window texture?")]
    public bool isRainyWindow = false;
    [EnableIf("isRainyWindow")]
    [Tooltip("The non-rainy window material")]
    public Material regularGlass;
    [EnableIf("isRainyWindow")]
    [Tooltip("The rainy window material")]
    public Material rainyGlass;

    [Header("Decor Settings")]
    [Tooltip("If true use across all design styles")]
    public bool universalDesignStyle = false;
    public List<DesignStylePreset> designStyles = new List<DesignStylePreset>();
    [Space(7)]
    public bool inheritColouringFromDecor = false;
    [Tooltip("If true the same material colours will be shared over all instances of this furniture for the room")]
    public FurniturePreset.ShareColours shareColours = FurniturePreset.ShareColours.none;
    public List<MaterialGroupPreset.MaterialVariation> variations = new List<MaterialGroupPreset.MaterialVariation>();

    [Header("Interactables")]
    [Tooltip("What interatables will be instanced on this? These won't be spawned but created and searched for within the furniture prefab")]
    public List<FurniturePreset.IntegratedInteractable> integratedInteractables = new List<FurniturePreset.IntegratedInteractable>();

    [Header("Classes")]
    public List<WallFrontageClass> classes = new List<WallFrontageClass>();
}
