﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "clothes_data", menuName = "Database/Clothing Item")]

public class ClothesPreset : SoCustomComparison
{
	[System.Serializable]
	public class MaterialSettings
	{
		public Color colour;
		[Range(1, 5)]
		public int weighting = 3;
	}

	[System.Serializable]
	public class ModelSettings
	{
		public GameObject prefab;
		public CitizenOutfitController.CharacterAnchor anchor;
		public Vector3 offsetPosition;
		public Vector3 offsetEuler;
		public bool exclusiveAnchorModel = true;
	}

	public enum OutfitCategory { casual, work, smart, outdoorsCasual, outdoorsWork, outdoorsSmart, undressed, bed, underwear };
	public enum ClothingColourSource { none, garment, skin, white, hair, underneathColour1, underneathColour2, underneathColour3, workUniformColour};

	[Header("Setup")]
	[Tooltip("Covers these components: If an anchor here is NOT covered by this outfit, it will look for other elements in outfits.")]
	[ReorderableList]
	public List<CitizenOutfitController.CharacterAnchor> covers = new List<CitizenOutfitController.CharacterAnchor>();
	[Space(7)]
	[ReorderableList]
	public List<OutfitCategory> outfitCategories = new List<OutfitCategory>();
	[ReorderableList]
	public List<Human.Gender> suitableForGenders = new List<Human.Gender>();
	[ReorderableList]
	public List<Descriptors.BuildType> suitableForBuilds = new List<Descriptors.BuildType>();

	public List<ClothesTags> tags = new List<ClothesTags>();
	public enum ClothesTags { longGarment, noLongGarments };

	[Tooltip("If true enable facial-feature specific setup")]
	public bool enableFacialFeatureSetup = false;
	[ReorderableList]
	public List<Descriptors.HairStyle> suitableForHairstyle = new List<Descriptors.HairStyle>();

	[Header("Head")]
	public bool isHead = false;
	[EnableIf("isHead")]
	public Vector3 pupilsOffset = new Vector3(0f, 0.15f, 0.1205f);
	[EnableIf("isHead")]
	public Vector3 eyebrowsOffset = new Vector3(0f, 0.1719f, 0.133f);
	[EnableIf("isHead")]
	public Vector3 mouthOffset = new Vector3(0f, 0.045f, 0.151f);

	[Header("Hair")]
	[Tooltip("This needs to be true for the game to render both the hair and hat")]
	public bool hatRenderCompatible = false;
	[Tooltip("Exclude these types of hats from compatibility...")]
	[EnableIf("hatRenderCompatible")]
	public List<ClothesPreset> excludeHats = new List<ClothesPreset>();

	[Header("Hat")]
	public HairRenderSetting hairRenderMode = HairRenderSetting.renderHatCompatibleHair;

	public enum HairRenderSetting { renderHatCompatibleHair, renderAllHair, dontRenderAnyHair};

	[Header("Feet")]
	public bool setFootwear = false;
	[EnableIf("setFootwear")]
	public Human.ShoeType footwear = Human.ShoeType.barefoot;

	[Header("Compatibility")]
	[Tooltip("Controls which clothing will be loaded first. If this is important to the outfit, eg wearing a coat outdoors, increase the priority.")]
	[Range(0, 5)]
	public int priority = 2;
	[Tooltip("Only choose this preset if the model can display all elements in the 'models' section below. Only applies when using clothing elements not from the category (eg using casual for outdoors casual)")]
	public bool onlyChooseIfAllModelPartsAreAvailable = false;
	[Tooltip("This cannot be chosen if these existing clothes are chosen")]
	[ReorderableList]
	public List<IncompatibilitySetting> incompatibility = new List<IncompatibilitySetting>();

	public enum Incompatibility { inAnyCategory, inThisCategory};

	[System.Serializable]
	public class IncompatibilitySetting
	{
		public Incompatibility incompatibleIf;
		public List<ClothesTags> tags;
		public ClothesPreset featured;
	}

	[Tooltip("The citizen/company must have at least this much wealth to have this outfit")]
	public bool useWealthValues = false;
	[EnableIf("useWealthValues")]
	[Range(0f, 1f)]
	public float minimumWealth = 0f;
	[EnableIf("useWealthValues")]
	[Range(0f, 1f)]
	public float maximumWealth = 1f;
	[Space(7)]


	[Header("Colours")]
	public ClothingColourSource baseColourSource = ClothingColourSource.white;
	[ReorderableList]
	public List<ColourPalettePreset> colourBase = new List<ColourPalettePreset>();

	[Space(5)]
	public ClothingColourSource colour1Source;
	[ReorderableList]
	public List<ColourPalettePreset> colour1 = new List<ColourPalettePreset>();

	[Space(5)]
	public ClothingColourSource colour2Source;
	[ReorderableList]
	public List<ColourPalettePreset> colour2 = new List<ColourPalettePreset>();

	[Space(5)]
	public ClothingColourSource colour3Source;
	[ReorderableList]
	public List<ColourPalettePreset> colour3 = new List<ColourPalettePreset>();



	[Header("Suited Personality")]
	[Tooltip("Include this when using citizen stats to pick a style")]
	public bool includeInPersonalityMatching = true;
	[Tooltip("The base chance of selecting this item of clothing. This is added to by HEXACO and Traits below...")]
	[Range(0, 10)]
	[EnableIf("includeInPersonalityMatching")]
	public int baseChance = 1;
	[Space(7)]

	[InfoBox("If enabled: The below HEXACO values will combine for a score of 1 to 10: this will be used to calculate the likihood of this being chosen vs others.")]
	[Tooltip("Use the below hexaco values to match to personality.")]
	public bool useHEXACO = false;
	public HEXACO hexaco;

	[Space(7)]
	[InfoBox("If enabled: The below traits will be used to calculate the likihood of this being chosen vs others.")]
	[Tooltip("Use character traits to match to personality.")]
	public bool useTraits = false;
	[ReorderableList]
	public List<TraitPickRule> characterTraits = new List<TraitPickRule>();

	[System.Serializable]
	public class TraitPickRule
	{
		public CharacterTrait.RuleType rule = CharacterTrait.RuleType.ifAnyOfThese;

		public List<CharacterTrait> traitList = new List<CharacterTrait>();
		[ShowIf("isTrait")]
		[Tooltip("If this isn't true then it won't be picked for application at all.")]
		public bool mustPassForApplication = true;
		[ShowIf("isTrait")]
		[Range(-10, 10)]
		[Tooltip("If the rules match, then apply this to the base chance...")]
		public int addChance = 0;
	}

	[Header("Models")]
	[InfoBox("Note: The 'covers anchor' box will ensure only this model will be loaded to cover this anchor. If you want more than one model to be loaded for this anchor, make sure one is unchecked.")]
	[ReorderableList]
	public List<ModelSettings> models = new List<ModelSettings>();
}
