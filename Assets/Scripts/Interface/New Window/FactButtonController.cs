﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class FactButtonController : ButtonController
{
    public Evidence.FactLink link;
    public Fact fact;

    public ButtonController toggleHiddenButton;

    public Image parentToThisIcon;
    public Image childOfThisIcon;

    public Sprite shownConnection;
    public Sprite hiddenConnection;
    public Color shownColor = Color.white;
    public Color hiddenColor = Color.gray;

    public RectTransform isSeenIcon;
    private bool isSetup = false;
    private bool enabledFirstTime = false;

    public bool inSlot = false; //Is this in a slot?

    public void Setup(Evidence.FactLink newFactLink, InfoWindow newParentWindow)
    {
        parentWindow = newParentWindow;
        SetupReferences();

        link = newFactLink;
        fact = newFactLink.fact;

        //If evidence is unseen, listen for seen event
        if(fact != null)
        {
            if(!fact.isSeen)
            {
                fact.OnSeen += OnSeen;
            }
        }

        //Spawn toggle hidden button
        GameObject newButton = Instantiate(PrefabControls.Instance.factHideToggleButton, this.transform.parent);
        toggleHiddenButton = newButton.GetComponent<ButtonController>();
        toggleHiddenButton.SetupReferences();
        toggleHiddenButton.OnPress += ToggleHidden;
        toggleHiddenButton.OnHoverChange += UpdatePulsate;

        UpdateTooltipText();
        VisualUpdate();

        //Listen for changes in the fact's connections' data keys
        fact.OnConnectingEvidenceChangeDataKey += VisualUpdate;
        fact.OnNewName += UpdateTooltipText;

        isSetup = true;
    }

    public void OnSeen()
    {
        fact.OnSeen -= OnSeen;
        VisualUpdate();
    }

    public void ToggleHidden(ButtonController thisButton)
    {
        if (CasePanelController.Instance.activeCase != null)
        {
            CasePanelController.Instance.activeCase.ToggleHidden(fact);
        }
    }

    private void OnEnable()
    {
        if (!enabledFirstTime && isSetup) enabledFirstTime = true;
    }

    private void OnDisable()
    {
        //Set as seen if enabled and disabled after initial creation
        if (fact != null)
        {
            if (!fact.isSeen)
            {
                if (enabledFirstTime)
                {
                    fact.SetSeen();
                }
            }
        }
    }

    private void OnDestroy()
    {
        if (toggleHiddenButton != null)
        {
            toggleHiddenButton.OnPress -= ToggleHidden;
            toggleHiddenButton.OnHoverChange -= UpdatePulsate;
            Destroy(toggleHiddenButton.gameObject);
        }

        UpdatePulsate(this, false);

        fact.OnConnectingEvidenceChangeDataKey -= VisualUpdate;
        fact.OnNewName -= UpdateTooltipText;

        if (fact != null)
        {
            if (!fact.isSeen)
            {
                fact.OnSeen -= OnSeen;
            }
        }
    }

    public override void VisualUpdate()
    {
        //Occasionally triggering NullReferenceException: Object reference not set to an instance of an object.
        //Not sure why/how; something to do with UnityEngine.UI.Graphic.SetMaterialDirty ()
        //So wrapping this in Try to avoid exceptions
        //Could disappear with a unity version update???
        try
        {
            if (fact != null && parentWindow.passedEvidence != null)
            {
                Evidence other = fact.GetOther(parentWindow.passedEvidence); //Get the 'other' evidence, ie not this

                //If other is null, it means this button's evidence contains this fact without being part of it, by default use the 'from' evidence
                if(other == null && fact.fromEvidence.Count > 0)
                {
                    other = fact.fromEvidence[0];
                }

                //Is this a parent or child?
                if(other != null && parentWindow.passedEvidence.children.Contains(other))
                {
                    if(childOfThisIcon != null) childOfThisIcon.gameObject.SetActive(true);
                    if (parentToThisIcon != null) parentToThisIcon.gameObject.SetActive(false);
                }
                else if(other != null && other.children.Contains(parentWindow.passedEvidence))
                {
                    if (childOfThisIcon != null) childOfThisIcon.gameObject.SetActive(false);
                    if(parentToThisIcon != null) parentToThisIcon.gameObject.SetActive(true);
                }
                else
                {
                    if (childOfThisIcon != null) childOfThisIcon.gameObject.SetActive(false);
                    if (parentToThisIcon != null) parentToThisIcon.gameObject.SetActive(false);
                }

                //Set sprite- use the reference in the fact class, if null use the first 'other' evidence
                if (fact.iconSprite != null)
                {
                    if (fact.iconSprite != null)
                    {
                        try
                        {
                            icon.sprite = fact.iconSprite;
                        }
                        catch
                        {

                        }
                    }
                }
                else
                {
                    if (parentWindow != null)
                    {
                        if (parentWindow.passedEvidence != null)
                        {
                            if (fact.fromEvidence != null && fact.fromEvidence.Contains(parentWindow.passedEvidence))
                            {
                                if (fact.toEvidence.Count > 0 && fact.toEvidence[0] != null && fact.toEvidence[0].iconSprite != null)
                                {
                                    Sprite sp = fact.toEvidence[0].GetIcon();

                                    if (sp != null)
                                    {
                                        try
                                        {
                                            icon.sprite = sp;
                                        }
                                        catch
                                        {

                                        }
                                    }
                                }
                            }
                            else if (fact.toEvidence != null && fact.toEvidence.Contains(parentWindow.passedEvidence))
                            {
                                if (fact.fromEvidence.Count > 0 && fact.fromEvidence[0] != null && fact.fromEvidence[0].iconSprite != null)
                                {
                                    Sprite sp = fact.fromEvidence[0].GetIcon();

                                    if (sp != null)
                                    {
                                        try
                                        {
                                            icon.sprite = sp;
                                        }
                                        catch
                                        {

                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                UpdateTooltipText();

                //Set 'is seen'
                if(isSeenIcon != null)
                {
                    if (!fact.isSeen)
                    {
                        isSeenIcon.gameObject.SetActive(true);
                    }
                    else isSeenIcon.gameObject.SetActive(false);
                }

                //Is this hidden?
                bool hidden = false;

                if(CasePanelController.Instance.activeCase != null)
                {
                    string factID = fact.GetIdentifier();

                    if (CasePanelController.Instance.activeCase.hiddenConnections.Contains(factID))
                    {
                        hidden = true;
                    }
                }

                if(hidden)
                {
                    toggleHiddenButton.icon.sprite = hiddenConnection;
                    toggleHiddenButton.background.color = hiddenColor;
                }
                else
                {
                    toggleHiddenButton.icon.sprite = shownConnection;
                    toggleHiddenButton.background.color = shownColor;
                }
            }
        }
        catch
        {

        }

    }

    //On double click, open evidence
    public override void OnLeftClick()
    {
        //Spawn all from evidence
        foreach(Evidence ev in fact.fromEvidence)
        {
            InfoWindow existing = InterfaceController.Instance.GetWindow(ev, evKeys: fact.fromDataKeys);
            if(existing == null) InterfaceController.Instance.SpawnWindow(ev, passedEvidenceKeys: fact.fromDataKeys);
        }

        //Spawn all to evidence
        foreach (Evidence ev in fact.toEvidence)
        {
            InfoWindow existing = InterfaceController.Instance.GetWindow(ev, evKeys: fact.toDataKeys);
            if (existing == null) InterfaceController.Instance.SpawnWindow(ev, passedEvidenceKeys: fact.toDataKeys);
        }

        if (!fact.isSeen)
        {
            fact.SetSeen();
        }
    }

    public override void UpdateTooltipText()
    {
        if (tooltip == null) return;

        tooltip.mainText = fact.GetName();
        text.text = tooltip.mainText;

        string det = string.Empty;
        string originalColourHex = "<color=#" + ColorUtility.ToHtmlStringRGB(InterfaceControls.Instance.defaultTextColour) + ">";

        tooltip.detailText = det;
    }

    public override void OnHoverStart()
    {
        UpdatePulsate(this, true);

        //text.fontStyle = FontStyles.Underline;

        //Start evidence mouse over
        //InterfaceController.Instance.NewFactButtonMouseOver(this);
    }

    public override void OnHoverEnd()
    {
        UpdatePulsate(this, false);

        //text.fontStyle = FontStyles.Normal;

        //End mouse over
        //InterfaceController.Instance.CancelCurrentFactButtonMouseOver();
    }

    public void UpdatePulsate(ButtonController hoveredButton, bool mouseOver)
    {
        foreach(StringController str in CasePanelController.Instance.spawnedStrings)
        {
            bool pulsate = false;

            if(mouseOver && str.connection.facts.Contains(fact))
            {
                pulsate = true;
            }

            str.juice.Pulsate(pulsate, false);
        }
    }
}
