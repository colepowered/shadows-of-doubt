﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;
using System;
using System.Net;
using UnityEngine.UIElements;
using UnityEditor;
using Newtonsoft.Json.Bson;

public class NewAIController : MonoBehaviour
{
    [System.NonSerialized]
    public Human human;
    public CapsuleCollider capCollider;

    [System.NonSerialized]
    public float delta;
    private float prevDelta;

    [Header("Debug: Status Stats")]
    [ProgressBar("Nourishment", 1f, EColor.Yellow)]
    public float nourishment = 0f;
    [ProgressBar("Hydration", 1f, EColor.Yellow)]
    public float hydration = 0f;
    [ProgressBar("Alertness", 1f, EColor.Yellow)]
    public float alertness = 0f;
    [ProgressBar("Energy", 1f, EColor.Yellow)]
    public float energy = 0f;
    [ProgressBar("Excitement", 1f, EColor.Yellow)]
    public float excitement = 0f;
    [ProgressBar("Chores", 1f, EColor.Yellow)]
    public float chores = 0f;
    [ProgressBar("Hygiene", 1f, EColor.Yellow)]
    public float hygiene = 0f;
    [ProgressBar("Bladder", 1f, EColor.Yellow)]
    public float bladder = 0f;
    [ProgressBar("Heat", 1f, EColor.Yellow)]
    public float heat = 0f;
    [ProgressBar("Drunk", 1f, EColor.Yellow)]
    public float drunk = 0f;
    [ProgressBar("Breath", 1f, EColor.Yellow)]
    public float breath = 0f;

    [ProgressBar("IdleSound", 1f, EColor.Yellow)]
    public float idleSound = 1f;

    [ProgressBar("Blink", 1f, EColor.Yellow)]
    public float blink = 1f;

    [Space(5)]

    [ProgressBar("Sees Player", 100, EColor.Blue)]
    public int debugSeesPlayer = 0;
    [ReadOnly]
    public float debugLastSeesPlayerChange = 0f;
    [ProgressBar("Hears Player", 1f, EColor.Blue)]
    public float hearsIllegal = 0f;
    public Actor hearTarget;

    [Header("Goals")]
    [Tooltip("Goals are a list of things this AI wants to achieve")]
    public List<NewAIGoal> goals = new List<NewAIGoal>();
    [Tooltip("The currently active goal")]
    [System.NonSerialized] //Don't serialize this so it can be null
    public NewAIGoal currentGoal = null;
    [Tooltip("The currently active action")]
    [System.NonSerialized] //Don't serialize this so it can be null
    public NewAIAction currentAction = null;
    [Tooltip("Investigation goal")]
    [System.NonSerialized] //Don't serialize this so it can be null
    public NewAIGoal investigationGoal = null;
    [Tooltip("Patrol goal")]
    [System.NonSerialized] //Don't serialize this so it can be null
    public NewAIGoal patrolGoal = null;
    [Tooltip("The current ineractable this is using")]
    [System.NonSerialized]
    public FurnitureLocation currentFurnitureUser = null;
    [System.NonSerialized]
    public NewNode currentFurnitureNode = null;
    public Interactable nextAIAction;

    public NewGameLocation confineLocation = null;
    public List<NewGameLocation> avoidLocations = new List<NewGameLocation>();

    [Header("Movement")]
    [System.NonSerialized]
    public int pathCursor = 0;
    [System.NonSerialized]
    public NewNode currentDestinationNode;
    [System.NonSerialized]
    public Vector3 currentDesitnationNodeCoord;
    public Vector3 currentDestinationPositon;

    public float movementAmount = 0f;
    public float distanceToNext = 0f;

    private Quaternion lastMovementRotation = Quaternion.identity;
    private bool doIMove = false;

    private float footStepDistanceCounter = 0f;
    private bool rightFootNext = false;

    public bool isTripping = false;

    public bool doorCheck = true;

    [Tooltip("If I've just opened a door, this is a reference to it so I can close it later")]
    [System.NonSerialized]
    public NewDoor openedDoor;
    [System.NonSerialized]
    private int delayFlag = 0; //Needed to stop multiple delays happening, and people getting stuck
    //[System.NonSerialized]
    //public bool rightFootNext = true;
    private List<NewDoor> doorInteractions = new List<NewDoor>(); //For keeping track of door interactions I am registered to...

    [Header("Turning")]
    public bool facingActive = false;

    [Header("Facing")]
    public Vector3 facingDirection = Vector3.zero;
    [System.NonSerialized]
    public Transform faceTransform = null;
    [System.NonSerialized]
    public Vector3 faceTransformOffset = Vector3.zero;
    public Quaternion facingQuat;

    private Quaternion lookingQuatPrevious;

    private Quaternion lookingQuatLastFrame;
    private Quaternion lookingQuatCurrent;

    //private float turnSpeed = 3f;
    private float lookAroundTimer = 0f;
    private Vector3 lookAroundPosition;

    //Look at targets
    [System.Serializable]
    public class TrackingTarget
    {
        public Actor actor; //If this is another person, reference it here
        public float lastValidSighting; //Last time the sighting was valid

        public bool priorityTarget; //If true will always appear first on look @ list
        public float attractionRank; //Sorted by this based on suspicion calculated on valid sightings
        public float distance;
        public float distanceRank;
        public float fovRank;
        public float itemRank;
        public float lookAtRank; //For sorting who to look @

        public bool active;
        public bool spookedByItem = false;
        public int spookTimer = 0;
    }

    [Header("Vision")]
    public List<TrackingTarget> trackedTargets = new List<TrackingTarget>();
    [NonSerialized]
    public TrackingTarget currentTrackTarget; //can't serialzed this otherwise it won't ever remove a target
    public Transform lookAtTransform = null;
    public float lookAtTransformRank = 0f;
    //private float lookAtSpeed = 4f;
    private float lastLookAtUpdateTime = 0f; //Timestamp of the last look@ update

    //Look clamping    
    [SerializeField, HideInInspector]
    Quaternion original;
    private Vector3 dirXZ, forwardXZ, dirYZ, forwardYZ;

    [Header("Expression")]
    [NonSerialized]
    public CitizenOutfitController.ExpressionSetup currentExpression = null;
    public float expressionProgress = 0f;
    public bool blinkInProgress = false;
    private float blinkTimer = 0f;
    public float eyesOpen = 1f;
    public float bargeTimer = 0f;
    //public bool bargeComplete = false;

    [Header("Investigate AI")]
    public Actor persuitTarget; //The focus of current attention
    public NewNode investigateLocation; //Last known node
    public Vector3 investigatePosition; //Last known position
    public Vector3 investigatePositionProjection; //Projection of where the current target will be (2m ahead of now)
    public Interactable investigateObject;
    public Interactable tamperedObject;
    public InvestigationUrgency investigationUrgency = InvestigationUrgency.walk;
    [NonSerialized]
    public NewAIAction audioFocusAction = null; //Inserted when the AI hears something suspicious
    public float lastInvestigate = 0f;
    private float persuitUpdateTimer = 0; //Draw a ray to check vision every x
    public bool persuit = false; //True if chasing (with a certain amount of grace period tracking)
    public bool seesOnPersuit = false; //Only true when actively seeing the player
    public float persuitChaseLogicUses = 0f; //Each time a player is spotted, it gets one lead on to where the player has disappeared to...
    public float minimumInvestigationTimeMultiplier = 1f; //Can be increased depending on the severety of the crime (remember to max this value)

    public ChaseLogic chaseLogic;

    [System.Serializable]
    public class ChaseLogic
    {
        public NewAIController ai;

        //Last need information
        public Vector3 lastSeenPosition;
        public NewNode lastSeenNode;
        public Vector3 lastSeenDirection;

        //Projected future position
        public NewNode projectedNode;
        public Vector3 projectedPosition;

        //Update last seen
        public void UpdateLastSeen()
        {
            if (ai.persuitTarget == null) return;

            lastSeenNode = ai.persuitTarget.currentNode;
            lastSeenPosition = ai.persuitTarget.transform.position;
            lastSeenDirection = ai.persuitTarget.transform.forward;

            if (ai.persuitTarget.isPlayer) Game.Log("AI: " + ai.name + " updated persuit last seen of player: " + lastSeenPosition + " facing dir " + lastSeenDirection);

            ////Is this visible from the last seen node?
            //if (DataRaycastController.Instance.NodeRaycast(ai.persuitTarget.currentNode, ai.human.currentNode, out _))
            //{
            //    lastSeenNode = ai.persuitTarget.currentNode;
            //    lastSeenPosition = ai.persuitTarget.transform.position;
            //    lastSeenDirection = ai.persuitTarget.transform.forward;

            //    if (ai.persuitTarget.isPlayer) Game.Log("AI: " + ai.name + " updated persuit last seen of player: " + lastSeenPosition + " facing dir " + lastSeenDirection);
            //}
            //else
            //{
            //    if (ai.persuitTarget.isPlayer) Game.Log("AI: " + ai.name + " unable to update persuit last seen of player because of no LOS between " + ai.persuitTarget.currentNode.position + " and " + ai.human.currentNode.position);
            //}
        }

        //Use the latest seen logic to produce a projection
        public void GenerateProjectedNode()
        {
            //Try and extend along the direction...
            if(lastSeenNode == null)
            {
                if (ai.persuitTarget != null) lastSeenNode = ai.persuitTarget.currentNode;
                else lastSeenNode = ai.human.currentNode;
            }

            if (lastSeenNode == null) return;

            Vector3 currentNodeCoord = lastSeenNode.nodeCoord;
            NewNode foundNode = lastSeenNode;

            //Create a 4 way direction from normalized...
            Vector3 normalizedDir = lastSeenDirection.normalized;
            if (Mathf.Abs(normalizedDir.x) > Mathf.Abs(normalizedDir.z)) normalizedDir = new Vector3(Mathf.RoundToInt(normalizedDir.x), 0, 0);
            else normalizedDir = new Vector3(0, Mathf.RoundToInt(normalizedDir.z), 0);

            projectedNode = lastSeenNode;
            int projectedDistance = 0; //The number of nodes/distance we are able to project successfully

            for (int i = 0; i < 12; i++)
            {
                Vector3Int nextNodeCoord = projectedNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(normalizedDir.x), Mathf.RoundToInt(normalizedDir.y), Mathf.RoundToInt(normalizedDir.z));
                Game.Log("AI: " + ai.name + " Distance " + projectedDistance + " attempting to find a node at " + nextNodeCoord + " using normalized direction " + normalizedDir);

                if(PathFinder.Instance.nodeMap.TryGetValue(nextNodeCoord, out foundNode))
                {
                    Game.Log("AI: " + ai.name + " found a valid node at " + foundNode);

                    if(!ai.human.IsTrespassing(foundNode.room, out _, out _))
                    {
                        //Is this visible from the last seen node?
                        if (DataRaycastController.Instance.NodeRaycast(lastSeenNode, foundNode, out _))
                        {
                            Game.Log("AI: " + ai.name + " data raycast between " + lastSeenPosition + " and node at " + foundNode.position);

                            //Update projected node
                            projectedNode = foundNode;
                            projectedDistance++;
                            continue;
                        }
                    }
                }

                //If under a certain distance, use an entrance of this room to project to...
                if(projectedDistance < 4)
                {
                    NewRoom projectedRoom = null;
                    float projectedRoomRank = -99999f;

                    if(projectedNode.room != ai.human.currentRoom)
                    {
                        projectedRoom = projectedNode.room;
                        projectedRoomRank = Toolbox.Instance.Rand(0f, 2f);

                        if(ai.persuitTarget != null && ai.persuitTarget.currentRoom == projectedRoom)
                        {
                            projectedRoomRank += 1f;
                        }
                    }

                    foreach(NewNode.NodeAccess acc in projectedNode.room.entrances)
                    {
                        if(acc.walkingAccess)
                        {
                            NewRoom otherRoom = acc.toNode.room;
                            if (otherRoom == projectedNode.room) otherRoom = acc.fromNode.room;

                            if(!ai.human.IsTrespassing(otherRoom, out _, out _))
                            {
                                float rank = Toolbox.Instance.Rand(0f, 2f); //Start with random

                                //Within certain distance
                                float dist = Vector3.Distance(projectedNode.position, acc.worldAccessPoint);


                                if (ai.persuitTarget != null && ai.persuitTarget.currentRoom == projectedRoom)
                                {
                                    rank += 1f;
                                }

                                if (dist < 10f && dist > 1.2f)
                                {
                                    if(rank > projectedRoomRank)
                                    {
                                        projectedRoom = otherRoom;
                                        projectedRoomRank = rank;
                                    }
                                }
                            }
                        }
                    }

                    if(projectedRoom != null)
                    {
                        //Get random node in room...
                        NewNode newN = ai.human.FindSafeTeleport(projectedRoom);

                        if (newN != null)
                        {
                            projectedNode = newN;
                            Game.Log("AI: " + ai.name + " found room using an entrance for " + projectedNode.room.name);
                        }
                    }

                    break;
                }
            }

            projectedPosition = projectedNode.position;

            if (ai.persuitTarget != null && ai.persuitTarget.isPlayer) Game.Log("AI: " + ai.name + " generated new projection of player at " + projectedPosition);
        }
    }

    public enum InvestigationUrgency { walk, run};

    public ReactionIndicatorController reactionIndicator;
    public enum ReactionState { none, investigatingSight, investigatingSound, persuing, searching};
    public ReactionState reactionState = ReactionState.none;

    [Header("Patrol AI")]
    public NewGameLocation patrolLocation; //Randomly patrol this gamelocation

    [Header("Attack")]
    public bool inCombat = false; //True if AI is in combat
    public bool inFleeState = false; //True if AI is in flee state
    public bool staticFromAnimation = false; //If true, an animation is blocking any physical movemnet
    public float staticAnimationSafetyTimer = 0f;
    public bool attackActive = false; //Is this AI attacking?
    public Actor attackTarget;
    public AttackBarController activeAttackBar;
    public float attackTimeout = 0f;
    public float attackProgress = 0f;
    private int revolverShots = 0;
    public bool damageColliderCreated = false;
    private bool ejectBrassCreated = false;
    public DamageColliderController damageCollider;
    public float attackDelay = 0f; //For delays between attacks
    private float attackActiveLength = 0f; //Failsafe for returning from active attack
    public bool ko = false; //True if AI is knocked-out
    public bool isRagdoll = false;
    public float koTime = 0f;
    public float koTransitionTimer = 0f;
    private float getUpDelayTimer = 0f;
    public float deadRagdollTimer = 0f;
    public bool restrained = false; //True if AI is restrained
    public bool outOfBreath = false;
    public float restrainTime = 0f;

    [System.NonSerialized]
    public Interactable currentWeapon; //The current weapon being used to attack
    public MurderWeaponPreset currentWeaponPreset; //Data for current weapon being used to attack
    public float weaponRangeMax = 1.75f;
    public float weaponRefire = 1f;
    public float weaponAccuracy = 1f;
    public float weaponDamage = 0.1f;

    [Header("Update")]
    //The desired tick rate unaffected by current actions
    public AITickRate desiredTickRate = AITickRate.medium; //How often the AI runs it's update. Can be lower for citizens away from player or doing certain activities
    //The actual tick rate affected by current actions
    public AITickRate previousTickRate = AITickRate.medium;
    public AITickRate tickRate = AITickRate.medium;
    public enum AITickRate { veryLow, low, medium, high, veryHigh };
    public bool dueUpdate = false; //True if this has been added to the update list, but it has not yet been executed...
    public float delayedUntil = 0f; //This person is delayed until...

    public float lastUpdated = 0f;
    private float lastSnore = 0f;
    public float timeSinceLastUpdate = 0f;
    public float timeAtCurrentAddress = 0f; //Track time spent at the same address

    private float drunkTripCheckTimer = 0f;

    private int doorCheckProcessTimer = 0;

    //For checking if loop is required
    private bool visibleMovementAnimationLerpRequired = false;

    //Is this is true it will stop the AI desired tick rate from being updated
    public bool disableTickRateUpdate = false;

    //Disabled goals for x time
    public Dictionary<AIGoalPreset, float> delayedGoalsForTime = new Dictionary<AIGoalPreset, float>();
    public Dictionary<AIActionPreset, float> delayedActionsForTime = new Dictionary<AIActionPreset, float>();

    //Queued actions are actions with time delays that will activate after a certain time...
    public class QueuedAction
    {
        public Interactable interactable;
        public InteractablePreset.InteractionAction actionSetting;
        public float delay;
    }

    public List<QueuedAction> queuedActions = new List<QueuedAction>();

    [Header("Held Items")]
    public GameObject spawnedRightItem;
    public GameObject spawnedLeftItem;
    [System.NonSerialized]
    public NewAIAction customItemSource;
    public bool usingCarryAnimation = false;
    public int combatMode = 0;
    [System.NonSerialized]
    public InteractablePreset throwItem = null;
    public bool throwActive = false;
    public float throwDelay = 0f;

    [Header("Special Cases")]
    public bool dontEverCloseDoors = false; //If true this AI will never close a door
    public List<MurderController.Murder> victimsForMurders = new List<MurderController.Murder>(); //Reference to victim
    public List<MurderController.Murder> killerForMurders = new List<MurderController.Murder>();
    public bool isConvicted = false;
    private bool usePointBusyRecursion = false;
    [System.NonSerialized]
    public NewGameLocation closeDoorsNormallyAfterLeaving = null;
    public List<Interactable> putDownItems = new List<Interactable>();
    private float drunkIdleTimer = 0f;
    private float restrainedIdleTimer = 0f;
    public Dictionary<Human, float> appliedNerveEffect = new Dictionary<Human, float>(); //For applying feat based on weapons drawn
    private bool tickActive = false; //Used to stop recursion errors within ticks
    public float spooked = 0f;
    public int spookCounter = 0;
    public float spookForgetCounter = 0f;

    [Header("Debug")]
    public List<string> lastActions = new List<string>();
    public List<string> debugDestinationPosition = new List<string>();
    public string jobDebug;
    public bool debugMovement = false;
    public AudioEvent debugLastHeardIllegalAudio;

    protected List<AIActionPreset> rem = new List<AIActionPreset>();

    public void Setup(Human newParent)
    {
        human = newParent;
        human.ai = this;
        lastUpdated = SessionData.Instance.gameTime;
        SetDesiredTickRate(AITickRate.veryLow, true); //Set to very low to begin with
        original = human.lookAtThisTransform.rotation;

        //Setup neck transforms
        lookingQuatPrevious = human.neckTransform.rotation;
        lookingQuatCurrent = human.neckTransform.rotation;

        SetExpression(CitizenOutfitController.Expression.neutral);
        UpdateCurrentWeapon();
    }

    //Update goal list: Refresh priorities and update currently active. AI Tick.
    public void AITick(bool forceUpdatePriorities = false)
    {
        //Return if dead
        if (human.isDead) return;
        if (Game.Instance.pauseAI) return;

        //Used to stop recursion errors
        if (tickActive) return;
        tickActive = true;

        if (Game.Instance.collectDebugData) human.SelectedDebug("AITick", Actor.HumanDebug.updates);

        //Update time since last update
        timeSinceLastUpdate = SessionData.Instance.gameTime - lastUpdated;
        timeAtCurrentAddress += timeSinceLastUpdate;
        dueUpdate = false; //Reset due update flag

        //Remove spook counter
        if(spookCounter > 0)
        {
            spookForgetCounter += timeSinceLastUpdate;

            if(spookForgetCounter >= 1f)
            {
                spookForgetCounter = 0;
                spookCounter--;
            }
        }

        //Remove delayed actions for time
        if (delayedActionsForTime.Count > 0)
        {
            rem.Clear();

            foreach (KeyValuePair<AIActionPreset, float> pair in delayedActionsForTime)
            {
                if (SessionData.Instance.gameTime > pair.Value)
                {
                    rem.Add(pair.Key);
                }
            }

            foreach (AIActionPreset p in rem)
            {
                delayedActionsForTime.Remove(p);
            }
        }

        if (human.isOnStreet)
        {
            human.AddHeat(timeSinceLastUpdate * SessionData.Instance.temperature);
        }
        else
        {
            human.AddHeat(timeSinceLastUpdate * GameplayControls.Instance.indoorTemperature);
        }

        //Drop litter
        if (human.isLitterBug)
        {
            if (human.isMoving && human.currentNode != null)
            {
                if (human.trash.Count > 0)
                {
                    if (Toolbox.Instance.Rand(0f, 1f) > 0.6f)
                    {
                        for (int i = 0; i < human.trash.Count; i++)
                        {
                            MetaObject t = CityData.Instance.FindMetaObject(human.trash[i]);

                            if(t != null)
                            {
                                InteractablePreset newPreset = null;

                                if (Toolbox.Instance.objectPresetDictionary.TryGetValue(t.preset, out newPreset))
                                {
                                    //Check compatible...
                                    if (newPreset.disposal == Human.DisposalType.homeOnly && !human.isHome) continue;
                                    else if(newPreset.disposal == Human.DisposalType.workOnly && human.job.employer != null && !human.isAtWork) continue;
                                    else if(newPreset.disposal == Human.DisposalType.homeOrWork && !human.isHome && !human.isAtWork) continue;

                                    //Add created time and trash flag to passed...
                                    t.passed.Add(new Interactable.Passed(Interactable.PassedVarType.isTrash, SessionData.Instance.gameTime)); //Pass trash variable and when it was turned into an interactable...

                                    Vector2 c = (UnityEngine.Random.insideUnitCircle * 0.5f);
                                    Vector3 r = human.transform.position;

                                    //Spawn in a small radius around this...
                                    if (human.currentNode.walkableNodeSpace.Count > 0)
                                    {
                                        List<NewNode.NodeSpace> nodeSpace = new List<NewNode.NodeSpace>(human.currentNode.walkableNodeSpace.Values);
                                        r = nodeSpace[Toolbox.Instance.Rand(0, nodeSpace.Count)].position + new Vector3(c.x, 0, c.y);
                                    }
                                    else
                                    {
                                        r += new Vector3(c.x, 0, c.y);
                                    }

                                    //Basic rotation...
                                    Vector3 rot = new Vector3(0, Toolbox.Instance.Rand(0f, 360f));

                                    //Drop on an angle...
                                    if(Toolbox.Instance.Rand(0f, 1f) <= newPreset.chanceOfDroppedAngle)
                                    {
                                        rot += new Vector3(-90f, 0, 0);
                                        r.y += newPreset.droppedAngleHeightBoost;
                                    }

                                    Interactable newTrash = InteractableCreator.Instance.CreateWorldInteractableFromMetaObject(t, newPreset, r, rot);
                                    newTrash.MarkAsTrash(true);
                                    newTrash.ft = true; //Force this to be spawned with colliders as triggers
                                    //Game.Log(name + ": Literbug trash " + newTrash);

                                    if (newPreset.disposal == Human.DisposalType.anywhere) human.anywhereTrash--;
                                    human.trash.RemoveAt(i);
                                    break;
                                }
                                else
                                {
                                    Game.LogError("Could not find preset for " + t.preset);
                                }
                            }

                            human.trash.RemoveAt(i);
                            i--;
                        }
                    }
                }
            }
        }

        //Update node space position
        if (human.visible)
        {
            human.UpdateCurrentNodeSpace();
        }

        //Decrease stats as time passes
        StatusStatUpdate();

        //If poisioned, force update the priorities
        if (human.poisoned > 0f || (human.currentRoom != null && human.currentRoom.gasLevel > 0.25f))
        {
            forceUpdatePriorities = true;
        }
        //Do the same if blinded
        else if(human.blinded > 0f)
        {
            forceUpdatePriorities = true;
        }

        //Ignore this update if in interaction
        if(!forceUpdatePriorities)
        {
            if (human.interactingWith != null)
            {
                if (Game.Instance.collectDebugData) human.SelectedDebug("Is interacting with " + human.interactingWith, Actor.HumanDebug.misc);
                tickActive = false;
                return;
            }

            //Ignore this update if in conversation
            if (human.inConversation)
            {
                //human.SelectedDebug("Is in conversation " + human.interactingWith);
                tickActive = false;
                return;
            }
        }

        //Copy human status to this for easy debugging (feel free to remove this after AI is completed)
        if(Game.Instance.devMode && Game.Instance.collectDebugData)
        {
            nourishment = human.nourishment;
            hydration = human.hydration;
            alertness = human.alertness;
            excitement = human.excitement;
            energy = human.energy;
            chores = human.chores;
            hygiene = human.hygiene;
            bladder = human.bladder;
            heat = human.heat;
            drunk = human.drunk;
            breath = human.breath;
        }

        //Set un-delayed
        if (human.isDelayed)
        {
            if (SessionData.Instance.gameTime > delayedUntil)
            {
                human.isDelayed = false;
                delayedUntil = 0f;
            }
        }

        //Update priorities for all goals
        if(!human.isStunned)
        {
            //Confined location check
            bool confined = false;
            NewGameLocation cfLoc = null;

            if (confineLocation != null)
            {
                cfLoc = confineLocation;
                confined = true;
            }

            if (victimsForMurders.Count > 0)
            {
                foreach (MurderController.Murder m in victimsForMurders)
                {
                    if (m.location == null) continue;
                    if ((int)m.state < 4) continue;

                    if (m.preset.blockVictimFromLeavingLocation)
                    {
                        confined = true;
                        cfLoc = m.location;
                        break;
                    }
                }
            }

            if(confined)
            {
                if(currentAction != null && currentAction.node != null && currentAction.node.gameLocation != cfLoc)
                {
                    if (Game.Instance.collectDebugData) human.SelectedDebug("Current desitnation for " + currentAction.name + " is not at confined location: " + cfLoc.name, Actor.HumanDebug.updates);
                    currentAction.Remove(currentAction.preset.repeatDelayOnActionFail);
                }
            }

            //Update goals
            if (!forceUpdatePriorities)
            {
                forceUpdatePriorities = true;

                if(currentAction != null && currentAction.isActive)
                {
                    if(currentAction.dontUpdateGoalPriorityForExtraTime > 0f)
                    {
                        if (Game.Instance.collectDebugData) human.SelectedDebug("Block update of priorities: Current action extra time: " + currentAction.dontUpdateGoalPriorityForExtraTime, Actor.HumanDebug.updates);
                        forceUpdatePriorities = false;
                    }
                    else if(currentAction.preset.dontUpdateGoalPriorityWhileActive)
                    {
                        if (Game.Instance.collectDebugData) human.SelectedDebug("Block update of priorities: While current action active", Actor.HumanDebug.updates);
                        forceUpdatePriorities = false;
                    }
                }

                if(currentGoal != null)
                {
                    if(currentGoal.isActive && currentGoal.preset.dontUpdateGoalPriorityWhileActive)
                    {
                        if (Game.Instance.collectDebugData) human.SelectedDebug("Block update of priorities: Goal blocking update", Actor.HumanDebug.updates);
                        forceUpdatePriorities = false;
                    }
                }
            }

            if(forceUpdatePriorities)
            {
                //Remove delayed goals for time
                if(delayedGoalsForTime.Count > 0)
                {
                    List<AIGoalPreset> rem = new List<AIGoalPreset>();

                    foreach(KeyValuePair<AIGoalPreset, float> pair in delayedGoalsForTime)
                    {
                        if(SessionData.Instance.gameTime > pair.Value)
                        {
                            rem.Add(pair.Key);
                        }
                    }

                    foreach(AIGoalPreset p in rem)
                    {
                        delayedGoalsForTime.Remove(p);
                    }
                }

                for (int i = 0; i < goals.Count; i++)
                {
                    goals[i].UpdatePriority();
                }
            }

            //Do a sightings check...
            if (human.seesOthers)
            {
                //Only if not asleep
                human.SightingCheck(GameplayControls.Instance.citizenFOV);
            }

            if (investigateLocation != null)
            {
                float investigationTime = SessionData.Instance.gameTime - lastInvestigate;

                //Force tick rate of high or higher
                if (tickRate != AITickRate.veryHigh)
                {
                    SetDesiredTickRate(AITickRate.veryHigh);
                }

                if (investigationTime <= (CitizenControls.Instance.minimumInvestigateTime * minimumInvestigationTimeMultiplier))
                {
                    investigationGoal.priority = 11;
                }
                else
                {
                    investigationGoal.priority = 0;

                    //Reset this unless we're killing someone!
                    if (!killerForMurders.Exists(item => item.state == MurderController.MurderState.executing))
                    {
                        ResetInvestigate();
                    }
                }

                TriggerReactionIndicator(); //Make sure this is displayed as avoiding
            }
            else
            {
                investigationGoal.priority = 0;
            }

            goals.Sort((p2, p1) => p1.priority.CompareTo(p2.priority)); //Highest priority first

            //Pause currently active goal if there is one
            if (goals.Count > 0)
            {
                if (Game.Instance.collectDebugData) human.SelectedDebug("...Checking for higher priority goal...", Actor.HumanDebug.actions);

                if (goals[0] != currentGoal)
                {
                    bool newGoal = false;
                    NewAIGoal interuptGoal = goals[0];

                    if (Game.Instance.collectDebugData) human.SelectedDebug("...The current goal " + currentGoal + " is different to the top one " + goals[0], Actor.HumanDebug.actions);

                    if (currentGoal != null)
                    {
                        if (currentGoal.preset.interuptable)
                        {
                            //Beat a threshold to interupt
                            int interuptionThreshold = 0;
                            if(currentGoal.preset.useInteruptionThreshold) interuptionThreshold = currentGoal.preset.interuptionThreshold;

                            if (currentGoal.preset.unteruptableByFollowingCategories)
                            {
                                interuptGoal = null;

                                foreach (NewAIGoal g in goals)
                                {
                                    if (g == currentGoal) continue;
                                    if (currentGoal.preset.uninteruptableByCategories.Contains(g.preset.category)) continue;

                                    if(g.priority >= currentGoal.priority + interuptionThreshold)
                                    {
                                        if(interuptGoal == null || g.priority > interuptGoal.priority)
                                        {
                                            newGoal = true;
                                            interuptGoal = g;
                                        }
                                    }
                                }
                            }
                            else if (goals[0].priority >= currentGoal.priority + interuptionThreshold)
                            {
                                newGoal = true;
                                interuptGoal = goals[0];
                                if (Game.Instance.collectDebugData) human.SelectedDebug("...The higher ranking goal (" + goals[0].priority + ") is enough to interupt the current goal (" + (currentGoal.priority + currentGoal.preset.interuptionThreshold) + ")", Actor.HumanDebug.actions);
                            }
                            else
                            {
                                if (Game.Instance.collectDebugData) human.SelectedDebug("...The top ranked goal " + goals[0].preset.name + " priority " + goals[0].priority + " is lower than the current goal rank + interuption threshold " + currentGoal.priority + " + " + currentGoal.preset.interuptionThreshold, Actor.HumanDebug.actions);
                            }
                        }
                        else
                        {
                            if (Game.Instance.collectDebugData) human.SelectedDebug("...The current goal is uninteruptable", Actor.HumanDebug.actions);
                        }

                        //The current action can override whether this is interuptable...
                        if(currentAction != null && currentAction.preset.uninteruptableWhileAtLocation && currentAction.isAtLocation)
                        {
                            if (Game.Instance.collectDebugData) human.SelectedDebug("...The current action is uninteruptable while at location", Actor.HumanDebug.actions);
                            newGoal = false;
                        }
                    }
                    else
                    {
                        newGoal = true;

                        //Deactivate any goals that are active for some reason
                        foreach(NewAIGoal g in goals)
                        {
                            if(g.isActive)
                            {
                                g.OnDeactivate(0.1f);
                            }
                        }
                    }

                    if (newGoal && interuptGoal != null)
                    {
                        //Deactivate previous
                        if (currentGoal != null)
                        {
                            currentGoal.OnDeactivate(currentGoal.preset.repeatDelayOnInterupt);
                        }

                        if (Game.Instance.collectDebugData) human.SelectedDebug("Activate a new goal: " + interuptGoal.preset.name, Actor.HumanDebug.actions);

                        interuptGoal.OnActivate();

                        //Trigger flee state flag
                        if(currentGoal != null && currentGoal.preset == RoutineControls.Instance.fleeGoal)
                        {
                            inFleeState = true;
                        }
                        else
                        {
                            inFleeState = false;
                        }
                    }
                }

                //Need a null check here as it could be removed above...
                if (currentGoal != null)
                {
                    //human.SelectedDebug("Call a new tick for the current goal: " + currentGoal.preset.name);

                    currentGoal.AITick();
                }
                else if (Game.Instance.collectDebugData) human.SelectedDebug("Current goal is null!", Actor.HumanDebug.actions);
            }
            else
            {
                inFleeState = false;
                currentGoal = null;

                if (Game.Instance.collectDebugData)
                {
                    human.SelectedDebug("Set current goal to null", Actor.HumanDebug.actions);
                }
            }

            //Reset state if not searching
            if (investigateLocation != null && !investigationGoal.isActive)
            {
                if(killerForMurders.Count <= 0 || !killerForMurders.Exists(item => item.state == MurderController.MurderState.executing))
                {
                    SetReactionState(ReactionState.none);
                }
            }

            //Snoring SFX
            if (human.isAsleep)
            {
                //Force awaken if at max energy
                if (human.energy >= 0.99f)
                {
                    human.WakeUp();
                }

                if (human.awakenPromt <= 0)
                {
                    //Add sleep depth
                    human.sleepDepth += (SessionData.Instance.gameTime - lastUpdated) * 0.75f; //Takes about 1.5 hours to get to 1
                    human.sleepDepth = Mathf.Clamp01(human.sleepDepth);

                    if (SessionData.Instance.gameTime >= lastSnore + (human.snoreDelay * 0.0167f))
                    {
                        //Small chance to randomly add awaken promt
                        if (Toolbox.Instance.Rand(0f, 1f) < 0.075f)
                        {
                            AwakenPrompt();
                        }
                        else if (human.snoring >= 0.6f)
                        {
                            if(human.genderScale >= 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.maleSnoreHeavy, human, human.currentNode, human.lookAtThisTransform.position);
                            }
                            else
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.femaleSnoreHeavy, human, human.currentNode, human.lookAtThisTransform.position);
                            }
                        }
                        else
                        {
                            if (human.genderScale >= 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.maleSnoreLight, human, human.currentNode, human.lookAtThisTransform.position);
                            }
                            else
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.femaleSnoreLight, human, human.currentNode, human.lookAtThisTransform.position);
                            }
                        }

                        lastSnore = SessionData.Instance.gameTime;

                        //Bark: Snore zzz
                        human.speechController.TriggerBark(SpeechController.Bark.sleeping);
                    }
                }
                else
                {
                    human.awakenRegen += (SessionData.Instance.gameTime - lastUpdated) * 11f;

                    if(human.awakenRegen >= 1f)
                    {
                        human.awakenRegen = 0f;
                        human.awakenPromt--;
                    }
                }
            }
            else
            {
                //Remove sleep depth
                if (human.sleepDepth > 0)
                {
                    //Remove sleep depth: This ensure citizen goes back to sleep heavily if woken for a brief period
                    human.sleepDepth -= (SessionData.Instance.gameTime - lastUpdated); //Takes about 1 hours to get to 0
                    human.sleepDepth = Mathf.Clamp01(human.sleepDepth);
                }

                //Play idle sound
                if (idleSound <= 0f)
                {
                    //Bark: Idle sound
                    human.speechController.TriggerBark(SpeechController.Bark.idleSounds);

                    idleSound = Toolbox.Instance.Rand(0.9f, 1f);
                }

                //Speech point
                if(currentTrackTarget != null && currentTrackTarget.distance < 8f)
                {
                    //Speech trigger point
                    Actor trackedTarget = null;

                    if (currentTrackTarget != null)
                    {
                        trackedTarget = currentTrackTarget.actor;
                    }

                    human.SpeechTriggerPoint(DDSSaveClasses.TriggerPoint.whileTickOnTrackTarget, trackedTarget);
                }
            }
        }

        //Update last updated tracker
        lastUpdated = SessionData.Instance.gameTime;

        //Remove penalty for recently purchases items
        List<RetailItemPreset> toRemove = new List<RetailItemPreset>();
        List<RetailItemPreset> recentKeys = human.recentPurchases.Keys.ToList();

        foreach(RetailItemPreset rItem in recentKeys)
        {
            human.recentPurchases[rItem] += timeSinceLastUpdate * 2.08f; //Remove -100 points over the course of ~48 hours
            if (human.recentPurchases[rItem] >= 0f) toRemove.Add(rItem);
        }

        foreach(RetailItemPreset r in toRemove)
        {
            human.recentPurchases.Remove(r);
        }

        ////Update meshes (when not visible)
        //if (human.updateMeshList && !human.visible)
        //{
        //    human.UpdateMeshList();
        //}

        tickActive = false; //Used to prevent recursion errors
    }

    //Create a new goal, but perform trait test first and apply multiplier
    public NewAIGoal CreateNewGoal(
        AIGoalPreset newPreset,
        float newTrigerTime,
        float newDuration,
        NewNode newPassedNode = null,
        Interactable newPassedInteractable = null,
        NewGameLocation newPassedGameLocation = null,
        GroupsController.SocialGroup newPassedGroup = null,
        MurderController.Murder newMurderRef = null,
        int newPassedVar = -2
        )
    {
        float priorityMP = 1f;

        //Trait pass...
        if (newPreset.goalModifiers.Count > 0)
        {
            if (!human.TraitGoalTest(newPreset, out priorityMP))
            {
                return null;
            }
        }

        NewAIGoal g = new NewAIGoal(this, newPreset, newTrigerTime, newDuration, newPassedNode, newPassedInteractable, newPassedGameLocation, newPassedGroup, newMurderRef, priorityMP, newPassedVar);

        if(newPreset.forcePriorityUpdateOnCreation && SessionData.Instance.startedGame)
        {
            //for (int i = 0; i < goals.Count; i++)
            //{
            //    goals[i].UpdatePriority();
            //}

            AITick(true);
        }

        return g;
    }


    public NewAIAction CreateNewAction(
        NewAIGoal newGoal,
        AIActionPreset newPreset,
        bool newInsertedAction = false,
        NewRoom newPassedRoom = null,
        Interactable newPassedInteractable = null,
        NewNode newForcedNode = null,
        GroupsController.SocialGroup newPassedGroup = null,
        List<InteractablePreset> newPassedAcquireItems = null,
        bool newForceRun = false,
        int newInsertedActionPriority = 3,
        string newDebug = "")
    {
        if (delayedActionsForTime.ContainsKey(newPreset))
        {
            if(Game.Instance.collectDebugData) human.SelectedDebug("Rejecting new action " + newPreset.name + " as there is an action delay until: " + delayedActionsForTime[newPreset], Actor.HumanDebug.actions);
            return null;
        }

        return new NewAIAction(newGoal, newPreset, newInsertedAction, newPassedRoom, newPassedInteractable, newForcedNode, newPassedGroup, newPassedAcquireItems, newForceRun, newInsertedActionPriority, newDebug);
    }

    //Decrease stats at a regular rate
    public void StatusStatUpdate()
    {
        human.AddNourishment(RoutineControls.Instance.hungerRate * -timeSinceLastUpdate);
        human.AddHydration(RoutineControls.Instance.thirstRate * -timeSinceLastUpdate);
        human.AddAlertness(RoutineControls.Instance.tirednessRate * -timeSinceLastUpdate);
        human.AddEnergy(RoutineControls.Instance.energyRate * -timeSinceLastUpdate);
        human.AddExcitement(RoutineControls.Instance.boredemRate * -timeSinceLastUpdate);
        human.AddChores(RoutineControls.Instance.choresRate * -timeSinceLastUpdate);
        human.AddHygiene(RoutineControls.Instance.hygeieneRate * -timeSinceLastUpdate);
        human.AddBladder(RoutineControls.Instance.bladderRate * -timeSinceLastUpdate);

        float soberMP = 1f;
        if (ko || human.isAsleep) soberMP = 2.5f; //Recover quicker if KO or asleep

        human.AddDrunk(RoutineControls.Instance.drunkRate * -timeSinceLastUpdate * soberMP);
        human.AddBreath(RoutineControls.Instance.breathRate * -timeSinceLastUpdate * Math.Max(0.2f, human.breathRecoveryRate));

        //Remove health if poisoned
        if(human.poisoned > 0f)
        {
            bool allowKill = false;
            
            if(victimsForMurders.Count > 0 && human.poisoner != null)
            {
                foreach(MurderController.Murder m in victimsForMurders)
                {
                    if(m.murderer == human.poisoner)
                    {
                        allowKill = true;
                        break;
                    }
                }
            }

            Transform mouth = human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.Head);

            human.RecieveDamage(human.poisoned * 13f * timeSinceLastUpdate, human.poisoner, mouth.position, mouth.forward, CitizenControls.Instance.vomitSpatter, null, alertSurrounding: false, enableKill: allowKill, allowRecoil: false, shockMP: 0f);
            human.AddPoisoned(RoutineControls.Instance.poisonRate * -timeSinceLastUpdate, null);
        }

        if(human.blinded > 0f)
        {
            human.AddBlinded(RoutineControls.Instance.blindedRate * -timeSinceLastUpdate);
        }

        if(human.currentRoom != null && human.currentRoom.gasLevel >= 0.2f)
        {
            human.AddNerve(-10f * timeSinceLastUpdate);
            Transform mouth = human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.Head);
            human.RecieveDamage(human.currentRoom.gasLevel * 10f * timeSinceLastUpdate, null, mouth.position, mouth.forward, CitizenControls.Instance.vomitSpatter, null, alertSurrounding: false, enableKill: false, allowRecoil: false, shockMP: 0f);
        }

        //Add idle sound timer
        idleSound += RoutineControls.Instance.idleSoundRate * -timeSinceLastUpdate;

        if(human.drunk > 0.2f)
        {
            drunkIdleTimer += timeSinceLastUpdate * human.drunk;

            if(drunkIdleTimer >= 0.16f)
            {
                human.speechController.TriggerBark(SpeechController.Bark.drunkIdle);
                drunkIdleTimer = 0f;
            }
        }

        if (restrained)
        {
            restrainedIdleTimer += timeSinceLastUpdate;

            if (restrainedIdleTimer >= 0.18f)
            {
                human.speechController.TriggerBark(SpeechController.Bark.restrainedIdle);
                restrainedIdleTimer = 0f;
            }
        }

        //Recover health
        if (human.currentHealth < human.maximumHealth)
        {
            float amount = human.recoveryRate * timeSinceLastUpdate;
            if (human.isAsleep) amount *= 2f; //Double speed health recovered when asleep

            if(human.currentNerve > 0.15f && human.heat > 0.5f) human.AddHealth(amount);
        }

        if(human.currentNerve < human.maxNerve)
        {
            float amount = human.recoveryRate * timeSinceLastUpdate;
            if (human.isAsleep) amount *= 2f; //Double speed health recovered when asleep

            if (human.lastScaredBy == null || (human.ai != null && !human.ai.trackedTargets.Exists(item => item.actor == human.lastScaredBy)))
            {
                human.AddNerve(amount * CitizenControls.Instance.nerveRecoveryRateMultiplier); //Nerve takes longer to get back
            }
        }
    }

    //Triggered when a goal has been completed (will be removed from list by the goal class)
    public void OnCompleteGoal(NewAIGoal completed)
    {
        AddDebugAction("Completed goal " + completed.name);
    }

    //Set the desired tick rate, most often based on player vicinity
    public void SetDesiredTickRate(AITickRate newRate, bool forceUpdate = false)
    {
        if (disableTickRateUpdate) return;

        if(desiredTickRate != newRate || forceUpdate)
        {
            if(investigateLocation != null || human.seesIllegal.Count > 0)
            {
                desiredTickRate = AITickRate.veryHigh;
            }
            else
            {
                desiredTickRate = newRate;
            }

            UpdateTickRate(forceUpdate);
        }
    }

    public void UpdateTickRate(bool forceUpdate = false)
    {
        previousTickRate = tickRate;
        tickRate = desiredTickRate;

        //Override tick rate
        if(currentAction != null)
        {
            if(currentAction.preset.limitTickRate)
            {
                //Above minimum
                if ((int)tickRate < (int)currentAction.preset.minimumTickRate)
                {
                    tickRate = currentAction.preset.minimumTickRate;
                }

                //Below maximum
                if ((int)tickRate > (int)currentAction.preset.maximumTickRate)
                {
                    tickRate = currentAction.preset.maximumTickRate;
                }
            }
        }

        //Set to correct list
        if(previousTickRate != tickRate || forceUpdate)
        {
            //Remove from previous list
            if(previousTickRate == AITickRate.veryLow)
            {
                CitizenBehaviour.Instance.veryLowTickRate.Remove(this);
            }
            else if(previousTickRate == AITickRate.low)
            {
                CitizenBehaviour.Instance.lowTickRate.Remove(this);
            }
            else if(previousTickRate == AITickRate.medium)
            {
                CitizenBehaviour.Instance.mediumTickRate.Remove(this);
            }
            else if(previousTickRate == AITickRate.high)
            {
                CitizenBehaviour.Instance.highTickRate.Remove(this);
            }
            else if(previousTickRate == AITickRate.veryHigh)
            {
                CitizenBehaviour.Instance.veryHighTickRate.Remove(this);
            }

            //Add to new list
            if (tickRate == AITickRate.veryLow)
            {
                CitizenBehaviour.Instance.veryLowTickRate.Add(this);
            }
            else if (tickRate == AITickRate.low)
            {
                CitizenBehaviour.Instance.lowTickRate.Add(this);
            }
            else if (tickRate == AITickRate.medium)
            {
                CitizenBehaviour.Instance.mediumTickRate.Add(this);
            }
            else if (tickRate == AITickRate.high)
            {
                CitizenBehaviour.Instance.highTickRate.Add(this);
            }
            else if (tickRate == AITickRate.veryHigh)
            {
                CitizenBehaviour.Instance.veryHighTickRate.Add(this);
            }
        }
    }

    //Similar to a LateUpdate function but is run less frequently when further away
    //Doing this in a late update allows the head to move independent from animation.
    public void FrequentUpdate()
    {
        if (!SessionData.Instance.startedGame) return;

        delta = Time.timeSinceLevelLoad - prevDelta;
        prevDelta = Time.timeSinceLevelLoad;

        if (SessionData.Instance.play && !Game.Instance.pauseAI)
        {
            visibleMovementAnimationLerpRequired = false;
            doIMove = false;
            facingActive = false;

            //Reset arms one-shot state
            if (human.animationController.armsBoolAnimationState == CitizenAnimationController.ArmsBoolSate.armsOneShotUse)
            {
                human.animationController.oneShotUseReset -= delta;

                if (human.animationController.oneShotUseReset <= 0f)
                {
                    human.animationController.SetArmsBoolState(CitizenAnimationController.ArmsBoolSate.none);
                }
            }

            //Safety event from setting static from animation
            if (staticFromAnimation)
            {
                staticAnimationSafetyTimer -= delta;

                if (staticAnimationSafetyTimer <= 0f)
                {
                    SetStaticFromAnimation(false);
                }
            }

            //Remove spooked
            if (spooked > 0f)
            {
                AddSpooked(GameplayControls.Instance.loseSpookedRate * delta);
            }

            //Lerp to desired move speed and animation weight if citizen is drawn
            MovementSpeedUpdate();

            //Handle hearing illegal
            HearingUpdate();

            //Cancel states if dead/stunned/delayed
            StatesUpdate();

            //Attack
            AttackUpdate();

            //Ragdoll transition
            KOUpdate();

            //Get out of restaints
            if (restrained)
            {
                if (SessionData.Instance.gameTime > restrainTime)
                {
                    SetRestrained(false, 0);
                }
            }

            //Conversation: If initiator then drive the conversation
            if (human.inConversation)
            {
                human.UpdateConversation();
            }

            //Trigger queued actions
            for (int i = 0; i < queuedActions.Count; i++)
            {
                QueuedAction q = queuedActions[i];
                q.delay -= delta;

                if (q.delay <= 0)
                {
                    q.interactable.OnInteraction(q.actionSetting, human, false);
                    queuedActions.RemoveAt(i);
                    i--;
                    continue;
                }
            }

            //Figure out if this needs to be active by checking against events in this loop...
            //Lerping movement speeds and animation is not needed...
            if (!visibleMovementAnimationLerpRequired)
            {
                //Can't be any of these states
                if (!attackActive && !ko && !isRagdoll && !human.isDelayed && !human.isStunned && !restrained && !outOfBreath)
                {
                    //Must not need to move or chase the player
                    if (!doIMove && !persuit && human.seesIllegal.Count <= 0)
                    {
                        //Must not be in conversation
                        if (!human.inConversation)
                        {
                            //Must not be restrained
                            if (!restrained)
                            {
                                //Must not be facing or looking lerp
                                if (!facingActive)
                                {
                                    //Must not want to move
                                    if (currentAction == null || (currentAction.isAtLocation && !currentAction.preset.requiresForcedUpdate))
                                    {
                                        //No queued actions
                                        if (queuedActions.Count <= 0)
                                        {
                                            //Must be invisible
                                            if (!human.visible)
                                            {
                                                SetUpdateEnabled(false); //Safe to disable
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //Keep neck updated even when paused
        else if (human != null)
        {
            human.neckTransform.rotation = lookingQuatLastFrame;
            ClampNeckRotation();
        }

        //Update meshes (when visible)
        if (human.updateMeshList && human.visible)
        {
            human.UpdateMeshList();
        }
    }

    //Handle update of movement speed and blinking
    private void MovementSpeedUpdate()
    {
        //Lerp to desired move speed and animation weight if citizen is drawn
        if (human.visible && (CityConstructor.Instance == null || !CityConstructor.Instance.preSimActive))
        {
            //Set desired speed...
            //If accelerating
            if (human.currentNormalizedSpeed < human.desiredNormalizedSpeed)
            {
                human.currentNormalizedSpeed += CitizenControls.Instance.acceleration.Evaluate(human.currentNormalizedSpeed) * SessionData.Instance.currentTimeMultiplier * delta;
                human.currentNormalizedSpeed = Mathf.Min(human.currentNormalizedSpeed, human.desiredNormalizedSpeed); //Limit to desired speed
                human.UpdateMovementSpeed();
                visibleMovementAnimationLerpRequired = true;
            }
            //If delecerating
            else if (human.currentNormalizedSpeed > human.desiredNormalizedSpeed)
            {
                //human.SelectedDebug("Decceleration: " + CitizenControls.Instance.decceleration.Evaluate(human.currentNormalizedSpeed) * SessionData.Instance.currentTimeMultiplier * Time.deltaTime);

                human.currentNormalizedSpeed -= CitizenControls.Instance.decceleration.Evaluate(human.currentNormalizedSpeed) * SessionData.Instance.currentTimeMultiplier * delta;
                human.currentNormalizedSpeed = Mathf.Max(human.currentNormalizedSpeed, human.desiredNormalizedSpeed); //Limit to desired speed
                human.UpdateMovementSpeed();
                visibleMovementAnimationLerpRequired = true;
            }

            //If restrained, we want the player to be able to move past them if they happen to be in a doorway...
            if(restrained)
            {
                capCollider.radius = CitizenControls.Instance.capsuleMovementThickness.y;
            }
            else
            {
                capCollider.radius = Mathf.Lerp(CitizenControls.Instance.capsuleMovementThickness.x, CitizenControls.Instance.capsuleMovementThickness.y, human.currentNormalizedSpeed * 2f);
            }

            if (human.animationController.mainAnimator != null)
            {
                //Arms animation weight
                float currentArmsLayerWeight = human.animationController.mainAnimator.GetLayerWeight(1);

                if (currentArmsLayerWeight < human.animationController.armsLayerDesiredWeight)
                {
                    currentArmsLayerWeight = Mathf.Clamp01(currentArmsLayerWeight + delta / 0.15f);
                    human.animationController.mainAnimator.SetLayerWeight(1, currentArmsLayerWeight);
                    visibleMovementAnimationLerpRequired = true;
                }
                else if (currentArmsLayerWeight > human.animationController.armsLayerDesiredWeight)
                {
                    currentArmsLayerWeight = Mathf.Clamp01(currentArmsLayerWeight - delta / 0.15f);
                    human.animationController.mainAnimator.SetLayerWeight(1, currentArmsLayerWeight);
                    visibleMovementAnimationLerpRequired = true;
                }

                //human.SelectedDebug("Set current arms layer weight: " + currentArmsLayerWeight + " (" + human.animationController.armsLayerDesiredWeight + ")");

                float currentUmbrellaLayerWeight = human.animationController.mainAnimator.GetLayerWeight(2);

                if (currentUmbrellaLayerWeight < human.animationController.umbreallLayerDesiredWeight)
                {
                    currentUmbrellaLayerWeight = Mathf.Clamp01(currentUmbrellaLayerWeight + delta / 0.5f);
                    human.animationController.mainAnimator.SetLayerWeight(2, currentUmbrellaLayerWeight);
                    visibleMovementAnimationLerpRequired = true;

                    if (human.animationController.umbrellaCanopy != null)
                    {
                        human.animationController.umbrellaCanopy.localScale = Vector3.Lerp(new Vector3(0.15f, 0.15f, 3f), Vector3.one, currentUmbrellaLayerWeight);
                    }
                }
                else if (currentUmbrellaLayerWeight > human.animationController.umbreallLayerDesiredWeight)
                {
                    currentUmbrellaLayerWeight = Mathf.Clamp01(currentUmbrellaLayerWeight - delta / 0.5f);
                    human.animationController.mainAnimator.SetLayerWeight(2, currentUmbrellaLayerWeight);
                    visibleMovementAnimationLerpRequired = true;

                    if (human.animationController.umbrellaCanopy != null)
                    {
                        human.animationController.umbrellaCanopy.localScale = Vector3.Lerp(new Vector3(0.15f, 0.15f, 3f), Vector3.one, currentUmbrellaLayerWeight);

                        if (currentUmbrellaLayerWeight <= 0)
                        {
                            Destroy(human.animationController.spawnedUmbrella);
                        }
                    }
                }
            }

            //Blink: Only do this if very close to the player (same room)
            if (human.currentRoom == Player.Instance.currentRoom)
            {
                if (blink >= 1f && currentExpression != null && currentExpression.allowBlinking)
                {
                    blinkInProgress = true;
                    blinkTimer = 0f;
                    blink = 0f;
                }
                else if (eyesOpen > 0f && currentExpression != null && currentExpression.allowBlinking)
                {
                    blink += delta * Toolbox.Instance.Rand(0.1f, 0.4f); //Blink every 3 seconds
                }

                //Change expression
                if ((expressionProgress < 1f || blinkInProgress) && currentExpression != null)
                {
                    expressionProgress += delta;

                    human.outfitController.rightEyebrow.localEulerAngles = Vector3.Lerp(human.outfitController.rightEyebrow.localEulerAngles, currentExpression.eyebrowsEuler, expressionProgress);
                    human.outfitController.leftEyebrow.localEulerAngles = Vector3.Lerp(human.outfitController.rightEyebrow.localEulerAngles, -currentExpression.eyebrowsEuler, expressionProgress);

                    float lerpedEyebrowRaise = Mathf.Lerp(human.outfitController.rightEyebrow.localPosition.y, currentExpression.eyebrowsRaise, expressionProgress);
                    human.outfitController.rightEyebrow.localPosition = new Vector3(0.045f, lerpedEyebrowRaise, 0);
                    human.outfitController.leftEyebrow.localPosition = new Vector3(-0.045f, lerpedEyebrowRaise, 0);

                    if (blinkInProgress)
                    {
                        blinkTimer += delta * 10f;

                        if (blinkTimer <= 1f)
                        {
                            float lerpedEyeHeight = Mathf.Lerp(human.outfitController.rightPupil.localScale.y, 0f, blinkTimer);
                            human.outfitController.rightPupil.localScale = new Vector3(0.02f, lerpedEyeHeight, 0.02f);
                            human.outfitController.leftPupil.localScale = new Vector3(0.02f, lerpedEyeHeight, 0.02f);

                            human.outfitController.rightPupil.localPosition = new Vector3(0.045f, -(0.02f - human.outfitController.leftPupil.localScale.y), 0);
                            human.outfitController.leftPupil.localPosition = new Vector3(-0.045f, -(0.02f - human.outfitController.leftPupil.localScale.y), 0);
                        }
                        else if (blinkTimer <= 2f)
                        {
                            float lerpedEyeHeight = Mathf.Lerp(human.outfitController.rightPupil.localScale.y, 0.02f * currentExpression.eyeHeightMultiplier * eyesOpen, blinkTimer - 1f);
                            human.outfitController.rightPupil.localScale = new Vector3(0.02f, lerpedEyeHeight, 0.02f);
                            human.outfitController.leftPupil.localScale = new Vector3(0.02f, lerpedEyeHeight, 0.02f);

                            human.outfitController.rightPupil.localPosition = new Vector3(0.045f, -(0.02f - human.outfitController.leftPupil.localScale.y), 0);
                            human.outfitController.leftPupil.localPosition = new Vector3(-0.045f, -(0.02f - human.outfitController.leftPupil.localScale.y), 0);
                        }
                        else
                        {
                            human.outfitController.rightPupil.localPosition = new Vector3(0.045f, 0, 0);
                            human.outfitController.leftPupil.localPosition = new Vector3(-0.045f, 0, 0);

                            blinkInProgress = false;
                            blinkTimer = 0f;
                        }
                    }
                    else
                    {
                        float lerpedEyeHeight = Mathf.Lerp(human.outfitController.rightPupil.localScale.y, 0.02f * currentExpression.eyeHeightMultiplier * eyesOpen, expressionProgress);
                        human.outfitController.rightPupil.localScale = new Vector3(0.02f, lerpedEyeHeight, 0.02f);
                        human.outfitController.leftPupil.localScale = new Vector3(0.02f, lerpedEyeHeight, 0.02f);
                    }
                }
            }
        }
        else
        {
            //Snap to desired speed
            if (human.currentNormalizedSpeed != human.desiredNormalizedSpeed)
            {
                human.currentNormalizedSpeed = human.desiredNormalizedSpeed;
                human.currentMovementSpeed = human.movementRunSpeed * human.currentNormalizedSpeed;
                human.UpdateMovementSpeed();

                capCollider.radius = Mathf.Lerp(CitizenControls.Instance.capsuleMovementThickness.x, CitizenControls.Instance.capsuleMovementThickness.y, human.currentNormalizedSpeed * 2f);
            }

            //Snap to desired layer weight
            if (human.animationController.mainAnimator != null)
            {
                float currentLayerWeight = human.animationController.mainAnimator.GetLayerWeight(1);

                if (currentLayerWeight != human.animationController.armsLayerDesiredWeight)
                {
                    human.animationController.mainAnimator.SetLayerWeight(1, human.animationController.armsLayerDesiredWeight);
                    //human.SelectedDebug("Set current arms layer weight: " + human.animationController.armsLayerDesiredWeight + " (" + human.animationController.armsLayerDesiredWeight + ")");
                }
            }
        }
    }

    //Handle hearing illegal sounds
    private void HearingUpdate()
    {
        //Handle hearing illegal
        if (hearsIllegal > 0f)
        {
            hearsIllegal -= delta * CitizenControls.Instance.hearingForgetThreshold;

            if (human.inConversation)
            {
                human.currentConversation.EndConversation();
            }

            if (hearsIllegal <= 0f)
            {
                hearsIllegal = 0f;
                hearTarget = null;

                if (audioFocusAction != null)
                {
                    audioFocusAction.Complete();

                    if (reactionState == ReactionState.investigatingSound)
                    {
                        SetReactionState(ReactionState.none);
                    }
                }
            }
        }
    }

    //Handle update of states and cancellation if Ko'd; also movement
    private void StatesUpdate()
    {
        //Cancel states if dead/stunned/delayed
        if (human.isDead || human.isStunned /* || human.isDelayed*/)
        {
            if (attackActive)
            {
                Game.Log(human.name + " Abort attack: Is dead/stunned");
                OnAbortAttack();
            }

            if (reactionState != ReactionState.none)
            {
                SetReactionState(ReactionState.none);
            }

            //End a conversation
            if (human.inConversation)
            {
                human.currentConversation.EndConversation();
            }

            human.SetDesiredSpeed(Human.MovementSpeed.stopped);
        }
        //Normal AI Behaviour
        else
        {
            PersuitUpdate();
            MovementUpdate();
            FacingUpdate();
        }
    }

    //Handle Persuit
    private void PersuitUpdate()
    {
        //If seeing illegal, increase the sight checking
        if (human.seesIllegal.Count > 0 || persuit)
        {
            //Do a persuit check every 5 frames
            if (persuitUpdateTimer > 0f)
            {
                persuitUpdateTimer -= delta;
            }
            else
            {
                if (Game.Instance.collectDebugData) human.SelectedDebug("Persuit update: " + human.seesIllegal.Count + " target(s) seen with persuit active: " + persuit + " for target " + persuitTarget + " at escalation level " + human.escalationLevel, Actor.HumanDebug.updates);

                //Update roughly 3 times a second...
                persuitUpdateTimer = 0.33f;

                //Perform a frequency sighting check if either persuing or focusing...
                human.SightingCheck(GameplayControls.Instance.citizenFOV);

                //Decide to persue a target...
                if (/*!persuit &&*/ human.escalationLevel >= 2)
                {
                    List<Actor> keys = new List<Actor>(human.seesIllegal.Keys);

                    foreach (Actor key in keys)
                    {
                        if (!human.seesIllegal.ContainsKey(key)) continue;

                        float value = human.seesIllegal[key];

                        if (Game.Instance.collectDebugData) human.SelectedDebug("Sees illegal value for " + key + ": " + value, Actor.HumanDebug.sight);

                        if (value >= 1f)
                        {
                            if (currentAction != null)
                            {
                                if (currentAction.preset.completeOnSeeIllegal)
                                {
                                    currentAction.progress = 1f;
                                    currentAction.ImmediateComplete();
                                }
                            }

                            //Found a valid persuit target...
                            if (Game.Instance.collectDebugData) human.SelectedDebug("Found a valid persuit target: " + key, Actor.HumanDebug.sight);
                            SetPersuit(true);
                            SetPersueTarget(key);
                            persuitChaseLogicUses = 0.5f; //Reset persuit logic to 1

                            //Set reaction state to persue
                            SetReactionState(ReactionState.persuing);

                            if (key.isPlayer)
                            {
                                InterfaceControls.Instance.seenJuice.Nudge(new Vector2(1.3f, 1.3f), new Vector2(2, 2));

                                //Confirm trespassing
                                if (Player.Instance.currentGameLocation.thisAsAddress != null && Player.Instance.illegalActionActive)
                                {
                                    StatusController.Instance.ConfirmFine(Player.Instance.currentGameLocation.thisAsAddress, null, StatusController.CrimeType.trespassing);
                                }
                            }

                            break;
                        }
                        //Investigate once past a certain threshold, but don't persue...
                        else if (value >= 0.75f)
                        {
                            if (currentAction != null)
                            {
                                if (currentAction.preset.completeOnSeeIllegal)
                                {
                                    currentAction.progress = 1f;
                                    currentAction.ImmediateComplete();
                                }
                            }

                            if (key.isPlayer)
                            {
                                InterfaceControls.Instance.seenJuice.Nudge(new Vector2(1.3f, 1.3f), new Vector2(2, 2));
                            }

                            if (Game.Instance.collectDebugData) human.SelectedDebug(value + " Investigate target...", Actor.HumanDebug.sight);

                            //Investigate
                            Investigate(key.currentNode, key.transform.position, key, ReactionState.investigatingSight, CitizenControls.Instance.sightingMinInvestigationTimeMP * 0.8f, 0);
                        }
                    }
                }

                //While on persuit...
                if (persuit && persuitTarget != null)
                {
                    //Can I currently see the target?
                    if (human.seenIllegalThisCheck.Contains(persuitTarget) && !persuitTarget.isStunned && !persuitTarget.isDead)
                    {
                        if (Game.Instance.collectDebugData) human.SelectedDebug("Seen persuit target " + persuitTarget + " this frame...", Actor.HumanDebug.sight);
                        SetSeesOnPersuit(true);
                    }
                    else
                    {
                        if (Game.Instance.collectDebugData) human.SelectedDebug("Not seen persuit target " + persuitTarget + " this frame...", Actor.HumanDebug.sight);
                        SetSeesOnPersuit(false);
                    }

                    //If I can currently see the target on a persuit, investigate their last known position...
                    if (seesOnPersuit)
                    {
                        chaseLogic.UpdateLastSeen();
                        ReactionState r = ReactionState.investigatingSight;
                        float mp = CitizenControls.Instance.sightingMinInvestigationTimeMP;

                        if (persuit)
                        {
                            r = ReactionState.persuing;
                            mp = CitizenControls.Instance.persuitMinInvestigationTimeMP;
                        }

                        Investigate(persuitTarget.currentNode, persuitTarget.transform.position, persuitTarget, r, mp, 0, setHighUrgency: true);
                        AddTrackedTarget(persuitTarget);

                        //Gain 1 persuit logic per sighting, up to a maximum of 4 (if escalation allows)
                        if (human.escalationLevel >= 2)
                        {
                            persuitChaseLogicUses += CitizenControls.Instance.persuitChaseLogicAdditionPerSecond * delta; //Gain a persuit logic every second
                            persuitChaseLogicUses = Mathf.Clamp(persuitChaseLogicUses, 0, CitizenControls.Instance.maxChaseLogic); //Gain at least 1 persuit logic
                        }
                        //If a minor crime, up patience levels before it turns into a more serious crime
                        else
                        {
                            //human.SelectedDebug("AI: Spotted minor crime: " + timesSpottedMinorCrime);
                            //timesSpottedMinorCrime++;

                            //if (timesSpottedMinorCrime >= 12)
                            //{
                            //    human.SetEscalation(1);
                            //    Player.Instance.currentRoom.TriggerTempEscalationBoost(GameplayControls.Instance.temporaryTrespassingEscalationLength);

                            //    //Add peril
                            //    if (Player.Instance.currentGameLocation.thisAsAddress != null)
                            //    {
                            //        StatusController.Instance.ConfirmFine(Player.Instance.currentGameLocation.thisAsAddress, null, StatusController.CrimeType.trespassing);
                            //    }

                            //    Game.Log("AI: Set temporary escalation boost for " + Player.Instance.currentRoom.name);
                            //}

                            //timesSpottedMinorCrime = Mathf.Clamp(timesSpottedMinorCrime, 0, 12);
                        }
                    }
                    //Unable to see the target, use up persuit logic...
                    else if (currentAction != null && !currentAction.isAtLocation)
                    {
                        //Not yet at destination, continue chase...

                        if (persuitChaseLogicUses > 0f)
                        {
                            //Add persuit logic
                            persuitChaseLogicUses += 0.1f;
                        }
                    }
                    else if (persuitChaseLogicUses >= 1f)
                    {
                        //New system; create projection...
                        chaseLogic.GenerateProjectedNode();
                        Investigate(chaseLogic.projectedNode, chaseLogic.projectedPosition, persuitTarget, ReactionState.persuing, CitizenControls.Instance.persuitMinInvestigationTimeMP, 0, setHighUrgency: true);
                        persuitChaseLogicUses = 0f;
                        if (Game.Instance.collectDebugData) human.SelectedDebug("Use a persuit logic (" + persuitChaseLogicUses + " left), investigate: " + chaseLogic.projectedPosition, Actor.HumanDebug.sight);

                        //if (persuitTarget.currentNode != investigateLocation)
                        //{
                        //    Debug.DrawRay(human.lookAtThisTransform.position, Player.Instance.transform.position - human.lookAtThisTransform.position, Color.cyan, 3f);
                        //    Investigate(persuitTarget.currentNode, persuitTarget.transform.position, persuitTarget, ReactionState.persuing, CitizenControls.Instance.persuitMinInvestigationTimeMP, 0, setHighUrgency: true);
                        //    persuitChaseLogicUses -= 1f;
                        //    human.SelectedDebug("Use a persuit logic (" + persuitChaseLogicUses + " left), investigate: " + persuitTarget.transform.position);
                        //}
                    }
                    //Otherwise...
                    else
                    {
                        //When focus is empty, cancel persue...
                        if (!human.seesIllegal.ContainsKey(persuitTarget))
                        {
                            CancelPersue();

                            if (human.escalationLevel >= 2)
                            {
                                SetReactionState(ReactionState.searching);
                            }
                            else SetReactionState(ReactionState.none);
                        }
                    }
                }
                else if (persuit)
                {
                    CancelPersue();
                }
            }
        }
    }

    //Handle movement
    private void MovementUpdate()
    {
        doIMove = false;

        if (currentAction != null && currentAction.isActive)
        {
            if (!ko && !restrained && !outOfBreath && !staticFromAnimation)
            {
                if (!currentAction.isAtLocation)
                {
                    if (human.interactingWith == null && (!human.inConversation || !human.currentConversation.tree.stopMovement))
                    {
                        if(doorCheck)
                        {
                            if (currentAction.path != null && pathCursor < currentAction.path.accessList.Count)
                            {
                                doIMove = true;
                                if (debugMovement) Game.Log("AI: " + human.name + " Moving.");
                            }
                            else if (this.transform.position != currentDestinationPositon)
                            {
                                doIMove = true;
                                if (debugMovement) Game.Log("AI: " + human.name + " Moving.");
                            }
                            else if (debugMovement) Game.Log("AI: " + human.name + " Not moving: Path cursor " + pathCursor + "/" + currentAction.path.accessList.Count + " dest pos: " + this.transform.position + "/" + currentDestinationPositon);
                        }
                        else
                        {
                            //For failed door check, allow movement within the same node
                            if (currentDestinationNode == human.currentNode)
                            {
                                doIMove = true;
                                if (debugMovement) Game.Log("AI: " + human.name + " Moving. Door check fail but destination is the current node " + currentDestinationNode.position);
                            }
                            else if (debugMovement) Game.Log("AI: " + human.name + " Not moving: Door check is false");
                        }
                    }
                    else if (debugMovement) Game.Log("AI: Not moving: Interacting");
                }
                else if (debugMovement) Game.Log("AI: " + human.name + " Not moving: Is at location: " + currentAction.isAtLocation);
            }
            else if (debugMovement) Game.Log("AI: " + human.name + " Not moving: KO: " + ko + " restrained: " + restrained + " staticAnim: " + staticFromAnimation + " out of breath: " + outOfBreath);
        }
        else if (debugMovement) Game.Log("AI: " +human.name +" Not moving: Current action is " + currentAction + " or inactive...");

        //Currently not at a location
        if (doIMove)
        {
            //Set moving
            if ((currentAction != null && (currentAction.preset.forceRun || currentAction.forceRun)) || (!inCombat && reactionState == ReactionState.none && spookCounter >= 1 && human.maxNerve < 0.7f && spooked >= 0.5f) || (currentAction.preset.runIfSeesPlayer && trackedTargets.Exists(item => item.actor != null && item.actor.isPlayer)))
            {
                human.SetDesiredSpeed(Human.MovementSpeed.running);
            } 
            //Running from rain if no umbrella
            else if (human.currentGameLocation != null && human.currentGameLocation.isOutside && SessionData.Instance.currentRain > 0.1f && !human.ownsUmbrella && !human.likesTheRain && !human.isHomeless)
            {
                human.SetDesiredSpeed(Human.MovementSpeed.running);
            }
            //Running if late for something
            else if(currentGoal != null && currentGoal.preset.useTiming && currentGoal.preset.runIfLate && human.currentGameLocation != currentGoal.gameLocation && !currentAction.preset.useInvestigationUrgency)
            {
                float timeDiff = (currentGoal.triggerTime - currentGoal.travelTime) - SessionData.Instance.gameTime;

                //As a proportion of the time window...
                //Before...
                if (timeDiff >= 0f && timeDiff <= currentGoal.preset.earlyTimingWindow)
                {
                    human.SetDesiredSpeed(Human.MovementSpeed.running);
                }
                else human.SetDesiredSpeed(Human.MovementSpeed.walking);
            }
            else
            {
                if (currentAction.preset.useInvestigationUrgency)
                {
                    if (investigationUrgency == InvestigationUrgency.run)
                    {
                        human.SetDesiredSpeed(Human.MovementSpeed.running);
                    }
                    else human.SetDesiredSpeed(Human.MovementSpeed.walking);
                }
                else human.SetDesiredSpeed(Human.MovementSpeed.walking);
            }

            //Get speed and distance to cover this frame...
            if (CityConstructor.Instance != null && CityConstructor.Instance.preSimActive)
            {
                movementAmount = human.currentMovementSpeed * SessionData.Instance.currentTimeMultiplier * GameplayControls.Instance.preSimSpeedMultiplier * delta;
            }
            else
            {
                movementAmount = human.currentMovementSpeed * SessionData.Instance.currentTimeMultiplier * delta;

                //A bit of a cheat; increase movement speed when off screen so citizens get places faster
                if(!human.visible)
                {
                    if(human.currentTile != null && human.currentTile.isStairwell)
                    {
                        movementAmount *= CitizenControls.Instance.offscreenStairwellMovementSpeedMultiplier;
                    }
                    else
                    {
                        movementAmount *= CitizenControls.Instance.offscreenMovementSpeedMultiplier;
                    }
                }
            }

            distanceToNext = Vector3.Distance(transform.position, currentDestinationPositon);

            //Check for immediate arrival
            if (distanceToNext <= 0f)
            {
                //New coordinate is reached; but only if no obstacle
                if(doorCheck) ReachNewPathNode("Node Reached"); //Update of game location is contained within this
                else if (!staticFromAnimation) SetStaticFromAnimation(true);

                //The above may cause alterations to the routine...
                if (currentAction == null)
                {
                    human.SetDesiredSpeed(Human.MovementSpeed.stopped);
                }
                else if (currentAction.path == null)
                {
                    human.SetDesiredSpeed(Human.MovementSpeed.stopped);
                }
                else if (currentAction.path.accessList == null)
                {
                    human.SetDesiredSpeed(Human.MovementSpeed.stopped);
                }
            }

            while (movementAmount > 0f && distanceToNext > 0f)
            {
                //Don't know why this is happening, but for some reason we have a null destination node here...
                if (currentAction != null && currentAction.path != null && currentDestinationNode == null && pathCursor < currentAction.path.accessList.Count)
                {
                    //Game.LogError("The current destination node is null, pathcursor = " + pathCursor + " currentaction path access list: " + currentAction.path.accessList.Count);

                    NewNode nextLoc = currentAction.path.accessList[pathCursor].toNode;

                    //Set the new world space destination
                    SetDestinationNode(nextLoc);
                }

                distanceToNext = Vector3.Distance(transform.position, currentDestinationPositon);

                //Footsteps (simulated)
                if (!human.animationController.mainAnimator.enabled)
                {
                    footStepDistanceCounter += movementAmount;

                    if (footStepDistanceCounter >= CitizenControls.Instance.citizenFootstepDistance * AudioDebugging.Instance.citizenFootstepDistanceMultiplier)
                    {
                        footStepDistanceCounter -= CitizenControls.Instance.citizenFootstepDistance * AudioDebugging.Instance.citizenFootstepDistanceMultiplier;
                        //AudioController.Instance.PlayWorldFootstep(human.footstepEvent, human, rightFootNext); //For now only play if visible
                        rightFootNext = !rightFootNext; //Alternate feet
                        human.OnFootstep(rightFootNext);
                    }
                }

                //If distance is greater or equal to the amount to move, then move towards
                if (distanceToNext > movementAmount)
                {
                    //Create a rotational movement if visibile
                    if (human.visible && SessionData.Instance.currentTimeSpeed == SessionData.TimeSpeed.normal)
                    {
                        Quaternion movementRot = lastMovementRotation;
                        Vector3 relativeDirection = (currentDestinationPositon - transform.position).normalized;
                        //relativeDirection.y = 0;

                        //Avoid rotation error
                        if (relativeDirection != Vector3.zero)
                        {
                            movementRot = Quaternion.LookRotation(relativeDirection, Vector3.up);
                        }

                        //Facing
                        if (movementRot != lastMovementRotation)
                        {
                            float angleBetween = 0f;
                            float lerpValue = GetRotationalLerpValue(lastMovementRotation, movementRot, CitizenControls.Instance.citizenRotationalMovementSpeed + (human.currentMovementSpeed * 2), out angleBetween) * SessionData.Instance.currentTimeMultiplier * delta;

                            //Quick snap
                            if(Mathf.Abs(angleBetween) < 0.5f)
                            {
                                lastMovementRotation = movementRot;
                                transform.position = Vector3.MoveTowards(transform.position, currentDestinationPositon, movementAmount);
                            }
                            else
                            {
                                lastMovementRotation = Quaternion.Slerp(lastMovementRotation, movementRot, lerpValue);

                                Vector3 movement = lastMovementRotation * Vector3.forward * movementAmount;
                                movement = transform.position + movement;

                                //We can't end up further away from the target position otherwise we'll go around in circles!
                                if(Vector3.Distance(movement, currentDestinationPositon) < distanceToNext)
                                {
                                    //Grab the Y from direct translation to avoid falling through floors
                                    Vector3 direct = Vector3.MoveTowards(transform.position, currentDestinationPositon, movementAmount);
                                    movement.y = direct.y;

                                    transform.position = movement;
                                }
                                else
                                {
                                    transform.position = Vector3.MoveTowards(transform.position, currentDestinationPositon, movementAmount);
                                }
                            }
                        }
                        else
                        {
                            transform.position = Vector3.MoveTowards(transform.position, currentDestinationPositon, movementAmount);
                        }
                    }
                    else
                    {
                        transform.position = Vector3.MoveTowards(transform.position, currentDestinationPositon, movementAmount);
                    }

                    movementAmount = 0f;
                }
                //If not then place at coordinate, and subtract the movement amount
                else
                {
                    transform.position = currentDestinationPositon;
                    movementAmount -= distanceToNext;

                    //New coordinate is reached
                    if (doorCheck) ReachNewPathNode("Node Reached"); //Update of game location is contained within this
                    else if (!staticFromAnimation) SetStaticFromAnimation(true);

                    //The above may cause alterations to the routine...
                    if (currentAction == null)
                    {
                        human.SetDesiredSpeed(Human.MovementSpeed.stopped);
                        break;
                    }
                    else if (currentAction.path == null)
                    {
                        human.SetDesiredSpeed(Human.MovementSpeed.stopped);
                        break;
                    }
                    else if (currentAction.path.accessList == null)
                    {
                        human.SetDesiredSpeed(Human.MovementSpeed.stopped);
                        break;
                    }

                    //If we don't break, multiple new nodes were being triggered in this while loop...
                    break;
                }
            }

            //trigger trip up
            if(human.drunk > 0f)
            {
                if(human.visible && !isTripping && !human.isDead && !human.isStunned && !human.isAsleep)
                {
                    if(drunkTripCheckTimer <= 0f)
                    {
                        float tripChance = human.currentNormalizedSpeed * human.drunk * CitizenControls.Instance.drunkFallChance;

                        if(Toolbox.Instance.Rand(0f, 1f) < tripChance)
                        {
                            Game.Log("Tripped up with chance of " + tripChance);
                            human.speechController.TriggerBark(SpeechController.Bark.fallOffChair);
                            Trip();
                        }

                        drunkTripCheckTimer = 1f;
                    }
                    else
                    {
                        drunkTripCheckTimer -= delta;
                    }
                }
            }

            //Lose breath
            if(human.isRunning)
            {
                human.AddBreath(-0.1f * delta);
            }
        }
        else
        {
            //Set stopped
            human.SetDesiredSpeed(Human.MovementSpeed.stopped);

            //Face interacting with...
            if (human.interactingWith != null && human.interactingWith.parentTransform != null)
            {
                if(currentAction == null || !currentAction.isAtLocation || currentAction.preset.facePlayerWhileTalkingTo)
                {
                    //Set fact interaction
                    faceTransform = human.interactingWith.parentTransform;
                }
            }

            if (human.inConversation)
            {
                //Set face conversation initiator
                if (human.currentConversation.tree.stopMovement && (currentAction == null || !currentAction.isAtLocation))
                {
                    //Face person talking
                    if (human.currentConversation.currentlyTalking != null && human != human.currentConversation.currentlyTalking)
                    {
                        faceTransform = human.currentConversation.currentlyTalking.transform;
                    }
                    //Face person being talked to
                    else if (human.currentConversation.currentlyTalking == human && human.currentConversation.currentlyTalkingTo != null)
                    {
                        faceTransform = human.currentConversation.currentlyTalkingTo.transform;
                    }
                    //Face person who last talked
                    else if (human.currentConversation.previouslyTalking != null && human != human.currentConversation.previouslyTalking)
                    {
                        faceTransform = human.currentConversation.previouslyTalking.transform;
                    }
                    //Else random
                    else
                    {
                        if (human.currentConversation.participantA != null && human.currentConversation.participantA != human) faceTransform = human.currentConversation.participantA.transform;
                        else if (human.currentConversation.participantB != null && human.currentConversation.participantB != human) faceTransform = human.currentConversation.participantB.transform;
                        else if (human.currentConversation.participantC != null && human.currentConversation.participantC != human) faceTransform = human.currentConversation.participantC.transform;
                        else if (human.currentConversation.participantD != null && human.currentConversation.participantD != human) faceTransform = human.currentConversation.participantD.transform;
                    }
                }
            }

            //Check if I can get through yet...
            if(!doorCheck)
            {
                if(doorCheckProcessTimer <= 0)
                {
                    DoorCheckProcess();
                    AITick(); //Force an update here to enable knocking on door inserted action
                }
                else
                {
                    doorCheckProcessTimer--;
                }
            }

            //Barge door
            if (currentAction != null && currentAction.preset == RoutineControls.Instance.bargeDoor)
            {
                Game.Log("Gameplay: AI " + human.name + " is barging door...");

                bargeTimer += delta;

                if (bargeTimer >= 1.5f && currentAction.passedInteractable != null)
                {
                    NewDoor d = (currentAction.passedInteractable.objectRef as NewDoor);

                    d.Barge(human);
                    bargeTimer = 0f;
                    //bargeComplete = true;

                    if (d.wall.currentDoorStrength <= 0f)
                    {
                        Game.Log("Gameplay: AI " + human.name + " barge door complete!");
                        bargeTimer = 0f;
                        currentAction.repeat = false; //Cancel repetition
                        currentAction.Complete();
                        //bargeComplete = false;
                    }
                }

                if (currentAction != null && currentAction.passedInteractable == null)
                {
                    bargeTimer = 0f;
                    currentAction.repeat = false; //Cancel repetition
                    currentAction.Complete();
                    //bargeComplete = false;
                }
            }
        }
    }

    //Get a lerp value that we can use on an angle transition
    private float GetRotationalLerpValue(Quaternion originalRotation, Quaternion targetRotation, float multiplier, out float angleBetween)
    {
        //The further from the correct angle, the higher the lerp value
        angleBetween = Quaternion.Angle(originalRotation, targetRotation);

        float angleLerp = angleBetween / 180f;

        //The closer to the target, the higher the lerp value
        float distLerp = 1f - Mathf.Clamp01(distanceToNext / 1.8f);

        //Apply additional multipliers
        return (angleLerp + distLerp) * 0.5f * multiplier;
    }

    //Handle facing
    private void FacingUpdate()
    {
        //Cannot do this if restrained
        if (restrained) return;

        //Get facing target
        if (faceTransform != null)
        {
            Vector3 offsetPos = faceTransform.position + faceTransformOffset;
            Vector3 relativePos = (offsetPos - transform.position).normalized;
            relativePos.y = 0;

            //Avoid rotation error
            if (relativePos != Vector3.zero)
            {
                facingQuat = Quaternion.LookRotation(relativePos, Vector3.up);
            }
        }

        if (human.visible)
        {
            //Facing
            if (facingQuat != transform.rotation)
            {
                facingActive = true; //Flag for detecting active rotation- used to help enable/disable the update loop

                float angleBetween = 0f;
                float lerpValue = GetRotationalLerpValue(transform.rotation, facingQuat, CitizenControls.Instance.citizenFaceSpeed, out angleBetween) * SessionData.Instance.currentTimeMultiplier * delta;

                //Quick snap
                if(Mathf.Abs(angleBetween) <= 0.33f)
                {
                    transform.rotation = facingQuat;
                }
                else
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, facingQuat, lerpValue);
                }
            }

            //Looking (Only bother with this at all if visible)
            Vector3 dirToTarget = human.transform.forward;

            if (human.interactingWith != null)
            {
                dirToTarget = lookAtTransform.position - human.interactingWith.wPos;
                if (dirToTarget != Vector3.zero) lookingQuatCurrent = Quaternion.LookRotation(dirToTarget, Vector3.up);

                //Fov check unless priority target
                if (currentTrackTarget == null || !currentTrackTarget.priorityTarget)
                {
                    float angleToOther = Vector3.Angle(dirToTarget, transform.forward); //Local angle

                    if (Mathf.Abs(angleToOther) >= GameplayControls.Instance.citizenFOV * 0.6f)
                    {
                        //Update tracked targets
                        SetTrackTarget(null);
                        lookingQuatCurrent = Quaternion.LookRotation(human.transform.forward, Vector3.up);
                        UpdateTrackedTargets();
                    }
                }
            }
            else
            {
                if (lookAtTransform != null)
                {
                    dirToTarget = lookAtTransform.position - human.lookAtThisTransform.position;
                    if (dirToTarget != Vector3.zero) lookingQuatCurrent = Quaternion.LookRotation(dirToTarget, Vector3.up);

                    //Fov check unless priority target
                    if (currentTrackTarget == null || !currentTrackTarget.priorityTarget)
                    {
                        float angleToOther = Vector3.Angle(dirToTarget, transform.forward); //Local angle

                        if (Mathf.Abs(angleToOther) >= GameplayControls.Instance.citizenFOV * 0.6f)
                        {
                            //human.SelectedDebug("Angle is greater than FoV, update tracked targets...");

                            //Update tracked targets
                            SetTrackTarget(null);
                            lookingQuatCurrent = Quaternion.LookRotation(human.transform.forward, Vector3.up);
                            UpdateTrackedTargets();
                        }
                    }
                }
                //Look around randomly
                else if (currentAction != null && currentAction.preset.lookAround)
                {
                    lookAroundTimer += Time.deltaTime * Toolbox.Instance.Rand(0.75f, 1.25f);

                    if (lookAroundTimer > 1.6f)
                    {
                        //Change target
                        //Pick a random poition infront
                        lookAroundPosition = new Vector3(Toolbox.Instance.Rand(-1.2f, 1.2f), Toolbox.Instance.Rand(1.45f, 1.6f), 1.5f);
                        lookAroundTimer = 0f;
                    }

                    dirToTarget = human.transform.TransformPoint(lookAroundPosition) - human.lookAtThisTransform.position;
                    lookingQuatCurrent = Quaternion.LookRotation(dirToTarget, Vector3.up);
                }
                //If none of the above are true, default to the animation's look angle...
                else
                {
                    lookingQuatCurrent = human.neckTransform.rotation;
                }
            }

            //Always lerp towards position
            float lookSpeed = delta * Mathf.Lerp(CitizenControls.Instance.citizenLookAtSpeed.x, CitizenControls.Instance.citizenLookAtSpeed.y, lookAtTransformRank);
            human.neckTransform.rotation = Quaternion.Slerp(lookingQuatLastFrame, lookingQuatCurrent, lookSpeed);

            //Clamp angles
            ClampNeckRotation();

            lookingQuatLastFrame = human.neckTransform.rotation; //Update with this value so we can continue lerping next frame

            //Move eyes to look
            Vector3 eyesLookAt = dirToTarget / 100f;

            //Clamp movement
            eyesLookAt = human.outfitController.pupilParentOffset + new Vector3(Mathf.Clamp(eyesLookAt.x, -0.0135f, 0.0135f), Mathf.Clamp(eyesLookAt.y, -0.008f, 0.008f), 0f);
            if (human.outfitController.pupilParent != null) human.outfitController.pupilParent.localPosition = eyesLookAt;
        }
        else
        {
            if (facingQuat != transform.rotation)
            {
                transform.rotation = facingQuat;
            }
        }
    }

    //Handle attacks
    private void AttackUpdate()
    {
        if (attackActive && attackTarget != null)
        {
            attackActiveLength += delta;

            human.animationController.umbreallLayerDesiredWeight = 0f; //Cancel umbrella weight

            SetFacingPosition(attackTarget.transform.position); //Alaways face whoever I'm attacking

            float dist = Vector3.Distance(attackTarget.transform.position, transform.position);

            //Range check
            if (!throwActive && (dist > weaponRangeMax || dist < currentWeaponPreset.minimumRange))
            {
                Game.Log(human.name + " Abort attack: Out of range " + dist + " (" + weaponRangeMax + "/" + currentWeaponPreset.minimumRange + ")");
                OnAbortAttack(); //Abort if out of range
            }
            else
            {
                //Throw object
                if(throwActive)
                {
                    //Account for transition intro time
                    attackProgress = Mathf.Clamp01(attackActiveLength / 1.25f);

                    //Set weight to arms
                    human.animationController.mainAnimator.SetLayerWeight(1, 1);
                    if (Game.Instance.collectDebugData) human.SelectedDebug("Set current arms layer weight: 1 (" + human.animationController.armsLayerDesiredWeight + ")", Actor.HumanDebug.misc);

                    //Trigger attack
                    if (!damageColliderCreated)
                    {
                        if (attackProgress >= 0.33f)
                        {
                            //Game.Log("THROW EXECUTE");

                            if (throwItem != null)
                            {
                                //Game.Log("THROW EXECUTE: " + human.name + ", " + throwItem.name);

                                if (human.currentConsumables.Count > 0)
                                {
                                    human.currentConsumables.RemoveAt(0);
                                }
                                else if (human.trash.Count > 0)
                                {
                                    human.trash.RemoveAt(0);
                                }

                                Transform spawnAt = human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.HandRight);

                                Interactable thrownObject = InteractableCreator.Instance.CreateWorldInteractable(throwItem, human, human, null, spawnAt.transform.position, Vector2.zero, null, null);

                                float throwForce = 30;
                                if (human.descriptors.build == Descriptors.BuildType.skinny) throwForce = 20;
                                else if (human.descriptors.build == Descriptors.BuildType.overweight) throwForce = 40;
                                else if (human.descriptors.build == Descriptors.BuildType.muscular) throwForce = 60;

                                thrownObject.ForcePhysicsActive(false, true, (transform.forward * throwForce));

                                throwItem = null;

                                DespawnRightItem();
                                UpdateHeldItems(AIActionPreset.ActionStateFlag.none);
                            }

                            damageColliderCreated = true;
                        }
                    }

                    //End attack
                    if (attackProgress >= 1f)
                    {
                        attackActive = false;
                        attackProgress = 0f;
                        damageColliderCreated = false;
                        ejectBrassCreated = false;
                        throwActive = false;
                        throwDelay = Toolbox.Instance.Rand(4f, 7f);

                        OnAttackComplete();
                    }
                }
                else
                {
                    //Punch
                    if (combatMode == 0)
                    {
                        //Atack is governed by animation state: Only start an attack if arms are in the combat pose animation
                        AnimatorStateInfo anim = human.animationController.mainAnimator.GetCurrentAnimatorStateInfo(1);

                        //Use animation state to time attacks
                        if (anim.IsTag("Punch") || anim.IsTag("Kick"))
                        {
                            //Set weight to arms
                            human.animationController.mainAnimator.SetLayerWeight(1, 1);

                            //Account for transition intro time 
                            //attackProgress = Mathf.Clamp01(anim.normalizedTime * 1.1f - 0.1f);
                            attackProgress = anim.normalizedTime % 1;

                            //Start attack object
                            //To spawn this object, target must be the player and the player must be facing them
                            if (attackTarget.isPlayer && FirstPersonItemController.Instance.currentItem != null && FirstPersonItemController.Instance.currentItem.name.ToLower() == "fists")
                            {
                                if (activeAttackBar == null)
                                {
                                    GameObject newAttack = Instantiate(PrefabControls.Instance.attackBar, InterfaceControls.Instance.speechBubbleParent);
                                    activeAttackBar = newAttack.GetComponent<AttackBarController>();
                                    activeAttackBar.Setup(this);
                                }
                            }

                            if (attackProgress >= currentWeaponPreset.attackTriggerPoint && !damageColliderCreated)
                            {
                                GameObject newAttack = null;

                                if(anim.IsTag("Kick"))
                                {
                                    newAttack = Instantiate(PrefabControls.Instance.damageCollider, human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.LowerLegRight));
                                }
                                else
                                {
                                    newAttack = Instantiate(PrefabControls.Instance.damageCollider, human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.LowerArmRight));
                                }

                                damageCollider = newAttack.GetComponent<DamageColliderController>();
                                damageCollider.Setup(human, attackTarget, weaponDamage, GetCurrentKillTarget(), currentWeaponPreset);
                                if (activeAttackBar != null) activeAttackBar.removeHit = true; //Play remove by hit animation

                                //Play punch sfx
                                if (currentWeaponPreset.fireEvent != null) AudioController.Instance.PlayWorldOneShot(currentWeaponPreset.fireEvent, human, human.currentNode, damageCollider.transform.position);

                                damageColliderCreated = true;
                            }
                            else if (attackProgress > currentWeaponPreset.attackRemovePoint && damageColliderCreated)
                            {
                                OnAttackComplete();

                                if (damageCollider != null)
                                {
                                    Destroy(damageCollider.gameObject);
                                }
                            }
                        }
                        //End attack
                        else if (damageColliderCreated && attackActiveLength > weaponRefire)
                        {
                            attackActive = false;
                            throwActive = false;
                            attackProgress = 0f;
                            damageColliderCreated = false;
                            ejectBrassCreated = false;

                            if (damageCollider != null)
                            {
                                Destroy(damageCollider.gameObject);
                            }

                            OnAttackComplete();
                        }
                    }
                    //Pistol
                    else if (combatMode >= 1 && combatMode <= 3)
                    {
                        //Account for transition intro time
                        attackProgress = Mathf.Clamp01(attackActiveLength / weaponRefire);

                        //Set weight to arms
                        human.animationController.mainAnimator.SetLayerWeight(1, 1);

                        //Trigger attack
                        if (!damageColliderCreated)
                        {
                            if (attackProgress >= currentWeaponPreset.attackTriggerPoint && spawnedRightItem != null)
                            {
                                Vector3 muzzlePoint = spawnedRightItem.transform.TransformPoint(currentWeaponPreset.muzzleOffset);

                                Vector3 brassEject = Vector3.zero;
                                bool ejectB = false;

                                if (currentWeaponPreset.ejectBrassSetting == MurderWeaponPreset.EjectBrass.onFire)
                                {
                                    brassEject = spawnedRightItem.transform.TransformPoint(currentWeaponPreset.brassEjectOffset);
                                    ejectB = true;
                                }
                                else if(currentWeaponPreset.ejectBrassSetting == MurderWeaponPreset.EjectBrass.revolver)
                                {
                                    revolverShots++;
                                }

                                for (int i = 0; i < currentWeaponPreset.shots; i++)
                                {
                                    Vector3 aimPoint = attackTarget.transform.position;
                                    if (attackTarget.aimTransform != null) aimPoint = attackTarget.aimTransform.position;
                                    else
                                    {
                                        if (attackTarget.lookAtThisTransform != null) aimPoint = attackTarget.lookAtThisTransform.position;
                                        else if (attackTarget.isPlayer) aimPoint = CameraController.Instance.cam.transform.position;
                                    }

                                    Toolbox.Instance.Shoot(human, muzzlePoint, aimPoint, weaponRangeMax, weaponAccuracy, weaponDamage, currentWeaponPreset, ejectB, brassEject, false);
                                    ejectB = false;
                                }

                                damageColliderCreated = true;
                            }
                        }

                        //Eject brass shotgun
                        if(!ejectBrassCreated && currentWeaponPreset.ejectBrassSetting == MurderWeaponPreset.EjectBrass.onPumpAction && currentWeaponPreset.shellCasing != null)
                        {
                            if (attackProgress >= currentWeaponPreset.attackTriggerPoint + 0.1f && currentWeaponPreset != null && spawnedRightItem != null)
                            {
                                Vector3 brassEject = spawnedRightItem.transform.TransformPoint(currentWeaponPreset.brassEjectOffset);

                                Interactable newCasing = InteractableCreator.Instance.CreateWorldInteractable(currentWeaponPreset.shellCasing, human, human, null, brassEject, Vector3.zero, null, null);

                                //The coin should be spawned upon creation...
                                //Set physics to simulate throw...
                                if (newCasing != null)
                                {
                                    newCasing.MarkAsTrash(true); //Remove this once we can

                                    Vector3 force = this.transform.right * Toolbox.Instance.Rand(3.5f, 4.5f) + new Vector3(Toolbox.Instance.Rand(-0.1f, 0.1f), Toolbox.Instance.Rand(-0.1f, 0.1f), Toolbox.Instance.Rand(-0.1f, 0.1f));

                                    if (newCasing.controller != null)
                                    {
                                        newCasing.controller.DropThis(false); //Activate physics

                                        //Apply sideways force to ejected shell
                                        newCasing.controller.rb.AddForce(force, ForceMode.VelocityChange);
                                    }
                                    else
                                    {
                                        newCasing.ForcePhysicsActive(false, true, force, ForceMode.VelocityChange);
                                    }
                                }

                                ejectBrassCreated = true;
                            }
                        }

                        //End attack
                        if (attackProgress >= 1f)
                        {
                            //Eject brass revolver
                            if (!ejectBrassCreated && currentWeaponPreset.ejectBrassSetting == MurderWeaponPreset.EjectBrass.revolver && currentWeaponPreset.shellCasing != null)
                            {
                                if (revolverShots >= 7 && spawnedRightItem != null && currentWeaponPreset != null)
                                {
                                    Vector3 brassEject = spawnedRightItem.transform.TransformPoint(currentWeaponPreset.brassEjectOffset);

                                    for (int i = 0; i < 7; i++)
                                    {
                                        Interactable newCasing = InteractableCreator.Instance.CreateWorldInteractable(currentWeaponPreset.shellCasing, human, human, null, brassEject, Vector3.zero, null, null);

                                        //The coin should be spawned upon creation...
                                        //Set physics to simulate throw...
                                        if (newCasing != null)
                                        {
                                            newCasing.MarkAsTrash(true); //Remove this once we can

                                            Vector3 force = this.transform.right * Toolbox.Instance.Rand(3.5f, 4.5f) + new Vector3(Toolbox.Instance.Rand(-0.1f, 0.1f), Toolbox.Instance.Rand(-0.1f, 0.1f), Toolbox.Instance.Rand(-0.1f, 0.1f));

                                            if (newCasing.controller != null)
                                            {
                                                newCasing.controller.DropThis(false); //Activate physics

                                                //Apply sideways force to ejected shell
                                                newCasing.controller.rb.AddForce(force, ForceMode.VelocityChange);
                                            }
                                            else
                                            {
                                                newCasing.ForcePhysicsActive(false, true, force, ForceMode.VelocityChange);
                                            }
                                        }
                                    }

                                    ejectBrassCreated = true;
                                    revolverShots = 0;
                                }
                            }

                            attackActive = false;
                            throwActive = false;
                            attackProgress = 0f;
                            damageColliderCreated = false;
                            ejectBrassCreated = false;

                            if (damageCollider != null)
                            {
                                Destroy(damageCollider.gameObject);
                            }

                            OnAttackComplete();
                        }
                    }
                    //Sword
                    else if(combatMode == 4)
                    {
                        //Set weight to arms
                        human.animationController.mainAnimator.SetLayerWeight(1, 1);

                        //Similar to punch but not blockable
                        //Atack is governed by animation state: Only start an attack if arms are in the combat pose animation
                        AnimatorStateInfo anim = human.animationController.mainAnimator.GetCurrentAnimatorStateInfo(1);

                        if (anim.IsTag("Swing"))
                        {
                            //The integer part is the number of time a state has been looped. The fractional part is the % (0-1) of progress in the current loop.
                            attackProgress = anim.normalizedTime % 1;

                            Game.Log("Attack progress: " + attackProgress);

                            if (attackProgress >= currentWeaponPreset.attackTriggerPoint && !damageColliderCreated)
                            {
                                GameObject newAttack = Instantiate(PrefabControls.Instance.damageCollider, human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.LowerArmRight));
                                damageCollider = newAttack.GetComponent<DamageColliderController>();
                                damageCollider.Setup(human, attackTarget, weaponDamage, GetCurrentKillTarget(), currentWeaponPreset);
                                //if (activeAttackBar != null) activeAttackBar.removeHit = true; //Play remove by hit animation

                                //Play punch sfx
                                if (currentWeaponPreset.fireEvent != null) AudioController.Instance.PlayWorldOneShot(currentWeaponPreset.fireEvent, human, human.currentNode, damageCollider.transform.position);

                                damageColliderCreated = true;
                            }
                            else if (attackProgress > currentWeaponPreset.attackRemovePoint && damageColliderCreated)
                            {
                                OnAttackComplete();

                                if (damageCollider != null)
                                {
                                    Destroy(damageCollider.gameObject);
                                }
                            }
                        }
                        //End attack
                        else if (damageColliderCreated && attackActiveLength > weaponRefire)
                        {
                            attackActive = false;
                            throwActive = false;
                            attackProgress = 0f;
                            damageColliderCreated = false;
                            ejectBrassCreated = false;

                            if (damageCollider != null)
                            {
                                Destroy(damageCollider.gameObject);
                            }

                            OnAttackComplete();
                        }
                    }
                }
            }
        }
        else if (attackDelay > 0f)
        {
            attackDelay -= delta;
            attackDelay = Mathf.Max(attackDelay, 0f);
        }

        if(throwDelay > 0f)
        {
            throwDelay -= delta;
            throwDelay = Mathf.Max(throwDelay, 0f);
        }

        //Timeout attack active
        if (attackActive)
        {
            if (attackTimeout < 3f)
            {
                attackTimeout += delta;
            }
            else EndAttack();
        }
    }

    //Return a kill target if active
    private Human GetCurrentKillTarget()
    {
        if (killerForMurders.Count > 0)
        {
            foreach(MurderController.Murder m in killerForMurders)
            {
                if(m.state == MurderController.MurderState.executing)
                {
                    return m.victim;
                }
            }
        }

        return null;
    }

    //Handle KO State
    private void KOUpdate()
    {
        //Ragdoll transition
        if (ko)
        {
            if (SessionData.Instance.gameTime < koTime || human.isDead)
            {
                //Wait for KO time
                //Game.Log(human.name + " KO: " + SessionData.Instance.gameTime + " < " + koTime);

                //Set centre of temporary mesh
                if (human.animationController.newBoxCollider != null)
                {
                    human.animationController.newBoxCollider.center = human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.lowerTorso).localPosition;
                }

                //If citizen is dead, we want to monitor velocity of core parts and then deactivate when all is settled...
                if (human.isDead)
                {
                    if (deadRagdollTimer < 6f)
                    {
                        //Contain victim within location
                        Vector3Int nodePos = CityData.Instance.RealPosToNodeInt(transform.position);

                        NewNode vNode = null;

                        if (!PathFinder.Instance.nodeMap.TryGetValue(nodePos, out vNode))
                        {
                            vNode = Toolbox.Instance.FindClosestValidNodeToWorldPosition(transform.position);
                        }

                        //Real pos is out of location!
                        if (vNode.gameLocation != human.death.GetDeathLocation())
                        {
                            Game.LogError("Murder: Victim ragdoll has moved outside of location range! Limiting...");
                            NewNode closestNode = null;
                            float closestDist = Mathf.Infinity;

                            NewGameLocation deathLoc = human.death.GetDeathLocation();

                            if(deathLoc != null)
                            {
                                foreach (NewNode n in deathLoc.nodes)
                                {
                                    float dist = Vector3.Distance(n.nodeCoord, vNode.nodeCoord);

                                    if (dist < closestDist)
                                    {
                                        closestNode = n;
                                        closestDist = dist;
                                    }
                                }
                            }

                            //Set position...
                            if (closestNode != null)
                            {
                                transform.position = closestNode.position + new Vector3(0, 1, 0);
                            }
                        }

                        if (human.animationController.upperTorsoRB != null && (Mathf.Abs(human.animationController.upperTorsoRB.velocity.x) + Mathf.Abs(human.animationController.upperTorsoRB.velocity.y) + Mathf.Abs(human.animationController.upperTorsoRB.velocity.z) < 0.1f))
                        {
                            deadRagdollTimer += delta;
                        }
                        else deadRagdollTimer += delta * 0.5f;
                        //else deadRagdollTimer = 0f;
                    }
                    else
                    {
                        human.animationController.SetRagdoll(false, true);
                        ko = false;
                    }
                }
                //If restrained, awaken
                else if (restrained)
                {
                    koTime = SessionData.Instance.gameTime;
                    Game.Log(human.name + " Set KO time: " + koTime);
                }
            }
            else if (koTransitionTimer > 0f)
            {
                //Cancel ragdoll: This will not however, re-enable animators
                if (isRagdoll)
                {
                    //Apply the ragdoll's movement to the actor
                    Vector3 amountMoved = human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.lowerTorso).position - this.transform.position;

                    //The new position is this + the amount moved
                    Vector3 newPosition = this.transform.position + amountMoved;

                    //Snap the Y position to nearest node
                    Vector3Int snappedNodePos = CityData.Instance.RealPosToNodeInt(newPosition);

                    NewNode foundNode = null;
                    Vector3 desiredPostion = newPosition;

                    //If we can find a node at this coord, set the Y pos to it
                    if (PathFinder.Instance.nodeMap.TryGetValue(snappedNodePos, out foundNode))
                    {
                        desiredPostion.y = foundNode.position.y;
                    }
                    //Otherwise find nearest valid node
                    else
                    {
                        foundNode = Toolbox.Instance.FindClosestValidNodeToWorldPosition(newPosition, true, false);
                        if (foundNode != null) desiredPostion = foundNode.position;
                    }

                    Vector3 positionDifference = desiredPostion - this.transform.position;

                    //Set the new position
                    this.transform.position = desiredPostion;

                    //Reset the local position
                    Transform lowerTorso = human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.lowerTorso);

                    lowerTorso.localPosition = new Vector3(0f, lowerTorso.transform.localPosition.y + positionDifference.y, 0f);
                    lowerTorso.SetParent(null, true); //Quick hacky way of keeping ths position is to swap parent

                    //Find the difference between current and the hips
                    Transform headTransform = human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.Head);
                    Vector3 headDiff = this.transform.position - headTransform.position;
                    this.transform.LookAt(headTransform.position + (headDiff * 3f));
                    this.transform.rotation = Quaternion.Euler(0, this.transform.rotation.eulerAngles.y, 0);

                    lowerTorso.transform.SetParent(human.modelParent.transform, true); //Quick hacky way of keeping ths position is to swap parent

                    human.animationController.SetRagdoll(false); //Cancel ragdoll (need to do this after above as this takes a limb snapshot)
                }

                //Use this to transition ragdoll state
                koTransitionTimer -= delta;

                //Normalized
                float transitionNormal = 1f - (koTransitionTimer / CitizenControls.Instance.ragdollTransitionTime);

                //Transition limbs
                if (CitizenControls.Instance.getUpManualAnimation.Count > 0)
                {
                    foreach (CitizenAnimationController.RagdollSnapshot rag in human.animationController.ragdollSnapshot)
                    {
                        CitizenControls.ManualAnimation animFrom = CitizenControls.Instance.getUpManualAnimation[0];

                        for (int i = 0; i < animFrom.limbData.Count; i++)
                        {
                            CitizenControls.LimbPos toPos = animFrom.limbData[i];

                            if (toPos.anchor == rag.anchorConfig.anchor)
                            {
                                Transform part = null;

                                if (human.outfitController.anchorReference.TryGetValue(toPos.anchor, out part))
                                {
                                    part.localPosition = Vector3.Lerp(rag.localPos, toPos.localPosition, transitionNormal);
                                    part.localRotation = Quaternion.Slerp(rag.localRot, toPos.localRotation, transitionNormal);
                                }
                            }
                        }
                    }
                }
            }
            else if (getUpDelayTimer > 0f)
            {
                //Manual animation
                float normalizedGetUp = 1f - Mathf.InverseLerp(0f, CitizenControls.Instance.getUpTimer, getUpDelayTimer);

                //Transition limbs
                CitizenControls.ManualAnimation animFrom = null;
                CitizenControls.ManualAnimation animTo = null;

                for (int i = 0; i < CitizenControls.Instance.getUpManualAnimation.Count - 1; i++)
                {
                    CitizenControls.ManualAnimation scan = CitizenControls.Instance.getUpManualAnimation[i];

                    if (scan.timeline <= normalizedGetUp || animFrom == null)
                    {
                        animFrom = CitizenControls.Instance.getUpManualAnimation[i];
                        animTo = CitizenControls.Instance.getUpManualAnimation[i + 1];
                    }
                }

                //Create normalized from/to
                float normalizedFromTo = Mathf.InverseLerp(animFrom.timeline, animTo.timeline, normalizedGetUp);

                for (int i = 0; i < animFrom.limbData.Count; i++)
                {
                    CitizenControls.LimbPos fromPos = animFrom.limbData[i];
                    CitizenControls.LimbPos toPos = animTo.limbData[i];

                    Transform part = null;

                    if (human.outfitController.anchorReference.TryGetValue(fromPos.anchor, out part))
                    {
                        part.localPosition = Vector3.Lerp(fromPos.localPosition, toPos.localPosition, normalizedFromTo);
                        part.localRotation = Quaternion.Slerp(fromPos.localRotation, toPos.localRotation, normalizedFromTo);
                    }
                }

                //Game.Log("Manual get up animation progress: " + normalizedGetUp + " K/F: " + normalizedFromTo);

                //human.animationController.mainAnimator.SetLayerWeight(0, 0); //No arm weight here

                ////Only re-enable this once...
                //if (human.animationController.paused)
                //{
                //    //In this phase, animation becomes active but we still have to wait for the get up animation to finish before the stun is cancelled
                //    //Enable character collider
                //    human.interactableController.coll.enabled = true;

                //    //Enable animators
                //    human.animationController.SetPauseAnimation(false);

                //    //Play the transition from animation
                //    human.animationController.mainAnimator.SetTrigger("ragdollTransitionFrom");
                //}

                getUpDelayTimer -= delta;
            }
            else
            {
                //Only re-enable this once...
                if (human.animationController.paused)
                {
                    //In this phase, animation becomes active but we still have to wait for the get up animation to finish before the stun is cancelled
                    //Enable character collider
                    human.interactableController.coll.enabled = true;

                    //Enable animators
                    human.animationController.SetPauseAnimation(false);

                    human.animationController.mainAnimator.Rebind();

                    //Play the transition from animation
                    //human.animationController.mainAnimator.SetTrigger("ragdollTransitionFrom");

                    if (restrained)
                    {
                        human.animationController.SetRestrained(true);
                        human.animationController.mainAnimator.SetTrigger("restrain");
                        human.animationController.SetArmsBoolState(CitizenAnimationController.ArmsBoolSate.armsCuffed);
                    }
                }

                SetKO(false);
            }
        }
    }

    //Set this to enabled/disabled
    public void SetUpdateEnabled(bool val)
    {
        if(this.enabled != val)
        {
            if(human != null && Game.Instance.collectDebugData) human.SelectedDebug("Set Update Enabled: " + val, Actor.HumanDebug.updates);
            this.enabled = val;
        }
    }

    //Clamp neck transform
    public void ClampNeckRotation()
    {
        //Clamp
        Vector3 lEuler = human.neckTransform.localEulerAngles;

        human.neckTransform.localEulerAngles = new Vector3(
            Toolbox.Instance.ClampAngle(lEuler.x, CitizenControls.Instance.upExtent, CitizenControls.Instance.downExtent),
            Toolbox.Instance.ClampAngle(lEuler.y, CitizenControls.Instance.leftExtent, CitizenControls.Instance.rightExtent),
            0
            );
    }

    //When this has reached a new coordinate on its path. This is different from the regular location update and will trigger only on the exact node coordinate.
    public void ReachNewPathNode(string debug, bool scanForNextNodeFurniture = true)
    {
        human.UpdateGameLocation();
        human.SetDesiredSpeed(Human.MovementSpeed.stopped); //Set stopped

        //Remove self from door interactions...
        if (doorInteractions.Count > 0)
        {
            foreach (NewDoor d in doorInteractions)
            {
                d.usingDoorList.Remove(human as Actor);
            }

            doorInteractions.Clear();
        }

        AIActionPreset.DoorRule doorRule = AIActionPreset.DoorRule.normal;

        if (currentAction != null)
        {
            doorRule = currentAction.goal.preset.doorRule;
            if (currentAction.preset.overrideGoalDoorRule) doorRule = currentAction.preset.doorRule;
        }

        //Close door
        if (openedDoor != null)
        {
            //Checks: Door is supposed to stay closed, ai can close doors, it's not already closed, it's not closing, and the user list is 0.
            if(openedDoor.doorSetting == NewDoor.DoorSetting.leaveClosed && !dontEverCloseDoors && !openedDoor.isClosed && !openedDoor.isClosing && openedDoor.usingDoorList.Count <= 0)
            {
                bool actionPass = true;

                if(human.ai.currentAction != null)
                {
                    if (doorRule == AIActionPreset.DoorRule.dontClose) actionPass = false;
                    else if(doorRule == AIActionPreset.DoorRule.onlyCloseToLocation || doorRule == AIActionPreset.DoorRule.onlyLockToLocation)
                    {
                        if(openedDoor.wall.node.gameLocation == openedDoor.wall.otherWall.node.gameLocation)
                        {
                            actionPass = false;
                        }
                    }
                }

                //Check if the player is talking to somebody through this door, and if not don't close it.
                if(InteractionController.Instance.talkingTo != null)
                {
                    if(InteractionController.Instance.talkingTo.isActor != null)
                    {
                        if(InteractionController.Instance.talkingTo.isActor.currentNode == openedDoor.wall.node || InteractionController.Instance.talkingTo.isActor.currentNode == openedDoor.wall.otherWall.node)
                        {
                            actionPass = false;
                        }
                    }
                }

                if(actionPass) openedDoor.SetOpen(0f, human);

                //Do I need to lock this door?
                //Force unlock action
                bool keepLocked = false;

                if (openedDoor.lockSetting == NewDoor.LockSetting.keepLocked)
                {
                    keepLocked = true;

                    //Depending on action door rules, maybe don't lock behind
                    if (doorRule == AIActionPreset.DoorRule.dontClose || doorRule == AIActionPreset.DoorRule.dontLock || doorRule == AIActionPreset.DoorRule.onlyCloseToLocation)
                    {
                        keepLocked = false;
                    }
                    else if (doorRule == AIActionPreset.DoorRule.onlyLockToLocation)
                    {
                        if (openedDoor.wall.node.gameLocation == openedDoor.wall.otherWall.node.gameLocation)
                        {
                            keepLocked = false;
                        }
                    }

                    if(openedDoor.wall.node.gameLocation.isCrimeScene || openedDoor.wall.node.gameLocation.currentOccupants.Exists(item => item.isEnforcer && item.isOnDuty))
                    {
                        keepLocked = false;
                    }
                    else if (openedDoor.wall.otherWall.node.gameLocation.isCrimeScene || openedDoor.wall.otherWall.node.gameLocation.currentOccupants.Exists(item => item.isEnforcer && item.isOnDuty))
                    {
                        keepLocked = false;
                    }
                }

                //Check if the player is talking to somebody through this door, and if not don't close it.
                if (InteractionController.Instance.talkingTo != null)
                {
                    if (InteractionController.Instance.talkingTo.isActor != null)
                    {
                        if (InteractionController.Instance.talkingTo.isActor.currentNode == openedDoor.wall.node || InteractionController.Instance.talkingTo.isActor.currentNode == openedDoor.wall.otherWall.node)
                        {
                            keepLocked = false;
                        }
                    }
                }

                if (currentGoal != null && keepLocked) currentGoal.InsertLockAction(openedDoor);
            }
            
            openedDoor = null;
        }

        //Advance path cursor for next location
        pathCursor++;

        if(currentAction != null && currentAction.path != null)
        {
            if (Game.Instance.collectDebugData)
            {
                if (currentAction.path.accessList.Count > pathCursor)
                {
                    DebugDestinationPosition("ReachNewPathNode: " + pathCursor + "/" + currentAction.path.accessList.Count + " world pos: " + currentAction.path.accessList[pathCursor].worldAccessPoint + ", actor pos: " + human.transform.position);
                    human.SelectedDebug("ReachNewPathNode: " + pathCursor + "/" + currentAction.path.accessList.Count + " world pos: " + currentAction.path.accessList[pathCursor].worldAccessPoint + ", actor pos: " + human.transform.position, Actor.HumanDebug.movement);
                }
                else
                {
                    DebugDestinationPosition("ReachNewPathNode: " + pathCursor + "/" + currentAction.path.accessList.Count + ", actor pos: " + human.transform.position);
                    human.SelectedDebug("ReachNewPathNode: " + pathCursor + "/" + currentAction.path.accessList.Count + ", actor pos: " + human.transform.position, Actor.HumanDebug.movement);
                }
            }

            //End of route (completed)
            if (pathCursor >= currentAction.path.accessList.Count)
            {
                currentAction.DestinationCheck();

                ////Is this at a destination?
                //if (!currentAction.isAtLocation && human.transform.position == currentDestinationPositon)
                //{
                //    DebugDestinationPosition("Actor has reached current destination position: " + currentDestinationPositon + " path: " + pathCursor + "/" + currentAction.path.accessList.Count);
                //    currentAction.SetAtDestination(true);
                //}
                //else if (currentAction.isAtLocation && human.transform.position != currentDestinationPositon)
                //{
                //    currentAction.SetAtDestination(false);
                //}
            }
            else
            {
                //The next location...
                doorCheck = true;
                if (Game.Instance.collectDebugData) human.SelectedDebug("Reset door check", Actor.HumanDebug.movement);

                //Check door
                if (currentAction.path.accessList[pathCursor].door == null)
                {
                    if (Game.Instance.collectDebugData) human.SelectedDebug("No door detected at path cursor " + pathCursor + " position " + currentAction.path.accessList[pathCursor].worldAccessPoint, Actor.HumanDebug.movement);
                }

                if (currentAction.path.accessList[pathCursor].door != null)
                {
                    DoorCheckProcess();
                }
                //Check security door
                else if(human.currentBuilding != null)
                {
                    if(human.currentNode.floor != null && human.currentNode.floor.alarmLockdown && pathCursor <= currentAction.path.accessList.Count - 1)
                    {
                        foreach(Interactable i in human.currentNode.floor.securityDoors)
                        {
                            //Is closed
                            if(!i.sw0)
                            {
                                //Security door on next node...
                                if (i.furnitureParent.coversNodes.Contains(currentAction.path.accessList[pathCursor].toNode))
                                {
                                    //Set stopped
                                    human.SetDesiredSpeed(Human.MovementSpeed.stopped);
                                    SetStaticFromAnimation(true);

                                    //Enforcers have authority to open doors
                                    if(human.isEnforcer && human.isOnDuty)
                                    {
                                        human.currentNode.floor.SetAlarmLockdown(false);
                                    }
                                    else
                                    {
                                        currentGoal.OnDeactivate(currentGoal.preset.repeatDelayOnBusy);
                                        return;
                                    }

                                    doorCheck = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                //Check use point busy
                else if(currentAction.usagePoint != null)
                {
                    if (!currentAction.InteractableUsePointCheck())
                    {
                        //Set stopped
                        human.SetDesiredSpeed(Human.MovementSpeed.stopped);
                        SetStaticFromAnimation(true);

                        //Recursion can happen here, this should stop it
                        if (!usePointBusyRecursion)
                        {
                            usePointBusyRecursion = true;
                            currentAction.OnUsePointBusy();
                            usePointBusyRecursion = false;
                        }
                        else
                        {
                            currentGoal.OnDeactivate(currentGoal.preset.repeatDelayOnBusy);
                        }
                    }
                }

                if (Game.Instance.collectDebugData) human.SelectedDebug("Door check: " + doorCheck, Actor.HumanDebug.movement);

                //Set destination
                if(currentAction != null && currentAction.path != null && currentAction.path.accessList != null && doorCheck)
                {
                    NewNode nextLoc = null;

                    if(pathCursor <= currentAction.path.accessList.Count - 1)
                    {
                        nextLoc = currentAction.path.accessList[pathCursor].toNode;
                    }

                    //Set the new world space destination
                    SetDestinationNode(nextLoc, scanForNextNodeFurniture);
                }
            }
        }

        usePointBusyRecursion = false;
    }

    //Update whether there is a door here that we may not be able to access
    public void DoorCheckProcess()
    {
        if(currentAction != null && currentAction.path != null && pathCursor > -1 && pathCursor < currentAction.path.accessList.Count && currentAction.path.accessList[pathCursor].door != null)
        {
            NewDoor.CitizenPassResult doorReason;
            NewDoor doorCheckDoor = currentAction.path.accessList[pathCursor].door;

            doorCheck = doorCheckDoor.CitizenPassCheck(human, out doorReason);
            if (Game.Instance.collectDebugData) human.SelectedDebug("Door check " + currentAction.path.accessList[pathCursor].door.name + " result: " + doorCheck + " reason: " + doorReason + " actor position" + this.transform.position, Actor.HumanDebug.movement);

            AIActionPreset.DoorRule doorRule = AIActionPreset.DoorRule.normal;

            if (currentAction != null)
            {
                doorRule = currentAction.goal.preset.doorRule;
                if (currentAction.preset.overrideGoalDoorRule) doorRule = currentAction.preset.doorRule;
            }

            if (doorCheck)
            {
                openedDoor = currentAction.path.accessList[pathCursor].door;

                if (openedDoor.isLocked)
                {
                    //Do I need to lock/unlock this door?
                    //Force unlock action
                    bool keepLocked = false;

                    if (openedDoor.lockSetting == NewDoor.LockSetting.keepLocked)
                    {
                        keepLocked = true;

                        //Depending on action door rules, maybe don't lock behind
                        if (doorRule == AIActionPreset.DoorRule.dontClose || doorRule == AIActionPreset.DoorRule.dontLock || doorRule == AIActionPreset.DoorRule.onlyCloseToLocation)
                        {
                            keepLocked = false;
                        }
                        else if (doorRule == AIActionPreset.DoorRule.onlyLockToLocation)
                        {
                            if (openedDoor.wall.node.gameLocation == openedDoor.wall.otherWall.node.gameLocation)
                            {
                                keepLocked = false;
                            }
                        }
                    }

                    //Attempt to insert unlock action
                    if (currentGoal != null)
                    {
                        doorCheck = currentGoal.InsertUnlockAction(openedDoor, keepLocked);
                        if (Game.Instance.collectDebugData) human.SelectedDebug("Door check; attempt to insert unlock action: " + doorCheck, Actor.HumanDebug.actions);
                    }

                    if (doorCheck)
                    {
                        AITick();
                        return;
                    }
                }

                if (doorCheck)
                {
                    //Add to door access list
                    if (!openedDoor.usingDoorList.Contains(human))
                    {
                        openedDoor.usingDoorList.Add(human);
                        doorInteractions.Add(openedDoor);
                    }

                    openedDoor.OpenByActor(human);
                }

            }

            //Cancel current action: Effectively wait...
            if (!doorCheck && doorCheckDoor != null)
            {
                //Set stopped
                human.SetDesiredSpeed(Human.MovementSpeed.stopped);
                SetStaticFromAnimation(true);

                float knockValue = GameplayController.Instance.GetDoorKnockAttemptValue(doorCheckDoor, human as Actor);

                if (knockValue <= 0f)
                {
                    //Play open attempt sfx
                    AudioController.Instance.PlayWorldOneShot(doorCheckDoor.preset.audioLockedEntryAttempt, human, doorCheckDoor.parentedWall.node, doorCheckDoor.transform.position);
                }

                if (Game.Instance.collectDebugData) human.SelectedDebug("Knock on door value: " + knockValue, Actor.HumanDebug.movement);

                //Trigger trapped bark
                if(knockValue > 1.5f)
                {
                    if (doorReason == NewDoor.CitizenPassResult.isJammed)
                    {
                        //Is this the only door?
                        bool onlyDoor = true;

                        foreach (NewNode.NodeAccess acc in human.currentRoom.entrances)
                        {
                            if (acc.door == doorCheckDoor) continue;

                            if (acc.door != null || acc.walkingAccess)
                            {
                                onlyDoor = false;
                                break;
                            }
                        }

                        if (onlyDoor)
                        {
                            human.speechController.TriggerBark(SpeechController.Bark.doorBlocked);
                        }
                    }
                }

                if (knockValue < 3f)
                {
                    //First allow knock on door attempts...
                    try
                    {
                        Game.Log("Debug: " + human.name + " Knock on door at " + human.currentRoom.name + ", " + human.currentGameLocation.name + ": Knock value: " + knockValue + " action: " + currentAction.preset.name + " reason: " + doorReason);
                    }
                    catch
                    {
                    
                    }

                    if (currentAction != null && !currentGoal.TryInsertDoorAction(doorCheckDoor, RoutineControls.Instance.knockOnDoor, NewAIGoal.DoorSide.mySide, 99, out _, immediateTick: false, debug: "Knock on door"))
                    {
                        //Recursion can happen here, this should stop it
                        if (!usePointBusyRecursion)
                        {
                            usePointBusyRecursion = true;
                            currentAction.OnUsePointBusy();
                            usePointBusyRecursion = false;
                        }
                        else
                        {
                            currentGoal.OnDeactivate(currentGoal.preset.repeatDelayOnBusy);
                        }

                        return;
                    }
                }
                else if (currentGoal != null)
                {
                    //Is this goal critical? Then break open door!
                    if (doorReason == NewDoor.CitizenPassResult.isJammed || (currentAction != null && currentAction.preset.breakDownDoors))
                    {
                        if (Game.Instance.collectDebugData) human.SelectedDebug("Break down door!", Actor.HumanDebug.actions);

                        NewAIGoal.DoorActionCheckResult result = NewAIGoal.DoorActionCheckResult.success;

                        if (!currentGoal.TryInsertDoorAction(doorCheckDoor, RoutineControls.Instance.bargeDoor, NewAIGoal.DoorSide.mySide, 99, out result, immediateTick: true, debug: "Barge door!"))
                        {
                            //Recursion can happen here, this should stop it
                            if (!usePointBusyRecursion)
                            {
                                usePointBusyRecursion = true;
                                currentAction.OnUsePointBusy();
                                usePointBusyRecursion = false;
                            }
                            else
                            {
                                currentGoal.OnDeactivate(currentGoal.preset.repeatDelayOnBusy);
                            }
                        }
                    }
                    //Deactivate goal
                    else
                    {
                        //Recursion can happen here, this should stop it
                        if (!usePointBusyRecursion)
                        {
                            usePointBusyRecursion = true;
                            currentAction.OnUsePointBusy();
                            usePointBusyRecursion = false;
                        }
                        else
                        {
                            currentGoal.OnDeactivate(currentGoal.preset.repeatDelayOnBusy);
                        }
                    }
                }
            }
        }
        else
        {
            if (Game.Instance.collectDebugData) human.SelectedDebug("Door check: Invalid current action, setting check to true", Actor.HumanDebug.movement);
            doorCheck = true;
        }

        doorCheckProcessTimer = 5;
    }

    //Set world space destination
    public void SetDestinationNode(NewNode newLocation, bool scanForNextNodeFurniture = true)
    {
        if(newLocation != null && Game.Instance.collectDebugData) human.SelectedDebug("Setting destination node location as " + newLocation.position + " (current: " + this.transform.position + ")", Actor.HumanDebug.actions);

        //Remove previous traveller
        if(currentDestinationNode != newLocation && currentDestinationNode != null)
        {
            //Clear previously reserved
            human.RemoveReservedNodeSpace();

            //currentDestinationNode.RemoveHumanTraveller(human);
        }

        bool occupiedSpaceOverride = false; //Used to override the node's captacity for people.

        //If the next location features an action that exceeds the user limit
        if (currentAction != null && scanForNextNodeFurniture)
        {
            //If the action is located on this node...
            if (currentAction.node == newLocation)
            {
                if (currentAction.interactable != null)
                {
                    if (Game.Instance.collectDebugData) human.SelectedDebug("Occupied space override as current action (" + currentAction.name + ") is on the new location node ("+newLocation +")", Actor.HumanDebug.actions );
                    occupiedSpaceOverride = true;

                    if(!currentAction.InteractableUsePointCheck())
                    {
                        currentAction.OnUsePointBusy();
                    }
                }
            }
        }

        currentDestinationNode = newLocation;
        
        if(currentDestinationNode != null)
        {
            currentDesitnationNodeCoord = currentDestinationNode.nodeCoord;
        }
        else
        {
            currentDesitnationNodeCoord = Vector3.zero;
        }

        bool validMovement = true;

        //First try a dynamic re-route
        if (currentDestinationNode != null && currentDestinationNode.room.gameObject.activeInHierarchy && Game.Instance.dynamicReRouting) //Room must be drawn to do this
        {
            if (currentDestinationNode.occupiedSpace.Count > 0 && !occupiedSpaceOverride)
            {
                if (currentAction != null && currentAction.path != null && currentAction.path.accessList != null && pathCursor + 1 < currentAction.path.accessList.Count)
                {
                    NewNode newDestination = null;
                    NewNode beyond = currentAction.path.accessList[pathCursor + 1].toNode;

                    if (DynamicReRoute(human.currentNode, currentDestinationNode, beyond, out newDestination))
                    {
                        currentDestinationNode = newDestination; //Set destination to this instead

                        if (currentDestinationNode != null)
                        {
                            currentDesitnationNodeCoord = currentDestinationNode.nodeCoord;
                        }
                        else
                        {
                            currentDesitnationNodeCoord = Vector3.zero;
                        }
                    }
                }
            }
        }

        //Fit human on this node. Get the use point from this too and set it as current target position
        if (currentDestinationNode != null && currentAction != null)
        {
            validMovement = currentDestinationNode.AddHumanTraveller(human, currentAction.usagePoint, out currentDestinationPositon);

            if(currentAction.usagePoint != null && Game.Instance.collectDebugData)
            {
                human.SelectedDebug("Updated destination position: " + currentDestinationPositon + " (node: " + currentDesitnationNodeCoord + ", usage point: " + currentAction.usagePoint.interactable.name + " " + currentAction.usagePoint.interactable.id +")", Actor.HumanDebug.movement);
                DebugDestinationPosition("Updated destination position: " + currentDestinationPositon + " (node: " + currentDesitnationNodeCoord + ", usage point: " + currentAction.usagePoint.interactable.name + " " + currentAction.usagePoint.interactable.id + ")");
            }
        }
        else
        {
            validMovement = false;

            if (Game.Instance.collectDebugData)
            {
                human.SelectedDebug("The destination was not set because of invalid input. It remains: " + currentDestinationPositon + " (node: " + currentDesitnationNodeCoord + ")", Actor.HumanDebug.movement);
                DebugDestinationPosition("The destination was not set because of invalid input. It remains: " + currentDestinationPositon + " (node: " + currentDesitnationNodeCoord + ")");
            }
        }

        //If that fails then delay behaviour...
        if (!validMovement && delayFlag < 3)
        {
            if (Game.Instance.collectDebugData) human.SelectedDebug("Invalid movement, creating short delay...", Actor.HumanDebug.movement);

            //Delay citizen
            currentDestinationNode = null; //Reset destination to null. This will trigger this again after delay is over.
            currentDesitnationNodeCoord = Vector3.zero;
            SetDelayed(Toolbox.Instance.Rand(1f, 2f));

            if(currentAction != null)
            {
                currentAction.OnInvalidMovement(delayFlag);
            }

            delayFlag++;
        }
        else
        {
            //This is the 3rd time this citizen has been delayed. We need to let them proceed anyway, otherwise they could be stuck here forever!
            delayFlag = 0;
        }

        //Look in travel direction
        if(currentAction == null || !currentAction.isAtLocation)
        {
            SetFaceTravelDirection();
        }
    }

    //Dynamically re-route to avoid this tile: Needs the current tile, the tile to avoid and the node beyond. Pick a v
    private bool DynamicReRoute(NewNode current, NewNode avoidThis, NewNode beyond, out NewNode bestAvoidanceTile)
    {
        bestAvoidanceTile = null;
        if (current == null || avoidThis == null || beyond == null) return false;

        //Gather a list of adjacent tiles that have access to both current + beyond
        int peopleHere = -1;

        foreach(KeyValuePair<NewNode, NewNode.NodeAccess> pair in current.accessToOtherNodes)
        {
            //Found the original destination node...
            if(pair.Key == avoidThis)
            {
                //This isn't possible if there is a door here
                if (pair.Value.door != null) return false;
            }

            if (pair.Key == avoidThis) continue; //Avoid node
            if (!pair.Value.walkingAccess) continue; //No walking access
            if (pair.Value.door != null) continue; //Don't do this with doors
            if (pair.Value.accessType != NewNode.NodeAccess.AccessType.adjacent && pair.Value.accessType != NewNode.NodeAccess.AccessType.streetToStreet) continue;
            if (pair.Key.isObstacle || pair.Key.noPassThrough) continue;
            if (pair.Key.nodeCoord.z != current.nodeCoord.z) continue; //Don't do this through different floors

            //Found a valid alternate tile, but now we need to check if it also connects to the 'beyond' node...
            NewNode.NodeAccess beyondAccess = null;

            if(pair.Key.accessToOtherNodes.TryGetValue(beyond, out beyondAccess))
            {
                if (!beyondAccess.walkingAccess) continue; //No walking access
                if (beyondAccess.door != null) continue; //Don't do this with doors
                if (beyondAccess.accessType != NewNode.NodeAccess.AccessType.adjacent && pair.Value.accessType != NewNode.NodeAccess.AccessType.streetToStreet) continue;
                if (pair.Key.nodeCoord.z != current.nodeCoord.z) continue; //Don't do this through different floors

                //Space on this node
                if (pair.Key.occupiedSpace.Count < pair.Key.walkableNodeSpace.Count)
                {
                    //The original destination is completely full OR this new node has less people on
                    if(avoidThis.occupiedSpace.Count >= avoidThis.walkableNodeSpace.Count || pair.Key.occupiedSpace.Count < avoidThis.occupiedSpace.Count)
                    {
                        //Success!
                        if(bestAvoidanceTile == null || pair.Key.occupiedSpace.Count < peopleHere)
                        {
                            bestAvoidanceTile = pair.Key;
                            peopleHere = pair.Key.occupiedSpace.Count;
                        }
                    }
                }
            }
        }

        if(bestAvoidanceTile == null)
        {
            if (Game.Instance.collectDebugData) human.SelectedDebug("Dynamic re-route failed: From " + current.position + ", avoiding " + avoidThis.position, Actor.HumanDebug.movement);

            return false;
        }

        //We have our new node to swap!
        if (Game.Instance.collectDebugData) human.SelectedDebug("Dynamic re-route success: From " + current.position + ", avoiding " + avoidThis.position + ", new: " + bestAvoidanceTile.position, Actor.HumanDebug.movement);
        return true;
    }

    //Look towards travel direction
    public void SetFaceTravelDirection()
    {
        SetFacingPosition(currentDestinationPositon);
    }

    //Set target direction
    public void SetFacingPosition(Vector3 newLookPoint)
    {
        //Look point must be different from position...
        if(newLookPoint == human.transform.position)
        {
            if (Game.Instance.collectDebugData) DebugDestinationPosition("New look point is same as position!");
            return;
        }

        faceTransform = null; //Set look @ transform to null
        facingDirection = newLookPoint - human.transform.position;

        if (Game.Instance.collectDebugData) human.SelectedDebug("Set facing position " + newLookPoint, Actor.HumanDebug.movement);

        //Update quaternion
        facingDirection.y = 0; // keep only the horizontal direction
        if (facingDirection != Vector3.zero) facingQuat = Quaternion.LookRotation(facingDirection);

        //Turn magnitude (0.25 to 1) multiplies how quickly to turn- the idea being minor turns happen more slowly
        //turnSpeed = Mathf.Clamp(Mathf.Abs(facingDirection.y - transform.eulerAngles.y) / 180f + 0.5f, 0.25f, 1f);

        DebugDestinationPosition("Set facing position: " + facingQuat.eulerAngles + " current: " + transform.eulerAngles);

        //If visible then enable, otherwise snap
        if(human.visible)
        {
            SetUpdateEnabled(true); //Enable this to enable facing lerp
        }
        else
        {
            transform.rotation = facingQuat;
        }
    }

    //Similar to above but uses a direction
    public void SetFacingDirection(Vector3 newLookDirection)
    {
        faceTransform = null; //Set look @ transform to null
        facingDirection = newLookDirection;

        if (Game.Instance.collectDebugData) human.SelectedDebug("Set facing direction " + newLookDirection, Actor.HumanDebug.movement);

        //Update quaternion
        facingDirection.y = 0; // keep only the horizontal direction
        if (facingDirection != Vector3.zero) facingQuat = Quaternion.LookRotation(facingDirection);

        //Turn magnitude (0.25 to 1) multiplies how quickly to turn- the idea being minor turns happen more slowly
        //turnSpeed = Mathf.Clamp(Mathf.Abs(facingDirection.y - transform.eulerAngles.y) / 180f + 0.5f, 0.25f, 1f);

        if(human.visible)
        {
            SetUpdateEnabled(true); //Enable this to enable facing lerp
        }
        else
        {
            transform.rotation = facingQuat;
        }
    }

    //Set facing a transform
    public void SetFacingTransform(Transform newLookAt, Vector3 offset)
    {
        faceTransform = newLookAt;
        faceTransformOffset = offset;
        if (Game.Instance.collectDebugData) human.SelectedDebug("Set facing transform " + newLookAt, Actor.HumanDebug.movement);
        //Game.Log("SetLookAtObject " + cit.name + ": " + newLookAt);

        //If visible, set active for smooth rotation, otherwise just set
        if (human.visible)
        {
            SetUpdateEnabled(true); //Enable this to enable facing lerp
        }
        else
        {
            Vector3 offsetPos = faceTransform.position + faceTransform.TransformPoint(faceTransformOffset);
            Vector3 relativePos = offsetPos - transform.position;
            relativePos.y = 0;
            facingQuat = Quaternion.LookRotation(relativePos, Vector3.up);
            transform.rotation = facingQuat;
        }
    }

    //Set looking at a transform
    public void SetLookAtTransform(Transform newTarget, float newRank)
    {
        if (lookAtTransform != newTarget)
        {
            lookAtTransform = newTarget;
        }

        lookAtTransformRank = newRank;
    }

    //Add/update a tracked target target: This is done or updated when seen
    public void AddTrackedTarget(Actor newTracked)
    {
        //Add if target doesn't exist already
        int existsAlreadyIndex = trackedTargets.FindIndex(item => item.actor == newTracked);

        TrackingTarget newTarget;

        //Create new
        if (existsAlreadyIndex <= -1)
        {
            newTarget = new TrackingTarget();
            newTarget.actor = newTracked;
            trackedTargets.Add(newTarget);
            if (Game.Instance.collectDebugData) human.SelectedDebug("Added new tracking target: " + newTarget.actor, Actor.HumanDebug.sight);

            //Update ranking
            newTarget.attractionRank = 0.5f;

            Human tracked = newTracked as Human;

            if (tracked != null)
            {
                if (human.attractedTo.Contains(tracked.gender))
                {
                    newTarget.attractionRank = 1f;
                }
            }
        }
        else
        {
            //Update validity
            newTarget = trackedTargets[existsAlreadyIndex];
        }

        //Global update of stats
        newTarget.lastValidSighting = SessionData.Instance.gameTime;
        newTarget.priorityTarget = newTracked.illegalStatus;
    }

    private void TrackingSpookCheck(TrackingTarget newTarget, bool seen)
    {
        if(seen)
        {
            if (!newTarget.spookedByItem && newTarget.active)
            {
                if (newTarget.actor.ai != null)
                {
                    if (newTarget.actor.ai.inCombat)
                    {
                        if (newTarget.actor.ai.attackTarget == human || newTarget.actor.ai.persuitTarget == human)
                        {
                            if (newTarget.actor.ai.currentWeaponPreset != null)
                            {
                                if (human.speechController != null && !human.ai.inCombat && Toolbox.Instance.Rand(0f, 1f) <= newTarget.actor.ai.currentWeaponPreset.barkTriggerChance)
                                {
                                    if (human.speechController.speechQueue.Count <= 0)
                                    {
                                        human.speechController.TriggerBark(newTarget.actor.ai.currentWeaponPreset.bark);
                                        newTarget.spookedByItem = true;
                                    }
                                }
                            }
                        }
                    }
                }
                else if (newTarget.actor.isPlayer)
                {
                    if (FirstPersonItemController.Instance.currentItem != null && FirstPersonItemController.Instance.currentItem.barkTriggerChance > 0f)
                    {
                        if (human.speechController != null && !human.ai.inCombat && Toolbox.Instance.Rand(0f, 1f) <= FirstPersonItemController.Instance.currentItem.barkTriggerChance)
                        {
                            if (human.speechController.speechQueue.Count <= 0)
                            {
                                human.speechController.TriggerBark(FirstPersonItemController.Instance.currentItem.bark);
                                newTarget.spookedByItem = true;
                            }
                        }
                    }
                }
            }
            else
            {
                if(newTarget.spookedByItem)
                {
                    if (newTarget.actor.ai != null && !newTarget.actor.ai.inCombat)
                    {
                        newTarget.spookedByItem = false;
                        newTarget.spookTimer = 0;
                    }
                    else if (newTarget.actor.isPlayer && (FirstPersonItemController.Instance.currentItem == null || FirstPersonItemController.Instance.currentItem.barkTriggerChance <= 0f))
                    {
                        newTarget.spookedByItem = false;
                        newTarget.spookTimer = 0;
                    }

                    newTarget.spookTimer++;

                    if(newTarget.spookTimer > 35)
                    {
                        newTarget.spookedByItem = false;
                        newTarget.spookTimer = 0;
                    }
                }
                else if(!newTarget.active)
                {
                    newTarget.spookedByItem = false;
                    newTarget.spookTimer = 0;
                }
            }
        }
        else
        {
            newTarget.spookedByItem = false;
        }

        UpdateHumanDrawnWeapon(newTarget.actor as Human, seen);
    }

    //Triggered when a spotted human draws a weapon
    public void UpdateHumanDrawnWeapon(Human who, bool seen)
    {
        if (who != null)
        {
            float appliedNerveChange = 0;

            //If the citizen is no longer seen, remove any effect
            if (!seen)
            {
                if (appliedNerveEffect.ContainsKey(who))
                {
                    appliedNerveChange = -appliedNerveEffect[who];
                    appliedNerveEffect.Remove(who);
                }
            }
            else
            {
                if (who.ai != null)
                {
                    //If their weapon is no longer drawn...
                    if (!who.ai.inCombat)
                    {
                        if (appliedNerveEffect.ContainsKey(who))
                        {
                            appliedNerveChange = -appliedNerveEffect[who];
                            appliedNerveEffect.Remove(who);
                        }
                    }
                    //Otherwise apply nerve effect if not already present in dictionary
                    else if (who.ai.currentWeaponPreset != null)
                    {
                        if (!appliedNerveEffect.ContainsKey(who))
                        {
                            appliedNerveEffect.Add(who, who.ai.currentWeaponPreset.drawnNerveModifier);
                            appliedNerveChange = appliedNerveEffect[who];
                        }
                    }
                }
                else if (who.isPlayer)
                {
                    //No weapon drawn
                    if (FirstPersonItemController.Instance.currentItem == null)
                    {
                        if (appliedNerveEffect.ContainsKey(who))
                        {
                            appliedNerveChange = -appliedNerveEffect[who];
                            appliedNerveEffect.Remove(who);
                        }
                    }
                    else
                    {
                        float nerveAmount = 0f;

                        if (FirstPersonItemController.Instance.currentItem.name.ToLower() == "fists")
                        {
                            nerveAmount = FirstPersonItemController.Instance.currentItem.drawnNerveModifier * (1f + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.fistsThreatModifier));
                        }
                        else nerveAmount = FirstPersonItemController.Instance.currentItem.drawnNerveModifier;

                        //If an existing effect; compare difference and calculate change
                        if (appliedNerveEffect.ContainsKey(who))
                        {
                            float existing = appliedNerveEffect[who];
                            appliedNerveChange = nerveAmount - existing;

                            if (appliedNerveChange != 0)
                            {
                                appliedNerveEffect[who] = nerveAmount; //Update the current record
                            }
                        }
                        //If nothing exists then just add
                        else
                        {
                            appliedNerveEffect.Add(who, nerveAmount);
                            appliedNerveChange = appliedNerveEffect[who];
                        }
                    }
                }
            }

            if (appliedNerveChange != 0)
            {
                human.AddNerve(appliedNerveChange * CitizenControls.Instance.nerveWeaponDrawMultiplier);
            }
        }
    }

    //Update look @ targets
    public void UpdateTrackedTargets()
    {
        //human.SelectedDebug("Update Tracked Targets...");

        //Expire targets
        float halfFOV = GameplayControls.Instance.citizenFOV * 0.5f;

        //Check validity
        for (int i = 0; i < trackedTargets.Count; i++)
        {
            TrackingTarget lt = trackedTargets[i];
            lt.active = false;

            //Check for expired sightings
            if (SessionData.Instance.gameTime > lt.lastValidSighting + CitizenControls.Instance.lookAtGracePeriod)
            {
                if (Game.Instance.collectDebugData) human.SelectedDebug("Removed tracking target due to sighting grace period: " + lt.actor, Actor.HumanDebug.sight);
                TrackingSpookCheck(lt, false);

                //Remove look @ target
                RemoveLookAtTargetAt(i);
                i--;
                continue;
            }
            else
            {
                lt.lookAtRank = 0f;

                lt.attractionRank = 0f;
                lt.fovRank = 0f;
                lt.distanceRank = 0f;
                lt.itemRank = 0f;

                //Calculate look @ rank
                if (lt.priorityTarget || lt.spookedByItem)
                {
                    lt.lookAtRank = 99f;
                }
                else
                {
                    //Check distance (33%)
                    lt.distance = Vector3.Distance(human.lookAtThisTransform.position, lt.actor.lookAtThisTransform.position);

                    if (lt.distance <= GameplayControls.Instance.citizenSightRange)
                    {
                        lt.distanceRank += (1f - Mathf.Clamp01(lt.distance / GameplayControls.Instance.citizenSightRange)) * 0.33f;
                    }
                    else lt.distanceRank = 0f;

                    //Is the citizen within my field of view?
                    Vector3 targetDir = lt.actor.lookAtThisTransform.position - human.lookAtThisTransform.position;
                    float angleToOther = Vector3.Angle(targetDir, transform.forward); //Local angle

                    if (angleToOther >= -halfFOV && angleToOther <= halfFOV) // 180° FOV
                    {
                        lt.fovRank += (1f - (Mathf.Abs(angleToOther) / halfFOV)) * 0.33f;
                    }

                    //Item rank
                    if(appliedNerveEffect.ContainsKey(lt.actor as Human))
                    {
                        lt.itemRank = appliedNerveEffect[lt.actor as Human] * -1f;
                    }

                    lt.lookAtRank = lt.attractionRank + lt.distanceRank + lt.fovRank + lt.itemRank; //Can never be equal to priority target
                }
            }
        }

        //If interacting, focus on that
        if (human.interactingWith != null)
        {
            //human.SelectedDebug("Look at interacting with");
            SetLookAtTransform(human.lookAtThisTransform, 2f);
        }
        else if (human.inConversation && human.currentConversation.currentlyTalking != null && human.currentConversation.currentlyTalking != human)
        {
            //human.SelectedDebug("Look at in conversation");
            SetLookAtTransform(human.currentConversation.currentlyTalking.lookAtThisTransform, 2f);
        }
        else if (human.inConversation && human.currentConversation.currentlyTalkingTo != null && human.currentConversation.currentlyTalkingTo != human)
        {
            //human.SelectedDebug("Look at in conversation");
            SetLookAtTransform(human.currentConversation.currentlyTalkingTo.lookAtThisTransform, 2f);
        }
        else if(seesOnPersuit)
        {
            //human.SelectedDebug("Look at persuit");
            SetLookAtTransform(Player.Instance.lookAtThisTransform, 2f);
        }
        else if(audioFocusAction != null)
        {
            SetTrackTarget(null);
        }
        else
        {
            //Sort by rank
            if (trackedTargets.Count > 0)
            {
                trackedTargets.Remove(null);

                try
                {
                    trackedTargets.Sort((a1, a2) => a2.lookAtRank.CompareTo(a1.lookAtRank)); //Using P2 first gives highest first
                }
                catch
                {
                    
                }

                if(trackedTargets[0] != currentTrackTarget)
                {
                    if(currentTrackTarget != null) TrackingSpookCheck(currentTrackTarget, false);
                    SetTrackTarget(trackedTargets[0]);
                }
            }
            //Set no target
            else if (lookAtTransform != null)
            {
                SetTrackTarget(null);
            }
        }

        if(currentTrackTarget != null)
        {
            currentTrackTarget.active = true;
            TrackingSpookCheck(currentTrackTarget, true);
        }

        //If visible, set active for smooth rotation, otherwise just set
        if (human.visible)
        {
            SetUpdateEnabled(true); //Enable for looking at lerp
        }
        else
        {
            if (lookAtTransform != null)
            {
                Vector3 relativePos = lookAtTransform.position - human.lookAtThisTransform.position;
                lookingQuatCurrent = Quaternion.LookRotation(relativePos, Vector3.up);
            }
            else
            {
                lookingQuatCurrent = Quaternion.LookRotation(human.transform.forward, Vector3.up);
            }

            human.neckTransform.rotation = lookingQuatCurrent;
            ClampNeckRotation();
        }

        lastLookAtUpdateTime = SessionData.Instance.gameTime;
    }

    //Set a tracking target
    public void SetTrackTarget(TrackingTarget newTrackingTarget)
    {
        if(newTrackingTarget == null)
        {
            //human.SelectedDebug("Set new tracking target: Null");
            SetLookAtTransform(null, 0);
            currentTrackTarget = null;
        }
        else if (newTrackingTarget != currentTrackTarget)
        {
            if(currentTrackTarget != null && Game.Instance.collectDebugData) human.SelectedDebug("Set new tracking target: " + newTrackingTarget.actor + " with rank of " + newTrackingTarget.lookAtRank + " vs old target " + currentTrackTarget.lookAtRank, Actor.HumanDebug.sight);
            currentTrackTarget = newTrackingTarget;
            SetLookAtTransform(newTrackingTarget.actor.lookAtThisTransform, newTrackingTarget.lookAtRank);
            OnNewTrackTarget();
        }
    }

    //Called when the look @ target has changed
    public void OnNewTrackTarget()
    {
        if(currentTrackTarget != null)
        {
            if(currentTrackTarget.actor != null)
            {
                human.SpeechTriggerPoint(DDSSaveClasses.TriggerPoint.onNewTrackTarget, currentTrackTarget.actor);

                //Trigger mugging...
                try
                {
                    //Target must be richer than them by a margin
                    Human other = currentTrackTarget.actor as Human;

                    if (other != null)
                    {
                        //Chance
                        if (Toolbox.Instance.Rand(0f, 1f) <= GameplayControls.Instance.muggingChance)
                        {
                            if (IsMuggingValid(other, out _))
                            {
                                //Must not already be mugging/in dialog mode
                                if (currentGoal != null && !currentGoal.actions.Exists(item => item.preset == RoutineControls.Instance.mugging) && !human.inConversation)
                                {
                                    if(currentGoal.TryInsertInteractableAction(other.interactable, RoutineControls.Instance.mugging, 99))
                                    {
                                        Game.Log("Triggered mugging of " + other.name + " by " + human.name + "...");
                                    }
                                }
                            }
                        }
                    }
                }
                catch
                {

                }
            }
        }
    }

    public bool IsMuggingValid(Human target, out string debugReason)
    {
        bool ret = false;
        debugReason = string.Empty;

        //Rooms must allow muggings
        if (human.currentRoom.preset.allowMuggings && target.currentRoom != null && target.currentRoom.preset.allowMuggings)
        {
            if(!human.IsTrespassing(target.currentRoom, out _, out _, false))
            {
                //Goal must allow mugging
                if (currentGoal == null || !currentGoal.preset.diabledMugging)
                {
                    if (!persuit && !human.isDead && !human.isStunned && !target.isAsleep && !target.isStunned && !target.isDead && !target.isAsleep && !target.inConversation)
                    {
                        if(!target.isPlayer || InteractionController.Instance.talkingTo != human.interactable)
                        {
                            if(!target.isPlayer || GameplayController.Instance.money > 0)
                            {
                                //Must have low agreeableness and humility
                                if (human.agreeableness + human.humility <= 0.7f)
                                {
                                    //Must be destitue
                                    if (human.societalClass <= 0.25f)
                                    {
                                        if (target.currentRoom != null)
                                        {
                                            if (target.isPlayer || target.societalClass > human.societalClass + 0.35f)
                                            {
                                                if (target.isPlayer || target.ai.currentGoal == null || !target.ai.currentGoal.preset.diabledMugging)
                                                {
                                                    //Must not know them
                                                    if (target.isPlayer || !human.acquaintances.Exists(item => item.with == target))
                                                    {
                                                        //Must be fewer than 3 people around
                                                        int vicinity = 0;

                                                        foreach(Actor a in target.currentRoom.currentOccupants)
                                                        {
                                                            if(Vector3.Distance(a.transform.position, target.transform.position) < 10f)
                                                            {
                                                                vicinity++;
                                                            }
                                                        }
                                                
                                                        foreach(NewRoom r in target.currentRoom.adjacentRooms)
                                                        {
                                                            if (r == target.currentRoom) continue;

                                                            foreach (Actor a in r.currentOccupants)
                                                            {
                                                                if (Vector3.Distance(a.transform.position, target.transform.position) < 10f)
                                                                {
                                                                    vicinity++;
                                                                }
                                                            }
                                                        }

                                                        if(vicinity < 3)
                                                        {
                                                            if (currentGoal != null)
                                                            {
                                                                ret = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            debugReason = "Too many people at target's location (" + vicinity + ")";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        debugReason = "Mugger knows target";
                                                    }
                                                }
                                                else
                                                {
                                                    debugReason = "Target's goal doe not allow mugging";
                                                }
                                            }
                                            else
                                            {
                                                debugReason = "Mismatch of target's societal class";
                                            }
                                        }
                                        else
                                        {
                                            debugReason = "Target is not in a room";
                                        }
                                    }
                                    else
                                    {
                                        debugReason = "Mugger's societal class is not compatible";
                                    }
                                }
                                else
                                {
                                    debugReason = "Mugger's traits are not compatible";
                                }
                            }
                        }
                        else
                        {
                            debugReason = "Player is talking to someboy else";
                        }
                    }
                    else
                    {
                        debugReason = "Mugger is inPersuit/Dead/Stunned or target is Asleep/Stunned/Dead/Asleep";
                    }
                }
                else
                {
                    debugReason = "Mugger's current goal does not allow mugging (" + currentGoal.preset.name + ")";
                }
            }
            else
            {
                debugReason = "Mugger would be trespassing at target's location (" + target.currentRoom.GetName() + ")";
            }
        }
        else
        {
            debugReason = "Location " + human.currentRoom.GetName() + " does not allow muggings";
        }

        return ret;
    }

    //For use only within the update look at targets function
    private void RemoveLookAtTargetAt(int index)
    {
        trackedTargets.RemoveAt(index);
    }

    //Triggered when the visibility for this actor is changed
    public void OnVisibilityChanged()
    {
        //if(human.visible)
        //{
        //    //Look forwards
        //    lookingQuat = Quaternion.LookRotation(human.transform.forward, Vector3.up);
        //    human.neckTransform.rotation = lookingQuat;
        //}
    }

    //Set expression
    public void SetExpression(CitizenOutfitController.Expression newExpression)
    {
        if(currentExpression == null || currentExpression.expression != newExpression)
        {
            expressionProgress = 0f;
            human.outfitController.expressionReference.TryGetValue(newExpression, out currentExpression);
        }
    }

    //Add debug action (display x5)
    public void AddDebugAction(string msg)
    {
        if (!Game.Instance.devMode || !Game.Instance.collectDebugData) return;

        if(lastActions.Count >= 20)
        {
            lastActions.RemoveAt(0);
        }

        lastActions.Add(SessionData.Instance.gameTime + ": " + msg);
    }

    //Debug teleport player to location
    [Button("Teleport Player")]
    public void DebugTeleportPlayerToLocation()
    {
        Player.Instance.Teleport(human.currentNode, null);
    }

    [Button("Give Sleep!")]
    public void GiveSleep()
    {
        human.AddEnergy(1f);
    }

    [Button("Remove Sleep!")]
    public void RemoveSleep()
    {
        human.AddEnergy(-1f);
    }

    [Button("Give Food!")]
    public void GiveFood()
    {
        human.AddNourishment(0.2f);
    }

    [Button("Remove Food!")]
    public void RemoveFood()
    {
        human.AddNourishment(-0.2f);
    }

    [Button("Give Drink!")]
    public void GiveDrink()
    {
        human.AddHydration(0.2f);
    }

    [Button("Remove Drink!")]
    public void RemoveDrink()
    {
        human.AddHydration(-0.2f);
    }

    [Button("Give Caffeine!")]
    public void GiveCaffeine()
    {
        human.AddAlertness(0.2f);
    }

    [Button("Remove Caffeine!")]
    public void RemoveCaffeine()
    {
        human.AddAlertness(-0.2f);
    }

    [Button("Give Fun!")]
    public void GiveFun()
    {
        human.AddExcitement(0.2f);
    }

    [Button("Remove Fun!")]
    public void RemoveFun()
    {
        human.AddExcitement(-0.2f);
    }

    [Button("Give Bladder!")]
    public void GiveBladder()
    {
        human.AddBladder(0.2f);
    }

    [Button("Remove Bladder!")]
    public void RemoveBladder()
    {
        human.AddBladder(-0.2f);
    }

    [Button("Give Hygeine!")]
    public void GiveHygiene()
    {
        human.AddHygiene(0.2f);
    }

    [Button("Remove Hygeine!")]
    public void RemoveHygiene()
    {
        human.AddHygiene(-0.2f);
    }

    [Button("Give Drunk!")]
    public void GiveDrunk()
    {
        human.AddDrunk(0.2f);
    }

    [Button("Remove Drunk!")]
    public void RemoveDrunk()
    {
        human.AddDrunk(-0.2f);
    }

    [Button("Murder")]
    public void MurderButton()
    {
        human.SetHealth(0);
        SetKO(true);
        human.Murder(Player.Instance, true, null, null);
    }

    [Button("Debug: Why Aren't I moving?")]
    public void DebugMovement()
    {
        debugMovement = !debugMovement;
        Game.Log("Debug movement for " + human.name + ": " + debugMovement);
    }

    [Button("Trip")]
    public void Trip()
    {
        if(!human.isDead && !human.isStunned && !isTripping)
        {
            if(human.animationController != null)
            {
                isTripping = true;
                human.animationController.TriggerTrip();
            }
        }
    }

    [Button()]
    public void UpdateProjectedChasePosition()
    {
        chaseLogic.GenerateProjectedNode();
    }

    public void HearIllegal(AudioEvent audioEvent, NewNode newInvestigateNode, Vector3 newInvestigatePosition, Actor newTarget, int escLevel)
    {
        float audioFocus = audioEvent.audioFocus;

        //If already in sight then ignore...
        if (seesOnPersuit) return;
        //If searching, treat this at immediate investigate...
        else if((persuit || (newTarget != null && human.seesIllegal.ContainsKey(newTarget))) && !seesOnPersuit)
        {
            audioFocus = 1f;
        }

        if (audioEvent != null) human.SelectedDebug("Hears illegal sound: " + audioEvent.name + " by " + newTarget + " audio focus: " + audioFocus, Actor.HumanDebug.sight);
        if (Game.Instance.devMode && Game.Instance.collectDebugData) debugLastHeardIllegalAudio = audioEvent;

        if (human.inConversation)
        {
            human.currentConversation.EndConversation();
        }

        if(audioEvent.spookValue != 0f)
        {
            if(!audioEvent.noSpookIfEnforcer || !human.isEnforcer)
            {
                human.AddNerve(-audioEvent.spookValue);
            }
        }

        hearTarget = newTarget;
        hearsIllegal += audioFocus;
        hearsIllegal = Mathf.Clamp01(hearsIllegal);

        //If this reaches 1, investigate...
        if (hearsIllegal >= 1f)
        {
            Investigate(newInvestigateNode, newInvestigatePosition, null, NewAIController.ReactionState.investigatingSound, CitizenControls.Instance.soundMinInvestigationTimeMP, escLevel, setHighUrgency: audioEvent.urgentResponse);

            if (newTarget != null && (int)audioEvent.citizenMemoryTag > 0)
            {
                human.UpdateLastSighting(newTarget as Human, false, (int)audioEvent.citizenMemoryTag);
            }
        }
        else if(hearsIllegal >= 0f)
        {
            //Otherwise insert an audio focus window...
            if (audioFocusAction == null && currentGoal != null)
            {
                if (human.speechController != null) human.speechController.TriggerBark(SpeechController.Bark.hearsSuspicious);
                audioFocusAction = CreateNewAction(currentGoal, RoutineControls.Instance.audioFocus, true, newForcedNode: newInvestigateNode, newInsertedActionPriority: 99);
                AITick();
                SetReactionState(ReactionState.investigatingSound);
            }
        }

        SetUpdateEnabled(true); //Make sure this is enabled
    }

    //Update the investigation location: Use null target if unseen
    public void Investigate(NewNode newInvestigateNode, Vector3 newInvestigatePosition, Actor newTarget, ReactionState newReactionState, float minimumInvestiationTimeMP, int escalation, bool setHighUrgency = false, float focusTimeMultiplier = 1f, Interactable newInvesigationObj = null)
    {
        if (human.isDead) return;

        //Confine to location...
        if(newInvestigateNode != null)
        {
            if (confineLocation != null && confineLocation != newInvestigateNode.gameLocation)
            {
                if (Game.Instance.collectDebugData)
                {
                    human.SelectedDebug("AI is confined to location " + confineLocation.name, Actor.HumanDebug.actions);
                    DebugDestinationPosition("AI is confined to location " + confineLocation.name);
                }

                return;
            }

            if (avoidLocations != null && avoidLocations.Contains(newInvestigateNode.gameLocation))
            {
                if (Game.Instance.collectDebugData)
                {
                    human.SelectedDebug("AI is avoiding location " + newInvestigateNode.gameLocation.name, Actor.HumanDebug.actions);
                    DebugDestinationPosition("AI is avoiding location " + newInvestigateNode.gameLocation.name);
                }

                return;
            }

        }

        if (victimsForMurders.Count > 0)
        {
            foreach(MurderController.Murder m in victimsForMurders)
            {
                if (m.location == null) continue;
                if ((int)m.state < 4) continue;

                if (m.preset.blockVictimFromLeavingLocation)
                {
                    if(m.location != newInvestigateNode.gameLocation)
                    {
                        Game.Log("Murder: Removing investigation exterior to murder location of " + m.location.name);
                        return;
                    }
                }
            }
        }

        //Murderer
        //if(killerForMurders.Count > 0)
        //{
        //    human.SelectedDebug("Removing investigation order due to being a killer", Actor.HumanDebug.sight);
        //    return;
        //}

        //Cancel audio focus action
        if(audioFocusAction != null)
        {
            audioFocusAction.Complete();
        }

        //Set a high urgency...
        if(setHighUrgency && investigationUrgency == InvestigationUrgency.walk)
        {
            SetInvestigationUrgency(InvestigationUrgency.run);
        }

        //Cancel conversation
        if (human.inConversation)
        {
            human.currentConversation.EndConversation();
        }

        //Change location if hiding
        if(newTarget != null && newTarget.isPlayer && newTarget.isHiding)
        {
            if(newTarget.currentNode == newInvestigateNode)
            {
                if (!Player.Instance.spottedWhileHiding.Contains(human as Actor))
                {
                    newInvestigateNode = Toolbox.Instance.PickNearbyNode(newTarget.currentNode);
                    newInvestigatePosition = newInvestigateNode.position;
                }
            }
        }

        investigatePosition = newInvestigatePosition;

        //Don't investigate if I'm running away or raising the alarm...
        if (currentGoal != null && currentGoal.preset == RoutineControls.Instance.fleeGoal) return;

        //Would I be trespassing here?
        bool trespass = false;

        if (newInvestigateNode != null)
        {
            trespass = human.IsTrespassing(newInvestigateNode.room, out _, out _, true);
        }

        if (trespass)
        {
            //Don't investigate if this would be trespassing...
            if (newReactionState != ReactionState.persuing)
            {
                Game.Log("AI: " + human.name + " ("+human.job.preset.name+") would be trespassing by investigating " + newInvestigateNode.position + " (" + newInvestigateNode.room.gameLocation.name+ ") so doing nothing...");
                return;
            }
            //If I am persuing, a flee behaviour will trigger through tick ranking...
        }
        else
        {
            try
            {
                //Game.Log(human.name + " investigating " + newInvestigateNode.position + " state " + newReactionState.ToString());
            }
            catch
            {
                //Game.Log(human.name + " investigating");
            }
        }

        SetUpdateEnabled(true); //Make sure this is enabled for investigate

        //Set escalation
        //If investiate already exists, update combat post
        if (Game.Instance.collectDebugData) human.SelectedDebug("Set escalation: MAX " + human.escalationLevel + "/" + escalation, Actor.HumanDebug.sight);
        human.SetEscalation(Mathf.Max(human.escalationLevel, escalation));

        //If persuit target isn't set then set this one, unless it's null
        //if(persuitTarget == null && newTarget != null)
        //{
        //    SetPersueTarget(newTarget);
        //}

        //Create a projection of 2m ahead of where the target will be...
        if(persuitTarget != null)
        {
            if (!persuitTarget.isPlayer)
            {
                //For most actors, simply projecting infront of the transform should work...
                investigatePositionProjection = persuitTarget.transform.TransformPoint(Vector3.forward * 2);
            }
            else
            {
                //If this is the player however, we need to use the actual direction the player is travelling as they could be running backwards etc...
                //Take the movement occured this frame, normalize it and * 2
                investigatePositionProjection = persuitTarget.transform.position + (Player.Instance.fps.movementThisUpdate.normalized * 2);
            }
        }
        else
        {
            investigatePositionProjection = newInvestigatePosition;
        }

        if (investigateLocation != newInvestigateNode)
        {
            investigateLocation = newInvestigateNode;

            if (investigationGoal.isActive)
            {
                if (Game.Instance.collectDebugData) human.SelectedDebug("Investigation goal is active and a new different node is given: Refresh actions", Actor.HumanDebug.actions);

                investigationGoal.RefreshActions(true);

                //Update the move to and search area actions
                investigationGoal.AITick();
            }
        }
        //If search is active, but AI is searching, refresh all goals
        else if(investigationGoal.isActive)
        {
            if (currentAction != null && (/*persuit ||*/ currentAction.preset == RoutineControls.Instance.searchArea || currentAction.preset == RoutineControls.Instance.searchAreaEnforcer))
            {
                if (Game.Instance.collectDebugData) human.SelectedDebug("Investigation goal is active and AI was previously searching: Refresh actions", Actor.HumanDebug.actions);

                investigationGoal.RefreshActions(true);

                //Update the move to and search area actions
                investigationGoal.AITick();
            }
        }

        lastInvestigate = SessionData.Instance.gameTime;

        //Set the investigation object, if there is one
        investigateObject = newInvesigationObj;

        //Set the minimum investigation time multiplier 
        minimumInvestigationTimeMultiplier = Mathf.Max(minimumInvestigationTimeMultiplier, minimumInvestiationTimeMP);

        //Set reaction state
        if ((persuit || human.seesIllegal.Count > 0) && (newReactionState == ReactionState.investigatingSound))
        {
            return; //Don't override persuing or sighted state with heard state
        }
        else if(persuit && newReactionState == ReactionState.investigatingSight)
        {
            return; //Don't override persuing with sighted state
        }
        else
        {
            bool bark = false;

            if(newInvesigationObj != null)
            {
                //A bit of cheat here but to avoid the audio bark from triggering when the player is trespassing etc make it only happen if the player is not seen
                if(!trackedTargets.Exists(item => item.actor != null && item.actor.isPlayer))
                {
                    //Chance to do an object-specific bark
                    if(newReactionState != reactionState)
                    {
                        if (newReactionState == ReactionState.investigatingSound && Toolbox.Instance.Rand(0f, 1f) > 0.75f)
                        {
                            bark = true;

                            //Bark: Investigate/hears object
                            human.speechController.TriggerBark(SpeechController.Bark.hearsObject);
                        }
                    }
                }
            }

            if(!bark && newReactionState != reactionState)
            {
                bark = true;

                if(newTarget != null && newTarget.isPlayer && spookCounter > 0)
                {
                    human.speechController.TriggerBark(SpeechController.Bark.spookConfront);
                }
                else
                {
                    if(newReactionState == ReactionState.investigatingSound && reactionState != ReactionState.investigatingSight)
                    {
                        //Bark: Hears suspicious
                        human.speechController.TriggerBark(SpeechController.Bark.hearsSuspicious);
                    }
                    else if(newReactionState == ReactionState.investigatingSight)
                    {
                        //Bark: Sees suspicious
                        human.speechController.TriggerBark(SpeechController.Bark.seesSuspicious);
                    }
                }
            }

            SetReactionState(newReactionState);
        }
    }

    public void SetInvestigationUrgency(InvestigationUrgency newUrgency)
    {
        investigationUrgency = newUrgency;
    }

    //Set the AI to persue: They will persue until lost regardless of whether the target is doing wrong. If the bool is true, anybody in the same room will also react
    public void SetPersue(Actor newTarget, bool publicFauxPas, int escalation, bool setHighUrgency, float responseRange = 10f)
    {
        if(!human.isStunned && !human.isDead && !human.isAsleep)
        {
            SetPersuit(true);
            SetPersueTarget(newTarget);
            human.AddToSeesIllegal(newTarget, 1f);
            persuitChaseLogicUses = 1f;
            Investigate(newTarget.currentNode, newTarget.transform.position, newTarget, ReactionState.persuing, CitizenControls.Instance.persuitMinInvestigationTimeMP, escalation, setHighUrgency: setHighUrgency);
        }

        //Also alert everybody in the room
        if(publicFauxPas)
        {
            List<Actor> a = new List<Actor>(human.currentRoom.currentOccupants);

            foreach (Actor actor in a)
            {
                if (actor == human) continue; //Skip me
                if (actor.isStunned || actor.isDead || actor.isAsleep) continue;

                float dist = Vector3.Distance(actor.transform.position, human.transform.position);

                if(dist <= responseRange)
                {
                    if (actor.ai != null)
                    {
                        actor.ai.SetPersuit(true);
                        actor.ai.SetPersueTarget(newTarget);
                        actor.AddToSeesIllegal(newTarget, 1f);
                        actor.ai.persuitChaseLogicUses = 0.5f;
                        actor.ai.Investigate(newTarget.currentNode, newTarget.transform.position, newTarget, ReactionState.persuing, CitizenControls.Instance.persuitMinInvestigationTimeMP, escalation, setHighUrgency: setHighUrgency);
                    }
                }
            }
        }
    }

    public void SetPersueTarget(Actor newTarget)
    {
        if(persuitTarget != null)
        {
            persuitTarget.RemovePersuedBy(human);
        }

        persuitTarget = newTarget;

        if(persuitTarget != null)
        {
            persuitTarget.AddPersuedBy(human);
            chaseLogic.UpdateLastSeen();
        }
    }

    public void CancelPersue()
    {
        if (Game.Instance.collectDebugData) human.SelectedDebug("Cancelled persuit", Actor.HumanDebug.sight);
        SetPersueTarget(null);
        SetPersuit(false);
        SetSeesOnPersuit(false);
        persuitChaseLogicUses = 0;
        SetPersueTarget(null);
        persuitUpdateTimer = 0;
    }

    public void SetPersuit(bool val)
    {
        if (persuit != val)
        {
            persuit = val;
            if (Game.Instance.collectDebugData) human.SelectedDebug("Set persuit: " + persuit, Actor.HumanDebug.sight);
        }
    }

    public void SetSeesOnPersuit(bool val)
    {
        if(seesOnPersuit != val)
        {
            seesOnPersuit = val;
            if (Game.Instance.collectDebugData) human.SelectedDebug("Set seesOnPersuit: " + seesOnPersuit, Actor.HumanDebug.sight);
        }
    }

    //Cancel investigation: When this is reset, AI will no longer persue the player on-sight alone.
    public void ResetInvestigate()
    {
        if (Game.Instance.collectDebugData) human.SelectedDebug("AI: Reset Investigate", Actor.HumanDebug.actions);
        human.ClearSeesIllegal();
        investigateLocation = null;
        lastInvestigate = 0f;
        CancelPersue();
        minimumInvestigationTimeMultiplier = 1f;
        human.SetEscalation(0);
        SetInvestigationUrgency(InvestigationUrgency.walk);

        if(InteractionController.Instance.mugger == human)
        {
            Game.Log("Resetting mugger state");
            InteractionController.Instance.mugger = null;
        }

        if (InteractionController.Instance.debtCollector == human)
        {
            Game.Log("Resetting debt collector state");
            InteractionController.Instance.debtCollector = null;
        }

        //Set indicator to none
        SetReactionState(ReactionState.none);
    }

    //Set a new patrol
    public void Patrol(NewGameLocation newPatLoc)
    {
        patrolLocation = newPatLoc;

        if (patrolGoal == null)
        {
            patrolGoal = CreateNewGoal(RoutineControls.Instance.patrolGoal, 0f, 0f);
        }
    }

    //Trigger an attack
    public void StartAttack(Actor newAttackTarget)
    {
        if(newAttackTarget != null && Game.Instance.collectDebugData) human.SelectedDebug(human.name + " Start attack: " + newAttackTarget.name, Actor.HumanDebug.attacks);

        if (attackActive || throwActive)
        {
            if (Game.Instance.collectDebugData) human.SelectedDebug("attack already active", Actor.HumanDebug.attacks);
            return; //Return if already active
        }

        if(restrained)
        {
            return;
        }

        if (attackDelay > 0f)
        {
            if (Game.Instance.collectDebugData) human.SelectedDebug("attack delay: " + attackDelay, Actor.HumanDebug.attacks);
            return; //Don't attack if delay is active
        }

        //Atack is governed by animation state: Only start an attack if arms are in the combat pose animation
        //AnimatorStateInfo anim = human.animationController.mainAnimator.GetCurrentAnimatorStateInfo(0);

        //if(!anim.IsTag("CombatPose"))
        //{
        //    human.SelectedDebug("not in combat pose");
        //    return;
        //}

        if(currentWeaponPreset.type != MurderWeaponPreset.WeaponType.handgun && currentWeaponPreset.type != MurderWeaponPreset.WeaponType.shotgun && currentWeaponPreset.type != MurderWeaponPreset.WeaponType.rifle)
        {
            //human.AddBreath(-0.05f / human.recoveryRate); //Remove breath

            if (outOfBreath)
            {
                if (Game.Instance.collectDebugData) human.SelectedDebug("out of breath", Actor.HumanDebug.movement);
                return; //Return if already active
            }
        }


        //Make sure previous attack bar is removed
        if (activeAttackBar != null && ((attackTarget != null && attackTarget.isPlayer) || (newAttackTarget != null && newAttackTarget.isPlayer)))
        {
            Destroy(activeAttackBar.gameObject);
        }

        human.speechController.TriggerBark(SpeechController.Bark.attack);

        attackActive = true;
        damageColliderCreated = false;
        ejectBrassCreated = false;
        attackProgress = 0f;
        attackActiveLength = 0f;
        attackTarget = newAttackTarget;

        Game.Log(human.name + ": TRIGGER ATTACK");

        //Detect if animation controller has been culled as damage happens as the result of an animation event
        human.animationController.AttackTrigger(); //Start animation
    }

    //Trigger throwing an object
    public void ThrowObject(Actor newAttackTarget)
    {
        //Game.Log("THROW OBJECT TRGGER...");

        if (currentGoal != null && currentGoal.preset.disableThrowing) return;

        if (newAttackTarget != null && Game.Instance.collectDebugData)  human.SelectedDebug(human.name + " Start throw: " + newAttackTarget.name, Actor.HumanDebug.attacks);

        if (outOfBreath)
        {
            if (Game.Instance.collectDebugData) human.SelectedDebug("out of breath", Actor.HumanDebug.movement);
            return; //Return if already active
        }

        if (restrained)
        {
            return;
        }

        if (attackActive || throwActive)
        {
            if (Game.Instance.collectDebugData) human.SelectedDebug("attack already active", Actor.HumanDebug.attacks);
            return; //Return if already active
        }

        if (attackDelay > 0f || throwDelay > 0f)
        {
            if (Game.Instance.collectDebugData) human.SelectedDebug("attack delay: " + attackDelay, Actor.HumanDebug.attacks);
            return; //Don't attack if delay is active
        }

        throwItem = null;

        if(human.currentConsumables.Count > 0)
        {
            throwItem = human.currentConsumables[0];
        }
        else if(human.trash.Count > 0)
        {
            MetaObject meta = CityData.Instance.FindMetaObject(human.trash[0]);
            if(meta != null) throwItem = meta.GetPreset();
        }

        if(throwItem != null)
        {
            Game.Log("THROW OBJECT " + throwItem.name);

            //Spawn new items
            if (spawnedRightItem == null)
            {
                Vector3 rightPos = throwItem.aiHeldObjectPosition;
                Vector3 rightEuler = throwItem.aiHeldObjectRotation;

                usingCarryAnimation = throwItem.requiredCarryAnimation;
                int carryAnim = throwItem.aiCarryAnimation;

                spawnedRightItem = Toolbox.Instance.SpawnObject(throwItem.prefab, human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.HandRight));
                spawnedRightItem.transform.localPosition = rightPos;
                spawnedRightItem.transform.localEulerAngles = rightEuler;

                Collider[] remColl = spawnedRightItem.GetComponentsInChildren<Collider>(true);

                for (int i = 0; i < remColl.Length; i++)
                {
                    Destroy(remColl[i]);
                }

                human.AddMesh(spawnedRightItem);
            }

            attackActive = true;
            damageColliderCreated = false;
            ejectBrassCreated = false;
            attackProgress = 0f;
            attackActiveLength = 0f;
            attackTarget = newAttackTarget;
            throwActive = true;

            human.animationController.ThrowTrigger(); //Start animation
        }
    }

    //Triggered when attack is finished
    public void OnAttackComplete()
    {
        if (Game.Instance.collectDebugData) human.SelectedDebug("Complete attack", Actor.HumanDebug.attacks);

        if (activeAttackBar != null) activeAttackBar.removeHit = true; //Play remove by hit animation

        //Normal attack delay
        SetAttackDelay();

        EndAttack();
    }

    //Triggered if an attack is blocked by the player
    public void OnAttackBlock(bool perfect = false)
    {
        if (activeAttackBar != null)
        {
            activeAttackBar.removeBlocked = true; //Play remove by block animation
            activeAttackBar.abortProgress = attackProgress;
        }

        Game.Log("Player: " + human.name + " blocked successfully! Perfect: " + perfect);

        SetAttackDelay(true, perfect);

        human.animationController.BlockTrigger(attackDelay, perfect);

        EndAttack();
    }

    //Abort an attack
    public void OnAbortAttack()
    {
        if (Game.Instance.collectDebugData) human.SelectedDebug("Abort attack", Actor.HumanDebug.attacks);

        if (activeAttackBar != null)
        {
            activeAttackBar.removeAbort = true; //Play remove by hit animation
            activeAttackBar.abortProgress = attackProgress;
        }

        //Normal attack delay
        SetAttackDelay();

        //Animation
        human.animationController.AbortAttackTrigger();

        EndAttack();
    }

    //Set an attack delay based on combat skill
    private void SetAttackDelay(bool blocked = false, bool blockedPerfect = false)
    {
        if(combatMode <= 0 && blocked)
        {
            if (blockedPerfect)
            {
                attackDelay = Mathf.Lerp(GameplayControls.Instance.perfectBlockAttackDelay.x, GameplayControls.Instance.perfectBlockAttackDelay.y, human.combatSkill);
            }
            else
            {
                attackDelay = Mathf.Lerp(GameplayControls.Instance.blockedAttackDelay.x, GameplayControls.Instance.blockedAttackDelay.y, human.combatSkill);
            }

            //Set physical delay stopping movement
            SetDelayed(attackDelay);
        }
        else
        {
            //Normal attack delay
            attackDelay = weaponRefire;
        }
    }

    //Remove an attack
    public void EndAttack()
    {
        if (Game.Instance.collectDebugData) human.SelectedDebug("Ended attack", Actor.HumanDebug.attacks);

        if (damageCollider != null)
        {
            Destroy(damageCollider.gameObject);
        }

        attackActive = false;
        throwActive = false;
        damageColliderCreated = false;
        ejectBrassCreated = false;
        attackProgress = 0f;
        attackActiveLength = 0f;
        attackTimeout = 0;

        if(!inCombat)
        {
            if(human.animationController != null)
            {
                if(human.animationController.mainAnimator != null)
                {
                    human.animationController.mainAnimator.cullingMode = AnimatorCullingMode.CullCompletely;
                }
            }
        }
    }

    //Talk to
    public void TalkTo(InteractionController.ConversationType convoType = InteractionController.ConversationType.normal)
    {
        if (Player.Instance.playerKOInProgress)
        {
            Game.Log("TalkTo player KO is in progress so igoring...");
            return; //If already executed, ignore
        }

        if (human.isDead || human.isAsleep || human.isStunned)
        {
            Game.Log("TalkTo candidate is dead/asleep/stunned...");
            return;
        }

        if(!restrained)
        {
            if (inFleeState)
            {
                Game.Log("TalkTo candidate is in flee state...");
                return;
            }
            else if (reactionState != ReactionState.none)
            {
                Game.Log("TalkTo candidate is in reaction state " + reactionState);
                return;
            }
        }

        //Drop carried item
        if(InteractionController.Instance.carryingObject != null)
        {
            InteractionController.Instance.carryingObject.DropThis(false);
        }

        Game.Log("Setting TalkTo");

        human.SetInteracting(Player.Instance.interactable);
        Player.Instance.SetInteracting(human.interactable);

        if(convoType == InteractionController.ConversationType.mugging)
        {
            InteractionController.Instance.mugger = human;
        }
        else if(convoType == InteractionController.ConversationType.loanSharkVisit)
        {
            InteractionController.Instance.debtCollector = human;
        }

        //Set as locked in interaction
        InteractionController.Instance.SetLockedInInteractionMode(human.interactable, 1);

        Player.Instance.TransformPlayerController(GameplayControls.Instance.citizenTalkToTransition, null, human.interactable, human.lookAtThisTransform, false);

        //Load dialog options
        InteractionController.Instance.SetDialog(true, human.interactable, newConvoType: convoType);

        //Listen for return from locked in 
        InteractionController.Instance.OnReturnFromLockedIn += OnReturnFromTalkTo;
    }

    public void OnReturnFromTalkTo()
    {
        //Trigger fights
        if(InteractionController.Instance.dialogType == InteractionController.ConversationType.mugging)
        {
            if(InteractionController.Instance.mugger != null)
            {
                Game.Log("Setting mugging combat persuit...");
                InteractionController.Instance.mugger.ai.SetPersue(Player.Instance, false, 2, true);
            }
        }
        else if(InteractionController.Instance.dialogType == InteractionController.ConversationType.loanSharkVisit)
        {
            if (InteractionController.Instance.debtCollector != null)
            {
                Game.Log("Setting debt collector combat persuit...");
                InteractionController.Instance.debtCollector.ai.SetPersue(Player.Instance, false, 2, true);
            }
        }

        human.SetInteracting(null);

        InteractionController.Instance.SetDialog(false, null);

        AITick();
        SetUpdateEnabled(true); //Enable check on returning from talk to

        //Cancel arrest action
        if (currentAction != null)
        {
            currentAction.OnDeactivate(executeDeactivateAnimation: false);
        }

        InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromTalkTo;
        InteractionController.Instance.SetLockedInInteractionMode(null);
        Player.Instance.ReturnFromTransform();
    }

    //Set stunned for these many seconds
    public void SetStunned(bool val)
    {
        Game.Log("AI: Set Stunned " + val);

        human.isStunned = val;
    }

    //Set delayed for these many seconds
    public void SetDelayed(float seconds)
    {
        human.isDelayed = true;
        delayedUntil = SessionData.Instance.gameTime + (seconds * 0.0167f);
        if(currentAction != null) currentAction.estimatedArrival = -1; //Reset estimated time of arrival
        //Game.Log("Set delayed! Until " + delayedUntil);
    }

    //Answer door
    public void AnswerDoor(NewDoor dc, NewGameLocation where, Actor byWho)
    {
        if (human.isDead || dc == null) return;

        //Answer doors that need answering...
        //Get the inside node
        NewNode insideNode = dc.wall.node;
        if (dc.wall.otherWall.node.gameLocation == where) insideNode = dc.wall.otherWall.node;

        //Is there an existing goal with this already? If not, create one...
        if(!goals.Exists(item => item.preset == RoutineControls.Instance.answerDoorGoal && item.passedInteractable == dc.handleInteractable))
        {
            Game.Log("Debug: " + human.name + " add answer door event");
            NewAIGoal newAnswerGoal = CreateNewGoal(RoutineControls.Instance.answerDoorGoal, 0f, 0f, insideNode, dc.handleInteractable);
            AITick(true);

            if(byWho != null && byWho.isPlayer)
            {
                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "Somebody is coming to answer the door..."));
            }
        }
        //Already exists
        else if (SessionData.Instance.gameTime - human.speechController.lastSpeech > 0.0167f)
        {
            //Bark: Answer the door
            human.speechController.TriggerBark(SpeechController.Bark.answeringDoor);
            human.ai.AITick(true);
        }
    }

    //Answer phone
    public void AnswerPhone(Telephone where)
    {
        if (human.isDead || human.isStunned || inCombat) return;
        if (goals.Exists(item => item.preset == RoutineControls.Instance.mourn)) return; //Don't do if mourning

        //Is there an existing goal with this already? If not, create one...
        if(currentGoal != null && (investigationGoal == null || !investigationGoal.isActive))
        {
            if(currentAction == null || !currentGoal.actions.Exists(item => item.preset == RoutineControls.Instance.answerTelephone))
            {
                Interactable nearestPhone = Toolbox.Instance.FindNearestWithAction(RoutineControls.Instance.answerTelephone, human.currentRoom, human, AIActionPreset.FindSetting.nonTrespassing, restrictTo: where.location, enforcersAllowedEverywhere: false);

                if(nearestPhone != null)
                {
                    human.SetInConversation(null, true);

                    Game.Log("Phone: Sending " + human.name + " to answer phone at " + where.location.name);
                    CreateNewAction(currentGoal, RoutineControls.Instance.answerTelephone, true, newPassedInteractable: nearestPhone, newInsertedActionPriority: 9);
                    AITick();
                }
            }
        }
    }

    //Sleep prompt (step towards waking up)
    public void AwakenPrompt()
    {
        if(human.awakenPromt < 2)
        {
            human.awakenPromt++;

            #if UNITY_EDITOR
            if (UnityEditor.Selection.Contains(this.gameObject))
            {
                Game.Log("Awaken promt++ (" + human.awakenPromt + ")");
            }
            #endif
        }

        //Remove sleep depth
        human.sleepDepth -= 0.12f;
        human.sleepDepth = Mathf.Clamp01(human.sleepDepth);

        //Wake up at this point
        if(human.awakenPromt >= 2)
        {
            human.WakeUp();
        }
        //Snort
        else if(human.isAsleep)
        {
            if(human.genderScale >= 0.5f)
            {
                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.maleSnort, human, human.currentNode, human.lookAtThisTransform.position);
            }
            else
            {
                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.femaleSnort, human, human.currentNode, human.lookAtThisTransform.position);
            }
        }
    }

    [Button]
    public void DisplayCurrentRoute()
    {
        if(currentAction != null)
        {
            if(currentAction.path != null)
            {
                for (int i = 0; i < currentAction.path.accessList.Count; i++)
                {
                    NewNode.NodeAccess access = currentAction.path.accessList[i];
                    Debug.DrawRay(access.fromNode.position, access.toNode.position - access.fromNode.position, Color.magenta, 5f);
                }
            }
        }
    }

    public void SetInCombat(bool val, bool forceUpdate = false)
    {
        if(inCombat != val || forceUpdate)
        {
            inCombat = val;

            if (inCombat)
            {
                human.SetEscalation(2);
                RecalculateWeaponStats();
                throwDelay = Toolbox.Instance.Rand(2f, 4f); //Initial throw delay

                human.AddNerve(-0.1f); //Automatically lose nerve from entering combat
            }
            else
            {
                human.AddNerve(0.1f); //Automatically gain nerve from exiting combat
            }

            human.animationController.SetInCombat(inCombat);
            if (Game.Instance.collectDebugData) human.SelectedDebug(human.name + " in combat: " + val, Actor.HumanDebug.actions);
            UpdateHeldItems(AIActionPreset.ActionStateFlag.none);
        }
    }

    public void RecalculateWeaponStats()
    {
        if(currentWeaponPreset != null)
        {
            weaponRangeMax = currentWeaponPreset.GetAttackValue(MurderWeaponPreset.AttackValue.range, human);
            weaponRefire = currentWeaponPreset.GetAttackValue(MurderWeaponPreset.AttackValue.fireDelay, human);
            weaponAccuracy = currentWeaponPreset.GetAttackValue(MurderWeaponPreset.AttackValue.accuracy, human);
            weaponDamage = currentWeaponPreset.GetAttackValue(MurderWeaponPreset.AttackValue.damage, human);
        }
    }

    public void SetKO(bool val, Vector3 impactPoint = new Vector3(), Vector3 impactDirection = new Vector3(), bool forced = false, float forcedDuration = 0f, bool resetInvesigate = true, float forceMultiplier = 1f)
    {
        Game.Log("AI: Set KO " + val);

        ko = val;
        SetStunned(ko);
        SetUpdateEnabled(true); //Make sure this is enabled to deal with KO

        TriggerReactionIndicator();

        //Set stunned: Only do this on KO. For reverse it must occur at the end of the get-up transition
        if (ko)
        {
            //Reset investigate
            if (resetInvesigate)
            {
                ResetInvestigate();
            }

            //End conversation
            if (human.inConversation) human.currentConversation.EndConversation();

            if (attackActive)
            {
                Game.Log(human.name + " Abort attack: KO");
                OnAbortAttack();
            }

            //Clear queued actions
            queuedActions.Clear();

            //Reset to maximum health
            if (!forced)
            {
                human.ResetHealthToMaximum();
                koTime = SessionData.Instance.gameTime + (Toolbox.Instance.Rand(GameplayControls.Instance.koTimeRange.x, GameplayControls.Instance.koTimeRange.y) * (1f + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.KOTimeModifier)));
                Game.Log(human.name + " Set KO time: " + koTime);
            }
            else
            {
                koTime = SessionData.Instance.gameTime + forcedDuration;
                Game.Log(human.name + " Set KO time: " + koTime);
            }

            koTransitionTimer = CitizenControls.Instance.ragdollTransitionTime; //Transition from ragdoll to get up animation
            getUpDelayTimer = CitizenControls.Instance.getUpTimer;

            //Disable animators
            human.animationController.SetPauseAnimation(true);
            human.animationController.SetRagdoll(true);

            //Apply punch force
            //Find the closest rigidbody to the impact point
            Rigidbody closestRB = human.animationController.upperTorsoRB;
            float distance = Mathf.Infinity;

            foreach(Rigidbody rb in human.animationController.createdRBs)
            {
                float dist = Vector3.Distance(rb.transform.position, impactPoint);

                if(dist < distance)
                {
                    closestRB = rb;
                    distance = dist;
                }
            }

            //Now apply force
            closestRB.AddForceAtPosition(impactDirection.normalized * GameplayControls.Instance.playerKOPunchForce * forceMultiplier, impactPoint);
        }
        else
        {
            if(resetInvesigate) human.speechController.TriggerBark(SpeechController.Bark.dazed);

            //Remove alarm targets
            foreach(NewBuilding b in CityData.Instance.buildingDirectory)
            {
                b.alarmTargets.Remove(human);
            }
        }

        if (human.interactable != null)
        {
            human.interactable.UpdateCurrentActions();

            //Update interactions
            if (InteractionController.Instance.currentLookingAtInteractable != null && InteractionController.Instance.currentLookingAtInteractable.interactable == human.interactable)
            {
                InteractionController.Instance.UpdateInteractionText();
            }
        }
    }

    //Set the citizen out of breath
    public void SetOutOfBreath(bool val)
    {
        if(outOfBreath != val)
        {
            outOfBreath = val;
            human.animationController.mainAnimator.SetBool("outOfBreath", outOfBreath);

            if(outOfBreath)
            {
                if(Toolbox.Instance.Rand(0f, 1f) > 0.5f)
                {
                    human.speechController.TriggerBark(SpeechController.Bark.outOfBreath);
                }
            }
        }
    }

    //Set the citizen to restrained
    public void SetRestrained(bool val, float duration)
    {
        if(val != restrained)
        {
            restrained = val;
            Game.Log("AI: Set restrained " + human.name + ": " + restrained);

            if(restrained)
            {
                //Undo KO
                if(ko && !human.isDead)
                {
                    koTime = 0;
                }

                human.speechController.TriggerBark(SpeechController.Bark.restrained);

                human.animationController.SetInBed(false);
                human.animationController.SetUmbrella(false);

                Game.Log(human.name + " Abort attack: Restrained");
                OnAbortAttack();

                SetFacingDirection(Player.Instance.transform.forward);
                SetExpression(CitizenOutfitController.Expression.angry);
                human.animationController.mainAnimator.SetTrigger("restrain");

                restrainTime = SessionData.Instance.gameTime + duration;

                MovementSpeedUpdate(); //Run this to make sure capsule collider is smaller (to avoid blocking player in hallways)
            }
            else
            {
                restrainTime = 0f;
            }

            human.animationController.SetRestrained(restrained);

            //Trigger any case objectives...
            foreach(Case c in CasePanelController.Instance.activeCases)
            {
                foreach(Case.ResolveQuestion q in c.resolveQuestions)
                {
                    if(q.inputType == Case.InputType.arrestPurp)
                    {
                        if (c.caseType == Case.CaseType.murder || c.caseType == Case.CaseType.mainStory)
                        {
                            MurderController.Instance.UpdateCorrectResolveAnswers();
                        }
                        else if (c.caseType == Case.CaseType.sideJob && c.job != null)
                        {
                            c.job.UpdateResolveAnswers();
                        }
                    }
                }
            }
        }
    }

    //Set reaction state
    public void SetReactionState(ReactionState newState)
    {
        if (human.isDead) newState = ReactionState.none;

        if(newState != reactionState)
        {
            reactionState = newState;

            if (Game.Instance.collectDebugData) human.SelectedDebug("New reaction state: " + newState, Actor.HumanDebug.updates);
        }

        TriggerReactionIndicator();

        ////Create indicator
        //if (reactionState != ReactionState.none || investigationGoal.priority > 0f)
        //{
        //    TriggerReactionIndicator();
        //}
        ////Otherwise remove
        //else if (reactionIndicator != null)
        //{
        //    //Remove indicator
        //    reactionIndicator.removeFade = true;
        //}
    }

    //Trigger a sighted display that uses the indicator system (or change its icon)
    public void TriggerReactionIndicator()
    {
        if(reactionIndicator != null) reactionIndicator.removeFade = false;

        //The following are allowed if not spotted by the player while doing something illegal...
        if(human.spottedState <= 0f)
        {
            //Remove under these conditions

            //Persuing someone else
            if (reactionState == ReactionState.persuing && (persuitTarget == null || persuitTarget != Player.Instance))
            {
                if (reactionIndicator != null) reactionIndicator.removeFade = true;
                return;
            }

            //Investigating sound of someone else
            if(reactionState == ReactionState.investigatingSound && (hearTarget == null || !hearTarget.isPlayer))
            {
                if (reactionIndicator != null) reactionIndicator.removeFade = true;
                return;
            }

            //Investigating sight of someone else
            if (reactionState == ReactionState.investigatingSight && (persuitTarget == null || !persuitTarget.isPlayer))
            {
                if (reactionIndicator != null) reactionIndicator.removeFade = true;
                return;
            }

            //Not investigating or KO'd
            if ((reactionState == ReactionState.none && investigationGoal.priority <= 0f) || ko)
            {
                if (reactionIndicator != null) reactionIndicator.removeFade = true;
                return;
            }

            //Searching non-escalated
            if ((reactionState == ReactionState.none || reactionState == ReactionState.searching) && human.escalationLevel <= 1)
            {
                if (reactionIndicator != null) reactionIndicator.removeFade = true;
                return;
            }

            //Escalated non player
            if(human.escalationLevel >= 2 && persuitTarget != Player.Instance && hearTarget != Player.Instance)
            {
                if (reactionIndicator != null) reactionIndicator.removeFade = true;
                return;
            }

            if (reactionIndicator != null)
            {
                reactionIndicator.UpdateReactionType();
                return;
            }
        }

        if (reactionIndicator != null)
        {
            reactionIndicator.UpdateReactionType();
        }
        else
        {
            Game.Log("Interface: Create reaction icon for " + human.name);
            GameObject newSpeech = Instantiate(PrefabControls.Instance.aiReactionIndicator, InterfaceControls.Instance.speechBubbleParent);
            reactionIndicator = newSpeech.GetComponent<ReactionIndicatorController>();
            reactionIndicator.Setup(human);

            //Create outline
            if(human.outline != null)
            {
                human.outline.SetOutlineActive(true);
            }
        }

        //Setup awareness
        if (reactionIndicator.awarenessIcon == null || !reactionIndicator.awarenessIcon.setup)
        {
            Game.Log("Interface: Create awareness icon for " + human.name);
            reactionIndicator.awarenessIcon = InterfaceController.Instance.AddAwarenessIcon(InterfaceController.AwarenessType.actor, InterfaceController.AwarenessBehaviour.invisibleInfront, human, null, Vector3.zero, InterfaceControls.Instance.spotted, 10, true, InterfaceControls.Instance.maxIndicatorDistance);

            //Update the reaction indicator type by running this
            reactionIndicator.UpdateReactionType();
        }
        else
        {
            //If this is in the process of removal, then stop
            if (reactionIndicator.awarenessIcon.removalFlag)
            {
                reactionIndicator.awarenessIcon.removalFlag = false;
                reactionIndicator.awarenessIcon.removalProgress = 0f;
            }
        }

        if(reactionIndicator.awarenessIcon != null)
        {
            if (reactionState == NewAIController.ReactionState.investigatingSight || reactionState == NewAIController.ReactionState.investigatingSound || reactionState == ReactionState.persuing)
            {
                reactionIndicator.awarenessIcon.TriggerAlert();
            }
        }
    }

    //Keep a console of set destination positions...
    public void DebugDestinationPosition(string input)
    {
        if (!Game.Instance.devMode || !Game.Instance.collectDebugData) return;

        if(debugDestinationPosition.Count > 50)
        {
            debugDestinationPosition.RemoveAt(0);
        }

        debugDestinationPosition.Add(input);
    }

    //Cancel combat
    public void CancelCombat()
    {
        if (Game.Instance.collectDebugData) human.SelectedDebug("Cancel combat", Actor.HumanDebug.actions);
        human.ClearSeesIllegal();
        ResetInvestigate();
    }

    //Set as victim
    public void SetAsVictim(MurderController.Murder newMurder)
    {
        if(!victimsForMurders.Contains(newMurder))
        {
            if (Game.Instance.collectDebugData) Game.Log("Murder: Add victim allowance: " + human.name);
            victimsForMurders.Add(newMurder);
        }
    }

    //Set as murderer
    public void SetAsMurderer(MurderController.Murder newMurderer)
    {
        if (!killerForMurders.Contains(newMurderer))
        {
            Game.Log("Murder: Add killer allowance: " + human.name);
            killerForMurders.Add(newMurderer);
        }
    }

    public void SetStaticFromAnimation(bool val)
    {
        if(val != staticFromAnimation)
        {
            staticFromAnimation = val;
            if (Game.Instance.collectDebugData) human.SelectedDebug("Set static from animation: " + staticFromAnimation, Actor.HumanDebug.movement);
        }

        if (staticFromAnimation)
        {
            staticAnimationSafetyTimer = 2.5f;
        }
    }

    [Button]
    public void GetRotationState()
    {
        Game.Log("Character rotation is " + transform.rotation + " (" + transform.eulerAngles + ")");
        Game.Log("Desired rotation is " + facingQuat + " (" + facingQuat.eulerAngles + ")");
        Game.Log("Rotation status is: " + facingActive);
        Game.Log("This enabled: " + this.enabled);
    }

    public void CloseDoorsNormallyAfterLeavingGamelocation(NewGameLocation afterLeaving)
    {
        closeDoorsNormallyAfterLeaving = afterLeaving;
    }

    public void UpdateCurrentWeapon()
    {
        Interactable bestWeapon = null;

        foreach (Interactable i in human.inventory)
        {
            if (i == null || i.preset == null) continue;

            if (i.preset.weapon != null)
            {
                if (i.preset.weapon.usedInPersonalDefence)
                {
                    if ((bestWeapon == null || bestWeapon.preset.weapon == null) || i.preset.weapon.basePriority > bestWeapon.preset.weapon.basePriority)
                    {
                        bestWeapon = i;
                    }
                }

                bool murderWeap = false;

                foreach(MurderController.Murder m in killerForMurders)
                {
                    if(m.state == MurderController.MurderState.executing || m.state == MurderController.MurderState.post || m.state == MurderController.MurderState.escaping)
                    {
                        if(m.murdererID == human.humanID)
                        {
                            if (m.weaponPreset == i.preset)
                            {
                                bestWeapon = i;
                                murderWeap = true;
                                break;
                            }
                        }
                    }
                }

                if (murderWeap) break;
            }
        }

        SetCurrentWeapon(bestWeapon);
    }

    public void SetCurrentWeapon(Interactable obj)
    {
        if(obj != currentWeapon)
        {
            if(obj != null && obj.preset.weapon != null)
            {
                currentWeapon = obj;
                currentWeaponPreset = obj.preset.weapon;
            }
            else
            {
                currentWeapon = null;
                currentWeaponPreset = InteriorControls.Instance.fistsWeapon;
            }

            if (Game.Instance.collectDebugData) human.SelectedDebug("Set current weapon to " + currentWeaponPreset.name, Actor.HumanDebug.attacks);

            RecalculateWeaponStats();

            if(currentWeaponPreset.type == MurderWeaponPreset.WeaponType.fists)
            {
                combatMode = 0;
            }
            else if(currentWeaponPreset.type == MurderWeaponPreset.WeaponType.handgun)
            {
                combatMode = 1;
            }
            else if(currentWeaponPreset.type == MurderWeaponPreset.WeaponType.rifle)
            {
                combatMode = 2;
            }
            else if(currentWeaponPreset.type == MurderWeaponPreset.WeaponType.shotgun)
            {
                combatMode = 3;
            }
            else if(currentWeaponPreset.type == MurderWeaponPreset.WeaponType.blade || currentWeaponPreset.type == MurderWeaponPreset.WeaponType.poison || currentWeaponPreset.type == MurderWeaponPreset.WeaponType.bluntObject || currentWeaponPreset.type == MurderWeaponPreset.WeaponType.strangulation)
            {
                combatMode = 4;
            }
            else
            {
                combatMode = 0;
            }

            UpdateHeldItems(AIActionPreset.ActionStateFlag.none);
        }

        human.animationController.SetCombatArmsOverride(combatMode);
    }

    public void UpdateHeldItems(AIActionPreset.ActionStateFlag state)
    {
        GameObject desiredRight = null;
        Vector3 rightPos = Vector3.zero;
        Vector3 rightEuler = Vector3.zero;

        GameObject desiredLeft = null;
        Vector3 leftPos = Vector3.zero;
        Vector3 leftEuler = Vector3.zero;

        usingCarryAnimation = false;
        int carryAnim = 0;

        //First priority is weapons, spawned if in a combat state
        if(inCombat)
        {
            if (currentWeaponPreset.itemRightOverride != null)
            {
                desiredRight = currentWeaponPreset.itemRightOverride;
                rightPos = currentWeaponPreset.itemRightLocalPos;
                rightEuler = currentWeaponPreset.itemRightLocalEuler;

                carryAnim = currentWeaponPreset.overrideCarryAnimation;
                usingCarryAnimation = currentWeaponPreset.overideUsesCarryAnimation;
            }
            else desiredRight = null;

            if (currentWeaponPreset.itemLeftOverride != null)
            {
                desiredLeft = currentWeaponPreset.itemLeftOverride;
                leftPos = currentWeaponPreset.itemLeftLocalPos;
                leftEuler = currentWeaponPreset.itemLeftLocalEuler;

                carryAnim = currentWeaponPreset.overrideCarryAnimation;
                usingCarryAnimation = currentWeaponPreset.overideUsesCarryAnimation;
            }
            else
            {
                desiredLeft = null;
            }
        }
        //Current action does not allow any items...
        else if(currentAction != null && !currentAction.preset.allowItems)
        {
            desiredRight = null;
            desiredLeft = null;
            customItemSource = null;
        }
        else
        {
            //Second priority is action-created items...
            //Is there an existing source for items...
            if(customItemSource != null)
            {
                //Check for removal
                if(customItemSource.preset.destroyCustomItemOn == state)
                {
                    if(state == AIActionPreset.ActionStateFlag.onDeactivation && (!customItemSource.isActive || currentAction != customItemSource))
                    {
                        customItemSource = null;
                    }
                    else if(state == AIActionPreset.ActionStateFlag.onGoalDeactivation && (!customItemSource.goal.isActive || currentGoal != customItemSource.goal))
                    {
                        customItemSource = null;
                    }
                }
            }

            //There is no existing custom item...
            if(customItemSource == null)
            {
                //Create new custom item
                if (currentAction != null && currentAction.preset.enableCustomItem && currentAction.preset.spawnCustomItemOn == state)
                {
                    desiredLeft = currentAction.preset.itemLeft;
                    leftPos = currentAction.preset.itemLeftLocalPos;
                    leftEuler = currentAction.preset.itemLeftLocalEuler;

                    usingCarryAnimation = currentAction.preset.requiresCarryAnimation;
                    carryAnim = currentAction.preset.overrideCarryAnimation;

                    desiredRight = currentAction.preset.itemRight;
                    rightPos = currentAction.preset.itemRightLocalPos;
                    rightEuler = currentAction.preset.itemRightLocalEuler;

                    customItemSource = currentAction;
                }
                else
                {
                    bool invCarry = false;

                    if (human.inventory.Count > 0)
                    {
                        foreach(Interactable i in human.inventory)
                        {
                            if(i.preset.inventoryCarryItem)
                            {
                                desiredLeft = null;

                                desiredRight = i.preset.prefab;
                                rightPos = i.preset.aiHeldObjectPosition;
                                rightEuler = i.preset.aiHeldObjectRotation;

                                usingCarryAnimation = i.preset.requiredCarryAnimation;
                                carryAnim = i.preset.aiCarryAnimation;

                                invCarry = true;
                                break;
                            }
                        }
                    }

                    if (!invCarry)
                    {
                        //Third priority is inventory/consumables
                        if (human.currentConsumables.Count > 0)
                        {
                            desiredLeft = null;

                            desiredRight = human.currentConsumables[0].prefab;
                            rightPos = human.currentConsumables[0].aiHeldObjectPosition;
                            rightEuler = human.currentConsumables[0].aiHeldObjectRotation;

                            usingCarryAnimation = human.currentConsumables[0].requiredCarryAnimation;
                            carryAnim = human.currentConsumables[0].aiCarryAnimation;
                        }
                        else
                        {
                            desiredLeft = null;
                            desiredRight = null;
                        }
                    }
                }
            }
        }

        //Perform switch
        bool newItem = false;

        //Remove existing
        if(customItemSource == null)
        {
            if (spawnedRightItem != null && (desiredRight == null || desiredRight.name != spawnedRightItem.name))
            {
                DespawnRightItem();
                newItem = true;
            }

            if (spawnedLeftItem != null && (desiredLeft == null || desiredLeft.name != spawnedLeftItem.name))
            {
                DespawnLeftItem();
                newItem = true;
            }
        }

        //Spawn new items
        if(desiredRight != null)
        {
            spawnedRightItem = Toolbox.Instance.SpawnObject(desiredRight, human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.HandRight));
            spawnedRightItem.transform.localPosition = rightPos;
            spawnedRightItem.transform.localEulerAngles = rightEuler;

            Collider[] remColl = spawnedRightItem.GetComponentsInChildren<Collider>(true);

            for (int i = 0; i < remColl.Length; i++)
            {
                Destroy(remColl[i]);
            }

            LODGroup lodG = spawnedRightItem.GetComponentInChildren<LODGroup>(true);

            if (lodG != null)
            {
                lodG.SetLODs(new LOD[] { });
                Destroy(lodG);
            }

            InteractableController ic = spawnedRightItem.GetComponentInChildren<InteractableController>(true);
            if (ic != null) Destroy(ic);

            human.AddMesh(spawnedRightItem);
            newItem = true;
        }

        if (desiredLeft != null)
        {
            spawnedLeftItem = Toolbox.Instance.SpawnObject(desiredLeft, human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.HandLeft));
            spawnedLeftItem.transform.localPosition = leftPos;
            spawnedLeftItem.transform.localEulerAngles = leftEuler;

            Collider[] remColl = spawnedLeftItem.GetComponentsInChildren<Collider>(true);

            for (int i = 0; i < remColl.Length; i++)
            {
                Destroy(remColl[i]);
            }

            LODGroup lodG = spawnedLeftItem.GetComponentInChildren<LODGroup>(true);

            if (lodG != null)
            {
                lodG.SetLODs(new LOD[] { });
                Destroy(lodG);
            }

            InteractableController ic = spawnedLeftItem.GetComponentInChildren<InteractableController>(true);
            if (ic != null) Destroy(ic);

            human.AddMesh(spawnedLeftItem);
            newItem = true;
        }

        //Set arms animations...
        if (newItem)
        {
            if (usingCarryAnimation)
            {
                //Set arms weight...
                human.animationController.SetCarryItemType(carryAnim);
                human.animationController.SetCarryingItem(true);

                if (human.animationController.armsBoolAnimationState == CitizenAnimationController.ArmsBoolSate.none)
                {
                    human.animationController.armsLayerDesiredWeight = 1f;
                    if (Game.Instance.collectDebugData) human.SelectedDebug("Set animator arms weight: 1 ("+ human.animationController.armsLayerDesiredWeight+")", Actor.HumanDebug.misc);
                }
            }
            else
            {
                //Set arms weight...
                human.animationController.SetCarryingItem(false);
                human.animationController.SetCarryItemType(0);
                human.animationController.SetArmsBoolState(CitizenAnimationController.ArmsBoolSate.none);
            }
        }
    }

    public void DespawnRightItem()
    {
        if (spawnedRightItem != null)
        {
            Toolbox.Instance.DestroyObject(spawnedRightItem);
            human.UpdateMeshList();
        }
    }

    public void DespawnLeftItem()
    {
        if (spawnedLeftItem != null)
        {
            Toolbox.Instance.DestroyObject(spawnedLeftItem);
            human.UpdateMeshList();
        }
    }

    //If target is close enough then immediately persue
    public void InstantPersuitCheck(Actor target)
    {
        if (target.isPlayer && Game.Instance.invisiblePlayer) return;

        if(!human.isStunned && !human.isDead && !target.isStunned && !target.isDead)
        {
            if (target.illegalStatus || persuitTarget == target || (human.currentBuilding != null && target.isPlayer && human.currentBuilding.wantedInBuilding > SessionData.Instance.gameTime && human.locationsOfAuthority.Contains(Player.Instance.currentGameLocation)))
            {
                target.UpdateTrespassing(false);

                if(!target.isTrespassing || target.trespassingEscalation >= 2)
                {
                    if(human.ActorRaycastCheck(target, 3f, out _, false, Color.green, Color.red, Color.white))
                    {
                        Game.Log(human.name+ " set " + target+ " instant persuit!");
                        SetPersue(target, false, 1, true);
                    }
                }
            }
        }
    }

    //Toggle AI
    public void EnableAI(bool val)
    {
        if(val)
        {
            if(!CitizenBehaviour.Instance.updateList.Contains(this))
            {
                CitizenBehaviour.Instance.updateList.Add(this);
            }

            SetDesiredTickRate(AITickRate.high, true);
            SetUpdateEnabled(true);
        }
        else
        {
            SetPersuit(false);
            ResetInvestigate();
            CancelCombat();

            if (currentAction != null) currentAction.Remove();
            if (currentGoal != null) currentGoal.Remove();

            CitizenBehaviour.Instance.updateList.Remove(this);
            CitizenBehaviour.Instance.veryHighTickRate.Remove(this);
            CitizenBehaviour.Instance.highTickRate.Remove(this);
            CitizenBehaviour.Instance.mediumTickRate.Remove(this);
            CitizenBehaviour.Instance.lowTickRate.Remove(this);
            CitizenBehaviour.Instance.veryLowTickRate.Remove(this);

            //Clear queued actions
            queuedActions.Clear();
            SetReactionState(NewAIController.ReactionState.none);
            if(!isRagdoll) SetUpdateEnabled(false);
        }
    }

    //Confine to location
    public void SetConfineLocation(NewGameLocation newConfine)
    {
        confineLocation = newConfine;
    }

    //Add to avoid locations
    public void AddAvoidLocation(NewGameLocation newAvoid)
    {
        if(avoidLocations.Contains(newAvoid))
        {
            avoidLocations.Add(newAvoid);
        }
    }

    public void RemoveAvoidLocation(NewGameLocation remAvoid)
    {
        avoidLocations.Remove(remAvoid);
    }

    //Check if I am confined to a location, and if so then return it, if not return input
    public NewGameLocation CheckConfinedLocation(NewGameLocation desired)
    {
        //Confine AI to location...
        if (confineLocation != null && confineLocation != desired)
        {
            if (Game.Instance.collectDebugData)
            {
                human.SelectedDebug("AI " + human.name + " is confined to location " + confineLocation.name + ", removing action for " + desired.name + "...", Actor.HumanDebug.actions);
                DebugDestinationPosition("AI " + human.name + " is confined to location " + confineLocation.name + ", removing action for " + desired.name + "...");
            }

            return confineLocation;
        }

        if (avoidLocations != null && avoidLocations.Contains(desired))
        {
            if (Game.Instance.collectDebugData)
            {
                human.SelectedDebug("AI " + human.name + " is avoiding location " + desired.name + ", removing action for " + desired.name + "...", Actor.HumanDebug.actions);
                DebugDestinationPosition("AI " + human.name + " is avoiding location " + desired.name + ", removing action for " + desired.name + "...");
            }

            return null;
        }

        if (victimsForMurders.Count > 0)
        {
            foreach (MurderController.Murder m in victimsForMurders)
            {
                if (m.location == null) continue;
                if ((int)m.state < 4) continue;

                if (m.preset.blockVictimFromLeavingLocation)
                {
                    if (m.location != desired)
                    {
                        Game.Log("Murder: AI " + human.name + " is confined to location " + m.location.name + ", removing action for " + desired.name + "...");
                        human.SelectedDebug("AI " + human.name + " is confined to location " + m.location.name + ", removing action for " + desired.name + "...", Actor.HumanDebug.actions);
                        DebugDestinationPosition("AI " + human.name + " is confined to location " + m.location.name + ", removing action for " + desired.name + "...");
                        return m.location;
                    }
                }
            }
        }

        return desired;
    }

    public void AddSpooked(float val)
    {
        spooked += val;
        spooked = Mathf.Clamp01(spooked);

        //if(val > 0) Game.Log("Add spooked to " + human.name + ": " + val + ", total: " + spooked);
    }

    [Button]
    public void IsTrespassingAtActionDestination()
    {
        if(currentAction != null)
        {
            string trespassingDebug = string.Empty;
            bool tp = human.IsTrespassing(currentAction.goal.passedInteractable.node.room, out _, out trespassingDebug);

            Game.Log("Trespassing at (goal) " + currentAction.goal.passedInteractable.node.room.name + ": " + tp + " " + trespassingDebug);

            tp = human.IsTrespassing(currentAction.node.room, out _, out trespassingDebug);

            Game.Log("Trespassing at (action) " + currentAction.node.room.name + ": " + tp + " " + trespassingDebug);
        }
    }

    [Button]
    public void CurrentGoalTriggerTime()
    {
        if(currentGoal != null)
        {
            Game.Log(currentGoal.triggerTime + " = " + SessionData.Instance.GameTimeToClock24String(currentGoal.triggerTime, true) + ", " + SessionData.Instance.ShortDateString(currentGoal.triggerTime, true));
        }
    }

    [Button]
    public void ForceNodeReached()
    {
        ReachNewPathNode("Force Node Reached"); //Update of game location is contained within this
    }

    [Button]
    public void DestinationCheck()
    {
        if(currentAction != null && !currentAction.isAtLocation)
        {
            currentAction.DestinationCheck("Force check");
        }
    }

    [Button]
    public void OpenEvidenceFirstName()
    {
        InterfaceController.Instance.SpawnWindow(human.evidenceEntry, Evidence.DataKey.firstName);
    }

    [Button]
    public void OpenEvidenceName()
    {
        InterfaceController.Instance.SpawnWindow(human.evidenceEntry, Evidence.DataKey.name);
    }

    [Button]
    public void OpenEvidencePhoto()
    {
        InterfaceController.Instance.SpawnWindow(human.evidenceEntry, Evidence.DataKey.photo);
    }

    [Button]
    public void ToggleHumanDebug()
    {
        if (Game.Instance.debugHuman.Contains(human))
        {
            Game.Log("Toggle AI debug for " + name + ": false");
            Game.Instance.debugHuman.Remove(human);
        }
        else
        {
            Game.Log("Toggle AI debug for " + name + ": true");
            Game.Instance.debugHuman.Add(human);
        }
    }
}
