﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using System;
using UnityEngine.EventSystems;
using System.Linq;
using NaughtyAttributes;

//Controller for a window.
//Script pass 1
public class InfoWindow : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Header("Presets")]
    public WindowStylePreset preset;

    //[Header("Naming")]
    //public string name;

    [Header("References")]
    public Canvas windowCanvas;
    public CanvasGroup windowCanvasGroup;
    public Canvas contentConvas;
    public CanvasGroup contentCanvasGroup;

    public RectTransform background;
    public TextMeshProUGUI titleText;
    //public TMP_InputField titleInput;
    public RectTransform rect;
    private ResizePanel[] resizeZones;
    public CustomScrollRect scrollRect;
    public GameObject tabBar;
    public ButtonController closeButton;
    public PinFolderButtonController pinButton;
    public ItemController item;
    public WindowContentController activeContent;
    public RectTransform contentRect;
    private Scrollbar horzScrollBar;
    private Scrollbar vertScrollBar;
    public RectTransform activeTabRect;
    public RectTransform pageRect;
    public Image typeIcon;
    public Image closeButtonIcon;
    public RectTransform dragZone;
    public RectTransform controllerSelect;
    public JuiceController controllerSelectJuice;
    public ControllerViewRectScroll controllerScrollView;
    public RectTransform interactionIconRect;

    public ButtonController clearTextButton;
    public ButtonController takeItemButton;

    [Header("Parameters")]
    public bool closable = true;
    public bool pinnable = true;
    public bool pinned = false;
    public bool selected = false;
    [NonSerialized]
    public Case.CaseElement currentPinnedCaseElement = null;
    [NonSerialized]
    public Case.CaseElement forcedPinnedCaseElement = null;
    public bool isOver = false;
    public bool isWorldInteraction = false;
    private bool updateNav = false;
    public bool forceDisablePin = false; //Used by tutorial sometimes to disable closing of the window
    public bool forceDisableClose = false; //Used by tutorial sometimes to disable closing of the window
    public bool dialogSuccess = false; //Used if this is opened by dialog to dictate a successful choice

    [Header("Graphics")]
    public Sprite iconLarge = null;
    public InterfaceControls.EvidenceColours evColour = InterfaceControls.EvidenceColours.red;
    public Image pinOverlay;
    public Image pinColour;
    public Image pinColourPressed;
    public Color pinColourActual;

    //Colours
    public Color baseColour = Color.white;
    public Color flashColour = Color.cyan;
    public Color borderColour = Color.white;

    [Header("Resizing")]
    public bool resizable = true;
	public Vector2 defaultSize = new Vector2(514, 658);
    public float centringTollerance = 16f;

    [Header("Content")]
    public Evidence passedEvidence;
    public List<Evidence.DataKey> passedKeys = new List<Evidence.DataKey>(); //The original keys used to open this window
    public List<Evidence.DataKey> evidenceKeys = new List<Evidence.DataKey>(); //Updated tied keys

    public List<WindowContentController> contentPages = new List<WindowContentController>();
    public List<WindowTabController> tabs = new List<WindowTabController>();
    //public List<WindowButtonController> buttons = new List<WindowButtonController>();
    [System.NonSerialized]
    public Interactable passedInteractable;
    [System.NonSerialized]
    public Case passedCase;

    [Header("Debug")]
    public Evidence.DataKey debugKeyOne;
    public Evidence.DataKey debugKeyTwo;
    public Vector2 debugSetAnchoredPosition;

    //Events
    //Triggered when the window is resized
    public delegate void ResizedWindow();
    public event ResizedWindow OnResizedWindow;

    //Triggered when the window is closed
    public delegate void WindowClosed();
    public event WindowClosed OnWindowClosed;

    //Triggered when the window is refreshed
    public delegate void WindowRefresh();
    public event WindowRefresh OnWindowRefresh;

    //Triggered when window's evidence keys are updated
    public delegate void WorldInteractionStateUpdate();
    public event WorldInteractionStateUpdate OnUpdateWorldInteractionState;

    //Use this for initialization. Setup can be run multiple times to completely refresh the content.
    public void Setup(WindowStylePreset newPreset, Evidence newEv, List<Evidence.DataKey> newKeys, bool worldInteraction = false, Interactable newInteractable = null, Case newCase = null, Case.CaseElement newForcePinnedCaseElement = null, bool passedDialogSuccess = true)
    {
        if (newEv != null) Game.Log("Interface: Spawn window for " + newEv.name + " (World interaction: " + worldInteraction + ", interactable: " + newInteractable + ")");

        //Set passed data
        preset = newPreset;
        passedEvidence = newEv;
        passedKeys = newKeys;
        passedInteractable = newInteractable;
        passedCase = newCase;
        forcedPinnedCaseElement = newForcePinnedCaseElement;

        dialogSuccess = passedDialogSuccess;

        UpdateEvidenceKeys();

        //Get rect transform
        if(rect == null) rect = GetComponent<RectTransform>();

        //Get resize zones
        resizeZones = GetComponentsInChildren<ResizePanel>();

        //Get scroll bars
        horzScrollBar = scrollRect.horizontalScrollbar;
        vertScrollBar = scrollRect.verticalScrollbar;

        closable = preset.closable;
        pinnable = preset.pinnable;
        resizable = preset.resizable;
        
        defaultSize = preset.defaultSize;

        //Size based on evidence summary page dimensions...
        if(passedEvidence != null)
        {
            DDSSaveClasses.DDSTreeSave foundTree = null;

            string ddsTree = passedEvidence.preset.ddsDocumentID;
            if (passedEvidence.overrideDDS != null && passedEvidence.overrideDDS.Length > 0) ddsTree = passedEvidence.overrideDDS;

            //Get DDS content
            if (Toolbox.Instance.allDDSTrees.TryGetValue(ddsTree, out foundTree))
            {
                if(foundTree.treeType == DDSSaveClasses.TreeType.vmail)
                {
                    defaultSize = Toolbox.Instance.allDDSTrees["04f9b47f-e525-4aef-82b2-3cffe52cc061"].document.size + preset.DDSadditionalSize;
                }
                else defaultSize = foundTree.document.size + preset.DDSadditionalSize;
            }

            passedEvidence.OnDataKeyChange += UpdateEvidenceKeys;
        }
        else
        {
            SetName(Strings.Get("ui.interface", preset.name)); //Set static name
        }

        iconLarge = preset.overrideIcon;
        SetWorldInteraction(worldInteraction);

        RectTransform scroll = scrollRect.GetComponent<RectTransform>();

        //Enable scrolling with both mouse buttons
        //scrollRect.leftMouseScroll = true;

        //Add item controller
        if (passedEvidence != null)
        {
            //Initialise
            if (item == null)
            {
                //Add item controller
                item = this.gameObject.AddComponent<ItemController>();

                //Set up the window as a draggable component
                DragPanel dp = this.gameObject.GetComponentInChildren<DragPanel>();
                dp.draggableComponent = true;
                dp.dragTag = passedEvidence.preset.name;
            }

            //Setup item
            item.Setup(this);

            //Spawn evidence icon
            if(passedEvidence != null)
            {
                typeIcon.sprite = passedEvidence.GetIcon();

                //GameObject newObj = Instantiate(PrefabControls.Instance.caseFolderClassIconButton, hierarchyIconRect);
                //RectTransform buttonRect = newObj.GetComponent<RectTransform>();
                //buttonRect.anchoredPosition = new Vector2(-2, 0f);

                //ClassIconButtonController cibc = newObj.GetComponent<ClassIconButtonController>();
                //cibc.Setup(passedEvidence, this, null);
            }
        }

        //Remove existing content
        foreach(WindowTabController wtc in tabs)
        {
            Destroy(wtc.gameObject);
        }

        tabs.Clear();

        //Load content pages
        foreach (WindowTabPreset tabPreset in preset.tabs)
        {
            //Disable various types of tabs according to evidence
            if(passedEvidence != null)
            {
                if (!passedEvidence.preset.enableFacts && tabPreset.contentType == WindowTabPreset.TabContentType.facts) continue;
                if (!passedEvidence.preset.enableSummary && tabPreset.contentType == WindowTabPreset.TabContentType.generated) continue;
            }

            LoadTab(tabPreset);
        }

        //Remember active content
        if(preset.name == "ApartmentDecor")
        {
            WindowContentController foundWcc = contentPages.Find(item => item.tabController.preset.contentType == PlayerApartmentController.Instance.rememberContent);

            if(foundWcc != null)
            {
                SetActiveContent(foundWcc);
            }
            else SetActiveContent(contentPages[0]);
        }
        else
        {
            //Set content as first entry
            SetActiveContent(contentPages[0]);
        }

        //Set window size
        ResizeWindow(defaultSize);

        //If resizing is disabled, destroy the zones. This is done instead of creating as almost all windows will be resizable.
        if (!resizable)
        {
            foreach (ResizePanel rp in resizeZones)
            {
                rp.gameObject.GetComponent<Button>().interactable = false;
                Destroy(rp);
            }
        }

        //Position tabs
        UpdateTabButtons();

        //Set closable
        SetClosable(closable);

        //If pinning is is disabled, remove the button.
        if (!pinnable)
        {
            Destroy(pinButton.gameObject);
        }

        //If world interaction, force centre of screen
        if (worldInteraction)
        {
            rect.anchoredPosition = new Vector2(InterfaceControls.Instance.hudCanvasRect.rect.width * 0.7f - rect.sizeDelta.x * 0.5f, -InterfaceControls.Instance.hudCanvasRect.rect.height * 0.5f + rect.sizeDelta.y * 0.5f);
        }

        //Play restore sound
        if ((passedEvidence as EvidenceStickyNote) != null)
        {
            AudioController.Instance.Play2DSound(AudioControls.Instance.stickyOpen);
        }
        else
        {
            AudioController.Instance.Play2DSound(AudioControls.Instance.folderOpen);
        }

        //Check to see if this is pinned...
        PinnedUpdateCheck();

        //Update controller navigation
        UpdateControllerNavigationEndOfFrame();

        InteractionController.Instance.UpdateInteractionText();
    }

    private void OnDestroy()
    {
        PopupMessageController.Instance.OnRightButton -= OnEditName;

        if (passedEvidence != null)
        {
            //Listen for update of unseen facts
            WindowTabController tabController = tabs.Find(item => item.preset.contentType == WindowTabPreset.TabContentType.facts);
            if(tabController != null) item.OnUpdateUnseenFacts -= tabController.SetNewItems;

            passedEvidence.OnDataKeyChange -= UpdateEvidenceKeys;
        }
    }

    public void SetWorldInteraction(bool val)
    {
        //Force world interaction mode
        if(passedEvidence != null && passedEvidence.preset.forceWorldInteraction)
        {
            val = true;
        }
        else if(preset.forceWorldInteraction)
        {
            val = true;
        }

        isWorldInteraction = val;
        interactionIconRect.gameObject.SetActive(isWorldInteraction);

        Game.Log("Interface: Set world interaction for " + name + ": " + isWorldInteraction);

        RefreshTakeButton();

        if(isWorldInteraction)
        {
            if (passedEvidence != null && passedEvidence.preset.useWindowFocusMode)
            {
                InterfaceController.Instance.ShowWindowFocus();
            }
            else if(preset.useWindowFocusMode)
            {
                InterfaceController.Instance.ShowWindowFocus();
            }
        }
        else
        {
            InterfaceController.Instance.RemoveWindowFocus();
        }

        //Fire event
        if (OnUpdateWorldInteractionState != null)
        {
            OnUpdateWorldInteractionState();
        }
    }

    public void RefreshTakeButton()
    {
        if (isWorldInteraction && passedInteractable != null && passedInteractable.inInventory != Player.Instance && passedInteractable.preset.GetActions().Exists(item => item.action == RoutineControls.Instance.takeFirstPersonItem))
        {
            takeItemButton.gameObject.SetActive(true);

            takeItemButton.icon.color = InterfaceControls.Instance.windowTakeItemIconDefaultColor;
            InteractablePreset.InteractionAction action = passedInteractable.preset.GetActions().Find(item => item.action == RoutineControls.Instance.takeFirstPersonItem);

            //Is this illegal?
            if(action != null)
            {
                //Ilegal
                if (action.actionIsIllegal)
                {
                    if (InteractionController.Instance.GetValidPlayerActionIllegal(passedInteractable, Player.Instance.currentNode))
                    {
                        takeItemButton.icon.color = InterfaceControls.Instance.interactionControlTextColourIllegal;
                    }
                }
            }

        }
        else takeItemButton.gameObject.SetActive(false);
    }

    public void CancelWorldInteractionButton()
    {
        if(isWorldInteraction)
        {
            SetWorldInteraction(false);
        }
    }

    //Load a tab
    public void LoadTab(WindowTabPreset tabPreset)
    {
        Game.Log("Interface: Load tab " + tabPreset.name);

        //Create a tab button
        GameObject newTab = Instantiate(PrefabControls.Instance.tabButton, tabBar.transform);
        WindowTabController tabController = newTab.GetComponent<WindowTabController>();
        tabController.tabButton = newTab.GetComponent<Button>();
        tabController.preset = tabPreset;
        tabs.Add(tabController);

        if(tabPreset.contentPrefab == null)
        {
            Game.Log("Interface: Content does not exist for tab " + tabPreset.name);
        }

        //Create content page
        GameObject newContentPage = Instantiate(tabPreset.contentPrefab, scrollRect.viewport.transform);
        newContentPage.transform.SetAsFirstSibling();
        newContentPage.name = tabPreset.tabName;

        WindowContentController newWcc = newContentPage.GetComponent<WindowContentController>();
        newWcc.tabButton = newTab.GetComponent<Button>();
        tabController.content = newWcc;
        newWcc.tabController = tabController;
        newWcc.window = this;
        tabController.content = newWcc;

        if (tabPreset.contentType == WindowTabPreset.TabContentType.facts)
        {
            item.factContent = newWcc;

            //Listen for update of unseen facts
            item.OnUpdateUnseenFacts += tabController.SetNewItems;

            item.UpdateFactsDisplay();
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.help)
        {
            HelpController help = newContentPage.gameObject.GetComponentInChildren<HelpController>();
            help.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.resolve)
        {
            ResolveController resolve = newContentPage.gameObject.GetComponentInChildren<ResolveController>();
            resolve.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.caseOptions)
        {
            ResolveOptionsController resolve = newContentPage.gameObject.GetComponentInChildren<ResolveOptionsController>();
            resolve.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.results)
        {
            ResultsController results = newContentPage.gameObject.GetComponentInChildren<ResultsController>();
            results.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.decor)
        {
            DecorController decor = newContentPage.gameObject.GetComponentInChildren<DecorController>();
            decor.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.furnishings)
        {
            FurnishingsController furn = newContentPage.gameObject.GetComponentInChildren<FurnishingsController>();
            furn.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.items)
        {
            ApartmentItemsController items = newContentPage.gameObject.GetComponentInChildren<ApartmentItemsController>();
            items.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.colourPicker)
        {
            ColourPickerController cp = newContentPage.gameObject.GetComponentInChildren<ColourPickerController>();
            cp.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.materialKey)
        {
            MaterialKeyController cp = newContentPage.gameObject.GetComponentInChildren<MaterialKeyController>();
            cp.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.history)
        {
            HistoryController history = newContentPage.gameObject.GetComponentInChildren<HistoryController>();
            history.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.passcodes)
        {
            PasscodesController passcodes = newContentPage.gameObject.GetComponentInChildren<PasscodesController>();
            passcodes.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.phoneNumbers)
        {
            PhoneNumbersController phoneNumbers = newContentPage.gameObject.GetComponentInChildren<PhoneNumbersController>();
            phoneNumbers.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.photoSelect)
        {
            PhotoSelectController select = newContentPage.gameObject.GetComponentInChildren<PhotoSelectController>();
            select.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.itemSelect)
        {
            ItemSelectController select = newContentPage.gameObject.GetComponentInChildren<ItemSelectController>();
            select.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.shop)
        {
            BuyInterfaceController select = newContentPage.gameObject.GetComponentInChildren<BuyInterfaceController>();
            select.Setup(newWcc);
        }
        else if (tabPreset.contentType == WindowTabPreset.TabContentType.objectives)
        {
            ObjectivesContentController select = newContentPage.gameObject.GetComponentInChildren<ObjectivesContentController>();
            select.Setup(newWcc);
        }
        else if(tabPreset.contentType == WindowTabPreset.TabContentType.callLogsIncoming)
        {
            //CallLogsContentController select = newContentPage.gameObject.GetComponentInChildren<CallLogsContentController>();
        }
        else if(tabPreset.contentType == WindowTabPreset.TabContentType.callLogsOutgoing)
        {
            //CallLogsContentController select = newContentPage.gameObject.GetComponentInChildren<CallLogsContentController>();
        }

        //Add Zoomable Content
        if (tabPreset.scalableContent)
        {
            ZoomContent zoom = newContentPage.AddComponent<ZoomContent>();
            zoom.enableZoomWithMouseWheel = tabPreset.zoomWithMouseWheel;

            newWcc.zoomController = zoom;

            //Set always centred if not zoomable with mouse wheel
            if (!tabPreset.zoomWithMouseWheel)
            {
                newWcc.SetAlwaysCentred(true);
            }
        }

        //Load proc content into tab
        if (passedEvidence != null && tabPreset.contentType == WindowTabPreset.TabContentType.generated)
        {
            newWcc.LoadContent();
        }

        //Setup button click
        tabController.SetupButton();
        newTab.GetComponent<Button>().onClick.AddListener(delegate { SetActiveContent(newWcc); });

        contentPages.Add(newWcc);
        newContentPage.SetActive(false);
    }

    //Triggered on resize of window
    public void OnResizeWindow()
    {
        //Update tabs
        UpdateTabButtons();

        //Update content best fit
        foreach (WindowContentController wcc in contentPages)
        {
            wcc.centred = false;

            if(Mathf.Abs(wcc.rect.anchoredPosition.x) + Mathf.Abs(wcc.rect.anchoredPosition.y) < centringTollerance)
            {
                if(wcc.zoomController != null)
                {
                    if(wcc.zoomController.zoom <= wcc.fitScale + 0.01f && wcc.zoomController.zoom >= wcc.fitScale - 0.01f)
                    {
                        wcc.centred = true;
                    }
                }
            }

            wcc.UpdateFitScale();

            if (wcc.centred || wcc.alwaysCentred) wcc.CentrePage();
        }

        //Update loaded icons
        if(item != null)
        {
            Toolbox.Instance.UpdateButtonListPositions(item.spawnedChildEvButtons);
            item.PositionSpawnedFacts();
        }

        //Update any progress bars
        ProgressBarController[] bars = this.gameObject.GetComponentsInChildren<ProgressBarController>();

        foreach(ProgressBarController bar in bars)
        {
            bar.VisualUpdate();
        }

        //Fire event
        if (OnResizedWindow != null) OnResizedWindow();
    }

    public void UpdateTabButtons()
    {
        float spacePerButton = tabBar.GetComponent<RectTransform>().rect.height / contentPages.Count;

        float xRoom = 0f;
        float overlap = 16f;
        float textMargin = 34f;

        for (int i = 0; i < contentPages.Count; i++)
        {
            RectTransform buttonRect = contentPages[i].tabButton.GetComponent<RectTransform>();

            buttonRect.sizeDelta = new Vector2(buttonRect.sizeDelta.x, spacePerButton + overlap);
            buttonRect.anchoredPosition = new Vector2(0f, xRoom);
            xRoom -= spacePerButton;

            //Set text box size
            RectTransform textRect = contentPages[i].tabController.text.gameObject.GetComponent<RectTransform>();
            textRect.sizeDelta = new Vector2(buttonRect.sizeDelta.y - textMargin, textRect.sizeDelta.y);
            textRect.anchoredPosition = new Vector2(textRect.anchoredPosition.x, textMargin * 0.5f);
        }

        foreach(WindowTabController tab in tabs)
        {
            tab.VisualUpdate();
        }
    }

    public void SetActiveContent(WindowContentController wcc)
    {
        Game.Log("Interface: Set active content: " + wcc.name);

        if (contentPages.Contains(wcc))
        {
            //Set all inactive
            foreach (WindowContentController cc in contentPages)
            {
                //Reset zoom/centre page
                cc.CentrePage();

                cc.gameObject.SetActive(false);

                //Set all tabs to inactive parent
                cc.tabButton.transform.SetParent(tabBar.transform);
                if (wcc.tabController.pulsateController != null && wcc.tabController.newItems > 0) wcc.tabController.pulsateController.enabled = true;
            }

            //Enable/disable mouse wheel scrolling depending on whether zooming in active
            if(wcc.tabController.preset.zoomWithMouseWheel)
            {
                //Set mouse wheel scrolling to 0
                scrollRect.scrollSensitivity = 0;
            }
            else
            {
                scrollRect.scrollSensitivity = GameplayControls.Instance.mouseWheelEvidenceScrollSensitivity;
            }

            wcc.gameObject.SetActive(true);
            wcc.tabButton.transform.SetParent(activeTabRect);

            if (wcc.tabController.pulsateController != null) wcc.tabController.pulsateController.enabled = false;

            wcc.tabButton.transform.SetAsLastSibling();

            activeContent = wcc;
            contentRect = wcc.gameObject.GetComponent<RectTransform>();

            scrollRect.content = contentRect;

            scrollRect.horizontal = wcc.tabController.preset.scrollBars;
            scrollRect.vertical = wcc.tabController.preset.scrollBars;

            //Enable/Disable scroll
            if (!wcc.tabController.preset.scrollBars)
            {
                if(scrollRect.horizontalScrollbar != null) scrollRect.horizontalScrollbar.gameObject.SetActive(false);
                scrollRect.horizontalScrollbar = null;
            }
            else
            {
                scrollRect.horizontalScrollbar = horzScrollBar;
                scrollRect.horizontalScrollbar.gameObject.SetActive(true);
            }

            if (!wcc.tabController.preset.scrollBars)
            {
                if (scrollRect.verticalScrollbar != null) scrollRect.verticalScrollbar.gameObject.SetActive(false);
                scrollRect.verticalScrollbar = null;
            }
            else
            {
                scrollRect.verticalScrollbar = vertScrollBar;
                scrollRect.verticalScrollbar.gameObject.SetActive(true);
            }

            //Set restrictions
            scrollRect.movementType = wcc.tabController.preset.scrollRestrcition;

            if(wcc.tabController.preset.contentType == WindowTabPreset.TabContentType.decor)
            {
                PlayerApartmentController.Instance.rememberContent = WindowTabPreset.TabContentType.decor;
            }
            else if(wcc.tabController.preset.contentType == WindowTabPreset.TabContentType.furnishings)
            {
                PlayerApartmentController.Instance.rememberContent = WindowTabPreset.TabContentType.furnishings;
            }
            else if (wcc.tabController.preset.contentType == WindowTabPreset.TabContentType.items)
            {
                PlayerApartmentController.Instance.rememberContent = WindowTabPreset.TabContentType.items;
            }

            //Force on window resize
            OnResizeWindow();

            //Update controller navigation
            UpdateControllerNavigationEndOfFrame();
        }
    }

    //Called after the window instance update by the instance update routine
    public void InstanceUpdateComplete()
    {
        if (item != null)
        {
            //item.UpdateChildEvidenceDisplay();
            item.UpdateFactsDisplay();
            item.UpdateNameDisplay();
        }

        //Fire event
        if (OnWindowRefresh != null) OnWindowRefresh();
    }

    public void UpdateEvidenceKeys()
    {
        if (passedEvidence == null) return;

        evidenceKeys = passedEvidence.GetTiedKeys(passedKeys);

        if(evidenceKeys != null && passedKeys != null) Game.Log("Interface: Update evidence keys for " + name + ": " + evidenceKeys.Count + " keys found using " + passedKeys.Count + " passed keys.");

        //Update seen keys & new info icon
        if (currentPinnedCaseElement != null)
        {
            currentPinnedCaseElement.sdk = new List<Evidence.DataKey>(evidenceKeys);
            if (currentPinnedCaseElement.pinnedController != null) currentPinnedCaseElement.pinnedController.UpdateNewInfoIcon();
        }

        //Set icon
        iconLarge = passedEvidence.GetIcon();
    }

    //Set name
    public void SetName(string newName)
	{
        Game.Log("Interface: Set window name: " + newName);
		name = newName.Trim();

        titleText.text = name;

		this.transform.name = name;

        //Adjust characters to try and fit name in
        RectTransform titleRect = titleText.gameObject.GetComponent<RectTransform>();

        if(titleText.preferredWidth >= titleRect.rect.width)
        {
            titleText.characterSpacing = -0.8f;
            titleText.wordSpacing = -0.8f;
        }
        else
        {
            titleText.characterSpacing = 0f;
            titleText.wordSpacing = 0f;
        }

        //foreach(WindowButtonController wbc in buttons)
        //{
        //    wbc.VisualUpdate();
        //}
	}

    public void ResizeWindow(Vector2 sizeDelta)
	{
		if (rect == null) rect = GetComponent<RectTransform>();

		rect.sizeDelta = new Vector2(Mathf.Clamp(defaultSize.x, preset.minSize.x, preset.maxSize.x), Mathf.Clamp(defaultSize.y, preset.minSize.y, preset.maxSize.y));

        Game.Log("Interface: Resize window " + name + " default " + sizeDelta + " = clamped: " + rect.sizeDelta);

        OnResizeWindow();
	}

    //Close window- this will override the closable bool, but not close if pinned
    public void CloseWindow(bool animate = true)
    {
        if (forceDisableClose) return;

        Game.Log("Interface: CloseWindow: " + name + " animate: " + animate + " pinned: " + pinned + " caseElement: " + currentPinnedCaseElement);

        //Remove from active windows
        InterfaceController.Instance.activeWindows.Remove(this);

        bool minimizing = false;

        //If pinned, animate to case element
        if (pinned && currentPinnedCaseElement != null)
        {
            minimizing = true;

            //Play minimize sound
            AudioController.Instance.Play2DSound(AudioControls.Instance.minimiseButton);

            if ((passedEvidence as EvidenceStickyNote) != null)
            {
                AudioController.Instance.Play2DSound(AudioControls.Instance.stickyClose);
                AudioController.Instance.Play2DSound(AudioControls.Instance.stickyNotePutDown);
            }
            else
            {
                AudioController.Instance.Play2DSound(AudioControls.Instance.folderClose);
                AudioController.Instance.Play2DSound(AudioControls.Instance.folderPutDown);
            }

            //Minimize window...
            //Now save window position for restoring
            //Get the canvas rect
            RectTransform itemCanvas = this.windowCanvas.GetComponent<RectTransform>();

            //Record the before positional stats so we can restore to this
            currentPinnedCaseElement.resPos = itemCanvas.position;
            currentPinnedCaseElement.resPiv = itemCanvas.pivot;

            Game.Log("Interface: Close window " + name + " (minimize to corkboard)");

            //Play minimize animation
            if (animate)
            {
                InterfaceController.Instance.MinimizeWindow(this);
            }
            else minimizing = false;
        }
        else
        {
            AudioController.Instance.Play2DSound(AudioControls.Instance.folderClose);
        }

        //If world interaction, unpause game
        if (isWorldInteraction && !PopupMessageController.Instance.active)
        {
            SessionData.Instance.ResumeGame();
        }

        //Fire event
        if (OnWindowClosed != null)
        {
            OnWindowClosed();
        }

        //Change selection mode
        if(CasePanelController.Instance.controllerMode)
        {
            if(CasePanelController.Instance.currentSelectMode == CasePanelController.ControllerSelectMode.windows)
            {
                if(InterfaceController.Instance.activeWindows.Count <= 0)
                {
                    CasePanelController.Instance.SetControllerMode(true, CasePanelController.ControllerSelectMode.caseBoard);
                }
            }
        }

        InteractionController.Instance.UpdateInteractionText();

        //Destroy, otherwise this is done after minimize...
        if(!minimizing && (currentPinnedCaseElement == null || !animate))
        {
            Game.Log("Interface: Close window " + name + " (destroy immediately)");
            Destroy(this.gameObject);
        }
    }

    //Toggle pinned
    public void TogglePinned()
    {
        if (!pinnable) return;
        if (CasePanelController.Instance.activeCase == null) return;
        if (item == null) return;

        //Add to tray
        if(!pinned)
        {
            //Get offset position to drag from...
            Vector2 pinPos = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(CasePanelController.Instance.pinnedContainer, Input.mousePosition, null, out pinPos);

            //Add distance to pin pivot
            pinPos.y -= 80f;

            CasePanelController.Instance.PinToCasePanel(CasePanelController.Instance.activeCase, passedEvidence, evidenceKeys, localPostion: pinPos);
        }
        else
        {
            CasePanelController.Instance.UnPinFromCasePanel(CasePanelController.Instance.activeCase, passedEvidence, evidenceKeys, true, forcedPinnedCaseElement);

            //Play unpin sound
            AudioController.Instance.Play2DSound(AudioControls.Instance.unPin);
        }
    }

    //Find if this is pinned? Run on spawn and on case changes...
    public void PinnedUpdateCheck()
    {
        if (CasePanelController.Instance.activeCase != null && passedEvidence != null)
        {
            UpdateEvidenceKeys();

            //Check the current case element against the new one...
            List<Case.CaseElement> matchingEv = CasePanelController.Instance.activeCase.caseElements.FindAll(item => passedEvidence != null && item.id == passedEvidence.evID);
            Case.CaseElement matching = null;

            foreach (Case.CaseElement el in matchingEv)
            {
                if(passedEvidence.preset.useDataKeys)
                {
                    foreach (Evidence.DataKey k in el.dk)
                    {
                        if (!passedEvidence.preset.IsKeyUnique(k)) continue;

                        if (evidenceKeys.Contains(k))
                        {
                            //Found matching data key; this evidence is pinned already...
                            matching = el;
                            break;
                        }
                    }

                    if (matching != null) break;
                }
                else
                {
                    matching = el;
                    break;
                }
            }

            if (matching != null)
            {
                OnWindowPinnedChange(true, matching);
            }
            else
            {
                if (forcedPinnedCaseElement != null && forcedPinnedCaseElement.pinnedController != null)
                {
                    OnWindowPinnedChange(true, forcedPinnedCaseElement);
                }
                else
                {
                    OnWindowPinnedChange(false, null);
                }
            }
        }
        else
        {
            if (pinned)
            {
                OnWindowPinnedChange(false, null); //This is not pinned because there is no active case
            }
        }

        //Update the pin colour here as it may change between different cases
        UpdatePinColour();
    }

    //Triggered by the case panel controller when pinned or unpinned
    public void OnWindowPinnedChange(bool isPinned, Case.CaseElement newCaseElement)
    {
        pinned = isPinned;
        currentPinnedCaseElement = newCaseElement;

        Game.Log("Interface: Pinned change for " + name + ": " + pinned);

        //Change close graphic
        if(!pinned)
        {
            closeButtonIcon.sprite = InterfaceControls.Instance.closeSprite;
            //closeButtonIcon.color = InterfaceControls.Instance.closeColour;
        }
        else
        {
            closeButtonIcon.sprite = InterfaceControls.Instance.minimizeSprite;
            //closeButtonIcon.color = InterfaceControls.Instance.minimizeColour;
        }

        //Update seen keys & new info icon
        if (currentPinnedCaseElement != null)
        {
            currentPinnedCaseElement.sdk = new List<Evidence.DataKey>(evidenceKeys);
            if (currentPinnedCaseElement.pinnedController != null) currentPinnedCaseElement.pinnedController.UpdateNewInfoIcon();
        }

        //Remove focus
        //InterfaceController.Instance.RemoveWindowFocus();

        pinButton.VisualUpdate();
    }

    //Set closable
    public void SetClosable(bool newClosble)
    {
        closable = newClosble;

        if(closable)
        {
            closeButton.SetInteractable(true);
            closeButton.gameObject.SetActive(true);
        }
        else
        {
            closeButton.SetInteractable(false);
            closeButton.gameObject.SetActive(false);
        }
    }

    //Set window's position
    public void SetAnchoredPosition(Vector2 newPos)
    {
        Game.Log("Interface: Set window anchored position: " + newPos);
        if (rect == null) rect = this.gameObject.GetComponent<RectTransform>();
        debugSetAnchoredPosition = newPos;

        //Size of the window's rect
        Vector2 size = rect.rect.size;

        //Difference from 0,0 pivot to window pivot
        Vector2 deltaPivot = Vector2.zero - rect.pivot;
        Vector3 deltaPosition = new Vector3(deltaPivot.x * size.x, deltaPivot.y * size.y);

        Vector2 inversePivot = Vector2.one - rect.pivot;
        Vector3 inversePosition = new Vector3(inversePivot.x * size.x, inversePivot.y * size.y);

        float clampedX = Mathf.Clamp(newPos.x, -deltaPosition.x, InterfaceControls.Instance.hudCanvasRect.rect.width - inversePosition.x);
        float clampedY = Mathf.Clamp(newPos.y, -InterfaceControls.Instance.hudCanvasRect.rect.height - deltaPosition.y, -inversePosition.y);

        rect.anchoredPosition = new Vector2(clampedX, clampedY);

        clampedX = Mathf.Clamp(rect.localPosition.x, InterfaceControls.Instance.hudCanvasRect.rect.width * -0.5f, InterfaceControls.Instance.hudCanvasRect.rect.width * 0.5f);
        clampedY = Mathf.Clamp(rect.localPosition.y, InterfaceControls.Instance.hudCanvasRect.rect.height * -0.5f, InterfaceControls.Instance.hudCanvasRect.rect.height * 0.5f);
        rect.localPosition = new Vector3(clampedX, clampedY, 0);
        Game.Log("Interface: Set window local position: " + rect.localPosition);
    }

    //Set window's pivot
    public void SetPivot(Vector2 p)
    {
        if (rect == null) rect = this.gameObject.GetComponent<RectTransform>();

        //Size of the window's rect
        Vector2 size = rect.rect.size;

        //Difference from original pivot to new pivot
        Vector2 deltaPivot = rect.pivot - p;

        //The different as above but calculated position
        Vector3 deltaPosition = new Vector3(deltaPivot.x * size.x, deltaPivot.y * size.y);

        //Game.Log("Original pivot: " + rect.pivot + " new: " + p + " Diff in pivot: " + deltaPivot + " original pos: " + rect.anchoredPosition + " sub position: " + deltaPosition + " new pos:" + new Vector2(rect.anchoredPosition.x - deltaPosition.x, rect.anchoredPosition.y - deltaPosition.y));

        //Set new pivot
        rect.pivot = p;

        //Add the difference in position
        SetAnchoredPosition(new Vector2(rect.anchoredPosition.x - deltaPosition.x, rect.anchoredPosition.y - deltaPosition.y));
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isOver = true;

        //Update pulsate
        if(currentPinnedCaseElement != null)
        {
            if(currentPinnedCaseElement.pinnedController != null)
            {
                currentPinnedCaseElement.pinnedController.UpdatePulsate();
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;

        isOver = false;

        //Update pulsate
        if (currentPinnedCaseElement != null)
        {
            if (currentPinnedCaseElement.pinnedController != null)
            {
                currentPinnedCaseElement.pinnedController.UpdatePulsate();
            }
        }
    }

    public void UpdatePinColour()
    {
        if (passedEvidence == null) return;

        //Copy pin colour from pinned case element
        if(currentPinnedCaseElement != null)
        {
            evColour = currentPinnedCaseElement.color;
        }

        pinColourActual = InterfaceController.Instance.GetEvidenceColour(evColour);
        pinColour.color = pinColourActual;
        pinColourPressed.color = pinColourActual;
    }

    //Rename evidence
    public void Rename()
    {
        if(passedEvidence != null && passedEvidence.preset.allowCustomNames)
        {
            //Bring up text enter
            PopupMessageController.Instance.PopupMessage("RenameEvidence", true, true, RButton: "Confirm", enableInputField: true, inputFieldDefault: titleText.text);
            PopupMessageController.Instance.OnRightButton += OnEditName;
        }
        else
        {
            PopupMessageController.Instance.PopupMessage("RenameDisabled", true, false, LButton: "Confirm");
        }
    }

    public void OnEditName()
    {
        PopupMessageController.Instance.OnRightButton -= OnEditName;

        if (passedEvidence != null && passedEvidence.preset.allowCustomNames)
        {
            passedEvidence.AddOrSetCustomName(passedKeys, PopupMessageController.Instance.inputField.text);
        }

        SetName(PopupMessageController.Instance.inputField.text);
    }

    //Context menu
    public void SetColourRed()
    {
        if (currentPinnedCaseElement != null)
        {
            currentPinnedCaseElement.SetColour(InterfaceControls.EvidenceColours.red);
        }
        else
        {
            evColour = InterfaceControls.EvidenceColours.red;
            UpdatePinColour();
        }
    }

    public void SetColourBlue()
    {
        if (currentPinnedCaseElement != null)
        {
            currentPinnedCaseElement.SetColour(InterfaceControls.EvidenceColours.blue);
        }
        else
        {
            evColour = InterfaceControls.EvidenceColours.blue;
            UpdatePinColour();
        }
    }

    public void SetColourYellow()
    {
        if (currentPinnedCaseElement != null)
        {
            currentPinnedCaseElement.SetColour(InterfaceControls.EvidenceColours.yellow);
        }
        else
        {
            evColour = InterfaceControls.EvidenceColours.yellow;
            UpdatePinColour();
        }
    }

    public void SetColourGreen()
    {
        if (currentPinnedCaseElement != null)
        {
            currentPinnedCaseElement.SetColour(InterfaceControls.EvidenceColours.green);
        }
        else
        {
            evColour = InterfaceControls.EvidenceColours.green;
            UpdatePinColour();
        }
    }

    public void SetColourPurple()
    {
        if (currentPinnedCaseElement != null)
        {
            currentPinnedCaseElement.SetColour(InterfaceControls.EvidenceColours.purple);
        }
        else
        {
            evColour = InterfaceControls.EvidenceColours.purple;
            UpdatePinColour();
        }
    }

    public void SetColourWhite()
    {
        if (currentPinnedCaseElement != null)
        {
            currentPinnedCaseElement.SetColour(InterfaceControls.EvidenceColours.white);
        }
        else
        {
            evColour = InterfaceControls.EvidenceColours.white;
            UpdatePinColour();
        }
    }

    public void SetColourBlack()
    {
        if (currentPinnedCaseElement != null)
        {
            currentPinnedCaseElement.SetColour(InterfaceControls.EvidenceColours.black);
        }
        else
        {
            evColour = InterfaceControls.EvidenceColours.black;
            UpdatePinColour();
        }
    }

    public void SetSelected(bool val)
    {
        if(selected != val)
        {
            selected = val;
            controllerScrollView.controlEnabled = selected;

            //Select pin button
            if (selected && CasePanelController.Instance.controllerMode)
            {
                if (InterfaceController.Instance.selectedElement != null)
                {
                    InterfaceController.Instance.selectedElement.OnDeselect();
                }

                pinButton.OnSelect();
            }

            UpdateControllerSelected();
        }
    }

    public void UpdateControllerSelected()
    {
        if(CasePanelController.Instance.controllerMode && CasePanelController.Instance.currentSelectMode == CasePanelController.ControllerSelectMode.windows)
        {
            controllerSelect.gameObject.SetActive(selected);
        }
        else controllerSelect.gameObject.SetActive(false);
    }

    public void UpdateControllerNavigationEndOfFrame()
    {
        if (!SessionData.Instance.startedGame) return; //Skip this if the game has't been started- city constructor will run it once
        
        if(!updateNav)
        {
            updateNav = true;
            StartCoroutine(UpdateControllerNavigation());
        }
    }

    public void OnClearTextButton()
    {
        if(activeContent != null)
        {
            activeContent.clearTextMode = !activeContent.clearTextMode;

            foreach(KeyValuePair <DDSSaveClasses.DDSMessageSettings, TextMeshProUGUI> pair in activeContent.spawnedText)
            {
                if(pair.Key.isHandwriting)
                {
                    if(activeContent.clearTextMode)
                    {
                        pair.Value.font = DDSControls.Instance.clearModeFont;
                    }
                    else
                    {
                        if (activeContent.window.passedEvidence.writer != null && activeContent.window.passedEvidence.writer.handwriting != null)
                        {
                            pair.Value.font = activeContent.window.passedEvidence.writer.handwriting.fontAsset;
                        }
                        else pair.Value.font = DDSControls.Instance.defaultHandwritingFont;
                    }

                    pair.Value.ForceMeshUpdate();
                    activeContent.TextOverflowCheck();
                }
            }
        }
    }

    public void OnTakeItemButton()
    {
        if (isWorldInteraction && passedInteractable != null)
        {
            PopupMessageController.Instance.PopupMessage("Take Item", true, true, RButton: "Confirm");
            PopupMessageController.Instance.OnRightButton += OnTakeConfirm;
            PopupMessageController.Instance.OnLeftButton += OnTakeCancel;
        }
    }

    public void OnTakeConfirm()
    {
        PopupMessageController.Instance.OnRightButton -= OnTakeConfirm;
        PopupMessageController.Instance.OnLeftButton -= OnTakeCancel;

        if (passedInteractable != null && passedInteractable.inInventory != Player.Instance)
        {
            InteractablePreset.InteractionAction inter = passedInteractable.preset.GetActions().Find(item => item.action == RoutineControls.Instance.takeFirstPersonItem);

            if(inter != null)
            {
                //Trigger response chance
                if (passedInteractable.inInventory != null && passedInteractable.inInventory != Player.Instance && Toolbox.Instance.Rand(0f, 1f) <= GameplayControls.Instance.stealTriggerChance)
                {
                    passedInteractable.inInventory.ai.SetPersue(Player.Instance, true, 1, true, CitizenControls.Instance.punchedResponseRange);
                }

                passedInteractable.OnInteraction(inter, Player.Instance);
            }
        }

        RefreshTakeButton();
    }

    public void OnTakeCancel()
    {
        PopupMessageController.Instance.OnRightButton -= OnTakeConfirm;
        PopupMessageController.Instance.OnLeftButton -= OnTakeCancel;
    }

    IEnumerator UpdateControllerNavigation()
    {
        int waitedFrame = 0;

        while(waitedFrame <= 1)
        {
            waitedFrame++;

            yield return null;
        }

        ExecuteUpdateControllerNavigation();
        updateNav = false;
    }

    [Button]
    public void ExecuteUpdateControllerNavigation()
    {
        if (activeContent == null) return;

        Game.Log("Interface: Updating controller navigation for window " + name);

        //The selectables on the current active content
        //List<Button> contentButtons = activeContent.transform.GetComponentsInChildren<Button>(true).ToList();
        //Toolbox.Instance.AutomaticNavigationSetup(ref contentButtons);

        //Setup tab buttons
        for (int i = 0; i < tabs.Count; i++)
        {
            WindowTabController t = tabs[i];

            t.tabButton.gameObject.GetComponent<ButtonController>().RefreshAutomaticNavigation();

            //Vector3 tScreenPos = t.tabButton.transform.position;

            //Navigation nav = new Navigation { mode = Navigation.Mode.Explicit };

            //if (i == 0)
            //{
            //    nav.selectOnUp = closeButton.button;
            //    Toolbox.Instance.AddNavigationInput(closeButton.button, newDown: t.tabButton);
            //}
            //else
            //{
            //    nav.selectOnUp = tabs[i - 1].tabButton;
            //}

            //if (i < tabs.Count - 1)
            //{
            //    nav.selectOnDown = tabs[i + 1].tabButton;
            //}

            ////Set left to closest content button
            //float closest = Mathf.Infinity;
            //Button closestButton = null;

            //foreach (Button b in contentButtons)
            //{
            //    Vector3 bScreenPos = b.transform.position;
            //    float dist = Vector3.Distance(bScreenPos, tScreenPos);

            //    if (dist < closest)
            //    {
            //        closest = dist;
            //        closestButton = b;
            //    }
            //}

            //if (closestButton != null)
            //{
            //    nav.selectOnLeft = closestButton;
            //    Toolbox.Instance.AddNavigationInput(closestButton, newRight: t.tabButton);
            //}
            //else
            //{
            //    nav.selectOnLeft = pinButton.button;
            //}

            //t.tabButton.navigation = nav;
        }

        ////Link pin with content buttons
        ////Set left to closest content button
        //float closestP = Mathf.Infinity;
        //Button closestButtonP = null;

        //if(pinButton != null)
        //{
        //    Vector3 pScreenPos = pinButton.transform.position;

        //    foreach (Button b in contentButtons)
        //    {
        //        Vector3 bScreenPos = b.transform.position;
        //        float dist = Vector3.Distance(bScreenPos, pScreenPos);

        //        if (dist < closestP)
        //        {
        //            closestP = dist;
        //            closestButtonP = b;
        //        }
        //    }
        //}

        //if(closestButtonP != null)
        //{
        //    Toolbox.Instance.AddNavigationInput(pinButton.button, newDown: closestButtonP);
        //    Toolbox.Instance.AddNavigationInput(closestButtonP, newUp: pinButton.button);
        //}
        //else if(closeButton != null)
        //{
        //    Toolbox.Instance.AddNavigationInput(pinButton.button, newDown: closeButton.button);
        //}

        ////Find closest tab button for content buttons
        //foreach(Button b in contentButtons)
        //{
        //    //Find the closest tab
        //    Button closestTab = null;
        //    float closeTab = Mathf.Infinity;

        //    foreach (WindowTabController t in tabs)
        //    {
        //        float dist = Vector3.Distance(b.transform.position, t.tabButton.transform.position);

        //        if(dist < closeTab)
        //        {
        //            closestTab = t.tabButton;
        //            closeTab = dist;
        //        }
        //    }

        //    if(closestTab != null)
        //    {
        //        Toolbox.Instance.AddNavigationInput(b, newRight: closestTab);
        //    }
        //}
    }

    [Button]
    public void ExecuteKeyMerge()
    {
        if(passedEvidence != null)
        {
            passedEvidence.MergeDataKeys(debugKeyOne, debugKeyTwo);
        }
    }

    [Button]
    public void SpawnFingerprintOwner()
    {
        if(passedEvidence != null)
        {
            InterfaceController.Instance.SpawnWindow(passedEvidence.writer.evidenceEntry, Evidence.DataKey.fingerprints);
        }
    }

    [Button]
    public void RestoreAnchoredPosition()
    {
        SetAnchoredPosition(debugSetAnchoredPosition);
    }
}
