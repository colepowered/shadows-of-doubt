﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SalesLedgerContentController : MonoBehaviour
{
    public WindowContentController windowContent;
    public InfoWindow parentWindow;
    public TextMeshProUGUI descriptionText;
    public GameObject entryPrefab;
    public List<SalesLedgerEntryController> spawnedEntries = new List<SalesLedgerEntryController>();

    public class Transaction
    {
        public string text;
        public int amount;
    }

    private void OnEnable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        if (windowContent == null) windowContent = this.gameObject.GetComponentInParent<WindowContentController>();
        parentWindow.passedEvidence.OnDataKeyChange += CheckEnabled;
        parentWindow.OnWindowRefresh += CheckEnabled;

        CheckEnabled();
    }

    private void OnDisable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange -= CheckEnabled;
        parentWindow.OnWindowRefresh -= CheckEnabled;
    }

    public void CheckEnabled()
    {
        while(spawnedEntries.Count > 0)
        {
            Destroy(spawnedEntries[0].gameObject);
            spawnedEntries.RemoveAt(0);
        }

        Company company = parentWindow.passedInteractable.node.gameLocation.thisAsAddress.company;

        //Make sure sales are sorted by time, lowest first
        company.sales.Sort((p1, p2) => p1.time.CompareTo(p2.time));

        float yPos = 0;

        for (int i = 0; i < company.sales.Count; i++)
        {
            Company.SalesRecord temp = company.sales[i];

            GameObject newT = Instantiate(entryPrefab, this.transform);
            SalesLedgerEntryController bs = newT.GetComponent<SalesLedgerEntryController>();
            bs.Setup(temp, this);
            bs.rect.anchoredPosition = new Vector2(bs.rect.anchoredPosition.x, yPos);
            yPos -= bs.rect.sizeDelta.y;

            spawnedEntries.Add(bs);
        }

        yPos -= 100;

        windowContent.pageRect.sizeDelta = new Vector2(windowContent.pageRect.sizeDelta.x, Mathf.Max(windowContent.pageRect.sizeDelta.y, -yPos));
        windowContent.rect.sizeDelta = new Vector2(windowContent.pageRect.sizeDelta.x, Mathf.Max(windowContent.pageRect.sizeDelta.y, -yPos));
    }
}
