﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using NaughtyAttributes;

public class CityControls : MonoBehaviour
{
    [System.Serializable]
    public struct WindowColour
    {
        public Color colourOne;
        public Color colourTwo;
    }

    [System.Serializable]
    public class NeonMaterial
    {
        public Color neonColour = Color.white;
        public Color altColour2 = Color.white;
        public Color altColour3 = Color.white;
        public Material regularMat; //Regular version
        public Material flickingMat; //Flicking version
        public AudioEvent flickerAudio;

        [Tooltip("Does this light flicker?")]
        public bool flicker = false;
        [Tooltip("When flickering, use this multiplier on the flicker colour to determin the actual colour (basically a darker version of flicker colour)")]
        public float flickerColourMultiplier = 0f;
        public float pulseSpeed = 1f;
        public float flickerState = 1f;
        public bool flickerSwitch = false;
        public bool flickerInterval = false;
        public float interval = 0f;
        public float intervalTime = 0f;
        public float brightness = 0f;

        [Space(5)]
        public string colourTag;
    }

    [System.Serializable]
    public class CitySize
    {
        public Size size;
        public Vector2 v2;
    }

    public enum Size { small, medium, large, veryLarge };


    [Header("City")]
    public string wardName = "Downtown";
    public string cityCustoms = "Whitaker Customs Department";
    public string cityCustomsAbr = "WCD";
    public string cityTax = "Whitaker Revenue Service";
    public string cityTaxAbr = "WRS";
    public string cityCurrency = "$";

    [Header("Size")]
    [ReorderableList]
    public List<CitySize> citySizes = new List<CitySize>();

    [Header("Infrastructure")]
    [Tooltip("Exact measurements of groundmap tiles in unity units (metres)")]
    public Vector3 cityTileSize = new Vector3(3.15f, 3.15f, 2f);
    [Tooltip("Tiles are multiplied by this per ground map unit for pathmap grid")]
    public int tileMultiplier = 5;
    [Tooltip("Nodes are multiplied by this per tile")]
    public int nodeMultiplier = 3;
    [Tooltip("Maximum size of a city block")]
    public int maxBlockSize = 7;
    [Tooltip("Chance block will expand into adjacent tile")]
    public float blockExpandChance = 100f;
    [Tooltip("?")]
    public float blockExpandCentreMultiplier = 2f;
    [Tooltip("?")]
    public float nonFavouredExpandMultiplier = 0.25f;
    [Tooltip("Minimum size of a district")]
    public int districtSizeMin = 4;
    [Tooltip("Maximum size of a district")]
    public int districtSizeMax = 8;
    [Tooltip("Chances of a side alley being formed")]
    public float sideAlleyChance = 0.33f;
    [Tooltip("Chances of a side alley being extended matching a previous side alley")]
    public float sideAlleyExtentionChance = 0.33f;
    [Tooltip("Enable overhead street placement")]
    public bool overheadStreet = true;

    [Header("Population")]
    [Tooltip("Amount to multiply the travel time distance by for calculating travel time guesses. X and Y planes only, IE Descrepencey between as the crow flies, and actual street route distance.")]
    public float travelTimeCrowFliesMultiplierEstimate = 1.66f;
    [Tooltip("Multiplier for travel time vs distance. Applied for guesses and calculated")]
    public float travelTimeMultiplier = 1.2f;
    [Tooltip("The city size (tile count) * by this will be created")]
    public float homelessMultiplier = 2f;

    //Ratios of building types
    [Header("Zoning")]
    public float residentialRatio = 0.5f;
    public float commercialRatio = 0.22f;
    public float industrialRatio = 0.19f;
    public float municipalRatio = 0.04f;
    public float parksRatio = 0.05f;

    [Header("Buildings")]
    [Tooltip("The address preset for lobbies/hallways")]
    public AddressPreset lobbyPreset;
    [Tooltip("An internal address/unit of this many tiles is categorised as...")]
    public Vector2 smallUnitRange = new Vector2(0, 4);
    [Tooltip("An internal address/unit of this many tiles is categorised as...")]
    public Vector2 mediumUnitRange = new Vector2(5, 8);
    [Tooltip("An internal address/unit of this many tiles is categorised as...")]
    public Vector2 lageUnitRange = new Vector2(9, 999);
    [Space(5)]
    [Tooltip("The default design style")]
    public DesignStylePreset defaultStyle;
    [Tooltip("Reference to default walls")]
    public DoorPairPreset defaultWalls;
    public MaterialGroupPreset defaultFloorMaterialGroup;
    public MaterialGroupPreset defaultCeilingMaterialGroup;
    public MaterialGroupPreset defaultWallMaterialGroup;
    [Space(5)]
    [Tooltip("Setup for interior 'null space'")]
    public RoomConfiguration nullDefaultRoom;
    [Tooltip("Setup for interior hallways")]
    public RoomConfiguration streetRoom;
    public RoomConfiguration alleyRoom;
    public RoomConfiguration backstreetRoom;
    [Header("Layout Configs")]
    public LayoutConfiguration outsideLayoutConfig;
    public LayoutConfiguration lobbyLayoutConfig;
    [Tooltip("Street design styles")]
    public DesignStylePreset street;
    [Space(5)]
    public int lowestFloor = -2;
    public float lowestFloorLightMultiplier = 0.5f;
    public float lowestFloorIncreaseFlickerChance = 0.5f;
    public float basementWaterLevel = -9.6f;

    [Header("Interior fallback")]
    public DesignStylePreset fallbackStyle;
    public ColourSchemePreset fallbackColourScheme;
    public MaterialGroupPreset fallbackFloorMat;
    public MaterialGroupPreset fallbackWallMat;
    public MaterialGroupPreset fallbackCeilingMat;

    [Header("Lighting")]
    [Tooltip("The directional light representing the sun")]
    public Light sunLight;
    public Transform sunPosition;
    public HDAdditionalLightData hdrpLightSunData;
    public Light exteriorAmbientLight;
    public HDAdditionalLightData exteriorAmbientHDRP;
    public Light interiorAmbientLight;
    public HDAdditionalLightData interiorAmbientHDRP;
    public Material seaMaterial;
    public Material skylineMaterial;
    public Material smokeMaterial;

    [Header("PP Profiles")]
    public List<PPProfile> sceneProfileSetup = new List<PPProfile>();
    public PPProfile captureSceneNormal;
    public PPProfile captureSceneCCTV;

    [System.Serializable]
    public class PPProfile
    {
        public SessionData.SceneProfile profile;
        public UnityEngine.Rendering.Volume volume;
        public GameObject objectRef;
    }

    [Header("Skybox")]
    public Transform ships1;

    [Tooltip("Angle of North")]
    public float angleOfSun = 0f;
    //[Tooltip("Sun rises at this hour (90 degrees to terrain)")]
    //public float sunRiseHour = 6.5f;
    //[Tooltip("Sun sets at this hour (90 degrees to terrain)")]
    //public float sunSetHour = 7f;
    //[Tooltip("Sun intensity curve")]
    //public AnimationCurve daytimeSunIntensityCurve = AnimationCurve.EaseInOut(1.0f, 1.0f, 0.0f, 0.0f);
    //[Tooltip("Multiply the above curve by this")]
    //public float sunIntensityBooster = 1.5f;
    //[Tooltip("Morning sun Colour")]
    //public Color morningSunColour = Color.red;
    //[Tooltip("Midday sun Colour")]
    //public Color middaySunColour = Color.white;
    //[Tooltip("Evening sun Colour")]
    //public Color eveningSunColour = Color.red;
    //[Tooltip("Skybox colour grades w/ fog colour settings")]
    //public List<SessionData.SkyboxGradient> skyboxGradientGrading = new List<SessionData.SkyboxGradient>();
    //[Tooltip("Sun shadow strength curve")]
    //public AnimationCurve sunShadowStrengthCurve = AnimationCurve.EaseInOut(1.0f, 1.0f, 0.0f, 0.0f);

    [Tooltip("Interior/Street Lights off")]
    public Vector2 lightsOff = new Vector2(8.5f, 9f);
    [Tooltip("Interior/Street Lights on")]
    public Vector2 lightsOn = new Vector2(15f, 16f);

    [Header("Street Furniture")]
    [Tooltip("Alley blocking wall preset")]
    public DoorPairPreset alleyBlockWallPreset;

    [Header("Fog")]
    public FogPreset weatherSettings;
    //[Tooltip("Fog distance ranges")]
    //public Vector2 fogDistanceRange = new Vector2(10f, 85f);
    //[Tooltip("Fog distance throughout the day")]
    //public AnimationCurve fogDistanceCurve = AnimationCurve.EaseInOut(1.0f, 1.0f, 0.0f, 0.0f);
    //public Vector2 maxFogDistanceRange = new Vector2(10f, 85f);
    //[Tooltip("Max Fog distance throughout the day")]
    //public AnimationCurve maxFogDistanceCurve = AnimationCurve.EaseInOut(1.0f, 1.0f, 0.0f, 0.0f);

    [Header("Weather")]
    [Tooltip("How long it takes in gametime for the city to get wet on max rain (1)")]
    public float timeForCityToGetWet = 0.1f;
    [Tooltip("How long it takes in gametime for the city to get dry on min rain (0)")]
    public float timeForCityToGetDry = 0.7f;
    [Tooltip("How long it takes in gametime for the city to get snowy on max rain (1)")]
    public float timeForCityToGetSnow = 0.1f;
    [Tooltip("How long it takes in gametime for the city to get not snowy on min rain (0)")]
    public float timeForCityToGetNotSnow = 0.7f;

    [Header("Signage")]
    [Tooltip("The default neon material")]
    public Material neonMaterial;
    [Tooltip("Neon HDR intensity")]
    public float neonIntensity = 1.85f;
    [Tooltip("Neon colours that can appear in signage, along with generated material references")]
    public List<NeonMaterial> neonColours = new List<NeonMaterial>();

    [Header("Street Cables")]
    public List<StreetCable> cables = new List<StreetCable>();

    [System.Serializable]
    public class StreetCable
    {
        public GameObject prefab;
        public float maximumWidth = 15f;
        public int frequency = 1;
        [Tooltip("The maximum angle deviation for cables. 0 is only straight.")]
        public float maximumCableAngle = 0.25f;
        public float minimumHeight = 0f;
        public float maximumHeight = 999f;

        [Space(7)]
        public bool onlyFromZoneType = false;
        public BuildingPreset.ZoneType zone;
        [Space(7)]
        public bool disitrctFrequencyModifier = false;
        public List<DistrictPreset> districts = new List<DistrictPreset>();
        public int frequencyModifier = 0;

        [Space(7)]
        [Tooltip("Change area colours")]
        public bool alterAreaLighting = false;
        [EnableIf("alterAreaLighting")]
        public List<Color> possibleColours = new List<Color>();
        [Tooltip("This is used in combination with the following to adjust street area lighting")]
        [EnableIf("alterAreaLighting")]
        public DistrictPreset.AffectStreetAreaLights lightOperation = DistrictPreset.AffectStreetAreaLights.lerp;
        [EnableIf("alterAreaLighting")]
        public float lightAmount = 0.12f;
        [Tooltip("This is added to brightness")]
        [EnableIf("alterAreaLighting")]
        public float brightnessModifier = 10f;
    }

    public float maximumCableAngle = 0.25f;

    [Header("Misc. References")]
    public LayoutConfiguration park;

    [Header("Hotels")]
    [Tooltip("Upper and lower ends for hotel rooms in the city")]
    public int hotelCostLower = 100;
    [Tooltip("Upper and lower ends for hotel rooms in the city")]
    public int hotelCostUpper = 200;
    [Tooltip("Time until the player is kicked out of their hotel room for not paying")]
    public float kickoutTime = 8;

    [Header("Basement Water")]
    public Transform basementWaterTransform;

    [Header("Lost & Found")]
    public InteractablePreset lostAndFoundNote;
    [Tooltip("Items that can be lost and posted about")]
    public List<InteractablePreset> lostAndFoundItems = new List<InteractablePreset>();

    //Singleton pattern
    private static CityControls _instance;
    public static CityControls Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        weatherSettings.skyboxGradientGrading.Sort(); //Sort skybox gradients by time order
    }
}
