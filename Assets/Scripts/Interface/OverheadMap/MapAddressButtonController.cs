﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

public class MapAddressButtonController : ButtonController
{
    public NewGameLocation gameLocation;
    public Image typeIcon;

    public Vector2 range;

    //Generated image
    public Image generatedImage;
    public Texture2D tex;

    Action UpdateMapTex;

    public void Setup(NewGameLocation newAddress)
    {
        //Setup actions
        UpdateMapTex += GenerateMapImage;

        base.SetupReferences();
        gameLocation = newAddress;
        gameLocation.mapButton = this; //Reference to this in the location class

        //Setup tooltip
        tooltip.mainText = string.Empty;
        tooltip.parentOverride = MapController.Instance.tooltipOverride;
        typeIcon.enabled = false;

        //Use the largest light zone
        NewRoom largestRoom = gameLocation.rooms[0];

        foreach(NewRoom room in gameLocation.rooms)
        {
            if(room.nodes.Count > largestRoom.nodes.Count)
            {
                largestRoom = room;
            }
        }

        Vector2 mapPos = MapController.Instance.RealPosToMap(largestRoom.middleRoomPosition);
        typeIcon.GetComponent<RectTransform>().anchoredPosition = mapPos - rect.anchoredPosition;

        if (rect.sizeDelta.x > 0 && rect.sizeDelta.y > 0)
        {
            UpdateMapImageEndOfFrame();
        }
    }

    //Update map image at end of frame (ignore duplicate requests)
    public void UpdateMapImageEndOfFrame()
    {
        //Update @ end of frame
        Toolbox.Instance.InvokeEndOfFrame(UpdateMapTex, "Update map image");
    }

    //Combine/refresh images into a single one...
    public void GenerateMapImage()
    {
        //Game.Log("Generating map image for " + gameLocation.name);
        Vector2 texRes = new Vector2(Mathf.RoundToInt(rect.sizeDelta.x / MapController.Instance.mapResolutionDivision), Mathf.RoundToInt(rect.sizeDelta.y / MapController.Instance.mapResolutionDivision));

        //Remove previous
        if(tex == null)
        {
            tex = new Texture2D((int)texRes.x, (int)texRes.y);
            tex.filterMode = FilterMode.Trilinear;
            tex.name = "Generated Texture";
            generatedImage.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100f);
            generatedImage.GetComponent<RectTransform>().sizeDelta = rect.sizeDelta;
            generatedImage.color = MapController.Instance.roomBaseColor;
            generatedImage.alphaHitTestMinimumThreshold = 0.5f;
        }

        //Color32 is apparently faster than normal color
        //Reset all pixels color to transparent
        Color32 resetColor = new Color32(0, 0, 0, 0);
        Color32[] resetColorArray = tex.GetPixels32();

        for (int i = 0; i < resetColorArray.Length; i++)
        {
            resetColorArray[i] = resetColor;
        }

        tex.SetPixels32(resetColorArray);

        List<NewTile> stairwellsWritten = new List<NewTile>(); //Keep track of stairwells writen to avoid writing them 9 times

        //Fill in the image in node-sized steps
        foreach (NewNode node in gameLocation.nodes)
        {
            //Get the 0,0 location of this node within in the image
            Vector2 mapCoord = MapController.Instance.NodeCoordToMap(node.nodeCoord);
            Vector2 withinImage = (mapCoord - rect.anchoredPosition - new Vector2(MapController.Instance.nodePositionMultiplier * 0.5f, MapController.Instance.nodePositionMultiplier * 0.5f)) / MapController.Instance.mapResolutionDivision;

            //Copy texture from floor tile (if floor exists)
            if (!node.isOutside && !node.gameLocation.thisAsAddress.preset.isOutside && (node.floorType == NewNode.FloorTileType.floorOnly || node.floorType == NewNode.FloorTileType.floorAndCeiling || node.room.explorationLevel <= 0))
            {
                Texture2D floorRef = MapController.Instance.publicFloorTexture;

                if(node.room.preset.forbidden == RoomConfiguration.Forbidden.alwaysForbidden && node.room.gameLocation != Player.Instance.home && !Player.Instance.apartmentsOwned.Contains(node.room.gameLocation))
                {
                    //Allow if given password
                    if(node.room.preset.allowedIfGivenCorrectPassword && node.gameLocation != null && node.gameLocation.thisAsAddress != null && GameplayController.Instance.playerKnowsPasswords.Contains(node.gameLocation.thisAsAddress.id))
                    {
                        floorRef = MapController.Instance.publicFloorTexture;
                    }
                    else floorRef = MapController.Instance.privateFloorTexture;
                }

                //Write null room
                if(node.room.isNullRoom)
                {
                    floorRef = MapController.Instance.nullRoomTexture;
                }

                if(node.room.explorationLevel <= 0)
                {
                    floorRef = MapController.Instance.undiscoveredTexture;
                }

                //Write floor texture
                Color32[] floorTex = floorRef.GetPixels32();
                tex.SetPixels32((int)withinImage.x, (int)withinImage.y, MapController.Instance.publicFloorTexture.width, MapController.Instance.publicFloorTexture.height, floorTex);
            }

            //Stairwell
            if(node.room.explorationLevel > 0)
            {
                if (node.tile.isStairwell && !stairwellsWritten.Contains(node.tile))
                {
                    stairwellsWritten.Add(node.tile);

                    //Get the lower left tile
                    NewNode bottomLeft = node.tile.nodes.Find(item => item.localTileCoord == Vector2.zero);

                    //Get the 0,0 location of this node within in the image
                    Vector2 stairsMapCoord = MapController.Instance.NodeCoordToMap(bottomLeft.nodeCoord);
                    Vector2 stairsWithinImage = (stairsMapCoord - rect.anchoredPosition - new Vector2(MapController.Instance.nodePositionMultiplier * 0.5f, MapController.Instance.nodePositionMultiplier * 0.5f)) / MapController.Instance.mapResolutionDivision;

                    if (node.tile.stairwellRotation == 0)
                    {
                        Color[] stairs = MapController.Instance.stairwell[0].GetPixels(0, 0, MapController.Instance.stairwell[0].width, MapController.Instance.stairwell[0].height);
                        tex.SetPixels((int)stairsWithinImage.x + MapController.Instance.wallWidth, (int)stairsWithinImage.y + MapController.Instance.wallWidth, MapController.Instance.stairwell[0].width, MapController.Instance.stairwell[0].height, stairs);
                    }
                    else if (node.tile.stairwellRotation == 90 || node.tile.stairwellRotation == -270)
                    {
                        Color[] stairs = MapController.Instance.stairwell[1].GetPixels(0, 0, MapController.Instance.stairwell[1].width, MapController.Instance.stairwell[1].height);
                        tex.SetPixels((int)stairsWithinImage.x + MapController.Instance.wallWidth, (int)stairsWithinImage.y + MapController.Instance.wallWidth, MapController.Instance.stairwell[1].width, MapController.Instance.stairwell[1].height, stairs);
                    }
                    else if (node.tile.stairwellRotation == 180)
                    {
                        Color[] stairs = MapController.Instance.stairwell[2].GetPixels(0, 0, MapController.Instance.stairwell[2].width, MapController.Instance.stairwell[2].height);
                        tex.SetPixels((int)stairsWithinImage.x + MapController.Instance.wallWidth, (int)stairsWithinImage.y, MapController.Instance.stairwell[2].width, MapController.Instance.stairwell[2].height, stairs);
                    }
                    else if (node.tile.stairwellRotation == 270 || node.tile.stairwellRotation == -90)
                    {
                        Color[] stairs = MapController.Instance.stairwell[3].GetPixels(0, 0, MapController.Instance.stairwell[3].width, MapController.Instance.stairwell[3].height);
                        tex.SetPixels((int)stairsWithinImage.x, (int)stairsWithinImage.y + MapController.Instance.wallWidth, MapController.Instance.stairwell[3].width, MapController.Instance.stairwell[3].height, stairs);
                    }
                }
            }
        }

        //Write furniture graphics (first layer)
        foreach (NewRoom room in gameLocation.rooms)
        {
            //Only write if exploration level is 2 or higher
            if(room.explorationLevel >= 2)
            {
                //Furniture
                foreach (FurnitureLocation furn in room.individualFurniture)
                {
                    if (furn.furniture.map != null)
                    {
                        if (!furn.furniture.drawUnderWalls) continue; //Draw this under walls

                        Texture2D furnTex = furn.furniture.map;

                        //Handle this using the true centre of the furniture in terms of node position
                        Vector3 avNodeCoord = Vector3.zero;

                        foreach (NewNode furnNode in furn.coversNodes)
                        {
                            avNodeCoord += furnNode.nodeCoord;
                        }

                        //Instance a version of this texture and rotate it (only if this needs rotating)
                        int angle = 0;

                        //Caculate angle
                        if(!furn.furniture.ignoreDirection)
                        {
                            Vector3 angleCalc = furn.anchorNode.room.transform.TransformDirection(new Vector3(0, furn.angle + furn.diagonalAngle, 0));
                            angle = Mathf.RoundToInt(angleCalc.y);

                            furnTex = RotateTexture(furnTex, -angle * furn.scaleMultiplier.x);
                        }

                        Vector2 furnMapCoord = MapController.Instance.NodeCoordToMap(avNodeCoord / (float)furn.coversNodes.Count);
                        Vector2 furnWithinImage = (furnMapCoord - rect.anchoredPosition - new Vector2(furnTex.width * 0.5f, furnTex.height * 0.5f)) / MapController.Instance.mapResolutionDivision;

                        //Write texture including transparency
                        for (int i = 0; i < furnTex.width; i++)
                        {
                            for (int u = 0; u < furnTex.height; u++)
                            {
                                Color setCol = furnTex.GetPixel(i, u);

                                //Skip pixel if fully transparent...
                                if (setCol.a <= 0.001f) continue;

                                Color destCol = tex.GetPixel((int)furnWithinImage.x + i, (int)furnWithinImage.y + u);
                                setCol = setCol * setCol.a + destCol * (1f - setCol.a); //Apply with alpha
                                tex.SetPixel((int)furnWithinImage.x + i, (int)furnWithinImage.y + u, setCol);
                            }
                        }
                    }
                }
            }
        }

        //Draw walls
        foreach (NewNode node in gameLocation.nodes)
        {
            //Get the 0,0 location of this node within in the image
            Vector2 mapCoord = MapController.Instance.NodeCoordToMap(node.nodeCoord);
            Vector2 withinImage = (mapCoord - rect.anchoredPosition - new Vector2(MapController.Instance.nodePositionMultiplier * 0.5f, MapController.Instance.nodePositionMultiplier * 0.5f)) / MapController.Instance.mapResolutionDivision;

            int privateBase = 0;

            if (!node.isOutside && !node.gameLocation.thisAsAddress.preset.isOutside && (node.floorType == NewNode.FloorTileType.floorOnly || node.floorType == NewNode.FloorTileType.floorAndCeiling))
            {
                if (node.room.preset.forbidden == RoomConfiguration.Forbidden.alwaysForbidden && node.room.gameLocation != Player.Instance.home && !Player.Instance.apartmentsOwned.Contains(node.room.gameLocation))
                {
                    //Allow if given password
                    if (node.room.preset.allowedIfGivenCorrectPassword && node.gameLocation != null && node.gameLocation.thisAsAddress != null && GameplayController.Instance.playerKnowsPasswords.Contains(node.gameLocation.thisAsAddress.id))
                    {
                        privateBase = 0;
                    }
                    else privateBase = 2;
                }
            }

            //Undiscovered
            if(node.room.explorationLevel <= 0)
            {
                privateBase = 4;
            }

            //Draw walls in wall texture
            bool wallLeft = false;
            bool wallRight = false;
            bool wallUp = false;
            bool wallDown = false;

            foreach (NewWall wall in node.walls)
            {
                //If level == 0 then only write exterior walls
                if (node.room.explorationLevel <= 0)
                {
                    //Otherwall must have exploration level of 0 also
                    if(wall.otherWall.node.room.explorationLevel <= 0)
                    {
                        if (wall.otherWall.node.gameLocation == wall.node.gameLocation)
                        {
                            continue;
                        }
                    }
                }

                int horz = privateBase + 1;

                if (wall.wallOffset.y < 0 || wall.wallOffset.y > 0)
                {
                    horz = privateBase + 0;
                }

                //Pick the texture
                Texture2D writeTex = MapController.Instance.wallEdge[horz];
                int wallWidth = MapController.Instance.wallWidth;

                if (wall.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
                {
                    writeTex = MapController.Instance.wallDoorway[horz];

                    if (wall.preset.dividerLeft)
                    {
                        if (wall.parentWall == wall)
                        {
                            writeTex = MapController.Instance.dividerLeft[horz];
                        }
                        else
                        {
                            writeTex = MapController.Instance.dividerRight[horz];
                        }
                    }
                    else if (wall.preset.dividerRight)
                    {
                        if (wall.parentWall == wall)
                        {
                            writeTex = MapController.Instance.dividerRight[horz];
                        }
                        else
                        {
                            writeTex = MapController.Instance.dividerLeft[horz];
                        }
                    }
                    else if (wall.preset.divider)
                    {
                        continue; //Otherwise don't write
                    }
                }
                else if (wall.preset.sectionClass == DoorPairPreset.WallSectionClass.window || wall.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge)
                {
                    if (node.isOutside || node.room.IsOutside() || node.room.building == null || (node.floorType != NewNode.FloorTileType.floorOnly && node.floorType != NewNode.FloorTileType.floorAndCeiling))
                    {
                        writeTex = MapController.Instance.outsideWindow[horz];
                        wallWidth = MapController.Instance.wallWidth + 2;
                    }
                    else
                    {
                        writeTex = MapController.Instance.wallWindow[horz];
                    }
                }

                //Map override
                if (wall.preset.mapOverride.Count > 0)
                {
                    writeTex = wall.preset.mapOverride[horz];
                }

                //Copy texture from wall...
                if (wall.wallOffset.x < 0)
                {
                    wallLeft = true;
                    Color[] wallTex = writeTex.GetPixels(0, 0, wallWidth, MapController.Instance.wallTexture.height);
                    tex.SetPixels((int)withinImage.x, (int)withinImage.y, wallWidth, MapController.Instance.wallTexture.height, wallTex);
                }
                else if (wall.wallOffset.x > 0)
                {
                    wallRight = true;
                    Color[] wallTex = writeTex.GetPixels(MapController.Instance.wallTexture.width - wallWidth, 0, wallWidth, MapController.Instance.wallTexture.height);
                    tex.SetPixels((int)withinImage.x + MapController.Instance.wallTexture.width - wallWidth, (int)withinImage.y, wallWidth, MapController.Instance.wallTexture.height, wallTex);
                }
                else if (wall.wallOffset.y < 0)
                {
                    wallDown = true;
                    Color[] wallTex = writeTex.GetPixels(0, 0, MapController.Instance.wallTexture.width, wallWidth);
                    tex.SetPixels((int)withinImage.x, (int)withinImage.y, MapController.Instance.wallTexture.width, wallWidth, wallTex);
                }
                else if (wall.wallOffset.y > 0)
                {
                    wallUp = true;
                    Color[] wallTex = writeTex.GetPixels(0, MapController.Instance.wallTexture.height - wallWidth, MapController.Instance.wallTexture.width, wallWidth);
                    tex.SetPixels((int)withinImage.x, (int)withinImage.y + MapController.Instance.wallTexture.height - wallWidth, MapController.Instance.wallTexture.width, wallWidth, wallTex);
                }
            }

            //Overwrite corners. This means you can use custom verson of the walls above for things like dividers, entrances, windows etc.
            if (wallLeft && wallDown)
            {
                Color[] wallTex = MapController.Instance.wallTexture.GetPixels(0, 0, MapController.Instance.wallWidth, MapController.Instance.wallWidth);
                tex.SetPixels((int)withinImage.x, (int)withinImage.y, MapController.Instance.wallWidth, MapController.Instance.wallWidth, wallTex);
            }

            if (wallLeft && wallUp)
            {
                Color[] wallTex = MapController.Instance.wallTexture.GetPixels(0, MapController.Instance.wallTexture.height - MapController.Instance.wallWidth, MapController.Instance.wallWidth, MapController.Instance.wallWidth);
                tex.SetPixels((int)withinImage.x, (int)withinImage.y + MapController.Instance.wallTexture.height - MapController.Instance.wallWidth, MapController.Instance.wallWidth, MapController.Instance.wallWidth, wallTex);
            }

            if (wallRight && wallDown)
            {
                Color[] wallTex = MapController.Instance.wallTexture.GetPixels(MapController.Instance.wallTexture.width - MapController.Instance.wallWidth, 0, MapController.Instance.wallWidth, MapController.Instance.wallWidth);
                tex.SetPixels((int)withinImage.x + MapController.Instance.wallTexture.width - MapController.Instance.wallWidth, (int)withinImage.y, MapController.Instance.wallWidth, MapController.Instance.wallWidth, wallTex);
            }

            if (wallRight && wallUp)
            {
                Color[] wallTex = MapController.Instance.wallTexture.GetPixels(MapController.Instance.wallTexture.width - MapController.Instance.wallWidth, MapController.Instance.wallTexture.height - MapController.Instance.wallWidth, MapController.Instance.wallWidth, MapController.Instance.wallWidth);
                tex.SetPixels((int)withinImage.x + MapController.Instance.wallTexture.width - MapController.Instance.wallWidth, (int)withinImage.y + MapController.Instance.wallTexture.height - MapController.Instance.wallWidth, MapController.Instance.wallWidth, MapController.Instance.wallWidth, wallTex);
            }

            //Outside corners
            if (!wallLeft && !wallDown)
            {
                NewNode found1;
                NewNode found2;

                //Check for nodes on left and down
                if (PathFinder.Instance.nodeMap.TryGetValue(node.nodeCoord + new Vector3Int(-1, 0, 0), out found1))
                {
                    if (PathFinder.Instance.nodeMap.TryGetValue(node.nodeCoord + new Vector3Int(0, -1, 0), out found2))
                    {
                        if (found1.walls.Exists(item => item.wallOffset.y < 0 && (item.node.room.explorationLevel >= 1 || item.otherWall.node.room.explorationLevel >= 1 || item.node.gameLocation != item.otherWall.node.gameLocation)))
                        {
                            if (found2.walls.Exists(item => item.wallOffset.x < 0 && (item.node.room.explorationLevel >= 1 || item.otherWall.node.room.explorationLevel >= 1 || item.node.gameLocation != item.otherWall.node.gameLocation)))
                            {
                                Color[] wallTex = MapController.Instance.wallTexCorners.GetPixels(0, 0, MapController.Instance.wallWidth, MapController.Instance.wallWidth);
                                tex.SetPixels((int)withinImage.x, (int)withinImage.y, MapController.Instance.wallWidth, MapController.Instance.wallWidth, wallTex);
                            }
                        }
                    }
                }
            }

            if (!wallLeft && !wallUp)
            {
                NewNode found1;
                NewNode found2;

                //Check for nodes on left and down
                if (PathFinder.Instance.nodeMap.TryGetValue(node.nodeCoord + new Vector3Int(-1, 0, 0), out found1))
                {
                    if (PathFinder.Instance.nodeMap.TryGetValue(node.nodeCoord + new Vector3Int(0, 1, 0), out found2))
                    {
                        if (found1.walls.Exists(item => item.wallOffset.y > 0 && (item.node.room.explorationLevel >= 1 || item.otherWall.node.room.explorationLevel >= 1 || item.node.gameLocation != item.otherWall.node.gameLocation)))
                        {
                            if (found2.walls.Exists(item => item.wallOffset.x < 0 && (item.node.room.explorationLevel >= 1 || item.otherWall.node.room.explorationLevel >= 1 || item.node.gameLocation != item.otherWall.node.gameLocation)))
                            {
                                Color[] wallTex = MapController.Instance.wallTexCorners.GetPixels(0, MapController.Instance.wallTexture.height - MapController.Instance.wallWidth, MapController.Instance.wallWidth, MapController.Instance.wallWidth);
                                tex.SetPixels((int)withinImage.x, (int)withinImage.y + MapController.Instance.wallTexture.height - MapController.Instance.wallWidth, MapController.Instance.wallWidth, MapController.Instance.wallWidth, wallTex);
                            }
                        }
                    }
                }
            }

            if (!wallRight && !wallDown)
            {
                NewNode found1;
                NewNode found2;

                //Check for nodes on left and down
                if (PathFinder.Instance.nodeMap.TryGetValue(node.nodeCoord + new Vector3Int(1, 0, 0), out found1))
                {
                    if (PathFinder.Instance.nodeMap.TryGetValue(node.nodeCoord + new Vector3Int(0, -1, 0), out found2))
                    {
                        if (found1.walls.Exists(item => item.wallOffset.y < 0 && (item.node.room.explorationLevel >= 1 || item.otherWall.node.room.explorationLevel >= 1 || item.node.gameLocation != item.otherWall.node.gameLocation)))
                        {
                            if (found2.walls.Exists(item => item.wallOffset.x > 0 && (item.node.room.explorationLevel >= 1 || item.otherWall.node.room.explorationLevel >= 1 || item.node.gameLocation != item.otherWall.node.gameLocation)))
                            {
                                Color[] wallTex = MapController.Instance.wallTexCorners.GetPixels(MapController.Instance.wallTexture.width - MapController.Instance.wallWidth, 0, MapController.Instance.wallWidth, MapController.Instance.wallWidth);
                                tex.SetPixels((int)withinImage.x + MapController.Instance.wallTexture.width - MapController.Instance.wallWidth, (int)withinImage.y, MapController.Instance.wallWidth, MapController.Instance.wallWidth, wallTex);
                            }
                        }
                    }
                }
            }

            if (!wallRight && !wallUp)
            {
                NewNode found1;
                NewNode found2;

                //Check for nodes on left and down
                if (PathFinder.Instance.nodeMap.TryGetValue(node.nodeCoord + new Vector3Int(1, 0, 0), out found1))
                {
                    if (PathFinder.Instance.nodeMap.TryGetValue(node.nodeCoord + new Vector3Int(0, 1, 0), out found2))
                    {
                        if (found1.walls.Exists(item => item.wallOffset.y > 0 && (item.node.room.explorationLevel >= 1 || item.otherWall.node.room.explorationLevel >= 1 || item.node.gameLocation != item.otherWall.node.gameLocation)))
                        {
                            if (found2.walls.Exists(item => item.wallOffset.x > 0 && (item.node.room.explorationLevel >= 1 || item.otherWall.node.room.explorationLevel >= 1 || item.node.gameLocation != item.otherWall.node.gameLocation)))
                            {
                                Color[] wallTex = MapController.Instance.wallTexCorners.GetPixels(MapController.Instance.wallTexture.width - MapController.Instance.wallWidth, MapController.Instance.wallTexture.height - MapController.Instance.wallWidth, MapController.Instance.wallWidth, MapController.Instance.wallWidth);
                                tex.SetPixels((int)withinImage.x + MapController.Instance.wallTexture.width - MapController.Instance.wallWidth, (int)withinImage.y + MapController.Instance.wallTexture.height - MapController.Instance.wallWidth, MapController.Instance.wallWidth, MapController.Instance.wallWidth, wallTex);
                            }
                        }
                    }
                }
            }
        }

        //Write furniture graphics (later layer)
        foreach(NewRoom room in gameLocation.rooms)
        {
            if(room.explorationLevel >= 2)
            {
                //Furniture
                foreach (FurnitureLocation furn in room.individualFurniture)
                {
                    if (furn.furniture.map != null)
                    {
                        if (furn.furniture.drawUnderWalls) continue; //Draw this under walls

                        Texture2D furnTex = furn.furniture.map;

                        //Handle this using the true centre of the furniture in terms of node position
                        Vector3 avNodeCoord = Vector3.zero;

                        foreach (NewNode furnNode in furn.coversNodes)
                        {
                            avNodeCoord += furnNode.nodeCoord;
                        }

                        int angle = 0;

                        if(!furn.furniture.ignoreDirection)
                        {
                            //Instance a version of this texture and rotate it (only if this needs rotating)
                            //Caculate angle
                            Vector3 angleCalc = furn.anchorNode.room.transform.TransformDirection(new Vector3(0, furn.angle + furn.diagonalAngle, 0));
                            angle = Mathf.RoundToInt(angleCalc.y);

                            if(angle != 0) furnTex = RotateTexture(furnTex, -angle * furn.scaleMultiplier.x);
                        }

                        Vector2 furnMapCoord = MapController.Instance.NodeCoordToMap(avNodeCoord / (float)furn.coversNodes.Count);
                        Vector2 furnWithinImage = (furnMapCoord - rect.anchoredPosition - new Vector2(furnTex.width * 0.5f, furnTex.height * 0.5f)) / MapController.Instance.mapResolutionDivision;

                        //Write texture including transparency
                        for (int i = 0; i < furnTex.width; i++)
                        {
                            for (int u = 0; u < furnTex.height; u++)
                            {
                                Color setCol = furnTex.GetPixel(i, u);

                                //Skip pixel if fully transparent...
                                if (setCol.a <= 0.001f) continue;

                                Color destCol = tex.GetPixel((int)furnWithinImage.x + i, (int)furnWithinImage.y + u);
                                setCol = setCol * setCol.a + destCol * (1f - setCol.a); //Apply with alpha
                                tex.SetPixel((int)furnWithinImage.x + i, (int)furnWithinImage.y + u, setCol);
                            }
                        }
                    }
                }
            }
        }

        tex.Apply(); //Apply changes

        UpdateIcon();
        UpdateTooltip();
    }

    public void UpdateIcon()
    {
        //Setup icon: Only if company/player apartment
        if (gameLocation.thisAsAddress != null)
        {
            if (gameLocation.thisAsAddress.addressPreset != null && gameLocation.evidenceEntry != null && (gameLocation.thisAsAddress.addressPreset.company != null || gameLocation == Player.Instance.home || Player.Instance.apartmentsOwned.Contains(gameLocation)))
            {
                if (gameLocation.evidenceEntry.keyTies[Evidence.DataKey.location].Contains(Evidence.DataKey.purpose))
                {
                    if (!typeIcon.enabled) typeIcon.enabled = true;
                    typeIcon.sprite = gameLocation.evidenceEntry.iconSprite;
                    typeIcon.GetComponent<RectTransform>().sizeDelta = new Vector2(32, 32);
                }
                else
                {
                    typeIcon.enabled = false;
                }
            }
            else
            {
                typeIcon.enabled = false;
            }
        }
        else
        {
            typeIcon.enabled = false;
        }

        typeIcon.transform.SetAsLastSibling();
    }

    public void UpdateTooltip()
    {
        if (gameLocation.evidenceEntry != null)
        {
            tooltip.mainText = gameLocation.evidenceEntry.GetNameForDataKey(Evidence.DataKey.location);
            tooltip.detailText = gameLocation.evidenceEntry.GetNoteComposed((new Evidence.DataKey[] { Evidence.DataKey.location}).ToList(), false);
        }

        tooltip.parentOverride = MapController.Instance.tooltipOverride;
    }

    Texture2D RotateTexture(Texture2D tex, float angle)
    {
        Texture2D rotImage = new Texture2D(tex.width, tex.height);
        int x, y;
        float x1, y1, x2, y2;

        int w = tex.width;
        int h = tex.height;
        float x0 = Rot_x(angle, -w / 2.0f, -h / 2.0f) + w / 2.0f;
        float y0 = Rot_y(angle, -w / 2.0f, -h / 2.0f) + h / 2.0f;

        float dx_x = Rot_x(angle, 1.0f, 0.0f);
        float dx_y = Rot_y(angle, 1.0f, 0.0f);
        float dy_x = Rot_x(angle, 0.0f, 1.0f);
        float dy_y = Rot_y(angle, 0.0f, 1.0f);

        x1 = x0;
        y1 = y0;

        for (x = 0; x < tex.width; x++)
        {
            x2 = x1;
            y2 = y1;
            for (y = 0; y < tex.height; y++)
            {
                x2 += dx_x;//rot_x(angle, x1, y1);
                y2 += dx_y;//rot_y(angle, x1, y1);
                rotImage.SetPixel((int)Mathf.Floor(x), (int)Mathf.Floor(y), GetPixel(tex, x2, y2));
            }

            x1 += dy_x;
            y1 += dy_y;
        }

        //rotImage.Apply();
        return rotImage;
    }

    private Color GetPixel(Texture2D tex, float x, float y)
    {
        Color pix;
        int x1 = (int)Mathf.Floor(x);
        int y1 = (int)Mathf.Floor(y);

        if (x1 > tex.width || x1 < 0 ||
           y1 > tex.height || y1 < 0)
        {
            pix = Color.clear;
        }
        else
        {
            pix = tex.GetPixel(x1, y1);
        }

        return pix;
    }

    private float Rot_x(float angle, float x, float y)
    {
        float cos = Mathf.Cos(angle / 180.0f * Mathf.PI);
        float sin = Mathf.Sin(angle / 180.0f * Mathf.PI);
        return (x * cos + y * (-sin));
    }
    private float Rot_y(float angle, float x, float y)
    {
        float cos = Mathf.Cos(angle / 180.0f * Mathf.PI);
        float sin = Mathf.Sin(angle / 180.0f * Mathf.PI);
        return (x * sin + y * cos);
    }

    public override void OnHoverStart()
    {
        generatedImage.color = MapController.Instance.roomBaseColor + MapController.Instance.highlightedColourAdditive;
    }

    public override void OnHoverEnd()
    {
        generatedImage.color = MapController.Instance.roomBaseColor;
    }

    //Open address file
    public override void OnLeftDoubleClick()
    {
        //Centre
        MapController.Instance.CentreOnObject(rect, false);

        InterfaceController.Instance.SpawnWindow(gameLocation.evidenceEntry, passedEvidenceKey: Evidence.DataKey.location);
    }

    //Open map context menu
    public override void OnRightClick()
    {
        MapController.Instance.mapContextMenu.OpenMenu();
    }

    //Commands
    public void OpenFolder()
    {
        InterfaceController.Instance.SpawnWindow(gameLocation.evidenceEntry, passedEvidenceKey: Evidence.DataKey.location);
    }

    public void MapRoute()
    {

    }

    public void DoFastTravel()
    {
        //FastTravel.Instance.ExecuteFastTravel(gameLocation.anchorNode);
    }

    public void DoFastTravelBuilding()
    {
        //FastTravel.Instance.ExecuteFastTravel(gameLocation.building.mainEntrance.node);
    }
}
