﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using NaughtyAttributes;
using FMODUnity;
using FMOD.Studio;
using System;

public class AudioController : MonoBehaviour
{
    public StudioListener playerListener;

    [Header("Misc. Settings")]
    [Tooltip("Speed of sound in unity (m) per (in-game) second")]
    public float speedOfSound = 343f;

    [Header("Occlusion: Modifiers")]
    [Space(7)]
    [Tooltip("Each occlusion unit will decrease volume by this amount...")]
    public float occlusionUnitVolumeModifier = -0.1f;
    [Range(0, 10)]
    public int openDoorOcclusionUnits = 0;
    [Range(0, 10)]
    public int closedDoorOcclusionUnits = 5;
    [Range(0, 10)]
    public int windowOcclusionUnits = 4;
    [Range(0, 10)]
    public int wallOcclusionUnits = 7;
    [Range(0, 10)]
    public int ceilingOcclusionUnits = 8;
    [Range(0, 10)]
    public int floorOcclusionUnits= 8;
    [Range(0, 10)]
    public int floorDifferenceOcclusionUnits = 2;
    [Space(5)]
    [Tooltip("Loop through this many rooms as a maximum...")]
    public int loopingMaximum = 32;
    [Tooltip("Sounds can travel this many rooms away from the source. After that they gain +1 occlusion unit per additional room.")]
    public int maxRoomDistance = 4;

    [Tooltip("The emulated rolloff of the sound")]
    public AnimationCurve emulationRolloff;
    [Tooltip("Sound needs to be playing at at least this volume for the AI to register it")]
    public float aiHearingThreshold = 0.1f;
    [Tooltip("Sound needs to be playing at at least this volume for the player to register it")]
    public float playerHearingThreshold = 0.1f;
    [Tooltip("A sound icon represents this much simulated range")]
    public float soundIconRangeUnit = 5f;

    [Header("Ambient Sound Properties")]
    [ReadOnly]
    public int updateClosestWindowTicker = 0;
    [ReadOnly]
    public int updateMixingTicker = 0;
    [ReadOnly]
    public float updateAmbientZonesTimer = 0;
    [Tooltip("Update closest windows and open ext door every X frames...")]
    public int updateClosestWindow = 8;
    [Tooltip("Update closest windows and open ext door every X frames...")]
    public int updateMixing = 2; //Update the closest window every x frames
    
    [Tooltip("Current closest window position.")]
    [ReadOnly]
    public Vector3 windowAudioPosition;
    [Tooltip("Distance from player to above.")]
    [ReadOnly]
    public float closestWindowDistance = 0f;
    [ReadOnly]
    [Tooltip("Normalized version of above")]
    public float closestWindowDistanceNormalized = 0f;
    [Tooltip("Window distance multiplier (used to create normalised variable)")]
    public float closestWindowDistanceMultiplier = 16f;
    [Tooltip("Curve used as a multiplier for the above, and based on how open the door is on X (0 = closed, 1 = open)")]
    public AnimationCurve openMultiplierCurve;

    [Space(5)]
    [Tooltip("Interpolated outdoors/indoors transition")]
    [ReadOnly]
    public float ventOutdoorsIndoors = 0f;
    [Tooltip("Distance to the nearest vent")]
    [ReadOnly]
    public float nearbyVent = 0f;

    [Tooltip("Current closest open external door position.")]
    [Space(5)]
    [ReadOnly]
    public Vector3 doorAudioPosition;
    [Tooltip("Distance from player to above.")]
    [ReadOnly]
    public float closestDoorDistance = 0f;
    [ReadOnly]
    [Tooltip("Normalized version of above")]
    public float closestDoorDistanceNormalized = 0f;
    [Tooltip("Window distance multiplier (used to create normalised variable)")]
    public float closestDoorDistanceMultiplier = 16f;

    [Space(5)]
    [Tooltip("Distance from an edge tile")]
    [ReadOnly]
    public float edgeDistance = 0f;
    [ReadOnly]
    [Tooltip("Normalized version of above")]
    public float edgeDistanceNormalized = 0f;
    [Tooltip("Edge distance multiplier (used to create normalised variable)")]
    public float edgeDistanceMultiplier = 30f;

    [Space(5)]
    [Tooltip("Distance from an exterior wall")]
    [ReadOnly]
    public float extWallDistance = 0f;
    [ReadOnly]
    [Tooltip("Normalized version of above")]
    public float extWallNormalized = 0f;
    [Tooltip("Edge distance multiplier (used to create normalised variable)")]
    public float extWallDistanceMultiplier = 16f;

    [Space(7)]
    [ReadOnly]
    public float passedWind = 0f;
    [ReadOnly]
    public float passedRain = 0f;
    [ReadOnly]
    public float passedCity = 0f;

    [BoxGroup("Ambient Zones")]
    public List<AmbientZoneInstance> ambientZones = new List<AmbientZoneInstance>();
    public Dictionary<AmbientZone, AmbientZoneInstance> ambientZoneReference = new Dictionary<AmbientZone, AmbientZoneInstance>();
    [Tooltip("As this is a 2D sound we need to apply volume falloff manually")]
    public AnimationCurve ambientFalloff;

    public LoopingSoundInfo ambienceWind;
    public LoopingSoundInfo ambienceRain;
    public LoopingSoundInfo ambiencePA;

    [System.NonSerialized]
    public AudioController.LoopingSoundInfo threatLoop = null;

    [System.Serializable]
    public class AmbientZoneInstance
    {
        public AmbientZone preset; //Preset data
        public float playerDistance = 0f;
        public int penetrationCount = 0;
        public NewRoom audibleRoom = null;
        public bool isActive = false; //Is a sound event active and playing?
        [Space(5)]
        public float desiredVolume = 0f; //Current desired volume
        public float actualVolume = 0f; //Applied volume
        [Space(5)]
        public float desiredWalla = 0f; //Current desired walla
        public float actualWalla = 0f; //Applied walla
        [NonSerialized]
        public LoopingSoundInfo eventData; //Loop data

        public HashSet<NewRoom> rooms = new HashSet<NewRoom>();

        public AmbientZoneInstance(AmbientZone newPreset)
        {
            preset = newPreset;
            AudioController.Instance.ambientZoneReference.Add(preset, this); //Setup reference dictionary
        }
    }

    //List of looping sounds
    [System.Serializable]
    public class LoopingSoundInfo
    {
        public string name;
        public bool init = false;
        public FMOD.Studio.EventInstance audioEvent;
        public bool isValid = false; //Is an audioevent current active?
        public FMOD.Studio.EventDescription description;
        public float volumeOverride = 1f;
        public AudioEvent eventPreset;
        public NewNode sourceLocation;
        public Actor who;
        [System.NonSerialized]
        public Interactable interactable;
        public bool forceSuspicious;
        public List<FMODParam> parameters;
        public float lastUpdated;
        public int currentOcclusion;
        //public bool pauseWhenGamePaused;
        public Vector3 worldPos;
        public FMOD.Studio.PLAYBACK_STATE state;
        public List<NewRoom> audibleRooms = new List<NewRoom>();
        public bool isBroadcast = false;
        public float occlusionVolume = 0f;
        public float fadeToVolume = 0f;
        public bool isActive = true; //True until told to stop
        public string debugStoppedReason;
        public InteractablePreset.IfSwitchStateSFX interactableLoopInfo;

        public FMOD.Studio.PLAYBACK_STATE UpdatePlayState()
        {
            audioEvent.getPlaybackState(out state);

            //if(this == AudioController.Instance.ambienceRain)
            //{
            //    if(state== PLAYBACK_STATE.STOPPED)
            //    {
            //        Game.LogError("RAIN HAS STOPPED!");
            //    }
            //}

            return state;
        }

        public void UpdateWorldPosition(Vector3 newWorldPos, NewNode newNodePos)
        {
            worldPos = newWorldPos;

            if(newNodePos == null)
            {
                //Calculate node positon from real pos
                Vector3Int nodeCoord = CityData.Instance.RealPosToNodeInt(worldPos);

                if(!PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out newNodePos))
                {
                    newNodePos = sourceLocation;
                }
            }

            sourceLocation = newNodePos;

            if (eventPreset.debug) Game.Log("Audio: Updating sound position of " + eventPreset.name + " to " + newWorldPos + " and sourcelocation node " + sourceLocation.position);

            UpdateOcclusion(true);
        }

        //Update looping sound occlusion
        public void UpdateOcclusion(bool ignoreLastUpdateTime = false)
        {
            if (sourceLocation == null) return; //This is a 2D sound
            if (SessionData.Instance.isFloorEdit) return;
            if (!SessionData.Instance.startedGame || CityConstructor.Instance.preSimActive) return; //Don't do this if we haven't started yet...
            if (!ignoreLastUpdateTime && lastUpdated >= SessionData.Instance.gameTime - 0.0005f)
            {
                if (eventPreset.debug) Game.Log("Audio: Cancelling occlusion update for " + eventPreset.name + " has it has been updated recently...");
                return; //Already updated very recently
            }

            //Set position
            if (interactable != null)
            {
                worldPos = interactable.GetWorldPosition();
                //FMODUnity.RuntimeManager.AttachInstanceToGameObject(audioEvent, interactable.controller.transform, new Rigidbody());
            }

            occlusionVolume = volumeOverride;
            int occlusionPenetration = 0;

            //Maximum distance for a sound with occlusion is 30m. If this is => then don't run
            List<ActiveListener> activeListeners = null;
            List<NewRoom> newAudibleRooms = null;
            bool isSuspicious = forceSuspicious;

            //Maximum distance for a sound with occlusion is 20m. If this is => then don't run
            float distance = Vector3.Distance(Player.Instance.transform.position, sourceLocation.position);

            //Hard coded limit of 30m
            if (distance <= 30)
            {
                if (eventPreset.disableOcclusion)
                {
                    occlusionVolume = 1f;
                }
                else
                {
                    occlusionVolume = AudioController.Instance.GetOcculusion(Player.Instance.currentNode, sourceLocation, eventPreset, occlusionVolume, who, null, out occlusionPenetration, out activeListeners, out isSuspicious, out newAudibleRooms, out _, forceSuspicious: forceSuspicious);

                    if (eventPreset.debug) Game.Log("Audio: Updated occlusion for sound " + eventPreset.name + " with volume " + occlusionVolume);

                    if (isSuspicious)
                    {
                        if (newAudibleRooms != null)
                        {
                            //Update audible rooms
                            for (int i = 0; i < audibleRooms.Count; i++)
                            {
                                NewRoom existingRoom = audibleRooms[i];

                                //Does the new list of audible rooms contain this existing?
                                if (newAudibleRooms.Contains(existingRoom))
                                {
                                    newAudibleRooms.Remove(existingRoom);
                                }
                                //If not in the new list, it is not required...
                                else
                                {
                                    existingRoom.audibleLoopingSounds.Remove(this);
                                    audibleRooms.RemoveAt(i);
                                    i--;
                                    continue;
                                }
                            }

                            while (newAudibleRooms.Count > 0)
                            {
                                newAudibleRooms[0].audibleLoopingSounds.Add(this);
                                audibleRooms.Add(newAudibleRooms[0]);
                                newAudibleRooms.RemoveAt(0);
                            }
                        }

                        if (who != null)
                        {
                            foreach (ActiveListener a in activeListeners)
                            {
                                if (a.listener != who) a.listener.HearIllegal(eventPreset, sourceLocation, sourceLocation.position, null, a.escalationLevel);
                            }
                        }
                    }
                }
            }
            else
            {
                occlusionVolume = 0;
                activeListeners = new List<ActiveListener>();
                newAudibleRooms = new List<NewRoom>();
                return;
            }

            //Override master volume with occlusion volume * overrides
            float vol = Mathf.Clamp01(occlusionVolume * eventPreset.masterVolumeScale);

            UpdatePlayState();

            isValid = audioEvent.isValid();

            //Start or stop looping event
            if(isActive && vol > 0.01f && (state == PLAYBACK_STATE.STOPPED /*|| state == PLAYBACK_STATE.STOPPING*/ || !isValid))
            {
                if(!isValid)
                {
                    if (eventPreset.debug)
                    {
                        Game.Log("Audio: Starting/restarting looping sound " + eventPreset.name + " at " + worldPos + " because volume is now above minimum threshold. Prev state: " + state.ToString() + ", vol: " + vol);
                    }

                    FMOD.GUID fmodEvent = FMOD.GUID.Parse(eventPreset.guid);
                    FMODUnity.RuntimeManager.StudioSystem.getEventByID(fmodEvent, out description);

                    //Setup event instance
                    try
                    {
                        audioEvent = FMODUnity.RuntimeManager.CreateInstance(fmodEvent);
                    }
                    catch
                    {
                        Game.Log("Audio: Cannot find audio event " + eventPreset.guid + " skipping...");
                    }
                }
                else if(state == PLAYBACK_STATE.STOPPED)
                {
                    if (eventPreset.debug)
                    {
                        Game.Log("Audio: Resuming a previously stopped looping sound " + eventPreset.name + " at " + worldPos + " because volume is now above minimum threshold. Prev state: " + state.ToString() + ", vol: " + vol);
                    }
                }

                //If broadcast, set to correct position
                if (isBroadcast)
                {
                    audioEvent.setTimelinePosition(Mathf.RoundToInt(SessionData.Instance.currentShowProgressSeconds * 1000));
                }

                //Play
                audioEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(worldPos));

                if(eventPreset.debug)
                {
                    Game.Log("Audio: Start event: " + eventPreset.name);
                }

                audioEvent.start();
                SetVolumeFadeTo(vol);

                //Start paused if game is paused
                //if (!SessionData.Instance.play && pauseWhenGamePaused)
                //{
                //    audioEvent.setPaused(true);
                //}
            }
            else if(!isActive || vol <= 0.01f)
            {
                if (eventPreset.debug) Game.Log("Audio: Stopping looping sound " + eventPreset.name + " at " + worldPos + " because volume is now below minimum threshold");

                audioEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
                audioEvent.release();
            }
            else
            {
                if (who != null)
                {
                    //Set stealth: -1 = running, 0 = normal, 1 = stealth
                    if (who.isRunning) audioEvent.setParameterByName("Stealth", -1f);
                    else if (!who.stealthMode) audioEvent.setParameterByName("Stealth", 0f);
                    else audioEvent.setParameterByName("Stealth", 1f);
                }

                audioEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(worldPos));

                //Set parameters
                audioEvent.setParameterByName("Occlusion", occlusionPenetration);

                if (parameters != null)
                {
                    foreach (FMODParam p in parameters)
                    {
                        audioEvent.setParameterByName(p.name, p.value);
                    }
                }

                //Override master volume with occlusion volume * overrides
                SetVolumeFadeTo(vol);
            }

            lastUpdated = SessionData.Instance.gameTime;

            UpdatePlayState();
            isValid = audioEvent.isValid();

            currentOcclusion = occlusionPenetration;

            //Force outline if player is illegal
            if ((who == null || !who.isPlayer) && eventPreset.forceOutlineForLoopIfPlayerTrespassing)
            {
                if(state == PLAYBACK_STATE.STOPPED)
                {
                    AudioController.Instance.ForceOutlineCheck(eventPreset, interactable, true);
                }
                else AudioController.Instance.ForceOutlineCheck(eventPreset, interactable);
            }
        }

        public void SetVolumeImmediate(float vol)
        {
            Game.Log("Audio: Setting new volume to sound " + eventPreset.name + " (immediate) of " + vol);

            //Override master volume with occlusion volume * overrides
            fadeToVolume = vol;
            audioEvent.setVolume(vol);
            AudioController.Instance.volumeChangingSounds.Remove(this);
        }

        public void SetVolumeFadeTo(float vol)
        {
            fadeToVolume = vol;

            if(!AudioController.Instance.volumeChangingSounds.Contains(this))
            {
                AudioController.Instance.volumeChangingSounds.Add(this);
            }
        }
    }

    public class ActiveListener
    {
        public Actor listener;
        public float soundLevel = 0;
        public int escalationLevel = 0;
    }

    public List<LoopingSoundInfo> loopingSounds = new List<LoopingSoundInfo>(); //Store currently looping sounds so we can update their occlusion
    public HashSet<LoopingSoundInfo> volumeChangingSounds = new HashSet<LoopingSoundInfo>();

    //List of delayed sounds
    [System.Serializable]
    public class DelayedSoundInfo
    {
        public float delay = 0f;
        public AudioEvent eventPreset;
        public Actor who;
        public NewNode location;
        public Vector3 worldPosition;
        public List<FMODParam> parameters = null;
        public float volumeOverride = 1f;
        public List<NewNode> additionalSources = null;
        public bool forceIgnoreOcclusion = false;
        public bool is2D = false;

        public DelayedSoundInfo(float newDelay, AudioEvent newEventPreset, Actor newWho, NewNode newLocation, Vector3 newWorldPosition, List<FMODParam> newParameters = null, float newVolumeOverride = 1f, List<NewNode> newAdditionalSources = null, bool newForceIgnoreOcclusion = false, bool newIs2D = false)
        {
            delay = newDelay;
            eventPreset = newEventPreset;
            who = newWho;
            location = newLocation;
            worldPosition = newWorldPosition;
            parameters = newParameters;
            volumeOverride = newVolumeOverride;
            additionalSources = newAdditionalSources;
            forceIgnoreOcclusion = newForceIgnoreOcclusion;
            is2D = newIs2D;
        }
    }

    [System.Serializable]
    public class SoundMaterialOverride
    {
        public float concrete = 0f;
        public float wood = 0f;
        public float carpet = 0f;
        public float tile = 0f;
        public float plaster = 0f;
        public float fabric = 0f;
        public float metal = 0f;
        public float glass = 0f;

        public SoundMaterialOverride(float newConcrete, float newWood, float newCarpet, float newTile, float newPlaster, float newFabric, float newMetal, float newGlass)
        {
            concrete = newConcrete;
            wood = newWood;
            carpet = newCarpet;
            tile = newTile;
            plaster = newPlaster;
            fabric = newFabric;
            metal = newMetal;
            glass = newGlass;
        }
    }

    public List<DelayedSoundInfo> delayedSound = new List<DelayedSoundInfo>();

    //Pre-calculate this for extra speed
    public int footstepLayerMask = 0;

    //AI reaction
    //None - no reaction
    //Suspicious - form memory
    //Highly suspicious - investigate
    //Danger - Run & hide
    public enum CitizenReaction { investigate, immediatePersue, alarm};

    //Footsteps
    public enum SurfaceType { concrete, woodenFloor, tile, carpet};

    //Stop
    public enum StopType { immediate, fade, triggerCue};

    Action updateAmbientZonesAction;

    //Singleton pattern
    private static AudioController _instance;
    public static AudioController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        //Setup update ambient zones action
        updateAmbientZonesAction += UpdateAmbientZones;

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        //Pre-calc layer mask for footsteps
        footstepLayerMask = Toolbox.Instance.CreateLayerMask(Toolbox.LayerMaskMode.castAllExcept, 2, 18, 20, 24, 30);

        //Load all ambient zones
        foreach(AmbientZone amb in Toolbox.Instance.allAmbientZones)
        {
            ambientZones.Add(new AmbientZoneInstance(amb));
        }
    }

    public struct FMODParam
    {
        public string name;
        public float value;
    }

    //Run every x frames
    public void UpdateMixing()
    {
        //Update vent indoor/outdoor
        if (Player.Instance.inAirVent)
        {
            UpdateVentIndoorOutdoor();
            UpdateDistanceToVent();
        }

        //Set ambient zone volumes and stop/start
        foreach (AmbientZoneInstance amb in ambientZones)
        {
            //Move towards desired volume
            bool updateStopStart = false;

            if(amb.actualVolume < amb.desiredVolume)
            {
                amb.actualVolume += Time.deltaTime * updateMixing;
                amb.actualVolume = Mathf.Min(amb.actualVolume, amb.desiredVolume);
                updateStopStart = true;
            }
            else if(amb.actualVolume > amb.desiredVolume)
            {
                amb.actualVolume -= Time.deltaTime * updateMixing;
                amb.actualVolume = Mathf.Max(amb.actualVolume, amb.desiredVolume);
                updateStopStart = true;
            }

            //Stop sounds
            if(updateStopStart)
            {
                if (amb.actualVolume <= 0.01f)
                {
                    if(amb.eventData != null)
                    {
                        amb.eventData.UpdatePlayState();

                        if(amb.eventData.state == PLAYBACK_STATE.PLAYING || amb.eventData.state == PLAYBACK_STATE.STARTING || amb.eventData.state == PLAYBACK_STATE.SUSTAINING)
                        {
                            StopSound(amb.eventData.audioEvent, StopType.immediate);
                        }
                    }
                }
                else
                {
                    bool createNew = false;

                    if (amb.eventData != null)
                    {
                        amb.eventData.UpdatePlayState();

                        if (amb.eventData.state == PLAYBACK_STATE.STOPPED || amb.eventData.state == PLAYBACK_STATE.STOPPING)
                        {
                            StopSound(amb.eventData.audioEvent, StopType.immediate);
                            amb.eventData = null;
                            createNew = true;
                        }
                        //Pass params
                        else
                        {
                            amb.eventData.audioEvent.setVolume(amb.actualVolume);
                        }
                    }

                    if(amb.eventData == null || createNew)
                    {
                        amb.eventData = Play2DLooping(amb.preset.mainEvent, null, amb.actualVolume);
                    }
                }
            }

            //Update params
            if(amb.eventData != null && (amb.eventData.state == PLAYBACK_STATE.PLAYING || amb.eventData.state == PLAYBACK_STATE.STARTING || amb.eventData.state == PLAYBACK_STATE.SUSTAINING))
            {
                //Setup params...
                bool passParams = false;

                if (amb.preset.passWalla)
                {
                    if(amb.audibleRoom != null)
                    {
                        int wallaCount = 0;

                        foreach(Actor a in CityData.Instance.visibleActors)
                        {
                            if (a.isDead || a.isAsleep || a.isStunned) continue;
                            if (a.isPlayer) continue;
                            if (a.lookAtThisTransform == null) continue;
                            if (a.isOnStreet != Player.Instance.isOnStreet) continue;

                            if(Vector3.Distance(CameraController.Instance.cam.transform.position, a.lookAtThisTransform.position) <= amb.preset.maxWallaRange)
                            {
                                wallaCount++;
                            }
                        }

                        amb.desiredWalla = Mathf.Clamp01((float)wallaCount / amb.preset.maxWallaCrowd);
                        //Game.Log("Walla: " + amb.preset.name + ": " + wallaCount + "/" + amb.preset.maxWallaCrowd + " range: " + amb.preset.maxWallaRange + " = " + amb.desiredWalla);
                    }
                    else
                    {
                        amb.desiredWalla = 0f;
                    }

                    if (amb.actualWalla < amb.desiredWalla)
                    {
                        amb.actualWalla += Time.deltaTime * updateMixing * 0.33f;
                        amb.actualWalla = Mathf.Min(amb.actualWalla, amb.desiredWalla);
                        passParams = true;
                    }
                    else if (amb.actualWalla > amb.desiredWalla)
                    {
                        amb.actualWalla -= Time.deltaTime * updateMixing * 0.33f;
                        amb.actualWalla = Mathf.Max(amb.actualWalla, amb.desiredWalla);
                        passParams = true;
                    }
                }

                if(passParams || amb.preset.passTimeOfDay)
                {
                    if (amb.preset.passTimeOfDay) amb.eventData.audioEvent.setParameterByName("TimeOfDay", SessionData.Instance.dayProgress);
                    if (amb.preset.passWalla) amb.eventData.audioEvent.setParameterByName("Walla", amb.actualWalla);
                }

                if (amb.preset.passPlayerInVent)
                {
                    float inVent = 0;
                    if (Player.Instance.inAirVent) inVent = 1;
                    amb.eventData.audioEvent.setParameterByName("PlayerInVent", inVent);
                }

                if (amb.preset.passPlayerVentExtInt)
                {
                    amb.eventData.audioEvent.setParameterByName("PlayerVentExtInt", AudioController.Instance.ventOutdoorsIndoors);
                    //Game.Log("Pass vent ext/int: " + AudioController.Instance.ventOutdoorsIndoors);
                }

                if(amb.preset.passDistanceToVent)
                {
                    amb.eventData.audioEvent.setParameterByName("DistanceToVent", AudioController.Instance.nearbyVent);
                }

                if (amb.preset.passRain)
                {
                    amb.eventData.audioEvent.setParameterByName("Rain", SessionData.Instance.currentRain);
                }

                if (amb.preset.passBasement)
                {
                    float basement = 0;

                    if (Player.Instance.currentNode != null && Player.Instance.currentNode.nodeCoord.z < 0)
                    {
                        basement = Mathf.Clamp01(Mathf.Abs(Player.Instance.transform.position.y) / 9.9f);
                    }

                    amb.eventData.audioEvent.setParameterByName("Basement", basement);
                }

                if (amb.preset.passHeightWindSpeed)
                {
                    float heightWindThreshold = Mathf.Clamp01(Player.Instance.transform.position.y * 0.04f); //Play max wind @ 25m up
                    float heightWS = Mathf.Clamp01(Mathf.Max(SessionData.Instance.currentWind, heightWindThreshold));

                    amb.eventData.audioEvent.setParameterByName("HeightWindSpeed", heightWS);
                }
            }
        }
    }

    //Start ambience tracks
    public void StartAmbienceTracks()
    {
        if (ambienceWind != null) StopSound(ambienceWind, StopType.immediate, "Init ambience");
        if (ambienceRain != null) StopSound(ambienceRain, StopType.immediate, "Init ambience");
        if (ambiencePA != null) StopSound(ambiencePA, StopType.immediate, "Init ambience");

        ambienceWind = Play2DLooping(AudioControls.Instance.ambienceWind);
        ambienceRain = Play2DLooping(AudioControls.Instance.ambienceRain);
        ambiencePA = Play2DLooping(AudioControls.Instance.ambiencePA);
    }

    //Because this happens so often, don't use paramters here.
    public bool PlayWorldFootstep(AudioEvent eventPreset, Actor actor, bool rightFoot = false)
    {
        if (eventPreset == null)
        {
            return false;
        }
        else if(!SessionData.Instance.play)
        {
            return false;
        }
        else if (SessionData.Instance.currentTimeSpeed != SessionData.TimeSpeed.normal)
        {
            return false; //Only play on normal game speed
        }
        else if(actor.currentCityTile == null)
        {
            return false;
        }
        else if(!actor.currentCityTile.isInPlayerVicinity)
        {
            return false; //Don't play if outside of vicinity
        }
        else if(!SessionData.Instance.startedGame)
        {
            return false;
        }
        else if(CityConstructor.Instance != null && CityConstructor.Instance.preSimActive)
        {
            return false;
        }
        else if(eventPreset.disabled)
        {
            return false;
        }
        else if(eventPreset.isLicensed && !Game.Instance.allowLicensedMusic)
        {
            return false;
        }

        //Intercept to replace with wading...
        if(actor != null && actor.transform.position.y < CityControls.Instance.basementWaterLevel)
        {
            if(actor.isPlayer)
            {
                eventPreset = AudioControls.Instance.playerWaterWade;

                Player.Instance.AddHygiene(-0.05f); //Make smelly!
            }
            else
            {
                eventPreset = AudioControls.Instance.footstepWaterWade;
            }
        }

        if(eventPreset.guid.Length <= 0)
        {
            Game.LogError("Invalid footstep GUID for " + eventPreset.name);
            return false;
        }

        FMOD.GUID fmodEvent = FMOD.GUID.Parse(eventPreset.guid);
        FMOD.Studio.EventDescription description;
        FMODUnity.RuntimeManager.StudioSystem.getEventByID(fmodEvent, out description);

        float maxDistance = 0f;
        description.getMinMaxDistance(out _, out maxDistance);

        float occlusionVolume = 1f;
        int occlusionPenetration = 0;

        //If outside of max range, return
        if(!actor.isPlayer)
        {
            if (Vector3.Distance(Player.Instance.transform.position, actor.transform.position) > maxDistance) return false;
        }

        SoundMaterialOverride detailedMaterialData = null;

        //Do we use a detailed check which uses a raycast to detect foot impact?
        if (actor.isPlayer || (actor.currentNode != null && actor.currentNode.individualFurniture.Count > 0 && actor.visible))
        {
            Vector3 rayStart = Vector3.zero;

            if (rightFoot)
            {
                rayStart = actor.transform.TransformPoint(new Vector3(0.13f, 0.1f, 0f));
            }
            else
            {
                rayStart = actor.transform.TransformPoint(new Vector3(-0.13f, 0.1f, 0f));
            }

            RaycastHit hit;

            //Scan
            if (Physics.Raycast(rayStart, Vector3.down, out hit, 2.6f, footstepLayerMask, QueryTriggerInteraction.Ignore))
            {
                //if(actor.isPlayer) Game.Log("Audio: Player footstep sound raycast hits: " + hit.transform.name);

                //Look for override controllers first...
                MaterialOverrideController moc = hit.transform.GetComponent<MaterialOverrideController>();

                if (moc != null)
                {
                    //if (actor.isPlayer) Game.Log("Audio: ... Found material override controller");
                    detailedMaterialData = new SoundMaterialOverride(moc.concrete, moc.wood, moc.carpet, moc.tile, moc.plaster, moc.fabric, moc.metal, moc.glass);
                }
                else
                {
                    //Look for interactable
                    InteractableController i = hit.transform.GetComponent<InteractableController>();

                    if(i != null && i.interactable != null && i.interactable.preset.useMaterialOverride)
                    {
                        //if (actor.isPlayer) Game.Log("Audio: ... Found interactable override");
                        detailedMaterialData = i.interactable.preset.materialOverride;
                    }
                    else
                    {
                        MeshFilter mf = hit.transform.GetComponent<MeshFilter>();

                        if (mf != null)
                        {
                            FurniturePreset f = Toolbox.Instance.GetFurnitureFromMesh(mf.sharedMesh);

                            if (f != null)
                            {
                                //if (actor.isPlayer) Game.Log("Audio: ... Found furniture");
                                detailedMaterialData = new SoundMaterialOverride(f.concrete, f.wood, f.carpet, f.tile, f.plaster, f.fabric, f.metal, f.glass);
                            }
                            else
                            {
                                //if (actor.isPlayer) Game.Log("Audio: ... Unable to get furniture from mesh " + mf.sharedMesh);
                            }
                        }
                    }
                }
            }
        }

        if(detailedMaterialData == null)
        {
            detailedMaterialData = new SoundMaterialOverride(actor.currentRoom.floorMaterial.concrete, actor.currentRoom.floorMaterial.wood, actor.currentRoom.floorMaterial.carpet, actor.currentRoom.floorMaterial.tile, actor.currentRoom.floorMaterial.plaster, actor.currentRoom.floorMaterial.fabric, actor.currentRoom.floorMaterial.metal, actor.currentRoom.floorMaterial.glass);
        }

        //Game.Log("Max range for " + eventPreset.name + " is " + maxRange);

        if (!eventPreset.disableOcclusion)
        {
            List<ActiveListener> activeListeners = null;
            bool isSus = false;

            occlusionVolume = GetOcculusion(Player.Instance.currentNode, actor.currentNode, eventPreset, occlusionVolume, actor, detailedMaterialData, out occlusionPenetration, out activeListeners, out isSus, out _, out _);

            if(isSus && actor != null)
            {
                foreach (ActiveListener a in activeListeners)
                {
                    if(a.listener != actor) a.listener.HearIllegal(eventPreset, actor.currentNode, actor.currentNode.position, actor, a.escalationLevel);
                }
            }
        }

        float wetness = 0f;
        float charWetness = 0f;
        float snow = 0f;
        float crouching = 0f;
        float running = 0f;

        if(actor.isOnStreet)
        {
            snow = SessionData.Instance.citySnow;
        }

        if(actor.isRunning)
        {
            running = 1f;
        }

        if (actor.isPlayer)
        {
            occlusionVolume = 1f; //If player then the volume is always 1
            occlusionPenetration = 0;

            //Chance of slipping...
            if (Toolbox.Instance.Rand(0f, 1f) < StatusController.Instance.tripChanceDrunk * 0.02f)
            {
                Player.Instance.Trip(Toolbox.Instance.Rand(0.05f, 0.1f));
            }
            //Chance of slipping...
            else if (Player.Instance.isRunning && (detailedMaterialData.tile >= 0.8f || detailedMaterialData.metal >= 0.8f || detailedMaterialData.glass >= 0.8f) && Toolbox.Instance.Rand(0f, 1f) < StatusController.Instance.tripChanceWet * 0.03f)
            {
                Player.Instance.Trip(Toolbox.Instance.Rand(0.05f, 0.1f));
            }

            if(Player.Instance.isOnStreet)
            {
                wetness = SessionData.Instance.cityWetness;
            }

            charWetness = Player.Instance.wet;

            if(Player.Instance.isCrouched)
            {
                crouching = 1f;
            }
        }

        //Get volume from the original value
        if (occlusionVolume <= 0.01f) return false;

        //Setup event instance
        FMOD.Studio.EventInstance step = FMODUnity.RuntimeManager.CreateInstance(fmodEvent);
        step.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(actor.footstepSoundTransform.gameObject));

        step.setParameterByName("Occlusion", occlusionPenetration); //Set occlusion

        step.setParameterByName("Concrete", detailedMaterialData.concrete);
        step.setParameterByName("Wood", detailedMaterialData.wood);
        step.setParameterByName("Carpet", detailedMaterialData.carpet);
        step.setParameterByName("Tile", detailedMaterialData.tile);
        step.setParameterByName("Plaster", detailedMaterialData.plaster);
        step.setParameterByName("Fabric", detailedMaterialData.fabric);
        step.setParameterByName("Metal", detailedMaterialData.metal);
        step.setParameterByName("Glass", detailedMaterialData.glass);

        step.setParameterByName("CityWetness", wetness);
        step.setParameterByName("CharacterWetness", charWetness);
        step.setParameterByName("CitySnow", snow);
        step.setParameterByName("Crouching", crouching);
        step.setParameterByName("Running", running);

        //Set stealth: -1 = running, 0 = normal, 1 = stealth
        if (actor.isRunning) step.setParameterByName("Stealth", -1f);
        else if (!actor.stealthMode) step.setParameterByName("Stealth", 0f);
        else step.setParameterByName("Stealth", 1f);

        //Play
        step.start();

        //Override master volume with occlusion volume
        step.setVolume(occlusionVolume);

        //Spot by player
        if (!actor.isPlayer)
        {
            if (Player.Instance.illegalStatus || Player.Instance.witnessesToIllegalActivity.Contains(actor))
            {
                //If virtual is false then trigger...
                bool virt = false;
                step.isVirtual(out virt);

                if (!virt)
                {
                    float soundLevel = GetPlayersSoundLevel(actor.currentNode, eventPreset, occlusionVolume, detailedMaterialData);

                    if(soundLevel > playerHearingThreshold)
                    {
                        //Game.Log("Audio: Footstep level for " + actor.name + ": " + soundLevel);
                        actor.HeardByPlayer();
                    }
                }
            }
        }

        //Relase after start to clean after playing
        step.release();

        if(detailedMaterialData != null) detailedMaterialData = null;

        return true;
    }

    public void PlayerPlayerImpactSound(float fallCount)
    {
        //fallCount = Mathf.Min((fallCount - 0.2f) * 2f, 1f);
        Game.Log("Audio: Playing player impact sound with param; " + fallCount);

        if (!SessionData.Instance.play)
        {
            return;
        }
        else if (SessionData.Instance.currentTimeSpeed != SessionData.TimeSpeed.normal)
        {
            return; //Only play on normal game speed
        }
        else if (!SessionData.Instance.startedGame)
        {
            return;
        }
        else if (CityConstructor.Instance != null && CityConstructor.Instance.preSimActive)
        {
            return;
        }

        SoundMaterialOverride detailedMaterialData = null;

        //Do we use a detailed check which uses a raycast to detect foot impact?
        Vector3 rayStart = Player.Instance.transform.TransformPoint(new Vector3(0f, 0.1f, 0f));

        RaycastHit hit;

        //Scan
        if (Physics.Raycast(rayStart, Vector3.down, out hit, 2.6f, footstepLayerMask, QueryTriggerInteraction.Ignore))
        {
            Game.Log("Audio: Player impact sound raycast hits: " + hit.transform.name);

            //Look for override controllers first...
            MaterialOverrideController moc = hit.transform.GetComponent<MaterialOverrideController>();

            if (moc != null)
            {
                detailedMaterialData = new SoundMaterialOverride(moc.concrete, moc.wood, moc.carpet, moc.tile, moc.plaster, moc.fabric, moc.metal, moc.glass);
            }
            else
            {
                //Look for interactable
                InteractableController i = hit.transform.GetComponent<InteractableController>();

                if (i != null && i.interactable != null && i.interactable.preset.useMaterialOverride)
                {
                    detailedMaterialData = i.interactable.preset.materialOverride;
                }
                else
                {
                    MeshFilter mf = hit.transform.GetComponent<MeshFilter>();

                    if (mf != null)
                    {
                        FurniturePreset f = Toolbox.Instance.GetFurnitureFromMesh(mf.sharedMesh);

                        if (f != null)
                        {
                            detailedMaterialData = new SoundMaterialOverride(f.concrete, f.wood, f.carpet, f.tile, f.plaster, f.fabric, f.metal, f.glass);
                        }
                    }
                }
            }
        }
        else
        {
            Game.Log("Audio: Player impact sound raycast does not hit anything, using room materials instead...");
        }

        if (detailedMaterialData == null)
        {
            detailedMaterialData = new SoundMaterialOverride(Player.Instance.currentRoom.floorMaterial.concrete, Player.Instance.currentRoom.floorMaterial.wood, Player.Instance.currentRoom.floorMaterial.carpet, Player.Instance.currentRoom.floorMaterial.tile, Player.Instance.currentRoom.floorMaterial.plaster, Player.Instance.currentRoom.floorMaterial.fabric, Player.Instance.currentRoom.floorMaterial.metal, Player.Instance.currentRoom.floorMaterial.glass);
        }

        AudioEvent eventPreset = null;

        if (detailedMaterialData.concrete >= 0.5f)
        {
            eventPreset = AudioControls.Instance.playerLandImpactConcrete;
        }
        else if (detailedMaterialData.metal >= 0.5f)
        {
            eventPreset = AudioControls.Instance.playerLandImpactMetal;
        }
        else if (detailedMaterialData.wood >= 0.5f)
        {
            eventPreset = AudioControls.Instance.playerLandImpactWood;
        }
        else return;

        FMODParam newParam = new FMODParam { name = "Fall", value = fallCount };
        List<FMODParam> fList = new List<FMODParam>();
        fList.Add(newParam);

        PlayWorldOneShot(eventPreset, Player.Instance, Player.Instance.currentNode, Player.Instance.footstepSoundTransform.position + new Vector3(0, 0.1f, 0), null, fList, surfaceData: detailedMaterialData);
    }

    //This needs to be fed a sound location: Either through the actor
    public FMOD.Studio.EventInstance PlayWorldOneShot(AudioEvent eventPreset, Actor who, NewNode location, Vector3 worldPosition, Interactable interactable = null, List<FMODParam> parameters = null, float volumeOverride = 1f, List<NewNode> additionalSources = null,  bool forceIgnoreOcclusion = false, SoundMaterialOverride surfaceData = null, bool forceSuspicious = false)
    {
        if (eventPreset == null || (eventPreset.guid.Length <= 0 && !eventPreset.isDummyEvent) || SessionData.Instance.currentTimeSpeed != SessionData.TimeSpeed.normal || !SessionData.Instance.startedGame || (CityConstructor.Instance != null && CityConstructor.Instance.preSimActive))
        {
            FMOD.Studio.EventInstance nullEvent = new FMOD.Studio.EventInstance();
            nullEvent.release();
            return nullEvent;
        }
        else if(eventPreset.disabled)
        {
            FMOD.Studio.EventInstance nullEvent = new FMOD.Studio.EventInstance();
            nullEvent.release();
            return nullEvent;
        }
        else if (eventPreset.isLicensed && !Game.Instance.allowLicensedMusic)
        {
            FMOD.Studio.EventInstance nullEvent = new FMOD.Studio.EventInstance();
            nullEvent.release();
            return nullEvent;
        }

        NewNode soundLocation = location;

        if (location == null)
        {
            if(PathFinder.Instance.nodeMap.TryGetValue(CityData.Instance.RealPosToNodeInt(worldPosition), out location))
            {
                if(eventPreset.debug && location.room != null) Game.Log("Audio: Node was not passed to audio controller, but one was found using world position (" + worldPosition + " = " + location.room.GetName() + ": " + location.position + ")");
            }
            else
            {
                if (eventPreset.debug) Game.Log("Audio: Node was not passed to audio controller, and unable to find one using world position so continuing without...");
            }
        }

        if (location != null && location.tile != null && location.tile.cityTile != null)
        {
            if (!location.tile.cityTile.isInPlayerVicinity)
            {
                //Game.Log("Audio: Skipping as player is not in vicinity");
                return new FMOD.Studio.EventInstance(); //Don't play if outside of vicinity
            }
        }

        FMOD.GUID fmodEvent = new FMOD.GUID();

        if (!eventPreset.isDummyEvent)
        {
            fmodEvent = FMOD.GUID.Parse(eventPreset.guid);
        }

        //FMOD.Studio.EventDescription description;
        //FMODUnity.RuntimeManager.StudioSystem.getEventByID(fmodEvent, out description);

        //float maxDistance = 0f;
        //description.getMaximumDistance(out maxDistance);

        //Get occlusion (only do this if not player as the sound playing will always be for the player's audio listener)
        float occlusionVolume = volumeOverride;
        int occlusionPenetration = 0;

        if(location == null)
        {
            Game.Log("Audio: Ignoring occlusion for " + eventPreset.name + " as no location was passed.");
        }

        List<ActiveListener> activeListeners = new List<ActiveListener>();

        if (!forceIgnoreOcclusion && !eventPreset.disableOcclusion && location != null)
        {
            bool isSus = false;

            occlusionVolume = GetOcculusion(Player.Instance.currentNode, location, eventPreset, occlusionVolume, who, surfaceData, out occlusionPenetration, out activeListeners, out isSus, out _, out _, additionalSources, forceSuspicious: forceSuspicious);

            //Execute fines & investigation
            if (who != null && isSus)
            {
                foreach (ActiveListener a in activeListeners)
                {
                    if(a.listener != who) a.listener.HearIllegal(eventPreset, location, location.position, who, a.escalationLevel);
                }
            }

            if (eventPreset.debug) Game.Log("Audio: Occlusion value for " + eventPreset.name + " is " + occlusionVolume + " with penetration " + occlusionPenetration);
        }

        //If volume is 0 then return null
        if (occlusionVolume <= 0.01f)
        {
            //Game.Log("Audio: Returned volume too small");
            FMOD.Studio.EventInstance nullEvent = new FMOD.Studio.EventInstance();
            nullEvent.release();
            return nullEvent;
        }

        //Setup event instance
        FMOD.Studio.EventInstance step;

        if (!eventPreset.isDummyEvent)
        {
            step = FMODUnity.RuntimeManager.CreateInstance(fmodEvent);
        }
        else
        {
            step = new FMOD.Studio.EventInstance();
        }

        step.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(worldPosition));

        //Set parameters
        step.setParameterByName("Occlusion", occlusionPenetration);

        if (who != null)
        {
            //Set stealth: -1 = running, 0 = normal, 1 = stealth
            if (who.isRunning) step.setParameterByName("Stealth", -1f);
            else if (!who.stealthMode) step.setParameterByName("Stealth", 0f);
            else step.setParameterByName("Stealth", 1f);
        }

        //Load passed paramters
        if(parameters != null)
        {
            foreach (FMODParam para in parameters)
            {
                step.setParameterByName(para.name, para.value);
            }
        }

        if(surfaceData != null)
        {
            step.setParameterByName("Concrete", surfaceData.concrete);
            step.setParameterByName("Wood", surfaceData.wood);
            step.setParameterByName("Carpet", surfaceData.carpet);
            step.setParameterByName("Tile", surfaceData.tile);
            step.setParameterByName("Plaster", surfaceData.plaster);
            step.setParameterByName("Fabric", surfaceData.fabric);
            step.setParameterByName("Metal", surfaceData.metal);
            step.setParameterByName("Glass", surfaceData.glass);
        }

        //bool isVirtual = false;
        //step.isVirtual(out isVirtual);
        //if (isVirtual) Game.Log("VIRTUAL ONE SHOT SOUND");

        //Play
        if (!eventPreset.isDummyEvent)
        {
            step.start();

            //Override master volume with occlusion volume * overrides
            step.setVolume(Mathf.Clamp01(occlusionVolume * eventPreset.masterVolumeScale));
        }

        if(eventPreset.debug) Game.Log("Audio: Playing " + eventPreset.name + " with occluded volume of " + Mathf.Clamp01(occlusionVolume * eventPreset.masterVolumeScale) + " and " + activeListeners.Count + " active listeners");

        //Spot by player
        if (who != null && !who.isPlayer && location != null)
        {
            if (Player.Instance.illegalStatus || Player.Instance.witnessesToIllegalActivity.Contains(who))
            {
                //If virtual is false then trigger...
                bool virt = false;
                step.isVirtual(out virt);

                if (!virt)
                {
                    //Check max distance to see if this should be audible...
                    //Calculate volume based on max distance...
                    float soundLevel = GetPlayersSoundLevel(location, eventPreset, occlusionVolume, null);

                    if (soundLevel > playerHearingThreshold)
                    {
                        Game.Log("Audio: Heard by player: " + who.name + "("+eventPreset.name +") passing through " + occlusionPenetration + " occ units, sound level of " + soundLevel);
                        who.HeardByPlayer();
                    }
                }
            }
        }

        //Relase after start to clean after playing
        step.release();

        if (surfaceData != null) surfaceData = null;

        return step;
    }

    //Play a sound with a delay first...
    public void PlayOneShotDelayed(float delay, AudioEvent eventPreset, Actor who, NewNode location, Vector3 worldPosition, List<FMODParam> parameters = null, float volumeOverride = 1f, List<NewNode> additionalSources = null, bool forceIgnoreOcclusion = false)
    {
        if (!SessionData.Instance.startedGame || (CityConstructor.Instance != null && CityConstructor.Instance.preSimActive)) return;
        delayedSound.Add(new DelayedSoundInfo(delay, eventPreset, who, location, worldPosition, parameters, volumeOverride, additionalSources, forceIgnoreOcclusion));
    }

    //Play a looping event that is static (interactable)
    public LoopingSoundInfo PlayWorldLooping(AudioEvent eventPreset, Actor who, Interactable interactable, List<FMODParam> parameters = null, float volumeOverride = 1f, bool forceSuspicious = false, bool pauseWhenGamePaused = false, bool isBroadcast = false, InteractablePreset.IfSwitchStateSFX newSwitchInfo = null)
    {
        return PlayWorldLooping(eventPreset, who, interactable.node, interactable.wPos, interactable, parameters, volumeOverride, forceSuspicious, pauseWhenGamePaused, isBroadcast, newSwitchInfo);
    }

    //Play a looping event that is static
    public LoopingSoundInfo PlayWorldLoopingStatic(AudioEvent eventPreset, Actor who, NewNode worldNode, Vector3 worldPos, List<FMODParam> parameters = null, float volumeOverride = 1f, bool forceSuspicious = false, bool pauseWhenGamePaused = false, bool isBroadcast = false, InteractablePreset.IfSwitchStateSFX newSwitchInfo = null)
    {
        return PlayWorldLooping(eventPreset, who, worldNode, worldPos, null, parameters, volumeOverride, forceSuspicious, pauseWhenGamePaused, isBroadcast, newSwitchInfo);
    }

    //Return the reference to the instance so we can use this to manually stop it
    public LoopingSoundInfo PlayWorldLooping(AudioEvent eventPreset, Actor who, NewNode worldNode, Vector3 worldPosition, Interactable interactable = null, List<FMODParam> parameters = null, float volumeOverride = 1f, bool forceSuspicious = false, bool pauseWhenGamePaused = false, bool isBroadcast = false, InteractablePreset.IfSwitchStateSFX newSwitchInfo = null)
    {
        if (eventPreset == null || eventPreset.guid.Length <= 0)
        {
            Game.Log("Audio: PlayWorldLooping: Null or invalid preset: " + eventPreset);
            return null;
        }
        else if(eventPreset.disabled)
        {
            return null;
        }
        else if (eventPreset.isLicensed && !Game.Instance.allowLicensedMusic)
        {
            return null;
        }

        if (eventPreset.debug) Game.Log("Audio: Playing new world looping sound " + eventPreset.name + "...");

        //Override sound event with broadcast
        if(isBroadcast && SessionData.Instance.currentShow != null)
        {
            eventPreset = SessionData.Instance.currentShow.audioEvent;
        }

        //System.Guid fmodEvent = new System.Guid(eventPreset.guid);
        //FMOD.Studio.EventDescription description;
        //FMODUnity.RuntimeManager.StudioSystem.getEventByID(fmodEvent, out description);

        if (worldNode == null)
        {
            //Calculate node positon from real pos
            Vector3Int nodeCoord = CityData.Instance.RealPosToNodeInt(worldPosition);
            PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out worldNode);
        }

        ////Get occlusion (only do this if not player as the sound playing will always be for the player's audio listener)
        //float occlusionVolume = volumeOverride;
        //int occlusionPenetration = 0;
        //bool isSuspicious = false;
        //List<NewRoom> audibleRooms = null;

        //if (worldNode != null && !eventPreset.disableOcclusion && SessionData.Instance.startedGame)
        //{
        //    List<ActiveListener> activeListeners = null;
        //    occlusionVolume = GetOcculusion(Player.Instance.currentNode, worldNode, eventPreset, occlusionVolume, who, null, out occlusionPenetration, out activeListeners, out isSuspicious, out audibleRooms, out _, forceSuspicious: forceSuspicious);

        //    if (who != null && who.isPlayer)
        //    {
        //        foreach (ActiveListener a in activeListeners)
        //        {
        //            a.listener.HearIllegal(eventPreset, worldNode, worldNode.position, null, a.escalationLevel);
        //        }
        //    }

        //    //Force outline if player is illegal
        //    if (occlusionVolume > 0f && (who == null || !who.isPlayer) && eventPreset.forceOutlineForLoopIfPlayerTrespassing)
        //    {
        //        ForceOutlineCheck(eventPreset, interactable);
        //    }
        //}

        //FMOD.Studio.EventInstance step;

        ////Setup event instance
        //try
        //{
        //    step = FMODUnity.RuntimeManager.CreateInstance(fmodEvent);
        //}
        //catch
        //{
        //    Game.Log("Audio: Cannot find audio event " + eventPreset.guid + " skipping...");
        //    return null;
        //}

        //if(interactable != null && interactable.controller != null)
        //{
        //    FMODUnity.RuntimeManager.AttachInstanceToGameObject(step, interactable.controller.transform, new Rigidbody());
        //}
        //else
        //{
        //    step.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(worldPosition));
        //}

        ////Set parameters
        //step.setParameterByName("Occlusion", occlusionPenetration);

        //if (who != null)
        //{
        //    //Set stealth: -1 = running, 0 = normal, 1 = stealth
        //    if (who.isRunning) step.setParameterByName("Stealth", -1f);
        //    else if (!who.stealthMode) step.setParameterByName("Stealth", 0f);
        //    else step.setParameterByName("Stealth", 1f);
        //}

        ////Load passed paramters
        //if (parameters != null)
        //{
        //    foreach (FMODParam para in parameters)
        //    {
        //        step.setParameterByName(para.name, para.value);
        //    }
        //}

        ////If broadcast, set to correct position
        //if (isBroadcast)
        //{
        //    step.setTimelinePosition(Mathf.RoundToInt(SessionData.Instance.currentShowProgressSeconds * 1000));
        //}

        ////Play
        //step.start();

        ////Start paused if game is paused
        //if(!SessionData.Instance.play && pauseWhenGamePaused)
        //{
        //    step.setPaused(true);
        //}

        ////Override master volume with occlusion volume * overrides
        //step.setVolume(Mathf.Clamp01(occlusionVolume * eventPreset.masterVolumeScale));

        //Store details for occlusion updates
        string loopName = string.Empty;

        if(Game.Instance.devMode)
        {
            loopName = eventPreset.name;
            if (worldNode != null) loopName += " " + worldNode.room.name + ", " + worldNode.room.gameLocation.name + " " + worldPosition;
        }

        LoopingSoundInfo newLoop = new LoopingSoundInfo { name = loopName, volumeOverride = volumeOverride, eventPreset = eventPreset, sourceLocation = worldNode, who = who, forceSuspicious = forceSuspicious, init = true, worldPos = worldPosition, isBroadcast = isBroadcast, interactableLoopInfo = newSwitchInfo, interactable = interactable };
        loopingSounds.Add(newLoop);

        //If this is suspicious, we also need to add it to rooms that are to be checked when AI walks in...
        //if (isSuspicious && audibleRooms != null)
        //{
        //    foreach (NewRoom r in audibleRooms)
        //    {
        //        r.audibleLoopingSounds.Add(newLoop);
        //    }
        //}

        newLoop.UpdateOcclusion(true);

        return newLoop;
    }

    //Return the reference to the instance so we can use this to manually stop it
    public LoopingSoundInfo Play2DLooping(AudioEvent eventPreset,  List<FMODParam> parameters = null, float volumeOverride = 1f)
    {
        if (eventPreset == null || eventPreset.guid.Length <= 0)
        {
            Game.Log("Audio: Play2DLooping: No preset");
            return null;
        }
        else if(eventPreset.disabled)
        {
            return null;
        }
        else if (eventPreset.isLicensed && !Game.Instance.allowLicensedMusic)
        {
            return null;
        }

        if (eventPreset.debug) Game.Log("Audio: Playing new 2D looping sound: " + eventPreset.name);

        FMOD.GUID fmodEvent = FMOD.GUID.Parse(eventPreset.guid);

        FMOD.Studio.EventDescription description;
        FMODUnity.RuntimeManager.StudioSystem.getEventByID(fmodEvent, out description);

        //Setup event instance
        FMOD.Studio.EventInstance step = FMODUnity.RuntimeManager.CreateInstance(fmodEvent);

        //Load passed paramters
        if (parameters != null)
        {
            foreach (FMODParam para in parameters)
            {
                step.setParameterByName(para.name, para.value);
            }
        }

        //Play
        step.start();

        //Override master volume with occlusion volume * overrides
        step.setVolume(Mathf.Clamp01(eventPreset.masterVolumeScale * volumeOverride));

        //Store details for occlusion updates
        LoopingSoundInfo newLoop = new LoopingSoundInfo { name = eventPreset.name, audioEvent = step, description = description, eventPreset = eventPreset, init = true };
        loopingSounds.Add(newLoop);

        return newLoop;
    }

    //Update all looping sounds
    public void UpdateAllLoopingSoundOcclusion()
    {
        if (CityConstructor.Instance != null && CityConstructor.Instance.preSimActive) return;

        for (int i = 0; i < loopingSounds.Count; i++)
        {
            //Also check for stopped sounds...
            LoopingSoundInfo loop = loopingSounds[i];

            if(!loop.isActive)
            {
                loop.isValid = loop.audioEvent.isValid();
                loop.UpdatePlayState();

                if(loop.state == PLAYBACK_STATE.STOPPED || !loop.isValid)
                {
                    if(loop.eventPreset.forceOutlineForLoopIfPlayerTrespassing)
                    {
                        ForceOutlineCheck(loop.eventPreset, loop.interactable, true);
                    }

                    if(loop.isValid) loop.audioEvent.release(); //This is a new addition, remove this if it screws anything up...

                    loopingSounds.RemoveAt(i);
                    i--;
                    continue;
                }
            }
            else
            {
                loopingSounds[i].UpdateOcclusion();
            }
        }
    }

    //Update closest window and open exterior door
    public void UpdateClosestWindowAndDoor(bool doorCheckOnly = false)
    {
        NewWall closest = null;

        if (!doorCheckOnly)
        {
            windowAudioPosition = Vector3.zero;
            closestWindowDistance = 9999f;
        }

        NewNode.NodeAccess closestDoor = null;
        doorAudioPosition = Vector3.zero;
        closestDoorDistance = 9999f;

        if (SessionData.Instance.startedGame)
        {
            //Scan all visible rooms for a window...
            foreach (NewRoom r in CityData.Instance.visibleRooms)
            {
                if(!doorCheckOnly)
                {
                    foreach (NewWall w in r.windows)
                    {
                        if(w.node.gameLocation == Player.Instance.currentGameLocation || w.otherWall.node.gameLocation == Player.Instance.currentGameLocation)
                        {
                            float dist = Vector3.Distance(Player.Instance.transform.position, w.position);

                            if (closest == null || dist < closestWindowDistance)
                            {
                                closest = w;
                                windowAudioPosition = w.position;
                                closestWindowDistance = dist;
                            }
                        }
                    }
                }

                //Only look for closest open door if the player is inside...
                if (Player.Instance.currentNode != null && !Player.Instance.currentNode.isOutside)
                {
                    foreach (NewNode.NodeAccess ent in r.entrances)
                    {
                        if (ent.accessType == NewNode.NodeAccess.AccessType.door && ent.door != null && !ent.door.isClosed)
                        {
                            if ((ent.fromNode.isOutside || ent.toNode.isOutside) && (ent.fromNode.gameLocation == Player.Instance.currentGameLocation || ent.toNode.gameLocation == Player.Instance.currentGameLocation))
                            {
                                //Calculate the actual volume this door would output based on how open it is
                                float dist = Vector3.Distance(Player.Instance.transform.position, ent.worldAccessPoint) * (2f - openMultiplierCurve.Evaluate(Mathf.Abs(ent.door.ajarProgress)));

                                //Game.Log("Nearest open door prog: " + ent.door.ajarProgress + " curve value: " + openMultiplierCurve.Evaluate(Mathf.Abs(ent.door.ajarProgress)) + " = " + dist);

                                if (closestDoor == null || dist < closestDoorDistance)
                                {
                                    closestDoor = ent;
                                    doorAudioPosition = ent.worldAccessPoint;
                                    closestDoorDistance = dist;
                                }
                            }
                        }
                    }
                }
                else
                {
                    closestDoorDistance = 0;
                }

                //Update footprints
                if (r.footprintUpdateQueued)
                {
                    r.UpdateFootprints();
                }
            }
        }

        if (!doorCheckOnly) PassWindowDistance();
        PassDistanceFromExternalDoor();
    }

    //Update the player distance from the edge of the city and from exterior walls
    public void UpdateDistanceFromEdge()
    {
        CityTile closestBorder = null;
        edgeDistance = 9999f;

        if(SessionData.Instance.startedGame && CityData.Instance.borderBlock != null)
        {
            foreach (CityTile t in CityData.Instance.borderBlock.cityTiles)
            {
                float dist = Mathf.Max(Vector3.Distance(Player.Instance.transform.position, t.transform.position) - 25f, 0);

                if (dist < edgeDistance)
                {
                    closestBorder = t;
                    edgeDistance = dist;
                }
            }
        }

        //Create normalized
        edgeDistanceNormalized = Mathf.Clamp01(edgeDistance / edgeDistanceMultiplier);

        PassEdgeDistance();
    }

    //Pass ambient variables to FMOD (for ambient)...
    public void PassWindowDistance()
    {
        //Create normalized
        closestWindowDistanceNormalized = Mathf.Clamp01(closestWindowDistance  / closestWindowDistanceMultiplier);

        //Passed from above
        if(ambienceRain != null) ambienceRain.audioEvent.setParameterByName("WindowDistance", closestWindowDistanceNormalized);
        if(ambienceWind != null) ambienceWind.audioEvent.setParameterByName("WindowDistance", closestWindowDistanceNormalized);
        if (ambiencePA != null) ambiencePA.audioEvent.setParameterByName("WindowDistance", closestWindowDistanceNormalized);

        //Update params passed to alarmed building
        foreach(NewBuilding b in GameplayController.Instance.activeAlarmsBuildings)
        {
            b.UpdateAlarmPAWindowDistance(closestWindowDistanceNormalized);
        }

        //Game.Log("Closest window: " + closestWindowDistanceNormalized);
    }

    //Pass distance from an external open door...
    public void PassDistanceFromExternalDoor()
    {
        //Create normalized
        closestDoorDistanceNormalized = Mathf.Clamp01(closestDoorDistance / closestDoorDistanceMultiplier);

        //Passed from above
        if (ambienceRain != null) ambienceRain.audioEvent.setParameterByName("ExternalDoorDistance", closestDoorDistanceNormalized);
        if (ambienceWind != null) ambienceWind.audioEvent.setParameterByName("ExternalDoorDistance", closestDoorDistanceNormalized);
        if (ambiencePA != null) ambiencePA.audioEvent.setParameterByName("ExternalDoorDistance", closestDoorDistanceNormalized);

        //Update params passed to alarmed building
        foreach (NewBuilding b in GameplayController.Instance.activeAlarmsBuildings)
        {
            b.UpdateAlarmPAExternalDoorDistance(closestDoorDistanceNormalized);
        }

        //Game.Log("Closest ext door: " + (Mathf.RoundToInt(closestDoorDistanceNormalized * 100f) / 100f) + " (" + closestDoorDistance + "/" + closestDoorDistanceMultiplier + ")");
    }

    //Pass current weather params to FMOD (for ambient)...
    [Button]
    public void PassWeatherParams()
    {
        //Passed when weather is interpolated, to match rainfall etc
        if (ambienceRain != null)
        {
            passedRain = SessionData.Instance.currentRain;

            //Make sure event is playing
            if(passedRain > 0f)
            {
                ambienceRain.UpdatePlayState();

                if(ambienceRain.state == PLAYBACK_STATE.STOPPED || ambienceRain.state == PLAYBACK_STATE.STOPPING)
                {
                    ambienceRain.audioEvent.start();
                }
            }

            ambienceRain.audioEvent.setParameterByName("Rain", passedRain);
            ambienceRain.audioEvent.setParameterByName("CityWetness", SessionData.Instance.cityWetness);
            //Game.Log("Passing rain " + passedRain);
        }

        if (ambienceWind != null)
        {
            passedWind = SessionData.Instance.currentWind;

            //Make sure event is playing
            if (passedWind > 0f)
            {
                ambienceWind.UpdatePlayState();
                if (ambiencePA != null) ambiencePA.UpdatePlayState();

                if (ambienceWind.state == PLAYBACK_STATE.STOPPED || ambienceWind.state == PLAYBACK_STATE.STOPPING)
                {
                    ambienceWind.audioEvent.start();
                    if (ambiencePA != null) ambiencePA.audioEvent.start();
                }
            }

            ambienceWind.audioEvent.setParameterByName("Wind", passedWind);
            ambienceWind.audioEvent.setParameterByName("EdgeDistance", edgeDistanceNormalized);

            if(ambiencePA != null)
            {
                ambiencePA.audioEvent.setParameterByName("Wind", passedWind);
                ambiencePA.audioEvent.setParameterByName("EdgeDistance", edgeDistanceNormalized);
            }
        }
    }

    //Pass outdoor/indoor variables to FMOD (for ambient)...
    public void PassIndoorOutdoor()
    {
        //0 = outdoors, 1 = indoors
        int i = 0;

        if (Player.Instance.currentNode == null || !Player.Instance.currentNode.isOutside || Player.Instance.inAirVent)
        {
            i = 1;
        }

        //Game.Log("Player: Current node is outside: " + Player.Instance.currentNode.isOutside + " current tile: " + Player.Instance.currentNode.tile.isOutside + " current location: " + Player.Instance.currentNode.gameLocation.isOutside);

        if (ambienceRain != null) ambienceRain.audioEvent.setParameterByName("Interior", i);
        if (ambienceWind != null) ambienceWind.audioEvent.setParameterByName("Interior", i);
        if (ambiencePA != null) ambiencePA.audioEvent.setParameterByName("Interior", i);

        //Update params passed to alarmed building
        foreach (NewBuilding b in GameplayController.Instance.activeAlarmsBuildings)
        {
            b.UpdateAlarmPAIntExt(i);
        }

        //Game.Log("Interior: " + i);
    }

    public void UpdateVentIndoorOutdoor()
    {
        //Game.Log("Update vent indoor/outdoor");

        if (Player.Instance.inAirVent)
        {
            if (Player.Instance.currentDuct != null && Player.Instance.currentDuctSection != null)
            {
                if(Player.Instance.currentDuctSection.ext)
                {
                    ventOutdoorsIndoors = 0;
                }
                else
                {
                    ventOutdoorsIndoors = 1;

                    int ventFill = 16;
                    int distanceToExt = 9999; //Distance to exterior duct unit (measured in duct sections)

                    Dictionary<AirDuctGroup.AirDuctSection, int> openSections = new Dictionary<AirDuctGroup.AirDuctSection, int>();
                    openSections.Add(Player.Instance.currentDuctSection, 0);

                    HashSet<AirDuctGroup.AirDuctSection> closedSections = new HashSet<AirDuctGroup.AirDuctSection>();

                    while (ventFill > 0 && openSections.Count > 0)
                    {
                        int currentDist = 99999;
                        AirDuctGroup.AirDuctSection currentSection = null;

                        foreach(KeyValuePair<AirDuctGroup.AirDuctSection, int> pair in openSections)
                        {
                            if(pair.Value < currentDist)
                            {
                                currentSection = pair.Key;
                                currentDist = pair.Value;
                            }
                        }

                        if (currentSection == null) break;

                        if(currentSection.ext)
                        {
                            distanceToExt = currentDist;
                            break;
                        }

                        //Maybe this isn't working???
                        List<AirDuctGroup.AirDuctSection> neighbors = currentSection.GetNeighborSections(out _, out _, out _);

                        foreach(AirDuctGroup.AirDuctSection sec in neighbors)
                        {
                            if(!closedSections.Contains(sec))
                            {
                                if(!openSections.ContainsKey(sec))
                                {
                                    openSections.Add(sec, currentDist + 1);
                                }
                            }
                        }

                        closedSections.Add(currentSection);
                        openSections.Remove(currentSection);

                        ventFill--;
                    }

                    //Game.Log("Vent calc: " + distanceToExt + " = " + Mathf.Clamp01(distanceToExt / 5f) + " ventfill remaining: " + ventFill);

                    ventOutdoorsIndoors = Mathf.Clamp01(distanceToExt / 5f);
                }
            }
            else ventOutdoorsIndoors = 1;
        }
        else ventOutdoorsIndoors = 1;
    }

    public void UpdateDistanceToVent()
    {
        nearbyVent = 0;

        if (Player.Instance.inAirVent)
        {
            if (Player.Instance.currentDuct != null && Player.Instance.currentDuctSection != null)
            {
                int ventFill = 16;
                int distanceToClosest = 9999; //Distance to exterior duct unit (measured in duct sections)

                Dictionary<AirDuctGroup.AirDuctSection, int> openSections = new Dictionary<AirDuctGroup.AirDuctSection, int>();
                openSections.Add(Player.Instance.currentDuctSection, 0);

                HashSet<AirDuctGroup.AirDuctSection> closedSections = new HashSet<AirDuctGroup.AirDuctSection>();

                while (ventFill > 0 && openSections.Count > 0)
                {
                    int currentDist = 99999;
                    AirDuctGroup.AirDuctSection currentSection = null;

                    foreach (KeyValuePair<AirDuctGroup.AirDuctSection, int> pair in openSections)
                    {
                        if (pair.Value < currentDist)
                        {
                            currentSection = pair.Key;
                            currentDist = pair.Value;
                        }
                    }

                    if (currentSection == null) break;

                    //Maybe this isn't working???
                    List<AirDuctGroup.AirVent> nVents = new List<AirDuctGroup.AirVent>();

                    List<AirDuctGroup.AirDuctSection> neighbors = currentSection.GetNeighborSections(out _, out nVents, out _);

                    if (currentSection.peekSection || nVents.Count > 0)
                    {
                        distanceToClosest = currentDist;
                        break;
                    }

                    foreach (AirDuctGroup.AirDuctSection sec in neighbors)
                    {
                        if (!closedSections.Contains(sec))
                        {
                            if (!openSections.ContainsKey(sec))
                            {
                                openSections.Add(sec, currentDist + 1);
                            }
                        }
                    }

                    closedSections.Add(currentSection);
                    openSections.Remove(currentSection);

                    ventFill--;
                }

                //Game.Log("Vent calc: " + distanceToClosest + " = " + Mathf.Clamp01(distanceToClosest / 5f) + " ventfill remaining: " + ventFill);

                nearbyVent = Mathf.Clamp01(distanceToClosest / 5f);
            }
            else nearbyVent = 0;
        }
        else nearbyVent = 0;
    }

    //Pass the time of day
    public void PassTimeOfDay()
    {
        
    }

    //Pass distance from the edge of the play area
    public void PassEdgeDistance()
    {
        //Create normalized
        edgeDistanceNormalized = Mathf.Clamp01(edgeDistance / edgeDistanceMultiplier);

        if (ambienceWind != null)
        {
            ambienceWind.audioEvent.setParameterByName("EdgeDistance", edgeDistanceNormalized);
        }

        if (ambiencePA != null)
        {
            ambiencePA.audioEvent.setParameterByName("EdgeDistance", edgeDistanceNormalized);
        }
    }

    //Update closest exterior wall
    public void UpdateClosestExteriorWall()
    {
        NewWall closest = null;
        extWallDistance = 99999f;

        if (SessionData.Instance.startedGame)
        {
            if(Player.Instance.isOnStreet && Player.Instance.currentRoom != null)
            {
                foreach(NewNode n in Player.Instance.currentRoom.nodes)
                {
                    foreach(NewWall w in n.walls)
                    {
                        if(w.node.isOutside || w.node.room.IsOutside())
                        {
                            float dist = Vector3.Distance(Player.Instance.transform.position, w.position);

                            if(dist < extWallDistance)
                            {
                                closest = w;
                                extWallDistance = dist;
                            }
                        }
                    }
                }

                foreach(NewRoom r in Player.Instance.currentRoom.adjacentRooms)
                {
                    foreach (NewNode n in r.nodes)
                    {
                        foreach (NewWall w in n.walls)
                        {
                            if (w.node.isOutside || w.node.room.IsOutside())
                            {
                                float dist = Vector3.Distance(Player.Instance.transform.position, w.position);

                                if (dist < extWallDistance)
                                {
                                    closest = w;
                                    extWallDistance = dist;
                                }
                            }
                        }
                    }
                }
            }
        }

        PassExteriorWallDistance();
    }

    //Pass distance from an exterior wall
    public void PassExteriorWallDistance()
    {
        //Create normalized
        extWallNormalized = Mathf.Clamp01(extWallDistance / extWallDistanceMultiplier);

        //Passed from above
        if (ambienceRain != null) ambienceRain.audioEvent.setParameterByName("ExtWallDistance", extWallNormalized);
        if (ambienceWind != null) ambienceWind.audioEvent.setParameterByName("ExtWallDistance", extWallNormalized);
        if (ambiencePA != null) ambiencePA.audioEvent.setParameterByName("ExtWallDistance", extWallNormalized);

        //Game.Log("ExtWallDistance: " + extWallNormalized);
    }

    //Is this sound playing?
    public bool IsSoundPlaying(LoopingSoundInfo sound)
    {
        if (sound == null || !sound.init) return false;

        return IsSoundPlaying(sound.audioEvent);
    }

    public bool IsSoundPlaying(FMOD.Studio.EventInstance sound)
    {
        FMOD.Studio.PLAYBACK_STATE playback = FMOD.Studio.PLAYBACK_STATE.STOPPED;
        sound.getPlaybackState(out playback);

        if (playback == FMOD.Studio.PLAYBACK_STATE.PLAYING || playback == FMOD.Studio.PLAYBACK_STATE.SUSTAINING)
        {
            return true;
        }
        else return false;
    }

    public void StopSound(LoopingSoundInfo loop, StopType stop, string debugReason)
    {
        if(loop != null)
        {
            loop.debugStoppedReason = debugReason;

            if (loop.eventPreset != null && loop.eventPreset.debug)
            {
                try
                {
                    Game.Log("Audio: Stopping sound " + loop.eventPreset.name + " " + stop.ToString() + ", reason: " + debugReason);
                }
                catch
                {

                }
            }

            if(loop.eventPreset != null && loop.eventPreset.forceOutlineForLoopIfPlayerTrespassing)
            {
                ForceOutlineCheck(loop.eventPreset, loop.interactable, true);
            }

            loop.isActive = false; //Set this flag to false so it will disappear on next check
            StopSound(loop.audioEvent, stop);
            loop.isValid = loop.audioEvent.isValid();
        }
    }

    //Stop & release any sound
    public void StopSound(FMOD.Studio.EventInstance sound, StopType stop)
    {
        if (stop == StopType.triggerCue)
        {
            sound.keyOff();
        }
        else if(stop == StopType.fade)
        {
            sound.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            sound.release();
        }
        else
        {
            sound.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            sound.release();
        }
    }

    //Play an interface sound
    public FMOD.Studio.EventInstance Play2DSound(AudioEvent eventPreset, List<FMODParam> parameters = null, float volumeOverride = 1f)
    {
        if(eventPreset == null || eventPreset.guid.Length <= 0)
        {
            FMOD.Studio.EventInstance returnNull = new FMOD.Studio.EventInstance();
            returnNull.release();
            Game.Log("Audio: Play2D: No preset");
            return returnNull;
        }

        if(eventPreset.debug) Game.Log("Audio: Playing sound 2D sound: " + eventPreset.name);

        FMOD.GUID fmodEvent = FMOD.GUID.Parse(eventPreset.guid);

        //Setup event instance
        FMOD.Studio.EventInstance step = FMODUnity.RuntimeManager.CreateInstance(fmodEvent);

        //Load passed paramters
        if (parameters != null)
        {
            foreach (FMODParam para in parameters)
            {
                step.setParameterByName(para.name, para.value);
            }
        }

        //Play
        step.start();

        //Override master volume with occlusion volume * overrides
        step.setVolume(Mathf.Clamp01(eventPreset.masterVolumeScale * volumeOverride));

        //Relase after start to clean after playing
        step.release();

        return step;
    }

    public void Play2DSoundDelayed(AudioEvent eventPreset, float delay, List<FMODParam> parameters = null, float volumeOverride = 1f)
    {
        if (!SessionData.Instance.startedGame || (CityConstructor.Instance != null && CityConstructor.Instance.preSimActive)) return;
        delayedSound.Add(new DelayedSoundInfo(delay, eventPreset, null, null, Vector3.zero, parameters, volumeOverride, null, true, newIs2D: true));
    }

    //Calculate audio from one game world location to another, applying differences between rooms, buildings etc.
    //This has two main functions; to apply room geometry to sounds and also to detect AI listening
    //THe output is occluded volume.
    public float GetOcculusion(NewNode listenerLocation, NewNode sourceLocation, AudioEvent audioEvent, float baseVolume, Actor soundMaker, SoundMaterialOverride detailedMaterialData, out int penetrationCount, out List<ActiveListener> activeListeners, out bool isSuspicious, out List<NewRoom> audibleRooms, out float rangeHearing, List<NewNode> additionalLocations = null, bool forceSuspicious = false)
    {
        //If this is suspicious carry on looping until all listeners have been triggered.
        //If not we can return safety as soon as we have found the the listener.
        float occludedVolume = baseVolume; //The result of this will be applied to the sound being played at the location of the listener
        penetrationCount = 0; //The result will return how many occlusion units the sound has passed through to reach the listener
        activeListeners = new List<ActiveListener>(); //This will return all the actors that can hear this sound
        isSuspicious = false;
        audibleRooms = new List<NewRoom>();
        rangeHearing = 0f;

        if (SessionData.Instance.isFloorEdit || SessionData.Instance.isTestScene) return occludedVolume;

        //Filtering events: Looping could potentially be expensive, so try to filter as much as possible before doing so...
        //No sounds can penetrate multiple buildings...
        if (listenerLocation.building != null && sourceLocation.building != null)
        {
            if (listenerLocation.building != sourceLocation.building)
            {
                occludedVolume = 0f;
                if (audioEvent.debug) Game.Log("Audio: Sound cannot penetrate multiple buildings, resulting in a 0 occlusion value");
                return occludedVolume;
            }
        }

        //No sounds can penetrate more than 3 floors
        if (Mathf.Abs(listenerLocation.nodeCoord.z - sourceLocation.nodeCoord.z) > 3)
        {
            occludedVolume = 0f;
            if (audioEvent.debug) Game.Log("Audio: Sound cannot penetrate more than 3 floors, resulting in a 0 occlusion value");
            return occludedVolume;
        }

        //Calculate sound ranges
        float rangeActual = 0f;

        if(audioEvent.guid.Length > 0)
        {
            FMOD.GUID fmodEvent = FMOD.GUID.Parse(audioEvent.guid);
            FMOD.Studio.EventDescription description;
            FMODUnity.RuntimeManager.StudioSystem.getEventByID(fmodEvent, out description);
            description.getMinMaxDistance(out _, out rangeActual);
        }

        rangeHearing = audioEvent.hearingRange;

        //Apply stealth, running modifiers...
        if(soundMaker != null)
        {
            if (soundMaker.isRunning) rangeHearing += audioEvent.runModifier;
            if (soundMaker.stealthMode) rangeHearing += audioEvent.stealthModeModifier;
        }

        //Modify by material...
        if(audioEvent.modifyBasedOnSurface && detailedMaterialData != null)
        {
            rangeHearing += detailedMaterialData.carpet * audioEvent.carpetHearingRangeModifier;
            rangeHearing += detailedMaterialData.concrete * audioEvent.concreteHearingRangeModifier;
            rangeHearing += detailedMaterialData.fabric * audioEvent.fabricHearingRangeModifier;
            rangeHearing += detailedMaterialData.glass * audioEvent.glassHearingRangeModifier;
            rangeHearing += detailedMaterialData.metal * audioEvent.metalHearingRangeModifier;
            rangeHearing += detailedMaterialData.plaster * audioEvent.plasterHearingRangeModifier;
            rangeHearing += detailedMaterialData.tile * audioEvent.tileHearingRangeModifier;
            rangeHearing += detailedMaterialData.wood * audioEvent.woodHearingRangeModifier;
        }

        rangeActual = Mathf.Max(rangeActual, rangeHearing); //The hearing range can't be more than the actual range 

        //Is this suspicious (ie should we check for AI listeners?)
        if((soundMaker == Player.Instance && !Game.Instance.inaudiblePlayer) || soundMaker != Player.Instance)
        {
            if (forceSuspicious)
            {
                isSuspicious = true;
            }
            else if (audioEvent.canBeSuspicious)
            {
                if (audioEvent.alwaysSuspicious)
                {
                    isSuspicious = true;
                }
                else if (audioEvent.suspiciousIfTresspassing)
                {
                    if (soundMaker != null)
                    {
                        //The sound maker is trespassing...
                        if (soundMaker.isTrespassing)
                        {
                            isSuspicious = true;
                        }
                    }
                }

                if(audioEvent.onlySuspiciousIfEmptyAddress)
                {
                    //For this to not be suspicious it needs player + two other citizens
                    if(sourceLocation.gameLocation.currentOccupants.Count > audioEvent.suspiciousIfCitizenCount + 1)
                    {
                        isSuspicious = false;
                    }
                }

                if(soundMaker != null)
                {
                    if(audioEvent.onlySuspiciousIfNotEnforcer && soundMaker.isEnforcer && soundMaker.isOnDuty)
                    {
                        isSuspicious = false;
                    }
                }
            }
        }

        //if(soundMaker != null) Game.Log(audioEvent.name + " is suspicious: " + isSuspicious + " by soundmaker " + soundMaker.name);

        //Get the volume modification per occlusion unit
        float volumeModPerUnit = occlusionUnitVolumeModifier;

        if(audioEvent.overrideOcclusionModifier)
        {
            volumeModPerUnit = audioEvent.occlusionUnitVolumeModifier;
        }

        if (volumeModPerUnit >= 0f)
        {
            Game.Log("Audio: Positive volume modification per unit detected for " + audioEvent.name + "! Look into this as it will cause occlusion errors!");
        }

        Dictionary<NewRoom, float> openSetVolume = new Dictionary<NewRoom, float>();
        Dictionary<NewRoom, int> openSetPenetration = new Dictionary<NewRoom, int>();
        Dictionary<NewRoom, int> openSetDistanceFromSouce = new Dictionary<NewRoom, int>();

        openSetVolume.Add(sourceLocation.room, occludedVolume);
        openSetPenetration.Add(sourceLocation.room, 0);
        openSetDistanceFromSouce.Add(sourceLocation.room, 0);

        //Add additional locations
        if (additionalLocations != null)
        {
            foreach(NewNode node in additionalLocations)
            {
                if(!openSetVolume.ContainsKey(node.room))
                {
                    openSetVolume.Add(node.room, occludedVolume);
                    openSetPenetration.Add(node.room, 0);
                    openSetDistanceFromSouce.Add(node.room, 0);
                }
            }
        }

        HashSet<NewRoom> closedSet = new HashSet<NewRoom>();
        int safety = Mathf.Max(sourceLocation.room.entrances.Count + 2, openSetVolume.Count + loopingMaximum); //Check 32 rooms before cancelling
        if (audioEvent.overrideMaximumLoops) safety = Mathf.Max(safety, audioEvent.overriddenMaxLoops);
        bool foundListener = false;

        while (openSetVolume.Count > 0 && safety > 0)
        {
            //Pick the open set with the highest value first
            NewRoom current = null; //Current room
            float currentVol = -99999f; //Curent volume
            int currentPenetration = 0;
            int currentDistanceFromSource = 0;

            //Always pick the highest volume of the remaining set to anylise first...
            foreach(KeyValuePair<NewRoom, float> pair in openSetVolume)
            {
                if(pair.Value > currentVol)
                {
                    current = pair.Key;
                    currentVol = openSetVolume[pair.Key];
                    currentPenetration = openSetPenetration[pair.Key];
                    currentDistanceFromSource = openSetDistanceFromSouce[pair.Key];
                }
            }

            if(current != null)
            {
                if (audioEvent.debug)
                {
                    try
                    {
                        Game.Log("Audio: Looping " + current.GetName() + " (" + current.roomID + ") on floor " + current.entrances[0].fromNode.nodeCoord.z + ": Vol: " + currentVol);
                    }
                    catch
                    {

                    }
                }

                //Add to audible rooms
                audibleRooms.Add(current);

                //The listener is in this room...
                if (!foundListener && current == listenerLocation.room)
                {
                    if (audioEvent.debug)
                    {
                        try
                        {
                            Game.Log("Audio: Found listener at " + listenerLocation.room.GetName() + " with " + currentPenetration + " applied and volume " + currentVol);
                        }
                        catch
                        {

                        }
                    }

                    //If this isn't a suspicious sound, return now, no need to carry on looping as we've found the listener...
                    if (!isSuspicious)
                    {
                        penetrationCount = currentPenetration;
                        occludedVolume = currentVol;
                        return occludedVolume;
                    }
                    else
                    {
                        //Otherwise record penetration status, but carry on looping so we can find AI listeners...
                        penetrationCount = currentPenetration;
                        occludedVolume = currentVol;
                    }

                    foundListener = true;
                }

                //Search for listeners...
                if (isSuspicious)
                {
                    foreach(Actor listener in current.currentOccupants)
                    {
                        //Actor listener = current.currentOccupants[i];
                        //Game.Log("Audio: ...Found listener: " + listener.name);

                        if (listener.isDead) continue;
                        if (listener.isPlayer) continue;
                        if (listener.isStunned) continue;
                        if (!listener.canListen) continue;
                        if (listener == soundMaker) continue;
                        if (listener.ai == null) continue; //No AI

                        Game.Log("Audio: ...Listener " + listener.name + " is active...");

                        //Investigate trigger (can be called after awakening above)
                        if (listener.ai != null && !listener.isAsleep)
                        {
                            //Debug.DrawRay(listener.lookAtThisTransform.position, sourceLocation.position - listener.lookAtThisTransform.position, Color.blue, 1.5f);

                            //Update investigation position if player isn't already seen
                            if (listener.seenIllegalThisCheck.Count <= 0)
                            {
                                //The source location shouldn't be somewhere that's illegal to me
                                bool illegalToMe = listener.IsTrespassing(sourceLocation.room, out _, out _, true);

                                if (!illegalToMe)
                                {
                                    //Game.Log("Audio: ...The source of the sound is not in an illegal location for " + listener.name);

                                    //Calculate volume based on max distance...
                                    float currentDistance = Vector3.Distance(listener.currentNode.position, sourceLocation.position);

                                    if (currentDistance <= rangeHearing)
                                    {
                                        float normalizedDistance = Mathf.Clamp01(currentDistance / rangeHearing); //Normalize this so we can apply rolloff
                                        float soundLevel = emulationRolloff.Evaluate(normalizedDistance) * currentVol * listener.hearingMultiplier;

                                        //Game.Log("Audio: ... Sound level for AI listener " + listener.name + ": " + soundLevel + " max: " + rangeHearing);

                                        if (soundLevel > aiHearingThreshold)
                                        {
                                            //If asleep there is a chance the AI won't hear, based on how deep a sleep they're in...
                                            if (listener.isAsleep)
                                            {
                                                //50% chance if the AI can hear this sound at all.
                                                if (Toolbox.Instance.Rand(0f, soundLevel) <= audioEvent.awakenChance)
                                                {
                                                    //Sleep wake promt...
                                                    listener.ai.AwakenPrompt();
                                                }
                                                else
                                                {
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                bool trespassCheck = false; //Check to see if the listener is allowed here...

                                                if(audioEvent.suspiciousIfTresspassing)
                                                {
                                                    trespassCheck = listener.IsTrespassing(sourceLocation.room, out _, out _, true);
                                                }

                                                //Don't alert with enforcers
                                                if(trespassCheck && soundMaker != null && soundMaker.isEnforcer)
                                                {
                                                    trespassCheck = false;
                                                }

                                                if(!trespassCheck)
                                                {
                                                    int esc = 0;

                                                    if (soundMaker != null && soundMaker.isTrespassing)
                                                    {
                                                        Human soundMakerHuman = soundMaker as Human;

                                                        if (soundMakerHuman != null)
                                                        {
                                                            esc = soundMakerHuman.trespassingEscalation;
                                                        }
                                                    }

                                                    activeListeners.Add(new ActiveListener { listener = listener, escalationLevel = esc, soundLevel = soundLevel });
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //Game.Log("Audio: ...The source of the sound is in an illegal location for " + listener.name);
                                }
                            }
                            else
                            {
                                //Game.Log("Audio: ...Listener " + listener.name + " has already seen the player, so we are disregarding this information.");
                            }
                        }
                    }
                }

                //Continue checking adjacent rooms...
                if (currentVol > 0.01f)
                {
                    //Loop entrances
                    foreach (NewNode.NodeAccess access in current.entrances)
                    {
                        float appliedVolume = currentVol;
                        int appliedPenetration = currentPenetration;

                        //Make sure we are within the sound's range...
                        float currentDistance = Vector3.Distance(sourceLocation.position, access.worldAccessPoint);
                        if (currentDistance > rangeActual) continue;

                        if (access.accessType == NewNode.NodeAccess.AccessType.window)
                        {
                            //Load override
                            int u = windowOcclusionUnits;
                            if (audioEvent.overrideWindowOcclusion) u = audioEvent.windowOcclusionUnits;

                            appliedPenetration += u;
                            appliedVolume += volumeModPerUnit * u;
                        }
                        else if (access.door == null)
                        {
                            //Load override
                            int u = openDoorOcclusionUnits;
                            if (audioEvent.overrideOpenDoorOcclusion) u = audioEvent.openDoorOcclusionUnits;

                            appliedPenetration += u;
                            appliedVolume += volumeModPerUnit * u;
                        }
                        else if (access.door != null)
                        {
                            if (access.door.isClosed)
                            {
                                //Load override
                                int u = closedDoorOcclusionUnits;
                                if (audioEvent.overrideClosedDoorOcclusion) u = audioEvent.closedDoorOcclusionUnits;

                                appliedPenetration += u;
                                appliedVolume += volumeModPerUnit * u;
                            }
                            else
                            {
                                //Load override
                                int u = openDoorOcclusionUnits;
                                if (audioEvent.overrideOpenDoorOcclusion) u = audioEvent.openDoorOcclusionUnits;

                                appliedPenetration += u;
                                appliedVolume += volumeModPerUnit * u;
                            }
                        }

                        //Also apply floor difference occlusion...
                        if((access.toNode.room != null && !access.toNode.room.IsOutside()) || (access.fromNode.room != null && !access.fromNode.room.IsOutside()))
                        {
                            int floorDiff = Mathf.Abs((int)access.toNode.nodeCoord.z - (int)access.fromNode.nodeCoord.z);
                            //Game.Log("Audio: Floor diff = " + floorDiff);
                            appliedPenetration += floorDiff * floorDifferenceOcclusionUnits;
                            appliedVolume += volumeModPerUnit * (floorDiff * floorDifferenceOcclusionUnits);
                        }

                        //Above maximum distance, apply another occlusion unit...
                        if (currentDistanceFromSource + 1 > maxRoomDistance)
                        {
                            appliedPenetration += 1;
                            appliedVolume += volumeModPerUnit;
                        }

                        //Add to open set
                        if (appliedVolume > 0.01f)
                        {
                            if (!closedSet.Contains(access.toNode.room))
                            {
                                if (!openSetPenetration.ContainsKey(access.toNode.room))
                                {
                                    openSetPenetration.Add(access.toNode.room, appliedPenetration);
                                    openSetVolume.Add(access.toNode.room, appliedVolume);
                                    openSetDistanceFromSouce.Add(access.toNode.room, currentDistanceFromSource + 1);
                                }
                                else
                                {
                                    openSetPenetration[access.toNode.room] = Mathf.Min(openSetPenetration[access.toNode.room], appliedPenetration);
                                    openSetVolume[access.toNode.room] = Mathf.Max(openSetVolume[access.toNode.room], appliedVolume);
                                    openSetDistanceFromSouce[access.toNode.room] = Mathf.Min(openSetDistanceFromSouce[access.toNode.room], currentDistanceFromSource + 1);
                                }
                            }
                        }
                    }

                    //Loop through walls...
                    if (audioEvent.canPenetrateWalls)
                    {
                        //Load override
                        int u = wallOcclusionUnits;
                        if (audioEvent.overrideWallOcclusion) u = audioEvent.wallOcclusionUnits;

                        float appliedVolume = currentVol + (u * volumeModPerUnit);
                        int appliedPenetration = currentPenetration + u;

                        //Above maximum distance, apply another occlusion unit...
                        if (currentDistanceFromSource + 1 > maxRoomDistance)
                        {
                            appliedPenetration += 1;
                            appliedVolume += volumeModPerUnit;
                        }

                        if (appliedVolume > currentVol)
                        {
                            Game.Log("Audio: Increased applied volume detected for " + audioEvent.name + "! This could cause weird occlusion errors: " + currentVol + " + (" + u + " * " + volumeModPerUnit);
                        }

                        //Add to open set
                        if (appliedVolume > 0.01f)
                        {
                            foreach (NewRoom room in current.adjacentRooms)
                            {
                                if (room == current) continue;

                                if (!closedSet.Contains(room))
                                {
                                    if (!openSetPenetration.ContainsKey(room))
                                    {
                                        openSetVolume.Add(room, appliedVolume);
                                        openSetPenetration.Add(room, appliedPenetration);
                                        openSetDistanceFromSouce.Add(room, currentDistanceFromSource + 1);
                                    }
                                    else
                                    {
                                        openSetPenetration[room] = Mathf.Min(openSetPenetration[room], appliedPenetration);
                                        openSetVolume[room] = Mathf.Max(openSetVolume[room], appliedVolume);
                                        openSetDistanceFromSouce[room] = Mathf.Min(openSetDistanceFromSouce[room], currentDistanceFromSource + 1);
                                    }
                                }
                            }
                        }
                    }

                    //Loop through ceilings...
                    if (audioEvent.canPenetrateCeilings)
                    {
                        //Load override
                        int u = ceilingOcclusionUnits;
                        if (audioEvent.overrideCeilingOcclusion) u = audioEvent.ceilingOcclusionUnits;

                        float appliedVolume = currentVol + (u * volumeModPerUnit) + (floorDifferenceOcclusionUnits * volumeModPerUnit);
                        int appliedPenetration = currentPenetration + u + 1;

                        //Above maximum distance, apply another occlusion unit...
                        if (currentDistanceFromSource + 1 > maxRoomDistance)
                        {
                            appliedPenetration += 1;
                            appliedVolume += volumeModPerUnit;
                        }

                        if(appliedVolume > currentVol)
                        {
                            Game.Log("Audio: Increased applied volume detected for " + audioEvent.name + "! This could cause weird occlusion errors: " + currentVol + " + (" + u + " * " + volumeModPerUnit + " " + floorDifferenceOcclusionUnits);
                        }

                        if (appliedVolume > 0.01f)
                        {
                            foreach (NewRoom room in current.aboveRooms)
                            {
                                if (room == current) continue;

                                if (!closedSet.Contains(room))
                                {
                                    if (!openSetPenetration.ContainsKey(room))
                                    {
                                        openSetVolume.Add(room, appliedVolume);
                                        openSetPenetration.Add(room, appliedPenetration);
                                        openSetDistanceFromSouce.Add(room, currentDistanceFromSource + 1);
                                    }
                                    else
                                    {
                                        openSetPenetration[room] = Mathf.Min(openSetPenetration[room], appliedPenetration);
                                        openSetVolume[room] = Mathf.Max(openSetVolume[room], appliedVolume);
                                        openSetDistanceFromSouce[room] = Mathf.Min(openSetDistanceFromSouce[room], currentDistanceFromSource + 1);
                                    }
                                }
                            }
                        }
                    }

                    //Loop through floors...
                    if (audioEvent.canPenetrateFloors)
                    {
                        //Load override
                        int u = floorOcclusionUnits;
                        if (audioEvent.overrideFloorOcclusion) u = audioEvent.floorOcclusionUnits;

                        float appliedVolume = currentVol + (u * volumeModPerUnit) + (floorDifferenceOcclusionUnits * volumeModPerUnit);
                        int appliedPenetration = currentPenetration + u + 1;

                        //Above maximum distance, apply another occlusion unit...
                        if (currentDistanceFromSource + 1 > maxRoomDistance)
                        {
                            appliedPenetration += 1;
                            appliedVolume += volumeModPerUnit;
                        }

                        if (appliedVolume > currentVol)
                        {
                            Game.Log("Audio: Increased applied volume detected for " + audioEvent.name + "! This could cause weird occlusion errors: " + currentVol + " + (" + u + " * " + volumeModPerUnit + " " + floorDifferenceOcclusionUnits);
                        }

                        //Add to open set
                        if (appliedVolume > 0.01f)
                        {
                            foreach (NewRoom room in current.belowRooms)
                            {
                                if (room == current) continue;

                                if (!closedSet.Contains(room))
                                {
                                    if (!openSetPenetration.ContainsKey(room))
                                    {
                                        openSetVolume.Add(room, appliedVolume);
                                        openSetPenetration.Add(room, appliedPenetration);
                                        openSetDistanceFromSouce.Add(room, currentDistanceFromSource + 1);
                                    }
                                    else
                                    {
                                        openSetPenetration[room] = Mathf.Min(openSetPenetration[room], appliedPenetration);
                                        openSetVolume[room] = Mathf.Max(openSetVolume[room], appliedVolume);
                                        openSetDistanceFromSouce[room] = Mathf.Min(openSetDistanceFromSouce[room], currentDistanceFromSource + 1);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            closedSet.Add(current);
            openSetPenetration.Remove(current);
            openSetVolume.Remove(current);
            openSetDistanceFromSouce.Remove(current);
            safety--;
        }

        //If we haven't found the listener, this sound is out of range.
        if(!foundListener)
        {
            if (audioEvent.debug) Game.Log("Audio: Unable to find the listener; resulting in 0 occlusion value");
            occludedVolume = 0;
        }
        else
        {
            if (audioEvent.debug) Game.Log("Audio: Returning with found listener at " + listenerLocation.room.GetName() + " with occlusion volume " + occludedVolume);
        }

        //Game.Log("Occlusion unable to find listener node " + listenerLocation.name + " from source at " + sourceLocation.name + " with ai occlusion start of " + aiOcclusionValue);
        return occludedVolume;
    }

    //Special version of the above but for ambient zones (outputs volume adjusted with distance and occlusion)
    public float GetAmbientZoneOcculusion(NewNode listenerLocation, AmbientZoneInstance ambientZone, out float distance, out int penetrationCount, out NewRoom audibleRoom)
    {
        //If this is suspicious carry on looping until all listeners have been triggered.
        //If not we can return safety as soon as we have found the the listener.
        float occludedVolume = 0f; //The result of this will be applied to the sound being played at the location of the listener
        penetrationCount = 0; //The result will return how many occlusion units the sound has passed through to reach the listener
        distance = 99999f;
        audibleRoom = null;

        if (SessionData.Instance.isFloorEdit || SessionData.Instance.isTestScene || listenerLocation == null || listenerLocation.room == null) return occludedVolume;

        //Get the volume modification per occlusion unit
        float volumeModPerUnit = occlusionUnitVolumeModifier;

        if (ambientZone.preset.overrideOcclusionModifier)
        {
            volumeModPerUnit = ambientZone.preset.occlusionUnitVolumeModifier;
        }

        Dictionary<NewRoom, float> openSetVolume = new Dictionary<NewRoom, float>();
        Dictionary<NewRoom, int> openSetPenetration = new Dictionary<NewRoom, int>();
        Dictionary<NewRoom, float> openSetActualDistance = new Dictionary<NewRoom, float>();

        //Add source locations...
        foreach(NewRoom r in ambientZone.rooms)
        {
            openSetVolume.Add(r, 1f);
            openSetPenetration.Add(r, 0);
            openSetActualDistance.Add(r, 0);
        }

        HashSet<NewRoom> closedSet = new HashSet<NewRoom>();
        int safety = loopingMaximum; //Check 32 rooms before cancelling

        while (openSetVolume.Count > 0 && safety > 0)
        {
            //Pick the open set with the highest value first
            NewRoom current = null; //Current room
            float currentVol = 0; //Curent volume
            int currentPenetration = 0;
            int currentDistanceFromSource = 0;
            float currentActualDistance = 0;

            //Always pick the highest volume of the remaining set to anylise first...
            foreach (KeyValuePair<NewRoom, float> pair in openSetVolume)
            {
                if (pair.Value > currentVol)
                {
                    current = pair.Key;
                    currentVol = openSetVolume[pair.Key];
                    currentPenetration = openSetPenetration[pair.Key];
                    currentActualDistance = openSetActualDistance[pair.Key];
                }
            }

            if (current != null)
            {
                //The listener is in this room...
                if (current == listenerLocation.room)
                {
                    //If the volume is at maximum, return here...
                    if(currentVol >= 0.9999f)
                    {
                        penetrationCount = currentPenetration;
                        occludedVolume = currentVol;
                        distance = currentActualDistance;
                        audibleRoom = current;

                        return currentVol;
                    }
                    //Else save max to return later...
                    else if(currentVol > occludedVolume)
                    {
                        penetrationCount = currentPenetration;
                        occludedVolume = currentVol;
                        distance = currentActualDistance;
                        audibleRoom = current;
                    }
                }

                float falloff1 = ambientFalloff.Evaluate(currentActualDistance / ambientZone.preset.maxRange);

                //Continue checking adjacent rooms...
                if (currentVol > 0.01f)
                {
                    //Loop entrances
                    foreach (NewNode.NodeAccess access in current.entrances)
                    {
                        if (access.toNode.room == null) continue;

                        float appliedVolume = currentVol;
                        int appliedPenetration = currentPenetration;
                        float appliedDistance = currentActualDistance;

                        ////Pass through building wall check
                        //if (access.fromNode.building != access.toNode.building || access.fromNode.room.isOutsideWindow != access.toNode.room.isOutsideWindow)
                        //{
                        //    if (passedThroughBuildingWalls)
                        //    {
                        //        continue;
                        //    }
                        //    else passedThroughBuildingWalls = true; //Toggle flag
                        //}

                        //Distance check
                        float distanceThis = Vector3.Distance(listenerLocation.position, access.worldAccessPoint);

                        //Apply distance falloff
                        appliedDistance += distanceThis;
                        if (appliedDistance > ambientZone.preset.maxRange) continue;
                        float falloff2 = ambientFalloff.Evaluate(distanceThis / ambientZone.preset.maxRange);

                        appliedVolume += falloff2 - falloff1;
                        if (appliedVolume < 0.01f) continue;

                        if (access.accessType == NewNode.NodeAccess.AccessType.window)
                        {
                            appliedPenetration += windowOcclusionUnits;
                            appliedVolume += volumeModPerUnit * windowOcclusionUnits;
                        }
                        else if (access.door == null)
                        {
                            appliedPenetration += openDoorOcclusionUnits;
                            appliedVolume += volumeModPerUnit * openDoorOcclusionUnits;
                        }
                        else if (access.door != null)
                        {
                            if (access.door.isClosed)
                            {
                                //Don't penetrate doors...
                                if(!ambientZone.preset.canPenetrateClosedDoors)
                                {
                                    continue;
                                }

                                appliedPenetration += closedDoorOcclusionUnits;
                                appliedVolume += volumeModPerUnit * closedDoorOcclusionUnits;
                            }
                            else
                            {
                                appliedPenetration += openDoorOcclusionUnits;
                                appliedVolume += volumeModPerUnit * openDoorOcclusionUnits;
                            }
                        }

                        //Also apply floor difference occlusion...
                        int floorDiff = Mathf.Abs((int)access.toNode.nodeCoord.z - (int)access.fromNode.nodeCoord.z);
                        //Game.Log("Audio: Floor diff = " + floorDiff);
                        appliedPenetration += floorDiff * floorDifferenceOcclusionUnits;
                        appliedVolume += volumeModPerUnit * (floorDiff * floorDifferenceOcclusionUnits);

                        //Above maximum distance, apply another occlusion unit...
                        if (currentDistanceFromSource + 1 > maxRoomDistance)
                        {
                            appliedPenetration += 1;
                            appliedVolume += volumeModPerUnit;
                        }

                        //Add to open set
                        if (appliedVolume > 0.01f)
                        {
                            if (!closedSet.Contains(access.toNode.room))
                            {
                                if (!openSetPenetration.ContainsKey(access.toNode.room))
                                {
                                    openSetPenetration.Add(access.toNode.room, appliedPenetration);
                                    openSetVolume.Add(access.toNode.room, appliedVolume);
                                    openSetActualDistance.Add(access.toNode.room, appliedDistance);
                                }
                                else
                                {
                                    openSetPenetration[access.toNode.room] = Mathf.Min(openSetPenetration[access.toNode.room], appliedPenetration);
                                    openSetVolume[access.toNode.room] = Mathf.Max(openSetVolume[access.toNode.room], appliedVolume);
                                    openSetActualDistance[access.toNode.room] = Mathf.Min(openSetActualDistance[access.toNode.room], appliedDistance);
                                }
                            }
                        }
                    }
                }

                closedSet.Add(current);
                openSetPenetration.Remove(current);
                openSetVolume.Remove(current);
                openSetActualDistance.Remove(current);
            }


            safety--;
        }

        //Game.Log("Occlusion unable to find listener node " + listenerLocation.name + " from source at " + sourceLocation.name + " with ai occlusion start of " + aiOcclusionValue);
        return occludedVolume;
    }

    //Switch to outline if player if player has illegal status
    public void ForceOutlineCheck(AudioEvent audioEvent, Interactable inter, bool forceOff = false)
    {
        if(inter == null)
        {
            Game.LogError("Trying to set forced outline for " + audioEvent.name + " but interactable isn't passed...");
            return;
        }

        if (inter.controller == null) return;

        Transform[] children = inter.controller.transform.GetComponentsInChildren<Transform>();

        if (!Player.Instance.illegalStatus || forceOff)
        {
            inter.controller.transform.gameObject.layer = 0;

            foreach (Transform t in children)
            {
                //Set to default layer
                t.gameObject.layer = 0;
            }
        }
        else
        {
            inter.controller.transform.gameObject.layer = 30;

            foreach (Transform t in children)
            {
                //Set to outline layer
                t.gameObject.layer = 30;
            }
        }
    }

    //Return the player's sound level after occlusion has been applied
    public float GetPlayersSoundLevel(NewNode sourceLocation, AudioEvent audioEvent, float occludedVolume, SoundMaterialOverride detailedMaterialData)
    {
        float soundLevel = 0f;

        //Get the hearing range...
        float rangeHearing = audioEvent.hearingRange;

        //Modify by material...
        if (audioEvent.modifyBasedOnSurface && detailedMaterialData != null)
        {
            rangeHearing += detailedMaterialData.carpet * audioEvent.carpetHearingRangeModifier;
            rangeHearing += detailedMaterialData.concrete * audioEvent.concreteHearingRangeModifier;
            rangeHearing += detailedMaterialData.fabric * audioEvent.fabricHearingRangeModifier;
            rangeHearing += detailedMaterialData.glass * audioEvent.glassHearingRangeModifier;
            rangeHearing += detailedMaterialData.metal * audioEvent.metalHearingRangeModifier;
            rangeHearing += detailedMaterialData.plaster * audioEvent.plasterHearingRangeModifier;
            rangeHearing += detailedMaterialData.tile * audioEvent.tileHearingRangeModifier;
            rangeHearing += detailedMaterialData.wood * audioEvent.woodHearingRangeModifier;
        }

        //Calculate volume based on max distance...
        float currentDistance = Vector3.Distance(Player.Instance.transform.position, sourceLocation.position);

        if (currentDistance <= rangeHearing)
        {
            float normalizedDistance = currentDistance / rangeHearing; //Normalize this so we can apply rolloff
            soundLevel = emulationRolloff.Evaluate(normalizedDistance) * occludedVolume * Player.Instance.hearingMultiplier;          
        }

        return soundLevel;
    }

    //Update ambient zones on end of frame
    public void UpdateAmbientZonesOnEndOfFrame()
    {
        if (Toolbox.Instance != null) Toolbox.Instance.InvokeEndOfFrame(updateAmbientZonesAction, "Update ambient zones");
    }

    //Update ambient zones
    public void UpdateAmbientZones()
    {
        //Update ambient indoors/outdoors
        PassIndoorOutdoor();

        //Update distance from edge
        UpdateDistanceFromEdge();

        //Update music cue
        Player.Instance.UpdatePlayerAmbientState();

        foreach (AmbientZoneInstance amb in ambientZones)
        {
            amb.isActive = false;
            amb.desiredVolume = 0f;
            amb.playerDistance = 0f;
            amb.penetrationCount = 0;
            amb.audibleRoom = null;

            //Player is in air duct
            //float volPen = 0f; //Other rooms play but can have volume penalty in air duct

            if(amb.preset.isAirDuctAmbience && Player.Instance.inAirVent)
            {
                amb.isActive = true;
                amb.desiredVolume = 1f;
                amb.audibleRoom = Player.Instance.currentRoom;
                //volPen = amb.preset.otherRoomVolumePenalty;
            }

            //Player is in room
            if(amb.rooms.Contains(Player.Instance.currentRoom))
            {
                amb.isActive = true;
                amb.desiredVolume = 1f;
                amb.audibleRoom = Player.Instance.currentRoom;
            }
            //Find occlusion...
            else if(amb.preset.useOcclusion)
            {
                amb.desiredVolume = Mathf.Clamp01(GetAmbientZoneOcculusion(Player.Instance.currentNode, amb, out amb.playerDistance, out amb.penetrationCount, out amb.audibleRoom));

                if(amb.desiredVolume > 0.01f)
                {
                    amb.isActive = true;
                }
            }
        }
    }

    public void ResetThis()
    {
        while(loopingSounds.Count > 0)
        {
            StopSound(loopingSounds[0], StopType.immediate, "Reset audio controller");
        }
    }

    public void SetVCALevel(string vcaName, float value)
    {
        try
        {
            VCA vca = FMODUnity.RuntimeManager.GetVCA("vca:/" + vcaName);
            Game.Log("Audio: Setting " + vcaName + " VCA to " + value);
            vca.setVolume(value);
        }
        catch
        {
            Game.LogError("Unable to find VCA: " + vcaName);
        }
    }

    [Button]
    public void UpdateAmbientPlaybackState()
    {
        if (ambienceRain != null) ambienceRain.UpdatePlayState();
        if (ambienceWind != null) ambienceWind.UpdatePlayState();
        if (ambiencePA != null) ambiencePA.UpdatePlayState();
    }

    public void StopAllSounds()
    {
        foreach(LoopingSoundInfo loop in loopingSounds)
        {
            StopSound(loop, StopType.immediate, "stop all");
        }

        foreach(AmbientZoneInstance amb in ambientZones)
        {
            StopSound(amb.eventData, StopType.immediate, "stop all");
        }

        StopSound(ambienceWind, StopType.immediate, "stop all");
        StopSound(ambienceRain, StopType.immediate, "stop all");
        StopSound(ambiencePA, StopType.immediate, "stop all");
    }

    public void UpdateVolumeChanging()
    {
        List<LoopingSoundInfo> toRemove = new List<LoopingSoundInfo>();

        foreach(LoopingSoundInfo loop in volumeChangingSounds)
        {
            float vol = 0f;
            loop.audioEvent.getVolume(out vol);

            if(loop.fadeToVolume != vol)
            {
                float fadeTime = 0.5f;
                if (loop.eventPreset.forceVolumeLevelFadeTime) fadeTime = loop.eventPreset.volumeLevelFadeTime;

                if(vol < loop.fadeToVolume)
                {
                    vol += Time.deltaTime / fadeTime;
                    vol = Mathf.Min(vol, loop.fadeToVolume);
                }
                else if (vol > loop.fadeToVolume)
                {
                    vol -= Time.deltaTime / fadeTime;
                    vol = Mathf.Max(vol, loop.fadeToVolume);
                }

                if (loop.eventPreset.debug) Game.Log("Audio: " + loop.eventPreset.name + " Actively fading to new audio level: " + vol + "/" + loop.fadeToVolume);
                loop.audioEvent.setVolume(vol);

                if(vol == loop.fadeToVolume)
                {
                    toRemove.Add(loop);
                }
            }
            else
            {
                toRemove.Add(loop);
            }
        }

        foreach(LoopingSoundInfo loop in toRemove)
        {
            volumeChangingSounds.Remove(loop);
        }
    }

    [Button]
    public void DebugWeatherLoopDisplay()
    {
        if(ambienceRain != null)
        {
            ambienceRain.UpdatePlayState();
            Game.Log("Rain: " + ambienceRain.state);
        }

        if (ambienceWind != null)
        {
            ambienceWind.UpdatePlayState();
            Game.Log("Wind: " + ambienceWind.state);
        }

        if (ambiencePA != null)
        {
            ambiencePA.UpdatePlayState();
            Game.Log("PA: " + ambiencePA.state);
        }
    }
}
