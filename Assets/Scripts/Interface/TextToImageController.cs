using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using NaughtyAttributes;
using System.IO;
using System.Linq;
using UnityEngine.UI;
using UnityEditor;

public class TextToImageController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform captureTextCanvasRect;
    public TextMeshProUGUI captureText;
    public Material newsTickerMaterial;
    public TMP_FontAsset newsTickerFont;
    public float newsTickerFontSize = 40;
    public float newsTickerDivider = 128f;
    public float newsTickerSpeedDivider = 4f;

    [Header("Settings")]
    public bool saveDebugScreenshot = false;
    public Vector2 maxSize = new Vector2(2048, 128);
    public TextToImageSettings defaultSettings = new TextToImageSettings();

    [Header("Preview")]
    public string lastText;
    [ReadOnly]
    public float lastFontSize = 0;
    [ReadOnly]
    public Vector2 lastDimenstions;
    [ShowAssetPreview]
    public Texture2D currentShot;
    private RenderTexture instancedRender;

    [System.Serializable]
    public class TextToImageSettings
    {
        public string textString = "Example";
        public float textSize = 42;
        public TMP_FontAsset font;
        public bool enableProcessing = true;
        public float contrast = 1.2f;
        public bool trim = true;
        public int trimPadding = 4;
        public bool useAlpha = true;
        public Color color = Color.white;
    }

    //Singleton pattern
    private static TextToImageController _instance;
    public static TextToImageController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    [Button]
    public Texture2D CaptureTextToImage(TextToImageSettings settings = null)
    {
        if (settings == null) settings = defaultSettings;

        //Set text
        captureText.enableAutoSizing = false;
        captureText.rectTransform.sizeDelta = new Vector2(9999, 0);

        captureText.font = settings.font;
        captureText.fontSize = settings.textSize;
        captureText.SetText(settings.textString);

        captureText.UpdateFontAsset();
        captureText.UpdateMeshPadding();
        captureText.ForceMeshUpdate();

        Vector2 desired = captureText.GetPreferredValues();

        int width = Mathf.CeilToInt(desired.x + 1);
        int height = Mathf.CeilToInt(desired.y + 1);
        //Game.Log("Desired text size: " + width + ", " + height);

        if(width > maxSize.x || height > maxSize.y)
        {
            width = Mathf.Min(width, (int)maxSize.x);
            height = Mathf.Min(height, (int)maxSize.y);

            captureText.rectTransform.sizeDelta = new Vector2(width, height);
            //Game.Log("Text has exceeded maximum size, using text scaling to " + maxSize + " instead...");

            captureText.enableAutoSizing = true;
            captureText.fontSizeMax = settings.textSize;
            captureText.fontSizeMin = 1;
        }
        else captureText.rectTransform.sizeDelta = new Vector2(width, height);

        captureText.UpdateMeshPadding();
        captureText.ForceMeshUpdate();

        lastText = captureText.text;
        lastFontSize = captureText.fontSize;
        lastDimenstions = new Vector2(width, height);

        //Enable photo room
        SceneCapture.Instance.photoRoomParent.SetActive(true);

        //Set orthographic
        CameraController.Instance.cam.orthographic = true;

        RenderTexture tempRT = new RenderTexture((int)maxSize.x, (int)maxSize.y, 24);
        tempRT.antiAliasing = 1;
        tempRT.filterMode = FilterMode.Point;
        tempRT.autoGenerateMips = false;
        tempRT.useDynamicScale = false;

        captureTextCanvasRect.sizeDelta = new Vector2(width, height);

        currentShot = SceneCapture.Instance.CaptureScene(SceneCapture.Instance.cameraTransform.position, SceneCapture.Instance.cameraTransform.eulerAngles, Toolbox.Instance.textToImageMask, false, 0f, tempRT, useCaptureLight: false, basicMode: true, ignoreEarlyCapError: true, captureProfile: SceneCapture.PostProcessingProfile.captureNormal);
        currentShot.filterMode = FilterMode.Point; //Make sure we have point sampling for this

        //Set orthographic
        CameraController.Instance.cam.orthographic = false;

        //Disable photo room
        SceneCapture.Instance.photoRoomParent.SetActive(false);

        if(saveDebugScreenshot && Game.Instance.devMode && Game.Instance.collectDebugData)
        {
#if UNITY_EDITOR

            if (currentShot != null)
            {
                byte[] bytes;
                bytes = currentShot.EncodeToPNG();

                string path = Application.streamingAssetsPath + "/Debug/TextToImageScreenshotDebug.png";
                Debug.Log("Writing: " + path);

                System.IO.File.WriteAllBytes(
                    path, bytes);

                AssetDatabase.Refresh();
            }
#endif
        }

        if (settings.enableProcessing) ProcessImage(settings);

        tempRT.Release();
        Destroy(tempRT);

        return currentShot;
    }

    [Button]
    public void UpdateNewsTickerHeadline(string newString = "")
    {
        if (newString.Length <= 0) newString = defaultSettings.textString;

        Game.Log("Gameplay: Updating news ticker: " + newString);

        TextToImageSettings tickerSettings = new TextToImageSettings();
        tickerSettings.textString = Strings.Get("misc", "newsstand_ticker") + " � " + newString + " � ";
        tickerSettings.font = newsTickerFont;
        tickerSettings.textSize = newsTickerFontSize;
        tickerSettings.trim = true;
        tickerSettings.trimPadding = 7;
        tickerSettings.useAlpha = false;
        tickerSettings.color = Color.white;

        Texture2D tickerImg = CaptureTextToImage(tickerSettings);

        newsTickerMaterial.SetTexture("_MarqueeImage", tickerImg);

        float w = newsTickerDivider / tickerImg.width;

        newsTickerMaterial.SetVector("_SignArea", new Vector4(w, 1f, 0f, 0f));
        newsTickerMaterial.SetFloat("_Speed", w / newsTickerSpeedDivider);
    }

    [Button]
    public void ProcessImage(TextToImageSettings settings = null)
    {
        if (settings == null) settings = defaultSettings;

        if(currentShot != null)
        {
            //Game.Log("Processing image with alpha: " + settings.useAlpha);

            Vector2 trimRectStart = new Vector2(99999, 99999);
            Vector2 trimRectEnd = Vector2.zero;

            for (int w = 0; w < currentShot.width; w++)
            {
                for (int h = 0; h < currentShot.height; h++)
                {
                    Color getPx = currentShot.GetPixel(w, h);

                    if(getPx.grayscale < 0.2f)
                    {
                        getPx = Color.black;
                    }
                    else
                    {
                        if (w < trimRectStart.x) trimRectStart.x = w;
                        if (h < trimRectStart.y) trimRectStart.y = h;

                        if (w > trimRectEnd.x) trimRectEnd.x = w;
                        if (h > trimRectEnd.y) trimRectEnd.y = h;

                        if (settings.contrast != 1f)
                        {
                            getPx *= settings.contrast;
                        }
                    }

                    //Convert to grayscale
                    Color32 pixel = getPx;
                    int p = ((256 * 256 + pixel.r) * 256 + pixel.b) * 256 + pixel.g;
                    int b = p % 256;
                    p = Mathf.FloorToInt(p / 256);
                    int g = p % 256;
                    p = Mathf.FloorToInt(p / 256);
                    int r = p % 256;
                    float l = (0.2126f * r / 255f) + 0.7152f * (g / 255f) + 0.0722f * (b / 255f);

                    Color c = new Color(l, l, l, 1);

                    currentShot.SetPixel(w, h, c);
                }
            }

            currentShot.Apply();

            //Game.Log("Trim start: " + trimRectStart + " trim end " + trimRectEnd);

            if (settings.trim)
            {
                trimRectStart = new Vector2(Mathf.Max(trimRectStart.x - settings.trimPadding, 0), Mathf.Max(trimRectStart.y - settings.trimPadding, 0));
                trimRectEnd = new Vector2(Mathf.Min(trimRectEnd.x + settings.trimPadding, currentShot.width), Mathf.Min(trimRectEnd.y + settings.trimPadding, currentShot.height));

                int newW = (int)trimRectEnd.x - (int)trimRectStart.x;
                int newH = (int)trimRectEnd.y - (int)trimRectStart.y;

                //Game.Log("Trim start: " + trimRectStart + " trim end " + trimRectEnd + " new size: " + newW + ", " + newH);

                Texture2D newTex = new Texture2D(newW, newH);
                newTex.filterMode = FilterMode.Point;

                for (int w = 0; w < newW; w++)
                {
                    for (int h = 0; h < newH; h++)
                    {
                        Color get = currentShot.GetPixel((int)trimRectStart.x + w, (int)trimRectStart.y + h);

                        float alpha = 1f;
                        if(settings.useAlpha) alpha = get.grayscale * 1.2f;

                        get = get * settings.color;
                        get.a = alpha;

                        newTex.SetPixel(w, h, get);
                    }
                }

                newTex.Apply();
                currentShot = newTex;
            }
        }
    }

    [Button]
    public void SavePNG()
    {
#if UNITY_EDITOR

        if (currentShot != null)
        {
            byte[] bytes;
            bytes = currentShot.EncodeToPNG();

            string path = Application.streamingAssetsPath + "/Debug/TextToImageDebug.png";
            Debug.Log("Writing: " + path);

            System.IO.File.WriteAllBytes(
                path, bytes);

            AssetDatabase.Refresh();

            //Change settings
            //TextureImporter importer = (TextureImporter)AssetImporter.GetAtPath(path);

            //if (importer != null)
            //{
            //    importer.textureType = TextureImporterType.Sprite;
            //    importer.mipmapEnabled = false;
            //    importer.filterMode = FilterMode.Trilinear;
            //    importer.textureShape = TextureImporterShape.Texture2D;
            //    importer.SaveAndReimport();
            //}
            //else
            //{
            //    Debug.LogError("Cannot find texture importer at " + path);
            //}
        }

#endif
    }
}
