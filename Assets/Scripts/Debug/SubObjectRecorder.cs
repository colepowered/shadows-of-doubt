﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class SubObjectRecorder : MonoBehaviour
{
    public FurniturePreset furniturePreset;

    [Button]
    public void RecordSubObjectPlacements()
    {
        List<FurniturePreset.SubObject> subObjects = new List<FurniturePreset.SubObject>();
        furniturePreset.subObjects.Clear();

        //Loop through all children, search for placement objects
        for (int i = 0; i < this.gameObject.transform.childCount; i++)
        {
            Transform child = this.gameObject.transform.GetChild(i);
            SubObjectPlacement sop = child.GetComponent<SubObjectPlacement>();

            if(sop != null)
            {
                FurniturePreset.SubObject newSO = new FurniturePreset.SubObject();
                newSO.preset = sop.preset;
                newSO.localPos = sop.transform.localPosition;
                newSO.localRot = sop.transform.localEulerAngles;
                newSO.belongsTo = sop.belongsTo;
                newSO.security = sop.security;

                //Add this to the preset
                furniturePreset.subObjects.Add(newSO);
            }

            //Cycle within children
            for (int u = 0; u < child.childCount; u++)
            {
                Transform child2 = child.GetChild(u);
                SubObjectPlacement sop2 = child2.GetComponent<SubObjectPlacement>();

                if (sop2 != null)
                {
                    FurniturePreset.SubObject newSO = new FurniturePreset.SubObject();
                    newSO.preset = sop2.preset;
                    newSO.localPos = sop2.transform.localPosition;
                    newSO.localRot = sop2.transform.localEulerAngles;
                    newSO.parent = child2.transform.parent.name;
                    newSO.belongsTo = sop2.belongsTo;
                    newSO.security = sop2.security;

                    //Add this to the preset
                    furniturePreset.subObjects.Add(newSO);
                }

                //Cycle within children
                for (int l = 0; l < child2.childCount; l++)
                {
                    Transform child3 = child2.GetChild(l);
                    SubObjectPlacement sop3 = child3.GetComponent<SubObjectPlacement>();

                    if (sop3 != null)
                    {
                        FurniturePreset.SubObject newSO = new FurniturePreset.SubObject();
                        newSO.preset = sop3.preset;
                        newSO.localPos = sop3.transform.localPosition;
                        newSO.localRot = sop3.transform.localEulerAngles;
                        newSO.parent = child3.transform.parent.name;
                        newSO.belongsTo = sop3.belongsTo;
                        newSO.security = sop3.security;

                        //Add this to the preset
                        furniturePreset.subObjects.Add(newSO);
                    }
                }
            }
        }

        //Force save data
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(furniturePreset);
#endif
    }
}
