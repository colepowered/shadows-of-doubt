﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BankStatementEntryController : MonoBehaviour
{
    public RectTransform rect;
    public TextMeshProUGUI descriptionText;
    public TextMeshProUGUI amountText;
    public TextMeshProUGUI balanceText;

    public void Setup(string text, int amount, int balance)
    {
        descriptionText.text = text;
        amountText.text = amount.ToString();
        balanceText.text = balance.ToString();
        name = text;

        if (balance < 0)
        {
            balanceText.color = Color.red;
        }
        else balanceText.color = Color.black;
    }
}
