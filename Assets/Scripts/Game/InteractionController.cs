﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using System.Linq;
using TMPro;
using System.Text;
using System.Reflection;
using UnityEditor;

public class InteractionController : MonoBehaviour
{
    public class InteractionSetting
    {
        public InteractablePreset.InteractionAction currentAction;
        public Interactable.InteractableCurrentAction currentSetting;
        [System.NonSerialized]
        public Interactable interactable;
        public bool isFPSItem = false;

        public AudioEvent audioEvent;
        //public List<CanvasRenderer> soundObjects;

        //Display references
        public int priority = 0;
        public string actionText;
        public ControlDisplayController newUIRef;

        public int GetActionCost()
        {
            int cost = 0;

            if (currentAction != null)
            {
                cost = currentAction.actionCost;

                //Get cost from placement
                if (currentAction.specialCase == InteractablePreset.InteractionAction.SpecialCase.decorPlacementPurchase)
                {
                    cost = PlayerApartmentController.Instance.GetCurrentCost();
                }
            }

            return cost;
        }
    }

    [Header("Interaction")]
    //List of current interactions: Inlcude NULL entries if the corresponding entry carries none.
    public Dictionary<InteractablePreset.InteractionKey, InteractionSetting> currentInteractions = new Dictionary<InteractablePreset.InteractionKey, InteractionSetting>();
    public List<InteractablePreset.InteractionKey> allInteractionKeys;
    public List<Interactable> nearbyInteractables = new List<Interactable>();
    public List<SelectionIconController> selectionIcons = new List<SelectionIconController>();
    public int nearbyInteractablesHint = 0;

    [Header("Player State")]
    public float inputCooldown = 0f; //Limit repeated interaction by a small amount
    public bool lookingAtInteractable = false;
    public bool displayingInteraction = false;
    private InteractableController previousLookingAtInteractable;
    public InteractableController currentLookingAtInteractable;
    public Transform currentLookAtTransform;
    private InteractableController currentLookingAtReadingRange;
    public InteractableController currentInteractable;
    public bool interactionMode = false;
    public bool distanceRecognitionMode = false;
    public bool readingMode = false;
    private float readingModeTransition = 0f;
    private Coroutine readingModeCoroutine = null;
    public float interactionAnimationModifier = 1f;
    public float interactionLookProgress = 0f;
    public InteractableController carryingObject = null;
    private List<NewDoor> addedToDoorInteractionList = new List<NewDoor>(); //List of door interaciton lists the player is part of.

    private RaycastHit playerPreviousRaycastHit;
    [System.NonSerialized]
    public RaycastHit playerCurrentRaycastHit; //Current game object the player is looking at

    [Header("Locked-In Interaction")]
    [System.NonSerialized]
    public Interactable lockedInInteraction = null;
    public int lockedInInteractionRef = 0; //For multiple types of locked-in interaction
    [System.NonSerialized]
    public Interactable hideInteractable = null; //Current hide interactable

    [Header("Interaction Action")]
    public bool activeInteractionAction = false;
    private float interactionActionAmount = 0f;
    private float interactionActionThreshold = 1f;
    private float interactionActionMultiplier = 1f;
    public string interactionActionName;
    //private float interactionActionBaseTime = 1f;
    private Transform interactionActionLookAt;
    private bool activeInteractionActionLookCheck = false;
    public GameObject lockpickGraphics;
    private bool cancelInteractionIfOutOfRange = true;

    private float lastLookAtForInteraction = 0f;

    //Discover things over this progres...
    public Dictionary<Interactable, float> discoveryOverTime = new Dictionary<Interactable, float>();
    public Dictionary<Evidence, float> discoveryOverTimeEvidence = new Dictionary<Evidence, float>();
    public Dictionary<MetaObject, float> discoveryOverTimeMeta = new Dictionary<MetaObject, float>();
    public Dictionary<EvidenceMultiPage.MultiPageContent, float> discoveryOverTimeDiscovery = new Dictionary<EvidenceMultiPage.MultiPageContent, float>();

    public List<LockpickProgressController> spawnedProgressControllers = new List<LockpickProgressController>();

    private Interactable sabotageInteractable;

    [Header("Dialog")]
    public bool dialogMode = false;
    public bool isRemote = false;
    public float dialogTransition = 0f;
    public ConversationType dialogType = ConversationType.normal;
    public TextMeshProUGUI citizenNameText;
    [System.NonSerialized]
    public Interactable talkingTo;
    [System.NonSerialized]
    public Interactable remoteOverride;
    public List<DialogButtonController> dialogOptions = new List<DialogButtonController>();
    public int dialogSelection = 0;
    public RectTransform moreOptionsScrollUpArrow;
    public RectTransform moreOptionsScrollDownArrow;

    //Use the below for mugging situations
    public Human mugger;
    public Human debtCollector;

    [Header("Interface")]
    //Interaction box display
    public bool inOut = false;
    public float inOutProgress = 0f;
    public float displayProgress = 0f;

    //Audio
    private AudioController.LoopingSoundInfo lockpickLoop;

    public enum ConversationType { normal, mugging, loanSharkVisit, accuseMurderer };

    //Events
    public delegate void ReturnFromLockedIn();
    public event ReturnFromLockedIn OnReturnFromLockedIn;

    //Triggered when interaction action is completed
    public delegate void InteractionActionCompleted();
    public event InteractionActionCompleted OnInteractionActionCompleted;

    //Triggered when interaction progress has changed
    public delegate void InteractionActionProgressChange(float amountThisFrame, float amountTotal);
    public event InteractionActionProgressChange OnInteractionActionProgressChange;

    //Triggered when interaction action is cancelled
    public delegate void InteractionActionCancelled();
    public event InteractionActionCancelled OnInteractionActionCancelled;

    //Singleton pattern
    private static InteractionController _instance;
    public static InteractionController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        DisplayInteractionCursor(false, true);

        //Create entries for all controls
        allInteractionKeys = InteractablePreset.InteractionKey.GetValues(typeof(InteractablePreset.InteractionKey)).Cast<InteractablePreset.InteractionKey>().ToList();

        foreach (InteractablePreset.InteractionKey key in allInteractionKeys)
        {
            currentInteractions.Add(key, new InteractionSetting());
        }
    }

    private void Update()
    {
        //Interaction- must be in play mode, and not in interaction mode
        if (SessionData.Instance.play && !CutSceneController.Instance.cutSceneActive)
        {
            if (carryingObject == null)
            {
                //Check for interactables
                if (!interactionMode)
                {
                    InteractionRaycastCheck();
                }
            }

            //Active action interaction
            if (activeInteractionAction)
            {
                activeInteractionActionLookCheck = false;

                //Must look @ this to completed
                if (interactionActionLookAt != null)
                {
                    //Ray ray = CameraController.Instance.cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                    Ray ray = new Ray(CameraController.Instance.cam.transform.position, CameraController.Instance.cam.transform.forward);

                    if (Physics.Raycast(ray, out playerCurrentRaycastHit, 10f, Toolbox.Instance.interactionRayLayerMask))
                    {
                        if (playerCurrentRaycastHit.transform == interactionActionLookAt)
                        {
                            activeInteractionActionLookCheck = true;
                            lastLookAtForInteraction = SessionData.Instance.gameTime;
                        }
                        else
                        {
                            Game.Log("Player: Interaction action raycast fail: " + playerCurrentRaycastHit.transform.name);
                        }
                    }
                    else
                    {
                        Game.Log("Player: Interaction action raycast fail: no hit");
                    }

                    //Display notification to look at object...
                    if(!activeInteractionActionLookCheck && SessionData.Instance.gameTime > lastLookAtForInteraction + 0.03f)
                    {
                        lastLookAtForInteraction = SessionData.Instance.gameTime;
                        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "Look at the target object to progress the action"), InterfaceControls.Icon.eye);
                    }
                }
                else
                {
                    activeInteractionActionLookCheck = true;
                }

                if(Player.Instance.isLockpicking)
                {
                    if (UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.triggerIllegalOnPick) > 0f)
                    {
                        InteractionController.Instance.SetIllegalActionActive(activeInteractionActionLookCheck);
                    }
                }

                //Cancel if too far away...
                if(lockedInInteraction != null && lockedInInteraction.controller != null)
                {
                    if (cancelInteractionIfOutOfRange && lockedInInteraction.controller.coll != null)
                    {
                        float dist = Vector3.Distance(CameraController.Instance.cam.transform.position, lockedInInteraction.controller.coll.bounds.center);
                        float iRange = lockedInInteraction.GetReachDistance() + 2.75f;

                        if (dist > iRange)
                        {
                            Game.Log("Player: Cancelled locked in interaction because of distance (" + dist + "/" + iRange + ")");
                            activeInteractionActionLookCheck = false;
                            InteractionController.Instance.SetLockedInInteractionMode(null);
                        }
                    }
                    else if(lockedInInteraction.controller.coll == null)
                    {
                        Game.Log("Player: Missing collider for locked in interaction on object: " + lockedInInteraction.controller.name + " (" + lockedInInteraction.id + ")");
                    }
                }
                else
                {
                    Game.Log("Player: Cancelled locked in interaction because interactable isn't spawned");
                    activeInteractionActionLookCheck = false;
                    InteractionController.Instance.SetLockedInInteractionMode(null);
                }
            }

            //Animate control graphics
            bool removedCustom = false;

            for (int i = 0; i < ControlsDisplayController.Instance.customActionsDisplayed.Count; i++)
            {
                ControlsDisplayController.CustomActionsDisplayed customAction = ControlsDisplayController.Instance.customActionsDisplayed[i];

                if(customAction.displayTime > 0f)
                {
                    customAction.displayTime -= Time.deltaTime;

                    if(customAction.displayTime <= 0f)
                    {
                        customAction.displayTime = 0f;
                        SetCurrentPlayerInteraction(customAction.key, null, null); //Set this
                        removedCustom = true;
                    }
                }
                else if(SessionData.Instance.gameTime >= customAction.lastDisplayedAt + ControlsDisplayController.Instance.minimumCustomControlDisplayTimeInterval)
                {
                    ControlsDisplayController.Instance.customActionsDisplayed.RemoveAt(i);
                    i--;
                }
            }

            if(removedCustom)
            {
                UpdateInteractionText();
            }

            //Detect interaction
            //Note: Recently added a transition check here- may possibly cause input problems (test)
            if (inputCooldown <= 0f && !InterfaceController.Instance.playerTextInputActive && !Player.Instance.transitionActive && (MapController.Instance == null || !MapController.Instance.displayFirstPerson))
            {
                InteractablePreset.InteractionKey input = InteractablePreset.InteractionKey.none;

                if (InputController.Instance.player.GetButtonDown("Primary"))
                {
                    input = InteractablePreset.InteractionKey.primary;
                }
                else if (InputController.Instance.player.GetButtonDown("Secondary"))
                {
                    input = InteractablePreset.InteractionKey.secondary;
                }
                else if (InputController.Instance.player.GetButtonDown("Alternative"))
                {
                    input = InteractablePreset.InteractionKey.alternative;
                }
                else if (InputController.Instance.player.GetButtonDown("ScrollAxisUp"))
                {
                    input = InteractablePreset.InteractionKey.scrollAxisUp;
                }
                else if (InputController.Instance.player.GetButtonDown("ScrollAxisDown"))
                {
                    input = InteractablePreset.InteractionKey.scrollAxisDown;
                }
                else if (InputController.Instance.player.GetButtonDown("NavigateUp"))
                {
                    input = InteractablePreset.InteractionKey.SelectUp;
                }
                else if (InputController.Instance.player.GetButtonDown("NavigateDown"))
                {
                    input = InteractablePreset.InteractionKey.SelectDown;
                }
                else if (InputController.Instance.player.GetButtonDown("LeanLeft"))
                {
                    input = InteractablePreset.InteractionKey.LeanLeft;
                }
                else if (InputController.Instance.player.GetButtonDown("LeanRight"))
                {
                    input = InteractablePreset.InteractionKey.LeanRight;
                }

                //else if (Mathf.Abs(InputController.Instance.player.GetAxis("Scroll Axis")) > 0)
                //{
                //    if(!MapController.Instance.displayFirstPerson || !SessionData.Instance.play)
                //    {
                //        float wheel = InputController.Instance.player.GetAxis("Scroll Axis");
                //        if (wheel > 0f) input = InteractablePreset.InteractionKey.scrollAxisUp;
                //        else if (wheel < 0f) input = InteractablePreset.InteractionKey.scrollAxisDown;
                //    }
                //}

                //bool usedControlAction = false;

                if (input != InteractablePreset.InteractionKey.none)
                {
                    //Weapon select
                    if(BioScreenController.Instance.isOpen)
                    {
                        //Select item
                        //if(input == InteractablePreset.InteractionKey.primary && FirstPersonItemController.Instance.hoveredSlot != null)
                        //{
                        //    //If this is already selected, then select empty...
                        //    if(FirstPersonItemController.Instance.selectedSlot == FirstPersonItemController.Instance.hoveredSlot)
                        //    {
                        //        FirstPersonItemController.Instance.SelectSlot(FirstPersonItemController.Instance.slots.Find(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.holster));
                        //    }
                        //    else
                        //    {
                        //        FirstPersonItemController.Instance.SelectSlot(FirstPersonItemController.Instance.hoveredSlot);
                        //    }
                        //}
                        //Decor home
                        //if(input == InteractablePreset.InteractionKey.secondary && Player.Instance.currentGameLocation == Player.Instance.home)
                        //{
                        //    StartDecorEdit();
                        //}
                        //Drop item
                        //else if(input == InteractablePreset.InteractionKey.alternative && FirstPersonItemController.Instance.hoveredSlot != null)
                        //{
                        //    if(FirstPersonItemController.Instance.hoveredSlot.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic && FirstPersonItemController.Instance.hoveredSlot.interactableID > -1)
                        //    {
                        //        FirstPersonItemController.Instance.EmptySlot(FirstPersonItemController.Instance.hoveredSlot);
                        //    }
                        //}

                        //inputCooldown = 0.1f;
                    }
                    else
                    {
                        InteractionSetting setting = currentInteractions[input];

                        //Prioritize active interactions
                        if (setting.currentAction != null && setting.currentSetting.enabled)
                        {
                            if(setting.newUIRef != null) setting.newUIRef.Execute();

                            if (setting.isFPSItem)
                            {
                                FirstPersonItemController.Instance.OnInteraction(input);
                            }
                            else setting.interactable.OnInteraction(input, Player.Instance);

                            //usedControlAction = true;

                            inputCooldown = 0.1f;
                        }
                    }
                }
            }

            if (lockedInInteraction != null) //Else in locked in interaction
            {
                //Interaction action
                if (activeInteractionAction)
                {
                    if (activeInteractionActionLookCheck)
                    {
                        if (interactionActionAmount < interactionActionThreshold)
                        {
                            //Actual progress
                            float amountThisFrame = Time.deltaTime * interactionActionMultiplier * Mathf.LerpUnclamped(GameplayControls.Instance.lockpickSpeedRange.x, GameplayControls.Instance.lockpickSpeedRange.y, UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.lockpickingSpeedModifier)) ;
                            interactionActionAmount += amountThisFrame;
                            interactionActionAmount = Mathf.Min(interactionActionAmount, interactionActionThreshold);

                            //Discover things over time...
                            if (discoveryOverTime.Count > 0)
                            {
                                List<Interactable> toRemoveInteractables = new List<Interactable>();

                                foreach (KeyValuePair<Interactable, float> pair in discoveryOverTime)
                                {
                                    if (interactionActionAmount >= pair.Value)
                                    {
                                        //Trigger find...
                                        ActionController.Instance.Inspect(pair.Key, Player.Instance.currentNode, Player.Instance);
                                        toRemoveInteractables.Add(pair.Key);
                                    }
                                }

                                foreach (Interactable i in toRemoveInteractables)
                                {
                                    discoveryOverTime.Remove(i);
                                }
                            }

                            //Discover things over time...
                            if (discoveryOverTimeDiscovery.Count > 0)
                            {
                                List<EvidenceMultiPage.MultiPageContent> toRemoveFacts = new List<EvidenceMultiPage.MultiPageContent>();

                                foreach (KeyValuePair<EvidenceMultiPage.MultiPageContent, float> pair in discoveryOverTimeDiscovery)
                                {
                                    if (interactionActionAmount >= pair.Value)
                                    {
                                        //Trigger find...
                                        Evidence getEv = null;

                                        if(GameplayController.Instance.evidenceDictionary.TryGetValue(pair.Key.discEvID, out getEv))
                                        {
                                            getEv.AddDiscovery(pair.Key.disc);
                                        }
                                        else
                                        {
                                            Game.LogError("Unable to find evidence " + pair.Key.discEvID);
                                        }

                                        toRemoveFacts.Add(pair.Key);
                                    }
                                }

                                foreach (EvidenceMultiPage.MultiPageContent i in toRemoveFacts)
                                {
                                    discoveryOverTimeDiscovery.Remove(i);
                                }
                            }

                            //Discover things over time...
                            //Find evidence...
                            List<Evidence> toRemoveEvidence = new List<Evidence>();

                            foreach (KeyValuePair<Evidence, float> pair in discoveryOverTimeEvidence)
                            {
                                if (interactionActionAmount >= pair.Value)
                                {
                                    //Inspect
                                    SessionData.Instance.PauseGame(true);

                                    Game.Log("Player: Find evidence " + pair.Key.preset.name);

                                    //Remove hygeine
                                    if (pair.Key.interactablePreset != null && pair.Key.interactablePreset.retailItem != null && (pair.Key.interactablePreset.retailItem.nourishment != 0f || pair.Key.interactablePreset.retailItem.hydration != 0f))
                                    {
                                        Player.Instance.AddHygiene(Toolbox.Instance.Rand(-0.15f, -0.25f));
                                    }

                                    InterfaceController.Instance.SpawnWindow(pair.Key, worldInteraction: true, autoPosition: false, forcePosition: new Vector2(0.5f, 0.5f), passedEvidenceKey: Evidence.DataKey.photo);

                                    //Trigger find...
                                    toRemoveEvidence.Add(pair.Key);
                                }
                            }

                            foreach (Evidence i in toRemoveEvidence)
                            {
                                discoveryOverTimeEvidence.Remove(i);
                            }

                            //Find meta...
                            List<MetaObject> metaRemove = new List<MetaObject>();

                            foreach (KeyValuePair<MetaObject, float> pair in discoveryOverTimeMeta)
                            {
                                if (interactionActionAmount >= pair.Value)
                                {
                                    Evidence ev = pair.Key.GetEvidence(true, Player.Instance.currentNodeCoord);

                                    //Inspect
                                    if(ev != null)
                                    {
                                        SessionData.Instance.PauseGame(true);

                                        //Remove hygeine
                                        InteractablePreset metaPreset = pair.Key.GetPreset();

                                        if (metaPreset != null && metaPreset.retailItem != null && (metaPreset.retailItem.nourishment != 0f || metaPreset.retailItem.hydration != 0f))
                                        {
                                            Player.Instance.AddHygiene(Toolbox.Instance.Rand(-0.15f, -0.25f));
                                        }

                                        Game.Log("Player: Find evidence " + pair.Key.preset);

                                        InterfaceController.Instance.SpawnWindow(ev, worldInteraction: true, autoPosition: false, forcePosition: new Vector2(0.5f, 0.5f), passedEvidenceKey: Evidence.DataKey.photo);
                                    }
                                    else
                                    {
                                        Game.Log("Player: Unable to get evidence for meta " + pair.Key.preset);
                                    }

                                    //Trigger find...
                                    metaRemove.Add(pair.Key);
                                }
                            }

                            foreach (MetaObject i in metaRemove)
                            {
                                discoveryOverTimeMeta.Remove(i);
                            }

                            //Set bars
                            if(!Player.Instance.playerKOInProgress)
                            {
                                float runningTotal = 0f;

                                for (int i = 0; i < spawnedProgressControllers.Count; i++)
                                {
                                    LockpickProgressController lpc = spawnedProgressControllers[i];
                                    lpc.SetAmount(interactionActionAmount - runningTotal);
                                    runningTotal += lpc.barMax;
                                }
                            }

                            //Fire event
                            Player.Instance.OnInteractionActionProgress(amountThisFrame, interactionActionAmount);

                            if (OnInteractionActionProgressChange != null)
                            {
                                OnInteractionActionProgressChange(amountThisFrame, interactionActionAmount);
                            }
                        }
                        //Complete action
                        else
                        {
                            CompleteInteractionAction();
                        }
                    }
                }
            }
            //Time out illegal action
            else if(Player.Instance.illegalActionTimer > 0f)
            {
                Player.Instance.illegalActionTimer -= Time.deltaTime;

                if(Player.Instance.illegalActionTimer <= 0f)
                {
                    SetIllegalActionActive(false);
                }
            }

            if (dialogMode || (!dialogMode && dialogTransition > 0f))
            {
                if (dialogMode && dialogTransition < 1f)
                {
                    dialogTransition += 2f * Time.deltaTime;
                }
                else if (!dialogMode && dialogTransition > 0f)
                {
                    dialogTransition -= 3f * Time.deltaTime;
                }

                dialogTransition = Mathf.Clamp01(dialogTransition);
                PrefabControls.Instance.dialogRect.anchoredPosition = new Vector2(Mathf.Lerp(-1600f, 0f, dialogTransition), 116f);

                //Select
                for (int i = 0; i < dialogOptions.Count; i++)
                {
                    DialogButtonController dbc = dialogOptions[i];

                    if (i != dialogSelection)
                    {
                        dbc.SetForceAdditionalHighlight(false);
                    }
                    else
                    {
                        dbc.SetForceAdditionalHighlight(true);
                    }
                }
            }

            //Animate cursor
            if (displayingInteraction)
            {
                if (displayProgress < 1f)
                {
                    displayProgress += 2.5f * Time.deltaTime;
                    displayProgress = Mathf.Clamp01(displayProgress);

                    for (int i = 0; i < InterfaceControls.Instance.interactionFadeInImages.Count; i++)
                    {
                        InterfaceControls.Instance.interactionFadeInImages[i].canvasRenderer.SetAlpha(displayProgress);
                    }

                    InterfaceControls.Instance.interactionText.alpha = displayProgress;
                }

                Vector2 min = Vector2.zero;
                Vector2 max = Vector2.zero;

                //Get screen box of the current ineractable
                currentLookingAtInteractable.GetScreenBox(out min, out max);

                //Position the cursor to min
                InterfaceControls.Instance.interactionRect.localPosition = Vector2.Lerp(min, max, 0.5f);
                //interactionRect.localPosition = min;

                //Desired scale
                Vector2 intSize = (max - min) * 0.9f;

                //Clamp to size restrictions
                intSize = Vector2.Max(intSize, InterfaceControls.Instance.interactionCursorMin);
                intSize = Vector2.Min(intSize, InterfaceControls.Instance.interactionCursorMax);

                //Set the size of the interaction rect to max - min
                InterfaceControls.Instance.interactionRect.sizeDelta = intSize;

                //Animate in/out
                if (InteractionController.Instance.inOut && InteractionController.Instance.inOutProgress < 1f)
                {
                    InteractionController.Instance.inOutProgress += (InterfaceControls.Instance.interactionCursorSpeed * InteractionController.Instance.interactionAnimationModifier) * Time.deltaTime;
                }
                else if (!InteractionController.Instance.inOut && InteractionController.Instance.inOutProgress > 0f)
                {
                    InteractionController.Instance.inOutProgress -= (InterfaceControls.Instance.interactionCursorSpeed * InteractionController.Instance.interactionAnimationModifier) * Time.deltaTime;
                }

                InteractionController.Instance.inOutProgress = Mathf.Clamp01(InteractionController.Instance.inOutProgress);

                if (InteractionController.Instance.inOutProgress >= 1f && InteractionController.Instance.inOut) InteractionController.Instance.inOut = false;
                else if (InteractionController.Instance.inOutProgress <= 0f && !InteractionController.Instance.inOut) InteractionController.Instance.inOut = true;

                float animationAmount = Mathf.Lerp(0f, 16f, InteractionController.Instance.inOutProgress);
                InterfaceControls.Instance.interactionULRect.anchoredPosition = new Vector2(animationAmount, -animationAmount);
                InterfaceControls.Instance.interactionURRect.anchoredPosition = new Vector2(-animationAmount, -32 - animationAmount);
                InterfaceControls.Instance.interactionBLRect.anchoredPosition = new Vector2(32 + animationAmount, animationAmount);
                InterfaceControls.Instance.interactionBRRect.anchoredPosition = new Vector2(-32 - animationAmount, 32 + animationAmount);

                //If the text desired can fit inside the bounding area, position it inside
                if (InterfaceControls.Instance.interactionTextContainer.sizeDelta.x < intSize.x - 32f)
                {
                    InterfaceControls.Instance.interactionTextContainer.anchoredPosition = new Vector2(0, -24);
                }
                else
                {
                    InterfaceControls.Instance.interactionTextContainer.anchoredPosition = new Vector2(0, -intSize.y - 5f);
                }

                //Fit reading text box outside
                if (InterfaceControls.Instance.readingTextContainer != null)
                {
                    InterfaceControls.Instance.readingTextContainer.anchoredPosition = new Vector2(0, -intSize.y - 46f);
                }
            }
            else if(displayProgress > 0)
            {
                displayProgress -= 3f * Time.deltaTime;
                displayProgress = Mathf.Clamp01(displayProgress);
            }
        }

        if (inputCooldown > 0f)
        {
            inputCooldown -= Time.deltaTime;
            inputCooldown = Mathf.Max(0, inputCooldown);
        }
    }

    public void StartDecorEdit()
    {
        Game.Log("Decor: Starting decor edit...");

        CasePanelController.Instance.SelectNoCaseButton(); //Select no case so we have a bank screen...

        SessionData.Instance.isDecorEdit = true;
        SessionData.Instance.PauseGame(true, openDesktopMode: false);

        InterfaceController.Instance.SetDesktopMode(true, false);
        InterfaceController.Instance.UpdateDOF();

        //Open a window if one doesn't exist
        if(!InterfaceController.Instance.activeWindows.Exists(item => item.preset.name == "ApartmentDecor"))
        {
            InterfaceController.Instance.SpawnWindow(null, Evidence.DataKey.name, presetName: "ApartmentDecor", worldInteraction: false);
        }

        BioScreenController.Instance.SetInventoryOpen(false, true, resumeGame: false);
    }

    //Sets the currently available interaction option for a specific interaction button
    public void SetCurrentPlayerInteraction(InteractablePreset.InteractionKey key, Interactable newInteractable, Interactable.InteractableCurrentAction newCurrentAction, bool fpsItem = false, int forcePriority = -1)
    {
        //Set action
        InteractionSetting thisSetting = null;

        if (currentInteractions.TryGetValue(key, out thisSetting))
        {
            thisSetting.interactable = newInteractable;
            thisSetting.currentSetting = newCurrentAction;

            if (newCurrentAction != null)
            {
                thisSetting.currentAction = newCurrentAction.currentAction;
                if(thisSetting.currentAction != null && thisSetting.currentAction.action != null) thisSetting.priority = thisSetting.currentAction.action.inputPriority;
                if (forcePriority >= 0) thisSetting.priority = forcePriority;

                thisSetting.isFPSItem = fpsItem;

                string strikedStart = string.Empty;
                string strikedEnd = string.Empty;

                string colourStart = "<color=#" + InterfaceControls.Instance.interactionControlTextNormalHex + ">";
                string colourEnd = string.Empty;

                bool illegal = false;
                string interactionName = string.Empty;

                if(newCurrentAction.currentAction != null)
                {
                    if (newCurrentAction.currentAction.action.debug)
                    {
                        try
                        {
                            Game.Log("Debug: Adding action " + newCurrentAction.currentAction.action.name + " to interactable " + newInteractable.name + " for key " + key.ToString());
                        }
                        catch
                        {

                        }
                    }

                    if (thisSetting.isFPSItem)
                    {
                        FirstPersonItem.FPSInteractionAction fpsAction = thisSetting.currentAction as FirstPersonItem.FPSInteractionAction;

                        if(fpsAction.actionIsIllegal) illegal = GetValidPlayerActionIllegal(newInteractable, Player.Instance.currentNode);
                        interactionName = fpsAction.interactionName;

                        if (fpsAction.mainUseSpecialColour)
                        {
                            colourStart = "<color=#" + ColorUtility.ToHtmlStringRGBA(fpsAction.mainSpecialColour) + ">";
                        }
                    }
                    else
                    {
                        if (newCurrentAction.currentAction.actionIsIllegal)
                        {
                            if (newInteractable != null)
                            {
                                NewNode node = newInteractable.node;
                                if (node == null) node = Player.Instance.currentNode;
                                illegal = GetValidPlayerActionIllegal(newInteractable, node);
                            }
                            else illegal = true;
                        }

                        interactionName = newCurrentAction.currentAction.interactionName;
                    }

                    if(thisSetting.currentAction.specialCase == InteractablePreset.InteractionAction.SpecialCase.takeSwap)
                    {
                        if(!FirstPersonItemController.Instance.IsSlotAvailable())
                        {
                            if (BioScreenController.Instance.selectedSlot != null && BioScreenController.Instance.selectedSlot.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic)
                            {
                                interactionName = "Swap";
                            }
                        }
                    }
                    else if (thisSetting.currentAction.specialCase == InteractablePreset.InteractionAction.SpecialCase.citizenReturn)
                    {
                        if (talkingTo == newInteractable)
                        { 
                            interactionName = "End Conversation";
                        }
                    }


                    //Is this disabled (+ display means display this as striked out)
                    if (!newCurrentAction.enabled)
                    {
                        strikedStart = "<s>";
                        strikedEnd = "</s>";
                    }
                }

                if(newCurrentAction.overrideInteractionName != null && newCurrentAction.overrideInteractionName.Length > 0)
                {
                    interactionName = newCurrentAction.overrideInteractionName;
                }

                if (illegal) colourStart = "<color=#" + InterfaceControls.Instance.interactionControlTextIllegalHex + ">";

                currentInteractions[key].actionText = colourStart + strikedStart + Strings.Get("ui.interaction", interactionName) + strikedEnd + colourEnd;

                //Get audio event
                if(newInteractable != null && newCurrentAction != null && newCurrentAction.currentAction != null)
                {
                    if (newInteractable.actionAudioEventOverrides.TryGetValue(newCurrentAction.currentAction.action, out currentInteractions[key].audioEvent))
                    {
                        //...
                    }
                    else currentInteractions[key].audioEvent = newCurrentAction.currentAction.soundEvent;
                }

                //Set audio level indicator
                if (currentInteractions[key].newUIRef != null && currentInteractions[key].newUIRef.soundIndicator != null)
                {
                    currentInteractions[key].newUIRef.soundIndicator.SetSoundEvent(currentInteractions[key].audioEvent, true);
                }
            }
            else
            {
                thisSetting.currentAction = null;
                currentInteractions[key].audioEvent = null;

                if(currentInteractions[key].newUIRef != null)
                {
                    if (currentInteractions[key].newUIRef != null && currentInteractions[key].newUIRef.soundIndicator != null)
                    {
                        currentInteractions[key].newUIRef.soundIndicator.SetSoundEvent(null, true);
                    }

                    currentInteractions[key].actionText = string.Empty;
                    //Game.Log("Set " + currentInteractions[key].uiReference.key.ToString() + " inactive");
                }
            }
        }
        else
        {
            Game.LogError("Unable to get key " + key + " in current interactions...");
        }
    }

    //Display interaction cursor
    public void DisplayInteractionCursor(bool val, bool forceUpdate = false)
    {
        if (displayingInteraction != val || forceUpdate)
        {
            //Only if not changed
            if(displayingInteraction && displayingInteraction != val)
            {
                //Reset cursor animation
                inOut = false;
                inOutProgress = 0f;
                displayProgress = 0f;
            }

            displayingInteraction = val;

            if (displayingInteraction)
            {
                //Trigger on looked at
                currentLookingAtReadingRange = currentLookingAtInteractable;

                if (currentLookingAtInteractable != null)
                {
                    if (currentLookingAtInteractable.isDoor != null)
                    {
                        currentLookingAtInteractable.isDoor.UpdateNameBasedOnPlayerPosition();
                    }

                    currentLookingAtInteractable.interactable.UpdateCurrentActions(); //Update current actions
                }

                //Set interaction text
                UpdateInteractionText();

                //Set the text container to the desired size
                InterfaceControls.Instance.interactionRect.gameObject.SetActive(true);

                if (InterfaceControls.Instance.seenIcon.gameObject.activeSelf) InterfaceControls.Instance.seenIcon.gameObject.SetActive(false);

                //If player looking at door
                if (currentLookingAtInteractable.isDoor != null)
                {
                    //Trigger tutorial
                    if (SessionData.Instance.enableTutorialText)
                    {
                        if (ChapterController.Instance == null || ChapterController.Instance.currentPart >= 22)
                        {
                            SessionData.Instance.TutorialTrigger("doors");
                        }

                        //SessionData.Instance.TutorialTrigger("barging");
                    }

                    //Have a key for this door
                    if (Player.Instance.keyring.Contains(currentLookingAtInteractable.isDoor) || Player.Instance.playerKeyringInt.Contains(currentLookingAtInteractable.interactable))
                    {
                        InterfaceControls.Instance.haveKeyIcon.gameObject.SetActive(true);
                    }
                    else
                    {
                        InterfaceControls.Instance.haveKeyIcon.gameObject.SetActive(false);
                    }

                    //This door is locked
                    if (currentLookingAtInteractable.isDoor.knowLockStatus)
                    {
                        //Switch icon to lock/unlocked
                        if(currentLookingAtInteractable.isDoor.isLocked)
                        {
                            InterfaceControls.Instance.lockedImg.sprite = InterfaceControls.Instance.lockedSprite;
                        }
                        else
                        {
                            InterfaceControls.Instance.lockedImg.sprite = InterfaceControls.Instance.unlockedSprite;
                        }

                        InterfaceControls.Instance.lockedIcon.gameObject.SetActive(true);

                        //Update lock & door strength
                        InterfaceControls.Instance.lockStrengthText.text = string.Empty;

                        //Diplay lock strength
                        if (currentLookingAtInteractable.isDoor.preset.lockType == DoorPreset.LockType.key)
                        {
                            //Lockpicks needed for this...
                            int picksNeeded = Toolbox.Instance.GetLockpicksNeeded(currentLookingAtInteractable.isDoor.wall.currentLockStrength);
                            InterfaceControls.Instance.lockStrengthText.text = Strings.Get("ui.interface", "Lock") + ": " + Mathf.CeilToInt(currentLookingAtInteractable.isDoor.wall.currentLockStrength * 100f) + "% (" + picksNeeded + " " + Strings.Get("ui.interface", "picks") + ")\n";
                        }

                        //Add strength
                        InterfaceControls.Instance.lockStrengthText.text += Strings.Get("ui.interface", "Strength") + ": " + Mathf.CeilToInt(currentLookingAtInteractable.isDoor.wall.currentDoorStrength * 100f) + "%";
                    }
                    else
                    {
                        InterfaceControls.Instance.lockedIcon.gameObject.SetActive(false);
                    }
                }
                //If player looking at padlock
                else if(currentLookingAtInteractable.interactable.preset.specialCaseFlag == InteractablePreset.SpecialCase.padlock)
                {
                    //Have a key for this door
                    //if (Player.Instance.playerKeyring.Contains(currentLookingAtInteractable.isDoor))
                    //{
                    //    InterfaceControls.Instance.haveKeyIcon.gameObject.SetActive(true);
                    //}
                    //else
                    //{
                    //    InterfaceControls.Instance.haveKeyIcon.gameObject.SetActive(false);
                    
                    if (currentLookingAtInteractable.interactable.locked)
                    {
                        InterfaceControls.Instance.lockedImg.sprite = InterfaceControls.Instance.lockedSprite;
                    }
                    else
                    {
                        InterfaceControls.Instance.lockedImg.sprite = InterfaceControls.Instance.unlockedSprite;
                    }

                    InterfaceControls.Instance.lockedIcon.gameObject.SetActive(true);

                    //Update lock & door strength
                    InterfaceControls.Instance.lockStrengthText.text = string.Empty;

                    //Diplay lock strength
                    //Lockpicks needed for this...
                    int picksNeeded = Toolbox.Instance.GetLockpicksNeeded(currentLookingAtInteractable.interactable.val);
                    InterfaceControls.Instance.lockStrengthText.text = Strings.Get("ui.interface", "Lock") + ": " + Mathf.CeilToInt(currentLookingAtInteractable.interactable.val * 100f) + "% (" + picksNeeded + " " + Strings.Get("ui.interface", "picks") + ")\n";

                    //Add strength
                    //InterfaceControls.Instance.lockStrengthText.text += Strings.Get("ui.interface", "Strength") + ": " + Mathf.CeilToInt(currentLookingAtInteractable.isDoor.wall.currentDoorStrength * 100f) + "%";
                }
                else
                {
                    InterfaceControls.Instance.haveKeyIcon.gameObject.SetActive(false);
                    InterfaceControls.Instance.lockedIcon.gameObject.SetActive(false);

                    ////If player looking at citizen
                    //if (currentLookingAtReadingRange.isActor != null)
                    //{
                    //    currentLookingAtCitizen = currentLookingAtReadingRange.isActor as Citizen;
                    //    //if (!currentVisibleTargets.Contains(currentLookingAtCitizen)) currentVisibleTargets.Add(currentLookingAtCitizen);

                    //    ////Force visual acquire
                    //    //if (!GameplayController.Instance.knownWhereabouts.Contains(currentLookingAtCitizen))
                    //    //{
                    //    //    //Game.Log("Add Whereabouts for " + currentLookingAtCitizen.name);
                    //    //    GameplayController.Instance.knownWhereabouts.Add(currentLookingAtCitizen);
                    //    //    //currentLookingAtCitizen.evidenceEntry.MergeDataKeys(Evidence.DataKey.location, Evidence.DataKey.photo); //Merge location to photo
                    //    //}
                    //}
                    //else currentLookingAtCitizen = null;
                }

                //Trigger tutorial
                if(SessionData.Instance.enableTutorialText)
                {
                    if(ChapterController.Instance == null || ChapterController.Instance.currentPart > 1)
                    {
                        SessionData.Instance.TutorialTrigger("interaction");
                    }

                    //if(currentLookingAtInteractable.interactable.evidence != null)
                    //{
                    //    if (currentLookingAtInteractable.interactable.preset.GetActions().Exists(item => item.interactionName == "Inspect"))
                    //    {
                    //        SessionData.Instance.TutorialTrigger("evidence");
                    //    }

                    //    if (currentLookingAtInteractable.interactable.preset.name == "CityDirectory")
                    //    {
                    //        SessionData.Instance.TutorialTrigger("citydirectory");
                    //    }
                    //}
                }

                AlignInteractionIcons();
            }
            else
            {
                InterfaceControls.Instance.interactionRect.gameObject.SetActive(false);

                UpdateInteractionText();

                InterfaceControls.Instance.haveKeyIcon.gameObject.SetActive(false);
                InterfaceControls.Instance.seenIcon.gameObject.SetActive(false);
                SetDistanceRecognitionMode(false);
                SetReadingMode(false, false);
                currentLookingAtReadingRange = null;
                //currentLookingAtCitizen = null;
            }
        }
    }

    //Aligns the displayed interaction icons
    public void AlignInteractionIcons()
    {
        //Line up secondary icons
        float pos = 38f;

        if (InterfaceControls.Instance.haveKeyIcon.gameObject.activeSelf)
        {
            InterfaceControls.Instance.haveKeyIcon.anchoredPosition = new Vector2(pos, 0);
            pos += 34f;
        }

        if (InterfaceControls.Instance.lockedIcon.gameObject.activeSelf)
        {
            InterfaceControls.Instance.lockedIcon.anchoredPosition = new Vector2(pos, 0);
            pos += 34f;
        }

        //if (InterfaceControls.Instance.forbiddenIcon.gameObject.activeSelf)
        //{
        //    InterfaceControls.Instance.forbiddenIcon.anchoredPosition = new Vector2(pos, 0);
        //    pos += 34f;
        //}

        if (InterfaceControls.Instance.seenIcon.gameObject.activeSelf)
        {
            InterfaceControls.Instance.seenIcon.anchoredPosition = new Vector2(pos, 0);
            pos += 34f;
        }
    }

    //Sets the distance recognition mode
    public void SetDistanceRecognitionMode(bool val)
    {
        distanceRecognitionMode = val;

        Color readingModeColour = new Color(0.5f, 0.5f, 0.5f, 0.66f);

        if (distanceRecognitionMode)
        {
            for (int i = 0; i < InterfaceControls.Instance.interactionBoundImages.Count; i++)
            {
                InterfaceControls.Instance.interactionBoundImages[i].color = readingModeColour;
            }

            InterfaceControls.Instance.interactionText.color = InterfaceControls.Instance.interactionTextDistanceColour;
            interactionAnimationModifier = 0.5f;
        }
        else
        {
            for (int i = 0; i < InterfaceControls.Instance.interactionBoundImages.Count; i++)
            {
                InterfaceControls.Instance.interactionBoundImages[i].color = InterfaceControls.Instance.interactionTextColour;
            }

            InterfaceControls.Instance.interactionText.color = InterfaceControls.Instance.interactionTextColour;
            interactionAnimationModifier = 1f;
        }
    }

    //Enables/disabled reading mode
    public void SetReadingMode(bool val, bool stopImmediately)
    {
        if (readingMode != val)
        {
            readingMode = val;
            Game.Log("Interface: Set reading mode: " + readingMode);

            UpdateReadingModeText();

            if (readingMode)
            {
                //Discover evidence
                if (currentLookingAtInteractable.interactable.preset.discoverOnRead)
                {
                    if (currentLookingAtInteractable.interactable.evidence != null)
                    {
                        currentLookingAtInteractable.interactable.evidence.SetFound(true);
                    }
                }

                if (readingModeCoroutine != null)
                {
                    StopCoroutine(readingModeCoroutine);
                }

                readingModeCoroutine = StartCoroutine(ReadingMode());
            }
            else
            {
                if(stopImmediately)
                {
                    if (readingModeCoroutine != null)
                    {
                        StopCoroutine(readingModeCoroutine);
                    }

                    if(InterfaceControls.Instance.readingTextContainer != null) InterfaceControls.Instance.readingTextContainer.gameObject.SetActive(false);
                    InterfaceControls.Instance.readingText.text = string.Empty;
                    readingModeTransition = 0f;
                }
            }
        }
    }

    public void UpdateReadingModeText()
    {
        if(!readingMode) InterfaceControls.Instance.readingText.text = string.Empty;
        //Game.Log("Interface: Update reading text");

        if(currentLookingAtInteractable != null && currentLookingAtInteractable.interactable != null && currentLookingAtInteractable.interactable.preset.readingEnabled)
        {
            //Set text
            if (currentLookingAtInteractable.interactable.preset.readingSource == InteractablePreset.ReadingModeSource.evidenceNote)
            {
                if (currentLookingAtInteractable.interactable.evidence != null)
                {
                    InterfaceControls.Instance.readingText.text = currentLookingAtInteractable.interactable.evidence.GetNoteComposed((new Evidence.DataKey[] { Evidence.DataKey.name }).ToList(), false);
                }
                else InterfaceControls.Instance.readingText.text = string.Empty;
            }
            else if (currentLookingAtInteractable.interactable.preset.readingSource == InteractablePreset.ReadingModeSource.mainEvidenceText)
            {
                if (currentLookingAtInteractable.interactable != null)
                {
                    InterfaceControls.Instance.readingText.text = Strings.GetMainTextFromInteractable(currentLookingAtInteractable.interactable, Strings.LinkSetting.forceNoLinks);
                }
                else InterfaceControls.Instance.readingText.text = string.Empty;
            }
            else if (currentLookingAtInteractable.interactable.preset.readingSource == InteractablePreset.ReadingModeSource.multipageEvidence)
            {
               // InterfaceControls.Instance.readingText.text = string.Empty;
            }
            else if (currentLookingAtInteractable.interactable.preset.readingSource == InteractablePreset.ReadingModeSource.time)
            {
                InterfaceControls.Instance.readingText.text = SessionData.Instance.GameTimeToClock12String(SessionData.Instance.gameTime, false);
            }
            else if (currentLookingAtInteractable.interactable.preset.readingSource == InteractablePreset.ReadingModeSource.bookPreset)
            {
                if (currentLookingAtInteractable.interactable.book == null)
                {
                    Game.Log("Unable to find book preset for " + currentLookingAtInteractable.interactable.name + ": Book serialized reference: " + currentLookingAtInteractable.interactable.bo);
                }
                else
                {
                    if (currentLookingAtInteractable.interactable.sw0)
                    {
                        DDSSaveClasses.DDSMessageSave bookMsg = null;

                        if(Toolbox.Instance.allDDSMessages.TryGetValue(currentLookingAtInteractable.interactable.book.ddsMessage, out bookMsg))
                        {
                            DDSSaveClasses.DDSBlockCondition titleBlock = bookMsg.blocks.Find(item => item.alwaysDisplay);
                            DDSSaveClasses.DDSBlockCondition subBlock = bookMsg.blocks.Find(item => item.group == 1);
                            DDSSaveClasses.DDSBlockCondition blurbBlock = bookMsg.blocks.Find(item => item.group == 2);

                            //Compile book text...
                            string str = "<b>" + Strings.Get("dds.blocks", titleBlock.blockID, forceNoWrite: true) + " — " + currentLookingAtInteractable.interactable.book.author + "</b>";

                            //Get series text...
                            if(subBlock != null)
                            {
                                str += "\n" + "<i>" + Strings.Get("dds.blocks", subBlock.blockID, forceNoWrite: true) + "</i>";
                            }

                            if (currentLookingAtInteractable.interactable.book.isSeries)
                            {
                                //Volume features
                                List<BookPreset> series = Toolbox.Instance.allBooks.FindAll(item => item.seriesTag == currentLookingAtInteractable.interactable.book.seriesTag && item.isSeries);

                                //Get volume text
                                str += " (" + currentLookingAtInteractable.interactable.book.seriesNumber + "/" + series.Count + ")" + "\n\n";
                            }
                            else
                            {
                                str += "\n\n";
                            }

                            string blurb = string.Empty;

                            if (blurbBlock != null)
                            {
                                blurb = Strings.Get("dds.blocks", blurbBlock.blockID, forceNoWrite: true);
                            }

                            InterfaceControls.Instance.readingText.SetText(str + blurb);

                            int moneyForSeries = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.readingSeriesBonus));

                            if (moneyForSeries > 0)
                            {
                                if (!GameplayController.Instance.booksRead.Contains(currentLookingAtInteractable.interactable.book.name))
                                {
                                    //Is this a series?
                                    if(currentLookingAtInteractable.interactable.book.isSeries)
                                    {
                                        List<BookPreset> series = Toolbox.Instance.allBooks.FindAll(item => item.seriesTag == currentLookingAtInteractable.interactable.book.seriesTag && item.isSeries);

                                        bool seriesPass = true;

                                        foreach(BookPreset b in series)
                                        {
                                            if(!GameplayController.Instance.booksRead.Contains(b.name) && b != currentLookingAtInteractable.interactable.book)
                                            {
                                                seriesPass = false;
                                                break;
                                            }
                                        }

                                        if(seriesPass) GameplayController.Instance.AddMoney(moneyForSeries, true, "readingformoney");
                                    }
                                }
                            }

                            int moneyForReading = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.readingMoney));

                            if (moneyForReading > 0)
                            {
                                if (!GameplayController.Instance.booksRead.Contains(currentLookingAtInteractable.interactable.book.name))
                                {
                                    GameplayController.Instance.AddMoney(moneyForReading, true, "readingformoney");
                                    GameplayController.Instance.booksRead.Add(currentLookingAtInteractable.interactable.book.name);
                                }
                            }
                        }
                        else
                        {
                            Game.Log("Cannot find DDS message for book " + currentLookingAtInteractable.interactable.book.name + ": " + currentLookingAtInteractable.interactable.book.ddsMessage);
                        }

                    }
                    else InterfaceControls.Instance.readingText.text = string.Empty;
                }
            }
            else if (currentLookingAtInteractable.interactable.preset.readingSource == InteractablePreset.ReadingModeSource.syncDiskPreset)
            {
                //Compile book text...
                string options = string.Empty;

                if (currentLookingAtInteractable.interactable.syncDisk.mainEffect1 != SyncDiskPreset.Effect.none)
                {
                    options += Strings.ComposeText(Strings.Get("evidence.syncdisks", currentLookingAtInteractable.interactable.syncDisk.mainEffect1Description), currentLookingAtInteractable.interactable.syncDisk, additionalObject: new int[] { 0, 0 });
                }

                if (currentLookingAtInteractable.interactable.syncDisk.mainEffect2 != SyncDiskPreset.Effect.none)
                {
                    options += "\n" + Strings.Get("evidence.syncdisks", "OR") + "\n" + Strings.ComposeText(Strings.Get("evidence.syncdisks", currentLookingAtInteractable.interactable.syncDisk.mainEffect2Description), currentLookingAtInteractable.interactable.syncDisk, additionalObject: new int[] { 1, 0 });
                }

                if (currentLookingAtInteractable.interactable.syncDisk.mainEffect3 != SyncDiskPreset.Effect.none)
                {
                    options += "\n" + Strings.Get("evidence.syncdisks", "OR") + "\n" + Strings.ComposeText(Strings.Get("evidence.syncdisks", currentLookingAtInteractable.interactable.syncDisk.mainEffect3Description), currentLookingAtInteractable.interactable.syncDisk, additionalObject: new int[] { 2, 0 });
                }

                string str = "<b>#" + currentLookingAtInteractable.interactable.syncDisk.syncDiskNumber + " " + Strings.Get("evidence.syncdisks", currentLookingAtInteractable.interactable.syncDisk.name) + "</b>\n\n" + options;

                InterfaceControls.Instance.readingText.SetText(str);
            }
            else if (currentLookingAtInteractable.interactable.preset.readingSource == InteractablePreset.ReadingModeSource.kaizenSkillDisplay)
            {
                if(currentLookingAtInteractable.interactable.isActor != null)
                {
                    string str = string.Empty;
                    Human hu = currentLookingAtInteractable.interactable.isActor as Human;

                    if(hu != null)
                    {
                        if (UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.singlePerception) > 0f)
                        {
                            if(hu.partner == null)
                            {
                                str += Strings.Get("ui.interface", "Single");
                            }
                        }

                        if(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.wealthPerception) > 0f)
                        {
                            if(hu.inventory.Exists(item => item.preset.name == "Wallet" || item.preset.name == "Purse"))
                            {
                                int money = 0;

                                foreach(Human.WalletItem i in hu.walletItems)
                                {
                                    if(i.itemType == Human.WalletItemType.money)
                                    {
                                        money += i.money;
                                    }
                                }

                                if (str.Length > 0) str += "\n";
                                str += CityControls.Instance.cityCurrency + money;
                            }
                        }

                        InterfaceControls.Instance.readingText.SetText(str);
                    }
                }
            }
        }

        //Game.Log("Update reading text: " + InterfaceControls.Instance.readingText.text);
    }

    //Coroutine that controls reading mode display
    IEnumerator ReadingMode()
    {
        InterfaceControls.Instance.readingTextContainer.gameObject.SetActive(true);
        int displayPage = -1;

        InterfaceControls.Instance.readingContainerRend.SetAlpha(readingModeTransition);
        InterfaceControls.Instance.readingTextRend.SetAlpha(readingModeTransition);

        while (readingMode || (!readingMode && readingModeTransition > 0f))
        {
            //Game.Log("Debug: Reading mode: " + readingMode + ", Transition: " + readingModeTransition);

            if (readingMode && readingModeTransition < 1f)
            {
                readingModeTransition += Time.deltaTime * 3f;
                readingModeTransition = Mathf.Clamp01(readingModeTransition);
            }
            else if (!readingMode && readingModeTransition > 0f)
            {
                readingModeTransition -= Time.deltaTime * 10f;
                readingModeTransition = Mathf.Clamp01(readingModeTransition);
            }

            //Set desired size
            Vector2 desiredSize = new Vector2(Mathf.Min(InterfaceControls.Instance.readingText.preferredWidth + 40f, InterfaceControls.Instance.readingBoxMaxSize.x), Mathf.Min(InterfaceControls.Instance.readingText.preferredHeight + 38f, InterfaceControls.Instance.readingBoxMaxSize.y));
            InterfaceControls.Instance.readingTextContainer.sizeDelta = desiredSize;

            InterfaceControls.Instance.readingContainerRend.SetAlpha(readingModeTransition);
            InterfaceControls.Instance.readingTextRend.SetAlpha(readingModeTransition);

            //Update content
            if (currentLookingAtInteractable != null)
            {
                if (currentLookingAtInteractable.interactable.preset.readingSource == InteractablePreset.ReadingModeSource.multipageEvidence)
                {
                    if (currentLookingAtInteractable.interactable.readingDelay > 0f)
                    {
                        InterfaceControls.Instance.readingText.text = string.Empty;
                    }
                    else
                    {
                        EvidenceMultiPage pageEv = currentLookingAtInteractable.interactable.evidence as EvidenceMultiPage;

                        if (pageEv != null)
                        {
                            if (displayPage != pageEv.page)
                            {
                                InterfaceControls.Instance.readingText.text = pageEv.GetCurrentPageStringContent();
                                displayPage = pageEv.page;

                                //Discovery
                                List<EvidenceMultiPage.MultiPageContent> content = pageEv.pageContent.FindAll(item => item.page == pageEv.page && item.discEvID != null && item.discEvID.Length > 0);

                                Game.Log("displaying page " + displayPage +": " + content.Count + " content. Reading text: " + pageEv.GetCurrentPageStringContent());

                                foreach (EvidenceMultiPage.MultiPageContent c in content)
                                {
                                    Evidence getEv = null;

                                    if(GameplayController.Instance.evidenceDictionary.TryGetValue(c.discEvID, out getEv))
                                    {
                                        getEv.AddDiscovery(c.disc);
                                    }
                                    else
                                    {
                                        Game.LogError("Unable to find evidence " + c.discEvID);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //If no text then override alpha
            if (InterfaceControls.Instance.readingText.text.Length <= 0)
            {
                InterfaceControls.Instance.readingContainerRend.SetAlpha(0f);
            }

            yield return null;
        }

        InterfaceControls.Instance.readingTextContainer.gameObject.SetActive(false);
        readingModeTransition = 0;
    }

    //Update the text display for interaction & sound levels
    public void UpdateInteractionText()
    {
        //Update First person interaction
        FirstPersonItemController.Instance.UpdateCurrentActions();

        //Loop all possible control keys to update the current available actions
        foreach(InteractablePreset.InteractionKey key in allInteractionKeys)
        {
            //If in case mode, display its controls
            if(InterfaceController.Instance.desktopMode)
            {
                if (Game.Instance.displayExtraControlHints && !CutSceneController.Instance.cutSceneActive)
                {
                    if(CasePanelController.Instance.currentSelectMode == CasePanelController.ControllerSelectMode.topBar)
                    {
                        if (key == InteractablePreset.InteractionKey.caseBoard)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Return", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else if (key == InteractablePreset.InteractionKey.nearestInteractable)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Cycle Interface", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else if(key == InteractablePreset.InteractionKey.ContentMoveAxisX && CasePanelController.Instance.mapScroll.controlEnabled)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Scroll Map", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else
                        {
                            SetCurrentPlayerInteraction(key, null, null); //Set this
                        }
                    }
                    else if(CasePanelController.Instance.currentSelectMode == CasePanelController.ControllerSelectMode.caseBoard)
                    {
                        if (key == InteractablePreset.InteractionKey.caseBoard)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Resume", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral, highlight = true };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else if (key == InteractablePreset.InteractionKey.nearestInteractable)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Cycle Interface", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else if (key == InteractablePreset.InteractionKey.ContentMoveAxisX)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Scroll Board", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else if (key == InteractablePreset.InteractionKey.MoveEvidenceAxisX)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Move Evidence", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else if (key == InteractablePreset.InteractionKey.CaseBoardZoomAxis)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Zoom", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else if (key == InteractablePreset.InteractionKey.secondary)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Context Menu", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else if (key == InteractablePreset.InteractionKey.CreateString && (CasePanelController.Instance.selectedPinned != null || InterfaceController.Instance.selectedPinned.Count > 0) && !CasePanelController.Instance.customLinkSelectionMode)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Create String", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else
                        {
                            SetCurrentPlayerInteraction(key, null, null); //Set this
                        }
                    }
                    else if(CasePanelController.Instance.currentSelectMode == CasePanelController.ControllerSelectMode.windows)
                    {
                        if (key == InteractablePreset.InteractionKey.caseBoard)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Return", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else if (key == InteractablePreset.InteractionKey.nearestInteractable)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Cycle Interface", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else if (key == InteractablePreset.InteractionKey.MoveEvidenceAxisX)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Move Window", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else if (InterfaceController.Instance.activeWindows.Count > 1 && key == InteractablePreset.InteractionKey.SelectLeft)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Prev. Window", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else if (InterfaceController.Instance.activeWindows.Count > 1 && key == InteractablePreset.InteractionKey.SelectRight)
                        {
                            Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Next Window", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                            SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                        }
                        else
                        {
                            SetCurrentPlayerInteraction(key, null, null); //Set this
                        }
                    }
                }
                else
                {
                    SetCurrentPlayerInteraction(key, null, null); //Set this
                }

                continue;
            }
            //If in weapon select, display its controls
            else if (BioScreenController.Instance.isOpen)
            {
                if (key == InteractablePreset.InteractionKey.primary)
                {
                    Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Select", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                    SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                }
                //else if (key == InteractablePreset.InteractionKey.alternative && BioScreenController.Instance.hoveredSlot != null && BioScreenController.Instance.hoveredSlot.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic && BioScreenController.Instance.hoveredSlot.interactableID > -1)
                //{
                //    Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = Strings.Get("ui.interaction", "Drop/Place"), forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                //    SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                //}
                //else if(key == InteractablePreset.InteractionKey.secondary && Player.Instance.currentGameLocation == Player.Instance.home)
                //{
                //    Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = Strings.Get("ui.interaction", "Change Decor"), forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                //    SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                //}
                else if (key == InteractablePreset.InteractionKey.WeaponSelect)
                {
                    Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Close", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                    SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                }
                else if (key == InteractablePreset.InteractionKey.secondary && (BioScreenController.Instance.hoveredSlot == null || BioScreenController.Instance.hoveredSlot == BioScreenController.Instance.selectedSlot))
                {
                    Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Close", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                    SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                }
                else if (key == InteractablePreset.InteractionKey.secondary && BioScreenController.Instance.hoveredSlot != null && BioScreenController.Instance.hoveredSlot != BioScreenController.Instance.selectedSlot)
                {
                    Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Select & Close", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.neutral };
                    SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
                }
                else
                {
                    SetCurrentPlayerInteraction(key, null, null); //Set this
                }

                continue;
            }
            //If in furniture placement mode, display its controls
            //else if(PlayerApartmentController.Instance.furniturePlacementMode)
            //{
            //    if (key == InteractablePreset.InteractionKey.LeanLeft)
            //    {
            //        Game.Log("Furn placement mode; " + key);

            //        Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Rotate Left", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.left };
            //        SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
            //    }
            //    else if (key == InteractablePreset.InteractionKey.LeanRight)
            //    {
            //        Game.Log("Furn placement mode; " + key);

            //        Interactable.InteractableCurrentAction newAction = new Interactable.InteractableCurrentAction { display = true, enabled = true, overrideInteractionName = "Rotate Right", forcePositioning = true, forcePosition = ControlDisplayController.ControlPositioning.right };
            //        SetCurrentPlayerInteraction(key, null, newAction, false, 11); //Set custom action
            //    }
            //}

            ControlsDisplayController.CustomActionsDisplayed customAction = ControlsDisplayController.Instance.customActionsDisplayed.Find(item => item.key == key && item.displayTime > 0f);

            if (customAction != null)
            {
                SetCurrentPlayerInteraction(key, null, customAction.action, false, 11); //Set custom action
                continue;
            }

            Interactable interactionFocus = null;

            //The first priority are objects we are looking at and are within range (displaying interaction will be true).
            //IF we are in a locked interaction, their active actions must be compatible...
            if (currentLookingAtInteractable != null && displayingInteraction)
            {
                interactionFocus = currentLookingAtInteractable.interactable;

                //Set the interaction text
                UpdateInteractionText(currentLookingAtInteractable.interactable.GetName());

                //This interactable has this key enabled for its action
                Interactable.InteractableCurrentAction currentAction = null;

                if(interactionFocus.currentActions.TryGetValue(key, out currentAction))
                {
                    //Display this action (+ set)
                    if(currentAction.currentAction != null && currentAction.display)
                    {
                        //Is there an FPS item with a higher priority?
                        if (FirstPersonItemController.Instance.drawnItem != null)
                        {
                            //This interactable has this key enabled for its action
                            Interactable.InteractableCurrentAction fpsAction = null;

                            if (FirstPersonItemController.Instance.currentActions.TryGetValue(key, out fpsAction))
                            {
                                //Display this action (+ set)
                                try
                                {
                                    if (fpsAction.currentAction != null && fpsAction.display && fpsAction.currentAction.action.inputPriority > currentAction.currentAction.action.inputPriority)
                                    {
                                        SetCurrentPlayerInteraction(key, interactionFocus, fpsAction, fpsItem: true); //Set this
                                        InterfaceControls.Instance.interactionText.color = InterfaceControls.Instance.interactionTextNormalColour;
                                        continue;
                                    }
                                }
                                catch
                                {

                                }
                            }
                        }

                        //What colour
                        Color setCol = InterfaceControls.Instance.interactionTextNormalColour;

                        if (interactionFocus.objectRef != null)
                        {
                            NewDoor doorRef = interactionFocus.objectRef as NewDoor;

                            if (doorRef != null)
                            {
                                if (doorRef.otherSideIsTrespassing)
                                {
                                    if (doorRef.otherSideTrespassingEscalation <= 0)
                                    {
                                        setCol = InterfaceControls.Instance.trespassingEscalationZero;
                                    }
                                    else if (doorRef.otherSideTrespassingEscalation >= 1)
                                    {
                                        setCol = InterfaceControls.Instance.trespassingEscalationOne;
                                    }
                                }
                            }
                        }
                        else if (interactionFocus.thisDoor != null)
                        {
                            NewDoor doorRef = interactionFocus.thisDoor.objectRef as NewDoor;

                            if (doorRef != null)
                            {
                                if (doorRef.otherSideIsTrespassing)
                                {
                                    if (doorRef.otherSideTrespassingEscalation <= 0)
                                    {
                                        setCol = InterfaceControls.Instance.trespassingEscalationZero;
                                    }
                                    else if (doorRef.otherSideTrespassingEscalation >= 1)
                                    {
                                        setCol = InterfaceControls.Instance.trespassingEscalationOne;
                                    }
                                }
                            }
                        }

                        InterfaceControls.Instance.interactionText.color = setCol;

                        SetCurrentPlayerInteraction(key, interactionFocus, currentAction); //Set this
                        continue;
                    }
                }
            }

            //Lastly, display weapon actions
            if(FirstPersonItemController.Instance.drawnItem != null)
            {
                //This interactable has this key enabled for its action
                Interactable.InteractableCurrentAction currentAction = null;

                if (FirstPersonItemController.Instance.currentActions.TryGetValue(key, out currentAction))
                {
                    //Display this action (+ set)
                    if (currentAction.currentAction != null && currentAction.display)
                    {
                        SetCurrentPlayerInteraction(key, interactionFocus, currentAction, fpsItem: true); //Set this
                        continue;
                    }
                }
            }

            //Fall back upon locked-in interaction interactions
            if(lockedInInteraction != null)
            {
                //Game.Log("Locked in interaction " + lockedInInteraction.preset.name);

                interactionFocus = lockedInInteraction;

                //This interactable has this key enabled for its action
                Interactable.InteractableCurrentAction currentAction = null;

                if (interactionFocus.currentActions.TryGetValue(key, out currentAction))
                {
                    //Display this action (+ set)
                    if (currentAction.currentAction != null && currentAction.display)
                    {
                        //Game.Log("Set interaction " + key.ToString() + " = " + currentAction.currentAction.interactionName);
                        SetCurrentPlayerInteraction(key, interactionFocus, currentAction); //Set this
                        continue;
                    }
                }
            }

            //Otherwise then disable this control action
            if (!SessionData.Instance.isFloorEdit)
            {
                SetCurrentPlayerInteraction(key, null, null); //Set this
            }
        }

        //Update the display
        if(ControlsDisplayController.Instance != null) ControlsDisplayController.Instance.UpdateControlDisplay();
    }

    public void UpdateInteractionText(string newText)
    {
        //Game.Log("Interface: Set interaction text: " + newText);
        if (newText != InterfaceControls.Instance.interactionText.text)
        {
            InterfaceControls.Instance.interactionText.SetText(newText);
            InterfaceControls.Instance.interactionTextContainer.sizeDelta = new Vector2(InterfaceControls.Instance.interactionText.preferredWidth + 28, 52);
        }
    }

    //Perform an interaction raycast check to see if looked at object is interactable...
    public void InteractionRaycastCheck()
    {
        //This is unavailable if not in fps mode, or the player is carrying an object
        if (carryingObject != null || InteractionController.Instance.dialogMode || InterfaceController.Instance.fade > 0f)
        {
            DisplayInteractionCursor(false);
            return;
        }

        if (CameraController.Instance == null || CameraController.Instance.cam == null) return;

        //Ray ray = CameraController.Instance.cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); //Causes weird frustrum error sometimes? Not sure why- could be a unity bug?
        Ray ray = new Ray(CameraController.Instance.cam.transform.position, CameraController.Instance.cam.transform.forward);

        int hitMask = Toolbox.Instance.interactionRayLayerMask;

        //This is a bit of a hacky workaround/fix but occasionally the room mesh would be in the way of air vents, resulting in the interaction ray being blocked.
        //So use this mask instead which exlcudes them to make sure the player can always interact with vents
        if (Player.Instance.inAirVent)
        {
            hitMask = Toolbox.Instance.interactionRayLayerMaskNoRoomMesh;
        }

        if (Physics.Raycast(ray, out playerCurrentRaycastHit, 12f, hitMask))
        {
            currentLookAtTransform = playerCurrentRaycastHit.transform;
        }
        else currentLookAtTransform = null;

        bool lookAtChange = false; //True if this has changed

        //Trigger looking @ change
        if (playerCurrentRaycastHit.transform != playerPreviousRaycastHit.transform)
        {
            OnPlayerLookAtChange();
            lookAtChange = true;
        }

        //If player is looking at an interactable, do the distance check here(needs frequent update)
        if (lookingAtInteractable)
        {
            //Reading mode...
            bool interactionTrue = false;

            //Distance recognition
            if (currentLookingAtInteractable.interactable.drm)
            {
                //Within readable distance...
                if (playerCurrentRaycastHit.distance <= currentLookingAtInteractable.interactable.preset.recognitionRange)
                {
                    //Is only readable, or is out of interaction range
                    if (currentLookingAtInteractable.interactable.preset.distanceRecognitionOnly || playerCurrentRaycastHit.distance > currentLookingAtInteractable.interactable.GetReachDistance())
                    {
                        interactionTrue = true;

                        //Set reading mode true
                        DisplayInteractionCursor(true, lookAtChange);
                        if (!distanceRecognitionMode) SetDistanceRecognitionMode(true);
                    }
                }
            }

            //Reading mode
            if (
                    currentLookingAtInteractable.interactable.preset.readingEnabled
                    &&
                    (
                        (!currentLookingAtInteractable.interactable.preset.readyingEnabledOnlyWithSwitchIsTue || (currentLookingAtInteractable.interactable.preset.readyingEnabledOnlyWithSwitchIsTue && currentLookingAtInteractable.interactable.sw0))
                        ||
                        (!currentLookingAtInteractable.interactable.preset.readingEnabledOnlyWithKaizenSkill || (currentLookingAtInteractable.interactable.preset.readingEnabledOnlyWithKaizenSkill && (UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.wealthPerception) > 0f || UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.singlePerception) > 0f)))
                    )
                )
            {
                //Within readable distance...
                if (playerCurrentRaycastHit.distance <= GameplayControls.Instance.readingRange)
                {
                    interactionTrue = true;

                    //Delay between pages
                    if (currentLookingAtInteractable.interactable.readingDelay > 0f)
                    {
                        currentLookingAtInteractable.interactable.readingDelay -= Time.deltaTime;

                        if (readingMode)
                        {
                            SetReadingMode(false, false);
                        }
                    }
                    else
                    {
                        //Set reading mode true
                        DisplayInteractionCursor(true, lookAtChange);
                        UpdateReadingModeText();
                        if (!readingMode) SetReadingMode(true, false);
                    }
                }
                else if (readingMode)
                {
                    Game.Log("Debug: Out of reading range: " + playerCurrentRaycastHit.distance + "/" + GameplayControls.Instance.readingRange);
                    SetReadingMode(false, false);
                }
            }
            else if (readingMode) SetReadingMode(false, false);

            //Interaction mode
            if(!currentLookingAtInteractable.interactable.preset.distanceRecognitionOnly)
            {
                float interactionDistance = currentLookingAtInteractable.interactable.GetReachDistance();

                if (playerCurrentRaycastHit.distance <= interactionDistance)
                {
                    if (!interactionTrue)
                    {
                        //Check for valid actions
                        bool interactionPass = false;

                        foreach (InteractablePreset.InteractionAction interaction in currentLookingAtInteractable.interactable.preset.GetActions())
                        {
                            if (interaction.GetInteractionKey() != InteractablePreset.InteractionKey.none)
                            {
                                interactionPass = true;
                                break;
                            }
                        }

                        if (interactionPass)
                        {
                            interactionTrue = true;
                        }
                    }

                    //Set interaction mode true
                    if (distanceRecognitionMode) SetDistanceRecognitionMode(false);
                }
                //Out of reach: Display extra control hints to crouch or jump
                else if (Game.Instance.displayExtraControlHints && !CutSceneController.Instance.cutSceneActive)
                {
                    //This is within 1m
                    if(playerCurrentRaycastHit.distance <= interactionDistance + 1f && !Player.Instance.isCrouched && !Player.Instance.fps.m_Jumping)
                    {
                        //I'm looking down...
                        if (Player.Instance.fps.m_Camera.transform.localRotation.x >= 0.4f)
                        {
                            ControlsDisplayController.Instance.DisplayControlIcon(InteractablePreset.InteractionKey.crouch, "Crouch", InterfaceControls.Instance.controlIconDisplayTime);
                        }
                        else if(Player.Instance.fps.m_Camera.transform.localRotation.x <= -0.4f)
                        {
                            ControlsDisplayController.Instance.DisplayControlIcon(InteractablePreset.InteractionKey.jump, "Jump", InterfaceControls.Instance.controlIconDisplayTime);
                        }
                    }
                }
            }
            else if (distanceRecognitionMode)
            {
                //Set interaction mode false
                SetDistanceRecognitionMode(false);
            }

            //Displaying when it shouldn't...
            if (!interactionTrue) DisplayInteractionCursor(false, lookAtChange);
            else DisplayInteractionCursor(true, lookAtChange);

            //If player is looking at a door add them to the door interaction list...
            if(currentLookingAtInteractable != null && currentLookingAtInteractable.isDoor != null && playerCurrentRaycastHit.distance <= 5f && !Player.Instance.illegalStatus)
            {
                if(!currentLookingAtInteractable.isDoor.usingDoorList.Contains(Player.Instance as Actor))
                {
                    currentLookingAtInteractable.isDoor.usingDoorList.Add(Player.Instance as Actor);
                    addedToDoorInteractionList.Add(currentLookingAtInteractable.isDoor);
                    Game.Log("Player: Player added to door using list (AI should keep door open for you!)");
                }
            }
            else if(addedToDoorInteractionList.Count > 0)
            {
                foreach(NewDoor d in addedToDoorInteractionList)
                {
                    d.usingDoorList.Remove(Player.Instance);
                }

                Game.Log("Player: Player removed from door using list.");
                addedToDoorInteractionList.Clear();
            }
        }
        else
        {
            if (readingMode) SetReadingMode(false, false);
        }

        //Handle citizens getting spooked...
        foreach(Actor actor in CityData.Instance.visibleActors)
        {
            if (actor.ai == null) continue;
            if (actor.isStunned || actor.isDead) continue;
            if (!actor.isMoving) continue;
            if (actor.ai.inCombat || actor.ai.persuit) continue;
            if (actor.isMachine) continue;
            if (actor.interactableController == null) continue;
            if (actor.currentTile != null && actor.currentTile.isStairwell || actor.currentTile.isInvertedStairwell) continue; //Don't do this on stairwells

            float distance = Vector3.Distance(actor.transform.position, Player.Instance.transform.position);

            if(distance <= GameplayControls.Instance.maxPlayerLookAtTailingDistance)
            {
                RaycastHit hit;

                if(Toolbox.Instance.RaycastCheck(CameraController.Instance.cam.transform.position, actor.interactableController.coll, GameplayControls.Instance.maxPlayerLookAtTailingDistance, out hit))
                {
                    //Calculate distance from centre of screen to create a multiplier
                    float centreOfScreenMP = 0f;

                    //Get the 8 points of the bounds
                    Vector3 center = hit.collider.bounds.center;

                    //Normalized screen point
                    Vector2 screenPoint = CameraController.Instance.cam.WorldToScreenPoint(center) / new Vector2(CameraController.Instance.cam.pixelWidth, CameraController.Instance.cam.pixelHeight);

                    //Distance
                    centreOfScreenMP = 1f - (Vector2.Distance(screenPoint, new Vector2(0.5f, 0.5f)) * 2f);
                    centreOfScreenMP = GameplayControls.Instance.screenCentreSpookCurve.Evaluate(centreOfScreenMP);

                    //Multiplier is doubled if the player has their flashlight on
                    if (FirstPersonItemController.Instance.flashlight) centreOfScreenMP *= 1.5f;

                    //This value is halfed if player isn't moving
                    if (!Player.Instance.isMoving) centreOfScreenMP *= 0.5f;

                    //If drunk lower awareness up to 50%
                    Human hu = actor as Human;
                    if(hu != null)
                    {
                        centreOfScreenMP *= 1 - (hu.drunk * 0.5f);

                        //Add alertness up to 20%
                        centreOfScreenMP *= 1 + (hu.alertness * 0.2f);
                    }

                    //Don't get spooked by people we can see
                    if (actor.ai.trackedTargets.Exists(item => item.actor != null && item.actor.isPlayer)) centreOfScreenMP *= 0.3f;

                    if (centreOfScreenMP <= 0.01f) continue;

                    centreOfScreenMP = Mathf.Clamp01(centreOfScreenMP);
                    //Game.Log("Centre of screen distance to " + actor.name + " = " + centreOfScreenMP + " (" + screenPoint + ")");

                    actor.ai.AddSpooked((Mathf.Max(GameplayControls.Instance.maxPlayerLookAtTailingDistance - playerCurrentRaycastHit.distance, 0) / GameplayControls.Instance.maxPlayerLookAtTailingDistance) * Time.deltaTime * GameplayControls.Instance.playerLookAtSpookRate * centreOfScreenMP);
                }
            }
            else
            {
                //Removed spooked if far away
                if(actor.ai.spooked > 0f)
                {
                    actor.ai.AddSpooked(Time.deltaTime * -0.5f);
                }
            }
        }

    }

    //Triggered when the player looks at a different object
    public void OnPlayerLookAtChange()
    {
        if (currentLookingAtInteractable != null && currentLookingAtInteractable.interactable.readingDelay > 0f)
        {
            currentLookingAtInteractable.interactable.readingDelay = 0f;
        }

        lookingAtInteractable = false;
        currentLookingAtInteractable = null;

        if (playerCurrentRaycastHit.transform != null)
        {
            //If this is a test scene, set up the interactable
            if (SessionData.Instance.isTestScene)
            {
                //Game.Log("Looking at " + playerCurrentRaycastHit.transform.name);

                currentLookingAtInteractable = playerCurrentRaycastHit.transform.gameObject.GetComponent<InteractableController>();

                if(currentLookingAtInteractable != null)
                {
                    //Game.Log("Found looking @ interactable controller...");

                    //Create interactable...
                    if (currentLookingAtInteractable.interactable == null)
                    {
                       // Game.Log("... No interactable, attempting to spawn one...");

                        //Find the correct preset
                        InteractablePreset p = null;

                        //Attempt to get item
                        foreach(KeyValuePair<string, InteractablePreset> pair in Toolbox.Instance.objectPresetDictionary)
                        {
                            if(pair.Value.prefab != null && pair.Value.prefab.name == currentLookingAtInteractable.transform.name)
                            {
                                p = pair.Value;
                                break;
                            }
                        }

                        if(p != null)
                        {
                            Game.Log("... Spawning interactable " + p.name);
                            currentLookingAtInteractable.interactable = InteractableCreator.Instance.CreateWorldInteractable(p, Player.Instance, Player.Instance, null, currentLookingAtInteractable.transform.position, currentLookingAtInteractable.transform.eulerAngles, null, null);
                            currentLookingAtInteractable.interactable.spawnedObject = currentLookingAtInteractable.gameObject;
                            currentLookingAtInteractable.Setup(currentLookingAtInteractable.interactable);
                            currentLookingAtInteractable.interactable.OnSpawn();
                        }
                        else
                        {
                            //Attempt to get furniture
                            foreach (FurniturePreset f in Toolbox.Instance.allFurniture)
                            {
                                //Match the prefab...
                                if (f.prefab != null)
                                {
                                    Transform tr = null;

                                    if(f.prefab.name == currentLookingAtInteractable.transform.name)
                                    {
                                        tr = PrefabControls.Instance.cityContainer.transform;
                                    }
                                    else if(f.prefab.name == currentLookingAtInteractable.transform.parent.name)
                                    {
                                        tr = currentLookingAtInteractable.transform;
                                    }

                                    if (tr != null)
                                    {
                                        foreach (FurniturePreset.IntegratedInteractable ii in f.integratedInteractables)
                                        {
                                            if (ii.pairToController == currentLookingAtInteractable.id)
                                            {
                                                Game.Log("... Spawning interactable " + ii.preset.name);
                                                currentLookingAtInteractable.interactable = InteractableCreator.Instance.CreateWorldInteractable(ii.preset, Player.Instance, Player.Instance, null, currentLookingAtInteractable.transform.position, currentLookingAtInteractable.transform.eulerAngles, null, null);

                                                if(ii.preset.isLight != null)
                                                {
                                                    currentLookingAtInteractable.interactable.SetAsLight(ii.preset.isLight, 0, ii.preset.isMainLight, null);
                                                }

                                                currentLookingAtInteractable.Setup(currentLookingAtInteractable.interactable);
                                                currentLookingAtInteractable.interactable.OnSpawn();
                                                break;
                                            }
                                        }

                                        if (currentLookingAtInteractable.interactable != null) break;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    //Game.Log("No IC found on " + playerCurrentRaycastHit.transform.name);
                }
            }

            //Am I looking at an interactable?
            if (playerCurrentRaycastHit.transform.CompareTag("Interactable"))
            {
                //Find the interactable controller that matches this...
                InteractableController[] controllers = playerCurrentRaycastHit.transform.gameObject.GetComponentsInChildren<InteractableController>();

                foreach(InteractableController ic in controllers)
                {
                    if(ic.coll == playerCurrentRaycastHit.collider || (ic.altColl != null && ic.altColl == playerCurrentRaycastHit.collider))
                    {
                        currentLookingAtInteractable = ic;
                        break;
                    }
                }

                //Search for interactable pointers
                if(currentLookingAtInteractable == null)
                {
                    InteractablePointer pointer = playerCurrentRaycastHit.transform.gameObject.GetComponentInChildren<InteractablePointer>();

                    if(pointer != null)
                    {
                        currentLookingAtInteractable = pointer.pointTo;
                    }
                }

                //Lastly, search parents
                if (currentLookingAtInteractable == null)
                {
                    currentLookingAtInteractable = playerCurrentRaycastHit.transform.gameObject.GetComponentInParent<InteractableController>();
                }

                if(currentLookingAtInteractable != null) lookingAtInteractable = true;
            }
        }

        //Cancel interaction cursor
        if (!lookingAtInteractable)
        {
            DisplayInteractionCursor(false);
        }

        //Set previous
        playerPreviousRaycastHit = playerCurrentRaycastHit;

        //Trigger change of looking @ interactable
        if (currentLookingAtInteractable != previousLookingAtInteractable)
        {
            OnPlayerLookAtInteractableChange();
        }

        //Update interaction
        UpdateInteractionText();
    }

    //Triggered when the player looks at a different interactable object (or nothing)
    public void OnPlayerLookAtInteractableChange()
    {
        //Set previous
        previousLookingAtInteractable = currentLookingAtInteractable;

        //Update icon
        UpdateHighlightedInteractionIcon();

        //Select in editor
        #if UNITY_EDITOR
        if (Game.Instance.selectCitizenOnLookAt)
        {
            if (currentLookingAtInteractable != null)
            {
                if (currentLookingAtInteractable.isActor != null)
                {
                    UnityEditor.Selection.activeObject = currentLookingAtInteractable.isActor.gameObject;
                }
            }
        }
        #endif
    }

    //Locked-in interaction mode: Disable various things and bring up right click cancel
    //Note: DropCarriedCheck false is used when dropping a carried item to prevent stack overflow
    public void SetLockedInInteractionMode(Interactable val, int reference = 0, bool dropCarriedCheck = true)
    {
        Interactable previous = lockedInInteraction;

        //Force return from previous locked in interaction
        if (lockedInInteraction != null)
        {
            //Drop anything being carried
            if (InteractionController.Instance.carryingObject != null && dropCarriedCheck)
            {
                InteractionController.Instance.carryingObject.DropThis(false);
            }

            if(lockedInInteraction != null)
            {
                if (lockedInInteraction.usagePoint != null)
                {
                    lockedInInteraction.usagePoint.TrySetUser(Interactable.UsePointSlot.defaultSlot, null);
                }

                if (Player.Instance.interactingWith != null)
                {
                    Actor otherActor = Player.Instance.interactingWith.objectRef as Actor;
                    if (otherActor != null) otherActor.SetInteracting(null);
                }

                Player.Instance.SetInteracting(null);

                //Re-enable collider
                if(lockedInInteraction.controller.coll != null) lockedInInteraction.controller.coll.enabled = true;

                //Fire event
                if (OnReturnFromLockedIn != null)
                {
                    OnReturnFromLockedIn();
                }
            }
        }

        lockedInInteraction = val;
        lockedInInteractionRef = reference;
        Player.Instance.SetInteracting(val);

        //Set usage point
        if (lockedInInteraction != null && lockedInInteraction.usagePoint != null)
        {
            lockedInInteraction.usagePoint.TrySetUser(Interactable.UsePointSlot.defaultSlot, Player.Instance);
        }

        //Update previous
        if (previous != null) previous.UpdateCurrentActions();

        if (lockedInInteraction != null)
        {
            Game.Log("Player: Set locked-in interaction: " + lockedInInteraction.preset.name);

            if (lockedInInteraction.preset.actionsPreset.Exists(item => item.disableCollider) && val != null && val.controller != null && val.controller.coll!= null) val.controller.coll.enabled = false; //Disable collider
            lockedInInteraction.UpdateCurrentActions();
        }

        InteractionRaycastCheck();
        UpdateInteractionText();

        if (lockedInInteraction == null)
        {
            if (Player.Instance.interactingWith != null)
            {
                Actor otherActor = Player.Instance.interactingWith.objectRef as Actor;
                if (otherActor != null) otherActor.SetInteracting(null);
            }

            Player.Instance.SetInteracting(null);

            //Fire event
            if (OnReturnFromLockedIn != null)
            {
                OnReturnFromLockedIn();
            }
        }

        Player.Instance.UpdateIllegalStatus();
    }

    //Action interactions
    public void SetInteractionAction(
                                        float startingValue, //Starting value
                                        float newThreshold, //Threshold
                                        float increaseRate, //Multiplier (ie skill)
                                        string dictName, //Dictionary name in ui.interaction
                                        bool isIllegal, //Is this action illegal?
                                        bool useLockpicks,
                                        Transform lookAtToComplete, //Player must look at this transform to progress
                                        bool cancelIfTooFar = true
                                    )
    {
        //Cancel if existing action
        if (activeInteractionAction) return;

        activeInteractionAction = true;
        activeInteractionActionLookCheck = false;
        cancelInteractionIfOutOfRange = cancelIfTooFar;
        interactionActionLookAt = lookAtToComplete;
        interactionActionAmount = startingValue;
        interactionActionThreshold = newThreshold;
        interactionActionMultiplier = increaseRate;
        interactionActionName = dictName;
        InterfaceControls.Instance.actionInteractionText.text = Strings.Get("ui.interaction", dictName) + "...";

        discoveryOverTime.Clear(); //Clear discoveries over time, set these up after

        //Remove any existing...
        while (spawnedProgressControllers.Count > 0)
        {
            Destroy(spawnedProgressControllers[0].gameObject);
            spawnedProgressControllers.RemoveAt(0);
        }

        //Setup progress display according to strength and items...
        float barWidth = InterfaceControls.Instance.actionInteractionAnchor.rect.width - 8f;

        lockpickGraphics.SetActive(useLockpicks);

        //Lockpicks needed for this...
        if(useLockpicks)
        {
            bool countedCurrent = false;
            float lockpickNeeded = interactionActionThreshold;
            int picksNeeded = 0; //This includes current lockpick...
            float xPos = 4f;

            while (lockpickNeeded > 0f)
            {
                float lockpickAmount = interactionActionMultiplier;

                //First, count the current lockpick that may be partially used...
                if (!countedCurrent)
                {
                    lockpickAmount = GameplayController.Instance.currentLockpickStrength * interactionActionMultiplier;
                    countedCurrent = true;
                }

                //Calculate amount needed with this pick...
                lockpickAmount = Mathf.Min(lockpickAmount, lockpickNeeded);

                //Create object
                GameObject newBar = Instantiate(PrefabControls.Instance.lockpickProgressBar, InterfaceControls.Instance.actionInteractionAnchor);
                LockpickProgressController lpc = newBar.GetComponent<LockpickProgressController>();
                lpc.rect.sizeDelta = new Vector2((lockpickAmount / interactionActionThreshold) * barWidth - 4f, lpc.rect.sizeDelta.y);
                lpc.rect.anchoredPosition = new Vector2(xPos, lpc.rect.anchoredPosition.y);
                xPos += lpc.rect.sizeDelta.x + 4f;

                lpc.SetBarMax(lockpickAmount);
                lpc.SetAmount(0f);

                spawnedProgressControllers.Add(lpc);

                lockpickNeeded -= lockpickAmount;
                picksNeeded++;
            }
        }
        else
        {
            //Create object
            GameObject newBar = Instantiate(PrefabControls.Instance.lockpickProgressBar, InterfaceControls.Instance.actionInteractionAnchor);
            LockpickProgressController lpc = newBar.GetComponent<LockpickProgressController>();
            //lpc.rect.sizeDelta = new Vector2(interactionActionThreshold * barWidth - 4f, lpc.rect.sizeDelta.y);
            lpc.rect.sizeDelta = new Vector2(barWidth - 4f, lpc.rect.sizeDelta.y);
            lpc.rect.anchoredPosition = new Vector2(4f, lpc.rect.anchoredPosition.y);

            lpc.SetBarMax(interactionActionThreshold);
            lpc.SetAmount(0f);

            spawnedProgressControllers.Add(lpc);
        }

        InterfaceControls.Instance.actionInteractionDisplay.gameObject.SetActive(true);

        if(isIllegal && !Player.Instance.locationsOfAuthority.Contains(Player.Instance.currentGameLocation) && Player.Instance.currentGameLocation != Player.Instance.home && !Player.Instance.apartmentsOwned.Contains(Player.Instance.currentGameLocation))
        {
            if(useLockpicks)
            {
                if(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.triggerIllegalOnPick) > 0f)
                {
                    SetIllegalActionActive(false);
                }
                else SetIllegalActionActive(true);
            }
            else SetIllegalActionActive(true);
        }
        else SetIllegalActionActive(false);
    }

    public void SetIllegalActionActive(bool val)
    {
        if(Player.Instance.illegalActionActive != val)
        {
            Player.Instance.illegalActionActive = val;
            Player.Instance.illegalActionTimer = GameplayControls.Instance.illegalActionMinimumTime;

            Game.Log("Player: Set illegal action active: " + val);
            Player.Instance.UpdateIllegalStatus();
        }
    }

    public void CancelInteractionAction()
    {
        while (spawnedProgressControllers.Count > 0)
        {
            Destroy(spawnedProgressControllers[0].gameObject);
            spawnedProgressControllers.RemoveAt(0);
        }

        if (activeInteractionAction)
        {
            SetIllegalActionActive(false);

            activeInteractionAction = false;
            InterfaceControls.Instance.actionInteractionDisplay.gameObject.SetActive(false);

            //Fire event
            if (OnInteractionActionCancelled != null)
            {
                OnInteractionActionCancelled();
            }
        }
    }

    public void CompleteInteractionAction()
    {
        if(!Player.Instance.playerKOInProgress)
        {
            while (spawnedProgressControllers.Count > 0)
            {
                Destroy(spawnedProgressControllers[0].gameObject);
                spawnedProgressControllers.RemoveAt(0);
            }

            InterfaceControls.Instance.actionInteractionDisplay.gameObject.SetActive(false);
        }

        if (activeInteractionAction)
        {
            SetIllegalActionActive(false);
            activeInteractionAction = false;

            Game.Log("Player: Complete interaction action");

            //Fire event
            if (OnInteractionActionCompleted != null)
            {
                OnInteractionActionCompleted();
            }
        }
    }

    private void OnDisable()
    {
        //Set last player positions so they will trigger an update when next triggered
        previousLookingAtInteractable = null;

        //Disable reading mode
        SetReadingMode(false, true);
    }

    //Pick up object
    public void PickUp(Interactable newObj)
    {
        if (carryingObject != null)
        {
            carryingObject.DropThis(false);
        }
        else
        {
            if(newObj != null)
            {
                if(newObj.controller == null)
                {
                    newObj.LoadInteractableToWorld(false, true);
                }

                if(newObj.controller != null) newObj.controller.MovablePickUpThis();

                //Set as locked in interaction
                SetLockedInInteractionMode(newObj, 2);
            }
        }
    }

    //Dialog: Use isRemote override for phone calls
    public void SetDialog(bool val, Interactable newTalkingTo, bool newIsRemote = false, Interactable newRemoteOverrideInteractable = null, ConversationType newConvoType = ConversationType.normal)
    {
        if(newTalkingTo != null)
        {
            Game.Log("Player: Set Dialog " + val + " talking to: " + newTalkingTo.id + " remote: " + newIsRemote);
        }
        else
        {
            Game.Log("Player: Set Dialog " + val);
        }

        dialogMode = val;
        isRemote = newIsRemote;
        talkingTo = newTalkingTo;
        remoteOverride = newRemoteOverrideInteractable;
        if(val) dialogType = newConvoType;
        Game.Log("Player: Setting dialog type: " + dialogType.ToString());
        DialogController.Instance.sideJobReference = null;

        moreOptionsScrollDownArrow.gameObject.SetActive(false);
        moreOptionsScrollUpArrow.gameObject.SetActive(false);

        if (dialogMode)
        {
            //Update depth of field
            InterfaceController.Instance.UpdateDOF();

            if (talkingTo != null && talkingTo.isActor != null)
            {
                //Tie voice to photo
                if (!isRemote)
                {
                    Game.Log("Merging voice and photo");
                    talkingTo.evidence.MergeDataKeys(Evidence.DataKey.voice, Evidence.DataKey.photo);
                }

                Human talkingToHuman = talkingTo.isActor as Human;
                talkingToHuman.SetInConversation(null, !isRemote); //Cancel existing conversation
            }

            PrefabControls.Instance.dialogRect.gameObject.SetActive(true);

            //Set citizen text
            if (talkingTo != null)
            {
                //if (talkingTo.preset.specialCaseFlag == InteractablePreset.SpecialCase.telephone)
                //{
                //    if(talkingTo.node.gameLocation.thisAsAddress != null && Player.Instance.currentGameLocation.thisAsAddress != null)
                //    {
                        
                //    }
                //}

                SetDialogSelection(0);
                RefreshDialogOptions();
                UpdateInteractionText();
            }

            ControlsDisplayController.Instance.SetControlDisplayArea(82, 0, 300, 300);
        }
        else
        {
            talkingTo = null;
            isRemote = false;
            remoteOverride = null;

            //Update depth of field
            InterfaceController.Instance.UpdateDOF();

            //Remove options
            for (int i = 0; i < dialogOptions.Count; i++)
            {
                DialogButtonController dialogButton = dialogOptions[i];
                Destroy(dialogButton.gameObject);
            }

            dialogOptions.Clear();

            PrefabControls.Instance.dialogRect.gameObject.SetActive(false);

            ControlsDisplayController.Instance.RestoreDefaultDisplayArea();

            //Update interaction actions
            if (lockedInInteraction != null)
            {
                lockedInInteraction.UpdateCurrentActions();
                UpdateInteractionText();
            }
        }

        if(InterfaceController.Instance.desktopMode)
        {
            InterfaceController.Instance.objectCycleAnchor.gameObject.SetActive(false);
        }
        else
        {
            InterfaceController.Instance.objectCycleAnchor.gameObject.SetActive(!dialogMode);
        }
    }

    //Refresh dialog options
    public void RefreshDialogOptions()
    {
        if(talkingTo != null)
        {
            Game.Log("Debug: Refreshing dialog options. Talking to " + talkingTo.id + " remote: " + isRemote);
        }

        EvidenceWitness witnessEvidence = talkingTo.evidence as EvidenceWitness;

        //Update name
        citizenNameText.text = witnessEvidence.GetNameForDataKey(Evidence.DataKey.voice); //Get name using voice. This should be tied to photo anyway if in person...

        //Remove options
        for (int i = 0; i < dialogOptions.Count; i++)
        {
            DialogButtonController dialogButton = dialogOptions[i];
            Destroy(dialogButton.gameObject);
        }

        dialogOptions.Clear();

        List<EvidenceWitness.DialogOption> dialogPresets = new List<EvidenceWitness.DialogOption>();

        //Load dialog options from evidence using photo key
        if (talkingTo != null && talkingTo.belongsTo != Player.Instance && talkingTo.speechController != null && !talkingTo.speechController.speechActive && ((isRemote && remoteOverride != null && remoteOverride.speechController != null) || !isRemote))
        {
            //Talking on phone. Get options for voice only...
            if(isRemote && !remoteOverride.speechController.speechActive && remoteOverride.speechController.speechQueue.Count <= 0)
            {
                Game.Log("Debug: ...IsRemote, remote override speech inactive");

                dialogPresets = witnessEvidence.GetDialogOptions(Evidence.DataKey.voice);
                //Debug.Log("Dialog options: " + dialogPresets.Count);
            }
            //Otherwise get options for photo and voice
            else if(!isRemote && talkingTo.speechController.speechQueue.Count <= 0)
            {
                Game.Log("Debug: ...Isn't remote, speech inactive: " + InteractionController.Instance.dialogType.ToString());

                dialogPresets = witnessEvidence.GetDialogOptions(Evidence.DataKey.photo);

                List <EvidenceWitness.DialogOption> voice = witnessEvidence.GetDialogOptions(Evidence.DataKey.voice);

                foreach(EvidenceWitness.DialogOption v in voice)
                {
                    if(!dialogPresets.Contains(v))
                    {
                        dialogPresets.Add(v);
                    }
                }
            }
        }
        //This is the hospital decision tree
        else if(talkingTo != null && talkingTo.preset.specialCaseFlag == InteractablePreset.SpecialCase.hospitalBed)
        {
            Game.Log("Debug: ...Loading hospital decision tree");
            dialogPresets.AddRange(witnessEvidence.GetDialogOptions(Evidence.DataKey.voice));
        }
        //This is a telephone
        else if (talkingTo != null && talkingTo.preset.specialCaseFlag == InteractablePreset.SpecialCase.telephone)
        {
            Game.Log("...Loading telephone decision tree");
            dialogPresets.AddRange(witnessEvidence.GetDialogOptions(Evidence.DataKey.voice));
        }

        //Sort options by rank
        dialogPresets.Sort((p1, p2) => p2.preset.ranking.CompareTo(p1.preset.ranking)); //Using P2 first gives highest first

        int yPos = 0;

        for (int i = 0; i < dialogPresets.Count; i++)
        {
            EvidenceWitness.DialogOption dialogPreset = dialogPresets[i];
            //Game.Log("Dialog option: " + dialogPreset.preset.name);

            //Use enable test
            if (!DialogController.Instance.TestSpecialCaseAvailability(dialogPreset.preset, talkingTo.isActor as Citizen, dialogPreset.jobRef)) continue;

            GameObject newButton = Instantiate(PrefabControls.Instance.dialogOption, PrefabControls.Instance.dialogOptionContainer);
            DialogButtonController dbc = newButton.GetComponent<DialogButtonController>();

            //Setup dialog
            dbc.Setup(dialogPreset);

            //Position
            dbc.rect.anchoredPosition = new Vector2(0, yPos);
            yPos -= 52;

            dialogOptions.Add(dbc);

            //Cost
            dbc.SetSelectable(true);

            int cost = dialogPreset.preset.GetCost(talkingTo.isActor, Player.Instance);

            if(dialogPreset.preset.specialCase == DialogPreset.SpecialCase.medicalCosts)
            {
                cost = Mathf.RoundToInt(dialogPreset.preset.GetCost(talkingTo.isActor, Player.Instance) * (1f - UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.reduceMedicalCosts)));
            }

            if (cost > 0)
            {
                if(cost > GameplayController.Instance.money)
                {
                    dbc.SetSelectable(false);
                }
            }
        }

        //Clamp selection
        SetDialogSelection(Mathf.Clamp(dialogSelection, 0, dialogOptions.Count - 1));

        //End dialog...
        //if (DialogController.Instance.endConversationOnEndOFDialog || DialogController.Instance.endConversationCompleteJob != null)
        //{
        //    if (!talkingTo.isActor.speechController.speechActive && talkingTo.isActor.speechController.speechQueue.Count <= 0)
        //    {
        //        //Does this end the conversation for the player?
        //        if (isRemote && !remoteOverride.speechController.speechActive && remoteOverride.speechController.speechQueue.Count <= 0)
        //        {
        //            Game.Log("Gameplay: Dialog ended call");
        //            if(DialogController.Instance.endConversationOnEndOFDialog) Player.Instance.activeCall.EndCall();
        //            if (DialogController.Instance.endConversationCompleteJob != null) DialogController.Instance.endConversationCompleteJob.CompleteCaseEndDialog();
        //        }
        //        else if(!isRemote)
        //        {
        //            if (DialogController.Instance.endConversationOnEndOFDialog) ActionController.Instance.Return(null, null, Player.Instance);
        //            if (DialogController.Instance.endConversationCompleteJob != null) DialogController.Instance.endConversationCompleteJob.CompleteCaseEndDialog();
        //        }

        //        DialogController.Instance.endConversationOnEndOFDialog = false;
        //        DialogController.Instance.endConversationCompleteJob = null;
        //    }
        //}
    }

    public void SetDialogSelection(int newVal)
    {
        dialogSelection = newVal;
        dialogSelection = Mathf.Clamp(dialogSelection, 0, dialogOptions.Count - 1);

        moreOptionsScrollUpArrow.gameObject.SetActive(false);
        moreOptionsScrollDownArrow.gameObject.SetActive(false);

        //Move container up if selection is more than 3
        PrefabControls.Instance.dialogOptionContainer.anchoredPosition = new Vector2(PrefabControls.Instance.dialogOptionContainer.anchoredPosition.x, -58 + (Mathf.Max(dialogSelection - 3, 0) * 52));

        for (int i = 0; i < dialogOptions.Count; i++)
        {
            DialogButtonController dbc = dialogOptions[i];

            if (dbc.rect.position.y >= moreOptionsScrollUpArrow.position.y)
            {
                dbc.gameObject.SetActive(false);
                moreOptionsScrollUpArrow.gameObject.SetActive(true);
            }
            else if(dbc.rect.position.y <= moreOptionsScrollDownArrow.position.y + 32)
            {
                dbc.gameObject.SetActive(false);
                moreOptionsScrollDownArrow.gameObject.SetActive(true);
            }
            else
            {
                dbc.gameObject.SetActive(true);
            }
        }
    }

    //On sabotage
    public void OnSabotage(Interactable inter)
    {
        //Set as locked in interaction
        InteractionController.Instance.SetLockedInInteractionMode(inter);

        sabotageInteractable = inter;

        Player.Instance.TransformPlayerController(GameplayControls.Instance.sabotageEnter, GameplayControls.Instance.sabotageExit, inter, null, false);

        SetInteractionAction(0f, sabotageInteractable.val, Mathf.LerpUnclamped(GameplayControls.Instance.lockpickEffectivenessRange.x, GameplayControls.Instance.lockpickEffectivenessRange.y, UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.lockpickingEfficiencyModifier)), "sabotaging", true, true, inter.controller.transform);
        Player.Instance.isLockpicking = true;

        //Listen for progress changes
        OnInteractionActionProgressChange += OnSabotageProgressChange;

        //Listen for complete lockpick
        OnInteractionActionCompleted += OnCompleteSabotage;

        //Listen for return from locked in 
        OnReturnFromLockedIn += OnReturnFromSabotage;

        //Play sfx looping
        AudioController.Instance.StopSound(lockpickLoop, AudioController.StopType.immediate, "Stop lockpick");
        lockpickLoop = AudioController.Instance.PlayWorldLooping(AudioControls.Instance.lockpick, Player.Instance, inter);

        //Add fine
        if(Player.Instance.home != Player.Instance.currentGameLocation && !Player.Instance.apartmentsOwned.Contains(Player.Instance.currentGameLocation))
        {
            StatusController.Instance.AddFineRecord(Player.Instance.currentGameLocation.thisAsAddress, inter, StatusController.CrimeType.breakingAndEntering);
        }
    }

    public void OnSabotageProgressChange(float amountChangeThisFrame, float amountToal)
    {
        //Game.Log("Player: On locking use: " + amountChangeThisFrame);
        sabotageInteractable.SetValue(sabotageInteractable.val - amountChangeThisFrame); //Set actual lock strength

        //Use up lockpick
        GameplayController.Instance.UseLockpick(amountChangeThisFrame);

        //Check we still have lockpicks to continue...
        if (GameplayController.Instance.lockPicks <= 0)
        {
            InteractionController.Instance.SetLockedInInteractionMode(null);
            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "not_enough_lockpicks"), InterfaceControls.Icon.lockpick, null, colourOverride: true, col: InterfaceControls.Instance.messageRed, ping: GameMessageController.PingOnComplete.lockpicks);
        }
    }

    public void OnCompleteSabotage()
    {
        //Stop SFX
        AudioController.Instance.StopSound(lockpickLoop, AudioController.StopType.fade, "Complete lockpick");
        lockpickLoop = null;

        InteractionController.Instance.OnInteractionActionProgressChange -= OnSabotageProgressChange;
        //InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromSabotage;
        InteractionController.Instance.OnInteractionActionCompleted -= OnCompleteSabotage;
        Player.Instance.isLockpicking = false;

        sabotageInteractable.SetValue(0f);
        sabotageInteractable.SetSwitchState(false, Player.Instance);

        //Return from lock in
        InteractionController.Instance.SetLockedInInteractionMode(null);

        //Remove fine record
        StatusController.Instance.RemoveFineRecord(null, sabotageInteractable, StatusController.CrimeType.breakingAndEntering, true);
    }

    public void OnReturnFromSabotage()
    {
        //Stop SFX
        AudioController.Instance.StopSound(lockpickLoop, AudioController.StopType.fade, "Return from lockpick");
        lockpickLoop = null;

        InteractionController.Instance.OnInteractionActionProgressChange -= OnSabotageProgressChange;
        InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromSabotage;
        InteractionController.Instance.OnInteractionActionCompleted -= OnCompleteSabotage;
        Player.Instance.isLockpicking = false;

        Player.Instance.ReturnFromTransform();

        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, GameplayController.Instance.lockPicks + " " + Strings.Get("ui.gamemessage", "lockpick_deplete"), InterfaceControls.Icon.lockpick, null, ping: GameMessageController.PingOnComplete.lockpicks);

        //Remove fine record
        StatusController.Instance.RemoveFineRecord(null, sabotageInteractable, StatusController.CrimeType.breakingAndEntering, true);
    }

    //Returns true if illegal. If allow public is true this will consider things non-illegal in public spaces
    public bool GetValidPlayerActionIllegal(Interactable inter, NewNode location, bool allowPublic = true, bool illegalIfNotPlayersHome = true)
    {
        bool illegal = false;

        if (inter == null)
        {
            //Game.Log("Player: Return action illegal: Interactable is null");
            return true;
        }

        if(inter.belongsTo == Player.Instance)
        {
            //Game.Log("Player: Belongs to player");
            return false;
        }

        if(inter.inInventory != null && inter.inInventory != Player.Instance)
        {
            //Game.Log("Player: Return action illegal: Is in other's inventory");
            return true;
        }

        if (location == null)
        {
            //Game.Log("Player: Return action illegal: location is null");
            return true;
        }

        if (location.gameLocation == Player.Instance.home || Player.Instance.apartmentsOwned.Contains(location.gameLocation))
        {
            //Game.Log("Player: Return action not illegal: Player is home");
            return false;
        }
        else if(illegalIfNotPlayersHome && inter.node != null && (inter.node.gameLocation == Player.Instance.home || Player.Instance.apartmentsOwned.Contains(inter.node.gameLocation)))
        {
            //Game.Log("Player: Return action not illegal: Player is home");
            return false;
        }

        if (!Player.Instance.locationsOfAuthority.Contains(location.gameLocation))
        {
            if (inter.belongsTo != Player.Instance)
            {
                if (inter.preset.onlyIllegalIfInNonPublic)
                {
                    if(inter.furnitureParent != null && inter.furnitureParent.furniture != null && inter.furnitureParent.furniture.forcePublicIllegal)
                    {
                        Game.Log("Player: Return action illegal: Force public illegal on furniture: True");
                        illegal = true;
                    }
                    else
                    {
                        if(allowPublic)
                        {
                            illegal = !location.room.IsAccessAllowed(Player.Instance);
                            Game.Log("Player: Return action illegal, !access allowed: " + illegal);
                        }
                        else
                        {
                            Game.Log("Player: Return action illegal: Public places not allowed: True");
                            illegal = true;
                        }
                    }
                }
                else
                {
                    Game.Log("Player: Return action illegal: True");
                    illegal = true;
                }
            }
        }

        return illegal;
    }

    //Update nearby interactables
    public void UpdateNearbyInteractables()
    {
        nearbyInteractables.Clear();

        if (SessionData.Instance.isFloorEdit) return;

        if(!Player.Instance.playerKOInProgress)
        {
            List<Interactable> getNearby = GetValidNearbyInteractables(Player.Instance.currentNode);

            foreach(Interactable i in getNearby)
            {
                if(!nearbyInteractables.Contains(i))
                {
                    nearbyInteractables.Add(i);
                }
            }

            //Search nodes surrounding player
            foreach(Vector2Int v2 in CityData.Instance.offsetArrayX8)
            {
                Vector3Int v3 = Player.Instance.currentNodeCoord + new Vector3Int(v2.x, v2.y, 0);

                NewNode n = null;

                if(PathFinder.Instance.nodeMap.TryGetValue(v3, out n))
                {
                    List<Interactable> getNearby2 = GetValidNearbyInteractables(n);

                    foreach (Interactable i in getNearby2)
                    {
                        if (!nearbyInteractables.Contains(i))
                        {
                            nearbyInteractables.Add(i);
                        }
                    }
                }
            }

            //Add citizens
            foreach(Actor a in CityData.Instance.visibleActors)
            {
                if (a.interactable == null) continue;
                if (a.interactable.controller == null) continue;
                if (a.interactable.controller.coll == null) continue;

                Vector3 boundsCentre = a.interactable.controller.coll.bounds.center;

                Ray ray = new Ray(CameraController.Instance.cam.transform.position, boundsCentre - CameraController.Instance.cam.transform.position);

                float interactionDistance = a.interactable.GetReachDistance();

                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, interactionDistance, Toolbox.Instance.interactionRayLayerMask))
                {
                    if (hit.collider == a.interactable.controller.coll)
                    {
                        //Check for valid actions
                        bool interactionPass = false;

                        foreach (InteractablePreset.InteractionAction interaction in a.interactable.preset.GetActions())
                        {
                            if (interaction.GetInteractionKey() != InteractablePreset.InteractionKey.none)
                            {
                                interactionPass = true;
                                break;
                            }
                        }

                        if (interactionPass && !nearbyInteractables.Contains(a.interactable))
                        {
                            nearbyInteractables.Add(a.interactable);
                        }
                    }
                }
            }

            //Sort by distance to player (lowest first)
            nearbyInteractables.Sort((p1, p2) => Vector3.Distance(p1.controller.coll.bounds.center, CameraController.Instance.cam.transform.position).CompareTo(Vector3.Distance(p2.controller.coll.bounds.center, CameraController.Instance.cam.transform.position)));

        }

        //Game.Log("Update nearby interactables: " + nearbyInteractables.Count);
        UpdateInteractionIcons();

        if (Game.Instance.displayExtraControlHints && !CutSceneController.Instance.cutSceneActive)
        {
            if(nearbyInteractables.Count > 1)
            {
                if(!InputController.Instance.mouseInputMode)
                {
                    nearbyInteractablesHint++;

                    if(nearbyInteractablesHint > 10)
                    {
                        ControlsDisplayController.Instance.DisplayControlIcon(InteractablePreset.InteractionKey.nearestInteractable, "Nearest Interactable", InterfaceControls.Instance.controlIconDisplayTime);
                        nearbyInteractablesHint = 0;
                    }
                }
            }
        }
    }

    public void ClearNearbyInteractables()
    {
        nearbyInteractables.Clear();
        UpdateInteractionIcons();
    }

    //Get nearby interactables within interaction range
    private List<Interactable> GetValidNearbyInteractables(NewNode node)
    {
        List<Interactable> ret = new List<Interactable>();

        foreach(Interactable i in node.interactables)
        {
            if (i.controller == null) continue;
            if (i.controller.coll == null) continue;
            if (ret.Contains(i)) continue;

            Vector3 boundsCentre = i.controller.coll.bounds.center;

            Ray ray = new Ray(CameraController.Instance.cam.transform.position, boundsCentre - CameraController.Instance.cam.transform.position);

            float interactionDistance = i.GetReachDistance();

            RaycastHit hit;

            if(Physics.Raycast(ray, out hit, interactionDistance, Toolbox.Instance.interactionRayLayerMask))
            {
                if(hit.collider == i.controller.coll)
                {
                    //Check for valid actions
                    bool interactionPass = false;

                    foreach (InteractablePreset.InteractionAction interaction in i.preset.GetActions())
                    {
                        if (interaction.GetInteractionKey() != InteractablePreset.InteractionKey.none)
                        {
                            interactionPass = true;
                            break;
                        }
                    }

                    if (interactionPass)
                    {
                        ret.Add(i);
                    }
                }
            }
        }

        return ret;
    }

    public void FocusOnInteractable(Interactable interactable)
    {
        if (interactable.controller == null) return;
        //if (interactable.controller.coll == null) return;

        Game.Log("Focus on: " + interactable.GetName());

        Player.Instance.ForceLookAt(interactable, 0.2f);
    }

    public void UpdateInteractionIcons()
    {
        List<Interactable> required = new List<Interactable>(nearbyInteractables);

        for (int i = 0; i < selectionIcons.Count; i++)
        {
            SelectionIconController icon = selectionIcons[i];

            if(required.Contains(icon.interactable))
            {
                required.Remove(icon.interactable);
                icon.destroy = false;
            }
            else
            {
                icon.Remove();
            }
        }

        foreach(Interactable i in required)
        {
            GameObject newObj = Instantiate(PrefabControls.Instance.objectSelectionIcon, PrefabControls.Instance.objectSelectionContainer);
            SelectionIconController sic = newObj.GetComponent<SelectionIconController>();
            sic.Setup(i);
            selectionIcons.Add(sic);
        }

        UpdateHighlightedInteractionIcon();
    }

    public void UpdateHighlightedInteractionIcon()
    {
        foreach(SelectionIconController sic in selectionIcons)
        {
            if (currentLookingAtInteractable != null && currentLookingAtInteractable.interactable == sic.interactable)
            {
                sic.SetHighlighted(true);
            }
            else sic.SetHighlighted(false);
        }
    }
}
