﻿namespace Rewired.Demos {

    using UnityEngine;
    using UnityEngine.UI;
    using UnityEngine.EventSystems;
    using System.Collections.Generic;
    using System.Collections;
    using TMPro;

    [AddComponentMenu("")]
    public class SimpleControlRemappingSOD : MonoBehaviour
    {
        private const string layout = "Default";
        private const string uiCategory = "UI";
        public List<string> categories = new List<string>();
        public bool enableInputMapping = true;
        public bool listeningForRemap = false;

        private List<Mapping> allMappers = new List<Mapping>();
        private List<Mapping> keyboardAndMouseMappers = new List<Mapping>();
        private List<Mapping> gamepadMappers = new List<Mapping>();

        public GameObject buttonPrefab;
        //public GameObject textPrefab;
        public RectTransform fieldGroupTransform;
        //public RectTransform actionGroupTransform;
        //public TextMeshProUGUI controllerNameUIText;
        public TextMeshProUGUI statusUIText;
        public ToggleController schemeToggle;
        public Button backButton;
        public Button resetControlsButton;
        public Button aboveButton;
        public Button aboveButton2;

        public ControllerType selectedControllerType = ControllerType.Keyboard;
        private int selectedControllerId = 0;

        public List<Row> rows = new List<Row>();
        private TargetMapping _replaceTargetMapping;

        //private ControllerMap controllerMap;
        private List<Controller> controllers = new List<Controller>();
        private List<ControllerMap> controllerMaps = new List<ControllerMap>();

        //Singleton pattern
        private static SimpleControlRemappingSOD _instance;
        public static SimpleControlRemappingSOD Instance { get { return _instance; } }

        //Init
        void Awake()
        {
            //Setup singleton
            if (_instance != null && _instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                _instance = this;
            }
        }

        private void OnEnable()
        {
            if(!ReInput.isReady) return; // don't run if Rewired hasn't been initialized
            if (InputController.Instance == null) return;

            GetController();

            schemeToggle.SetIsOnWithoutNotify(InputController.Instance.mouseInputMode);

            statusUIText.text = string.Empty;

            // Subscribe to events
            ReInput.ControllerConnectedEvent += OnControllerChanged;
            ReInput.ControllerDisconnectedEvent += OnControllerChanged;

            // Create UI elements
            InitializeUI();
        }

        private void OnDisable()
        {
            StopMapping(true);

            ReInput.ControllerConnectedEvent -= OnControllerChanged;
            ReInput.ControllerDisconnectedEvent -= OnControllerChanged;
        }

        private void ClearUI()
        {
            foreach (Row r in rows)
            {
                if (r.button != null)
                {
                    Destroy(r.button.gameObject);
                }
            }

            rows = new List<Row>();
        }

        private void InitializeUI()
        {
            ClearUI();

            if (controllers.Count <= 0 || controllerMaps.Count <= 0)
            {
                // no controller is selected
                return;
            }

            // Create Action fields and input field buttons
            foreach (string str in categories)
            {
                foreach(InputAction action in ReInput.mapping.ActionsInCategory(str))
                {
                    if (!action.userAssignable) continue;
                    Game.Log("Menu: Creating control remap row for " + action + " in " + str);

                    if(action.type == InputActionType.Axis)
                    {
                        // Create a full range, one positive, and one negative field for Axis-type Actions
                        CreateUIRow(action, AxisRange.Full, action.descriptiveName);
                        CreateUIRow(action, AxisRange.Positive, !string.IsNullOrEmpty(action.positiveDescriptiveName) ? action.positiveDescriptiveName : action.descriptiveName + " +");
                        CreateUIRow(action, AxisRange.Negative, !string.IsNullOrEmpty(action.negativeDescriptiveName) ? action.negativeDescriptiveName : action.descriptiveName + " -");
                    }
                    else if(action.type == InputActionType.Button)
                    {
                        // Just create one positive field for Button-type Actions
                        CreateUIRow(action, AxisRange.Positive, action.descriptiveName);
                    }
                }
            }

            RedrawUI();
        }

        private void CreateUIRow(InputAction action, AxisRange actionRange, string label)
        {
            // Create the input field button
            GameObject buttonGo = Instantiate(buttonPrefab, fieldGroupTransform);
            buttonGo.transform.SetAsLastSibling();

            RemapController rc = buttonGo.GetComponent<RemapController>();
            rc.controlDescriptionText.text = Strings.Get("ui.interface", label);
            rc.gameObject.name = rc.controlDescriptionText.text;

            Navigation newNav = new Navigation();
            newNav.mode = Navigation.Mode.Explicit;
            newNav.selectOnLeft = backButton;
            newNav.selectOnRight = resetControlsButton;

            if(rows.Count <= 0)
            {
                newNav.selectOnUp = aboveButton;

                Navigation aboveNav1 = new Navigation();
                aboveNav1.mode = Navigation.Mode.Explicit;
                aboveNav1.selectOnLeft = aboveButton.navigation.selectOnLeft;
                aboveNav1.selectOnRight = aboveButton.navigation.selectOnRight;
                aboveNav1.selectOnUp = aboveButton.navigation.selectOnUp;
                aboveNav1.selectOnDown = rc.primaryControlButton.button;

                aboveButton.navigation = aboveNav1;

                Navigation aboveNav2 = new Navigation();
                aboveNav2.mode = Navigation.Mode.Explicit;
                aboveNav2.selectOnLeft = aboveButton2.navigation.selectOnLeft;
                aboveNav2.selectOnRight = aboveButton2.navigation.selectOnRight;
                aboveNav2.selectOnUp = aboveButton2.navigation.selectOnUp;
                aboveNav2.selectOnDown = rc.primaryControlButton.button;

                aboveButton2.navigation = aboveNav2;
            }
            else
            {
                newNav.selectOnUp = rows[rows.Count - 1].button.primaryControlButton.button;

                Navigation upNav = new Navigation();
                upNav.mode = Navigation.Mode.Explicit;
                upNav.selectOnLeft = rows[rows.Count - 1].button.primaryControlButton.button.navigation.selectOnLeft;
                upNav.selectOnRight = rows[rows.Count - 1].button.primaryControlButton.button.navigation.selectOnRight;
                upNav.selectOnUp = rows[rows.Count - 1].button.primaryControlButton.button.navigation.selectOnUp;
                upNav.selectOnDown = rc.primaryControlButton.button;

                rows[rows.Count - 1].button.primaryControlButton.button.navigation = upNav;
            }

            rc.primaryControlButton.button.navigation = newNav;

            // Add the row to the rows list
            rows.Add(
                new Row()
                {
                    action = action,
                    actionRange = actionRange,
                    button = rc,
                }
            );
        }

        private void RedrawUI()
        {
            Game.Log("Menu: Controls: Redraw UI");

            if (controllers.Count <= 0)
            { // no controller is selected
                ClearUI();
                return;
            }

            // Update joystick name in UI
            //controllerNameUIText.text = controller.name;

            // Update each button label with the currently mapped element identifier
            for (int i = 0; i < rows.Count; i++)
            {
                Row row = rows[i];
                InputAction action = rows[i].action;

                //row.button.primaryControlButton.SetInteractable(true);
                row.button.primaryControlButton.icon.gameObject.SetActive(false);
                row.button.primaryControlButton.text.text = string.Empty;

                // Find the first ActionElementMap that maps to this Action and is compatible with this field type
                foreach(ControllerMap cm in controllerMaps)
                {
                    foreach (var actionElementMap in cm.ElementMapsWithAction(action.id))
                    {
                        if (actionElementMap.ShowInField(row.actionRange))
                        {
                            row.button.primaryControlButton.text.text = actionElementMap.elementIdentifierName;
                            //Game.Log("Menu: Control " + row.button.primaryControlButton.transform.parent.parent.name + " = " + actionElementMap.elementIdentifierName);
                            break;
                        }
                    }
                }
            }
        }

        private void SetSelectedController(ControllerType controllerType)
        {
            bool changed = false;

            // Check if the controller type changed
            if (controllerType != selectedControllerType)
            { // controller type changed
                selectedControllerType = controllerType;
                changed = true;
            }

            // Check if the controller id changed
            int origId = selectedControllerId;

            if((int)selectedControllerType >= 2)
            {
                Game.Log("Menu: Detected player is using a joystick...");
                if(InputController.Instance.player.controllers.joystickCount > 0) selectedControllerId = InputController.Instance.player.controllers.Joysticks[0].id;
                else selectedControllerId = -1;
                schemeToggle.SetIsOnWithoutNotify(false);
            }
            else
            {
                Game.Log("Menu: Detected player is using a mouse/keyboard...");
                selectedControllerId = 0;
                schemeToggle.SetIsOnWithoutNotify(true);
            }

            if(selectedControllerId != origId) changed = true;

            // If the controller changed, stop the input mapper and update the UI
            if(changed)
            {
                StopMapping();
                GetController();
                InitializeUI();
            }
        }

        public void GetController()
        {
            Game.Log("Menu: Attempting to get all controllers and maps...");
            controllers = new List<Controller>();

            if(selectedControllerType == ControllerType.Keyboard || selectedControllerType == ControllerType.Mouse)
            {
                Controller k = InputController.Instance.player.controllers.GetController(ControllerType.Keyboard, selectedControllerId);

                if (k != null)
                {
                    Game.Log("Menu: Adding controller: " + k.hardwareName);
                    controllers.Add(k);
                }

                Controller m = InputController.Instance.player.controllers.GetController(ControllerType.Mouse, selectedControllerId);

                if (m != null)
                {
                    Game.Log("Menu: Adding controller: " + m.hardwareName);
                    controllers.Add(m);
                }
            }
            else
            {
                Controller c = InputController.Instance.player.controllers.GetController(selectedControllerType, selectedControllerId);

                if (c != null)
                {
                    Game.Log("Menu: Adding controller: " + c.hardwareName + " (" + selectedControllerType + ")");
                    controllers.Add(c);
                }
            }

            controllerMaps = new List<ControllerMap>();

            foreach(Controller c in controllers)
            {
                foreach(string str in categories)
                {
                    ControllerMap m = InputController.Instance.player.controllers.maps.GetMap(c.type, c.id, str, layout);

                    if (m == null)
                    {
                        Game.LogError("Unable to get controller map for " + c.type.ToString() + ", id: " + c.id + ", category: " + str + ", layout: " + layout);
                        continue;
                    }
                    else
                    {
                        Game.Log("Menu: Adding map " + m.name + " for controller " + c.hardwareName + " (" + c.type.ToString() + ")");
                        controllerMaps.Add(m);
                    }
                }
            }

            keyboardAndMouseMappers.Clear();
            gamepadMappers.Clear();
            allMappers.Clear();

            foreach (ControllerMap map in controllerMaps)
            {
                Mapping mapping = new Mapping();
                mapping.map = map;

                mapping.mapper = new InputMapper();
                mapping.mapper.options.timeout = 5f;
                mapping.mapper.options.ignoreMouseXAxis = true;
                mapping.mapper.options.ignoreMouseYAxis = true;
                mapping.mapper.InputMappedEvent += OnInputMapped;
                mapping.mapper.StoppedEvent += OnStopped;

                if (map.controllerType == ControllerType.Keyboard || map.controllerType == ControllerType.Mouse)
                {
                    Game.Log("Menu: Found Keyboard/mouse map: " + map.name);
                    keyboardAndMouseMappers.Add(mapping);
                }
                else
                {
                    Game.Log("Menu: Found Gamepad map: " + map.name);
                    gamepadMappers.Add(mapping);
                }

                allMappers.Add(mapping);
            }
        }

        // Event Handlers

        // Called by the controller UI Buttons when pressed
        public void OnControllerSelected(int controllerType)
        {
            SetSelectedController((ControllerType)controllerType);
        }

        public void ResetControls()
        {
            Game.Log("Menu: Reset Controls...");
            InputController.Instance.player.controllers.maps.LoadDefaultMaps(ControllerType.Keyboard);
            InputController.Instance.player.controllers.maps.LoadDefaultMaps(ControllerType.Mouse);
            InputController.Instance.player.controllers.maps.LoadDefaultMaps(ControllerType.Joystick);
            InputController.Instance.player.controllers.maps.LoadDefaultMaps(ControllerType.Custom);

            if (ReInput.userDataStore != null)
            {
                Game.Log("Menu: Controls config saved");
                ReInput.userDataStore.Save();
            }

            InitializeUI();
        }

        // Called by the input field UI Button when pressed
        public void OnInputFieldClicked(RemapController remap)
        {
            if (!enableInputMapping) return;
            enableInputMapping = false;

            Row r = rows.Find(item => item.button == remap);
            if (r == null) return;
            if(controllers.Count <= 0) return; // there is no Controller selected

            int actionElementMapId = -1;

            // Find the first ActionElementMap that maps to this Action and is compatible with this field type
            ControllerMap controllerMapWithReplacement = null;

            foreach(ControllerMap cm in controllerMaps)
            {
                foreach (var actionElementMap in cm.ElementMapsWithAction(r.action.id))
                {
                    if (actionElementMap.ShowInField(r.actionRange))
                    {
                        actionElementMapId = actionElementMap.id;
                        controllerMapWithReplacement = cm;
                        break;
                    }
                }

                if (controllerMapWithReplacement != null) break;
            }

            // Store the information about the replacement if any
            _replaceTargetMapping = new TargetMapping()
            {
                actionElementMapId = actionElementMapId,
                controllerMap = controllerMapWithReplacement
            };

            // Begin listening for input, but use a coroutine so it starts only after a short delay to prevent
            // the button bound to UI Submit from binding instantly when the input field is activated.
            StartCoroutine(StartListeningDelayed(r, actionElementMapId));
        }

        private IEnumerator StartListeningDelayed(Row row, int actionElementMapToReplaceId)
        {
            Game.Log("Menu: Listening for remap...");

            row.button.primaryControlButton.icon.gameObject.SetActive(true);

            foreach (Row r in rows)
            {
                row.button.primaryControlButton.SetInteractable(false);
            }

            //Disable raycaster
            Game.Log("Menu: Disable menu raycaster");
            MainMenuController.Instance.raycaster.enabled = false;

            // Update the UI text
            statusUIText.text = Strings.Get("ui.interface", "Press Any Key");
            row.button.primaryControlButton.text.text = Strings.Get("ui.interface", "Press Any Key");

            // Don't allow a binding for a short period of time after input field is activated
            // to prevent button bound to UI Submit from binding instantly when input field is activated.
            yield return new WaitForSeconds(0.1f);

            if(selectedControllerType == ControllerType.Keyboard || selectedControllerType == ControllerType.Mouse)
            {
                listeningForRemap = true;

                if (keyboardAndMouseMappers.Count <= 0)
                {
                    Game.Log("Menu: There are no keyboard/mouse mappers to listen to! " + keyboardAndMouseMappers.Count + "/" + allMappers.Count);
                    StopMapping();
                }

                foreach (Mapping m in keyboardAndMouseMappers)
                {
                    Game.Log("Menu: Listening on map " + m.map.controllerType.ToString() + "...");

                    m.mapper.options.allowKeyboardModifierKeyAsPrimary = true;
                    m.mapper.options.allowKeyboardKeysWithModifiers = false; //You may want to enable this in future but it requires modifier keys to be held down to assign them solo

                    m.mapper.options.checkForConflicts = false; //Turn this off as it's just easier!
                    m.mapper.options.checkForConflictsWithAllPlayers = false;
                    m.mapper.options.checkForConflictsWithSelf = false;
                    m.mapper.options.checkForConflictsWithSystemPlayer = false;

                    m.mapper.options.defaultActionWhenConflictFound = InputMapper.ConflictResponse.Ignore;

                    // Start listening
                    m.mapper.Start(
                        new InputMapper.Context()
                        {
                            actionId = row.action.id,
                            controllerMap = m.map,
                            actionRange = row.actionRange,
                            
                            //actionElementMapToReplace = null
                            actionElementMapToReplace = m.map.GetElementMap(actionElementMapToReplaceId)
                        }
                    );
                }
            }
            else
            {
                listeningForRemap = true;

                if (gamepadMappers.Count <= 0)
                {
                    Game.Log("Menu: There are no gamepad mappers to listen to! " + gamepadMappers.Count + "/" + allMappers.Count);
                    StopMapping();
                }

                foreach (Mapping m in gamepadMappers)
                {
                    Game.Log("Menu: Listening on map " + m.map.controllerType.ToString() + "...");

                    m.mapper.options.checkForConflicts = false; //Turn this off as it's just easier!
                    m.mapper.options.checkForConflictsWithAllPlayers = false;
                    m.mapper.options.checkForConflictsWithSelf = false;
                    m.mapper.options.checkForConflictsWithSystemPlayer = false;

                    m.mapper.options.defaultActionWhenConflictFound = InputMapper.ConflictResponse.Ignore;

                    // Start listening
                    m.mapper.Start(
                        new InputMapper.Context()
                        {
                            actionId = row.action.id,
                            controllerMap = m.map,
                            actionRange = row.actionRange,
                            //actionElementMapToReplace = null
                            actionElementMapToReplace = m.map.GetElementMap(actionElementMapToReplaceId)
                        }
                    );
                }
            }

            //Disable the UI Controller Maps while listening to prevent UI control and submissions.
            //InputController.Instance.player.controllers.maps.SetMapsEnabled(false, uiCategory);
        }

        private void OnControllerChanged(ControllerStatusChangedEventArgs args)
        {
            SetSelectedController(selectedControllerType);
        }

        private void OnInputMapped(InputMapper.InputMappedEventData data)
        {
            StopMapping();

            Game.Log("Menu: OnInputMapped: " + data.actionElementMap.actionDescriptiveName + " = " +data.actionElementMap.elementIdentifierName.ToString());

            //Handle cross device type binding replacement
            if (_replaceTargetMapping.controllerMap != null)
            { // we have a replacement

                Game.Log("Menu: Replacement detected...");

                // Remove the replacement from the other map if the binding was made on the opposite device type
                if (data.actionElementMap.controllerMap != _replaceTargetMapping.controllerMap)
                {
                    Game.Log("Menu: Deleting other control");
                    _replaceTargetMapping.controllerMap.DeleteElementMap(_replaceTargetMapping.actionElementMapId);
                }
            }

            for (int i = 0; i < controllerMaps.Count; i++)
            {
                ControllerMap cm = controllerMaps[i];

                Game.Log("Menu: Removing conflicts with " + data.actionElementMap);
                //cm.RemoveElementAssignmentConflicts(data.actionElementMap);

                List<int> idToRemove = new List<int>();

                foreach (ActionElementMap actionElementMap in cm.ElementMapsWithAction(data.actionElementMap.actionId))
                {
                    if(actionElementMap.elementIdentifierId != data.actionElementMap.elementIdentifierId && actionElementMap.actionDescriptiveName == data.actionElementMap.actionDescriptiveName)
                    {
                        Game.Log("Menu: Found existing control for " + actionElementMap.actionDescriptiveName + " = " + actionElementMap.elementIdentifierName.ToString() + ", deleting...");
                        idToRemove.Add(actionElementMap.id);
                    }
                }

                foreach(int id in idToRemove)
                {
                    cm.DeleteElementMap(id);
                }
            }

            if (ReInput.userDataStore != null)
            {
                Game.Log("Menu: Controls config saved");
                ReInput.userDataStore.Save();
            }

            RedrawUI();
        }

        public void StopMapping(bool removeEvents = false)
        {
            Game.Log("Menu: Stop mapping");

            foreach (Mapping m in allMappers)
            {
                m.mapper.Stop();
                if(removeEvents) m.mapper.RemoveAllEventListeners();
            }

            listeningForRemap = false;
            InteractionController.Instance.inputCooldown = 0.1f;
            OnStopped(null);
        }

        private void OnStopped(InputMapper.StoppedEventData data)
        {
            Game.Log("Menu: Listening stopped, re-enabling proper controls...");

            listeningForRemap = false;

            for (int i = 0; i < rows.Count; i++)
            {
                Row row = rows[i];
                InputAction action = rows[i].action;

                row.button.primaryControlButton.SetInteractable(true);
            }

            statusUIText.text = string.Empty;

            // Re-enable UI Controller Maps after listening is finished.
            InputController.Instance.player.controllers.maps.SetMapsEnabled(true, uiCategory);

            if (!MainMenuController.Instance.raycaster.enabled)
            {
                Game.Log("Menu: Enable menu raycaster");
                MainMenuController.Instance.raycaster.enabled = true;
            }

            StartCoroutine(RemapDelay());

            RedrawUI();
        }

        //A small delay to stop buttons being re-clicked
        IEnumerator RemapDelay()
        {
            float delay = 1f;

            while(delay > 0f && !enableInputMapping)
            {
                delay -= Time.deltaTime;
                yield return null;
            }

            enableInputMapping = true;
        }

        // A small class to store information about the input field buttons
        [System.Serializable]
        public class Row
        {
            public InputAction action;
            public AxisRange actionRange;
            public RemapController button;
        }

        private struct TargetMapping
        {
            public ControllerMap controllerMap;
            public int actionElementMapId;
        }

        private struct Mapping
        {
            public InputMapper mapper;
            public ControllerMap map;
        }
    }
}