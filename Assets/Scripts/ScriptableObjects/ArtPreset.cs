﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using TMPro;

//A scriptable object that can be used to make window style presets.
[CreateAssetMenu(fileName = "artpreset_data", menuName = "Database/Decor/Art Preset")]

public class ArtPreset : SoCustomComparison
{
    public bool disable = false;

    [Header("Art Settings")]
    [ShowAssetPreview]
    public Texture2D texturePreview;
    public Material material;

    public enum ArtOrientation { portrait, landscape, square, poster, litter, wallGrimeTop, wallGrimeBottom, dynamicClue, graffiti};
    public List<ArtOrientation> orientationCompatibility = new List<ArtOrientation>();

    public float pixelScaleMultiplier = 0.025f;

    [Header("Suitability")]
    public bool allowInResidential = true;
    public bool allowInCommerical = true;
    public bool allowInLobby = true;
    public bool allowOnStreet = false;

    [Range(0, 3)]
    public int basePriority = 1;

    [Space(7)]
    [InfoBox("Colour matching gives a score out of 5")]
    [Tooltip("used to match with room colour scheme")]
    public List<Color> colourMatching = new List<Color>();
    [Range(0, 5)]
    public int colourMatchingScale = 5;

    [Space(7)]
    [Range(0f, 1f)]
    public float minimumWealth = 0f;
    [Range(0f, 1f)]
    public float maximumWealth = 1f;
    [Space(5)]
    [InfoBox("The following gives a score out of x")]
    [Range(0, 5)]
    public int roomMatchingScale = 5;
    [Tooltip("0 = old fashioned/conservative, 1 = modern/liberal: Driven by the design style")]
    [Range(0, 10)]
    public int modernity = 5;
    [Tooltip("0 = informal/cosy, 1 = clean/souless: Driven by the room type.")]
    [Range(0, 10)]
    public int cleanness = 5;
    [Tooltip("0 = understated/quiet, 1 = loud/bold: Driven by the owner's personality")]
    [Range(0, 10)]
    public int loudness = 5;
    [Tooltip("0 = cold/hard, 1 = warm/sensitive: Driven by the owner's personality")]
    [Range(0, 10)]
    public int emotive = 5;

    public bool mustRequireTraitFromBelow = false;
    public List<ArtPreference> traitModifiers = new List<ArtPreference>();

    [System.Serializable]
    public class ArtPreference
    {
        public CharacterTrait trait;
        public int modifier = 1;
    }

    [Header("Dynamic Text")]
    public bool useDynamicText;
    [EnableIf("useDynamicText")]
    public DynamicTextSouce dynamicTextSource = DynamicTextSouce.weaponsDealerPassword;
    [EnableIf("useDynamicText")]
    public TMP_FontAsset textFont;
    [EnableIf("useDynamicText")]
    public Color textColour = Color.white;
    [EnableIf("useDynamicText")]
    public float textSize = 24f;

    public enum DynamicTextSouce { weaponsDealerPassword, blackMarketTraderPassword };

    [Button]
    public void GenerateColourMatching()
    {
#if UNITY_EDITOR
        if (material != null)
        {
            Debug.Log("Colour matching for art " + name);
            colourMatching = new List<Color>();
            Texture2D getTex = material.GetTexture("_BaseColorMap") as Texture2D;

            if(getTex != null)
            {
                for (int i = 0; i < 5; i++)
                {
                    Color getRandom = getTex.GetPixel(UnityEngine.Random.Range(2, 40), UnityEngine.Random.Range(2, 40));
                    colourMatching.Add(getRandom);
                }
            }

            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
    }
}
