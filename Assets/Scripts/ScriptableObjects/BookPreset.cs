﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;
using System.Text;
using NaughtyAttributes;
using UnityEditor;
using TMPro;
using UnityEngine.Networking;
using UnityEngine.EventSystems;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "book_data", menuName = "Database/Book")]

public class BookPreset : SoCustomComparison
{
	public enum BookGenre { crime, history, esoteric, romance, medical, science, architecture, sciFi, memoir, propaganda, politics, beauty, food, nature, poetry};
	public enum BookSeries { none, detectiveGill, talesOfTheHeart, candorHistory};


	[ReadOnly]
	public string bookName;
	[Header("Settings")]
	public string author;
	[ReorderableList]
	public List<BookGenre> genre;
	[Tooltip("Is this part of a series?")]
	public bool isSeries = false;
	[EnableIf("isSeries")]
	public BookSeries seriesTag;
	[EnableIf("isSeries")]
	public int seriesNumber = 1;

	[Header("Ownership rules")]
	[Tooltip("How common this book is")]
	[Range(0f, 1f)]
	public float common = 0.75f;
	[Tooltip("How likely anyone is to own this...")]
	[Range(0f, 1f)]
	public float baseChance = 0f;
	[ReorderableList]
	public List<CharacterTrait.TraitPickRule> pickRules = new List<CharacterTrait.TraitPickRule>();
	[Tooltip("Rules for spawning this (when not on shelf).")]
	public SpawnRules spawnRule = SpawnRules.onlyAtHome;
	public enum SpawnRules { onlyAtHome, onlyAtWork, homeOrWork, secret};

	[Header("Visuals")]
	public Mesh bookMesh;
	public Material bookMaterial;

	[Header("Text")]
	[Tooltip("Where the text is located in the DDS editor")]
	public string ddsMessage;
}
