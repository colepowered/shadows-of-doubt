﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using UnityEngine.UI;

//A class containing static variables used to hold generated city data & references.
//Script pass 1
public class CityData : MonoBehaviour
{
    //Map data
    //Be sure to load this from save data!
    public string cityName = "New City";
    public Vector2 citySize = new Vector2(2, 2);
    public string cityBuiltWith;
    //public enum PopulationAmount { ultraLow, veryLow, low, medium, high, veryHigh, maximum }
    //public PopulationAmount populationAmount = PopulationAmount.high;
    public int citizensToGenerate = 0; //Convert the above into citizens to populate with based on available apartments
    public float populationMultiplier = 1f; //Be sure to set this when loaded from the above
    public string seed; //For generating random numbers

    //Building a city this size
    public Vector2 maxCoord;

    //Boundary locations
    public float boundaryLeft;
    public float boundaryRight;
    public float boundaryUp;
    public float boundaryDown;

    //List of lists that holds all data related to individual tiles.
    public Dictionary<Vector2Int, CityTile> cityTiles = new Dictionary<Vector2Int, CityTile>();

    //This is the block that contains all the border tiles
    public BlockController borderBlock;

    //Struct for parsing tile data from text files
    public struct ParsedFloorTile
    {
        public Vector2 tileLocation;
        public int roomID;
        public List<Vector2> tileAccessList;
        public int designation;
        public int tileType;
        public bool addressAnchor;

        //Rotations
        public float floorRotation;

        //Doors access (will not list this coordinate, just the 'other')
        public List<Vector2> doorsAccess;

        //Windows access (key = coord, value = type)
        public Dictionary<Vector2, int> windowsAccess;

        //Lightswitch
        public bool lightswitch;

        //CCTV (0= none, 1+ = directions)
        public int cctv;
    }

    //Class for containing floor tiles within units/addresses
    public class ParsedFloorData
    {
        public Dictionary<int, List<ParsedFloorTile>> unitData;

        //List of all tiles
        public List<ParsedFloorTile> allTiles = new List<ParsedFloorTile>();

        //Reference from outside tile(key) to inside tile(value)
        public Vector2 mainEntranceOutside = new Vector2(-1, -1);
        public Vector2 mainEntranceInside = new Vector2(-1, -1);

        //Entrances are from outside tile(key) to inside tile(value)
        public Dictionary<Vector2, Vector2> additionalEntrances = new Dictionary<Vector2, Vector2>();

        //Heights
        public float floorHeight = 0f;
        public float ceilingHeight = 0.8f;
    }

    //Dictionary of parsed floor information
    public Dictionary<string, FloorSaveData> floorData = new Dictionary<string, FloorSaveData>();

    //Directory list for buildings & companies (contain references)
    //public Dictionary<Vector2, NewAddress> pathmapEntrances = new Dictionary<Vector2, NewAddress>();
    public List<DistrictController> districtDirectory = new List<DistrictController>();
	public List<BlockController> blocksDirectory = new List<BlockController>();
	public List<StreetController> streetDirectory = new List<StreetController>();
	public List<NewBuilding> buildingDirectory = new List<NewBuilding>();
	public List<NewAddress> addressDirectory = new List<NewAddress>();
    public List<NewFloor> floorDirectory = new List<NewFloor>();
    public Dictionary<int, NewAddress> addressDictionary = new Dictionary<int, NewAddress>();
    public List<NewGameLocation> gameLocationDirectory = new List<NewGameLocation>();
    public List<NewRoom> roomDirectory = new List<NewRoom>();
    public Dictionary<int, NewRoom> roomDictionary = new Dictionary<int, NewRoom>();
    public List<ResidenceController> residenceDirectory = new List<ResidenceController>();
	public List<Company> companyDirectory = new List<Company>();
	public List<Citizen> citizenDirectory = new List<Citizen>();
    public List<Citizen> homelessDirectory = new List<Citizen>();
    public List<Citizen> homedDirectory = new List<Citizen>();
    public Dictionary<int, Human> citizenDictionary = new Dictionary<int, Human>();
    public List<Human> deadCitizensDirectory = new List<Human>();
    public List<Occupation> jobsDirectory = new List<Occupation>();
    public List<Occupation> assignedJobsDirectory = new List<Occupation>();
    public List<Occupation> unemployedDirectory = new List<Occupation>();
    public List<Occupation> criminalJobDirectory = new List<Occupation>();
    public List<ReflectionProbeController> reflectionProbeDirectory = new List<ReflectionProbeController>();
    public List<FurnitureLocation> jobBoardsDirectory = new List<FurnitureLocation>();
    public Dictionary<int, NewDoor> doorDictionary = new Dictionary<int, NewDoor>();
    public List<AirDuctGroup> airDuctGroupDirectory = new List<AirDuctGroup>();
    public List<AirDuctGroup.AirVent> airVentDirectory = new List<AirDuctGroup.AirVent>();
    public List<Interactable> interactableDirectory = new List<Interactable>();
    public List<SceneRecorder> surveillanceDirectory = new List<SceneRecorder>();
    public Dictionary<int, Telephone> phoneDictionary = new Dictionary<int, Telephone>();
    public Dictionary<int, Interactable> savableInteractableDictionary = new Dictionary<int, Interactable>();
    public List<Interactable> caseTrays = new List<Interactable>();
    public Dictionary<int, MetaObject> metaObjectDictionary = new Dictionary<int, MetaObject>();
    public List<LightController> dynamicShadowSystemLights = new List<LightController>();

    public List<Citizen> homlessAssign = new List<Citizen>(); //Used for assigning shanty shacks

    public Dictionary<AddressPreset, List<NewAddress>> addressTypeReference = new Dictionary<AddressPreset, List<NewAddress>>();

    public Dictionary<RetailItemPreset, Evidence> itemSingletons = new Dictionary<RetailItemPreset, Evidence>();

    //Maintained list of rendered rooms
    public HashSet<NewRoom> visibleRooms = new HashSet<NewRoom>();

    //Maintaned list of rendered actors
    public List<Actor> visibleActors = new List<Actor>();

    //Retail items
    //[category][item][address list]
    //public Dictionary<RetailItemPreset.Desire, Dictionary<RetailItemPreset, List<NewAddress>>> retailLocations = new Dictionary<RetailItemPreset.Desire, Dictionary<RetailItemPreset, List<NewAddress>>>();

	//Misc
    public Vector2 floorRange = Vector2.zero;

	//Stat counters
	public int residentialBuildings = 0;
	public int commercialBuildings = 0;
	public int industrialBuildings = 0;
	public int municipalBuildings = 0;
	public int parkBuildings = 0;
	public int inhabitedResidences = 0;
	public int employedCitizens = 0;
	public int extraUnemloyedCreated = 0;

	public float averageShoeSize = 0f;

    //City directory evidence
    public Evidence cityDirectory;
    public Evidence elevatorControls;
    public EvidenceWitness telephone;
    public EvidenceWitness hospitalBed;

    public class CityDirectoryEntry
    {
        public int linkID;
        public string entryName;
        public string sortString;

        //Phone book sort
        public static int PhoneBookSort(CityDirectoryEntry other1, CityDirectoryEntry other2)
        {
            if (other2.sortString == other1.sortString)
            {
                return other1.entryName.CompareTo(other2.entryName);
            }
            else return other1.sortString.CompareTo(other2.sortString);
        }
    }

    public Dictionary<int, string> cityDirText = new Dictionary<int, string>();

	//Singleton pattern
	private static CityData _instance;
	public static CityData Instance { get { return _instance; } }

	void Awake()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }

	//Useful tools
    //4 Directions array
	public Vector2Int[] offsetArrayX4=
	{
		new Vector2Int (0, -1),
		new Vector2Int (-1, 0),
		new Vector2Int (1, 0),
		new Vector2Int (0, 1),
	};

    //4 Directions array: Diagonal .5
    public Vector2[] offsetArrayX4StreetJunction =
    {
        new Vector2 (-0.5f, 0.5f),
        new Vector2 (0.5f, 0.5f),
        new Vector2 (0.5f, -0.5f),
        new Vector2 (-0.5f, -0.5f)
    };

    //4 Directions array: Diagonals
    public Vector2Int[] offsetArrayX4Diagonal =
    {
        new Vector2Int (-1, -1),
        new Vector2Int (-1, 1),
        new Vector2Int (1, -1),
        new Vector2Int (1, 1),
    };

    public Vector2Int[] offsetArrayX8 = 
	{
		new Vector2Int (-1, -1),
		new Vector2Int (0, -1),
		new Vector2Int (1, -1),
		new Vector2Int (-1, 0),
		new Vector2Int (1, 0),
		new Vector2Int (-1, 1),
		new Vector2Int (0, 1),
		new Vector2Int (1, 1)
	};

    
    public enum BlockingDirection { none, behindLeft, behind, behindRight, left, right, frontLeft, front, frontRight };

    public Vector3Int[] offsetArrayX6 =
    {
        new Vector3Int (0, -1, 0),
        new Vector3Int (-1, 0, 0),
        new Vector3Int (1, 0, 0),
        new Vector3Int (0, 1, 0),

        new Vector3Int (0, 0, 1),
        new Vector3Int (0, 0, -1)
    };

    public Vector2Int[] offsetArrayX24 =
    {
        new Vector2Int (-1, -1),
        new Vector2Int (0, -1),
        new Vector2Int (1, -1),
        new Vector2Int (-1, 0),
        new Vector2Int (1, 0),
        new Vector2Int (-1, 1),
        new Vector2Int (0, 1),
        new Vector2Int (1, 1),

        new Vector2Int (-2, -2),
        new Vector2Int (-1, -2),
        new Vector2Int (0, -2),
        new Vector2Int (1, -2),
        new Vector2Int (2, -2),
        new Vector2Int (2, -1),
        new Vector2Int (2, 0),
        new Vector2Int (2, 1),
        new Vector2Int (2, 2),
        new Vector2Int (1, 2),
        new Vector2Int (0, 2),
        new Vector2Int (-1, 2),
        new Vector2Int (-2, 2),
        new Vector2Int (-2, 1),
        new Vector2Int (-2, 0),
        new Vector2Int (-2, -1)
    };

    public int[] angleArrayX4 =
    {
        0,
        90,
        180,
        270
    };

    public int[] angleArrayX8 =
    {
        0,
        45,
        90,
        135,
        180,
        225,
        270,
        315
    };

    void Start()
	{
        //Calculate the maximum real coordinate
        maxCoord = new Vector2(CityTileToRealpos(CityData.Instance.citySize).x, CityTileToRealpos(CityData.Instance.citySize).z);

        //Parse floor data for use in buildings
        ParseFloorData();
    }

    //Parse floor data from text files
    public void ParseFloorData()
    {
        floorData.Clear();

        //Load design styles
        List<TextAsset> floorFiles = AssetLoader.Instance.GetAllFloorData();

        for (int i = 0; i < floorFiles.Count; i++)
        {
            TextAsset text = floorFiles[i];

            FloorSaveData savedData = null;
            savedData = JsonUtility.FromJson<FloorSaveData>(text.text);
            floorData.Add(text.name, savedData);
        }
    }

    //Convert groundmap coords to pathfinder map coords (only works for buildings as the groundmap resolution is lower).
    //Flooring multiplier will work because of zero basing.
    public Vector3Int CityTileToTile(Vector2Int coords)
	{
		Vector3Int output = new Vector3Int
		(
			Mathf.FloorToInt(Mathf.Clamp((coords.x * CityControls.Instance.tileMultiplier) + Mathf.Floor(CityControls.Instance.tileMultiplier * 0.5f), 0f, PathFinder.Instance.tileCitySize.x - 1f)),
            Mathf.FloorToInt(Mathf.Clamp((coords.y * CityControls.Instance.tileMultiplier) + Mathf.Floor(CityControls.Instance.tileMultiplier * 0.5f), 0f, PathFinder.Instance.tileCitySize.y - 1f)),
            0
        );

		return output;
	}

	//Convert pathfinder map coords to groundmap coords
	public Vector2Int PathmapToGroundmap(Vector3Int coords)
	{
		Vector2Int output = new Vector2Int
			(
                Mathf.FloorToInt(Mathf.Clamp(Mathf.Floor(coords.x / CityControls.Instance.tileMultiplier), 0f, CityData.Instance.citySize.x - 1f)),
                Mathf.FloorToInt(Mathf.Clamp(Mathf.Floor(coords.y / CityControls.Instance.tileMultiplier), 0f, CityData.Instance.citySize.y - 1f))
			);

		return output;
	}

    //Convert real unity coords to groundmap
    public Vector2Int RealPosToGroundmap(Vector3 coords)
	{
        Vector2Int output = new Vector2Int
            (
                Mathf.FloorToInt(Mathf.Clamp(Mathf.FloorToInt((PathFinder.Instance.halfCitySizeReal.x + coords.x) / CityControls.Instance.cityTileSize.x), 0, CityData.Instance.citySize.x - 1)),
                Mathf.FloorToInt(Mathf.Clamp(Mathf.FloorToInt((PathFinder.Instance.halfCitySizeReal.y + coords.z) / CityControls.Instance.cityTileSize.y), 0, CityData.Instance.citySize.y - 1))
            );

		return output;
	}

	//Convert real unity coords to pathmap
	public Vector3Int RealPosToPathmap(Vector3 coords)
	{
        Vector3Int output = new Vector3Int
            (
                Mathf.FloorToInt(Mathf.Clamp(Mathf.FloorToInt((PathFinder.Instance.halfCitySizeReal.x + coords.x) / PathFinder.Instance.tileSize.x), 0, PathFinder.Instance.tileCitySize.x - 1)),
                Mathf.FloorToInt(Mathf.Clamp(Mathf.FloorToInt((PathFinder.Instance.halfCitySizeReal.y + coords.z) / PathFinder.Instance.tileSize.y), 0, PathFinder.Instance.tileCitySize.y - 1)),
                0
            );

		return output;
	}

    public Vector3Int RealPosToPathmapIncludingZ(Vector3 coords)
    {
        Vector3Int output = new Vector3Int
            (
                Mathf.FloorToInt(Mathf.Clamp(Mathf.FloorToInt((PathFinder.Instance.halfCitySizeReal.x + coords.x) / PathFinder.Instance.tileSize.x), 0, PathFinder.Instance.tileCitySize.x - 1)),
                Mathf.FloorToInt(Mathf.Clamp(Mathf.FloorToInt((PathFinder.Instance.halfCitySizeReal.y + coords.z) / PathFinder.Instance.tileSize.y), 0, PathFinder.Instance.tileCitySize.y - 1)),
                Mathf.FloorToInt(coords.y / PathFinder.Instance.tileSize.z)
            );

        return output;
    }

    public Vector3 RealPosToNode(Vector3 coords)
    {
        //Bost Y coord slightly. This will ensure objects placed at 0 will not be outputted to the floor below
        coords.y += 0.01f;

        Vector3 output = new Vector3
            (
                Mathf.Clamp(Mathf.FloorToInt((PathFinder.Instance.halfCitySizeReal.x + coords.x) / PathFinder.Instance.nodeSize.x), 0, PathFinder.Instance.nodeCitySize.x - 1),
                Mathf.Clamp(Mathf.FloorToInt((PathFinder.Instance.halfCitySizeReal.y + coords.z) / PathFinder.Instance.nodeSize.y), 0, PathFinder.Instance.nodeCitySize.y - 1),
                Mathf.FloorToInt(coords.y / PathFinder.Instance.nodeSize.z)
            );

        return output;
    }

    //Version of above that doesn't return whole numbers
    public Vector3 RealPosToNodeFloat(Vector3 coords)
    {
        //Bost Y coord slightly. This will ensure objects placed at 0 will not be outputted to the floor below
        coords.y += 0.01f;

        Vector3 output = new Vector3
            (
                Mathf.Clamp((PathFinder.Instance.halfCitySizeReal.x + coords.x) / PathFinder.Instance.nodeSize.x, 0, PathFinder.Instance.nodeCitySize.x - 1),
                Mathf.Clamp((PathFinder.Instance.halfCitySizeReal.y + coords.z) / PathFinder.Instance.nodeSize.y, 0, PathFinder.Instance.nodeCitySize.y - 1),
                coords.y / PathFinder.Instance.nodeSize.z
            );

        return output;
    }

    //Version of the above that returns a Vector3Int
    public Vector3Int RealPosToNodeInt(Vector3 coords)
    {
        //Bost Y coord slightly. This will ensure objects placed at 0 will not be outputted to the floor below
        coords.y += 0.01f;

        Vector3Int output = new Vector3Int
            (
                Mathf.FloorToInt(Mathf.Clamp(Mathf.FloorToInt((PathFinder.Instance.halfCitySizeReal.x + coords.x) / PathFinder.Instance.nodeSize.x), 0, PathFinder.Instance.nodeCitySize.x - 1)),
                Mathf.FloorToInt(Mathf.Clamp(Mathf.FloorToInt((PathFinder.Instance.halfCitySizeReal.y + coords.z) / PathFinder.Instance.nodeSize.y), 0, PathFinder.Instance.nodeCitySize.y - 1)),
                Mathf.FloorToInt(coords.y / PathFinder.Instance.nodeSize.z)
            );

        return output;
    }

    //Convert groundmap coords to real unity coords
    public Vector3 CityTileToRealpos(Vector2 coords)
	{
		Vector3 output = new Vector3
		(
			coords.x * CityControls.Instance.cityTileSize.x - ((CityData.Instance.citySize.x * 0.5f) * CityControls.Instance.cityTileSize.x) + (CityControls.Instance.cityTileSize.x * 0.5f),
			0,
			coords.y * CityControls.Instance.cityTileSize.y - ((CityData.Instance.citySize.y * 0.5f) * CityControls.Instance.cityTileSize.y) + (CityControls.Instance.cityTileSize.y * 0.5f)
		);

		return output;
	}

	//Convert pathmap coords to real unity coords
	public Vector3 TileToRealpos(Vector3Int coords)
	{
		//pathSpace = pre-computed real space between pathFinding grid squares.
		//Minus half of the citysize * tileSpace because the city centre is 0,0, not the corner.
		//Add the pathSpace * 0.5 because this grid coordinate represents the middle of the path space square.
		Vector3 output = new Vector3
		(
				coords.x * PathFinder.Instance.tileSize.x - ((CityData.Instance.citySize.x * 0.5f) * CityControls.Instance.cityTileSize.x) + (PathFinder.Instance.tileSize.x * 0.5f),
                coords.z * PathFinder.Instance.tileSize.z,
				coords.y * PathFinder.Instance.tileSize.y - ((CityData.Instance.citySize.y * 0.5f) * CityControls.Instance.cityTileSize.y) + (PathFinder.Instance.tileSize.y * 0.5f)
		);

		return output;
	}

    public Vector3 TileToRealpos(Vector3 coords)
    {
        //pathSpace = pre-computed real space between pathFinding grid squares.
        //Minus half of the citysize * tileSpace because the city centre is 0,0, not the corner.
        //Add the pathSpace * 0.5 because this grid coordinate represents the middle of the path space square.
        Vector3 output = new Vector3
        (
                coords.x * PathFinder.Instance.tileSize.x - ((CityData.Instance.citySize.x * 0.5f) * CityControls.Instance.cityTileSize.x) + (PathFinder.Instance.tileSize.x * 0.5f),
                coords.z * PathFinder.Instance.tileSize.z,
                coords.y * PathFinder.Instance.tileSize.y - ((CityData.Instance.citySize.y * 0.5f) * CityControls.Instance.cityTileSize.y) + (PathFinder.Instance.tileSize.y * 0.5f)
        );

        return output;
    }

    //Convert node coords to real unity coords
    public Vector3 NodeToRealpos(Vector3 coords)
    {
        //pathSpace = pre-computed real space between pathFinding grid squares.
        //Minus half of the citysize * tileSpace because the city centre is 0,0, not the corner.
        //Add the pathSpace * 0.5 because this grid coordinate represents the middle of the path space square.
        Vector3 output = new Vector3
        (
                coords.x * PathFinder.Instance.nodeSize.x - ((CityData.Instance.citySize.x * 0.5f) * CityControls.Instance.cityTileSize.x) + (PathFinder.Instance.nodeSize.x * 0.5f),
                coords.z * PathFinder.Instance.nodeSize.z,
                coords.y * PathFinder.Instance.nodeSize.y - ((CityData.Instance.citySize.y * 0.5f) * CityControls.Instance.cityTileSize.y) + (PathFinder.Instance.nodeSize.y * 0.5f)
        );

        return output;
    }

    public Vector3 NodeToRealposInt(Vector3Int coords)
    {
        //pathSpace = pre-computed real space between pathFinding grid squares.
        //Minus half of the citysize * tileSpace because the city centre is 0,0, not the corner.
        //Add the pathSpace * 0.5 because this grid coordinate represents the middle of the path space square.
        Vector3 output = new Vector3
        (
                coords.x * PathFinder.Instance.nodeSize.x - ((CityData.Instance.citySize.x * 0.5f) * CityControls.Instance.cityTileSize.x) + (PathFinder.Instance.nodeSize.x * 0.5f),
                coords.z * PathFinder.Instance.nodeSize.z,
                coords.y * PathFinder.Instance.nodeSize.y - ((CityData.Instance.citySize.y * 0.5f) * CityControls.Instance.cityTileSize.y) + (PathFinder.Instance.nodeSize.y * 0.5f)
        );

        return output;
    }


    //Get the height of a tile (real world coordinate of the highest element on a tile)
    public float GetTileHeight(Vector2 coords)
    {
        float ret = 0f;

        //Get origin
        Vector3 rayOrigin = TileToRealpos(new Vector3Int(Mathf.RoundToInt(coords.x), Mathf.RoundToInt(coords.y), 0));
        rayOrigin = new Vector3(rayOrigin.x, CameraController.Instance.camHeightLimit.y, rayOrigin.z);

        //Raycast down from maximum camera height- use maxdistance of the height + 1
        RaycastHit hit;

        //Triggered on hit
        if(Physics.Raycast(rayOrigin, Vector3.down, out hit))
        {
            ret = hit.transform.position.y;
        }

        return ret;
    }

    //Create singleton evidence
    public void CreateSingletons()
    {
        //Create city directory
        cityDirectory = EvidenceCreator.Instance.CreateEvidence("CityDirectory", "CityDirectory") as Evidence;

        //Create elevator controls
        elevatorControls = EvidenceCreator.Instance.CreateEvidence("ElevatorControls", "ElevatorControls") as Evidence;

        //Create telephone
        telephone = EvidenceCreator.Instance.CreateEvidence("Telephone", "Telephone") as EvidenceWitness;

        //Add telephone options
        telephone.dialogOptions.Clear();

        //Create hospital bed
        hospitalBed = EvidenceCreator.Instance.CreateEvidence("HospitalBed", "HospitalBed") as EvidenceWitness;

        //Add telephone options
        hospitalBed.dialogOptions.Clear();

        //Setup default dialog
        foreach (DialogPreset dia in Toolbox.Instance.defaultDialogOptions)
        {
            if (dia.telephoneCallOption)
            {
                //Game.Log("Add " + dia.name + " to telephone options");
                telephone.AddDialogOption(dia.tiedToKey, dia);
            }

            if (dia.hospitalDecisionOption)
            {
                //Game.Log("Add " + dia.name + " to telephone options");
                hospitalBed.AddDialogOption(dia.tiedToKey, dia);
            }
        }

        //Create singletons for retail items without packaging
        foreach (RetailItemPreset r in Toolbox.Instance.allItems)
        {
            //Skip items where we can't tell the place of purchase from the packaging...
            if (r.itemPreset.spawnEvidence == GameplayControls.Instance.retailItemSoldDiscovery) continue;

            List<object> passed = new List<object>();
            passed.Add(r.itemPreset);
            passed.Add(r);

            Evidence itemSingleton = EvidenceCreator.Instance.CreateEvidence("RetailItemSingleton", r.name, passedObjects: passed);
            itemSingletons.Add(r, itemSingleton);
        }

        //Add basic telephone numbers...
        GameplayController.Instance.AddOrMergePhoneNumberData(1540000, false, null, "Indentify Number", false);
        TelephoneController.Instance.AddFakeNumber(1540000, new TelephoneController.CallSource(TelephoneController.CallType.fakeOutbound, CitizenControls.Instance.identifyNumberDialog));

        GameplayController.Instance.AddOrMergePhoneNumberData(5410000, false, null, "Indentify Last Caller", false);
        TelephoneController.Instance.AddFakeNumber(5410000, new TelephoneController.CallSource(TelephoneController.CallType.fakeOutbound, CitizenControls.Instance.lastCallerDialog));

        GameplayController.Instance.AddOrMergePhoneNumberData(9110000, false, null, "Enforcers", false);
        TelephoneController.Instance.AddFakeNumber(9110000, new TelephoneController.CallSource(TelephoneController.CallType.fakeOutbound, CitizenControls.Instance.policeDialog));
    }

    //Create city directory content
    public void CreateCityDirectory()
    {
        Game.Log("CityGen: Generating city directory text...");

        cityDirText.Clear();

        foreach(Company c in CityData.Instance.companyDirectory)
        {
            if(c == null)
            {
                Game.LogError("Null entry found in company directory");
            }
            else if(c.preset == null)
            {
                Game.LogError("Company " + c.companyID + ": " + c.name + " has no preset!");
            }
            else if(c.name == null || c.name.Length <= 0)
            {
                Game.LogError("Company " + c.companyID + ": " + c.preset + " Has no name!");
            }
        }

        //Create city directory lists
        for (int i = 0; i < Toolbox.Instance.alphabet.Length + 1; i++)
        {
            List<CityDirectoryEntry> cityDir = new List<CityDirectoryEntry>();

            //Get alphabet letter (+ number/special)
            char character = '0';

            if (i < Toolbox.Instance.alphabet.Length)
            {
                character = Toolbox.Instance.alphabet[i];

                //Find all companies beginning with this letter
                List<Company> companyList = CityData.Instance.companyDirectory.FindAll(item => (item.preset != null && (!item.preset.isSelfEmployed || item.preset.publicFacing)) && item.name != null && item.name.Length > 0 && item.name.StartsWith(character.ToString(), System.StringComparison.CurrentCultureIgnoreCase));

                foreach (Company comp in companyList)
                {
                    //Create link data: The constructor will assign a link ID you can use directly in compliation of the directory text.
                    Strings.LinkData l = Strings.AddOrGetLink(comp.placeOfBusiness.evidenceEntry, Toolbox.Instance.GetList<Evidence.DataKey>(Evidence.DataKey.location, Evidence.DataKey.name));

                    CityDirectoryEntry newEntry = new CityDirectoryEntry();
                    newEntry.linkID = l.id;
                    newEntry.entryName = comp.name;
                    newEntry.sortString = comp.name;

                    cityDir.Add(newEntry);
                }

                //Find all forced beginning with this letter
                List<NewAddress> forcedList = CityData.Instance.addressDirectory.FindAll(item => item.addressPreset != null && item.addressPreset.forceCityDirectoryInclusion && item.name.Length > 0 && item.name.StartsWith(character.ToString(), System.StringComparison.CurrentCultureIgnoreCase));

                foreach (NewAddress ad in forcedList)
                {
                    //Create link data: The constructor will assign a link ID you can use directly in compliation of the directory text.
                    Strings.LinkData l = Strings.AddOrGetLink(ad.evidenceEntry);

                    CityDirectoryEntry newEntry = new CityDirectoryEntry();
                    newEntry.linkID = l.id;
                    newEntry.entryName = ad.name;
                    newEntry.sortString = ad.name;

                    cityDir.Add(newEntry);
                }

                //Find all citizens with last name beginning with A
                List<Citizen> citizenList = CityData.Instance.citizenDirectory.FindAll(item => item.home != null && item.GetSurName().StartsWith(character.ToString(), System.StringComparison.CurrentCultureIgnoreCase));

                foreach (Citizen cit in citizenList)
                {
                    //Create link data: The constructor will assign a link ID you can use directly in compliation of the directory text.
                    Strings.LinkData l = Strings.AddOrGetLink(cit.home.evidenceEntry);

                    CityDirectoryEntry newEntry = new CityDirectoryEntry();
                    newEntry.linkID = l.id;
                    newEntry.entryName = cit.GetInitialledName();
                    newEntry.sortString = cit.GetSurName();

                    cityDir.Add(newEntry);
                }

                //Player name
                if(Player.Instance.home != null && Player.Instance.GetSurName().StartsWith(character.ToString(), System.StringComparison.CurrentCultureIgnoreCase))
                {
                    Game.Log("CityGen: Adding player to city directory under " + Player.Instance.GetSurName());

                    //Create link data: The constructor will assign a link ID you can use directly in compliation of the directory text.
                    Strings.LinkData l = Strings.AddOrGetLink(Player.Instance.home.evidenceEntry);

                    CityDirectoryEntry newEntry = new CityDirectoryEntry();
                    newEntry.linkID = l.id;
                    newEntry.entryName = Player.Instance.GetInitialledName();
                    newEntry.sortString = Player.Instance.GetSurName();

                    cityDir.Add(newEntry);
                }
            }
            //Find special characters
            else
            {
                //Find all companies beginning with this letter
                List<Company> companyList = CityData.Instance.companyDirectory.FindAll(item => (item.preset != null && (!item.preset.isSelfEmployed || item.preset.publicFacing)) && item.name != null && item.name.Length > 0 && !Toolbox.Instance.alphabet.Contains(item.name.Substring(0, 1).ToUpper().ToCharArray()[0]));

                foreach (Company comp in companyList)
                {
                    //Create link data: The constructor will assign a link ID you can use directly in compliation of the directory text.
                    Strings.LinkData l = Strings.AddOrGetLink(comp.placeOfBusiness.evidenceEntry);

                    CityDirectoryEntry newEntry = new CityDirectoryEntry();
                    newEntry.linkID = l.id;
                    newEntry.entryName = comp.name;
                    newEntry.sortString = comp.name;

                    cityDir.Add(newEntry);
                }

                //Find all forced beginning with this letter
                List<NewAddress> forcedList = CityData.Instance.addressDirectory.FindAll(item => item.addressPreset != null && item.addressPreset.forceCityDirectoryInclusion && item.name.Length > 0 && item.name.StartsWith(character.ToString(), System.StringComparison.CurrentCultureIgnoreCase));

                foreach (NewAddress ad in forcedList)
                {
                    //Create link data: The constructor will assign a link ID you can use directly in compliation of the directory text.
                    Strings.LinkData l = Strings.AddOrGetLink(ad.evidenceEntry);

                    CityDirectoryEntry newEntry = new CityDirectoryEntry();
                    newEntry.linkID = l.id;
                    newEntry.entryName = ad.name;
                    newEntry.sortString = ad.name;

                    cityDir.Add(newEntry);
                }

                //Find all citizens with last name beginning with A
                List<Citizen> citizenList = CityData.Instance.citizenDirectory.FindAll(item => item.home != null && item.GetSurName().Length > 0 && !Toolbox.Instance.alphabet.Contains(item.GetSurName().Substring(0, 1).ToUpper().ToCharArray()[0]));

                foreach (Citizen cit in citizenList)
                {
                    //Create link data: The constructor will assign a link ID you can use directly in compliation of the directory text.
                    Strings.LinkData l = Strings.AddOrGetLink(cit.home.evidenceEntry);

                    CityDirectoryEntry newEntry = new CityDirectoryEntry();
                    newEntry.linkID = l.id;
                    newEntry.entryName = cit.GetInitialledName();
                    newEntry.sortString = cit.GetSurName();

                    cityDir.Add(newEntry);
                }
            }

            //Sort by alphabetical
            cityDir.Sort(CityDirectoryEntry.PhoneBookSort);

            //Parse to text
            StringBuilder sb = new StringBuilder();

            for (int u = 0; u < cityDir.Count; u++)
            {
                CityDirectoryEntry toParse = cityDir[u];

                //Add new line
                if (u > 0)
                {
                    sb.Append("\n");
                }

                //Adjust link text to contain the reference
                string item = "<link=" + toParse.linkID.ToString() + ">" + toParse.entryName + "</link>";
                sb.Append(item);
            }

            cityDirText.Add(i, sb.ToString());
        }
    }

    //Get x8 offset from direction
    public Vector2Int GetOffsetFromDirection(BlockingDirection dir)
    {
        if (dir == BlockingDirection.none)
        {
            return Vector2Int.zero;
        }

        return offsetArrayX8[(int)dir - 1];
    }

    public MetaObject FindMetaObject(int id)
    {
        MetaObject ret = null;

        metaObjectDictionary.TryGetValue(id, out ret);

        if(ret == null)
        {
            Game.Log("Unable to find meta object " + id);
        }

        return ret;
    }

    //Return a human from the dictionary
    public bool GetHuman(int id, out Human output, bool includePlayer = true)
    {
        output = null;

        if (includePlayer && id == 1)
        {
            output = Player.Instance;
            return true;
        }

        citizenDictionary.TryGetValue(id, out output);

        if (!includePlayer && output != null && output.isPlayer)
        {
            output = null;
            return false;
        }

        if (output != null) return true;
        else return false;
    }
}
