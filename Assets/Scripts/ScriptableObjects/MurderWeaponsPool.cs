﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "murderweapons_data", menuName = "Database/Murder Weapons Pool")]

public class MurderWeaponsPool : SoCustomComparison
{
    [InfoBox("The killer will pick one of these to kill ALL their victims...", EInfoBoxType.Normal)]
    public List<MurderWeaponPick> murderWeaponPool = new List<MurderWeaponPick>();

    [System.Serializable]
    public class MurderWeaponPick
    {
        [Tooltip("The weapon itself")]
        public InteractablePreset weapon;
        [Tooltip("Chance of killer dropping this at scene")]
        [Range(0f, 1f)]
        public float chanceOfDroppingAtScene = 0f;

        [Space(7)]
        public Vector2 randomScoreRange = new Vector2(0f, 1f);
        public List<MurderPreset.MurdererModifierRule> traitModifiers = new List<MurderPreset.MurdererModifierRule>();
    }
}
