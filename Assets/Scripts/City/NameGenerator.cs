﻿using UnityEngine;
using System.Collections;
using System.Text;

//Generates names for elements in the city
//Script pass 1
public class NameGenerator : MonoBehaviour
{
	//Singleton pattern
	private static NameGenerator _instance;
	public static NameGenerator Instance { get { return _instance; } }

	void Awake ()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
	}

    public string GenerateName(string prefixList, float prefixChance, string mainList, float mainChance, string suffixList, float suffixChance, string useCustomSeed = "")
    {
        return GenerateName(prefixList, prefixChance, mainList, mainChance, suffixList, suffixChance, false, 0, 0, out _, out _, out _, out _, out _, useCustomSeed);
    }

    public string GenerateName(string prefixList, float prefixChance, string mainList, float mainChance, string suffixList, float suffixChance, out string prefixOutput, out string mainOutput, out string suffixOutput, out bool needsSuffixForShortName, out string alternateTags, string useCustomSeed = "")
    {
		return GenerateName(prefixList, prefixChance, mainList, mainChance, suffixList, suffixChance, false, 0, 0, out prefixOutput, out mainOutput, out suffixOutput, out needsSuffixForShortName, out alternateTags, useCustomSeed);
	}

	public string GenerateName(
                                string prefixList, //List to use for prefix
                                float prefixChance, //Chance of prefix
                                string mainList, //List to use for main
                                float mainChance, //Chance of main
                                string suffixList, //List to use for suffix
                                float suffixChance, //Chance of suffix
                                bool mainIsCitizenName, //The main list variable is a name, so treat it accordingly
								int prefixMainAlliterationWeight, //Weight towards alliteration in the main
								int mainSuffixAlliterationWeight, //Weight towards alliteration in the suffix
								out string prefixOutput,
                                out string mainOutput, //Outputs the main only
                                out string suffixOutput, //Outputs the suffix only
                                out bool needsSuffixForShortName, //If true a short variation of this must include both main + suffix
								out string alternateTags, //Alternate text tags
								string useCustomSeed = ""
                                )
	{
		StringBuilder sb = new StringBuilder();
		bool prefix = false;
		bool main = false;

		alternateTags = string.Empty;
		prefixOutput = string.Empty;
        mainOutput = string.Empty;
        suffixOutput = string.Empty;

        needsSuffixForShortName = false;

		float r = 0;
		if (useCustomSeed.Length > 0) r = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, useCustomSeed, out useCustomSeed);
		else r = Toolbox.Instance.Rand(0f, 1f);

		if (prefixChance > 0f && r <= prefixChance)
		{
			if(prefixList.Substring(0, 5) == "this." && prefixList.Length > 5)
			{
				prefixOutput = prefixList.Substring(5);
			}
			else
			{
				string addToAlternate = string.Empty;
				prefixOutput = Strings.GetRandom(prefixList, out _, out addToAlternate, useCustomSeed);

				if (alternateTags.Length > 0) alternateTags += ";";
				if(addToAlternate.Length > 0) alternateTags += addToAlternate;
			}

			prefix = true;
			sb.Append(prefixOutput);
		}

		r = 0;
		if(useCustomSeed.Length > 0) r = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, useCustomSeed, out useCustomSeed);
		else r = Toolbox.Instance.Rand(0f, 1f);

		if (mainChance > 0f && r <= mainChance)
		{
			if (prefix)
			{
				sb.Append(' ');
			}

			if(mainList.Length > 5 && mainList.Substring(0, 5) == "this.")
			{
                mainOutput = mainList.Substring(5);
			}
			else
			{
				//Get alliteration
				string alliterationStr = string.Empty;

				if (prefixMainAlliterationWeight > 0 && prefix && prefixOutput.Length >= 2)
				{
					alliterationStr = prefixOutput.Substring(0, 1);
				}
				else
				{
					prefixMainAlliterationWeight = 0;
				}

				string addToAlternate = string.Empty;

				mainOutput = Strings.GetRandom(mainList, alliterationStr, prefixMainAlliterationWeight, out needsSuffixForShortName, out addToAlternate, useCustomSeed);

				if (alternateTags.Length > 0) alternateTags += ";";
				if (addToAlternate.Length > 0) alternateTags += addToAlternate;
			}

			//Add 's
			if (mainIsCitizenName && mainOutput.Length > 0)
			{
				if (mainOutput.Substring(mainOutput.Length - 1) == "s")
				{
					mainOutput += "'";
				}
				else mainOutput += "'s";
			}

			sb.Append(mainOutput);
            main = true;
		}

		r = 0;
		if (useCustomSeed.Length > 0) r = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, useCustomSeed, out useCustomSeed);
		else r = Toolbox.Instance.Rand(0f, 1f);

		if (suffixChance > 0f && r <= suffixChance)
		{
			if (main)
			{
				sb.Append(' ');
			}

			if(suffixList.Length > 5 && suffixList.Substring(0, 5) == "this.")
			{
                suffixOutput = suffixList.Substring(5);
			}
			else
			{
				//Get alliteration
				string alliterationStr = string.Empty;

				if(mainSuffixAlliterationWeight > 0 && main && mainOutput.Length >= 2)
				{
					alliterationStr = mainOutput.Substring(0, 1);
				}
				else
				{
					mainSuffixAlliterationWeight = 0;
				}

				string addToAlternate = string.Empty;

				suffixOutput = Strings.GetRandom(suffixList, alliterationStr, mainSuffixAlliterationWeight, out _, out addToAlternate, useCustomSeed);

				if (alternateTags.Length > 0) alternateTags += ";";
				if (addToAlternate.Length > 0) alternateTags += addToAlternate;
			}

            sb.Append(suffixOutput);
		}

		string output = sb.ToString();

        //If no string is returned, then override the 'main' chance and return that.
        if (output == null || output.Length <= 0)
        {
			string addToAlternate = string.Empty;

			output = Strings.GetRandom(mainList, out needsSuffixForShortName, out addToAlternate, useCustomSeed);

			if (alternateTags.Length > 0) alternateTags += ";";
			if (addToAlternate.Length > 0) alternateTags += addToAlternate;

			mainOutput = output;
            suffixOutput = output;
        }

		return output;
	}
}
