﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using System.Linq;
using System.IO;
using System;
using TMPro;

public class PlayerPrefsController : MonoBehaviour
{
    [System.Serializable]
    public class GameSetting
    {
        public string identifier;
        public int intDefault;
        public int intValue;
        public string strDefault;
        public string strValue;
        public ToggleController toggle;
        public SliderController slider;
        public DropdownController dropdown;
        public DropdownController secondaryDropdown;
        public MultiSelectController multiselect;
        public TextMeshProUGUI valueDisplayText;
        public bool lateLoad;
        public bool useDropdownInt = false;
    }

    [ReorderableList]
    public List<GameSetting> gameSettingControls = new List<GameSetting>();

    public bool playedBefore = false; //Is this the first time the player has played the game?
    public bool loadedPlayerPrefs = false;

    //Singleton pattern
    private static PlayerPrefsController _instance;
    public static PlayerPrefsController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        if(!loadedPlayerPrefs)
        {
            //Set language default to system language...
            //GameSetting language = gameSettingControls.Find(item => item.identifier == "language");

            //if(language != null) language.strDefault = Application.systemLanguage.ToString();

            playedBefore = Convert.ToBoolean(PlayerPrefs.GetInt("playedBefore", 0)); //Detect if the game has been played before
            Game.Log("Menu: Played before: " + playedBefore);

            //Set to played before
            if (!playedBefore)
            {
                PlayerPrefs.SetInt("playedBefore", 1);
            }

            LoadPlayerPrefs();
        }
    }

    //Load player prefs
    public void LoadPlayerPrefs(bool lateLoad = false)
    {
        Game.Log("Loading player prefs...");

        foreach (GameSetting set in gameSettingControls)
        {
            if(lateLoad == set.lateLoad)
            {
                if (set.toggle != null)
                {
                    set.intValue = PlayerPrefs.GetInt(set.identifier, set.intDefault); //Use the default value from the list above
                    set.toggle.SetIsOnWithoutNotify(Convert.ToBoolean(set.intValue)); //Set the toggle
                }
                else if (set.slider != null)
                {
                    set.intValue = PlayerPrefs.GetInt(set.identifier, set.intDefault); //Use the default value from the list above
                    set.slider.SetValueWithoutNotify(set.intValue); //Set the toggle
                }
                else if (set.dropdown != null)
                {
                    if(set.useDropdownInt)
                    {
                        set.intValue = PlayerPrefs.GetInt(set.identifier, set.intDefault); //Use the default value from the list above
                        set.dropdown.dropdown.SetValueWithoutNotify(set.intValue);

                        if(set.secondaryDropdown != null)
                        {
                            set.secondaryDropdown.dropdown.SetValueWithoutNotify(set.intValue);
                        }

                        Game.Log("Menu: Set " + set.identifier + " to " + set.intValue);
                    }
                    else
                    {
                        set.strValue = PlayerPrefs.GetString(set.identifier, set.strDefault); //Use the default value from the list above
                        if (set.strValue == null || set.strValue.Length <= 0) set.strValue = set.strDefault;
                        set.dropdown.SelectFromStaticOption(set.strValue);

                        if (set.secondaryDropdown != null)
                        {
                            set.secondaryDropdown.SelectFromStaticOption(set.strValue);
                        }

                        Game.Log("Menu: Set " + set.identifier + " to " + set.strValue);
                    }
                }
                else if (set.multiselect != null)
                {
                    set.intValue = PlayerPrefs.GetInt(set.identifier, set.intDefault);
                    set.multiselect.SetChosen(set.intValue);

                    Game.Log("Menu: Multiselect " + set.identifier + " to " + set.intValue);
                }

                //Force an update (not triggering sound, not fetching from controls)
                OnToggleChanged(set.identifier, false);

                //Set display text
                if (set.valueDisplayText != null)
                {
                    set.valueDisplayText.text = set.intValue.ToString();
                }
            }
        }

        loadedPlayerPrefs = true;
    }

    //Reset all player prefs to defaults
    public void ResetPlayerPrefsToDefaults()
    {
        foreach (GameSetting set in gameSettingControls)
        {
            //PlayerPrefs.GetInt(set.identifier, set.intDefault);
            //PlayerPrefs.GetString(set.identifier, set.strDefault);
            set.intValue = set.intDefault; //Use the default value from the list above
            set.strValue = set.strDefault;

            //Convert ui control values...
            if (set.toggle != null)
            {
                set.toggle.SetIsOnWithoutNotify(Convert.ToBoolean(set.intValue)); //Set the toggle
            }
            else if (set.slider != null)
            {
                set.slider.SetValueWithoutNotify(set.intValue);
            }
            else if(set.dropdown != null)
            {
                if(set.useDropdownInt)
                {
                    set.dropdown.dropdown.SetValueWithoutNotify(set.intValue);

                    if (set.secondaryDropdown != null)
                    {
                        set.secondaryDropdown.dropdown.SetValueWithoutNotify(set.intValue);
                    }
                }
                else
                {
                    set.dropdown.SelectFromStaticOption(set.strValue);

                    if (set.secondaryDropdown != null)
                    {
                        set.secondaryDropdown.SelectFromStaticOption(set.strValue);
                    }
                }
            }

            //Force an update (not triggering sound)
            OnToggleChanged(set.identifier, false);

            //Set display text
            if (set.valueDisplayText != null)
            {
                set.valueDisplayText.text = set.intValue.ToString();
            }
        }
    }

    public int GetSettingInt(string id)
    {
        int ret = 0;
        GameSetting setting = gameSettingControls.Find(item => item.identifier.ToLower() == id.ToLower()); //Case insensitive find
        if (setting != null) ret = setting.intValue;
        return ret;
    }

    public string GetSettingStr(string id)
    {
        string ret = string.Empty;
        GameSetting setting = gameSettingControls.Find(item => item.identifier.ToLower() == id.ToLower()); //Case insensitive find
        if (setting != null) ret = setting.strValue;
        return ret;
    }

    //When a toggle setting is changed
    public void OnToggleChanged(string id, bool fetchValueFromControls, MonoBehaviour elementScript = null)
    {
        GameSetting findSetting = gameSettingControls.Find(item => item.identifier.ToLower() == id.ToLower()); //Case insensitive find

        //Convert ui control values...
        if (findSetting.toggle != null)
        {
            if (fetchValueFromControls) findSetting.intValue = Convert.ToInt32(findSetting.toggle.isOn);
            PlayerPrefs.SetInt(findSetting.identifier, findSetting.intValue); //Save to player prefs
        }
        else if (findSetting.slider != null)
        {
            if (fetchValueFromControls) findSetting.intValue = Mathf.RoundToInt(findSetting.slider.slider.value);
            PlayerPrefs.SetInt(findSetting.identifier, findSetting.intValue); //Save to player prefs
        }
        else if (findSetting.dropdown != null)
        {
            if(findSetting.secondaryDropdown == null || findSetting.dropdown == elementScript)
            {
                if (findSetting.useDropdownInt)
                {
                    if (fetchValueFromControls) findSetting.intValue = findSetting.dropdown.dropdown.value;
                    PlayerPrefs.SetInt(findSetting.identifier, findSetting.intValue); //Save to player prefs
                }
                else
                {
                    if (fetchValueFromControls) findSetting.strValue = findSetting.dropdown.GetCurrentSelectedStaticOption();
                    PlayerPrefs.SetString(findSetting.identifier, findSetting.strValue); //Save to player prefs
                }

                if(findSetting.secondaryDropdown != null)
                {
                    findSetting.secondaryDropdown.dropdown.SetValueWithoutNotify(findSetting.dropdown.dropdown.value);
                }
            }
            else if(findSetting.secondaryDropdown != null && findSetting.secondaryDropdown == elementScript)
            {
                if (findSetting.useDropdownInt)
                {
                    if (fetchValueFromControls) findSetting.intValue = findSetting.secondaryDropdown.dropdown.value;
                    PlayerPrefs.SetInt(findSetting.identifier, findSetting.intValue); //Save to player prefs
                }
                else
                {
                    if (fetchValueFromControls) findSetting.strValue = findSetting.secondaryDropdown.GetCurrentSelectedStaticOption();
                    PlayerPrefs.SetString(findSetting.identifier, findSetting.strValue); //Save to player prefs
                }

                if (findSetting.dropdown != null)
                {
                    findSetting.dropdown.dropdown.SetValueWithoutNotify(findSetting.secondaryDropdown.dropdown.value);
                }
            }
        }
        else if (findSetting.multiselect != null)
        {
            if (fetchValueFromControls) findSetting.intValue = Convert.ToInt32(findSetting.multiselect.chosenIndex);
            PlayerPrefs.SetInt(findSetting.identifier, findSetting.intValue); //Save to player prefs
        }

        //Set display text
        if (findSetting.valueDisplayText != null)
        {
            findSetting.valueDisplayText.text = findSetting.intValue.ToString();
        }

        //Execute changes
        if (findSetting.identifier == "motionBlur")
        {
            if (!SessionData.Instance.isFloorEdit)
            {
                SessionData.Instance.motionBlur.active = Convert.ToBoolean(findSetting.intValue);
                SessionData.Instance.motionBlur.intensity.value = Game.Instance.motionBlurIntensity + SessionData.Instance.GetGameSpeedMotionBlurModifier();
            }
        }
        else if (findSetting.identifier == "depthBlur")
        {
            if (!SessionData.Instance.isFloorEdit) Game.Instance.SetDepthBlur(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "filmGrain")
        {
            if (!SessionData.Instance.isFloorEdit) SessionData.Instance.grain.active = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "bloom")
        {
            if (!SessionData.Instance.isFloorEdit)
            {
                SessionData.Instance.bloom.active = Convert.ToBoolean(findSetting.intValue);
                SessionData.Instance.bloom.intensity.value = Game.Instance.bloomIntensity;
            }
        }
        else if (findSetting.identifier == "colourGrading")
        {
            if (!SessionData.Instance.isFloorEdit) SessionData.Instance.toneMapping.active = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "everywhereIllegal")
        {
            Game.Instance.everywhereIllegal = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "keysToTheCity")
        {
            Game.Instance.keysToTheCity = Convert.ToBoolean(findSetting.intValue);

            //Trigger now
            if (SessionData.Instance.startedGame && Game.Instance.keysToTheCity)
            {
                //Game.Log("Player: Adding " + CityData.Instance.doorDictionary.Count + " door keys to player...");

                foreach (NewAddress ad in CityData.Instance.addressDirectory)
                {
                    Player.Instance.AddToKeyring(ad, false);

                    if(ad.passcode.used)
                    {
                        GameplayController.Instance.AddPasscode(ad.passcode, false);
                    }

                    foreach(NewRoom room in ad.rooms)
                    {
                        if(room.passcode.used)
                        {
                            GameplayController.Instance.AddPasscode(room.passcode, false);
                        }
                    }
                }

                foreach(Human h in CityData.Instance.citizenDirectory)
                {
                    GameplayController.Instance.AddPasscode(h.passcode, false);
                }
            }
        }
        else if (findSetting.identifier == "invisiblePlayer")
        {
            Game.Instance.invisiblePlayer = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "inaudiblePlayer")
        {
            Game.Instance.inaudiblePlayer = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "invinciblePlayer")
        {
            Game.Instance.invinciblePlayer = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "disableFallDamage")
        {
            Game.Instance.disableFallDamage = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "routeTeleport")
        {
            Game.Instance.routeTeleport = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "allUpgrades")
        {
            Game.Instance.giveAllUpgrades = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "allRooms")
        {
            Game.Instance.discoverAllRooms = Convert.ToBoolean(findSetting.intValue);

            if (Game.Instance.discoverAllRooms && SessionData.Instance.startedGame)
            {
                foreach (NewRoom room in CityData.Instance.roomDirectory)
                {
                    room.SetExplorationLevel(2);

                    //Discover ducts
                    foreach (AirDuctGroup ductGrp in room.ductGroups)
                    {
                        foreach (AirDuctGroup.AirDuctSection duct in ductGrp.airDucts)
                        {
                            duct.SetDiscovered(true);
                        }
                    }
                }
            }
        }
        else if (findSetting.identifier == "music")
        {
            //Only do this if setting is different
            bool newSetting = Convert.ToBoolean(findSetting.intValue);

            if (MusicController.Instance != null && MusicController.Instance.enableMusic != newSetting)
            {
                MusicController.Instance.enableMusic = Convert.ToBoolean(findSetting.intValue);

                if (MusicController.Instance.enableMusic)
                {
                    MusicController.Instance.ForceNextTrack();
                }
                else
                {
                    MusicController.Instance.StopCurrentTrack();
                }
            }
        }
        else if (findSetting.identifier == "headBob")
        {
            GameplayControls.Instance.headBobMultiplier = findSetting.intValue / 100f;
        }
        else if (findSetting.identifier == "weatherChange")
        {
            Game.Instance.weatherChangeFrequency = findSetting.intValue / 100f;
        }
        else if (findSetting.identifier == "rainDetail")
        {
            if (findSetting.intValue <= 0)
            {
                Game.Instance.SetRaindrops(false);
                Game.Instance.SetRainWindows(false);
            }
            else if (findSetting.intValue == 1)
            {
                Game.Instance.SetRaindrops(true);
                Game.Instance.SetRainWindows(false);
            }
            else if (findSetting.intValue >= 2)
            {
                Game.Instance.SetRaindrops(true);
                Game.Instance.SetRainWindows(true);
            }
        }
        else if (findSetting.identifier == "uiScale")
        {
            Game.Instance.SetUIScale(findSetting.intValue);
        }
        else if (findSetting.identifier == "language")
        {
            Game.Instance.language = findSetting.strValue;
        }
        else if (findSetting.identifier == "fpsfov")
        {
            Game.Instance.SetFOV(findSetting.intValue);
        }
        else if (findSetting.identifier == "objectiveMarkers")
        {
            Game.Instance.SetObjectiveMarkers(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "gameDifficulty")
        {
            if (findSetting.strValue == "Easy")
            {
                Game.Instance.SetGameDifficulty(0);
            }
            else if (findSetting.strValue == "Hard")
            {
                Game.Instance.SetGameDifficulty(2);
            }
            else if (findSetting.strValue == "Extreme")
            {
                Game.Instance.SetGameDifficulty(3);
            }
            else
            {
                Game.Instance.SetGameDifficulty(1); //Default to normal difficulty
            }
        }
        else if (findSetting.identifier == "gameLength")
        {
            if (findSetting.strValue == "Very Short")
            {
                Game.Instance.SetGameLength(0, SessionData.Instance.startedGame, false, false);
            }
            else if (findSetting.strValue == "Short")
            {
                Game.Instance.SetGameLength(1, SessionData.Instance.startedGame, false, false);
            }
            else if (findSetting.strValue == "Long")
            {
                Game.Instance.SetGameLength(3, SessionData.Instance.startedGame, false, false);
            }
            else if (findSetting.strValue == "Very Long")
            {
                Game.Instance.SetGameLength(4, SessionData.Instance.startedGame, false, false);
            }
            else
            {
                Game.Instance.SetGameLength(2, SessionData.Instance.startedGame, false, false); //Default to normal length
            }
        }
        else if (findSetting.identifier == "startTime")
        {
            //Game.Log("Start time str: " + findSetting.strValue);

            if (findSetting.strValue == "Morning")
            {
                Game.Instance.SetSandboxStartTime(9f);
            }
            else if (findSetting.strValue == "Midday")
            {
                Game.Instance.SetSandboxStartTime(12f);
            }
            else if (findSetting.strValue == "Evening")
            {
                Game.Instance.SetSandboxStartTime(18f);
            }
            else
            {
                Game.Instance.SetSandboxStartTime(0f);
            }
        }
        else if (findSetting.identifier == "resumeAfterPin")
        {
            Game.Instance.resumeAfterPin = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "closeInteractionsOnResume")
        {
            Game.Instance.closeInteractionsOnResume = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "directionalArrow")
        {
            Game.Instance.SetDirectionalArrow(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "awarenessIndicator")
        {
            Game.Instance.SetAwarenessIndicator(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "mouseSensitivityX")
        {
            Game.Instance.mouseSensitivity.x = findSetting.intValue;
        }
        else if (findSetting.identifier == "mouseSensitivityY")
        {
            Game.Instance.mouseSensitivity.y = findSetting.intValue;
        }
        else if (findSetting.identifier == "controllerSensitivityX")
        {
            Game.Instance.controllerSensitivity.x = findSetting.intValue;
        }
        else if (findSetting.identifier == "controllerSensitivityY")
        {
            Game.Instance.controllerSensitivity.y = findSetting.intValue;
        }
        else if (findSetting.identifier == "controlAutoSwitch")
        {
            Game.Instance.controlAutoSwitch = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "controlMethod")
        {
            InputController.Instance.SetMouseInputMode(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "invertX")
        {
            Game.Instance.axisMP.x = (Mathf.Clamp01(findSetting.intValue) * 2 - 1) * -1;
        }
        else if (findSetting.identifier == "invertY")
        {
            Game.Instance.axisMP.y = (Mathf.Clamp01(findSetting.intValue) * 2 - 1) * -1;
        }
        else if (findSetting.identifier == "mouseSmoothing")
        {
            Game.Instance.mouseSmoothing = findSetting.intValue;
        }
        else if (findSetting.identifier == "controllerSmoothing")
        {
            Game.Instance.controllerSmoothing = findSetting.intValue;
        }
        else if (findSetting.identifier == "enableMurders")
        {
            Game.Instance.SetMurders(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "startingApartment")
        {
            Game.Instance.SetSandboxStartingApartment(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "startingMoney")
        {
            Game.Instance.SetSandboxStartingMoney(findSetting.intValue);
        }
        else if (findSetting.identifier == "startingLockpicks")
        {
            Game.Instance.SetSandboxStartingLockpicks(findSetting.intValue);
        }
        else if (findSetting.identifier == "coldStatus")
        {
            Game.Instance.SetEnableColdStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "smellyStatus")
        {
            Game.Instance.SetEnableSmellyStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "injuryStatus")
        {
            Game.Instance.SetEnableInjuryStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "bleedingStatus")
        {
            Game.Instance.SetEnableBleedingStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "hungerStatus")
        {
            Game.Instance.SetEnableHungerStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "hydrationStatus")
        {
            Game.Instance.SetEnableHydrationStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "tiredStatus")
        {
            Game.Instance.SetEnableTiredStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "numbStatus")
        {
            Game.Instance.SetEnableNumbStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "sickStatus")
        {
            Game.Instance.SetEnableSickStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "wetStatus")
        {
            Game.Instance.SetEnableWetStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "drunkStatus")
        {
            Game.Instance.SetEnableDrunkStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "headacheStatus")
        {
            Game.Instance.SetEnableHeadacheStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "energizedStatus")
        {
            Game.Instance.SetEnableEnergizedStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "hydratedStatus")
        {
            Game.Instance.SetEnableHydratedStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "focusedStatus")
        {
            Game.Instance.SetEnableFocusedStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "wellRestedStatus")
        {
            Game.Instance.SetEnableWellRestedStatus(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "controlHints")
        {
            Game.Instance.displayExtraControlHints = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "popupTips")
        {
            SessionData.Instance.SetDisplayTutorialText(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "screenshotMode")
        {
            Game.Instance.SetScreenshotMode(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "forceSideMissionDifficulty")
        {
            Game.Instance.SetForceSideJobDifficulty(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "sideMissionDifficulty")
        {
            Game.Instance.SetForcedSideJobDifficulty(findSetting.intValue);
        }
        else if (findSetting.identifier == "demoMode")
        {
            Game.Instance.demoMode = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "pauseAI")
        {
            Game.Instance.SetPauseAI(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "freeCam")
        {
            Game.Instance.SetFreeCamMode(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "fastForward")
        {
            Game.Instance.SetFastForward(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "skipIntro")
        {
            Game.Instance.skipIntro = (Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "movementSpeed")
        {
            Game.Instance.movementSpeed = findSetting.intValue / 100f;
        }
        else if (findSetting.identifier == "drawDist")
        {
            Game.Instance.SetDrawDistance(findSetting.intValue / 100f);
        }
        else if (findSetting.identifier == "vsync")
        {
            Game.Instance.SetVsync(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "enableFrameCap")
        {
            Game.Instance.SetEnableFrameCap(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "frameCap")
        {
            Game.Instance.SetFrameCap(findSetting.intValue);
        }
        else if (findSetting.identifier == "textspeed")
        {
            Game.Instance.textSpeed = findSetting.intValue / 100f;
        }
        else if (findSetting.identifier == "disableTrespass")
        {
            Game.Instance.disableTrespass = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "timeScale")
        {
            Time.timeScale = Mathf.Max(0.1f, findSetting.intValue / 100f);
            //Game.Log("Timescale: " + Time.timeScale);
        }
        else if (findSetting.identifier == "masterVolume")
        {
            if(AudioController.Instance !=null) AudioController.Instance.SetVCALevel("SFX", findSetting.intValue / 100f);
        }
        else if (findSetting.identifier == "musicVolume")
        {
            if (AudioController.Instance != null) AudioController.Instance.SetVCALevel("Soundtrack", findSetting.intValue / 100f);
        }
        else if (findSetting.identifier == "ambienceVolume")
        {
            if (AudioController.Instance != null) AudioController.Instance.SetVCALevel("Ambience", findSetting.intValue / 100f);
        }
        else if (findSetting.identifier == "weatherVolume")
        {
            if (AudioController.Instance != null) AudioController.Instance.SetVCALevel("Weather", findSetting.intValue / 100f);
        }
        else if (findSetting.identifier == "footstepsVolume")
        {
            if (AudioController.Instance != null) AudioController.Instance.SetVCALevel("Footsteps", findSetting.intValue / 100f);
        }
        else if (findSetting.identifier == "interfaceVolume")
        {
            if (AudioController.Instance != null) AudioController.Instance.SetVCALevel("UI", findSetting.intValue / 100f);
        }
        else if (findSetting.identifier == "notificationsVolume")
        {
            if (AudioController.Instance != null) AudioController.Instance.SetVCALevel("Notifications", findSetting.intValue / 100f);
        }
        else if (findSetting.identifier == "paVolume")
        {
            if (AudioController.Instance != null) AudioController.Instance.SetVCALevel("PA system", findSetting.intValue / 100f);
        }
        else if (findSetting.identifier == "otherVolume")
        {
            if (AudioController.Instance != null) AudioController.Instance.SetVCALevel("Other SFX", findSetting.intValue / 100f);
        }
        else if (findSetting.identifier == "dithering")
        {
            Game.Instance.SetDithering(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "aaMode")
        {
            Game.Instance.SetAAMode(findSetting.intValue);
        }
        else if (findSetting.identifier == "aaQuality")
        {
            Game.Instance.SetAAQuality(findSetting.intValue);
        }
        else if(findSetting.identifier == "licensedMusic")
        {
            Game.Instance.SetAllowLicensedMusic(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "twitchAudienceCitizens")
        {
            StreamingOptionsController.Instance.SetEnableTwitchAudienceCitizens(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "passcodeOverrideToggle")
        {
            Game.Instance.SetPasscodeOverrideToggle(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "passcodeOverride")
        {
            Game.Instance.SetPasscodeOverride(findSetting.intValue);
        }
        else if (findSetting.identifier == "flickeringLights")
        {
            Game.Instance.SetFlickingLights(Convert.ToBoolean(findSetting.intValue));
        }
        else if (findSetting.identifier == "lightDistance")
        {
            Game.Instance.SetLightDistance(findSetting.intValue / 100f);
        }
        else if (findSetting.identifier == "saveGameCompression")
        {
            Game.Instance.useSaveGameCompression = Convert.ToBoolean(findSetting.intValue);
        }
        else if (findSetting.identifier == "cityDataCompression")
        {
            Game.Instance.useCityDataCompression = Convert.ToBoolean(findSetting.intValue);
        }
        else if(findSetting.identifier == "dynamicResolution")
        {
            bool dRes = Convert.ToBoolean(findSetting.intValue);
            DynamicResolutionController.Instance.DynamicResolutionEnabled = dRes;
            DynamicResolutionController.Instance.DLSSEnabled = dRes;
        }
        //else if (findSetting.identifier == "autoUpdateTwitch")
        //{
        //    StreamingOptionsController.Instance.SetAutoUpdate(Convert.ToBoolean(findSetting.intValue));
        //}
        //else if (findSetting.identifier == "twitchUpdateFrequency")
        //{
        //    StreamingOptionsController.Instance.SetUpdateFrequency(findSetting.intValue);
        //}
    }

    [Button]
    public void ResetFirstPlay()
    {
        PlayerPrefs.SetInt("playedBefore", 0);
    }
}
