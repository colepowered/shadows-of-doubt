using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class FurnitureClusterDebug : MonoBehaviour
{
    public MeshRenderer rend;
    public FurnitureCluster cluster;
    public NewNode node;
    public List<DebugFurnitureAnglePlacement> entries = new List<DebugFurnitureAnglePlacement>();

    public Material validMaterial;
    public Material invalidMaterial;

    [System.Serializable]
    public class DebugFurnitureAnglePlacement
    {
        public string name;
        public int angle;
        public bool isValid;
        public List<NewNode> coversNodes;
        public List<string> log;

        [Space(7)]
        public List<string> pathingLog;
    }


    public void Setup(FurnitureCluster newCluster, NewNode newNode)
    {
        node = newNode;
        this.transform.position = CityData.Instance.NodeToRealpos(node.nodeCoord);

        cluster = newCluster;
        name = cluster + " @ " + node.nodeCoord;
    }

    public void AddEntry(DebugFurnitureAnglePlacement newEntry)
    {
        entries.Add(newEntry);

        if(newEntry.isValid)
        {
            name = "*" + name;
            rend.material = validMaterial;
        }
    }
}
