using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;
using UnityEngine.UI;
using UnityEditor;

public class ObjectPhotoCaptureController : MonoBehaviour
{
    [Header("Components")]
    public Transform spawnParent;
    public GameObject spawnedObject;
    public Camera captureCam;

    [Header("Settings")]
    public int resolution = 128;

    [InfoBox("Don't forget to turn film grain off before capturing, unless you want it featured")]

    [Header("Capture")]
    [ShowAssetPreview]
    public Sprite captured;
    [ShowAssetPreview]
    public Sprite icon;
    public InteractablePreset prefabOverrideObject;
    public GameObject prefabOverride;
    [Range(0.1f, 3f)]
    public float scale = 1f;
    [ReadOnly]
    public Vector3 itemPos;
    [ReadOnly]
    public Vector3 itemEuler;
    [Space(7)]
    public Vector2 captureIndex;
    public InteractablePreset captureSingle;

    [Button]
    public void LoadIndex()
    {
        int index = (int)captureIndex.x;

        List<InteractablePreset> allPresets = GetValidPresets();

        if (index >= 0 && index < allPresets.Count)
        {
            captureSingle = allPresets[index];
            LoadSingle();
        }
    }

    private List<InteractablePreset> GetValidPresets()
    {
        List<InteractablePreset> valid = null;

#if UNITY_EDITOR

        List<InteractablePreset> allPresets = AssetLoader.Instance.GetAllInteractables();
        valid = allPresets.FindAll(item => item.spawnable && item.prefab != null && (item.useEvidence || (item.allowInApartmentShop || item.allowInApartmentStorage)));

        captureIndex.y = valid.Count;
#endif

        return valid;
    }

    [Button]
    public void LoadSingle()
    {
#if UNITY_EDITOR
        if(captureSingle != null && captureSingle.spawnable && captureSingle.prefab != null)
        {
            captured = captureSingle.staticImage;
            icon = captureSingle.iconOverride;

            itemPos = captureSingle.imagePos;
            itemEuler = captureSingle.imageRot;
            scale = captureSingle.imageScale;
            prefabOverride = captureSingle.imagePrefabOverride;
            if (prefabOverrideObject != null) prefabOverride = prefabOverrideObject.prefab;
            if (scale == 0f) scale = 1f;

            //Destroy existing
            if (spawnedObject != null) DestroyImmediate(spawnedObject);

            GameObject spawn = captureSingle.prefab;
            if (prefabOverride != null) spawn = prefabOverride;

            spawnedObject = Instantiate(spawn, spawnParent);
            spawnedObject.transform.localPosition = itemPos;
            spawnedObject.transform.localEulerAngles = itemEuler;
            spawnedObject.transform.localScale = new Vector3(scale, scale, scale);
        }
#endif
    }

    [Button]
    public void UpdatePositions()
    {
#if UNITY_EDITOR
        if (spawnedObject != null)
        {
            itemPos = spawnedObject.transform.localPosition;
            itemEuler = spawnedObject.transform.localEulerAngles;
            scale = spawnedObject.transform.localScale.x;

            spawnedObject.transform.localPosition = itemPos;
            spawnedObject.transform.localEulerAngles = itemEuler;
            spawnedObject.transform.localScale = new Vector3(scale, scale, scale);
        }
#endif
    }

    [Button]
    public void NextIndex()
    {
        captureIndex.x++;
        LoadIndex();
    }

    [Button]
    public void PreviousIndex()
    {
        captureIndex.x--;
        if (captureIndex.y < 0) captureIndex.y = 0;
        LoadIndex();
    }

    [Button]
    public void CaptureSingle()
    {
#if UNITY_EDITOR

        if (captureSingle == null) return;

        string prefabName = string.Empty;

        if (spawnedObject == null)
        {
            GameObject spawn = captureSingle.prefab;
            if (prefabOverrideObject != null) prefabOverride = prefabOverrideObject.prefab;
            if (prefabOverride != null) spawn = prefabOverride;
            spawnedObject = Instantiate(spawn, spawnParent);
        }

        prefabName = spawnedObject.name.Substring(0, spawnedObject.name.Length - 7);

        spawnedObject.transform.localPosition = itemPos;
        spawnedObject.transform.localEulerAngles = itemEuler;
        spawnedObject.transform.localScale = new Vector3(scale, scale, scale);

        RenderTexture tempRT = new RenderTexture(resolution, resolution, 24);
        // the 24 can be 0,16,24, formats like
        // RenderTextureFormat.Default, ARGB32 etc.

        captureCam.targetTexture = tempRT;
        captureCam.Render();

        RenderTexture.active = tempRT;
        Texture2D virtualPhoto =
            new Texture2D(resolution, resolution, TextureFormat.RGB24, false);
        // false, meaning no need for mipmaps
        virtualPhoto.ReadPixels(new Rect(0, 0, resolution, resolution), 0, 0);

        RenderTexture.active = null; //can help avoid errors 
        captureCam.targetTexture = null;
        // consider ... Destroy(tempRT);

        byte[] bytes;
        bytes = virtualPhoto.EncodeToPNG();

        string path = Application.dataPath + "/Textures/Interface/NewGraphics/EvidencePhotos/" + prefabName + ".png";
        Debug.Log("Writing: " + path);

        System.IO.File.WriteAllBytes(
            path, bytes);
        // virtualCam.SetActive(false); ... no great need for this.

        //AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate); //Force update
        AssetDatabase.Refresh();

        //path = Application.dataPath + "/Textures/Interface/NewGraphics/EvidencePhotos/" + prefabName + ".png";
        path = "Assets/Textures/Interface/NewGraphics/EvidencePhotos/" + prefabName + ".png";

        //Change settings
        TextureImporter importer = (TextureImporter)AssetImporter.GetAtPath(path);

        if(importer != null)
        {
            importer.textureType = TextureImporterType.Sprite;
            importer.mipmapEnabled = false;
            importer.filterMode = FilterMode.Trilinear;
            importer.textureShape = TextureImporterShape.Texture2D;
            importer.SaveAndReimport();
        }
        else
        {
            Debug.LogError("Cannot find texture importer at " + path);
        }

        //Set sprite
        Sprite newSprite = AssetDatabase.LoadAssetAtPath<Sprite>(path);
        captureSingle.staticImage = newSprite;
        captureSingle.imagePos = itemPos;
        captureSingle.imageRot = itemEuler;
        captureSingle.imageScale = scale;
        captureSingle.imagePrefabOverride = prefabOverride;
        if (icon != null) captureSingle.iconOverride = icon; //set icon
        EditorUtility.SetDirty(captureSingle);

        captured = newSprite;

        AssetDatabase.Refresh();

        Editor[] ed = (Editor[])Resources.FindObjectsOfTypeAll<Editor>();
        for (int i = 0; i < ed.Length; i++)
        {
            if (ed[i].GetType() == this.GetType())
            {
                ed[i].Repaint();
                return;
            }
        }
#endif
    }

    [Button]
    public void CaptureAllSpawnableInteractables()
    {
#if UNITY_EDITOR

        //Load all interactables to resources...
        List<InteractablePreset> allPresets = GetValidPresets();

        Debug.Log("All valid presets: " + allPresets.Count);

        HashSet<GameObject> prefabsCaptured = new HashSet<GameObject>();

        int captured = 0;

        foreach(InteractablePreset p in allPresets)
        {
            //Already captured this model, use the same image...
            //if(prefabsCaptured.Contains(p.prefab))
            //{
            //    //Set sprite
            //    string path = "Assets/Textures/Interface/NewGraphics/EvidencePhotos/" + p.prefab.name + ".png";
            //    Sprite newSprite = AssetDatabase.LoadAssetAtPath<Sprite>(path);
            //    captureSingle.staticImage = newSprite;
            //    EditorUtility.SetDirty(captureSingle);
            //}
            //else
            //{
                captureSingle = p;
            LoadSingle();
                CaptureSingle();

                prefabsCaptured.Add(p.prefab);
                captured++;
            //}

            Debug.Log(captured + " images captured");

            //Destroy existing
            if (spawnedObject != null) DestroyImmediate(spawnedObject);
        }
#endif
    }
}
