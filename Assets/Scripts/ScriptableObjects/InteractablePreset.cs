﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using System;
using UnityEditor;
using System.Reflection;
using System.IO;
using System.Linq;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "object_data", menuName = "Database/Interactable Preset")]

public class InteractablePreset : SoCustomComparison
{
    [System.Serializable]
    public class AIUseSetting
    {
        [Tooltip("Usage point relative to the interactable. Calculated on spawn/move position.")]
        public Vector3 usageOffset = new Vector3(0, 0, 0.35f);
        [Tooltip("Look at point relative to the interactable. Calculated on spawn/move position.")]
        public Vector3 facingOffset;
        [Tooltip("If true, this will use the parent node's floor Y value for the position")]
        public bool useNodeFloorPosition = true;
        [Tooltip("If true, flip the Z axis usage offset depending on actor relative position to the door")]
        public bool useDoorBehaviour = false;
        [Tooltip("If true use the citizen's sitting offset position")]
        public bool useSittingOffset = false;
        [Tooltip("If true use the citizen's standing offset position")]
        public bool useArmsStandingOffset = false;
    }

    public enum InteractionKey { none, primary, secondary, alternative, scrollAxisUp, scrollAxisDown, jump, crouch, sprint, flashlight, caseBoard, map, notebook, moveHorizontal, moveVertical, lookHorizontal, lookVertical, WeaponSelect, nearestInteractable, CaseBoardZoomAxis, MoveEvidenceAxisX, MoveEvidenceAxisY, ContentMoveAxisX, ContentMoveAxisY, SelectLeft, SelectRight, SelectUp, SelectDown, CreateString, LeanLeft, LeanRight };
    public enum Switch { switchState, custom1, custom2, custom3, lockState, lockedIn, sprinting, enforcersInside, ko, securityGrid, carryPhysicsObject};

    [System.Serializable]
    public class SwitchState
    {
        public Switch switchState;
        public bool boolIs;
    }

    [System.Serializable]
    public class IfSwitchState
    {
        public Switch switchState;
        public bool boolIs;
    }

    [System.Serializable]
    public class IfSwitchStateSFX
    {
        public Switch switchState;
        public bool boolIs;
        public AudioEvent triggerAudio;
        public bool isLoop = false;
        public bool isBroadcast = false;
        public bool isMusicPlayer = false;
        public AudioController.StopType stop = AudioController.StopType.fade;
        [Tooltip("Passes an open parameter to FMOD based on switch state")]
        public bool passOpenParam = false;
        [Tooltip("Passes the consumable state parameter to FMOD based on switch state")]
        public bool passCSParam = false;
        [Tooltip("Pass door opening or closing direction")]
        public bool passDoorDirParam = false;
        [Tooltip("Only if player is inside sync bed/chamber")]
        public bool onlyIfInSyncBed = false;
        [Tooltip("Only if player is not inside sync bed/chamber")]
        public bool onlyIfNotInSyncBed = false;
        [Tooltip("Only if this door features a neon sign")]
        public bool onlyIfNeonSign = false;
    }

    [System.Serializable]
    public class InteractionAction
    {
        [Tooltip("The dictionary reference to this action's name")]
        public string interactionName;
        [Tooltip("The action preset")]
        public AIActionPreset action;
        [Tooltip("Use the default key as found on the action preset...")]
        public bool useDefaultKeySetting = true;
        [Tooltip("Which key will activate this?")]
        public InteractionKey keyOverride = InteractionKey.none;
        [Tooltip("Alter the interaction name based on special cases")]
        public SpecialCase specialCase = SpecialCase.none;
        public enum SpecialCase { none, takeSwap, onlyIfDeadAsleepOrUncon, availableInFastForward, onlyAvailableInFastForward, caseFormsNeeded, activeCaseHandInReady, search, knockOnDoor, putBack, originalPlace, onlyIfRestrained, onlyIfNotRestrained, ifInventoryItemDrawn, onlyIfSick, nonCombat, onlyIfMultiPageHasPages, onlyInNormalTimeAndAwakeNonDialog, nonDialog, decorPlacementPurchase, furniturePlacement, decorItemPlacement, citizenReturn, nonCombatOrRestrained };
        [Space(7)]
        [Tooltip("Is this usable by the AI")]
        public bool usableByAI = true;
        [ShowIf("usableByAI")]
        [Tooltip("When AI is performing this, use a delay (seconds)")]
        public float aiUsageDelay = 0f;
        [Space(7)]
        [Tooltip("This action effects these states")]
        public List<SwitchState> effectSwitchStates = new List<SwitchState>();
        [Space(7)]
        [Tooltip("This action is only enabled if the following is true")]
        public List<IfSwitchState> onlyActiveIf = new List<IfSwitchState>();

        [Space(7)]
        [Tooltip("Is this action illegal?")]
        public bool actionIsIllegal = false;
        [Tooltip("Is this action available while illegal?")]
        public bool availableWhileIllegal = true;
        [Tooltip("If above is true, is this allowed when others have witnessed illegal activity?")]
        [EnableIf("availableWhileIllegal")]
        public bool availableWhileWitnessesToIllegal = false;
        [Tooltip("Is this action available while using a locked in action?")]
        public bool availableWhileLockedIn = false;
        [Tooltip("Is this action available while jumping?")]
        public bool availableWhileJumping = true;
        [Tooltip("Cost of performing this action")]
        public int actionCost = 0;
        [Space(5)]
        [Tooltip("If true when this action is unavailable, it will be striked through instead of invisible")]
        public bool useStrikethrough = false;
        [Tooltip("Is this a hiding place?")]
        public bool isHidingPlace = false;
        [EnableIf("isHidingPlace")]
        [Tooltip("Only a hiding place in areas classed as public")]
        public bool onlyHidingPlaceIfPublic = false;
        [Space(7)]
        [Tooltip("Sound event reference for this action")]
        public AudioEvent soundEvent;
        [Tooltip("If true this sound event will automatically be played on trigger. If false then this is just a reference for the sound indicator to known the sound level.")]
        public bool playOnTrigger = true;

        public InteractionKey GetInteractionKey()
        {
            InteractionKey ret = InteractionKey.none;

            if(useDefaultKeySetting && action != null)
            {
                ret = action.defaultKey;
            }
            else
            {
                ret = keyOverride;
            }

            return ret;
        }
    }

    [Header("Spawning")]
    [Tooltip("If true this object can be spawned through the object creator")]
    public bool spawnable = true;
    [ShowIf("spawnable")]
    [Tooltip("You only need to set this if the item is spawnable.")]
    public GameObject prefab;
    [Tooltip("This value is held as a workaround for not being able to access the prefab in multithreading")]
    [ShowIf("spawnable")]
    [ReadOnly]
    public Vector3 prefabLocalEuler = Vector3.zero;
    [Tooltip("This value is held as a workaround for not being able to access the prefab in multithreading")]
    [ShowIf("spawnable")]
    [ReadOnly]
    public Vector3 prefabLocalScale = Vector3.one;
    [Tooltip("Don't save with state data")]
    public bool dontSaveWithSaveGames = false;
    [ShowIf("dontSaveWithSaveGames")]
    [Tooltip("Override the above behaviour if this is classed a world object; useful for lightswitches if they have been placed by the player")]
    public bool onlySaveWithSaveGamesIfWorldObject = false;
    [Tooltip("Object pooling will not be used for this")]
    [ShowIf("spawnable")]
    public bool excludeFromObjectPooling = false;
    [Tooltip("If true, the mesh renderers on this object won't get turned on and off with range or room visibility.")]
    [ShowIf("excludeFromObjectPooling")]
    public bool excludeFromVisibilityRangeChecks = false;
    [Tooltip("Load in at this range")]
    [ShowIf("spawnable")]
    [DisableIf("excludeFromVisibilityRangeChecks")]
    public ObjectPoolingController.ObjectLoadRange spawnRange = ObjectPoolingController.ObjectLoadRange.medium;

    [Header("Scene Capture")]
    [Tooltip("If true include in any scene capturing. If false the object will be hidden. Toggle for any static or integrated objects.")]
    public bool showWorldObjectInSceneCapture = false;
    [EnableIf("showWorldObjectInSceneCapture")]
    [Tooltip("If true, capture and set the state of this object for captures")]
    public bool captureStateInSceneCapture = false;
    [DisableIf("showWorldObjectInSceneCapture")]
    public bool createProxy = false;
    [ShowIf("createProxy")]
    public bool onlyCreateProxyInDetailedCapture = true;
    [ShowIf("createProxy")]
    public ObjectPoolingController.ObjectLoadRange createProxyAtRange = ObjectPoolingController.ObjectLoadRange.veryClose;

    [Header("Colour")]
    [Tooltip("If true the same material colours will be shared over all instances of this furniture for the room. Does not apply to integrated interactables which will be coloured by their parent furniture.")]
    public bool inheritColouringFromDecor = false;
    [ShowIf("inheritColouringFromDecor")]
    [Tooltip("If true the same material colours will be shared over all instances of this furniture for the room. Difference from furniture: This cannot 'create' a material key, so furniture with it must already exist in the room.")]
    public FurniturePreset.ShareColours shareColoursWithFurniture = FurniturePreset.ShareColours.none;
    [Tooltip("If this object needs custom colours...")]
    [HideIf("inheritColouringFromDecor")]
    public bool useOwnColourSettings = false;
    [ShowIf("useOwnColourSettings")]
    public InteractableColourSetting mainColour = InteractableColourSetting.none;
    [ShowIf("useOwnColourSettings")]
    public InteractableColourSetting customColour1 = InteractableColourSetting.none;
    [ShowIf("useOwnColourSettings")]
    public InteractableColourSetting customColour2 = InteractableColourSetting.none;
    [ShowIf("useOwnColourSettings")]
    public InteractableColourSetting customColour3 = InteractableColourSetting.none;
    [ShowIf("useOwnColourSettings")]
    public bool inheritGrubValue = false;

    public enum InteractableColourSetting { none, ownersFavColour, randomColour, randomDecorColour, syncDisk};

    [Header("Setup")]
    //[Tooltip("Search for the interactable controller in the prefab with this ID to pair to")]
    //public InteractableController.InteractableID pairToID = InteractableController.InteractableID.A;
    [Tooltip("Attempt to name this using evidence entry or preset name, if false you must set this manually.")]
    public bool autoName = true;
    [Tooltip("Include belongs to name in interactable name")]
    public bool includeBelongsTo = false;
    [Tooltip("Use a shorthand version of the name (Initial + Surname)")]
    [ShowIf("includeBelongsTo")]
    public bool useNameShorthand = false;
    [ShowIf("includeBelongsTo")]
    public bool useApartmentName = false;
    [Tooltip("Is this a light?")]
    public LightingPreset isLight = null;
    public Switch lightswitch = Switch.switchState;
    [Tooltip("If true, allows an unscrewed override state (cutsom switch 1)")]
    public bool allowUnscrewed = true;
    public bool isMainLight = false;
    [Tooltip("If true, this light is added to layer 1; the light layer for street lights")]
    public bool forceIncludeOnStreetLightLayer = false;
    [ShowAssetPreview]
    public Sprite staticImage;
    [ReadOnly]
    public Vector3 imagePos;
    [ReadOnly]
    public Vector3 imageRot;
    [ReadOnly]
    public float imageScale = 1f;
    [ReadOnly]
    public GameObject imagePrefabOverride;
    [Tooltip("Weapon selection icon override")]
    [ShowAssetPreview]
    public Sprite iconOverride;
    public ItemClass itemClass = ItemClass.misc;
    public bool allowInApartmentStorage = true;
    [EnableIf("allowInApartmentStorage")]
    public bool allowInApartmentShop = false;
    [Tooltip("If enabled, this item cannot be 'moved to storage'. Only needed for spawnable items")]
    [EnableIf("spawnable")]
    public bool disableMoveToStorage = false;
    public enum ItemClass { consumable, medical, equipment, document, misc, electronics };

    [EnableIf("allowInApartmentStorage")]
    [Tooltip("The method of placement used when the player uses the apartment editor to place this")]
    public ApartmentPlacementMode apartmentPlacementMode = ApartmentPlacementMode.physics;
    public List<FurniturePreset> mustTouchFurniture = new List<FurniturePreset>();

    public enum ApartmentPlacementMode { physics, vertical, ceiling}; 

    [Space(7)]
    public bool useMaterialOverride = false;
    [EnableIf("useMaterialOverride")]
    public AudioController.SoundMaterialOverride materialOverride;

    [Header("Interaction")]
    [Tooltip("Setup of actions able to be performed")]
    public List<InteractableActionsPreset> actionsPreset;
    [Tooltip("Illegal actions are only classed as illegal if the item is in a non-public space")]
    public bool onlyIllegalIfInNonPublic = true;
    [Tooltip("This modifier will be added to the interactable distance")]
    public float rangeModifier = 0f;

    [Header("Physics")]
    public PhysicsProfile physicsProfile;
    public bool overrideMass = false;
    public bool forcePhysicsAlwaysOn = false;
    [Tooltip("If true this object will react with doors, damage impacts etc")]
    public bool reactWithExternalStimuli = false;
    [ShowIf("overrideMass")]
    public float mass = 1f;
    public bool breakable = false;
    [EnableIf("breakable")]
    public ParticleEffect particleProfile;

    [EnableIf("breakable")]
    public bool overrideShatterSettings = false;
    [EnableIf("overrideShatterSettings")]
    [Tooltip("The size of the shards created")]
    public Vector3 shardSize = new Vector3(0.025f, 0.025f, 0.025f);
    [EnableIf("overrideShatterSettings")]
    [Tooltip("Create a shard every this amount of pixels on the texture")]
    public int shardEveryXPixels = 64;

    [EnableIf("breakable")]
    public bool overrideSpatterSettings = false;
    [EnableIf("overrideSpatterSettings")]
    public SpatterPatternPreset spatterSimulation;
    [EnableIf("overrideSpatterSettings")]
    public float spatterCountMultiplier = 1f;

    [Header("Hiding Place Override")]
    [Tooltip("Use this to override the hiding place camera settings in the furniture preset")]
    public bool overrideFurnitureSetting = false;
    [ShowIf("overrideFurnitureSetting")]
    public PlayerTransitionPreset enterTransition;
    [ShowIf("overrideFurnitureSetting")]
    public PlayerTransitionPreset exitTransition;
    [ShowIf("overrideFurnitureSetting")]
    public PlayerTransitionPreset enterTransition2;
    [ShowIf("overrideFurnitureSetting")]
    public PlayerTransitionPreset exitTransition2;

    [Header("Trigger Sounds")]
    [Tooltip("Trigger audio on these switch events")]
    public List<IfSwitchStateSFX> switchSFX = new List<IfSwitchStateSFX>();

    [Header("Starting States")]
    [Tooltip("Set the switch state to this on start")]
    public bool startingSwitchState = false;
    [Tooltip("Set the switch state to this on start")]
    public bool startingCustomState1 = false;
    [Tooltip("Set the switch state to this on start")]
    public bool startingCustomState2 = false;
    [Tooltip("Set the switch state to this on start")]
    public bool startingCustomState3 = false;
    [Tooltip("Set the lock state to this on start")]
    public bool startingLockState = false;

    [Header("Value")]
    [Tooltip("Monetary value of this object. Min/Max.")]
    [MinMaxSlider(0, 10000)]
    public Vector2 value = new Vector2(1, 1);

    [Header("AI")]
    [Tooltip("AI will rank actions by this if there are multiple copies")]
    [Range(0, 10)]
    public int AIPriority = 5;
    [Tooltip("Is this incompatible for social gatherings? Ie if I'm meeting someone here...")]
    public bool disableForSocialGroups = false;
    [Tooltip("When chosing between interactables, how much to factor in the closest one?")]
    public float pickDistanceMultiplier = 1f;
    [Tooltip("Use unique settings per action for each of the following")]
    public List<AIUsePriority> perActionPrioritySettings = new List<AIUsePriority>();

    [System.Serializable]
    public class AIUsePriority
    {
        public List<AIActionPreset> actions;
        [Tooltip("AI will rank actions by this if there are multiple copies")]
        [Range(0, 10)]
        public float AIPriority = 5;
        [Tooltip("When chosing between interactables, how much to factor in the closest one?")]
        public float pickDistanceMultiplier = 1f;
    }

    [Tooltip("Will the AI notice if this is moved?")]
    public bool tamperEnabled = true;

    [Space(7)]
    [Tooltip("The AI will move to one of these postions to use this")]
    public AIUseSetting useSetting;

    [Header("Reading")]
    [Tooltip("If within reading range then display text contained in this evidence")]
    public bool readingEnabled = false;
    [ShowIf("readingEnabled")]
    [Tooltip("Reading mode is only active while switch status is true")]
    public bool readyingEnabledOnlyWithSwitchIsTue = false;
    [ShowIf("readingEnabled")]
    [Tooltip("Reading mode is only active while switch status is true")]
    public bool readingEnabledOnlyWithKaizenSkill = false;
    public enum ReadingModeSource { evidenceNote, multipageEvidence, time, bookPreset, recordPreset, syncDiskPreset, mainEvidenceText, kaizenSkillDisplay};
    [ShowIf("readingEnabled")]
    [Tooltip("Where to pull the text info from")]
    public ReadingModeSource readingSource = ReadingModeSource.mainEvidenceText;
    [ShowIf("readingEnabled")]
    [Tooltip("Discover evidence upon read")]
    public bool discoverOnRead = false;
    [ShowIf("readingEnabled")]
    [Tooltip("A delay to reading when a page is turned")]
    public float pageTurnReadingDelay = 0f;

    [Header("Distance Recognition")]
    [Tooltip("If within a certain range, then display a grey-ed out interaction icon with name text")]
    public bool distanceRecognitionEnabled = false;
    public bool distanceRecognitionOnly = false;
    public float recognitionRange = 5f;

    [Header("Placement")]
    [Tooltip("Spawn this object using this sub object group")]
    public List<SubObjectClassPreset> subObjectClasses = new List<SubObjectClassPreset>();
    [Tooltip("If the object fails to be placed in the above, use this class as a fall-back placement option. This is irrelevent for auto placement, as objects are spawned by the individual placements upon furniture, these places won't be considered.")]
    public List<SubObjectClassPreset> backupClasses = new List<SubObjectClassPreset>();
    public enum AutoPlacement { always, onlyInCompany, onlyInHomes, onlyOnStreet, never};

    [Space(5)]
    [Tooltip("Whether this will be automatically placed along with furniture...")]
    public AutoPlacement autoPlacement = AutoPlacement.always;

    [Header("...Per Game Location")]
    [Tooltip("If true, these objects will be placed with no owners at every gamelocation (based on other filters in this section).")]
    public bool alwaysPlaceAtGameLocation = false;
    [Tooltip("The minimum number of objects that will be auto-placed at every gamelocation")]
    [Range(0, 20)]
    [ShowIf("alwaysPlaceAtGameLocation")]
    public int frequencyPerGamelocationMin = 0;
    [Tooltip("The minimum number of objects that will be auto-placed at every gamelocation")]
    [Range(0, 20)]
    [ShowIf("alwaysPlaceAtGameLocation")]
    public int frequencyPerGameLocationMax = 0;
    [Tooltip("Dictates in what order objects should be placed in...")]
    [Range(0, 10)]
    [ShowIf("alwaysPlaceAtGameLocation")]
    public int perGameLocationObjectPriority = 0;

    [Header("...Per Owner")]
    [Tooltip("If true, owners/inhabitants/employees will be scanned for these traits and items will be placed accordingly...")]
    public bool placeIfFiltersPresentInOwner = false;
    [Tooltip("Place if this is the citizen's home")]
    [ShowIf("placeIfFiltersPresentInOwner")]
    public bool placeAtHome = true;
    [Tooltip("Place if this is the citizen's place of work")]
    [ShowIf("placeIfFiltersPresentInOwner")]
    public bool placeAtWork = false;
    public List<TraitPick> traitModifiers = new List<TraitPick>();
    [Tooltip("The minimum number of objects that will be auto-placed for each owner")]
    [Range(0, 20)]
    [ShowIf("placeIfFiltersPresentInOwner")]
    public int frequencyPerOwnerMin = 0;
    [Tooltip("The minimum number of objects that will be auto-placed for each owner")]
    [Range(0, 20)]
    [ShowIf("placeIfFiltersPresentInOwner")]
    public int frequencyPerOwnerMax = 0;
    [Tooltip("If true, the overall frequency range will be multiplied by the inverse of conscientiousness (untidy = more)")]
    [ShowIf("placeIfFiltersPresentInOwner")]
    public bool multiplyByMessiness = false;
    [Tooltip("Dictates in what order objects should be placed in...")]
    [Range(0, 10)]
    [ShowIf("placeIfFiltersPresentInOwner")]
    public int perOwnerObjectPriority = 0;
    [ShowIf("placeIfFiltersPresentInOwner")]
    public EvidencePreset.BelongsToSetting writerIs = EvidencePreset.BelongsToSetting.self;
    [ShowIf("placeIfFiltersPresentInOwner")]
    public EvidencePreset.BelongsToSetting receiverIs = EvidencePreset.BelongsToSetting.self;
    [ShowIf("placeIfFiltersPresentInOwner")]
    [Tooltip("If the above two options are different, is this allowed to be from the same person to the same person?")]
    public bool canBeFromSelf = false;

    [System.Serializable]
    public class TraitPick
    {
        public CharacterTrait.RuleType rule = CharacterTrait.RuleType.ifAnyOfThese;
        public List<CharacterTrait> traitList = new List<CharacterTrait>();

        [Tooltip("If this isn't true then it won't be picked for application at all.")]
        public bool mustPassForApplication = true;
        [Range(0, 20)]
        [Tooltip("If the rules match, then apply this frequency")]
        public int appliedFrequencyMin = 0;
        [Range(0, 20)]
        [Tooltip("If the rules match, then apply this frequency")]
        public int appliedFrequencyMax = 0;
    }

    [Header("Placement Limits")]
    public bool limitPerObject = false;
    [Tooltip("How many of these objects can be spawned per object?")]
    [ShowIf("limitPerObject")]
    public int perObjectLimit = 1;

    public bool limitPerRoom = false;
    [Tooltip("How many of these objects can be spawned per room?")]
    [ShowIf("limitPerRoom")]
    public int perRoomLimit = 99;

    public bool limitPerAddress = false;
    [Tooltip("How many of these objects can be spawned per address?")]
    [ShowIf("limitPerAddress")]
    public int perAddressLimit = 99;

    public bool limitInResidential = false;
    [Tooltip("How many of these objects can be spawned if residential?")]
    [ShowIf("limitInResidential")]
    public int perResidentialLimit = 1;

    public bool limitInCommercial = false;
    [Tooltip("How many of these objects can be spawned if residential?")]
    [ShowIf("limitInCommercial")]
    public int perCommercialLimit = 1;

    [Tooltip("Ban this item from being placed in certain room types")]
    [HideIf("limitToCertainRooms")]
    public List<RoomConfiguration> banFromRooms = new List<RoomConfiguration>();

    [Tooltip("Only feature this item in certain room types")]
    public bool limitToCertainRooms = false;
    [ShowIf("limitToCertainRooms")]
    public List<RoomConfiguration> onlyInRooms = new List<RoomConfiguration>();

    [Tooltip("Only feature this item in certain building types")]
    public bool limitToCertainBuildings = false;
    [ShowIf("limitToCertainBuildings")]
    public List<BuildingPreset> onlyInBuildings = new List<BuildingPreset>();

    [Space(7)]
    [Tooltip("If this is not null, it will attempt to place this evidence inside a folder matching this evidence type.")]
    public EvidencePreset attemptToStoreInFolder;
    [Tooltip("If the above is not null, the chance of being placed in the folder.")]
    [Range(0f, 1f)]
    public float folderPlacementChance = 1f;
    [Tooltip("If unable to place in folder, then don't place at all")]
    public bool dontPlaceIfNoFolder = false;
    [Tooltip("Folder's ownership must match")]
    public bool folderOwnershipMustMatch = false;
    [Tooltip("If true this will also look to spawn upon on other objects (and prioritize them)")]
    public bool useSubSpawning = false;
    [Tooltip("This will try to be placed in a place of security matching this, if not higher...")]
    [Range(0, 3)]
    public int securityLevel = 0;
    public enum OwnedPlacementRule { nonOwnedOnly, ownedOnly, prioritiseNonOwned, prioritiseOwned, both };
    [Tooltip("Rules about being placed in owned vs non-owned locations. 'Prioritise' settings will favour owned locations but sill place in non-owned, while 'only' settings will only place in that location.")]
    public OwnedPlacementRule ownedRule = OwnedPlacementRule.both;
    [Tooltip("Override with ownedOnly if at work")]
    public bool overrideWithOnlyOwnedSpawnAtWork = false;

    [Space(7)]
    [Tooltip("Can sub spawn objects with this class")]
    public SubObjectClassPreset subSpawnClass;
    [Tooltip("Sub spawning slots within this")]
    public List<SubSpawnSlot> subSpawnPositions = new List<SubSpawnSlot>();

    [Header("Relocation")]
    [Tooltip("If the object is moved by this person, also set the spawn point so it doesn't get reset.")]
    public RelocationAuthority relocationAuthority = RelocationAuthority.AIAndOwnersCanRelocate;
    [Tooltip("Will not reset if placed in the player's home")]
    public bool relocateIfPlacedInPlayersHome = true;
    [Tooltip("AI will attempt to put back this if it is out of place")]
    public bool AIWillCorrectPosition = true;

    public enum RelocationAuthority { AIAndOwnersCanRelocate, ownerCanRelocate, anyoneCanRelocate, nooneCanRelocate };

    [Header("Evidence")]
    [Tooltip("Does this interactable need to reference a piece of evidence? If true will attempt to find the evidence as below (will be overriden by passed variabes in the constructor)")]
    public bool useEvidence = false;
    [ShowIf("useEvidence")]
    [Tooltip("If not null, will attempt to find the singleton using this preset...")]
    public EvidencePreset useSingleton;
    [ShowIf("useEvidence")]
    [Tooltip("Use a specific evidence from below")]
    public FindEvidence findEvidence = FindEvidence.none;
    public enum FindEvidence { none, residentsContract, sideJob, companyRoster, addressKey, businessCard, namePlacard, photo, calendar, retailItem, workID, salesRecords, diary, menu, homeFile, birthCertificate, bankStatement, medicalDetails, IDCard, addressBook, residentRoster, telephone, callLogs, hospitalBed};
    [Tooltip("Create an evidence class of below")]
    public EvidencePreset spawnEvidence;
    [ShowIf("useEvidence")]
    [Tooltip("On create evidence: Use the item's location as evidence parent")]
    public bool locationIsParent = true;
    [Tooltip("Use this DDS message ID for the summary")]
    public string summaryMessageSource;

    [Space(7)]
    public bool overrideEvidencePhotoSettings = false;
    [ShowIf("overrideEvidencePhotoSettings")]
    public Vector3 relativeCamPhotoPos = new Vector3(-0.075f, 0.25f, 0.2f);
    [ShowIf("overrideEvidencePhotoSettings")]
    public Vector3 relativeCamPhotoEuler = new Vector3(45, 155, 0);

    [Header("Locks")]
    public InteractablePreset includeLock;
    public Vector3 lockOffset = Vector3.zero;
    [Tooltip("Preferred password source")]
    public RoomConfiguration.RoomPasswordPreference passwordSource = RoomConfiguration.RoomPasswordPreference.interactableBelongsTo;
    [Tooltip("Play this when attempted to open while locked")]
    public AudioEvent attemptedOpenSound;
    [Tooltip("The lock is armed when the door movement is closed")]
    public bool armLockOnClose = true;
    [Tooltip("If this isn't an actual door, this is the lock strength range...")]
    public Vector2 lockStrength = new Vector2(0.1f, 0.4f);
    [Tooltip("This object itself acts as the lock")]
    public bool isSelfLock = false;

    [Header("Material Changes")]
    public bool useMaterialChanges = false;
    [ShowIf("useMaterialChanges")]
    public Material lockOffMaterial;
    [ShowIf("useMaterialChanges")]
    public Material lockOnMaterial;

    [Header("Computer")]
    [Tooltip("Is this a computer (cruncher)?")]
    public bool isComputer = false;
    [ShowIf("isComputer")]
    [Tooltip("The boot application")]
    public CruncherAppPreset bootApp;
    [ShowIf("isComputer")]
    [Tooltip("The booted app (what this boots to)")]
    public CruncherAppPreset logInApp;
    [ShowIf("isComputer")]
    [Tooltip("The desktop app")]
    public CruncherAppPreset desktopApp;
    [ShowIf("isComputer")]
    [Tooltip("Additional apps")]
    public List<CruncherAppPreset> additionalApps = new List<CruncherAppPreset>();

    [Header("Fingerprints")]
    [Tooltip("Should there be fingerprints here?")]
    public bool fingerprintsEnabled = true;
    [ShowIf("fingerprintsEnabled")]
    [Tooltip("The source of the prints")]
    public RoomConfiguration.PrintsSource printsSource = RoomConfiguration.PrintsSource.ownersWritersReceivers;
    [ShowIf("fingerprintsEnabled")]
    [Tooltip("Fingerprint density")]
    [Range(0f, 5f)]
    public float fingerprintDensity = 3f;
    [ShowIf("fingerprintsEnabled")]
    [Tooltip("Dynamic fingerprints will be left when an AI uses this")]
    public bool enableDynamicFingerprints = true;
    [ShowIf("fingerprintsEnabled")]
    [Tooltip("Override the default fingerprint maximum")]
    public bool overrideMaxDynamicFingerprints = false;
    [ShowIf("fingerprintsEnabled")]
    [EnableIf("overrideMaxDynamicFingerprints")]
    public int maxDynamicFingerprints = 8;

    [Header("First Person Setup")]
    [Tooltip("If this is a first person item, the corresponding item ID")]
    public FirstPersonItem fpsItem = null;
    public bool isInventoryItem = false;
    [ShowIf("isInventoryItem")]
    [Tooltip("Offset of held item")]
    public Vector3 fpsItemOffset = Vector3.zero;
    [ShowIf("isInventoryItem")]
    public Vector3 fpsItemRotation = Vector3.zero;
    //[ShowIf("isInventoryItem")]
    [Tooltip("The amount of consumable; consumed at 1 per second by the player")]
    public float consumableAmount = 0f;
    [ShowIf("isInventoryItem")]
    [Tooltip("Destroy when this is all consumed")]
    public bool destroyWhenAllConsumed = false;
    [ShowIf("isInventoryItem")]
    [Tooltip("Trash object")]
    public bool useSameModelAsTrash = true;
    [ShowIf("isInventoryItem")]
    [DisableIf("useSameModelAsTrash")]
    public InteractablePreset trashItem;
    [ShowIf("isInventoryItem")]
    public AudioEvent playerConsumeLoop;
    [ShowIf("isInventoryItem")]
    public AudioEvent takeOneEvent;

    [Space(7)]
    [ShowIf("isInventoryItem")]
    [DisableIf("destroyWhenAllConsumed")]
    public Human.DisposalType disposal = Human.DisposalType.anywhere;
    [DisableIf("destroyWhenAllConsumed")]
    [Range(0f, 1f)]
    public float chanceOfDroppedAngle = 0f;
    [DisableIf("destroyWhenAllConsumed")]
    public float droppedAngleHeightBoost = 0.1f;
    [ShowIf("isInventoryItem")]
    public MurderWeaponPreset weapon;
    [ShowIf("isInventoryItem")]
    [Tooltip("If in inventory, display object")]
    public bool inventoryCarryItem = false;
    [ShowIf("isInventoryItem")]
    [Tooltip("This required a carrying animation")]
    public bool requiredCarryAnimation = true;
    [ShowIf("isInventoryItem")]
    [Tooltip("If an AI can carry this, which carrying animation to play")]
    public int aiCarryAnimation = 1;
    [ShowIf("isInventoryItem")]
    [Tooltip("position object by this when AI is holding")]
    public Vector3 aiHeldObjectPosition = new Vector3(-0.05f, -0.1f, -0.02f);
    [ShowIf("isInventoryItem")]
    [Tooltip("Rotate object by this when AI is holding")]
    public Vector3 aiHeldObjectRotation = new Vector3(90, 0, 0);
    [ShowIf("isInventoryItem")]
    [Tooltip("The AI will put this down when at home")]
    public bool putDownAtHome = false;
    [Tooltip("The AI will take this when they leave home")]
    public bool takeWith = false;
    [ShowIf("isInventoryItem")]
    public List<SubObjectClassPreset> putDownPositions = new List<SubObjectClassPreset>();
    [ShowIf("isInventoryItem")]
    public List<SubObjectClassPreset> backupPutDownPositions = new List<SubObjectClassPreset>();

    [Header("Special cases")]
    public SpecialCase specialCaseFlag = SpecialCase.none;
    public enum SpecialCase { none, sleepPosition, workDesk, workCounter, workKitchen, securityDoor, alarmSystem, sentryGun, securityCamera, interestBook, bookStack, thrownItem, fingerprint, shower, syncDisk, unused1, unused2, codebreaker, doorWedge, telephone, hospitalBed, syncBed, padlock, salesLedger, caseTray, footprint, breakerSecurity, breakerLights, breakerDoors, fridge, stovetopKettle, syncDiskUpgrade, otherSecuritySystem, gasReleaseSystem, tracker, grenade, ballisticArmour, forceStanding, lightswitch, airVent, burningBarrel, addressBook, garbageDisposal};
    [Tooltip("Affect room steam amount with switch state 1")]
    public bool affectRoomSteamLevel = false;
    [Tooltip("This is a payphone")]
    public bool isPayphone = false;
    [Tooltip("Searching this causes player to get stinky!")]
    public bool isStinky = false;
    [Tooltip("This is a clock; use hourly chimes")]
    public bool isClock = false;
    [Tooltip("If true this will be a naming special case.")]
    public bool isMoney = false;
    [Tooltip("Is this a heat source? Only active when switch 0 is on")]
    public bool isHeatSource = false;
    [Tooltip("Mark this as trash as soon as it is created, for removal as soon as possible")]
    public bool markAsTrashOnCreate = false;
    [Tooltip("If picked up, the AI will seek to put this in a bin/gets added to their carrying trash")]
    public bool isLitter = false;
    [Tooltip("Will require an art asset sent to a decal projector")]
    public bool isDecal = false;
    [Tooltip("Resets switch states to starting configuration after x amount of time")]
    public bool resetSwitchStates = false;
    [EnableIf("resetSwitchStates")]
    public float resetTimer = 0.01f;
    [Tooltip("Don't save switch states")]
    public bool dontSaveSwitchStates = false;
    [Tooltip("Don't load switch states")]
    public bool dontLoadSwitchStates = false;
    [Tooltip("If this is a music player: Track list")]
    public List<AudioEvent> musicTracks = new List<AudioEvent>();
    [Tooltip("Is this a retailItem? If so here's the reference. This is set by having a RetailItem Preset that points to this.")]
    public RetailItemPreset retailItem;
    [Tooltip("If this is associated with a shop interface, override the location's menu with this one (useful for vending machines)")]
    public MenuPreset menuOverride;
    [ShowIf("isClock")]
    public AudioEvent hourlyChime;
    [ShowIf("isClock")]
    [Tooltip("Do as many chimes as the hour dictates")]
    public bool chimeEqualToHour = false;
    [ShowIf("isClock")]
    [Tooltip("Delay between chimes if above is true")]
    public float chimeDelay = 1.5f;
    [Tooltip("Audio loop played on search")]
    public AudioEvent searchLoop;

    [Header("Debug")]
    public InteractablePreset copyFrom;

    [System.Serializable]
    public class SubSpawnSlot
    {
        public Vector3 localPos;
        public Vector3 localEuler;
    }

    public List<InteractionAction> GetActions(int lockedInPhase = 0)
    {
        List<InteractionAction> ret = new List<InteractionAction>();

        foreach (InteractableActionsPreset p in actionsPreset)
        {
            if (p == null) Game.Log(presetName + " has missing actions!");
            ret.AddRange(p.actions);

            if(lockedInPhase == 3)
            {
                ret.AddRange(p.physicsActions);
            }
            else
            {
                if (lockedInPhase >= 1) ret.AddRange(p.lockedInActions1);
                if (lockedInPhase >= 2) ret.AddRange(p.lockedInActions2);
            }
        }

        return ret;
    }

    public PhysicsProfile GetPhysicsProfile()
    {
        if (physicsProfile != null) return physicsProfile;

        return GameplayControls.Instance.defaultObjectPhysicsProfile;
    }

    [Button]
    public void CopyFPSHeldPostionFromTransform()
    {
#if UNITY_EDITOR

        if (FirstPersonItemController.Instance.rightHandObjectParent != null && FirstPersonItemController.Instance.rightHandObjectParent.childCount > 0)
        {


            fpsItemOffset = FirstPersonItemController.Instance.rightHandObjectParent.GetChild(0).localPosition;
            fpsItemRotation = FirstPersonItemController.Instance.rightHandObjectParent.GetChild(0).localEulerAngles;

            //Force save data
            UnityEditor.EditorUtility.SetDirty(this);
        }
#endif
    }

    [Button]
    public void CalculateDroppedAngleHeightBoost()
    {
#if UNITY_EDITOR
        if (prefab != null)
        {
            MeshRenderer c = prefab.GetComponentInChildren<MeshRenderer>();

            if (c != null)
            {
                droppedAngleHeightBoost = c.bounds.size.x * 0.5f;

                //Force save data
                UnityEditor.EditorUtility.SetDirty(this);
            }
            else Debug.Log("Missing Collider!");
        }
        else Debug.Log("Missing prefab!");
#endif
    }

    [Button]
    public void SpawnIntoInventory()
    {
        if(isInventoryItem)
        {
            if (!FirstPersonItemController.Instance.IsSlotAvailable())
            {
                PopupMessageController.Instance.PopupMessage("dropitem", true, false, "Confirm", anyButtonClosesMsg: true);
            }
            //Go ahead with purchase...
            else
            {
                Interactable newObj = InteractableCreator.Instance.CreateWorldInteractable(this, Player.Instance, Player.Instance, null, Player.Instance.transform.position, Player.Instance.transform.eulerAngles, null, null);

                //Execute purchase
                if (newObj != null)
                {
                    newObj.SetSpawnPositionRelevent(false); //Set spawn to irrelevent so there's no 'put back' action

                    if (FirstPersonItemController.Instance.PickUpItem(newObj, false, false))
                    {
                        newObj.MarkAsTrash(true); //Remove this once we can
                    }
                    else newObj.Delete();
                }
            }
        }
    }
}
