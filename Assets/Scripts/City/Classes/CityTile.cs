﻿using System;
using System.Collections.Generic;
using UnityEngine;

//Data container for the game city's grid layout
//Script pass 1
//[System.Serializable]
public class CityTile : Controller, IComparable<CityTile>
{
    [Header("Location")]
    public Vector2Int cityCoord; //Coordinate relative to other city tiles
    public DistrictController district; //The district this is part of
    public int districtID = -1; //Used on load only
	public BlockController block; //The block this is part of
    public int blockID = -1; //Used on load only
	public NewBuilding building; //Building present on the tile
    public List<NewTile> outsideTiles = new List<NewTile>(); //List of outside street tiles
    public bool isInPlayerVicinity = false; //True if this tile is within 1 tile of the player
    public bool playerPresent = false; //True if the player is in this building's groumap tile

    [Header("Details")]
    public BuildingPreset.Density density = BuildingPreset.Density.medium; //Density (how tall are the buildings)
    public BuildingPreset.LandValue landValue = BuildingPreset.LandValue.medium; //Land value

    public void Setup(Vector2Int newCoord)
	{
		cityCoord = newCoord;

        //Add to dictionary
        CityData.Instance.cityTiles.Add(cityCoord, this);

        //Position object in game world
        //thistotaltiles - totaltiles/2 + halftile
        float loadPosX = cityCoord.x * CityControls.Instance.cityTileSize.x - ((CityData.Instance.citySize.x * 0.5f) * CityControls.Instance.cityTileSize.x) + (CityControls.Instance.cityTileSize.x * 0.5f);
        float loadPosY = cityCoord.y * CityControls.Instance.cityTileSize.y - ((CityData.Instance.citySize.y * 0.5f) * CityControls.Instance.cityTileSize.y) + (CityControls.Instance.cityTileSize.y * 0.5f);
        this.transform.position = new Vector3(loadPosX, 0, loadPosY);

        name = "CityTile " + Mathf.RoundToInt(cityCoord.x) + "," + Mathf.RoundToInt(cityCoord.y);
        this.transform.name = name;
    }

    public void LoadTileOnly(CitySaveData.CityTileCitySave data)
    {
        name = data.name;
        cityCoord = data.cityCoord;

        //District & blocks haven't been created yet, so store their ID here
        districtID = data.districtID;
        blockID = data.blockID;

        density = data.density;
        landValue = data.landValue;

        //Add to dictionary
        CityData.Instance.cityTiles.Add(cityCoord, this);

        //Position object in game world
        //thistotaltiles - totaltiles/2 + halftile
        float loadPosX = cityCoord.x * CityControls.Instance.cityTileSize.x - ((CityData.Instance.citySize.x * 0.5f) * CityControls.Instance.cityTileSize.x) + (CityControls.Instance.cityTileSize.x * 0.5f);
        float loadPosY = cityCoord.y * CityControls.Instance.cityTileSize.y - ((CityData.Instance.citySize.y * 0.5f) * CityControls.Instance.cityTileSize.y) + (CityControls.Instance.cityTileSize.y * 0.5f);
        this.transform.position = new Vector3(loadPosX, 0, loadPosY);
    }

    public void SetDensity(BuildingPreset.Density newDensity)
    {
        density = newDensity;

        if (district != null)
        {
            density = (BuildingPreset.Density)Mathf.Clamp((int)density, (int)district.preset.minimumDensity, (int)district.preset.maximumDensity);
        }
    }

    public void SetLandVlaue(BuildingPreset.LandValue newLandvalue)
    {
        landValue = newLandvalue;

        if(district != null)
        {
            landValue = (BuildingPreset.LandValue)Mathf.Clamp((int)landValue, (int)district.preset.minimumLandValue, (int)district.preset.maximumLandValue);
        }
    }

    public void AddOutsideTile(NewTile newTile)
    {
        if(!outsideTiles.Contains(newTile))
        {
            newTile.cityTile = this;
            outsideTiles.Add(newTile);
        }
    }

    // Default comparer (average land value)
    public int CompareTo(CityTile compare)
	{
		return this.landValue.CompareTo(compare.landValue);
	}

    //Set player present within 1 groundmap tile
    public void SetPlayerInVicinity(bool val)
    {
        isInPlayerVicinity = val;
    }

    //Set player present on this groundmap tile: Use this for lights & detail
    public void SetPlayerPresentOnGroundmap(bool val)
    {
        playerPresent = val;

        //If not present, set all lights to no shadows: This used to be when on the same groundmap
        if(building != null)
        {
            if (Game.Instance.noShadowsWhenPlayerIsInDifferentGoundmapLocation)
            {
                for (int i = 0; i < building.allInteriorMainLights.Count; i++)
                {
                    if (playerPresent)
                    {
                        try
                        {
                            if(building.allInteriorMainLights[i].hdrpLightData != null) building.allInteriorMainLights[i].hdrpLightData.EnableShadows(true);
                            if(building.allInteriorMainLights[i].lightComponent != null) building.allInteriorMainLights[i].lightComponent.shadows = LightShadows.Hard;
                        }
                        catch
                        {

                        }
                    }
                    else
                    {
                        try
                        {
                            if(building.allInteriorMainLights[i].hdrpLightData != null) building.allInteriorMainLights[i].hdrpLightData.EnableShadows(false);
                            if(building.allInteriorMainLights[i].lightComponent != null) building.allInteriorMainLights[i].lightComponent.shadows = LightShadows.None;
                        }
                        catch
                        {

                        }
                    }
                }
            }
        }
    }

    //Create save data
    public CitySaveData.CityTileCitySave GenerateSaveData()
    {
        CitySaveData.CityTileCitySave output = new CitySaveData.CityTileCitySave();
        output.name = name;
        output.cityCoord = cityCoord;
        output.density = density;
        output.landValue = landValue;

        //Get building
        output.building = building.GenerateSaveData();

        //Get outside tiles
        foreach(NewTile tile in outsideTiles)
        {
            output.outsideTiles.Add(tile.GenerateSaveData());
        }

        //Save reference to block and district ID
        output.blockID = block.blockID;
        output.districtID = district.districtID;

        return output;
    }
}
