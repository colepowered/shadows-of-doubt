﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using NaughtyAttributes;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEditor;

public class Actor : Controller
{
    [Header("Flags")]
    //Status Flags
    public bool isMoving = false; //True if moving
    public bool isRunning = false; //True if running
    public bool isMachine = false; //If this is a machine class (eg cctv)
    public bool isPlayer = false; //Is this the player?
    public bool isAsleep = false; //Sleeping (>= Human)
    public bool isDelayed = false; //If delayed, citizen does not move for x seconds
    public bool isStunned = false; //If stunned, citizen cannot respond to any input stimuli
    public bool isDead = false; //Dead (>= Human)
    public bool unreportable = false; //If true, other AI won't see/report this if dead or KO'd
    public bool isTrespassing = false; //True if in an illegal location
    public bool isOnStreet = false; //If on street, this citizen is eligable to be seen by others on the street using a raycast check.
    public bool seesOthers = true; //Does this see others?
    public bool isSeenByOthers = true; //Do others capture memories of this?
    public bool canListen = true; //Can hear sounds
    public bool visible = true; //Is drawn by the renderer
    public bool isHome = false; //Actively updated bool showing if this human is home
    public bool isAtWork = false; //Actively updated bool showing if this human is at work
    public bool inAirVent = false; //If true this actor is in an air vent
    public bool isHiding = false; //True if this is in a hiding place
    public bool isInBed = false; //True if in bed
    public bool inConversation = false; //True if in conversation
    public bool isSpeaking = false; //True if this actor has as speech bubble present
    public bool isHomeless = false; //True if this actor has no home
    public bool isLitterBug = false;
    public bool isOnDuty = false;
    public bool isEnforcer = false;
    public bool ownsUmbrella = true;
    public bool likesTheRain = false;
    public bool forceTarget = false; //If true other AI see this as a threat...

    [Header("Illegal State")]
    [ProgressBar("Sees Player", 100, EColor.Blue)]
    public int debugSeesPlayer = 0;
    [ReadOnly]
    public float debugLastSeesPlayerChange = 0f;
    [Space(5)]

    public Dictionary<Actor, float> seesIllegal = new Dictionary<Actor, float>(); //Sees illegal along with focus progress...
    public HashSet<Actor> seenIllegalThisCheck = new HashSet<Actor>(); //Sees illegal in the latest check only
    public HashSet<Actor> witnessesToIllegalActivity = new HashSet<Actor>(); //Witnesses of this...
    public HashSet<Actor> persuedBy = new HashSet<Actor>(); //Triggered once persue is active, remove once investigate is inactive
    public bool illegalActionActive = false;
    public bool illegalAreaActive = false;
    public int trespassingEscalation = 0; //How severe trespassing is
    public bool illegalStatus = false;

    [Space(7)]
    [Tooltip("Transform that should be in the centre of this object, when others look at this, they will use this.")]
    public Transform lookAtThisTransform;
    public Transform aimTransform;

    [Header("Common Components")]
    public GameObject modelParent;
    public GameObject distantLOD;
    public List<MeshRenderer> meshes = new List<MeshRenderer>();
    public List<MeshRenderer> meshesLOD1 = new List<MeshRenderer>();
    public CitizenAnimationController animationController;
    public SpeechController speechController;
    public Transform neckTransform;
    public InteractablePreset citizenObjectPreset;
    [System.NonSerialized]
    public Interactable interactable;
    [System.NonSerialized]
    public Interactable leftHandInteractable;
    [System.NonSerialized]
    public Interactable rightHandInteractable;
    public InteractableController interactableController;
    public NewAIController ai;
    public Transform footstepSoundTransform;
    public OutlineController outline;

    [Header("Health")]
    [ReadOnly]
    public float currentHealth = 1f;
    [ReadOnly]
    public float maximumHealth = 1f;
    [ReadOnly]
    public float currentHealthNormalized = 1f; //Health on a scale of 0 - 1
    [ReadOnly]
    public float recoveryRate = 0.5f; //Recovers this health (normalized) over a period of 1 in-game hour

    [Header("Combat")]
    [ReadOnly]
    public float combatSkill = 1f; //How fast this citizen attacks
    [ReadOnly]
    public float combatHeft = 0.1f; //How much damage this citizen does per hit
    [ReadOnly]
    public float currentNerve = 0.1f; //When an AI will switch tactics to running away (when at 0)
    [ReadOnly]
    public Actor lastScaredBy;
    [ReadOnly]
    public NewGameLocation lastScaredAt;
    [ReadOnly]
    public float maxNerve = 0.5f; //Nerve resets to this over time

    [Header("Location Data")]
    [System.NonSerialized]
    public CityTile previousCityTile;
    [System.NonSerialized]
    public CityTile currentCityTile;
    //[Space(4)]
    [System.NonSerialized]
    public NewTile previousTile;
    [System.NonSerialized]
    public NewTile currentTile;
    //[Space(4)]
    [System.NonSerialized]
    public NewBuilding previousBuilding;
    [System.NonSerialized]
    public NewBuilding currentBuilding;
    //[Space(4)]
    [System.NonSerialized]
    public NewGameLocation previousGameLocation;
    [System.NonSerialized]
    public NewGameLocation currentGameLocation;
    [Space(4)]
    [System.NonSerialized]
    public NewRoom previousRoom;
    public NewRoom currentRoom;
    [Space(4)]
    [System.NonSerialized]
    public AirDuctGroup currentDuct;
    [System.NonSerialized]
    public AirDuctGroup previousDuct;
    [Space(4)]
    public NewNode.NodeSpace currentNodeSpace;
    public HashSet<NewNode.NodeSpace> reservedNodeSpace = new HashSet<NewNode.NodeSpace>();
    [Space(4)]
    public NewNode debugPrevNode2;
    public NewNode debugPrevNode1;
    public NewNode previousNode;
    public NewNode currentNode;
    [System.NonSerialized]
    public Vector3Int currentNodeCoord;

    [Space(7)]
    public List<NewDoor> keyring = new List<NewDoor>();

    //Evidence
    [System.NonSerialized]
    public EvidenceWitness evidenceEntry;

    [Header("Vision")]
    //public List<Actor> currentVisibleTargets = new List<Actor>(); //List of citizens that this can see
    [ReadOnly]
    public float currentLightLevel = 1f; //Driven by a camera render texture system for player- dynamic level of light applied to this character.
    [System.NonSerialized]
    private float lightLevelTransition = 1f; //A smoothed value of the above for use with visibility calculation
    [ReadOnly]
    public bool stealthMode = false; //True if using stealth mode
    [ReadOnly]
    public bool isCrouched = false; //True if crouched
    [ReadOnly]
    public float appliedStealth = 0f; //Stealth amount active
    [ReadOnly]
    private float currentVisibilityPotential = 1f; //Base value to subtract from when calculating visibility
    [ReadOnly]
    public float overallVisibility = 1f; //Accumulation value for visibility
    [ReadOnly]
    public float stealthDistance = 20f; //The distance from which this actor can be spotted.
    [ReadOnly]
    public int escalationLevel = 0; //If this reaches 2 then use combat
    public float timeOfLastSightCheck = 0f; //For getting interval between sight checks...
    //public float playerAlertFocus = 0f;
    [System.NonSerialized]
    public float spottedState = 0f; //1 if the player has spotted this actor. Active if player is illegal
    [System.NonSerialized]
    public float spottedGraceTime = 0f; //How long the player can see them at the '1' spotted state...

    [Space(5)]
    public float spotFocusSpeedMultiplier = 1f; //How fast a persuit target is acquired.
    public float spotLoseFocusSpeedMultiplier = 1f; //How fast a persuit target is lost.
    public float hearingMultiplier = 1f; //How well this AI can hear

    public HashSet<NewGameLocation> locationsOfAuthority = new HashSet<NewGameLocation>(); //Locations in addition to public places where I can investigate strange sounds etc.

    [Header("Interaction")]
    [System.NonSerialized]
    public Interactable interactingWith;

    [Header("Inventory")]
    public List<Interactable> inventory = new List<Interactable>();

    [Header("Skill Variables")]
    public float stealthSkill = 1f;

    //Sleeping
    [System.NonSerialized]
    public float sleepDepth = 0f; //How deep of a sleep this person is in. Increases closest to #
    [System.NonSerialized]
    public int awakenPromt = 0; //Will eventually cause the actor to awaken
    [System.NonSerialized]
    public float awakenRegen = 0f;

    public enum HumanDebug { movement, actions, attacks, updates, misc, sight};

    //Events
    //Triggered when a memory by this citizen is discovered and needs to be included on timlines

    //Triggered when interaction is changed
    public delegate void InteractionChanged();
    public event InteractionChanged OnInteractionChanged;

    //Triggered on new routine
    public delegate void RoutineChanged();
    public event RoutineChanged OnRoutineChange;

    public void SetInteracting(Interactable other)
    {
        if (interactingWith != other)
        {
            interactingWith = other;

#if UNITY_EDITOR
            if (UnityEditor.Selection.Contains(this.gameObject))
            {
                Game.Log("Set interaction: " + other);
            }
#endif

            if (other != null)
            {
                //Is the interactable another citizen?
                if (other.objectRef != null)
                {
                    Actor otherActor = other.objectRef as Actor;

                    if (otherActor != null)
                    {
                        if (this as Citizen != null)
                        {
                            if ((this as Citizen).ai != null) (this as Citizen).ai.UpdateTrackedTargets();
                        }
                    }
                }

                OnNewInteraction();
            }
            else
            {
                if (ai != null)
                {
                    ai.faceTransform = null; //Reset face transform
                }
            }

            //Fire event
            if (OnInteractionChanged != null)
            {
                OnInteractionChanged();
            }
        }
    }

    //Triggered on a new interaction
    public virtual void OnNewInteraction()
    {
        //Stuff
    }

    //Physically move this object to a new node. Most commonly used for spawning. Do not generate memories upon teleport.
    public virtual void Teleport(NewNode teleportLocation, Interactable.UsagePoint usagePoint, bool cancelVent = true)
    {
        if (teleportLocation == null)
        {
            Game.LogError("Teleport location for " + name + " is null! Teleporting to random street instead...");
            Human h = this as Human;

            if(h != null)
            {
                if(h.home != null)
                {
                    Teleport(h.FindSafeTeleport(h.home), null);
                }
                else
                {
                    Teleport(h.FindSafeTeleport(CityData.Instance.streetDirectory[Toolbox.Instance.Rand(0, CityData.Instance.streetDirectory.Count)]), null);
                }
            }

            return;
        }

        if(isDead)
        {
            Game.LogError("Teleporting a dead person: Are you sure you want to do this?");
        }

        //Is this AI?
        if (ai != null)
        {
            if (ai.currentGoal != null)
            {
                //Look for existing actions at the current location
                if (currentGameLocation == null || teleportLocation.gameLocation != currentGameLocation)
                {
                    int safety = 50;

                    //Immediately-complete any current actions
                    while (ai.currentGoal.actions.Count > 0 && safety > 0)
                    {
                        ai.currentGoal.actions[0].ImmediateComplete();
                        safety--;

                        if (safety <= 0)
                        {
                            Game.LogError("While trying to teleport AI, the loop that immediately completes actions is reaching the safety limit. This means there's something creating actions as we're trying to remove them!");
                        }
                    }

                    //To be extra sure of not triggering irrelevent events, restart this goal...
                    ai.currentGoal.OnDeactivate(0f);
                }
            }
        }

        if (usagePoint != null) transform.position = usagePoint.GetUsageWorldPosition(teleportLocation.position, this);
        else
        {
            if (teleportLocation.defaultSpace != null) transform.position = teleportLocation.defaultSpace.position;
            else transform.position = teleportLocation.position;
        }

        UpdateGameLocation();

        //Game.Log("Teleported " + name + " to " + teleportLocation.gameLocation.name);
        if (Game.Instance.collectDebugData)
        {
            SelectedDebug("Teleported to " + teleportLocation.gameLocation.name, HumanDebug.movement);
            if (ai != null) ai.debugDestinationPosition.Add("Teleport: " + teleportLocation.gameLocation.name);
        }
    }

    //Use a function because the current max is affected by a multiplier
    public float GetCurrentMaxHealth()
    {
        return maximumHealth * StatusController.Instance.maxHealthMultiplier;
    }

    //Use this to update location using current game world position
    public virtual void UpdateGameLocation(float feetOffset = 0f)
    {
        if (SessionData.Instance.isFloorEdit) return;

        //Update the node
        Vector3 pos = new Vector3(this.transform.position.x, this.transform.position.y + feetOffset, this.transform.position.z);
        Vector3Int nodeKey = CityData.Instance.RealPosToNodeInt(pos);

        NewNode getNode = null;

        if (!PathFinder.Instance.nodeMap.TryGetValue(nodeKey, out getNode))
        {
            getNode = Toolbox.Instance.GetNearestGroundLevelOutside(pos);
            
            if(getNode != null)
            {

            }
            else
            {
                if (isPlayer && SessionData.Instance.startedGame) Game.Log("Unable to find node for pos " + nodeKey + " (" + PathFinder.Instance.nodeMap.Count + " nodes)");
            }
        }

        if(getNode != null)
        {
            //Change location trigger
            if (getNode != currentNode)
            {
                debugPrevNode2 = debugPrevNode1;
                debugPrevNode1 = previousNode;
                previousNode = currentNode;
                currentNode = getNode;
                currentNodeCoord = currentNode.nodeCoord;
                OnNodeChange();

                //Change tiles
                if (currentNode.tile != currentTile)
                {
                    previousTile = currentTile;
                    currentTile = currentNode.tile;

                    OnTileChange();
                }

                //Change room
                if (currentNode.room != currentRoom)
                {
                    previousRoom = currentRoom;
                    currentRoom = currentNode.room;

                    //Remove/add occupant
                    if (previousRoom != null) previousRoom.RemoveOccupant(this);
                    if (currentRoom != null) currentRoom.AddOccupant(this);

                    OnRoomChange();
                }

                //Change game location
                if (currentNode.gameLocation != currentGameLocation)
                {
                    previousGameLocation = currentGameLocation;
                    currentGameLocation = currentNode.gameLocation;

                    //Remove/add occupant
                    if (previousGameLocation != null) previousGameLocation.RemoveOccupant(this);
                    if (currentGameLocation != null) currentGameLocation.AddOccupant(this);

                    //Set on/off street
                    if (currentGameLocation.isOutside)
                    {
                        SetOnStreet(true);
                    }
                    else
                    {
                        SetOnStreet(false);
                    }

                    OnGameLocationChange();

                    //Change building
                    if (currentNode.building != currentBuilding)
                    {
                        previousBuilding = currentBuilding;
                        currentBuilding = currentNode.building;
                        OnBuildingChange();
                    }
                }

                //Change groundmap
                if (currentNode.tile.cityTile != currentCityTile)
                {
                    previousCityTile = currentCityTile;
                    currentCityTile = currentNode.tile.cityTile;
                    OnCityTileChange();
                }
            }
        }
    }

    //Location changes
    public virtual void OnCityTileChange()
    {

    }

    public virtual void OnBuildingChange()
    {
        //Set light layers
        for (int i = 0; i < meshes.Count; i++)
        {
            Toolbox.Instance.SetLightLayer(meshes[i], currentBuilding);
        }

        for (int i = 0; i < meshesLOD1.Count; i++)
        {
            Toolbox.Instance.SetLightLayer(meshesLOD1[i], currentBuilding);
        }

        //if(ai != null)
        //{
        //    if(ai.spawnedRightItem != null) Toolbox.Instance.SetLightLayer(ai.spawnedRightItem, currentBuilding);
        //    if (ai.spawnedLeftItem != null) Toolbox.Instance.SetLightLayer(ai.spawnedLeftItem, currentBuilding);
        //}
    }

    public virtual void OnTileChange()
    {

    }

    public virtual void OnGameLocationChange(bool enableSocialSightings = true, bool forceDisableLocationMemory = false)
    {
        ////If location has windows
        //if (currentGameLocation != null && currentGameLocation.thisAsAddress != null)
        //{
        //    if ((currentGameLocation.thisAsAddress.adjacentView.Count > 0) && !isAtWindowedAddress) SetAtWindowedAddress(true);
        //    else if (isAtWindowedAddress) SetAtWindowedAddress(false);
        //}
        //else if (isAtWindowedAddress) SetAtWindowedAddress(false);

        //Reset AI time spent at this lcocation
        if (ai != null)
        {
            ai.timeAtCurrentAddress = 0f;
        }
    }

    public virtual void OnNodeChange()
    {
        //#if UNITY_EDITOR
        //if (UnityEditor.Selection.Contains(this.gameObject))
        //{
        //    Game.Log("-> OnNodeChange: " + currentNode.name);
        //}
        //#endif

        //Update interactable position
        if(interactable != null)
        {
            interactable.UpdateWorldPositionAndNode(true);
        }

        //Update chase last seen positions
        //if(persuedBy.Count > 0)
        //{
        //    foreach(Actor a in persuedBy)
        //    {
        //        if(a.ai != null && a.ai.persuitTarget == this && a.ai.seesOnPersuit)
        //        {
        //            a.ai.chaseLogic.UpdateLastSeen();
        //        }
        //    }
        //}
    }

    public virtual void OnRoomChange()
    {
        //Set visibility based on room's
        if (!visible && currentRoom.isVisible)
        {
            SetVisible(true);

            //Set light layer if forced street lighting is different
            if(currentRoom.preset.forceStreetLightLayer != previousRoom.preset.forceStreetLightLayer)
            {
                //Set light layers
                for (int i = 0; i < meshes.Count; i++)
                {
                    Toolbox.Instance.SetLightLayer(meshes[i], currentBuilding);
                }

                for (int i = 0; i < meshesLOD1.Count; i++)
                {
                    Toolbox.Instance.SetLightLayer(meshesLOD1[i], currentBuilding);
                }

                //if (ai != null)
                //{
                //    if (ai.spawnedRightItem != null) Toolbox.Instance.SetLightLayer(ai.spawnedRightItem, currentBuilding);
                //    if (ai.spawnedLeftItem != null) Toolbox.Instance.SetLightLayer(ai.spawnedLeftItem, currentBuilding);
                //}
            }
        }
        else if (visible && !currentRoom.isVisible)
        {
            SetVisible(false);
        }
    }

    //Set on/off street
    public virtual void SetOnStreet(bool val)
    {
        if (!isOnStreet && val)
        {
            isOnStreet = true;
        }
        else if (isOnStreet && !val)
        {
            isOnStreet = false;
        }
    }

    //Add to keyring
    public virtual void AddToKeyring(NewAddress ad, bool gameMessage = true)
    {
        if (ad == null) return;

        Game.Log("Gameplay: Add to keyring: " + ad.name);

        //Give keys
        foreach (NewRoom room in ad.rooms)
        {
            foreach (NewNode.NodeAccess acc in room.entrances)
            {
                if (acc.wall != null && acc.wall.door != null)
                {
                    if (acc.wall.door.preset.lockType != DoorPreset.LockType.none)
                    {
                        AddToKeyring(acc.wall.door, false);
                    }
                }
                else if(acc.door != null)
                {
                    if (acc.door.preset.lockType != DoorPreset.LockType.none)
                    {
                        AddToKeyring(acc.door, false);
                    }
                }
            }
        }

        //Give mailbox keys
        if(ad.residence != null && isPlayer)
        {
            if(ad.residence.mailbox != null)
            {
                //Game.Log("Attempting to add mailbox key for " + ad.name);

                foreach(KeyValuePair<FurnitureLocation.OwnerKey, int> pair in ad.residence.mailbox.ownerMap)
                {
                    if(pair.Key.address == ad)
                    {
                        foreach(Interactable i in ad.residence.mailbox.integratedInteractables)
                        {
                            if(i.pv != null && i.pv.Exists(item => item.varType == Interactable.PassedVarType.ownedByAddress && item.value == ad.id))
                            {
                                Player.Instance.AddToKeyring(i, false);
                            }
                        }
                    }
                }
            }
        }

        //Also give key for the building's main door
        if(ad.building.mainEntrance != null)
        {
            if (ad.building.mainEntrance.door != null)
            {
                AddToKeyring(ad.building.mainEntrance.door, false);
            }
        }
        else
        {
            Game.Log("CityGen: " + ad.building.name + " features no 'main entrance', listing all entrances...");

            foreach(NewWall e in ad.building.additionalEntrances)
            {
                Game.Log("CityGen: ..." + e.id + " " + e.node.position + " (" + e.node.room.name + ", " + e.otherWall.node.room.name + ") Tile " + e.node.tile.globalTileCoord + " Entrance: " + e.node.tile.isEntrance + " main: " + e.node.tile.isMainEntrance);
            }
        }

        //Add keys to other entrances just in case
        if(ad.building != null)
        {
            foreach(NewWall wall in ad.building.additionalEntrances)
            {
                if (wall.door != null)
                {
                    if (wall.door.preset.lockType != DoorPreset.LockType.none)
                    {
                        AddToKeyring(wall.door, false);
                    }
                }
            }
        }
    }

    public virtual void AddToKeyring(NewDoor ac, bool gameMessage = true)
    {
        if (!keyring.Contains(ac))
        {
            keyring.Add(ac);
        }
    }

    public virtual void RemoveFromKeyring(NewAddress ad)
    {
        //Give keys
        foreach (NewNode.NodeAccess acc in ad.entrances)
        {
            if(acc.wall != null)
            {
                if (acc.wall.door != null)
                {
                    RemoveFromKeyring(acc.wall.door);
                }
            }
        }
    }

    public virtual void RemoveFromKeyring(NewDoor ac)
    {
        keyring.Remove(ac);
    }

    //Set visible (so citizen can be hidden when in building)
    public virtual void SetVisible(bool vis, bool force = false)
    {
        if (Player.Instance == this) return; //Ignore player
        if (vis == visible) return;
        if (!force && outline != null && outline.outlineActive && !vis) return; //Cannot turn invisible if outline is active...

        if (Game.Instance.collectDebugData) SelectedDebug("Set visible official method: " + vis + " force: " + force, HumanDebug.misc);

        if (vis)
        {
            visible = true;
            CitizenBehaviour.Instance.visibleHumans++;

            SetModelParentVisibility(true, "SetVisible");

            CityData.Instance.visibleActors.Add(this); //Add to list of visible

            //Force animation state update
            if (animationController != null) animationController.ForceUpdateAnimationSate(true);
        }
        else
        {
            visible = false;
            CitizenBehaviour.Instance.visibleHumans--;

            CityData.Instance.visibleActors.Remove(this); //Add to list of visible

            SetModelParentVisibility(false, "SetVisible");
        }

        if (ai != null)
        {
            ai.OnVisibilityChanged();
        }
    }

    public void SetModelParentVisibility(bool val, string debugReason)
    {
        if (modelParent != null)
        {
            if (Game.Instance.collectDebugData) SelectedDebug("Set model parent visibility: " + val + " (" + debugReason + ") Previously: " + modelParent.activeSelf, HumanDebug.misc);
            modelParent.SetActive(val);
        }

        if(distantLOD != null)
        {
            distantLOD.SetActive(val);
        }
    }

    //On wake/sleep
    public virtual void GoToSleep()
    {
        if (isAsleep) return;
        isAsleep = true;
        sleepDepth = 0f;
    }

    //Return true if I've discovered a crime scene in the process of forming memories at this location.
    public virtual void WakeUp(bool forceImmediate = false)
    {
        if (isDead) return;
        if (!isAsleep) return;
        isAsleep = false;
    }

    //Triggered by AI when routine changes
    public void RoutineChange()
    {
        if (OnRoutineChange != null)
        {
            OnRoutineChange();
        }
    }

    //Triggerd by AI on routine end
    public void OnRoutineEnd()
    {
        //Fire event
        if (OnRoutineChange != null)
        {
            OnRoutineChange();
        }
    }

    //Stealth & visibility
    public void SetStealthMode(bool newVal)
    {
        if (newVal != stealthMode)
        {
            stealthMode = newVal;
            appliedStealth = 0f; //Reset applied stealth for now

            if(isPlayer)
            {
                Player.Instance.nodesTraversedWhileWalking = 0; //Reset this
            }

            if (stealthMode)
            {
                CitizenBehaviour.Instance.actorsInStealthMode.Add(this);
            }
            else
            {
                CitizenBehaviour.Instance.actorsInStealthMode.Remove(this);

                if(!isPlayer)
                {
                    currentLightLevel = 1f;
                    lightLevelTransition = 1f;
                    currentVisibilityPotential = 1f;
                }
            }

            UpdateOverallVisibility();
            OnStealthModeChange();
        }
    }

    //Set crouched
    public void SetCrouched(bool newVal, bool instant = false)
    {
        if (newVal != isCrouched)
        {
            isCrouched = newVal;
            Game.Log("Player: Set crouched: " + isCrouched);

            Player thisAsPlayer = this as Player;

            //Crouching activates stealth mode
            if (!stealthMode && isCrouched)
            {
                SetStealthMode(true);
            }
            //Cancel stealth mode with crouch unless in an illegal area
            else if (!isCrouched && (thisAsPlayer == null || (thisAsPlayer != null && !thisAsPlayer.illegalStatus)))
            {
                SetStealthMode(false);
            }

            if(instant && thisAsPlayer != null)
            {
                if(isCrouched)
                {
                    thisAsPlayer.crouchedTransition = 0;
                    thisAsPlayer.SetPlayerHeight(Player.Instance.GetPlayerHeightNormal());
                    thisAsPlayer.SetCameraHeight(GameplayControls.Instance.cameraHeightNormal);
                    thisAsPlayer.crouchTransitionActive = false;
                }
                else
                {
                    thisAsPlayer.crouchedTransition = 1;
                    thisAsPlayer.SetPlayerHeight(Player.Instance.GetPlayerHeightCrouched());
                    thisAsPlayer.SetCameraHeight(GameplayControls.Instance.cameraHeightCrouched);
                    thisAsPlayer.crouchTransitionActive = false;
                }
            }

            UpdateOverallVisibility();
            OnCrouchedChange();
        }
    }

    //Called for all actors in stealth mode
    public void StealthModeLoop()
    {
        UpdateLightLevel();

        //Smooth light level transition
        if (lightLevelTransition < currentLightLevel)
        {
            lightLevelTransition += GameplayControls.Instance.stealthModeLoopUpdateFrequency;
            lightLevelTransition = Mathf.Min(lightLevelTransition, currentLightLevel);
        }
        else if (lightLevelTransition > currentLightLevel)
        {
            lightLevelTransition -= GameplayControls.Instance.stealthModeLoopUpdateFrequency;
            lightLevelTransition = Mathf.Max(lightLevelTransition, currentLightLevel);
        }

        //Capture visibility base
        float desiredVisibilityPotential = currentLightLevel;

        //Applied stealth: Apply maximum skill when stopped, if moving apply 25% of skill
        if (isMoving)
        {
            if (appliedStealth < stealthSkill * 0.25f)
            {
                appliedStealth += CitizenControls.Instance.stealthSkillApplicationRate * GameplayControls.Instance.stealthModeLoopUpdateFrequency;
                appliedStealth = Mathf.Min(appliedStealth, stealthSkill * 0.25f);
            }
            else if (appliedStealth > stealthSkill * 0.25f)
            {
                appliedStealth -= CitizenControls.Instance.stealthSkillCancelRate * GameplayControls.Instance.stealthModeLoopUpdateFrequency;
                appliedStealth = Mathf.Max(appliedStealth, stealthSkill * 0.25f);
            }

            if (isRunning)
            {
                desiredVisibilityPotential = 1f;
            }
        }
        else
        {
            if (appliedStealth < stealthSkill)
            {
                appliedStealth += CitizenControls.Instance.stealthSkillApplicationRate * GameplayControls.Instance.stealthModeLoopUpdateFrequency;
            }
        }

        //Update visibility potential for smooth transition when running/stopped/walking
        if (currentVisibilityPotential < desiredVisibilityPotential)
        {
            currentVisibilityPotential += GameplayControls.Instance.stealthModeLoopUpdateFrequency;
            currentVisibilityPotential = Mathf.Min(currentVisibilityPotential, desiredVisibilityPotential);
        }
        else if (currentVisibilityPotential > desiredVisibilityPotential)
        {
            currentVisibilityPotential -= GameplayControls.Instance.stealthModeLoopUpdateFrequency;
            currentVisibilityPotential = Mathf.Max(currentVisibilityPotential, desiredVisibilityPotential);
        }

        appliedStealth = Mathf.Clamp(appliedStealth, 0f, stealthSkill);
        UpdateOverallVisibility();
    }

    public virtual void UpdateLightLevel()
    {
        //Get the light level for this character
        currentLightLevel = 1f;
    }

    public virtual void OnStealthModeChange()
    {

    }

    public virtual void OnCrouchedChange()
    {

    }

    //Update overall visilibty
    public void UpdateOverallVisibility()
    {
        //Stealth mode cancels light level based on amount of darkness...
        //Applied stealth only works when stopped (100%) or walking (25%)
        overallVisibility = Mathf.Clamp01(currentVisibilityPotential - ((1f - lightLevelTransition) * appliedStealth) * 0.33f);

        //If flashlight is on, visibility is always 1
        if (isPlayer)
        {
            if (FirstPersonItemController.Instance.flashlight)
            {
                overallVisibility = 1f;
            }
        }

        //Maximum distance with stealth factored in...
        float distanceMinusMinimum = GameplayControls.Instance.citizenSightRange - GameplayControls.Instance.minimumStealthDetectionRange;
        stealthDistance = GameplayControls.Instance.minimumStealthDetectionRange + //Min range
                                ((overallVisibility * overallVisibility) * distanceMinusMinimum); //Max range with minimum subtracted: Use quadratic scaling

        //Game.Log("Stealth distance = " + stealthDistance);
    }

    public virtual void SetHiding(bool val, Interactable newHidingPlace)
    {
        if(val != isHiding)
        {
            isHiding = val;
        }
    }

    //Called when recieving damage
    public virtual void RecieveDamage(float amount, Actor fromWho, Vector3 damagePosition, Vector3 damageDirection, SpatterPatternPreset forwardSpatter, SpatterPatternPreset backSpatter, SpatterSimulation.EraseMode spatterErase = SpatterSimulation.EraseMode.useDespawnTime, bool alertSurrounding = true, bool forceRagdoll = false, float forcedRagdollDuration = 0f, float shockMP = 1f, bool enableKill = false, bool allowRecoil = true, float ragdollForceMP = 1f)
    {
        //Damage is not taken if already stunned or zero health
        if (currentHealth <= 0f || isStunned) return;

        damageDirection = damageDirection.normalized;

        if(isAsleep)
        {
            WakeUp();
        }

        currentHealth -= amount;
        currentHealth = Mathf.Max(currentHealth, 0); //Clamp health
        currentHealthNormalized = currentHealth / maximumHealth;

        Game.Log(name + " damage health of " + amount + " remaining: " + currentHealth);

        //Trigger zero health reached
        if (currentHealth <= 0f)
        {
            OnZeroHealthReached();
        }

        //Minus nerve: Take the percentage damage of this damage
        float shock = (amount / maximumHealth) * shockMP;

        //Apply to damage and remove from nerve
        AddNerve(-shock * CitizenControls.Instance.nerveDamageShockMultiplier, fromWho);

        Game.Log(name + " damage nerve of " + shock + " remaining: " + currentNerve);
    }

    public virtual void AddHealth(float amount, bool affectedByGameDifficulty = true, bool displayDamageIndicator = false)
    {
        currentHealth += amount;
        currentHealth = Mathf.Clamp(currentHealth, 0, maximumHealth); //Clamp health
        currentHealthNormalized = currentHealth / maximumHealth;
    }

    public virtual void SetHealth(float amount)
    {
        currentHealth = amount;
        currentHealth = Mathf.Clamp(currentHealth, 0, maximumHealth); //Clamp health
        currentHealthNormalized = currentHealth / maximumHealth;
    }

    public virtual void AddNerve(float amount, Actor scaredBy = null)
    {
        float prev = currentNerve;

        //Enforcers on duty take less nerve damage
        if(isEnforcer && (isOnDuty || (ai != null && ai.inCombat)) && amount < 0f)
        {
            amount *= 0.3f;
        }

        //Killers take no nerve damage while killing
        if(ai != null && ai.killerForMurders.Count > 0 && amount < 0f)
        {
            if(ai.killerForMurders.Exists(item => item.state == MurderController.MurderState.executing || item.state == MurderController.MurderState.travellingTo || item.state == MurderController.MurderState.escaping)) return;
        }

        //Drunkneness lessens nerve impact
        currentNerve += amount;
        currentNerve = Mathf.Clamp(currentNerve, 0, maxNerve); //Clamp nerve
        //Game.Log(name + " set nerve: " + currentNerve + " (Add: " + amount + ")");

        if (scaredBy != null && amount < 0f)
        {
            lastScaredBy = scaredBy;

            if (lastScaredBy != null)
            {
                lastScaredAt = lastScaredBy.currentGameLocation;

                if (lastScaredBy.currentGameLocation != null && lastScaredBy.currentGameLocation.thisAsAddress == null && currentGameLocation.thisAsAddress != null)
                {
                    lastScaredAt = currentGameLocation.thisAsAddress;
                }
            }
        }

        //Trigger zero health reached
        if (currentNerve <= 0f && prev != currentNerve)
        {
            OnZeroNerveReached();
        }
    }

    public virtual void SetNerve(float amount)
    {
        currentNerve = amount;
        currentNerve = Mathf.Clamp(currentNerve, 0, maxNerve); //Clamp nerve

        //Killers take no nerve damage while killing
        if (ai != null && ai.killerForMurders.Count > 0 && amount < 0f)
        {
            if (ai.killerForMurders.Exists(item => item.state == MurderController.MurderState.executing || item.state == MurderController.MurderState.travellingTo || item.state == MurderController.MurderState.escaping)) currentNerve = maxNerve; //Clamp nerve
        }
    }

    //Triggered when health reaches zero (or lower)
    public virtual void OnZeroHealthReached()
    {
        if (isAsleep)
        {
            WakeUp();
        }

        Game.Log("Zero health reached: " + name);
    }

    public virtual void ResetHealthToMaximum()
    {
        AddHealth(99999999999999f);
    }

    public virtual void ResetNerveToMaximum()
    {
        AddNerve(99999999999999f);
    }

    //Triggered when nerve reaches zero
    public virtual void OnZeroNerveReached()
    {
        if (isAsleep)
        {
            WakeUp();
        }

        Game.Log("Zero nerve reached: " + name);

        //Force an AI tick which should trigger the flee goal
        if(ai != null)
        {
            ai.AITick(true);
        }
    }

    public virtual void SetMaxHealth(float newMax, bool setToMax = false)
    {
        maximumHealth = Mathf.Max(newMax, 0.01f); //Don't go below 0.01 otherwise this could cause problems

        if(setToMax)
        {
            ResetHealthToMaximum();
        }
    }

    public virtual void SetMaxNerve(float newMax, bool setToMax = false)
    {
        maxNerve = Mathf.Max(newMax, 0.01f); //Don't go below 0.01 otherwise this could cause problems

        if (setToMax)
        {
            ResetNerveToMaximum();
        }
    }

    public virtual void SetRecoveryRate(float newRate)
    {
        recoveryRate = Mathf.Clamp(newRate, 0.01f, 100f);
    }

    public virtual void SetCombatSkill(float newSkill)
    {
        combatSkill = Mathf.Clamp(newSkill, 0.01f, 1f);
    }

    public virtual void SetCombatHeft(float newHeft)
    {
        combatHeft = Mathf.Clamp(newHeft, 0f, 1f);
    }

    public void SetInBed(bool newVal)
    {
        isInBed = newVal;

        if (animationController != null)
        {
            animationController.SetInBed(isInBed);
        }
    }

    //Update actor's current position on node
    public virtual void UpdateCurrentNodeSpace()
    {
        if (!visible) return;

        //Find nearest node space in current node
        if(currentNode != null)
        {
            NewNode.NodeSpace closest = null;
            float closestDist = Mathf.Infinity;

            foreach(KeyValuePair<Vector3, NewNode.NodeSpace> pair in currentNode.walkableNodeSpace)
            {
                if(pair.Value.occ == NewNode.NodeSpaceOccupancy.empty || pair.Value.occupier == this)
                {
                    float dist = Vector3.Distance(pair.Value.position, this.transform.position);

                    if (dist < closestDist)
                    {
                        closest = pair.Value;
                        closestDist = dist;
                    }
                }
            }

            //New closest node
            if(closest != currentNodeSpace && currentNodeSpace != null)
            {
                currentNodeSpace.SetEmpty();
            }

            currentNodeSpace = closest;
            if (currentNodeSpace != null) closest.SetOccuppier(this, NewNode.NodeSpaceOccupancy.position);
        }
    }

    //Add reversed node space
    public virtual void AddReservedNodeSpace(NewNode.NodeSpace newSpace)
    {
        if(!reservedNodeSpace.Contains(newSpace))
        {
            reservedNodeSpace.Add(newSpace);
        }
    }

    //Remove all reserved node space
    public virtual void RemoveReservedNodeSpace()
    {
        foreach(NewNode.NodeSpace space in reservedNodeSpace)
        {
            space.SetEmpty();
        }

        reservedNodeSpace.Clear();
    }

    public virtual void UpdateTrespassing(bool allowEnforcersEverywhere)
    {
        bool enforcersAllowed = false;

        if(ai != null)
        {
            if (ai.currentGoal != null) enforcersAllowed = ai.currentGoal.preset.allowEnforcersEverywhere;
        }

        isTrespassing = IsTrespassing(currentRoom, out trespassingEscalation, out _, enforcersAllowed);
        illegalAreaActive = isTrespassing;

        UpdateIllegalStatus();
    }

    //AI Sighting check
    public virtual void SightingCheck(float fov, bool ignoreLightAndStealth = false)
    {
        seenIllegalThisCheck.Clear();

        //If I can't see at all...
        if (!seesOthers || isDead || isAsleep)
        {
            ClearSeesIllegal();
            return;
        }

        //Time since last sight check
        float timeSinceLast = SessionData.Instance.gameTime - timeOfLastSightCheck;
        if (timeSinceLast <= 0f) return;

        Human hu = this as Human;

        //Shortlist of other citizens within range
        Dictionary<Actor, float> shortlist = new Dictionary<Actor, float>(); //This dictionary uses a citizen key, and a vector4 consisting of angle.x, angle.y, angle.z, distance
        float halfFOV = fov * 0.5f;

        //Shortlist of illegal sightings this check
        List<Actor> illegalSightingsThisCycle = new List<Actor>();

        for (int i = 0; i < CityData.Instance.visibleActors.Count; i++)
        {
            Actor cc = CityData.Instance.visibleActors[i] as Actor;
            if (cc == this) continue; //Ignore self
            if (!cc.isSeenByOthers) continue;
            if (Game.Instance.invisiblePlayer && cc.isPlayer) continue;
            if (cc.inAirVent) continue;
            if (cc.isPlayer && Player.Instance.playerKOInProgress) continue;

            //Hiding...
            if(cc.isHiding)
            {
                if (cc.isPlayer)
                {
                    if (!Player.Instance.spottedWhileHiding.Contains(this))
                    {
                        continue;
                    }
                }
                else continue;
            }

            //Only detect downed citizens if invisible
            //if(!visible)
            //{
            //    if(cc.isPlayer || (!cc.isDead && !cc.isStunned))
            //    {
            //        continue;
            //    }
            //}

            //Range check
            float distance = Vector3.Distance(lookAtThisTransform.position, cc.lookAtThisTransform.position);

            //Limit max distance by the current stealth distance...
            if(!ignoreLightAndStealth)
            {
                cc.UpdateOverallVisibility(); //Update actor visibility
            }

            float spotDist = Mathf.Min(GameplayControls.Instance.citizenSightRange, cc.stealthDistance);

            if (isMachine)
            {
                spotDist = Mathf.Min(GameplayControls.Instance.securitySightRange, cc.stealthDistance);
            }

            if (distance <= spotDist)
            {
                //Is the citizen within my field of view?
                Vector3 targetDir = cc.transform.position - lookAtThisTransform.position;
                float angleToOther = Vector3.Angle(targetDir, lookAtThisTransform.forward); //Local angle

                //Add to shortlist if within FoV and range...
                if (angleToOther >= -halfFOV && angleToOther <= halfFOV) // 180° FOV
                {
                    if (!shortlist.ContainsKey(cc))
                    {
                        shortlist.Add(cc, distance); //Add to shortlist
                    }
                }
            }
        }

        //Raycast from target back to viewer to avoid wall clipping issues...
        foreach (KeyValuePair<Actor, float> pair in shortlist)
        {
            if (pair.Key == this) continue; //Ignore self

            //Update sighting
            if(hu != null && !hu.isDead && hu.ai != null && (hu.ai.currentAction == null || !hu.ai.currentAction.preset.disableSightingUpdates))
            {
                hu.UpdateLastSighting(pair.Key as Human);
            }

            bool isSeen = false;

            RaycastHit hit = new RaycastHit();

            bool drawRays = false;

            #if UNITY_EDITOR
            if (Game.Instance.debugHuman.Contains(this) || UnityEditor.Selection.Contains(this.gameObject))
            {
                drawRays = true;
            }
            #endif

            //This feature does a double check that uses both directions....
            //Override check with basic proximity...
            if((pair.Key.isDead || pair.Key.isStunned) && !pair.Key.unreportable && (currentRoom == pair.Key.currentRoom || pair.Value <= 6.5f) && (ai == null || ai.attackTarget != pair.Key))
            {
                if(pair.Key.currentGameLocation != null && !pair.Key.currentGameLocation.isCrimeScene)
                {
                    //SelectedDebug("Seen: " + pair.Key + " distance: " + pair.Value);
                    isSeen = true;
                }
            }

            //Spooked
            if(!isSeen && ai != null && ai.currentAction != null && ai.currentAction.preset.spookAction && pair.Key.isPlayer)
            {
                isSeen = true;
            }

            //Cancel combat
            if(!isMachine && (pair.Key.isDead || pair.Key.isStunned) && (ai != null && ai.inCombat && ai.attackTarget == pair.Key || ai.persuitTarget == pair.Key || seesIllegal.ContainsKey(pair.Key)))
            {
                if(speechController != null)
                {
                    if(seesIllegal.ContainsKey(pair.Key))
                    {
                        if(speechController.speechQueue.Count <= 0) speechController.TriggerBark(SpeechController.Bark.targetDown);
                        RemoveFromSeesIllegal(pair.Key, 0);
                    }

                    if(pair.Key == ai.attackTarget || pair.Key == ai.persuitTarget)
                    {
                        ai.CancelPersue();
                    }

                    if (seesIllegal.Count <= 0)
                    {
                        ai.CancelCombat();
                    }
                }
            }

            if(pair.Value < GameplayControls.Instance.minimumStealthDetectionRange)
            {
                if (Game.Instance.collectDebugData) SelectedDebug("Seen: " + pair.Key + " distance: " + pair.Value, HumanDebug.sight);
                isSeen = true;
            }
            else if(ActorRaycastCheck(pair.Key, pair.Value + 3f, out hit, drawRays, Color.green, Color.red, Color.white))
            {
                if (Game.Instance.collectDebugData) SelectedDebug("Seen: " + pair.Key + " distance: " + pair.Value, HumanDebug.sight);
                isSeen = true;
            }
            else
            {
                //SelectedDebug("Not seen: " + pair.Key + " distance: " + pair.Value, HumanDebug.sight);
            }

            //Is seen
            if (isSeen)
            {
                //Add to track targets list
                if (!pair.Key.isMachine)
                {
                    //Add as look at target
                    OnAddTrackedTarget(pair.Key);
                }

                //Is this person doing something illegal?
                bool allowEnforcersEverywhere = false;
                if (pair.Key.ai != null && pair.Key.ai.currentGoal != null) allowEnforcersEverywhere = pair.Key.ai.currentGoal.preset.allowEnforcersEverywhere;

                pair.Key.UpdateTrespassing(allowEnforcersEverywhere);

                //Citizens will never persue an enforcer
                bool looksLikeEnforcer = false;

                if(pair.Key.isEnforcer)
                {
                    if(pair.Key.isOnDuty)
                    {
                        looksLikeEnforcer = true;
                    }
                    else
                    {
                        Human h = pair.Key as Human;

                        if(h != null)
                        {
                            if(h.outfitController != null && (h.outfitController.currentOutfit == ClothesPreset.OutfitCategory.work || h.outfitController.currentOutfit == ClothesPreset.OutfitCategory.outdoorsWork))
                            {
                                looksLikeEnforcer = true;
                            }
                        }
                    }
                }

                //For now only spot player
                //if (!pair.Key.isPlayer) continue;

                bool alarmSysTarget = false;

                if (isMachine && interactable != null && interactable.node != null)
                {
                    alarmSysTarget = interactable.node.gameLocation.IsAlarmSystemTarget(pair.Key as Human);
                    if(alarmSysTarget) Game.Log("Machine illegal: " + pair.Key.name + ": " + alarmSysTarget + " mode: " + interactable.node.building.targetMode.ToString());
                }

                //Surveillance will trigger if player wanted in this building...
                //Investigate KO'd or dead body
                if (ai != null && !isMachine && (pair.Key.isStunned || pair.Key.isDead) && !pair.Key.unreportable && !ai.goals.Exists(item => item.preset == RoutineControls.Instance.findDeadBody))
                {
                    Human other = pair.Key as Human;

                    if(other != null && (other.death == null || !other.death.isDead || !other.death.reported) && (other.death == null || !other.death.isDead || other.death.GetKiller() != this) && (other.ai == null || (!other.ai.isTripping && other.drunk <= 0.2f)))
                    {
                        Game.Log(pair.Key.name + " Sees downed body at " + other.currentGameLocation.name);
                        ai.CreateNewGoal(RoutineControls.Instance.findDeadBody, 0f, 0f, newPassedInteractable: pair.Key.interactable, newPassedVar: (int)Human.Death.ReportType.visual);
                    }

                    //Add mourn
                    Human h = this as Human;

                    if(h != null && (other.death == null || !other.death.isDead || other.death.GetKiller() != this))
                    {
                        Acquaintance aq = null;

                        if(h.FindAcquaintanceExists(pair.Key as Human, out aq))
                        {
                            //Game.Log("Found acquaintance: " + aq.connections[0].ToString() + " known " + aq.known);

                            if (aq.known > SocialControls.Instance.knowMournThreshold || aq.connections[0] == Acquaintance.ConnectionType.lover || aq.connections[0] == Acquaintance.ConnectionType.paramour || aq.connections[0] == Acquaintance.ConnectionType.housemate)
                            {
                                NewAIGoal mournGoal = ai.goals.Find(item => item.preset == RoutineControls.Instance.mourn);

                                if (mournGoal == null)
                                {
                                    //Game.Log("Create mourn goal");
                                    ai.CreateNewGoal(RoutineControls.Instance.mourn, 0f, 0f);
                                }
                                //Otherwise reprioritize mourn
                                else
                                {
                                    mournGoal.activeTime = 0f;
                                }
                            }
                        }
                    }
                }
                //Trigger on see illegal if this has a persuit target...
                else if (
                    (pair.Key.illegalStatus && !pair.Key.isStunned && !pair.Key.isDead && !looksLikeEnforcer) ||
                    (!isMachine && ai != null && ai.persuitTarget == pair.Key) ||
                    (isMachine && alarmSysTarget) ||
                    (pair.Key.isPlayer && ai != null && (ai.spookCounter >= 2 || (ai.currentAction != null && ai.currentAction.preset.spookAction))) ||
                    (pair.Key.isPlayer && currentBuilding != null && currentBuilding.wantedInBuilding > SessionData.Instance.gameTime && (locationsOfAuthority.Contains(pair.Key.currentGameLocation) || isMachine || (isEnforcer && isOnDuty)))
                    )
                {
                    illegalSightingsThisCycle.Add(pair.Key); //Keep track of illegal sightings this check...

                    //Do a check based on escalation level...
                    float spotDist = Player.Instance.stealthDistance;

                    if (isMachine)
                    {
                        spotDist = GameplayControls.Instance.securitySightRange;
                    }
                    else
                    {
                        spotDist = GameplayControls.Instance.citizenSightRange;
                    }

                    float spotDistance = Mathf.Lerp(spotDist * 0.75f, spotDist, escalationLevel / 2f);

                    if (hit.distance <= spotDistance)
                    {
                        if (Game.Instance.collectDebugData) SelectedDebug(pair.Key.name + " seen distance: " + hit.distance, HumanDebug.sight);

                        //Game.Log("player illegal activity seen!");
                        if (ai != null) ai.SetUpdateEnabled(true); //Enable this to fill counter

                        //How much to add to focus...
                        //Ratio of maximum distance
                        float rangeRatio = pair.Value / GameplayControls.Instance.citizenSightRange; //0 = closest, 1 = furthest
                        float sightAddition = Mathf.Lerp(CitizenControls.Instance.persuitTimerThreshold.x, CitizenControls.Instance.persuitTimerThreshold.y, rangeRatio);

                        //Get escalation level
                        int esc = 0;

                        if ((!isMachine && pair.Key.illegalAreaActive) || (isMachine && alarmSysTarget))
                        {
                            esc = pair.Key.trespassingEscalation;

                            if (esc >= 2 || escalationLevel >= 2 || (isMachine && alarmSysTarget))
                            {
                                if (Game.Instance.collectDebugData) SelectedDebug("Add sight using: >Current Time MP: " + SessionData.Instance.currentTimeMultiplier + " >Sight Addition (Ratio " + rangeRatio + "): " + sightAddition + " >SpotFocusSpeed: " + spotFocusSpeedMultiplier, HumanDebug.sight);

                                AddToSeesIllegal(pair.Key, SessionData.Instance.currentTimeMultiplier * sightAddition * spotFocusSpeedMultiplier);
                                seenIllegalThisCheck.Add(pair.Key);
                            }
                            else
                            {
                                if (ai != null)
                                {
                                    //Update escalation
                                    currentRoom.gameLocation.AddEscalation(pair.Key);
                                    esc += currentRoom.gameLocation.GetAdditionalEscalation(pair.Key);

                                    if (Game.Instance.collectDebugData) SelectedDebug("AI: Get escalation for " + pair.Key.name + " in " + currentRoom.name + ": " + esc, HumanDebug.sight);

                                    if (pair.Key.isPlayer)
                                    {
                                        //Add peril
                                        if (Player.Instance.currentGameLocation.thisAsAddress != null)
                                        {
                                            StatusController.Instance.ConfirmFine(Player.Instance.currentGameLocation.thisAsAddress, null, StatusController.CrimeType.trespassing);
                                        }
                                    }

                                    //Trespass bark
                                    if(speechController != null)
                                    {
                                        if(speechController.speechQueue.Count <= 0)
                                        {
                                            speechController.TriggerBark(SpeechController.Bark.trespass);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if(isMachine)
                            {
                                //Special case; surveillance ignores if in the period between wanting to close and closing
                                if (pair.Key.illegalAreaActive)
                                {

                                    if (pair.Key.currentGameLocation != null && pair.Key.currentGameLocation.thisAsAddress != null && pair.Key.currentGameLocation.thisAsAddress.company != null)
                                    {
                                        if (!pair.Key.currentGameLocation.thisAsAddress.company.openForBusinessDesired && pair.Key.currentGameLocation.thisAsAddress.company.openForBusinessActual)
                                        {
                                            continue;
                                        }
                                    }

                                    //if (pair.Key.trespassingEscalation < 2) continue; //Ignore escalation level 1
                                }

                                esc = 2;

                                if (Game.Instance.collectDebugData) SelectedDebug("Add sight using: >Current Time MP: " + SessionData.Instance.currentTimeMultiplier + " >Sight Addition (Ratio " + rangeRatio + "): " + sightAddition + " >SpotFocusSpeed: " + spotFocusSpeedMultiplier, HumanDebug.sight);
                                AddToSeesIllegal(pair.Key, SessionData.Instance.currentTimeMultiplier * sightAddition * spotFocusSpeedMultiplier);
                                seenIllegalThisCheck.Add(pair.Key);
                            }
                            else
                            {
                                //If illegal action or sees persuit target...
                                if (pair.Key.illegalActionActive || (ai != null && ai.persuitTarget == pair.Key))
                                {
                                    esc = 2;

                                    if (Game.Instance.collectDebugData) SelectedDebug("Add sight using: >Current Time MP: " + SessionData.Instance.currentTimeMultiplier + " >Sight Addition (Ratio " + rangeRatio + "): " + sightAddition + " >SpotFocusSpeed: " + spotFocusSpeedMultiplier, HumanDebug.sight);
                                    AddToSeesIllegal(pair.Key, SessionData.Instance.currentTimeMultiplier * sightAddition * spotFocusSpeedMultiplier);
                                    seenIllegalThisCheck.Add(pair.Key);
                                }
                                //Spooked behaviour
                                else if (pair.Key.isPlayer && ai != null && (ai.spookCounter >= 2 || (ai.currentAction != null && ai.currentAction.preset.spookAction)))
                                {
                                    Game.Log("Seen: Spooked behaviour");
                                    float focus = SessionData.Instance.currentTimeMultiplier * sightAddition * spotFocusSpeedMultiplier;

                                    ai.AddSpooked(focus * (0.05f * ai.spookCounter));
                                    //esc = Mathf.RoundToInt(ai.spooked * 2f);
                                    esc = ai.spookCounter;

                                    if (!seesIllegal.ContainsKey(pair.Key))
                                    {
                                        speechController.TriggerBark(SpeechController.Bark.spookConfront);
                                    }

                                    AddToSeesIllegal(pair.Key, focus * (0.05f * ai.spookCounter));
                                    seenIllegalThisCheck.Add(pair.Key);

                                    AddNerve(focus * -0.125f);
                                }
                                //Don't pursue if low escalation
                                else if (esc < 2)
                                {
                                    continue;
                                }
                            }
                        }

                        //Surveillance ignores minor/esc 0 crimes unless wanted
                        //if (isMachine && esc <= 0 /*&& interactable.node.building.wantedInBuilding <= 0f*/)
                        //{
                        //    continue;
                        //}

                        //Special case; surveillance ignores if in the period between wanting to close and closing
                        if(pair.Key.illegalAreaActive && isMachine)
                        {
                            if(pair.Key.currentGameLocation != null && pair.Key.currentGameLocation.thisAsAddress != null && pair.Key.currentGameLocation.thisAsAddress.company != null)
                            {
                                if(!pair.Key.currentGameLocation.thisAsAddress.company.openForBusinessDesired && pair.Key.currentGameLocation.thisAsAddress.company.openForBusinessActual)
                                {
                                    continue;
                                }
                            }
                        }

                        if (pair.Key.isPlayer)
                        {
                            InterfaceController.Instance.CrosshairReaction(); //Trigger UI feedback

                            //Trigger person being searched to notice...
                            if(Player.Instance.searchInteractable != null && Player.Instance.searchInteractable.isActor != null)
                            {
                                Human sI = Player.Instance.searchInteractable.isActor as Human;

                                if(sI != null && sI != this && sI.ai != null)
                                {
                                    //Persue
                                    sI.ai.SetPersue(Player.Instance, true, 2, true, CitizenControls.Instance.punchedResponseRange);
                                }
                            }
                        }

                        //Investigate
                        OnInvestigate(pair.Key, esc);
                    }
                }
            }
        }

        if(seesIllegal.Count > 0)
        {
            //Wind down focus if not seen this frame
            List<Actor> allSeen = new List<Actor>(seesIllegal.Keys);

            foreach(Actor a in allSeen)
            {
                if(!illegalSightingsThisCycle.Contains(a))
                {
                    RemoveFromSeesIllegal(a, timeSinceLast * SessionData.Instance.currentTimeMultiplier * CitizenControls.Instance.persuitForgetThreshold * spotLoseFocusSpeedMultiplier * (3f - escalationLevel));
                }
            }

            //If I can see illegal activity, make sure to maintain a high tick rate
            if (ai != null)
            {
                if (ai.tickRate != NewAIController.AITickRate.veryHigh)
                {
                    ai.SetDesiredTickRate(NewAIController.AITickRate.veryHigh);
                }
            }
        }

        //Update look @ targets
        if(ai != null)
        {
            ai.UpdateTrackedTargets();
        }

        //Update time since
        timeOfLastSightCheck = SessionData.Instance.gameTime;
    }

    //Perform a basic sign check to test if within FoV, and apply light settings
    public virtual bool CanISee(Interactable interactable)
    {
        float sightRange = GameplayControls.Instance.citizenSightRange;
        if (isMachine) sightRange = GameplayControls.Instance.securitySightRange;

        float fov = GameplayControls.Instance.citizenFOV;
        if (isMachine) fov = GameplayControls.Instance.securityFOV;

        //Apply light: If the main lights are off, * by 0.1, if secondary lights are on only, * by 0.5
        if (interactable.node != null)
        {
            if(!interactable.node.room.mainLightStatus && !interactable.node.room.secondaryLightStatus)
            {
                sightRange *= 0.1f;
            }
            else if(!interactable.node.room.mainLightStatus && interactable.node.room.secondaryLightStatus)
            {
                sightRange *= 0.5f;
            }
        }

        //Basic distance check
        if(Vector3.Distance(interactable.wPos, lookAtThisTransform.position) <= sightRange)
        {
            //FoV Check
            float halfFOV = fov * 0.5f;

            //Is the citizen within my field of view?
            Vector3 targetDir = interactable.wPos - lookAtThisTransform.position;
            float angleToOther = Vector3.Angle(targetDir, lookAtThisTransform.forward); //Local angle

            if (angleToOther >= -halfFOV && angleToOther <= halfFOV) // 180° FOV
            {
                //Check is passed
                //If both visible, do a raycast check
                if (interactable.controller != null)
                {
                    Ray ray = new Ray(lookAtThisTransform.position, targetDir);

                    RaycastHit hit;

                    if (Physics.Raycast(ray, out hit, sightRange, Toolbox.Instance.aiSightingLayerMask))
                    {
                        InteractableController ic = hit.transform.gameObject.GetComponent<InteractableController>();

                        if (ic != null && ic.interactable == interactable)
                        {
                            return true;
                        }
                        else
                        {
                            InteractableController[] childrenIC = hit.transform.gameObject.GetComponentsInChildren<InteractableController>();

                            foreach(InteractableController i in childrenIC)
                            {
                                if (i != null && i.interactable == interactable)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
                //If not assume yes
                else return true;
            }
        }

        return false;
    }

    //Use dual raycast to detect if AI can see something...
    public bool ActorRaycastCheck(Actor other, float maxRange, out RaycastHit hit, bool drawLine = false, Color lineSuccess = new Color(), Color lineFail = new Color(), Color lineNothing = new Color(), float lineTime = 1f)
    {
        bool ret = false;
        hit = new RaycastHit();

        for (int i = 0; i < 2; i++)
        {
            //From this actor to other (head, feet)
            Vector3 startPos = lookAtThisTransform.position;
            Vector3 endPos = other.lookAtThisTransform.position;
            if (i == 1) endPos = other.transform.position + new Vector3(0, 0.5f, 0);
            Vector3 dir = endPos - startPos;
            Ray ray = new Ray(startPos, dir);

            if (Physics.Raycast(ray, out hit, maxRange, Toolbox.Instance.aiSightingLayerMask, QueryTriggerInteraction.Collide))
            {
                if (hit.transform.parent == other.transform || hit.transform == other.lookAtThisTransform || hit.transform == other.transform)
                {
                    if (drawLine)
                    {
                        Debug.DrawLine(startPos, hit.point, lineSuccess, lineTime);
                    }

                    if (other.isDead || other.isStunned) return true; //Don't do the inverse check if they're KO'd.

                    //Now do the same in reverse...
                    Vector3 startPos2 = endPos;
                    Vector3 endPos2 = startPos;
                    Vector3 dir2 = endPos2 - startPos2;
                    Ray ray2 = new Ray(startPos2, dir2);
                    RaycastHit hit2;

                    if (Physics.Raycast(ray2, out hit2, maxRange, Toolbox.Instance.aiSightingLayerMask))
                    {
                        if (hit2.transform.parent == this.transform || hit2.transform == lookAtThisTransform || hit2.transform == transform)
                        {
                            if (drawLine)
                            {
                                Debug.DrawLine(startPos2, hit2.point, lineSuccess, lineTime);
                            }

                            return true;
                        }
                        else if (drawLine)
                        {
                            Debug.DrawLine(startPos2, hit2.point, lineFail, lineTime);
                        }
                    }
                    else if (drawLine)
                    {
                        Debug.DrawRay(startPos2, dir2.normalized * maxRange, lineNothing, lineTime);
                    }

                }
                else if (drawLine)
                {
                    Debug.DrawLine(startPos, hit.point, lineFail, lineTime);
                }
            }
            else if (drawLine)
            {
                Debug.DrawRay(startPos, dir.normalized * maxRange, lineNothing, lineTime);
            }
        }

        return ret;
    }

    //Triggered when an investigation reaction is needed
    public virtual void OnInvestigate(Actor newTarget, int escalation)
    {
        if(ai != null)
        {
            ai.Investigate(newTarget.currentNode, newTarget.transform.position, newTarget, NewAIController.ReactionState.investigatingSight, CitizenControls.Instance.sightingMinInvestigationTimeMP, escalation);
        }
    }

    //Triggered when a tracked target is added
    public virtual void OnAddTrackedTarget(Actor newTarget)
    {
        if(ai != null)
        {
            ai.AddTrackedTarget(newTarget);
        }
    }

    //Triggered when this sees a player illegal
    public virtual void AddToSeesIllegal(Actor newTarget, float focus)
    {
        if(!seesIllegal.ContainsKey(newTarget))
        {
            seesIllegal.Add(newTarget, 0f);
        }

        seesIllegal[newTarget] += focus;
        seesIllegal[newTarget] = Mathf.Clamp01(seesIllegal[newTarget]);

        if (Game.Instance.collectDebugData) SelectedDebug("Add sight focus target " + newTarget.name + ": " + focus + ", total: " + seesIllegal[newTarget], HumanDebug.sight);

        if (newTarget.isPlayer)
        {
            debugSeesPlayer = Mathf.RoundToInt(seesIllegal[newTarget] * 100f);
            debugLastSeesPlayerChange = focus;

            if(ai != null)
            {
                ai.debugSeesPlayer = debugSeesPlayer;
                ai.debugLastSeesPlayerChange = debugLastSeesPlayerChange;
            }
        }

        if(!newTarget.witnessesToIllegalActivity.Contains(this as Actor))
        {
            newTarget.witnessesToIllegalActivity.Add(this as Actor);
        }
    }

    public virtual void RemoveFromSeesIllegal(Actor newTarget, float focus)
    {
        if(seesIllegal.ContainsKey(newTarget))
        {
            seesIllegal[newTarget] -= focus;

            if (newTarget.isPlayer)
            {
                debugSeesPlayer = Mathf.RoundToInt(seesIllegal[newTarget] * 100f);
                debugLastSeesPlayerChange = -focus;

                if (ai != null)
                {
                    ai.debugSeesPlayer = debugSeesPlayer;
                    ai.debugLastSeesPlayerChange = debugLastSeesPlayerChange;
                }
            }

            try
            {
                if (Game.Instance.collectDebugData) SelectedDebug("Remove sight focus target " + newTarget.name + ": " + focus + ", total: " + seesIllegal[newTarget], HumanDebug.sight);
            }
            catch
            {

            }

            if (seesIllegal[newTarget] <= 0f)
            {
                seesIllegal.Remove(newTarget);
                newTarget.witnessesToIllegalActivity.Remove(this as Actor);
            }

            //newTarget.RemovePersuedBy(this);
        }
    }

    public virtual void AddPersuedBy(Actor newTarget)
    {
        if (!persuedBy.Contains(newTarget))
        {
            persuedBy.Add(newTarget);
            //Game.Log(name + " pursued by " + newTarget.name);
            //StatusController.Instance.ForceStatusCheck();

            if(isPlayer)
            {
                InterfaceController.Instance.movieBarJuice.Pulsate(true);

                if(Player.Instance.combatSnapshot == null)
                {
                    Player.Instance.combatSnapshot = AudioController.Instance.Play2DLooping(AudioControls.Instance.combatSnapshot);
                }
            }

            if(isPlayer || newTarget.isPlayer)
            {
                Player.Instance.StatusCheckEndOfFrame();
            }
        }
    }

    public virtual void RemovePersuedBy(Actor newTarget)
    {
        if (persuedBy.Contains(newTarget))
        {
            persuedBy.Remove(newTarget);
            //StatusController.Instance.ForceStatusCheck();

            if(persuedBy.Count <= 0)
            {
                if (isPlayer)
                {
                    InterfaceController.Instance.movieBarJuice.Pulsate(false);

                    if (Player.Instance.combatSnapshot != null)
                    {
                        AudioController.Instance.StopSound(Player.Instance.combatSnapshot, AudioController.StopType.triggerCue, "Exit combat");
                        Player.Instance.combatSnapshot = null;
                    }
                }
            }

            if (isPlayer || newTarget.isPlayer)
            {
                Player.Instance.StatusCheckEndOfFrame();
            }
        }
    }

    //Triggered upon hearing (only) an illegal sound
    public void HearIllegal(AudioEvent audioEvent, NewNode newInvestigateNode, Vector3 newInvestigatePosition, Actor newTarget, int escLevel)
    {
        if(ai!= null)
        {
            if(newTarget != null && newTarget.isPlayer) InterfaceController.Instance.CrosshairReaction(); //Trigger UI feedback

            ai.HearIllegal(audioEvent, newInvestigateNode, newInvestigatePosition, newTarget, escLevel);
        }
    }

    public virtual void ClearSeesIllegal()
    {
        List<Actor> allKeys = new List<Actor>(seesIllegal.Keys);

        foreach(Actor a in allKeys)
        {
            RemoveFromSeesIllegal(a, 1f);
        }

        debugSeesPlayer = 0;
        debugLastSeesPlayerChange = 0f;

        if (ai != null)
        {
            ai.debugSeesPlayer = debugSeesPlayer;
            ai.debugLastSeesPlayerChange = debugLastSeesPlayerChange;
        }
    }

    public virtual void SetEscalation(int newEsc)
    {
        if (escalationLevel != newEsc)
        {
            escalationLevel = newEsc;

            if (escalationLevel >= 2)
            {
                Game.Log("AI: " + name + " is at escalation level 2!");
            }

            if(ai != null)
            {
                if (ai.currentAction != null) ai.currentAction.UpdateCombatPose();
                ai.TriggerReactionIndicator();
            }
        }
    }

    //Debug
    public void SelectedDebug(string str, HumanDebug debug)
    {
        #if UNITY_EDITOR
        if (Game.Instance.debugHuman.Contains(this) || UnityEditor.Selection.Contains(this.gameObject))
        {
            if(debug == HumanDebug.actions && Game.Instance.debugHumanActions)
            {
                Game.Log("AI: Actions: " + name + ": " + str);
            }
            else if(debug == HumanDebug.attacks && Game.Instance.debugHumanAttacks)
            {
                Game.Log("AI: Attacks: " + name + ": " + str);
            }
            else if(debug == HumanDebug.misc && Game.Instance.debugHumanMisc)
            {
                Game.Log("AI: Misc: " + name + ": " + str);
            }
            else if(debug == HumanDebug.movement && Game.Instance.debugHumanMovement)
            {
                Game.Log("AI: Movement: " + name + ": " + str);
            }
            else if(debug == HumanDebug.updates && Game.Instance.debugHumanUpdates)
            {
                Game.Log("AI: Updates: " + name + ": " + str);
            }
            else if (debug == HumanDebug.sight && Game.Instance.debugHumanSight)
            {
                Game.Log("AI: Updates: " + name + ": " + str);
            }
        }
        #endif
    }

    //Trigger spotted by player (including outline)
    public void SpottedByPlayer(float graceTimeMultiplier = 1f)
    {
        //Player.Instance.currentVisibleTargets.Add(this);
        spottedState = 1f;
        spottedGraceTime = Mathf.Max(GameplayControls.Instance.spottedGraceTime * graceTimeMultiplier * (1f + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.securityGraceTimeModifier)), spottedGraceTime);

        if (ai != null)
        {
            ai.TriggerReactionIndicator();
        }

        if (!Player.Instance.spottedByPlayer.Contains(this))
        {
            Player.Instance.spottedByPlayer.Add(this);
            InterfaceController.Instance.footstepAudioIndicator.UpdateCurrentEvent();

            //Update current interaction sounds...
            foreach(KeyValuePair<InteractablePreset.InteractionKey, InteractionController.InteractionSetting> pair in InteractionController.Instance.currentInteractions)
            {
                if (pair.Value.audioEvent != null)
                {
                    if (pair.Value.newUIRef != null)
                    {
                        if (pair.Value.newUIRef.soundIndicator != null)
                        {
                            pair.Value.newUIRef.soundIndicator.UpdateCurrentEvent();
                        }
                    }
                }
            }
        }
    }

    public void HeardByPlayer()
    {
        if(Player.Instance.illegalStatus || Player.Instance.witnessesToIllegalActivity.Contains(this))
        {
            //Game.Log("Audio: " + name + " heard by player");

            spottedState = 1f;
            spottedGraceTime = Mathf.Max(GameplayControls.Instance.spottedGraceTime * GameplayControls.Instance.audioOnlySpotGraceTimeMultiplier, spottedGraceTime);

            if (ai != null)
            {
                ai.TriggerReactionIndicator();
            }

            if (!Player.Instance.spottedByPlayer.Contains(this))
            {
                Player.Instance.spottedByPlayer.Add(this);
                InterfaceController.Instance.footstepAudioIndicator.UpdateCurrentEvent();

                //Update current interaction sounds...
                foreach (KeyValuePair<InteractablePreset.InteractionKey, InteractionController.InteractionSetting> pair in InteractionController.Instance.currentInteractions)
                {
                    if (pair.Value.audioEvent != null)
                    {
                        if (pair.Value.newUIRef != null)
                        {
                            if (pair.Value.newUIRef.soundIndicator != null)
                            {
                                pair.Value.newUIRef.soundIndicator.UpdateCurrentEvent();
                            }
                        }
                    }
                }
            }
        }
    }

    //Would I be trespassing in this room?
    public virtual bool IsTrespassing(NewRoom room, out int trespassEscalation, out string debugOutput, bool enforcersAllowedEverywhere = true)
    {
        trespassEscalation = 0;
        debugOutput = string.Empty;
        bool ret = false;

        return ret;
    }

    public void AddLocationOfAuthorty(NewGameLocation newLoc)
    {
        if(!locationsOfAuthority.Contains(newLoc))
        {
            locationsOfAuthority.Add(newLoc);
        }
    }

    public void RemoveLocationOfAuthority(NewGameLocation newLoc)
    {
        locationsOfAuthority.Remove(newLoc);
    }

    //Called when illegal status could be changed/needs updating
    public virtual void UpdateIllegalStatus()
    {
        bool newIllegalStatus = false;

        if (illegalActionActive)
        {
            newIllegalStatus = true;
        }

        if (illegalAreaActive)
        {
            newIllegalStatus = true;
        }

        if(forceTarget)
        {
            newIllegalStatus = true;
        }

        //Trigger on change of illegal area
        if (newIllegalStatus != illegalStatus)
        {
            illegalStatus = newIllegalStatus;
        }
    }

    [Button]
    public void ListSeesIllegal()
    {
        foreach (KeyValuePair<Actor, float> pair in seesIllegal)
        {
            Game.Log(pair.Key.name + " " + pair.Key.transform.name + ": " + pair.Value);
        }
    }

    [Button]
    public void ListWitnessToIllegal()
    {
        foreach(Actor a in witnessesToIllegalActivity)
        {
            Game.Log(a.name + " " + a.transform.name);

            if(a.seesIllegal.ContainsKey(this))
            {
                Game.Log("Sees illegal: " + a.seesIllegal[this]);
            }
            else
            {
                Game.Log("Sees illegal missing!");
            }
        }
    }
}
