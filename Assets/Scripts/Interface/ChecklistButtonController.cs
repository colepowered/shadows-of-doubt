﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChecklistButtonController : ButtonController
{
    public Objective objective;

    public CanvasRenderer bgRend;
    public CanvasRenderer textRend;
    public CanvasRenderer progressBGrend;
    public CanvasRenderer barRend;
    public CanvasRenderer iconRend;
    public float fadeInProgress = 0f;
    public bool fadeOut = false;
    public float strikeThroughProgress = 0f;

    public Vector2 desiredAnchoredPosition;

    public Sprite checkedSprite;
    public RectTransform progressRect;
    public FlashController flash;

    public List<CanvasRenderer> childRendereres = new List<CanvasRenderer>();

    public void Setup(Objective newObjective)
    {
        SetupReferences();

        objective = newObjective;
        text.text = objective.name;

        text.ForceMeshUpdate();
        Vector2 textPreferred = text.GetPreferredValues();
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, Mathf.Max(Mathf.CeilToInt(28f + textPreferred.y), 62f));

        //Set sprite to unchecked
        icon.sprite = objective.sprite;

        //Listen for completion
        objective.OnProgressChange += OnObjectiveProgressChange;

        //Force update right away
        OnObjectiveProgressChange();

        //Add to list
        InterfaceController.Instance.objectiveList.Add(this);
    }

    public void OnObjectiveProgressChange()
    {
        InterfaceController.Instance.ActivateObjectivesDisplay();

        if(objective.progress >= 1f)
        {
            //Unsubscribe from event
            objective.OnProgressChange -= OnObjectiveProgressChange;

            //Set icon
            if(icon != null) icon.sprite = checkedSprite;

            //Flash
            if(flash != null) flash.Flash(2);
        }

        //Avoid 0 progress graphic
        if(progressRect != null)
        {
            if(objective.progress <= 0f)
            {
                progressRect.gameObject.SetActive(false);
            }
            else
            {
                //Set progress
                progressRect.offsetMax = new Vector2(-6f - ((rect.rect.width - 12f) * (1f - objective.progress)), progressRect.offsetMax.y);
                progressRect.gameObject.SetActive(true);
            }
        }

        CasePanelController.Instance.UpdateResolveNotifications();
    }

    //Triggered when the objective is complete
    public void OnComplete()
    {
        Remove();
    }

    //Triggered to remove
    public void Remove()
    {
        //Fade out
        fadeOut = true;
    }
}
