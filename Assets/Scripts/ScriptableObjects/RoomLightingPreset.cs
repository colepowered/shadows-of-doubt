﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "roomlighting_data", menuName = "Database/Room Lighting Preset")]

public class RoomLightingPreset : SoCustomComparison
{
	public bool disable = false;

	[Header("Light Objects")]
	[ReorderableList]
	public List<InteractablePreset> lightObjects = new List<InteractablePreset>();
	public LightingPreset lightingPreset;

	[Header("Room Compatibility")]
	[ReorderableList]
	public List<RoomConfiguration> roomCompatibility = new List<RoomConfiguration>();
	public int minimumRoomSize = 1;
	public int maximumRoomSize = 9999;

	[Header("Building Compatibility")]
	public List<BuildingPreset> onlyAllowInBuildings = new List<BuildingPreset>();
	public List<BuildingPreset> banFromBuildings = new List<BuildingPreset>();

	public enum StairwellLightRule { noStairwells, onlyStairwells, either};
	public StairwellLightRule stairwellRule = StairwellLightRule.noStairwells; 

	[Header("Design Style Compatibility")]
	[ReorderableList]
	public List<DesignStylePreset> designStyleCompatibility = new List<DesignStylePreset>();

	[Header("Ceiling Fan Compatibility")]
	[ReorderableList]
	public List<GameObject> ceilingFans = new List<GameObject>();

	[Header("Misc.")]
	[Tooltip("How often these appear compared to others")]
	public int frequency = 1;
}
