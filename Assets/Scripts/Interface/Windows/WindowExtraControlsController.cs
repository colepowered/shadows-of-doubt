﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using UnityEngine.EventSystems;

public class WindowExtraControlsController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public bool isOver = false;
    public float fade = 0f;
    public RawImage mouseOverDetector;

    [Header("Drawing Controls")]
    public bool drawingControlsEnabled = false;
    public RectTransform drawingControls;
    [ReorderableList]
    public List<CanvasRenderer> drawingRenderers = new List<CanvasRenderer>();

    public DrawingController drawingController;
    public ButtonController toggleDrawingButton;
    public ColourSelectorButtonController colourButton;
    public ButtonController eraserButton;
    public ButtonController clearButton;

    private void Awake()
    {
        this.enabled = false;
    }

    //Must be enabled for this to be active
    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        isOver = true;
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;

        isOver = false;
    }

    public void SetEnableDrawingControls(bool val)
    {
        drawingControlsEnabled = val;

        if(val)
        {
            mouseOverDetector.raycastTarget = true;
            this.enabled = true;

            GameObject newDrawingButtons = Instantiate(PrefabControls.Instance.drawingControls, this.transform);
            newDrawingButtons.SetActive(false); //Initialise as inactive
            drawingControls = newDrawingButtons.GetComponent<RectTransform>();

            //Fetch buttons by name
            toggleDrawingButton = drawingControls.Find("ToggleDrawingMode").gameObject.GetComponent<ButtonController>();
            colourButton = drawingControls.Find("SetDrawingColour").gameObject.GetComponent<ColourSelectorButtonController>();
            eraserButton = drawingControls.Find("ToggleEraser").gameObject.GetComponent<ButtonController>();
            clearButton = drawingControls.Find("ClearDrawing").gameObject.GetComponent<ButtonController>();

            //Add renderers
            drawingRenderers.Add(newDrawingButtons.GetComponent<CanvasRenderer>());

            drawingRenderers.Add(toggleDrawingButton.gameObject.GetComponent<CanvasRenderer>());
            drawingRenderers.Add(toggleDrawingButton.icon.GetComponent<CanvasRenderer>());

            drawingRenderers.Add(colourButton.gameObject.GetComponent<CanvasRenderer>());
            drawingRenderers.Add(colourButton.icon.GetComponent<CanvasRenderer>());

            drawingRenderers.Add(eraserButton.gameObject.GetComponent<CanvasRenderer>());
            drawingRenderers.Add(eraserButton.icon.GetComponent<CanvasRenderer>());

            drawingRenderers.Add(clearButton.gameObject.GetComponent<CanvasRenderer>());
            drawingRenderers.Add(clearButton.icon.GetComponent<CanvasRenderer>());

            toggleDrawingButton.OnPress += ToggleDrawingMode;
            colourButton.OnChangeColour += OnChangeDrawingColour;
            eraserButton.OnPress += ToggleEraser;
            clearButton.OnPress += ClearDrawing;
        }
        else
        {
            Destroy(drawingControls.gameObject);
            drawingRenderers.Clear();
            fade = 0f;
            mouseOverDetector.raycastTarget = false;
            this.enabled = false;
        }
    }

    void OnDestroy()
    {
        if(toggleDrawingButton != null) toggleDrawingButton.OnPress -= ToggleDrawingMode;
        if(colourButton != null) colourButton.OnChangeColour -= OnChangeDrawingColour;
        if(eraserButton != null) eraserButton.OnPress -= ToggleEraser;
        if(clearButton != null) clearButton.OnPress -= ClearDrawing;
    }

    private void Update()
    {
        if(isOver || (drawingController != null && drawingController.drawingActive))
        {
            if(drawingControlsEnabled)
            {
                if (drawingControls != null) drawingControls.gameObject.SetActive(true);

                if(fade < 1f)
                {
                    fade += Time.deltaTime * 8f;
                    fade = Mathf.Clamp01(fade);

                    foreach(CanvasRenderer can in drawingRenderers)
                    {
                        can.SetAlpha(fade);
                    }
                }
            }
        }
        else
        {
            if(drawingControlsEnabled)
            {
                if (fade > 0f)
                {
                    fade -= Time.deltaTime * 8f;
                    fade = Mathf.Clamp01(fade);

                    foreach (CanvasRenderer can in drawingRenderers)
                    {
                        can.SetAlpha(fade);
                    }

                    if(fade <= 0f)
                    {
                        if(drawingControls != null) drawingControls.gameObject.SetActive(false);
                    }
                }
            }
        }
    }

    //Toggle drawing mode
    public void ToggleDrawingMode(ButtonController pressedButton)
    {
        if(drawingControlsEnabled)
        {
            drawingController.SetDrawingActive(!drawingController.drawingActive);

            if (drawingController.drawingActive)
            {
                mouseOverDetector.raycastTarget = false; //Disable this so it doesn't conflict with drawing
                toggleDrawingButton.background.color = InterfaceControls.Instance.selectionColour;
            }
            else
            {
                mouseOverDetector.raycastTarget = true;
                toggleDrawingButton.background.color = InterfaceControls.Instance.nonSelectionColour;
            }
        }
    }

    public void OnChangeDrawingColour()
    {
        if (drawingControlsEnabled)
        {
            drawingController.SetBrushColour(colourButton.selectedColour);
        }
    }

    public void ToggleEraser(ButtonController pressedButton)
    {
        if (drawingControlsEnabled)
        {
            drawingController.SetEraserMode(!drawingController.eraserMode);

            if (drawingController.eraserMode)
            {
                eraserButton.background.color = InterfaceControls.Instance.selectionColour;
            }
            else
            {
                eraserButton.background.color = InterfaceControls.Instance.nonSelectionColour;
            }
        }
    }

    public void ClearDrawing(ButtonController pressedButton)
    {
        if (drawingControlsEnabled)
        {
            drawingController.ResetDrawingTexture();
        }
    }
}
