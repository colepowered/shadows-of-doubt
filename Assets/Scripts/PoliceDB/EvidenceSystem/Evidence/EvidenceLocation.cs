﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using System.Text;

public class EvidenceLocation : Evidence
{
    public NewGameLocation locationController;

    //Constructor
    public EvidenceLocation(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        //Set citizen controller
        locationController = newController as NewGameLocation;
    }

    public override void Compile()
    {
        base.Compile();

        //Merge keys for public places
        if (locationController != null)
        {
            if (locationController.thisAsAddress != null && locationController.thisAsAddress.addressPreset != null && !locationController.thisAsAddress.addressPreset.playerKnowsPurpose)
            {
                //Player doesn't know purpose of this location
            }
            else
            {
                MergeDataKeys(DataKey.name, DataKey.purpose);
                MergeDataKeys(DataKey.name, DataKey.location);

                if (locationController.thisAsStreet != null)
                {
                    MergeDataKeys(DataKey.name, DataKey.photo);
                    MergeDataKeys(DataKey.name, DataKey.blueprints);
                }
            }
        }
    }

    public override void MergeDataKeys(DataKey keyOne, DataKey keyTwo)
    {
        //Game.Log("Merging keys " + keyOne.ToString() + " and " + keyTwo.ToString() + " for " + locationController.name);
        base.MergeDataKeys(keyOne, keyTwo);
    }

    //Triggered when the player arrives here
    public void OnPlayerArrival()
    {
        if (!keyTies[DataKey.name].Contains(DataKey.purpose))
        {
            MergeDataKeys(DataKey.name, DataKey.purpose);
        }

        if (!keyTies[DataKey.name].Contains(DataKey.location))
        {
            MergeDataKeys(DataKey.name, DataKey.location);
        }

        if (!keyTies[DataKey.name].Contains(DataKey.blueprints))
        {
            MergeDataKeys(DataKey.name, DataKey.blueprints);
        }

        if (!keyTies[DataKey.name].Contains(DataKey.photo))
        {
            MergeDataKeys(DataKey.name, DataKey.photo);
        }

        if (locationController.mapButton != null)
        {
            MapController.Instance.AddUpdateCall(locationController.mapButton);
        }
    }

    //Get note with text composed with the data reference from this evidence
    public override string GetNoteComposed(List<Evidence.DataKey> keys, bool useLinks = true)
    {
        Strings.LinkSetting links = Strings.LinkSetting.forceNoLinks;
        if (useLinks) links = Strings.LinkSetting.forceLinks;
        return Strings.ComposeText(GetNote(keys), locationController, links, evidenceKeys: keys);
    }

    public override string GetSummary(List<DataKey> keys)
    {
        if(locationController.thisAsAddress != null)
        {
            return Strings.GetTextForComponent("ea090069-a4a6-45d4-b20b-362146677052", locationController, dataKeys: keys);
        }
        else if(locationController.thisAsStreet != null)
        {
            return Strings.GetTextForComponent("5dd6c421-d4e1-4c53-afab-097cc1c6df13", locationController, dataKeys: keys);
        }

        return base.GetSummary(keys);
    }

    //Name
    public override string GenerateName()
    {
         return controller.name;
    }

    //Override name variants
    public override string GetNameForDataKey(List<DataKey> inputKeys)
    {
        string ret = string.Empty;

        try
        {
            if (inputKeys == null) return Strings.Get("evidence.names", preset.name);

            List<DataKey> keys = GetTiedKeys(inputKeys);

            //Return custom names first
            if (customNames.Count > 0)
            {
                foreach (CustomName n in customNames)
                {
                    if (keys.Contains(n.key))
                    {
                        return n.name;
                    }
                }
            }

            ret = string.Empty;

            List<DataKey> nameKeys = GetTiedKeys(DataKey.name);

            //Return name
            if (keys.Contains(DataKey.name) || nameKeys.Exists(item => inputKeys.Contains(item)))
            {
                return name;
            }
            else
            {
                //'Unknown'
                ret = Strings.Get("evidence.generic", "Unknown", Strings.Casing.firstLetterCaptial) + " " + Strings.Get("evidence.generic", "Address", Strings.Casing.firstLetterCaptial);
            }

            return ret;
        }
        catch
        {
            return ret;
        }
    }

    //Return a list of know info 
    public override string GetNote(List<DataKey> keys)
    {
        StringBuilder sb = new StringBuilder();

        List<DataKey> tiedKeys = GetTiedKeys(keys);

        string checkboxEmpty = "<sprite=\"icons\" name=\"Checkbox Empty\">";
        string checkboxChecked = "<sprite=\"icons\" name=\"Checkbox Checked\">";
        string handwritingFont = "<font=\"PapaManAOE SDF\">";
        string fontEnd = "</font>";

        string checkbox = string.Empty;

        //Blueprints/type
        if (tiedKeys.Contains(DataKey.purpose)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append(checkbox + Strings.Get("descriptors", "Type", Strings.Casing.firstLetterCaptial) + ": ");
        if (tiedKeys.Contains(DataKey.purpose))
        {
            sb.Append(handwritingFont + "|type|" + fontEnd);
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Location
        if (tiedKeys.Contains(DataKey.location)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n"+checkbox + Strings.Get("descriptors", "Location", Strings.Casing.firstLetterCaptial) + ": ");
        if (tiedKeys.Contains(DataKey.location))
        {
            sb.Append(handwritingFont + "|building|" + fontEnd);
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Blueprints
        if(locationController != null && locationController.thisAsAddress != null)
        {
            if (tiedKeys.Contains(DataKey.blueprints)) checkbox = checkboxChecked;
            else checkbox = checkboxEmpty;

            sb.Append("\n" + checkbox + Strings.Get("descriptors", "Blueprints", Strings.Casing.firstLetterCaptial) + ": ");
            if (tiedKeys.Contains(DataKey.blueprints))
            {
                sb.Append(handwritingFont + Strings.Get("descriptors", "Yes", Strings.Casing.firstLetterCaptial) + fontEnd);
            }
            else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        }

        //Other notes
        //sb.Append("\n" + Strings.Get("descriptors", "Additional Notes", Strings.Casing.firstLetterCaptial) + ": \n" + handwritingFont);


        return sb.ToString();
    }
}
