using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KettleBehaviour : SwitchSyncBehaviour
{
    [ColorUsageAttribute(true, true)]
    public Color steamColour;
    public Renderer rend;

    private void Update()
    {
        if (isOn)
        {
            Color sCol = steamColour;
            sCol.a = Mathf.Lerp(0f, 1f, syncInteractable.interactable.cs);

            rend.material.SetColor("Color_C6D75069", sCol);
        }
        else
        {
            if (syncInteractable.interactable.cs <= 0f)
            {
                foreach (GameObject obj in basicBehaviourObjects)
                {
                    obj.SetActive(isOn);
                }
            }
            else
            {
                Color sCol = steamColour;
                sCol.a = Mathf.Lerp(0f, 1f, syncInteractable.interactable.cs);

                rend.material.SetColor("Color_C6D75069", sCol);
            }
        }
    }

    public override void SetOn(bool val)
    {
        if (inverted) val = !val;
        isOn = val;

        if (syncInteractable != null)
        {
            if (syncWithState == InteractablePreset.Switch.switchState)
            {
                syncInteractable.interactable.SetSwitchState(isOn, null);
            }
            else if (syncWithState == InteractablePreset.Switch.custom1)
            {
                syncInteractable.interactable.SetCustomState1(isOn, null);
            }
            else if (syncWithState == InteractablePreset.Switch.custom2)
            {
                syncInteractable.interactable.SetCustomState2(isOn, null);
            }
            else if (syncWithState == InteractablePreset.Switch.custom3)
            {
                syncInteractable.interactable.SetCustomState3(isOn, null);
            }
            else if (syncWithState == InteractablePreset.Switch.lockState)
            {
                syncInteractable.interactable.SetLockedState(isOn, null);
            }
            else if (syncWithState == InteractablePreset.Switch.carryPhysicsObject)
            {
                syncInteractable.interactable.SetPhysicsPickupState(isOn, null);
            }
        }

        if (basicBehaviour == BasicBehaviour.hideWhenOn)
        {
            foreach (GameObject obj in basicBehaviourObjects)
            {
                obj.SetActive(!isOn);
            }
        }
        else if (basicBehaviour == BasicBehaviour.hideWhenOff)
        {
            if(syncInteractable.interactable.cs <= 0f)
            {
                foreach (GameObject obj in basicBehaviourObjects)
                {
                    obj.SetActive(isOn);
                }
            }
        }
    }
}
