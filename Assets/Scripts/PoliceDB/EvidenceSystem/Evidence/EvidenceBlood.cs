﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class EvidenceBlood : Evidence
{
    public Citizen citizenController;
    //public MedicalData medicalData;

    //Constructor
    public EvidenceBlood(EvidencePreset newPreset, string newID, Controller newController, List<object> newPassedObjects) : base(newPreset, newID, newController, newPassedObjects)
    {
        citizenController = newController as Citizen;
        //if(citizenController != null) medicalData = citizenController.medicalData;
    }

    //Name blood group
    public override string GenerateNameSuffix()
    {
        return citizenController.GetBloodTypeString();
    }
}
