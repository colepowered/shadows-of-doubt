﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "firstperson_data", menuName = "Database/First Person Item")]

public class FirstPersonItem : SoCustomComparison
{
    public enum SpecialAction { none, block, handcuff, takedown, punch, consumeTrue, consumeFalse, putDown, attack, raiseTrue, raiseFalse, takePicture, placeCodebreaker, placeDoorWedge, takeOne, placeFurniture, cancelFurniture, give, placeTracker, placeFlashbomb, placeIncapacitator, takeBriefcaseCash, openBriefcaseBomb, rotateFurnLeft, rotateFurnRight};

    [Header("Setup")]
    [Tooltip("Priority of this within the inventory hierarchy.")]
    public int slotPriority = 0;
    [Tooltip("Should the arm models be displayed at all?")]
    public bool modelActive = true;
    public AnimationClip idleClip;
    public Sprite selectionIcon;
    public string summaryMsgID;
    public string triggerTutorial;
    public bool disableBracketDisplayName = false;

    [Header("Animation")]
    [Tooltip("How fast to play the draw animation")]
    public float drawSpeed = 2f;
    [Tooltip("How fast to play the holster animation")]
    public float holsterSpeed = 2.5f;

    [Header("Objects")]
    public GameObject leftHandObject;
    public GameObject rightHandObject;
    public Vector3 spawnScale = Vector3.one;
    public bool useFoodSlotItem = false;
    public bool useAlternateTrashObjects = false;
    [EnableIf("useAlternateTrashObjects")]
    public GameObject leftHandObjectTrash;
    [EnableIf("useAlternateTrashObjects")]
    public GameObject rightHandObjectTrash;

    [Header("Interaction")]
    [Tooltip("Setup of actions able to be performed")]
    [ReorderableList]
    public List<FPSInteractionAction> actions = new List<FPSInteractionAction>();

    [System.Serializable]
    public class FPSInteractionAction : InteractablePreset.InteractionAction
    {
        [Space(7)]
        public AttackAvailability availability = AttackAvailability.never;
        public float attackMainSpeed = 2f;
        public PlayerTransitionPreset attackTrasition;
        [Tooltip("Minimum time between possible attacks: You might want to match this with the attack animation length")]
        public float attackDelay = 1f;
        public SpecialAction mainSpecialAction = SpecialAction.none;
        public bool mainUseSpecialColour = false;
        public Color mainSpecialColour = Color.black;
        public AudioEvent attackEvent;
        public bool useCameraJolt = false;
        public Vector2 joltXRange;
        public Vector2 joltYRange;
        public Vector2 joltZRange;
        public float joltAmplitude = 1f;
        public float joltSpeed = 1f;
    }

    public enum AttackAvailability { never, always, handcuffs, behindCitizen, onConsuming, onNotConsuming, onNotConsumingButLeftovers, nearPutDown, onRaised, onNotRaised, codebreaker, doorWedge, giveItem, tracker, onRaisedButLeftovers };

    [Tooltip("How this impacts nerve levels of a citizen if drawn")]
    public float drawnNerveModifier = 0f;
    [Tooltip("Chance of bark trigger")]
    public float barkTriggerChance = 0f;
    public SpeechController.Bark bark = SpeechController.Bark.threatenByItem;

    [Header("Compatibility")]
    public bool compatibleWithLockedIn = true;
    public bool compatibleWithHidden = true;

    [Header("Audio")]
    public float equipSoundDelay = 0f;
    public AudioEvent equipEvent;
    public float holsterSoundDelay = 0f;
    public AudioEvent holsterEvent;
    public AudioEvent activeLoop;
}
