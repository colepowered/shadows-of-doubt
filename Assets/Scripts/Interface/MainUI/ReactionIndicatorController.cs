﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ReactionIndicatorController : MonoBehaviour
{
    public RectTransform rect;
    public RectTransform bubbleRect;
    public Image img;
    public float distance = 0f;

    public float fadeProgress = 0f;

    public Actor actor;

    public InterfaceController.AwarenessIcon awarenessIcon;

    private NewAIController.ReactionState previousReactionState = NewAIController.ReactionState.none;

    public List<CanvasRenderer> graphics = new List<CanvasRenderer>();

    public Vector2 bubbleDesiredSize = Vector2.zero;
    public bool displayOnScreen = true;
    public Vector3 desiredPosition = Vector3.zero;

    private bool firstPositionInit = false; //We need to position this properly on the first frame

    [Header("Removal")]
    public float removalProgress = 0f;
    public bool removeHit = false;
    public bool removeBlocked = false;
    public bool removeFade = false;
    public float abortProgress = 0f;

    public void Setup(Actor newActor)
    {
        actor = newActor;
        bubbleRect.sizeDelta = new Vector2(1, 1);
    }

    public void UpdateReactionType()
    {
        //Trigger awareness alert
        if(actor.ai.reactionState != previousReactionState)
        {
            if(actor.ai.reactionState == NewAIController.ReactionState.investigatingSight || actor.ai.reactionState == NewAIController.ReactionState.investigatingSound)
            {
                if (awarenessIcon != null) awarenessIcon.TriggerAlert();
            }

            previousReactionState = actor.ai.reactionState;
        }

        if(actor.ai.reactionState == NewAIController.ReactionState.investigatingSight)
        {
            img.sprite = InterfaceControls.Instance.reactionInvestigateSightSprite;
            if (awarenessIcon != null) awarenessIcon.SetTexture(InterfaceControls.Instance.reactionInvestigateSightTex);
        }
        else if(actor.ai.reactionState == NewAIController.ReactionState.investigatingSound)
        {
            img.sprite = InterfaceControls.Instance.reactionInvestigateSoundSprite;
            if (awarenessIcon != null) awarenessIcon.SetTexture(InterfaceControls.Instance.reactionInvestigateSoundTex);
        }
        else if(actor.ai.reactionState == NewAIController.ReactionState.persuing)
        {
            img.sprite = InterfaceControls.Instance.reactionPersueSprite;
            if (awarenessIcon != null) awarenessIcon.SetTexture(InterfaceControls.Instance.reactionPersueTex);
        }
        else if (actor.ai.reactionState == NewAIController.ReactionState.searching)
        {
            img.sprite = InterfaceControls.Instance.reactionSearchSprite;
            if (awarenessIcon != null) awarenessIcon.SetTexture(InterfaceControls.Instance.reactionSearchTex);
        }
        //Avoid
        else
        {
            img.sprite = InterfaceControls.Instance.reactionAvoidSprite;
            if (awarenessIcon != null) awarenessIcon.SetTexture(InterfaceControls.Instance.reactionAvoidTex);
        }
    }

    void Update()
    {
        //Animate
        if (SessionData.Instance.play)
        {
            //Get angle from player...
            Vector3 forwardRelative = Vector3.zero;
            float angleBetween = 0;

            forwardRelative = (actor.lookAtThisTransform.position + new Vector3(0, 0.52f, 0)) - Player.Instance.transform.position;
            angleBetween = Vector3.SignedAngle(forwardRelative, Player.Instance.transform.forward, Vector3.up);

            //Display on screen position or in the anchor display...
            if (Mathf.Abs(angleBetween) < 75f)
            {
                //Screen position
                if (!displayOnScreen)
                {
                    displayOnScreen = true;
                }

                //Update distance
                distance = Vector3.Distance(Player.Instance.lookAtThisTransform.position, actor.lookAtThisTransform.position);
                float distanceRel = 1f - Mathf.Clamp01(distance / InterfaceControls.Instance.maxIndicatorDistance);

                Vector3 screenPoint = CameraController.Instance.cam.WorldToScreenPoint(actor.lookAtThisTransform.position + new Vector3(0, 0.52f, 0)); //Get point above their head and convert it to a screen position
                Vector2 rectPos;
                RectTransformUtility.ScreenPointToLocalPointInRectangle(InterfaceController.Instance.firstPersonUI, screenPoint, null, out rectPos);

                //Set position
                desiredPosition = InterfaceControls.Instance.speechBubbleParent.TransformPoint(rectPos);

                //Fade with distance and visible to player state...
                float alpha = Mathf.Clamp01(distanceRel * actor.spottedState);

                foreach (CanvasRenderer gfx in graphics)
                {
                    gfx.SetAlpha(alpha);
                }

                //Set outline alpha
                if(actor.outline != null)
                {
                    actor.outline.SetAlpha(Mathf.Clamp01(alpha * 2f));
                }

                //Postion properly on the first frame
                if (!firstPositionInit)
                {
                    rect.position = desiredPosition;
                    //bubbleRect.sizeDelta = bubbleDesiredSize;

                    firstPositionInit = true;
                }
                else
                {
                    //This handles the positioning lag
                    if (desiredPosition != rect.position)
                    {
                        //rect.position = Vector2.Lerp(rect.position, desiredPosition, Time.deltaTime * 16f * SessionData.Instance.currentTimeMultiplier);
                        rect.position = desiredPosition;
                    }

                    //This handles the lag between sizing the bubble background
                    if (bubbleDesiredSize != bubbleRect.sizeDelta)
                    {
                        bubbleRect.sizeDelta = Vector2.Lerp(bubbleRect.sizeDelta, bubbleDesiredSize, Time.deltaTime * 9f * SessionData.Instance.currentTimeMultiplier);
                    }
                }

                //Set scale
                float scale = Mathf.Lerp(InterfaceControls.Instance.indicatorMinMaxScale.x, InterfaceControls.Instance.indicatorMinMaxScale.y, distanceRel);
                rect.localScale = new Vector3(scale, scale, scale);

                if (actor.isDead) removeFade = true; //Remove if dead

                //Removal animations
                if (removeFade)
                {
                    removalProgress += Time.deltaTime * 4f;

                    //Fade out
                    foreach (CanvasRenderer r in graphics)
                    {
                        r.SetAlpha(alpha - removalProgress);
                    }

                    //Set outline alpha
                    if (actor.outline != null)
                    {
                        actor.outline.SetAlpha(Mathf.Clamp01(Mathf.Clamp01(alpha * 2f) - removalProgress));
                    }

                    //Remove object
                    if (removalProgress >= 1f)
                    {
                        if (actor.outline != null) actor.outline.SetOutlineActive(false);
                        if (awarenessIcon != null) awarenessIcon.Remove();
                        Destroy(this.gameObject);
                    }
                }
            }
            else
            {
                if (displayOnScreen)
                {
                    displayOnScreen = false;

                    foreach (CanvasRenderer gfx in graphics)
                    {
                        gfx.SetAlpha(0f);
                    }
                }

                //Remove immediately
                if(removeFade)
                {
                    if (actor.outline != null) actor.outline.SetOutlineActive(false);
                    if (awarenessIcon != null) awarenessIcon.Remove();
                    Destroy(this.gameObject);
                }
            }
        }
    }
}
