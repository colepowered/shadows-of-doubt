using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashController : MonoBehaviour
{
    public Image blackBG;
    public List<SplashImage> splashes = new List<SplashImage>();
    public float progress = 0f;
    public int splash = 0;
    public float fadeOutTime = 1.25f;
    public bool fadeOut = false;
    public float fadeProg = 1f;

    [System.Serializable]
    public class SplashImage
    {
        public RectTransform rect;
        public CanvasRenderer rend;
        public float displayTime = 1.5f;
    }

    private void Awake()
    {
        foreach(SplashImage i in splashes)
        {
            i.rend.SetAlpha(0f);
            i.rect.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(fadeOut)
        {
            if(fadeProg > 0f)
            {
                fadeProg -= Time.deltaTime / fadeOutTime;
                fadeProg = Mathf.Clamp01(fadeProg);
                blackBG.canvasRenderer.SetAlpha(fadeProg);
            }
            else
            {
                MainMenuController.Instance.SetMenuComponent(MainMenuController.Component.mainMenuButtons);
                this.enabled = false;
            }
        }
        else
        {
            //Exit splash loop
            if(splash >= splashes.Count)
            {
                fadeOut = true;
            }
            else
            {
                //Fade in
                SplashImage current = splashes[splash];
                if (!current.rect.gameObject.activeSelf) current.rect.gameObject.SetActive(true);

                 progress += Time.deltaTime / current.displayTime;

                if (progress <= 0.1f)
                {
                    current.rend.SetAlpha(progress * 10f);
                }
                else if(progress >= 0.9f)
                {
                    current.rend.SetAlpha((1f - progress) * 10f);
                }
                else
                {
                    current.rend.SetAlpha(1f);
                }

                if(progress >= 1f)
                {
                    current.rect.gameObject.SetActive(false);
                    progress = 0f;
                    splash++;
                }
            }
        }
    }
}
