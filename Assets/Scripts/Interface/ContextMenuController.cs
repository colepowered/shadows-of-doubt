﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using NaughtyAttributes;

public class ContextMenuController : MonoBehaviour, IPointerClickHandler
{
    [Header("Usage")]
    public bool useLeftButton = false;

    [Header("Positioning & Size")]
    public Vector2 pos = new Vector2(0f, -0.5f);
    public bool useCursorPos = false;
    [EnableIf("useCursorPos")]
    public Vector2 cursorPosOffset = new Vector2(10f, -7f);

    public bool useGlobalWidth = true;
    [DisableIf("useGlobalWidth")]
    public float width = 190f;

    [Header("Configuration")]
    public MenuFlag flag = MenuFlag.none;
    public enum MenuFlag { none, pinnedSelected};
    [ReorderableList]
    public List<ContextMenuButtonSetup> menuButtons = new List<ContextMenuButtonSetup>();

    [Tooltip("Disabled items")]
    public List<string> disabledItems = new List<string>();

    [System.Serializable]
    public class ContextMenuButtonSetup
    {
        public string commandString;
        [Space(5)]
        public bool useText = true;
        public string overrideText;
        [Space(5)]
        public bool useColour = false;
        public Color colour;
        [Space(5)]
        public bool devOnly = false;
    }

    public MonoBehaviour commandObject;

    [Header("Spawned")]
    public static ContextMenuController activeMenu = null;
    public ContextButtonController lastButton; //Last clicked-on button can be used for reference in the actions

    public GameObject spawnedMenu;
    private RectTransform menuRect;

    public delegate void OpenedMenu();
    public event OpenedMenu OnOpenMenu;

    public void OnPointerClick(PointerEventData eventData)
    {
        if(InputController.Instance.mouseInputMode)
        {
            if(eventData.button == PointerEventData.InputButton.Right)
            {
                if(!useLeftButton)
                {
                    OpenMenu();
                }
            }

            if (eventData.button == PointerEventData.InputButton.Left)
            {
                if (useLeftButton)
                {
                    OpenMenu();
                }
            }
        }
    }

    public void OpenMenu()
    {
        if (spawnedMenu == null)
        {
            if (activeMenu != null) activeMenu.ForceClose();

            //Sfx
            AudioController.Instance.Play2DSound(AudioControls.Instance.itemEditAppear);

            //Check for entries
            int validEntries = 0;

            foreach (ContextMenuController.ContextMenuButtonSetup e in menuButtons)
            {
                //Check for disabled item
                if (disabledItems.Contains(e.commandString)) continue;
                if (e.devOnly && !Game.Instance.devMode) continue;
                validEntries++;
            }

            if (validEntries <= 0) return; //Abort if no entries in menu

            activeMenu = this;

            //Close currently active tooltip
            TooltipController.RemoveActiveTooltip();

            spawnedMenu = Instantiate(PrefabControls.Instance.contextMenuPanel, this.transform);
            ContextMenuPanelController cmpc = spawnedMenu.GetComponent<ContextMenuPanelController>();
            cmpc.Setup(this);

            menuRect = spawnedMenu.GetComponent<RectTransform>();

            //Position relative to parent
            RectTransform thisRect = this.gameObject.GetComponent<RectTransform>();

            if (useCursorPos && InputController.Instance.mouseInputMode)
            {
                Vector2 localCursor = new Vector2(pos.x * thisRect.sizeDelta.x, pos.y * thisRect.sizeDelta.y);

                RectTransformUtility.ScreenPointToLocalPointInRectangle(thisRect, ClampToWindow(Input.mousePosition), null, out localCursor);
                menuRect.localPosition = localCursor;
                menuRect.position += new Vector3(cursorPosOffset.x, cursorPosOffset.y, 0);
            }
            else
            {
                menuRect.position = this.transform.TransformPoint(new Vector2(pos.x * thisRect.sizeDelta.x, pos.y * thisRect.sizeDelta.y));
            }

            //Set the parent to the container
            menuRect.SetParent(PrefabControls.Instance.contextMenuContainer);
            menuRect.rotation = Quaternion.identity;
            menuRect.localScale = Vector2.one;

            //Clamp onscreen
            menuRect.localPosition = new Vector2(Mathf.Clamp(menuRect.localPosition.x, Screen.width * -0.5f, Screen.width * 0.5f - menuRect.sizeDelta.x),
                                            Mathf.Clamp(menuRect.localPosition.y, Screen.height * -0.5f + menuRect.sizeDelta.y, Screen.height * 0.5f));

            //Now change parent to the top level canvas, so nothing appears above the tooltip
            menuRect.SetAsLastSibling();

            //Fire event
            if(OnOpenMenu != null)
            {
                OnOpenMenu();
            }
        }
    }

    public void OnCommand(ContextButtonController button)
    {
        //Set last button
        lastButton = button;

        //Invoke
        commandObject.Invoke(button.setup.commandString, 0);
        
        ForceClose();
    }

    public void ForceClose()
    {
        Game.Log("Interface: Force close context menu");

        Destroy(spawnedMenu);

        //Set active menu as null
        activeMenu = null;
    }

    Vector2 ClampToWindow(Vector2 rawPointerPosition)
    {
        Vector3[] canvasCorners = new Vector3[4];
        PrefabControls.Instance.tooltipsCanvas.GetWorldCorners(canvasCorners);

        float clampedX = Mathf.Clamp(rawPointerPosition.x, canvasCorners[0].x, canvasCorners[2].x - menuRect.sizeDelta.x);
        float clampedY = Mathf.Clamp(rawPointerPosition.y, canvasCorners[0].y, canvasCorners[2].y);

        Vector2 newPointerPosition = new Vector2(clampedX, clampedY);
        return newPointerPosition;
    }

    public void SetScreenPosition(Vector2 pointerPosition)
    {
        if (spawnedMenu == null) return;

        //Clamp
        pointerPosition = ClampToWindow(pointerPosition);

        Game.Log("Clamped pointer: " + pointerPosition);

        Vector2 localPointerPosition;

        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
            PrefabControls.Instance.contextMenuContainer, pointerPosition, null, out localPointerPosition
        ))
        {
            Game.Log("Local: " + localPointerPosition);
            menuRect.localPosition = localPointerPosition;
        }
    }
}
