﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowViewpointCreator : Creator
{
    //Chunks to load per frame
    public int loadChunk = 2;

    //Singleton pattern
    private static WindowViewpointCreator _instance;
    public static WindowViewpointCreator Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public override void StartLoading()
    {
        Game.Log("CityGen: Setting up window viewpoints...");
        StartCoroutine("Load");
    }

    IEnumerator Load()
    {
        int roomCursor = 0;

        while(roomCursor < CityData.Instance.roomDirectory.Count)
        {
            for (int u = 0; u < loadChunk; u++)
            {
                foreach (NewNode node in CityData.Instance.roomDirectory[roomCursor].nodes)
                {
                    List<NewWall> windows = node.walls.FindAll(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.window || item.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge);

                    foreach (NewWall window in windows)
                    {
                        //window.GetWindowViewTiles();
                    }
                }

                roomCursor++;

                if (roomCursor >= CityData.Instance.roomDirectory.Count) break;
            }

            CityConstructor.Instance.loadingProgress = Mathf.Min((float)roomCursor / (float)CityData.Instance.roomDirectory.Count, 0.99f) * 0.9f;
            yield return null;
        }

        roomCursor = 0;

        while (roomCursor < CityData.Instance.streetDirectory.Count)
        {
            for (int u = 0; u < loadChunk; u++)
            {
                //CityData.Instance.streetDirectory[roomCursor].GetVisibleStreets();

                roomCursor++;

                if (roomCursor >= CityData.Instance.streetDirectory.Count) break;
            }

            CityConstructor.Instance.loadingProgress = 0.9f + (Mathf.Min((float)roomCursor / (float)CityData.Instance.streetDirectory.Count, 0.99f) * 0.1f);
            yield return null;
        }

        //Remove street level colliders
        //foreach(NewBuilding building in CityData.Instance.buildingDirectory)
        //{
        //    if(building.streetLevelCollisionDetector != null)
        //    {
        //        Destroy(building.streetLevelCollisionDetector);
        //    }
        //}

        //Complete!
        SetComplete();
    }
}
