﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchSyncBehaviour : MonoBehaviour
{
    [Header("State")]
    public InteractablePreset.Switch syncWithState = InteractablePreset.Switch.switchState;
    public bool isOn = false;
    public bool inverted = false;

    [Header("Basic Behaviour")]
    public BasicBehaviour basicBehaviour = BasicBehaviour.none;
    public List<GameObject> basicBehaviourObjects = new List<GameObject>();
    public enum BasicBehaviour { none, hideWhenOn, hideWhenOff};
    [Tooltip("Sync this interactable")]
    public InteractableController syncInteractable;
    public bool onlySyncWhenParentIsOn = false;

    public virtual void SetOn(bool val)
    {
        if (inverted) val = !val;
        isOn = val;

        if(syncInteractable != null && syncInteractable.interactable != null)
        {
            if (syncWithState == InteractablePreset.Switch.switchState)
            {
                syncInteractable.interactable.SetSwitchState(isOn, null);
            }
            else if (syncWithState == InteractablePreset.Switch.custom1)
            {
                syncInteractable.interactable.SetCustomState1(isOn, null);
            }
            else if (syncWithState == InteractablePreset.Switch.custom2)
            {
                syncInteractable.interactable.SetCustomState2(isOn, null);
            }
            else if (syncWithState == InteractablePreset.Switch.custom3)
            {
                syncInteractable.interactable.SetCustomState3(isOn, null);
            }
            else if (syncWithState == InteractablePreset.Switch.lockState)
            {
                syncInteractable.interactable.SetLockedState(isOn, null);
            }
            else if (syncWithState == InteractablePreset.Switch.carryPhysicsObject)
            {
                syncInteractable.interactable.SetPhysicsPickupState(isOn, null);
            }
        }

        if(basicBehaviour == BasicBehaviour.hideWhenOn)
        {
            foreach(GameObject obj in basicBehaviourObjects)
            {
                if (obj == null) continue;
                obj.SetActive(!isOn);
            }
        }
        else if(basicBehaviour == BasicBehaviour.hideWhenOff)
        {
            foreach (GameObject obj in basicBehaviourObjects)
            {
                if (obj == null) continue;
                obj.SetActive(isOn);
            }
        }
    }
}
