﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CityInfoController : MonoBehaviour
{
	public Text text;
	
	// Use this for initialization
	void Start ()
	{
		UpdateSelf();	
	}

	public void UpdateSelf()
	{
		text.text = "";

		float per = 0f;

		//City size
		text.text += Strings.Get("ui.interface", "City Size") + ": " + CityData.Instance.citySize.x.ToString() + " x " + CityData.Instance.citySize.y.ToString() + '\n';

		//Building counts
		per = Mathf.RoundToInt((float)CityData.Instance.residentialBuildings/(float)CityData.Instance.buildingDirectory.Count * 100f);
		text.text += Strings.Get("ui.interface", "Residential Buildings") + ": " + CityData.Instance.residentialBuildings.ToString() + " (" + per.ToString() + "%)" + '\n';

		per = Mathf.RoundToInt((float)CityData.Instance.commercialBuildings/(float)CityData.Instance.buildingDirectory.Count * 100f);
		text.text += Strings.Get("ui.interface", "Commercial Buildings") + ": " + CityData.Instance.commercialBuildings.ToString() + " (" + per.ToString() + "%)" + '\n';

		per = Mathf.RoundToInt((float)CityData.Instance.industrialBuildings/(float)CityData.Instance.buildingDirectory.Count * 100f);
		text.text += Strings.Get("ui.interface", "Industrial Buildings") + ": " + CityData.Instance.industrialBuildings.ToString() + " (" + per.ToString() + "%)" + '\n';

		per = Mathf.RoundToInt((float)CityData.Instance.municipalBuildings/(float)CityData.Instance.buildingDirectory.Count * 100f);
		text.text += Strings.Get("ui.interface", "Municipal Buildings") + ": " + CityData.Instance.municipalBuildings.ToString() + " (" + per.ToString() + "%)" + '\n';

		per = Mathf.RoundToInt((float)CityData.Instance.parkBuildings/(float)CityData.Instance.buildingDirectory.Count * 100f);
		text.text += Strings.Get("ui.interface", "Park Buildings") + ": " + CityData.Instance.parkBuildings.ToString() + " (" + per.ToString() + "%)" + '\n';

		text.text += '\n';

		//Population
		text.text += Strings.Get("ui.interface", "Population") + ": " + CityData.Instance.citizenDirectory.Count.ToString() + '\n';

		per = Mathf.RoundToInt((float)CityData.Instance.inhabitedResidences/(float)CityData.Instance.residenceDirectory.Count * 100f);
		text.text += Strings.Get("ui.interface", "Residences/Inhabited") + ": " + CityData.Instance.residenceDirectory.Count.ToString() + "/" + CityData.Instance.inhabitedResidences.ToString() + " (" + per.ToString() + "%)" + '\n';

		per = Mathf.RoundToInt((float)CityData.Instance.employedCitizens/(float)CityData.Instance.jobsDirectory.Count * 100f);
		text.text += Strings.Get("ui.interface", "Employed/Jobs") + ": " + CityData.Instance.employedCitizens.ToString() + "/" + CityData.Instance.jobsDirectory.Count.ToString() + " (" + per.ToString() + "%)" + '\n';

		per = Mathf.RoundToInt((float)CityData.Instance.unemployedDirectory.Count/(float)CityData.Instance.citizenDirectory.Count * 100f);
		text.text += Strings.Get("ui.interface", "Unemployed") + ": " + CityData.Instance.unemployedDirectory.Count.ToString() + " (" + per.ToString() + "%)" + '\n';

		text.text += Strings.Get("ui.interface", "Extra Unemployed Created") + ": " + CityData.Instance.extraUnemloyedCreated.ToString() + '\n';
	}
}
