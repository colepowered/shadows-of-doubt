﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClassIconButtonController : ButtonController
{
    [System.NonSerialized]
    public Fact connectionFact;
    [System.NonSerialized]
    public Evidence evidenceEntry;

    public void Setup(Evidence newEvidenceEntry, InfoWindow newParentWindow, Fact newFact)
    {
        evidenceEntry = newEvidenceEntry;
        parentWindow = newParentWindow;

        SetupReferences();
        connectionFact = newFact;

        //Listen for changes in the fact's connections' data keys
        if(connectionFact != null)
        {
            connectionFact.OnConnectingEvidenceChangeDataKey += VisualUpdate;
            connectionFact.OnNewName += UpdateTooltipText;
        }

        evidenceEntry.OnDataKeyChange += VisualUpdate;

        VisualUpdate();
    }

    public override void VisualUpdate()
    {
        icon.sprite = evidenceEntry.GetIcon();

        UpdateTooltipText();
    }

    private void OnDestroy()
    {
        if (connectionFact != null)
        {
            connectionFact.OnConnectingEvidenceChangeDataKey -= VisualUpdate;
            connectionFact.OnNewName -= UpdateTooltipText;
        }

        evidenceEntry.OnDataKeyChange -= VisualUpdate;
    }

    //On double click, open evidence
    public override void OnLeftDoubleClick()
    {
        //foreach(Evidence ev in connectionFact.fromEvidence)
        //{
        //    if (ev == parentWindow.passedEvidence) continue;
        //    InterfaceController.Instance.SpawnWindow(ev, passedEvidenceKeys: connectionFact.fromDataKeys);
        //}

        //foreach (Evidence ev in connectionFact.toEvidence)
        //{
        //    if (ev == parentWindow.passedEvidence) continue;
        //    InterfaceController.Instance.SpawnWindow(ev, passedEvidenceKeys: connectionFact.toDataKeys);
        //}
    }

    public override void UpdateTooltipText()
    {
        if (connectionFact != null)
        {
            tooltip.mainText = evidenceEntry.GetNameForDataKey(connectionFact.fromDataKeys);
            tooltip.detailText = connectionFact.GetName();
        }
        else
        {
            tooltip.mainText = string.Empty;
            tooltip.detailText = string.Empty;
        }
    }
}
