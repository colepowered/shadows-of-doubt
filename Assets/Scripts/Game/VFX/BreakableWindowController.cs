using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class BreakableWindowController : MonoBehaviour
{
    public bool isBroken;
    public bool isBoarded;
    [ReadOnly]
    public float brokenAt;
    public float breakForce = 0.12f;
    public AudioEvent breakAudioEvent;

    [System.Serializable]
    public class WindowBreakSetting
    {
        public MeshFilter filter;
        public MeshRenderer renderer;
        [Space(5)]
        public Material defaultMat;
        public Material brokenMat;
        public Material boardedMat;
        [Space(5)]
        public Mesh defaultMesh;
        public Mesh brokenMesh;
        public Mesh boardedMesh;
        [Space(5)]
        public Collider collider;
        public bool removeColliderWhenBroken = true;
        [Space(5)]
        public bool removeWhenBoarded = false;
        public bool shatter = false;
        [EnableIf("shatter")]
        public Vector3 shardSize = new Vector3(0.065f, 0.065f, 0.065f);
        [EnableIf("shatter")]
        public int shardEveryXPixels = 18;
        [EnableIf("shatter")]
        public float shatterForceMultiplier = 2.4f;
    }

    public List<WindowBreakSetting> panes = new List<WindowBreakSetting>();

    public void InteractableCollision(Collision collision, float damage, Actor brokenBy)
    {
        if (isBroken) return;

        //Damage force is velocity * mass
        Game.Log("Object: " + name + " Collides with window " + name + " with force: " + damage);

        if (damage >= breakForce)
        {
            ContactPoint cp = collision.GetContact(0);

            BreakWindow(cp.point, collision.relativeVelocity, brokenBy);

            //Alarm!
            if(brokenBy != null && brokenBy.isPlayer)
            {
                InteractionController.Instance.SetIllegalActionActive(true);
            }
        }
    }

    public Vector3 GetAveragePosition()
    {
        Vector3 ret = Vector3.zero;

        foreach(WindowBreakSetting pane in panes)
        {
            if(pane != null && pane.collider != null && pane.collider.bounds != null)
            {
                ret += pane.collider.bounds.center;
            }
        }

        ret /= panes.Count;
        return ret;
    }

    public void BreakWindow(Vector3 contactPosition, Vector3 relativeVelocity, Actor brokenBy, bool noDebris = false)
    {
        SetBroken(true);

        Game.Log("Shatter glass at " + contactPosition);

        //Play sound
        if (breakAudioEvent != null) AudioController.Instance.PlayWorldOneShot(breakAudioEvent, brokenBy, null, contactPosition, null);

        //Create particles
        if(!noDebris) Shatter(contactPosition, relativeVelocity);

        //Add vandalism fine
        if(brokenBy != null)
        {
            if(brokenBy.isPlayer)
            {
                NewAddress add = GetAddress();

                if(add != null)
                {
                    Vector3 pos = GetRoughPosition();

                    add.AddVandalism(pos);

                    StatusController.Instance.AddFineRecord(add, null, StatusController.CrimeType.vandalism, true, forcedPenalty: GameplayControls.Instance.breakingWindowsFine);
                }

                InteractionController.Instance.SetIllegalActionActive(true);
            }
        }
    }

    private void Start()
    {
        SpawnStateCheck();
    }

    //Get the broken state from recorded in dictionary
    private void OnEnable()
    {
        SpawnStateCheck();
    }

    private void SpawnStateCheck()
    {
        bool newBrokenState = false;
        bool newBoardedState = false;
        Vector3 keyPos = GetRoughPosition();
        brokenAt = 0f;

        //Check to see if this should be broken...
        try
        {
            if (GameplayController.Instance.brokenWindows != null && keyPos != null && GameplayController.Instance.brokenWindows.TryGetValue(keyPos, out brokenAt))
            {
                newBrokenState = true;

                if (SessionData.Instance.gameTime >= brokenAt + GameplayControls.Instance.brokenWindowBoardTime)
                {
                    //Game.Log("Window at " + keyPos + " is boarded, broken at " + brokenAt);
                    newBoardedState = true;
                }
            }
            else
            {
                //Game.Log("No instance of window in dictionary at " + keyPos);
                newBoardedState = false;
                newBrokenState = false;
            }
        }
        catch
        {
            newBoardedState = false;
            newBrokenState = false;
        }

        //Detect state change
        if (newBrokenState != isBroken || newBoardedState != isBoarded)
        {
            //Remove vandalism reference
            if (newBrokenState != isBroken && !isBroken)
            {
                //Remove vandalism
                NewAddress add = GetAddress();

                if (add != null)
                {
                    add.RemoveVandalism(GetRoughPosition());
                }
            }

            isBroken = newBrokenState;
            isBoarded = newBoardedState;
            UpdateBrokenState();
        }
    }

    //Set to broken/unbroken
    public void SetBroken(bool val)
    {
        if(val != isBroken)
        {
            isBroken = val;

            if(isBroken)
            {
                GameplayController.Instance.brokenWindows.Add(GetRoughPosition(), SessionData.Instance.gameTime);
            }
            else
            {
                isBoarded = false;

                //Remove vandalism
                NewAddress add = GetAddress();

                if(add != null)
                {
                    add.RemoveVandalism(GetRoughPosition());
                }
            }

            UpdateBrokenState();
        }
    }

    private Vector3 GetRoughPosition()
    {
        return new Vector3(Mathf.RoundToInt(this.transform.position.x * 100f) / 100f, Mathf.RoundToInt(this.transform.position.y * 100f) / 100f, Mathf.RoundToInt(this.transform.position.z * 100f) / 100f);
    }

    public NewAddress GetAddress()
    {
        Vector3 pos = GetRoughPosition();
        Vector3Int nodePos = CityData.Instance.RealPosToNodeInt(pos);
        NewNode winNode = null;

        if(PathFinder.Instance.nodeMap.TryGetValue(nodePos, out winNode))
        {
            //Return this address
            if(!winNode.gameLocation.IsOutside())
            {
                return winNode.gameLocation.thisAsAddress;
            }
            else
            {
                foreach(Vector2Int v2 in CityData.Instance.offsetArrayX8)
                {
                    Vector3Int surrounding = nodePos + new Vector3Int(v2.x, v2.y, 0);
                    NewNode n = null;

                    if(PathFinder.Instance.nodeMap.TryGetValue(surrounding, out n))
                    {
                        if(!n.gameLocation.IsOutside())
                        {
                            return n.gameLocation.thisAsAddress;
                        }
                    }
                }
            }

            if (winNode.gameLocation.thisAsAddress != null)
            {
                return winNode.gameLocation.thisAsAddress;
            }
        }

        return null; //Fail
    }

    //Update the state without changing any variables
    public void UpdateBrokenState()
    {
        foreach (WindowBreakSetting wbs in panes)
        {
            if (wbs == null) continue;

            //Set mesh
            if (isBroken)
            {
                if(isBoarded)
                {
                    wbs.filter.sharedMesh = wbs.boardedMesh;
                }
                else
                {
                    wbs.filter.sharedMesh = wbs.brokenMesh;
                }
            }
            else
            {
                wbs.filter.sharedMesh = wbs.defaultMesh;
            }

            //Set material
            if (isBroken)
            {
                if(isBoarded)
                {
                    wbs.renderer.sharedMaterial = wbs.boardedMat;
                }
                else
                {
                    wbs.renderer.sharedMaterial = wbs.brokenMat;
                }
            }
            else
            {
                wbs.renderer.sharedMaterial = wbs.defaultMat;
            }

            if (wbs.collider != null && wbs.removeColliderWhenBroken)
            {
                if(isBoarded)
                {
                    wbs.collider.enabled = true;
                }
                else
                {
                    wbs.collider.enabled = !isBroken;

                    //if(isBroken)
                    //{
                    //    Physics.SyncTransforms();
                    //}
                }
            }

            if(wbs.removeWhenBoarded && isBoarded)
            {
                wbs.renderer.gameObject.SetActive(!isBoarded);
            }
        }
    }

    //Use mesh UV pixels to generate shatting voxels...
    public void Shatter(Vector3 contact, Vector3 velocity)
    {
        if (!this.gameObject.activeInHierarchy) return; //Only do if active

        foreach (WindowBreakSetting r in panes)
        {
            if (r == null) continue;
            if (!r.shatter) continue;
            Texture2D diffuse = r.defaultMat.GetTexture("_BaseColorMap") as Texture2D;

            if (diffuse == null)
            {
                Game.Log("Misc Error: Cannot find base Texture for " + r.defaultMat.name);
                continue;
            }
            else if (!diffuse.isReadable)
            {
                Game.LogError("Texture for " + name + " is not readable! Set this if you want to shatter this object...");
            }

            Vector3 voxelScale = r.shardSize;

            int shardsSpawned = 0; //The number of shards spawned
            int spawnEveryXpixels = r.shardEveryXPixels; //Spawn a shard every this number of pixels
            int pixelCursor = 0; //Used to calc above
            int usedUVPixels = 0; //The amount of used UV pixels in the model

            //Scan for non black pixels...
            for (int i = 0; i < diffuse.width; i++)
            {
                for (int l = 0; l < diffuse.height; l++)
                {
                    Color pxCol = diffuse.GetPixel(i, l);

                    if (pxCol != Color.black && pxCol != Color.clear)
                    {
                        if (pixelCursor <= 0)
                        {
                            Vector2 coord = new Vector2(i / (float)diffuse.width, l / (float)diffuse.height);

                            GameObject shard = Instantiate(PrefabControls.Instance.glassShard, PrefabControls.Instance.mapContainer);
                            shard.transform.position = this.transform.TransformPoint(UvTo3D(coord, r.filter.mesh));

                            shard.transform.localScale = voxelScale;
                            pxCol.a = 0.5f;

                            MeshRenderer mr = shard.GetComponent<MeshRenderer>();
                            mr.material.SetColor("_BaseColor", pxCol);
                            shardsSpawned++;
                            pixelCursor = spawnEveryXpixels;

                            Rigidbody rb = shard.GetComponent<Rigidbody>();
                            rb.AddExplosionForce(velocity.magnitude * r.shatterForceMultiplier * Toolbox.Instance.Rand(0.8f, 1.2f), contact, r.filter.mesh.bounds.size.magnitude);

                            //Add a force multiplied by distance to contact point
                            float distToContact = Vector3.Distance(shard.transform.position, contact);

                            distToContact = Mathf.Clamp(1f - distToContact, -0.1f, 1f);
                            rb.AddForce(-velocity * distToContact * 0.8f, ForceMode.VelocityChange);
                        }
                        else pixelCursor--;

                        usedUVPixels++;
                    }
                }
            }

            Game.Log("...Created " + shardsSpawned + " shards.");
        }
    }

    //Get local mesh coordinates of this UV coordinate
    private Vector3 UvTo3D(Vector2 uv, Mesh mesh)
    {
        if (mesh == null || !mesh.isReadable)
        {
            Game.LogError("Mesh is not readable! Fingerprints cannot be gathered as the verts aren't readable...");
            return Vector3.zero;
        }

        int[] tris = mesh.triangles;
        Vector2[] uvs = mesh.uv;
        Vector3[] verts = mesh.vertices;

        for (int i = 0; i < tris.Length; i += 3)
        {
            Vector2 u1 = uvs[tris[i]]; // get the triangle UVs
            Vector2 u2 = uvs[tris[i + 1]];
            Vector2 u3 = uvs[tris[i + 2]];

            // calculate triangle area - if zero, skip it
            float a = Area(u1, u2, u3);
            if (a == 0) continue;

            // calculate barycentric coordinates of u1, u2 and u3
            // if anyone is negative, point is outside the triangle: skip it
            float a1 = Area(u2, u3, uv) / a;
            if (a1 < 0) continue;

            float a2 = Area(u3, u1, uv) / a;
            if (a2 < 0) continue;

            float a3 = Area(u1, u2, uv) / a;
            if (a3 < 0) continue;

            // point inside the triangle - find mesh position by interpolation...
            Vector3 p3D = a1 * verts[tris[i]] + a2 * verts[tris[i + 1]] + a3 * verts[tris[i + 2]];

            //Return as local coordinates
            return p3D;
        }

        //point outside any uv triangle: return Vector3.zero
        return Vector3.zero;
    }


    //calculate signed triangle area using a kind of "2D cross product":
    private float Area(Vector2 p1, Vector2 p2, Vector2 p3)
    {
        Vector2 v1 = p1 - p3;
        Vector2 v2 = p2 - p3;
        return (v1.x * v2.y - v1.y * v2.x) / 2;
    }
}
