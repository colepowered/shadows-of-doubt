﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CitizenAnimationEvents : MonoBehaviour
{
    public NewAIController ai;

    //The citizen will not move
    public void SetStaticAnimation()
    {
        ai.SetStaticFromAnimation(true);
    }

    //The citizen can move again
    public void ResetStaticAnimation()
    {
        ai.SetStaticFromAnimation(false);
    }

    //Reset from one shot use
    public void ResetOneShotUse()
    {
        if(ai.human.animationController.armsBoolAnimationState == CitizenAnimationController.ArmsBoolSate.armsOneShotUse)
        {
            ai.human.animationController.SetArmsBoolState(CitizenAnimationController.ArmsBoolSate.none);
        }
    }

    public void FootstepLeft()
    {
        ai.human.OnFootstep(false);
    }

    public void FootstepRight()
    {
        ai.human.OnFootstep(true);
    }

    public void TripKO()
    {
        ai.isTripping = false;
        ai.human.animationController.CancelTrip();
        ai.SetKO(true, impactDirection: ai.transform.forward, forced: true, forcedDuration: 0.002f, resetInvesigate: false);
    }

    public void MeleeAttackTrigger()
    {
        ////Trigger a meleee attack
        //Transform attackPoint = null;

        //AnimatorStateInfo anim = ai.human.animationController.mainAnimator.GetCurrentAnimatorStateInfo(1);

        ////Use animation state to time attacks
        //if (anim.IsTag("Punch") || anim.IsTag("Swing"))
        //{
        //    attackPoint = ai.human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.HandRight);
        //    Game.Log("MELEE ATTACK TRIGGER: RIGHT HAND");
        //}
        //else if(anim.IsTag("Kick"))
        //{
        //    attackPoint = ai.human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.RightFoot);
        //    Game.Log("MELEE ATTACK TRIGGER: RIGHT FOOT");
        //}

        //GameObject newAttack = Instantiate(PrefabControls.Instance.damageCollider, attackPoint);
        //ai.damageCollider = newAttack.GetComponent<DamageColliderController>();
        //ai.damageCollider.Setup(ai.human, ai.attackTarget, ai.weaponDamage);
        //if (ai.activeAttackBar != null) ai.activeAttackBar.removeHit = true; //Play remove by hit animation

        ////Play punch sfx
        //if (ai.currentWeaponPreset.fireEvent != null) AudioController.Instance.PlayWorldOneShot(ai.currentWeaponPreset.fireEvent, ai.human, ai.human.currentNode, attackPoint.position);

        //ai.damageColliderCreated = true;
    }

    public void MeleeAttackRemove()
    {
        //if(ai.damageCollider != null)
        //{
        //    Destroy(ai.damageCollider);
        //}
    }
}
