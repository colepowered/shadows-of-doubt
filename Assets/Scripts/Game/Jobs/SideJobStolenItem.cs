﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SideJobStolenItem : SideJob
{
    [Header("Serialized Data")]
    public float theftTime = 0f;
    public float theftTimeFrom = 0f;
    public float theftTimeTo = 0f;
    public int stolenItemRoom;

    public SideJobStolenItem(JobPreset newPreset, SideJobController.JobPickData newData, bool immediatePost) : base(newPreset, newData, immediatePost)
    {
        theftTime = SessionData.Instance.gameTime - Toolbox.Instance.Rand(4f, 12f);

        SimulateTheft();

        Vector2 range = Toolbox.Instance.CreateTimeRange(theftTime, 0.75f, true, true, 15);
        theftTimeFrom = range.x;
        theftTimeTo = range.y;
    }

    //Setup theft
    public void SimulateTheft()
    {
        Interactable stolenItem = GetItem(JobPreset.JobTag.A);

        if (stolenItem != null)
        {
            stolenItemRoom = stolenItem.node.room.roomID;

            FurnitureLocation originalFurniture = stolenItem.furnitureParent;

            if(originalFurniture != null)
            {
                foreach (Interactable i in originalFurniture.integratedInteractables)
                {
                    i.AddNewDynamicFingerprint(purp, Interactable.PrintLife.timed);
                    i.AddNewDynamicFingerprint(purp, Interactable.PrintLife.timed);
                    i.AddNewDynamicFingerprint(purp, Interactable.PrintLife.timed);
                    i.AddNewDynamicFingerprint(purp, Interactable.PrintLife.timed);
                    i.AddNewDynamicFingerprint(purp, Interactable.PrintLife.timed);
                }
            }

            //Add footprints
            float dirt = Toolbox.Instance.Rand(0.2f, 0.5f);
            NewNode.NodeAccess apartmentEntrance = poster.home.entrances.Find(item => item.accessType == NewNode.NodeAccess.AccessType.door);

            PathFinder.PathData path = PathFinder.Instance.GetPath(apartmentEntrance.fromNode, stolenItem.node, poster);

            if(path != null)
            {
                //Prints to
                for (int i = 0; i < path.accessList.Count; i++)
                {
                    NewNode.NodeAccess p = path.accessList[i];

                    Vector3 dir = Vector3.zero;

                    if(i < path.accessList.Count - 1)
                    {
                        NewNode.NodeAccess next = path.accessList[i + 1];
                        dir = next.toNode.position - p.toNode.position;
                    }

                    GameplayController.Footprint newPrint = new GameplayController.Footprint(purp, p.toNode.position, Quaternion.LookRotation(dir).eulerAngles, dirt, 0f, p.toNode.room);
                }

                //Prints from
                for (int i = path.accessList.Count - 1; i > 0; i--)
                {
                    NewNode.NodeAccess p = path.accessList[i];

                    Vector3 dir = Vector3.zero;

                    if (i > 0)
                    {
                        NewNode.NodeAccess next = path.accessList[i - 1];
                        dir = next.toNode.position - p.toNode.position;
                    }

                    GameplayController.Footprint newPrint = new GameplayController.Footprint(purp, p.toNode.position, Quaternion.LookRotation(dir).eulerAngles, dirt, 0f, p.toNode.room);
                }
            }

            //Surveillance: Add theft to cctv
            Toolbox.Instance.RetroactiveSurveillanceAddition(purp, purp.FindSafeTeleport(purp.home), stolenItem.node, true, null, theftTime, 0.04f, ClothesPreset.OutfitCategory.outdoorsCasual);

            //Reposition this item to the purp's apartment
            NewGameLocation.ObjectPlacement pl = purp.home.GetBestSpawnLocation(stolenItem.preset, false, purp, purp, null, out _, null, true, 3, InteractablePreset.OwnedPlacementRule.prioritiseOwned, 3);

            if (pl != null)
            {
                if (stolenItem.inInventory) stolenItem.SetAsNotInventory(pl.furnParent.anchorNode);
                stolenItem.ConvertToFurnitureSpawnedObject(pl.furnParent, pl.location, true, true);

                Game.Log("Jobs: Successfully relocated " + stolenItem.name + " to " + stolenItem.node.gameLocation.name + " (" + purp.name + ")");

                //Add fignerprints
                stolenItem.AddNewDynamicFingerprint(purp, Interactable.PrintLife.manualRemoval);
                stolenItem.AddNewDynamicFingerprint(purp, Interactable.PrintLife.manualRemoval);
                stolenItem.AddNewDynamicFingerprint(purp, Interactable.PrintLife.manualRemoval);

                //Add fingerprints to poster's door handles
                foreach(NewNode.NodeAccess acc in poster.home.entrances)
                {
                    if(acc.door != null)
                    {
                        if(acc.door.handleInteractable != null)
                        {
                            acc.door.handleInteractable.AddNewDynamicFingerprint(purp, Interactable.PrintLife.timed);
                            acc.door.handleInteractable.AddNewDynamicFingerprint(purp, Interactable.PrintLife.timed);
                            acc.door.handleInteractable.AddNewDynamicFingerprint(purp, Interactable.PrintLife.timed);
                        }

                        if(acc.door.doorInteractable != null)
                        {
                            acc.door.handleInteractable.AddNewDynamicFingerprint(purp, Interactable.PrintLife.timed);
                            acc.door.handleInteractable.AddNewDynamicFingerprint(purp, Interactable.PrintLife.timed);
                            acc.door.handleInteractable.AddNewDynamicFingerprint(purp, Interactable.PrintLife.timed);
                        }
                    }
                }
            }
            else
            {
                Game.LogError("Unable to get object placement position");
            }
        }
        else
        {
            Game.LogError("Unable to setup stolen item as it is not spawned");
        }
    }

    public override void Complete()
    {
        base.Complete();

        ReturnItem();
    }

    public void ReturnItem()
    {
        //Move position back to original spawn
        Interactable stolenItem = GetItem(JobPreset.JobTag.A);

        if (stolenItem != null)
        {
            //Reposition this item to the purp's apartment
            NewGameLocation.ObjectPlacement pl = poster.home.GetBestSpawnLocation(stolenItem.preset, false, poster, poster, null, out _, null, true, 3, InteractablePreset.OwnedPlacementRule.prioritiseOwned, 3);

            if (pl != null)
            {
                if (stolenItem.inInventory) stolenItem.SetAsNotInventory(pl.furnParent.anchorNode);
                stolenItem.ConvertToFurnitureSpawnedObject(pl.furnParent, pl.location, true, true);
            }

            //remove fignerprints created
            stolenItem.RemoveManuallyCreatedFingerprints();
        }
    }

    public override void DebugDisplayAnswers()
    {
        base.DebugDisplayAnswers();

        Interactable getStolen = GetItem(JobPreset.JobTag.A);

        if(getStolen != null)
        {
            Game.Log("Jobs: Stolen item: " + getStolen.GetWorldPosition() + " at " + getStolen.node.gameLocation.name + " " + getStolen.node.room.name);
        }
    }
}
