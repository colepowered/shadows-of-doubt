﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Unity.Mathematics;
using NaughtyAttributes;

public class DataRaycastController : MonoBehaviour
{
    [System.Serializable]
    public struct NodeRaycastHit
    {
        public Vector3Int coord;
        public List<int> conditionalDoors; //A list of doors required to be open for this to be seen from the original location.
    }

    //Singleton pattern
    private static DataRaycastController _instance;
    public static DataRaycastController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public bool EntranceRaycast(NewNode.NodeAccess fromEntrance, NewNode.NodeAccess toEntrance, out List<NodeRaycastHit> path/*, out List<string> debug*/, bool debugMode = false)
    {
        return NodeRaycast(fromEntrance.toNode, toEntrance.fromNode, out path, fromEntrance.door, debugMode);
    }

    //The non-job system system raycast: Returns true if nothing is hit
    public bool NodeRaycast(NewNode fromNode, NewNode toNode, out List<NodeRaycastHit> path/*, out List<string> debug*/, NewDoor startingDoor = null, bool debugMode = false)
    {
        path = new List<NodeRaycastHit>();
        //debug = new List<string>();

        Vector3 dir = toNode.nodeCoord - fromNode.nodeCoord;

        int x0 = (int)fromNode.nodeCoord.x;
        int y0 = (int)fromNode.nodeCoord.y;
        int z0 = (int)fromNode.nodeCoord.z;

        int x1 = (int)toNode.nodeCoord.x;
        int y1 = (int)toNode.nodeCoord.y;
        int z1 = (int)toNode.nodeCoord.z;

        int dx = Mathf.Abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
        int dy = Mathf.Abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
        int dz = Mathf.Abs(z1 - z0), sz = z0 < z1 ? 1 : -1;
        int dm = Mathf.Max(dx, dy, dz), i = dm; /* maximum difference */
        x1 = y1 = z1 = dm / 2; /* error offset */

        Vector3Int lastCoord = fromNode.nodeCoord;
        List<int> conditionalDoors = new List<int>();

        //Add starting door
        if(startingDoor != null && !startingDoor.preset.isTransparent)
        {
            conditionalDoors.Add(startingDoor.wall.id);
        }

        float distance = 0f;

        Vector3Int current;
        NewNode foundNode = null;
        int floorDiff;
        NewNode prevNode = null;
        Vector2 wallOffset;
        NewWall foundWall = null;

        while (true)
        {  /* loop */

            //Check valid path...
            current = new Vector3Int(x0, y0, z0);

            foundNode = null;
            floorDiff = (int)lastCoord.z - (int)current.z;

            //Check for obstacles from 2nd loop onwards
            if (current != fromNode.nodeCoord && PathFinder.Instance.nodeMap.TryGetValue(current, out foundNode))
            {
                distance += PathFinder.Instance.nodeSize.x;

                //Check if we're going down a floor this loop. If so the current node must have no ceiling
                if (floorDiff > 0)
                {
                    if (foundNode.floorType == NewNode.FloorTileType.floorAndCeiling || foundNode.floorType == NewNode.FloorTileType.CeilingOnly)
                    {
                        //Test adjacent nodes for no ceiling adjacent to a bannister, if true then do a pass (no wall between)
                        if (!TestAdjacentForNoCeilingAdjBannister(foundNode)) return false;
                    }
                }
                //Check if we're going up a floor this loop. If so the current node must have no floor
                else if (floorDiff < 0)
                {
                    if (foundNode.floorType == NewNode.FloorTileType.floorAndCeiling || foundNode.floorType == NewNode.FloorTileType.floorOnly)
                    {
                        //Test adjacent nodes for no floor adjacent to a bannister, if true then do a pass (no wall between)
                        if (!TestAdjacentForNoFloorAdjBannister(foundNode)) return false;
                    }
                }

                //Get the previous node...
                prevNode = null;

                if (PathFinder.Instance.nodeMap.TryGetValue(lastCoord, out prevNode))
                {
                    //Do a wall check...
                    wallOffset = new Vector2(current.x - lastCoord.x, current.y - lastCoord.y) * 0.5f;

                    //debug.Add("Wall offset for " + prevNode + " is " + wallOffset + "...");
                    if (debugMode) Game.Log("Wall offset for " + prevNode.position + " is " + wallOffset + "...");

                    //Check previous node's walls
                    //If diagonal, check for both x and y
                    if (wallOffset.x != 0)
                    {
                        foundWall = null;

                        if (prevNode.wallDict.TryGetValue(new Vector2(wallOffset.x, 0), out foundWall))
                        {
                            //debug.Add("...Wall found: " + foundWall.preset.sectionClass);
                            if (debugMode) Game.Log("...Wall found: " + foundWall.preset.sectionClass);

                            if (!foundWall.preset.ignoreCullingRaycasts)
                            {
                                //If this is a window, do an outside > inside check
                                if (foundWall.preset.sectionClass == DoorPairPreset.WallSectionClass.window || foundWall.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge || (foundWall.door != null && foundWall.door.preset.isTransparent))
                                {
                                    //If going from outside to inside, only allow this on the ground floor
                                    if (prevNode.room.IsOutside() && !foundNode.room.IsOutside() && foundNode.nodeCoord.z > 0)
                                    {
                                        return false;
                                    }
                                    //Don't penetrate windows after a certain distance...
                                    else if(distance > CullingControls.Instance.windowCullingRange)
                                    {
                                        return false;
                                    }
                                }
                                else if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                {
                                    //If in close range, check for adjacent nodes with non-walls...
                                    if(distance <= 12f)
                                    {
                                        if(!TestAdjacentForNoWall(foundWall))
                                        {
                                            return false;
                                        }
                                    }
                                    else return false;
                                }
                                else if (foundWall.door != null && !foundWall.door.preset.isTransparent)
                                {
                                    //Don't penetrate doors after a certain distance
                                    if(distance > CullingControls.Instance.doorCullingRange)
                                    {
                                        return false;
                                    }

                                    //This is an entrance with a door, and must be open to continue the raycast...
                                    conditionalDoors.Add(foundWall.door.wall.id);
                                }
                            }
                        }
                        else
                        {
                            //debug.Add("...Wall not found");
                            if (debugMode) Game.Log("...Wall not found");
                        }
                    }

                    if (wallOffset.y != 0)
                    {
                        foundWall = null;

                        if (prevNode.wallDict.TryGetValue(new Vector2(0, wallOffset.y), out foundWall))
                        {
                            //debug.Add("...Wall found: " + foundWall.preset.sectionClass);
                            if (debugMode) Game.Log("...Wall found: " + foundWall.preset.sectionClass);

                            if (!foundWall.preset.ignoreCullingRaycasts)
                            {
                                //If this is a window, do an outside > inside check
                                if (foundWall.preset.sectionClass == DoorPairPreset.WallSectionClass.window || foundWall.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge || (foundWall.door != null && foundWall.door.preset.isTransparent))
                                {
                                    //If going from outside to inside, only allow this on the ground floor
                                    if (prevNode.room.IsOutside() && !foundNode.room.IsOutside() && foundNode.nodeCoord.z > 0)
                                    {
                                        return false;
                                    }
                                    //Don't penetrate windows after a certain distance...
                                    else if (distance > CullingControls.Instance.windowCullingRange)
                                    {
                                        return false;
                                    }
                                }
                                else if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                {
                                    //If in close range, check for adjacent nodes with non-walls...
                                    if (distance < 12f)
                                    {
                                        if (!TestAdjacentForNoWall(foundWall))
                                        {
                                            return false;
                                        }
                                    }
                                    else return false;
                                }
                                else if (foundWall.door != null && !foundWall.door.preset.isTransparent)
                                {
                                    //Don't penetrate doors after a certain distance
                                    if (distance > CullingControls.Instance.doorCullingRange)
                                    {
                                        return false;
                                    }

                                    //This is an entrance with a door, and must be open to continue the raycast...
                                    conditionalDoors.Add(foundWall.door.wall.id);
                                }
                            }
                        }
                        else
                        {
                            //debug.Add("...Wall not found");
                            if (debugMode) Game.Log("...Wall not found");
                        }
                    }
                }
            }
            //If this doesn't find anything, continue path as-is (it's probably blank space)

            path.Add(new NodeRaycastHit { coord = current, conditionalDoors = conditionalDoors });

            //End
            if (i-- == 0)
            {
                break;
            }

            x1 -= dx; if (x1 < 0) { x1 += dm; x0 += sx; }
            y1 -= dy; if (y1 < 0) { y1 += dm; y0 += sy; }
            z1 -= dz; if (z1 < 0) { z1 += dm; z0 += sz; }

            lastCoord = current; //Keep reference to last coord to check against next loop
        }

        conditionalDoors = null;

        //If we've reached this far, there are no obstacles.
        return true;
    }

    //Tests adjacent nodes for no ceiling adjacent a bannister
    private bool TestAdjacentForNoCeilingAdjBannister(NewNode n)
    {
        Vector3Int nodeCoord;
        NewNode foundNode = null;
        Vector2 wOffset;
        NewWall foundWall;

        foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
        {
            nodeCoord = n.nodeCoord + new Vector3Int(v2.x, v2.y, 0);

            if(PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out foundNode))
            {
                //If in different room, check for blockers (wall etc)
                if(foundNode.room != n.room)
                {
                    //Wall offset
                    wOffset = new Vector2((foundNode.nodeCoord.x - n.nodeCoord.x) * -0.5f, (foundNode.nodeCoord.y - n.nodeCoord.y) * -0.5f);

                    if(foundNode.wallDict.TryGetValue(wOffset, out foundWall))
                    {
                        //Allows through bannisters and no doors
                        if(foundWall.preset.ignoreCullingRaycasts)
                        {
                            if (foundNode.floorType == NewNode.FloorTileType.floorOnly || foundNode.floorType == NewNode.FloorTileType.none || foundNode.floorType == NewNode.FloorTileType.noneButIndoors)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    //Tests adjacent nodes for no floor adjacent a banniser
    private bool TestAdjacentForNoFloorAdjBannister(NewNode n)
    {
        Vector3Int nodeCoord;
        NewNode foundNode = null;
        Vector2 wOffset;
        NewWall foundWall = null;

        foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
        {
            nodeCoord = n.nodeCoord + new Vector3Int(v2.x, v2.y, 0);

            if (PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out foundNode))
            {
                //If in different room, check for blockers (wall etc)
                if (foundNode.room != n.room)
                {
                    //Wall offset
                    wOffset = new Vector2((foundNode.nodeCoord.x - n.nodeCoord.x) * -0.5f, (foundNode.nodeCoord.y - n.nodeCoord.y) * -0.5f);

                    if (foundNode.wallDict.TryGetValue(wOffset, out foundWall))
                    {
                        //Allows through bannisters and no doors
                        if (foundWall.preset.ignoreCullingRaycasts)
                        {
                            if (foundNode.floorType == NewNode.FloorTileType.CeilingOnly || foundNode.floorType == NewNode.FloorTileType.none || foundNode.floorType == NewNode.FloorTileType.noneButIndoors)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    //Tests wall offset of adjacent nodes to see if there's an open entrance here...
    private bool TestAdjacentForNoWall(NewWall w)
    {
        Vector3Int nodeCoord;
        NewNode foundNode = null;
        NewWall foundAdjOffet = null;

        foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
        {
            if (w.node.walls.Exists(item => item.wallOffset * 2 == v2)) continue;//NEW: Ignore behind walls on this node...

            nodeCoord = w.node.nodeCoord + new Vector3Int(v2.x, v2.y, 0);

            if (PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out foundNode))
            {
                if(foundNode.wallDict.TryGetValue(w.wallOffset, out foundAdjOffet))
                {
                    if (foundAdjOffet.door == null && foundAdjOffet.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    ////Job System...
    //[BurstCompile]
    //public struct DataRaycastJob : IJob
    //{
    //    //We have to use IDs here as the job can't use classes.
    //    [ReadOnly]
    //    public float3 fromNode; //Use node coords
    //    [ReadOnly]
    //    public float3 toNode; //Use node coords

    //    //Extra variables needed for pathfinding
    //    [ReadOnly]
    //    public NativeHashMap<float3, int> floorTypes; //Array of node floors types (int conversion of enum)
    //    [ReadOnly]
    //    public NativeHashMap<float3, NativeHashMap<float2, float2>> walls; //Hash map containing node coord > wall offets > float2(wall section type, door id (-1 = none))
    //    [ReadOnly]
    //    public NativeHashMap<float3, int> outsideWindow; //Nodes with 'outside window' = true

    //    //This is accessed by the main thread
    //    [WriteOnly]
    //    public NativeHashMap<float3, NativeList<int>> path; //Path of valid nodes as key, list of conditional doors as value

    //    public void Execute()
    //    {
    //        float3 dir = toNode - fromNode;

    //        int x0 = (int)fromNode.x;
    //        int y0 = (int)fromNode.y;
    //        int z0 = (int)fromNode.z;
    //        int x1 = (int)toNode.x;
    //        int y1 = (int)toNode.y;
    //        int z1 = (int)toNode.z;

    //        int dx = Mathf.Abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
    //        int dy = Mathf.Abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
    //        int dz = Mathf.Abs(z1 - z0), sz = z0 < z1 ? 1 : -1;
    //        int dm = Mathf.Max(dx, dy, dz), i = dm; /* maximum difference */
    //        x1 = y1 = z1 = dm / 2; /* error offset */

    //        float3 lastCoord = fromNode;
    //        NativeList<int> conditionalDoors = new NativeList<int>(); //Uses wall ID.

    //        while (true)
    //        {  /* loop */

    //            //Check valid path...
    //            float3 current = new float3(x0, y0, z0);

    //            int floorType = 0; //Default to 0 (none)
    //            int floorDiff = (int)lastCoord.z - (int)current.z;

    //            //Check for obstacles from 2nd loop onwards
    //            if ((x0 != fromNode.x || y0 != fromNode.y || z0 != fromNode.z) && floorTypes.TryGetValue(current, out floorType))
    //            {
    //                //Check if we're going down a floor this loop. If so the current node must have no ceiling
    //                if (floorDiff > 0)
    //                {
    //                    if (floorType == 1 || floorType == 3)
    //                    {
    //                        conditionalDoors.Dispose();
    //                        return;
    //                    }
    //                }
    //                //Check if we're going up a floor this loop. If so the current node must have no floor
    //                else if (floorDiff < 0)
    //                {
    //                    if (floorType == 1 || floorType == 2)
    //                    {
    //                        conditionalDoors.Dispose();
    //                        return;
    //                    }
    //                }

    //                //Get the previous node...
    //                NativeHashMap<float2, float2> wallData;

    //                if(walls.TryGetValue(lastCoord, out wallData))
    //                {
    //                    //Do a wall check...
    //                    float2 wallOffset = new float2(current.x - lastCoord.x, current.y - lastCoord.y) * 0.5f;

    //                    //Check previous node's walls
    //                    //If diagonal, check for both x and y
    //                    if (wallOffset.x != 0)
    //                    {
    //                        float2 foundWall;

    //                        if (wallData.TryGetValue(new float2(wallOffset.x, 0), out foundWall))
    //                        {
    //                            //If this is a window, do an outside > inside check
    //                            if (foundWall.x == 1|| foundWall.x == 2)
    //                            {
    //                                //If going from outside to inside, only allow this on the ground floor
    //                                if(current.z > 0 && outsideWindow.TryGetValue(lastCoord, out _) && !outsideWindow.TryGetValue(current, out _))
    //                                {
    //                                    wallData.Dispose();
    //                                    conditionalDoors.Dispose();
    //                                    return;
    //                                }
    //                            }
    //                            else if (foundWall.x != 3)
    //                            {
    //                                wallData.Dispose();
    //                                conditionalDoors.Dispose();
    //                                return;
    //                            }
    //                            else if (foundWall.y >= 0)
    //                            {
    //                                //This is an entrance with a door, and must be open to continue the raycast...
    //                                conditionalDoors.Add((int)foundWall.y);
    //                            }
    //                        }
    //                    }

    //                    if (wallOffset.y != 0)
    //                    {
    //                        float2 foundWall;

    //                        if (wallData.TryGetValue(new float2(0, wallOffset.y), out foundWall))
    //                        {
    //                            //If this is a window, do an outside > inside check
    //                            if (foundWall.x == 1 || foundWall.x == 2)
    //                            {
    //                                //If going from outside to inside, only allow this on the ground floor
    //                                if (current.z > 0 && outsideWindow.TryGetValue(lastCoord, out _) && !outsideWindow.TryGetValue(current, out _))
    //                                {
    //                                    wallData.Dispose();
    //                                    conditionalDoors.Dispose();
    //                                    return;
    //                                }
    //                            }
    //                            else if (foundWall.x != 3)
    //                            {
    //                                wallData.Dispose();
    //                                conditionalDoors.Dispose();
    //                                return;
    //                            }
    //                            else if (foundWall.y >= 0)
    //                            {
    //                                //This is an entrance with a door, and must be open to continue the raycast...
    //                                conditionalDoors.Add((int)foundWall.y);
    //                            }
    //                        }
    //                    }
    //                }

    //                wallData.Dispose();
    //            }
    //            //If this doesn't find anything, continue path as-is (it's probably blank space)

    //            path.Add(current, conditionalDoors);

    //            if (i-- == 0) break;
    //            x1 -= dx; if (x1 < 0) { x1 += dm; x0 += sx; }
    //            y1 -= dy; if (y1 < 0) { y1 += dm; y0 += sy; }
    //            z1 -= dz; if (z1 < 0) { z1 += dm; z0 += sz; }

    //            lastCoord = current; //Keep reference to last coord to check against next loop
    //        }

    //        //If we've reached this far, there are no obstacles.
    //        conditionalDoors.Dispose();
    //        return;
    //    }
    //}
}
