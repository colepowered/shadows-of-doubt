﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactAge : Fact
{
    //Constructor
    public FactAge(FactPreset newPreset, List<Evidence> newFromEvidence, List<Evidence> newToEvidence, List<object> newPassedObjects, List<Evidence.DataKey> overrideFromKeys, List<Evidence.DataKey> overrideToKeys, bool isCustomFact) : base(newPreset, newFromEvidence, newToEvidence, newPassedObjects, overrideFromKeys, overrideToKeys, isCustomFact)
    {

    }

    //Get name from the connecting evidence. Optional: Get name for a specific link
    public override string GetName(Evidence.FactLink specificLink = null)
    {
        string ret = string.Empty;

        //Get from names
        if (specificLink == null || !specificLink.thisIsTheFromEvidence)
        {
            for (int i = 0; i < fromEvidence.Count; i++)
            {
                Evidence fromEv = fromEvidence[i];

                if (i > 0) ret += " & ";
                ret += fromEv.GetNameForDataKey(fromDataKeys);
            }
        }
        else
        {
            ret += specificLink.thisEvidence.GetNameForDataKey(fromDataKeys);
        }

        ret += " " + Strings.Get("evidence.generic", preset.name) + " ";

        //Get to names
        for (int i = 0; i < toEvidence.Count; i++)
        {
            Evidence toEv = toEvidence[i];

            if (i > 0) ret += " & ";
            ret += toEv.GetNameForDataKey(Evidence.DataKey.name);
        }

        //Get age
        int age = (fromEvidence[0] as EvidenceCitizen).witnessController.GetAge();
        ret += " (" + Strings.Get("evidence.generic", "age") + " " + age.ToString() + ")";

        //Trim extra whitespace
        ret = ret.Trim();

        return ret;
    }
}
