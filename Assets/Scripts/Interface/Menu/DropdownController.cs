﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using NaughtyAttributes;
using System.IO;

public class DropdownController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform dropdownRect;
    public RectTransform dropdownArrow;
    public TMP_Dropdown dropdown;
    public RectTransform buttonsRect;
    public ButtonController prevButton;
    public ButtonController nextButton;

    [Header("Configuration")]
    public string playerPrefsID;
    public List<string> staticOptionReference = new List<string>();

    [Header("State")]
    [ReadOnly]
    public float normalWidth = 550f;

    private void Start()
    {
        normalWidth = dropdownRect.sizeDelta.x;
        OnControlModeChange();
        InputController.Instance.OnInputModeChange += OnControlModeChange;

        //Assign button events
        nextButton.button.onClick.AddListener(delegate { OnNextButton(); });
        prevButton.button.onClick.AddListener(delegate { OnPreviousButton(); });
    }

    //Add options either using dictionary or not. Also populates static options.
    public void AddOptions(List<string> newOptions, bool useDictionary, List<string> newListedOptions = null)
    {
        dropdown.ClearOptions();

        if(useDictionary)
        {
            List<string> dictOptions = new List<string>();

            foreach (string c in newOptions)
            {
                dictOptions.Add(Strings.Get("ui.interface", c));
            }

            dropdown.AddOptions(dictOptions);
        }
        else
        {
            if(newListedOptions != null)
            {
                dropdown.AddOptions(newListedOptions);
            }
            else dropdown.AddOptions(newOptions);
        }

        staticOptionReference.Clear();
        staticOptionReference.AddRange(newOptions);
    }

    //Select option from static option
    public void SelectFromStaticOption(string staticOption)
    {
        int index = staticOptionReference.FindIndex(item => item.ToLower() == staticOption.ToLower());

        if(index > -1)
        {
            Game.Log("Menu: Found static option reference for " + staticOption + ": " + staticOptionReference[dropdown.value]);
            dropdown.SetValueWithoutNotify(index);
        }
        else
        {
            Game.Log("Menu: Unable to find static option reference for " + staticOption);
        }
    }

    public string GetCurrentSelectedStaticOption()
    {
        if (staticOptionReference.Count > dropdown.value)
        {
            Game.Log("Menu: Found static option reference: " + staticOptionReference[dropdown.value]);
            return staticOptionReference[dropdown.value];
        }
        else return string.Empty;
    }

    //Triggered when the control mode is changed
    public void OnControlModeChange()
    {
        if(InputController.Instance.mouseInputMode)
        {
            buttonsRect.gameObject.SetActive(false);
            dropdownRect.sizeDelta = new Vector2(normalWidth, dropdownRect.sizeDelta.y);
            prevButton.SetInteractable(false);
            nextButton.SetInteractable(false);
            dropdownArrow.gameObject.SetActive(true);
            dropdown.interactable = true;
        }
        else
        {
            if(dropdown.IsExpanded)
            {
                dropdown.Hide();
            }

            buttonsRect.gameObject.SetActive(true);
            dropdownRect.sizeDelta = new Vector2(normalWidth - 140f, dropdownRect.sizeDelta.y);
            prevButton.SetInteractable(true);
            nextButton.SetInteractable(true);
            dropdownArrow.gameObject.SetActive(false);
            dropdown.interactable = false;
        }
    }

    public void OnNextButton()
    {
        dropdown.value++;
    }

    public void OnPreviousButton()
    {
        dropdown.value--;
    }

    public void OnValueChange()
    {
        //Game.Log("Value changed: " + dropdown.value);

        if(playerPrefsID != null && playerPrefsID.Length > 0)
        {
            PlayerPrefsController.Instance.OnToggleChanged(playerPrefsID, true, this);
        }
    }
}
