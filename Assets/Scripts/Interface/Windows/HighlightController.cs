﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class HighlightController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	[System.NonSerialized]
    public InfoWindow window = null;
	public string selectableType = string.Empty;

	public bool selectable = false;
	public bool highlighted = false;

	void Start()
	{
		////Find the type/evidence
		//window = this.gameObject.GetComponent<InfoWindow>();

		//if(window != null)
		//{
		//	if(window.item != null)
		//	{
		//		entry = window.item.evidence;
  //              selectableType = entry.preset.name;
		//	}
		//}

		////If no evidence entry at this point, remove this script
		//if(entry == null)
		//{
		//	Destroy(this);
		//}

		////Add to list of highlightable elemets
		//if(!InterfaceController.Instance.activeHighlightableElements.Contains(this)) InterfaceController.Instance.activeHighlightableElements.Add(this);

		////Check currently active highlights
		//if(InterfaceController.Instance.activeHighlights.Contains(selectableType) || InterfaceController.Instance.activeHighlights.Contains("all"))
		//{
		//	if(!InterfaceController.Instance.highlightable.Contains(this))
		//	{
		//		InterfaceController.Instance.highlightable.Add(this);
		//	}

		//	SetSelectable(true);
		//}
	}

	public void SetSelectable(bool tf)
	{
		selectable = tf;

		if(selectable)
		{
			enabled = true;
		}
		else
		{
			enabled = false;
		}
	}

	void OnDestroy()
	{
		////Remove from list of highlightable elements
		//if(InterfaceController.Instance.activeHighlightableElements.Contains(this)) InterfaceController.Instance.activeHighlightableElements.Remove(this);
	}

	public void OnPointerEnter(PointerEventData data)
	{
		//if(selectable)
		//{
		//	InterfaceController.Instance.NewSelected(this);
		//}
	}

	public void OnPointerExit(PointerEventData data)
	{
		if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(data, this.transform)) return;

		//if(selectable)
		//{
		//	if(InterfaceController.Instance.highlighted == this)
		//	{
		//		InterfaceController.Instance.highlighted = null;
		//	}

		//	Restore();
		//	highlighted = false;
		//}
	}

	//Detect selection
	void Update()
	{
		//if(selectable)
		//{
		//	if(highlighted)
		//	{
  //              if(window.item.instance.isItem)
  //              {
  //                  if (Input.GetMouseButtonUp(0))
  //                  {
  //                      InterfaceController.Instance.Selection(entry, window.item.instance as ItemInstance);
  //                  }
  //              }
		//	}
		//}
	}

	public void Hightlight()
	{
		//if(!selectable) return;

		//for(int i = 0; i < window.colourCodeElements.Count; ++i)
		//{
		//	Image element = window.colourCodeElements[i];
  //          element.color = window.highlightColour;
  //  	}

  //  	highlighted = true;
	}

	public void Restore()
	{
		//for(int i = 0; i < window.colourCodeElements.Count; ++i)
		//{
		//	Image element = window.colourCodeElements[i];
  //          element.color = window.baseColour;
  //  	}

  //  	highlighted = false;
	}
}
