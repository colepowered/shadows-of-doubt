﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class EvidenceTelephone : Evidence
{
    public Telephone telephone;
    public bool discoveredEverything = false;

    //Constructor
    public EvidenceTelephone(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        //Get date string from passed object
        telephone = newPassedObjects[0] as Telephone;
        interactable = telephone.interactable;

        //Listen for discovery...
        if (telephone.locationEntry.locationController.thisAsAddress != null && telephone.locationEntry.locationController.thisAsAddress.residence != null)
        {
            //Scan for telephone personal key link
            foreach (Human h in telephone.locationEntry.locationController.thisAsAddress.inhabitants)
            {
                h.evidenceEntry.OnDiscoveryChanged += OnInhabitantDiscovery;
            }
        }
    }

    //Name
    public override string GenerateName()
    {
        string ret = string.Empty;

        if(telephone != null)
        {
            ret = telephone.numberString;
        }

        return ret;
    }

    //Build data sources
    public override void BuildDataSources()
    {
        base.BuildDataSources();

        ////Build data source references
        //dataSource = new Dictionary<string, EvidenceLinkData>();

        ////Location type
        //dataSource.Add("phonenumber", new EvidenceLinkData(telephone.numberString));
    }

    public override void OnConnectedFactDiscovery(CaseComponent discovered)
    {
        base.OnConnectedFactDiscovery(discovered);

        MergedDataCheck(false);
    }

    public override void OnDiscovery()
    {
        base.OnDiscovery();

        MergedDataCheck(true);
    }

    public void OnInhabitantDiscovery(Evidence.Discovery disc)
    {
        if(disc == Discovery.livesAt || disc == Discovery.phonePersonal)
        {
            MergedDataCheck(false);
        }
    }

    //Check for merged data...
    public void MergedDataCheck(bool displayMessage)
    {
        if (discoveredEverything) return;

        bool knowLocation = false;
        List<Human> owners = new List<Human>();
        bool knowAllOwners = false;

        foreach(FactLink f in allFacts)
        {
            if(f.fact.isFound && f.fact.preset.name == "IsTelephoneNumberFor")
            {
                knowLocation = true;
                break;
            }
        }

        if(telephone.locationEntry.locationController.thisAsAddress != null && telephone.locationEntry.locationController.thisAsAddress.residence != null)
        {
            //foreach(FactLink f in telephone.locationEntry.allFacts)
            //{
            //    if(f.fact.isFound && f.fact.preset.name == "LivesAt")
            //    {
            //        Human h = f.fact.fromEvidence[0].controller as Human;

            //        if(!owners.Contains(h))
            //        {
            //            owners.Add(h);
            //        }
                        
            //        if(owners.Count >= telephone.locationEntry.locationController.thisAsAddress.inhabitants.Count)
            //        {
            //            knowAllOwners = true;
            //            break;
            //        }
            //    }
            //}

            //Scan for telephone personal key link
            foreach(Human h in telephone.locationEntry.locationController.thisAsAddress.inhabitants)
            {
                if(h.evidenceEntry.discoveryProgress.Contains(Discovery.phonePersonal))
                {
                    if (!owners.Contains(h))
                    {
                        owners.Add(h);
                    }
                }

                if(knowLocation && h.evidenceEntry.discoveryProgress.Contains(Discovery.livesAt))
                {
                    if (!owners.Contains(h))
                    {
                        owners.Add(h);
                    }
                }
            }

            if (owners.Count >= telephone.locationEntry.locationController.thisAsAddress.inhabitants.Count)
            {
                knowAllOwners = true;

                //Unsibscribe
                foreach (Human h in telephone.locationEntry.locationController.thisAsAddress.inhabitants)
                {
                    h.evidenceEntry.OnDiscoveryChanged -= OnInhabitantDiscovery;
                }
            }
        }
        else knowAllOwners = true;

        if(knowAllOwners && knowLocation)
        {
            discoveredEverything = true;
        }

        if(knowLocation || owners.Count > 0)
        {
            Game.Log("Update telephone no: " + knowLocation + " owners: " + owners.Count);
            GameplayController.Instance.AddOrMergePhoneNumberData(telephone.number, knowLocation, owners, null);
        }
    }
}
