﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "subobject_data", menuName = "Database/Sub Object Class")]

public class SubObjectClassPreset : SoCustomComparison
{
    [Header("Spawning")]
    public bool limitCountPerObject = false;
    [EnableIf("limitCountPerObject")]
    [Tooltip("If true only one of these types will be spawned per object")]
    public int maxPerObject = 1;
    [Tooltip("The chance of spawning here on a per-object basis")]
    [Range(0f, 1f)]
    public float perObjectSpawnChance = 1f;
    [Tooltip("The chance of spawning here on a per-instance basis")]
    [Range(0f, 1f)]
    public float perInstanceSpawnChance = 1f;
    [Tooltip("Added to the perInstanceSpawnChance as modifiers, uses test type found on the furniture preset")]
    [ReorderableList]
    public List<CharacterTrait.TraitPickRule> perInstanceModifiers = new List<CharacterTrait.TraitPickRule>();

    public enum PlacementTypeLimit { all, companyOnly, homeOnly, indoorsOnly, outdoorsOnly};
    public PlacementTypeLimit typeLimit = PlacementTypeLimit.all;
}
