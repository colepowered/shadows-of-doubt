﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

//Assigned to a block of buildings. In game this is practically hidden- the only visible evidence of it is the postcode.
//Script pass 1
public class BlockController : Controller, IComparable<BlockController>
{
    [Header("ID")]
    public int blockID = 0;
    public static int assignID = 0;

    [Header("Location")]
	//public DistrictController district;
	public int favourVertical = 0; //Favour vertical/Horizontal/None
    public List<CityTile> cityTiles = new List<CityTile>();

    //Average density
    [NonSerialized]
	public float averageDensity = 0f;
	[NonSerialized]
	public float averageLandValue = 0f;

    public void Setup(DistrictController newDistrict)
    {
        //Assign ID
        blockID = assignID;
        assignID++;
        name = "Block " + blockID;
        this.transform.name = name;

        newDistrict.AddBlock(this);

        //Set block to favour vertical or horizontal rows
        favourVertical = Toolbox.Instance.GetPsuedoRandomNumberContained(0, 2, newDistrict.seed, out newDistrict.seed);

        CityData.Instance.blocksDirectory.Add(this); //Add to dictionary
    }

    public void Load(CitySaveData.BlockCitySave data, DistrictController newDistrict)
    {
        name = data.name;
        blockID = data.blockID;
        averageDensity = data.averageDensity;
        averageLandValue = data.averageLandValue;

        newDistrict.AddBlock(this);

        CityData.Instance.blocksDirectory.Add(this); //Add to dictionary
    }

    //Add a city tile to this block
    public void AddCityTile(CityTile newTile)
    {
        if (!cityTiles.Contains(newTile))
        {
            //Unassign previous district
            if (newTile.block != null)
            {
                newTile.block.cityTiles.Remove(newTile);
            }

            newTile.block = this;
            //newTile.transform.SetParent(this.transform, true); //Set parent
            cityTiles.Add(newTile);

            //if(district != null) district.AddCityTile(newTile);
        }
    }

    //Update the average density value
    public void UpdateAverageDensity()
    {
        float sumDensity = 0;

        foreach(CityTile tile in cityTiles)
        {
            sumDensity += (int)tile.density;
        }

        averageDensity = (float)sumDensity / (float)cityTiles.Count;
    }

    //Update the average land value
    public void UpdateAverageLandValue()
    {
        float sumValue = 0;

        foreach (CityTile tile in cityTiles)
        {
            sumValue += (int)tile.landValue;
        }

        averageLandValue = (float)sumValue / (float)cityTiles.Count;
    }

    // Default comparer (average density)
    public int CompareTo(BlockController compare)
	{
		return this.averageDensity.CompareTo(compare.averageDensity);
	}

    //Compare by average land value
    public static Comparison<BlockController> LandValueComparison = delegate (BlockController object1, BlockController object2)
    {
        return object1.averageLandValue.CompareTo(object2.averageLandValue);
    };

    //Create save data
    public CitySaveData.BlockCitySave GenerateSaveData()
    {
        CitySaveData.BlockCitySave output = new CitySaveData.BlockCitySave();
        output.name = name;
        output.blockID = blockID;
        output.averageDensity = averageDensity;
        output.averageLandValue = averageLandValue;

        return output;
    }
}
