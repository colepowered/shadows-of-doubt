﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceSurveillance : Evidence
{
    public int captureID;
    public SceneRecorder.SceneCapture savedCapture;

    //Constructor
    public EvidenceSurveillance(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        //Use the referenced note
        Interactable.Passed passed = newPassedObjects[0] as Interactable.Passed;
        captureID = (int)passed.value;

        //Get capture...
        //Get capture from player
        if(savedCapture == null)
        {
            savedCapture = Player.Instance.sceneRecorder.interactable.sCap.Find(item => item.id == captureID);
        }

        if(savedCapture == null)
        {
            foreach(SceneRecorder cam in CityData.Instance.surveillanceDirectory)
            {
                if (cam.interactable.sCap.Count <= 0) continue;
                savedCapture = cam.interactable.sCap.Find(item => item.id == captureID);

                if (savedCapture != null) return;
            }
        }

        if(savedCapture == null)
        {
            Game.LogError("Unable to locate saved capture " + captureID);
        }
    }

    public override string GenerateName()
    {
        if (savedCapture == null)
        {
            Game.Log("Saved capture is null");
            return string.Empty;
        }
        else
        {
            string ret = string.Empty;
            NewGameLocation loc = savedCapture.GetCaptureGamelocation();

            if (loc != null) ret = loc.name + " ";
            ret += SessionData.Instance.TimeAndDate(savedCapture.t, true, true, true);

            return ret;
        }
    }

    public override void OnDiscovery()
    {
        //Create facts...
        if (savedCapture == null) return;

        if(savedCapture.recorder.interactable.node.gameLocation != null)
        {
            EvidenceCreator.Instance.CreateFact("For", this, savedCapture.recorder.interactable.node.gameLocation.evidenceEntry, forceDiscoveryOnCreate: true);

        }

        //Create appears in facts...
        //Get the humans visible in this scene. This data should be already cached so no extra work will be triggered here...
        //SceneCapture.Instance.GetSurveillanceScene(savedCapture);

        foreach (SceneRecorder.ActorCapture h in savedCapture.aCap)
        {
            Human foundCitizen = h.GetHuman();
            if(foundCitizen != null) EvidenceCreator.Instance.CreateFact("FeaturesIn", foundCitizen.evidenceEntry, this, forceDiscoveryOnCreate: true);
        }

        base.OnDiscovery();
    }
}
