﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEditor;
using System.Text.RegularExpressions;
using System.Threading;
using System.Runtime.CompilerServices;

public class GroupsController : MonoBehaviour
{
    [Header("Groups")]
    public List<SocialGroup> groups = new List<SocialGroup>();

    [System.Serializable]
    public class SocialGroup
    {
        public string preset;
        public int id = 0;
        public float decimalStartTime;
        public List<SessionData.WeekDay> weekDays;
        public List<int> members;
        public int meetingPlace;

        //Non serialized
        [System.NonSerialized]
        public List<Interactable> reserved = new List<Interactable>();

        public NewAddress GetMeetingPlace()
        {
            NewAddress meetUp = null;

            if (meetingPlace > -1)
            {
                CityData.Instance.addressDictionary.TryGetValue(meetingPlace, out meetUp);
            }

            return meetUp;
        }

        public float GetNextMeetingTime()
        {
            float duration = Toolbox.Instance.groupsDictionary[preset].meetUpLength;

            float endDecimal = decimalStartTime + duration;
            if (endDecimal >= 24f) endDecimal -= 24f;

            //Convert start time to the gametime format
            return SessionData.Instance.GetNextOrPreviousGameTimeForThisHour(weekDays, decimalStartTime, endDecimal);
        }

        public GroupPreset GetPreset()
        {
            return Toolbox.Instance.groupsDictionary[preset];
        }
    }

    public static int assignID = 1;

    //Singleton pattern
    private static GroupsController _instance;
    public static GroupsController Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    //Setup groups...
    public void CreateGroups()
    {
        //Create special interest groups...
        foreach(GroupPreset g in Toolbox.Instance.allGroups)
        {
            int instances = 1;
            List<Citizen> instancePool = new List<Citizen>();

            //If the type is set to relationships, we create many instances of a group...
            if(g.groupType == GroupPreset.GroupType.couples)
            {
                //Gather one side of each couple...
                foreach(Citizen cit in CityData.Instance.homedDirectory)
                {
                    if(cit.partner != null)
                    {
                        if(!instancePool.Contains(cit.partner))
                        {
                            instancePool.Add(cit);
                        }
                    }
                }

                instances = instancePool.Count;
            }
            else if (g.groupType == GroupPreset.GroupType.cheaters)
            {
                //Gather one side of each couple...
                foreach (Citizen cit in CityData.Instance.homedDirectory)
                {
                    if (cit.paramour != null)
                    {
                        if (!instancePool.Contains(cit.paramour))
                        {
                            instancePool.Add(cit);
                        }
                    }
                }

                instances = instancePool.Count;
            }
            else if (g.groupType == GroupPreset.GroupType.work)
            {
                //Pick the most social person in each company to form a work's social group...
                foreach (Company comp in CityData.Instance.companyDirectory)
                {
                    if(!comp.preset.isSelfEmployed)
                    {
                        Human social = null;
                        
                        foreach(Occupation j in comp.companyRoster)
                        {
                            if(j.employee != null)
                            {
                                if(j.employee.extraversion > 0.5f && (social == null || j.employee.extraversion > social.extraversion))
                                {
                                    social = j.employee;
                                }
                            }
                        }

                        if(social != null)
                        {
                            instancePool.Add(social as Citizen);
                        }
                    }
                }

                instances = instancePool.Count;
            }

            for (int u = 0; u < instances; u++)
            {
                //Apply instance chance...
                if (Toolbox.Instance.SeedRand(0f, 1f) > g.chance) continue;

                bool createGroup = false;

                //We need to find the minimum number to make a new group...
                List<Citizen> members = new List<Citizen>();

                //Pick days of the week to meet up
                List<SessionData.WeekDay> meetingDays = new List<SessionData.WeekDay>();

                //Pick a starting decimal hour (round to 1/2 hours)
                float startingDecimalHour = Mathf.Round(Toolbox.Instance.SeedRand(g.timeRange.x, g.timeRange.y) / 0.5f) * 0.5f;

                //Pick a meeting place
                List<Company> validPlaces = new List<Company>();

                if (g.enableMeetUps)
                {
                    validPlaces = CityData.Instance.companyDirectory.FindAll(item => item.preset != null && !item.preset.isSelfEmployed && item.address != null && g.meetUpLocations.Contains(item.preset));

                    if (validPlaces.Count > 0)
                    {
                        bool passTimeFinder = true;

                        //Override with convenient times for couples...
                        if (g.groupType == GroupPreset.GroupType.couples)
                        {
                            members.Add(instancePool[u]);
                            members.Add(instancePool[u].partner);
                            passTimeFinder = DecimalTimeFinder(g, members, out startingDecimalHour, out meetingDays); //This will find an appropriate time and days
                        }
                        else if (g.groupType == GroupPreset.GroupType.cheaters)
                        {
                            members.Add(instancePool[u]);
                            members.Add(instancePool[u].paramour);
                            passTimeFinder = DecimalTimeFinder(g, members, out startingDecimalHour, out meetingDays); //This will find an appropriate time and days
                        }
                        else if (g.groupType == GroupPreset.GroupType.work)
                        {
                            Citizen self = instancePool[u];

                            members.Add(self);

                            foreach(Occupation j in self.job.employer.companyRoster)
                            {
                                if(j.employee != null)
                                {
                                    if (j.employee == self) continue;

                                    Acquaintance aq = null;

                                    if(self.FindAcquaintanceExists(j.employee, out aq))
                                    {
                                        if(aq.like > 0.4f)
                                        {
                                            if(members.Count < g.maxMembers)
                                            {
                                                members.Add(j.employee as Citizen);
                                            }
                                            //Do I like this person more than someone in the group?
                                            //If so replace them!
                                            else
                                            {
                                                for (int i = 0; i < members.Count; i++)
                                                {
                                                    Citizen c = members[i];
                                                    if (c == self) continue;

                                                    Acquaintance aq2 = null;

                                                    if (self.FindAcquaintanceExists(c, out aq2))
                                                    {
                                                        if(aq2.like < aq.like)
                                                        {
                                                            members.RemoveAt(i);
                                                            members.Add(j.employee as Citizen);
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            passTimeFinder = DecimalTimeFinder(g, members, out startingDecimalHour, out meetingDays); //This will find an appropriate time and days
                        }
                        //Else pick random...
                        else
                        {
                            for (int i = 0; i < g.daysPerWeek; i++)
                            {
                                int day = Toolbox.Instance.SeedRand(0, 7);

                                while(meetingDays.Contains((SessionData.WeekDay)day))
                                {
                                    day += 2; //Try and spread meetings out a bit
                                    if (day >= 7) day -= 7;
                                }

                                meetingDays.Add((SessionData.WeekDay)day);
                            }
                        }

                        //Remove places to meet using days closed...
                        foreach(SessionData.WeekDay d in meetingDays)
                        {
                            //Remove places closed at the meeting time or at some point during the meeting...
                            validPlaces.RemoveAll(item => !item.publicFacing || !item.IsOpenAtDecimalTime(d, startingDecimalHour) || !item.IsOpenAtDecimalTime(d, startingDecimalHour + g.meetUpLength));
                        }

                        if(passTimeFinder && validPlaces.Count > 0)
                        {
                            createGroup = true;
                        }
                    }
                }
                else
                {
                    createGroup = true;
                }

                if(createGroup)
                {
                    if(g.groupType == GroupPreset.GroupType.interestGroup)
                    {
                        List<Citizen> randomList = new List<Citizen>(CityData.Instance.homedDirectory); //Homed only

                        while (randomList.Count > 0)
                        {
                            if (members.Count >= g.maxMembers) break; //We're done if we've found enough members

                            int citIndex = Toolbox.Instance.SeedRand(0, randomList.Count);
                            Citizen cit = randomList[citIndex];

                            if (!cit.isPlayer && cit.extraversion >= g.minimumExtraversion)
                            {
                                bool pass = true;

                                //Check character traits...
                                if (g.requiredTraits.Count > 0)
                                {
                                    foreach (CharacterTrait t in g.requiredTraits)
                                    {
                                        if (!cit.characterTraits.Exists(item => item.trait == t))
                                        {
                                            pass = false;
                                            break;
                                        }
                                    }
                                }

                                //Check work times...
                                if(g.enableMeetUps)
                                {
                                    if (pass && cit.job != null && cit.job.employer != null)
                                    {
                                        //Must be able to attend at least one meeting...
                                        bool dayPass = false;

                                        foreach (SessionData.WeekDay workingDay in cit.job.workDaysList)
                                        {
                                            if (!meetingDays.Contains(workingDay))
                                            {
                                                dayPass = true;
                                                break;
                                            }
                                        }

                                        //Can I attend after work?
                                        if (!dayPass && startingDecimalHour >= cit.job.endTimeDecialHour || (startingDecimalHour + 1f) <= cit.job.startTimeDecimalHour)
                                        {
                                            pass = true;
                                        }
                                        else if (!dayPass)
                                        {
                                            pass = false;
                                        }
                                    }
                                }

                                //Add to group...
                                if (pass)
                                {
                                    members.Add(cit);
                                }
                            }

                            randomList.RemoveAt(citIndex);
                        }

                        //Certain jobs required additions to groups
                        foreach(Citizen cit in CityData.Instance.homedDirectory)
                        {
                            if(cit.job != null)
                            {
                                if(cit.job.preset.joinGroups.Contains(g))
                                {
                                    if (members.Contains(cit)) continue;

                                    Game.Log("CityGen: Adding citizen to group through job requirement: " + g.name);

                                    members.Add(cit);
                                }
                            }
                        }
                    }

                    //Have we found enough members to create this group?
                    if (members.Count >= g.minMembers)
                    {
                        NewAddress meeting = null;

                        if (g.enableMeetUps)
                        {
                            meeting = validPlaces[Toolbox.Instance.SeedRand(0, validPlaces.Count)].address;
                        }
                    
                        Game.Log("CityGen: Group " + g.name + " created with " + members.Count + " members, meeting at " + meeting + " load state key: " + Toolbox.Instance.lastRandomNumberKey);

                        SocialGroup newGroup = new SocialGroup();
                        newGroup.decimalStartTime = startingDecimalHour;
                        newGroup.preset = g.name;
                        newGroup.members = new List<int>();
                        newGroup.weekDays = new List<SessionData.WeekDay>(meetingDays);
                        if(meeting != null) newGroup.meetingPlace = meeting.id;
                        newGroup.id = assignID;

                        assignID++;

                        foreach (Citizen c in members)
                        {
                            newGroup.members.Add(c.humanID);
                            c.groups.Add(newGroup); //Add group reference to citizen...

                            //Add acquaintances
                            foreach (Citizen mem in members)
                            {
                                if (c == mem) continue;

                                c.AddAcquaintance(mem, Toolbox.Instance.VectorToRandomSeedContained(SocialControls.Instance.knowGroupRange, c.seed, out c.seed), Acquaintance.ConnectionType.groupMember, true, group: newGroup);
                            }
                        }

                        groups.Add(newGroup);

                        //Start group vmail threads...
                        foreach(GroupPreset.MeetUpVmailThread thread in g.vmails)
                        {
                            //Get sender/receiver...
                            List<Human> sender = null;
                            List<Human> otherParticipants = new List<Human>();

                            if(GetVmailGroupParticiapnts(newGroup, thread.sender, out sender))
                            {
                                if(GetVmailGroupParticiapnts(newGroup, thread.recevier, out otherParticipants))
                                {
                                    DDSSaveClasses.DDSTreeSave tree = null;

                                    if (!Toolbox.Instance.allDDSTrees.TryGetValue(thread.treeID, out tree))
                                    {
                                        Game.LogError("Cannot find vmail tree " + thread.treeID);
                                        return;
                                    }

                                    //Build data source...
                                    //if(newGroup.dataSource == null)
                                    //{
                                    //    newGroup.SetupDataSource();
                                    //}

                                    //If this point is reached, the speech preset is valid so do the vmail thread. Use random progress
                                    Toolbox.Instance.NewVmailThread(sender[0], otherParticipants, thread.treeID, SessionData.Instance.gameTime + Toolbox.Instance.SeedRand(-48f, -12f), Toolbox.Instance.SeedRand(0, tree.messageRef.Count + 1), StateSaveData.CustomDataSource.groupID, newGroup.id);
                                }
                            }
                        }

                        //Spawn clues
                        foreach(GroupPreset.ClubClue clue in g.clues)
                        {
                            List<Interactable.Passed> passed = new List<Interactable.Passed>();
                            passed.Add(new Interactable.Passed(Interactable.PassedVarType.groupID, newGroup.id)); //Pass group ID

                            if(clue.spawnAt == GroupPreset.SpawnAt.meetingPlace)
                            {
                                if(meeting != null) meeting.PlaceObject(clue.preset, members[0], members[0], null, out _, passed);
                            }
                            else if(clue.spawnAt == GroupPreset.SpawnAt.leadersApartment)
                            {
                                if(members[0].home != null) members[0].home.PlaceObject(clue.preset, members[0], members[0], null, out _, passed);
                            }
                            else if(clue.spawnAt == GroupPreset.SpawnAt.entireGroupsApartments)
                            {
                                foreach(Human h in members)
                                {
                                    if(h.home != null) h.home.PlaceObject(clue.preset, members[0], members[0], null, out _, passed);
                                }
                            }
                        }
                    }
                    else
                    {
                        Game.Log("CityGen: Not enough members (" + members.Count + ") to create " + g.name);
                    }
                }
            }
        }
    }

    public bool GetVmailGroupParticiapnts(SocialGroup group, GroupPreset.MeetUpVmailSender setting, out List<Human> particiapnts)
    {
        particiapnts = new List<Human>();

        if(setting == GroupPreset.MeetUpVmailSender.groupLeader && group.members.Count > 0)
        {
            Human h = null;

            if (CityData.Instance.GetHuman(group.members[0], out h))
            {
                particiapnts.Add(h);
            }
            else return false;
        }
        else if(setting == GroupPreset.MeetUpVmailSender.entireGroup)
        {
            foreach(int i in group.members)
            {
                Human h = null;

                if (CityData.Instance.GetHuman(i, out h))
                {
                    particiapnts.Add(h);
                }
            }
        }
        else if(setting == GroupPreset.MeetUpVmailSender.groupRandom && group.members.Count > 0)
        {
            Human h = null;

            if (CityData.Instance.GetHuman(group.members[Toolbox.Instance.SeedRand(0, group.members.Count)], out h))
            {
                particiapnts.Add(h);
            }
        }
        else if(setting == GroupPreset.MeetUpVmailSender.meetupPlace && group.meetingPlace > -1)
        {
            NewAddress add = null;

            if (CityData.Instance.addressDictionary.TryGetValue(group.meetingPlace, out add))
            {
                if (add.owners.Count > 0)
                {
                    particiapnts.Add(add.owners[0]);
                }
                else return false;
            }
            else return false;
        }
        else if(setting == GroupPreset.MeetUpVmailSender.prioritiseFaithful)
        {
            foreach (int i in group.members)
            {
                Human cit = null;

                if(CityData.Instance.GetHuman(i, out cit))
                {
                    if(cit.paramour == null)
                    {
                        particiapnts.Add(cit as Human);
                        return true;
                    }
                }

                Human r = null;

                if (CityData.Instance.GetHuman(group.members[Toolbox.Instance.SeedRand(0, group.members.Count)], out r ))
                {
                    particiapnts.Add(r);
                }
            }
        }

        return true;
    }

    public void LoadGroups()
    {
        if(CityConstructor.Instance.currentData != null)
        {
            groups = new List<SocialGroup>(CityConstructor.Instance.currentData.groups);

            foreach(SocialGroup g in groups)
            {
                foreach(int member in g.members)
                {
                    Human cit = null;

                    if(CityData.Instance.GetHuman(member, out cit))
                    {
                        cit.groups.Add(g);

                        //Add all acquaintance group references...
                        foreach(int other in g.members)
                        {
                            if (other == member) continue;

                            Acquaintance aq = null;

                            Human o = null;

                            if(CityData.Instance.GetHuman(other, out o))
                            {
                                if (cit.FindAcquaintanceExists(o, out aq))
                                {
                                    aq.group = g;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //Return whether an appropriate time and days were selected...
    public bool DecimalTimeFinder(GroupPreset g, List<Citizen> people, out float appropriateTime, out List<SessionData.WeekDay> availableDays)
    {
        bool found = false;
        availableDays = new List<SessionData.WeekDay>();

        //First attempt a time after work for both...
        appropriateTime = Mathf.Round(Toolbox.Instance.SeedRand(g.timeRange.x, g.timeRange.y) / 0.5f) * 0.5f;

        Vector2 timeRange = new Vector2(appropriateTime, appropriateTime + g.meetUpLength);
        if (timeRange.y > 24f) timeRange.y -= 24;
        if (timeRange.y < 0) timeRange.y += 24;

        float checkedClock = 0f;

        while (!found && checkedClock <= 24f)
        {
            found = true;

            foreach(Citizen c in people)
            {
                if(c.job != null && c.job.employer != null)
                {
                    //Make finishing times always higher...
                    //Give an hour grace time...
                    float start = c.job.startTimeDecimalHour - 1f;
                    float end = c.job.endTimeDecialHour + 1f;

                    if (start > 24f) start -= 24;
                    if (start < 0) start += 24;

                    if (end > 24f) end -= 24;
                    if (end < 0) end += 24;

                    //This won't be compatible...
                    if (!Toolbox.Instance.DecimalTimeRangeOverlap(new Vector2(start, end), timeRange))
                    {
                        found = false;
                        break;
                    }
                }
            }

            if(!found)
            {
                appropriateTime += 0.5f;
                checkedClock += 0.5f;
                if(appropriateTime >= 24f) appropriateTime -= 24f;
            }
        }

        //If found a successful time then we can pick any days...
        if(found)
        {
            for (int i = 0; i < g.daysPerWeek; i++)
            {
                int day = Toolbox.Instance.SeedRand(0, 7);

                while (availableDays.Contains((SessionData.WeekDay)day))
                {
                    day += 2; //Try and spread meetings out a bit
                    if (day >= 7) day -= 7;
                }

                availableDays.Add((SessionData.WeekDay)day);
            }
        }
        //Otherwise pick days off work...
        else
        {
            //Doesn't matter what time now, so go for the preset's range...
            appropriateTime = Mathf.Round(Toolbox.Instance.SeedRand(g.timeRange.x, g.timeRange.y) / 0.5f) * 0.5f;

            List<SessionData.WeekDay> daysPool = new List<SessionData.WeekDay>();

            for (int i = 0; i < 7; i++)
            {
                bool dayPass = true;

                foreach (Citizen c in people)
                {
                    if (c.job != null && c.job.employer != null)
                    {
                        if(c.job.workDaysList.Contains((SessionData.WeekDay)i))
                        {
                            dayPass = false;
                            break;
                        }
                    }
                }

                if(dayPass)
                {
                    daysPool.Add((SessionData.WeekDay)i);
                }
            }

            for (int i = 0; i < g.daysPerWeek; i++)
            {
                if(daysPool.Count > 0)
                {
                    int chosenIndex = Toolbox.Instance.SeedRand(0, daysPool.Count);
                    availableDays.Add(daysPool[chosenIndex]);
                    daysPool.RemoveAt(chosenIndex);
                }
            }

            //Consider this a success if we can find a day...
            if(availableDays.Count > 0)
            {
                found = true;
            }
        }

        return found;
    }
}
