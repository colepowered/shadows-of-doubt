﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;

public class DebugPathfind : MonoBehaviour
{
    NewNode.NodeAccess access;

    [Tooltip("Parent to this room")]
    public NewRoom room;
    [Tooltip("Parent to this gamelocation")]
    public NewGameLocation gameLocation;
    [ReadOnly]
    public Vector3 fromNodePos;
    [ReadOnly]
    public Vector3 toNodePos;

    [Header("Access Details")]
    [ReadOnly]
    public bool walkingAccess = false;
    [ReadOnly]
    public bool employeeDoor = false;
    [ReadOnly]
    public bool noPassThroughOnFromNode = false;
    [ReadOnly]
    public bool noPassThroughOnToNode = false;
    [ReadOnly]
    public bool noAccessOnFromNode = false;
    [ReadOnly]
    public bool noAccessOnToNode = false;

    [System.Serializable]
    public class DebugLocationLink
    {
        public string name;
        public NewNode.NodeAccess access;

        public DebugLocationLink(NewNode.NodeAccess acc, string reason)
        {
            access = acc;
            name = acc.fromNode.gameLocation.name + " -> " + acc.toNode.gameLocation.name + " (" + reason + ")";
        }
    }

    [Space(7)]
    [Tooltip("Links to other gamelocations")]
    public List<DebugLocationLink> locationLinkAttempts = new List<DebugLocationLink>();

    public void Setup(NewNode.NodeAccess newAccess, NewRoom newRoom, List<DebugLocationLink> linkList)
    {
        access = newAccess;
        walkingAccess = access.walkingAccess;
        employeeDoor = access.employeeDoor;
        noPassThroughOnFromNode = access.fromNode.noPassThrough;
        noPassThroughOnToNode = access.toNode.noPassThrough;
        noAccessOnFromNode = access.fromNode.noAccess;
        noAccessOnToNode = access.toNode.noAccess;

        fromNodePos = access.fromNode.position;
        toNodePos = access.toNode.position;

        room = newRoom;
        gameLocation = newRoom.gameLocation;
        locationLinkAttempts = linkList;

        if(room == null)
        {
            room = access.fromNode.room;
        }

        this.gameObject.name = room.name + ", " + room.gameLocation.name;
        this.transform.position = access.worldAccessPoint + new Vector3(0, 1, 0);
        this.transform.localScale = new Vector3(0.25f, 0.25f, 1.8f);

        Vector3 direction = access.toNode.position - access.fromNode.position;
        this.transform.rotation = Quaternion.LookRotation(direction, Vector3.up);
    }

    [Button]
    public void TeleportPlayer()
    {
        Player.Instance.Teleport(access.fromNode, null);
    }
}
