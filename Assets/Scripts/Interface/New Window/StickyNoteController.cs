﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class StickyNoteController : MonoBehaviour
{
    public RectTransform rect;
    public WindowContentController windowContent;
    public InfoWindow parentWindow;
    public TMP_InputField input;
    public TextMeshProUGUI text;

    private void OnEnable()
    {
        if (rect == null) rect = this.gameObject.GetComponent<RectTransform>();
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        if (windowContent == null) windowContent = this.gameObject.GetComponentInParent<WindowContentController>();

        //Get content
        input.SetTextWithoutNotify(parentWindow.passedEvidence.GetNote((new Evidence.DataKey[]{ Evidence.DataKey.name}).ToList()));
    }

    private void OnDisable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
    }

    public void OnNoteEdit()
    {
        //Save content
        parentWindow.passedEvidence.SetNote((new Evidence.DataKey[] { Evidence.DataKey.name }).ToList(), input.text);
    }

    public void SetPlayerTextInput(bool val)
    {
        InterfaceController.Instance.SetPlayerTextInput(val);
    }
}
