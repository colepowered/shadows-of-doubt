﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class SideJobStealBriefcase : SideJob
{
    [Header("Serialized Data")]
    public int carrier = -1;
    public Vector3Int meetNodeLocation;
    public bool triggeredSwitch = false;
    public bool triggeredMeet = false;
    public float meetTimer = 0f;

    [System.NonSerialized]
    public Human caseCarrier;
    [System.NonSerialized]
    public NewNode destination;
    private float gwTime = 0f;
    private Objective waitObjective = null;

    public struct NodeCompare
    {
        public NewNode node;
        public float score;
    }

    public SideJobStealBriefcase(JobPreset newPreset, SideJobController.JobPickData newData, bool immediatePost) : base(newPreset, newData, immediatePost)
    {

    }

    //Pick meeting location; somewhere empty, dark, unvisited etc.
    private void PickMeet()
    {
        List<NodeCompare> nodePool = new List<NodeCompare>();

        foreach(NewGameLocation st in CityData.Instance.gameLocationDirectory)
        {
            foreach(NewRoom r in st.rooms)
            {
                float roomScore = 0f;

                if(Toolbox.Instance.RankRoomShadiness(r, out roomScore))
                {
                    foreach(NewNode n in r.nodes)
                    {
                        float nodeScore = 0f;

                        if(Toolbox.Instance.RankNodeShadiness(n, out nodeScore))
                        {
                            nodePool.Add(new NodeCompare { node = n, score = roomScore + nodeScore });
                        }
                    }
                }
            }
        }

        //Pick a node in the top 25%
        if(nodePool.Count > 0)
        {
            nodePool.Sort((p1, p2) => p2.score.CompareTo(p1.score)); //Hightest first
            NodeCompare chosen = nodePool[Toolbox.Instance.Rand(0, Mathf.CeilToInt(nodePool.Count * 0.25f))];
            destination = chosen.node;
            meetNodeLocation = chosen.node.nodeCoord;
            Game.Log("Jobs: Chosen meeting point for briefcase job: " + chosen.node.position + " out of " + nodePool.Count + " options...");
        }
    }

    //Triggered as the player picks up the phone; must be the right call...
    //We need to do this to select the carrier before the phone message is displayed...
    public override void OnGooseChaseSuccess()
    {
        base.OnGooseChaseSuccess();
        SetupCarrier();
    }

    public void SetupCarrier()
    {
        //Pick someone nearby to become the briefcase carrier...
        caseCarrier = null;
        float bestScore = -9999f;

        //Is there nobody here? Teleport someone in...
        if(Player.Instance.currentRoom.currentOccupants.Count <= 1)
        {
            Human rando = CityData.Instance.citizenDirectory.Find(item => !item.isAsleep && !item.isStunned && item != purp && item != poster && item.humility < 0.7f);
            if (rando != null) rando.Teleport(rando.FindSafeTeleport(Player.Instance.currentRoom), null);
        }

        foreach (Actor actor in Player.Instance.currentRoom.currentOccupants)
        {
            Human hu = actor as Human;

            if (hu != null)
            {
                if (hu.isPlayer) continue;
                if (hu.ai == null) continue;

                if (caseCarrier == null)
                {
                    caseCarrier = hu;
                    continue;
                }

                float score = (1f - hu.humility) * 5f; //Value citizens with low humility

                //Value sitting citizens
                if (actor.animationController.idleAnimationState == CitizenAnimationController.IdleAnimationState.sitting)
                {
                    score += 10;
                }

                if (score > bestScore)
                {
                    caseCarrier = hu;
                    bestScore = score;
                }
            }
        }

        if (caseCarrier != null)
        {
            carrier = caseCarrier.humanID;
            meetTimer = Toolbox.Instance.Rand(3f, 5f) * 0.01667f; //Set the leaving timer to 2 to 4 in game mins
            Game.Log("Jobs: Chosen briefcase carrier " + caseCarrier.name + "...");
        }
    }

    public override void OnAcquireJobInfo(string infoDialogMessage)
    {
        base.OnAcquireJobInfo(infoDialogMessage);

        //Spawn a briefcase in their possession
        Interactable briefcase = GetItem(JobPreset.JobTag.A);

        if (briefcase != null)
        {
            Game.Log("Jobs: Spawned briefcase: " + briefcase.preset.name);
            briefcase.SetInInventory(caseCarrier); //Set in inventory of carrier

            //Pick the best meeting point
            PickMeet();
        }
    }

    public NewNode GetLocationNode()
    {
        if (destination != null) return destination;
        NewNode ret = null;
        PathFinder.Instance.nodeMap.TryGetValue(meetNodeLocation, out ret);
        return ret;
    }

    public override Human GetExtraPerson1()
    {
        if (caseCarrier != null) return caseCarrier;
        Human ret = null;
        CityData.Instance.GetHuman(carrier, out ret);
        return ret;
    }

    public override void GameWorldLoop()
    {
        base.GameWorldLoop();

        //Make sure purp and carrier are meeting
        if(knowHandInLocation && carrier > -1 && !triggeredMeet)
        {
            if(meetTimer > 0f)
            {
                meetTimer -= SessionData.Instance.gameTime - gwTime;
                Game.Log("Jobs: Briefcase job: Meet timer for exchange: " + meetTimer);
            }
            else
            {
                Game.Log("Jobs: Briefcase job: Meeting should happen soon...");

                if (caseCarrier == null) caseCarrier = GetExtraPerson1();
                if (destination == null) destination = GetLocationNode();

                bool carrierGoalActive = false;

                //Make sure case carrier is going to destination
                if (caseCarrier != null && caseCarrier.ai != null && caseCarrier.ai.goals != null)
                {
                    caseCarrier.ai.SetDesiredTickRate(NewAIController.AITickRate.veryHigh);

                    NewAIGoal goTo = caseCarrier.ai.goals.Find(item => item.preset == RoutineControls.Instance.toGoWalkGoal && item.passedNode == destination);

                    if (goTo == null)
                    {
                        Game.Log("Jobs: Briefcase job: Did not find case carrier goto goal...");

                        caseCarrier.AddBladder(1f);
                        caseCarrier.AddHydration(1f);
                        caseCarrier.AddNourishment(1f);
                        caseCarrier.AddEnergy(1f);
                        caseCarrier.AddHeat(1f);
                        caseCarrier.AddAlertness(1f);
                        caseCarrier.AddExcitement(1f);
                        caseCarrier.AddHygiene(1f);
                        caseCarrier.AddNerve(1f);

                        if(destination != null)
                        {
                            Game.Log("Jobs: Briefcase job: Creating goal for " + caseCarrier.name + " to go to " + destination.position);
                            caseCarrier.ai.CreateNewGoal(RoutineControls.Instance.toGoWalkGoal, 0f, 0f, newPassedNode: destination);
                        }
                    }
                    else
                    {
                        Game.Log("Jobs: Briefcase job: Found existing case carrier goto goal...");

                        if (!goTo.isActive)
                        {
                            carrierGoalActive = false;
                            caseCarrier.ai.AITick(true); //Force update of goals/priorities
                        }
                        else
                        {
                            if (caseCarrier.ai.currentGoal == goTo)
                            {
                                carrierGoalActive = true;
                            }
                            else carrierGoalActive = false;
                        }
                    }
                }

                //Make sure purp is going to destination
                if (purp != null && purp.ai != null && purp.ai.goals != null)
                {
                    NewAIGoal goTo = purp.ai.goals.Find(item => item.preset == RoutineControls.Instance.toGoGoal && item.passedNode == destination);

                    if (goTo == null)
                    {
                        purp.AddBladder(1f);
                        purp.AddHydration(1f);
                        purp.AddNourishment(1f);
                        purp.AddEnergy(1f);
                        purp.AddHeat(1f);
                        purp.AddAlertness(1f);
                        purp.AddExcitement(1f);
                        purp.AddHygiene(1f);
                        purp.AddNerve(1f);

                        if(destination != null)
                        {
                            Game.Log("Jobs: Briefcase job: Creating goal for " + purp.name + " to go to " + destination.position);
                            purp.ai.CreateNewGoal(RoutineControls.Instance.toGoGoal, 0f, 0f, newPassedNode: destination);
                        }
                    }
                    else
                    {
                        purp.ai.SetDesiredTickRate(NewAIController.AITickRate.veryHigh);

                        if (!goTo.isActive) purp.ai.AITick(true); //Force update of goals/priorities
                        else
                        {
                            if(carrierGoalActive)
                            {
                                //If the purp is going to arrive after the carrier, make them run
                                if(purp.ai.currentAction != null && caseCarrier != null && caseCarrier.ai != null && caseCarrier.ai.currentAction != null)
                                {
                                    if (purp.ai.currentAction.estimatedArrival > caseCarrier.ai.currentAction.estimatedArrival || caseCarrier.ai.currentAction.isAtLocation)
                                    {
                                        purp.SetDesiredSpeed(Human.MovementSpeed.running);
                                    }
                                }
                            }
                        }
                    }
                }

                //Check for meet trigger...
                if(caseCarrier != null && purp != null && caseCarrier.ai != null && purp.ai != null)
                {
                    if(Vector3.Distance(purp.transform.position, caseCarrier.transform.position) <= 3f)
                    {
                        if(caseCarrier.ai.trackedTargets.Exists(item => item.actor == purp) || purp.ai.trackedTargets.Exists(item => item.actor == caseCarrier))
                        {
                            Game.Log("Jobs: Briefcase job: Trigger meet...");
                            triggeredMeet = true;

                            //Init convo...
                            DDSSaveClasses.DDSTreeSave convo = null;

                            if(Toolbox.Instance.allDDSTrees.TryGetValue("18fea754-5675-4f73-aeb8-78c0a754465e", out convo))
                            {
                                Game.Log("Jobs: Briefcase job: Executing conversation");
                                caseCarrier.ExecuteConversationTree(convo, (new Human[] { purp }).ToList());
                            }
                        }

                        if(waitObjective != null)
                        {
                            waitObjective.Complete();
                        }
                    }
                    else
                    {
                        if(waitObjective == null)
                        {
                            waitObjective = thisCase.currentActiveObjectives.Find(item => item.queueElement.entryRef == "Wait for the meeting to take place");

                            if (waitObjective == null)
                            {
                                Game.Log("Jobs: Briefcase job: Setting up wait objective");
                                Objective.ObjectiveTrigger tailBriefcase = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.sideMissionMeetTriggered, "");
                                AddObjective("Wait for the meeting to take place", tailBriefcase, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nothing, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.time);
                            }
                        }
                    }
                }
            }
        }
        else if(triggeredMeet && !triggeredSwitch)
        {
            if(caseCarrier != null && !caseCarrier.inConversation && caseCarrier.ai != null)
            {
                Interactable briefcase = GetItem(JobPreset.JobTag.A);

                if(briefcase != null && purp != null && purp.ai != null)
                {
                    briefcase.SetAsNotInventory(caseCarrier.currentNode);
                    briefcase.SetInInventory(purp);

                    Game.Log("Jobs: Briefcase job: Triggered briefcase switch...");
                    caseCarrier.ai.UpdateHeldItems(AIActionPreset.ActionStateFlag.none);
                    purp.ai.UpdateHeldItems(AIActionPreset.ActionStateFlag.none);

                    if(caseCarrier.animationController != null) caseCarrier.animationController.SetArmsBoolState(CitizenAnimationController.ArmsBoolSate.armsOneShotUse);
                    if(purp.animationController != null) purp.animationController.SetArmsBoolState(CitizenAnimationController.ArmsBoolSate.armsOneShotUse);

                    //Purp now goes home to have a shower
                    purp.AddHygiene(-1f);
                    if(purp.home != null) purp.ai.CreateNewGoal(RoutineControls.Instance.toGoWalkGoal, 0f, 0f, newPassedNode: purp.FindSafeTeleport(purp.home));

                    triggeredSwitch = true;
                }
            }
        }

        gwTime = SessionData.Instance.gameTime;
    }

    public override void UpdateResolveAnswers()
    {
        base.UpdateResolveAnswers();

        Case.ResolveQuestion doc = thisCase.resolveQuestions.Find(item => item.tag == JobPreset.JobTag.B);

        if(doc != null)
        {
            doc.correctAnswers = new List<string>();

            //Photograph including both carrier and purp
            List<Interactable> surveillance = CityData.Instance.interactableDirectory.FindAll(item => item.preset == InteriorControls.Instance.surveillancePrintout);

            foreach (Interactable s in surveillance)
            {
                if (s.evidence != null)
                {
                    EvidenceSurveillance ev = s.evidence as EvidenceSurveillance;

                    if (ev != null && ev.savedCapture != null)
                    {
                        if (purp != null && ev.savedCapture.aCap.Exists(item => item.id == purp.humanID))
                        {
                            if (ev.savedCapture.aCap.Exists(item => item.id == carrier))
                            {
                                if (!doc.correctAnswers.Contains(ev.evID)) doc.correctAnswers.Add(ev.evID);
                            }
                        }
                    }
                }
            }
        }
    }

    public override void OnDestroyMissionObject(Interactable destroyed)
    {
        base.OnDestroyMissionObject(destroyed);

        //This is now done automatically with block triggers (untested for this mission type)
        //Game.Log("SideJobStealBriefcase: " + destroyed.name + " destroyed!");

        //foreach(KeyValuePair<JobPreset.JobTag, Interactable> pair in activeJobItems)
        //{
        //    if(pair.Value == destroyed)
        //    {
        //        Game.Log("Found destroyed tag " + pair.Key.ToString());

        //        if (pair.Key == JobPreset.JobTag.A)
        //        {
        //            if(handIn != "DestroyItem")
        //            {
        //                TriggerFail("Briefcase was destroyed");
        //                break;
        //            }
        //        }
        //    }
        //}
    }
}
