﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "designstyle_data", menuName = "Database/Decor/Design Style Preset")]

public class DesignStylePreset : SoCustomComparison
{
    [Header("Suited Personality")]
    [Tooltip("Include this when using citizen stats to pick a style")]
    public bool includeInPersonalityMatching = true;

    [Tooltip("Compatible Units")]
    [ReorderableList]
    public List<LayoutConfiguration> compatibleAddressTypes = new List<LayoutConfiguration>();

    [Tooltip("The citizen/company must have at least this much wealth to use this decor")]
    [Range(0f, 1f)]
    public float minimumWealth = 0f;
    [Space(5)]
    [Tooltip("Honesty-Humility (H): sincere, honest, faithful, loyal, modest/unassuming versus sly, deceitful, greedy, pretentious, hypocritical, boastful, pompous")]
    [Range(0, 10)]
    public int humility = 5;
    [Tooltip("Emotionality (E): emotional, oversensitive, sentimental, fearful, anxious, vulnerable versus brave, tough, independent, self-assured, stable")]
    [Range(0, 10)]
    public int emotionality = 5;
    [Tooltip("Extraversion (X): outgoing, lively, extraverted, sociable, talkative, cheerful, active versus shy, passive, withdrawn, introverted, quiet, reserved")]
    [Range(0, 10)]
    public int extraversion = 5;
    [Tooltip("Agreeableness (A): patient, tolerant, peaceful, mild, agreeable, lenient, gentle versus ill-tempered, quarrelsome, stubborn, choleric")]
    [Range(0, 10)]
    public int agreeableness = 5;
    [Tooltip("Conscientiousness (C): organized, disciplined, diligent, careful, thorough, precise versus sloppy, negligent, reckless, lazy, irresponsible, absent-minded")]
    [Range(0, 10)]
    public int conscientiousness = 5;
    [Tooltip("Openness to Experience (O): intellectual, creative, unconventional, innovative, ironic versus shallow, unimaginative, conventional")]
    [Range(0, 10)]
    public int creativity = 5;

    [Header("Suited Colour Schemes")]
    [Range(0, 10)]
    public int modernity = 5;

    [Header("Ceilings")]
    public bool allowCoving = true;

    [Header("Misc.")]
    [Tooltip("Force this style if below ground")]
    public bool isBasement = false;
}
