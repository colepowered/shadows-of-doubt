using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class LaserGridController : SwitchSyncBehaviour
{
    [Header("Components")]
    public Transform movementParent;
    public Transform laserParent;
    public Transform laser;
    public InteractableController controller;

    [Header("Settings")]
    public float speed = 1f;
    public float range = 4.78f;

    public bool useMovementX = false;
    [EnableIf("useMovementX")]
    public AnimationCurve movementX;

    public bool useMovementY = false;
    [EnableIf("useMovementY")]
    public AnimationCurve movementY;

    public bool useMovementZ = false;
    [EnableIf("useMovementZ")]
    public AnimationCurve movementZ;

    public bool useRotationX = false;
    [EnableIf("useRotationX")]
    public AnimationCurve rotationX;

    public bool useRotationY = false;
    [EnableIf("useRotationY")]
    public AnimationCurve rotationY;

    public bool useRotationZ = false;
    [EnableIf("useRotationZ")]
    public AnimationCurve rotationZ;

    [Header("State")]
    public float cycle = 0f;
    public bool bounce = false;
    public float randomMultiplier = 0f;

    private void Awake()
    {
        cycle = Toolbox.Instance.Rand(0f, 1f);
        randomMultiplier = Toolbox.Instance.Rand(0.8f, 1.2f);
    }

    // Update is called once per frame
    void Update()
    {
        if(isOn)
        {
            if(SessionData.Instance.play)
            {
                if(!bounce)
                {
                    if (cycle < 1f) cycle += Time.deltaTime * speed * randomMultiplier;
                    else bounce = true;
                }
                else if(bounce)
                {
                    if (cycle > 0f) cycle -= Time.deltaTime * speed * randomMultiplier;
                    else bounce = false;
                }

                Vector3 movement = movementParent.localPosition;
                if (useMovementX) movement.x = movementX.Evaluate(cycle);
                if (useMovementY) movement.y = movementY.Evaluate(cycle);
                if (useMovementZ) movement.z = movementZ.Evaluate(cycle);

                Vector3 euler = movementParent.localEulerAngles;
                if (useRotationX) euler.x = rotationX.Evaluate(cycle);
                if (useRotationY) euler.y = rotationY.Evaluate(cycle);
                if (useRotationZ) euler.z = rotationZ.Evaluate(cycle);

                movementParent.localPosition = movement;
                movementParent.localEulerAngles = euler;
            }

            //Do laser
            RaycastHit rayHit;
            Ray r = new Ray(laserParent.transform.position, laserParent.transform.forward);

            if (Physics.Raycast(r, out rayHit, range, Toolbox.Instance.aiSightingLayerMask, QueryTriggerInteraction.Collide))
            {
                //Unparent to set global scale
                Transform originalParent = laser.transform.parent;
                laser.transform.parent = null;
                laser.transform.localScale = new Vector3(laser.transform.localScale.x, laser.transform.localScale.y, rayHit.distance);
                laser.transform.parent = originalParent;

                //Is AI?
                Human hu = null;
                Player getPlayer = rayHit.transform.GetComponentInChildren<Player>();

                if (getPlayer != null && !Game.Instance.invisiblePlayer)
                {
                    hu = getPlayer;
                }
                else
                {
                    Citizen getCitizen = rayHit.transform.GetComponentInChildren<Citizen>();

                    if (getCitizen != null)
                    {
                        hu = getCitizen;
                    }
                }

                if (hu != null && !SessionData.Instance.isFloorEdit)
                {
                    hu.UpdateTrespassing(true);

                    if(hu.isTrespassing && hu.trespassingEscalation > 1)
                    {
                        hu.currentNode.gameLocation.thisAsAddress.SetAlarm(true, hu); //Trigger alarm!
                    }
                }
            }
            else
            {
                //Unparent to set global scale
                Transform originalParent = laser.transform.parent;
                laser.transform.parent = null;
                laser.transform.localScale = new Vector3(laser.transform.localScale.x, laser.transform.localScale.y, range);
                laser.transform.parent = originalParent;
            }
        }
    }
}
