﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using NaughtyAttributes;
using System.Linq;
using System.Reflection;

public class StatusController : MonoBehaviour
{
    [Header("References")]
    public RectTransform statusParent;

    [Header("Settings")]
    public float elementDefaultWdith = 280f;
    public float elementMinimizedWidth = 42f;
    public float elementDefaultHeight = 42f;
    public float elementYInterval = -4f;
    public AnimationCurve detailTextFadeInCurve;

    [Header("State")]
    [ReadOnly]
    public bool disabledRecovery = false;
    [ReadOnly]
    public bool disabledSprint = false;
    [ReadOnly]
    public bool disabledJump = false;
    [ReadOnly]
    public float recoveryRateMultiplier = 1f;
    [ReadOnly]
    public float maxHealthMultiplier = 1f;
    [ReadOnly]
    public float movementSpeedMultiplier = 1f;
    [ReadOnly]
    public float temperatureGainMultiplier = 1f;
    [ReadOnly]
    public float damageIncomingMultiplier = 1f;
    [ReadOnly]
    public float damageOutgoingMultiplier = 1f;
    [ReadOnly]
    public float drunkControls = 0f;
    public Dictionary<AnimationCurve, float> affectHeadBobs = new Dictionary<AnimationCurve, float>();
    [ReadOnly]
    public float drunkVision = 0f;
    [ReadOnly]
    public float shiverVision = 0f;
    [ReadOnly]
    public float headacheVision = 0f;
    [ReadOnly]
    public float drunkLensDistort = 0f;
    [ReadOnly]
    public float tripChanceWet = 0f;
    [ReadOnly]
    public float tripChanceDrunk = 0f;
    [ReadOnly]
    public float bloomIntensityMultiplier = 1;
    [ReadOnly]
    public float motionBlurMultiplier = 1;
    [ReadOnly]
    public float chromaticAbberationAmount = 0;
    [ReadOnly]
    public float vignetteAmount = 1;
    [ReadOnly]
    public float exposureAmount = 0;
    [ReadOnly]
    public float channelRedR = 100;
    [ReadOnly]
    public float channelRedG = 0;
    [ReadOnly]
    public float channelRedB = 0;
    [ReadOnly]
    public float channelGreenR = 0;
    [ReadOnly]
    public float channelGreenG = 100;
    [ReadOnly]
    public float channelGreenB = 0;
    [ReadOnly]
    public float channelBlueR = 0;
    [ReadOnly]
    public float channelBlueG = 0;
    [ReadOnly]
    public float channelBlueB = 100;

    [Header("Interface")]
    public List<StateElementController> spawnedControllers = new List<StateElementController>();

    //Active status counts
    public Dictionary<StatusInstance, List<StatusCount>> activeStatusCounts = new Dictionary<StatusInstance, List<StatusCount>>();
    public HashSet<StatusPreset> activeStatuses = new HashSet<StatusPreset>(); //For quickly checking active statuses

    //Special case used for potential fines...
    public List<FineRecord> activeFineRecords = new List<FineRecord>();

    [System.Serializable]
    public class FineRecord
    {
        public int addressID = -1; //Used for address-specific instances
        public int objectID = -1; //Used for object-specific instances
        public CrimeType crime;
        public bool confirmed = false; //If unconfirmed, this can be avoided (eg in the case of trespassing)
        public int forcedPenalty = -1;

        public FineRecord(NewAddress ad, Interactable obj, CrimeType newCrime)
        {
            if(ad != null) addressID = ad.id;
            if (obj != null) objectID = obj.id;
            crime = newCrime;
        }

        public void SetConfirmed(bool val)
        {
            if(confirmed != val)
            {
                confirmed = val;
            }
        }
    }

    public enum CrimeType { assault, theft, breakingAndEntering, trespassing, tampering, vandalism};

    public struct StatusInstance
    {
        public StatusPreset preset;
        public NewBuilding building;
        public NewAddress address;
    }

    public class StatusCount
    {
        public StatusInstance statusInstance;
        public StatusPreset preset;
        public StatusPreset.StatusCountConfig statusCountConfig;
        public FineRecord fineRecord;

        public float amount = 0f;

        //If passed without a count config, use the first (and hopefully only) as a default
        public StatusCount(StatusInstance newInstance)
        {
            statusInstance = newInstance;
            preset = statusInstance.preset;
            statusCountConfig = preset.countConfig[0];

            if (preset.onAcquire != null)
            {
                if (SessionData.Instance.startedGame && (CityConstructor.Instance == null || !CityConstructor.Instance.preSimActive)) AudioController.Instance.Play2DSound(preset.onAcquire);
            }

            if (statusCountConfig.onAcquire != null)
            {
                if (SessionData.Instance.startedGame && (CityConstructor.Instance == null || !CityConstructor.Instance.preSimActive)) AudioController.Instance.Play2DSound(statusCountConfig.onAcquire);
            }

            if (!StatusController.Instance.activeStatusCounts.ContainsKey(statusInstance))
            {
                StatusController.Instance.activeStatusCounts.Add(statusInstance, new List<StatusCount>());
            }

            StatusController.Instance.activeStatusCounts[statusInstance].Add(this);

            if(!StatusController.Instance.activeStatuses.Contains(preset))
            {
                StatusController.Instance.activeStatuses.Add(preset);
                //Game.Log("New status: " + preset.name);
            }

            if(preset.alertWhenNewCountIsAdded)
            {
                StateElementController sec = StatusController.Instance.spawnedControllers.Find(item => item.statusInstance.preset == statusInstance.preset && item.statusInstance.building == statusInstance.building && item.statusInstance.address == statusInstance.address);

                if(sec != null)
                {
                    sec.SetMinimized(false);
                    sec.iconJuice.Flash(2, false);
                    sec.juice.Flash(2, false);
                }
            }
        }

        public StatusCount(StatusInstance newInstance, StatusPreset.StatusCountConfig newConfig)
        {
            statusInstance = newInstance;
            preset = statusInstance.preset;
            statusCountConfig = newConfig;

            if (preset.onAcquire != null)
            {
                if (SessionData.Instance.startedGame && (CityConstructor.Instance == null || !CityConstructor.Instance.preSimActive)) AudioController.Instance.Play2DSound(preset.onAcquire);
            }

            if(statusCountConfig.onAcquire != null)
            {
                if (SessionData.Instance.startedGame && (CityConstructor.Instance == null || !CityConstructor.Instance.preSimActive)) AudioController.Instance.Play2DSound(statusCountConfig.onAcquire);
            }

            if (!StatusController.Instance.activeStatusCounts.ContainsKey(statusInstance))
            {
                StatusController.Instance.activeStatusCounts.Add(statusInstance, new List<StatusCount>());
            }

            StatusController.Instance.activeStatusCounts[statusInstance].Add(this);

            if (!StatusController.Instance.activeStatuses.Contains(preset))
            {
                StatusController.Instance.activeStatuses.Add(preset);
                Game.Log("Player: New status: " + preset.name);
            }

            if (preset.alertWhenNewCountIsAdded)
            {
                StateElementController sec = StatusController.Instance.spawnedControllers.Find(item => item.statusInstance.preset == statusInstance.preset && item.statusInstance.building == statusInstance.building && item.statusInstance.address == statusInstance.address);

                if (sec != null)
                {
                    //sec.SetMinimized(false);
                    sec.iconJuice.Flash(2, false);
                    sec.juice.Flash(2, false);
                }
            }
        }

        public void Remove()
        {
            StatusController.Instance.activeStatusCounts[statusInstance].Remove(this);

            if (preset.alertWhenNewCountIsAdded)
            {
                StateElementController sec = StatusController.Instance.spawnedControllers.Find(item => item.statusInstance.preset == statusInstance.preset && item.statusInstance.building == statusInstance.building && item.statusInstance.address == statusInstance.address);

                if (sec != null)
                {
                    sec.iconJuice.Flash(2, false);
                    sec.juice.Flash(2, false);
                }
            }

            if(!StatusController.Instance.activeStatusCounts.ContainsKey(statusInstance) || StatusController.Instance.activeStatusCounts[statusInstance].Count <= 0)
            {
                if(StatusController.Instance.activeStatuses.Contains(preset))
                {
                    //Detect if all active statuses of this type have been removed
                    int remaining = 0;

                    foreach(KeyValuePair<StatusInstance, List<StatusCount>> pair in StatusController.Instance.activeStatusCounts)
                    {
                        if(pair.Key.preset == preset)
                        {
                            remaining += pair.Value.Count;

                            if(remaining > 0)
                            {
                                break;
                            }
                        }
                    }

                    if(remaining <= 0)
                    {
                        StatusController.Instance.activeStatuses.Remove(preset);
                        //Game.Log("Remove status: " + preset.name);
                    }
                }
            }

            if (preset.maxHealthPlusMP != 0f) Player.Instance.AddHealth(0f); //Reset health
        }

        public int GetPenaltyAmount()
        {
            int ret = 0;

            if(fineRecord != null && fineRecord.forcedPenalty > -1)
            {
                return fineRecord.forcedPenalty;
            }

            if (statusCountConfig.penaltyRule == StatusPreset.PenaltyRule.fixedValue)
            {
                ret = Mathf.CeilToInt(statusCountConfig.penalty);
            }
            else if(statusCountConfig.penaltyRule == StatusPreset.PenaltyRule.percentageValue)
            {
                ret = Mathf.CeilToInt(GameplayController.Instance.money * statusCountConfig.penalty);
            }
            else if(statusCountConfig.penaltyRule == StatusPreset.PenaltyRule.objectValueMultiplied)
            {
                if(fineRecord != null)
                {
                    Interactable i = null;

                    if(CityData.Instance.savableInteractableDictionary.TryGetValue(fineRecord.objectID, out i))
                    {
                        ret = Mathf.CeilToInt(i.val * statusCountConfig.penalty);
                    }
                }
            }

            return ret;
        }
    }

    //For holding references to all checking methods
    Dictionary<StatusPreset, MethodInfo> checkingRef = new Dictionary<StatusPreset, MethodInfo>(); //Store pre-configured method info in here for speed

    //Singleton pattern
    private static StatusController _instance;
    public static StatusController Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        //Load status into methods
        Toolbox.Instance.allStatuses.Sort((p2, p1) => p1.priority.CompareTo(p2.priority));

        foreach (StatusPreset s in Toolbox.Instance.allStatuses)
        {
            if (!s.useCustomMethod) continue;

            //Create method info
            MethodInfo mi = this.GetType().GetMethod(s.name);

            if (mi != null)
            {
                checkingRef.Add(s, mi);
            }
        }

        BioScreenController.Instance.OnInventoryOpenChange += DisplayCheck;
        DisplayCheck();
    }

    private void DisplayCheck()
    {
        //Un-minimize any icons...
        if(BioScreenController.Instance.isOpen)
        {
            foreach(StateElementController sec in spawnedControllers)
            {
                sec.SetMinimized(false);
                sec.SetMaximized(true);
            }
        }
        else
        {
            foreach (StateElementController sec in spawnedControllers)
            {
                sec.SetMaximized(false);
            }
        }
    }

    //Remove all counts of a preset
    public void RemoveAllCounts(StatusInstance inst)
    {
        if (activeStatusCounts.ContainsKey(inst))
        {
            if (activeStatusCounts[inst].Count > 0 && inst.preset.onRemove != null)
            {
                if(SessionData.Instance.startedGame && (CityConstructor.Instance == null || !CityConstructor.Instance.preSimActive)) AudioController.Instance.Play2DSound(inst.preset.onRemove);
            }

            while (activeStatusCounts[inst].Count > 0)
            {
                activeStatusCounts[inst][0].Remove();
            }
        }

        if (inst.preset.maxHealthPlusMP != 0f) Player.Instance.AddHealth(0f); //Reset health
    }

    public void RemoveAllCounts(StatusPreset preset)
    {
        List<StatusInstance> toRemove = new List<StatusInstance>();

        foreach(KeyValuePair<StatusInstance, List<StatusCount>> pair in activeStatusCounts)
        {
            if(pair.Key.preset == preset)
            {
                toRemove.Add(pair.Key);
            }
        }

        foreach(StatusInstance si in toRemove)
        {
            activeStatusCounts.Remove(si);
            if (SessionData.Instance.startedGame && (CityConstructor.Instance == null || !CityConstructor.Instance.preSimActive)) AudioController.Instance.Play2DSound(preset.onRemove);
        }

        if (preset.maxHealthPlusMP != 0f) Player.Instance.AddHealth(0f); //Reset health
    }

    //Execute a status check for all statuses
    public void ForceStatusCheck()
    {
        ForceStatusCheck(false);
    }

    public void ForceStatusCheck(bool bypassKOCheck = false)
    {
        if (CutSceneController.Instance.cutSceneActive) return; //Don't do in cut scenes

        //if (!bypassKOCheck && Player.Instance.playerKOInProgress) return;

        //Game.Log("Status check...");

        foreach (KeyValuePair<StatusPreset, MethodInfo> pair in checkingRef)
        {
            //Build the correct instance to pass
            StatusInstance newInst = new StatusInstance();
            newInst.preset = pair.Key;

            object[] passed = { newInst };

            pair.Value.Invoke(this, passed);
        }

        //Execute display
        List<StatusInstance> required = new List<StatusInstance>();

        //Count modifiers
        disabledRecovery = false;
        disabledJump = false;
        disabledSprint = false;
        recoveryRateMultiplier = 1f;
        maxHealthMultiplier = 1f;
        movementSpeedMultiplier = 1f;
        temperatureGainMultiplier = 1f;
        damageIncomingMultiplier = 1f;
        damageOutgoingMultiplier = 1f;
        tripChanceWet = 0f;
        tripChanceDrunk = 0f;
        drunkControls = 0f;
        affectHeadBobs.Clear();
        drunkVision = 0f;
        shiverVision = 0f;
        drunkLensDistort = 0f;
        headacheVision = 0f;

        bloomIntensityMultiplier = 0f;
        motionBlurMultiplier = 0f;
        chromaticAbberationAmount = 0f;
        vignetteAmount = 0f;
        exposureAmount = 0f;

        Dictionary<StatusPreset, float> channelMix = new Dictionary<StatusPreset, float>();
        bool channelMixActive = false;

        channelRedR = 100;
        channelRedG = 0;
        channelRedB = 0;

        channelGreenR = 0;
        channelGreenG = 100;
        channelGreenB = 0;

        channelBlueR = 0;
        channelBlueG = 0;
        channelBlueB = 100;

        foreach (KeyValuePair<StatusInstance, List<StatusCount>> pair in activeStatusCounts)
        {
            if (pair.Value != null && pair.Value.Count > 0)
            {
                required.Add(pair.Key);

                //Binary
                if (pair.Key.preset.stopsRecovery)
                {
                    disabledRecovery = true;
                }

                if (pair.Key.preset.stopsJump)
                {
                    disabledJump = true;
                }

                if (pair.Key.preset.stopsSprint)
                {
                    disabledSprint = true;
                }

                //Gradual
                recoveryRateMultiplier += pair.Key.preset.recoveryRatePlusMP * pair.Value[0].amount;
                maxHealthMultiplier += pair.Key.preset.maxHealthPlusMP * pair.Value[0].amount;
                movementSpeedMultiplier += pair.Key.preset.movementSpeedPlusMP * pair.Value[0].amount;
                temperatureGainMultiplier += pair.Key.preset.temperatureGainPlusMP * pair.Value[0].amount;
                damageIncomingMultiplier += pair.Key.preset.damageIncomingPlusMP * pair.Value[0].amount;
                damageOutgoingMultiplier += pair.Key.preset.damageOutgoingPlusMP * pair.Value[0].amount;

                tripChanceWet += pair.Key.preset.tripChanceWet * pair.Value[0].amount;
                tripChanceDrunk += pair.Key.preset.tripChanceDrunk * pair.Value[0].amount;
                drunkControls += pair.Key.preset.drunkControls * pair.Value[0].amount;

                if(pair.Key.preset.affectHeadBob != 0f)
                {
                    affectHeadBobs.Add(pair.Key.preset.headBob, pair.Value[0].amount);
                }

                drunkVision += pair.Key.preset.drunkVision * pair.Value[0].amount;
                shiverVision += pair.Key.preset.shiverVision * pair.Value[0].amount;
                drunkLensDistort += pair.Key.preset.drunkLensDistort * pair.Value[0].amount;
                headacheVision += pair.Key.preset.headacheVision * pair.Value[0].amount;

                bloomIntensityMultiplier += pair.Key.preset.bloomIntensityPlusMP * pair.Value[0].amount;
                motionBlurMultiplier += pair.Key.preset.motionBlurPlusMP * pair.Value[0].amount;
                chromaticAbberationAmount += pair.Key.preset.chromaticAbberationAmount * pair.Value[0].amount;
                vignetteAmount += pair.Key.preset.vignetteAmount * pair.Value[0].amount;
                exposureAmount += pair.Key.preset.expsosure * pair.Value[0].amount;

                //Channel mixer
                if(pair.Key.preset.useChannelMixer)
                {
                    channelMixActive = true;

                    if(!channelMix.ContainsKey(pair.Key.preset))
                    {
                        channelMix.Add(pair.Key.preset, pair.Value[0].amount);
                    }
                    else channelMix[pair.Key.preset] += pair.Value[0].amount;
                }

                //Minimum limits 10%
                maxHealthMultiplier = Mathf.Max(maxHealthMultiplier, 0.1f);
                movementSpeedMultiplier = Mathf.Max(movementSpeedMultiplier, 0.1f);

                drunkControls = Mathf.Clamp01(drunkControls);
                shiverVision = Mathf.Clamp01(shiverVision);

                //Nothing else effects these, so set it here right away
                SessionData.Instance.motionBlur.intensity.value = Game.Instance.motionBlurIntensity * motionBlurMultiplier + SessionData.Instance.GetGameSpeedMotionBlurModifier();
                SessionData.Instance.bloom.intensity.value = Game.Instance.bloomIntensity * bloomIntensityMultiplier;

                if(exposureAmount != 0f)
                {
                    SessionData.Instance.exposure.active = true;
                    SessionData.Instance.exposure.fixedExposure.value = Mathf.Min(exposureAmount, -Player.Instance.hurt * 0.5f);
                }
                else if(SessionData.Instance.exposure.IsActive() && Player.Instance.hurt <= 0f)
                {
                    SessionData.Instance.exposure.active = false;
                }

                //Turn on/off chromatic abberation based on transition
                if ((!Player.Instance.transitionActive || (Player.Instance.currentTransition != null && !Player.Instance.currentTransition.useChromaticAberration)))
                {
                    if (chromaticAbberationAmount > 0f)
                    {
                        //Turn on
                        SessionData.Instance.chromaticAberration.active = true;

                        //Game.Log("Set CA: " + chromaticAbberationAmount);
                        SessionData.Instance.chromaticAberration.intensity.value = chromaticAbberationAmount;
                    }
                    else if(SessionData.Instance.chromaticAberration.IsActive())
                    {
                        SessionData.Instance.chromaticAberration.active = false;
                    }
                }

                if (InterfaceController.Instance.desktopModeTransition <= 0f && InterfaceController.Instance.desktopModeDesiredTransition <= 0f)
                {
                    if(vignetteAmount > 0f)
                    {
                        //Turn on
                        SessionData.Instance.vignette.active = true;

                        //Game.Log("Set Vignette: " + chromaticAbberationAmount);
                        SessionData.Instance.vignette.intensity.value = Mathf.Max(vignetteAmount, InterfaceController.Instance.fade);
                    }
                    else if(SessionData.Instance.vignette.IsActive())
                    {
                        SessionData.Instance.vignette.active = false;
                    }
                }
            }
        }

        if(channelMixActive)
        {
            SessionData.Instance.channelMixer.active = true;

            foreach(KeyValuePair<StatusPreset, float> ch in channelMix)
            {
                channelRedR += ch.Key.redR * ch.Value;
                channelRedG += ch.Key.redG * ch.Value;
                channelRedB += ch.Key.redB * ch.Value;

                channelGreenR += ch.Key.greenR * ch.Value;
                channelGreenG += ch.Key.greenG * ch.Value;
                channelGreenB += ch.Key.greenB * ch.Value;

                channelBlueR += ch.Key.blueR * ch.Value;
                channelBlueG += ch.Key.blueG * ch.Value;
                channelBlueB += ch.Key.blueB * ch.Value;
            }

            SessionData.Instance.channelMixer.redOutRedIn.value = channelRedR;
            SessionData.Instance.channelMixer.redOutGreenIn.value = channelRedG;
            SessionData.Instance.channelMixer.redOutBlueIn.value = channelRedB;

            SessionData.Instance.channelMixer.greenOutRedIn.value = channelGreenR;
            SessionData.Instance.channelMixer.greenOutGreenIn.value = channelGreenG;
            SessionData.Instance.channelMixer.greenOutBlueIn.value = channelGreenB;

            SessionData.Instance.channelMixer.blueOutRedIn.value = channelBlueR;
            SessionData.Instance.channelMixer.blueOutGreenIn.value = channelBlueG;
            SessionData.Instance.channelMixer.blueOutBlueIn.value = channelBlueB;
        }
        else
        {
            SessionData.Instance.channelMixer.active = false;
        }

        for (int i = 0; i < spawnedControllers.Count; i++)
        {
            StateElementController controller = spawnedControllers[i];

            if(required.Contains(controller.statusInstance))
            {
                controller.VisualUpdate();
                required.Remove(controller.statusInstance);
                if(controller.removing) controller.SetRemove(false);
                this.enabled = true;
            }
            else
            {
                controller.SetRemove(true);
            }
        }

        foreach(StatusInstance s in required)
        {
            GameObject newObj = Instantiate(PrefabControls.Instance.statusElement, statusParent);
            StateElementController sec = newObj.GetComponent<StateElementController>();
            sec.Setup(s);
            spawnedControllers.Add(sec);
            this.enabled = true;

            SessionData.Instance.TutorialTrigger("statuses");

            if(s.preset.autoNotificationMessage)
            {
                string str = Strings.Get("ui.states", s.preset.name) + ": " + Strings.Get("ui.states", s.preset.name + "_description");
                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, str, InterfaceControls.Icon.agent, iconOverride: s.preset.icon, colourOverride: true, col: s.preset.color);
            }
        }

        //Sort spawned controllers
        spawnedControllers.Sort((p2, p1) => p1.preset.priority.CompareTo(p2.preset.priority));

        //Game.Log("Sort");

        //Position new icons
        float yPos = 0f;

        for (int i = 0; i < spawnedControllers.Count; i++)
        {
            StateElementController controller = spawnedControllers[i];

            controller.rect.localPosition = new Vector3(0, yPos, 0);

            if(required.Contains(controller.statusInstance))
            {
                controller.juice.Nudge(new Vector2(1.5f, 1.5f), new Vector2(8f, 8f), true, true, true);
                controller.juice.Flash(2, false);
            }

            yPos -= controller.rect.sizeDelta.y + elementYInterval;
        }

        //Update inventory content
        for (int i = 0; i < FirstPersonItemController.Instance.slots.Count; i++)
        {
            if (FirstPersonItemController.Instance.slots[i] == null) continue;

            if(FirstPersonItemController.Instance.slots[i].spawnedSegment != null)
            {
                FirstPersonItemController.Instance.slots[i].spawnedSegment.OnUpdateContent();
            }
        }
    }

    private void Update()
    {
        if (SessionData.Instance.isFloorEdit) return;

        bool triggerSort = false;
        float yPos = 0f;

        for (int i = 0; i < spawnedControllers.Count; i++)
        {
            StateElementController controller = spawnedControllers[i];

            //Set minimize to icon
            if (SessionData.Instance.play && !BioScreenController.Instance.isOpen)
            {
                if (controller.preset.minimizeToIcon && !controller.isWanted)
                {
                    if (controller.minimizeTimer > 0f)
                    {
                        controller.minimizeTimer -= Time.deltaTime;
                        if (controller.minimized) controller.SetMinimized(false);
                    }
                    else
                    {
                        controller.SetMinimized(true);
                    }
                }

                if (controller.maximized && controller.maximizeTimer > 0f)
                {
                    controller.maximizeTimer -= Time.deltaTime;
                }
                else
                {
                    controller.SetMaximized(false);
                }
            }

            //Minimize animation...
            if(controller.minimized && controller.widthResizingProgress > 0f)
            {
                controller.widthResizingProgress -= Time.deltaTime * 6f;
                controller.widthResizingProgress = Mathf.Clamp01(controller.widthResizingProgress);
                controller.rect.sizeDelta = new Vector2(Mathf.SmoothStep(elementMinimizedWidth, elementDefaultWdith, controller.widthResizingProgress), controller.rect.sizeDelta.y);
                controller.mainText.canvasRenderer.SetAlpha(controller.widthResizingProgress * 2f);

                if(controller.preset.displayTotalFineWhenMinimized)
                {
                    controller.fineText.canvasRenderer.SetAlpha(1f - controller.widthResizingProgress);
                }
            }
            else if(!controller.minimized && controller.widthResizingProgress < 1f)
            {
                controller.widthResizingProgress += Time.deltaTime * 6f;
                controller.widthResizingProgress = Mathf.Clamp01(controller.widthResizingProgress);
                controller.rect.sizeDelta = new Vector2(Mathf.SmoothStep(elementMinimizedWidth, elementDefaultWdith, controller.widthResizingProgress), controller.rect.sizeDelta.y);
                controller.mainText.canvasRenderer.SetAlpha(controller.widthResizingProgress * 2f);

                if (controller.preset.displayTotalFineWhenMinimized)
                {
                    controller.fineText.canvasRenderer.SetAlpha(1f - controller.widthResizingProgress);
                }
            }

            if (controller.preset.displayTotalFineWhenMinimized && controller.displayedFine != controller.fineTotal)
            {
                if(controller.displayedFine < controller.fineTotal)
                {
                    controller.displayedFine++;
                }
                else if (controller.displayedFine > controller.fineTotal)
                {
                    controller.displayedFine--;
                }

                if (controller.displayedFine + 10 < controller.fineTotal)
                {
                    controller.displayedFine += 10;
                }
                else if (controller.displayedFine - 10 > controller.fineTotal)
                {
                    controller.displayedFine -= 10;
                }

                controller.fineText.text = CityControls.Instance.cityCurrency + controller.displayedFine;
            }

            //Maximize animation
            else if (controller.maximized && controller.heightResizingProgress < 1f)
            {
                controller.heightResizingProgress += Time.deltaTime * 6f;
                controller.heightResizingProgress = Mathf.Clamp01(controller.heightResizingProgress);
                controller.rect.sizeDelta = new Vector2(controller.rect.sizeDelta.x, Mathf.SmoothStep(elementDefaultHeight, controller.maximizedHeight, controller.heightResizingProgress));
                controller.detailText.canvasRenderer.SetAlpha(detailTextFadeInCurve.Evaluate(controller.heightResizingProgress));
            }
            else if (!controller.maximized && controller.heightResizingProgress > 0f)
            {
                controller.heightResizingProgress -= Time.deltaTime * 6f;
                controller.heightResizingProgress = Mathf.Clamp01(controller.heightResizingProgress);
                controller.rect.sizeDelta = new Vector2(controller.rect.sizeDelta.x, Mathf.SmoothStep(elementDefaultHeight, controller.maximizedHeight, controller.heightResizingProgress));
                controller.detailText.canvasRenderer.SetAlpha(detailTextFadeInCurve.Evaluate(controller.heightResizingProgress));
            }

            //Set progress bar
            if(controller.preset.enableProgressBar)
            {
                //Set progress bar value
                float barVal = 0f;

                if(controller.preset.barTracking == StatusPreset.ProgressBarTrack.witnesses)
                {
                    if(controller.isWanted)
                    {
                        barVal = controller.statusInstance.building.wantedInBuilding / GameplayControls.Instance.buildingWantedTime;
                    }
                    else barVal = Player.Instance.seenProgressLag;
                }
                else if (controller.preset.barTracking == StatusPreset.ProgressBarTrack.wantedInBuilding)
                {
                    barVal = (controller.statusInstance.building.wantedInBuilding - SessionData.Instance.gameTime) / GameplayControls.Instance.buildingWantedTime;
                }
                else if (controller.preset.barTracking == StatusPreset.ProgressBarTrack.alarmTime)
                {
                    if (Player.Instance.currentGameLocation != null && Player.Instance.currentGameLocation.thisAsAddress != null && Player.Instance.currentGameLocation.thisAsAddress.alarmActive)
                    {
                        barVal = Player.Instance.currentGameLocation.thisAsAddress.alarmTimer / Player.Instance.currentGameLocation.thisAsAddress.GetAlarmTime();
                    }
                    else if (Player.Instance.currentBuilding != null && Player.Instance.currentBuilding.alarmActive)
                    {
                        barVal = Player.Instance.currentBuilding.alarmTimer / Player.Instance.currentBuilding.GetAlarmTime();
                    }
                    else barVal = 0;
                }
                else if (controller.preset.barTracking == StatusPreset.ProgressBarTrack.guestPassTime)
                {
                    Vector2 gp = Vector2.zero;

                    if(GameplayController.Instance.guestPasses.TryGetValue(controller.statusInstance.address, out gp))
                    {
                        barVal = (gp.x - SessionData.Instance.gameTime) / gp.y;
                    }
                    else
                    {
                        barVal = 0;
                    }
                }

                //Set progress bar size
                controller.progressBar.sizeDelta = new Vector2((controller.rect.sizeDelta.x - 8) * barVal, controller.progressBar.sizeDelta.y);
            }

            //Set Ypos
            if (controller.rect.localPosition.y != yPos)
            {
                controller.rect.localPosition = new Vector3(0, controller.rect.localPosition.y + ((yPos - controller.rect.localPosition.y) / 3f), 0);
            }

            yPos -= controller.rect.sizeDelta.y + elementYInterval;

            statusParent.sizeDelta = new Vector2(statusParent.sizeDelta.x, -yPos);

            //Set removal animation
            if(controller.removing)
            {
                if(controller.removalTimer > 0f)
                {
                    controller.removalTimer -= Time.deltaTime * 1.66f;

                    foreach(CanvasRenderer r in controller.renderElements)
                    {
                        r.SetAlpha(controller.removalTimer);
                    }

                    if(controller.xIcon != null)
                    {
                        float size = Mathf.SmoothStep(0f, 120f, 1f - controller.removalTimer);
                        controller.xIcon.sizeDelta = new Vector2(size, size);
                        controller.xIconRend.SetAlpha(controller.removalTimer);
                        controller.xIcon.anchoredPosition = Vector3.zero;
                    }
                }
                else
                {
                    //Actually remove...
                    spawnedControllers.RemoveAt(i);
                    Destroy(controller.gameObject);
                    triggerSort = true;
                    i--;
                    continue;
                }
            }
        }

        //Triggered if an element is completely removed from the list...
        if(triggerSort)
        {
            //Game.Log(" Removal Sort");

            //Sort spawned controllers
            spawnedControllers.Sort((p2, p1) => p1.preset.priority.CompareTo(p2.preset.priority));
        }

        //Disable this if nothing's here
        if(spawnedControllers.Count <= 0)
        {
            statusParent.sizeDelta = new Vector2(statusParent.sizeDelta.x, 0);
            this.enabled = false;
        }
    }

    public void AddFineRecord(NewAddress address, Interactable obj, CrimeType crime, bool confirm = false, int forcedPenalty = -1, bool ignoreDuplicates = false)
    {
        FineRecord existing = activeFineRecords.Find(item => !ignoreDuplicates && ((address == null && item.addressID == -1) || (address != null && item.addressID == address.id)) && item.crime == crime && ((item.objectID <= -1 && obj == null) || (obj != null && item.objectID == obj.id)));
        
        //Confirm existing...
        if(existing != null)
        {
            Game.Log("Player: Existing fine detected " + crime.ToString() + " " + obj + " at " + address + "; confirm " + confirm);

            if (confirm)
            {
                existing.SetConfirmed(true);
            }
        }
        else
        {
            Game.Log("Player: No existing fine detected " + crime.ToString() + " " + obj + " at " + address + "; setup new...");

            FineRecord newRecord = new FineRecord(address, obj, crime);
            newRecord.SetConfirmed(confirm);

            if (forcedPenalty > -1)
            {
                newRecord.forcedPenalty = forcedPenalty;
            }

            activeFineRecords.Add(newRecord);
        }

        //Confirm trespassing
        //if(crime == CrimeType.theft || crime == CrimeType.vandalism)
        //{
        //    ConfirmFinesAtLocation(address, CrimeType.breakingAndEntering);
        //    ConfirmFinesAtLocation(address, CrimeType.trespassing);
        //}

        ForceStatusCheck();
    }

    public void RemoveFineRecord(NewAddress address, Interactable obj, CrimeType crime, bool onlyUnconfirmed = false, bool matchAddress = true)
    {
        if(onlyUnconfirmed)
        {
            activeFineRecords.RemoveAll(item => !item.confirmed && (!matchAddress || (address == null && item.addressID == -1) || (address != null && item.addressID == address.id)) && item.crime == crime && ((item.objectID <= -1 && obj == null) || (obj != null && item.objectID == obj.id)));
        }
        else activeFineRecords.RemoveAll(item => (!matchAddress || (address == null && item.addressID == -1) || (address != null && item.addressID == address.id)) && item.crime == crime && ((item.objectID <= -1 && obj == null) || (obj != null && item.objectID == obj.id)));

        ForceStatusCheck();
    }

    public void FineEscapeCheck()
    {
        bool removedAny = false;

        for (int i = 0; i < activeFineRecords.Count; i++)
        {
            FineRecord f = activeFineRecords[i];
            NewAddress ad = null;

            if (CityData.Instance.addressDictionary.TryGetValue(f.addressID, out ad))
            {
                //If player is not wanted in this building...
                //Game.Log("Player: Wanted in building " + ad.building.name + ": " + ad.building.wantedInBuilding);

                if (Player.Instance.currentBuilding != ad.building)
                {
                    Game.Log("Player: Removing fine as the player is in a different building (" + Player.Instance.currentBuilding + "/" + ad.building + ")");
                    activeFineRecords.RemoveAt(i);
                    removedAny = true;
                    i--;
                    continue;
                }
            }
            else
            {
                Game.Log("Player: Removing fine as there is no address entry for " + f.addressID);
                activeFineRecords.RemoveAt(i);
                removedAny = true;
                i--;
                continue;
            }
        }

        if (removedAny)
        {
            ForceStatusCheck();
        }
    }

    public void SetWantedInBuilding(NewBuilding b, float time)
    {
        //Set wanted time
        b.wantedInBuilding = Mathf.Max(b.wantedInBuilding, SessionData.Instance.gameTime + time);
        Game.Log("Player: Set wanted in building " + b.name + ": " + b.wantedInBuilding);

        ForceStatusCheck();
    }

    public void SetDetainedInBuilding(NewBuilding b, bool val)
    {
        if (b == null) return;
        Game.Log("Set detained: " + b.name + " " + val);

        //Create status isntance
        StatusInstance newInst = new StatusInstance();
        newInst.preset = GameplayControls.Instance.detainedStatus;
        newInst.building = b;

        if(activeStatusCounts.ContainsKey(newInst) && activeStatusCounts[newInst].Count > 0)
        {
            if(!val)
            {
                RemoveAllCounts(newInst);
            }

            Game.Log("Detained: Existing status key");
        }
        else
        {
            if(val)
            {
                InterfaceController.Instance.CrosshairReaction();
                new StatusCount(newInst);
            }
        }
    }

    //True equals illegal
    public bool GetCurrentDetainedStatus()
    {
        if(activeStatuses.Contains(GameplayControls.Instance.detainedStatus))
        {
            //Game.Log("Contains detained status...");

            if(InteractionController.Instance.lockedInInteraction != null && InteractionController.Instance.lockedInInteraction.preset.specialCaseFlag == InteractablePreset.SpecialCase.hospitalBed)
            {
                //Game.Log("Is in hospital bed...");
                return false;
            }

            foreach(KeyValuePair<StatusInstance, List<StatusCount>> pair in activeStatusCounts)
            {
                if(pair.Key.preset == GameplayControls.Instance.detainedStatus && pair.Key.building == Player.Instance.currentBuilding)
                {
                    //Game.Log("Return true...");
                    return true;
                }
            }
        }
        //else
        //{
        //    foreach(StatusPreset p in activeStatuses)
        //    {
        //        Game.Log(p.name);
        //    }
        //}

        //foreach (KeyValuePair<StatusInstance, List<StatusCount>> pair in StatusController.Instance.activeStatusCounts)
        //{
        //    Game.Log(pair.Key.preset.name + " " + pair.Key.building + ", " + pair.Value.Count);
        //}

        //Game.Log("Return false...");

        return false;
    }

    public void ConfirmFinesAtLocation(NewAddress address, CrimeType crime)
    {
        List<FineRecord> foundRecords = activeFineRecords.FindAll(item => !item.confirmed && ((address == null && item.addressID == -1) || (address != null && item.addressID == address.id)) && item.crime == crime);

        foreach(FineRecord fr in foundRecords)
        {
            fr.SetConfirmed(true);
            Player.Instance.StatusCheckEndOfFrame();
        }
    }

    public void ConfirmFine(NewAddress address, Interactable obj, CrimeType crime)
    {
        FineRecord foundRecord = activeFineRecords.Find(item => !item.confirmed && ((address == null && item.addressID == -1) || (address != null && item.addressID == address.id)) && item.crime == crime && ((item.objectID <= -1 && obj == null) || (obj != null && item.objectID == obj.id)));

        if(foundRecord != null)
        {
            foundRecord.SetConfirmed(true);
            Player.Instance.StatusCheckEndOfFrame();
        }
    }

    //Triggerd on capture
    public void PayActiveFines()
    {
        int pen = 0;

        foreach(KeyValuePair<StatusInstance, List<StatusCount>> pair in activeStatusCounts)
        {
            for (int i = 0; i < pair.Value.Count; i++)
            {
                StatusCount count = pair.Value[i];

                //Reset stolen object
                if(count.fineRecord != null)
                {
                    if(count.fineRecord.crime == CrimeType.theft && count.fineRecord.objectID > -1)
                    {
                        Interactable stolen = null;

                        if (CityData.Instance.savableInteractableDictionary.TryGetValue(count.fineRecord.objectID, out stolen))
                        {
                            FirstPersonItemController.InventorySlot slot = FirstPersonItemController.Instance.slots.Find(item => item.interactableID == count.fineRecord.objectID);

                            if (slot != null)
                            {
                                slot.SetSegmentContent(null);
                            }

                            stolen.inInventory = null;
                            stolen.inv = 0;
                            stolen.rPl = false;
                            stolen.originalPosition = false;
                            stolen.SetOriginalPosition(true);

                            Game.Log("Removing stolen object: " + stolen.name + ", new postion: " + stolen.wPos + " (spawn: " + stolen.spWPos + ")");
                        }
                    }
                }

                pen += count.GetPenaltyAmount();
                count.Remove();
                i--;
            }
        }

        GameplayController.Instance.AddMoney(-pen, false, "fines");

        Game.Log("Player: Pay fines: " + pen);
    }

    public void Trespassing(StatusInstance inst)
    {
        //Is player trespassing?
        if(Player.Instance.illegalAreaActive)
        {
            StatusPreset.StatusCountConfig neededCountConfig = inst.preset.countConfig[Mathf.Clamp(Player.Instance.trespassingEscalation - 1, 0, 1)];
            List<StatusCount> existingCounts = null;

            if(activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if(existingCounts.Count > 0)
                {
                    //The existing count matches what is needed...
                    if(existingCounts[0].statusCountConfig == neededCountConfig)
                    {
                        return;
                    }
                    //A new count is needed
                    else
                    {
                        RemoveAllCounts(inst);
                        new StatusCount(inst, neededCountConfig);
                    }
                }
                else new StatusCount(inst, neededCountConfig);
            }
            else new StatusCount(inst, neededCountConfig);

            //Create a fine record
            if(Player.Instance.currentGameLocation.thisAsAddress != null)
            {
                if(!activeFineRecords.Exists(item => item.crime == CrimeType.trespassing && item.addressID == Player.Instance.currentGameLocation.thisAsAddress.id))
                {
                    AddFineRecord(Player.Instance.currentGameLocation.thisAsAddress, null, CrimeType.trespassing);
                }
            }
        }
        else
        {
            RemoveAllCounts(inst);

            //Remove all fine records from unconfirmed trespassing
            activeFineRecords.RemoveAll(item => item.crime == CrimeType.trespassing && !item.confirmed);
        }
    }

    public void AlarmActive(StatusInstance inst)
    {
        if ((Player.Instance.currentGameLocation != null && Player.Instance.currentGameLocation.thisAsAddress != null && Player.Instance.currentGameLocation.thisAsAddress.alarmActive) ||
            (Player.Instance.currentBuilding != null && Player.Instance.currentBuilding.alarmActive))
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst.preset);
        }
    }

    public void IllegalAction(StatusInstance inst)
    {
        //Is player performing an illegal action?
        if (Player.Instance.illegalActionActive)
        {
            new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void CaptureRisk(StatusInstance inst)
    {
        foreach(FineRecord f in activeFineRecords)
        {
            NewBuilding building = null;

            if(f.addressID > -1)
            {
                NewAddress output = null;

                if(CityData.Instance.addressDictionary.TryGetValue(f.addressID, out output))
                {
                    building = output.building;
                }
            }

            //Create status isntance
            StatusInstance newInst = new StatusInstance();
            newInst.preset = inst.preset;
            newInst.building = building;

            StatusPreset.StatusCountConfig neededCountConfig = inst.preset.countConfig.Find(item => item.name == f.crime.ToString());

            //Game.Log("Player: Capture risk: " + f.crime + " with building " + building + " count config: " + neededCountConfig.name);

            //Check for existing...
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(newInst, out existingCounts))
            {
                if(!existingCounts.Exists(item => item.fineRecord == f))
                {
                    StatusCount newCount = new StatusCount(newInst, neededCountConfig);
                    newCount.fineRecord = f;
                    Game.Log("Player: Create new crime count: " + f.crime);
                }
            }
            else
            {
                StatusCount newCount = new StatusCount(newInst, neededCountConfig);
                newCount.fineRecord = f;
                Game.Log("Player: Create new crime count: " + f.crime);
            }
        }

        //Now get rid of any un-needed...
        foreach(KeyValuePair<StatusInstance, List<StatusCount>> pair in activeStatusCounts)
        {
            if (pair.Key.preset.countType != StatusPreset.StatusCountType.crime) continue;

            for (int i = 0; i < pair.Value.Count; i++)
            {
                StatusCount count = pair.Value[i];

                if(!activeFineRecords.Contains(count.fineRecord))
                {
                    Game.Log("Player: Remove crime count: " + count.statusCountConfig.name + " (active fine records: " + activeFineRecords.Count + ")");
                    pair.Value.Remove(count);
                }
            }
        }
    }

    public void ImageCaptured(StatusInstance inst)
    {
        foreach(NewBuilding b in CityData.Instance.buildingDirectory)
        {
            //Create status isntance
            StatusInstance newInst = new StatusInstance();
            newInst.preset = inst.preset;
            newInst.building = b;

            //For this status to be active the played must also not be wanted
            //if (b.capturedPlayerImage && b.wantedInBuilding <= 0f && Player.Instance.currentBuilding == b)
            //{
            //    List<StatusCount> existingCounts = null;

            //    if (activeStatusCounts.TryGetValue(newInst, out existingCounts))
            //    {
            //        if (existingCounts.Count > 0)
            //        {
            //            return;
            //        }
            //        else
            //        {
            //            InterfaceController.Instance.CrosshairReaction();
            //            new StatusCount(newInst);
            //        }
            //    }
            //    else
            //    {
            //        InterfaceController.Instance.CrosshairReaction();
            //        new StatusCount(newInst);
            //    }
            //}
            //else
            //{
            //    RemoveAllCounts(newInst);
            //}
        }
    }

    public void Wanted(StatusInstance inst)
    {
        foreach (NewBuilding b in CityData.Instance.buildingDirectory)
        {
            //Create status isntance
            StatusInstance newInst = new StatusInstance();
            newInst.preset = inst.preset;
            newInst.building = b;

            //For this status to be active the played must also not be wanted
            if (b.wantedInBuilding > SessionData.Instance.gameTime)
            {
                List<StatusCount> existingCounts = null;

                if (activeStatusCounts.TryGetValue(newInst, out existingCounts))
                {
                    if (existingCounts.Count > 0)
                    {
                        return;
                    }
                    else
                    {
                        new StatusCount(newInst);
                    }
                }
                else
                {
                    new StatusCount(newInst);
                }
            }
            else
            {
                RemoveAllCounts(newInst);
            }
        }
    }

    public void GuestPass(StatusInstance inst)
    {
        if(GameplayController.Instance.guestPasses.Count > 0)
        {
            foreach (KeyValuePair<NewAddress, Vector2> pair in GameplayController.Instance.guestPasses)
            {
                //Create status isntance
                StatusInstance newInst = new StatusInstance();
                newInst.preset = inst.preset;
                newInst.address = pair.Key;

                //For this status to be active the played must also not be wanted
                if (pair.Value.x > SessionData.Instance.gameTime)
                {
                    List<StatusCount> existingCounts = null;

                    if (activeStatusCounts.TryGetValue(newInst, out existingCounts))
                    {
                        if (existingCounts.Count > 0)
                        {
                            return;
                        }
                        else
                        {
                            new StatusCount(newInst);
                        }
                    }
                    else
                    {
                        new StatusCount(newInst);
                    }
                }
                else
                {
                    RemoveAllCounts(newInst);
                }
            }
        }
        else
        {
            RemoveAllCounts(inst.preset);
        }
    }

    public void Detained(StatusInstance inst)
    {
        foreach (NewBuilding b in CityData.Instance.buildingDirectory)
        {
            //Create status isntance
            StatusInstance newInst = new StatusInstance();
            newInst.preset = inst.preset;
            newInst.building = b;

            //For this status to be active the played must also not be wanted
            if (Player.Instance.currentBuilding != b)
            {
                RemoveAllCounts(newInst);
            }
        }
    }

    public void Hiding(StatusInstance inst)
    {
        if (Player.Instance.isHiding)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Stinky(StatusInstance inst)
    {
        if(!Game.Instance.smellyStatusEnabled || UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.noSmelly) > 0f)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.hygiene < 1f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = 1f - Player.Instance.hygiene;

                    return;
                }
                else new StatusCount(inst);
            }
            else
            {
                if (Toolbox.Instance.Rand(0f, 1f) < 0.75f)
                {
                    Player.Instance.speechController.Speak("dds.blocks", "38b0dfff-8764-471e-aaad-aae40a3d8a93", true, false, false, 2f, false);
                }

                new StatusCount(inst);
            }
        }
        else if(Player.Instance.hygiene >= 1f)
        {
            RemoveAllCounts(inst);
        }
    }

    public void Cold(StatusInstance inst)
    {
        if (!Game.Instance.coldStatusEnabled || UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.noCold) > 0f)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.heat <= 0.38f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = 1f - (Player.Instance.heat / 0.38f);

                    return;
                }
                else new StatusCount(inst);
            }
            else
            {
                if (Toolbox.Instance.Rand(0f, 1f) < 0.75f)
                {
                    Player.Instance.speechController.Speak("dds.blocks", "7bfb0916-d70b-4749-88d5-11db123941f1", true, false, false, 2f, false);
                }

                new StatusCount(inst);
            }
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Hungry(StatusInstance inst)
    {
        if (!Game.Instance.hungerStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.nourishment <= 0.2f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = 1f - (Player.Instance.nourishment / 0.2f);

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Energized(StatusInstance inst)
    {
        if (!Game.Instance.energizedStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.nourishment >= 0.9f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = (Player.Instance.nourishment - 0.9f) / 0.1f;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Thirsty(StatusInstance inst)
    {
        if (!Game.Instance.hydrationStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.hydration <= 0.2f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = 1f - (Player.Instance.hydration / 0.2f);

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Hydrated(StatusInstance inst)
    {
        if (!Game.Instance.hydratedStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.hydration >= 0.9f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = (Player.Instance.hydration - 0.9f) / 0.1f;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Drunk(StatusInstance inst)
    {
        if (!Game.Instance.drunkStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.drunk > 0.05f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = (Player.Instance.drunk - 0.05f) / 0.95f;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Sick(StatusInstance inst)
    {
        if (!Game.Instance.sickStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.sick > 0.05f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = (Player.Instance.sick - 0.05f) / 0.95f;

                    return;
                }
                else
                {
                    new StatusCount(inst);
                }
            }
            else
            {
                if (Toolbox.Instance.Rand(0f, 1f) < 0.75f)
                {
                    Player.Instance.speechController.Speak("dds.blocks", "ed1f5108-5416-46aa-b4b9-4c37182958b8", true, false, false, 2f, false);
                }

                new StatusCount(inst);
            }
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void StarchAddiction(StatusInstance inst)
    {
        if (!Game.Instance.starchAddictionEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.starchAddiction > 0.1f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = (Player.Instance.starchAddiction - 0.1f) / 0.9f;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Headache(StatusInstance inst)
    {
        if (!Game.Instance.headacheStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.headache > 0.05f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = (Player.Instance.headache - 0.05f) / 0.95f;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Wet(StatusInstance inst)
    {
        if (!Game.Instance.wetStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.wet > 0.1f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = (Player.Instance.wet - 0.1f) / 0.9f;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void BrokenLeg(StatusInstance inst)
    {
        if (!Game.Instance.injuryStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.brokenLeg > 0f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = Player.Instance.brokenLeg;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Bruised(StatusInstance inst)
    {
        if (!Game.Instance.injuryStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.bruised > 0f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = Player.Instance.bruised;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void BlackEye(StatusInstance inst)
    {
        if (!Game.Instance.injuryStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.blackEye > 0f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = Player.Instance.blackEye;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void BlackedOut(StatusInstance inst)
    {
        if (Player.Instance.blackedOut > 0f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = Player.Instance.blackedOut;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Numb(StatusInstance inst)
    {
        if (!Game.Instance.numbStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.numb > 0.1f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = (Player.Instance.numb - 0.1f) / 0.9f;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Poisoned(StatusInstance inst)
    {
        if (!Game.Instance.poisonStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.poisoned > 0f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = Player.Instance.poisoned;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Blinded(StatusInstance inst)
    {
        if (!Game.Instance.blindedStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.blinded > 0f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = Player.Instance.blinded;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Bleeding(StatusInstance inst)
    {
        if (!Game.Instance.bleedingStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.bleeding > 0f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = Player.Instance.bleeding;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Tired(StatusInstance inst)
    {
        if (!Game.Instance.tiredStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.energy <= 0.2f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = 1f - (Player.Instance.energy / 0.2f);

                    return;
                }
                else new StatusCount(inst);
            }
            else
            {
                if (Toolbox.Instance.Rand(0f, 1f) < 0.75f)
                {
                    if(Toolbox.Instance.Rand(0f, 1f) < 0.5f) Player.Instance.speechController.Speak("dds.blocks", "d3443636-b897-4df3-a072-45aa7907737f", true, false, false, 2f, false);
                    else Player.Instance.speechController.Speak("dds.blocks", "f67684e7-ba63-49d8-83dc-f689d0b670f5", true, false, false, 2f, false);
                }

                new StatusCount(inst);
            }
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Focused(StatusInstance inst)
    {
        if (!Game.Instance.focusedStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.alertness >= 0.9f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = (Player.Instance.alertness - 0.9f) / 0.1f;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void Pursued(StatusInstance inst)
    {
        if (Player.Instance.persuedBy.Count > 0)
        {
            //foreach (Actor act in Player.Instance.persuedBy)
            //{
            //    Game.Log("Player persued: " + act.name);
            //}

            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void WellRested(StatusInstance inst)
    {
        if (!Game.Instance.wellRestedStatusEnabled)
        {
            RemoveAllCounts(inst);
            return;
        }

        if (Player.Instance.wellRested > 0f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = Player.Instance.wellRested;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void SyncDiskInstall(StatusInstance inst)
    {
        if (Player.Instance.syncDiskInstall > 0f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = Player.Instance.syncDiskInstall;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }

    public void ToxicGas(StatusInstance inst)
    {
        if (Player.Instance.gasLevel > 0f)
        {
            List<StatusCount> existingCounts = null;

            if (activeStatusCounts.TryGetValue(inst, out existingCounts))
            {
                if (existingCounts.Count > 0)
                {
                    //Update amount
                    existingCounts[0].amount = Player.Instance.gasLevel;

                    return;
                }
                else new StatusCount(inst);
            }
            else new StatusCount(inst);
        }
        else
        {
            RemoveAllCounts(inst);
        }
    }
}
