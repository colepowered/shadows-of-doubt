﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Rewired;
using NaughtyAttributes;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Linq;
using System;

public class InputController : MonoBehaviour
{
    [Header("Setup")]
    public bool enableInput = true;
    [System.NonSerialized]
    public Rewired.Player player;
    public AnimationCurve nearestLookAtCurve;

    [Header("Menu")]
    public ControllerType lastActiveController = ControllerType.Custom;
    public bool mouseInputMode = true;
    private bool initalInputModeSet = false; //Flag for updating the input mode fully when the game starts
    public bool cursorVisible = true;
    private ButtonController currentButtonDown = null; //The held down button

    private float controlFallbackCheck = 0f;

    public delegate void InputModeChange();
    public event InputModeChange OnInputModeChange;

    //Singleton pattern
    private static InputController _instance;
    public static InputController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Update()
    {
        if (!enableInput) return;
        if (!ReInput.isReady) return; // Exit if Rewired isn't ready. This would only happen during a script recompile in the editor.
        //if (InterfaceController.Instance.playerTextInputActive) return;
        if (PopupMessageController.Instance != null && PopupMessageController.Instance.active && PopupMessageController.Instance.appearProgress < 1f) return;
        if (PopupMessageController.Instance != null && !PopupMessageController.Instance.active && PopupMessageController.Instance.appearProgress > 0f) return;
        if (InteractionController.Instance.inputCooldown > 0f) return;

        if (Rewired.Demos.SimpleControlRemappingSOD.Instance != null)
        {
            if(Rewired.Demos.SimpleControlRemappingSOD.Instance.listeningForRemap)
            {
                //Only allow menu button when rebinding controls
                if (player.GetButtonDown("Menu"))
                {
                    Rewired.Demos.SimpleControlRemappingSOD.Instance.StopMapping();
                }

                return;
            }
        }

        if (player == null)
        {
            player = ReInput.players.GetPlayer(0);

            if (player == null)
            {
                return;
            }
        }
        else if(!initalInputModeSet && !SessionData.Instance.isFloorEdit)
        {
            SetMouseInputMode(System.Convert.ToBoolean(PlayerPrefs.GetInt("controlMethod")), true);
            initalInputModeSet = true;
        }

        //Detect if an input box is active...
        if (mouseInputMode && player.GetAnyButton())
        {
            GameObject currentSelected = EventSystem.current.currentSelectedGameObject;

            if (currentSelected != null)
            {
                //Game.Log("Current selected: " + currentSelected.name);

                TMP_InputField inputField = currentSelected.GetComponent<TMP_InputField>();

                if (inputField != null)
                {
                    //Game.Log("Interface: Detected selected input field!");
                    return;
                }
            }
        }

        //Switch between mouse and keyboard control schemes
        if (Game.Instance.controlAutoSwitch)
        {
            if(mouseInputMode && (ReInput.controllers.GetAnyButtonUp(ControllerType.Joystick)))
            {
                lastActiveController = ReInput.controllers.GetLastActiveControllerType();
                SetMouseInputMode(false);
            }   
            else if(!mouseInputMode && (ReInput.controllers.GetAnyButton(ControllerType.Mouse) || ReInput.controllers.GetAnyButton(ControllerType.Keyboard)))
            {
                lastActiveController = ReInput.controllers.GetLastActiveControllerType();
                SetMouseInputMode(true);
            }
        }

        //Failsafe for if the game is set to controller mode but no controller is detected (check every 2 seconds)...
        if(!mouseInputMode)
        {
            controlFallbackCheck += Time.deltaTime;

            if(controlFallbackCheck >= 2f)
            {
                //Failsafe controls for falling back to some kind of input...
                if (!mouseInputMode && ReInput.controllers.joystickCount <= 0)
                {
                    Game.Log("Player: Falling back to mouse/keyboard mode, as no joystick is detected...");
                    SetMouseInputMode(true, true);
                }

                controlFallbackCheck = 0f;
            }
        }

        if (!mouseInputMode)
        {
            //Game.Log("Stick L: " + player.GetAxis("MoveHorizontal") + ", " + player.GetAxis("MoveVertical") + " R: " + player.GetAxis("LookHorizontal") + ", " + player.GetAxis("LookVertical"));

            //Handle menu navigation
            if (!SessionData.Instance.isFloorEdit)
            {
                if (!MainMenuController.Instance.mainMenuActive || (PopupMessageController.Instance != null && PopupMessageController.Instance.active) || MainMenuController.Instance.currentComponent.component != MainMenuController.Component.loadingCity)
                {
                    if (InterfaceController.Instance.selectedElement != null)
                    {
                        //Select
                        if (!mouseInputMode)
                        {
                            //Deselect the resolve button if not available
                            if(CasePanelController.Instance.controllerMode)
                            {
                                if (InterfaceController.Instance.selectedElement == CasePanelController.Instance.closeCaseButton && CasePanelController.Instance.activeCase == null)
                                {
                                    CasePanelController.Instance.closeCaseButton.OnDeselect();

                                    Selectable selectOnLeft = CasePanelController.Instance.closeCaseButton.button.FindSelectableOnLeft();

                                    if (selectOnLeft != null)
                                    {
                                        ButtonController bc = selectOnLeft.gameObject.GetComponent<ButtonController>();

                                        //Select button
                                        if (bc != null)
                                        {
                                            bc.OnSelect(); //Fire enter event
                                        }
                                    }
                                }
                            }

                            if (player.GetButtonUp("Select"))
                            {
                                if(currentButtonDown != null)
                                {
                                    //Fake a left button click here
                                    UnityEngine.EventSystems.PointerEventData pointerEvent = new UnityEngine.EventSystems.PointerEventData(null) { button = UnityEngine.EventSystems.PointerEventData.InputButton.Left };
                                    currentButtonDown.OnPointerUp(pointerEvent);
                                    currentButtonDown = null;
                                }
                                else
                                {
                                    currentButtonDown = null;
                                }
                            }

                            //Select the current button
                            if (player.GetButtonDown("Select") && currentButtonDown == null)
                            {
                                currentButtonDown = InterfaceController.Instance.selectedElement;

                                //Fake a left button click here
                                UnityEngine.EventSystems.PointerEventData pointerEvent = new UnityEngine.EventSystems.PointerEventData(null) { button = UnityEngine.EventSystems.PointerEventData.InputButton.Left };
                                currentButtonDown.OnPointerDown(pointerEvent);
                                if(currentButtonDown != null) currentButtonDown.OnPointerClick(pointerEvent);
                                return;
                            }
                            else if(player.GetButtonDown("Secondary") && currentButtonDown == null && InterfaceController.Instance.selectedElement.secondaryIsRightClick)
                            {
                                currentButtonDown = InterfaceController.Instance.selectedElement;

                                //Fake a left button click here
                                UnityEngine.EventSystems.PointerEventData pointerEvent = new UnityEngine.EventSystems.PointerEventData(null) { button = UnityEngine.EventSystems.PointerEventData.InputButton.Right };
                                currentButtonDown.OnPointerDown(pointerEvent);
                                if (currentButtonDown != null) currentButtonDown.OnPointerClick(pointerEvent);
                                return;
                            }
                        }

                        if (InterfaceController.Instance.selectedElement != null && InterfaceController.Instance.selectedElement.button != null)
                        {
                            Selectable nextSelectable = null;

                            if (player.GetButtonDown("NavigateUp"))
                            {
                                nextSelectable = InterfaceController.Instance.selectedElement.button.FindSelectableOnUp();
                            }

                            if (player.GetButtonDown("NavigateDown"))
                            {
                                nextSelectable = InterfaceController.Instance.selectedElement.button.FindSelectableOnDown();
                            }

                            if (player.GetButtonDown("NavigateLeft"))
                            {
                                nextSelectable = InterfaceController.Instance.selectedElement.button.FindSelectableOnLeft();
                            }

                            if (player.GetButtonDown("NavigateRight"))
                            {
                                nextSelectable = InterfaceController.Instance.selectedElement.button.FindSelectableOnRight();
                            }

                            if (nextSelectable != null && nextSelectable != InterfaceController.Instance.selectedElement)
                            {
                                Game.Log("Menu: New button mouse over through non-mouse input: " + nextSelectable.gameObject.name);
                                InterfaceController.Instance.selectedElement.OnDeselect(); //Fire exit event

                                ButtonController bc = nextSelectable.gameObject.GetComponent<ButtonController>();

                                //Select button
                                if(bc != null)
                                {
                                    bc.OnSelect(); //Fire enter event

                                    //Save previously selected button...
                                    if (MainMenuController.Instance.mainMenuActive)
                                    {
                                        if (MainMenuController.Instance.currentComponent != null)
                                        {
                                            if (MainMenuController.Instance.currentComponent.buttons.Contains(bc))
                                            {
                                                MainMenuController.Instance.currentComponent.previouslySelected = bc;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //Select input field
                                    TMP_InputField field = nextSelectable.gameObject.GetComponent<TMP_InputField>();

                                    if(field != null)
                                    {
                                        field.ActivateInputField();
                                    }
                                }

                                return;
                            }
                        }
                        else
                        {
                            Game.LogError("Menu: Current selectable doesn't have a button object attached! Cannot navigate from here...");
                        }
                    }
                    else
                    {
                        if (currentButtonDown != null) currentButtonDown = null; //Reset current button down

                        if (MainMenuController.Instance.mainMenuActive)
                        {
                            if(MainMenuController.Instance.raycaster.enabled)
                            {
                                Game.Log("Menu: Nothing is current selected, finding default...");

                                if (MainMenuController.Instance.currentComponent != null)
                                {
                                    if (MainMenuController.Instance.currentComponent.previouslySelected != null && MainMenuController.Instance.currentComponent.previouslySelected.interactable)
                                    {
                                        Game.Log("Menu: ...Selected button " + MainMenuController.Instance.currentComponent.previouslySelected.gameObject.name + " in " + MainMenuController.Instance.currentComponent.component);
                                        MainMenuController.Instance.currentComponent.previouslySelected.OnSelect();
                                    }
                                    else
                                    {
                                        ButtonController highestRanked = null;

                                        for (int i = 0; i < MainMenuController.Instance.currentComponent.buttons.Count; i++)
                                        {
                                            if (MainMenuController.Instance.currentComponent.buttons[i].interactable)
                                            {
                                                if(highestRanked == null || MainMenuController.Instance.currentComponent.buttons[i].defaultSelectionPriority > highestRanked.defaultSelectionPriority)
                                                {
                                                    highestRanked = MainMenuController.Instance.currentComponent.buttons[i];
                                                }
                                            }
                                        }

                                        if(highestRanked != null)
                                        {
                                            Game.Log("Menu: ...Selected button " + highestRanked.gameObject.name + " in " + MainMenuController.Instance.currentComponent.component);
                                            highestRanked.OnSelect();
                                        }
                                    }
                                }
                            }
                        }
                        else if(!PopupMessageController.Instance.active && (InterfaceController.Instance.desktopMode || BioScreenController.Instance.isOpen))
                        {
                            if(CasePanelController.Instance.currentSelectMode == CasePanelController.ControllerSelectMode.windows)
                            {
                                if(InterfaceController.Instance.activeWindows.Count <= 0)
                                {
                                    CasePanelController.Instance.SetControllerMode(true, CasePanelController.ControllerSelectMode.caseBoard);
                                }
                                else
                                {
                                    if (CasePanelController.Instance.selectedWindow != null)
                                    {
                                        CasePanelController.Instance.SetSelectedWindow(CasePanelController.Instance.selectedWindow, true);
                                    }
                                    else CasePanelController.Instance.SetSelectedWindow(InterfaceController.Instance.activeWindows[0], true);
                                }
                            }

                            //Prompt to select something
                            if((UpgradesController.Instance.isOpen || BioScreenController.Instance.isOpen))
                            {
                                CasePanelController.Instance.SetControllerMode(true, CasePanelController.ControllerSelectMode.topBar);
                            }

                            if (CasePanelController.Instance.currentSelectMode == CasePanelController.ControllerSelectMode.caseBoard)
                            {
                                if(CasePanelController.Instance.spawnedPins.Count > 0)
                                {
                                    if (CasePanelController.Instance.selectedPinned != null)
                                    {
                                        CasePanelController.Instance.SetSelectedPinned(CasePanelController.Instance.selectedPinned, true);
                                    }
                                    else CasePanelController.Instance.SetSelectedPinned(CasePanelController.Instance.GetClosestPinnedToCentre(), true);
                                }
                            }
                        }
                    }
                }

                //Exit cut scene
                if (player.GetButtonDown("Back") && !MainMenuController.Instance.mainMenuActive && CutSceneController.Instance.cutSceneActive)
                {
                    CutSceneController.Instance.StopScene();
                    return;
                }

                //Back...
                if (MainMenuController.Instance.mainMenuActive && MainMenuController.Instance.currentComponent.component != MainMenuController.Component.loadingCity)
                {
                    //Back
                    if (player.GetButtonDown("Back") && MainMenuController.Instance.currentComponent != null && !PopupMessageController.Instance.active)
                    {
                        //Find the back button
                        ButtonController backButton = MainMenuController.Instance.currentComponent.buttons.Find(item => item.buttonType == ButtonController.ButtonAudioType.back && item.interactable);

                        if (backButton != null)
                        {
                            backButton.OnPointerClick(new UnityEngine.EventSystems.PointerEventData(null) { button = UnityEngine.EventSystems.PointerEventData.InputButton.Left });
                        }

                        if (InterfaceController.Instance.selectedElement != null)
                        {
                            InterfaceController.Instance.selectedElement.OnDeselect();
                        }

                        return;
                    }
                }
            }
        }
        else
        {
            if(Cursor.visible != cursorVisible)
            {
                Cursor.visible = cursorVisible;
            }
        }

        //Only do the following if no popup is present
        if ((PopupMessageController.Instance == null || !PopupMessageController.Instance.active) && MainMenuController.Instance != null && MainMenuController.Instance.currentComponent.component != MainMenuController.Component.loadingCity)
        {
            //Not allowed in menu or cut scene
            if (!MainMenuController.Instance.mainMenuActive && !CutSceneController.Instance.cutSceneActive)
            {
                //Pause toggle
                if (player.GetButtonDown("CaseBoard"))
                {
                    if (SessionData.Instance.enableUserPause)
                    {
                        //Always allow if paused
                        if (!SessionData.Instance.play && !SessionData.Instance.isFloorEdit)
                        {
                            if(BioScreenController.Instance.isOpen && !InterfaceController.Instance.desktopMode)
                            {
                                //Enable desktop mode if we are paused but not in desktop mode 
                                InterfaceController.Instance.SetDesktopMode(true, true);
                                return;
                            }
                            else if(BioScreenController.Instance.isOpen)
                            {
                                //Resume game otherwise in addition to closing the inventory
                                BioScreenController.Instance.SetInventoryOpen(false, true);
                                SessionData.Instance.ResumeGame();
                                return;
                            }
                            else
                            {
                                SessionData.Instance.ResumeGame();
                                return;
                            }
                        }
                        else if(SessionData.Instance.play)
                        {
                            //Search for alt control, which is often the same as pause, to see if we should bring up the case board...
                            InteractionController.InteractionSetting alt = null;

                            if(InteractionController.Instance.currentInteractions.TryGetValue(InteractablePreset.InteractionKey.alternative, out alt))
                            {
                                if(alt.currentSetting != null && alt.currentSetting.enabled)
                                {
                                    //Do nothing
                                }
                                else
                                {
                                    SessionData.Instance.PauseGame(true);
                                    return;
                                }
                            }
                            else
                            {
                                SessionData.Instance.PauseGame(true);
                                return;
                            }
                        }
                    }
                }

                //First person map
                if (SessionData.Instance.enableFirstPersonMap)
                {
                    if (player.GetButtonDown("Map"))
                    {
                        if (!MapController.Instance.displayFirstPerson && (!InterfaceController.Instance.desktopMode /*&& !InterfaceController.Instance.showDesktopMap*/))
                        {
                            MapController.Instance.OpenMap(true);
                            return;
                        }
                        else
                        {
                            Game.Log("Map disp fp: " + MapController.Instance.displayFirstPerson + " && " + InterfaceController.Instance.desktopMode + " " + InterfaceController.Instance.showDesktopMap);
                        }
                    }
                }

                //Toggle notebook
                if (player.GetButtonDown("Notebook"))
                {
                    InterfaceController.Instance.ToggleNotebook();
                    return;
                }

                //Case panel/inventory controls
                if (InterfaceController.Instance.desktopMode || BioScreenController.Instance.isOpen)
                {
                    if(CasePanelController.Instance.controllerMode)
                    {
                        if (player.GetButtonDown("NearestInteractable"))
                        {
                            int opMode = (int)CasePanelController.Instance.currentSelectMode;
                            opMode++;

                            if (opMode == 2 && InterfaceController.Instance.activeWindows.Count <= 0) opMode ++;
                            if (opMode > 3 || (opMode == 3 && !InterfaceController.Instance.minimapCanvas.gameObject.activeSelf)) opMode = 0;

                            if (UpgradesController.Instance.isOpen) opMode = 0; //Force top bar mode if ugprades are open

                            Game.Log("Primary " + (CasePanelController.ControllerSelectMode)opMode);
                            CasePanelController.Instance.SetControllerMode(true, (CasePanelController.ControllerSelectMode)opMode);
                            return;
                        }

                        //Navigate
                        if (player.GetButtonDown("NavigateRight"))
                        {
                            CasePanelController.Instance.ControllerNavigate(new Vector2(1, 0));
                            return;
                        }

                        if (player.GetButtonDown("NavigateLeft"))
                        {
                            CasePanelController.Instance.ControllerNavigate(new Vector2(-1, 0));
                            return;
                        }

                        if (player.GetButtonDown("NavigateUp"))
                        {
                            CasePanelController.Instance.ControllerNavigate(new Vector2(0, 1));
                            return;
                        }

                        if (player.GetButtonDown("NavigateDown"))
                        {
                            CasePanelController.Instance.ControllerNavigate(new Vector2(0, -1));
                            return;
                        }

                        //Shoulder navigate
                        if (player.GetButtonDown("SelectLeft"))
                        {
                            CasePanelController.Instance.ShoulderNavigate(false);
                            return;
                        }

                        if (player.GetButtonDown("SelectRight"))
                        {
                            CasePanelController.Instance.ShoulderNavigate(true);
                            return;
                        }

                        if (CasePanelController.Instance.currentSelectMode == CasePanelController.ControllerSelectMode.windows)
                        {
                            if (CasePanelController.Instance.selectedWindow != null)
                            {
                                if(CasePanelController.Instance.selectedWindow.currentPinnedCaseElement == null || CasePanelController.Instance.selectedWindow.currentPinnedCaseElement.pinnedController == null || !CasePanelController.Instance.selectedWindow.currentPinnedCaseElement.pinnedController.isDragging)
                                {
                                    Vector2 moveWindow = new Vector2(GetAxisRelative("MoveEvidenceAxisX"), GetAxisRelative("MoveEvidenceAxisY"));

                                    if (moveWindow.magnitude > 0.15f)
                                    {
                                        //Game.Log("Move window: " + moveWindow + " mag: " + moveWindow.magnitude);
                                        moveWindow *= 12f;
                                        CasePanelController.Instance.selectedWindow.SetAnchoredPosition(CasePanelController.Instance.selectedWindow.rect.anchoredPosition + moveWindow);
                                    }
                                }
                            }
                        }
                        else if(CasePanelController.Instance.currentSelectMode == CasePanelController.ControllerSelectMode.caseBoard)
                        {
                            if(CasePanelController.Instance.selectedPinned != null)
                            {
                                Vector2 movePinned = new Vector2(GetAxisRelative("MoveEvidenceAxisX"), GetAxisRelative("MoveEvidenceAxisY"));

                                if (movePinned.magnitude > 0.15f)
                                {
                                    movePinned *= 12f;
                                    CasePanelController.Instance.selectedPinned.ForceDrag();
                                    CasePanelController.Instance.selectedPinned.dragController.ForceDragController(new Vector2(CasePanelController.Instance.selectedPinned.rect.localPosition.x, CasePanelController.Instance.selectedPinned.rect.localPosition.y) + movePinned);
                                }
                            }
                        }
                    }

                    //Quick create string
                    if(CasePanelController.Instance.controllerMode && CasePanelController.Instance.selectedPinned != null)
                    {
                        if(player.GetButtonDown("CreateString") && CasePanelController.Instance.customString == null)
                        {
                            Game.Log("Create string");
                            CasePanelController.Instance.CustomStringLinkSelection(CasePanelController.Instance.selectedPinned, true);
                        }
                    }
                    else if(!CasePanelController.Instance.controllerMode && InterfaceController.Instance.selectedPinned.Count > 0)
                    {
                        if (player.GetButtonDown("CreateString") && CasePanelController.Instance.customString == null)
                        {
                            Game.Log("Create string");
                            CasePanelController.Instance.CustomStringLinkSelection(InterfaceController.Instance.selectedPinned[0], true);
                        }
                    }
                }

                //Weapon select (toggle)
                if (!PlayerApartmentController.Instance.furniturePlacementMode && !InterfaceController.Instance.playerTextInputActive && !CutSceneController.Instance.cutSceneActive && !Player.Instance.transitionActive && (MapController.Instance == null || !MapController.Instance.displayFirstPerson))
                {
                    if (!InteractionController.Instance.dialogMode)
                    {
                        if (player.GetButtonDown("WeaponSelect"))
                        {
                            Game.Log("Interface: Weapon select");

                            //Close inventory
                            if (BioScreenController.Instance.isOpen && BioScreenController.Instance.inventoryDisplayProgress >= 1f)
                            {
                                BioScreenController.Instance.SetInventoryOpen(false, false);
                                return;
                            }
                            else if (!BioScreenController.Instance.isOpen && BioScreenController.Instance.inventoryDisplayProgress <= 0f)
                            {
                                BioScreenController.Instance.SetInventoryOpen(true, false);
                                return;
                            }
                        }
                        //Escape/RMB also closes
                        else if (player.GetButtonDown("Menu") || (player.GetButtonDown("Secondary") && !player.GetButton("WeaponSelect") && (BioScreenController.Instance.hoveredSlot == null || BioScreenController.Instance.hoveredSlot == BioScreenController.Instance.selectedSlot)))
                        {
                            if (BioScreenController.Instance.isOpen && BioScreenController.Instance.inventoryDisplayProgress >= 1f)
                            {
                                Game.Log("Interface: Cancel inventory: " + player.GetButtonDown("Secondary") + " ");
                                BioScreenController.Instance.SetInventoryOpen(false, false);
                                return;
                            }
                        }
                    }
                    else if (BioScreenController.Instance.isOpen && BioScreenController.Instance.inventoryDisplayProgress >= 1f)
                    {
                        BioScreenController.Instance.SetInventoryOpen(false, false);
                        return;
                    }
                }

                //Quicksave
                if (player.GetButtonDown("QuickSave"))
                {
                    if(MainMenuController.Instance.IsSaveGameAllowed())
                    {
                        StartQuickSaveAsync();
                    }
                    else
                    {
                        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "Unable to save at this time"));
                    }
                }

                //Assign inventory hotkeys
                if (mouseInputMode)
                {
                    if (BioScreenController.Instance.isOpen)
                    {
                        if (BioScreenController.Instance.hoveredSlot != null)
                        {
                            if (player.GetButtonDown("0"))
                            {
                                BioScreenController.Instance.hoveredSlot.SetHotKey("0");
                            }
                            else if (player.GetButtonDown("1"))
                            {
                                BioScreenController.Instance.hoveredSlot.SetHotKey("1");
                            }
                            else if (player.GetButtonDown("2"))
                            {
                                BioScreenController.Instance.hoveredSlot.SetHotKey("2");
                            }
                            else if (player.GetButtonDown("3"))
                            {
                                BioScreenController.Instance.hoveredSlot.SetHotKey("3");
                            }
                            else if (player.GetButtonDown("4"))
                            {
                                BioScreenController.Instance.hoveredSlot.SetHotKey("4");
                            }
                            else if (player.GetButtonDown("5"))
                            {
                                BioScreenController.Instance.hoveredSlot.SetHotKey("5");
                            }
                            else if (player.GetButtonDown("6"))
                            {
                                BioScreenController.Instance.hoveredSlot.SetHotKey("6");
                            }
                            else if (player.GetButtonDown("7"))
                            {
                                BioScreenController.Instance.hoveredSlot.SetHotKey("7");
                            }
                            else if (player.GetButtonDown("8"))
                            {
                                BioScreenController.Instance.hoveredSlot.SetHotKey("8");
                            }
                            else if (player.GetButtonDown("9"))
                            {
                                BioScreenController.Instance.hoveredSlot.SetHotKey("9");
                            }
                        }
                        else if (BioScreenController.Instance.selectedSlot != null)
                        {
                            if (player.GetButtonDown("0"))
                            {
                                BioScreenController.Instance.selectedSlot.SetHotKey("0");
                            }
                            else if (player.GetButtonDown("1"))
                            {
                                BioScreenController.Instance.selectedSlot.SetHotKey("1");
                            }
                            else if (player.GetButtonDown("2"))
                            {
                                BioScreenController.Instance.selectedSlot.SetHotKey("2");
                            }
                            else if (player.GetButtonDown("3"))
                            {
                                BioScreenController.Instance.selectedSlot.SetHotKey("3");
                            }
                            else if (player.GetButtonDown("4"))
                            {
                                BioScreenController.Instance.selectedSlot.SetHotKey("4");
                            }
                            else if (player.GetButtonDown("5"))
                            {
                                BioScreenController.Instance.selectedSlot.SetHotKey("5");
                            }
                            else if (player.GetButtonDown("6"))
                            {
                                BioScreenController.Instance.selectedSlot.SetHotKey("6");
                            }
                            else if (player.GetButtonDown("7"))
                            {
                                BioScreenController.Instance.selectedSlot.SetHotKey("7");
                            }
                            else if (player.GetButtonDown("8"))
                            {
                                BioScreenController.Instance.selectedSlot.SetHotKey("8");
                            }
                            else if (player.GetButtonDown("9"))
                            {
                                BioScreenController.Instance.selectedSlot.SetHotKey("9");
                            }
                        }
                    }
                }

                //Toggle screenshot mode
                if (Game.Instance.devMode)
                {
                    if(player.GetButtonDown("ToggleScreenshotMode"))
                    {
                        Game.Instance.SetScreenshotMode(!Game.Instance.screenshotMode);
                    }
                }
            }

            //Allowed in menu and outside
            //Menu toggle: Allowed because we need to be able to toggle back...
            if (player.GetButtonDown("Menu"))
            {
                //Exit cut scene
                if (!MainMenuController.Instance.mainMenuActive && CutSceneController.Instance.cutSceneActive)
                {
                    CutSceneController.Instance.StopScene();
                    return;
                }

                //Return from case mode
                if (SessionData.Instance.startedGame && !SessionData.Instance.play && !MainMenuController.Instance.mainMenuActive)
                {
                    if (SessionData.Instance.enableUserPause)
                    {
                        SessionData.Instance.ResumeGame();
                        return;
                    }
                }

                //Game must be started
                if (SessionData.Instance.startedGame && (CityConstructor.Instance == null || !CityConstructor.Instance.preSimActive))
                {
                    if (!MainMenuController.Instance.mainMenuActive)
                    {
                        SessionData.Instance.PauseGame(true);
                        MainMenuController.Instance.EnableMainMenu(true, true);
                    }
                    else
                    {
                        MainMenuController.Instance.EnableMainMenu(false, true, true);
                    }

                    return;
                }

                //Escape inventory
                if (BioScreenController.Instance.isOpen)
                {
                    BioScreenController.Instance.SetInventoryOpen(false, true);
                    return;
                }

                //Resume game if paused
                if (!SessionData.Instance.play && !SessionData.Instance.isFloorEdit && SessionData.Instance.startedGame && (CityConstructor.Instance == null || !CityConstructor.Instance.preSimActive) &&!MainMenuController.Instance.mainMenuActive)
                {
                    SessionData.Instance.ResumeGame();
                    return;
                }
            }
        }

        //Play mode controls
        if (SessionData.Instance.play && SessionData.Instance.startedGame && !CutSceneController.Instance.cutSceneActive && (CityConstructor.Instance == null || !CityConstructor.Instance.preSimActive))
        {
            //Flashlight
            if (player.GetButtonDown("Flashlight"))
            {
                FirstPersonItemController.Instance.ToggleFlashlight();
                return;
            }

            //Cycle Nearby Interactable
            if(player.GetButtonDown("NearestInteractable") && !InteractionController.Instance.dialogMode/* && PlayerApartmentController.Instance.furniturePlacementMode*/)
            {
                InteractionController.Instance.UpdateNearbyInteractables();

                if(InteractionController.Instance.nearbyInteractables.Count > 0)
                {
                    int current = 0;

                    if(InteractionController.Instance.currentLookingAtInteractable != null)
                    {
                        current = InteractionController.Instance.nearbyInteractables.IndexOf(InteractionController.Instance.currentLookingAtInteractable.interactable);
                        Game.Log("Debug: Index of " + current + " found: " + current);
                    }

                    for (int i = 0; i < InteractionController.Instance.nearbyInteractables.Count; i++)
                    {
                        current++;
                        if (current >= InteractionController.Instance.nearbyInteractables.Count) current = 0;
                        if (InteractionController.Instance.currentLookingAtInteractable == null || InteractionController.Instance.nearbyInteractables[current] != InteractionController.Instance.currentLookingAtInteractable.interactable) break;
                    }

                    Interactable focusOn = InteractionController.Instance.nearbyInteractables[current];
                    Game.Log("Debug: Focus index: " + focusOn.GetName() + ": " + focusOn.id);

                    InteractionController.Instance.FocusOnInteractable(focusOn);
                    InteractionController.Instance.nearbyInteractablesHint = 0; //Reset hint counter

                    return;
                }
            }

            //Detect crouch toggle
            if (player.GetButtonDown("Crouch"))
            {
                if ((InteractionController.Instance.lockedInInteraction == null || InteractionController.Instance.carryingObject != null) && !Player.Instance.inAirVent && (!Player.Instance.transitionActive || InteractionController.Instance.carryingObject != null))
                {
                    Player.Instance.SetCrouched(!Player.Instance.isCrouched);
                    return;
                }
            }

            //Inventory hotkeys
            if (mouseInputMode)
            {
                if(!BioScreenController.Instance.isOpen)
                {
                    FirstPersonItemController.InventorySlot selectedSlot = null;

                    if (player.GetButtonDown("0"))
                    {
                        selectedSlot = FirstPersonItemController.Instance.slots.Find(item => item.hotkey == "0");
                    }
                    else if (player.GetButtonDown("1"))
                    {
                        selectedSlot = FirstPersonItemController.Instance.slots.Find(item => item.hotkey == "1");
                    }
                    else if (player.GetButtonDown("2"))
                    {
                        selectedSlot = FirstPersonItemController.Instance.slots.Find(item => item.hotkey == "2");
                    }
                    else if (player.GetButtonDown("3"))
                    {
                        selectedSlot = FirstPersonItemController.Instance.slots.Find(item => item.hotkey == "3");
                    }
                    else if (player.GetButtonDown("4"))
                    {
                        selectedSlot = FirstPersonItemController.Instance.slots.Find(item => item.hotkey == "4");
                    }
                    else if (player.GetButtonDown("5"))
                    {
                        selectedSlot = FirstPersonItemController.Instance.slots.Find(item => item.hotkey == "5");
                    }
                    else if (player.GetButtonDown("6"))
                    {
                        selectedSlot = FirstPersonItemController.Instance.slots.Find(item => item.hotkey == "6");
                    }
                    else if (player.GetButtonDown("7"))
                    {
                        selectedSlot = FirstPersonItemController.Instance.slots.Find(item => item.hotkey == "7");
                    }
                    else if (player.GetButtonDown("8"))
                    {
                        selectedSlot = FirstPersonItemController.Instance.slots.Find(item => item.hotkey == "8");
                    }
                    else if (player.GetButtonDown("9"))
                    {
                        selectedSlot = FirstPersonItemController.Instance.slots.Find(item => item.hotkey == "9");
                    }

                    if (selectedSlot != null)
                    {
                        if(BioScreenController.Instance.selectedSlot == selectedSlot)
                        {
                            //Nothing
                            BioScreenController.Instance.SelectSlot(FirstPersonItemController.Instance.slots.Find(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.holster));
                        }
                        else BioScreenController.Instance.SelectSlot(selectedSlot, false, false);
                    }
                }
            }
        }

        if(Game.Instance.demoAutoReset && Game.Instance.demoMode)
        {
            if(player.GetAnyButton())
            {
                SessionData.Instance.autoResetTimer = 0f;
            }
        }
    }

    public async void StartQuickSaveAsync()
    {
        string savePath = Application.persistentDataPath + "/Save/" + Strings.Get("ui.interface", "Quick Save") + ".sod";

        Game.Log("CityGen: Quick saving at: " + savePath);
        await SaveStateController.Instance.CaptureSaveStateAsync(savePath);
    }

    public void ResetCurrentButtonDown()
    {
        currentButtonDown = null;
    }

    public float GetAxisRelative(string actionId)
    {
        if (player == null)
        {
            Game.LogError("Input player is null!");
            return 0;
        }

        float value = player.GetAxis(actionId);
        //if (player.GetAxisCoordinateMode(actionId) == Rewired.AxisCoordinateMode.Absolute) value *= /*Time.unscaledDeltaTime **/ 40f;

        //if (Mathf.Abs(value) > 0.01f) Game.Log("Axis: " + value);

        if (Mathf.Abs(value) < 0.085f) return 0f; //Dead zone

        SessionData.Instance.autoPauseTimer = 0; //Reset pause timer
        SessionData.Instance.autoResetTimer = 0; //Reset reset timer

        return value;
    }

    public void SetMouseInputMode(bool val, bool forceUpdate = false)
    {
        if(mouseInputMode != val || forceUpdate)
        {
            mouseInputMode = val;
            Game.Log("Player: Set mouse input mode: " + mouseInputMode + " (Saving to player prefs)");

            PlayerPrefsController.GameSetting findSetting = PlayerPrefsController.Instance.gameSettingControls.Find(item => item.identifier == "controlMethod"); //Case insensitive find
            
            if(mouseInputMode)
            {
                findSetting.intValue = 1;
            }
            else
            {
                findSetting.intValue = 0;
            }

            PlayerPrefsController.Instance.OnToggleChanged("controlMethod", false);

            //Enable/disable the appropriate maps
            try
            {
                List<ControllerMap> maps = player.controllers.maps.GetAllMaps().ToList();

                foreach (ControllerMap m in maps)
                {
                    if (m.controllerType == ControllerType.Keyboard || m.controllerType == ControllerType.Mouse)
                    {
                        m.enabled = mouseInputMode;
                    }
                    else
                    {
                        m.enabled = !mouseInputMode;
                    }
                }
            }
            catch
            {
                Game.Log("Player: Unable to enabled/disable appropriate controller maps. The player may not be yet initialised...");
            }

            InterfaceController.Instance.ClearAllMouseOverElements();

            if (!mouseInputMode)
            {
                SetCursorVisible(false);
            }
            else
            {
                if(!cursorVisible)
                {
                    SetCursorVisible(true);

                    //Deselect currently selected...
                    if (InterfaceController.Instance.selectedElement != null)
                    {
                        InterfaceController.Instance.selectedElement.OnDeselect();
                    }
                }
            }

            if(InterfaceController.Instance.desktopMode)
            {
                CasePanelController.Instance.SetControllerMode(!mouseInputMode, CasePanelController.Instance.currentSelectMode);
            }

            //Fire event
            if (OnInputModeChange != null)
            {
                OnInputModeChange();
            }
        }
    }

    public void SetCursorVisible(bool val)
    {
        cursorVisible = val;

        if (Cursor.visible != cursorVisible)
        {
            if (mouseInputMode && cursorVisible)
            {
                Game.Log("Menu: Set cursor visible: " + true);
                Cursor.visible = true;
            }
            else
            {
                Game.Log("Menu: Set cursor visible: " + false);
                Cursor.visible = false;
            }
        }
    }

    public void SetCursorLock(bool value)
    {
        Player.Instance.fps.m_MouseLook.lockCursor = value;
        Game.Log("Menu: Set cursor lock: " + Player.Instance.fps.m_MouseLook.lockCursor);

        if (!Player.Instance.fps.m_MouseLook.lockCursor)
        {//we force unlock the cursor if the user disable the cursor locking helper
            Cursor.lockState = CursorLockMode.None;
            //InputController.Instance.SetCursorVisible(true);
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}
