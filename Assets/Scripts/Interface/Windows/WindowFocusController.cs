using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WindowFocusController : ForceMouseOverInput, IPointerEnterHandler, IPointerExitHandler
{
    void Update()
    {
        //Click to remove this
        if(InputController.Instance.mouseInputMode && mouseOver)
        {
            if (InputController.Instance.player.GetButtonDown("Select") || Input.GetMouseButtonDown(0))
            {
                InterfaceController.Instance.RemoveWindowFocus();
                InterfaceController.Instance.RemoveMouseOverElement(this);
            }
        }
    }
}
