using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;

public class SelectionIconController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public CanvasRenderer rend;
    public Image image;

    [Header("State")]
    public bool highlighted = false;
    public bool fadeIn = true;
    public bool destroy = false;
    public float alpha = 0f;
    public Interactable interactable;
    public float highlightProgress = 0f;

    [Header("Settings")]
    public Color highlightedColor = Color.white;
    public Color unHighlightedColor = Color.white;

    public void Setup(Interactable newInteractable)
    {
        interactable = newInteractable;

        //Set Icon
        if(interactable.preset.iconOverride != null)
        {
            image.sprite = interactable.preset.iconOverride;
        }
        else if(interactable.evidence != null)
        {
            image.sprite = interactable.evidence.GetIcon();
        }
        
        rend.SetAlpha(alpha);
    }

    private void Update()
    {
        if(destroy)
        {
            if(alpha > 0f)
            {
                alpha -= Time.deltaTime / 0.1f;
                alpha = Mathf.Clamp01(alpha);
                rend.SetAlpha(alpha);
            }
            else
            {
                InteractionController.Instance.selectionIcons.Remove(this);
                Destroy(this.gameObject);
            }
        }
        else
        {
            if (alpha < 1f)
            {
                alpha += Time.deltaTime / 0.1f;
                alpha = Mathf.Clamp01(alpha);
                rend.SetAlpha(alpha);
            }
            else if (fadeIn)
            {
                fadeIn = false;
            }

            if(highlighted)
            {
                if(highlightProgress < 1f)
                {
                    highlightProgress += Time.deltaTime / 0.1f;
                    highlightProgress = Mathf.Clamp01(highlightProgress);
                    image.color = Color.Lerp(unHighlightedColor, highlightedColor, highlightProgress);
                }
            }
            else
            {
                if (highlightProgress > 0f)
                {
                    highlightProgress -= Time.deltaTime / 0.1f;
                    highlightProgress = Mathf.Clamp01(highlightProgress);
                    image.color = Color.Lerp(unHighlightedColor, highlightedColor, highlightProgress);
                }
            }
        }
    }

    public void Remove()
    {
        destroy = true;
    }

    public void SetHighlighted(bool val)
    {
        if(highlighted != val)
        {
            highlighted = val;
        }
    }
}
