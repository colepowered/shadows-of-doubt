﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Linq;

//This script contains a function that populates the map with buildings.
//Script pass 1
public class BuildingCreator : Creator
{
    //Chunks to load per frame
    public int loadChunk = 10;

    //Array of buildings from ScriptableObjects
    private List<BuildingPreset> buildingPresets;

	//Selection list - for drawing random buildings
    public class PickBuilding
    {
        public BuildingPreset preset;
        public float rank;
    }

	List<PickBuilding> selectionList = new List<PickBuilding>();

	//Singleton pattern
	private static BuildingCreator _instance;
    public static BuildingCreator Instance { get { return _instance; } }

	void Awake ()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
	}

    public override void StartLoading()
    {
        Game.Log("CityGen: Populating with buildings...");
        StartCoroutine("Load");
	}

	IEnumerator Load()
	{
		//Load all buildings into memory, create list
        if(CityConstructor.Instance.generateNew)
        {
            buildingPresets = AssetLoader.Instance.GetAllBuildingPresets();

            //Return if no buildings in preset list
            if (buildingPresets.Count <= 0 || buildingPresets == null)
            {
                Game.LogError("CityGen: Buildings preset list is empty!");
                yield break;
            }
        }

		//Collect all tiles
        List<CityTile> all = new List<CityTile>();

        foreach (KeyValuePair<Vector2Int, CityTile> pair in CityData.Instance.cityTiles)
        {
            all.Add(pair.Value);
        }

        int loadBuildingsTotal = all.Count;
		int loadBuildingsProgress = 0;

        //Loop through all tiles
        int loopFailSafe = 9999;

		while(all.Count > 0 & loopFailSafe > 0)
		{
			for (int b = 0; b < loadChunk; b++)
			{
				//Break loop when all buildings are constructed
				if (all.Count <= 0) break;

                CityTile current = all[0];

				//Double check to see if this is empty
				if (current.building == null)
				{
                    if (CityConstructor.Instance.generateNew)
                    {
                        //Clear selection list
                        selectionList.Clear();

                        //Loop through each element in building list, add appropriate entries to genList
                        for (int bd = 0; bd < buildingPresets.Count; bd++)
                        {
                            BuildingPreset item = buildingPresets[bd];

                            if (item.disable) continue;

                            bool isBoundary = false;

                            //Is this a boundary?
                            if (current.cityCoord.x == 0 || current.cityCoord.x == CityData.Instance.citySize.x - 1 || current.cityCoord.y == 0 || current.cityCoord.y == CityData.Instance.citySize.y - 1)
                            {
                                isBoundary = true;

                                if (!item.boundary) continue; //Exclude

                                //Is this a boundary corner?
                                if ((current.cityCoord.x == 0 || current.cityCoord.x == CityData.Instance.citySize.x - 1) && (current.cityCoord.y == 0 || current.cityCoord.y == CityData.Instance.citySize.y - 1))
                                {
                                    //Don't consider non-corner pieces
                                    if (!item.boundaryCorner)
                                    {
                                        continue;
                                    }
                                }
                                else if (item.boundaryCorner)
                                {
                                    continue;
                                }
                            }
                            else if (item.boundary) continue;

                            //Correct district
                            if (!item.allowedInAllDistricts && !item.allowedInDistricts.Contains(current.district.preset))
                            {
                                continue;
                            }

                            //Check city ratio
                            int currentCount = CityData.Instance.buildingDirectory.FindAll(n => n.preset == item).Count;

                            //Hard limit reached (ignore if boundary)
                            if (currentCount >= item.hardLimit && !isBoundary)
                            {
                                continue;
                            }

                            float rank = Toolbox.Instance.GetPsuedoRandomNumber(0f, 0.2f, current.cityCoord.ToString() + NewBuilding.assignID); //Random base

                            //Correct zone type
                            //if (item.zoneType.Contains(current.zoneType))
                            //{
                            //    rank += 10f;
                            //}

                            //Correct density: +10
                            if ((int)current.density >= (int)item.densityMinimum && (int)current.density <= (int)item.densityMaximum)
                            {
                                rank += 10f;
                            }

                            //Correct land value: +10
                            if ((int)current.landValue >= (int)item.landValueMinimum && (int)current.landValue <= (int)item.landValueMaximum)
                            {
                                rank += 10f;
                            }

                            float currentRatio = (float)currentCount / ((float)(CityData.Instance.citySize.x - 2) * (float)(CityData.Instance.citySize.y - 2));

                            //Failsafe for NaN
                            if (float.IsNaN(currentRatio))
                            {
                                currentRatio = 0f;
                            }

                            //Correct ratio limit: + up to 10
                            rank += (item.desiredRatio - currentRatio) * 10f; //Add difference in ratio

                            //Minimum & feature importance: up to +100
                            if(currentCount < item.minimum)
                            {
                                rank += item.featureImportance * 10f;
                            }

                            selectionList.Add(new PickBuilding { preset = item, rank = rank });
                        }

                        //Rank selection list by highest first
                        selectionList.Sort((p1, p2) => p2.rank.CompareTo(p1.rank));

                        if (selectionList.Count > 0)
                        {
                            //Selection list is generated!
                            PickBuilding selection = selectionList[0];

                            //Spawn building
                            GameObject newBuildingObj = Instantiate(PrefabControls.Instance.building, current.transform);
                            NewBuilding newBuilding = newBuildingObj.GetComponent<NewBuilding>();
                            newBuilding.Setup(current, selection.preset);
                        }
                    }
                    else
                    {
                        //Find city tile data
                        CitySaveData.CityTileCitySave tileData = CityConstructor.Instance.currentData.cityTiles.Find(item => item.cityCoord == current.cityCoord);

                        //Spawn building
                        GameObject newBuildingObj = Instantiate(PrefabControls.Instance.building, current.transform);
                        NewBuilding newBuilding = newBuildingObj.GetComponent<NewBuilding>();
                        newBuilding.Load(tileData.building, current);
                    }
				}

				//This tile is loaded, remove top entry to progress to the next.
				all.RemoveAt(0);
				loadBuildingsProgress++;
			}

			CityConstructor.Instance.loadingProgress = (float)loadBuildingsProgress / (float)loadBuildingsTotal;

            if(loopFailSafe <= 0)
            {
                Game.LogError("CityGen: Loop failsafe triggered!");
                break;
            }

            loopFailSafe--;
			yield return null;
		}

        //Complete!
        SetComplete();
	}
}
