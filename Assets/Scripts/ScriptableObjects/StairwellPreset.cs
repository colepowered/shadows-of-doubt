﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "stairwell_data", menuName = "Database/Stairwell Preset")]

public class StairwellPreset : SoCustomComparison
{
    [Header("Setup")]
    public GameObject spawnObject;
    public GameObject objectTop;
    public GameObject centralSteps;

    [Header("Elevator")]
    [Tooltip("Does this stairwell feature an elevator?")]
    public bool featuresElevator = true;
    [Tooltip("The elevator object to spawn")]
    public GameObject elevatorObject;
    [Tooltip("How fast the elevator can travel")]
    public float elevatorMaxSpeed = 2f;
    [Tooltip("How fast the elevator can accelerate")]
    public float elevatorAcceleration = 0.25f;
    [Tooltip("The elevator accelerates if further away than this from its destination")]
    public float accelerateWhileThisFarAway = 10f;
    [Tooltip("How long the lift stays at a destination when there is somewhere else to go")]
    public float liftDelay = 3f;
    [Tooltip("How long the lift stays put after a new call when beginning movement")]
    public float movementDelay = 1f;
}
