﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalculateNearest : Creator
{
    //Chunks to load per frame
    public int loadChunk = 20;

    //Singleton pattern
    private static CalculateNearest _instance;
    public static CalculateNearest Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    //Create evidence files
    public override void StartLoading()
    {
        StartCoroutine("GenChunk");
    }

    IEnumerator GenChunk()
    {
        int cursor = 0;

        while (cursor < CityData.Instance.gameLocationDirectory.Count)
        {
            for (int i = 0; i < loadChunk; i++)
            {
                //Break loop if complete
                if (cursor >= CityData.Instance.gameLocationDirectory.Count) break;

                //CityData.Instance.gameLocationDirectory[cursor].GetNearestPublicActions();

                cursor++;
            }

            CityConstructor.Instance.loadingProgress = ((float)cursor / (float)CityData.Instance.gameLocationDirectory.Count);
            yield return null;
        }

        //Complete!
        SetComplete();
    }
}
