﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using TMPro;
using UnityEngine.UI;

public class ObjectiveContentListEntry : ButtonController
{
    [Header("References")]
    public TextMeshProUGUI objectiveText;
    public Case.ResolveQuestion question;
    public ObjectivesContentController objectivesController;

    public void Setup(ObjectivesContentController newController, Case.ResolveQuestion newStarting)
    {
        SetupReferences();

        objectivesController = newController;
        question = newStarting;

        VisualUpdate();
    }

    public override void VisualUpdate()
    {
        if(question != null)
        {
            objectiveText.text = question.GetText(null, true, true);
        }
    }
}
