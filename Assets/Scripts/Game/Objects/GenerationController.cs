using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.IO;
using System.Linq;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Rendering;
using UnityEditor;
using NaughtyAttributes;
using System.Runtime.CompilerServices;

public class GenerationController : MonoBehaviour
{
    //Room location class
    [System.Serializable]
    public class PossibleRoomLocation : IComparable<PossibleRoomLocation>
    {
        public List<NewNode> nodes = new List<NewNode>();

        //Ranking breakdown
        public float randomRanking = 0f;

        public float exteriorWindowRanking = 0f;
        public float exteriorWallsRanking = 0f;
        public float floorSpaceRanking = 0f;
        public float entrancesRanking = 0f;

        public List<OverrideData> overrideRankingData = new List<OverrideData>();

        public float ranking = 0f;
        public List<NewNode> requiredAdjoiningOptions = new List<NewNode>(); //If this room config must adjoin another room, these are the options

        public List<NewNode> requiredHallway = new List<NewNode>(); //Also generate this hallway to connect with an entrance

        public GenerationDebugController debugScript;

        //Default comparer (ranking)
        public int CompareTo(PossibleRoomLocation otherObject)
        {
            return this.ranking.CompareTo(otherObject.ranking);
        }
    }

    //For holding room override ranking data
    [System.Serializable]
    public struct OverrideData
    {
        public NewRoom room;
        public float floorSpacePenalty;
        public float exteriorWindowPenalty;
        public float exteriorWallPenalty;

        public float overridingPenalty;
    }

    //Doorway location class
    [System.Serializable]
    public class PossibleDoorwayLocation : IComparable<PossibleDoorwayLocation>
    {
        public NewWall wall;
        public float ranking = 0;
        public bool requireFlatDoorway = false;
        public List<NewWall> roomDivider; //If possible, the walls which will become dividers (must contain 2+ entries)

        //Default comparer (ranking)
        public int CompareTo(PossibleDoorwayLocation otherObject)
        {
            return this.ranking.CompareTo(otherObject.ranking);
        }
    }

    //Null expansion class
    [System.Serializable]
    public class PossibleNullExpansion : IComparable<PossibleNullExpansion>
    {
        public List<NewNode> nodesToExpand = new List<NewNode>();
        public NewRoom addToRoom;
        public float ranking = 0;

        //Default comparer (ranking)
        public int CompareTo(PossibleNullExpansion otherObject)
        {
            return this.ranking.CompareTo(otherObject.ranking);
        }
    }

    public struct ClusterRank
    {
        public FurnitureCluster cluster;
        public float rank;
    }

    //Update values
    private bool updateGeometryActive = false;
    private List<NewFloor> updateTheseFloors = new List<NewFloor>();

    private bool loadGeometryActive = false;
    private List<NewFloor> loadTheseFloors = new List<NewFloor>();

    private bool roomUnloadCheckActive = false;
    public int oldestRoomUnloadTimer = 0;

    //Room loading pool
    public List<NewRoom> spawnedRooms = new List<NewRoom>(); //List of rooms that are loaded to game. These could be hidden, but basically where the walls, floor and ceiling have been spawned.

    //Singleton pattern
    private static GenerationController _instance;
    public static GenerationController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    //Refresh/update geometry for a floor
    public void UpdateGeometryFloor(NewFloor editFloor, string debug = "")
    {
        //Game.Log(debug);

        if (!updateTheseFloors.Contains(editFloor))
        {
            updateTheseFloors.Add(editFloor);

            //Do this @ end of frame
            if (!updateGeometryActive)
            {
                try
                {
                    StartCoroutine(ExeUpdateGeometryAtEndOfFrame());
                }
                catch
                {

                }
            }
        }
    }

    IEnumerator ExeUpdateGeometryAtEndOfFrame()
    {
        updateGeometryActive = true;
        bool wait = true;

        while (wait)
        {
            wait = false;

            yield return new WaitForEndOfFrame();
        }

        foreach (NewFloor floor in updateTheseFloors)
        {
            if (floor == null) continue;

            //UpdateFloorCeiling(floor);
            //UpdateWalls(floor);

            foreach (NewAddress add in floor.addresses)
            {
                foreach (NewRoom room in add.rooms)
                {
                    UpdateFloorCeilingRoom(room);
                    UpdateWallsRoom(room);
                }
            }
        }

        updateTheseFloors.Clear();

        updateGeometryActive = false;
    }

    //Load/refresh geometry for a floor (this only happens in floor editor)
    public void LoadGeometryFloor(NewFloor editFloor)
    {
        if (!loadTheseFloors.Contains(editFloor))
        {
            loadTheseFloors.Add(editFloor);

            //Do this @ end of frame
            if (!loadGeometryActive)
            {
                try
                {
                    StartCoroutine(LoadGeometryAtEndOfFrame());
                }
                catch
                {

                }
            }
        }
    }

    IEnumerator LoadGeometryAtEndOfFrame()
    {
        loadGeometryActive = true;
        bool wait = true;

        while (wait)
        {
            wait = false;

            yield return new WaitForEndOfFrame();
        }

        foreach (NewFloor floor in loadTheseFloors)
        {
            if (floor == null)
            {
                continue;
            }

            //Spawn floor/ceiling
            foreach (KeyValuePair<Vector2Int, NewTile> pair in floor.tileMap)
            {
                pair.Value.SetFloorCeilingOptimization(pair.Value.useOptimizedFloor, true); //Set optimization

                //Spawn stairs and elevators
                pair.Value.SetAsStairwell(pair.Value.isStairwell, true, pair.Value.isInvertedStairwell);
            }

            //Spawn walls
            foreach (NewAddress add in floor.addresses)
            {
                foreach (NewNode nod in add.nodes)
                {
                    foreach (NewWall wal in nod.walls)
                    {
                        wal.SpawnWall(false);
                        wal.SpawnFrontage();
                    }
                }
            }

            //These are now loaded
            foreach (NewAddress add in floor.addresses)
            {
                foreach (NewRoom room in add.rooms)
                {
                    //if (!SessionData.Instance.isFloorEdit && CityData.Instance.spawnedRooms.Count > ObjectPoolingController.Instance.roomsLoaded)
                    //{
                    //    UnloadOldestRoom();
                    //}

                    LoadCornersRoom(room);
                    room.geometryLoaded = true;

                    //if (!SessionData.Instance.isFloorEdit)
                    //{
                    //    CityData.Instance.spawnedRooms.Add(room);
                    //    ObjectPoolingController.Instance.roomsLoaded = CityData.Instance.spawnedRooms.Count;
                    //}
                }
            }
        }

        loadTheseFloors.Clear();
        loadGeometryActive = false;
    }

    //Load/refresh geometry for a room
    public void LoadGeometryRoom(NewRoom room)
    {
        if (!SessionData.Instance.isFloorEdit && !GenerationController.Instance.spawnedRooms.Contains(room))
        {
            GenerationController.Instance.spawnedRooms.Add(room);
            ObjectPoolingController.Instance.roomsLoaded = GenerationController.Instance.spawnedRooms.Count;
        }

        //Do not spawn any room geometry for outdoors
        if (room.isOutsideWindow && room.gameLocation.thisAsStreet == null)
        {
            //This is now loaded
            room.geometryLoaded = true;
            room.gameObject.SetActive(true);

            return;
        }

        if(!SessionData.Instance.isFloorEdit) MeshPoolingController.Instance.SpawnMeshesForRoom(room); //The mesh pooler will handle caching etc...

        //This is now loaded
        room.geometryLoaded = true;
        room.gameObject.SetActive(true);

        //Setup lights
        if(room.preset != null)
        {
            //Spawn area lights
            foreach (NewRoom.LightZoneData lz in room.lightZones)
            {
                if (room.preset.useAdditionalAreaLights)
                {
                    bool success = lz.CreateAreaLight();

                    if (success && InteriorControls.Instance.oneAreaLightPerRoom && room.gameLocation.thisAsStreet == null)
                    {
                        break;
                    }
                }
                //TODO: NO LIGHTING WORKAROUND: Create an area light instead...
                //THIS SHOULD NOW BE FIXED SO DELETE, HERE FOR WASD BUILD WORKAROUND
                else if (lz.allowLight && room.mainLights.Count <= 0 && room.preset.useMainLights)
                {
                    lz.areaLightBrightness = 150;
                    bool success = lz.CreateAreaLight();

                    if (success && InteriorControls.Instance.oneAreaLightPerRoom && room.gameLocation.thisAsStreet == null)
                    {
                        break;
                    }
                }
            }

            //Boost ceiling emission
            if (room.preset.boostCeilingEmission && room.ceilingMat != null && room.uniqueCeilingMaterial)
            {
                if (room.mainLightStatus)
                {
                    Color areaLightColour = Color.white;
                    if (room.lightZones.Count > 0) areaLightColour = room.lightZones[0].areaLightColour;
                    room.ceilingMat.SetColor("_EmissiveColor", room.preset.ceilingEmissionBoost * areaLightColour);
                }
                else
                {
                    room.ceilingMat.SetColor("_EmissiveColor", Color.black);
                }
            }

            //Spawn bugs...
            if (room.preset.allowBugs)
            {
                int nodesWithFloor = 0;

                foreach(NewNode n in room.nodes)
                {
                    if(n.floorType == NewNode.FloorTileType.floorAndCeiling || n.floorType == NewNode.FloorTileType.floorOnly)
                    {
                        nodesWithFloor++;
                    }
                }

                int bugsToSpawn = Mathf.FloorToInt(room.floorMatKey.grubiness * (nodesWithFloor * 0.4f) * room.preset.bugAmountMultiplier);

                for (int i = room.spawnedBugs.Count; i < bugsToSpawn; i++)
                {
                    //Spawn bug
                    GameObject newBug = Toolbox.Instance.SpawnObject(InteriorControls.Instance.bug, room.transform);
                    BugController bug = newBug.GetComponent<BugController>();
                    bug.Setup(room);
                    room.spawnedBugs.Add(bug);
                }
            }
        }
    }

    //Attempt to unload rooms not used in order to get back under the threshold...
    public void UnloadOldestRooms()
    {
        if (roomUnloadCheckActive) return;

        if (oldestRoomUnloadTimer > 0)
        {
            oldestRoomUnloadTimer--;
            return; //Only do this every so often...
        }

        oldestRoomUnloadTimer = 10;

        StartCoroutine(UnloadOldestRoomsAtEndOfFrame());
    }

    IEnumerator UnloadOldestRoomsAtEndOfFrame()
    {
        roomUnloadCheckActive = true;
        bool wait = true;

        while (wait)
        {
            wait = false;

            yield return new WaitForEndOfFrame();
        }

        Game.Log("Debug: Attempting to unload oldest room(s)...");

        for (int i = 0; i < spawnedRooms.Count; i++)
        {
            if (spawnedRooms.Count < ObjectPoolingController.Instance.maxRoomCache) break; //There are now enough rooms in the cache

            NewRoom room = spawnedRooms[i];
            if (room.gameObject.activeSelf) continue; //Don't unload anything that is currently active...

            //Game.Log("Unloading room " + room.name + "...");

            //Unload furnishings/objects
            foreach (NewNode node in room.nodes)
            {
                for (int l = 0; l < node.interactables.Count; l++)
                {
                    node.interactables[l].DespawnObject();
                }
            }

            foreach (FurnitureClusterLocation furnLoc in room.furniture)
            {
                furnLoc.UnloadFurniture(false, FurnitureClusterLocation.RemoveInteractablesOption.keep);
            }

            //Remove meshes
            Toolbox.Instance.DestroyObject(room.combinedFloor);
            Toolbox.Instance.DestroyObject(room.combinedCeiling);
            Toolbox.Instance.DestroyObject(room.combinedWalls);

            List<NewBuilding> additionalKeys = room.additionalWalls.Keys.ToList();

            while (additionalKeys.Count > 0)
            {
                Destroy(room.additionalWalls[additionalKeys[0]]);
                additionalKeys.RemoveAt(0);
            }

            room.additionalWalls = new Dictionary<NewBuilding, GameObject>();

            while (room.spawnedBugs.Count > 0)
            {
                Destroy(room.spawnedBugs[0].gameObject);
                room.spawnedBugs.RemoveAt(0);
            }

            room.spawnedBugs = new List<BugController>();

            //Unload wall frontages

            //Unload area lights
            foreach (NewRoom.LightZoneData lz in room.lightZones)
            {
                lz.RemoveAreaLight();
            }

            //This is now unloaded
            room.geometryLoaded = false;
            room.gameObject.SetActive(false);
            GenerationController.Instance.spawnedRooms.RemoveAt(i);
            ObjectPoolingController.Instance.roomsLoaded = GenerationController.Instance.spawnedRooms.Count;
            i--;
        }

        roomUnloadCheckActive = false;
    }

    //Scan all tiles to see where floor & ceiling optimizations can be made by spawning a large tile instead of a small one
    public void UpdateFloorCeilingFloor(NewFloor editFloor)
    {
        //Game.Log("Update floor/ceiling for floor " + editFloor);

        if (editFloor == null)
        {
            //Game.Log("floor is missing");
            return;
        }

        foreach (KeyValuePair<Vector2Int, NewTile> pair in editFloor.tileMap)
        {
            pair.Value.SetFloorCeilingOptimization(pair.Value.CanBeOptimized(), false); //Set optimization
        }
    }

    public void UpdateFloorCeilingRoom(NewRoom room)
    {
        //Game.Log("Update floor/ceiling for floor " + editFloor);

        if (room == null)
        {
            //Game.Log("floor is missing");
            return;
        }

        List<NewTile> tilesInRoom = new List<NewTile>();

        foreach (NewNode node in room.nodes)
        {
            if (!tilesInRoom.Contains(node.tile))
            {
                tilesInRoom.Add(node.tile);
            }
        }

        foreach (NewTile tile in tilesInRoom)
        {
            tile.SetFloorCeilingOptimization(tile.CanBeOptimized(), false); //Set optimization
        }
    }

    public void UpdateWallsFloor(NewFloor editFloor)
    {
        //Game.Log("Update walls for floor " + editFloor + editFloor.floorID.ToString());

        if (editFloor == null)
        {
            //Game.Log("floor is missing");
            return;
        }

        //Justify all walls existance
        List<NewWall> allWalls = new List<NewWall>();
        List<NewTile> tiles = new List<NewTile>();

        foreach (KeyValuePair<Vector2Int, NewNode> pair in editFloor.nodeMap)
        {
            foreach (NewWall wall in pair.Value.walls)
            {
                allWalls.Add(wall);
                wall.optimizationAnchor = false;
                wall.optimizationOverride = false;
            }
        }

        //Setup walls
        foreach (KeyValuePair<Vector2Int, NewNode> pair in editFloor.nodeMap)
        {
            NewNode nod = pair.Value;

            //Add a tile for checking later
            if (!tiles.Contains(nod.tile))
            {
                tiles.Add(nod.tile);
            }

            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
            {
                Vector3Int check = new Vector3Int(nod.nodeCoord.x + v2.x, nod.nodeCoord.y + v2.y, nod.nodeCoord.z);
                NewNode foundNode = null;

                if (PathFinder.Instance.nodeMap.TryGetValue(check, out foundNode))
                {
                    //If the adjacent has a different assigned room, then there needs to be a wall here...
                    if (foundNode.room != nod.room)
                    {
                        //Ignore if connected 2 streets...
                        if (foundNode.gameLocation.thisAsStreet != null && nod.gameLocation.thisAsStreet != null) continue;

                        //Add a tile for checking later
                        if (!tiles.Contains(foundNode.tile))
                        {
                            tiles.Add(foundNode.tile);
                        }

                        //What's the offset?
                        Vector2 diff = new Vector2(foundNode.nodeCoord.x - nod.nodeCoord.x, foundNode.nodeCoord.y - nod.nodeCoord.y);
                        Vector2 wallOffset = diff * 0.5f;

                        //Does this wall exist already?
                        NewWall wallEx = null;

                        //If this doesn't exist, spawn it...
                        if (!nod.wallDict.TryGetValue(wallOffset, out wallEx))
                        {
                            //GameObject newWallObj = Instantiate(PrefabControls.Instance.wall, nod.room.transform);
                            //NewWall newWall = newWallObj.GetComponent<NewWall>();

                            NewWall newWall = new NewWall();

                            //Setup pairing...
                            //Is there a wall behind this one?
                            newWall.otherWall = foundNode.walls.Find(item => item.wallOffset == wallOffset * -1); //The pair is the opposite tile with wall of opposite offset

                            //Use existing preset...
                            if (newWall.otherWall != null)
                            {
                                newWall.Setup(newWall.otherWall.preset, nod, wallOffset, nod.tile.isEdge);
                                //newWall.presetAssignDebug.Add("Preset through setup: Copied from other wall");
                            }
                            else
                            {
                                newWall.Setup(CityControls.Instance.defaultWalls, nod, wallOffset, nod.tile.isEdge);
                                //newWall.presetAssignDebug.Add("Preset through setup: Default walls");
                            }

                            //Found a pair
                            if (newWall.otherWall != null)
                            {
                                newWall.otherWall.otherWall = newWall; //Set in other

                                //Parent ranking:
                                bool isParentRoom = false;

                                //If the other room is null space
                                if (newWall.node.room.roomType != InteriorControls.Instance.nullRoomType && newWall.otherWall.node.room.roomType == InteriorControls.Instance.nullRoomType)
                                {
                                    isParentRoom = true;
                                }
                                //If neither are null space
                                else if (newWall.node.room.roomType != InteriorControls.Instance.nullRoomType && newWall.otherWall.node.room.roomType != InteriorControls.Instance.nullRoomType)
                                {
                                    if (newWall.node.room.roomType.cyclePriority > newWall.otherWall.node.room.roomType.cyclePriority)
                                    {
                                        isParentRoom = true; //If the priority is higher
                                    }
                                    //If priorities are the same
                                    else if (newWall.node.room.roomType.cyclePriority == newWall.otherWall.node.room.roomType.cyclePriority)
                                    {
                                        if (newWall.node.room.roomFloorID > newWall.otherWall.node.room.roomFloorID) isParentRoom = true; //Now onto meaningless stats just to decide a constant parent
                                    }
                                }

                                //Outside is the 'lowest', then use room importance to get parent
                                if (isParentRoom)
                                {
                                    newWall.parentWall = newWall;
                                    newWall.childWall = newWall.otherWall;

                                    newWall.otherWall.parentWall = newWall;
                                    newWall.otherWall.childWall = newWall.otherWall;
                                }
                                else
                                {
                                    newWall.parentWall = newWall.otherWall;
                                    newWall.childWall = newWall;

                                    newWall.otherWall.parentWall = newWall.otherWall;
                                    newWall.otherWall.childWall = newWall;
                                }

                                //Copy the preset from the parent to both walls (parent wall wins)
                                if (newWall.childWall.preset != newWall.parentWall.preset)
                                {
                                    Game.Log("Copy preset from parent wall...");
                                    newWall.childWall.SetDoorPairPreset(newWall.parentWall.preset);
                                    //newWall.childWall.presetAssignDebug.Add("Preset copied from parent wall");
                                }

                                //Building exterior setup
                                if (newWall.node.building != null && newWall.otherWall.node.building == null)
                                {
                                    if (newWall.node.building.preset.defaultExteriorWallMaterial.Count > 0)
                                    {
                                        newWall.otherWall.separateWall = true;
                                    }
                                }
                                else if (newWall.node.building == null && newWall.otherWall.node.building != null)
                                {
                                    if (newWall.otherWall.node.building.preset.defaultExteriorWallMaterial.Count > 0)
                                    {
                                        newWall.separateWall = true;
                                    }
                                }

                                //Update debug
                                //newWall.childName = newWall.childWall.name;
                                //newWall.parentName = newWall.parentWall.name;
                                //newWall.otherName = newWall.otherWall.name;

                                //newWall.otherWall.childName = newWall.childWall.name;
                                //newWall.otherWall.parentName = newWall.parentWall.name;
                                //newWall.otherWall.otherName = newWall.otherWall.otherWall.name;
                            }
                            else
                            {

                            }
                        }
                        else
                        {
                            //If this already exists then remove it from 'allwalls'
                            allWalls.Remove(wallEx);
                        }
                    }
                }
                else
                {
                    //If no tile exists here, this must be an edge tile and this direction faces nothing...
                }
            }
        }

        //Remove all unwanted walls
        for (int i = 0; i < allWalls.Count; i++)
        {
            allWalls[i].RemoveWall();

            allWalls.RemoveAt(i);
            i--;
        }

        //Search for instances of 3x walls in a row so we can use one object
        foreach (NewTile tile in tiles)
        {
            //Scan for 3x in a row horizonal
            for (int i = 0; i < CityControls.Instance.nodeMultiplier; i++)
            {
                //List of walls horizonally in a row
                List<NewWall> wallsAbove = new List<NewWall>();
                List<NewWall> wallsBelow = new List<NewWall>();

                //List of wall vertically in a column
                List<NewWall> wallsLeft = new List<NewWall>();
                List<NewWall> wallsRight = new List<NewWall>();

                for (int u = 0; u < CityControls.Instance.nodeMultiplier; u++)
                {
                    //Check horizonal
                    Vector2 check = new Vector2(u, i);

                    //Find this node
                    NewNode node = tile.nodes.Find(item => item.localTileCoord == check);

                    if (node != null)
                    {
                        //Wall above
                        NewWall wallAbove = node.walls.Find(item => item.wallOffset.x == 0 && item.wallOffset.y > 0 && item.preset.optimizeSections);
                        if (wallAbove != null) wallsAbove.Add(wallAbove);

                        //Wall below
                        NewWall wallBelow = node.walls.Find(item => item.wallOffset.x == 0 && item.wallOffset.y < 0 && item.preset.optimizeSections);
                        if (wallBelow != null) wallsBelow.Add(wallBelow);
                    }

                    //Check vertical
                    check = new Vector2(i, u);

                    //Find this node
                    node = tile.nodes.Find(item => item.localTileCoord == check);

                    if (node != null)
                    {
                        //Wall left
                        NewWall wallLeft = node.walls.Find(item => item.wallOffset.x < 0 && item.wallOffset.y == 0 && item.preset.optimizeSections);
                        if (wallLeft != null) wallsLeft.Add(wallLeft);

                        //Wall right
                        NewWall wallRight = node.walls.Find(item => item.wallOffset.x > 0 && item.wallOffset.y == 0 && item.preset.optimizeSections);
                        if (wallRight != null) wallsRight.Add(wallRight);
                    }
                }

                //Set wall optimizations
                List<List<NewWall>> listOfLists = new List<List<NewWall>>();
                listOfLists.Add(wallsAbove);
                listOfLists.Add(wallsBelow);
                listOfLists.Add(wallsLeft);
                listOfLists.Add(wallsRight);

                foreach (List<NewWall> wallList in listOfLists)
                {
                    //There are 3 walls in a line...
                    if (wallList.Count >= CityControls.Instance.nodeMultiplier)
                    {
                        //The walls all belong to the same room
                        if (wallList[0].node.room == wallList[1].node.room && wallList[0].node.room == wallList[2].node.room)
                        {
                            ////The walls all share the same material group
                            //if (wallList[0].room.wallMaterial == wallList[1].wallMaterial && wallList[0].wallMaterial == wallList[2].wallMaterial)
                            //{
                            //    //The walls all share the same material key
                            //    if (wallList[0].wallMatKey.Equals(wallList[1].wallMatKey) && wallList[0].wallMatKey.Equals(wallList[2].wallMatKey))
                            //    {
                            for (int l = 0; l < wallList.Count; l++)
                            {
                                if (l == Mathf.FloorToInt(CityControls.Instance.nodeMultiplier * 0.5f))
                                {
                                    wallList[l].optimizationOverride = false;
                                    wallList[l].optimizationAnchor = true;
                                }
                                else
                                {
                                    wallList[l].optimizationOverride = true;
                                    wallList[l].optimizationAnchor = false;
                                }
                            }
                            //    }
                            //}
                        }
                    }
                }
            }
        }
    }

    //Room-wide wall update
    public void UpdateWallsRoom(NewRoom room)
    {
        //Game.Log("Update walls for floor " + editFloor + editFloor.floorID.ToString());

        if (room == null)
        {
            //Game.Log("floor is missing");
            return;
        }

        //Justify all walls existance
        List<NewWall> allWalls = new List<NewWall>();
        List<NewTile> tiles = new List<NewTile>();

        foreach (NewNode nod in room.nodes)
        {
            foreach (NewWall wall in nod.walls)
            {
                allWalls.Add(wall);
                wall.optimizationAnchor = false;
                wall.optimizationOverride = false;
            }
        }

        //Setup walls
        foreach (NewNode nod in room.nodes)
        {
            //Add a tile for checking later
            if (!tiles.Contains(nod.tile))
            {
                tiles.Add(nod.tile);
            }

            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
            {
                Vector3Int check = new Vector3Int(nod.nodeCoord.x + v2.x, nod.nodeCoord.y + v2.y, nod.nodeCoord.z);
                NewNode foundNode = null;

                if (PathFinder.Instance.nodeMap.TryGetValue(check, out foundNode))
                {
                    //If the adjacent has a different assigned room, then there needs to be a wall here...
                    if (foundNode.room != nod.room)
                    {
                        bool isAlleyBlocker = false;

                        //Ignore if connected 2 streets...
                        if (foundNode.gameLocation.thisAsStreet != null && nod.gameLocation.thisAsStreet != null)
                        {
                            //If both are alleyways then allow this wall...
                            if (!foundNode.gameLocation.thisAsStreet.isAlley || !nod.gameLocation.thisAsStreet.isAlley)
                            {
                                continue;
                            }
                            else isAlleyBlocker = true;
                        }

                        //Add a tile for checking later
                        if (!tiles.Contains(foundNode.tile))
                        {
                            tiles.Add(foundNode.tile);
                        }

                        //What's the offset?
                        Vector2 diff = new Vector2(foundNode.nodeCoord.x - nod.nodeCoord.x, foundNode.nodeCoord.y - nod.nodeCoord.y);
                        Vector2 wallOffset = diff * 0.5f;

                        //Does this wall exist already?
                        NewWall wallEx = null;

                        //If this doesn't exist, spawn it...
                        if (!nod.wallDict.TryGetValue(wallOffset, out wallEx))
                        {
                            //GameObject newWallObj = Instantiate(PrefabControls.Instance.wall, nod.room.transform);
                            //NewWall newWall = newWallObj.GetComponent<NewWall>();

                            NewWall newWall = new NewWall();

                            //Setup pairing...
                            //Is there a wall behind this one?
                            newWall.otherWall = foundNode.walls.Find(item => item.wallOffset == wallOffset * -1); //The pair is the opposite tile with wall of opposite offset

                            //Use existing preset...
                            if (newWall.otherWall != null)
                            {
                                newWall.Setup(newWall.otherWall.preset, nod, wallOffset, nod.tile.isEdge);
                                //newWall.presetAssignDebug.Add("Preset through setup: Copied from other wall");
                            }
                            else
                            {
                                if (isAlleyBlocker)
                                {
                                    newWall.Setup(CityControls.Instance.alleyBlockWallPreset, nod, wallOffset, nod.tile.isEdge);
                                }
                                else
                                {
                                    newWall.Setup(CityControls.Instance.defaultWalls, nod, wallOffset, nod.tile.isEdge);
                                }
                            }

                            //Found a pair
                            if (newWall.otherWall != null)
                            {
                                newWall.otherWall.otherWall = newWall; //Set in other

                                //Parent ranking:
                                bool isParentRoom = false;

                                //If the other room is null space
                                if (newWall.node.room.roomType != InteriorControls.Instance.nullRoomType && newWall.otherWall.node.room.roomType == InteriorControls.Instance.nullRoomType)
                                {
                                    isParentRoom = true;
                                }
                                //If neither are null space
                                else if (newWall.node.room.roomType != InteriorControls.Instance.nullRoomType && newWall.otherWall.node.room.roomType != InteriorControls.Instance.nullRoomType)
                                {
                                    if (newWall.node.room.roomType.cyclePriority > newWall.otherWall.node.room.roomType.cyclePriority)
                                    {
                                        isParentRoom = true; //If the priority is higher
                                    }
                                    //If priorities are the same
                                    else if (newWall.node.room.roomType.cyclePriority == newWall.otherWall.node.room.roomType.cyclePriority)
                                    {
                                        if (newWall.node.room.roomFloorID > newWall.otherWall.node.room.roomFloorID) isParentRoom = true; //Now onto meaningless stats just to decide a constant parent
                                    }
                                }

                                if (isParentRoom)
                                {
                                    newWall.parentWall = newWall;
                                    newWall.childWall = newWall.otherWall;

                                    newWall.otherWall.parentWall = newWall;
                                    newWall.otherWall.childWall = newWall.otherWall;
                                }
                                else
                                {
                                    newWall.parentWall = newWall.otherWall;
                                    newWall.childWall = newWall;

                                    newWall.otherWall.parentWall = newWall.otherWall;
                                    newWall.otherWall.childWall = newWall;
                                }

                                //Copy the preset from the parent to both walls (parent wall wins)
                                if (newWall.childWall.preset != newWall.parentWall.preset)
                                {
                                    Game.Log("Copy preset from parent wall...");
                                    newWall.childWall.SetDoorPairPreset(newWall.parentWall.preset);
                                    //newWall.childWall.presetAssignDebug.Add("Preset copied from parent wall");
                                }

                                //Update debug
                                //newWall.childName = newWall.childWall.name;
                                //newWall.parentName = newWall.parentWall.name;
                                //newWall.otherName = newWall.otherWall.name;

                                //newWall.otherWall.childName = newWall.childWall.name;
                                //newWall.otherWall.parentName = newWall.parentWall.name;
                                //newWall.otherWall.otherName = newWall.otherWall.otherWall.name;

                                //Building exterior setup
                                if (newWall.node.building != null && newWall.otherWall.node.building == null)
                                {
                                    if (newWall.node.building.preset.defaultExteriorWallMaterial.Count > 0)
                                    {
                                        newWall.otherWall.separateWall = true;
                                    }
                                }
                                else if (newWall.node.building == null && newWall.otherWall.node.building != null)
                                {
                                    if (newWall.otherWall.node.building.preset.defaultExteriorWallMaterial.Count > 0)
                                    {
                                        newWall.separateWall = true;
                                    }
                                }
                            }
                            else
                            {

                            }
                        }
                        else
                        {
                            //If this already exists then remove it from 'allwalls'
                            allWalls.Remove(wallEx);
                        }
                    }
                }
                else
                {
                    //If no tile exists here, this must be an edge tile and this direction faces nothing...
                }
            }
        }

        //Remove all unwanted walls
        for (int i = 0; i < allWalls.Count; i++)
        {
            allWalls[i].RemoveWall();

            //Destroy(allWalls[i].gameObject);
            allWalls.RemoveAt(i);
            i--;
        }

        //Search for instances of 3x walls in a row so we can use one object
        foreach (NewTile tile in tiles)
        {
            //Scan for 3x in a row horizonal
            for (int i = 0; i < CityControls.Instance.nodeMultiplier; i++)
            {
                //List of walls horizonally in a row
                List<NewWall> wallsAbove = new List<NewWall>();
                List<NewWall> wallsBelow = new List<NewWall>();

                //List of wall vertically in a column
                List<NewWall> wallsLeft = new List<NewWall>();
                List<NewWall> wallsRight = new List<NewWall>();

                for (int u = 0; u < CityControls.Instance.nodeMultiplier; u++)
                {
                    //Check horizonal
                    Vector2 check = new Vector2(u, i);

                    //Find this node
                    NewNode node = tile.nodes.Find(item => item.localTileCoord == check);

                    if (node != null)
                    {
                        //Wall above
                        NewWall wallAbove = node.walls.Find(item => item.wallOffset.x == 0 && item.wallOffset.y > 0 && item.preset.optimizeSections);
                        if (wallAbove != null) wallsAbove.Add(wallAbove);

                        //Wall below
                        NewWall wallBelow = node.walls.Find(item => item.wallOffset.x == 0 && item.wallOffset.y < 0 && item.preset.optimizeSections);
                        if (wallBelow != null) wallsBelow.Add(wallBelow);
                    }

                    //Check vertical
                    check = new Vector2(i, u);

                    //Find this node
                    node = tile.nodes.Find(item => item.localTileCoord == check);

                    if (node != null)
                    {
                        //Wall left
                        NewWall wallLeft = node.walls.Find(item => item.wallOffset.x < 0 && item.wallOffset.y == 0 && item.preset.optimizeSections);
                        if (wallLeft != null) wallsLeft.Add(wallLeft);

                        //Wall right
                        NewWall wallRight = node.walls.Find(item => item.wallOffset.x > 0 && item.wallOffset.y == 0 && item.preset.optimizeSections);
                        if (wallRight != null) wallsRight.Add(wallRight);
                    }
                }

                //Set wall optimizations
                List<List<NewWall>> listOfLists = new List<List<NewWall>>();
                listOfLists.Add(wallsAbove);
                listOfLists.Add(wallsBelow);
                listOfLists.Add(wallsLeft);
                listOfLists.Add(wallsRight);

                foreach (List<NewWall> wallList in listOfLists)
                {
                    //There are 3 walls in a line...
                    if (wallList.Count >= CityControls.Instance.nodeMultiplier)
                    {
                        //The walls all belong to the same room
                        if (wallList[0].node.room == wallList[1].node.room && wallList[0].node.room == wallList[2].node.room)
                        {
                            ////The walls all share the same material group
                            //if (wallList[0].wallMaterial == wallList[1].wallMaterial && wallList[0].wallMaterial == wallList[2].wallMaterial)
                            //{
                            //    //The walls all share the same material key
                            //    if (wallList[0].wallMatKey.Equals(wallList[1].wallMatKey) && wallList[0].wallMatKey.Equals(wallList[2].wallMatKey))
                            //    {
                            for (int l = 0; l < wallList.Count; l++)
                            {
                                if (l == Mathf.FloorToInt(CityControls.Instance.nodeMultiplier * 0.5f))
                                {
                                    wallList[l].optimizationOverride = false;
                                    wallList[l].optimizationAnchor = true;
                                }
                                else
                                {
                                    wallList[l].optimizationOverride = true;
                                    wallList[l].optimizationAnchor = false;
                                }
                            }
                            //    }
                            //}
                        }
                    }
                }
            }
        }
    }

    //Refresh corner data following wall refresh
    public void LoadCornersRoom(NewRoom room)
    {
        if (room == null)
        {
            Game.Log("room is missing");
            return;
        }

        foreach (NewNode node in room.nodes)
        {
            foreach (NewWall wall in node.walls)
            {
                wall.SpawnCorner(false);
            }
        }
    }

    public void GenerateAddressLayout(NewAddress ad)
    {
        string randomSeed;

        if (SessionData.Instance.isFloorEdit)
        {
            randomSeed = Toolbox.Instance.GenerateUniqueID();
        }
        else
        {
            randomSeed = CityData.Instance.seed + ad.id * 2 + ad.building.cityTile.cityCoord.ToString() + ad.id;
        }

        GameObject newDebugParent = null;

        ResetLayout(ad, out newDebugParent);

        //Gather list of edge nodes for later use
        //Pick a starting tile (must be on the edge of the address as to not create awkward spaces)
        List<NewNode> edgeNodes = new List<NewNode>();

        //List of address entrances
        List<NewNode> entranceNodes = new List<NewNode>();
        List<NewNode> mainEntranceNodes = new List<NewNode>(); //If one of the entrances connects to outside the building (non lobby), then this is the only one that matters...

        foreach (NewNode nodes in ad.nullRoom.nodes)
        {
            nodes.preventEntrances = new List<Vector2>();

            //Check 4 directions from tile
            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
            {
                Vector2Int check = nodes.floorCoord + v2;

                if (ad.floor.nodeMap.ContainsKey(check))
                {
                    //Different address
                    if (ad.floor.nodeMap[check].gameLocation != nodes.gameLocation)
                    {
                        edgeNodes.Add(nodes);

                        //Find entrance nodes
                        foreach (NewWall wall in nodes.walls)
                        {
                            //This wall child or parent must be from a different address
                            if (wall.parentWall.node.gameLocation != ad || wall.childWall.node.gameLocation != ad)
                            {
                                if (wall.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
                                {
                                    entranceNodes.Add(nodes); //This contains all entrance nodes

                                    if (wall.parentWall.node.isOutside || wall.childWall.node.isOutside)
                                    {
                                        //If one entrance connects to a location outside the building, use this as the only 'main' entrance.
                                        //Node exist; add
                                        if (mainEntranceNodes.Count <= 0)
                                        {
                                            mainEntranceNodes.Add(nodes);
                                        }
                                        //The existing nodes connect to off-building; skip
                                        else
                                        {
                                            //Is a configured main entrance: Clear existing and add
                                            if (wall.parentWall.node.tile.isMainEntrance || wall.childWall.node.tile.isMainEntrance)
                                            {
                                                mainEntranceNodes.Clear();
                                                mainEntranceNodes.Add(nodes);
                                            }
                                            else
                                            {
                                                bool isSecondaryEntrance = false;

                                                if (wall.parentWall.node.tile.isEntrance || wall.childWall.node.tile.isEntrance)
                                                {
                                                    isSecondaryEntrance = true;
                                                }

                                                bool existingMainEntrance = false;
                                                bool existingSecondaryEntrance = false;

                                                foreach (NewNode n in mainEntranceNodes)
                                                {
                                                    foreach (NewWall w in n.walls)
                                                    {
                                                        if (w.parentWall.node.tile.isMainEntrance || w.childWall.node.tile.isMainEntrance)
                                                        {
                                                            existingMainEntrance = true;
                                                            break;
                                                        }
                                                        else if (w.parentWall.node.tile.isEntrance || w.childWall.node.tile.isEntrance)
                                                        {
                                                            existingSecondaryEntrance = true;
                                                        }
                                                    }

                                                    if (existingMainEntrance) break;
                                                }

                                                //If no main entrance exists...
                                                if (!existingMainEntrance)
                                                {
                                                    //This is secondary, so overwrite
                                                    if (isSecondaryEntrance)
                                                    {
                                                        mainEntranceNodes.Clear();
                                                        mainEntranceNodes.Add(nodes);
                                                    }
                                                    //No main or secondary, so add
                                                    else if (!existingSecondaryEntrance)
                                                    {
                                                        mainEntranceNodes.Add(nodes);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //List of free & assigned nodes
        List<NewNode> allNodes = new List<NewNode>(ad.nullRoom.nodes);

        //Create forced rooms
        CreateForcedRooms(ad);

        //Sorted layout list
        List<RoomTypePreset> roomList = new List<RoomTypePreset>(ad.preset.roomLayout);
        roomList.Sort((p1, p2) => p2.cyclePriority.CompareTo(p1.cyclePriority)); //Using P2 first gives highest first

        //Get the list of room settings and order them by priority...
        List<RoomTypePreset> layout = new List<RoomTypePreset>();

        for (int i = 0; i < 5; i++)
        {
            foreach (RoomTypePreset config in roomList)
            {
                if (config.maximumRoomTypesPerAddress > i)
                {
                    layout.Add(config);
                }
            }
        }

        //Hallway: Not always needed, but useful for larger addresses
        NewRoom newHall = null;

        //Find the most remote tiles in terms of +/- x and +/1 y difference
        Vector2 bottomLeft = new Vector2(0, 0);
        NewNode bottomLeftNode = null;

        Vector2 upperLeft = new Vector2(0, 0);
        NewNode upperLeftNode = null;

        Vector2 bottomRight = new Vector2(0, 0);
        NewNode bottomRightNode = null;

        Vector2 upperRight = new Vector2(0, 0);
        NewNode upperRightNode = null;

        //First, draw a hallway from the entrance to near the end of the address space. Calculate the nodes that need to be connected...
        Dictionary<NewNode, NewNode> remoteNodes = new Dictionary<NewNode, NewNode>(); //The far node and it's closest entrance node
        int distanceThreshold = ad.preset.hallwayDistanceThreshold; //How far away can a node in this address be from an entrance

        //Figure out if we need to start with a hallway- are rooms too far away from the door?
        //Do this in a random order
        List<NewNode> unassignedNodesRandom = new List<NewNode>(ad.nullRoom.nodes);

        while (unassignedNodesRandom.Count > 0)
        {
            NewNode node = unassignedNodesRandom[Toolbox.Instance.RandContained(0, unassignedNodesRandom.Count, randomSeed, out randomSeed)];

            float distanceFromEntrance = 0f;
            Vector2 offset = Vector2.zero;
            NewNode nearestEntrance = null;

            foreach (NewNode entrance in entranceNodes)
            {
                float dist = Vector2.Distance(node.floorCoord, entrance.floorCoord);

                if (dist > distanceFromEntrance)
                {
                    distanceFromEntrance = dist;
                    offset = new Vector2(node.localTileCoord.x - entrance.localTileCoord.x, node.localTileCoord.y - entrance.localTileCoord.y); //+ means to the right, - means to the left, + means above, - means below
                    nearestEntrance = entrance;
                }
            }

            //Threshold
            if (distanceFromEntrance > distanceThreshold)
            {
                bool addToOffsets = false;

                if (offset.x <= bottomLeft.x && offset.y <= bottomLeft.y)
                {
                    if (bottomLeftNode != null) remoteNodes.Remove(bottomLeftNode);
                    addToOffsets = true;
                    bottomLeft = offset;
                    bottomLeftNode = node;
                }

                if (offset.x <= upperLeft.x && offset.y >= upperLeft.y)
                {
                    if (upperLeftNode != null) remoteNodes.Remove(upperLeftNode);
                    addToOffsets = true;
                    upperLeft = offset;
                    upperLeftNode = node;
                }

                if (offset.x >= bottomRight.x && offset.y <= bottomRight.y)
                {
                    if (bottomRightNode != null) remoteNodes.Remove(bottomRightNode);
                    addToOffsets = true;
                    bottomRight = offset;
                    bottomRightNode = node;
                }

                if (offset.x >= upperRight.x && offset.y >= upperRight.y)
                {
                    if (upperRightNode != null) remoteNodes.Remove(upperRightNode);
                    addToOffsets = true;
                    upperRight = offset;
                    upperRightNode = node;
                }

                if (addToOffsets) remoteNodes.Add(node, nearestEntrance);
            }

            unassignedNodesRandom.Remove(node);
        }

        //Evaluate findings: Are there nodes that might have trouble accessing the entrance?
        if (ad.preset.requiresHallway && remoteNodes.Count > 0)
        {
            //Game.Log("Found " + remoteNodes.Count + " remote nodes...");

            foreach (KeyValuePair<NewNode, NewNode> pair in remoteNodes)
            {
                //Game.Log("Remote node @ " + pair.Key.floorCoord);

                //Make sure the distance to the nearest corridoor is greater than the threshold, otherwise skip this...
                if (newHall != null)
                {
                    bool isWithinDist = false;

                    foreach (NewNode node in newHall.nodes)
                    {
                        float dist = Vector2.Distance(node.floorCoord, pair.Key.floorCoord);

                        if (dist < distanceThreshold)
                        {
                            //Game.Log("Hallway is now within threshold (" + dist + "), skipping this... " + pair.Key.floorCoord + "/" + node.floorCoord);
                            isWithinDist = true;
                            break;
                        }
                    }

                    if (isWithinDist) continue;
                }

                //Build a list of possible nodes that are closer to the entrance than this node, but also within the threshold distance to the node...
                NewNode closestNode = null;
                float furthestDist = -1f;
                //List<NewNode> possibleCloseNodes = new List<NewNode>();

                foreach (NewNode node in ad.nullRoom.nodes)
                {
                    float distanceToThis = Vector2.Distance(node.floorCoord, pair.Key.floorCoord);

                    if (distanceToThis <= distanceThreshold - 1)
                    {
                        if (distanceToThis > furthestDist)
                        {
                            closestNode = node;
                            furthestDist = distanceToThis;
                        }

                        //possibleCloseNodes.Add(node);
                    }
                }

                //Game.Log("Found " + possibleCloseNodes.Count + " possible close nodes...");

                if (closestNode != null)
                {
                    List<NewNode> hallwayPath = HallwayPathfind(pair.Value, closestNode, ad); //Pathfind between found node and it's nearest entrance

                    if (hallwayPath != null && hallwayPath.Count > 0)
                    {
                        //Create hallway
                        if (newHall == null)
                        {
                            GameObject newObj2 = Instantiate(PrefabControls.Instance.room, ad.transform);
                            newHall = newObj2.GetComponent<NewRoom>();
                            newHall.SetupAll(ad, ad.preset.hallway);

                            //Create debug object
                            if (SessionData.Instance.isFloorEdit)
                            {
                                //Setup debug object for this room config
                                GameObject newConfigAttempt = new GameObject();
                                newConfigAttempt.transform.SetParent(newDebugParent.transform);
                                newConfigAttempt.transform.localPosition = Vector3.zero;
                                newConfigAttempt.name = "Hallway";

                                //Create a new debug attempt
                                GameObject newDebugAttempt = Instantiate(PrefabControls.Instance.debugAttemptObject, newConfigAttempt.transform);
                                GenerationDebugController debug = newDebugAttempt.GetComponent<GenerationDebugController>();
                                debug.Setup("Generated Hallway", ad.preset.hallway.roomType);
                                debug.attemptedValidNodes = hallwayPath;
                                Game.Log("Hallways has been created using pathfinding from " + closestNode.name + " to " + pair.Value.name);
                            }
                        }

                        //Game.Log("Add to hallway...");

                        //Add nodes
                        foreach (NewNode node in hallwayPath)
                        {
                            newHall.AddNewNode(node);
                            //addressSelection.protectedNodes.Add(node);
                        }
                    }
                }
            }
        }

        //Start generation process...
        for (int i = 0; i < layout.Count; i++)
        {
            RoomTypePreset room = layout[i];

            //Does this address contain a stairwell or elevator? If so make this room first...
            NewNode stairwayNode = ad.nodes.Find(item => (item.tile.isStairwell || item.tile.isInvertedStairwell) && item.room == ad.nullRoom);

            if (stairwayNode != null)
            {
                NewTile stairTile = stairwayNode.tile;

                //Create room
                GameObject newStairwayObj = Instantiate(PrefabControls.Instance.room, ad.transform);
                NewRoom stairwellRoom = newStairwayObj.GetComponent<NewRoom>();
                stairwellRoom.SetupLayoutOnly(ad, room);

                //Add the 3 tiles at the front of the stairwell...
                int rot = 0;

                if (stairTile.isInvertedStairwell) rot = stairTile.elevatorRotation;
                else if (stairTile.isStairwell) rot = stairTile.stairwellRotation;

                foreach (NewNode searchNode in stairTile.nodes)
                {
                    stairwellRoom.AddNewNode(searchNode);
                    ad.protectedNodes.Add(searchNode);

                    foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                    {
                        NewNode foundNode = null;

                        if (GetAdjacentNode(searchNode, v2, out foundNode))
                        {
                            if (stairTile.nodes.Contains(foundNode)) continue; //Skip nodes in the tile area

                            //Is this not at the front of the stairs? If not this is invalid
                            if (rot == 0)
                            {
                                if (v2.y > 0)
                                {
                                    stairwellRoom.AddNewNode(foundNode);
                                    ad.protectedNodes.Add(foundNode);

                                    //Stop other walls from becoming an entrance
                                    foreach (Vector2Int pre in CityData.Instance.offsetArrayX4)
                                    {
                                        if (pre != v2)
                                        {
                                            if (foundNode.preventEntrances == null) foundNode.preventEntrances = new List<Vector2>();
                                            foundNode.preventEntrances.Add((Vector2)pre / 2f);
                                        }
                                    }
                                }
                            }
                            else if (rot == 90)
                            {
                                if (v2.x > 0)
                                {
                                    stairwellRoom.AddNewNode(foundNode);
                                    ad.protectedNodes.Add(foundNode);

                                    //Stop other walls from becoming an entrance
                                    foreach (Vector2Int pre in CityData.Instance.offsetArrayX4)
                                    {
                                        if (pre != v2)
                                        {
                                            if (foundNode.preventEntrances == null) foundNode.preventEntrances = new List<Vector2>();
                                            foundNode.preventEntrances.Add((Vector2)pre / 2f);
                                        }
                                    }
                                }
                            }
                            else if (rot == 180)
                            {
                                if (v2.y < 0)
                                {
                                    stairwellRoom.AddNewNode(foundNode);
                                    ad.protectedNodes.Add(foundNode);

                                    //Stop other walls from becoming an entrance
                                    foreach (Vector2Int pre in CityData.Instance.offsetArrayX4)
                                    {
                                        if (pre != v2)
                                        {
                                            if (foundNode.preventEntrances == null) foundNode.preventEntrances = new List<Vector2>();
                                            foundNode.preventEntrances.Add((Vector2)pre / 2f);
                                        }
                                    }
                                }
                            }
                            else if (rot == 270)
                            {
                                if (v2.x < 0)
                                {
                                    stairwellRoom.AddNewNode(foundNode);
                                    ad.protectedNodes.Add(foundNode);

                                    //Stop other walls from becoming an entrance
                                    foreach (Vector2Int pre in CityData.Instance.offsetArrayX4)
                                    {
                                        if (pre != v2)
                                        {
                                            if (foundNode.preventEntrances == null) foundNode.preventEntrances = new List<Vector2>();
                                            foundNode.preventEntrances.Add((Vector2)pre / 2f);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Chance of skipping
            if (allNodes.Count < room.minimumAddressSize) continue;
            if (Toolbox.Instance.RandContained(0f, 1f, randomSeed, out randomSeed) > room.chance) continue;

            ////Stop checking if there are no free nodes and not many to overwrite...
            //int potentialNodes = allNodes.Count;

            //foreach(NewRoom r in ad.rooms)
            //{
            //    if (r.preset.room == RoomConfiguration.RoomType.nullSpace) continue;

            //    //Add nodes over minimum
            //    int minimum = Mathf.RoundToInt(r.preset.minimumRoomAreaShape.x * r.preset.minimumRoomAreaShape.y);
            //    potentialNodes += r.nodes.Count - minimum;
            //}

            //Try and place this within available space...
            List<PossibleRoomLocation> possibilities = GetPossibleRoomLocations(ad, room, allNodes, entranceNodes, mainEntranceNodes, edgeNodes, newDebugParent.transform);

            //If no possibilities, continue
            if (possibilities.Count <= 0)
            {
                //if (room.canBeOpenPlan && room.openPlanRoom != null)
                //{
                //    //Are there no rooms of this type that exist?
                //    if (!ad.rooms.Exists(item => item.preset.roomType == room))
                //    {
                //        NewRoom roomToAddTo = ad.rooms.Find(item => item.preset.roomType == room.openPlanRoom);

                //        if (roomToAddTo != null)
                //        {
                //            //Game.Log("Added " + room.room.ToString() + " to " + roomToAddTo.name + " in open plan format");
                //            roomToAddTo.AddOpenPlanElement(room);
                //            continue;
                //        }
                //        else continue;
                //    }
                //    else continue;
                //}
                //else continue;

                continue;
            }

            //Pick a random possible placement
            possibilities.Sort();
            possibilities.Reverse(); //Highest rank first
            PossibleRoomLocation chosenPlacement = possibilities[0];

            //Update debug display
            chosenPlacement.debugScript.name = "*" + chosenPlacement.debugScript.name;
            chosenPlacement.debugScript.executed = true;
            chosenPlacement.debugScript.gameObject.transform.SetAsFirstSibling();

            //Create room
            GameObject newObj = Instantiate(PrefabControls.Instance.room, ad.transform);
            NewRoom newRoom = newObj.GetComponent<NewRoom>();
            newRoom.SetupLayoutOnly(ad, room);
            newRoom.debugController = chosenPlacement.debugScript;

            //Apply debug info
            //newRoom.randomRanking = chosenPlacement.randomRanking;
            //newRoom.exteriorWallRanking = chosenPlacement.exteriorWallRanking;
            //newRoom.exteriorWindowRanking = chosenPlacement.exteriorWindowRanking;
            //newRoom.floorSpaceRanking = chosenPlacement.floorSpaceRanking;
            //newRoom.uniformShapeRanking = chosenPlacement.uniformShapeRanking;
            newRoom.overrideData = chosenPlacement.overrideRankingData;
            //newRoom.totalRanking = chosenPlacement.ranking;

            List<NewRoom> overridenRooms = new List<NewRoom>();

            foreach (NewNode node in chosenPlacement.nodes)
            {
                //If overwriting, lower the overwrite limit by 1
                if (node.room.preset != CityControls.Instance.nullDefaultRoom)
                {
                    node.overwriteLimit--;
                }
                //Otherwise set the overwrite limit to this room's configuration
                else
                {
                    node.overwriteLimit = room.overwriteLimit;
                }

                newRoom.AddNewNode(node);
            }

            //Calculate override penalties
            //foreach (OverrideData data in chosenPlacement.overrideRankingData)
            //{
            //    data.room.overridenApplication += data.overridingPenalty;
            //    data.room.overridenTotal = data.room.totalRanking + data.room.overridenApplication;
            //}

            //Create/add to hallway if needed
            if (chosenPlacement.requiredHallway != null)
            {
                if (chosenPlacement.requiredHallway.Count > 0)
                {
                    foreach (NewNode hallNode in chosenPlacement.requiredHallway)
                    {
                        if (hallNode.room != newRoom)
                        {
                            //Create hallway
                            if (newHall == null)
                            {
                                GameObject newObj2 = Instantiate(PrefabControls.Instance.room, ad.transform);
                                newHall = newObj2.GetComponent<NewRoom>();
                                newHall.SetupAll(ad, ad.preset.hallway);

                                //Create debug object
                                if (SessionData.Instance.isFloorEdit)
                                {
                                    //Setup debug object for this room config
                                    GameObject newConfigAttempt = new GameObject();
                                    newConfigAttempt.transform.SetParent(newDebugParent.transform);
                                    newConfigAttempt.transform.localPosition = Vector3.zero;
                                    newConfigAttempt.name = "Hallway (" + room.ToString() + ")";

                                    //Create a new debug attempt
                                    GameObject newDebugAttempt = Instantiate(PrefabControls.Instance.debugAttemptObject, newConfigAttempt.transform);
                                    GenerationDebugController debug = newDebugAttempt.GetComponent<GenerationDebugController>();
                                    debug.Setup("Generated Hallway", ad.preset.hallway.roomType);
                                    debug.attemptedValidNodes = chosenPlacement.requiredHallway;
                                    Game.Log("Hallways has been created using pathfinding");
                                }
                            }

                            if(newHall != null)
                            {
                                newHall.AddNewNode(hallNode);
                                ad.protectedNodes.Add(hallNode);
                            }
                        }
                    }

                    //If hallway is too small, just make it part of this room
                    if (newHall.nodes.Count <= 2)
                    {
                        for (int l = 0; l < chosenPlacement.requiredHallway.Count; l++)
                        {
                            newRoom.AddNewNode(chosenPlacement.requiredHallway[l]);
                        }

                        ad.RemoveRoom(newHall);
                    }
                }
            }
        }

        //Cycle rooms to check for expansion into null space
        List<PossibleNullExpansion> possNullExpansion = new List<PossibleNullExpansion>();

        //Cycle null nodes in this address
        foreach (NewNode nullNode in ad.nullRoom.nodes)
        {
            if (ad.protectedNodes.Contains(nullNode)) continue; //Skip if protected

            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
            {
                Vector2Int scan = nullNode.floorCoord + v2;

                //Found node: Adjacent to a null node from this room that is of existing room.
                NewNode foundNodeAdjacentToNull = null;

                if (ad.floor.nodeMap.TryGetValue(scan, out foundNodeAdjacentToNull))
                {
                    //Different address
                    if (foundNodeAdjacentToNull.gameLocation == null || foundNodeAdjacentToNull.gameLocation != ad) continue; //Skip if different address
                    if (foundNodeAdjacentToNull.room == ad.nullRoom) continue; //Skip if part of the null room
                    if (!foundNodeAdjacentToNull.room.roomType.expandIntoNull) continue; //Skip if can't expand into null
                    if (foundNodeAdjacentToNull.tile.isStairwell || foundNodeAdjacentToNull.tile.isInvertedStairwell) continue; //Cannot expand stairs or elevator

                    List<NewNode> openSet = new List<NewNode>();
                    List<NewNode> closedSet = new List<NewNode>(); //List of nodes to expand into
                    openSet.Add(nullNode);

                    int safety = 24;

                    //Scan for connecting null space
                    while (openSet.Count > 0 && safety > 0)
                    {
                        NewNode expandIntoNode = openSet[0]; //Adjacent to a null node from this room that is of existing room.

                        foreach (Vector2Int scanVec in CityData.Instance.offsetArrayX4)
                        {
                            Vector2Int scanForOtherNull = expandIntoNode.floorCoord + scanVec;
                            NewNode foundOtherNull = null; //Found a null next to the adjacent

                            if (ad.floor.nodeMap.TryGetValue(scanForOtherNull, out foundOtherNull))
                            {
                                //Matches room: Found adjacent null
                                if (foundOtherNull.room == ad.nullRoom && foundOtherNull.gameLocation == ad)
                                {
                                    //This isn't in the closed set already...
                                    if (!closedSet.Contains(foundOtherNull))
                                    {
                                        //Can also access original room
                                        foreach (Vector2Int scanVec2 in CityData.Instance.offsetArrayX4)
                                        {
                                            Vector2Int scanForOriginal = foundOtherNull.floorCoord + scanVec2;

                                            NewNode findTheRoom = null;

                                            if (ad.floor.nodeMap.TryGetValue(scanForOriginal, out findTheRoom))
                                            {
                                                if (findTheRoom.room == foundNodeAdjacentToNull.room)
                                                {
                                                    if (!openSet.Contains(foundOtherNull) && !closedSet.Contains(foundOtherNull))
                                                    {
                                                        openSet.Add(foundOtherNull);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        closedSet.Add(expandIntoNode);
                        openSet.RemoveAt(0);
                        safety--;
                    }

                    //Is this enough?
                    if (closedSet.Count >= foundNodeAdjacentToNull.room.roomType.expandIntoNullAdjacencyMinimum)
                    {
                        PossibleNullExpansion newNullExp = new PossibleNullExpansion();
                        newNullExp.nodesToExpand = closedSet;
                        newNullExp.addToRoom = foundNodeAdjacentToNull.room;

                        //Ranking is number + found room priority (higher is better)
                        newNullExp.ranking = closedSet.Count + foundNodeAdjacentToNull.room.roomType.cyclePriority;
                        possNullExpansion.Add(newNullExp);
                    }
                }
            }
        }

        //Rank null expansion possibilities
        possNullExpansion.Sort();
        possNullExpansion.Reverse(); //Highest first

        for (int i = 0; i < possNullExpansion.Count; i++)
        {
            //If this is still a valid addition...
            List<NewNode> nodesToAdd = possNullExpansion[i].nodesToExpand.FindAll(item => item.room == ad.nullRoom);

            if (nodesToAdd.Count >= possNullExpansion[i].addToRoom.roomType.expandIntoNullAdjacencyMinimum)
            {
                //Game.Log("Null expansion chosen");

                for (int u = 0; u < nodesToAdd.Count; u++)
                {
                    possNullExpansion[i].addToRoom.AddNewNode(nodesToAdd[u]);
                }
            }
        }

        //Search for hallways that have been split in half, if there is any then treat them as seperate rooms
        List<NewRoom> hallways = new List<NewRoom>();

        if (newHall != null)
        {
            hallways = ConvertSplitRoom(ref newHall.nodes, ad);
        }

        //Refresh walls immediately before doing the following...
        UpdateFloorCeilingFloor(ad.floor);
        UpdateWallsFloor(ad.floor);

        //Cycle walls to search for doorways to create
        foreach (NewRoom room in ad.rooms)
        {
            if (room.debugController != null) room.debugController.Log("Begin primary entrances debug...");
            if (room.roomType == InteriorControls.Instance.nullRoomType || room.preset == ad.preset.hallway)
            {
                if (room.debugController != null) room.debugController.Log("Null space or hallway, this needs no entrances...");
                continue;
            }
            else if (room.roomType.mustConnectWithEntrance && hallways.Count <= 0)
            {
                if (room.debugController != null) room.debugController.Log("This must connect with entrance + there is no hallways: Doorway will open onto this...");
                continue; //Doorway will open onto this...
            }
            if (room.entrances.Count >= room.roomType.maxDoors || room.roomType.forceNoDoors)
            {
                if (room.debugController != null) room.debugController.Log("Maximum number of doors reached: " + room.entrances.Count + "/" + room.roomType.maxDoors);
                continue; //Skip if max entrances reached
            }

            List<PossibleDoorwayLocation> possibleDoorways = new List<PossibleDoorwayLocation>();

            foreach (NewNode node in room.nodes)
            {
                if (node.tile.isStairwell || node.tile.isInvertedStairwell) continue; //Cannot link from a tile with elevator/stairs

                //Cycle walls
                foreach (NewWall wall in node.walls)
                {
                    if (wall.otherWall == null) continue;
                    if (wall.preset.sectionClass != DoorPairPreset.WallSectionClass.wall) continue; //Must be a solid wall only...
                    if (wall.preventEntrance || wall.otherWall.preventEntrance) continue;
                    if (wall.otherWall.node.room.entrances.Count >= wall.otherWall.node.room.roomType.maxDoors) continue; //Skip if other room has max entrances
                    if (wall.otherWall.node.room.roomType.forceNoDoors || wall.node.room.roomType.forceNoDoors) continue;

                    //Does this wall border another room but is part of the same address?
                    if (wall.otherWall.node.room.gameLocation == ad && wall.otherWall.node.room != room && wall.otherWall.node.room.roomType != InteriorControls.Instance.nullRoomType)
                    {
                        //Can't lead to staircase or elevator
                        if (wall.otherWall.node.tile.isStairwell || wall.otherWall.node.tile.isInvertedStairwell) continue;

                        //Does this wall border a required room?
                        if (room.roomType.mustAdjoinRooms.Count <= 0 || room.roomType.mustAdjoinRooms.Contains(wall.otherWall.node.room.roomType))
                        {
                            PossibleDoorwayLocation newLoc = new PossibleDoorwayLocation();
                            newLoc.wall = wall;
                            newLoc.ranking = (5 - room.roomType.mustAdjoinRooms.FindIndex(item => item == wall.otherWall.node.room.roomType)) * 2 + Toolbox.Instance.RandContained(0f, 0.1f, randomSeed, out randomSeed);

                            List<NewWall> doorwaysHereAlready = node.walls.FindAll(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance || item.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge);
                            doorwaysHereAlready.AddRange(wall.otherWall.node.walls.FindAll(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance || item.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge));

                            //Favour ends of corridoors, so rank + 0.2 per wall
                            newLoc.ranking += wall.otherWall.node.walls.Count * 0.2f;

                            //Favour closest to entrance
                            float distToEntrance = 999f;

                            foreach (NewNode ent in entranceNodes)
                            {
                                float dist = Vector2.Distance(ent.position, wall.position);
                                distToEntrance = Mathf.Min(distToEntrance, dist);
                            }

                            newLoc.ranking -= distToEntrance;

                            //Lower ranking by 1 per existing doorway on this tile
                            //Except opposites which only carry a penalty of 0.5f
                            foreach (NewWall doorway in doorwaysHereAlready)
                            {
                                if (doorway.wallOffset == wall.wallOffset * -1f)
                                {
                                    newLoc.ranking -= 0.5f;
                                }
                                else
                                {
                                    newLoc.ranking -= 1f;
                                    newLoc.requireFlatDoorway = true;
                                }
                            }

                            possibleDoorways.Add(newLoc);
                        }
                    }
                }
            }

            if (room.debugController != null) room.debugController.Log("Possible doorways count: " + possibleDoorways.Count);

            //Rank list (higher is better)
            possibleDoorways.Sort();
            possibleDoorways.Reverse();

            if (possibleDoorways.Count > 0)
            {
                if (room.debugController != null) room.debugController.Log("Picked doorway on node: " + possibleDoorways[0].wall.node.localTileCoord + " with offset of " + possibleDoorways[0].wall.wallOffset + " (" + possibleDoorways[0].wall.node.room.name + " <-> " + possibleDoorways[0].wall.otherWall.node.room.name);

                if (possibleDoorways[0].requireFlatDoorway && room.gameLocation.thisAsAddress.preset.doorwaysFlat.Count > 0)
                {
                    //Force no update as it will be updated soon anway!
                    possibleDoorways[0].wall.SetDoorPairPreset(room.gameLocation.thisAsAddress.preset.doorwaysFlat[Toolbox.Instance.RandContained(0, room.gameLocation.thisAsAddress.preset.doorwaysFlat.Count, randomSeed, out randomSeed)], false);
                }
                else
                {
                    possibleDoorways[0].wall.SetDoorPairPreset(room.gameLocation.thisAsAddress.preset.doorwaysNormal[Toolbox.Instance.RandContained(0, room.gameLocation.thisAsAddress.preset.doorwaysNormal.Count, randomSeed, out randomSeed)], false);
                }

                ad.generatedInteriorEntrances.Add(possibleDoorways[0].wall);

                //Add entrance to protected nodes if room size > 1
                if (possibleDoorways[0].wall.node.room.nodes.Count > 1 && possibleDoorways[0].wall.otherWall.node.room.nodes.Count > 1)
                {
                    ad.protectedNodes.Add(possibleDoorways[0].wall.node);
                    ad.protectedNodes.Add(possibleDoorways[0].wall.otherWall.node);
                }
            }
        }

        //Tidy up corridoor ends: Tidy ends of corridors
        //Find the relevent ends: nodes with 3x walls & no doors
        foreach (NewRoom hallway in hallways)
        {
            List<NewNode> openKeys = new List<NewNode>();
            Dictionary<NewNode, NewRoom> openedSet = new Dictionary<NewNode, NewRoom>(); //The current node and the room that will replace it

            if (hallway != null)
            {
                foreach (NewNode node in hallway.nodes)
                {
                    //End node
                    if (node.walls.Count >= 3)
                    {
                        NewRoom addTo = null;

                        //Check directions
                        for (int i = 0; i < CityData.Instance.offsetArrayX4.Length; i++)
                        {
                            //Find the replacement room
                            NewNode accessNeighbor = null;
                            Dictionary<NewRoom, int> adjacentRooms = new Dictionary<NewRoom, int>();

                            if (GetAdjacentNode(node, CityData.Instance.offsetArrayX4[i], out accessNeighbor))
                            {
                                //Same address
                                if (accessNeighbor.gameLocation == ad)
                                {
                                    //Is other: This potentially can be added to
                                    if (accessNeighbor.room.roomType.allowCorridorReplacement)
                                    {
                                        //if (accessNeighbor.room.nodes.Count < accessNeighbor.room.preset.roomSizeMaximum)
                                        //{
                                        if (adjacentRooms.ContainsKey(accessNeighbor.room))
                                        {
                                            adjacentRooms[accessNeighbor.room]++;
                                        }
                                        else
                                        {
                                            adjacentRooms.Add(accessNeighbor.room, 1);
                                        }
                                        //}
                                    }
                                }
                            }

                            if (addTo == null && adjacentRooms.Count > 0)
                            {
                                int chosenCount = -1;

                                foreach (KeyValuePair<NewRoom, int> pair in adjacentRooms)
                                {
                                    if (pair.Value > chosenCount)
                                    {
                                        addTo = pair.Key;
                                        chosenCount = pair.Value;
                                    }
                                }
                            }
                        }

                        if (addTo == null) break;

                        List<NewWall> entrancesHere = node.walls.FindAll(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance);

                        //If no entrances, then continue
                        if (entrancesHere == null || entrancesHere.Count <= 0)
                        {
                            openedSet.Add(node, addTo);
                            openKeys.Add(node);
                        }
                        //If there are entrances on this tile, are they able to open onto the replacement room? If so you can continue...
                        else if (addTo != null)
                        {
                            bool entPass = true;

                            foreach (NewWall ent in entrancesHere)
                            {
                                //Entrance can open onto this or is itself
                                if (!ent.otherWall.node.room.roomType.mustAdjoinRooms.Contains(addTo.roomType) && ent.otherWall.node.room != addTo)
                                {
                                    entPass = false;
                                    break;
                                }
                            }

                            if (entPass)
                            {
                                openedSet.Add(node, addTo);
                                openKeys.Add(node);
                            }
                        }
                    }
                }

            }

            int safe = 999;

            while (openedSet.Count > 0 && safe > 0)
            {
                NewNode current = openKeys[0];

                //Which room should this become a part of? The surrounding room with the highest priority
                NewRoom addTo = openedSet[openKeys[0]];

                //Check if this can be added
                //if (addTo.nodes.Count >= addTo.preset.roomSizeMaximum) break;

                //Would this break the corridor?
                List<NewNode> splitNodes = new List<NewNode>(hallway.nodes);
                splitNodes.Remove(current);
                if (!RoomSplitCheck(ref splitNodes, null)) break;

                //Check directions
                for (int i = 0; i < CityData.Instance.offsetArrayX4.Length; i++)
                {
                    NewNode accessNeighbor = null;

                    if (GetAdjacentNode(current, CityData.Instance.offsetArrayX4[i], out accessNeighbor))
                    {
                        //Same address
                        if (accessNeighbor.gameLocation == ad)
                        {
                            //Is not stairwell or elevator
                            if (!accessNeighbor.tile.isStairwell && !accessNeighbor.tile.isInvertedStairwell)
                            {
                                //Is hallway: Possibly add this to the open set
                                if (accessNeighbor.room.gameLocation.thisAsAddress != null && accessNeighbor.room.preset == accessNeighbor.room.gameLocation.thisAsAddress.preset.hallway)
                                {
                                    //Game.Log("Hallway node found...");
                                    List<NewWall> entrancesHere = accessNeighbor.walls.FindAll(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance);

                                    //If no entrances, then continue
                                    if (entrancesHere == null || entrancesHere.Count <= 0)
                                    {
                                        if (!openedSet.ContainsKey(accessNeighbor))
                                        {
                                            openedSet.Add(accessNeighbor, addTo);
                                            openKeys.Add(accessNeighbor);
                                        }
                                    }
                                    //If there are entrances on this tile, are they able to open onto the replacement room? If so you can continue...
                                    else if (addTo != null)
                                    {
                                        bool entPass = true;

                                        foreach (NewWall ent in entrancesHere)
                                        {
                                            if (!ent.otherWall.node.room.roomType.mustAdjoinRooms.Contains(addTo.roomType) && ent.otherWall.node.room != addTo)
                                            {
                                                entPass = false;
                                                break;
                                            }
                                        }

                                        if (entPass)
                                        {
                                            if (!openedSet.ContainsKey(accessNeighbor))
                                            {
                                                openedSet.Add(accessNeighbor, addTo);
                                                openKeys.Add(accessNeighbor);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //Add to room
                if (addTo != null)
                {
                    addTo.AddNewNode(current);
                }

                openedSet.Remove(current);
                openKeys.RemoveAt(0);

                //Is above min size
                if (hallway.nodes.Count <= 2)
                {
                    //Game.Log("Hallway minimum reached: " + newHall.nodes.Count);
                    break;
                }

                safe--;
            }
        }

        //Refresh walls immediately before doing the following...
        UpdateFloorCeilingFloor(ad.floor);
        UpdateWallsFloor(ad.floor);

        //Perform a check to see what rooms can reach the entrance
        HashSet<NewRoom> unreachableRooms = GetUnreachableRooms(entranceNodes, ad);

        //Add extra entrances: If a room can be joined then join it
        foreach (NewRoom room in ad.rooms)
        {
            if (room.roomType == InteriorControls.Instance.nullRoomType || room.roomType.forceNoDoors) continue;

            //Has this got spare doors to spawn?
            if (room.entrances.Count >= room.roomType.maxDoors && !room.roomType.allowRoomDividers && !unreachableRooms.Contains(room)) continue;

            if (room.debugController != null) room.debugController.Log("Begin secondary entrances debug...");

            List<PossibleDoorwayLocation> possibleDoorways = new List<PossibleDoorwayLocation>();

            foreach (NewNode node in room.nodes)
            {
                if (node.tile.isStairwell || node.tile.isInvertedStairwell) continue; //Tiles with stairs or elevators cannot have entrances

                //Cycle walls
                foreach (NewWall wall in node.walls)
                {
                    if (wall.otherWall == null) continue;
                    if (wall.preset.divider) continue; //Is part of a divider...
                    if (wall.node.tile.isStairwell || wall.node.tile.isInvertedStairwell) continue;
                    if (wall.otherWall.node.tile.isStairwell || wall.otherWall.node.tile.isInvertedStairwell) continue;
                    if (wall.preventEntrance || wall.otherWall.preventEntrance) continue;

                    //Only do this if the room already has entrances
                    if (room.reachableFromEntrance)
                    {
                        if (!wall.otherWall.node.room.roomType.allowRoomDividers && wall.otherWall.node.room.entrances.Count >= wall.otherWall.node.room.roomType.maxDoors) continue;
                    }

                    if (room.roomType.allowRoomDividers && wall.otherWall.node.room.roomType.allowRoomDividers)
                    {
                        if (room.roomDividers.Count >= room.roomType.maxDividers) continue;
                        if (wall.otherWall.node.room.roomDividers.Count >= wall.otherWall.node.room.roomType.maxDividers) continue;

                        if (room.roomType.onlyAllowDividersAdjoining.Count <= 0 || room.roomType.onlyAllowDividersAdjoining.Contains(wall.otherWall.node.room.roomType))
                        {
                            if (wall.otherWall.node.room.roomType.onlyAllowDividersAdjoining.Count <= 0 || room.roomType.onlyAllowDividersAdjoining.Contains(room.roomType))
                            {
                                //Only walls and other entrances allowed...
                                if (wall.preset.sectionClass != DoorPairPreset.WallSectionClass.wall && wall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance) continue;
                            }
                            else if (wall.preset.sectionClass != DoorPairPreset.WallSectionClass.wall) continue;
                        }
                        else if (wall.preset.sectionClass != DoorPairPreset.WallSectionClass.wall) continue;
                    }
                    else
                    {
                        //Only walls allowed
                        if (wall.preset.sectionClass != DoorPairPreset.WallSectionClass.wall) continue;
                    }

                    //Does this wall border another room but is part of the same address?
                    if (wall.otherWall.node.room.gameLocation == ad && wall.otherWall.node.room != room && wall.otherWall.node.room.roomType != InteriorControls.Instance.nullRoomType)
                    {
                        //Do both rooms allow dividers?
                        List<NewWall> dividerWalls = new List<NewWall>();

                        if (room.roomType.allowRoomDividers && wall.otherWall.node.room.roomType.allowRoomDividers)
                        {
                            if(room.roomDividers.Count < room.roomType.maxDividers && wall.otherWall.node.room.roomDividers.Count < wall.otherWall.node.room.roomType.maxDividers)
                            {
                                if (room.roomType.onlyAllowDividersAdjoining.Count <= 0 || room.roomType.onlyAllowDividersAdjoining.Contains(wall.otherWall.node.room.roomType))
                                {
                                    if (wall.otherWall.node.room.roomType.onlyAllowDividersAdjoining.Count <= 0 || room.roomType.onlyAllowDividersAdjoining.Contains(room.roomType))
                                    {
                                        dividerWalls.Add(wall);

                                        //Expand to find other adjacent walls with relevent properties
                                        foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                                        {
                                            NewNode foundNode = null;

                                            if (GetAdjacentNode(node, v2, out foundNode))
                                            {
                                                if (foundNode.room == node.room)
                                                {
                                                    //Check the same offset for a wall
                                                    NewWall foundWall = foundNode.walls.Find(item => item.wallOffset == wall.wallOffset && !item.preset.divider && !item.node.tile.isStairwell && !item.node.tile.isInvertedStairwell && !item.otherWall.node.tile.isStairwell && !item.otherWall.node.tile.isInvertedStairwell);

                                                    if (foundWall != null)
                                                    {
                                                        //Does this wall border the same rooms?
                                                        if (foundWall.otherWall.node.room == wall.otherWall.node.room || foundWall.node.room == wall.otherWall.node.room)
                                                        {
                                                            if (!foundWall.preventEntrance && !foundWall.otherWall.preventEntrance)
                                                            {
                                                                if (!dividerWalls.Contains(foundWall))
                                                                {
                                                                    dividerWalls.Add(foundWall);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //Can the adjacent room support enough doors? (Override this if no entrances)
                        if (room.reachableFromEntrance)
                        {
                            if (wall.otherWall.node.room.entrances.Count >= wall.otherWall.node.room.roomType.maxDoors && dividerWalls.Count < 2) continue;
                        }

                        //Can be accessed from this room
                        if (room.roomType.mustAdjoinRooms.Count <= 0 || room.roomType.mustAdjoinRooms.Contains(wall.otherWall.node.room.roomType) /*|| !room.reachableFromEntrance*/)
                        {
                            //Either this 
                            //Is this room not yet linked to?
                            List<NewNode.NodeAccess> existingLinked = room.entrances.FindAll(item => item.toNode.room == wall.node.room);
                            List<NewNode.NodeAccess> existingLinked2 = room.entrances.FindAll(item => item.toNode.room == wall.otherWall.node.room);
                            existingLinked.AddRange(existingLinked2);

                            //Proceed if either this is already linked-to or there can be a divider of 2 or more and this would overwrite existing entrances
                            if (existingLinked.Count <= 0 || (dividerWalls.Count >= 2 /*&& existingLinked.Exists(item => item.fromNode == wall.node || item.fromNode == wall.otherWall.node || item.toNode == wall.node || item.toNode == wall.otherWall.node)*/))
                            {
                                PossibleDoorwayLocation newLoc = null;

                                //Entrance ranking
                                if (dividerWalls.Count < 2 && wall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                {
                                    newLoc = new PossibleDoorwayLocation();
                                    newLoc.wall = wall;

                                    //Priority goes to 'must adjoin rooms'
                                    int mustAdjoinIndex = room.roomType.mustAdjoinRooms.FindIndex(item => item == wall.otherWall.node.room.roomType);

                                    if (mustAdjoinIndex > -1)
                                    {
                                        newLoc.ranking = (6 - mustAdjoinIndex) * 3 + Toolbox.Instance.RandContained(0f, 0.1f, randomSeed, out randomSeed);
                                    }
                                    else
                                    {
                                        newLoc.ranking = Toolbox.Instance.RandContained(0f, 0.1f, randomSeed, out randomSeed);
                                    }

                                    //Give a large boost if either room is not reachable from the entrance & the other is
                                    if (!wall.node.room.reachableFromEntrance && wall.otherWall.node.room.reachableFromEntrance)
                                    {
                                        newLoc.ranking += 100;
                                    }
                                    else if (wall.node.room.reachableFromEntrance && !wall.otherWall.node.room.reachableFromEntrance)
                                    {
                                        newLoc.ranking += 100;
                                    }

                                    List<NewWall> doorwaysHereAlready = node.walls.FindAll(item => item.preset.sectionClass != DoorPairPreset.WallSectionClass.wall);
                                    doorwaysHereAlready.AddRange(wall.otherWall.node.walls.FindAll(item => item.preset.sectionClass != DoorPairPreset.WallSectionClass.wall));

                                    //Favour ends of corridoors, so rank + 0.2 per wall
                                    newLoc.ranking += wall.otherWall.node.walls.Count * 0.2f;

                                    //Favour furtherst from entrance (the first door should be closest!)
                                    float distToEntrance = -1f;

                                    foreach (NewNode ent in entranceNodes)
                                    {
                                        float dist = Vector2.Distance(ent.position, wall.position);
                                        distToEntrance = Mathf.Max(distToEntrance, dist);
                                    }

                                    newLoc.ranking += distToEntrance;

                                    //Lower ranking by 1 per existing doorway on this tile
                                    //Except opposites which only carry a penalty of 0.5f
                                    foreach (NewWall doorway in doorwaysHereAlready)
                                    {
                                        if (doorway.wallOffset == wall.wallOffset * -1f)
                                        {
                                            newLoc.ranking -= 0.5f;
                                        }
                                        else
                                        {
                                            newLoc.ranking -= 1f;
                                            newLoc.requireFlatDoorway = true;
                                        }
                                    }

                                    possibleDoorways.Add(newLoc);
                                }
                                //Divider ranking
                                else
                                {
                                    newLoc = new PossibleDoorwayLocation();
                                    newLoc.wall = wall;

                                    newLoc.roomDivider = dividerWalls;
                                    newLoc.ranking = dividerWalls.Count * 10;

                                    possibleDoorways.Add(newLoc);
                                }
                            }
                        }
                    }
                }
            }

            if (room.debugController != null) room.debugController.Log("Possible doorways count: " + possibleDoorways.Count);

            //Rank list (higher is better)
            possibleDoorways.Sort();
            possibleDoorways.Reverse();

            foreach (PossibleDoorwayLocation pos in possibleDoorways)
            {
                if (room.debugController != null) room.debugController.Log(pos.ranking + " - " + pos.wall.node.room.name + " <-> " + pos.wall.otherWall.node.room.name);
            }

            //Pick a 'new' entrance: Only +1 regular entrance allowed...
            bool pickedNew = false;

            int safety = 99;

            while (possibleDoorways.Count > 0 && safety > 0)
            {
                PossibleDoorwayLocation newDoorway = possibleDoorways[0];

                if (!pickedNew && (newDoorway.roomDivider == null || newDoorway.roomDivider.Count < 2))
                {
                    if (room.debugController != null) room.debugController.Log("Picked doorway on node: " + newDoorway.wall.node.localTileCoord + " with offset of " + newDoorway.wall.wallOffset + " (" + newDoorway.wall.node.room.name + " <-> " + newDoorway.wall.otherWall.node.room.name);

                    if (newDoorway.requireFlatDoorway && room.gameLocation.thisAsAddress.preset.doorwaysFlat.Count > 0)
                    {
                        //Force no update as it will be updated soon anway!
                        newDoorway.wall.SetDoorPairPreset(room.gameLocation.thisAsAddress.preset.doorwaysFlat[Toolbox.Instance.RandContained(0, room.gameLocation.thisAsAddress.preset.doorwaysFlat.Count, randomSeed, out randomSeed)], false);
                    }
                    else
                    {
                        newDoorway.wall.SetDoorPairPreset(room.gameLocation.thisAsAddress.preset.doorwaysNormal[Toolbox.Instance.RandContained(0, room.gameLocation.thisAsAddress.preset.doorwaysNormal.Count , randomSeed, out randomSeed)], false);
                    }

                    ad.generatedInteriorEntrances.Add(newDoorway.wall);

                    //Set reachable
                    if (!newDoorway.wall.node.room.reachableFromEntrance && newDoorway.wall.otherWall.node.room.reachableFromEntrance) newDoorway.wall.node.room.reachableFromEntrance = true;
                    else if (newDoorway.wall.node.room.reachableFromEntrance && !newDoorway.wall.otherWall.node.room.reachableFromEntrance) newDoorway.wall.otherWall.node.room.reachableFromEntrance = true;

                    pickedNew = true;
                }
                else if (newDoorway.roomDivider != null && newDoorway.roomDivider.Count >= 2)
                {
                    //Only overwrite if this is already not a divider...
                    bool divPass = true;

                    //Ignore if in dictionary already...
                    if (newDoorway.wall.node.room.roomDividers.Exists(item => item.fromRoom == newDoorway.wall.node.room && item.toRoom == newDoorway.wall.otherWall.node.room))
                    {
                        divPass = false;
                    }
                    else if (newDoorway.wall.node.room.roomDividers.Exists(item => item.fromRoom == newDoorway.wall.otherWall.node.room && item.toRoom == newDoorway.wall.node.room))
                    {
                        divPass = false;
                    }

                    foreach (NewWall div in newDoorway.roomDivider)
                    {
                        if (div.parentWall.preset.dividerLeft || div.parentWall.preset.dividerRight)
                        {
                            divPass = false;
                            break;
                        }
                    }

                    if (divPass)
                    {
                        if (room.debugController != null) room.debugController.Log("Picked divider on node: " + newDoorway.wall.node.localTileCoord);

                        //Arrange dividers
                        List<NewWall> sortedList = null;

                        //Sort list
                        if (newDoorway.roomDivider[0].wallOffset.y > 0)
                        {
                            sortedList = newDoorway.roomDivider.OrderBy(item1 => item1.node.floorCoord.x).ToList();
                        }
                        else if (newDoorway.roomDivider[0].wallOffset.y < 0)
                        {
                            sortedList = newDoorway.roomDivider.OrderBy(item1 => item1.node.floorCoord.x).ToList();
                            sortedList.Reverse();
                        }
                        else if (newDoorway.roomDivider[0].wallOffset.x > 0)
                        {
                            sortedList = newDoorway.roomDivider.OrderBy(item1 => item1.node.floorCoord.y).ToList();
                            sortedList.Reverse();
                        }
                        else if (newDoorway.roomDivider[0].wallOffset.x < 0)
                        {
                            sortedList = newDoorway.roomDivider.OrderBy(item1 => item1.node.floorCoord.y).ToList();
                        }

                        for (int i = 0; i < sortedList.Count; i++)
                        {
                            NewWall thisWall = sortedList[i];

                            ////Remove any other entrances to this room
                            //List<NewNode.NodeAccess> otherEntrances = thisWall.node.room.entrances.FindAll(item => (item.fromNode.room == thisWall.node.room && item.toNode.room == thisWall.otherWall.node.room) || (item.fromNode.room == thisWall.otherWall.node.room && item.toNode.room == thisWall.node.room));

                            ////Reset any other entrances back to default walls
                            //foreach (NewNode.NodeAccess access in otherEntrances)
                            //{
                            //    access.wall.SetDoorPairPreset(CityControls.Instance.defaultWalls);
                            //    Game.Log("Removing existing doorway");
                            //}

                            //Left wall
                            if (i == 0)
                            {
                                //Force no update as it will be updated soon anyway!
                                if (thisWall.parentWall == thisWall)
                                {
                                    thisWall.SetDoorPairPreset(room.gameLocation.thisAsAddress.preset.roomDividersLeft[Toolbox.Instance.RandContained(0, room.gameLocation.thisAsAddress.preset.roomDividersLeft.Count, randomSeed, out randomSeed)], false, true);
                                }
                                else
                                {
                                    thisWall.SetDoorPairPreset(room.gameLocation.thisAsAddress.preset.roomDividersRight[Toolbox.Instance.RandContained(0, room.gameLocation.thisAsAddress.preset.roomDividersRight.Count, randomSeed, out randomSeed)], false, true);
                                }

                            }
                            //Right wall
                            else if (i >= sortedList.Count - 1)
                            {
                                //Force no update as it will be updated soon anyway!
                                if (thisWall.parentWall == thisWall)
                                {
                                    thisWall.SetDoorPairPreset(room.gameLocation.thisAsAddress.preset.roomDividersRight[Toolbox.Instance.RandContained(0, room.gameLocation.thisAsAddress.preset.roomDividersRight.Count, randomSeed, out randomSeed)], false, true);
                                }
                                else
                                {
                                    thisWall.SetDoorPairPreset(room.gameLocation.thisAsAddress.preset.roomDividersLeft[Toolbox.Instance.RandContained(0, room.gameLocation.thisAsAddress.preset.roomDividersLeft.Count, randomSeed, out randomSeed)], false, true);
                                }
                            }
                            //Centre
                            else
                            {
                                //Force no update as it will be updated soon anyway!
                                thisWall.SetDoorPairPreset(room.gameLocation.thisAsAddress.preset.roomDividersCentre[Toolbox.Instance.RandContained(0, room.gameLocation.thisAsAddress.preset.roomDividersCentre.Count, randomSeed, out randomSeed)], false, true);
                            }

                            ad.generatedInteriorEntrances.Add(thisWall);
                        }

                        //Add to divider dictionary for both rooms...
                        NewRoom.RoomDivider newDiv = new NewRoom.RoomDivider();
                        newDiv.fromRoom = newDoorway.wall.node.room;
                        newDiv.toRoom = newDoorway.wall.otherWall.node.room;
                        newDiv.dividerWalls = sortedList;
                        newDoorway.wall.node.room.roomDividers.Add(newDiv);

                        NewRoom.RoomDivider newDiv2 = new NewRoom.RoomDivider();
                        newDiv2.fromRoom = newDoorway.wall.otherWall.node.room;
                        newDiv2.toRoom = newDoorway.wall.node.room;
                        newDiv2.dividerWalls = sortedList;
                        newDoorway.wall.otherWall.node.room.roomDividers.Add(newDiv2);

                        //Set reachable
                        if (!newDoorway.wall.node.room.reachableFromEntrance && newDoorway.wall.otherWall.node.room.reachableFromEntrance) newDoorway.wall.node.room.reachableFromEntrance = true;
                        else if (newDoorway.wall.node.room.reachableFromEntrance && !newDoorway.wall.otherWall.node.room.reachableFromEntrance) newDoorway.wall.otherWall.node.room.reachableFromEntrance = true;
                    }
                }

                possibleDoorways.RemoveAt(0);
                safety--;
            }

            //Now these rooms have a divider, if there is any other entrance with the same room parameters, remove it
            for (int i = 0; i < room.roomDividers.Count; i++)
            {
                NewRoom.RoomDivider div = room.roomDividers[i];

                //Find regular entrances matching the rooms of this divider
                List<NewNode.NodeAccess> ents = room.entrances.FindAll(item => ((item.fromNode.room == div.fromRoom && item.toNode.room == div.toRoom) || (item.toNode.room == div.fromRoom && item.fromNode.room == div.toRoom)) && !item.wall.preset.divider);

                while (ents.Count > 0)
                {
                    if (room.debugController != null) room.debugController.Log("Removing regular entrance because of existing divider: " + ents[0].wall.node.localTileCoord);

                    ents[0].wall.parentWall.SetDoorPairPreset(CityControls.Instance.defaultWalls);

                    ents[0].wall.node.room.RemoveEntrance(ents[0].fromNode, ents[0].toNode);
                    ents[0].wall.node.room.RemoveEntrance(ents[0].toNode, ents[0].fromNode);

                    ents[0].wall.otherWall.node.room.RemoveEntrance(ents[0].fromNode, ents[0].toNode);
                    ents[0].wall.otherWall.node.room.RemoveEntrance(ents[0].toNode, ents[0].fromNode);

                    ents.RemoveAt(0);
                }
            }
        }

        unreachableRooms = GetUnreachableRooms(entranceNodes, ad);

        //Remove rooms with no connection to the entrance...
        for (int i = 0; i < ad.rooms.Count; i++)
        {
            NewRoom room = ad.rooms[i];
            if (room.roomType == InteriorControls.Instance.nullRoomType || room.roomType.forceNoDoors) continue;
            if (room == ad.nullRoom) continue;
            if (room.gameLocation.isLobby) continue;

            if(unreachableRooms.Contains(room))
            {
                Game.Log("Removing " + room.roomType.name + " as it is unreachable from any entrance...");

                while(room.nodes.Count > 0)
                {
                    ad.nullRoom.AddNewNode(room.nodes.First());
                }

                if (room.debugController != null) room.debugController.Log("Removing room as it has no connection with entrance");
                ad.rooms.RemoveAt(i);
                Destroy(room.gameObject);
                i--;
            }
        }

        //Remove rooms with 0 nodes (apart from null)
        for (int i = 0; i < ad.rooms.Count; i++)
        {
            NewRoom room = ad.rooms[i];

            if (room == ad.nullRoom) continue;

            if (room.nodes.Count <= 0)
            {
                if (room.debugController != null) room.debugController.Log("Removing room as it now has 0 nodes");
                ad.rooms.RemoveAt(i);
                Destroy(room.gameObject);
                i--;
            }
        }

        //Refresh the room wall data
        UpdateGeometryFloor(ad.floor);
    }

    public void ResetLayout(NewAddress ad, out GameObject newDebugParent)
    {
        //Generate random numbers using contained seed system to avoid unreliable replication over threading
        bool removeEverything = false;

        if (SessionData.Instance.isFloorEdit)
        {
            removeEverything = true;
        }

        ad.generatedRoomConfigs = false;

        //Reset furniture
        ad.RemoveAllInhabitantFurniture(true, FurnitureClusterLocation.RemoveInteractablesOption.remove);

        foreach (NewRoom room in ad.rooms)
        {
            room.blockedAccess = new Dictionary<NewNode, List<NewNode>>();
            room.furniture = new List<FurnitureClusterLocation>();
            room.individualFurniture = new List<FurnitureLocation>();
            room.specialCaseInteractables = new Dictionary<InteractablePreset.SpecialCase, List<Interactable>>();
            room.airVents = new List<AirDuctGroup.AirVent>();
            room.noAccessNodes = new List<NewNode>();
            room.pickFurnitureCache = null;
            room.localizedRoomNodeMaps = null;
        }

        //Remove generated interior doorways
        foreach (NewWall wall in ad.generatedInteriorEntrances)
        {
            if (wall == null) continue;

            //wall.isDivider = false; //Reference divider state
            wall.SetDoorPairPreset(CityControls.Instance.defaultWalls, enableUpdate: false);
        }

        ad.generatedInteriorEntrances.Clear();

        //Reset protected nodes
        ad.protectedNodes.Clear();

        //Transfer all nodes to default room
        for (int i = 0; i < ad.rooms.Count; i++)
        {
            NewRoom room = ad.rooms[i];
            room.roomDividers.Clear();
            room.reachableFromEntrance = false;
            room.openPlanElements.Clear();

            if (room == ad.nullRoom)
            {
                continue;
            }

            NewNode[] nodesArr = room.nodes.ToArray();

            foreach (NewNode n in nodesArr)
            {
                n.accessToOtherNodes.Clear(); //Clear access to other nodes
                ad.nullRoom.AddNewNode(n);
            }

            //for (int u = 0; u < room.nodes.Count; u++)
            //{
            //    room.nodes[u].accessToOtherNodes.Clear(); //Clear access to other nodes
            //    ad.nullRoom.AddNewNode(room.nodes[u]);
            //    u--;
            //}

            ad.RemoveRoom(room);
            if (room.gameLocation != null) room.gameLocation.RemoveRoom(room);
            Destroy(room.gameObject);
            i--;
        }

        //Run update
        UpdateFloorCeilingFloor(ad.floor);
        UpdateWallsFloor(ad.floor);

        //Create debug object
        newDebugParent = null;

        if (SessionData.Instance.isFloorEdit)
        {
            if (ad.floorEditDebugParent != null)
            {
                Destroy(ad.floorEditDebugParent);
            }

            newDebugParent = new GameObject();
            newDebugParent.transform.SetParent(FloorEditController.Instance.debugContainer.transform);
            newDebugParent.transform.localPosition = Vector3.zero;
            newDebugParent.name = ad.name;
            ad.floorEditDebugParent = newDebugParent;
        }
    }

    private HashSet<NewRoom> GetUnreachableRooms(List<NewNode> entranceNodes, NewAddress ad)
    {
        //Perform a check to see what rooms can reach the entrance
        HashSet<NewRoom> openReachSet = new HashSet<NewRoom>();
        HashSet<NewRoom> unreachableRooms = new HashSet<NewRoom>(ad.rooms);

        foreach(NewRoom r in ad.rooms)
        {
            r.reachableFromEntrance = false;
        }

        //Game.Log(ad.name + ": Get unreachable rooms out of " + ad.rooms.Count);

        if (entranceNodes.Count <= 0 || entranceNodes == null)
        {
            //Game.Log(ad.name + ": No given entrance nodes; no rooms will be reachable...");
            return unreachableRooms; 
        }

        foreach (NewNode ent in entranceNodes)
        {
            if(!openReachSet.Contains(ent.room))
            {
                //Game.Log("...From entrance room " + ent.room.roomID + " " + ent.room.roomType.name);
                openReachSet.Add(ent.room);
            }
        }

        int safetyReachable = 999;

        while (openReachSet.Count > 0 && safetyReachable > 0)
        {
            NewRoom current = openReachSet.FirstOrDefault();
            current.reachableFromEntrance = true;
            unreachableRooms.Remove(current);

            foreach(NewNode n in current.nodes)
            {
                foreach(NewWall w in n.walls)
                {
                    if(w.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
                    {
                        NewRoom otherRoom = w.otherWall.node.room;
                        if (otherRoom == current) otherRoom = w.node.room;

                        if(otherRoom.gameLocation == ad)
                        {
                            if (!openReachSet.Contains(otherRoom) && unreachableRooms.Contains(otherRoom))
                            {
                                openReachSet.Add(otherRoom);
                            }
                        }
                    }
                }
            }

            openReachSet.Remove(current);
            safetyReachable--;
        }

        if(Game.Instance.devMode)
        {
            if(unreachableRooms.Count > 0)
            {
                string unreachable = string.Empty;

                foreach(NewRoom r in unreachableRooms)
                {
                    unreachable += r.roomType.name + "(" + r.nodes.Count + "), ";
                }

                Game.Log(ad.name + ": Unreachable rooms: " + unreachableRooms.Count +"/" + ad.rooms.Count + " = " + unreachable);
            }
        }

        return unreachableRooms;
    }

    private List<PossibleRoomLocation> GetPossibleRoomLocations(NewAddress address, RoomTypePreset config, List<NewNode> possibleNodes, List<NewNode> entranceNodes, List<NewNode> mainEntranceNodes, List<NewNode> edgeNodes, Transform debugParent)
    {
        //Setup debug object for this room config
        GameObject newConfigAttempt = new GameObject();
        newConfigAttempt.transform.SetParent(debugParent);
        newConfigAttempt.transform.localPosition = Vector3.zero;
        newConfigAttempt.name = config.name;

        //Game.Log("Getting possible room locations for " + config.name + "...");

        List<PossibleRoomLocation> possibilities = new List<PossibleRoomLocation>(); //Return

        //Do a search from minimum size up to maxium size. Use a square ratio.
        List<Vector2> scanForRects = new List<Vector2>();

        //Build a list of rect sizes to scan for. Use square root to get sizes and round up.
        for (int i = (int)config.minimumRoomAreaShape.x; i <= (int)config.maximumRoomAreaShape.x; i++)
        {
            for (int u = (int)config.minimumRoomAreaShape.y; u <= (int)config.maximumRoomAreaShape.y; u++)
            {
                Vector2 newSize = new Vector2(i, u);
                if (!scanForRects.Contains(newSize)) scanForRects.Add(newSize);

                //Add opposite dimensions
                if (newSize.x != newSize.y)
                {
                    Vector2 newSize2 = new Vector2(u, i);
                    if (!scanForRects.Contains(newSize2)) scanForRects.Add(newSize2);
                }
            }
        }

        //Game.Log("Rects for " + config.room.ToString() + ": " + scanForRects.Count + " across " + possibleNodes.Count);

        //Debug
        int overrideRoomSplitFails = 0;
        int selfRoomSplitFails = 0;
        int connectionFails = 0;

        //Scanning all room nodes results in some positions not being checked, as we are measuring in positives from each node only. To mesaure positions we want we have to use a rect of nodes using min/max values
        List<NewNode> scanNodes = new List<NewNode>();

        int minX = 999999;
        int maxX = -999999;
        int minY = 999999;
        int maxY = -999999;

        foreach (NewNode node in possibleNodes)
        {
            minX = Mathf.Min((int)node.floorCoord.x, minX);
            maxX = Mathf.Max((int)node.floorCoord.x, maxX);

            minY = Mathf.Min((int)node.floorCoord.y, minY);
            maxY = Mathf.Max((int)node.floorCoord.y, maxY);
        }

        for (int i = minX; i <= maxX; i++)
        {
            for (int u = minY; u <= maxY; u++)
            {
                Vector2Int floorC = new Vector2Int(i, u);

                NewNode foundNode = null;

                if (address.floor.nodeMap.TryGetValue(floorC, out foundNode))
                {
                    if (!scanNodes.Contains(foundNode))
                    {
                        scanNodes.Add(foundNode);
                    }
                }
            }
        }

        //Now do a scan for each of these shapes...
        for (int i = 0; i < scanForRects.Count; i++)
        {
            Vector2 roomSize = scanForRects[i];

            //Scan all nodes
            foreach (NewNode node in scanNodes)
            {
                //Create a new debug attempt
                GameObject newDebugAttempt = Instantiate(PrefabControls.Instance.debugAttemptObject, newConfigAttempt.transform);
                GenerationDebugController debug = newDebugAttempt.GetComponent<GenerationDebugController>();
                debug.Setup(config.name + " " + roomSize + " @ " + node.nodeCoord, config);
                //Game.Log("Checking placement with rect of " + roomSize + " starting at " + node.name + " with " + possibilities.Count + " possible nodes");

                Dictionary<NewRoom, List<NewNode>> roomNodeOverrides = new Dictionary<NewRoom, List<NewNode>>();
                List<NewNode> validRoomNodes = new List<NewNode>();

                //Does this contain a stariwell or elevator? If so the minimum room size is 12
                //The minimum room size may need to be extended slightly to accomodate a stairwell, so use this variable instead...
                Vector2 mininumRoomSize = config.minimumRoomAreaShape;

                //To avoid predictable layouts, give this a random base.
                //Record rank of this attempt...
                string seed = string.Empty;

                if (SessionData.Instance.isFloorEdit) seed = Toolbox.Instance.GenerateUniqueID();
                else seed = node.nodeCoord.ToString();

                float randomRank = Toolbox.Instance.GetPsuedoRandomNumberContained(0f, InteriorControls.Instance.roomRankingRandomThreshold, seed, out seed);
                float floorSpaceRank = 0f;

                int exteriorWindows = 0;
                float exteriorWindowsRank = 0f;

                int exteriorWalls = 0;
                float exteriorWallsRank = 0f;

                int entrances = 0;
                float entrancesRank = 0f;

                int overrideCount = 0;

                //Search for room area validity here...
                for (int u = 0; u < roomSize.x; u++)
                {
                    for (int l = 0; l < roomSize.y; l++)
                    {
                        Vector2Int v2 = node.floorCoord + new Vector2Int(u, l);
                        NewNode thisNode = null;

                        //This node is starting tile
                        if (address.floor.nodeMap.TryGetValue(v2, out thisNode))
                        {
                            //different address
                            if (thisNode.gameLocation != address)
                            {
                                debug.attemptedInvalidNodes.Add(thisNode, "Different address");
                                continue;
                            }

                            //This is an entrance, and they are not allowed for this room...
                            if (!config.allowMainAddressEntrance && mainEntranceNodes.Contains(thisNode))
                            {
                                debug.attemptedInvalidNodes.Add(thisNode, "Not allowed main address entrance");
                                continue;
                            }

                            if (!config.allowSecondaryAddressEntrance && (entranceNodes.Contains(thisNode) && !mainEntranceNodes.Contains(thisNode)))
                            {
                                debug.attemptedInvalidNodes.Add(thisNode, "Not allowed secondary address entrance");
                                continue;
                            }

                            //This node is unassigned, therefore valid
                            bool validAddition = false;

                            if (thisNode.room == address.nullRoom || thisNode.room.roomType == InteriorControls.Instance.nullRoomType)
                            {
                                //Is this a stairwell or elevator? If so, extend minimum size to 12 and add to rank
                                if (thisNode.tile.isStairwell || thisNode.tile.isInvertedStairwell)
                                {
                                    //Game.Log("Using minimum room size of 12+");
                                    if (mininumRoomSize.x <= 3)
                                    {
                                        mininumRoomSize = new Vector2(3, Mathf.Max(mininumRoomSize.x, 4));
                                    }
                                    else if (mininumRoomSize.y <= 4)
                                    {
                                        mininumRoomSize = new Vector2(Mathf.Max(mininumRoomSize.x, 3), 4);
                                    }
                                }

                                validAddition = true;
                            }
                            //This node is assigned, BUT it can be overridden if the room allows it...
                            else
                            {
                                //Cannot overwrite protected
                                if (!thisNode.room.roomType.overridable)
                                {
                                    debug.attemptedInvalidNodes.Add(thisNode, "OVR " + thisNode.room.roomType.name + " Non -overridable room: " + thisNode.room.roomType.name);
                                    continue;
                                }
                                else if (thisNode.overwriteLimit < 1)
                                {
                                    debug.attemptedInvalidNodes.Add(thisNode, "OVR " + thisNode.room.roomType.name + " node overwrite limit reached");
                                    continue;
                                }
                                else if (address.protectedNodes.Contains(thisNode))
                                {
                                    debug.attemptedInvalidNodes.Add(thisNode, "OVR " + thisNode.room.roomType.name + " Protected node: " + thisNode.name);
                                    continue;
                                }
                                else if (thisNode.room.roomType.cyclePriority > config.overwriteWithPriorityUpTo)
                                {
                                    debug.attemptedInvalidNodes.Add(thisNode, "OVR " + thisNode.room.roomType.name + " Lower overwrite priority than: " + thisNode.room.roomType.name);
                                    continue; //The room to be overwritten has a priority higher than what is allowed
                                }
                                else if (thisNode.room.roomType.blockOverridesFromType.Contains(config))
                                {
                                    debug.attemptedInvalidNodes.Add(thisNode, "OVR " + thisNode.room.roomType.name + " Room blocks overwrite from type " + thisNode.room.roomType.ToString());
                                    continue; //The room to be overwritten has a priority higher than what is allowed
                                }

                                //Enter into dictionary
                                if (!roomNodeOverrides.ContainsKey(thisNode.room)) roomNodeOverrides.Add(thisNode.room, new List<NewNode>());

                                //Do a minimum shape check...
                                List<NewNode> overridenNodes = new List<NewNode>(thisNode.room.nodes);
                                overridenNodes.Remove(thisNode);

                                foreach (NewNode n in roomNodeOverrides[thisNode.room])
                                {
                                    overridenNodes.Remove(n);
                                }

                                //Check minimum shape
                                if (RoomMinimumShapeCheck(ref overridenNodes, thisNode.room.roomType.minimumRoomAreaShape, debug))
                                {
                                    //Check must adjoin
                                    if (MustAdjoinOneOfCheck(ref overridenNodes, thisNode.room.gameLocation, thisNode.room.roomType.mustAdjoinRooms, thisNode.room.roomType.mustConnectWithEntrance, out _, debug))
                                    {
                                        //Check entrance
                                        if (!thisNode.room.roomType.mustConnectWithEntrance || CheckEntranceConnection(ref overridenNodes, thisNode.room.gameLocation, debug))
                                        {
                                            //Add this node to overrides to do a proper analysis. Remove if upon fail.
                                            roomNodeOverrides[thisNode.room].Add(thisNode); //Increase override count
                                            overrideCount++;

                                            validAddition = true;

                                            debug.overridenNodes.Add(thisNode, thisNode.room.roomType.name);
                                        }
                                        else
                                        {
                                            debug.attemptedInvalidNodes.Add(thisNode, "OVR " + thisNode.room.roomType.name + " entrance connect fail: " + thisNode.room.roomType.name);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        debug.attemptedInvalidNodes.Add(thisNode, "OVR " + thisNode.room.roomType.name + " Adjoining fail: " + thisNode.room.roomType.name);
                                        continue;
                                    }
                                }
                                else
                                {
                                    debug.attemptedInvalidNodes.Add(thisNode, "OVR " + thisNode.room.roomType.name + " Minimum room shape fail: " + thisNode.room.roomType.name);
                                    continue;
                                }
                            }

                            if (validAddition)
                            {
                                //Exterior wall: Borders on the edge of the address...
                                NewNode foundNode = null;

                                //Apply exterior ranking
                                foreach (Vector2Int off in CityData.Instance.offsetArrayX4)
                                {
                                    if (GetAdjacentNode(thisNode, off, out foundNode))
                                    {
                                        if (foundNode.gameLocation != thisNode.gameLocation)
                                        {
                                            exteriorWalls++;

                                            if (thisNode.walls.Exists(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.window || item.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge))
                                            {
                                                exteriorWindows++;
                                            }
                                            else if (thisNode.walls.Exists(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance && item.otherWall.node.gameLocation != item.node.gameLocation))
                                            {
                                                entrances++;

                                                if(thisNode.room.roomType.preferMainAddressEntrance)
                                                {
                                                    //This wall child or parent must be from a different address
                                                    if(thisNode.walls.Exists(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance && item.parentWall.node.isOutside || item.childWall.node.isOutside))
                                                    {
                                                        //Double score for main entrances
                                                        entrances++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                validRoomNodes.Add(thisNode);
                            }
                        }
                    }
                }

                //We now have a map of all the valid nodes for this placement, however it may not still be valid as we need to check size, shape etc of both this and any overridden rooms...
                //Game.Log("...This placement has found " + validRoomNodes.Count + " valid room nodes (" + overrideCount + " overridden)");
                debug.attemptedValidNodes = validRoomNodes;

                //First check the valid nodes we have pass requirements for this room...
                if (!RoomMinimumShapeCheck(ref validRoomNodes, mininumRoomSize, debug))
                {
                    //Game.Log("Failed on minimum shape check of " + validRoomNodes.Count + " nodes, size: " + mininumRoomSize);
                    continue;
                }

                //Tesselation check
                //Game.Log("Tesselation check for new room...");

                if (!TesselationShapeCheck(ref validRoomNodes, config.tesselationShape, debug))
                {
                    //Game.Log("Failed on tesselation check of " + validRoomNodes.Count + " nodes, shape: " + config.tesselationShape);
                    continue;
                }

                //Room split check: The room must not be split in half!
                if (!RoomSplitCheck(ref validRoomNodes, debug))
                {
                    //Game.Log("Failed on room split check");
                    selfRoomSplitFails++;
                    continue;
                }

                //Check must adjoin
                List<NewNode> adjoiningRoomNodes = null;

                if (!MustAdjoinOneOfCheck(ref validRoomNodes, address, config.mustAdjoinRooms, config.mustConnectWithEntrance, out adjoiningRoomNodes, debug))
                {
                    //Game.Log("Failed on adjoining check");
                    continue;
                }

                //Check valid overrides: Rooms must not be split in 2
                bool overridesPass = true;

                List<OverrideData> overrideData = new List<OverrideData>();

                //Check validity and penalty of overridding rooms...
                foreach (KeyValuePair<NewRoom, List<NewNode>> pair in roomNodeOverrides)
                {
                    //Simulate an overriden room's nodes by using the full node list and removing overridden cells
                    List<NewNode> thisRoomsNewNodes = new List<NewNode>(pair.Key.nodes);

                    foreach (NewNode rem in pair.Value)
                    {
                        thisRoomsNewNodes.Remove(rem);
                    }

                    //Check for room split (exception: Hallways)
                    if (pair.Key.preset != address.preset.hallway)
                    {
                        if (!RoomSplitCheck(ref thisRoomsNewNodes, debug))
                        {
                            //Game.Log("Failed on override room split check");
                            overridesPass = false;
                            overrideRoomSplitFails++;
                            break;
                        }
                    }

                    //Check room tesselation
                    //Game.Log("Tesselation check for overwritten room...");
                    if (!TesselationShapeCheck(ref thisRoomsNewNodes, pair.Key.roomType.tesselationShape, debug))
                    {
                        //Game.Log("Failed on override room tesselation check");
                        //Abort whole thing
                        overridesPass = false;
                        break;
                    }

                    //Calculate penalty ranking for overriden room
                    //Calculate overriding penalty...
                    int overridenWindows = 0;
                    int overrdenWalls = 0;

                    foreach (NewNode ovr in pair.Value)
                    {
                        //Exterior wall: Borders on the edge of the address...
                        NewNode foundNode = null;

                        foreach (Vector2Int off in CityData.Instance.offsetArrayX4)
                        {
                            if (GetAdjacentNode(ovr, off, out foundNode))
                            {
                                if (foundNode.gameLocation != ovr.gameLocation)
                                {
                                    if (ovr.walls.Exists(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.window || item.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge))
                                    {
                                        overridenWindows++;
                                    }

                                    overrdenWalls++;
                                }
                            }
                        }
                    }

                    //Ranking...
                    //calculate penalty by adding up differences
                    OverrideData newOvr = new OverrideData();
                    newOvr.room = pair.Key;
                    newOvr.floorSpacePenalty = (pair.Value.Count * 0.5f * pair.Key.roomType.floorSpaceWeight) * -1f; //Floor space: 0.5 per tile above minimum
                    newOvr.exteriorWindowPenalty = (overridenWindows * 0.5f * pair.Key.roomType.exteriorWindowWeight) * -1f; //Windows: 0.5 per window
                    newOvr.exteriorWallPenalty = (overrdenWalls * 0.1f * pair.Key.roomType.exteriorWallWeight) * -1f; //Walls: 0.1 per wall

                    newOvr.overridingPenalty = newOvr.floorSpacePenalty + newOvr.exteriorWindowPenalty + newOvr.exteriorWallPenalty;

                    overrideData.Add(newOvr);
                }

                //If this is invalid, continue
                if (!overridesPass)
                {
                    //Game.Log("Failed on override checking (details above).");
                    continue;
                }

                //Must connect to an entrance
                List<NewNode> hallwayPath = null;

                if (config.mustConnectWithEntrance)
                {
                    //If this still is not a pass, attempt to generate a hallway to connect...
                    if (!CheckEntranceConnection(ref validRoomNodes, address, debug))
                    {
                        bool pass = false;

                        if(address.preset.requiresHallway && address.preset.hallway != null)
                        {
                            if (mainEntranceNodes.Count > 0)
                            {
                                foreach (NewNode entrance in mainEntranceNodes)
                                {
                                    if (entrance.room.roomType != InteriorControls.Instance.nullRoomType) continue; //If this entrance is already assigned a room, then skip

                                    hallwayPath = HallwayPathfind(entrance, validRoomNodes[Toolbox.Instance.GetPsuedoRandomNumberContained(0, validRoomNodes.Count, seed, out seed)], address);

                                    if (hallwayPath != null && hallwayPath.Count > 0)
                                    {
                                        pass = true;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                foreach (NewNode entrance in entranceNodes)
                                {
                                    if (entrance.room.roomType != InteriorControls.Instance.nullRoomType) continue; //If this entrance is already assigned a room, then skip

                                    hallwayPath = HallwayPathfind(entrance, validRoomNodes[Toolbox.Instance.GetPsuedoRandomNumberContained(0, validRoomNodes.Count, seed, out seed)], address);

                                    if (hallwayPath != null && hallwayPath.Count > 0)
                                    {
                                        pass = true;
                                        break;
                                    }
                                }
                            }
                        }


                        //If there are no entrance nodes then ignore...
                        if (entranceNodes.Count <= 0)
                        {
                            pass = true;
                        }

                        //If still no pass then abort...
                        if (!pass)
                        {
                            Game.Log("New room would not connect with the entrance.", 2);
                            connectionFails++;
                            continue;
                        }
                    }
                }

                //Ranking...
                //Floor space: 0 for min, 1 for max
                float minNodes = mininumRoomSize.x * mininumRoomSize.y;
                float floorSpace = validRoomNodes.Count - minNodes;

                //Floor space: 0.5 per tile above minimum
                floorSpaceRank = floorSpace * 0.5f * config.floorSpaceWeight;

                //Windows: 0.5 per window
                exteriorWindowsRank = exteriorWindows * 0.5f * config.exteriorWindowWeight;

                //Walls: 0.5 per window
                exteriorWallsRank = exteriorWalls * 0.1f * config.exteriorWallWeight;

                //Entrances:0.5 per entrances
                entrancesRank = entrances * 0.5f * config.entranceWeight;

                //Create data class
                PossibleRoomLocation newLoc = new PossibleRoomLocation();
                newLoc.nodes.AddRange(validRoomNodes);
                newLoc.exteriorWindowRanking = exteriorWindowsRank;
                newLoc.exteriorWallsRanking = exteriorWallsRank;
                newLoc.floorSpaceRanking = floorSpaceRank;
                newLoc.entrancesRanking = entrancesRank;
                newLoc.randomRanking = randomRank;
                newLoc.overrideRankingData = overrideData;
                newLoc.debugScript = debug;
                newLoc.ranking = exteriorWindowsRank + floorSpaceRank + entrancesRank + randomRank + exteriorWallsRank;

                //Minus override data
                foreach (OverrideData data in overrideData)
                {
                    newLoc.ranking += data.overridingPenalty;
                }

                newLoc.requiredAdjoiningOptions = adjoiningRoomNodes;
                newLoc.requiredHallway = hallwayPath;

                //if (newLoc.ranking < 0) continue;

                possibilities.Add(newLoc);
                debug.location = newLoc;
                debug.valid = true;
                debug.name = "-" + debug.name + " : " + newLoc.ranking;
                debug.gameObject.transform.SetAsFirstSibling();
            }
        }

        //Game.Log("Override room split fails: " + overrideRoomSplitFails);
        //Game.Log("Minimum size fails: " + minimumSizeFails);
        //Game.Log("Self room split fails: " + selfRoomSplitFails);
        //Game.Log("Adjoining fails: " + adjoiningFails);
        //Game.Log("Connection fails: " + connectionFails);

        return possibilities;
    }

    //Minimum shape check: Try and fit this shape within the room nodes...
    private bool RoomMinimumShapeCheck(ref List<NewNode> nodes, Vector2 minimumShape, GenerationDebugController debug)
    {
        if (minimumShape.x + minimumShape.y <= 0)
        {
            //Game.Log("Minimum shape amount to space of 0, returning true...");
            return true;
        }

        foreach (NewNode node in nodes)
        {
            bool pass = true;

            //Search for room area validity here...
            //We may need to check both rotations
            int rotCheck = 1;
            if (minimumShape.x != minimumShape.y) rotCheck = 2;

            for (int i = 0; i < rotCheck; i++)
            {
                Vector2 shapeCheck = minimumShape;
                if (i == 1) shapeCheck = new Vector2(minimumShape.y, minimumShape.x);

                pass = true;
                //Game.Log("Rotation check for shape of " + shapeCheck + " starting at node " + node.nodeCoord + "...");

                for (int u = 0; u < shapeCheck.x; u++)
                {
                    for (int l = 0; l < shapeCheck.y; l++)
                    {
                        Vector3Int offset = node.nodeCoord + new Vector3Int(u, l, 0);
                        //Game.Log(offset + "...", 2);

                        NewNode foundNode = null;

                        if (PathFinder.Instance.nodeMap.TryGetValue(offset, out foundNode))
                        {
                            //Not in this list- fail
                            if (!nodes.Contains(foundNode))
                            {
                                //Game.Log(offset + " node not in collection");
                                pass = false;
                                break;
                            }
                        }
                        else
                        {
                            //Game.Log(offset + " node not found");
                            pass = false;
                            break;
                        }
                    }

                    if (!pass) break;
                }

                if (pass) break;
            }

            if (!pass) continue;
            else
            {
                if(debug != null) debug.Log("Passed minimum room shape check...");
                return true; //If valid pass then we are successful
            }
        }

        if (debug != null) debug.Log("Failed minimum room shape check...");
        return false;
    }

    private bool RoomMinimumShapeCheck(ref HashSet<NewNode> nodes, Vector2 minimumShape, GenerationDebugController debug, bool nodesMustBeUnoccupied = false)
    {
        if (minimumShape.x + minimumShape.y <= 0)
        {
            //Game.Log("Minimum shape amount to space of 0, returning true...");
            return true;
        }

        foreach (NewNode node in nodes)
        {
            bool pass = true;

            //Search for room area validity here...
            //We may need to check both rotations
            int rotCheck = 1;
            if (minimumShape.x != minimumShape.y) rotCheck = 2;

            for (int i = 0; i < rotCheck; i++)
            {
                Vector2 shapeCheck = minimumShape;
                if (i == 1) shapeCheck = new Vector2(minimumShape.y, minimumShape.x);

                pass = true;
                //Game.Log("Rotation check for shape of " + shapeCheck + " starting at node " + node.nodeCoord + "...");

                for (int u = 0; u < shapeCheck.x; u++)
                {
                    for (int l = 0; l < shapeCheck.y; l++)
                    {
                        Vector3Int offset = node.nodeCoord + new Vector3Int(u, l, 0);
                        //Game.Log(offset + "...", 2);

                        NewNode foundNode = null;

                        if (PathFinder.Instance.nodeMap.TryGetValue(offset, out foundNode))
                        {
                            if(nodesMustBeUnoccupied)
                            {
                                if(!foundNode.allowNewFurniture)
                                {
                                    pass = false;
                                    break;
                                }
                            }

                            //Not in this list- fail
                            if (!nodes.Contains(foundNode))
                            {
                                //Game.Log(offset + " node not in collection");
                                pass = false;
                                break;
                            }
                        }
                        else
                        {
                            //Game.Log(offset + " node not found");
                            pass = false;
                            break;
                        }
                    }

                    if (!pass) break;
                }

                if (pass) break;
            }

            if (!pass) continue;
            else
            {
                if (debug != null) debug.Log("Passed minimum room shape check...");
                return true; //If valid pass then we are successful
            }
        }

        if (debug != null) debug.Log("Failed minimum room shape check...");
        return false;
    }

    //Tesselation shape check: Shape must fit into the nodes once or more times as to not leave any remnants
    private bool TesselationShapeCheck(ref List<NewNode> nodes, Vector2 tessShape, GenerationDebugController debugController)
    {
        if (tessShape.x + tessShape.y <= 0) return true;

        List<NewNode> tesselated = new List<NewNode>();

        foreach (NewNode node in nodes)
        {
            //We may need to check both rotations
            int rotCheck = 1;
            if (tessShape.x != tessShape.y) rotCheck = 2;

            for (int i = 0; i < rotCheck; i++)
            {
                Vector2 shapeCheck = tessShape;
                if (i == 1) shapeCheck = new Vector2(tessShape.y, tessShape.x);

                bool pass = true;
                List<NewNode> thisPass = new List<NewNode>();

                //Search for room area validity here...
                for (int u = 0; u < shapeCheck.x; u++)
                {
                    for (int l = 0; l < shapeCheck.y; l++)
                    {
                        Vector3Int offset = node.nodeCoord + new Vector3Int(u, l, 0);

                        NewNode foundNode = null;

                        if (PathFinder.Instance.nodeMap.TryGetValue(offset, out foundNode))
                        {
                            //Not in this list- fail
                            if (!nodes.Contains(foundNode))
                            {
                                pass = false;
                                break;
                            }
                            else
                            {
                                if (!tesselated.Contains(foundNode)) thisPass.Add(foundNode);
                            }
                        }
                        else
                        {
                            pass = false;
                            break;
                        }
                    }

                    if (!pass) break;
                }

                if (!pass) continue;
                else
                {
                    //If valid then consider these nodes tesselated
                    tesselated.AddRange(thisPass);
                }
            }
        }

        if (debugController != null) debugController.Log("...Tesselation results: " + tesselated.Count + "/" + nodes.Count);

        //If the number of tesselated equals the original count, then success!
        if (tesselated.Count == nodes.Count)
        {
            return true;
        }
        else return false;
    }

    //Check if these nodes are adjoined to at least one of these room types
    private bool MustAdjoinOneOfCheck(ref List<NewNode> nodes, NewGameLocation thisGameLocation, List<RoomTypePreset> roomTypes, bool includeEntrance, out List<NewNode> internalAdjoiningRoomNodes, GenerationDebugController debug)
    {
        bool pass = false;

        internalAdjoiningRoomNodes = new List<NewNode>();

        if (roomTypes.Count <= 0 && !includeEntrance)
        {
            debug.Log("Passed adjoin check (automatic)");
            return true; //Skip this check if nothing to check for
        }

        foreach (NewNode adjCheck in nodes)
        {
            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
            {
                Vector3Int nodeChk = adjCheck.nodeCoord + new Vector3Int(v2.x, v2.y, 0);
                NewNode scanNode = null;

                if (PathFinder.Instance.nodeMap.TryGetValue(nodeChk, out scanNode))
                {
                    //Must be same address
                    if (scanNode.gameLocation == thisGameLocation)
                    {
                        //Must be one of required room types
                        if (scanNode.room != null)
                        {
                            if (roomTypes.Contains(scanNode.room.roomType))
                            {
                                pass = true;

                                if (!internalAdjoiningRoomNodes.Contains(scanNode))
                                {
                                    internalAdjoiningRoomNodes.Add(scanNode);
                                }
                            }
                        }
                    }
                    //Also scan address entrances
                    else if (includeEntrance)
                    {
                        NewWall wall = adjCheck.walls.Find(item => item.node == scanNode || item.otherWall.node == scanNode);

                        if (wall != null)
                        {
                            if (wall.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
                            {
                                pass = true;
                            }
                        }
                    }
                }
            }
        }

        if (!pass)
        {
            debug.Log("Failed adjoin check...");
            return false;
        }
        else
        {
            debug.Log("Passed adjoin check...");
            return true;
        }
    }

    //Check entrance connection
    private bool CheckEntranceConnection(ref List<NewNode> nodes, NewGameLocation thisGameLocation, GenerationDebugController debug)
    {
        bool pass = false;

        //Pass if this opens onto entrance anyway
        foreach (NewNode checkNode in nodes)
        {
            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
            {
                Vector2Int nodeChk = checkNode.floorCoord + v2;

                NewNode scanNode = null;

                if (checkNode.floor.nodeMap.TryGetValue(nodeChk, out scanNode))
                {
                    //Hallway: Must be same address
                    if (scanNode.gameLocation == thisGameLocation)
                    {
                        //Must be one of required room types
                        if (scanNode.room.gameLocation.thisAsAddress != null && scanNode.room.gameLocation.thisAsAddress.preset != null && scanNode.room.gameLocation.thisAsAddress.preset.hallway != null && scanNode.room.roomType == scanNode.room.gameLocation.thisAsAddress.preset.hallway.roomType)
                        {
                            pass = true;
                            break;
                        }
                    }
                    //Entrance address
                    else
                    {
                        NewWall wall = checkNode.walls.Find(item => item.node == scanNode || item.otherWall.node == scanNode);

                        if (wall != null)
                        {
                            if (wall.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
                            {
                                pass = true;
                            }
                        }
                    }
                }
            }

            if (pass) break;
        }

        debug.Log("Check entrance connection pass: " + pass);
        return pass;
    }

    private void CreateForcedRooms(NewAddress ad)
    {
        for (int i = 0; i < ad.nodes.Count; i++)
        {
            NewNode node = ad.nodes[i];

            //This has a forced room setting and belongs to null space...
            if (node.forcedRoom != null && node.forcedRoomRef.Length > 0 && node.room == ad.nullRoom)
            {
                //Create room
                GameObject newObj = Instantiate(PrefabControls.Instance.room, ad.transform);
                NewRoom newRoom = newObj.GetComponent<NewRoom>();
                newRoom.SetupAll(ad, node.forcedRoom);

                //Find nodes with the same forceroom. Expand looking in adjacent directions
                List<NewNode> openSet = new List<NewNode>();
                openSet.Add(node);

                int safety = 999;

                while (openSet.Count > 0 && safety > 0)
                {
                    NewNode currentNode = openSet[0];
                    newRoom.AddNewNode(currentNode);
                    ad.protectedNodes.Add(currentNode); //Add to protected list to stop this from being overriden

                    foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                    {
                        NewNode foundNode = null;

                        if (GetAdjacentNode(currentNode, v2, out foundNode))
                        {
                            //Same room type
                            if (foundNode.forcedRoom != null && foundNode.forcedRoomRef.Length > 0 && foundNode.forcedRoom == node.forcedRoom)
                            {
                                //Not yet assigned
                                if (foundNode.room == ad.nullRoom)
                                {
                                    if (!openSet.Contains(foundNode) && !newRoom.nodes.Contains(foundNode))
                                    {
                                        openSet.Add(foundNode);
                                    }
                                }
                            }
                        }
                    }

                    openSet.RemoveAt(0);
                    safety--;
                }
            }
        }
    }

    //Calculate room uniformity, considering jutting out nodes and room ratio
    private float GetRoomUniformity(List<NewNode> nodes, out int wallCount, out float shapeRatio)
    {
        float output = 0f;

        //Calculate uniformity of the room...
        int uniformWallCount = 0;
        Vector2 uniformBounds = Vector2.zero;
        wallCount = CalculateRoomEdges(nodes, out uniformWallCount, out uniformBounds);

        //Shape ratio: Shortest edge divided by the longest
        shapeRatio = Mathf.Min(uniformBounds.x, uniformBounds.y) / Mathf.Max(uniformBounds.x, uniformBounds.y);

        //This will decreate if nodes of other rooms jut out into this one
        float jutValue = (float)uniformWallCount / (float)wallCount;

        //This will decrease as the room's bounds are occupied with other rooms
        float spaceRatio = (float)nodes.Count / (float)(uniformBounds.x * uniformBounds.y);

        output = jutValue * spaceRatio;

        return output;
    }

    //Calculate how many edges this would have
    private int CalculateRoomEdges(List<NewNode> nodes, out int uniformWallCount, out Vector2 uniformBoundsSize)
    {
        int ret = 0; //Actual number of walls
        uniformWallCount = 0; //Number of walls if this was a uniform shape
        uniformBoundsSize = Vector2.zero;

        int lowestX = 9999;
        int lowestY = 9999;
        int highestX = -1;
        int highestY = -1;

        foreach (NewNode node in nodes)
        {
            lowestX = (int)Mathf.Min(lowestX, node.floorCoord.x);
            lowestY = (int)Mathf.Min(lowestY, node.floorCoord.y);
            highestX = (int)Mathf.Max(highestX, node.floorCoord.x);
            highestY = (int)Mathf.Max(highestY, node.floorCoord.y);

            //Check directions
            for (int i = 0; i < CityData.Instance.offsetArrayX4.Length; i++)
            {
                Vector2Int nodeCheck = node.floorCoord + CityData.Instance.offsetArrayX4[i];

                if (node.floor.nodeMap.ContainsKey(nodeCheck))
                {
                    NewNode accessNeighbor = node.floor.nodeMap[nodeCheck];

                    if (!nodes.Contains(accessNeighbor))
                    {
                        ret++;
                    }
                }
                else ret++;
            }
        }

        //Dimensions = the difference
        uniformBoundsSize = new Vector2(highestX - lowestX + 1, highestY - lowestY + 1);
        uniformWallCount = Mathf.RoundToInt((uniformBoundsSize.x * 2) + (uniformBoundsSize.y * 2));

        return ret;
    }

    //Every node in a room must be able to reach every other node
    //Since this cycles all nodes, also check for nooks
    private bool RoomSplitCheck(ref List<NewNode> nodes, GenerationDebugController debug)
    {
        if (nodes.Count <= 0) return false;

        List<NewNode> openSet = new List<NewNode>();
        List<NewNode> closedSet = new List<NewNode>();

        openSet.Add(nodes[0]);
        int safety = 9999;

        while (openSet.Count > 0 && safety > 0)
        {
            NewNode current = openSet[0];

            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
            {
                NewNode foundNode = null;

                if (GetAdjacentNode(current, v2, out foundNode))
                {
                    //Matches room
                    if (nodes.Contains(foundNode))
                    {
                        //Isn't in overrides
                        if (!openSet.Contains(foundNode) && !closedSet.Contains(foundNode))
                        {
                            openSet.Add(foundNode);
                        }
                    }
                }
            }

            closedSet.Add(current);
            openSet.RemoveAt(0);
            safety--;
        }

        if (closedSet.Count >= nodes.Count)
        {
            if(debug != null) debug.Log("Passed room split check...");
            return true;
        }
        else
        {
            if (debug != null) debug.Log("Failed room split check...");
            return false;
        }
    }

    //Convert rooms with split nodes into 2 or more seperate rooms
    private List<NewRoom> ConvertSplitRoom(ref HashSet<NewNode> nodes, NewAddress ad)
    {
        List<NewRoom> ret = new List<NewRoom>();
        if (nodes.Count <= 0) return ret;

        List<NewNode> allNodes = new List<NewNode>(nodes);
        List<NewNode> openSet = new List<NewNode>();
        List<NewNode> closedSet = new List<NewNode>();

        NewRoom currentRoom = nodes.FirstOrDefault().room;
        ret.Add(currentRoom);

        int safety2 = 9999;

        while (allNodes.Count > 0 && safety2 > 0)
        {
            openSet.Add(allNodes[0]);
            int safety = 9999;

            while (openSet.Count > 0 && safety > 0)
            {
                NewNode current = openSet[0];

                foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                {
                    NewNode foundNode = null;

                    if (GetAdjacentNode(current, v2, out foundNode))
                    {
                        //Matches room
                        if (nodes.Contains(foundNode))
                        {
                            //Isn't in overrides
                            if (!openSet.Contains(foundNode) && !closedSet.Contains(foundNode))
                            {
                                openSet.Add(foundNode);
                            }
                        }
                    }
                }

                if (current.room != currentRoom)
                {
                    currentRoom.AddNewNode(current);
                }

                allNodes.Remove(current);
                closedSet.Add(current);
                openSet.RemoveAt(0);
                safety--;
            }

            //If this is reached, make a new room
            if (allNodes.Count > 0)
            {
                GameObject newObj = Instantiate(PrefabControls.Instance.room, ad.transform);
                NewRoom newRoom = newObj.GetComponent<NewRoom>();
                newRoom.SetupAll(ad, currentRoom.preset);
                currentRoom = newRoom;
                ret.Add(newRoom);
            }

            safety2--;
        }

        return ret;
    }

    //Pathfind for a hallway using A*. Give weighting to existing rooms
    private List<NewNode> HallwayPathfind(NewNode origin, NewNode destination, NewAddress address)
    {
        // The set of nodes already evaluated.
        Dictionary<NewNode, int> closedSet = new Dictionary<NewNode, int>();

        // The set of currently discovered nodes still to be evaluated.
        // Initially, only the start node is known.
        Dictionary<NewNode, int> openSet = new Dictionary<NewNode, int>(); //Value is the direction - weight is lowered for consecutive directions to avoid zig zagging

        //Find the direction of the entrance door
        NewWall entrance = origin.walls.Find(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance);
        Vector2Int invertedEntranceOffset = new Vector2Int(Mathf.RoundToInt(entrance.wallOffset.x * -2f), Mathf.RoundToInt(entrance.wallOffset.y * -2f));
        int directionIndex = CityData.Instance.offsetArrayX4.ToList().FindIndex(item => item == invertedEntranceOffset);
        //Game.Log("Index for " + invertedEntranceOffset + " is " + directionIndex);
        openSet.Add(origin, directionIndex); //This int correspond to the current direction, which is needed for straight route weighting. This should be equal to the direction of the entrance.

        // For each node, which node it can most efficiently be reached from.
        // If a node can be reached from many nodes, cameFrom will eventually contain the
        // most efficient previous step.
        Dictionary<NewNode, NewNode> cameFrom = new Dictionary<NewNode, NewNode>();

        // For each node, the cost of getting from the start node to that node.
        Dictionary<NewNode, float> fromStartToThis = new Dictionary<NewNode, float>();

        // For each node, the total cost of getting from the start node to the goal
        // by passing by that node. That value is partly known, partly heuristic.
        Dictionary<NewNode, float> fromStartViaThis = new Dictionary<NewNode, float>();

        // The cost of going from start to start is zero.
        if (!fromStartToThis.ContainsKey(origin)) fromStartToThis.Add(origin, 0);
        fromStartToThis[origin] = 0;

        // For the first node, that value is completely heuristic.
        if (!fromStartViaThis.ContainsKey(origin)) fromStartViaThis.Add(origin, Mathf.Infinity);
        fromStartViaThis[origin] = Toolbox.Instance.HeuristicCostEstimate(origin, destination);

        int calculationLoops = 0;

        //while openSet is not empty
        while (openSet.Count > 0)
        {
            //current := the node in openSet having the lowest fScore[] value
            NewNode current = null;

            float lowestFScore = Mathf.Infinity;
            int fromDirection = 0;

            foreach (KeyValuePair<NewNode, int> pair in openSet)
            {
                //If doesn't exist, assume infinity (ie not lower than lowest score)
                if (fromStartViaThis.ContainsKey(pair.Key))
                {
                    if (fromStartViaThis[pair.Key] < lowestFScore)
                    {
                        lowestFScore = fromStartViaThis[pair.Key];
                        current = pair.Key;
                        fromDirection = pair.Value;

                        //Game.Log("Pathfinder: Set current to " + current.parent.pathCoord + " (" + current.parent.locationID + ") Type: " + current.parent.locationType.ToString() + " lobby: " + current.parent.isLobby + " AccessCount: " + current.accessToOtherNodes.Count);
                    }
                }
            }

            //if current == goal then we're done!
            if (current == destination)
            {
                //Build an output data struct to save in the dictionary
                return Toolbox.Instance.ConstructPathAccurate(cameFrom, current);
            }

            openSet.Remove(current);
            closedSet.Add(current, fromDirection);

            //Check directions
            for (int i = 0; i < CityData.Instance.offsetArrayX4.Length; i++)
            {
                NewNode accessNeighbor = null;

                if (GetAdjacentNode(current, CityData.Instance.offsetArrayX4[i], out accessNeighbor))
                {
                    //Must be same address
                    if(accessNeighbor.gameLocation == address)
                    {
                        //Must be same room or of type or existing hallway
                        if (accessNeighbor.room == current.room || (accessNeighbor.room.gameLocation.thisAsAddress != null && accessNeighbor.room.preset == accessNeighbor.room.gameLocation.thisAsAddress.preset.hallway) || accessNeighbor.room.roomType == InteriorControls.Instance.nullRoomType)
                        {
                            //if neighbor in closedSet
                            // Ignore the neighbor which is already evaluated.
                            int b = 0;
                            if (closedSet.TryGetValue(accessNeighbor, out b)) continue;

                            //The path weight from start to the neighbor
                            //Existing hallway carries no weight
                            float weight = 1f;

                            //If from the same direction as previous, lower the weight by 0.5f
                            if (i == fromDirection) weight = 0f;

                            if (accessNeighbor.room.gameLocation.thisAsAddress != null && accessNeighbor.room.preset == accessNeighbor.room.gameLocation.thisAsAddress.preset.hallway) weight = 0;

                            float tentativeGScore = fromStartToThis[current] + weight;

                            //Discover a new node- add to open set
                            if (!openSet.TryGetValue(accessNeighbor, out b))
                            {
                                //Game.Log("Pathfinder: Add " + neighbor.pathCoord + " to open set, count: " + openSet.Count);
                                openSet.Add(accessNeighbor, i);
                            }

                            //Path hasn't been added (assume infinity) if tentative score is lower then this is automatically the best until now.
                            if (!fromStartToThis.ContainsKey(accessNeighbor) || tentativeGScore < fromStartToThis[accessNeighbor])
                            {
                                if (!cameFrom.ContainsKey(accessNeighbor)) cameFrom.Add(accessNeighbor, current);
                                else cameFrom[accessNeighbor] = current;

                                if (!fromStartToThis.ContainsKey(accessNeighbor)) fromStartToThis.Add(accessNeighbor, tentativeGScore);
                                else fromStartToThis[accessNeighbor] = tentativeGScore;

                                if (!fromStartViaThis.ContainsKey(accessNeighbor)) fromStartViaThis.Add(accessNeighbor, fromStartToThis[accessNeighbor] + Toolbox.Instance.HeuristicCostEstimate(accessNeighbor, destination));
                                else fromStartViaThis[accessNeighbor] = fromStartToThis[accessNeighbor] + Toolbox.Instance.HeuristicCostEstimate(accessNeighbor, destination);
                            }
                        }
                    }

                }
            }

            calculationLoops++;

            if (calculationLoops > 999)
            {
                return null;
            }
        }

        return null;
    }

    public void GenerateGeometry(NewAddress ad)
    {
        //Generate random numbers using contained seed system to avoid unreliable replication over threading
        string randomSeed = string.Empty;

        if (SessionData.Instance.isFloorEdit)
        {
            randomSeed = CityData.Instance.seed;

            //Reset furniture
            foreach (NewRoom room in ad.rooms)
            {
                foreach (FurnitureClusterLocation furn in room.furniture)
                {
                    furn.UnloadFurniture(true, FurnitureClusterLocation.RemoveInteractablesOption.remove);
                }

                foreach (NewNode node in room.nodes)
                {
                    foreach (Interactable interact in node.interactables)
                    {
                        interact.DespawnObject();
                    }

                    foreach (NewWall wall in node.walls)
                    {
                        wall.placedWallFurn = false;
                        //wall.individualFurniture = new List<FurnitureLocation>();
                    }

                    node.individualFurniture = new List<FurnitureLocation>();
                    node.interactables = new List<Interactable>();
                    node.SetAllowNewFurniture(true);
                    node.noPassThrough = false;
                    //node.walkableTile = true; //Reset to allow furniture...

                    //...Unless this is an entrance
                    //List<NewWall> entranceWalls = node.walls.FindAll(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance);

                    //if (entranceWalls.Count > 0 && entranceWalls.Exists(item => !item.preset.divider))
                    //{
                    //    node.SetAllowNewFurniture(false);
                    //}

                    //reset frontage
                    foreach (NewWall wall in node.walls)
                    {
                        wall.frontagePresets = new List<NewWall.FrontageSetting>();
                    }
                }

                room.blockedAccess = new Dictionary<NewNode, List<NewNode>>();
                room.furniture = new List<FurnitureClusterLocation>();
                room.individualFurniture = new List<FurnitureLocation>();
                room.specialCaseInteractables = new Dictionary<InteractablePreset.SpecialCase, List<Interactable>>();
            }

            //Remove citizens generated by the editor for this address
            for (int i = 0; i < ad.owners.Count; i++)
            {
                Destroy(ad.owners[i].gameObject);
                ad.owners.RemoveAt(i);
                i--;
            }

            //Generate new fake citizen and move them in...
            GameObject newBlob = Instantiate(PrefabControls.Instance.citizen, FloorEditController.Instance.fakeCitizenHolder);
            Citizen cic = newBlob.GetComponent<Citizen>();
            cic.outfitController.debugOverride = false;
            newBlob.GetComponent<NewAIController>().SetUpdateEnabled(false); //Disable AI

            //Give job
            //cic.SetJob(null);
            cic.SetSexualityAndGender();
            cic.SetPersonality();

            //Move in
            ad.AddOwner(cic);

            cic.SetupGeneral();
            cic.CalculateAge();

            //Pick design style
            ad.UpdateDesignStyle();
        }
        else
        {
            randomSeed = CityData.Instance.seed + ad.id * 2 + ad.building.cityTile.cityCoord.ToString() + ad.id;

            //Reset furniture
            foreach (NewRoom room in ad.rooms)
            {
                foreach (NewNode node in room.nodes)
                {
                    foreach (NewWall wall in node.walls)
                    {
                        wall.placedWallFurn = false;
                        //wall.individualFurniture = new List<FurnitureLocation>();
                    }

                    node.individualFurniture = new List<FurnitureLocation>();
                    node.SetAllowNewFurniture(true);
                    node.noPassThrough = false;
                    //node.walkableTile = true; //Reset to allow furniture...

                    //...Unless this is an entrance
                    //List<NewWall> entranceWalls = node.walls.FindAll(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance);

                    //if (entranceWalls.Count > 0 && entranceWalls.Exists(item => !item.preset.divider))
                    //{
                    //    node.SetAllowNewFurniture(false);
                    //}

                    //reset frontage
                    foreach (NewWall wall in node.walls)
                    {
                        wall.frontagePresets.Clear();
                    }
                }

                room.blockedAccess = new Dictionary<NewNode, List<NewNode>>();
                room.furniture = new List<FurnitureClusterLocation>();
                room.individualFurniture = new List<FurnitureLocation>();
            }

            //Pick design style
            ad.UpdateDesignStyle();
        }

        //Spawn lightswitches
        //Rules: At least one lightswitch located next to doorways, priority given to outside entrances. Additional switches next to other entrances if greater than 3 nodes away.
        foreach (NewRoom room in ad.rooms)
        {
            if (room.preset !=null && !room.preset.useMainLights && !room.preset.useAdditionalAreaLights) continue;

            //Find common rooms
            room.commonRooms.Clear();

            if (room.roomType.shareFeaturesWithCommonAdjacent)
            {
                room.commonRooms = ad.rooms.FindAll(item => item.gameLocation == room.gameLocation && item.roomType == room.roomType);
            }
            else room.commonRooms.Add(room);

            //Reset lightswitches
            for (int i = 0; i < room.lightswitches.Count; i++)
            {
                NewWall lightswitchWall = room.lightswitches[i];
                lightswitchWall.SetAsLightswitch(null);
                i--;
            }

            room.mainLights.Clear();

            //if (!room.preset.useMainLights) continue;
            //if (!room.preset.useLightSwitches) continue;

            //Make sure main entrances are first in this list
            List<NewWall> doorways = new List<NewWall>();

            //Add all doorways from common rooms
            foreach (NewRoom common in room.commonRooms)
            {
                foreach (NewNode nodes in common.nodes)
                {
                    foreach (NewWall wall in nodes.walls)
                    {
                        if (wall.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
                        {
                            //Is this an entrance to the address or entrance into hallway? Insert @ top.
                            if (wall.otherWall.node.gameLocation != room.gameLocation || (wall.otherWall.node.room.gameLocation.thisAsAddress != null && wall.otherWall.node.room.preset == wall.otherWall.node.room.gameLocation.thisAsAddress.preset.hallway))
                            {
                                doorways.Insert(0, wall);
                            }
                            else
                            {
                                doorways.Add(wall);
                            }
                        }
                    }
                }
            }

            //For each doorway, find a wall adjacent to place a lightswitch
            if (room.preset != null && room.preset.useLightSwitches)
            {
                foreach (NewWall doorway in doorways)
                {
                    //Maximum of 2 lightswitches
                    int lightSwitchCount = 0;

                    foreach (NewRoom thisRoom in room.commonRooms)
                    {
                        lightSwitchCount += thisRoom.lightswitches.Count;
                    }

                    if (lightSwitchCount >= 2)
                    {
                        break;
                    }

                    //This doorway must be sufficiently far away from other switches...
                    bool pass = true;

                    foreach (NewRoom thisRoom in room.commonRooms)
                    {
                        foreach (NewWall switchWall in thisRoom.lightswitches)
                        {
                            float dist = Vector2.Distance(switchWall.node.floorCoord, doorway.node.floorCoord);

                            if (dist < 3f)
                            {
                                pass = false;
                                break;
                            }
                        }
                    }

                    if (!pass) continue;

                    List<NewWall> possibleLocations = new List<NewWall>();

                    //List<string> lightswitchDebug = new List<string>();

                    //Phase 1: Check adjacent nodes for walls sharing the same offset
                    if (possibleLocations.Count <= 0)
                    {
                        //lightswitchDebug.Add("Phase 1");

                        for (int i = 0; i < CityData.Instance.offsetArrayX4.Length; i++)
                        {
                            Vector2Int nodeCheck = doorway.node.floorCoord + CityData.Instance.offsetArrayX4[i];
                            NewNode accessNeighbor = null;

                            if (ad.floor.nodeMap.TryGetValue(nodeCheck, out accessNeighbor))
                            {
                                if (accessNeighbor.floorType == NewNode.FloorTileType.floorAndCeiling || accessNeighbor.floorType == NewNode.FloorTileType.floorOnly)
                                {
                                    //Must be same address
                                    if (accessNeighbor.gameLocation == doorway.node.gameLocation)
                                    {
                                        //Must be same room or shared features
                                        if (accessNeighbor.room == doorway.node.room || (doorway.node.room.roomType.shareFeaturesWithCommonAdjacent && accessNeighbor.room.roomType == doorway.node.room.roomType))
                                        {
                                            //If wall exists of the same offset
                                            NewWall sameOffset = accessNeighbor.walls.Find(item => item.wallOffset == doorway.wallOffset && item.preset.supportsWallProps);

                                            if (sameOffset != null)
                                            {
                                                possibleLocations.Add(sameOffset);
                                            }
                                            else
                                            {
                                                //sameOffset = accessNeighbor.walls.Find(item => item.wallOffset == doorway.wallOffset);

                                                //if(sameOffset != null)
                                                //{
                                                //    lightswitchDebug.Add("Same offset found but doesn't support wall props: " + sameOffset.preset.name);
                                                //}
                                                //else lightswitchDebug.Add("No same offset found");
                                            }
                                        }
                                        else
                                        {
                                            //lightswitchDebug.Add("Different room/non-shared features");
                                        }
                                    }
                                    else
                                    {
                                        //lightswitchDebug.Add("Different address");
                                    }
                                }
                                else
                                {
                                    //lightswitchDebug.Add("Invalid floor type");
                                }
                            }
                            else
                            {
                                //lightswitchDebug.Add("No access neighbor found");
                            }
                        }
                    }

                    //Phase 2: Look for other walls on this node (but not opposite entrance)
                    if (possibleLocations.Count <= 0)
                    {
                        //lightswitchDebug.Add("Phase 2");

                        foreach (NewWall wall in doorway.node.walls)
                        {
                            if (wall.wallOffset != (doorway.wallOffset * -1) && wall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance && wall.preset.supportsWallProps)
                            {
                                possibleLocations.Add(wall);
                            }
                            //else
                            //{
                            //    if(wall.wallOffset != (doorway.wallOffset * -1))
                            //    {
                            //        lightswitchDebug.Add("Wall isn't opposite");
                            //    }
                            //}
                        }
                    }

                    //Phase 3: If all else fails then use any wall on adjacent nodes
                    if (possibleLocations.Count <= 0)
                    {
                        for (int i = 0; i < CityData.Instance.offsetArrayX4.Length; i++)
                        {
                            Vector2Int nodeCheck = doorway.node.floorCoord + CityData.Instance.offsetArrayX4[i];
                            NewNode accessNeighbor = null;

                            if (ad.floor.nodeMap.TryGetValue(nodeCheck, out accessNeighbor))
                            {
                                //Must be same address
                                if (accessNeighbor.gameLocation == doorway.node.gameLocation)
                                {
                                    //Must be same room or shared features
                                    if (accessNeighbor.room == doorway.node.room || (doorway.node.room.roomType.shareFeaturesWithCommonAdjacent && accessNeighbor.room.roomType == doorway.node.room.roomType))
                                    {
                                        //If wall exists of the same offset
                                        NewWall sameOffset = accessNeighbor.walls.Find(item => item.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance && item.preset.supportsWallProps);
                                        if (sameOffset != null) possibleLocations.Add(sameOffset);
                                    }
                                }
                            }
                        }
                    }

                    //Finally, place on any wall in the room (nearest entrance)
                    if (possibleLocations.Count <= 0)
                    {
                        float closestDist = Mathf.Infinity;
                        NewWall closestWall = null;

                        foreach (NewNode n in room.nodes)
                        {
                            foreach (NewWall w in n.walls)
                            {
                                if (w.preset.supportsWallProps && w.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                {
                                    float dist = Vector3.Distance(doorway.node.nodeCoord, w.node.nodeCoord);

                                    if (dist < closestDist)
                                    {
                                        closestDist = dist;
                                        closestWall = w;
                                    }
                                }
                            }
                        }

                        if (closestWall != null)
                        {
                            possibleLocations.Add(closestWall);
                        }
                    }

                    //Pick a possible location
                    if (possibleLocations.Count > 0)
                    {
                        NewWall chosenWall = possibleLocations[Toolbox.Instance.RandContained(0, possibleLocations.Count, randomSeed, out randomSeed)];
                        chosenWall.SetAsLightswitch(room);
                    }
                    else
                    {
                        //Game.LogError("No locations for lightswitches in room " + room.preset.name + " Doorways: " + doorways.Count + " Nodes: " + room.nodes.Count + " Address " + room.gameLocation.name);

                        //foreach(string str in lightswitchDebug)
                        //{
                        //    Game.Log(str);
                        //}
                    }
                }
            }

            GenerateLightZones(room);
        }

        //Update the geometry as wall sections may now be divided
        UpdateGeometryFloor(ad.floor);
    }

    public void GenerateLightZones(NewRoom room)
    {
        //Spawn main light(s)
        room.lightZones.Clear(); //Clear existing light zones

        string randomSeed = CityData.Instance.seed + room.roomID * 2 + room.roomFloorID;

        if (room.preset == null || (!room.preset.useMainLights && !room.preset.useAdditionalAreaLights))
        {
            if(room.nodes.Count > 0)
            {
                //Before existing, calculate the middle room position...
                NewNode[] asArray = room.nodes.ToArray();
                NewNode randomLine = asArray[Toolbox.Instance.RandContained(0, asArray.Length, randomSeed, out randomSeed)];
                room.middleRoomPosition = randomLine.position;
            }

            return;
        }

        //Order nodes by lowest x & y values first
        List<NewNode> unassignedNodes = room.nodes.OrderBy(item => item.nodeCoord.x + item.nodeCoord.y).ToList();
        int safety = 9999;

        while (unassignedNodes.Count > 0 && safety > 0)
        {
            NewNode node = unassignedNodes[0];

            List<NewNode> currentRect = new List<NewNode>();

            int axisLimit1 = (int)room.boundsSize.x;
            int axisLimit2 = (int)room.boundsSize.y;

            //Randomly decide whether to expand x or y first
            bool useXFirst = true;

            if (Toolbox.Instance.RandContained(0f, 1f, randomSeed, out randomSeed) > 0.5f)
            {
                useXFirst = false;
                axisLimit1 = (int)room.boundsSize.y;
                axisLimit2 = (int)room.boundsSize.x;
            }

            for (int i = 0; i < axisLimit1; i++)
            {
                for (int u = 0; u < axisLimit2; u++)
                {
                    Vector3 localCoord = node.nodeCoord + new Vector3(i, u, 0);

                    if (!useXFirst)
                    {
                        localCoord = node.nodeCoord + new Vector3(u, i, 0);
                    }

                    NewNode foundNode = unassignedNodes.Find(item => item.nodeCoord == localCoord);

                    //Found this node
                    if (foundNode != null)
                    {
                        currentRect.Add(foundNode);
                        unassignedNodes.Remove(foundNode);
                    }
                    else
                    {
                        //Failed to find a node- skip to next axis
                        axisLimit2 = u; //Set a new limit
                        safety--;
                        break;
                    }
                }

                //When y limit reaches 0 then finish this
                if (axisLimit2 <= 0)
                {
                    break;
                }
            }

            //Save this rect to the room
            if (currentRect.Count > 0) room.lightZones.Add(new NewRoom.LightZoneData(room, currentRect));

            safety--;
        }

        //Sort light zones by their size
        room.lightZones = room.lightZones.OrderBy(item => item.nodeList.Count).ToList();
        room.lightZones.Reverse(); //Largest first

        //The middle should be the largest light zone's middle
        if (room.lightZones.Count > 0)
        {
            //Choose middle of largest zone
            int zoneNodeCount = -1;
            room.middleRoomPosition = room.lightZones[0].centreWorldPosition;

            foreach(NewRoom.LightZoneData lz in room.lightZones)
            {
                if(lz.nodeList.Count > zoneNodeCount)
                {
                    room.middleRoomPosition = lz.centreWorldPosition;
                    zoneNodeCount = lz.nodeList.Count;
                }
            }
        }
        else
        {
            if(room.nodes.Count > 0)
            {
                //Choose a node that's definitely in the room, preferably towards the middle
                NewNode[] asArray = room.nodes.ToArray();
                NewNode randomLine = asArray[Toolbox.Instance.RandContained(0, asArray.Length, randomSeed, out randomSeed)];
                room.middleRoomPosition = randomLine.position;
            }
        }
    }

    public void GenerateAddressDecor(NewAddress ad)
    {
        if(!SessionData.Instance.isFloorEdit && !ad.generatedRoomConfigs)
        {
            //Game.Log("Generating room configs first...");
            ad.GenerateRoomConfigs();
        }

        ad.RemoveAllInhabitantFurniture(true, FurnitureClusterLocation.RemoveInteractablesOption.remove);

        //Generate random numbers using contained seed system to avoid unreliable replication over threading
        string randomSeed = string.Empty;

        if(SessionData.Instance.isFloorEdit)
        {
            randomSeed = Toolbox.Instance.GenerateUniqueID();
        }
        else
        {
            randomSeed = CityData.Instance.seed + (ad.id * 2 + ad.id);
        }

        //Furnish room
        if (!Game.Instance.disableFurniture)
        {
            List<NewRoom> roomDecoration = new List<NewRoom>(ad.rooms);

            try
            {
                roomDecoration.Sort((p1, p2) => p2.preset.decorationPriority.CompareTo(p1.preset.decorationPriority)); //P2 first = highest first
            }
            catch
            {

            }

            foreach (NewRoom room in roomDecoration)
            {
                if (SessionData.Instance.isFloorEdit) room.seed = Toolbox.Instance.GenerateUniqueID();
                FurnishRoom(room);

                foreach (NewNode node in room.nodes)
                {
                    //Does this feature a stairwell?
                    if (node.tile.isStairwell)
                    {
                        room.featuresStairwell = true;
                    }

                    node.SetAllowNewFurniture(true);
                    node.noPassThrough = false;

                    foreach (NewWall wall in node.walls)
                    {
                        wall.SelectFrontage();

                        //Paint doors
                        if (wall.door != null && !SessionData.Instance.isFloorEdit)
                        {
                            wall.door.SelectColouring();
                        }
                    }
                }

                //Spawn lights in the centre of their zoned area
                //Pick a light model for this room to use (check for already assigned models)
                NewRoom roomWithPreset = room.commonRooms.Find(item => item.mainLightPreset != null);
                bool foundLightPreset = false;

                if (roomWithPreset != null)
                {
                    room.mainLightPreset = roomWithPreset.mainLightPreset; //Copy from existing room

                    //Check compatiblity...
                    if (room.mainLightPreset.stairwellRule != RoomLightingPreset.StairwellLightRule.either)
                    {
                        if (room.featuresStairwell && room.mainLightPreset.stairwellRule == RoomLightingPreset.StairwellLightRule.noStairwells)
                        {
                            room.mainLightPreset = null;
                        }
                        else if (!room.featuresStairwell && room.mainLightPreset.stairwellRule == RoomLightingPreset.StairwellLightRule.onlyStairwells)
                        {
                            room.mainLightPreset = null;
                        }
                    }

                    if (room.mainLightPreset != null)
                    {
                        foundLightPreset = true; //Successfully coppied light preset
                    }
                }

                if (!foundLightPreset)
                {
                    List<RoomLightingPreset> validRoomLights = new List<RoomLightingPreset>();
                    List<RoomLightingPreset> validAtriumLights = new List<RoomLightingPreset>();

                    foreach (RoomLightingPreset rlp in Toolbox.Instance.allRoomLighting)
                    {
                        if (rlp.disable) continue;
                        if (!rlp.roomCompatibility.Contains(room.preset)) continue; //Room compatibility
                        if (!rlp.designStyleCompatibility.Contains(room.gameLocation.designStyle)) continue; //Design compatibility
                        if (room.building.preset != null && rlp.banFromBuildings.Contains(room.building.preset)) continue;
                        if (room.building.preset != null && rlp.onlyAllowInBuildings.Count > 0 && !rlp.onlyAllowInBuildings.Contains(room.building.preset)) continue;
                        if (room.building.preset == null && rlp.onlyAllowInBuildings.Count > 0) continue;
                        if (room.nodes.Count < rlp.minimumRoomSize) continue;
                        if (room.nodes.Count > rlp.maximumRoomSize) continue;

                        if (rlp.stairwellRule != RoomLightingPreset.StairwellLightRule.either)
                        {
                            if (room.featuresStairwell && rlp.stairwellRule == RoomLightingPreset.StairwellLightRule.noStairwells)
                            {
                                //if (Game.Instance.devMode) room.debugLightswitches.Add("Room discounted " + rlp.name + " because features stairwell and rule is " + rlp.stairwellRule);
                                continue;
                            }
                            else if (!room.featuresStairwell && rlp.stairwellRule == RoomLightingPreset.StairwellLightRule.onlyStairwells)
                            {
                                //if (Game.Instance.devMode) room.debugLightswitches.Add("Room discounted " + rlp.name + " because DOESN'T feature stairwell and rule is " + rlp.stairwellRule);
                                continue;
                            }
                        }

                        //if (Game.Instance.devMode)
                        //{
                        //    //room.debugLightswitches.Add("Room allowed " + rlp.name + " stairwell: " + rlp.stairwellRule + "(" + room.featuresStairwell + ")");

                        //    foreach (NewNode n in room.nodes)
                        //    {
                        //        if (n.tile.isStairwell)
                        //        {
                        //            //room.debugLightswitches.Add("Node features stairwell in this room");
                        //        }
                        //    }
                        //}

                        if (!rlp.lightingPreset.isAtriumLight)
                        {
                            for (int i = 0; i < rlp.frequency; i++)
                            {
                                validRoomLights.Add(rlp);
                            }
                        }
                        //Atrium light compatibility
                        else
                        {
                            //Scan down for supported floor count
                            bool compatible = false;

                            foreach (NewNode n1 in room.nodes)
                            {
                                if (n1.floorType == NewNode.FloorTileType.CeilingOnly)
                                {
                                    Vector3Int searchNode = Vector3Int.zero;
                                    bool downSearchCompatible = false;

                                    for (int i = 1; i < rlp.lightingPreset.minimumFloors; i++)
                                    {
                                        searchNode = n1.nodeCoord - new Vector3Int(0, 0, i);
                                        NewNode foundNode = null;

                                        if (PathFinder.Instance.nodeMap.TryGetValue(searchNode, out foundNode))
                                        {
                                            //Empty gap
                                            if (foundNode.floorType != NewNode.FloorTileType.floorOnly && foundNode.floorType != NewNode.FloorTileType.floorAndCeiling)
                                            {
                                                downSearchCompatible = true;
                                            }
                                            else
                                            {
                                                downSearchCompatible = false;
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            downSearchCompatible = false;
                                            break;
                                        }
                                    }

                                    //We've found some compatibility here...
                                    if(downSearchCompatible)
                                    {
                                        compatible = true;
                                        break;
                                    }
                                }
                            }

                            if(compatible)
                            {
                                for (int i = 0; i < rlp.frequency; i++)
                                {
                                    validAtriumLights.Add(rlp);
                                }
                            }
                        }
                    }

                    //Are atrium lights present? If so, check to see if this room can support an atrium light...
                    //To support an atrium light, there must be a gap of at least 2 floors...
                    bool atriumSupported = true;

                    //Below is no longer needed as it performs this before adding to the 'valid' list
                    //if (validAtriumLights.Count > 0)
                    //{
                    //    foreach (NewNode n1 in room.nodes)
                    //    {
                    //        if (n1.floorType == NewNode.FloorTileType.CeilingOnly)
                    //        {
                    //            Vector3Int searchNode = n1.nodeCoord - new Vector3Int(0, 0, 1);
                    //            NewNode foundNode = null;

                    //            if (PathFinder.Instance.nodeMap.TryGetValue(searchNode, out foundNode))
                    //            {
                    //                if (foundNode.floorType == NewNode.FloorTileType.noneButIndoors)
                    //                {
                    //                    searchNode = searchNode - new Vector3Int(0, 0, 1);

                    //                    if (PathFinder.Instance.nodeMap.TryGetValue(searchNode, out foundNode))
                    //                    {
                    //                        if (foundNode.floorType == NewNode.FloorTileType.noneButIndoors || foundNode.floorType == NewNode.FloorTileType.floorOnly)
                    //                        {
                    //                            atriumSupported = true;
                    //                            break;
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}

                    //If it can, remove any non-atrium lights...
                    if (atriumSupported && validAtriumLights.Count > 0)
                    {
                        room.mainLightPreset = validAtriumLights[Toolbox.Instance.RandContained(0, validAtriumLights.Count, randomSeed, out randomSeed)];
                        foundLightPreset = true;
                    }
                    //If it can't, remove any atrium lights
                    else if (validRoomLights.Count <= 0)
                    {
                        //This is only worth communicating for non-null rooms...
                        if (!room.isNullRoom)
                        {
                            Game.Log("CityGen: There are no valid room lights for " + room.roomID);
                        }
                    }
                    else
                    {
                        room.mainLightPreset = validRoomLights[Toolbox.Instance.RandContained(0, validRoomLights.Count, randomSeed, out randomSeed)];
                        foundLightPreset = true;
                    }
                }

                //Feature ceiling fans
                if (room.mainLightPreset != null)
                {
                    if (room.mainLightPreset.ceilingFans.Count > 0)
                    {
                        if (Toolbox.Instance.RandContained(0f, 1f, randomSeed, out randomSeed) <= room.preset.chanceOfCeilingFans)
                        {
                            room.ceilingFans = Toolbox.Instance.RandContained(0, room.mainLightPreset.ceilingFans.Count, randomSeed, out randomSeed);
                        }
                    }
                }

                //Moved to post interior generation so it can run on the main thread...
                //Create lights...
                //if (!SessionData.Instance.isFloorEdit)
                //{
                //    for (int i = 0; i < room.lightZones.Count; i++)
                //    {
                //        NewRoom.LightZoneData lightZone = room.lightZones[i];

                //        if (lightZone.nodeList == null || lightZone.nodeList.Count <= 0)
                //        {
                //            room.lightZones.Remove(lightZone);
                //            i--;
                //            continue;
                //        }

                //        //Is the middle far enough away from another light source?
                //        bool distanceCheck = true;

                //        foreach (Interactable light in room.mainLights)
                //        {
                //            if (Vector3.Distance(light.node.position, lightZone.centreWorldPosition) < InteriorControls.Instance.lightZoneMinDistance)
                //            {
                //                distanceCheck = false;
                //                break;
                //            }
                //        }

                //        if (!distanceCheck)
                //        {
                //            continue; //If distance check fails, move to next light zone
                //        }

                //        //You now have everything you need to spawn a light!
                //        //Do this for main lights as they are interactables that will be saved. Area lights are created on spawn of geometry.
                //        lightZone.CreateMainLight();
                //    }
                //}
            }
        }
    }

    public void FurnishRoom(NewRoom room)
    {
        System.Diagnostics.Stopwatch stopWatch = null;

        if (Game.Instance.devMode && Game.Instance.collectDebugData && CityConstructor.Instance != null && CityConstructor.Instance.debugLoadTime != null)
        {
            stopWatch = new System.Diagnostics.Stopwatch();
            stopWatch.Start();

            //if (room.gameLocation.thisAsAddress != null)
            //{
            //    Game.Log("CityGen: Room key at start for room " + room.roomID + ": " + room.seed + "(Address: " + room.gameLocation.thisAsAddress.seed +")");
            //}
            //else
            //{
            //    Game.Log("CityGen: Room key at start for room " + room.roomID + ": " + room.seed + "(street)");
            //}
        }

        room.furnitureGroups.Clear(); //Reset furniture groups

        if(room.gameLocation.thisAsAddress != null && !room.gameLocation.thisAsAddress.generatedRoomConfigs)
        {
            //Game.Log("Generating room configs...");
            room.gameLocation.thisAsAddress.GenerateRoomConfigs();
        }

        // Also get the furniture clusters...
        List<ClusterRank> compatibleClusters = new List<ClusterRank>();

        //List of rooms this represents
        List<RoomConfiguration> roomTypes = new List<RoomConfiguration>();

        //Make sure all room types are unique in this list
        if(!roomTypes.Contains(room.preset)) roomTypes.Add(room.preset);

        foreach(RoomConfiguration cf in room.openPlanElements)
        {
            if(!roomTypes.Contains(cf))
            {
                roomTypes.Add(cf);
            }
        }

        //Essential furniture
        List<ClusterRank> essential = new List<ClusterRank>();

        foreach (RoomConfiguration config in roomTypes)
        {
            foreach (FurnitureCluster cluster in Toolbox.Instance.allFurnitureClusters)
            {
                if (cluster == null) continue;
                if (cluster.disable) continue;

                //Already contains cluster
                if (essential.Exists(item => item.cluster == cluster)) continue;
                if (compatibleClusters.Exists(item => item.cluster == cluster)) continue;

                if (cluster.enableDebug) Game.Log("Cluster check for " + cluster.presetName + " in ID " + room.roomID + "...");

                if (room.nodes.Count < Mathf.Max(cluster.minimumRoomSize, cluster.calculatedMinRoomSize))
                {
                    if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed room size check for room  ID " + room.roomID + " (too small)");
                    continue;
                }

                if (cluster.useMaximumRoomSize && room.nodes.Count > cluster.maximumRoomSize)
                {
                    if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed room size check for room  ID " + room.roomID + " (too big)");
                    continue;
                }

                //Removed this as I wasn't using it...
                //if(cluster.useMinimumTesselation)
                //{
                //    //First check the valid nodes we have pass requirements for this room...
                //    if (!RoomMinimumShapeCheck(ref room.nodes, cluster.minimumTesselation, null, cluster.nodesMustBeUnoccupied))
                //    {
                //        if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed tesselation check for room  ID " + room.roomID + " " + cluster.minimumTesselation);
                //        continue;
                //    }
                //}

                if (cluster.limitToFloor)
                {
                    if (room.floor != null && room.floor.floor != cluster.allowedOnFloor)
                    {
                        if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed floor check for room ID " + room.roomID);
                        continue;
                    }
                }
                else if(cluster.limitToFloorRange)
                {
                    if (room.floor != null && (room.floor.floor < cluster.allowedOnFloorRange.x || room.floor.floor > cluster.allowedOnFloorRange.y))
                    {
                        if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed floor check for room ID " + room.roomID);
                        continue;
                    }
                }

                if (room.gameLocation.thisAsAddress != null)
                {
                    if (!cluster.allowInResidential && room.gameLocation.thisAsAddress.residence != null)
                    {
                        if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed residence check for room ID " + room.roomID);
                        continue;
                    }

                    if (!cluster.allowInCompanies && room.gameLocation.thisAsAddress.company != null && !room.gameLocation.thisAsAddress.company.preset.isSelfEmployed)
                    {
                        if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed company check for room ID " + room.roomID);
                        continue;
                    }

                    //Skip if no inhabitants
                    if (!SessionData.Instance.isFloorEdit && cluster.skipIfNoAddressInhabitants && room.gameLocation.thisAsAddress.inhabitants.Count <= 0)
                    {
                        if(!cluster.onlySkipNoInhabitantsIfResidenceOrCompany || (room.gameLocation.thisAsAddress.company != null || room.gameLocation.thisAsAddress.residence != null))
                        {
                            if(!cluster.dontSkipNoInhabitantsIfIn.Contains(room.preset.roomClass))
                            {
                                if(room.gameLocation.thisAsAddress.residence == null || room.gameLocation.thisAsAddress.residence.preset == null || !room.gameLocation.thisAsAddress.residence.preset.furnitureIfUnihabited)
                                {
                                    if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed inhabitants check for roomID " + room.roomID);
                                    continue;
                                }
                            }
                        }
                    }
                }
                else if (!cluster.allowOnStreets && room.gameLocation.thisAsStreet != null)
                {
                    if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed street check for room ID " + room.roomID);
                    continue;
                }

                if(!SessionData.Instance.isFloorEdit)
                {
                    if (cluster.limitToDistricts)
                    {
                        if(!cluster.allowedInDistricts.Contains(room.gameLocation.district.preset))
                        {
                            if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed district limit check for room ID " + room.roomID);
                            continue;
                        }
                    }

                    if(cluster.banFromDistricts)
                    {
                        if (cluster.notAllowedInDistricts.Contains(room.gameLocation.district.preset))
                        {
                            if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed district ban check for room ID " + room.roomID);
                            continue;
                        }
                    }
                }

                //Apply limits outside of this room...
                if (!ClusterCountChecks(cluster, room, cluster.enableDebug))
                {
                    if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed cluster counts check for room ID " + room.roomID);
                    continue;
                }

                //Security door
                if (cluster.securityDoor)
                {
                    if (config.securityDoors == RoomConfiguration.SecurityDoorRule.never)
                    {
                        if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed security door check for room ID " + room.roomID);
                        continue;
                    }
                }

                //Breaker box
                if(cluster.isBreakerBox)
                {
                    if (room.gameLocation.thisAsAddress != null && room.gameLocation.thisAsAddress.addressPreset != null)
                    {
                        //Does this address need it's own breaker box?
                        if (room.gameLocation.thisAsAddress.addressPreset.useOwnBreakerBox)
                        {
                            if (room.gameLocation.thisAsAddress.breakerLightsID >= 0) continue; //Already placed...
                        }
                        //Does this floor need a breaker box?
                        else
                        {
                            if (room.gameLocation.floor != null)
                            {
                                if (room.gameLocation.floor.breakerLightsID >= 0) continue; //Already placed
                            }
                            else continue;
                        }
                    }
                    else continue;
                }

                //Suitable for this room?
                bool roomSuitabilityPass = false;
                bool openPlanPass = false;

                //Not allowed in open plan
                if (cluster.allowedInOpenPlan == FurnitureCluster.AllowedOpenPlan.no && room.openPlanElements.Count > 0)
                {
                    if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed open plan check for room ID " + room.roomID);
                    continue;
                }

                foreach (RoomTypeFilter filter in cluster.allowedRoomFilters)
                {
                    if (filter == null || filter.roomClasses == null)
                    {
                        Game.LogError("Missing room filter in " + cluster.presetName);
                        continue;
                    }

                    //Matching
                    foreach (RoomConfiguration rt in roomTypes)
                    {
                        if(rt == null)
                        {
                            continue;
                        }

                        if (filter.roomClasses.Contains(rt.roomClass))
                        {
                            //Check for open plan
                            if (!openPlanPass)
                            {
                                if (cluster.allowedInOpenPlan == FurnitureCluster.AllowedOpenPlan.yes)
                                {
                                    openPlanPass = true;
                                }
                                else
                                {
                                    if (room.openPlanElements.Contains(rt))
                                    {
                                        if (cluster.allowedInOpenPlan == FurnitureCluster.AllowedOpenPlan.openPlanOnly)
                                        {
                                            openPlanPass = true;
                                        }
                                    }
                                    else if (cluster.allowedInOpenPlan == FurnitureCluster.AllowedOpenPlan.no)
                                    {
                                        openPlanPass = true;
                                    }
                                }
                            }

                            roomSuitabilityPass = true;

                            //Possible early breakage
                            if(roomSuitabilityPass)
                            {
                                if(openPlanPass && (cluster.allowedInOpenPlan == FurnitureCluster.AllowedOpenPlan.yes || cluster.allowedInOpenPlan == FurnitureCluster.AllowedOpenPlan.openPlanOnly))
                                {
                                    break;
                                }
                            }
                        }
                    }
                }

                if (!openPlanPass)
                {
                    if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed open plan check for room ID " + room.roomID);
                    continue;
                }
                if (!roomSuitabilityPass)
                {
                    if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed room suitability check for room ID " + room.roomID);
                    continue;
                }

                //Skipping altogether chance
                float placeChance = cluster.placementChance;

                if (cluster.modifyPlacementChanceTraits.Count > 0)
                {
                    //Use an average of everyone who lives here or whoever the room belongs to.
                    List<Human> useTraits = new List<Human>(room.belongsTo);

                    if (room.gameLocation.thisAsAddress != null)
                    {
                        if (room.gameLocation.thisAsAddress.residence != null)
                        {
                            foreach (Human h in room.gameLocation.thisAsAddress.inhabitants)
                            {
                                if (!useTraits.Contains(h))
                                {
                                    useTraits.Add(h);
                                }
                            }
                        }
                    }

                    float averageMod = 0f;

                    foreach (Human h in useTraits)
                    {
                        //Calculate base chance...
                        averageMod += h.GetChance(ref cluster.modifyPlacementChanceTraits, 0f);
                    }

                    if (averageMod > 0f) averageMod /= (float)useTraits.Count;
                    placeChance += averageMod; //Add to rank
                    placeChance = Mathf.Clamp01(placeChance);
                }

                string r = room.seed + cluster.presetName;

                if (Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, r) > placeChance)
                {
                    if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " failed placement chance check for room ID " + room.roomID);
                    continue;
                }

                //Calculate modified rank
                float modRank = cluster.roomPriority + Toolbox.Instance.GetPsuedoRandomNumber(0f, 0.05f, r);

                if (cluster.modifyPriorityTraits.Count > 0)
                {
                    //Use an average of everyone who lives here or whoever the room belongs to.
                    List<Human> useTraits = new List<Human>(room.belongsTo);

                    if (room.gameLocation.thisAsAddress != null)
                    {
                        if (room.gameLocation.thisAsAddress.residence != null)
                        {
                            foreach (Human h in room.gameLocation.thisAsAddress.inhabitants)
                            {
                                if (!useTraits.Contains(h))
                                {
                                    useTraits.Add(h);
                                }
                            }
                        }
                    }

                    float averageMod = 0f;

                    foreach (Human h in useTraits)
                    {
                        //Calculate base chance...
                        averageMod += h.GetChance(ref cluster.modifyPriorityTraits, 0f) * 10f;
                    }

                    averageMod /= (float)useTraits.Count;
                    modRank += averageMod; //Add to rank
                }

                if (cluster.essentialFurniture)
                {
                    if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " added to essential furniture for ID " + room.roomID);
                    essential.Add(new ClusterRank { cluster = cluster, rank = modRank });
                }
                else
                {
                    if (cluster.enableDebug) Game.Log("Cluster " + cluster.presetName + " added to compatible furniture for ID " + room.roomID);
                    compatibleClusters.Add(new ClusterRank { cluster = cluster, rank = modRank });
                }

                //Add this again to try and place 2 in the room
                //compatibleClusters.Insert(Toolbox.Instance.RandContained(0, compatibleClusters.Count, room.randomSeed, out room.randomSeed), new ClusterRank { cluster = cluster, rank = modRank });
            }
        }

        if (compatibleClusters.Count <= 0 && essential.Count <= 0)
        {
            //Game.Log("Room furnishing: No compatible clusters for " + room.name);
            return;
        }

        if (room.entrances.Count <= 0)
        {
            if (room.preset != CityControls.Instance.nullDefaultRoom)
            {
                if (room.floor != null)
                {
                    Game.LogError("Room " + room.roomID + ", floor " + room.floor.floorName + " has no entrances! Cannot furnish...");
                }
                else
                {
                    Game.LogError("Room " + room.roomID + ", has no entrances! Cannot furnish...");
                }
            }

            Game.LogError("Room " + room.roomID + ", has no entrances! Cannot furnish...");

            return;
        }

        //List clusters by rank
        if (compatibleClusters.Count > 0)
        {
            compatibleClusters.Sort((p1, p2) => ((float)p2.rank).CompareTo((float)p1.rank)); //Using P2 first gives highest first
            //if (compatibleClusters.Count >= 2) Game.Log("first = " + compatibleClusters[0].roomPriority + " second = " + compatibleClusters[1].roomPriority);
        }

        if (essential.Count > 0)
        {
            essential.Sort((p1, p2) => ((float)p2.rank).CompareTo((float)p1.rank)); //Using P2 first gives highest first
        }

        //Game.Log("Sorting check, top of list is " + compatibleClusters[0].name);

        //Anylise cluster placements...
        //Gather possible locations...
        List<FurnitureClusterLocation> possibleLocations = null;

        int remainingfurnitureClusters = 42;

        if (room.preset.overrideMaxFurnitureClusters)
        {
            remainingfurnitureClusters = room.preset.overridenMaxFurniture;
        }
        else
        {
            float attemptsPerNode = InteriorControls.Instance.roomSizeClusterAttemptMultiplier;
            if (room.preset.overrideAttemptsPerNodeMultiplier) attemptsPerNode = room.preset.overridenAttemptsPerNode;

            remainingfurnitureClusters = Mathf.CeilToInt(attemptsPerNode * room.nodes.Count);
        }

        //Dictionary for keeping track of how many of each item are placed...
        Dictionary<FurnitureCluster, int> furnitureCounts = new Dictionary<FurnitureCluster, int>();

        //Loop through ranked clusters in turn in order to place them
        while (remainingfurnitureClusters > 0 && (compatibleClusters.Count > 0 || essential.Count > 0))
        {
            ClusterRank cluster;

            if (essential.Count > 0)
            {
                cluster = essential[0];
                essential.RemoveAt(0);

                if (cluster.cluster.enableDebug)
                {
                    Game.Log("...Placing essential cluster " + cluster.cluster.presetName + " in ID " + room.roomID + "...");
                }
            }
            else
            {
                cluster = compatibleClusters[0];
                compatibleClusters.RemoveAt(0);

                if (cluster.cluster.enableDebug)
                {
                    Game.Log("...Placing non-essential cluster " + cluster.cluster.presetName + " in ID " + room.roomID + "...");
                }
            }

            if(cluster.cluster == null)
            {
                Game.LogError("Null cluster detected, this shouldn't be happening...");
                if (essential.Count > 0) essential.RemoveAll(item => item.cluster == cluster.cluster);
                if (compatibleClusters.Count > 0) compatibleClusters.RemoveAll(item => item.cluster == cluster.cluster);

                continue;
            }

            //If counts have been reached...
            if (cluster.cluster.limitPerRoom)
            {
                int currentFurniureCount = 0;
                furnitureCounts.TryGetValue(cluster.cluster, out currentFurniureCount);

                if (currentFurniureCount >= cluster.cluster.maximumPerRoom)
                {
                    if (cluster.cluster.enableDebug)
                    {
                        Game.LogError("...Maximum furniture count reached for " + cluster.cluster.presetName + " in ID " + room.roomID);
                    }

                    if (essential.Count > 0) essential.RemoveAll(item => item.cluster == cluster.cluster);
                    if (compatibleClusters.Count > 0) compatibleClusters.RemoveAll(item => item.cluster == cluster.cluster);

                    continue; //Maximum has been reached
                }
            }

            if (!ClusterCountChecks(cluster.cluster, room, cluster.cluster.enableDebug))
            {
                if (cluster.cluster.enableDebug)
                {
                    Game.LogError("...Cluster count check for " + cluster.cluster.presetName + " in ID " + room.roomID + " has failed...");
                }

                if (essential.Count > 0) essential.RemoveAll(item => item.cluster == cluster.cluster);
                if (compatibleClusters.Count > 0) compatibleClusters.RemoveAll(item => item.cluster == cluster.cluster);

                continue; //Do count checks
            }

            FurnitureClusterLocation chosenLoc = GetBestFurnitureClusterLocation(room, cluster.cluster, cluster.cluster.enableDebug);

            //if(cluster.cluster.name == "ShantyShack")
            //{
            //    Game.Log("Possible locations for shanty: " + possibleLocations.Count);
            //}

            //If no possible locations, remove
            if (chosenLoc == null)
            {
                if (essential.Count > 0) essential.RemoveAll(item => item.cluster == cluster.cluster);
                if (compatibleClusters.Count > 0) compatibleClusters.RemoveAll(item => item.cluster == cluster.cluster);

                foreach (FurnitureCluster cl in cluster.cluster.removeClustersOnFail)
                {
                    if (essential.Count > 0) essential.RemoveAll(item => item.cluster == cl);
                    if (compatibleClusters.Count > 0) compatibleClusters.RemoveAll(item => item.cluster == cl);
                }

                if (cluster.cluster.enableDebug)
                {
                    Game.LogError("Unable to place furniture cluster " + cluster.cluster.presetName + " in ID " + room.roomID + " (0 valid locations found)");
                }

                continue;
            }
            else
            {
                if (cluster.cluster.enableDebug && possibleLocations != null && cluster.cluster != null && room != null)
                {
                    Game.Log( possibleLocations.Count + " locations for cluster " + cluster.cluster.presetName + " in room ID " + room.roomID);
                }

                //Rank and pick highest
                //possibleLocations.Sort((p1, p2) => p2.ranking.CompareTo(p1.ranking));//Highest first

                //Add
                //FurnitureClusterLocation chosenLoc = possibleLocations[0];

                room.AddFurniture(chosenLoc, true);
                remainingfurnitureClusters--;

                if(Game.Instance.devMode && Game.Instance.collectDebugData)
                {
                    room.clustersPlaced += "(" + cluster.rank + " " + essential.Count + "/" + compatibleClusters.Count + ")";
                }

                if (!furnitureCounts.ContainsKey(chosenLoc.cluster))
                {
                    furnitureCounts.Add(chosenLoc.cluster, 1);
                }
                else furnitureCounts[chosenLoc.cluster]++;

                //Add at bottom of list
                if(!cluster.cluster.limitPerRoom || furnitureCounts[chosenLoc.cluster] < cluster.cluster.maximumPerRoom)
                {
                    if(!compatibleClusters.Contains(cluster))
                    {
                        compatibleClusters.Add(cluster);
                    }
                }

                //Add on success (helps optimization)
                foreach (FurnitureCluster cl in cluster.cluster.addClustersOnSuccess)
                {
                    if (cl.enableDebug)
                    {
                        Game.Log(cl.presetName + " is being added as an option for ID " + room.roomID + " becuase of successful placement of " + cluster.cluster.presetName);
                    }

                    ClusterRank newF = new ClusterRank { cluster = cl, rank = cl.roomPriority };

                    if (cl.essentialFurniture && !essential.Exists(item => item.cluster == cl))
                    {
                        essential.Add(newF);
                    }
                    else if(!compatibleClusters.Exists(item => item.cluster == cl))
                    {
                        compatibleClusters.Add(newF);
                    }
                }

                //Remove on success (helps optimization)
                foreach (FurnitureCluster cl in cluster.cluster.removeClustersOnSuccess)
                {
                    if(cl.enableDebug)
                    {
                        Game.Log(cl.presetName + " is being removed as an option for ID " + room.roomID + " becuase of successful placement of " + cluster.cluster.presetName);
                    }

                    if (essential.Count > 0) essential.RemoveAll(item => item.cluster == cl);
                    if(compatibleClusters.Count > 0) compatibleClusters.RemoveAll(item => item.cluster == cl);
                }
            }
        }

        //If floor edit, switch on lights
        if (SessionData.Instance.isFloorEdit)
        {
            room.SetMainLights(true, "FloorEdit", forceUpdate: true);
            room.ConnectNodes();
        }

        if (room.pickFurnitureCache != null) room.pickFurnitureCache = null;

        //Record timing
        if (Game.Instance.devMode && Game.Instance.collectDebugData && CityConstructor.Instance != null && CityConstructor.Instance.debugLoadTime != null)
        {
            stopWatch.Stop();
            float seconds = (float)stopWatch.Elapsed.TotalSeconds;

            //if(room.gameLocation.thisAsAddress != null)
            //{
            //    Game.Log("CityGen: Room key at end for room " + room.roomID + ": " + room.seed + " (AdDress: " + room.gameLocation.thisAsAddress.seed + ")");
            //}
            //else
            //{
            //    Game.Log("CityGen: Room key at end for room " + room.roomID + ": " + room.seed + " (Street)");
            //}
        }
    }

    private bool ClusterCountChecks(FurnitureCluster cluster, NewRoom room, bool enableDebug = false)
    {
        if (cluster.limitPerAddress)
        {
            if (GetClustersInGameLocation(room.gameLocation, cluster).Count >= cluster.maximumPerAddress)
            {
                if(enableDebug) Game.Log("... Limit per address reached");
                return false;
            }
        }

        //if (cluster.limitPerBuilding && room.building != null)
        //{
        //    if (GetClustersInBuilding(room.building, cluster).Count >= cluster.maximumPerBuilding)
        //    {
        //        if (enableDebug) Game.Log("... Limit per building reached");
        //        return false;
        //    }
        //}

        //if (cluster.limitPerCity)
        //{
        //    if (GetClustersInCity(cluster).Count >= cluster.maximumPerCity)
        //    {
        //        if (enableDebug) Game.Log("... Limit per city reached");
        //        return false;
        //    }
        //}

        if(cluster.wealthLimit)
        {
            float lv = Toolbox.Instance.GetNormalizedLandValue(room.gameLocation);

            if (lv < cluster.minimumWealth || lv > cluster.maximumWealth)
            {
                if (enableDebug) Game.Log("... Wealth limit reached");
                return false;
            }
        }

        if (cluster.useRoomGrub)
        {
            float grub = room.defaultWallKey.grubiness;

            if (grub < cluster.minimumGrub || grub > cluster.maximumGrub)
            {
                if (enableDebug) Game.Log("... Grub limit reached");
                return false;
            }
        }

        if (cluster.useBuildingResidences)
        {
            if (room.building == null)
            {
                if (enableDebug) Game.Log("... Building residences: No building found");
                return false;
            }

            //Count all residences in building...
            int resCount = 0;

            foreach(KeyValuePair<int, NewFloor> pair in room.building.floors)
            {
                resCount += pair.Value.addresses.FindAll(item => item.residence != null).Count;
            }

            if (resCount < cluster.minimumResidences || resCount > cluster.maximumResidences)
            {
                if (enableDebug) Game.Log("... Building residence limit reached");
                return false;
            }
        }

        return true;
    }

    public List<FurnitureClusterLocation> GetPossibleFurnitureClusterLocations(NewRoom room, FurnitureCluster cluster, bool enableDebug = false)
    {
        //Collect possible locations and rotations
        List<FurnitureClusterLocation> ret = new List<FurnitureClusterLocation>();

        List<FurnitureCluster.FurnitureClusterRule> validElements = new List<FurnitureCluster.FurnitureClusterRule>();

        Dictionary<FurnitureClass, List<FurnitureLocation>> existingElementsRoom = null;
        Dictionary<FurnitureClass, List<FurnitureLocation>> existingElementsAddress = null;
        //Dictionary<FurnitureClass, List<FurnitureLocation>> existingElementsBuilding = null;
        //Dictionary<FurnitureClass, List<FurnitureLocation>> existingElementsCity = null;

        //Scan through non location-based limitations for placing various elements within this cluster to cut down on the calls when we position things...
        for (int l = 0; l < cluster.clusterElements.Count; l++)
        {
            FurnitureCluster.FurnitureClusterRule element = cluster.clusterElements[l];
            if (enableDebug) Game.Log("Searching placements for element " + (l + 1) +"/" + cluster.clusterElements.Count + "...");

            bool valid = true;

            if (element == null || element.furnitureClass == null)
            {
                valid = false;
                Game.LogError("Null furniture class found in cluster " + cluster.presetName + " index " + l);
                continue;
            }

            //Check for maximum count...
            if (element.furnitureClass.limitPerRoom)
            {
                if (existingElementsRoom == null) existingElementsRoom = new Dictionary<FurnitureClass, List<FurnitureLocation>>();
                int classCount = 0;

                if(existingElementsRoom.ContainsKey(element.furnitureClass))
                {
                    classCount = existingElementsRoom[element.furnitureClass].Count;
                }
                else
                {
                    List<FurnitureLocation> furnitureInRoom = GetFurnitureInRoom(room, element.furnitureClass);
                    classCount += furnitureInRoom.Count;
                    existingElementsRoom.Add(element.furnitureClass, furnitureInRoom);
                }

                //Room limit reached
                if (classCount >= element.furnitureClass.maximumNumberPerRoom)
                {
                    if (enableDebug) Game.Log("Element Room limit reached (" + classCount + "/" + element.furnitureClass.maximumNumberPerRoom +")");
                    valid = false;

                    //If this is important to this cluster, you aren't going to be able to place it in this room at all...
                    if (element.importantToCluster)
                    {
                        return ret;
                    }
                }
            }

            int resLimitPerAddress = 9999;
            int jobLimitPerAddress = 9999;

            //Address residences limit
            if (element.furnitureClass.limitPerBuildingResidence)
            {
                if (room.building == null)
                {
                    if (enableDebug) Game.Log("Element per residence reached");
                    valid = false;
                }
                else
                {
                    int residences = 0;

                    foreach (KeyValuePair<int, NewFloor> pair in room.building.floors)
                    {
                        residences += pair.Value.addresses.FindAll(item => item.residence != null).Count;
                    }

                    resLimitPerAddress = Mathf.CeilToInt((float)residences / (float)element.furnitureClass.perBuildingResidences);
                }
            }

            //Jobs residences limit
            if (element.furnitureClass.limitPerJobs)
            {
                if (room.gameLocation.thisAsAddress != null && room.gameLocation.thisAsAddress.company != null)
                {
                    int jobs = room.gameLocation.thisAsAddress.company.companyRoster.Count;
                    jobLimitPerAddress = Mathf.CeilToInt((float)jobs / (float)element.furnitureClass.perJobs);
                }
                else valid = false;
            }

            //Address limit reached
            if (element.furnitureClass.limitPerAddress || element.furnitureClass.limitPerBuildingResidence || element.furnitureClass.limitPerJobs)
            {
                if (existingElementsAddress == null) existingElementsAddress = new Dictionary<FurnitureClass, List<FurnitureLocation>>();
                int addressClassCount = 0;

                if (existingElementsAddress.ContainsKey(element.furnitureClass))
                {
                    addressClassCount = existingElementsAddress[element.furnitureClass].Count;
                }
                else
                {
                    List<FurnitureLocation> furnitureInAddress = GetFurnitureInGameLocation(room.gameLocation, element.furnitureClass);
                    addressClassCount += furnitureInAddress.Count;
                    existingElementsAddress.Add(element.furnitureClass, furnitureInAddress);
                }

                if (element.furnitureClass.limitPerAddress && addressClassCount >= element.furnitureClass.maximumNumberPerAddress)
                {
                    if (enableDebug) Game.Log("Element Room limit reached (" + addressClassCount + "/" + element.furnitureClass.maximumNumberPerAddress + ")");
                    valid = false;

                    //If this is important to this cluster, you aren't going to be able to place it in this room at all...
                    if (element.importantToCluster)
                    {
                        return ret;
                    }
                }
                else if (element.furnitureClass.limitPerBuildingResidence && addressClassCount >= resLimitPerAddress)
                {
                    if (enableDebug) Game.Log("Element Room limit reached residences (" + addressClassCount + "/" + resLimitPerAddress + ")");
                    valid = false;

                    //If this is important to this cluster, you aren't going to be able to place it in this room at all...
                    if (element.importantToCluster)
                    {
                        return ret;
                    }
                }
                else if (element.furnitureClass.limitPerJobs && addressClassCount >= jobLimitPerAddress)
                {
                    if (enableDebug) Game.Log("Element Room limit reached residences (" + addressClassCount + "/" + jobLimitPerAddress + ")");
                    valid = false;

                    //If this is important to this cluster, you aren't going to be able to place it in this room at all...
                    if (element.importantToCluster)
                    {
                        return ret;
                    }
                }
            }

            //Building limit reached
            //if (element.furnitureClass.limitPerBuilding || element.furnitureClass.limitPerBuildingResidence)
            //{
            //    if (existingElementsBuilding == null) existingElementsBuilding = new Dictionary<FurnitureClass, List<FurnitureLocation>>();
            //    int buildingClassCount = 0;

            //    if (existingElementsBuilding.ContainsKey(element.furnitureClass))
            //    {
            //        buildingClassCount = existingElementsBuilding[element.furnitureClass].Count;
            //    }
            //    else
            //    {
            //        List<FurnitureLocation> furnitureInBuilding = GetFurnitureInBuilding(room.gameLocation.building, element.furnitureClass);
            //        buildingClassCount += furnitureInBuilding.Count;
            //        existingElementsBuilding.Add(element.furnitureClass, furnitureInBuilding);
            //    }

            //    if (element.furnitureClass.limitPerBuilding && buildingClassCount >= element.furnitureClass.maximumNumberPerBuilding)
            //    {
            //        if (enableDebug) Game.Log("Element Room limit reached (" + buildingClassCount + "/" + element.furnitureClass.maximumNumberPerBuilding + ")");
            //        valid = false;
            //    }
            //    else if (element.furnitureClass.limitPerBuildingResidence && buildingClassCount >= resLimitPerBuilding)
            //    {
            //        if (enableDebug) Game.Log("Element Room limit reached (" + buildingClassCount + "/" + resLimitPerBuilding + ")");
            //        valid = false;
            //    }
            //}

            //City limit reached
            //if (element.furnitureClass.limitPerCity)
            //{
            //    //if (existingElementsCity == null) existingElementsCity = new Dictionary<FurnitureClass, List<FurnitureLocation>>();
            //    //int cityClassCount = 0;

            //    //if (existingElementsCity.ContainsKey(element.furnitureClass))
            //    //{
            //    //    cityClassCount = existingElementsCity[element.furnitureClass].Count;
            //    //}
            //    //else
            //    //{
            //    //    List<FurnitureLocation> furnitureInCity = GetFurnitureInCity(element.furnitureClass);
            //    //    cityClassCount += furnitureInCity.Count;
            //    //    existingElementsCity.Add(element.furnitureClass, furnitureInCity);
            //    //}

            //    //if (cityClassCount >= element.furnitureClass.maximumNumberPerCity)
            //    //{
            //    //    if (enableDebug) Game.Log("Element Room limit reached (" + cityClassCount + "/" + element.furnitureClass.maximumNumberPerCity + ")");
            //    //    valid = false;
            //    //}
            //}

            if (valid && element.furnitureClass.skipIfNoAddressInhabitants && room.gameLocation != null && room.gameLocation.thisAsAddress != null && room.gameLocation.thisAsAddress.inhabitants.Count <= 0)
            {
                if (enableDebug) Game.Log("Element Skipping as no inhabitants...");
                valid = false;

                //If this is important to this cluster, you aren't going to be able to place it in this room at all...
                if (element.importantToCluster)
                {
                    return ret;
                }
            }

            //Chance of skipping element...
            if (valid && Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, room.seed) > element.chanceOfPlacementAttempt)
            {
                if (enableDebug) Game.Log("Element Skipping because of element placement chance (" + element.chanceOfPlacementAttempt + ")");
                valid = false;
            }

            //Limit to floor
            if (valid && element.furnitureClass.limitToFloor)
            {
                if (Mathf.RoundToInt(room.nodes.FirstOrDefault().nodeCoord.z) != element.furnitureClass.allowedOnFloor)
                {
                    if (enableDebug) Game.Log("Element Skipping as limited to floor " + element.furnitureClass.allowedOnFloor);
                    valid = false;

                    //If this is important to this cluster, you aren't going to be able to place it in this room at all...
                    if (element.importantToCluster)
                    {
                        return ret;
                    }
                }
            }

            //Limit to floor range
            if (valid && element.furnitureClass.limitToFloorRange)
            {
                int floor = Mathf.RoundToInt(room.nodes.FirstOrDefault().nodeCoord.z);

                if (floor < element.furnitureClass.allowedOnFloorRange.x || floor > element.furnitureClass.allowedOnFloorRange.y)
                {
                    if (enableDebug) Game.Log("Element Skipping as limited to floor range " + element.furnitureClass.limitToFloorRange);
                    valid = false;

                    //If this is important to this cluster, you aren't going to be able to place it in this room at all...
                    if (element.importantToCluster)
                    {
                        return ret;
                    }
                }
            }

            //Does valid furniture exist for this
            if (valid)
            {
                valid = GetValidFurniture(element.furnitureClass, room, false, out _, false);
                if(enableDebug) Game.Log("Valid furniture exists for element: " + valid);
            }

            if(valid)
            {
                if (enableDebug) Game.Log("Element: Is a valid element...");
                validElements.Add(element); //This is a valid element to place, add to the list...
            }
            //This is an invalid element to place...
            else
            {
                if (enableDebug) Game.Log("Element: Unable to find valid furniture for this cluster/room combination");

                //This element is invalid and is important to the cluster based on non-positional requirements, so this cluster is impossible to place...
                if (element.importantToCluster)
                {
                    return ret;
                }
            }
        }

        //Scan node-by-node...
        foreach (NewNode node in room.nodes)
        {
            //Coastal only
            if (cluster.coastalOnly && !SessionData.Instance.isFloorEdit)
            {
                bool pass = true;

                //Must be within a TILE of boundary
                foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                {
                    Vector3Int tileSearch = node.tile.globalTileCoord + new Vector3Int(v2.x, v2.y, 0);
                    NewTile foundTile = null;

                    if (PathFinder.Instance.tileMap.TryGetValue(tileSearch, out foundTile))
                    {
                        if (foundTile.cityTile.building != null && !foundTile.cityTile.building.preset.boundary)
                        {
                            pass = false;
                        }
                    }
                }

                if (!pass) continue;
            }

            FurnitureClusterDebug debugClass = null;

            if(SessionData.Instance.isFloorEdit && enableDebug)
            {
                GameObject newD = Instantiate(PrefabControls.Instance.furnitureDebug, FloorEditController.Instance.debugContainer);
                debugClass = newD.GetComponent<FurnitureClusterDebug>();
                debugClass.Setup(cluster, node);
            }

            //For each node try all possible angles...
            int[] angles = CityData.Instance.angleArrayX4;

            foreach (int angle in angles)
            {
                //Apply cluster node rules check
                if(cluster.zeroNodeWallRules.Count > 0)
                {
                    //First get the placement point that's been rotated by just the angle
                    Vector2 clusterPlacementPos = Toolbox.Instance.RotateVector2CW(Vector2.zero, angle);
                    bool zeroNodeWallPass = true;

                    foreach (FurnitureClass.FurnitureWallRule wallRule in cluster.zeroNodeWallRules)
                    {
                        Vector2 o = CityData.Instance.GetOffsetFromDirection(wallRule.wallDirection);

                        Vector2 rotatedNodeOffset = Toolbox.Instance.RotateVector2CW(wallRule.nodeOffset, angle);
                        Vector3Int tileOffsetCoord = node.nodeCoord + new Vector3Int(Mathf.RoundToInt(rotatedNodeOffset.x), Mathf.RoundToInt(rotatedNodeOffset.y), 0);

                        //Scan for rear wall
                        Vector2 rotatedWallOffset = Toolbox.Instance.RotateVector2CW(o, angle) * 0.5f;
                        rotatedWallOffset = new Vector2(Mathf.Round(rotatedWallOffset.x * 2f) / 2f, Mathf.Round(rotatedWallOffset.y * 2f) / 2f); //Set to 0-0.5

                        NewNode foundWallNode = null;
                        bool wallRuleValid = true;
                        bool offsetExists = false;

                        if (PathFinder.Instance.nodeMap.TryGetValue(tileOffsetCoord, out foundWallNode))
                        {
                            offsetExists = true;

                            NewWall foundWall = null;

                            if (foundWallNode.wallDict.TryGetValue(rotatedWallOffset, out foundWall))
                            {
                                if (wallRuleValid)
                                {
                                    if (wallRule.tag == FurnitureClass.WallRule.entrance)
                                    {
                                        if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.entranceDoorOnly)
                                    {
                                        if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                        {
                                            wallRuleValid = false;
                                        }
                                        else if (foundWall.preset.divider)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.entraceDivider)
                                    {
                                        if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                        {
                                            wallRuleValid = false;
                                        }
                                        else if (!foundWall.preset.divider)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.entranceToRoomOfType && wallRule.roomType != null)
                                    {
                                        if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance || foundWall.otherWall.node.room.preset != wallRule.roomType)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.addressEntrance)
                                    {
                                        if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance || foundWall.otherWall.node.room.gameLocation == foundWall.node.room.gameLocation)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.nothing)
                                    {
                                        wallRuleValid = false;
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.ventLower)
                                    {
                                        if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventLower)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.ventUpper)
                                    {
                                        if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventUpper)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.ventTop)
                                    {
                                        if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventTop)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.wall)
                                    {
                                        if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.wall)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.window)
                                    {
                                        if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.window)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.windowLarge)
                                    {
                                        if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.windowLarge)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.anyWindow)
                                    {
                                        if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.windowLarge && foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.window)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.wallOrUpperVent)
                                    {
                                        if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.wall && foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventUpper)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.fence)
                                    {
                                        if (!foundWall.preset.isFence)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.lightswitch)
                                    {
                                        if (foundWall.lightswitchInteractable == null)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else if (wallRule.tag == FurnitureClass.WallRule.securityDoorDivider)
                                    {
                                        if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                        {
                                            wallRuleValid = false;
                                        }
                                        else if (!foundWall.preset.divider)
                                        {
                                            wallRuleValid = false;
                                        }
                                        else if (room.preset.securityDoors == RoomConfiguration.SecurityDoorRule.never)
                                        {
                                            wallRuleValid = false;
                                        }
                                        else if (room.preset.securityDoors == RoomConfiguration.SecurityDoorRule.onlyToOtherAddress)
                                        {
                                            if (foundWall.node.gameLocation == foundWall.otherWall.node.gameLocation)
                                            {
                                                wallRuleValid = false;
                                            }
                                        }
                                        else if (room.preset.securityDoors == RoomConfiguration.SecurityDoorRule.onlyToStairwell)
                                        {
                                            bool p = false;

                                            foreach (NewNode n in foundWall.otherWall.node.room.nodes)
                                            {
                                                if (n.tile.isStairwell)
                                                {
                                                    p = true;
                                                    break;
                                                }
                                            }

                                            if (!p)
                                            {
                                                wallRuleValid = false;
                                            }
                                        }

                                        //Search for existing...
                                        if (wallRuleValid)
                                        {
                                            foreach (Interactable d in foundWall.node.floor.securityDoors)
                                            {
                                                if (d.furnitureParent.coversNodes.Exists(item => item == foundWall.node || item == foundWall.otherWall.node))
                                                {
                                                    //Game.Log("found existing");
                                                    wallRuleValid = false;
                                                    break;
                                                }
                                            }
                                        }

                                        //Make sure there is an alternate route out of this floor...
                                        if (wallRuleValid)
                                        {
                                            if (room.floor.floor != 0)
                                            {
                                                //Scan for vent access downwards...
                                                bool p = false;

                                                foreach (NewAddress ad in room.floor.addresses)
                                                {
                                                    List<AirDuctGroup.AirDuctSection> upConnections = new List<AirDuctGroup.AirDuctSection>();
                                                    List<AirDuctGroup.AirDuctSection> downConnections = new List<AirDuctGroup.AirDuctSection>();

                                                    //Write ducts
                                                    foreach (NewNode n in ad.nodes)
                                                    {
                                                        //Write ducts
                                                        foreach (AirDuctGroup.AirDuctSection duct in n.airDucts)
                                                        {
                                                            //Find the map section to write
                                                            List<Vector3Int> ductOffsets;
                                                            List<Vector3Int> ventOffsets;
                                                            duct.GetNeighborSections(out ductOffsets, out _, out ventOffsets);

                                                            //Combine duct and vent offsets for this purpse
                                                            ductOffsets.AddRange(ventOffsets);

                                                            //Is this a down connection?
                                                            if (duct.level == 0)
                                                            {
                                                                if (ductOffsets.Exists(item => item.z < 0))
                                                                {
                                                                    p = true;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                if (!p)
                                                {
                                                    wallRuleValid = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            //Invalid unless we're looking for nothing...
                            else if (wallRule.tag != FurnitureClass.WallRule.nothing)
                            {
                                wallRuleValid = false;
                            }
                        }
                        else wallRuleValid = false; //Can't find node

                        //We should now have an answer to the qestion, but what to do with it...
                        //if (wallRule.option == FurnitureClass.FurnitureRuleOption.canFeature)
                        //{
                        //    //Add points to this placement
                        //    if (wallRuleValid)
                        //    {
                        //        placementScore += wallRule.addScore;
                        //    }
                        //}
                        if (wallRule.option == FurnitureClass.FurnitureRuleOption.cantFeature)
                        {
                            if (wallRuleValid || !offsetExists)
                            {
                                zeroNodeWallPass = false;
                                break;
                            }
                        }
                        else if (wallRule.option == FurnitureClass.FurnitureRuleOption.mustFeature)
                        {
                            if (!wallRuleValid || !offsetExists)
                            {
                                zeroNodeWallPass = false;
                                break;
                            }
                        }
                    }

                    if (!zeroNodeWallPass)
                    {
                        continue;
                    }
                }

                //This is where we start checking the cluster rules...
                bool isAnglePlacementValid = true;
                int overallRank = 0;

                //Record access that's blocked in this placement, so we can check access validity later
                Dictionary<NewNode, List<NewNode>> blockedAccess = new Dictionary<NewNode, List<NewNode>>();
                List<NewNode> noPassThrough = new List<NewNode>();
                List<NewNode> noAccessNeeded = new List<NewNode>();

                List<NewNode> coversNodes = new List<NewNode>(); //Keep track of all nodes this covers
                List<NewWall> rearWalls = new List<NewWall>();

                //Record valid elements here...
                List<FurnitureLocation> validObjects = new List<FurnitureLocation>();
                Dictionary<FurnitureClass, int> validObjectPlacement = new Dictionary<FurnitureClass, int>();

                bool previousObjectGotPlaced = false;

                //Record debug
                FurnitureClusterDebug.DebugFurnitureAnglePlacement newEntry = null;

                if (SessionData.Instance.isFloorEdit && enableDebug && debugClass != null)
                {
                    newEntry = new FurnitureClusterDebug.DebugFurnitureAnglePlacement();
                    newEntry.angle = angle;
                    newEntry.log = new List<string>();
                }

                //Loop through the objects in the furniture cluster, and get their nodes to anylise...
                for (int l = 0; l < validElements.Count; l++)
                {
                    FurnitureCluster.FurnitureClusterRule element = validElements[l];

                    if (element.onlyValidIfPreviousObjectPlaced && !previousObjectGotPlaced) continue; //Skip if the previous object isn't placed.

                    previousObjectGotPlaced = false;

                    bool isElementValid = false;

                    //Check for maximum count...
                    //Note: We need these here as the valid object placement for this cluster may effects these limitations
                    if (isAnglePlacementValid && element.furnitureClass.limitPerRoom)
                    {
                        int classCount = existingElementsRoom[element.furnitureClass].Count;

                        int plus = 0;
                        if (validObjectPlacement.TryGetValue(element.furnitureClass, out plus)) classCount += plus;

                        //Room limit reached
                        if (classCount >= element.furnitureClass.maximumNumberPerRoom)
                        {
                            if (element.importantToCluster)
                            {
                                isAnglePlacementValid = false;
                                break;
                            }
                            else continue;
                        }
                    }

                    //Address limit reached
                    if (isAnglePlacementValid && element.furnitureClass.limitPerAddress)
                    {
                        int addressClassCount = existingElementsAddress[element.furnitureClass].Count;

                        int plus = 0;
                        if (validObjectPlacement.TryGetValue(element.furnitureClass, out plus)) addressClassCount += plus;

                        if (element.furnitureClass.limitPerAddress && addressClassCount >= element.furnitureClass.maximumNumberPerAddress)
                        {
                            if (element.importantToCluster)
                            {
                                isAnglePlacementValid = false;
                                break;
                            }
                            else continue;
                        }
                    }

                    //Building limit reached
                    //if (isAnglePlacementValid && element.furnitureClass.limitPerBuilding)
                    //{
                    //    int buildingClassCount = existingElementsBuilding[element.furnitureClass].Count;

                    //    int plus = 0;
                    //    if (validObjectPlacement.TryGetValue(element.furnitureClass, out plus)) buildingClassCount += plus;

                    //    if (buildingClassCount >= element.furnitureClass.maximumNumberPerBuilding)
                    //    {
                    //        if (element.importantToCluster)
                    //        {
                    //            isAnglePlacementValid = false;
                    //            break;
                    //        }
                    //        else continue;
                    //    }
                    //}

                    //City limit reached
                    //if (isAnglePlacementValid && element.furnitureClass.limitPerCity)
                    //{
                    //    int cityClassCount = existingElementsCity[element.furnitureClass].Count;

                    //    int plus = 0;
                    //    if (validObjectPlacement.TryGetValue(element.furnitureClass, out plus)) cityClassCount += plus;

                    //    if (cityClassCount >= element.furnitureClass.maximumNumberPerCity)
                    //    {
                    //        if (element.importantToCluster)
                    //        {
                    //            isAnglePlacementValid = false;
                    //            break;
                    //        }
                    //        else continue;
                    //    }
                    //}

                    //if(isAnglePlacementValid && element.furnitureClass.limitPerBuildingResidence)
                    //{
                    //    if (room.building == null)
                    //    {
                    //        if (enableDebug) Game.Log("Element per residence reached");
                    //        isAnglePlacementValid = false;
                    //        break;
                    //    }
                    //    else
                    //    {
                    //        int residences = 0;

                    //        foreach (KeyValuePair<int, NewFloor> pair in room.building.floors)
                    //        {
                    //            residences += pair.Value.addresses.FindAll(item => item.residence != null).Count;
                    //        }

                    //        int resLimitPerBuilding = Mathf.CeilToInt((float)residences / (float)element.furnitureClass.perBuildingResidences);

                    //        int buildingClassCount = existingElementsBuilding[element.furnitureClass].Count;

                    //        int plus = 0;
                    //        if (validObjectPlacement.TryGetValue(element.furnitureClass, out plus)) buildingClassCount += plus;

                    //        if (buildingClassCount >= resLimitPerBuilding)
                    //        {
                    //            if (element.importantToCluster)
                    //            {
                    //                isAnglePlacementValid = false;
                    //                break;
                    //            }
                    //            else continue;
                    //        }
                    //    }
                    //}

                    //Record debug
                    if (enableDebug && newEntry != null)
                    {
                        newEntry.log.Add("Anylizing placements for element " + l + "/" + cluster.clusterElements.Count + " " + element.furnitureClass.name + "...");
                    }

                    //Run through placements
                    foreach (Vector2 v2 in element.placements)
                    {
                        bool isPlacementValid = true;

                        //Record debug
                        if (enableDebug && newEntry != null)
                        {
                            newEntry.log.Add("Anylizing placement " + v2);
                        }

                        //Record placement nodes of this furniture class if the furniture blocks walking...
                        Dictionary<NewNode, List<NewNode>> newBlockedAccess = new Dictionary<NewNode, List<NewNode>>();
                        List<NewNode> coversNewNodes = new List<NewNode>();
                        rearWalls.Clear();
                        NewNode anchorNode = null;

                        //First get the placement point that's been rotated by just the angle
                        Vector2 placementPos = Toolbox.Instance.RotateVector2CW(v2, angle);

                        //Get the combined angle of the cluster angle and the furniture facing
                        int combinedAngle = angle + GetAngleForFurnitureFacing(element.facing) - 180;

                        //Rotate the offset using the angle we are checking...
                        //Loop object's size and find the placement nodes.
                        for (int i = 0; i < element.furnitureClass.objectSize.x; i++)
                        {
                            for (int u = 0; u < element.furnitureClass.objectSize.y; u++)
                            {
                                //Next get the furniture tile offset that's been rotated by both the angle and the facing
                                Vector2 furniturePos = Toolbox.Instance.RotateVector2CW(new Vector2(i, u), combinedAngle);

                                //The sum of these should be what we need.
                                Vector2 rotated = placementPos + furniturePos;

                                Vector3Int offsetCoord = node.nodeCoord + new Vector3Int(Mathf.RoundToInt(rotated.x), Mathf.RoundToInt(rotated.y), 0);
                                NewNode foundNode = null;

                                //This node must be present in the room...
                                if (PathFinder.Instance.nodeMap.TryGetValue(offsetCoord, out foundNode))
                                {
                                    if (anchorNode == null) anchorNode = foundNode; //Set anchorNode (first node)

                                    //Must be in same room...
                                    if (foundNode.room != room)
                                    {
                                        //Record debug
                                        if (enableDebug && newEntry != null && !isPlacementValid)
                                        {
                                            newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because the furniture would reach another room");
                                        }

                                        isPlacementValid = false;
                                        break;
                                    }

                                    //Not allowed on stairwell
                                    if (!element.furnitureClass.allowedOnStairwell)
                                    {
                                        if(foundNode.tile.isStairwell)
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because this is a stairwell");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }
                                        else
                                        {
                                            //Also search adjacent for stairwell nodes
                                            foreach (Vector2Int sv2 in CityData.Instance.offsetArrayX4)
                                            {
                                                Vector3Int nodeC = foundNode.nodeCoord + new Vector3Int(sv2.x, sv2.y, 0);

                                                NewNode sNode = null;

                                                //This node must be present in the room...
                                                if (PathFinder.Instance.nodeMap.TryGetValue(nodeC, out sNode))
                                                {
                                                    if(sNode.tile.isStairwell)
                                                    {
                                                        //Record debug
                                                        if (enableDebug && newEntry != null && !isPlacementValid)
                                                        {
                                                            newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because this is a stairwell");
                                                        }

                                                        isPlacementValid = false;
                                                        break;
                                                    }
                                                }
                                            }

                                            if (!isPlacementValid) break;
                                        }
                                    }
                                    else
                                    {
                                        if(element.furnitureClass.onlyOnStairwell && !foundNode.tile.isStairwell)
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because this isn't a stairwell");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //Not allowed if no floor
                                    if(foundNode.gameLocation.thisAsStreet == null)
                                    {
                                        if (!element.furnitureClass.allowIfNoFloor && (foundNode.floorType == NewNode.FloorTileType.CeilingOnly || foundNode.floorType == NewNode.FloorTileType.none || foundNode.floorType == NewNode.FloorTileType.noneButIndoors))
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because there is no floor");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //Ceiling piece
                                    if (element.furnitureClass.ceilingPiece)
                                    {
                                        if (foundNode.floorType == NewNode.FloorTileType.floorOnly || foundNode.floorType == NewNode.FloorTileType.none || foundNode.floorType == NewNode.FloorTileType.noneButIndoors)
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because there is no ceiling");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }

                                        //Don't allow a light to spawn on ducts...
                                        if (foundNode.airDucts.Exists(item => item.level == 1) || (foundNode.floor.defaultCeilingHeight > 42 && foundNode.airDucts.Exists(item => item.level == 2)))
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because of nearby air ducts");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }

                                        //Can't be placed here if other ceiling items are here...
                                        if (foundNode.individualFurniture.Exists(item => item.furniture.classes[0].ceilingPiece && item.furniture.classes[0].blocksCeiling))
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because of other ceiling items");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }
                                    }
                                    else if(element.furnitureClass.requiresCeiling)
                                    {
                                        if (foundNode.floorType == NewNode.FloorTileType.floorOnly || foundNode.floorType == NewNode.FloorTileType.none || foundNode.floorType == NewNode.FloorTileType.noneButIndoors)
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because there is no ceiling");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //This is blocking an entrance...
                                    if (element.furnitureClass.occupiesTile)
                                    {
                                        //Doesn't count if entrance is a room divider
                                        List<NewWall> entrances = foundNode.walls.FindAll(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance);

                                        if (entrances.Count > 0 && entrances.Exists(item => !item.preset.divider))
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because it blocks an entrance");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //This is a window piece
                                    if(element.furnitureClass.windowPiece)
                                    {
                                        //Can't be placed here if other window pieces are here
                                        if (foundNode.individualFurniture.Exists(item => item.furniture.classes[0].windowPiece))
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because other window piece is here");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //This is a tall item blocking a window
                                    if (element.furnitureClass.tall || element.furnitureClass.wallPiece)
                                    {
                                        //Check for blocking window
                                        if (foundNode.walls.Exists(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.window || item.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge))
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because it is tall and blocks a window");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }

                                        //Can't be placed here if other tall items are here...
                                        if (foundNode.individualFurniture.Exists(item => item.furniture.classes[0].tall || item.furniture.classes[0].wallPiece))
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because other tall/wall furniture is here");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }

                                        //Can't be placed here if an air duct exists...
                                        if (element.furnitureClass.tall && foundNode.airDucts.Exists(item => item.level >= 0 && item.level <= 1))
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because an air duct is here");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //Check for ceiling blockers
                                    if (element.furnitureClass.ceilingPiece)
                                    {
                                        //Avoid ceiling air vents here...
                                        if (foundNode.ceilingAirVent)
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because it blocks a ceiling vent");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }

                                        //Can't be placed here if other tall items are here...
                                        if (foundNode.individualFurniture.Exists(item => item.furniture.classes[0].blocksCeiling))
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid becuase it blocks the ceiling");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //Allow lightswitches...
                                    if (!element.furnitureClass.allowLightswitch && element.furnitureClass.occupiesTile)
                                    {
                                        //Check for blocking lightswitch
                                        if (foundNode.walls.Exists(item => item.containsLightswitch != null))
                                        {
                                            isPlacementValid = false;

                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because a lightswitch is here");
                                            }

                                            break;
                                        }
                                    }

                                    //Scan for rear wall (only if jutting not allowed and X size is > 1)
                                    //if (!element.furnitureClass.allowJutting && element.furnitureClass.objectSize.x > 1)
                                    //{
                                        //Vector2 rotatedOffset = Toolbox.Instance.RotateVector2CW(new Vector2(0, -0.5f), angle + GetAngleForFurnitureFacing(element.facing));
                                        //rotatedOffset = new Vector2(Mathf.Round(rotatedOffset.x * 2f) / 2f, Mathf.Round(rotatedOffset.y * 2f) / 2f); //Set to 0-0.5
                                        //NewWall rearWall = foundNode.walls.Find(item => item.wallOffset == rotatedOffset);
                                        //if (rearWall != null) rearWalls.Add(rearWall);
                                    //}

                                    //Check for allowing new furniture...
                                    if (foundNode.allowNewFurniture || !element.furnitureClass.occupiesTile)
                                    {
                                        //If this is reached, add to the recorded placement nodesif walking access is blocked
                                        if (isPlacementValid)
                                        {
                                            coversNewNodes.Add(foundNode);
                                        }
                                    }
                                    else
                                    {
                                        //Record debug
                                        if (enableDebug && newEntry != null && !isPlacementValid)
                                        {
                                            newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because no new furniture is allowed here");
                                        }

                                        isPlacementValid = false;
                                    }
                                }
                                else
                                {
                                    isPlacementValid = false;
                                }

                                //Invalid location for this furniture, escape size loop...
                                if (!isPlacementValid)
                                {
                                    break;
                                }
                            }

                            //Invalid location for this furniture, escape size loop...
                            if (!isPlacementValid)
                            {
                                break;
                            }
                        }

                        //Record debug
                        if (enableDebug && newEntry != null && !isPlacementValid)
                        {
                            newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid after size check");
                        }

                        //If this is reached, it's all good so far: But next we need to check the individual class rules for the furniture...
                        if (isPlacementValid)
                        {
                            //Record debug
                            if (enableDebug && newEntry != null)
                            {
                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is valid after size check");
                            }

                            int placementScore = 0;

                            //Check for jutting
                            //if (!element.furnitureClass.allowJutting && element.furnitureClass.objectSize.x > 1)
                            //{
                                //if (rearWalls.Count > 0 && rearWalls.Count < element.furnitureClass.objectSize.x)
                                //{
                                //    //Record debug
                                //    if (enableDebug && newEntry != null && isPlacementValid)
                                //    {
                                //        newEntry.log.Add("...Element " + element.furnitureClass.name + " fail on jutting");
                                //    }

                                //    isPlacementValid = false;
                                //}
                            //}

                            //Check for minimum distance
                            if (element.furnitureClass.minimumNodeDistance > 0 && isPlacementValid)
                            {
                                foreach (FurnitureClass furnClass in element.furnitureClass.awayFromClasses)
                                {
                                    //Check this room...
                                    List<FurnitureLocation> matchingFurniture = room.individualFurniture.FindAll(item => item.furnitureClasses.Contains(furnClass));

                                    //Also check adjacent rooms on this floor
                                    if(room.floor != null)
                                    {
                                        for (int u = 0; u < room.entrances.Count; u++)
                                        {
                                            NewNode.NodeAccess acc = room.entrances[u];

                                            if (acc.door == null && acc.accessType != NewNode.NodeAccess.AccessType.window && acc.accessType != NewNode.NodeAccess.AccessType.bannister)
                                            {
                                                //Must be on same floor
                                                if(acc.toNode.floor == room.floor && acc.fromNode.floor == room.floor)
                                                {
                                                    for (int i = 0; i < acc.toNode.room.individualFurniture.Count; i++)
                                                    {
                                                        FurnitureLocation fLoc = acc.toNode.room.individualFurniture[i];
                                                        if (fLoc == null) continue;

                                                        if (fLoc.furnitureClasses != null && fLoc.furnitureClasses.Contains(furnClass))
                                                        {
                                                            matchingFurniture.Add(fLoc);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    foreach (FurnitureLocation existingFurn in matchingFurniture)
                                    {
                                        foreach (NewNode coveredNode in existingFurn.coversNodes)
                                        {
                                            foreach (NewNode thisNode in coversNewNodes)
                                            {
                                                float nodeDistance = Vector3.Distance(coveredNode.nodeCoord, thisNode.nodeCoord);

                                                if (nodeDistance < element.furnitureClass.minimumNodeDistance)
                                                {
                                                    isPlacementValid = false;
                                                    break;
                                                }
                                            }

                                            if (!isPlacementValid) break;
                                        }

                                        if (!isPlacementValid) break;
                                    }

                                    if (!isPlacementValid) break;
                                }

                                if (enableDebug && !isPlacementValid)
                                {
                                    //Record debug
                                    if (enableDebug && newEntry != null)
                                    {
                                        newEntry.log.Add("...Element " + element.furnitureClass.name + " fail on minimum distance");
                                    }
                                }
                            }

                            //Check walls rules
                            if (isPlacementValid)
                            {
                                foreach (FurnitureClass.FurnitureWallRule wallRule in element.furnitureClass.wallRules)
                                {
                                    Vector2 o = CityData.Instance.GetOffsetFromDirection(wallRule.wallDirection);

                                    Vector2 rotatedNodeOffset = Toolbox.Instance.RotateVector2CW(wallRule.nodeOffset, angle + GetAngleForFurnitureFacing(element.facing));
                                    Vector3Int tileOffsetCoord = anchorNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(rotatedNodeOffset.x), Mathf.RoundToInt(rotatedNodeOffset.y), 0);

                                    //Scan for rear wall
                                    Vector2 rotatedWallOffset = Toolbox.Instance.RotateVector2CW(o, angle + GetAngleForFurnitureFacing(element.facing)) * 0.5f;
                                    rotatedWallOffset = new Vector2(Mathf.Round(rotatedWallOffset.x * 2f) / 2f, Mathf.Round(rotatedWallOffset.y * 2f) / 2f); //Set to 0-0.5

                                    NewNode foundWallNode = null;
                                    bool wallRuleValid = true;
                                    bool offsetExists = false;

                                    if (PathFinder.Instance.nodeMap.TryGetValue(tileOffsetCoord, out foundWallNode))
                                    {
                                        offsetExists = true;

                                        NewWall foundWall = null;

                                        if (foundWallNode.wallDict.TryGetValue(rotatedWallOffset, out foundWall))
                                        {
                                            //Wall piece
                                            if (element.furnitureClass.wallPiece)
                                            {
                                                if (/*foundWall.individualFurniture.Count > 0*/ foundWall.placedWallFurn)
                                                {
                                                    wallRuleValid = false;
                                                }
                                            }

                                            //Disallow because of lightswitch
                                            if (!element.furnitureClass.allowLightswitch)
                                            {
                                                //Check for blocking lightswitch
                                                if (foundWall.containsLightswitch != null)
                                                {
                                                    wallRuleValid = false;
                                                }
                                            }

                                            if (wallRuleValid)
                                            {
                                                if (wallRule.tag == FurnitureClass.WallRule.entrance)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.entranceDoorOnly)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                    else if (foundWall.preset.divider)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.entraceDivider)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                    else if (!foundWall.preset.divider)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.entranceToRoomOfType && wallRule.roomType != null)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance || foundWall.otherWall.node.room.preset != wallRule.roomType)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.addressEntrance)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance || foundWall.otherWall.node.room.gameLocation == foundWall.node.room.gameLocation)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.nothing)
                                                {
                                                    wallRuleValid = false;
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.ventLower)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventLower)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.ventUpper)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventUpper)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.ventTop)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventTop)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.wall)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.wall)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.window)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.window)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.windowLarge)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.windowLarge)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.anyWindow)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.windowLarge && foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.window)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.wallOrUpperVent)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.wall && foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventUpper)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.fence)
                                                {
                                                    if (!foundWall.preset.isFence)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.lightswitch)
                                                {
                                                    if (foundWall.lightswitchInteractable == null)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.securityDoorDivider)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                    else if (!foundWall.preset.divider)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                    else if (room.preset.securityDoors == RoomConfiguration.SecurityDoorRule.never)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                    else if (room.preset.securityDoors == RoomConfiguration.SecurityDoorRule.onlyToOtherAddress)
                                                    {
                                                        if (foundWall.node.gameLocation == foundWall.otherWall.node.gameLocation)
                                                        {
                                                            wallRuleValid = false;
                                                        }
                                                    }
                                                    else if (room.preset.securityDoors == RoomConfiguration.SecurityDoorRule.onlyToStairwell)
                                                    {
                                                        bool p = false;

                                                        foreach (NewNode n in foundWall.otherWall.node.room.nodes)
                                                        {
                                                            if (n.tile.isStairwell)
                                                            {
                                                                p = true;
                                                                break;
                                                            }
                                                        }

                                                        if (!p)
                                                        {
                                                            wallRuleValid = false;
                                                        }
                                                    }

                                                    //Search for existing...
                                                    if (wallRuleValid)
                                                    {
                                                        foreach (Interactable d in foundWall.node.floor.securityDoors)
                                                        {
                                                            if (d.furnitureParent.coversNodes.Exists(item => item == foundWall.node || item == foundWall.otherWall.node))
                                                            {
                                                                //Game.Log("found existing");
                                                                wallRuleValid = false;
                                                                break;
                                                            }
                                                        }
                                                    }

                                                    //Make sure there is an alternate route out of this floor...
                                                    if (wallRuleValid)
                                                    {
                                                        if (room.floor.floor != 0)
                                                        {
                                                            //Scan for vent access downwards...
                                                            bool p = false;

                                                            foreach (NewAddress ad in room.floor.addresses)
                                                            {
                                                                List<AirDuctGroup.AirDuctSection> upConnections = new List<AirDuctGroup.AirDuctSection>();
                                                                List<AirDuctGroup.AirDuctSection> downConnections = new List<AirDuctGroup.AirDuctSection>();

                                                                //Write ducts
                                                                foreach (NewNode n in ad.nodes)
                                                                {
                                                                    //Write ducts
                                                                    foreach (AirDuctGroup.AirDuctSection duct in n.airDucts)
                                                                    {
                                                                        //Find the map section to write
                                                                        List<Vector3Int> ductOffsets;
                                                                        List<Vector3Int> ventOffsets;
                                                                        duct.GetNeighborSections(out ductOffsets, out _, out ventOffsets);

                                                                        //Combine duct and vent offsets for this purpse
                                                                        ductOffsets.AddRange(ventOffsets);

                                                                        //Is this a down connection?
                                                                        if (duct.level == 0)
                                                                        {
                                                                            if (ductOffsets.Exists(item => item.z < 0))
                                                                            {
                                                                                p = true;
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            if (!p)
                                                            {
                                                                wallRuleValid = false;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //Invalid unless we're looking for nothing...
                                        else if (wallRule.tag != FurnitureClass.WallRule.nothing)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else wallRuleValid = false; //Can't find node

                                    //We should now have an answer to the qestion, but what to do with it...
                                    if (wallRule.option == FurnitureClass.FurnitureRuleOption.canFeature)
                                    {
                                        //Add points to this placement
                                        if (wallRuleValid)
                                        {
                                            placementScore += wallRule.addScore;
                                        }
                                    }
                                    else if (wallRule.option == FurnitureClass.FurnitureRuleOption.cantFeature)
                                    {
                                        if (wallRuleValid || !offsetExists)
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                    }
                                    else if (wallRule.option == FurnitureClass.FurnitureRuleOption.mustFeature)
                                    {
                                        if (!wallRuleValid || !offsetExists)
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                    }
                                }

                                if (enableDebug && !isPlacementValid)
                                {
                                    //Record debug
                                    if (enableDebug && newEntry != null)
                                    {
                                        newEntry.log.Add("...Element " + element.furnitureClass.name + " fail on wall rules");
                                    }
                                }
                            }

                            if (isPlacementValid)
                            {
                                //Check tile rules
                                foreach (FurnitureClass.FurnitureNodeRule nodeRule in element.furnitureClass.nodeRules)
                                {
                                    //Rotate the tile & wall offset using the angle
                                    Vector2 rotatedNodeOffset = Toolbox.Instance.RotateVector2CW(nodeRule.offset, angle + GetAngleForFurnitureFacing(element.facing));
                                    Vector3Int tileOffsetCoord = anchorNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(rotatedNodeOffset.x), Mathf.RoundToInt(rotatedNodeOffset.y), 0);

                                    NewNode foundRuleNode = null;
                                    bool ruleValid = false;

                                    if (PathFinder.Instance.nodeMap.TryGetValue(tileOffsetCoord, out foundRuleNode))
                                    {
                                        foreach (FurnitureClusterLocation furnLoc in room.furniture)
                                        {
                                            List<FurnitureLocation> foundCluster = null;
                                            bool foundHere = false;

                                            if (furnLoc.clusterObjectMap.TryGetValue(foundRuleNode, out foundCluster))
                                            {
                                                foreach (FurnitureLocation furn in foundCluster)
                                                {
                                                    if(nodeRule.anyOccupiedTile && furn.furnitureClasses.Exists(item => item.occupiesTile))
                                                    {
                                                        foundHere = true;
                                                        break;
                                                    }
                                                    else if (furn.furnitureClasses.Contains(nodeRule.furnitureClass))
                                                    {
                                                        foundHere = true;
                                                        break;
                                                    }
                                                }
                                            }

                                            if (foundHere)
                                            {
                                                ruleValid = true;
                                                break;
                                            }
                                        }
                                    }

                                    //We should now have an answer to the qestion, but what to do with it...
                                    if (nodeRule.option == FurnitureClass.FurnitureRuleOption.canFeature)
                                    {
                                        //Add points to this placement
                                        if (ruleValid)
                                        {
                                            placementScore += nodeRule.addScore;
                                        }
                                    }
                                    else if (nodeRule.option == FurnitureClass.FurnitureRuleOption.cantFeature)
                                    {
                                        if (ruleValid)
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                    }
                                    else if (nodeRule.option == FurnitureClass.FurnitureRuleOption.mustFeature)
                                    {
                                        if (!ruleValid)
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                    }
                                }

                                if (enableDebug && !isPlacementValid)
                                {
                                    //Record debug
                                    if (enableDebug && newEntry != null)
                                    {
                                        newEntry.log.Add("...Element " + element.furnitureClass.name + " fail on tile rules");
                                    }
                                }
                            }

                            if (isPlacementValid)
                            {
                                //Calculate blocked access for this placement alone....
                                foreach (FurnitureClass.BlockedAccess blocked in element.furnitureClass.blockedAccess)
                                {
                                    if (blocked.disabled) continue;

                                    Vector2 rotated = Toolbox.Instance.RotateVector2CW(blocked.nodeOffset, angle + GetAngleForFurnitureFacing(element.facing));
                                    Vector3Int offsetCoord = anchorNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(rotated.x), Mathf.RoundToInt(rotated.y), 0);
                                    NewNode foundNode = null;

                                    //This is the node for which to apply the blocks...
                                    if (PathFinder.Instance.nodeMap.TryGetValue(offsetCoord, out foundNode))
                                    {
                                        foreach (CityData.BlockingDirection b in blocked.blocked)
                                        {
                                            Vector2 blockOffset = CityData.Instance.GetOffsetFromDirection(b);

                                            //Scan for blocked nodes
                                            Vector2 blockedOffsetRotated = Toolbox.Instance.RotateVector2CW(blockOffset, angle + GetAngleForFurnitureFacing(element.facing));
                                            Vector3Int blockedCoord = foundNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(blockedOffsetRotated.x), Mathf.RoundToInt(blockedOffsetRotated.y), 0);
                                            NewNode blockedNode = null;

                                            if (PathFinder.Instance.nodeMap.TryGetValue(blockedCoord, out blockedNode))
                                            {
                                                //Add blocked in both directions...
                                                if (!newBlockedAccess.ContainsKey(foundNode))
                                                {
                                                    newBlockedAccess.Add(foundNode, new List<NewNode>());
                                                }

                                                newBlockedAccess[foundNode].Add(blockedNode);

                                                //Add blocked in both directions...
                                                if (!newBlockedAccess.ContainsKey(blockedNode))
                                                {
                                                    newBlockedAccess.Add(blockedNode, new List<NewNode>());
                                                }

                                                newBlockedAccess[blockedNode].Add(foundNode);
                                            }
                                        }
                                    }

                                    //Apply external diagonal blocking
                                    if (blocked.blockExteriorDiagonals)
                                    {
                                        if (blocked.blocked.Contains(CityData.BlockingDirection.behindLeft))
                                        {
                                            Vector2 rotatedOff1 = Toolbox.Instance.RotateVector2CW(new Vector2(-1, 0), angle + GetAngleForFurnitureFacing(element.facing));
                                            Vector3Int offsetCoordOff1 = offsetCoord + new Vector3Int(Mathf.RoundToInt(rotatedOff1.x), Mathf.RoundToInt(rotatedOff1.y), 0);
                                            NewNode foundNodeOff1 = null;

                                            //This is the node for which to apply the blocks...
                                            if (PathFinder.Instance.nodeMap.TryGetValue(offsetCoordOff1, out foundNodeOff1))
                                            {
                                                Vector2 rotatedOff2 = Toolbox.Instance.RotateVector2CW(new Vector2(0, -1), angle + GetAngleForFurnitureFacing(element.facing));
                                                Vector3Int offsetCoordOff2 = offsetCoord + new Vector3Int(Mathf.RoundToInt(rotatedOff2.x), Mathf.RoundToInt(rotatedOff2.y), 0);
                                                NewNode foundNodeOff2 = null;

                                                //This is the node for which to apply the blocks...
                                                if (PathFinder.Instance.nodeMap.TryGetValue(offsetCoordOff2, out foundNodeOff2))
                                                {
                                                    //Add blocked in both directions...
                                                    try
                                                    {
                                                        if (!newBlockedAccess.ContainsKey(foundNodeOff1))
                                                        {
                                                            newBlockedAccess.Add(foundNodeOff1, new List<NewNode>());
                                                        }

                                                        newBlockedAccess[foundNodeOff1].Add(foundNodeOff2);
                                                    }
                                                    catch
                                                    {

                                                    }

                                                    //Add blocked in both directions...
                                                    try
                                                    {
                                                        if (!newBlockedAccess.ContainsKey(foundNodeOff2))
                                                        {
                                                            newBlockedAccess.Add(foundNodeOff2, new List<NewNode>());
                                                        }

                                                        newBlockedAccess[foundNodeOff2].Add(foundNodeOff1);
                                                    }
                                                    catch
                                                    {

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                //Lastly, check all tiles in the room would still be accessable...
                                if (element.furnitureClass.blockedAccess.Count > 0 || element.furnitureClass.noPassThrough)
                                {
                                    //Gather both the dictionary for this cluster so far + the blocks for this object placement alone, so we can check nothings blocking anything
                                    Dictionary<NewNode, List<NewNode>> furnCheckBlocked = new Dictionary<NewNode, List<NewNode>>(blockedAccess);

                                    //Add this...
                                    foreach (KeyValuePair<NewNode, List<NewNode>> pair in newBlockedAccess)
                                    {
                                        if (!furnCheckBlocked.ContainsKey(pair.Key))
                                        {
                                            furnCheckBlocked.Add(pair.Key, new List<NewNode>());
                                        }

                                        furnCheckBlocked[pair.Key].AddRange(pair.Value);
                                    }

                                    List<NewNode> furnCheckNoPassThrough = new List<NewNode>(noPassThrough);

                                    //Add this
                                    if (element.furnitureClass.noPassThrough)
                                    {
                                        furnCheckNoPassThrough.AddRange(coversNodes);
                                    }

                                    List<NewNode> furnCheckNoAccessNeeded = new List<NewNode>(noAccessNeeded);

                                    //Add this
                                    if (element.furnitureClass.noAccessNeeded)
                                    {
                                        furnCheckNoAccessNeeded.AddRange(coversNodes);
                                    }

                                    //Add blocked tiles into no access needed...
                                    List<FurnitureClass.BlockedAccess> allDirsBlocked = element.furnitureClass.blockedAccess.FindAll(item => item.blocked.Count >= 8);

                                    foreach (FurnitureClass.BlockedAccess blocked in allDirsBlocked)
                                    {
                                        if (blocked.disabled) continue;

                                        Vector2 rotated = Toolbox.Instance.RotateVector2CW(blocked.nodeOffset, angle + GetAngleForFurnitureFacing(element.facing));
                                        Vector3Int offsetCoord = anchorNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(rotated.x), Mathf.RoundToInt(rotated.y), 0);
                                        NewNode foundNode = null;

                                        //This is the node for which to apply the blocks...
                                        if (PathFinder.Instance.nodeMap.TryGetValue(offsetCoord, out foundNode))
                                        {
                                            if (furnCheckNoAccessNeeded == null) furnCheckNoAccessNeeded = new List<NewNode>();

                                            if(!furnCheckNoAccessNeeded.Contains(foundNode))
                                            {
                                                furnCheckNoAccessNeeded.Add(foundNode);
                                            }
                                        }
                                    }

                                    if(!element.furnitureClass.noBlocking)
                                    {
                                        List<string> debugOutput = null;
                                        isPlacementValid = IsFurniturePlacementValid(room, ref furnCheckBlocked, ref furnCheckNoPassThrough, ref furnCheckNoAccessNeeded, cluster.enableDebug, out debugOutput);

                                        if (enableDebug && !isPlacementValid)
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " fail on pathing check");
                                                newEntry.pathingLog = debugOutput;
                                            }
                                        }
                                    }
                                }

                                //Finally, we've arrived at a valid placement! Save there details...
                                if (isPlacementValid)
                                {
                                    List<FurnitureClass> furnitureClasses = new List<FurnitureClass>();
                                    furnitureClasses.Add(element.furnitureClass);

                                    //Get furniture angle
                                    int furnAngle = GetAngleForFurnitureFacing(element.facing);

                                    furnAngle += angle;

                                    while (furnAngle >= 360)
                                    {
                                        furnAngle -= 360;
                                    }

                                    //Debug str
                                    //string debugStr = element.facing.ToString();

                                    //foreach(NewWall wall in rearWalls)
                                    //{
                                    //    debugStr += " " + wall.wallOffset;
                                    //}

                                    //Add to blocked access
                                    foreach (KeyValuePair<NewNode, List<NewNode>> pair in newBlockedAccess)
                                    {
                                        if (!blockedAccess.ContainsKey(pair.Key))
                                        {
                                            blockedAccess.Add(pair.Key, new List<NewNode>());
                                        }

                                        blockedAccess[pair.Key].AddRange(pair.Value);

                                        //foreach (NewNode bNode in pair.Value)
                                        //{
                                        //    debugStr += " " + bNode.nodeCoord;
                                        //}
                                    }

                                    //Add to no pass through
                                    if(element.furnitureClass.noPassThrough)
                                    {
                                        noPassThrough.AddRange(coversNewNodes);
                                    }

                                    //Add to no access needed
                                    if (element.furnitureClass.noAccessNeeded)
                                    {
                                        noAccessNeeded.AddRange(coversNewNodes);
                                    }

                                    coversNodes.AddRange(coversNewNodes); //Add to nodes covered

                                    //Create a valid placement class
                                    FurnitureLocation newObj = new FurnitureLocation(furnitureClasses, furnAngle, anchorNode, coversNewNodes, element.useFovBlock, element.blockDirection, element.maxFOVBlockDistance, element.localScale, newOffset: element.positionOffset);
                                    validObjects.Add(newObj);
                                    overallRank += placementScore; //Add bost from object class rules
                                    overallRank += element.placementScoreBoost; //Add boost for placing this

                                    if (!validObjectPlacement.ContainsKey(element.furnitureClass))
                                    {
                                        validObjectPlacement.Add(element.furnitureClass, 1);
                                    }
                                    else validObjectPlacement[element.furnitureClass]++;

                                    //Record debug
                                    if (enableDebug && newEntry != null)
                                    {
                                        newEntry.log.Add("...Element is valid at " + anchorNode.nodeCoord + " " + anchorNode.position + " angle " + furnAngle);
                                    }
                                }
                            }
                        }

                        //If we've found a valid position, break the placement loop...
                        if (isPlacementValid)
                        {
                            if (enableDebug)
                            {
                                //Record debug
                                if (enableDebug && newEntry != null)
                                {
                                    newEntry.log.Add("...Element " + element.furnitureClass.name + " placement is VALID at " + angle + " at " + anchorNode.nodeCoord + " " + anchorNode.position);
                                }
                            }

                            previousObjectGotPlaced = true;
                            isElementValid = true;
                            break;
                        }
                    }

                    //If this placement is essential to the placement of the whole cluster, then skip to the next angle...
                    if (!isElementValid && element.importantToCluster)
                    {
                        isAnglePlacementValid = false;
                        break;
                    }
                }

                //Success! A new possible location for a whole cluster has been found!
                if (isAnglePlacementValid)
                {
                    //Game.Log("> New possible location for cluster " + cluster.name);

                    FurnitureClusterLocation newPossibleLocation = new FurnitureClusterLocation(node, cluster, angle, overallRank + Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, room.seed + cluster.presetName));

                    //Add valid objects
                    foreach (FurnitureLocation valid in validObjects)
                    {
                        valid.cluster = newPossibleLocation; //Set cluster manually here

                        foreach (NewNode coversNode in valid.coversNodes)
                        {
                            if (!newPossibleLocation.clusterObjectMap.ContainsKey(coversNode))
                            {
                                newPossibleLocation.clusterObjectMap.Add(coversNode, new List<FurnitureLocation>());
                            }

                            newPossibleLocation.clusterObjectMap[coversNode].Add(valid);
                            if (!newPossibleLocation.clusterList.Contains(valid)) newPossibleLocation.clusterList.Add(valid);
                        }
                    }

                    //Record debug
                    if (enableDebug && newEntry != null)
                    {
                        newEntry.isValid = true;
                        newEntry.coversNodes = coversNodes;
                    }

                    ret.Add(newPossibleLocation);
                }

                //Record debug
                if ((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug)
                {
                    if(newEntry != null)
                    {
                        if (newEntry.isValid)
                        {
                            newEntry.name = "VALID";
                        }
                        else
                        {
                            newEntry.name = "INVALID";
                        }

                        debugClass.AddEntry(newEntry);
                    }
                }
            }
        }

        //Game.Log(ret.Count + " locations for for cluster " + cluster.name);

        return ret;
    }

    public FurnitureClusterLocation GetBestFurnitureClusterLocation(NewRoom room, FurnitureCluster cluster, bool enableDebug = false)
    {
        System.Diagnostics.Stopwatch stopWatch = null;
        CityConstructor.DecorClusterGenerationTimeInfo timingInfo = null;

        if (Game.Instance.devMode && Game.Instance.collectDebugData && CityConstructor.Instance != null && CityConstructor.Instance.debugLoadTime != null)
        {
            stopWatch = new System.Diagnostics.Stopwatch();
            stopWatch.Start();

            timingInfo = new CityConstructor.DecorClusterGenerationTimeInfo();
            timingInfo.cluster = cluster;
        }

        //Collect possible locations and rotations
        FurnitureClusterLocation ret = null;

        List<FurnitureCluster.FurnitureClusterRule> validElements = new List<FurnitureCluster.FurnitureClusterRule>();

        Dictionary<FurnitureClass, List<FurnitureLocation>> existingElementsRoom = null;
        Dictionary<FurnitureClass, List<FurnitureLocation>> existingElementsAddress = null;
        //Dictionary<FurnitureClass, List<FurnitureLocation>> existingElementsBuilding = null;
        //Dictionary<FurnitureClass, List<FurnitureLocation>> existingElementsCity = null;

        //Scan through non location-based limitations for placing various elements within this cluster to cut down on the calls when we position things...
        for (int l = 0; l < cluster.clusterElements.Count; l++)
        {
            FurnitureCluster.FurnitureClusterRule element = cluster.clusterElements[l];
            if (enableDebug) Game.Log("Searching placements for element " + (l + 1) + "/" + cluster.clusterElements.Count + "...");

            bool valid = true;

            if (element == null || element.furnitureClass == null)
            {
                valid = false;
                Game.LogError("Null furniture class found in cluster " + cluster.presetName + " index " + l);
                continue;
            }

            //Check for maximum count...
            if (element.furnitureClass.limitPerRoom)
            {
                if (existingElementsRoom == null) existingElementsRoom = new Dictionary<FurnitureClass, List<FurnitureLocation>>();
                int classCount = 0;

                if (existingElementsRoom.ContainsKey(element.furnitureClass))
                {
                    classCount = existingElementsRoom[element.furnitureClass].Count;
                }
                else
                {
                    List<FurnitureLocation> furnitureInRoom = GetFurnitureInRoom(room, element.furnitureClass);
                    classCount += furnitureInRoom.Count;
                    existingElementsRoom.Add(element.furnitureClass, furnitureInRoom);
                }

                //Room limit reached
                if (classCount >= element.furnitureClass.maximumNumberPerRoom)
                {
                    if (enableDebug) Game.Log("Element Room limit reached (" + classCount + "/" + element.furnitureClass.maximumNumberPerRoom + ")");
                    valid = false;

                    //If this is important to this cluster, you aren't going to be able to place it in this room at all...
                    if (element.importantToCluster)
                    {
                        return ret;
                    }
                }
            }

            int resLimitPerBuilding = 9999;
            int jobLimitPerAddress = 9999;

            //Building residences limit
            if (element.furnitureClass.limitPerBuildingResidence)
            {
                if (room.building == null)
                {
                    if (enableDebug) Game.Log("Element per residence reached");
                    valid = false;
                }
                else
                {
                    int residences = 0;

                    foreach (KeyValuePair<int, NewFloor> pair in room.building.floors)
                    {
                        residences += pair.Value.addresses.FindAll(item => item.residence != null).Count;
                    }

                    resLimitPerBuilding = Mathf.CeilToInt((float)residences / (float)element.furnitureClass.perBuildingResidences);
                }
            }

            //Jobs limit
            if (element.furnitureClass.limitPerJobs)
            {
                if (room.gameLocation.thisAsAddress != null && room.gameLocation.thisAsAddress.company != null)
                {
                    int jobs = room.gameLocation.thisAsAddress.company.companyRoster.Count;
                    jobLimitPerAddress = Mathf.CeilToInt((float)jobs / (float)element.furnitureClass.perJobs);
                }
                else valid = false;
            }

            //Address limit reached
            if (element.furnitureClass.limitPerAddress || element.furnitureClass.limitPerBuildingResidence || element.furnitureClass.limitPerJobs)
            {
                if (existingElementsAddress == null) existingElementsAddress = new Dictionary<FurnitureClass, List<FurnitureLocation>>();
                int addressClassCount = 0;

                if (existingElementsAddress.ContainsKey(element.furnitureClass))
                {
                    addressClassCount = existingElementsAddress[element.furnitureClass].Count;
                }
                else
                {
                    List<FurnitureLocation> furnitureInAddress = GetFurnitureInGameLocation(room.gameLocation, element.furnitureClass);
                    addressClassCount += furnitureInAddress.Count;
                    existingElementsAddress.Add(element.furnitureClass, furnitureInAddress);
                }

                if (addressClassCount >= element.furnitureClass.maximumNumberPerAddress && element.furnitureClass.limitPerAddress)
                {
                    if (enableDebug) Game.Log("Element Room limit reached (" + addressClassCount + "/" + element.furnitureClass.maximumNumberPerAddress + ")");
                    valid = false;

                    //If this is important to this cluster, you aren't going to be able to place it in this room at all...
                    if (element.importantToCluster)
                    {
                        return ret;
                    }
                }
                else if (addressClassCount >= resLimitPerBuilding && element.furnitureClass.limitPerBuildingResidence)
                {
                    if (enableDebug) Game.Log("Element Room limit reached residences (" + addressClassCount + "/" + resLimitPerBuilding + ")");
                    valid = false;

                    //If this is important to this cluster, you aren't going to be able to place it in this room at all...
                    if (element.importantToCluster)
                    {
                        return ret;
                    }
                }
                else if (addressClassCount >= jobLimitPerAddress && element.furnitureClass.limitPerJobs)
                {
                    if (enableDebug) Game.Log("Element Room limit reached residences (" + addressClassCount + "/" + jobLimitPerAddress + ")");
                    valid = false;

                    //If this is important to this cluster, you aren't going to be able to place it in this room at all...
                    if (element.importantToCluster)
                    {
                        return ret;
                    }
                }
            }

            //Building limit reached
            //if (element.furnitureClass.limitPerBuilding || element.furnitureClass.limitPerBuildingResidence)
            //{
            //    if (existingElementsBuilding == null) existingElementsBuilding = new Dictionary<FurnitureClass, List<FurnitureLocation>>();
            //    int buildingClassCount = 0;

            //    if (existingElementsBuilding.ContainsKey(element.furnitureClass))
            //    {
            //        buildingClassCount = existingElementsBuilding[element.furnitureClass].Count;
            //    }
            //    else
            //    {
            //        List<FurnitureLocation> furnitureInBuilding = GetFurnitureInBuilding(room.gameLocation.building, element.furnitureClass);
            //        buildingClassCount += furnitureInBuilding.Count;
            //        existingElementsBuilding.Add(element.furnitureClass, furnitureInBuilding);
            //    }

            //    if (element.furnitureClass.limitPerBuilding && buildingClassCount >= element.furnitureClass.maximumNumberPerBuilding)
            //    {
            //        if (enableDebug) Game.Log("Element Room limit reached (" + buildingClassCount + "/" + element.furnitureClass.maximumNumberPerBuilding + ")");
            //        valid = false;
            //    }
            //    else if (element.furnitureClass.limitPerBuildingResidence && buildingClassCount >= resLimitPerBuilding)
            //    {
            //        if (enableDebug) Game.Log("Element Room limit reached (" + buildingClassCount + "/" + resLimitPerBuilding + ")");
            //        valid = false;
            //    }
            //}

            //City limit reached
            //if (element.furnitureClass.limitPerCity)
            //{
            //    if (existingElementsCity == null) existingElementsCity = new Dictionary<FurnitureClass, List<FurnitureLocation>>();
            //    int cityClassCount = 0;

            //    if (existingElementsCity.ContainsKey(element.furnitureClass))
            //    {
            //        cityClassCount = existingElementsCity[element.furnitureClass].Count;
            //    }
            //    else
            //    {
            //        List<FurnitureLocation> furnitureInCity = GetFurnitureInCity(element.furnitureClass);
            //        cityClassCount += furnitureInCity.Count;
            //        existingElementsCity.Add(element.furnitureClass, furnitureInCity);
            //    }

            //    if (cityClassCount >= element.furnitureClass.maximumNumberPerCity)
            //    {
            //        if (enableDebug) Game.Log("Element Room limit reached (" + cityClassCount + "/" + element.furnitureClass.maximumNumberPerCity + ")");
            //        valid = false;
            //    }
            //}

            if (valid && element.furnitureClass.skipIfNoAddressInhabitants && room.gameLocation != null && room.gameLocation.thisAsAddress != null && room.gameLocation.thisAsAddress.inhabitants.Count <= 0)
            {
                if (enableDebug) Game.Log("Element Skipping as no inhabitants...");
                valid = false;

                //If this is important to this cluster, you aren't going to be able to place it in this room at all...
                if (element.importantToCluster)
                {
                    return ret;
                }
            }

            //Chance of skipping element...
            if (valid && Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, room.seed) > element.chanceOfPlacementAttempt)
            {
                if (enableDebug) Game.Log("Element Skipping because of element placement chance (" + element.chanceOfPlacementAttempt + ")");
                valid = false;
            }

            //Limit to floor
            if (valid && element.furnitureClass.limitToFloor)
            {
                if (Mathf.RoundToInt(room.nodes.FirstOrDefault().nodeCoord.z) != element.furnitureClass.allowedOnFloor)
                {
                    if (enableDebug) Game.Log("Element Skipping as limited to floor " + element.furnitureClass.allowedOnFloor);
                    valid = false;

                    //If this is important to this cluster, you aren't going to be able to place it in this room at all...
                    if (element.importantToCluster)
                    {
                        return ret;
                    }
                }
            }

            //Limit to floor range
            if (valid && element.furnitureClass.limitToFloorRange)
            {
                int floor = Mathf.RoundToInt(room.nodes.FirstOrDefault().nodeCoord.z);

                if (floor < element.furnitureClass.allowedOnFloorRange.x || floor > element.furnitureClass.allowedOnFloorRange.y)
                {
                    if (enableDebug) Game.Log("Element Skipping as limited to floor range " + element.furnitureClass.limitToFloorRange);
                    valid = false;

                    //If this is important to this cluster, you aren't going to be able to place it in this room at all...
                    if (element.importantToCluster)
                    {
                        return ret;
                    }
                }
            }

            //Does valid furniture exist for this
            if (valid)
            {
                valid = GetValidFurniture(element.furnitureClass, room, false, out _, false);
                if (enableDebug) Game.Log("Valid furniture exists for element: " + valid);
            }

            if (valid)
            {
                if (enableDebug) Game.Log("Element: Is a valid element...");
                validElements.Add(element); //This is a valid element to place, add to the list...
            }
            //This is an invalid element to place...
            else
            {
                if (enableDebug) Game.Log("Element: Unable to find valid furniture for this cluster/room combination");

                //This element is invalid and is important to the cluster based on non-positional requirements, so this cluster is impossible to place...
                if (element.importantToCluster)
                {
                    return ret;
                }
            }
        }

        //Scan node-by-node...
        foreach (NewNode node in room.nodes)
        {
            if (cluster.useCustomZeroNodeMinWallCount && node.walls.Count < cluster.customZeroNodeMinWallCount)
            {
                if (enableDebug) Game.Log("Element: Custom zero node min wall count reached");
                continue;
            }
            else if (cluster.useCustomZeroNodeMaxWallCount && node.walls.Count > cluster.customZeroNodeMaxWallCount)
            {
                if (enableDebug) Game.Log("Element: Custom zero node max wall count reached");
                continue;
            }

            //PreCalc optimizations; skip this node if it features an incompatible number of walls
            if (node.walls.Count < cluster.minimumZeroNodeWallCount)
            {
                if (enableDebug) Game.Log("Element: Zero node min wall count reached");
                continue;
            }
            else if(node.walls.Count > cluster.maximumZeroNodeWallCount)
            {
                if (enableDebug) Game.Log("Element: Zero node max wall count reached");
                continue;
            }

            //Precalc optimizations: 
            //Coastal only
            if (cluster.coastalOnly && !SessionData.Instance.isFloorEdit)
            {
                bool pass = true;

                //Must be within a TILE of boundary
                foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                {
                    Vector3Int tileSearch = node.tile.globalTileCoord + new Vector3Int(v2.x, v2.y, 0);
                    NewTile foundTile = null;

                    if (PathFinder.Instance.tileMap.TryGetValue(tileSearch, out foundTile))
                    {
                        if (foundTile.cityTile.building != null && !foundTile.cityTile.building.preset.boundary)
                        {
                            pass = false;
                        }
                    }
                }

                if (!pass)
                {
                    if (enableDebug) Game.Log("Element: Coastal only");
                    continue;
                }
            }

            FurnitureClusterDebug debugClass = null;

            if ((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug)
            {
                Transform debugContainer = null;
                if (SessionData.Instance.isFloorEdit) debugContainer = FloorEditController.Instance.debugContainer;
                else debugContainer = PrefabControls.Instance.debugDecorContainer;

                GameObject newD = Instantiate(PrefabControls.Instance.furnitureDebug, debugContainer);
                debugClass = newD.GetComponent<FurnitureClusterDebug>();
                debugClass.Setup(cluster, node);
            }

            //For each node try all possible angles...
            int[] angles = CityData.Instance.angleArrayX4;

            foreach (int angle in angles)
            {
                //This is where we start checking the cluster rules...
                bool isAnglePlacementValid = true;
                int overallRank = 0;

                //Record access that's blocked in this placement, so we can check access validity later
                Dictionary<NewNode, List<NewNode>> blockedAccess = new Dictionary<NewNode, List<NewNode>>();
                List<NewNode> noPassThrough = new List<NewNode>();
                List<NewNode> noAccessNeeded = new List<NewNode>();

                List<NewNode> coversNodes = new List<NewNode>(); //Keep track of all nodes this covers
                List<NewWall> rearWalls = new List<NewWall>();

                //Record valid elements here...
                List<FurnitureLocation> validObjects = new List<FurnitureLocation>();
                Dictionary<FurnitureClass, int> validObjectPlacement = new Dictionary<FurnitureClass, int>();

                bool previousObjectGotPlaced = false;

                //Record debug
                FurnitureClusterDebug.DebugFurnitureAnglePlacement newEntry = null;

                if ((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug)
                {
                    newEntry = new FurnitureClusterDebug.DebugFurnitureAnglePlacement();
                    newEntry.angle = angle;
                    newEntry.log = new List<string>();
                }

                //Loop through the objects in the furniture cluster, and get their nodes to anylise...
                for (int l = 0; l < validElements.Count; l++)
                {
                    FurnitureCluster.FurnitureClusterRule element = validElements[l];

                    if (element.onlyValidIfPreviousObjectPlaced && !previousObjectGotPlaced) continue; //Skip if the previous object isn't placed.

                    previousObjectGotPlaced = false;

                    bool isElementValid = false;

                    //Check for maximum count...
                    if (isAnglePlacementValid && element.furnitureClass.limitPerRoom)
                    {
                        int classCount = existingElementsRoom[element.furnitureClass].Count;

                        int plus = 0;
                        if (validObjectPlacement.TryGetValue(element.furnitureClass, out plus)) classCount += plus;

                        //Room limit reached
                        if (classCount >= element.furnitureClass.maximumNumberPerRoom)
                        {
                            if (element.importantToCluster)
                            {
                                isAnglePlacementValid = false;
                                break;
                            }
                            else continue;
                        }
                    }

                    //Address limit reached
                    if (isAnglePlacementValid && element.furnitureClass.limitPerAddress)
                    {
                        int addressClassCount = existingElementsAddress[element.furnitureClass].Count;

                        int plus = 0;
                        if (validObjectPlacement.TryGetValue(element.furnitureClass, out plus)) addressClassCount += plus;

                        if (addressClassCount >= element.furnitureClass.maximumNumberPerAddress)
                        {
                            if (element.importantToCluster)
                            {
                                isAnglePlacementValid = false;
                                break;
                            }
                            else continue;
                        }
                    }

                    //Building limit reached
                    //if (isAnglePlacementValid && element.furnitureClass.limitPerBuilding)
                    //{
                    //    int buildingClassCount = existingElementsBuilding[element.furnitureClass].Count;

                    //    int plus = 0;
                    //    if (validObjectPlacement.TryGetValue(element.furnitureClass, out plus)) buildingClassCount += plus;

                    //    if (buildingClassCount >= element.furnitureClass.maximumNumberPerBuilding)
                    //    {
                    //        if (element.importantToCluster)
                    //        {
                    //            isAnglePlacementValid = false;
                    //            break;
                    //        }
                    //        else continue;
                    //    }
                    //}

                    ////City limit reached
                    //if (isAnglePlacementValid && element.furnitureClass.limitPerCity)
                    //{
                    //    int cityClassCount = existingElementsCity[element.furnitureClass].Count;

                    //    int plus = 0;
                    //    if (validObjectPlacement.TryGetValue(element.furnitureClass, out plus)) cityClassCount += plus;

                    //    if (cityClassCount >= element.furnitureClass.maximumNumberPerCity)
                    //    {
                    //        if (element.importantToCluster)
                    //        {
                    //            isAnglePlacementValid = false;
                    //            break;
                    //        }
                    //        else continue;
                    //    }
                    //}

                    if (isAnglePlacementValid && element.furnitureClass.limitPerBuildingResidence)
                    {
                        //if (room.building == null)
                        //{
                        //    if (enableDebug) Game.Log("Element per residence reached");
                        //    isAnglePlacementValid = false;
                        //    break;
                        //}
                        //else
                        //{
                        //    int residences = 0;

                        //    foreach (KeyValuePair<int, NewFloor> pair in room.building.floors)
                        //    {
                        //        residences += pair.Value.addresses.FindAll(item => item.residence != null).Count;
                        //    }

                        //    int resLimitPerBuilding = Mathf.CeilToInt((float)residences / (float)element.furnitureClass.perBuildingResidences);

                        //    int buildingClassCount = existingElementsBuilding[element.furnitureClass].Count;

                        //    int plus = 0;
                        //    if (validObjectPlacement.TryGetValue(element.furnitureClass, out plus)) buildingClassCount += plus;

                        //    if (buildingClassCount >= resLimitPerBuilding)
                        //    {
                        //        if (element.importantToCluster)
                        //        {
                        //            isAnglePlacementValid = false;
                        //            break;
                        //        }
                        //        else continue;
                        //    }
                        //}
                    }

                    //Record debug
                    if ((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug)
                    {
                        if(newEntry != null)
                        {
                            newEntry.log.Add("Anylizing placements for element " + l + "/" + cluster.clusterElements.Count + " " + element.furnitureClass.name + "...");
                        }
                    }

                    //Run through placements
                    foreach (Vector2 v2 in element.placements)
                    {
                        bool isPlacementValid = true;

                        //Record debug
                        if ((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug)
                        {
                            if(newEntry != null)
                            {
                                newEntry.log.Add("Anylizing placement " + v2);
                            }
                        }

                        //Record placement nodes of this furniture class if the furniture blocks walking...
                        Dictionary<NewNode, List<NewNode>> newBlockedAccess = new Dictionary<NewNode, List<NewNode>>();
                        List<NewNode> coversNewNodes = new List<NewNode>();
                        rearWalls.Clear();
                        NewNode anchorNode = null;

                        //First get the placement point that's been rotated by just the angle
                        Vector2 placementPos = Toolbox.Instance.RotateVector2CW(v2, angle);

                        //Get the combined angle of the cluster angle and the furniture facing
                        int combinedAngle = angle + GetAngleForFurnitureFacing(element.facing) - 180;

                        //Rotate the offset using the angle we are checking...
                        //Loop object's size and find the placement nodes.
                        for (int i = 0; i < element.furnitureClass.objectSize.x; i++)
                        {
                            for (int u = 0; u < element.furnitureClass.objectSize.y; u++)
                            {
                                //Next get the furniture tile offset that's been rotated by both the angle and the facing
                                Vector2 furniturePos = Toolbox.Instance.RotateVector2CW(new Vector2(i, u), combinedAngle);

                                //The sum of these should be what we need.
                                Vector2 rotated = placementPos + furniturePos;

                                Vector3Int offsetCoord = node.nodeCoord + new Vector3Int(Mathf.RoundToInt(rotated.x), Mathf.RoundToInt(rotated.y), 0);
                                NewNode foundNode = null;

                                //This node must be present in the room...
                                if (PathFinder.Instance.nodeMap.TryGetValue(offsetCoord, out foundNode))
                                {
                                    if (anchorNode == null)
                                    {
                                        anchorNode = foundNode; //Set anchorNode (first node)

                                        //Run class based wall count checks; skip this node if it features an incompatible number of walls
                                        if (anchorNode.walls.Count < element.furnitureClass.minimumZeroNodeWallCount)
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                        else if (anchorNode.walls.Count > element.furnitureClass.maximumZeroNodeWallCount)
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //Must be in same room...
                                    if (foundNode.room != room)
                                    {
                                        isPlacementValid = false;
                                        break;
                                    }

                                    //Not allowed on stairwell
                                    if (!element.furnitureClass.allowedOnStairwell)
                                    {
                                        if (foundNode.tile.isStairwell)
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                        else
                                        {
                                            //Also search adjacent for stairwell nodes
                                            foreach (Vector2Int sv2 in CityData.Instance.offsetArrayX4)
                                            {
                                                Vector3Int nodeC = foundNode.nodeCoord + new Vector3Int(sv2.x, sv2.y, 0);

                                                NewNode sNode = null;

                                                //This node must be present in the room...
                                                if (PathFinder.Instance.nodeMap.TryGetValue(nodeC, out sNode))
                                                {
                                                    if (sNode.tile.isStairwell)
                                                    {
                                                        isPlacementValid = false;
                                                        break;
                                                    }
                                                }
                                            }

                                            if (!isPlacementValid) break;
                                        }
                                    }
                                    else
                                    {
                                        if (element.furnitureClass.onlyOnStairwell && !foundNode.tile.isStairwell)
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //Not allowed if no floor
                                    if (foundNode.gameLocation.thisAsStreet == null)
                                    {
                                        if (!element.furnitureClass.allowIfNoFloor && (foundNode.floorType == NewNode.FloorTileType.CeilingOnly || foundNode.floorType == NewNode.FloorTileType.none || foundNode.floorType == NewNode.FloorTileType.noneButIndoors))
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //Ceiling piece
                                    if (element.furnitureClass.ceilingPiece)
                                    {
                                        if (foundNode.floorType == NewNode.FloorTileType.floorOnly || foundNode.floorType == NewNode.FloorTileType.none || foundNode.floorType == NewNode.FloorTileType.noneButIndoors)
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }

                                        //Don't allow a light to spawn on ducts...
                                        if (foundNode.airDucts.Exists(item => item.level == 1) || (foundNode.floor.defaultCeilingHeight > 42 && foundNode.airDucts.Exists(item => item.level == 2)))
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }

                                        //Can't be placed here if other ceiling items are here...
                                        if (foundNode.individualFurniture.Exists(item => item.furniture.classes[0].ceilingPiece && item.furniture.classes[0].blocksCeiling))
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                    }
                                    else if (element.furnitureClass.requiresCeiling)
                                    {
                                        if (foundNode.floorType == NewNode.FloorTileType.floorOnly || foundNode.floorType == NewNode.FloorTileType.none || foundNode.floorType == NewNode.FloorTileType.noneButIndoors)
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " failed as this requires ceiling");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //This is blocking an entrance...
                                    if (element.furnitureClass.occupiesTile)
                                    {
                                        //Doesn't count if entrance is a room divider
                                        List<NewWall> entrances = foundNode.walls.FindAll(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance);

                                        if (entrances.Count > 0 && entrances.Exists(item => !item.preset.divider))
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //Only allow one window piece
                                    if(element.furnitureClass.windowPiece)
                                    {
                                        if (foundNode.individualFurniture.Exists(item => item.furniture.classes[0].windowPiece))
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " failed as window already has a piece here");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //This is a tall item blocking a window
                                    if (element.furnitureClass.tall || element.furnitureClass.wallPiece)
                                    {
                                        //Check for blocking window
                                        if (foundNode.walls.Exists(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.window || item.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge))
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " failed as window is blocked by tall furniture");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }

                                        //Can't be placed here if other tall items are here...
                                        if (foundNode.individualFurniture.Exists(item => item.furniture.classes[0].tall || item.furniture.classes[0].wallPiece))
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " failed as wall is blocked by tall furniture");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }

                                        //Can't be placed here if an air duct exists...
                                        if (element.furnitureClass.tall && foundNode.airDucts.Exists(item => item.level >= 0 && item.level <= 1))
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null && !isPlacementValid)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid because an air duct is here");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //Check for ceiling blockers
                                    if (element.furnitureClass.ceilingPiece)
                                    {
                                        //Avoid ceiling air vents here...
                                        if (foundNode.ceilingAirVent)
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " failed as ceiling vent would be blocked");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }

                                        //Can't be placed here if other tall items are here...
                                        if (foundNode.individualFurniture.Exists(item => item.furniture.classes[0].blocksCeiling))
                                        {
                                            //Record debug
                                            if (enableDebug && newEntry != null)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " failed as ceiling is blocked");
                                            }

                                            isPlacementValid = false;
                                            break;
                                        }
                                    }

                                    //Allow lightswitches...
                                    if (foundNode.gameLocation.thisAsStreet == null)
                                    {
                                        if (!element.furnitureClass.allowLightswitch && element.furnitureClass.occupiesTile)
                                        {
                                            //Check for blocking lightswitch
                                            if (foundNode.walls.Exists(item => item.containsLightswitch != null))
                                            {
                                                //Record debug
                                                if (enableDebug && newEntry != null)
                                                {
                                                    newEntry.log.Add("...Element " + element.furnitureClass.name + " failed as lightswitch is not allowed");
                                                }

                                                isPlacementValid = false;
                                                break;
                                            }
                                        }
                                    }

                                    //Scan for rear wall (only used if jutting is not allowed & x size of > 1)
                                    //if (!element.furnitureClass.allowJutting && element.furnitureClass.objectSize.x > 1)
                                    //{
                                        //Vector2 rotatedOffset = Toolbox.Instance.RotateVector2CW(new Vector2(0, -0.5f), angle + GetAngleForFurnitureFacing(element.facing));
                                        //rotatedOffset = new Vector2(Mathf.Round(rotatedOffset.x * 2f) / 2f, Mathf.Round(rotatedOffset.y * 2f) / 2f); //Set to 0-0.5
                                        //NewWall rearWall = foundNode.walls.Find(item => item.wallOffset == rotatedOffset);
                                        //if (rearWall != null) rearWalls.Add(rearWall);
                                    //}

                                    //Check for allowing new furniture...
                                    if (foundNode.allowNewFurniture || !element.furnitureClass.occupiesTile)
                                    {
                                        //If this is reached, add to the recorded placement nodesif walking access is blocked
                                        if (isPlacementValid)
                                        {
                                            coversNewNodes.Add(foundNode);
                                        }
                                    }
                                    else
                                    {
                                        //Record debug
                                        if (enableDebug && newEntry != null)
                                        {
                                            newEntry.log.Add("...Element " + element.furnitureClass.name + " failed as no new furniture is allowed here");
                                        }

                                        isPlacementValid = false;
                                    }
                                }
                                else
                                {
                                    isPlacementValid = false;
                                }

                                //Invalid location for this furniture, escape size loop...
                                if (!isPlacementValid)
                                {
                                    break;
                                }
                            }

                            //Invalid location for this furniture, escape size loop...
                            if (!isPlacementValid)
                            {
                                break;
                            }
                        }

                        //Record debug
                        if (((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug) && newEntry != null && !isPlacementValid)
                        {
                            newEntry.log.Add("...Element " + element.furnitureClass.name + " is invalid after size check");
                        }

                        //If this is reached, it's all good so far: But next we need to check the individual class rules for the furniture...
                        if (isPlacementValid)
                        {
                            //Record debug
                            if (((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug) && newEntry != null)
                            {
                                newEntry.log.Add("...Element " + element.furnitureClass.name + " is valid after size check");
                            }

                            int placementScore = 0;

                            //Check for jutting
                            //if (!element.furnitureClass.allowJutting && element.furnitureClass.objectSize.x > 1)
                            //{
                                //if (rearWalls.Count > 0 && rearWalls.Count < element.furnitureClass.objectSize.x)
                                //{
                                //    //Record debug
                                //    if (((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug) && newEntry != null && isPlacementValid)
                                //    {
                                //        newEntry.log.Add("...Element " + element.furnitureClass.name + " fail on jutting");
                                //    }

                                //    isPlacementValid = false;
                                //}
                            //}

                            //Check for minimum distance
                            if (element.furnitureClass.minimumNodeDistance > 0 && isPlacementValid)
                            {
                                foreach (FurnitureClass furnClass in element.furnitureClass.awayFromClasses)
                                {
                                    //Check this room...
                                    List<FurnitureLocation> matchingFurniture = room.individualFurniture.FindAll(item => item.furnitureClasses.Contains(furnClass));

                                    //Also check adjacent rooms on this floor
                                    if (room.floor != null)
                                    {
                                        for (int u = 0; u < room.entrances.Count; u++)
                                        {
                                            NewNode.NodeAccess acc = room.entrances[u];

                                            if (acc.door == null && acc.accessType != NewNode.NodeAccess.AccessType.window && acc.accessType != NewNode.NodeAccess.AccessType.bannister)
                                            {
                                                //Must be on same floor
                                                if (acc.toNode.floor == room.floor && acc.fromNode.floor == room.floor)
                                                {
                                                    for (int i = 0; i < acc.toNode.room.individualFurniture.Count; i++)
                                                    {
                                                        FurnitureLocation fLoc = acc.toNode.room.individualFurniture[i];
                                                        if (fLoc == null) continue;

                                                        if (fLoc.furnitureClasses != null && fLoc.furnitureClasses.Contains(furnClass))
                                                        {
                                                            matchingFurniture.Add(fLoc);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    foreach (FurnitureLocation existingFurn in matchingFurniture)
                                    {
                                        foreach (NewNode coveredNode in existingFurn.coversNodes)
                                        {
                                            foreach (NewNode thisNode in coversNewNodes)
                                            {
                                                float nodeDistance = Vector3.Distance(coveredNode.nodeCoord, thisNode.nodeCoord);

                                                if (nodeDistance < element.furnitureClass.minimumNodeDistance)
                                                {
                                                    isPlacementValid = false;
                                                    break;
                                                }
                                            }

                                            if (!isPlacementValid) break;
                                        }

                                        if (!isPlacementValid) break;
                                    }

                                    if (!isPlacementValid) break;
                                }

                                if (enableDebug && !isPlacementValid)
                                {
                                    //Record debug
                                    if ((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug)
                                    {
                                        if(newEntry != null) newEntry.log.Add("...Element " + element.furnitureClass.name + " fail on minimum distance");
                                    }
                                }
                            }

                            //Check walls rules
                            if (isPlacementValid)
                            {
                                foreach (FurnitureClass.FurnitureWallRule wallRule in element.furnitureClass.wallRules)
                                {
                                    Vector2 o = CityData.Instance.GetOffsetFromDirection(wallRule.wallDirection);

                                    Vector2 rotatedNodeOffset = Toolbox.Instance.RotateVector2CW(wallRule.nodeOffset, angle + GetAngleForFurnitureFacing(element.facing));
                                    Vector3Int tileOffsetCoord = anchorNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(rotatedNodeOffset.x), Mathf.RoundToInt(rotatedNodeOffset.y), 0);

                                    //Scan for rear wall
                                    Vector2 rotatedWallOffset = Toolbox.Instance.RotateVector2CW(o, angle + GetAngleForFurnitureFacing(element.facing)) * 0.5f;
                                    rotatedWallOffset = new Vector2(Mathf.Round(rotatedWallOffset.x * 2f) / 2f, Mathf.Round(rotatedWallOffset.y * 2f) / 2f); //Set to 0-0.5

                                    NewNode foundWallNode = null;
                                    bool wallRuleValid = true;
                                    bool offsetExists = false;

                                    if (PathFinder.Instance.nodeMap.TryGetValue(tileOffsetCoord, out foundWallNode))
                                    {
                                        offsetExists = true;

                                        NewWall foundWall = null;

                                        if (foundWallNode.wallDict.TryGetValue(rotatedWallOffset, out foundWall))
                                        {
                                            //Wall piece
                                            if (element.furnitureClass.wallPiece)
                                            {
                                                if (/*foundWall.individualFurniture.Count > 0*/ foundWall.placedWallFurn)
                                                {
                                                    wallRuleValid = false;
                                                }
                                            }

                                            //Disallow because of lightswitch
                                            if (!element.furnitureClass.allowLightswitch)
                                            {
                                                //Check for blocking lightswitch
                                                if (foundWall.containsLightswitch != null)
                                                {
                                                    wallRuleValid = false;
                                                }
                                            }

                                            if (wallRuleValid)
                                            {
                                                if (wallRule.tag == FurnitureClass.WallRule.entrance)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.entranceDoorOnly)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                    else if (foundWall.preset.divider)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.entraceDivider)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                    else if (!foundWall.preset.divider)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.entranceToRoomOfType && wallRule.roomType != null)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance || foundWall.otherWall.node.room.preset != wallRule.roomType)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.addressEntrance)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance || foundWall.otherWall.node.room.gameLocation == foundWall.node.room.gameLocation)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.nothing)
                                                {
                                                    wallRuleValid = false;
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.ventLower)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventLower)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.ventUpper)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventUpper)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.ventTop)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventTop)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.wall)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.wall)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.window)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.window)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.windowLarge)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.windowLarge)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.anyWindow)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.windowLarge && foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.window)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.wallOrUpperVent)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.wall && foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventUpper)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.fence)
                                                {
                                                    if (!foundWall.preset.isFence)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.lightswitch)
                                                {
                                                    if (foundWall.lightswitchInteractable == null)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                }
                                                else if (wallRule.tag == FurnitureClass.WallRule.securityDoorDivider)
                                                {
                                                    if (foundWall.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                    else if (!foundWall.preset.divider)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                    else if (room.preset.securityDoors == RoomConfiguration.SecurityDoorRule.never)
                                                    {
                                                        wallRuleValid = false;
                                                    }
                                                    else if (room.preset.securityDoors == RoomConfiguration.SecurityDoorRule.onlyToOtherAddress)
                                                    {
                                                        if (foundWall.node.gameLocation == foundWall.otherWall.node.gameLocation)
                                                        {
                                                            wallRuleValid = false;
                                                        }
                                                    }
                                                    else if (room.preset.securityDoors == RoomConfiguration.SecurityDoorRule.onlyToStairwell)
                                                    {
                                                        bool p = false;

                                                        foreach (NewNode n in foundWall.otherWall.node.room.nodes)
                                                        {
                                                            if (n.tile.isStairwell)
                                                            {
                                                                p = true;
                                                                break;
                                                            }
                                                        }

                                                        if (!p)
                                                        {
                                                            wallRuleValid = false;
                                                        }
                                                    }

                                                    //Search for existing...
                                                    if (wallRuleValid)
                                                    {
                                                        foreach (Interactable d in foundWall.node.floor.securityDoors)
                                                        {
                                                            if (d.furnitureParent.coversNodes.Exists(item => item == foundWall.node || item == foundWall.otherWall.node))
                                                            {
                                                                //Game.Log("found existing");
                                                                wallRuleValid = false;
                                                                break;
                                                            }
                                                        }
                                                    }

                                                    //Make sure there is an alternate route out of this floor...
                                                    if (wallRuleValid)
                                                    {
                                                        if (room.floor.floor != 0)
                                                        {
                                                            //Scan for vent access downwards...
                                                            bool p = false;

                                                            foreach (NewAddress ad in room.floor.addresses)
                                                            {
                                                                List<AirDuctGroup.AirDuctSection> upConnections = new List<AirDuctGroup.AirDuctSection>();
                                                                List<AirDuctGroup.AirDuctSection> downConnections = new List<AirDuctGroup.AirDuctSection>();

                                                                //Write ducts
                                                                foreach (NewNode n in ad.nodes)
                                                                {
                                                                    //Write ducts
                                                                    foreach (AirDuctGroup.AirDuctSection duct in n.airDucts)
                                                                    {
                                                                        //Find the map section to write
                                                                        List<Vector3Int> ductOffsets;
                                                                        List<Vector3Int> ventOffsets;
                                                                        duct.GetNeighborSections(out ductOffsets, out _, out ventOffsets);

                                                                        //Combine duct and vent offsets for this purpse
                                                                        ductOffsets.AddRange(ventOffsets);

                                                                        //Is this a down connection?
                                                                        if (duct.level == 0)
                                                                        {
                                                                            if (ductOffsets.Exists(item => item.z < 0))
                                                                            {
                                                                                p = true;
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            if (!p)
                                                            {
                                                                wallRuleValid = false;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //Invalid unless we're looking for nothing...
                                        else if (wallRule.tag != FurnitureClass.WallRule.nothing)
                                        {
                                            wallRuleValid = false;
                                        }
                                    }
                                    else wallRuleValid = false; //Can't find node

                                    //We should now have an answer to the qestion, but what to do with it...
                                    if (wallRule.option == FurnitureClass.FurnitureRuleOption.canFeature)
                                    {
                                        //Add points to this placement
                                        if (wallRuleValid)
                                        {
                                            placementScore += wallRule.addScore;
                                        }
                                    }
                                    else if (wallRule.option == FurnitureClass.FurnitureRuleOption.cantFeature)
                                    {
                                        if (wallRuleValid || !offsetExists)
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                    }
                                    else if (wallRule.option == FurnitureClass.FurnitureRuleOption.mustFeature)
                                    {
                                        if (!wallRuleValid || !offsetExists)
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                    }
                                }

                                if (enableDebug && !isPlacementValid)
                                {
                                    //Record debug
                                    if (((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug) && newEntry != null)
                                    {
                                        newEntry.log.Add("...Element " + element.furnitureClass.name + " fail on wall rules");
                                    }
                                }
                            }

                            if (isPlacementValid)
                            {
                                //Check tile rules
                                foreach (FurnitureClass.FurnitureNodeRule nodeRule in element.furnitureClass.nodeRules)
                                {
                                    //Rotate the tile & wall offset using the angle
                                    Vector2 rotatedNodeOffset = Toolbox.Instance.RotateVector2CW(nodeRule.offset, angle + GetAngleForFurnitureFacing(element.facing));
                                    Vector3Int tileOffsetCoord = anchorNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(rotatedNodeOffset.x), Mathf.RoundToInt(rotatedNodeOffset.y), 0);

                                    NewNode foundRuleNode = null;
                                    bool ruleValid = false;

                                    if (PathFinder.Instance.nodeMap.TryGetValue(tileOffsetCoord, out foundRuleNode))
                                    {
                                        foreach (FurnitureClusterLocation furnLoc in room.furniture)
                                        {
                                            List<FurnitureLocation> foundCluster = null;
                                            bool foundHere = false;

                                            if (furnLoc.clusterObjectMap.TryGetValue(foundRuleNode, out foundCluster))
                                            {
                                                foreach (FurnitureLocation furn in foundCluster)
                                                {
                                                    if (nodeRule.anyOccupiedTile && furn.furnitureClasses.Exists(item => item.occupiesTile))
                                                    {
                                                        foundHere = true;
                                                        break;
                                                    }
                                                    else if (furn.furnitureClasses.Contains(nodeRule.furnitureClass))
                                                    {
                                                        foundHere = true;
                                                        break;
                                                    }
                                                }
                                            }

                                            if (foundHere)
                                            {
                                                ruleValid = true;
                                                break;
                                            }
                                        }
                                    }

                                    //We should now have an answer to the qestion, but what to do with it...
                                    if (nodeRule.option == FurnitureClass.FurnitureRuleOption.canFeature)
                                    {
                                        //Add points to this placement
                                        if (ruleValid)
                                        {
                                            placementScore += nodeRule.addScore;
                                        }
                                    }
                                    else if (nodeRule.option == FurnitureClass.FurnitureRuleOption.cantFeature)
                                    {
                                        if (ruleValid)
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                    }
                                    else if (nodeRule.option == FurnitureClass.FurnitureRuleOption.mustFeature)
                                    {
                                        if (!ruleValid)
                                        {
                                            isPlacementValid = false;
                                            break;
                                        }
                                    }
                                }

                                if (enableDebug && !isPlacementValid)
                                {
                                    //Record debug
                                    if (((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug) && newEntry != null)
                                    {
                                        newEntry.log.Add("...Element " + element.furnitureClass.name + " fail on tile rules");
                                    }
                                }
                            }

                            if (isPlacementValid)
                            {
                                //Calculate blocked access for this placement alone....
                                foreach (FurnitureClass.BlockedAccess blocked in element.furnitureClass.blockedAccess)
                                {
                                    if (blocked.disabled) continue;

                                    Vector2 rotated = Toolbox.Instance.RotateVector2CW(blocked.nodeOffset, angle + GetAngleForFurnitureFacing(element.facing));
                                    Vector3Int offsetCoord = anchorNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(rotated.x), Mathf.RoundToInt(rotated.y), 0);
                                    NewNode foundNode = null;

                                    //This is the node for which to apply the blocks...
                                    if (PathFinder.Instance.nodeMap.TryGetValue(offsetCoord, out foundNode))
                                    {
                                        foreach (CityData.BlockingDirection b in blocked.blocked)
                                        {
                                            Vector2 blockOffset = CityData.Instance.GetOffsetFromDirection(b);

                                            //Scan for blocked nodes
                                            Vector2 blockedOffsetRotated = Toolbox.Instance.RotateVector2CW(blockOffset, angle + GetAngleForFurnitureFacing(element.facing));
                                            Vector3Int blockedCoord = foundNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(blockedOffsetRotated.x), Mathf.RoundToInt(blockedOffsetRotated.y), 0);
                                            NewNode blockedNode = null;

                                            if (PathFinder.Instance.nodeMap.TryGetValue(blockedCoord, out blockedNode))
                                            {
                                                //Add blocked in both directions...
                                                if (!newBlockedAccess.ContainsKey(foundNode))
                                                {
                                                    newBlockedAccess.Add(foundNode, new List<NewNode>());
                                                }

                                                newBlockedAccess[foundNode].Add(blockedNode);

                                                //Add blocked in both directions...
                                                if (!newBlockedAccess.ContainsKey(blockedNode))
                                                {
                                                    newBlockedAccess.Add(blockedNode, new List<NewNode>());
                                                }

                                                newBlockedAccess[blockedNode].Add(foundNode);
                                            }
                                        }
                                    }

                                    //Apply external diagonal blocking
                                    if (blocked.blockExteriorDiagonals)
                                    {
                                        if (blocked.blocked.Contains(CityData.BlockingDirection.behindLeft))
                                        {
                                            Vector2 rotatedOff1 = Toolbox.Instance.RotateVector2CW(new Vector2(-1, 0), angle + GetAngleForFurnitureFacing(element.facing));
                                            Vector3Int offsetCoordOff1 = offsetCoord + new Vector3Int(Mathf.RoundToInt(rotatedOff1.x), Mathf.RoundToInt(rotatedOff1.y), 0);
                                            NewNode foundNodeOff1 = null;

                                            //This is the node for which to apply the blocks...
                                            if (PathFinder.Instance.nodeMap.TryGetValue(offsetCoordOff1, out foundNodeOff1))
                                            {
                                                Vector2 rotatedOff2 = Toolbox.Instance.RotateVector2CW(new Vector2(0, -1), angle + GetAngleForFurnitureFacing(element.facing));
                                                Vector3Int offsetCoordOff2 = offsetCoord + new Vector3Int(Mathf.RoundToInt(rotatedOff2.x), Mathf.RoundToInt(rotatedOff2.y), 0);
                                                NewNode foundNodeOff2 = null;

                                                //This is the node for which to apply the blocks...
                                                if (PathFinder.Instance.nodeMap.TryGetValue(offsetCoordOff2, out foundNodeOff2))
                                                {
                                                    //Add blocked in both directions...
                                                    try
                                                    {
                                                        if (!newBlockedAccess.ContainsKey(foundNodeOff1))
                                                        {
                                                            newBlockedAccess.Add(foundNodeOff1, new List<NewNode>());
                                                        }

                                                        newBlockedAccess[foundNodeOff1].Add(foundNodeOff2);
                                                    }
                                                    catch
                                                    {

                                                    }

                                                    //Add blocked in both directions...
                                                    try
                                                    {
                                                        if (!newBlockedAccess.ContainsKey(foundNodeOff2))
                                                        {
                                                            newBlockedAccess.Add(foundNodeOff2, new List<NewNode>());
                                                        }

                                                        newBlockedAccess[foundNodeOff2].Add(foundNodeOff1);
                                                    }
                                                    catch
                                                    {

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                //Lastly, check all tiles in the room would still be accessable...
                                if (element.furnitureClass.blockedAccess.Count > 0 || element.furnitureClass.noPassThrough)
                                {
                                    //Gather both the dictionary for this cluster so far + the blocks for this object placement alone, so we can check nothings blocking anything
                                    Dictionary<NewNode, List<NewNode>> furnCheckBlocked = new Dictionary<NewNode, List<NewNode>>(blockedAccess);

                                    //Add this...
                                    foreach (KeyValuePair<NewNode, List<NewNode>> pair in newBlockedAccess)
                                    {
                                        if (!furnCheckBlocked.ContainsKey(pair.Key))
                                        {
                                            furnCheckBlocked.Add(pair.Key, new List<NewNode>());
                                        }

                                        furnCheckBlocked[pair.Key].AddRange(pair.Value);
                                    }

                                    List<NewNode> furnCheckNoPassThrough = new List<NewNode>(noPassThrough);

                                    //Add this
                                    if (element.furnitureClass.noPassThrough)
                                    {
                                        furnCheckNoPassThrough.AddRange(coversNodes);
                                    }

                                    List<NewNode> furnCheckNoAccessNeeded = new List<NewNode>(noAccessNeeded);

                                    //Add this
                                    if (element.furnitureClass.noAccessNeeded)
                                    {
                                        furnCheckNoAccessNeeded.AddRange(coversNodes);
                                    }

                                    //Add blocked tiles into no access needed...
                                    List<FurnitureClass.BlockedAccess> allDirsBlocked = element.furnitureClass.blockedAccess.FindAll(item => item.blocked.Count >= 8);

                                    foreach (FurnitureClass.BlockedAccess blocked in allDirsBlocked)
                                    {
                                        if (blocked.disabled) continue;

                                        Vector2 rotated = Toolbox.Instance.RotateVector2CW(blocked.nodeOffset, angle + GetAngleForFurnitureFacing(element.facing));
                                        Vector3Int offsetCoord = anchorNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(rotated.x), Mathf.RoundToInt(rotated.y), 0);
                                        NewNode foundNode = null;

                                        //This is the node for which to apply the blocks...
                                        if (PathFinder.Instance.nodeMap.TryGetValue(offsetCoord, out foundNode))
                                        {
                                            if (furnCheckNoAccessNeeded == null) furnCheckNoAccessNeeded = new List<NewNode>();

                                            if (!furnCheckNoAccessNeeded.Contains(foundNode))
                                            {
                                                furnCheckNoAccessNeeded.Add(foundNode);
                                            }
                                        }
                                    }

                                    if (!element.furnitureClass.noBlocking)
                                    {
                                        List<string> debugOutput = null;
                                        isPlacementValid = IsFurniturePlacementValid(room, ref furnCheckBlocked, ref furnCheckNoPassThrough, ref furnCheckNoAccessNeeded, enableDebug, out debugOutput);

                                        if (enableDebug && !isPlacementValid)
                                        {
                                            //Record debug
                                            if (((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug) && newEntry != null)
                                            {
                                                newEntry.log.Add("...Element " + element.furnitureClass.name + " fail on pathing check");
                                                newEntry.pathingLog = debugOutput;
                                            }
                                        }
                                    }
                                }

                                //Finally, we've arrived at a valid placement! Save there details...
                                if (isPlacementValid)
                                {
                                    List<FurnitureClass> furnitureClasses = new List<FurnitureClass>();
                                    furnitureClasses.Add(element.furnitureClass);

                                    //Get furniture angle
                                    int furnAngle = GetAngleForFurnitureFacing(element.facing);

                                    furnAngle += angle;

                                    while (furnAngle >= 360)
                                    {
                                        furnAngle -= 360;
                                    }

                                    //Debug str
                                    //string debugStr = element.facing.ToString();

                                    //foreach(NewWall wall in rearWalls)
                                    //{
                                    //    debugStr += " " + wall.wallOffset;
                                    //}

                                    //Add to blocked access
                                    foreach (KeyValuePair<NewNode, List<NewNode>> pair in newBlockedAccess)
                                    {
                                        if (!blockedAccess.ContainsKey(pair.Key))
                                        {
                                            blockedAccess.Add(pair.Key, new List<NewNode>());
                                        }

                                        blockedAccess[pair.Key].AddRange(pair.Value);

                                        //foreach (NewNode bNode in pair.Value)
                                        //{
                                        //    debugStr += " " + bNode.nodeCoord;
                                        //}
                                    }

                                    //Add to no pass through
                                    if (element.furnitureClass.noPassThrough)
                                    {
                                        noPassThrough.AddRange(coversNewNodes);
                                    }

                                    //Add to no access needed
                                    if (element.furnitureClass.noAccessNeeded)
                                    {
                                        noAccessNeeded.AddRange(coversNewNodes);
                                    }

                                    coversNodes.AddRange(coversNewNodes); //Add to nodes covered

                                    //Create a valid placement class
                                    FurnitureLocation newObj = new FurnitureLocation(furnitureClasses, furnAngle, anchorNode, coversNewNodes, element.useFovBlock, element.blockDirection, element.maxFOVBlockDistance, element.localScale, newOffset: element.positionOffset);
                                    validObjects.Add(newObj);
                                    overallRank += placementScore; //Add bost from object class rules
                                    overallRank += element.placementScoreBoost; //Add boost for placing this

                                    if (!validObjectPlacement.ContainsKey(element.furnitureClass))
                                    {
                                        validObjectPlacement.Add(element.furnitureClass, 1);
                                    }
                                    else validObjectPlacement[element.furnitureClass]++;

                                    //Record debug
                                    if (((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug) && newEntry != null)
                                    {
                                        newEntry.log.Add("...Element is valid at " + anchorNode.nodeCoord + " " + anchorNode.position + " angle " + furnAngle);
                                    }
                                }
                            }
                        }

                        //If we've found a valid position, break the placement loop...
                        if (isPlacementValid)
                        {
                            if (enableDebug)
                            {
                                //Record debug
                                if (((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug) && newEntry != null)
                                {
                                    newEntry.log.Add("...Element " + element.furnitureClass.name + " placement is VALID at " + angle + " at " + anchorNode.nodeCoord + " " + anchorNode.position);
                                }
                            }

                            previousObjectGotPlaced = true;
                            isElementValid = true;
                            break;
                        }
                    }

                    //If this placement is essential to the placement of the whole cluster, then skip to the next angle...
                    if (!isElementValid && element.importantToCluster)
                    {
                        isAnglePlacementValid = false;
                        break;
                    }
                }

                //Success! A new possible location for a whole cluster has been found!
                if (isAnglePlacementValid)
                {
                    //Game.Log("> New possible location for cluster " + cluster.name);

                    FurnitureClusterLocation newPossibleLocation = new FurnitureClusterLocation(node, cluster, angle, overallRank + Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, room.seed + cluster.presetName));

                    //Add valid objects
                    foreach (FurnitureLocation valid in validObjects)
                    {
                        valid.cluster = newPossibleLocation; //Set cluster manually here

                        foreach (NewNode coversNode in valid.coversNodes)
                        {
                            if (!newPossibleLocation.clusterObjectMap.ContainsKey(coversNode))
                            {
                                newPossibleLocation.clusterObjectMap.Add(coversNode, new List<FurnitureLocation>());
                            }

                            newPossibleLocation.clusterObjectMap[coversNode].Add(valid);
                            if (!newPossibleLocation.clusterList.Contains(valid)) newPossibleLocation.clusterList.Add(valid);
                        }
                    }

                    //Record debug
                    if ((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug)
                    {
                        if(newEntry != null)
                        {
                            newEntry.isValid = true;
                            newEntry.coversNodes = coversNodes;
                        }
                    }

                    if(ret == null || newPossibleLocation.ranking > ret.ranking)
                    {
                        ret = newPossibleLocation;
                    }
                }

                //Record debug
                if ((SessionData.Instance.isFloorEdit || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebug)
                {
                    if(newEntry != null)
                    {
                        if (newEntry.isValid)
                        {
                            newEntry.name = "VALID";
                        }
                        else
                        {
                            newEntry.name = "INVALID";
                        }

                        debugClass.AddEntry(newEntry);
                    }
                }
            }
        }

        if (Game.Instance.devMode && Game.Instance.collectDebugData && CityConstructor.Instance != null && CityConstructor.Instance.debugLoadTime != null)
        {
            stopWatch.Stop();
            timingInfo.time = (float)stopWatch.Elapsed.TotalSeconds;
            if (ret != null) timingInfo.found = true;
            else timingInfo.found = false;

            try
            {
                if (!CityConstructor.Instance.debugLoadTime.decorTimes.ContainsKey(room))
                {
                    CityConstructor.Instance.debugLoadTime.decorTimes.Add(room, new List<CityConstructor.DecorClusterGenerationTimeInfo>());
                }

                CityConstructor.Instance.debugLoadTime.decorTimes[room].Add(timingInfo);
            }
            catch
            {

            }
        }

        //Game.Log(ret.Count + " locations for for cluster " + cluster.name);

        return ret;
    }

    //Get the angle for furniture facing
    private int GetAngleForFurnitureFacing(FurnitureCluster.FurnitureFacing facing)
    {
        if (facing == FurnitureCluster.FurnitureFacing.up) return 0;
        else if (facing == FurnitureCluster.FurnitureFacing.down) return 180;
        else if (facing == FurnitureCluster.FurnitureFacing.left) return 270;
        else if (facing == FurnitureCluster.FurnitureFacing.right) return 90;

        return 0;
    }

    //New system
    public bool IsFurniturePlacementValid(NewRoom room, ref Dictionary<NewNode, List<NewNode>> newBlockAccess, ref List<NewNode> newNoPassNodes, ref List<NewNode> newNoAccessNodes, bool printDebug, out List<string> debugOutput, bool ignoreNoPassThrough = false)
    {
        //No pass nodes: Must be accessable, but cannot access more nodes
        //No access nodes: Ignore them completely
        debugOutput = null;

        if (printDebug)
        {
            debugOutput = new List<string>();
        }

        if (room == null)
        {
            Game.LogError("Trying to place furniture in null room!");
            return false;
        }

        //Find the entrances that need to be reachable...
        HashSet<NewNode> reachableEntrances = new HashSet<NewNode>();

        foreach (NewNode.NodeAccess ent in room.entrances)
        {
            if (!ent.walkingAccess) continue;
            if (ent.accessType != NewNode.NodeAccess.AccessType.door && ent.accessType != NewNode.NodeAccess.AccessType.openDoorway && ent.accessType != NewNode.NodeAccess.AccessType.adjacent) continue;

            //Add an entrance tile as this should always be acessable...
            if(ent.fromNode.room == room && !reachableEntrances.Contains(ent.fromNode))
            {
                reachableEntrances.Add(ent.fromNode);

                if (printDebug)
                {
                    debugOutput.Add("Found entrance node at " + ent.fromNode.position);
                }
            }

            if(ent.toNode.room == room && !reachableEntrances.Contains(ent.toNode))
            {
                reachableEntrances.Add(ent.toNode);

                if (printDebug)
                {
                    debugOutput.Add("Found entrance node at " + ent.fromNode.position);
                }
            }
        }

        bool pass = true;

        //Each entrance must be able to access every node...
        foreach (NewNode entranceNode in reachableEntrances)
        {
            //Each entrance must be able to reach the other...
            HashSet<NewNode> openSet = new HashSet<NewNode>();
            openSet.Add(entranceNode); //Start with this entrance...

            HashSet<NewNode> closedSet = new HashSet<NewNode>();

            //Performing a contains check here makes sure we're not ending up with duplicate nodes in the closed set, as this will cause problems!
            foreach (NewNode n in room.noAccessNodes)
            {
                if (!closedSet.Contains(n))
                {
                    closedSet.Add(n);
                }
            }

            //Make sure the closed set contains all the nodes that we don't have to access, including the new ones...
            if (newNoAccessNodes != null)
            {
                //Performing a contains check here makes sure we're not ending up with duplicate nodes in the closed set, as this will cause problems!
                foreach (NewNode n in newNoAccessNodes)
                {
                    if (!closedSet.Contains(n))
                    {
                        closedSet.Add(n);
                    }
                }
            }

            int safety = room.nodes.Count + 2;

            while (openSet.Count > 0 && safety > 0 && closedSet.Count < room.nodes.Count)
            {
                NewNode current = openSet.First();

                //if (printDebug) debugOutput.Add("Checking node " + current.position);

                //If we can't pass through this, skip the search pattern
                if ((current.noPassThrough && !ignoreNoPassThrough) || (newNoPassNodes != null && newNoPassNodes.Contains(current) && !ignoreNoPassThrough))
                {
                    //We can mark this as reached, but as we can't pass through it don't extend the search...
                    if (printDebug) debugOutput.Add("Node at " + current.position + " is reached but does not allow pass-through...");
                }
                else
                {
                    foreach (Vector2Int v2 in CityData.Instance.offsetArrayX8)
                    {
                        Vector3Int searchVector = current.nodeCoord + new Vector3Int(v2.x, v2.y, 0);
                        NewNode foundNode = null;

                        if (PathFinder.Instance.nodeMap.TryGetValue(searchVector, out foundNode))
                        {
                            //Is this in the reachable entrances set?
                            //reachableEntrances.Remove(foundNode);
                            if (foundNode.room != room) continue; //Different room: Invalid
                            if (closedSet.Contains(foundNode)) continue; //If already in the closed set we can safety skip this
                            if (openSet.Contains(foundNode)) continue;

                            //Diagonal vectors are only valid if there are no walls on the sandwiching vectors...
                            if (Mathf.Abs(v2.x) + Mathf.Abs(v2.y) == 2)
                            {
                                Vector2 diagOffset = new Vector2(v2.x * 0.5f, v2.y * 0.5f);

                                //You need to check both this node and the other node for the inverse offsets...
                                Vector2 offset1 = new Vector2(0, diagOffset.y);
                                Vector2 offset2 = new Vector2(diagOffset.x, 0);

                                if (current.walls.Exists(item => item.wallOffset == offset1 || item.wallOffset == offset2))
                                {
                                    continue; //Skip this vector search
                                }

                                offset1 = new Vector2(0, -diagOffset.y);
                                offset2 = new Vector2(-diagOffset.x, 0);

                                if (foundNode.walls.Exists(item => item.wallOffset == offset1 || item.wallOffset == offset2))
                                {
                                    continue; //Skip this vector search
                                }
                            }

                            //Check access: This should be accessible unless there's blocking access in the room controller
                            bool blocked = false;

                            //Check existing blocks (placed furniture)
                            if (room.blockedAccess == null) room.blockedAccess = new Dictionary<NewNode, List<NewNode>>();

                            try
                            {
                                if (room.blockedAccess.ContainsKey(current))
                                {
                                    //Access to this is blocked...
                                    if (room.blockedAccess[current].Contains(foundNode))
                                    {
                                        blocked = true;
                                    }
                                }

                                if (!blocked)
                                {
                                    //Check blocks passed to this
                                    if (newBlockAccess.ContainsKey(current))
                                    {
                                        //Access to this is blocked...
                                        if (newBlockAccess[current].Contains(foundNode))
                                        {
                                            blocked = true;
                                        }
                                    }
                                }
                            }
                            catch
                            {

                            }

                            //Free access
                            if (!blocked)
                            {
                                //if (printDebug) debugOutput.Add(" + Adding node " + current.position + " at offset " + v2);
                                openSet.Add(foundNode);
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                }

                closedSet.Add(current);
                openSet.Remove(current);
                safety--;
            }

            //At the end of this check, if the closed set count is equal to the room's node count then everything is accessable. Otherwise it's not
            if (closedSet.Count >= room.nodes.Count)
            {
                //Continue with the next entrance...
                pass = true;
                continue;
            }
            else
            {
                if (printDebug)
                {
                    debugOutput.Add("Fail: Closed Set = " + closedSet.Count + ", Room Count = " + room.nodes.Count + " reachable entrances remaining: " + reachableEntrances.Count +". Unreachable nodes are as follows:");

                    foreach(NewNode n in room.nodes)
                    {
                        if (closedSet.Contains(n)) continue;
                        debugOutput.Add("... " + n.position);
                    }
                }

                pass = false;
                break;
            }
        }

        return pass;
    }

    //Check to see if this furniture location is valid: Can all nodes be accessed?
    //public bool IsFurniturePlacementValid(NewRoom room, ref Dictionary<NewNode, List<NewNode>> newBlockAccess, List<NewNode> newNoPassNodes = null, List<NewNode> newNoAccessNodes = null, bool printDebug = false)
    //{
    //    //No pass nodes: Must be accessable, but cannot access more nodes
    //    //No access nodes: Ignore them completely

    //    if (printDebug) Game.Log("IsFurniturePlacementValid?... noPassNodes: " + newNoPassNodes);

    //    //Load or create a localised node map of the room
    //    //if(room.localizedRoomNodeMaps == null)
    //    //{
    //    //    room.localizedRoomNodeMaps = new Dictionary<Vector3, NewNode>();

    //    //    foreach(NewNode n in room.nodes)
    //    //    {
    //    //        //We need to add this and all surrounding nodes
    //    //        if(!room.localizedRoomNodeMaps.ContainsKey(n.nodeCoord))
    //    //        {
    //    //            room.localizedRoomNodeMaps.Add(n.nodeCoord, n);
    //    //        }

    //    //        foreach(Vector2 v2 in CityData.Instance.offsetArrayX8)
    //    //        {
    //    //            Vector3 searchVector = n.nodeCoord + new Vector3(v2.x, v2.y, 0);

    //    //            if (!room.localizedRoomNodeMaps.ContainsKey(searchVector))
    //    //            {
    //    //                NewNode foundNode = null;

    //    //                if (PathFinder.Instance.nodeMap.TryGetValue(searchVector, out foundNode))
    //    //                {
    //    //                    room.localizedRoomNodeMaps.Add(searchVector, foundNode);
    //    //                }
    //    //            }
    //    //        }
    //    //    }
    //    //}

    //    HashSet<NewNode> openSet = new HashSet<NewNode>();
    //    HashSet<NewNode> closedSet = new HashSet<NewNode>(room.noAccessNodes);

    //    //Add all no access nodes to the closed set
    //    if (newNoAccessNodes != null)
    //    {
    //        foreach(NewNode n in newNoAccessNodes)
    //        {
    //            closedSet.Add(n);
    //        }
    //    }

    //    //Flood fill must also reach these nodes
    //    HashSet<NewNode> reachableEntrances = new HashSet<NewNode>();

    //    foreach (NewNode.NodeAccess ent in room.entrances)
    //    {
    //        if (!ent.walkingAccess) continue;
    //        if (ent.accessType != NewNode.NodeAccess.AccessType.door && ent.accessType != NewNode.NodeAccess.AccessType.openDoorway && ent.accessType != NewNode.NodeAccess.AccessType.adjacent) continue;

    //        //Add an entrance tile as this should always be acessable...
    //        if (openSet.Count <= 0)
    //        {
    //            openSet.Add(ent.fromNode);
    //            if(printDebug) Game.Log("Starting with entrance node at " + ent.fromNode.position);
    //        }
    //        else
    //        {
    //            reachableEntrances.Add(ent.toNode);
    //        }
    //    }

    //    int safety = room.nodes.Count + 2;

    //    while (openSet.Count > 0 && safety > 0)
    //    {
    //        NewNode current = openSet.First();

    //        if (printDebug) Game.Log("Checking node " + current.position);

    //        //If we can't pass through this, skip the search pattern
    //        if (current.noPassThrough || (newNoPassNodes != null && newNoPassNodes.Contains(current)))
    //        {
    //            //We can mark this as reached, but as we can't pass through it don't extend the search...
    //        }
    //        else
    //        {
    //            //Is this in the reachable entrances set?
    //            reachableEntrances.Remove(current);

    //            foreach (Vector2 v2 in CityData.Instance.offsetArrayX8)
    //            {
    //                Vector3 searchVector = current.nodeCoord + new Vector3(v2.x, v2.y, 0);
    //                NewNode foundNode = null;

    //                if (PathFinder.Instance.nodeMap.TryGetValue(searchVector, out foundNode))
    //                {
    //                    //Is this in the reachable entrances set?
    //                    reachableEntrances.Remove(foundNode);
    //                    if (foundNode.room != room) continue; //Different room: Invalid
    //                    if (closedSet.Contains(foundNode)) continue; //If already in the closed set we can safety skip this
    //                    if (openSet.Contains(foundNode)) continue;

    //                    //Diagonal vectors are only valid if there are no walls on the sandwiching vectors...
    //                    if (Mathf.Abs(v2.x) + Mathf.Abs(v2.y) == 2)
    //                    {
    //                        Vector2 diagOffset = v2 * 0.5f;

    //                        //You need to check both this node and the other node for the inverse offsets...
    //                        Vector2 offset1 = new Vector2(0, diagOffset.y);
    //                        Vector2 offset2 = new Vector2(diagOffset.x, 0);

    //                        if (current.walls.Exists(item => item.wallOffset == offset1 || item.wallOffset == offset2))
    //                        {
    //                            continue; //Skip this vector search
    //                        }

    //                        offset1 = new Vector2(0, -diagOffset.y);
    //                        offset2 = new Vector2(-diagOffset.x, 0);

    //                        if (foundNode.walls.Exists(item => item.wallOffset == offset1 || item.wallOffset == offset2))
    //                        {
    //                            continue; //Skip this vector search
    //                        }
    //                    }

    //                    //Check access: This should be accessible unless there's blocking access in the room controller
    //                    bool blocked = false;

    //                    //Check existing blocks (placed furniture)
    //                    if (room.blockedAccess.ContainsKey(current))
    //                    {
    //                        //Access to this is blocked...
    //                        if (room.blockedAccess[current].Contains(foundNode))
    //                        {
    //                            blocked = true;
    //                        }
    //                    }

    //                    if (!blocked)
    //                    {
    //                        //Check blocks passed to this
    //                        if (newBlockAccess.ContainsKey(current))
    //                        {
    //                            //Access to this is blocked...
    //                            if (newBlockAccess[current].Contains(foundNode))
    //                            {
    //                                blocked = true;
    //                            }
    //                        }
    //                    }

    //                    //Free access
    //                    if (!blocked)
    //                    {
    //                        if (printDebug) Game.Log(" + Adding node " + current.position + " at offset " + v2);
    //                        openSet.Add(foundNode);
    //                    }
    //                }
    //                else continue;
    //            }
    //        }

    //        closedSet.Add(current);
    //        openSet.Remove(current);
    //        safety--;
    //    }

    //    //At the end of this check, if the closed set count is equal to the room's node count then everything is accessable. Otherwise it's not
    //    if (closedSet.Count >= room.nodes.Count && reachableEntrances.Count <= 0)
    //    {
    //        return true;
    //    }
    //    else
    //    {
    //        if (printDebug) Game.Log("Fail: Closed Set = " + closedSet.Count + ", Room Count = " + room.nodes.Count + " reachable entrances remaining: " + reachableEntrances.Count);
    //        return false;
    //    }
    //}

    //Check to see if this furniture location is valid: Can all nodes be accessed?
    public bool IsFurniturePlacementValidOLD(NewRoom room, ref Dictionary<NewNode, List<NewNode>> newBlockAccess, List<NewNode> newNoPassNodes = null, List<NewNode> newNoAccessNodes = null, bool printDebug = false)
    {
        //No pass nodes: Must be accessable, but cannot access more nodes
        //No access nodes: Ignore them completely

        if (printDebug) Game.Log("IsFurniturePlacementValid?... noPassNodes: " + newNoPassNodes);

        List<NewNode> openSet = new List<NewNode>();
        List<NewNode> closedSet = new List<NewNode>(room.noAccessNodes);

        //Add all no access nodes to the closed set
        if (newNoAccessNodes != null)
        {
            closedSet.AddRange(newNoAccessNodes);
        }

        //Add an entrance tile as this should always be acessable...
        openSet.Add(room.entrances[0].fromNode);

        int safety = 1000;

        while (openSet.Count > 0 && safety > 0)
        {
            NewNode current = openSet[0];

            //If we can't pass through this, skip the search pattern
            if (current.noPassThrough || (newNoPassNodes != null && newNoPassNodes.Contains(current)))
            {
                //We can mark this as reached, but as we can't pass through it don't extend the search...
            }
            else
            {
                foreach (Vector2Int v2 in CityData.Instance.offsetArrayX8)
                {
                    Vector3Int searchVector = current.nodeCoord + new Vector3Int(v2.x, v2.y, 0);
                    NewNode foundNode = null;

                    if (PathFinder.Instance.nodeMap.TryGetValue(searchVector, out foundNode))
                    {
                        if (foundNode.room != room) continue; //Different room: Invalid

                        //Diagonal vectors are only valid if there are no walls on the sandwiching vectors...
                        if (Mathf.Abs(v2.x) + Mathf.Abs(v2.y) == 2)
                        {
                            Vector2 diagOffset = new Vector2(v2.x * 0.5f, v2.y * 0.5f);

                            //You need to check both this node and the other node for the inverse offsets...
                            Vector2 offset1 = new Vector2(0, diagOffset.y);
                            Vector2 offset2 = new Vector2(diagOffset.x, 0);

                            if (current.walls.Exists(item => item.wallOffset == offset1 || item.wallOffset == offset2))
                            {
                                continue; //Skip this vector search
                            }

                            offset1 = new Vector2(0, -diagOffset.y);
                            offset2 = new Vector2(-diagOffset.x, 0);

                            if (foundNode.walls.Exists(item => item.wallOffset == offset1 || item.wallOffset == offset2))
                            {
                                continue; //Skip this vector search
                            }
                        }

                        if (closedSet.Contains(foundNode)) continue; //If already in the closed set we can safety skip this

                        //Check access: This should be accessible unless there's blocking access in the room controller
                        bool blocked = false;

                        //Check existing blocks (placed furniture)
                        if (room.blockedAccess.ContainsKey(current))
                        {
                            //Access to this is blocked...
                            if (room.blockedAccess[current].Contains(foundNode))
                            {
                                blocked = true;
                            }
                        }

                        if (!blocked)
                        {
                            //Check blocks passed to this
                            if (newBlockAccess.ContainsKey(current))
                            {
                                //Access to this is blocked...
                                if (newBlockAccess[current].Contains(foundNode))
                                {
                                    blocked = true;
                                }
                            }
                        }

                        //Free access
                        if (!blocked)
                        {
                            if (!openSet.Contains(foundNode))
                            {
                                openSet.Add(foundNode);
                            }
                        }
                    }
                    else continue;
                }
            }

            closedSet.Add(current);
            openSet.RemoveAt(0);
            safety--;
        }

        //At the end of this check, if the closed set count is equal to the room's node count then everything is accessable. Otherwise it's not
        if (closedSet.Count == room.nodes.Count)
        {
            return true;
        }
        else
        {
            if (printDebug) Game.Log("Fail: Closed Set = " + closedSet.Count + ", Room Count = " + room.nodes.Count);
            return false;
        }
    }

    //Pick a furniture piece based on the preset's suitability
    public FurniturePreset PickFurniture(FurnitureClass furnClass, NewRoom room, string randomSeed, bool debug = false)
    {
        List<FurniturePreset> possibleFurniture = null;

        //GetValidFurniture(furnClass, room, true, out possibleFurniture, false); //This shouldn't return 0/false

        //if (possibleFurniture == null || possibleFurniture.Count <= 0) return null;
        //if (possibleFurniture != null && possibleFurniture.Count == 1)
        //{
        //    Game.Log("... Chosing only option for " + furnClass.name);
        //    return possibleFurniture[0]; //If there's only 1 then go ahead and return it instead of bothering to calculate the below.
        //}

        //Now process using other stats...
        List<FurniturePreset> calculatedOptions = new List<FurniturePreset>();

        //Use cache
        if(room.pickFurnitureCache != null && room.pickFurnitureCache.ContainsKey(furnClass) && !furnClass.isSecurityCamera)
        {
            calculatedOptions = room.pickFurnitureCache[furnClass];
        }
        else
        {
            GetValidFurniture(furnClass, room, true, out possibleFurniture, false); //This shouldn't return 0/false

            if (possibleFurniture == null || possibleFurniture.Count <= 0) return null;
            if (possibleFurniture != null && possibleFurniture.Count == 1)
            {
                //Game.Log("... Chosing only option for " + furnClass.name);
                return possibleFurniture[0]; //If there's only 1 then go ahead and return it instead of bothering to calculate the below.
            }

            foreach (FurniturePreset furn in possibleFurniture)
            {
                if (room.gameLocation.thisAsAddress != null)
                {
                    int av = 2;

                    if(furn.usePersonalityWeighting)
                    {
                        //Weight by design style with a hint of owner's creativity (10%)
                        int modernity = 10 - Mathf.RoundToInt(Mathf.Abs(furn.modernity - (room.gameLocation.designStyle.modernity * 0.9f + room.gameLocation.thisAsAddress.averageCreativity)));

                        //Weight by room type...
                        int cleanness = 10 - Mathf.RoundToInt(Mathf.Abs(furn.cleanness - room.preset.cleanness));

                        //Weight by personality... 75% Extraversion,  25% -Agreeableness
                        int loudness = 10 - Mathf.RoundToInt(Mathf.Abs(furn.loudness - ((room.gameLocation.thisAsAddress.averageExtraversion * 7.5f) + ((1f - room.gameLocation.thisAsAddress.averageAgreeableness) * 2.5f))));

                        //Weight by personality... 70% Emotionality, 30% Creativity
                        int emotive = 10 - Mathf.RoundToInt(Mathf.Abs(furn.emotive - ((room.gameLocation.thisAsAddress.averageEmotionality * 7f) + (room.gameLocation.thisAsAddress.averageCreativity * 3f))));

                        //Add and divide by 8 to give a score out of 5
                        Mathf.Max(av = Mathf.FloorToInt((float)(modernity + cleanness + loudness + emotive) / 8f), 1);
                    }

                    for (int i = 0; i < av; i++)
                    {
                        calculatedOptions.Add(furn);
                    }
                }
                else
                {
                    calculatedOptions.Add(furn);
                }
            }

            if (room.pickFurnitureCache == null) room.pickFurnitureCache = new Dictionary<FurnitureClass, List<FurniturePreset>>();

            if(!room.pickFurnitureCache.ContainsKey(furnClass))
            {
                room.pickFurnitureCache.Add(furnClass, calculatedOptions); //Add to the cache incase we need to pick this furniture again...
            }
            else room.pickFurnitureCache[furnClass] = calculatedOptions; //Add to the cache incase we need to pick this furniture again...
        }

        //Return random of this
        if (calculatedOptions.Count > 0)
        {
            //Game.Log("... Chosing out of " + calculatedOptions.Count + " possible furniture options for " + furnClass.name);
            return calculatedOptions[Toolbox.Instance.GetPsuedoRandomNumber(0, calculatedOptions.Count, randomSeed)];
        }
        else if (possibleFurniture.Count > 0)
        {
            //Game.Log("... Chosing out of " + possibleFurniture.Count + " possible furniture options for " + furnClass.name);
            return possibleFurniture[Toolbox.Instance.GetPsuedoRandomNumber(0, possibleFurniture.Count, randomSeed)];
        }
        else
        {
            if (!debug)
            {
                Game.LogError("Unable to pick furniture of class " + furnClass.name + " for room " + room.preset.name);
                //GetValidFurniture(furnClass, room, true, out possibleFurniture, true);
            }

            return null;
        }
    }

    //Does this furniture class have a valid entry: Also with option to retun all compatible furniture
    public bool GetValidFurniture(FurnitureClass furnClass, NewRoom room, bool returnList, out List<FurniturePreset> possibleFurniture, bool debug = false)
    {
        float wealthLevel = Toolbox.Instance.GetNormalizedLandValue(room.gameLocation);

        possibleFurniture = null;

        //List of furniture that fits design style and meets minimum wealth etc...
        foreach (FurniturePreset furn in Toolbox.Instance.furnitureDesignStyleRef[room.gameLocation.designStyle])
        {
            //This is run *a lot* during generation, so make the filtering process optimal...
            if (furn == null)
            {
                continue;
            }

            if (debug) Game.Log("---" + furn.name + "---");

            if (wealthLevel < furn.minimumWealth)
            {
                if (debug) Game.Log("... Below minimum wealth of " + furn.minimumWealth);
                continue;
            }

            if (room.nodes.Count < furn.minimumRoomSize)
            {
                if (debug) Game.Log("... Below minimum room size of " + furn.minimumRoomSize);
                continue;
            }

            //No open plan
            if (furn.allowedInOpenPlan == FurnitureCluster.AllowedOpenPlan.no && room.openPlanElements.Count > 0)
            {
                continue;
            }

            if (furn.furnitureGroup != FurniturePreset.FurnitureGroup.none)
            {
                if (room.furnitureGroups.ContainsKey(furn.furnitureGroup))
                {
                    if (furn.groupID != room.furnitureGroups[furn.furnitureGroup])
                    {
                        if (debug) Game.Log("... Furniture group doesn't match: " + room.furnitureGroups[furn.furnitureGroup]);
                        continue;
                    }
                }
            }

            //Check valid class...
            if (!furn.classes.Contains(furnClass))
            {
                if (debug) Game.Log("... Not in class " + furnClass.name);
                continue;
            }

            //Limit security cameras in room
            if (furn.isSecurityCamera && room.preset.limitSecurityCameras)
            {
                List<FurnitureLocation> cams = room.individualFurniture.FindAll(item => item.furniture.isSecurityCamera);

                if (cams.Count >= room.preset.securityCameraLimit)
                {
                    if (debug) Game.Log("... Security camera limit...");
                    continue;
                }
            }

            if (debug) Game.Log("... Checking room compatibility...");

            //Only allow in certain buildings...
            if (!SessionData.Instance.isFloorEdit)
            {
                if (furn.OnlyAllowInBuildings)
                {
                    if (room.gameLocation.building == null || !furn.allowedInBuildings.Contains(room.gameLocation.building.preset))
                    {
                        continue;
                    }
                }

                if (furn.banFromBuildings)
                {
                    if (room.gameLocation.building != null && furn.notAllowedInBuildings.Contains(room.gameLocation.building.preset))
                    {
                        continue;
                    }
                }
            }

            //Only allow in certain districts...
            if (!SessionData.Instance.isFloorEdit)
            {
                if (furn.OnlyAllowInDistricts)
                {
                    if (!furn.allowedInDistricts.Contains(room.gameLocation.district.preset))
                    {
                        continue;
                    }
                }

                if (furn.banFromDistricts)
                {
                    if (furn.notAllowedInDistricts.Contains(room.gameLocation.district.preset))
                    {
                        continue;
                    }
                }
            }

            //Gendered inhabitants
            if(furn.requiresGenderedInhabitants)
            {
                if (room.gameLocation.thisAsAddress != null)
                {
                    bool genderPass = true;

                    foreach (Human h in room.gameLocation.thisAsAddress.inhabitants)
                    {
                        if (!furn.enableIfGenderPresent.Contains(h.gender))
                        {
                            genderPass = false;
                        }
                    }

                    if (!genderPass)
                    {
                        continue;
                    }
                }
                else continue;
            }

            //Only allow in certain address presets...
            if (furn.onlyAllowInFollowing)
            {
                if (room.gameLocation.thisAsAddress != null && room.gameLocation.thisAsAddress.addressPreset != null)
                {
                    if (!furn.allowedInAddressesOfType.Contains(room.gameLocation.thisAsAddress.addressPreset))
                    {
                        if (debug) Game.Log("... Not allowed in address " + room.gameLocation.thisAsAddress.addressPreset.name);
                        continue;
                    }
                }
                else
                {
                    if (debug) Game.Log("... Not assigned an address preset");
                    continue;
                }
            }

            if (furn.banInFollowing)
            {
                if (room.gameLocation.thisAsAddress != null && room.gameLocation.thisAsAddress.addressPreset != null)
                {
                    if (furn.bannedInAddressesOfType.Contains(room.gameLocation.thisAsAddress.addressPreset))
                    {
                        if (debug) Game.Log("... Banned in address " + room.gameLocation.thisAsAddress.addressPreset.name);
                        continue;
                    }
                }
            }

            //Suitable for this room?
            bool pass = false;

            foreach (RoomTypeFilter filter in furn.allowedRoomFilters)
            {
                if (filter == null)
                {
                    Game.LogError("Null filter found in " + furn.name);
                    continue;
                }

                //Matching base room
                if (furn.allowedInOpenPlan != FurnitureCluster.AllowedOpenPlan.openPlanOnly)
                {
                    HashSet<FurniturePreset> foundFurniture = null;

                    if (Toolbox.Instance.furnitureRoomTypeRef.TryGetValue(room.preset.roomClass, out foundFurniture))
                    {
                        if (foundFurniture.Contains(furn))
                        {
                            pass = true;
                            break;
                        }
                    }
                }

                foreach (RoomConfiguration rt in room.openPlanElements)
                {
                    if (furn.allowedInOpenPlan != FurnitureCluster.AllowedOpenPlan.no)
                    {
                        HashSet<FurniturePreset> foundFurniture = null;

                        if (Toolbox.Instance.furnitureRoomTypeRef.TryGetValue(rt.roomClass, out foundFurniture))
                        {
                            if (foundFurniture.Contains(furn))
                            {
                                pass = true;
                                break;
                            }
                        }
                    }
                }
            }

            if (!pass)
            {
                if (debug) Game.Log("... Open plan/filter check failed.");
                continue;
            }

            if (returnList)
            {
                if (possibleFurniture == null) possibleFurniture = new List<FurniturePreset>();
                possibleFurniture.Add(furn);
            }
            else return true;
        }

        if (possibleFurniture == null || possibleFurniture.Count <= 0)
        {
            return false;
        }
        else
        {
            //Game.Log("Searching for " + furnClass.name + " with design style " + room.gameLocation.designStyle.name + ": " + possibleFurniture.Count);
            return true;
        }
    }

    //Pick an art piece based on the preset's suitability
    public ArtPreset PickArt(ArtPreset.ArtOrientation orientation, NewRoom room)
    {
        float wealthLevel = 0f;

        if (room.gameLocation.thisAsAddress != null)
        {
            wealthLevel = Toolbox.Instance.GetNormalizedLandValue(room.gameLocation.thisAsAddress);
        }

        List<ArtPreset> possibleArt = new List<ArtPreset>();

        //List of furniture that fits orientation and meets minimum wealth and are not already present in the address
        possibleArt.AddRange(Toolbox.Instance.allArt.FindAll(item => item.orientationCompatibility.Contains(orientation) && wealthLevel >= item.minimumWealth && wealthLevel <= item.maximumWealth && !room.gameLocation.artPieces.Contains(item) && !item.disable));
        if (possibleArt.Count == 1) return possibleArt[0]; //If there's only 1 then go ahead and return it instead of bothering to calculate the below.

        //Now process using other stats...
        List<ArtPreset> calculatedOptions = new List<ArtPreset>();

        foreach (ArtPreset art in possibleArt)
        {
            if (!art.allowInCommerical && room.gameLocation.thisAsAddress != null && room.gameLocation.thisAsAddress.company != null) continue;
            if (!art.allowInResidential && room.gameLocation.thisAsAddress != null && room.gameLocation.thisAsAddress.residence != null) continue;
            if (!art.allowInLobby && room.gameLocation.thisAsAddress != null && room.gameLocation.thisAsAddress.isLobby) continue;
            if (!art.allowOnStreet && room.gameLocation.thisAsStreet != null) continue;

            int basePriority = art.basePriority;

            if (room.gameLocation.thisAsAddress != null)
            {
                bool traitFound = false;

                foreach (ArtPreset.ArtPreference pref in art.traitModifiers)
                {
                    foreach (Human owner in room.gameLocation.thisAsAddress.owners)
                    {
                        if (owner.characterTraits.Exists(item => item.trait == pref.trait))
                        {
                            traitFound = true;
                            basePriority += pref.modifier;
                        }
                    }
                }

                if (art.mustRequireTraitFromBelow && !traitFound) continue;
            }

            int modernity = 2;

            //Weight by room type...
            int cleanness = 2;

            //Weight by personality... 75% Extraversion,  25% -Agreeableness
            int loudness = 2;

            //Weight by personality... 70% Emotionality, 30% Creativity
            int emotive = 2;

            int colourMatchScore = 0;

            //Weight by design style with a hint of owner's creativity (10%)
            if (room.gameLocation != null && room.gameLocation.thisAsAddress != null)
            {
                modernity = 10 - Mathf.RoundToInt(Mathf.Abs(art.modernity - (room.gameLocation.designStyle.modernity * 0.9f + room.gameLocation.thisAsAddress.averageCreativity)));

                //Weight by room type...
                cleanness = 10 - Mathf.RoundToInt(Mathf.Abs(art.cleanness - room.preset.cleanness));

                //Weight by personality... 75% Extraversion,  25% -Agreeableness
                loudness = 10 - Mathf.RoundToInt(Mathf.Abs(art.loudness - ((room.gameLocation.thisAsAddress.averageExtraversion * 7.5f) + ((1f - room.gameLocation.thisAsAddress.averageAgreeableness) * 2.5f))));

                //Weight by personality... 70% Emotionality, 30% Creativity
                emotive = 10 - Mathf.RoundToInt(Mathf.Abs(art.emotive - ((room.gameLocation.thisAsAddress.averageEmotionality * 7f) + (room.gameLocation.thisAsAddress.averageCreativity * 3f))));

                if(room.colourScheme != null)
                {
                    float score = 0f;

                    foreach(Color c in art.colourMatching)
                    {
                        float matchPrimary1 = (Mathf.Abs(c.r - room.colourScheme.primary1.r) + Mathf.Abs(c.g - room.colourScheme.primary1.g) + Mathf.Abs(c.b - room.colourScheme.primary1.b)) / 3f;
                        float matchPrimary2 = (Mathf.Abs(c.r - room.colourScheme.primary2.r) + Mathf.Abs(c.g - room.colourScheme.primary2.g) + Mathf.Abs(c.b - room.colourScheme.primary2.b)) / 3f;
                        float matchSecondary1 = (Mathf.Abs(c.r - room.colourScheme.secondary1.r) + Mathf.Abs(c.g - room.colourScheme.secondary1.g) + Mathf.Abs(c.b - room.colourScheme.secondary1.b)) / 3f;
                        float matchSecondary2= (Mathf.Abs(c.r - room.colourScheme.secondary2.r) + Mathf.Abs(c.g - room.colourScheme.secondary2.g) + Mathf.Abs(c.b - room.colourScheme.secondary2.b)) / 3f;

                        score += Mathf.Max(new float[] { matchPrimary1, matchPrimary2, matchSecondary1, matchSecondary2 }); //Choose best match; returns a 0 - 1 value
                    }

                    score /= (float)art.colourMatching.Count; //Returns average of all colours
                    colourMatchScore = Mathf.RoundToInt(score * art.colourMatchingScale);
                }
            }

            //Add and divide by 8 to give a score out of 10, then * by the base priority
            int av = Mathf.FloorToInt((float)((modernity + cleanness + loudness + emotive) / 40f) * art.colourMatchingScale) + colourMatchScore + basePriority;

            for (int i = 0; i < av; i++)
            {
                calculatedOptions.Add(art);
            }
        }

        //Return random of this
        if (calculatedOptions.Count > 0)
        {
            return calculatedOptions[Toolbox.Instance.GetPsuedoRandomNumber(0, calculatedOptions.Count, room.seed)];
        }
        else if (possibleArt.Count > 0)
        {
            return possibleArt[Toolbox.Instance.GetPsuedoRandomNumber(0, possibleArt.Count, room.seed)];
        }
        else
        {
            Game.Log("CityGen: Unable to pick art of orentation " + orientation.ToString() + " for " + room.roomID + " with soc level: " + wealthLevel + ", picking random...");

            possibleArt.AddRange(Toolbox.Instance.allArt.FindAll(item => item.orientationCompatibility.Contains(orientation) && !item.disable));
            return possibleArt[Toolbox.Instance.GetPsuedoRandomNumber(0, possibleArt.Count, room.seed)];
        }
    }

    //Get the node offset to this coordinate
    private bool GetAdjacentNode(NewNode original, Vector2Int offset, out NewNode output)
    {
        output = null;
        Vector2Int nodeCheck = original.floorCoord + offset;

        if (original.floor.nodeMap.ContainsKey(nodeCheck))
        {
            output = original.floor.nodeMap[nodeCheck];
            return true;
        }
        else return false;
    }

    //Get furniture in city
    public List<FurnitureLocation> GetFurnitureInCity(FurnitureClass furnClass)
    {
        List<FurnitureLocation> ret = new List<FurnitureLocation>();

        for (int b = 0; b < CityData.Instance.buildingDirectory.Count; b++)
        {
            NewBuilding building = CityData.Instance.buildingDirectory[b];

            foreach(KeyValuePair<int, NewFloor> pair in building.floors)
            {
                for (int a = 0; a < pair.Value.addresses.Count; a++)
                {
                    NewAddress address = pair.Value.addresses[a];

                    for (int r = 0; r < address.rooms.Count; r++)
                    {
                        NewRoom room = address.rooms[r];

                        for (int u = 0; u < room.individualFurniture.Count; u++)
                        {
                            FurnitureLocation loc = room.individualFurniture[u];

                            try
                            {
                                if (loc.furnitureClasses.Contains(furnClass))
                                {
                                    ret.Add(loc);
                                }
                            }
                            catch
                            {

                            }
                        }
                    }
                }
            }
        }

        for (int s = 0; s < CityData.Instance.streetDirectory.Count; s++)
        {
            StreetController street = CityData.Instance.streetDirectory[s];

            for (int r = 0; r < street.rooms.Count; r++)
            {
                NewRoom room = street.rooms[r];

                for (int u = 0; u < room.individualFurniture.Count; u++)
                {
                    FurnitureLocation loc = room.individualFurniture[u];

                    try
                    {
                        if (loc.furnitureClasses.Contains(furnClass))
                        {
                            ret.Add(loc);
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        return ret;
    }

    //Get furniture in building
    public List<FurnitureLocation> GetFurnitureInBuilding(NewBuilding building, FurnitureClass furnClass)
    {
        List<FurnitureLocation> ret = new List<FurnitureLocation>();

        foreach(KeyValuePair<int, NewFloor> pair in building.floors)
        {
            for (int a = 0; a < pair.Value.addresses.Count; a++)
            {
                NewAddress address = pair.Value.addresses[a];

                for (int r = 0; r < address.rooms.Count; r++)
                {
                    NewRoom room = address.rooms[r];

                    for (int u = 0; u < room.individualFurniture.Count; u++)
                    {
                        FurnitureLocation loc = room.individualFurniture[u];

                        try
                        {
                            if(loc.furnitureClasses.Contains(furnClass))
                            {
                                ret.Add(loc);
                            }
                        }
                        catch
                        {

                        }
                    }
                }
            }
        }

        return ret;
    }

    //Get furniture in address
    public List<FurnitureLocation> GetFurnitureInGameLocation(NewGameLocation address, FurnitureClass furnClass)
    {
        List<FurnitureLocation> ret = new List<FurnitureLocation>();

        for (int r = 0; r < address.rooms.Count; r++)
        {
            NewRoom room = address.rooms[r];

            for (int u = 0; u < room.individualFurniture.Count; u++)
            {
                FurnitureLocation loc = room.individualFurniture[u];

                if (loc.furnitureClasses.Contains(furnClass))
                {
                    ret.Add(loc);
                }
            }
        }

        return ret;
    }

    //Get furniture in room
    public List<FurnitureLocation> GetFurnitureInRoom(NewRoom room, FurnitureClass furnClass)
    {
        List<FurnitureLocation> ret = new List<FurnitureLocation>();

        for (int u = 0; u < room.individualFurniture.Count; u++)
        {
            FurnitureLocation loc = room.individualFurniture[u];

            if (loc.furnitureClasses.Contains(furnClass))
            {
                ret.Add(loc);
            }
        }

        return ret;
    }

    //Get clusters in city
    public List<FurnitureClusterLocation> GetClustersInCity(FurnitureCluster cluster)
    {
        List<FurnitureClusterLocation> ret = new List<FurnitureClusterLocation>();

        for (int b = 0; b < CityData.Instance.buildingDirectory.Count; b++)
        {
            NewBuilding building = CityData.Instance.buildingDirectory[b];

            foreach(KeyValuePair<int, NewFloor> pair in building.floors)
            {
                for (int a = 0; a < pair.Value.addresses.Count; a++)
                {
                    NewAddress address = pair.Value.addresses[a];

                    for (int r = 0; r < address.rooms.Count; r++)
                    {
                        NewRoom room = address.rooms[r];

                        for (int u = 0; u < room.furniture.Count; u++)
                        {
                            FurnitureClusterLocation loc = room.furniture[u];

                            try
                            {
                                if (loc.cluster == cluster)
                                {
                                    ret.Add(loc);
                                }
                            }
                            catch
                            {

                            }
                        }
                    }
                }
            }
        }

        for (int s = 0; s < CityData.Instance.streetDirectory.Count; s++)
        {
            StreetController street = CityData.Instance.streetDirectory[s];

            for (int r = 0; r < street.rooms.Count; r++)
            {
                NewRoom room = street.rooms[r];

                for (int u = 0; u < room.furniture.Count; u++)
                {
                    FurnitureClusterLocation loc = room.furniture[u];

                    try
                    {
                        if (loc.cluster == cluster)
                        {
                            ret.Add(loc);
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }

        return ret;
    }

    //Get clusters in building
    public List<FurnitureClusterLocation> GetClustersInBuilding(NewBuilding building, FurnitureCluster cluster)
    {
        List<FurnitureClusterLocation> ret = new List<FurnitureClusterLocation>();

        foreach(KeyValuePair<int, NewFloor> pair in building.floors)
        {
            for (int a = 0; a < pair.Value.addresses.Count; a++)
            {
                NewAddress address = pair.Value.addresses[a];

                for (int r = 0; r < address.rooms.Count; r++)
                {
                    NewRoom room = address.rooms[r];

                    for (int u = 0; u < room.furniture.Count; u++)
                    {
                        FurnitureClusterLocation loc = room.furniture[u];

                        try
                        {
                            if (loc.cluster == cluster)
                            {
                                ret.Add(loc);
                            }
                        }
                        catch
                        {

                        }
                    }
                }
            }
        }

        return ret;
    }

    //Get clusters in address
    public List<FurnitureClusterLocation> GetClustersInGameLocation(NewGameLocation address, FurnitureCluster cluster)
    {
        List<FurnitureClusterLocation> ret = new List<FurnitureClusterLocation>();

        for (int r = 0; r < address.rooms.Count; r++)
        {
            NewRoom room = address.rooms[r];

            for (int u = 0; u < room.furniture.Count; u++)
            {
                FurnitureClusterLocation loc = room.furniture[u];

                try
                {
                    if (loc.cluster == cluster)
                    {
                        ret.Add(loc);
                    }
                }
                catch
                {

                }
            }
        }

        return ret;
    }

    //Clear the cache
    public void ClearCache()
    {
        Game.Log("Clearing interior generation cache...");
        Toolbox.Instance.furnitureDesignStyleRef = null;
        Toolbox.Instance.furnitureRoomTypeRef = null;
    }
}
