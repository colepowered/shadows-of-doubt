﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class GenerationDebugController : MonoBehaviour
{
    [Tooltip("This room attempt was valid")]
    public bool valid = false;
    [Tooltip("This room attempt was successful")]
    public bool executed = false;
    [Tooltip("The configuration of the attempted room")]
    public RoomTypePreset preset;
    [Tooltip("The generated room location")]
    public GenerationController.PossibleRoomLocation location;

    //Log debug
    public List<string> log = new List<string>();

    [System.NonSerialized]
    public List<NewNode> attemptedValidNodes = null; //All valid nodes
    public Dictionary<NewNode, string> overridenNodes = new Dictionary<NewNode, string>(); //Contained in above but these have overriden a room
    public Dictionary<NewNode, string> attemptedInvalidNodes = new Dictionary<NewNode, string>(); //Attempted but invalid

    //Spawned objects
    private List<GameObject> spawnedObjects = new List<GameObject>();

    public void Setup(string newName, RoomTypePreset newPreset)
    {
        this.name = newName;
        preset = newPreset;
    }

    public void Log(string newLog)
    {
        log.Add(newLog);
    }

    [Button]
    public void DisplayAttempedArea()
    {
        if (FloorEditController.Instance.currentlyDisplayingArea != null) FloorEditController.Instance.currentlyDisplayingArea.RemoveAttempedArea();
        RemoveAttempedArea(); //First remove any spawned

        foreach(NewNode node in attemptedValidNodes)
        {
            GameObject newDisp = Instantiate(PrefabControls.Instance.debugNodeDisplay, this.transform);
            newDisp.transform.position = node.position;
            spawnedObjects.Add(newDisp);

            if(overridenNodes.ContainsKey(node))
            {
                newDisp.GetComponent<MeshRenderer>().material.SetColor("_BaseColor", Color.blue);
                //newDisp.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.blue);
                newDisp.name = "OVR " + overridenNodes[node] + " Valid";
            }
            else
            {
                newDisp.GetComponent<MeshRenderer>().material.SetColor("_BaseColor", Color.green);
                //newDisp.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.green);
                newDisp.name = "Valid";
            }
        }

        foreach (KeyValuePair<NewNode, string> pair in attemptedInvalidNodes)
        {
            GameObject newDisp = Instantiate(PrefabControls.Instance.debugNodeDisplay, this.transform);
            newDisp.transform.position = pair.Key.position;
            newDisp.name = pair.Value;
            spawnedObjects.Add(newDisp);

            newDisp.GetComponent<MeshRenderer>().material.SetColor("_BaseColor", Color.red);
            //newDisp.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.red);
        }

        FloorEditController.Instance.currentlyDisplayingArea = this;
    }

    [Button]
    public void RemoveAttempedArea()
    {
        while(spawnedObjects.Count > 0)
        {
            Destroy(spawnedObjects[0]);
            spawnedObjects.RemoveAt(0);
        }

        if (FloorEditController.Instance.currentlyDisplayingArea == this) FloorEditController.Instance.currentlyDisplayingArea = null;
    }
}
