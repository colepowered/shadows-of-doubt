﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoSelectController : MonoBehaviour
{
    [Header("References")]
    public RectTransform pageRect;
    public WindowContentController wcc;

    [Header("Prefabs")]
    public GameObject photoPrefab;

    private List<PhotoSelectButtonController> spawned = new List<PhotoSelectButtonController>();

    public class CitAsk
    {
        public Human citizen;
        public Case.CaseElement element;
    }

    public void Setup(WindowContentController newWcc)
    {
        wcc = newWcc;

        while(spawned.Count > 0)
        {
            Destroy(spawned[0].gameObject);
            spawned.RemoveAt(0);
        }

        //Get pinned citizen photos
        List<CitAsk> getCitizens = new List<CitAsk>();

        foreach(Case c in CasePanelController.Instance.activeCases)
        {
            foreach (Case.CaseElement el in c.caseElements)
            {
                Evidence ev = null;

                if (GameplayController.Instance.evidenceDictionary.TryGetValue(el.id, out ev))
                {
                    EvidenceCitizen cit = ev as EvidenceCitizen;

                    if (cit != null)
                    {
                        getCitizens.Add(new CitAsk { citizen = cit.witnessController, element = el });
                    }
                }
            }
        }

        float xPos = 16;
        float yPos = -16;

        foreach(CitAsk a in getCitizens)
        {
            Human c = a.citizen;

            if(c != null)
            {
                //if (c.interactable == InteractionController.Instance.talkingTo) continue;

                GameObject newObject = Instantiate(photoPrefab, pageRect);
                PhotoSelectButtonController psbc = newObject.GetComponent<PhotoSelectButtonController>();
                spawned.Add(psbc);

                psbc.rect.anchoredPosition = new Vector2(xPos, yPos);

                psbc.Setup(c, a.element, wcc.window);

                xPos += psbc.rect.sizeDelta.x + 10f;

                if(xPos > 500)
                {
                    xPos = 12;
                    yPos -= psbc.rect.sizeDelta.y + 10f;
                }

                pageRect.sizeDelta = new Vector2(pageRect.sizeDelta.x, -yPos + psbc.rect.sizeDelta.y + 32f);
            }
        }
    }
}
