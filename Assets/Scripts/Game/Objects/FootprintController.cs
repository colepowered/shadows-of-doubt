using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

public class FootprintController : MonoBehaviour
{
    private const int INITIAL_POOL_SIZE = 300;
    private const float RECYCLED_Y_POSITION = -1000f;
    private static Queue<FootprintController> footprintPool = new Queue<FootprintController>();

    [Header("Components")]
    public GameplayController.Footprint footprint;
    public MeshRenderer quad;
    public DecalProjector projector;
    public Human human;

    [Header("Values/Settings")]
    [Tooltip("Use a quad instead of a decal projector")]
    public bool useQuad = false;
    public float scanProgress = 0f;
    public bool printConfirmed = false;
    public InteractableController printInteractable;

    public void Setup(GameplayController.Footprint newFootprint)
    {
        footprint = newFootprint;

        CityData.Instance.GetHuman(footprint.hID, out human);

        SetUseQuad(Game.Instance.useQuadsForFootprints);

        //Set scale of this
        float normalizedShoeSize = Mathf.InverseLerp(CitizenControls.Instance.shoeSizeRange.x, CitizenControls.Instance.shoeSizeRange.y, human.descriptors.shoeSize);
        float lerpedScale = Mathf.Lerp(GameplayControls.Instance.footprintScaleRange.x, GameplayControls.Instance.footprintScaleRange.y, normalizedShoeSize);

        this.transform.localScale = new Vector3(lerpedScale, lerpedScale, lerpedScale);

        //Calculate position
        this.transform.position = footprint.wP;
        this.transform.eulerAngles = footprint.eU;

        //Setup already confirmed...
        Interactable confirmedInteractable = null;

        if (GameplayController.Instance.confirmedFootprints.TryGetValue(footprint.wP, out confirmedInteractable))
        {
            scanProgress = 1f;
            printConfirmed = true;
            printInteractable = this.gameObject.AddComponent<InteractableController>();
            printInteractable.Setup(confirmedInteractable);
            InteractionController.Instance.OnPlayerLookAtChange();
        }
    }

    //Setup to either use quads or decal projectors
    public void SetUseQuad(bool val)
    {
        useQuad = val;

        if (useQuad && quad != null)
        {
            quad.sharedMaterial = MaterialsController.Instance.GetFootprintMaterial(this); //Get material (new or existing)
            quad.enabled = true;

            if(projector != null) projector.transform.gameObject.SetActive(false);
            quad.transform.gameObject.SetActive(true);
        }
        else if(!useQuad && projector != null)
        {
            projector.material = MaterialsController.Instance.GetFootprintMaterial(this); //Get material (new or existing)
            projector.enabled = true;

            projector.transform.gameObject.SetActive(true);
            if(quad != null) quad.transform.gameObject.SetActive(false);
        }
    }

    public void ResetScan()
    {
        if (printConfirmed) return; //Don't reset if we've created interactable
        scanProgress = 0f;
    }

    public void PrintConfirmed()
    {
        if (!printConfirmed)
        {
            printConfirmed = true;

            //I don't think this can ever happen, but don't create a print at exactly this position...
            if (GameplayController.Instance.confirmedFootprints.ContainsKey(footprint.wP)) return;

            //Create interactable
            printInteractable = this.gameObject.AddComponent<InteractableController>();

            Interactable newPrint = null;

            Game.Log("Player: Discovered print belonging to " + human.GetCitizenName());

            newPrint = InteractableCreator.Instance.CreateFootprintInteractable(human, footprint.wP, footprint.eU, footprint);

            printInteractable.Setup(newPrint);

            InteractionController.Instance.OnPlayerLookAtChange();
        }
    }

    public static void InitialisePool()
    {
        for (int i = 0; i < INITIAL_POOL_SIZE; i++)
        {
            FootprintController footprintController = Instantiate(PrefabControls.Instance.footprint).GetComponent<FootprintController>();
#if UNITY_EDITOR
            footprintController.transform.SetParent(Toolbox.PoolingGroup.transform);
#else
            footprintController.transform.SetParent(null);
#endif
            footprintController.transform.position = new Vector3(0, RECYCLED_Y_POSITION, 0);
            footprintPool.Enqueue(footprintController);
        }
    }

    public static FootprintController GetNewFootprint()
    {
        //Made some changes here as I think footprints are being removed elsewhere...
        FootprintController footprintController = null;

        if (footprintPool.Count > 0)
        {
            while(footprintPool.Count > 0 && footprintPool.Peek() == null)
            {
                footprintPool.Dequeue();
            }

            if(footprintPool.Count > 0)
            {
                footprintController = footprintPool.Dequeue();
                return footprintController;
            }
        }

        //If nothing has been returned, make a new one...
        footprintController = Instantiate(PrefabControls.Instance.footprint).GetComponent<FootprintController>();

#if UNITY_EDITOR
        footprintController.transform.SetParent(Toolbox.PoolingGroup.transform);
#else
        footprintController.transform.SetParent(null);
#endif
        return footprintController;
    }

    public static void RecycleFootprint(FootprintController footprintController)
    {
#if UNITY_EDITOR
        footprintController.transform.SetParent(Toolbox.PoolingGroup.transform);
#else
            footprintController.transform.SetParent(null);
#endif
        footprintController.transform.position = new Vector3(0, RECYCLED_Y_POSITION, 0);
        footprintController.scanProgress = 0f;
        footprintPool.Enqueue(footprintController);
    }
}
