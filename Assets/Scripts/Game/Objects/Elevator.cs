﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator
{
    [Tooltip("Setup")]
    public NewBuilding building;
    public Transform spawnedObject;
    public StairwellPreset preset;
    [System.NonSerialized]
    public Interactable controls;
    public Collider vehicleDetector;
    public Transform cable1;
    public Transform cable2;
    public AudioController.LoopingSoundInfo movementAudio;

    [System.Serializable]
    public class ElevatorFloor
    {
        public int floor = 0;
        public NewTile elevatorTile;
        public NewRoom elevatorRoom;
        public GameObject spawned;
        [System.NonSerialized]
        public Interactable upButton;
        [System.NonSerialized]
        public Interactable downButton;
        [System.NonSerialized]
        public Interactable door;
    }

    [System.Serializable]
    public class ElevatorCall
    {
        public ElevatorFloor floor;
        public bool callUp;
        public float registered;

        public ElevatorCall(ElevatorFloor newFloor, bool newUp, float newRegistered)
        {
            floor = newFloor;
            callUp = newUp;
            registered = newRegistered;
        }
    }

    [Tooltip("List of floors that the elevator can travel too using the passed elevator rooms")]
    public Dictionary<int, ElevatorFloor> elevatorFloors = new Dictionary<int, ElevatorFloor>();

    [Header("Game State")]
    [Tooltip("The bottom tile (start")]
    public NewTile bottom;
    [Tooltip("The top")]
    public NewTile top;
    private float reachedSpeed = 0f;
    public float currentSpeed = 0f;
    public float desiredY = 0f;
    public float liftTimer = 0f;

    [Tooltip("The elevator's current floor position")]
    public int currentFloor = 0;
    [Tooltip("Is the elevator currently moving?")]
    public bool inTransit = false;
    [Tooltip("The elevator's current direction")]
    public bool isGoingUp = true;
    [Tooltip("The elevator's current destination: The next floor this will stop at")]
    public int currentDestination = 0;
    [Tooltip("The elevator's current destination: The furthest floor this will stop at")]
    public int ultimateDesitnation = 0;

    public bool isActive = false; //True if this elevator needs to move
    public bool isMoving = false;

    [Tooltip("Used for elevator AI: Call on floors on the way to destination")]
    public Dictionary<int, List<ElevatorCall>> calls = new Dictionary<int, List<ElevatorCall>>();

    public Elevator(StairwellPreset newPreset, NewBuilding newBuilding, NewTile newBottom)
    {
        preset = newPreset;
        bottom = newBottom; //This might not actually be the bottom, but that will auto-correct as we add new floors
        building = newBuilding;

        building.stairwells.Add(bottom, this); //Add to build's ref

        AddFloor(bottom);

        //Spawn the elevator (parent to building)
        //The elevators spawns at the created floor
        currentFloor = (int)bottom.globalTileCoord.z;
        currentDestination = (int)bottom.globalTileCoord.z;
        ultimateDesitnation = (int)bottom.globalTileCoord.z;

        //Is this a lift?
        if (preset.featuresElevator)
        {
            //Game.Log("CityGen: Spawning elevator object for " + building.name + " with bottom on floor " + bottom.floor.floor);

            spawnedObject = Toolbox.Instance.SpawnObject(preset.elevatorObject, newBottom.parent).transform;

            //Setup killbox
            ElevatorKillBox kb = spawnedObject.GetComponentInChildren<ElevatorKillBox>();
            if (kb != null) kb.elevator = this;

            //Parent to node that isn't stairs
            foreach(NewNode node in newBottom.nodes)
            {
                if (node.room != null && !node.room.isNullRoom)
                {
                    spawnedObject.SetParent(node.room.transform, true);
                    break;
                }
            }

            spawnedObject.position = newBottom.position + new Vector3(0, 0.01f, 0);
            //spawnedObject.position = newBottom.transform.TransformPoint(new Vector3(0, 0.01f, 0));

            //Setup controls
            InteractableController ic = spawnedObject.gameObject.GetComponentInChildren<InteractableController>();
            controls = InteractableCreator.Instance.CreateTransformInteractable(InteriorControls.Instance.elevatorControls, spawnedObject, null, null, ic.gameObject.transform.localPosition, ic.gameObject.transform.localEulerAngles, null);
            ic.Setup(controls);

            //Set object ref
            controls.SetPolymorphicReference(this);

            //Get cables
            cable1 = spawnedObject.transform.Find("Cable1");
            cable2 = spawnedObject.transform.Find("Cable2");
            //Game.Log(cable1);

            UpdateCables();
        }
    }

    //Load an elevator position (Y)
    public void LoadElevatorSaveData(StateSaveData.ElevatorStateSave data)
    {
        if (data == null) return;

        if (spawnedObject != null) spawnedObject.transform.position = new Vector3(spawnedObject.transform.position.x, data.yPos, spawnedObject.transform.position.z);
        currentFloor = data.floor;
    }

    //Add a floor to this stairwell system
    public void AddFloor(NewTile newTile)
    {
        //Don't add if already featured
        if (elevatorFloors.ContainsKey((int)newTile.globalTileCoord.z))
        {
            return;
        }

        //Is this the top?
        if(top == null || newTile.globalTileCoord.z > top.globalTileCoord.z)
        {
            top = newTile;
        }

        //Is this the bottom?
        if(bottom == null || newTile.globalTileCoord.z < bottom.globalTileCoord.z)
        {
            bottom = newTile;
        }

        //New floor data
        if(preset.featuresElevator)
        {
            ElevatorFloor newFloor = new ElevatorFloor();
            newFloor.elevatorRoom = newTile.nodes[0].room;
            newFloor.floor = (int)newTile.globalTileCoord.z;
            newFloor.elevatorTile = newTile;

            //This is already spawned
            if (newTile.stairwell != null)
            {
                OnSpawnStairwell(newTile);
            }

            //Add reference
            elevatorFloors.Add((int)newTile.globalTileCoord.z, newFloor);

            UpdateCables();
        }
    }

    //When a stairwell prefab is spawned
    public void OnSpawnStairwell(NewTile tile)
    {
        //Find the floor
        ElevatorFloor foundFloor = null;

        if(!elevatorFloors.TryGetValue((int)tile.globalTileCoord.z, out foundFloor))
        {
            Game.LogError("Elevator at " + tile.building.name + " cannot find floor entry for floor " + (int)tile.globalTileCoord.z);
            return;
        }

        foundFloor.spawned = tile.stairwell;

        //Setup buttons
        //Find interactable controllers within children of this
        InteractableController[] buttons = foundFloor.spawned.GetComponentsInChildren<InteractableController>();

        //A is the up button, B is the down button
        foreach (InteractableController ic in buttons)
        {
            if (ic.id == InteractableController.InteractableID.A)
            {
                foundFloor.upButton = InteractableCreator.Instance.CreateTransformInteractable(InteriorControls.Instance.elevatorUpButton, ic.transform.parent, null, null, ic.transform.localPosition, ic.transform.localEulerAngles, null);

                //Set polymorphic reference to this
                foundFloor.upButton.SetPolymorphicReference(this);

                ic.Setup(foundFloor.upButton);
            }
            else if (ic.id == InteractableController.InteractableID.B)
            {
                foundFloor.downButton = InteractableCreator.Instance.CreateTransformInteractable(InteriorControls.Instance.elevatorDownButton, ic.transform.parent, null, null, ic.transform.localPosition, ic.transform.localEulerAngles, null);

                //Set polymorphic reference to this
                foundFloor.downButton.SetPolymorphicReference(this);

                ic.Setup(foundFloor.downButton);
            }
            else if(ic.id == InteractableController.InteractableID.C)
            {
                foundFloor.door = InteractableCreator.Instance.CreateTransformInteractable(InteriorControls.Instance.doorC, ic.transform.parent, null, null, ic.transform.localPosition, ic.transform.localEulerAngles, null);

                //Set polymorphic reference to this
                foundFloor.door.SetPolymorphicReference(this);

                ic.Setup(foundFloor.door);
            }
        }

        //Make sure the lift has the correct rotation
        spawnedObject.eulerAngles = tile.stairwell.transform.eulerAngles;
    }

    //Set the elevator destination; Use the bool to pass whether the up or down button was pressed
    public void CallElevator(int newFloor, bool upButton)
    {
        //Clamp to floor range
        newFloor = Mathf.Clamp(newFloor, (int)bottom.globalTileCoord.z, (int)top.globalTileCoord.z);

        //Does this call exist already?
        if(!calls.ContainsKey(newFloor))
        {
            calls.Add(newFloor, new List<ElevatorCall>());
            calls[newFloor].Add(new ElevatorCall(elevatorFloors[newFloor], upButton, SessionData.Instance.gameTime));
        }
        else if(!calls[newFloor].Exists(item => item.callUp == upButton))
        {
            calls[newFloor].Add(new ElevatorCall(elevatorFloors[newFloor], upButton, SessionData.Instance.gameTime));
        }

        Game.Log("Call elevator " + newFloor + " up: " + upButton);

        //Add to session data list
        if(!isActive)
        {
            isActive = true;
            SessionData.Instance.activeElevators.Add(this);
            ElevatorUpdate(); //Manually force update
        }

        //Update destinations
        //UpdateDestination();
    }

    //Called every frame when active
    public void ElevatorUpdate()
    {
        try
        {
            //New call from idle
            if(!inTransit && calls.Count > 0)
            {
                //We need a new travel direction, so pick the first registered
                float pickedTime = Mathf.Infinity;
                int pickedDest = currentFloor;

                foreach(KeyValuePair<int, List<ElevatorCall>> pair in calls)
                {
                    foreach(ElevatorCall c in pair.Value)
                    {
                        if(c.registered < pickedTime)
                        {
                            pickedTime = c.registered;
                            pickedDest = pair.Key;
                        }
                    }
                }

                //Short delay before movement
                if(!inTransit)
                {
                    liftTimer = preset.movementDelay;
                    SetInTransit(true);

                    //Close door
                    if(elevatorFloors != null)
                    {
                        if(elevatorFloors.ContainsKey(currentFloor))
                        {
                            if(elevatorFloors[currentFloor].door != null)
                            {
                                try
                                {
                                    elevatorFloors[currentFloor].door.OnInteraction(elevatorFloors[currentFloor].door.preset.GetActions().Find(item => item.interactionName == "Close"), null);
                                    //elevatorFloors[currentFloor].door.SetSwitchState(false, null);
                                }
                                catch
                                {
                                    Game.Log("Unable to close elevator door!");
                                }
                            }
                        }
                    }
                }

                //Trigger movement towards this
                isGoingUp = false;

                if (pickedDest > currentFloor)
                {
                    isGoingUp = true;
                }

                //Game.Log("Going Up? " + isGoingUp);

                //Now the direction is set, set the destination
                UpdateDestination();
            }
        }
        catch
        {
            Game.LogError("Elevator call from idle error!");
        }

        try
        {
            if(inTransit)
            {
                //Update aduio
                if(movementAudio != null)
                {
                    if(spawnedObject != null) movementAudio.UpdateWorldPosition(spawnedObject.transform.position, null);

                    //Pass speed param
                    movementAudio.audioEvent.setParameterByName("Speed", currentSpeed / preset.elevatorMaxSpeed);

                    //Pulse a 1 every time we go past a floor...
                    if(Player.Instance.currentNode != null)
                    {
                        float pulse = 0f;
                        float ceilingY = Player.Instance.currentNode.position.y + (Player.Instance.currentNode.floor.defaultCeilingHeight * 0.1f) - (Player.Instance.currentNode.floorHeight * 0.1f);
                        float floorY = Player.Instance.currentNode.position.y;
                        float middleY = Mathf.Lerp(floorY, ceilingY, 0.5f);
                        float camY = CameraController.Instance.cam.transform.position.y;

                        //Game.Log("Elevator Y: Floor pos: " + floorY + ", ceiling pos: " + ceilingY + ", middle pos: " + middleY);

                        if(camY < floorY || camY > ceilingY)
                        {
                            //Game.Log("Elevator Y: Camera is below floor or above ceiling");
                            pulse = 1f;
                        }
                        else
                        {
                            //Closer to floor than ceiling
                            if(camY <= middleY)
                            {
                                pulse = 1f - Mathf.InverseLerp(floorY, middleY, camY);
                                //Game.Log("Elevator Y: Camera is closer to floor: " + pulse);
                            }
                            //Closer the ceiling than floor
                            else
                            {
                                pulse = Mathf.InverseLerp(middleY, ceilingY, camY);
                                //Game.Log("Elevator Y: Camera is closer to ceiling: " + pulse);
                            }
                        }

                        //Pass pulse param
                        movementAudio.audioEvent.setParameterByName("FloorPulse", pulse);
                    }
                }

                float distance = Mathf.Abs(desiredY - spawnedObject.transform.position.y);

                if (liftTimer > 0f)
                {
                    //Game.Log("Lift timer: " + liftTimer);
                    liftTimer -= Time.deltaTime;
                    liftTimer = Mathf.Max(liftTimer, 0);
                }
                //Move towards next
                else if(distance > 0.005f)
                {
                    isMoving = true;

                    //Accelerate while this far away
                    if (distance >= preset.accelerateWhileThisFarAway)
                    {
                        //Acceleration
                        if (currentSpeed < preset.elevatorMaxSpeed)
                        {
                            currentSpeed += preset.elevatorAcceleration * Time.deltaTime;
                            currentSpeed = Mathf.Clamp(currentSpeed, 0, preset.elevatorMaxSpeed);
                            reachedSpeed = currentSpeed;
                        }
                    }
                    //Brake
                    else
                    {
                        //Speed as ratio of 1
                        currentSpeed = Mathf.Lerp(reachedSpeed, 0.15f, 1f - distance / preset.accelerateWhileThisFarAway);
                    }

                    //Game.Log("Distance: " + distance + " speed: " + currentSpeed);

                    if(spawnedObject != null) spawnedObject.transform.position = Vector3.MoveTowards(spawnedObject.transform.position, new Vector3(spawnedObject.transform.position.x, desiredY, spawnedObject.transform.position.z), currentSpeed * Time.deltaTime);

                    //Manually update player physics
                    if (Player.Instance.currentVehicle == spawnedObject.transform || Vector3.Distance(Player.Instance.transform.position, spawnedObject.transform.position) <= 4f)
                    {
                        //Sync transforms if this contains the player
                        SessionData.Instance.ExecuteSyncPhysics(SessionData.PhysicsSyncType.onPlayerMovement);
                        //Player.Instance.fps.UpdateMovement();
                    }

                    UpdateCables();

                    //Game.Log(spawnedObject.transform.position + " Moving towards " + desiredY);

                    //Update current floor
                    Vector3Int currentTilePos = CityData.Instance.RealPosToPathmapIncludingZ(spawnedObject.transform.position);

                    int cur = Mathf.FloorToInt(currentTilePos.z);

                    if (cur != currentFloor)
                    {
                        currentFloor = cur;

                        NewTile currentTile = PathFinder.Instance.tileMap[currentTilePos];

                        //Parent to node
                        foreach (NewNode node in currentTile.nodes)
                        {
                            if (node.room != null)
                            {
                                if(spawnedObject != null) spawnedObject.SetParent(node.room.transform, true);
                                break;
                            }
                        }

                        //Game.Log("Floor changed: " + currentFloor);
                    }
                }
                //Arrived at destination
                else
                {
                    //Game.Log("Arrived at current destination: " + currentDestination);
                    currentSpeed = 0f;
                    reachedSpeed = 0f;

                    //Remove from calls: Do this once
                    if (calls.ContainsKey(currentFloor))
                    {
                        calls.Remove(currentFloor);

                        //Play ding
                        if(isMoving)
                        {
                            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.elevatorDing, Player.Instance, elevatorFloors[currentFloor].door.node, elevatorFloors[currentFloor].door.controller.transform.position);
                            isMoving = false;
                        }

                        //Open door
                        if (elevatorFloors != null)
                        {
                            if (elevatorFloors.ContainsKey(currentFloor))
                            {
                                if (elevatorFloors[currentFloor].door != null)
                                {
                                    try
                                    {
                                        elevatorFloors[currentFloor].door.OnInteraction(elevatorFloors[currentFloor].door.preset.GetActions().Find(item => item.interactionName == "Open"), null);
                                        //elevatorFloors[currentFloor].door.SetSwitchState(false, null);
                                    }
                                    catch
                                    {
                                        Game.Log("Unable to open elevator door!");
                                    }
                                }
                            }
                        }

                        if (calls.Count > 0)
                        {
                            liftTimer = preset.liftDelay; //Create a delay before moving to the next floor
                        }
                    }
                    //Else cancel transit: This will trigger a direction update
                    else
                    {
                        SetInTransit(false);
                    }
                }
            }
        }
        catch(System.Exception e)
        {
            Game.LogError("Elevator inTransit error: " + e.ToString());
        }

        //Is there nowhere to go?
        if(calls != null && calls.Count <= 0)
        {
            EndMovement();
        }
    }

    //Update the cable length (call this when the elevator moves)
    private void UpdateCables()
    {
        //Calculate distance between top and the elevator
        if(top != null && cable1 != null)
        {
            float cableDist = (top.position.y + PathFinder.Instance.tileSize.z) - cable1.localPosition.y;

            cable1.localScale = new Vector3(1, cableDist, 1);
            cable2.localScale = new Vector3(1, cableDist, 1);
        }
    }

    //End the lift movement
    private void EndMovement()
    {
        //Game.Log("End movement");
        isActive = false;
        SetInTransit(false);
        currentSpeed = 0f;
        reachedSpeed = 0f;
        liftTimer = 0f;
        SessionData.Instance.activeElevators.Remove(this);
    }

    public void SetInTransit(bool val)
    {
        if(val != inTransit)
        {
            Game.Log("Set elevator in transit: " + val);
            inTransit = val;

            if (controls != null) controls.SetSwitchState(inTransit, null);
            else Game.Log("Elevator controls is null!");

            if(inTransit)
            {
                if(movementAudio != null)
                {
                    AudioController.Instance.StopSound(movementAudio, AudioController.StopType.fade, "Elevator stopped");
                    Game.Log("Stop elevator sound");
                }

                movementAudio = AudioController.Instance.PlayWorldLooping(AudioControls.Instance.elevatorMovement, null, null, spawnedObject.transform.position, null, pauseWhenGamePaused: true);
                Game.Log("Play elevator sound loop");
            }
            else
            {
                //Key off
                if (movementAudio != null)
                {
                    //Pass speed param
                    movementAudio.audioEvent.setParameterByName("Speed", 0f);

                    AudioController.Instance.StopSound(movementAudio, AudioController.StopType.triggerCue, "Elevator stop");

                    //Game.Log("Keying off elevator sound");
                    //movementAudio.audioEvent.triggerCue();
                    //movementAudio.isActive = false; //Set this flag to false so it will disappear on next check
                }
            }
        }

    }

    public void UpdateDestination()
    {
        if(calls.Count <= 0)
        {
            EndMovement();
            return;
        }

        if(isGoingUp)
        {
            //Scan for all floors in the call list that
            int highest = currentFloor;
            int next = 99999;

            foreach (KeyValuePair<int, List<ElevatorCall>> pair in calls)
            {
                //The ultimate destination will be the highest call
                if(pair.Key > highest)
                {
                    highest = pair.Key;
                }

                //The next destination is the closest one with a call button that's going up
                if(pair.Key > currentFloor)
                {
                    if(pair.Key < next)
                    {
                        next = pair.Key;
                    }
                }
            }

            ultimateDesitnation = highest;
            currentDestination = Mathf.Min(next, highest);
        }
        else
        {
            //Scan for all floors in the call list that
            int lowest = currentFloor;
            int next = -99999;

            foreach (KeyValuePair<int, List<ElevatorCall>> pair in calls)
            {
                //The ultimate destination will be the highest call
                if (pair.Key < lowest)
                {
                    lowest = pair.Key;
                }

                //The next destination is the closest one with a call button that's going up
                if (pair.Key < currentFloor)
                {
                    if (pair.Key > next)
                    {
                        next = pair.Key;
                    }
                }
            }
            
            ultimateDesitnation = lowest;
            currentDestination = Mathf.Max(next, lowest);
        }

        Game.Log("New current destination " + currentDestination);

        //Set the desired Y position
        desiredY = elevatorFloors[currentDestination].elevatorTile.position.y + 0.01f;
        //Game.Log("Set desired Y " + desiredY);

        //Game.Log("Elevator destination updated. Next: " + currentDestination + " ultimate: " + ultimateDesitnation + " current: " + currentFloor + " desiredY: " + desiredY);
    }
}
