﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;
using TMPro;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.EventSystems;
using NaughtyAttributes;
using UnityEditor;

public class MainMenuController : MonoBehaviour
{
    [Header("Background")]
    public RectTransform mainMenuContainer;
    public Image backgroundImage;
    public Image logoImage;
    public TextMeshProUGUI buildText;
    public GameObject buildNameObject;
    public float time = 0f;

    [Header("Components")]
    public Component previousComponent = Component.none;
    public MenuComponent currentComponent = null;
    public float componentMotion = 0f;
    public AeLa.EasyFeedback.FeedbackForm feedbackForm;
    public AeLa.EasyFeedback.FormField feedbackPlayerInfo;
    public bool saveDof = false;
    public TextMeshProUGUI betaMessageText;
    public GraphicRaycaster raycaster;
    public bool askedStreamerQuestion = false;

    [ReorderableList]
    public List<MenuComponent> components = new List<MenuComponent>();

    [System.Serializable]
    public class MenuComponent
    {
        public Component component;
        public RectTransform rect;
        public int xPhase = 0;
        public Vector2 onscreenAnchoredPosition;
        public List<ButtonController> buttons = new List<ButtonController>();
        public ButtonController previouslySelected;
        public bool skipMotion = false;
    }

    //private Component previouslyCompletedComponent = Component.none;

    public enum Component { none, mainMenuButtons, settings, newGameSelect, city, citySelect, generateCity, charSetup, interfaceSettings, graphicsSettings, audioSettings, gameplaySettings, controlSettings, devSettings, saveGame, loadGame, credits, loadingCity, splash, controlDetect, streamingSettings, bugReport };

    [Header("Tips")]
    public string loadingTipsDDSTree;
    [ReorderableList]
    public List<LoadingTip> loadingTips = new List<LoadingTip>();
    public float nextTipTimer = 0f;

    [System.Serializable]
    public class LoadingTip
    {
        public string dictRef;
        public Sprite image;
    }

    [Header("Dropdowns")]
    public DropdownController languageDropdown;
    public DropdownController resolutionsDropdown;
    public DropdownController fullScreenModeDropdown;
    public DropdownController startTimeDropdown;
    public DropdownController gameDifficultyDropdown;
    public DropdownController gameDifficultyDropdown2;
    public DropdownController gameLengthDropdown;
    public DropdownController selectCityDropdown;
    public DropdownController playerGenderDropdown;
    public DropdownController partnerGenderDropdown;
    public DropdownController citySizeDropdown;
    public DropdownController cityPopDropdown;
    public DropdownController statusEffectsDropdown;
    public DropdownController aaModeDropdown;
    public DropdownController aaQualityDropdown;
    public List<ToggleController> statusEffectToggles = new List<ToggleController>();

    [Header("Main Menu")]
    public ButtonController saveGameButton;
    public ButtonController loadGameButton;
    public ButtonController sandboxGameButton;
    public ButtonController cityGenButton;
    public ButtonController resumeGameButton;
    public ButtonController helpButton;
    public ButtonController bugReportButton;
    public ButtonController feedbackButton;

    [Header("City Setup Menu")]
    public TextMeshProUGUI selectedCityShareCode;
    public TextMeshProUGUI selectedCityDetailsText;
    public ButtonController selectedCityContinueButton;

    [System.NonSerialized]
    public CityInfoData selectedCityInfoData;
    public ButtonController selectedCityCopyShareCodeButton;
    public ButtonController deleteCityButton;

    private List<FileInfo> cityMapFiles = new List<FileInfo>(); //List of paths to the map files
    private List<FileInfo> cityInfoFiles = new List<FileInfo>(); //List of paths to the info files
    private Dictionary<string, CityInfoData> cityInfoDict = new Dictionary<string, CityInfoData>();

    [Header("Dev Controls")]
    public ButtonController developerOptionsButton;
    public Slider windSlider;
    public Slider rainSlider;
    public Slider lightningSlider;
    public Slider snowSlider;
    public Button setWeatherButton;
    public ToggleController allowLicensedMusicToggle;

    [Header("New Character")]
    public ButtonController playerNameButton;
    public MultiSelectController playerSkinToneSelect;

    [Header("City Generation")]
    public TextMeshProUGUI shareCodeText;
    public ButtonController pasteShareCodeButton;
    public ButtonController changeCityNameButton;
    public TextMeshProUGUI generationWarningText;

    [Header("Credits")]
    public TextMeshProUGUI creditsText;
    public RectTransform creditsPageContent;

    [Header("Main Menu")]
    public TextMeshProUGUI mouseOverText;
    public bool mainMenuActive = false;

    [Header("Language")]
    public string loadedLanguage;

    [Header("Loading Bar")]
    public TextMeshProUGUI loadingText;
    public Slider loadingSlider;
    public TextMeshProUGUI tipText;
    public Image tipImg;

    [Header("Menu Fading")]
    public CanvasRenderer fadeOverlay;
    public float desiredFade = 0f;
    public float fade = 1f;
    private bool exitMainMenuAfterFade = false;

    [Header("Save/Load Game")]
    public RectTransform loadGameContentRect;
    public RectTransform saveGameContentRect;
    public GameObject saveGameEntryPrefab;
    private List<SaveGameEntryController> spawnedSaveGames = new List<SaveGameEntryController>();
    private List<SaveGameEntryController> spawnedLoadGames = new List<SaveGameEntryController>();
    public SaveGameEntryController selectedSave;
    public ButtonController saveButton;
    public ButtonController loadButton;
    public ButtonController deleteButton;
    public ButtonController deleteButton2;
    public SaveGameEntryController newSaveGameEntry;

    [Header("Bug Report (New)")]
    public DropdownController bugSaveDropdown;
    public DropdownController priorityDropdown;
    public DropdownController categoryDropdown;
    public ButtonController bugNameInput;
    public ButtonController bugDetailsInput;
    public ToggleController sendScreenshotToggle;
    public ToggleController sendSystemSpecsToggle;
    public ToggleController sendPrevLogToggle;
    public float bugReportTimer = 0f; //So people can't spam bug reports

    [Space(7)]
    public TMP_Dropdown ffPriority;
    public TMP_Dropdown ffCategory;
    public TMP_InputField ffNameInput;
    public TMP_InputField ffDescriptionInput;
    public AeLa.EasyFeedback.FormField ffSystemInfo;
    public AeLa.EasyFeedback.FormElement ffPrevLogCollector;

    [Header("Animation")]
    public CanvasRenderer topBarRend;
    public CanvasRenderer bottomBarRend;
    public TextMeshProUGUI titleText;
    public AnimationCurve titleTextKerningAnimation;

    //Singleton pattern
    private static MainMenuController _instance;
    public static MainMenuController Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        //Load build text
        if (buildText != null)
        {
            if(Game.Instance.displayBuildInMenu)
            {
                buildText.text = "v" + Game.Instance.buildID;
            }
            else
            {
                buildText.text = string.Empty;
            }
        }

        //Copy build desc to info collector
        if(feedbackPlayerInfo != null)
        {
            feedbackPlayerInfo.version = Game.Instance.buildDescription;
        }

        //BETA text
        if(Game.Instance.displayBetaMessage && betaMessageText != null)
        {
            betaMessageText.gameObject.SetActive(true);
        }
        else
        {
            Destroy(betaMessageText.gameObject);
        }

        //Check directories exists
        if (!Directory.Exists(Application.persistentDataPath +  "/Save"))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(Application.persistentDataPath +  "/Save");
        }

        if (!Directory.Exists(Application.persistentDataPath +  "/Cities"))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(Application.persistentDataPath +  "/Cities");
        }

        //Reset loading bar
        loadingSlider.value = 0f;
        tipText.text = string.Empty;
        tipImg.color = Color.clear;

        //LoadTip();

        //Set mouse over text to nothing
        mouseOverText.text = string.Empty;

        //Get onscreen positions
        foreach (MenuComponent c in components)
        {
            if (c.rect != null)
            {
                c.onscreenAnchoredPosition = c.rect.anchoredPosition;

                if (c.xPhase > currentComponent.xPhase)
                {
                    c.rect.anchoredPosition = new Vector2(PrefabControls.Instance.menuCanvas.sizeDelta.x, c.rect.anchoredPosition.y);
                }
                else if (c.xPhase < currentComponent.xPhase)
                {
                    c.rect.anchoredPosition = new Vector2(-PrefabControls.Instance.menuCanvas.sizeDelta.x, c.rect.anchoredPosition.y);
                }

                //Get buttons
                ButtonController[] buttons = c.rect.gameObject.GetComponentsInChildren<ButtonController>();
                c.buttons = buttons.ToList();

                foreach (ButtonController bc in c.buttons)
                {
                    if (bc.button != null) bc.button.interactable = false; //Silently set this to non-interactable
                }
            }
        }

        //Populate dropdown boxes
        LoadDropdownContent();

        //Load some prefs after initialisation phase as marked in the class list
        PlayerPrefsController.Instance.LoadPlayerPrefs(true);

        if (SessionData.Instance.isDialogEdit) return;
        if (SessionData.Instance.isFloorEdit) return; //No menu if in floor editor
        if (SessionData.Instance.isTestScene) return; //No menu if in test scene

        if (bugReportButton != null)
        {
            if(!Game.Instance.enableBugReporting)
            {
                Destroy(bugReportButton.gameObject);
            }
        }

        if (feedbackButton != null)
        {
            if (!Game.Instance.enableFeedbackFormLink)
            {
                Destroy(feedbackButton.gameObject);
            }
        }

        //Search for restart commands...
        if (RestartSafeController.Instance.loadFromDirty)
        {
            RestartSafeController.Instance.loadFromDirty = false;

            if (RestartSafeController.Instance.newGameLoadCity)
            {
                EnableMainMenu(true, true); //Menu @ start
                SetMenuComponent(Component.loadingCity);
            }
            else if (RestartSafeController.Instance.loadSaveGame)
            {
                EnableMainMenu(true, true); //Menu @ start
                Game.Log("CityGen: Attempting to load save game after restart...");
                SetMenuComponent(Component.loadingCity);
            }
            else if (RestartSafeController.Instance.generateNew)
            {
                EnableMainMenu(true, true); //Menu @ start
                Game.Log("CityGen: Attempting to generate new city after restart...");
                SetMenuComponent(Component.loadingCity);
            }
        }
        else
        {
            Component startComp = Component.mainMenuButtons;
            EnableMainMenu(true, false, menuPhase: startComp); //Menu @ start

            //Time limit popup
            //if(Game.Instance.timeLimited)
            //{
            //    PopupMessageController.Instance.PopupMessage("Demo Welcome", true, LButton: "Continue");
            //}

            //Set default options
            citySizeDropdown.SelectFromStaticOption("medium");
            cityPopDropdown.SelectFromStaticOption("maximum");

            OnGenerateNewSeed();
            RandomCityName();
        }

        //Add delegate to set weather
        if (setWeatherButton != null) setWeatherButton.onClick.AddListener(delegate { SessionData.Instance.SetWeather(rainSlider.normalizedValue, windSlider.normalizedValue, snowSlider.normalizedValue, lightningSlider.normalizedValue); });

        //Select skin tone
        playerSkinToneSelect.OnSelect += OnSkinToneChange;

        //Enable/disable developer mode
        string[] args = System.Environment.GetCommandLineArgs();

        if(args != null)
        {
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i].Trim().ToLower() == "-uncleeden")
                {
                    Game.Instance.devMode = true;
                }

                if (args[i].Trim().ToLower() == "-resetprefs")
                {
                    PlayerPrefs.DeleteAll();
                }
            }
        }

        //Set loaded language
        loadedLanguage = PlayerPrefsController.Instance.GetSettingStr("language");

        developerOptionsButton.SetInteractable(Game.Instance.devMode);
        developerOptionsButton.gameObject.SetActive(Game.Instance.devMode);

        generationWarningText.text = Strings.Get("ui.interface", "Depending on size, new cities may take a long time to generate...");
    }

    public void LoadDropdownContent()
    {
        //Language dropdown
        languageDropdown.dropdown.ClearOptions();

        string languageDir = Application.streamingAssetsPath + "/Strings/";

        try
        {
            List<string> newOptions = new List<string>();

            for (int i = 0; i < Strings.Instance.fileInputConfig.Count; i++)
            {
                Strings.LocInput lang = Strings.Instance.fileInputConfig[i];
                newOptions.Add(lang.languageCode);
            }

            languageDropdown.AddOptions(newOptions, false); //Don't use dictionary for languages as this could cause complications
            languageDropdown.SelectFromStaticOption(PlayerPrefsController.Instance.GetSettingStr("language"));
        }
        catch
        {

        }

        //Resolutions dropdowns
        //Load screen resolutions...
        Resolution[] resolution = Screen.resolutions;

        if (resolutionsDropdown != null)
        {
            List<string> resOptions = new List<string>();

            foreach (Resolution r in resolution)
            {
                resOptions.Add(r.ToString());
            }

            //Add current resolution to options too...  "width x height @ refreshRateHz"
            string currentRes = Screen.width + " x " + Screen.height + " @ " + Screen.currentResolution.refreshRate + "Hz";

            if (!resOptions.Contains(currentRes))
            {
                resOptions.Add(currentRes);
            }

            resolutionsDropdown.AddOptions(resOptions, false);
            resolutionsDropdown.dropdown.SetValueWithoutNotify(resOptions.IndexOf(currentRes));
        }

        if (fullScreenModeDropdown != null)
        {
            //Load full screen mode
            List<FullScreenMode> allModes = FullScreenMode.GetValues(typeof(FullScreenMode)).Cast<FullScreenMode>().ToList();
            List<string> fsOptions = new List<string>();

            for (int i = 0; i < allModes.Count; i++)
            {
                fsOptions.Add(allModes[i].ToString());
            }

            fullScreenModeDropdown.AddOptions(fsOptions, true);
            fullScreenModeDropdown.SelectFromStaticOption(Screen.fullScreenMode.ToString());
        }

        //AA
        if(aaModeDropdown != null)
        {
            List<UnityEngine.Rendering.HighDefinition.HDAdditionalCameraData.AntialiasingMode> allModes = UnityEngine.Rendering.HighDefinition.HDAdditionalCameraData.AntialiasingMode.GetValues(typeof(UnityEngine.Rendering.HighDefinition.HDAdditionalCameraData.AntialiasingMode)).Cast<UnityEngine.Rendering.HighDefinition.HDAdditionalCameraData.AntialiasingMode>().ToList();
            List<string> fsOptions = new List<string>();

            for (int i = 0; i < allModes.Count; i++)
            {
                fsOptions.Add(allModes[i].ToString());
            }

            aaModeDropdown.AddOptions(fsOptions, true);
            aaModeDropdown.SelectFromStaticOption(PlayerPrefsController.Instance.GetSettingStr("aaMode"));
        }

        if(aaQualityDropdown != null)
        {
            List<UnityEngine.Rendering.HighDefinition.HDAdditionalCameraData.SMAAQualityLevel> allModes = UnityEngine.Rendering.HighDefinition.HDAdditionalCameraData.AntialiasingMode.GetValues(typeof(UnityEngine.Rendering.HighDefinition.HDAdditionalCameraData.SMAAQualityLevel)).Cast<UnityEngine.Rendering.HighDefinition.HDAdditionalCameraData.SMAAQualityLevel>().ToList();
            List<string> fsOptions = new List<string>();

            for (int i = 0; i < allModes.Count; i++)
            {
                fsOptions.Add(allModes[i].ToString());
            }

            aaQualityDropdown.AddOptions(fsOptions, true);
            aaQualityDropdown.SelectFromStaticOption(PlayerPrefsController.Instance.GetSettingStr("aaQuality"));
        }

        //Game difficulty
        if (gameDifficultyDropdown != null)
        {
            List<string> difficulties = new List<string>();
            difficulties.Add("Easy");
            difficulties.Add("Normal");
            difficulties.Add("Hard");
            difficulties.Add("Extreme");

            gameDifficultyDropdown.AddOptions(difficulties, true);
            gameDifficultyDropdown.SelectFromStaticOption(PlayerPrefsController.Instance.GetSettingStr("gameDifficulty"));

            if(gameDifficultyDropdown2 != null)
            {
                gameDifficultyDropdown2.AddOptions(difficulties, true);
                gameDifficultyDropdown2.SelectFromStaticOption(PlayerPrefsController.Instance.GetSettingStr("gameDifficulty"));
            }
        }

        //Game difficulty
        if (gameLengthDropdown != null)
        {
            List<string> length = new List<string>();
            length.Add("Very Short");
            length.Add("Short");
            length.Add("Normal");
            length.Add("Long");
            length.Add("Very Long");

            gameLengthDropdown.AddOptions(length, true);
            gameLengthDropdown.SelectFromStaticOption(PlayerPrefsController.Instance.GetSettingStr("gameLength"));
        }

        //Start time
        if (startTimeDropdown != null)
        {
            List<string> startTimes = new List<string>();
            startTimes.Add("Morning");
            startTimes.Add("Midday");
            startTimes.Add("Evening");
            startTimes.Add("Midnight");

            startTimeDropdown.AddOptions(startTimes, true);
            startTimeDropdown.SelectFromStaticOption(PlayerPrefsController.Instance.GetSettingStr("startTime"));
        }

        //Gender select
        if (playerGenderDropdown != null && partnerGenderDropdown != null)
        {
            List<string> genders = new List<string>();
            genders.Add("Male");
            genders.Add("Female");
            genders.Add("Non-Binary");

            playerGenderDropdown.AddOptions(genders, true);
            playerGenderDropdown.SelectFromStaticOption(PlayerPrefsController.Instance.GetSettingStr("startingGender"));
            OnPlayerGenderChange();

            partnerGenderDropdown.AddOptions(genders, true);
            partnerGenderDropdown.SelectFromStaticOption(PlayerPrefsController.Instance.GetSettingStr("startingPartnerGender"));
            OnPartnerGenderChange();
        }

        //Size select
        if (citySizeDropdown != null)
        {
            List<string> citySize = new List<string>();

            citySize.Add("small");

            if(!Game.Instance.smallCitiesOnly)
            {
                citySize.Add("medium");
                citySize.Add("large");
                citySize.Add("veryLarge");
            }

            citySizeDropdown.AddOptions(citySize, true);
            citySizeDropdown.SelectFromStaticOption(PlayerPrefsController.Instance.GetSettingStr("cityGenSize"));
        }

        //Population select
        if (cityPopDropdown != null)
        {
            List<string> cityPop = new List<string>();

            cityPop.Add("ultraLow");
            cityPop.Add("veryLow");
            cityPop.Add("low");
            cityPop.Add("medium");
            cityPop.Add("high");
            cityPop.Add("veryHigh");
            cityPop.Add("maximum");

            cityPopDropdown.AddOptions(cityPop, true);
            cityPopDropdown.SelectFromStaticOption(PlayerPrefsController.Instance.GetSettingStr("cityGenPop"));
        }

        //Status effects
        if (statusEffectsDropdown != null)
        {
            List<string> statusFX = new List<string>();

            statusFX.Add("All");
            statusFX.Add("Only Positive");
            statusFX.Add("Only Negative");
            statusFX.Add("None");
            statusFX.Add("Custom");

            statusEffectsDropdown.AddOptions(statusFX, true);
            SetDropdownAccordingToStatusEffects();
        }

        //Bug report category
        if (categoryDropdown != null)
        {
            List<string> bugCategories = new List<string>();

            bugCategories.Add("General Bugs");
            bugCategories.Add("AI Bugs");
            bugCategories.Add("Geometry Bugs");
            bugCategories.Add("Feedback");

            categoryDropdown.AddOptions(bugCategories, true);
            categoryDropdown.SelectFromStaticOption(PlayerPrefsController.Instance.GetSettingStr("bugCategory"));
        }

        //Bug report priority
        if (priorityDropdown != null)
        {
            List<string> bugPriority = new List<string>();

            bugPriority.Add("Low");
            bugPriority.Add("Medium");
            bugPriority.Add("High");

            priorityDropdown.AddOptions(bugPriority, true);
            priorityDropdown.SelectFromStaticOption(PlayerPrefsController.Instance.GetSettingStr("bugPriority"));
        }
    }

    public void OnNewMouseOver()
    {
        if (InterfaceController.Instance.selectedElement != null)
        {
            if (InterfaceController.Instance.selectedElement.menuMouseoverReference.Length > 0)
            {
                mouseOverText.text = Strings.Get("ui.interface", InterfaceController.Instance.selectedElement.menuMouseoverReference);
            }
            else
            {
                mouseOverText.text = string.Empty;
            }
        }
        else
        {
            //Set mouse over text to nothing
            mouseOverText.text = string.Empty;
        }
    }

    public void EnableMainMenu(bool val, bool useFade = false, bool exitMain = false, Component menuPhase = Component.mainMenuButtons)
    {
        //Reset fade overlay
        if (!mainMenuActive && val && useFade)
        {
            fadeOverlay.gameObject.SetActive(true); //Start with menu covered
            fadeOverlay.SetAlpha(1); //Start with menu covered
            fade = 1; //Start with menu covered
        }

        SelectNewSave(null);

        mainMenuActive = val;
        Game.Log("Menu: Main menu active: " + mainMenuActive);

        if (mainMenuActive)
        {
            mainMenuContainer.gameObject.SetActive(true); //Make sure main container is active
            exitMainMenuAfterFade = false; //Cancel exit main when faded

            time = 0f;

            //Reset loading bar
            loadingSlider.value = 0f;

            //Disable hud while loading
            InterfaceController.Instance.SetInterfaceActive(false);
            PrefabControls.Instance.menuCanvas.gameObject.SetActive(true);

            MusicController.Instance.SetGameState(MusicCue.MusicTriggerGameState.menu);

            SetMenuComponent(menuPhase);
        }
        else
        {
            exitMainMenuAfterFade = exitMain;

            //Note: Below is now handled by automatic canvas scaling
            //Set the 3D appropriately to the resolution (you designed models with 1440p in mind for the correct 'size')
            //float viewmodelScaleX = (CameraController.Instance.cam.pixelWidth / 2560f);
            //float viewmodelScaleY = (CameraController.Instance.cam.pixelHeight / 1440f);
            //float viewmodelScale = Mathf.Max(viewmodelScaleX, viewmodelScaleY);

            //Vector3 newScale = new Vector3(viewmodelScale, viewmodelScale, viewmodelScale);
            //InterfaceControls.Instance.firstPersonItemsParent.localScale = newScale;
            //MapController.Instance.directionalArrowContainer.transform.localScale = newScale;
            //InterfaceController.Instance.compassContainer.transform.localScale = newScale;

            //Game.Log("Interface: 3D UI Scale: " + viewmodelScale);

            if (!useFade)
            {
                InterfaceController.Instance.SetInterfaceActive(true);
                PrefabControls.Instance.menuCanvas.gameObject.SetActive(false);
            }

            MusicController.Instance.SetGameState(MusicCue.MusicTriggerGameState.inGame);
            Player.Instance.UpdatePlayerAmbientState(); //Update the player ambient state

            SetMenuComponent(Component.none);
        }

        if (mainMenuActive)
        {
            desiredFade = 0f;
        }
        else
        {
            desiredFade = 1f;
        }

        if (useFade)
        {
            StopCoroutine("FadeMenu");
            StartCoroutine("FadeMenu");
        }
        else
        {
            fade = desiredFade;
        }

        //if (CameraController.Instance.cam.enabled == mainMenuActive)
        //{
        //    CameraController.Instance.cam.enabled = !mainMenuActive;
        //}
    }

    //Animate menu components
    private void Update()
    {
        if (mainMenuActive)
        {
            time += Time.deltaTime;

            topBarRend.SetAlpha(Mathf.Clamp01(time * 0.1f));
            bottomBarRend.SetAlpha(Mathf.Clamp01(time * 0.1f));

            titleText.characterSpacing = titleTextKerningAnimation.Evaluate(time);
        }

        //Count down the bug report timer
        if(bugReportTimer > 0f)
        {
            bugReportTimer -= Time.deltaTime;
        }
    }

    IEnumerator FadeMenu()
    {
        desiredFade = Mathf.Clamp01(desiredFade);

        Game.Log("Menu: Execute menu fade: " + fade + "/" + desiredFade);

        if (!mainMenuActive && !CameraController.Instance.cam.enabled)
        {
            CameraController.Instance.cam.enabled = true;
        }

        while (desiredFade != fade)
        {
            if (PopupMessageController.Instance != null && PopupMessageController.Instance.active)
            {
                yield return null; //Wait for popup to disappear
            }
            else if (fade < desiredFade)
            {
                fade += 2f * Time.deltaTime;
            }
            else if (fade > desiredFade)
            {
                fade -= 2f * Time.deltaTime;
            }

            fade = Mathf.Clamp01(fade);
            fadeOverlay.SetAlpha(fade);

            if (exitMainMenuAfterFade)
            {
                if (fade >= 1)
                {
                    InterfaceController.Instance.SetInterfaceActive(true);
                    mainMenuContainer.gameObject.SetActive(false);
                    desiredFade = 0f;
                }
                else if (fade <= 0 && desiredFade <= 0)
                {
                    //Disable menu canvas
                    PrefabControls.Instance.menuCanvas.gameObject.SetActive(false);
                }
            }

            yield return null;
        }

        if (mainMenuActive && CameraController.Instance.cam.enabled)
        {
            CameraController.Instance.cam.enabled = false;
        }
    }

    public void SetMenuComponent(int newComponent)
    {
        SetMenuComponent((Component)newComponent);
    }

    public void SetMenuComponent(Component newComponent)
    {
        if (newComponent == Component.generateCity && Game.Instance.disableCityGeneration) return;
        if (newComponent == Component.saveGame && Game.Instance.disableSaveLoadGames) return;
        if (newComponent == Component.loadGame && Game.Instance.disableSaveLoadGames) return;

        //If this is selected, find another...
        if (InterfaceController.Instance.selectedElement != null)
        {
            Game.Log("Menu: Deselect button " + InterfaceController.Instance.selectedElement.name + " through selection");
            InterfaceController.Instance.selectedElement.OnDeselect();
        }

        Game.Log("Menu: Set menu component: " + newComponent);

        //Only set previous if it is different
        if (newComponent != currentComponent.component)
        {
            if (currentComponent != null)
            {
                //Game.Log("Menu: Setting previous component: " + currentComponent.component);
                previousComponent = currentComponent.component;
            }

            //Menu credits
            if(newComponent == Component.credits)
            {
                creditsText.text = CreditsController.Instance.GetFormattedCreditsText();
                creditsPageContent.sizeDelta = new Vector2(creditsPageContent.sizeDelta.x, creditsText.GetPreferredValues().y + 32f);
            }
            else if(newComponent == Component.mainMenuButtons && !PlayerPrefsController.Instance.playedBefore)
            {
                if(!askedStreamerQuestion)
                {
                    PopupMessageController.Instance.PopupMessage("Streamer Mode", true, true, LButton: "Keep Enabled", RButton: "Disable");
                    PopupMessageController.Instance.OnLeftButton += CancelStreamerMode;
                    PopupMessageController.Instance.OnRightButton += SetToStreamerMode;
                    askedStreamerQuestion = true;
                }
            }
            else if(newComponent == Component.bugReport)
            {
                RefreshSaveGameDropdown();
                ResetBugReportDetails();
                OnOpenBugReport();
            }

            if(previousComponent == Component.bugReport && newComponent != Component.bugReport)
            {
                OnCloseBugReport();
            }
        }

        currentComponent = components.Find(item => item.component == newComponent);
        componentMotion = 0f;
        StartCoroutine(MenuMotion(currentComponent.skipMotion));
    }

    public void SetToStreamerMode()
    {
        Game.Log("Menu: Setting to streamer safe mode");
        PopupMessageController.Instance.OnLeftButton -= CancelStreamerMode;
        PopupMessageController.Instance.OnRightButton -= SetToStreamerMode;

        PlayerPrefs.SetInt("licensedMusic", 0); //Save to player prefs

        PlayerPrefsController.GameSetting findSetting = PlayerPrefsController.Instance.gameSettingControls.Find(item => item.identifier == "licensedMusic"); //Case insensitive find
        findSetting.intValue = 0;
        allowLicensedMusicToggle.SetOff();

        PlayerPrefsController.Instance.OnToggleChanged("licensedmusic", false);
    }

    public void CancelStreamerMode()
    {
        PopupMessageController.Instance.OnLeftButton -= CancelStreamerMode;
        PopupMessageController.Instance.OnRightButton -= SetToStreamerMode;
    }

    IEnumerator MenuMotion(bool skipMotion)
    {
        while (componentMotion < 1f)
        {
            componentMotion += Time.deltaTime / 0.5f;
            if (skipMotion) componentMotion = 1;
            componentMotion = Mathf.Clamp01(componentMotion);

            foreach (MenuComponent c in components)
            {
                //Disable all buttons in movement
                foreach (ButtonController bc in c.buttons)
                {
                    if (bc.button != null) bc.button.interactable = false;
                }

                //This is the current component;
                if (c == currentComponent && c.rect != null)
                {
                    c.rect.gameObject.SetActive(true); //Set current component active
                    c.rect.anchoredPosition = Vector2.Lerp(c.rect.anchoredPosition, c.onscreenAnchoredPosition, componentMotion);
                }
                else if (c.rect != null)
                {
                    if (c.xPhase > currentComponent.xPhase)
                    {
                        c.rect.anchoredPosition = Vector2.Lerp(c.rect.anchoredPosition, new Vector2(PrefabControls.Instance.menuCanvas.sizeDelta.x, c.rect.anchoredPosition.y), componentMotion);
                    }
                    else if (c.xPhase < currentComponent.xPhase)
                    {
                        c.rect.anchoredPosition = Vector2.Lerp(c.rect.anchoredPosition, new Vector2(-PrefabControls.Instance.menuCanvas.sizeDelta.x, c.rect.anchoredPosition.y), componentMotion);
                    }
                }
            }

            yield return null;
        }

        //Disable other components
        foreach (MenuComponent c in components)
        {
            //This is not the current component;
            if (c != currentComponent && c.rect != null)
            {
                c.rect.gameObject.SetActive(false);
            }
            //If this is the current component, activate buttons
            else
            {
                foreach (ButtonController bc in c.buttons)
                {
                    if (bc.interactable)
                    {
                        if (bc.button != null) bc.button.interactable = true;
                    }
                }

                //Disable/enable main menu buttons
                if (currentComponent.component == Component.mainMenuButtons)
                {
                    saveGameButton.SetInteractable(IsSaveGameAllowed());

                    loadGameButton.SetInteractable(!Game.Instance.disableSaveLoadGames);

                    resumeGameButton.SetInteractable(SessionData.Instance.startedGame);
                    helpButton.SetInteractable(SessionData.Instance.startedGame);
                }
                else if(currentComponent.component == Component.citySelect)
                {
                    cityGenButton.SetInteractable(!Game.Instance.disableCityGeneration);
                }
                else if(currentComponent.component == Component.newGameSelect)
                {
                    sandboxGameButton.SetInteractable(!Game.Instance.disableSandbox);
                }
            }
        }

        OnMenuComponentSwitchComplete();
    }

    public bool IsSaveGameAllowed()
    {
        if (Game.Instance.disableSaveLoadGames || Player.Instance.transitionActive || Player.Instance.answeringPhone != null || !SessionData.Instance.startedGame || CutSceneController.Instance.cutSceneActive)
        {
            return false;
        }

        return true;
    }

    //Triggered when a new menu component has finished moving in...
    public void OnMenuComponentSwitchComplete()
    {
        if (currentComponent == null) return;
        if (currentComponent.component == previousComponent) return; //Stop this from triggering multiple times
        //previousComponent = currentComponent.component;

        Game.Log("Menu: Current menu component switch complete: " + currentComponent.component);

        //When loading...
        if (currentComponent.component == Component.loadingCity)
        {
            //Make a new city constructor if needed...
            if (CityConstructor.Instance == null)
            {
                PrefabControls.Instance.mapContainer.gameObject.AddComponent<CityConstructor>();
            }

            //If currently generating a new city, stop
            if (CityConstructor.Instance.enabled)
            {
                CityConstructor.Instance.Cancel();
            }

            //Reset loading bar
            loadingSlider.value = 0f;

            //Go!
            //If already loaded a game, reset this scene with the instruction to load right away
            if (SessionData.Instance.dirtyScene)
            {
                RestartSafeController.Instance.loadFromDirty = true;

                Game.Log("CityGen: Scene is dirty, restarting to continue load process...");

                //Reset the scene to safely remove everything
                AudioController.Instance.StopAllSounds();
                UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
            }
            else
            {
                Game.Log("CityGen: Scene is clean, continuing with load process...");

                //Figure out what to do...
                if (RestartSafeController.Instance.loadSaveGame)
                {
                    CityConstructor.Instance.LoadSaveGame();
                }
                else if (RestartSafeController.Instance.generateNew)
                {
                    CityConstructor.Instance.GenerateNewCity();
                }
                else if (RestartSafeController.Instance.newGameLoadCity)
                {
                    CityConstructor.Instance.LoadCityStartNewGame();
                }
            }
        }
    }

    //Select city button
    public void SelectCityButton()
    {
        Game.Log("Menu: Select city");

        RestartSafeController.Instance.newGameLoadCity = true;
        RestartSafeController.Instance.generateNew = false;
        RestartSafeController.Instance.loadSaveGame = false;

        RefreshMapDropdown();

        if (selectCityDropdown.dropdown.options.Count > 0)
        {
            //Load existing...
            if (RestartSafeController.Instance.loadCityFileInfo != null)
            {
                LoadCityInfo(RestartSafeController.Instance.loadCityFileInfo);
            }
            //Load previously selected...
            else
            {
                string prevSelected = PlayerPrefs.GetString("lastCity");

                FileInfo foundInfo = null;

                foreach (FileInfo fileInfo in cityMapFiles)
                {
                    if(fileInfo.Name == prevSelected)
                    {
                        foundInfo = fileInfo;
                        break;
                    }
                }

                if (foundInfo != null)
                {
                    LoadCityInfo(foundInfo);
                }
                //Load default value...
                else
                {
                    LoadCityInfo(cityMapFiles[selectCityDropdown.dropdown.value]);
                }
            }
        }
        else LoadCityInfo(null);
    }

    private void RefreshMapDropdown()
    {
        cityMapFiles.Clear();
        cityInfoFiles.Clear();
        cityInfoDict.Clear();

        //Get embedded files
        var embed = new DirectoryInfo(Application.streamingAssetsPath + "/Cities");
        List<FileInfo> embeddedFiles = embed.GetFiles().ToList();

        foreach (FileInfo map in embeddedFiles)
        {
            if (map.Extension.ToLower() == ".cit" || map.Extension.ToLower() == ".citb")
            {
                cityMapFiles.Add(map);
            }
            else if (map.Extension.ToLower() == ".txt")
            {
                cityInfoFiles.Add(map);
            }
        }

        //Get files from saved directory
        //Check city dir
        if (!Directory.Exists(Application.persistentDataPath +  "/Cities"))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(Application.persistentDataPath +  "/Cities");
        }

        var info = new DirectoryInfo(Application.persistentDataPath +  "/Cities");
        List<FileInfo> createdFiles = info.GetFiles().ToList();

        foreach (FileInfo map in createdFiles)
        {
            if (map.Extension.ToLower() == ".cit" || map.Extension.ToLower() == ".citb")
            {
                cityMapFiles.Add(map);
            }
            else if (map.Extension.ToLower() == ".txt")
            {
                cityInfoFiles.Add(map);
            }
        }

        List<string> newOptionsNames = new List<string>();
        List<string> newOptionsPaths = new List<string>();

        for (int i = 0; i < cityMapFiles.Count; i++)
        {
            FileInfo fileInfo = cityMapFiles[i];
            string[] split = fileInfo.Name.Split('.');

            string cityName = split[0];

            if (!cityInfoDict.ContainsKey(fileInfo.Name))
            {
                //Find or generate city info file...
                FileInfo infoFile = null;
                CityInfoData infoData = null;

                foreach (FileInfo fi in cityInfoFiles)
                {
                    if (fi.Name.Substring(0, fi.Name.Length - fi.Extension.Length) == fileInfo.Name.Substring(0, fileInfo.Name.Length - fileInfo.Extension.Length))
                    {
                        infoFile = fi;
                        break;
                    }
                }

                //We've found the city info file...
                if (infoFile != null)
                {
                    //Parse in the info to class and keep reference to it...
                    using (StreamReader streamReader = File.OpenText(infoFile.FullName))
                    {
                        string jsonString = streamReader.ReadToEnd();
                        infoData = JsonUtility.FromJson<CityInfoData>(jsonString);
                    }
                }
                //Generate and save a new city info file...
                else
                {
                    infoData = Toolbox.Instance.GenerateCityInfoFile(fileInfo);
                }

                //Load to dictionary...
                cityInfoDict.Add(fileInfo.Name, infoData);
            }

            newOptionsNames.Add(cityName);
            newOptionsPaths.Add(fileInfo.Name);
        }

        selectCityDropdown.AddOptions(newOptionsPaths, false, newOptionsNames);
    }

    //Called when a new city is selected (including on enable city options)
    public void OnNewCitySelected()
    {
        if (selectCityDropdown.dropdown.options.Count > 0)
        {
            LoadCityInfo(cityMapFiles[selectCityDropdown.dropdown.value]);
        }
        else LoadCityInfo(null);
    }

    //Load city info data
    public void LoadCityInfo(FileInfo fileInfo)
    {
        Game.Log("Menu: Load city info: " + fileInfo.FullName);
        RestartSafeController.Instance.loadCityFileInfo = fileInfo;

        string infoName = string.Empty;

        if (fileInfo != null)
        {
            string[] split = RestartSafeController.Instance.loadCityFileInfo.Name.Split('.');
            infoName = split[0];
        }

        if (infoName != null && infoName.Length > 0 && cityInfoDict.ContainsKey(fileInfo.Name))
        {
            selectedCityInfoData = cityInfoDict[fileInfo.Name];

            Game.Log("Menu: Loading city info " + fileInfo.Name + ": " + selectedCityInfoData.shareCode);

            int sizeIndex = CityControls.Instance.citySizes.FindIndex(item => Mathf.RoundToInt(item.v2.x) == Mathf.RoundToInt(selectedCityInfoData.citySize.x) && Mathf.RoundToInt(item.v2.y) == Mathf.RoundToInt(selectedCityInfoData.citySize.y));
            if (sizeIndex < 0) sizeIndex = 0;

            string sizeString = citySizeDropdown.dropdown.options[sizeIndex].text;

            selectedCityDetailsText.text = infoName + "\n" + Strings.Get("ui.interface", "City Size") + ": " + sizeString + "\n" + Strings.Get("ui.interface", "Population") + ": " + selectedCityInfoData.population;
            selectedCityShareCode.text = Strings.Get("ui.interface", "Share Code") + ": " + selectedCityInfoData.shareCode;

            selectedCityContinueButton.SetInteractable(true);
            selectedCityCopyShareCodeButton.SetInteractable(true);

            //Only enable deletion if this is not internal
            if (fileInfo.DirectoryName.Contains("StreamingAssets"))
            {
                Game.Log("Menu: " + fileInfo.DirectoryName + ": Disabling deletion button");
                deleteCityButton.SetInteractable(false);
            }
            else
            {
                Game.Log("Menu: " + fileInfo.DirectoryName + ": Enabling deletion button");
                deleteCityButton.SetInteractable(true);
            }

            //Save last selected city...
            PlayerPrefs.SetString("lastCity", fileInfo.Name);

            //Set dropdown
            selectCityDropdown.SelectFromStaticOption(fileInfo.Name);

            //Load this city instead of generating a new one...
            RestartSafeController.Instance.generateNew = false;
        }
        else
        {
            selectedCityInfoData = null;
            RestartSafeController.Instance.loadCityFileInfo = null;
            selectedCityDetailsText.text = string.Empty;
            selectedCityShareCode.text = string.Empty;

            selectedCityContinueButton.SetInteractable(false);
            selectedCityCopyShareCodeButton.SetInteractable(false);
            deleteCityButton.SetInteractable(false);

            RestartSafeController.Instance.generateNew = true;
        }
    }

    //Called when gen city is entered
    public void SelectGenNewCity()
    {
        Game.Log("Menu: Generate city");

        RestartSafeController.Instance.newGameLoadCity = false;
        RestartSafeController.Instance.generateNew = true;
        RestartSafeController.Instance.loadSaveGame = false;
    }

    public void RandomCityName()
    {
        List<string> mainList = new List<string>();
        mainList.Add("names.district.western");
        mainList.Add("names.district.eastern");

        if (Toolbox.Instance.Rand(0f, 1f) < 0.3f)
        {
            //Prefix
            if (Toolbox.Instance.Rand(0f, 1f) < 0.5f)
            {
                RestartSafeController.Instance.cityName = NameGenerator.Instance.GenerateName("names.district.prefix", 1f, mainList[Toolbox.Instance.Rand(0, mainList.Count)], 1f, "names.district.suffix", 0f);
            }
            //Suffix
            else
            {
                RestartSafeController.Instance.cityName = NameGenerator.Instance.GenerateName("names.district.prefix", 0f, mainList[Toolbox.Instance.Rand(0, mainList.Count)], 1f, "names.district.suffix", 1f);
            }
        }
        else
        {
            RestartSafeController.Instance.cityName = NameGenerator.Instance.GenerateName("names.district.prefix", 0f, mainList[Toolbox.Instance.Rand(0, mainList.Count)], 1f, "names.district.suffix", 0f);
        }

        OnChangeCityGenerationOption();
    }

    public void PasteShareCode()
    {
        if (GUIUtility.systemCopyBuffer != null && GUIUtility.systemCopyBuffer.Length > 0)
        {
            ParseShareCode(GUIUtility.systemCopyBuffer);
        }
    }

    private void ParseShareCode(string newCode)
    {
        string parsedCityName = "New City";
        int parsedSizeX = 5;
        int parsedSizeY = 5;
        //int parsedPop = 4;
        string parsedVersion = string.Empty;
        string parsedSeed = "daifbeufgaho";

        Toolbox.Instance.ParseShareCode(newCode, out parsedCityName, out parsedSizeX, out parsedSizeY, out parsedVersion, out parsedSeed);

        RestartSafeController.Instance.cityName = parsedCityName;
        RestartSafeController.Instance.cityX = parsedSizeX;
        RestartSafeController.Instance.cityY = parsedSizeY;

        int sizeIndex = CityControls.Instance.citySizes.FindIndex(item => Mathf.RoundToInt(item.v2.x) == parsedSizeX && Mathf.RoundToInt(item.v2.y) == parsedSizeY);
        if (sizeIndex < 0) sizeIndex = 0;

        citySizeDropdown.dropdown.SetValueWithoutNotify(sizeIndex);

        //parsedPop = Mathf.Clamp(parsedPop, 0, cityPopDropdown.dropdown.options.Count);
        //cityPopDropdown.dropdown.SetValueWithoutNotify(parsedPop);
        //RestartSafeController.Instance.population = parsedPop;

        RestartSafeController.Instance.seed = parsedSeed;

        OnChangeCityGenerationOption();
    }

    public void CopyShareCodeGenerate()
    {
        GUIUtility.systemCopyBuffer = shareCodeText.text;
    }

    public void CustomShareCodeButton()
    {
        PopupMessageController.Instance.PopupMessage("shareCode", true, true, RButton: "Confirm", enableInputField: true);
        PopupMessageController.Instance.inputField.text = shareCodeText.text;
        PopupMessageController.Instance.OnLeftButton += OnChangeShareCodePopupCancel;
        PopupMessageController.Instance.OnRightButton += OnChangeShareCodePopupConfirm;
    }

    public void OnChangeShareCodePopupCancel()
    {
        PopupMessageController.Instance.OnLeftButton -= OnChangeShareCodePopupCancel;
        PopupMessageController.Instance.OnRightButton -= OnChangeShareCodePopupConfirm;
    }

    public void OnChangeShareCodePopupConfirm()
    {
        PopupMessageController.Instance.OnLeftButton -= OnChangeShareCodePopupCancel;
        PopupMessageController.Instance.OnRightButton -= OnChangeShareCodePopupConfirm;

        //Try to get last character name
        ParseShareCode(PopupMessageController.Instance.inputField.text);
    }

    public void OnGenerateNewSeed()
    {
        RestartSafeController.Instance.seed = Toolbox.Instance.GenerateSeed(16);
        OnChangeCityGenerationOption();
    }

    public void OnChangeCityNameButton()
    {
        PopupMessageController.Instance.PopupMessage("cityName", true, true, RButton: "Confirm", enableInputField: true);
        PopupMessageController.Instance.inputField.text = RestartSafeController.Instance.cityName;
        PopupMessageController.Instance.OnLeftButton += OnChangeCityNamePopupCancel;
        PopupMessageController.Instance.OnRightButton += OnChangeCityNamePopupConfirm;
    }

    public void OnChangeCityNamePopupCancel()
    {
        PopupMessageController.Instance.OnLeftButton -= OnChangeCityNamePopupCancel;
        PopupMessageController.Instance.OnRightButton -= OnChangeCityNamePopupConfirm;
    }

    public void OnChangeCityNamePopupConfirm()
    {
        PopupMessageController.Instance.OnLeftButton -= OnChangeCityNamePopupCancel;
        PopupMessageController.Instance.OnRightButton -= OnChangeCityNamePopupConfirm;

        //Try to get last character name
        RestartSafeController.Instance.cityName = Toolbox.Instance.RemoveCharacters(PopupMessageController.Instance.inputField.text, true, false, true, false);

        //Avoid long city names
        if (RestartSafeController.Instance.cityName.Length > 100) RestartSafeController.Instance.cityName = RestartSafeController.Instance.cityName.Substring(0, 100);

        OnChangeCityGenerationOption();
    }

    public void OnChangeCityGenerationOption()
    {
        changeCityNameButton.text.text = RestartSafeController.Instance.cityName;
        //RestartSafeController.Instance.population = cityPopDropdown.dropdown.value;
        RestartSafeController.Instance.cityX = Mathf.RoundToInt(CityControls.Instance.citySizes[citySizeDropdown.dropdown.value].v2.x);
        RestartSafeController.Instance.cityY = Mathf.RoundToInt(CityControls.Instance.citySizes[citySizeDropdown.dropdown.value].v2.y);

        if(Game.Instance.smallCitiesOnly)
        {
            RestartSafeController.Instance.cityX = Mathf.RoundToInt(CityControls.Instance.citySizes[0].v2.x);
            RestartSafeController.Instance.cityY = Mathf.RoundToInt(CityControls.Instance.citySizes[0].v2.y);
        }

        shareCodeText.text = Toolbox.Instance.GetShareCode(RestartSafeController.Instance.cityName, RestartSafeController.Instance.cityX, RestartSafeController.Instance.cityY, Game.Instance.buildID, RestartSafeController.Instance.seed);
    }

    //Called when char selection is entered
    public void NewCharacter()
    {
        //Try to get last character name
        string lastPlayerName = PlayerPrefs.GetString("lastPlayerName");

        //No previous player name: Generate random
        if (lastPlayerName == null || lastPlayerName.Length <= 0)
        {
            RandomPlayerGender();
            RandomPartnerGender();
            RandomPlayerName();
            RandomSkinTone();
        }
        else
        {
            SetPlayerName(lastPlayerName);
        }
    }

    //Push player name to game data
    public void SetPlayerName(string newName)
    {
        //Get rid of spaces at start and end
        newName = newName.Trim();

        if (newName == null || newName.Length <= 0)
        {
            RandomPlayerName();
        }

        string[] split = newName.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

        //New player name needed
        if (split.Length <= 0)
        {
            RandomPlayerName();
        }
        //Player surname needed
        else if (split.Length == 1)
        {
            RestartSafeController.Instance.newGamePlayerFirstName = split[0];
            RandomPlayerName(true);
        }
        //We're ok
        else
        {
            RestartSafeController.Instance.newGamePlayerFirstName = split[0];

            string surname = string.Empty;

            for (int i = 1; i < split.Length; i++)
            {
                if (split[i].Length <= 0) continue;
                surname += split[i];
                if (i < split.Length - 1) surname += " ";
            }

            RestartSafeController.Instance.newGamePlayerSurname = surname;
            OnPlayerNameChanged();
        }
    }

    public void RandomPlayerName(bool surnameOnly = false)
    {
        List<Descriptors.EthnicGroup> groups = new List<Descriptors.EthnicGroup>();

        foreach (SocialStatistics.EthnicityFrequency freq in SocialStatistics.Instance.ethnicityFrequencies)
        {
            for (int i = 0; i < freq.frequency; i++)
            {
                groups.Add(freq.ethnicity);
            }
        }

        Descriptors.EthnicGroup nameGroup = groups[Toolbox.Instance.Rand(0, groups.Count)];

        //Generate name
        string nameFileString = "male";

        if (RestartSafeController.Instance.newGamePlayerGender == Human.Gender.female)
        {
            nameFileString = "female";
        }

        string prefixOnly;
        string mainOnly;
        string suffixOnly;
        bool notNeeded;

        if (!surnameOnly) RestartSafeController.Instance.newGamePlayerFirstName = NameGenerator.Instance.GenerateName(null, 0f, "names." + nameGroup.ToString() + ".first." + nameFileString, 1f, null, 0f, out prefixOnly, out mainOnly, out suffixOnly, out notNeeded, out _);
        RestartSafeController.Instance.newGamePlayerSurname = NameGenerator.Instance.GenerateName(null, 0f, "names." + nameGroup.ToString() + ".sur", 1f, null, 0f, out prefixOnly, out mainOnly, out suffixOnly, out notNeeded, out _);

        OnPlayerNameChanged();
    }

    public void OnChangeNameButton()
    {
        PopupMessageController.Instance.PopupMessage("characterName", true, true, RButton: "Confirm", enableInputField: true);
        PopupMessageController.Instance.inputField.text = RestartSafeController.Instance.newGamePlayerFirstName + " " + RestartSafeController.Instance.newGamePlayerSurname;
        PopupMessageController.Instance.OnLeftButton += OnChangeNamePopupCancel;
        PopupMessageController.Instance.OnRightButton += OnChangeNamePopupConfirm;
    }

    public void OnChangeNamePopupCancel()
    {
        PopupMessageController.Instance.OnLeftButton -= OnChangeNamePopupCancel;
        PopupMessageController.Instance.OnRightButton -= OnChangeNamePopupConfirm;
    }

    public void OnChangeNamePopupConfirm()
    {
        PopupMessageController.Instance.OnLeftButton -= OnChangeNamePopupCancel;
        PopupMessageController.Instance.OnRightButton -= OnChangeNamePopupConfirm;

        //Try to get last character name
        SetPlayerName(Toolbox.Instance.RemoveCharacters(PopupMessageController.Instance.inputField.text, true, true, true, false));
    }

    public void OnPlayerNameChanged()
    {
        Game.Log("Menu: New player name: " + RestartSafeController.Instance.newGamePlayerFirstName + " " + RestartSafeController.Instance.newGamePlayerSurname);
        PlayerPrefs.SetString("lastPlayerName", RestartSafeController.Instance.newGamePlayerFirstName + " " + RestartSafeController.Instance.newGamePlayerSurname);
        playerNameButton.text.text = RestartSafeController.Instance.newGamePlayerFirstName + " " + RestartSafeController.Instance.newGamePlayerSurname;
    }

    public void OnPlayerGenderChange()
    {
        RestartSafeController.Instance.newGamePlayerGender = (Human.Gender)playerGenderDropdown.dropdown.value;
    }

    public void OnPartnerGenderChange()
    {
        RestartSafeController.Instance.newGamePartnerGender = (Human.Gender)partnerGenderDropdown.dropdown.value;
    }

    public void RandomPlayerGender()
    {
        playerGenderDropdown.dropdown.value = Toolbox.Instance.Rand(0, 3);
    }

    public void RandomPartnerGender()
    {
        partnerGenderDropdown.dropdown.value = Toolbox.Instance.Rand(0, 3);
    }

    public void RandomSkinTone()
    {
        playerSkinToneSelect.SetChosen(Toolbox.Instance.Rand(0, 12));
    }

    public void OnSkinToneChange()
    {
        RestartSafeController.Instance.newGamePlayerSkinTone = playerSkinToneSelect.GetCurrentSelectedColourValue();
    }

    //Save game button
    public void SaveGame()
    {
        if (Game.Instance.disableSaveLoadGames) return;
        RefreshSaveEntries();
        SelectNewSave(newSaveGameEntry);
    }

    //Called when load game is selected
    public void LoadGame()
    {
        if (Game.Instance.disableSaveLoadGames) return;
        RefreshSaveEntries();
        SelectNewSave(null);

        RestartSafeController.Instance.newGameLoadCity = false;
        RestartSafeController.Instance.generateNew = false;
        RestartSafeController.Instance.loadSaveGame = true;
    }

    public void OnSaveButton()
    {
        if (Game.Instance.disableSaveLoadGames) return;

        if (selectedSave != null && selectedSave.info != null)
        {
            PopupMessageController.Instance.PopupMessage("saveGameOverwrite", true, true, RButton: "Confirm", enableInputField: true, inputFieldDefault: selectedSave.info.Name.Substring(0, selectedSave.info.Name.Length - selectedSave.info.Extension.Length));
            PopupMessageController.Instance.OnLeftButton += CancelOverwriteSave;
            PopupMessageController.Instance.OnRightButton += OverwriteSave;
        }
        else
        {
            PopupMessageController.Instance.PopupMessage("saveGame", true, true, RButton: "Confirm", enableInputField: true, inputFieldDefault: Game.Instance.playerFirstName + " " + Game.Instance.playerSurname + " [" + CityData.Instance.cityName + " " + System.DateTime.Now.ToString("yyyy-dd-M-HH-mm-ss.ff")+"]");
            PopupMessageController.Instance.OnLeftButton += CancelOverwriteSave;
            PopupMessageController.Instance.OnRightButton += OverwriteSave;
        }
    }

    public void CancelOverwriteSave()
    {
        PopupMessageController.Instance.OnLeftButton -= CancelOverwriteSave;
        PopupMessageController.Instance.OnRightButton -= OverwriteSave;
    }

    public void OverwriteSave()
    {
        PopupMessageController.Instance.OnLeftButton -= CancelOverwriteSave;
        PopupMessageController.Instance.OnRightButton -= OverwriteSave;

        StartSaveAsync();
    }

    public async void StartSaveAsync()
    {
        if (Player.Instance.transitionActive) return;
        if (Game.Instance.disableSaveLoadGames) return;

        string saveName = Toolbox.Instance.RemoveCharacters(PopupMessageController.Instance.inputField.text, true, false, false, false);

        //Avoid long city names
        if (saveName.Length > 100) saveName = saveName.Substring(0, 100);

        string savePath = Application.persistentDataPath + "/Save/" + saveName + ".sod";

        //if (saveToDevCloudToggle != null && saveToDevCloudToggle.isOn && Toolbox.Instance.devCloudSaveConfig != null)
        //{
        //    savePath = Toolbox.Instance.devCloudSaveConfig.path + "\\" + saveGameInput.text + ".sod";
        //    Game.Log("Cloud save path: " + savePath);
        //}

        Game.Log("CityGen: Saving at: " + savePath);
        await SaveStateController.Instance.CaptureSaveStateAsync(savePath);

        SetMenuComponent(Component.mainMenuButtons);

        //Resume game
        EnableMainMenu(false, true, true);
    }

    //Execute a save complete message
    private void SaveCompleteMessage()
    {
        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "Game saved"));
    }

    public void OnDeleteSaveButton()
    {
        if(selectedSave != null)
        {
            if (File.Exists(selectedSave.info.FullName))
            {
                PopupMessageController.Instance.PopupMessage("citySave", true, true, RButton: "Confirm");
                PopupMessageController.Instance.OnLeftButton += CancelDeleteSave;
                PopupMessageController.Instance.OnRightButton += DeleteSave;
            }
        }
    }

    public void CancelDeleteSave()
    {
        PopupMessageController.Instance.OnLeftButton -= CancelDeleteSave;
        PopupMessageController.Instance.OnRightButton -= DeleteSave;
    }

    public void DeleteSave()
    {
        PopupMessageController.Instance.OnLeftButton -= CancelDeleteSave;
        PopupMessageController.Instance.OnRightButton -= DeleteSave;

        if (File.Exists(selectedSave.info.FullName) && !selectedSave.isInternal)
        {
            File.Delete(selectedSave.info.FullName);

            RefreshSaveEntries();
        }
    }

    public void RefreshSaveEntries()
    {
        Game.Log("Menu: Refresh save/load entries");

        SelectNewSave(null);

        //Wipe
        while (spawnedLoadGames.Count > 0)
        {
            Destroy(spawnedLoadGames[0].gameObject);
            spawnedLoadGames.RemoveAt(0);
        }

        while (spawnedSaveGames.Count > 0)
        {
            Destroy(spawnedSaveGames[0].gameObject);
            spawnedSaveGames.RemoveAt(0);
        }

        //Get file info
        //Check city dir
        if (!Directory.Exists(Application.persistentDataPath + "/Save"))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(Application.persistentDataPath + "/Save");
        }

        //Get internal files
        var embedInternal = new DirectoryInfo(Application.streamingAssetsPath + "/Save");
        List<FileInfo> embeddedFilesInternal = embedInternal.GetFiles("*.sod", SearchOption.AllDirectories).ToList();

        //Get save files
        var embed = new DirectoryInfo(Application.persistentDataPath + "/Save");

        List<FileInfo> embeddedFiles = embed.GetFiles("*.sod", SearchOption.AllDirectories).ToList(); //This detects sodb as well??
        embeddedFiles.AddRange(embed.GetFiles("*.sodb", SearchOption.AllDirectories).ToList());

        float ySave = -4;
        float yLoad = -4;

        //Create 'new save' button
        GameObject newSaveGame = Instantiate(saveGameEntryPrefab, saveGameContentRect);
        newSaveGameEntry = newSaveGame.GetComponent<SaveGameEntryController>();
        newSaveGameEntry.Setup(null);
        newSaveGameEntry.icon.enabled = false;
        spawnedSaveGames.Add(newSaveGameEntry);

        ySave -= 62;
        yLoad -= 62;

        //Only list internal saves in the load list
        foreach (FileInfo save in embeddedFilesInternal)
        {
            GameObject newLoad = Instantiate(saveGameEntryPrefab, loadGameContentRect);
            SaveGameEntryController sec2 = newLoad.GetComponent<SaveGameEntryController>();
            sec2.Setup(save);
            sec2.isInternal = true;
            sec2.icon.enabled = false;
            spawnedLoadGames.Add(sec2);

            yLoad -= 62;

            loadGameContentRect.sizeDelta = new Vector2(loadGameContentRect.sizeDelta.x, -yLoad);
        }

        //Sort files by last modified
        embeddedFiles.Sort((p2, p1) => p1.LastWriteTime.CompareTo(p2.LastWriteTime)); //Newest first

        foreach (FileInfo save in embeddedFiles)
        {
            GameObject newSave = Instantiate(saveGameEntryPrefab, saveGameContentRect);
            SaveGameEntryController sec = newSave.GetComponent<SaveGameEntryController>();
            sec.Setup(save);
            sec.icon.enabled = false;
            spawnedSaveGames.Add(sec);

            GameObject newLoad = Instantiate(saveGameEntryPrefab, loadGameContentRect);
            SaveGameEntryController sec2 = newLoad.GetComponent<SaveGameEntryController>();
            sec2.Setup(save);
            sec2.icon.enabled = false;
            spawnedLoadGames.Add(sec2);

            ySave -= 62;
            yLoad -= 62;

            saveGameContentRect.sizeDelta = new Vector2(saveGameContentRect.sizeDelta.x, -ySave);
            loadGameContentRect.sizeDelta = new Vector2(loadGameContentRect.sizeDelta.x, -ySave);
        }
    }

    public void SelectNewSave(SaveGameEntryController sec)
    {
        loadButton.SetInteractable(false);
        deleteButton.SetInteractable(false);
        deleteButton2.SetInteractable(false);

        foreach (SaveGameEntryController exist in spawnedLoadGames)
        {
            exist.icon.enabled = false;
        }

        foreach (SaveGameEntryController exist in spawnedSaveGames)
        {
            exist.icon.enabled = false;
        }

        selectedSave = sec;

        if (selectedSave != null)
        {
            if(!selectedSave.isInternal)
            {
                deleteButton.SetInteractable(true);
                deleteButton2.SetInteractable(true);
            }

            selectedSave.icon.enabled = true;
            loadButton.SetInteractable(true);
            RestartSafeController.Instance.saveStateFileInfo = selectedSave.info;
        }
        else
        {
            //RestartSafeController.Instance.saveStateFileInfo = null;
            loadButton.SetInteractable(false);
            deleteButton.SetInteractable(false);
            deleteButton2.SetInteractable(false);
        }
    }

    public void DeleteCityButton()
    {
        if (File.Exists(RestartSafeController.Instance.loadCityFileInfo.FullName))
        {
            PopupMessageController.Instance.PopupMessage("cityDelete", true, true, RButton: "Confirm");
            PopupMessageController.Instance.OnLeftButton += CancelDeleteCity;
            PopupMessageController.Instance.OnRightButton += DeleteCity;
        }
    }

    public void CancelDeleteCity()
    {
        PopupMessageController.Instance.OnLeftButton -= CancelDeleteCity;
        PopupMessageController.Instance.OnRightButton -= DeleteCity;
    }

    public void DeleteCity()
    {
        PopupMessageController.Instance.OnLeftButton -= CancelDeleteCity;
        PopupMessageController.Instance.OnRightButton -= DeleteCity;

        if (File.Exists(RestartSafeController.Instance.loadCityFileInfo.FullName))
        {
            string txtPath = RestartSafeController.Instance.loadCityFileInfo.FullName.Substring(0, RestartSafeController.Instance.loadCityFileInfo.FullName.Length - RestartSafeController.Instance.loadCityFileInfo.Extension.Length) + ".txt";

            File.Delete(RestartSafeController.Instance.loadCityFileInfo.FullName);

            //Also delete txt header file
            if(txtPath != null && txtPath.Length > 0 && File.Exists(txtPath))
            {
                File.Delete(txtPath);
            }

            if (CityConstructor.Instance != null) CityConstructor.Instance.currentData = null;
            RefreshMapDropdown();
            OnNewCitySelected();
        }
    }

    //Exit game
    public void ExitGame()
    {
        Application.Quit();
    }

    //Resume game
    public void ResumeGame()
    {
        MainMenuController.Instance.EnableMainMenu(false, true, true);
    }

    //Spawn help section
    public void Help()
    {
        MainMenuController.Instance.EnableMainMenu(false, true, true);
        InterfaceController.Instance.ToggleNotebook(openHelpSection: true);
    }

    //Bug report
    public void BugReport()
    {
        if(Game.Instance.enableBugReporting)
        {
            try
            {
                Game.Log("Bug report");

                //Disable depth of field
                if(SessionData.Instance.dof != null)
                {
                    saveDof = SessionData.Instance.dof.active;
                    SessionData.Instance.dof.active = false;
                }

                feedbackForm.Show();

                //Disable regular raycasters
                foreach(GraphicRaycaster raycaster in PopupMessageController.Instance.otherCanvasRaycasters)
                {
                    raycaster.enabled = false;
                }
            }
            catch
            {

            }
        }
    }

    //Feedback form
    public void FeedbackForm()
    {
        Application.OpenURL("https://fireshinegames.jotform.com/230333480267957");
    }

    public void OnFeedbackFormClosed()
    {
        try
        {
            if (SessionData.Instance.dof != null)
            {
                SessionData.Instance.dof.active = saveDof;
            }

            //Enable regular raycasters
            foreach (GraphicRaycaster raycaster in PopupMessageController.Instance.otherCanvasRaycasters)
            {
                if (raycaster == null) continue;
                raycaster.enabled = true;
            }
        }
        catch
        {
            Game.LogError("Error detected while closing feedback form");
        }
    }

    public void OnOpenBugReport()
    {
        Game.Log("Menu: Opening bug report (new)...");

        if (Game.Instance.enableBugReporting)
        {
            try
            {
                //Disable depth of field
                if (SessionData.Instance.dof != null)
                {
                    saveDof = SessionData.Instance.dof.active;
                    SessionData.Instance.dof.active = false;
                }

                feedbackForm.Show();
            }
            catch
            {
                Game.LogError("Error detected while opening feedback form");
            }
        }
    }

    public void OnCloseBugReport()
    {
        try
        {
            if (SessionData.Instance.dof != null)
            {
                SessionData.Instance.dof.active = saveDof;
            }
        }
        catch
        {
            Game.LogError("Error detected while closing feedback form");
        }
    }

    //New version of the bug report form
    public void SumbitBugReport()
    {
        Game.Log("Menu: Submitting bug report (new)...");

        if (Game.Instance.enableBugReporting)
        {
            if(bugReportTimer <= 0f)
            {
                bugReportTimer = 15f;

                try
                {
                    //Push input data to feedback form
                    ffCategory.value = categoryDropdown.dropdown.value;
                    ffPriority.value = priorityDropdown.dropdown.value;
                    ffNameInput.text = bugNameInput.text.text;
                    ffDescriptionInput.text = bugDetailsInput.text.text;

                    //Send a current screenshot
                    if(sendScreenshotToggle.isOn)
                    {
                        try
                        {
                            //Create a rendertexture to render camera only
                            RenderTexture screenTexture = new RenderTexture(Screen.width, Screen.height, 16);
                            CameraController.Instance.cam.targetTexture = screenTexture;
                            RenderTexture.active = screenTexture;
                            CameraController.Instance.cam.Render();
                            Texture2D renderedTexture = new Texture2D(Screen.width, Screen.height);
                            renderedTexture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
                            RenderTexture.active = null;

                            if ((renderedTexture.width ^ 2 * renderedTexture.height ^ 2) > 4082)
                            {
                                float scale = 1920 / Mathf.Max(renderedTexture.width, renderedTexture.height);

                                var width = Mathf.RoundToInt(renderedTexture.width * scale);
                                var height = Mathf.RoundToInt(renderedTexture.height * scale);

                                // prepare texture
                                renderedTexture.filterMode = FilterMode.Trilinear;
                                renderedTexture.Apply(false);

                                // scale texture using the gpu
                                var rt = new RenderTexture(width, height, 32);
                                Graphics.SetRenderTarget(rt);
                                GL.LoadPixelMatrix(0, 1, 1, 0);
                                GL.Clear(true, true, default);
                                Graphics.DrawTexture(new Rect(0, 0, 1, 1), renderedTexture);

                                // apply to texture
                                renderedTexture.Reinitialize(width, height);
                                renderedTexture.ReadPixels(new Rect(0, 0, width, height), 0, 0, false);
                                renderedTexture.Apply(false);
                            }

                            //byte[] byteArray = renderedTexture.EncodeToPNG();
                            feedbackForm.CurrentReport.AttachFile("screenshot.png", renderedTexture.EncodeToPNG());

                            screenTexture.Release();
                            CameraController.Instance.cam.targetTexture = null;
                        }
                        catch
                        {
                            Game.LogError("Unable to capture screenshot for bug report");
                        }
                    }

                    //Send the previous log
                    ffPrevLogCollector.collectionEnabled = sendPrevLogToggle.isOn;

                    //Send system specs
                    ffSystemInfo.collectionEnabled = sendSystemSpecsToggle.isOn;

                    //Send a save game
                    if(bugSaveDropdown.dropdown.value > 0)
                    {
                        //Get the save file info
                        if (Directory.Exists(Application.persistentDataPath + "/Save"))
                        {
                            var embed = new DirectoryInfo(Application.persistentDataPath + "/Save");

                            List<FileInfo> embeddedFiles = embed.GetFiles("*.sod", SearchOption.AllDirectories).ToList();
                            embeddedFiles.AddRange(embed.GetFiles("*.sodb", SearchOption.AllDirectories).ToList());

                            FileInfo foundSave = embeddedFiles.Find(item => item.Name == bugSaveDropdown.staticOptionReference[bugSaveDropdown.dropdown.value]);

                            if (foundSave != null)
                            {
                                //Have a filesize of 200mb maximum
                                if (foundSave.Length <= 1000000 * 200)
                                {
                                    //Does this need compressing?
                                    if(foundSave.Extension.ToLower() == ".sodb")
                                    {
                                        Game.Log("Menu: Save to attached is already in a compressed format: " + foundSave.FullName + " the size is " + (foundSave.Length / 1000000f) + "mb");

                                        //File is already compressed...
                                        try
                                        {
                                            string saveName = Toolbox.Instance.RemoveCharacters(bugNameInput.text.text, true, false, true, false);
                                            feedbackForm.CurrentReport.AttachFile(saveName + ".sodb", foundSave.FullName);
                                        }
                                        catch
                                        {
                                            Game.LogError("Unable to read compressed save at: " + foundSave.FullName);
                                        }
                                    }
                                    else
                                    {
                                        Game.Log("Menu: Found save file to attach: " + foundSave.FullName + " the size is " + (foundSave.Length / 1000000f) + "mb");

                                        string compressedFile = foundSave.Directory.FullName + "\\" + "BugReportCompressedSave.temp";
                                        ulong[] progress = new ulong[1];

                                        Game.Log("Menu: Compressing file...");
                                        brotli.compressFile(foundSave.FullName, compressedFile, progress);

                                        //Attach
                                        if (File.Exists(compressedFile))
                                        {
                                            try
                                            {
                                                string saveName = Toolbox.Instance.RemoveCharacters(bugNameInput.text.text, true, false, true, false);
                                                feedbackForm.CurrentReport.AttachFile(saveName + ".sodb", compressedFile);
                                            }
                                            catch
                                            {
                                                Game.LogError("Unable to read compressed save at: " + compressedFile);
                                            }
                                        }
                                        else Game.LogError("Compressed save does not exist here: " + compressedFile);
                                    }
                                }
                                else Game.LogError("The save file is too large to submit!");

                            }
                            else Game.LogError("Unable to find save file!");
                        }
                    }

                    feedbackForm.Submit();

                    PopupMessageController.Instance.PopupMessage("bugSubmit", true, false, LButton: "Confirm", anyButtonClosesMsg: true);

                    ResetBugReportDetails();
                    SetMenuComponent(previousComponent);
                }
                catch
                {
                    Game.LogError("Unable to submit bug report!");
                }
            }
            else
            {
                Game.Log("Menu: Please wait for bug report timer: " + bugReportTimer);
                PopupMessageController.Instance.PopupMessage("bugSubmitDelay", true, false, LButton: "Confirm", anyButtonClosesMsg: true);
            }
        }
    }

    //Update the bug report dropdown with the latest save games
    public void RefreshSaveGameDropdown()
    {
        if(bugSaveDropdown != null)
        {
            List<string> saveGames = new List<string>();

            saveGames.Add("-"); //Add option for no save game

            //Get file info
            //Get embedded files
            if(Directory.Exists(Application.persistentDataPath + "/Save"))
            {
                var embed = new DirectoryInfo(Application.persistentDataPath + "/Save");
                List<FileInfo> embeddedFiles = embed.GetFiles("*.sod", SearchOption.AllDirectories).ToList();
                embeddedFiles.AddRange(embed.GetFiles("*.sodb", SearchOption.AllDirectories).ToList());

                //Sort files by last modified
                embeddedFiles.Sort((p2, p1) => p1.LastWriteTime.CompareTo(p2.LastWriteTime)); //Newest first

                //Only list internal saves in the load list
                foreach (FileInfo save in embeddedFiles)
                {
                    saveGames.Add(save.Name);
                }
            }

            bugSaveDropdown.AddOptions(saveGames, false);
            bugSaveDropdown.SelectFromStaticOption("-");
        }
    }

    //Reset the bug submission text boxes
    public void ResetBugReportDetails()
    {
        bugNameInput.text.text = Strings.Get("ui.interface", "Bug Name");
        bugDetailsInput.text.text = Strings.Get("ui.interface", "Bug Details");
    }

    public void OnChangeBugNameButton()
    {
        PopupMessageController.Instance.PopupMessage("bugName", true, true, RButton: "Confirm", enableInputField: true);
        PopupMessageController.Instance.inputField.text = bugNameInput.text.text;
        PopupMessageController.Instance.OnLeftButton += OnChangeBugNameCancel;
        PopupMessageController.Instance.OnRightButton += OnChangeBugNameConfirm;
    }

    public void OnChangeBugNameCancel()
    {
        PopupMessageController.Instance.OnLeftButton -= OnChangeBugNameCancel;
        PopupMessageController.Instance.OnRightButton -= OnChangeBugNameConfirm;
    }

    public void OnChangeBugNameConfirm()
    {
        bugNameInput.text.text = PopupMessageController.Instance.inputField.text;
        PopupMessageController.Instance.OnLeftButton -= OnChangeBugNameCancel;
        PopupMessageController.Instance.OnRightButton -= OnChangeBugNameConfirm;
    }

    public void OnChangeBugDetailsButton()
    {
        PopupMessageController.Instance.PopupMessage("bugDetails", true, true, RButton: "Confirm", enableInputField: true);
        PopupMessageController.Instance.inputField.text = bugDetailsInput.text.text;
        PopupMessageController.Instance.OnLeftButton += OnChangeBugDetailsCancel;
        PopupMessageController.Instance.OnRightButton += OnChangeBugDetailsConfirm;
    }

    public void OnChangeBugDetailsCancel()
    {
        PopupMessageController.Instance.OnLeftButton -= OnChangeBugDetailsCancel;
        PopupMessageController.Instance.OnRightButton -= OnChangeBugDetailsConfirm;
    }

    public void OnChangeBugDetailsConfirm()
    {
        bugDetailsInput.text.text = PopupMessageController.Instance.inputField.text;
        PopupMessageController.Instance.OnLeftButton -= OnChangeBugDetailsCancel;
        PopupMessageController.Instance.OnRightButton -= OnChangeBugDetailsConfirm;
    }

    //Sound event: Button click
    public void PlayButtonClick()
    {
        AudioController.Instance.Play2DSound(AudioControls.Instance.mainButton);
    }

    //Sound event: Button click
    public void PlayForwardButtonClick()
    {
        AudioController.Instance.Play2DSound(AudioControls.Instance.mainButtonForward);
    }

    //Sound event: Back button click
    public void PlayBackButtonClick()
    {
        AudioController.Instance.Play2DSound(AudioControls.Instance.mainButtonBack);
    }

    //Sound event: Checkbox
    public void PlayTickbox()
    {
        AudioController.Instance.Play2DSound(AudioControls.Instance.tickbox);
    }

    public void OnLanguageChange()
    {
        PopupMessageController.Instance.PopupMessage("lang_change", true, true, RButton: "Restart", anyButtonClosesMsg: true);
        PopupMessageController.Instance.OnLeftButton += LangCancelRestartGame;
        PopupMessageController.Instance.OnRightButton += LangRestartGame;
    }

    private void LangRestartGame()
    {
        PopupMessageController.Instance.OnLeftButton -= LangCancelRestartGame;
        PopupMessageController.Instance.OnRightButton -= LangRestartGame;

        Game.Log("CityGen: Restarting game due to language change...");

        AudioController.Instance.StopAllSounds();
        UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
    }

    private void LangCancelRestartGame()
    {
        PopupMessageController.Instance.OnLeftButton -= LangCancelRestartGame;
        PopupMessageController.Instance.OnRightButton -= LangRestartGame;

        //Re-load previous language
        Game.Log("CityGen: Reverting to loaded language: " + loadedLanguage);
        languageDropdown.SelectFromStaticOption(loadedLanguage);
        PlayerPrefsController.Instance.OnToggleChanged("language", true);
    }

    public void OnChangeResolution()
    {
        //Set to this resolution, save prefs manually
        List<Resolution> resolution = Screen.resolutions.ToList();

        int w = Screen.width;
        int h = Screen.height;
        int r = Screen.currentResolution.refreshRate;

        int index = resolution.FindIndex(item => item.ToString() == resolutionsDropdown.staticOptionReference[resolutionsDropdown.dropdown.value]);

        if (index > -1)
        {
            w = resolution[index].width;
            h = resolution[index].height;
            r = resolution[index].refreshRate;
        }

        FullScreenMode newFullScreen = Screen.fullScreenMode;
        newFullScreen = (FullScreenMode)fullScreenModeDropdown.dropdown.value;

        Screen.SetResolution(w, h, newFullScreen, r);

        //Save to prefs
        PlayerPrefs.SetInt("width", w);
        PlayerPrefs.SetInt("height", h);
        PlayerPrefs.SetInt("refresh", r);
        PlayerPrefs.SetInt("fullscreen", (int)newFullScreen);
    }

    public void CopyShareCodeToClipboard()
    {
        if (selectedCityInfoData != null)
        {
            GUIUtility.systemCopyBuffer = selectedCityInfoData.shareCode;
            Game.Log("Menu: Coppied share code to clipboard: " + GUIUtility.systemCopyBuffer);
        }
    }

    public void NewGameTypeButton(bool sandbox)
    {
        if (Game.Instance.disableSandbox && sandbox) return;
        RestartSafeController.Instance.sandbox = sandbox; //Set sandbox mode here
        SetMenuComponent(Component.city);
    }

    public void PreviousMenu()
    {
        SetMenuComponent(previousComponent);
    }

    public void LoadTip()
    {
        if(Toolbox.Instance.allDDSTrees.ContainsKey(loadingTipsDDSTree))
        {
            DDSSaveClasses.DDSTreeSave tree = Toolbox.Instance.allDDSTrees[loadingTipsDDSTree];

            DDSSaveClasses.DDSMessageSettings randomM = tree.messages[Toolbox.Instance.Rand(0, tree.messages.Count, definitelyNotPartOfCityGeneration: true)];

            tipText.text = Strings.GetTextForComponent(randomM.msgID, null, linkSetting: Strings.LinkSetting.forceNoLinks);

            LoadingTip newTip = loadingTips[Toolbox.Instance.Rand(0, MainMenuController.Instance.loadingTips.Count, definitelyNotPartOfCityGeneration: true)];
            //tipText.text = Strings.Get("ui.tips", newTip.dictRef);
            tipImg.sprite = newTip.image;
            tipImg.color = Color.white;
            nextTipTimer = 8f;
        }
    }

    public void ShadowsWebsiteLink()
    {
        Application.OpenURL("http://shadows.game");
    }

    public void OnEffectStatusChange()
    {
        SetStatusEffectOptionsAccordingToDropdown();
    }

    public void SetStatusEffectOptionsAccordingToDropdown()
    {
        //All
        if(statusEffectsDropdown.dropdown.value == 0)
        {
            foreach(ToggleController tc in statusEffectToggles)
            {
                tc.SetOn();
            }
        }
        //+ only
        else if (statusEffectsDropdown.dropdown.value == 1)
        {
            foreach (ToggleController tc in statusEffectToggles)
            {
                if(tc.playerPrefsID == "energizedStatus" || tc.playerPrefsID == "hydratedStatus" || tc.playerPrefsID == "focusedStatus" || tc.playerPrefsID == "wellRestedStatus")
                {
                    tc.SetOn();
                }
                else tc.SetOff();
            }
        }
        //- only
        else if (statusEffectsDropdown.dropdown.value == 2)
        {
            foreach (ToggleController tc in statusEffectToggles)
            {
                if (tc.playerPrefsID == "energizedStatus" || tc.playerPrefsID == "hydratedStatus" || tc.playerPrefsID == "focusedStatus" || tc.playerPrefsID == "wellRestedStatus")
                {
                    tc.SetOff();
                }
                else tc.SetOn();
            }
        }
        //None
        else if (statusEffectsDropdown.dropdown.value == 3)
        {
            foreach (ToggleController tc in statusEffectToggles)
            {
                tc.SetOff();
            }
        }

        //Other = custom
    }

    public void SetDropdownAccordingToStatusEffects()
    {
        //All on
        bool allOn = true;
        bool allOff = true;

        foreach (ToggleController tc in statusEffectToggles)
        {
            if(tc.isOn)
            {
                if (allOff) allOff = false;
            }
            else
            {
                if (allOn) allOn = false;
            }
        }

        if(allOn)
        {
            statusEffectsDropdown.dropdown.SetValueWithoutNotify(0);
        }
        else if(allOff)
        {
            statusEffectsDropdown.dropdown.SetValueWithoutNotify(3);
        }
        else
        {
            statusEffectsDropdown.dropdown.SetValueWithoutNotify(4);
        }
    }

    public void ResetControls()
    {
        Rewired.Demos.SimpleControlRemappingSOD.Instance.ResetControls();
        SetMenuComponent(Component.settings);
    }
}
