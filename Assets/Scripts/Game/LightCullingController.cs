using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class LightCullingController : MonoBehaviour
{
    //A class that manually loops through all visible lights in an attempt to cull any we don't need (experimental)
    [Header("Settings")]
    public int lightsToCheckPerFrame = 10;

    private List<LightController> lightsToCheck = new List<LightController>();

    // Update is called once per frame
    void Update()
    {
        int checkPerFrame = lightsToCheckPerFrame;

        if(lightsToCheck.Count <= 0)
        {
            foreach(NewRoom r in CityData.Instance.visibleRooms)
            {

            }
        }

        while(checkPerFrame > 0 && lightsToCheck.Count > 0)
        {

        }
    }


}
