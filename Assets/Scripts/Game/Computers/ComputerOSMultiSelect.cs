﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ComputerOSMultiSelect : MonoBehaviour
{
    public ComputerController controller;
    public GameObject elementPrefab;
    public List<ComputerOSMultiSelectElement> options = new List<ComputerOSMultiSelectElement>();
    public RectTransform elementParent;
    public ComputerOSMultiSelectElement selected;
    public bool usePages = false;
    public int page = 0;
    public int maxPerPage = 999;
    public List<OSMultiOption> allOptions;

    public delegate void NewSelection();
    public event NewSelection OnNewSelection;

    public delegate void ChangePage();
    public event ChangePage OnChangePage;

    [System.Serializable]
    public class OSMultiOption
    {
        public string text;
        public Human human;
        public StateSaveData.MessageThreadSave msgThread;
        public int msgIndex;
        [System.NonSerialized]
        public Company.SalesRecord salesRecord;
    }

    public void Setup(ComputerController newComp)
    {
        controller = newComp;
    }

    public void UpdateElements(List<OSMultiOption> newOptions)
    {
        allOptions = newOptions;

        SpawnList();
    }

    private void SpawnList()
    {
        foreach (ComputerOSMultiSelectElement el in options)
        {
            Destroy(el.gameObject);
        }

        options.Clear();

        float yPos = 0f;

        int optionStart = 0;
        int optionCount = allOptions.Count;

        if(usePages)
        {
            optionStart = page * maxPerPage;
            optionCount = optionStart + maxPerPage;
        }

        for (int i = optionStart; i < optionCount; i++)
        {
            if (i >= allOptions.Count) break;
            OSMultiOption opt = allOptions[i];

            GameObject newEl = Instantiate(elementPrefab, elementParent);
            ComputerOSMultiSelectElement e = newEl.GetComponent<ComputerOSMultiSelectElement>();
            e.Setup(opt, this);
            e.rect.anchoredPosition = new Vector2(0, yPos);
            yPos -= e.rect.sizeDelta.y;

            options.Add(e);
        }
    }

    public void NextPage(int newPage)
    {
        page = page + newPage;

        //Clamp page
        page = Mathf.Clamp(page, 0, Mathf.FloorToInt(allOptions.Count / maxPerPage));

        SpawnList();

        //Fire event
        if(OnChangePage != null)
        {
            OnChangePage();
        }
    }

    public void SetSelected(ComputerOSMultiSelectElement newSelection)
    {
        selected = newSelection;

        foreach (ComputerOSMultiSelectElement el in options)
        {
            el.selected = false;
            el.backgroundImage.color = el.backgroundColourNormal;
        }

        if(selected != null)
        {
            selected.selected = true;
            selected.backgroundImage.color = selected.backgroundColourSelected;
            selected.multiSelect.selected = selected;
        }

        //Fire event
        if (OnNewSelection != null)
        {
            OnNewSelection();
        }
    }
}
