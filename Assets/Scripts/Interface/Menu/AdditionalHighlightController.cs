﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdditionalHighlightController : MonoBehaviour
{
    public RectTransform rect;
    public AnimationCurve curve;
    public float speed = 1f;
    public float val = 0f;

    // Update is called once per frame
    void Update()
    {
        val += speed * Time.deltaTime;
        float scale = curve.Evaluate(val);
        rect.localScale = new Vector3(scale, scale, scale);
    }
}
