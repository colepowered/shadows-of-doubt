﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems; //required for Event data

public class CanvasMouseOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    //Singleton pattern
    private static CanvasMouseOver _instance;
    public static CanvasMouseOver Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public GameObject currentHover;

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerCurrentRaycast.gameObject != null)
        {
            //Game.Log("Mouse Over: " + eventData.pointerCurrentRaycast.gameObject.name);
            currentHover = eventData.pointerCurrentRaycast.gameObject;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;
        currentHover = null;
    }

    //void Update()
    //{
    //    if (currentHover)
    //        Game.Log(currentHover.name + " @ " + Input.mousePosition);
    //}
}
