﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "door_data", menuName = "Database/Door Preset")]

public class DoorPreset : SoCustomComparison
{
    public enum LockType { none, key, keypad};

    [Header("Visuals")]
    public GameObject doorModel;
    public InteractablePreset objectPreset;
    public GameObject handleModel;
    public InteractablePreset handlePreset;
    public Vector3 handleOffset = new Vector3(-1.25f, 1.05f, -0.05f);
    public bool isTransparent = false;

    [Header("Signs")]
    public Vector3 doorSignOffset = new Vector3(-0.68f, 1.8f, -0.1f);
    [ReorderableList]
    public List<DoorSign> doorSigns = new List<DoorSign>();

    [System.Serializable]
    public class DoorSign
    {
        public List<GameObject> signagePool = new List<GameObject>();
        public List<RoomConfiguration> ifEntranceToRoom = new List<RoomConfiguration>();
        public bool placeIfFromPublicArea = true;
        public bool placeIfFromOutside = false;
        public bool placeIfFromInside = false;
    }

    [Header("Decor Settings")]
    public bool inheritColouringFromDecor = false;
    [Tooltip("If true the same material colours will be shared over all instances of this furniture for the room")]
    public FurniturePreset.ShareColours shareColours = FurniturePreset.ShareColours.none;
    public List<MaterialGroupPreset.MaterialVariation> variations = new List<MaterialGroupPreset.MaterialVariation>();

    [Header("Behaviour")]
    [Tooltip("How fast the door opens and closes")]
    public float doorOpenSpeed = 1.47f;
    [Tooltip("The maximum amount this door can open")]
    public float openAngle = 89.9f;
    [Tooltip("Can the player peek underneath this door?")]
    public bool canPeakUnderneath = false;
    public enum ClosingBehaviour { nothing, closeOnCull, closeOnDespawn};
    [Tooltip("If open, close the door depending on this behaviour")]
    public ClosingBehaviour closeBehaviour = ClosingBehaviour.nothing;

    [Header("Lock")]
    public LockType lockType = LockType.key;
    [Tooltip("If the above is set to something other than none or key, then setup this lock interactable...")]
    public InteractablePreset lockInteractable;
    public Vector3 lockOffsetFront = new Vector3(-1.25f, 1.5f, 0f);
    public Vector3 lockOffsetRear = new Vector3(-1.25f, 1.5f, -0.1f);
    [Tooltip("The lock is armed when the door movement is closed")]
    public bool armLockOnClose = true;
    [Tooltip("The door strength range")]
    [MinMaxSlider(0f, 1f)]
    public Vector2 doorStrengthRange = new Vector2(0.1f, 0.2f);
    [Tooltip("The lock strength range")]
    [MinMaxSlider(0f, 1f)]
    public Vector2 lockStrengthRange = new Vector2(0.1f, 0.2f);

    [Header("Audio")]
    public AudioEvent audioOpen;
    public AudioEvent audioClose;
    public AudioEvent audioCloseAction;
    public AudioEvent audioLock;
    public AudioEvent audioUnlock;
    public AudioEvent audioLockedEntryAttempt;

    public AudioEvent audioKnockLight;
    public AudioEvent audioKnockMed;
    public AudioEvent audioKnockHeavy;
}
