﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using NaughtyAttributes;
using FMOD.Studio;

public class LightController : MonoBehaviour
{
    public bool isSetup = false;

    [Header("Location")]
    public NewRoom room;
    [System.NonSerialized]
    public Interactable interactable;

    [Header("Profile")]
    public LightingPreset preset;

    [Header("Light")]
    public bool isOn = true;
    public bool isUnscrewed = false;
    public bool closedBreaker = false;
    public bool isCulled = false;
    public float lightState = 1f;
    public Light lightComponent; //The main area illuminator and shadow caster
    public HDAdditionalLightData hdrpLightData;
    [Space(7)]
    [Tooltip("Colour of the light")]
    public Color lightColour;
    [Tooltip("If flicker is present, flicker to this colour")]
    private Color flickerColour;
    [Tooltip("The model's emissive colour (instaned materials only)")]
    private Color emissionColour;
    [Tooltip("Intensity of the light")]
    public float intensity = 1f;

    //A special copy of the light set to the street light layer, designed to give window shadows
    //[Header("Exterior Shadow Light")]
    //public Light exteriorShadowLight;
    //public HDAdditionalLightData exteriorShadowLightHdrpData;

    [Header("Model")]
    [Tooltip("Change material on this model")]
    public MeshRenderer rend;
    [Tooltip("The material of the parent model: For altering emission")]
    public Material mat;

    [Header("Voumetrics")]
    public bool useVolumetrics = false;

    [Header("Shadows")]
    public bool useShadows = false;

    [Header("Flicker")]
    [Tooltip("Does this light flicker?")]
    public bool flicker = false;
    [Tooltip("When flickering, use this multiplier on the flicker colour to determin the actual colour (basically a darker version of flicker colour)")]
    public float flickerColourMultiplier = 0f;
    public float pulseSpeed = 1f;
    private float flickerState = 1f;
    private bool flickerSwitch = false;
    private bool flickerInterval = false;
    private float interval = 0f;
    private float intervalTime = 0f;

    [Header("Ceiling Fan")]
    public Transform ceilingFan;
    public bool ceilingFanOn = true;
    public float ceilingFanSpeed = 0f;

    //Generate from profile settings
    public void Setup(NewRoom newRoom, Interactable newInteractable, Interactable.LightConfiguration configData, LightingPreset newPreset, int lightZoneSize = -1, Transform newCeilingFan = null)
    {
        preset = newPreset;
        room = newRoom;
        interactable = newInteractable;
        ceilingFan = newCeilingFan;

        //Get references if needed
        if (lightComponent == null) lightComponent = this.gameObject.GetComponentInChildren<Light>();
        if (hdrpLightData == null) hdrpLightData = this.gameObject.GetComponentInChildren<HDAdditionalLightData>();
        if (rend == null) rend = this.transform.parent.GetComponent<MeshRenderer>();

        if (mat == null && rend != null)
        {
            if(preset.useInstancedEmissive)
            {
                Material instMat = Instantiate(rend.sharedMaterial);
                instMat.name = rend.sharedMaterial + " [Instanced by Light]";
                mat = instMat;
                rend.sharedMaterial = instMat;
                MaterialsController.Instance.lightMaterialInstances++;
            }
            else mat = rend.sharedMaterial;
        }

        string randomKey = string.Empty;
        if (interactable != null) randomKey = interactable.seed;
        else randomKey = Toolbox.Instance.SeedRand(0, 9999999).ToString();

        //Generate new config data if needed...
        if(configData == null)
        {
            configData = new Interactable.LightConfiguration();

            Color chosenColour = Color.white;

            //Decide colour
            Color col1 = Color.Lerp(preset.coolColours[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.coolColours.Count, randomKey, out randomKey)].colourOne,
            preset.warmColours[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.warmColours.Count, randomKey, out randomKey)].colourOne, Toolbox.Instance.GetPsuedoRandomNumberContained(0.33f, 0.66f, randomKey, out randomKey));

            Color col2 = Color.Lerp(preset.coolColours[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.coolColours.Count, randomKey, out randomKey)].colourTwo,
            preset.warmColours[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.warmColours.Count, randomKey, out randomKey)].colourTwo, Toolbox.Instance.GetPsuedoRandomNumberContained(0.33f, 0.66f, randomKey, out randomKey));

            configData.colour = Color.Lerp(col1, col2, 0.5f);

            configData.range = preset.defaultRange;

            //Get intensity
            float intense = preset.defaultIntensity;

            float intensityMP = 1f;

            if(room != null)
            {
                if (room.lowerRoom != null) intensityMP = 1.33f;

                if (lightZoneSize < 0)
                {
                    intense = (room.nodes.Count * intensityMP) * 100f - 50f;
                }
                else
                {
                    intense = (lightZoneSize * intensityMP) * 100f - 50f;
                }

                if (room.preset.wellLit)
                {
                    intense *= 1.5f;
                    configData.range *= 3f;
                }
            }

            configData.intensity = Mathf.Clamp(intense, preset.intensityRange.x, preset.intensityRange.y);

            configData.flickerColourMultiplier = Toolbox.Instance.GetPsuedoRandomNumberContained(preset.flickerMultiplierRange.x, preset.flickerMultiplierRange.y, randomKey, out randomKey);
            configData.pulseSpeed = Toolbox.Instance.GetPsuedoRandomNumberContained(preset.flickerPulseRange.x, preset.flickerPulseRange.y, randomKey, out randomKey);
            configData.intervalTime = Toolbox.Instance.GetPsuedoRandomNumberContained(preset.flickerNormalityIntervalRange.x, preset.flickerNormalityIntervalRange.y, randomKey, out randomKey);

            //Setup flicker
            float chanceExtra = 0f;

            if(room != null)
            {
                if(room.gameLocation.floor != null)
                {
                    if(room.gameLocation.floor.floor <= CityControls.Instance.lowestFloor)
                    {
                        chanceExtra = CityControls.Instance.lowestFloorIncreaseFlickerChance;
                    }
                }
            }

            if (preset.chanceOfFlicker + chanceExtra >= Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, randomKey, out randomKey))
            {
                configData.flicker = true;
            }
            else
            {
                configData.flicker = false;
            }
        }

        //Set to correct light layer
        if (room == null || room.building == null)
        {
            //Set to street layer (1)
            hdrpLightData.lightlayersMask = LightLayerEnum.LightLayer1;
        }
        else if(room.building != null)
        {
            //Set to building's light layer
            if(room.building.interiorLightCullingLayer == 0)
            {
                hdrpLightData.lightlayersMask = LightLayerEnum.LightLayer2;
            }
            else if (room.building.interiorLightCullingLayer == 1)
            {
                hdrpLightData.lightlayersMask = LightLayerEnum.LightLayer3;
            }
            else if (room.building.interiorLightCullingLayer == 2)
            {
                hdrpLightData.lightlayersMask = LightLayerEnum.LightLayer4;
            }
            else if (room.building.interiorLightCullingLayer == 3)
            {
                hdrpLightData.lightlayersMask = LightLayerEnum.LightLayer5;
            }

            //Add street layer mask if on ground floor / outside or forced
            if((room.floor != null && room.floor.floor == 0) || (interactable != null && interactable.preset.forceIncludeOnStreetLightLayer) || room.IsOutside())
            {
                hdrpLightData.lightlayersMask = (LightLayerEnum)Toolbox.Instance.CreateLayerMask(Toolbox.LayerMaskMode.onlyCast, room.building.interiorLightCullingLayer + 2, 1);
            }

            //Add to building's interior light list
            room.building.allInteriorMainLights.Add(this);
        }

        if(transform.parent != null)
        {
            if(transform.parent.gameObject.layer != 23 && transform.gameObject.layer != 23 && interactable != null)
            {
                Game.Log("Misc Error: Light fitting " + transform.parent.name + " (" + interactable.name + ") is not on layer 23, and therefore might not work properly with light level raycasting...");
            }
        }

        if(SessionData.Instance.isTestScene)
        {
            hdrpLightData.lightlayersMask = LightLayerEnum.Everything;
        }

        SetShadows(preset.enableShadows);
        SetVolumetrics(preset.enableVolumetrics);

        //Set shadow tint
        if (preset.enableShadows && room != null && room.preset.baseLightingShadowTint)
        {
            hdrpLightData.shadowTint = room.GetShadowTint(configData.colour, room.preset.baseLightingShadowTintIntensity);
        }

        if(room != null)
        {
            if(!room.IsOutside())
            {
                hdrpLightData.penumbraTint = true;
            }
        }

        //Set cull distance
        hdrpLightData.fadeDistance = preset.fadeDistance * Game.Instance.lightFadeDistanceMultiplier;

        isOn = lightComponent.enabled;
        ceilingFanOn = lightComponent.enabled;

        //Setup window shadow light
        //if(preset.enableExteriorLight)
        //{
        //    if (room != null)
        //    {
        //        //Entrance to street in this room...
        //        if (room.entrances.Exists(item => item.toNode.gameLocation.thisAsStreet != null))
        //        {
        //            if (exteriorShadowLight == null)
        //            {
        //                GameObject newShadowLight = Instantiate(PrefabControls.Instance.exteriorShadowLight, this.transform);
        //                newShadowLight.transform.localPosition = Vector3.zero;

        //                if (exteriorShadowLight == null) exteriorShadowLight = newShadowLight.GetComponent<Light>();

        //                if (exteriorShadowLightHdrpData == null)
        //                {
        //                    exteriorShadowLightHdrpData = newShadowLight.GetComponent<HDAdditionalLightData>();
        //                    exteriorShadowLightHdrpData.intensity = hdrpLightData.intensity; //Match intensity of original light
        //                }
        //            }
        //        }
        //    }
        //}

        SetColour(configData.colour);

        SetIntensity(configData.intensity);
        lightComponent.range = configData.range;

        //Add to light culling
        if(preset.cullMode == LightingPreset.CullMode.occlusion || (Game.Instance.overrideLightCullingMode && Game.Instance.lightCullOverride == LightingPreset.CullMode.occlusion))
        {
            if(room != null)
            {
                if(!room.lightCulling.Contains(this))
                {
                    room.lightCulling.Add(this);
                }
            }
        }

        //Flicker variables
        flickerColourMultiplier = configData.flickerColourMultiplier;
        pulseSpeed = configData.pulseSpeed;
        intervalTime = configData.intervalTime;
        SetFlicker(configData.flicker);

        //Set on off
        //SetOn(preset.onByDefault, true);

        if(preset.onByDefault)
        {
            SetOn(true, true);
        }
        else if(interactable.preset.lightswitch == InteractablePreset.Switch.switchState) SetOn(interactable.sw0, true);
        else if (interactable.preset.lightswitch == InteractablePreset.Switch.custom1) SetOn(interactable.sw1, true);
        else if (interactable.preset.lightswitch == InteractablePreset.Switch.custom2) SetOn(interactable.sw2, true);
        else if (interactable.preset.lightswitch == InteractablePreset.Switch.custom3) SetOn(interactable.sw3, true);

        //Setup atrium settings
        if (preset.isAtriumLight)
        {
            float heightToFloor = PathFinder.Instance.tileSize.z;

            Vector3Int searchNode = Vector3Int.zero;

            for (int i = 1; i < 50; i++)
            {
                searchNode = interactable.node.nodeCoord - new Vector3Int(0, 0, i);
                NewNode foundNode = null;

                if (PathFinder.Instance.nodeMap.TryGetValue(searchNode, out foundNode))
                {
                    if (foundNode.floorType != NewNode.FloorTileType.floorOnly && foundNode.floorType != NewNode.FloorTileType.floorAndCeiling)
                    {
                        heightToFloor += PathFinder.Instance.tileSize.z;
                    }
                    else break;
                }
                else break;
            }

            //Remove one instance of the max height to stop bulbs spawning too close to the floor
            //heightToFloor -= preset.heightInterval;

            //Spawn lights
            float intervalCursor = preset.heightInterval;
            GameObject lastBulb = null;
            float lastBulbHeight = 0f;

            while(intervalCursor < heightToFloor)
            {
                if(preset.bulbPrefab != null)
                {
                    GameObject pref = preset.bulbPrefab;

                    //Use last
                    if(intervalCursor + preset.heightInterval >= heightToFloor && preset.endBulbPrefab != null)
                    {
                        pref = preset.endBulbPrefab;
                    }

                    lastBulb = Instantiate(pref, this.transform.parent);
                    lastBulb.transform.position = interactable.wPos - new Vector3(0, intervalCursor, 0);
                    Toolbox.Instance.SetLightLayer(lastBulb, room.building, false);

                    if (preset.cablePrefab != null)
                    {
                        GameObject newCable = Instantiate(preset.cablePrefab, lastBulb.transform);
                        newCable.transform.localEulerAngles = Vector3.zero;
                        newCable.transform.localScale = new Vector3(newCable.transform.localScale.x, preset.heightInterval, newCable.transform.localScale.z);
                        newCable.transform.localPosition = new Vector3(0, preset.heightInterval * 0.5f, 0);

                        Toolbox.Instance.SetLightLayer(newCable, room.building, false);
                    }
                }

                lastBulbHeight = intervalCursor;
                intervalCursor += preset.heightInterval;
            }

            //Set length of tube light
            hdrpLightData.shapeWidth = lastBulbHeight;
            this.transform.localPosition = new Vector3(0, -lastBulbHeight * 0.5f - 5f, 0); //Move the light down slightly to cover the bottom floor

            //Set cull distance
            hdrpLightData.fadeDistance = Mathf.Max(preset.fadeDistance * Game.Instance.lightFadeDistanceMultiplier, hdrpLightData.shapeWidth);
        }

        isSetup = true;
    }

    //Update fade distances; for when fade distance multipliers are changed
    public void UpdateFadeDistances()
    {
        if(preset != null && hdrpLightData != null)
        {
            //Set cull distance
            hdrpLightData.fadeDistance = preset.fadeDistance * Game.Instance.lightFadeDistanceMultiplier;
            hdrpLightData.volumetricFadeDistance = preset.fadeDistance * Game.Instance.lightFadeDistanceMultiplier;

            if (preset.isAtriumLight)
            {
                //Set cull distance
                hdrpLightData.fadeDistance = Mathf.Max(preset.fadeDistance * Game.Instance.lightFadeDistanceMultiplier, hdrpLightData.shapeWidth);
            }

            if(useShadows)
            {
                hdrpLightData.SetShadowFadeDistance(preset.shadowFadeDistance * Game.Instance.shadowFadeDistanceMultiplier);
            }
        }
    }

    //Set the colour to this
    public void SetColour(Color newCol)
    {
        lightColour = newCol;
        if(lightComponent != null) lightComponent.color = lightColour;
        flickerColour = lightColour * flickerColourMultiplier;

        //if (exteriorShadowLight != null) exteriorShadowLight.color = lightColour;

        //Set emission colour to the point light colour
        emissionColour = lightColour;
        emissionColour.a = preset.emissionMultiplier;

        //Create instanced material
        if (mat != null && preset.useInstancedEmissive)
        {
            mat.SetColor("_EmissiveColor", emissionColour);
        }
    }

    //Set the intensity to this
    public void SetIntensity(float newInt)
    {
        intensity = newInt * CityControls.Instance.weatherSettings.globalLightIntensityMultiplier;

        if(room != null)
        {
            if(room.floor != null)
            {
                if(room.floor.floor <= CityControls.Instance.lowestFloor)
                {
                    intensity *= CityControls.Instance.lowestFloorLightMultiplier;
                }
            }
        }

        hdrpLightData.intensity = intensity;
    }

    // Set light to use shadows
    public void SetShadows(bool val)
    {
        useShadows = val;
        hdrpLightData.EnableShadows(useShadows);

        if (useShadows)
        {
            hdrpLightData.linkShadowLayers = true;

            hdrpLightData.SetShadowResolutionLevel((int)preset.resolution);
            hdrpLightData.SetShadowFadeDistance(preset.shadowFadeDistance * Game.Instance.shadowFadeDistanceMultiplier);

            if(Game.Instance.overrideLightControllerShadowMode)
            {
                if (Game.Instance.shadowModeOverride == LightingPreset.ShadowMode.everyFrame)
                {
                    hdrpLightData.SetShadowUpdateMode(ShadowUpdateMode.EveryFrame);
                }
                else if (Game.Instance.shadowModeOverride == LightingPreset.ShadowMode.onDemand)
                {
                    hdrpLightData.SetShadowUpdateMode(ShadowUpdateMode.OnDemand);
                }
                else if (Game.Instance.shadowModeOverride == LightingPreset.ShadowMode.onEnable)
                {
                    hdrpLightData.SetShadowUpdateMode(ShadowUpdateMode.OnEnable);
                }
                else if (Game.Instance.shadowModeOverride == LightingPreset.ShadowMode.dynamicSystemStatic)
                {
                    hdrpLightData.SetShadowUpdateMode(ShadowUpdateMode.OnDemand);
                }
                else if (Game.Instance.shadowModeOverride == LightingPreset.ShadowMode.dynamicSystemSlowerUpdate)
                {
                    hdrpLightData.SetShadowUpdateMode(ShadowUpdateMode.OnDemand);

                    if(!CityData.Instance.dynamicShadowSystemLights.Contains(this))
                    {
                        CityData.Instance.dynamicShadowSystemLights.Add(this);
                    }
                }
            }
            else
            {
                if(preset.shadowMode == LightingPreset.ShadowMode.everyFrame)
                {
                    hdrpLightData.SetShadowUpdateMode(ShadowUpdateMode.EveryFrame);
                }
                else if(preset.shadowMode == LightingPreset.ShadowMode.onDemand)
                {
                    hdrpLightData.SetShadowUpdateMode(ShadowUpdateMode.OnDemand);
                }
                else if(preset.shadowMode == LightingPreset.ShadowMode.onEnable)
                {
                    hdrpLightData.SetShadowUpdateMode(ShadowUpdateMode.OnEnable);
                }
                else if(preset.shadowMode == LightingPreset.ShadowMode.dynamicSystemStatic)
                {
                    hdrpLightData.SetShadowUpdateMode(ShadowUpdateMode.OnDemand);
                }
                else if (preset.shadowMode == LightingPreset.ShadowMode.dynamicSystemSlowerUpdate)
                {
                    hdrpLightData.SetShadowUpdateMode(ShadowUpdateMode.OnDemand);

                    if (!CityData.Instance.dynamicShadowSystemLights.Contains(this))
                    {
                        CityData.Instance.dynamicShadowSystemLights.Add(this);
                    }
                }
            }
        }
    }

    public void SetVolumetrics(bool val)
    {
        //TODO: Can't find the script toggle for this
        useVolumetrics = val;

        hdrpLightData.volumetricFadeDistance = preset.fadeDistance * Game.Instance.lightFadeDistanceMultiplier;

        //Set the atmosphere: Use the atmosphere value from the room preset, then multiply by the light preset
        float roomValue = 1f;
        if (room != null) roomValue = room.preset.baseRoomAtmosphere;
        SetVolumentricAtmosphere(roomValue * preset.atmosphereMultiplier);
    }

    //Set the volumetric atmopshere
    public void SetVolumentricAtmosphere(float newVal)
    {
        hdrpLightData.volumetricDimmer = Mathf.Clamp01(newVal);
        hdrpLightData.volumetricFadeDistance = preset.defaultRange;
    }

    public void SetFlicker(bool val)
    {
        flicker = val;

        if(flicker)
        {
            //If light is on, enable the container
            if(isOn)
            {
                this.gameObject.SetActive(true);
            }

            this.enabled = true;
        }
        else
        {
            flickerState = 1f;

            if (isOn && lightState == 1f || !isOn && lightState == 0f)
            {
                this.enabled = false;

                //If light is off, disable container
                if (!isOn && lightState == 0f)
                {
                    this.gameObject.SetActive(false);
                }
            }
        }
    }

    private void Update()
    {
        //Fade on/off
        if(isOn && lightState < 1f)
        {
            lightState += Time.deltaTime * SessionData.Instance.currentTimeMultiplier * preset.fadeSpeed;
            lightState = Mathf.Clamp01(lightState);
        }
        else if(!isOn && lightState > 0f)
        {
            lightState -= Time.deltaTime * SessionData.Instance.currentTimeMultiplier * preset.fadeSpeed;
            lightState = Mathf.Clamp01(lightState);
        }

        if (ceilingFan != null)
        {
            if (SessionData.Instance.play)
            {
                if (ceilingFanOn)
                {
                    if(ceilingFanSpeed < InteriorControls.Instance.ceilingFanSpeed)
                    {
                        ceilingFanSpeed += 1f * Time.deltaTime;
                    }
                }
                else
                {
                    if (ceilingFanSpeed > 0)
                    {
                        ceilingFanSpeed -= 1f * Time.deltaTime;
                    }
                }

                ceilingFanSpeed = Mathf.Clamp(ceilingFanSpeed, 0f, InteriorControls.Instance.ceilingFanSpeed);

                ceilingFan.transform.localEulerAngles = new Vector3(0f, ceilingFan.transform.localEulerAngles.y + ceilingFanSpeed, 0f);
            }
        }

        if(flicker && isOn)
        {
            if(SessionData.Instance.play)
            {
                if (Game.Instance.flickeringLights) interval += Time.deltaTime * SessionData.Instance.currentTimeMultiplier;
                else interval = 0;

                if (!flickerInterval && interval >= intervalTime)
                {
                    interval = 0f;
                    flickerInterval = true;
                    intervalTime = Toolbox.Instance.Rand(preset.flickerIntervalRange.x, preset.flickerIntervalRange.y);
                }
                else if (flickerInterval && interval >= intervalTime)
                {
                    interval = 0f;
                    flickerInterval = false;
                    intervalTime = Toolbox.Instance.Rand(preset.flickerNormalityIntervalRange.x, preset.flickerNormalityIntervalRange.y);
                }

                if (flickerState < 1f && flickerSwitch)
                {
                    flickerState += Time.deltaTime * pulseSpeed;
                }
                else if (flickerState >= 1f && flickerSwitch)
                {
                    flickerSwitch = false;

                    //Flicker variables
                    flickerColourMultiplier = Toolbox.Instance.Rand(preset.flickerMultiplierRange.x, preset.flickerMultiplierRange.y);
                    pulseSpeed = Toolbox.Instance.Rand(preset.flickerPulseRange.x, preset.flickerPulseRange.y);
                }

                if (flickerInterval && flickerState > 0f && !flickerSwitch)
                {
                    flickerState -= Time.deltaTime * pulseSpeed;
                }
                else if (flickerState <= 0f && !flickerSwitch)
                {
                    flickerSwitch = true;

                    //Flicker variables
                    flickerColourMultiplier = Toolbox.Instance.Rand(preset.flickerMultiplierRange.x, preset.flickerMultiplierRange.y);
                }

                flickerState = Mathf.Clamp01(flickerState);
                flickerColour = lightColour * flickerColourMultiplier;

                UpdateLight();
            }
        }
        else
        {
            UpdateLight();

            //Disable
            if ((isOn && lightState >= 1f) || (!isOn && lightState <= 0f))
            {
                if (ceilingFan == null || (!ceilingFanOn && ceilingFanSpeed == 0f))
                {
                    this.enabled = false; //Safely disable

                    if (!isOn && lightState <= 0f)
                    {
                        //If off, safely disable container (including light)
                        this.gameObject.SetActive(false);
                    }
                }
            }
        }
    }

    public void SetOn(bool val, bool forceInstant = false)
    {
        isOn = val;
        ceilingFanOn = isOn; //For the moment tie this to the light value

        if (!preset.fadeOnOff || forceInstant)
        {
            //Skip to the correct light state
            if (isOn)
            {
                lightState = 1f;
            }
            else
            {
                lightState = 0f;
            }

            UpdateLight();

            if (!flicker)
            {
                if(isOn)
                {
                    //Can disable this script but not the gameobject
                    if(ceilingFan == null)
                    {
                        this.enabled = false;
                    }

                    if (!this.gameObject.activeSelf) this.gameObject.SetActive(true);
                }
                else
                {
                    //If off, safely disable the container object.
                    if(ceilingFan == null)
                    {
                        this.enabled = false;
                        this.gameObject.SetActive(false);
                    }
                }
            }
            else
            {
                if (!this.gameObject.activeSelf) this.gameObject.SetActive(true);
                this.enabled = true;
            }
        }

        //On material
        if((preset.useOnMaterial != null || preset.useBroadcastMaterial) && rend != null)
        {
            if (isOn)
            {
                if (preset.useBroadcastMaterial)
                {
                    rend.sharedMaterial = SessionData.Instance.broadcastMaterialInstanced;
                }
                else if(preset.useOnMaterial != null)
                {
                    rend.sharedMaterial = preset.useOnMaterial;
                }
            }
            else
            {
                rend.sharedMaterial = mat;
            }
        }

        if ((preset.fadeOnOff && !forceInstant) || ceilingFan != null)
        {
            //This needs to be active to fade on/off light
            this.enabled = true;
            this.gameObject.SetActive(true);
        }
    }

    public void SetUnscrewed(bool val, bool forceInstance = false)
    {
        if (interactable != null && !interactable.preset.allowUnscrewed) val = false;
        isUnscrewed = val;
        UpdateLight();
    }

    public void SetClosedBreaker(bool val, bool forceInstance = false)
    {
        closedBreaker = val;
        UpdateLight();
    }

    private void OnEnable()
    {
        if(isSetup)
        {
            UpdateLight();
        }
    }

    private void OnDisable()
    {
        if (isSetup)
        {
            UpdateLight();
        }
    }

    //Update light
    public void UpdateLight()
    {
        if(lightComponent == null)
        {
            this.transform.name = Toolbox.Instance.GenerateUniqueID();
            lightComponent = this.gameObject.GetComponent<Light>();

            if(lightComponent == null)
            {
                Game.Log("Unable to find light component on " + transform.name);
            }
            else Game.Log("Had to assign Light Component on " + transform.name);
        }

        if (hdrpLightData == null)
        {
            this.transform.name = Toolbox.Instance.GenerateUniqueID();
            hdrpLightData = this.gameObject.GetComponent<HDAdditionalLightData>();

            if (hdrpLightData == null)
            {
                Game.Log("Unable to find light component on " + transform.name);
            }
            else Game.Log("Had to assign HDAdditionalLightData on " + transform.name);
        }

        if (isUnscrewed || closedBreaker)
        {
            hdrpLightData.intensity = 0f;
            //if (exteriorShadowLightHdrpData != null) exteriorShadowLight.intensity = 0f;
        }
        else
        {
            if (flicker && Game.Instance.flickeringLights)
            {
                lightComponent.color = Color.Lerp(flickerColour, lightColour, flickerState);
            }
            else
            {
                lightComponent.color = lightColour;
            }

            //For light on/off use intensity
            hdrpLightData.intensity = intensity * lightState;
            //if (exteriorShadowLightHdrpData != null) exteriorShadowLight.intensity = 1000f * lightState;

            //Set emission
            if (mat != null && preset.useInstancedEmissive)
            {
                Color tempEmColour = lightComponent.color;
                mat.SetColor("_EmissiveColor", tempEmColour * lightState);
            }

            //Set on/off material
            //if (preset.useOnMaterial != null && rend != null)
            //{
            //    if (lightState >= 0.5f)
            //    {
            //        rend.sharedMaterial = preset.useOnMaterial;
            //    }
            //    else
            //    {
            //        rend.sharedMaterial = mat;
            //    }
            //}
        }

        //Draw shadows
        if (useShadows && isOn && lightComponent.enabled && hdrpLightData != null)
        {
            if (preset.shadowMode == LightingPreset.ShadowMode.dynamicSystemStatic || preset.shadowMode == LightingPreset.ShadowMode.dynamicSystemSlowerUpdate)
            {
                hdrpLightData.RequestShadowMapRendering();
            }
        }
    }

    public void SetCulled(bool val)
    {
        isCulled = val;

        try
        {
            this.gameObject.SetActive(!isCulled);
        }
        catch
        {
            //Not sure why this would be failing tbh!
        }
    }

    [Button]
    public void CullToggle()
    {
        SetCulled(!isCulled);
    }

    private void OnDestroy()
    {
        if (preset.useInstancedEmissive && mat != null)
        {
            MaterialsController.Instance.lightMaterialInstances--;
            Destroy(mat); //Remove instanced material
        }
    }
}
