﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceDebug : Evidence
{
    public static int assignID = 0;
    public int id;

    //Constructor
    public EvidenceDebug(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        id = assignID;
        assignID++;
    }

    //Name
    public override string GenerateName()
    {
        return "Debug Evidence " + id.ToString();
    }
}
