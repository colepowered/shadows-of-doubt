﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceReceipt : Evidence
{
    public Company soldHere;
    public float purchasedTime;
    public EvidenceTime purchaseTimeEvidence;
    public Fact fromFact;

    public List<InteractablePreset> purchased = new List<InteractablePreset>();

    //Constructor
    public EvidenceReceipt(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        for (int i = 0; i < newPassedObjects.Count; i++)
        {
            object obj = newPassedObjects[i];

            Interactable.Passed passed = obj as Interactable.Passed; //Company is passed

            if (passed != null)
            {
                if(passed.varType == Interactable.PassedVarType.companyID)
                {
                    //Game.Log("Find address ID " + passed.value + "...");
                    soldHere = CityData.Instance.companyDirectory.Find(item => item.companyID == (int)passed.value); //Find address id
                    //if(soldHere != null) Game.Log("Found company " + soldHere.name);
                }
                else if(passed.varType == Interactable.PassedVarType.time)
                {
                    //Game.Log("Find time... " + passed.value);
                    purchasedTime = passed.value;
                }
                else if(passed.varType == Interactable.PassedVarType.stringInteractablePreset)
                {
                    //Game.Log("Find interactable preset... " + passed.str);
                    InteractablePreset ip = null;

                    if(Toolbox.Instance.objectPresetDictionary.TryGetValue(passed.str, out ip))
                    {
                        //Game.Log("Found purchased item " + ip.name);
                        purchased.Add(ip);
                    }
                }
            }

            Interactable inter = obj as Interactable;

            if (inter != null)
            {
                interactable = inter;
                interactablePreset = interactable.preset;
            }

            Company cc = obj as Company;

            if(cc != null)
            {
                soldHere = cc;
            }
        }
    }

    //Override data sources: This needs it's own, not the owners like in EvidenceItem
    public override void BuildDataSources()
    {
        //base.BuildDataSources();

        //Add from fact
        if (fromFact == null)
        {
            if(soldHere != null && soldHere.address != null) fromFact = EvidenceCreator.Instance.CreateFact("From", this, soldHere.address.evidenceEntry) as Fact;
        }
    }

    //Name: Use iteractable preset
    public override string GenerateName()
    {
        string ret = Strings.Get("evidence.names", preset.name);

        //If singleton then include purchase place name
        if (isFound && soldHere != null)
        {
            ret += " " + Strings.Get("evidence.names", "from") + " " + soldHere.name;
        }

        return ret;
    }

    //Set ineractable name to evidence name
    public override void OnDiscovery()
    {
        base.OnDiscovery();

        UpdateName();

        //Time evidence
        if (purchaseTimeEvidence == null)
        {
            purchaseTimeEvidence = EvidenceCreator.Instance.GetTimeEvidence(purchasedTime, purchasedTime, "PurchasedTime") as EvidenceTime;
        }

        //Update interactable name
        if (interactable != null)
        {
            //Game.Log("Interactable update name = " + GetNameForDataKey(DataKey.name));
            interactable.UpdateName(true, DataKey.name);
        }
    }
}
