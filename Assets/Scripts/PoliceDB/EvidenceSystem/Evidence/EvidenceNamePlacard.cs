﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceNamePlacard : Evidence
{
    //Constructor
    public EvidenceNamePlacard(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
    }

    public override void Compile()
    {
        if (belongsTo != null)
        {
            SetNote(Toolbox.Instance.allDataKeys, belongsTo.GetCitizenName() + "\n" + Strings.Get("jobs", belongsTo.job.preset.name));
        }
        else
        {
            Game.LogError("Name placard evidence has no owner");
        }

        base.Compile();
    }

    //Name: Use iteractable preset
    public override string GenerateName()
    {
        return belongsTo.GetCitizenName() + "\n" + Strings.Get("jobs", belongsTo.job.preset.name);
    }
}
