﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class ContextMenuPanelController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public RectTransform rect;

	private bool isOver = false;
	public ContextMenuController cmc;

    public List<ContextButtonController> spawnedButtons = new List<ContextButtonController>();

    public void Setup(ContextMenuController newController)
    {
        cmc = newController;
        rect = this.gameObject.GetComponent<RectTransform>();

        float spawnPositionY = -4;

        foreach (ContextMenuController.ContextMenuButtonSetup e in cmc.menuButtons)
        {
            //Check for disabled item
            if (cmc.disabledItems.Contains(e.commandString)) continue;
            if (e.devOnly && !Game.Instance.devMode) continue;

            GameObject newButton = Instantiate(PrefabControls.Instance.contextMenuButton, rect);
            ContextButtonController cbc = newButton.GetComponent<ContextButtonController>();
            cbc.Setup(cmc, this, e);
            spawnedButtons.Add(cbc);

            //Set position
            cbc.rect.anchoredPosition = new Vector2(0, spawnPositionY);
            spawnPositionY -= 34;

            //Set size of panel
            float width = InterfaceControls.Instance.contextMenuWidth;

            if(!cmc.useGlobalWidth)
            {
                width = cmc.width;
            }

            rect.sizeDelta = new Vector2(width, -spawnPositionY + 4);
        }

        for (int i = 0; i < spawnedButtons.Count; i++)
        {
            ContextButtonController b = spawnedButtons[i];
            b.RefreshAutomaticNavigation();

            //if(i > 0)
            //{
            //    Toolbox.Instance.AddNavigationInput(b.button, newUp: spawnedButtons[i - 1].button);
            //}

            //if(i < spawnedButtons.Count - 1)
            //{
            //    Toolbox.Instance.AddNavigationInput(b.button, newDown: spawnedButtons[i + 1].button);
            //}
        }

        if(!InputController.Instance.mouseInputMode && spawnedButtons.Count > 0)
        {
            if(InterfaceController.Instance.selectedElement != null)
            {
                InterfaceController.Instance.selectedElement.OnDeselect();
            }

            spawnedButtons[0].OnSelect();
        }
    }

	public void OnPointerEnter(PointerEventData eventData)
	{
        if(InputController.Instance.mouseInputMode)
        {
            isOver = true;
        }
	}

	public void OnPointerExit(PointerEventData eventData)
	{
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;

        if (InputController.Instance.mouseInputMode)
        {
            isOver = false;
        }
	}

	void Update ()
	{
        if (!isOver)
        {
            if (InputController.Instance.mouseInputMode && (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)))
            {
                cmc.ForceClose();
            }
        }

        if (cmc == null) Destroy(this.gameObject);
    }
}
