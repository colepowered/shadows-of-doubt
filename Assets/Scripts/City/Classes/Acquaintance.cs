﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
public class Acquaintance : IComparable<Acquaintance>
{
	public Human from;
	public Human with;
    public enum ConnectionType { friend, neighbor, housemate, lover, boss, workTeam, workOther, regularCustomer, regularStaff, familiarResidence, familiarWork, publicFigure, stranger, paramour, player, anyoneNotPlayer, friendOrWork, knowsName, anyAcquaintance, anyone, workNotBoss, relationshipMatch, corpDove, spamVmail, corpStarch, corpIndigo, corpKaizen, corpElgen, corpCandor, flairQuotes, randomSpamVmail, noReplyVmail, bookGrubs, pestControl, landlord, groupMember, storyPartner };
    //public ConnectionType connection = ConnectionType.friend;
    public ConnectionType secretConnection = ConnectionType.friend;
	public float compatible = 0f;
	public float known = 0.1f;
	public float like = 0f;
    [NonSerialized]
    public GroupsController.SocialGroup group; //Reference to group, if known by group...

    public List<ConnectionType> connections = new List<ConnectionType>();

	public float customSort;

    //Known keys- these are taken from the connection detail(s)
    public List<Evidence.DataKey> dataKeys = new List<Evidence.DataKey>();

    //References to the connection detail- there should be only one as two way details are not allowed in this instance
    public List<Fact> connectionFacts = new List<Fact>();

	public Acquaintance(Human newFrom, Human newWith, float newKnown, ConnectionType newConnection, ConnectionType newSecretConnection, GroupsController.SocialGroup newGroup)
	{
        secretConnection = newSecretConnection;
        group = newGroup;

		from = newFrom;
		with = newWith;

        AddConnection(newKnown, newConnection);

        from.acquaintances.Add(this);
	}

    public void AddConnection(float newKnown, ConnectionType newConnection)
    {
        if(!connections.Contains(newConnection))
        {
            if(newKnown > known)
            {
                connections.Insert(0, newConnection);
            }
            else connections.Add(newConnection);
        }

        known = Mathf.Max(newKnown, known);
        compatible = CalculateCompatible();

        if (newKnown > 0f)
        {
            CalculateLike();
        }

        SetupFacts();
    }

    public Acquaintance(CitySaveData.AcquaintanceCitySave data)
    {
        from = CityData.Instance.citizenDirectory.Find(item => item.humanID == data.from);
        with = CityData.Instance.citizenDirectory.Find(item => item.humanID == data.with);

        if (from == null) Game.LogError("Acquaintance from is missing! " + data.from);
        if (with == null) Game.LogError("Acquaintance with is missing! " + data.with);

        connections = new List<ConnectionType>(data.connections);
        secretConnection = data.secret;
        compatible = data.compatible;
        known = data.known;
        like = data.like;
        dataKeys = data.dataKeys;

        from.acquaintances.Add(this);

        SetupFacts();
    }

    //Setup facts. This is different as there is one fact for each 'end' of the acquaintance...
    public void SetupFacts()
    {
        foreach(ConnectionType conn in connections)
        {
            //Are the facts created already? To check we need to look for the inverse acquaintance connection...
            Acquaintance inverse = with.acquaintances.Find(item => item.with == from && item.connections.Contains(conn));

            if (inverse != null)
            {
                foreach(Fact connectionDetail in inverse.connectionFacts)
                {
                    if (conn == Acquaintance.ConnectionType.boss)
                    {
                        from.AddDetailToDict("IsBossOf_" + from.GetCitizenName(), connectionDetail);
                    }
                    else if (conn == Acquaintance.ConnectionType.friend)
                    {
                        from.AddDetailToDict("IsFriendsWith_" + with.GetCitizenName(), connectionDetail);
                    }
                    else if (conn == Acquaintance.ConnectionType.lover)
                    {
                        from.AddDetailToDict("IsInRelationshipWith_" + with.GetCitizenName(), connectionDetail);
                    }
                    else if (conn == Acquaintance.ConnectionType.housemate)
                    {
                        from.AddDetailToDict("IsHousemateOf" + with.GetCitizenName(), connectionDetail);
                    }
                    else if (conn == Acquaintance.ConnectionType.neighbor)
                    {
                        from.AddDetailToDict("IsNeighborOf_" + with.GetCitizenName(), connectionDetail);
                    }
                    else if (conn == Acquaintance.ConnectionType.workOther)
                    {
                        from.AddDetailToDict("WorksWith_" + with.GetCitizenName(), connectionDetail);
                    }
                    else if (conn == Acquaintance.ConnectionType.workTeam)
                    {
                        from.AddDetailToDict("WorksWith_" + with.GetCitizenName(), connectionDetail);
                    }
                    else if (conn == Acquaintance.ConnectionType.familiarResidence)
                    {
                        from.AddDetailToDict("FamiliarResidence" + with.GetCitizenName(), connectionDetail);
                    }
                    else if (conn == Acquaintance.ConnectionType.familiarWork)
                    {
                        from.AddDetailToDict("FamiliarWork" + with.GetCitizenName(), connectionDetail);
                    }

                    //Set a reference to the connection detail within the acquaintance- this can be used for discovery on questioning
                    if(!connectionFacts.Contains(connectionDetail))
                    {
                        connectionFacts.Add(connectionDetail);
                    }
                }
            }
            //Else create the relevent facts
            else
            {
                //Generate a detail for this connection
                Fact connectionDetail = null;

                if (conn == Acquaintance.ConnectionType.boss)
                {
                    connectionDetail = EvidenceCreator.Instance.CreateFact("IsBossOf", with.evidenceEntry, from.evidenceEntry) as Fact;
                    if (connectionDetail != null) from.AddDetailToDict("IsBossOf_" + from.GetCitizenName(), connectionDetail);
                }
                else if (conn == Acquaintance.ConnectionType.friend)
                {
                    connectionDetail = EvidenceCreator.Instance.CreateFact("IsFriendsWith", from.evidenceEntry, with.evidenceEntry) as Fact;
                    if (connectionDetail != null) from.AddDetailToDict("IsFriendsWith_" + with.GetCitizenName(), connectionDetail);
                }
                else if (conn == Acquaintance.ConnectionType.lover)
                {
                    connectionDetail = EvidenceCreator.Instance.CreateFact("IsInRelationshipWith", from.evidenceEntry, with.evidenceEntry) as Fact;
                    if (connectionDetail != null) from.AddDetailToDict("IsInRelationshipWith_" + with.GetCitizenName(), connectionDetail);
                }
                else if (conn == Acquaintance.ConnectionType.housemate)
                {
                    connectionDetail = EvidenceCreator.Instance.CreateFact("IsHousemateOf", from.evidenceEntry, with.evidenceEntry) as Fact;
                    if (connectionDetail != null) from.AddDetailToDict("IsHousemateOf_" + with.GetCitizenName(), connectionDetail);
                }
                else if (conn == Acquaintance.ConnectionType.neighbor)
                {
                    connectionDetail = EvidenceCreator.Instance.CreateFact("IsNeighborOf", from.evidenceEntry, with.evidenceEntry) as Fact;
                    if (connectionDetail != null) from.AddDetailToDict("IsNeighborOf_" + with.GetCitizenName(), connectionDetail);
                }
                else if (conn == Acquaintance.ConnectionType.workOther)
                {
                    connectionDetail = EvidenceCreator.Instance.CreateFact("WorksWith", from.evidenceEntry, with.evidenceEntry) as Fact;
                    if (connectionDetail != null) from.AddDetailToDict("WorksWith_" + with.GetCitizenName(), connectionDetail);
                }
                else if (conn == Acquaintance.ConnectionType.workTeam)
                {
                    connectionDetail = EvidenceCreator.Instance.CreateFact("WorksWith", from.evidenceEntry, with.evidenceEntry) as Fact;
                    if (connectionDetail != null) from.AddDetailToDict("WorksWith_" + with.GetCitizenName(), connectionDetail);
                }
                else if (conn == Acquaintance.ConnectionType.familiarResidence)
                {
                    connectionDetail = EvidenceCreator.Instance.CreateFact("FamiliarResidence", from.evidenceEntry, with.evidenceEntry) as Fact;
                    if (connectionDetail != null) from.AddDetailToDict("FamiliarResidence_" + with.GetCitizenName(), connectionDetail);
                }
                else if (conn == Acquaintance.ConnectionType.familiarWork)
                {
                    connectionDetail = EvidenceCreator.Instance.CreateFact("FamiliarWork", from.evidenceEntry, with.evidenceEntry) as Fact;
                    if (connectionDetail != null) from.AddDetailToDict("FamiliarWork_" + with.GetCitizenName(), connectionDetail);
                }

                //Set a reference to the connection detail within the acquaintance- this can be used for discovery on questioning
                if (connectionDetail != null && !connectionFacts.Contains(connectionDetail))
                {
                    connectionFacts.Add(connectionDetail);
                }
            }
        }

        //Paramour fact already?
        Acquaintance inverseSecret = with.acquaintances.Find(item => item.with == from && item.secretConnection == secretConnection);

        if (inverseSecret != null)
        {
            foreach (Fact connectionDetail in inverseSecret.connectionFacts)
            {
                if (secretConnection == Acquaintance.ConnectionType.paramour)
                {
                    from.AddDetailToDict("IsParamourOf_" + from.GetCitizenName(), connectionDetail);
                }

                //Set a reference to the connection detail within the acquaintance- this can be used for discovery on questioning
                if(!connectionFacts.Contains(connectionDetail)) connectionFacts.Add(connectionDetail);
            }
        }
        else
        {
            //Generate a detail for this connection
            Fact connectionDetail = null;

            if (secretConnection == Acquaintance.ConnectionType.paramour)
            {
                connectionDetail = EvidenceCreator.Instance.CreateFact("IsParamourOf", with.evidenceEntry, from.evidenceEntry) as Fact;
                if (connectionDetail != null) from.AddDetailToDict("IsParamourOf_" + from.GetCitizenName(), connectionDetail);
            }

            //Set a reference to the connection detail within the acquaintance- this can be used for discovery on questioning
            if (connectionDetail != null)
            {
                if (!connectionFacts.Contains(connectionDetail)) connectionFacts.Add(connectionDetail);
            }
        }
    }

	public float CalculateCompatible()
	{
		//Like scale is from 0 to 1
		float ret = 0f;

		//Honesty-Humility (H): sincere, honest, faithful, loyal, modest/unassuming versus sly, deceitful, greedy, pretentious, hypocritical, boastful, pompous
		//If close to own personality type, they get on. If other has high humility, they like them more
		ret += (1f - Mathf.Abs(from.humility - with.humility)) * 0.1f; //10%
		ret += with.humility * 0.2f; //20%

		//Emotionality (E): emotional, oversensitive, sentimental, fearful, anxious, vulnerable versus brave, tough, independent, self-assured, stable
		ret += (1f - Mathf.Abs(from.emotionality - with.emotionality)) * 0.1f; //10%

		//Extraversion (X): outgoing, lively, extraverted, sociable, talkative, cheerful, active versus shy, passive, withdrawn, introverted, quiet, reserved
		ret += (1f - Mathf.Abs(from.extraversion - with.extraversion)) * 0.1f; //10%

		//Agreeableness (A): patient, tolerant, peaceful, mild, agreeable, lenient, gentle versus ill-tempered, quarrelsome, stubborn, choleric
		//If close to own personality type, they get on. If other has high humility, they like them more
		ret += (1f - Mathf.Abs(from.agreeableness - with.agreeableness)) * 0.1f; //10%
		ret += with.agreeableness * 0.2f; //20%

		//Conscientiousness (C): organized, disciplined, diligent, careful, thorough, precise versus sloppy, negligent, reckless, lazy, irresponsible, absent-minded
		ret += (1f - Mathf.Abs(from.conscientiousness - with.conscientiousness)) * 0.1f; //10%

		//Openness to Experience (O): intellectual, creative, unconventional, innovative, ironic versus shallow, unimaginative, conventional
		ret += (1f - Mathf.Abs(from.creativity - with.creativity)) * 0.05f; //5%

		//The last 5% is random
		//ret += Toolbox.Instance.GetPsuedoRandomNumber(0f, 0.05f, (from.humanID * with.humanID).ToString());
		ret = Mathf.Clamp01(ret);

		return ret;
	}

    public Human GetOther(Human other)
    {
        if (other == from) return with;
        else if (other = with) return from;

        return null;
    }

	public void AddKnow(float plusKnow)
	{
		known += plusKnow;
		known = Mathf.Clamp01(known);

		CalculateLike();
	}

	public void CalculateLike()
	{
		compatible = Mathf.Clamp01(compatible);
		known = Mathf.Clamp01(known);

		//The base like is the person's agreeableness
		//The more is known, the bigger the portion of influence from the compatibility
		like = Mathf.Lerp(Mathf.Clamp(from.agreeableness * 0.66f + 0.33f, 0.33f, 0.66f), compatible, known);
	}

    //Knowledge that others know
    //Scan others to see if they should know this knowledge
    public void OthersKnowledgeUpdate()
    {
        //Use the connection detail mystery keys from the connection details- these are usually the keys without DBDetail evidence, as these will also be added below.
        foreach(Fact det in connectionFacts)
        {
            foreach (Evidence.DataKey know in det.preset.applyFromKeysOnDiscovery)
            {
                if (!dataKeys.Contains(know)) dataKeys.Add(know);
            }

            foreach (Evidence.DataKey know in det.preset.applyToKeysOnDiscovery)
            {
                if (!dataKeys.Contains(know)) dataKeys.Add(know);
            }
        }

        //Know place of work - if known 50% or connection is work related
        if(known > SocialControls.Instance.knowPlaceOfWorkThreshold || connections.Contains(ConnectionType.familiarWork) || connections.Contains(ConnectionType.workOther) || connections.Contains(ConnectionType.workTeam) || connections.Contains(ConnectionType.boss) || secretConnection == ConnectionType.paramour)
        {
            //Find detail
            Fact work = null;

            if(with.factDictionary.TryGetValue("WorksAt", out work))
            {
                //Add mystery keys
                foreach (Evidence.DataKey know in work.preset.applyFromKeysOnDiscovery)
                {
                    if (!dataKeys.Contains(know)) dataKeys.Add(know);
                }

                foreach (Evidence.DataKey know in work.preset.applyToKeysOnDiscovery)
                {
                    if (!dataKeys.Contains(know)) dataKeys.Add(know);
                }

                //Tie works at detail to connection
                foreach(Fact det in connectionFacts)
                {
                    //det.AddTiedDiscovery(work);
                }
            }
        }

        //Know residence - if known 80% or connection is friendship or residence related
        if (known > SocialControls.Instance.knowAddressThreshold || connections.Contains(ConnectionType.friend) || connections.Contains(ConnectionType.housemate) || connections.Contains(ConnectionType.lover) || connections.Contains(ConnectionType.neighbor) || secretConnection == ConnectionType.paramour)
        {
            //Find detail
            Fact home = null;

            if (with.factDictionary.TryGetValue("LivesAt", out home))
            {
                //Add mystery keys
                foreach (Evidence.DataKey know in home.preset.applyFromKeysOnDiscovery)
                {
                    if (!dataKeys.Contains(know)) dataKeys.Add(know);
                }

                foreach (Evidence.DataKey know in home.preset.applyToKeysOnDiscovery)
                {
                    if (!dataKeys.Contains(know)) dataKeys.Add(know);
                }

                //Tie works at detail to connection
                foreach (Fact det in connectionFacts)
                {
                    //det.AddTiedDiscovery(work);
                }
            }
        }
    }

	// Default comparer(like)
	public int CompareTo(Acquaintance comp)
	{
		return this.like.CompareTo(comp.like);
	}

    public CitySaveData.AcquaintanceCitySave GenerateSaveData()
    {
        CitySaveData.AcquaintanceCitySave output = new CitySaveData.AcquaintanceCitySave();

        output.from = from.humanID;
        output.with = with.humanID;
        output.connections = new List<ConnectionType>(connections);
        output.secret = secretConnection;
        output.compatible = compatible;
        output.known = known;
        output.like = like;
        output.dataKeys = dataKeys;

        return output;
    }

	//Compare by custom float (set this first ofcourse!)
	public static Comparison<Acquaintance> customComparison = delegate(Acquaintance object1, Acquaintance object2)
	{
		return object1.customSort.CompareTo(object2.customSort);
	};
}
