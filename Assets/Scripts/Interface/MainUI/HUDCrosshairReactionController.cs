﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDCrosshairReactionController : MonoBehaviour
{
    public RectTransform rect;
    public Image img;
    public float progress = 0f;
    public AnimationCurve curve;
    public float maxSize = 200f;
    public CanvasRenderer rend;

    private void Awake()
    {
        rend.SetAlpha(0f);
    }

    // Update is called once per frame
    void Update()
    {
        progress += Time.deltaTime * 2f;

        if(progress >= 1f)
        {
            Destroy(this.gameObject);
        }

        img.color = InterfaceControls.Instance.lightOrbFillImg.color;

        float eval = curve.Evaluate(progress);

        float size = Mathf.LerpUnclamped(0, maxSize, eval);

        rect.sizeDelta = new Vector2(size, size);

        rend.SetAlpha(1f - progress);
    }
}
