﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System;

public class SaveStateController : MonoBehaviour
{
    //Singleton pattern
    private static SaveStateController _instance;
    public static SaveStateController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public async Task CaptureSaveStateAsync(string path)
    {
        //Clear previous save state
        StateSaveData save = new StateSaveData();

        //Basic data
        save.build = Game.Instance.buildID;
        save.cityShare = Toolbox.Instance.GetShareCode(CityData.Instance.cityName, (int)CityData.Instance.citySize.x, (int)CityData.Instance.citySize.y, CityData.Instance.cityBuiltWith, CityData.Instance.seed);
        save.saveTime = System.DateTime.Now.ToString("yyyy-dd-M-HH-mm-ss.ff"); //Save the save time
        save.gameTime = SessionData.Instance.gameTime;
        save.timeLimit = SessionData.Instance.gameTimeLimit;
        save.leapCycle = SessionData.Instance.leapYearCycle;
        save.fingerprintLoop = GameplayController.Instance.printsLetterLoop;
        save.assignCaptureID = SceneRecorder.assignCapID;
        save.assignMessageThreadID = GameplayController.Instance.assignMessageThreadID;
        save.assignGroupID = GroupsController.assignID;
        save.assignStickNote = InterfaceController.assignStickyNoteID;
        save.assignInteractableID = Interactable.worldAssignID;
        save.assignCaseID = Case.assignCaseID;
        save.assignMurderID = MurderController.Instance.assignMurderID;
        save.gameLength = Game.Instance.gameLength;

        //Weather data
        save.currentRain = SessionData.Instance.currentRain;
        save.desiredRain = SessionData.Instance.desiredRain;
        save.currentWind = SessionData.Instance.currentWind;
        save.desiredWind = SessionData.Instance.desiredWind;
        save.currentSnow = SessionData.Instance.currentSnow;
        save.desiredSnow = SessionData.Instance.desiredSnow;
        save.currentLightning = SessionData.Instance.currentLightning;
        save.desiredLightning = SessionData.Instance.desiredLightning;
        save.cityWetness = SessionData.Instance.cityWetness;
        save.citySnow = SessionData.Instance.citySnow;
        save.weatherChange = SessionData.Instance.weatherChangeTimer;

        //Jobs
        save.jobDiffLevel = GameplayController.Instance.jobDifficultyLevel;

        foreach(KeyValuePair<int, SideJob> pair in SideJobController.Instance.allJobsDictionary)
        {
            SideJobAffair affairJob = pair.Value as SideJobAffair;

            if(affairJob != null)
            {
                save.affairJobs.Add(affairJob);
                continue;
            }
            else
            {
                SideJobSabotage saboJob = pair.Value as SideJobSabotage;

                if (saboJob != null)
                {
                    save.sabotageJobs.Add(saboJob);
                    continue;
                }
                else
                {
                    SideJobStolenItem stolenJob = pair.Value as SideJobStolenItem;

                    if (stolenJob != null)
                    {
                        save.stolenItemJobs.Add(stolenJob);
                        continue;
                    }
                    else
                    {
                        SideJobMissingPerson missingPJob = pair.Value as SideJobMissingPerson;

                        if (missingPJob != null)
                        {
                            save.missingPersonJobs.Add(missingPJob);
                            continue;
                        }
                        else
                        {
                            SideJobRevenge revengeJob = pair.Value as SideJobRevenge;

                            if(revengeJob != null)
                            {
                                save.revengeJobs.Add(revengeJob);
                                continue;
                            }
                            else
                            {
                                SideJobStealBriefcase briefcaseJob = pair.Value as SideJobStealBriefcase;

                                if(briefcaseJob != null)
                                {
                                    save.briefcaseJobs.Add(briefcaseJob);
                                    continue;
                                }
                            }
                        }
                    }
                }
            }

            //Otherwise this is a basic job
            save.basicJobs.Add(pair.Value);
        }

        Game.Log("CityGen: Saved " + save.affairJobs.Count + " affair jobs, " + save.sabotageJobs.Count + " sabotage jobs, " + save.stolenItemJobs.Count + " stolen item jobs, " + save.missingPersonJobs.Count + " missing person jobs, " + save.revengeJobs.Count + " revenge jobs, " + save.basicJobs.Count + " basic jobs.");

        //Map path
        if(MapController.Instance.playerRoute != null)
        {
            save.mapPathActive = true;
            save.mapPathNodeSpecific = MapController.Instance.playerRoute.nodeSpecific;
            save.mapPathNode = MapController.Instance.playerRoute.end.nodeCoord;
        }

        //Chapter data
        if (ChapterController.Instance.loadedChapter != null && ChapterController.Instance.chapterScript != null)
        {
            if(ChapterController.Instance.chapterScript.thisCase != null && CasePanelController.Instance.activeCases.Contains(ChapterController.Instance.chapterScript.thisCase))
            {
                save.chapter = ChapterController.Instance.loadedChapter.chapterNumber;
                save.chapterPart = ChapterController.Instance.currentPart;
                save.chapterSaveState = ChapterController.Instance.chapterScript.GetChapterSaveData();
            }
            else
            {
                save.chapter = -1;
                save.chapterPart = -1;
            }
        }
        else
        {
            save.chapter = -1;
            save.chapterPart = -1;
        }

        save.activeCases = CasePanelController.Instance.activeCases;
        //save.archivedCases = CasePanelController.Instance.archivedCases;
        if (CasePanelController.Instance.activeCase != null) save.activeCase = CasePanelController.Instance.activeCase.id;

        save.footprints = new List<GameplayController.Footprint>(GameplayController.Instance.footprintsList);

        save.history = GameplayController.Instance.history;
        save.passcodes = GameplayController.Instance.acquiredPasscodes;
        save.numbers = GameplayController.Instance.acquiredNumbers;
        save.enforcerCalls = new List<GameplayController.EnforcerCall>(GameplayController.Instance.enforcerCalls.Values);

        //Player data
        save.playerFirstName = Game.Instance.playerFirstName;
        save.playerSurname = Game.Instance.playerSurname;
        save.playerGender = Game.Instance.playerGender;
        save.partnerGender = Game.Instance.partnerGender;
        save.playerSkinColour = Game.Instance.playerSkinColour;
        save.playerBirthDay = Game.Instance.playerBirthDay;
        save.playerBirthMonth = Game.Instance.playerBirthMonth;
        save.playerBirthYear = Game.Instance.playerBirthYear;
        save.accidentCover = Player.Instance.claimedAccidentCover;
        save.foodH = Player.Instance.foodHygeinePhotos;
        save.sanitary = Player.Instance.sanitaryHygeinePhotos;
        save.ops = Player.Instance.illegalOpsPhotos;
        save.knowsPasswords = GameplayController.Instance.playerKnowsPasswords;
        save.debt = new List<GameplayController.LoanDebt>(GameplayController.Instance.debt);

        if(Player.Instance.residence != null) save.residence = Player.Instance.residence.address.id;
        
        foreach(NewAddress ad in Player.Instance.apartmentsOwned)
        {
            save.apartmentsOwned.Add(ad.id);
        }

        if (InteractionController.Instance.carryingObject != null)
        {
            save.carried = InteractionController.Instance.carryingObject.interactable.id;
        }
        else save.carried = -1;

        save.tutorial = SessionData.Instance.enableTutorialText;
        //save.tutTextRead = new List<string>(SessionData.Instance.tutorialTextRead);
        save.tutTextTriggered = new List<string>(SessionData.Instance.tutorialTextTriggered);

        //First person items
        save.firstPersonItems = FirstPersonItemController.Instance.slots.FindAll(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic);

        //Saved scanned prints cache
        foreach(KeyValuePair<Interactable, List<Interactable>> pair in BioScreenController.Instance.scannedObjectsPrintsCache)
        {
            StateSaveData.ScannedObjPrint newScanned = new StateSaveData.ScannedObjPrint { objID = pair.Key.id, prints = new List<int>() };

            foreach(Interactable i in pair.Value)
            {
                newScanned.prints.Add(i.id);
            }

            save.scannedPrints.Add(newScanned);
        }

        save.playerPos = Player.Instance.transform.position;
        save.playerRot = Player.Instance.transform.rotation;
        save.money = GameplayController.Instance.money;
        Game.Log("Gameplay: Save money: " + save.money);
        save.lockpicks = GameplayController.Instance.lockPicks;
        save.socCredit = GameplayController.Instance.socialCredit;
        save.health = Player.Instance.currentHealth;

        save.nourishment = Player.Instance.nourishment;
        save.hydration = Player.Instance.hydration;
        save.alertness = Player.Instance.alertness;
        save.energy = Player.Instance.energy;
        save.hygiene = Player.Instance.hygiene;
        save.heat = Player.Instance.heat;
        save.drunk = Player.Instance.drunk;
        save.sick = Player.Instance.sick;
        save.headache = Player.Instance.headache;
        save.wet = Player.Instance.wet;
        save.brokenLeg = Player.Instance.brokenLeg;
        save.bruised = Player.Instance.bruised;
        save.blackEye = Player.Instance.blackEye;
        save.blackedOut = Player.Instance.blackedOut;
        save.numb = Player.Instance.numb;
        save.poisoned = Player.Instance.poisoned;
        save.bleeding = Player.Instance.bleeding;
        save.wellRested = Player.Instance.wellRested;
        save.starchAddiction = Player.Instance.starchAddiction;
        save.syncDiskInstall = Player.Instance.syncDiskInstall;
        save.blinded = Player.Instance.blinded;

        save.crouched = Player.Instance.isCrouched;

        save.upgrades = new List<UpgradesController.Upgrades>(UpgradesController.Instance.upgrades);
        save.sabotaged = new List<string>(GameplayController.Instance.companiesSabotaged);
        save.booksRead = new List<string>(GameplayController.Instance.booksRead);

        save.playerSavedCaptures.AddRange(Player.Instance.sceneRecorder.interactable.sCap);
        save.speech.AddRange(Player.Instance.speechController.speechQueue);

        //Keyring
        foreach (NewDoor d in Player.Instance.keyring)
        {
            save.keyring.Add(d.wall.id);
        }

        foreach (Interactable i in Player.Instance.playerKeyringInt)
        {
            save.keyringInt.Add(i.id);
        }

        foreach (KeyValuePair<int, TelephoneController.CallSource> pair in TelephoneController.Instance.fakeTelephoneDictionary)
        {
            save.fakeTelephone.Add(new StateSaveData.FakeTelephone { number = pair.Key, source = pair.Value });
        }

        //Murders
        save.pgLoop = MurderController.Instance.procGenLoopActive; //Proc gen loop state
        if(MurderController.Instance.currentMurderer != null) save.currentMurderer = MurderController.Instance.currentMurderer.humanID;
        if (MurderController.Instance.currentVictim != null) save.currentVictim = MurderController.Instance.currentVictim.humanID;
        if (MurderController.Instance.currentActiveCase != null) save.currentActiveCase = MurderController.Instance.currentActiveCase.id;
        if (MurderController.Instance.murderPreset != null) save.murderPreset = MurderController.Instance.murderPreset.name;
        if (MurderController.Instance.chosenMO != null) save.chosenMO = MurderController.Instance.chosenMO.name;

        save.previousMurderers = new List<int>();

        foreach(Human h in MurderController.Instance.previousMurderers)
        {
            save.previousMurderers.Add(h.humanID);
        }

        save.pauseBetweenMurders = MurderController.Instance.pauseBetweenMurders;
        save.murderRoutineActive = MurderController.Instance.murderRoutineActive;
        save.maxMurderDiffLevel = MurderController.Instance.maxDifficultyLevel;

        save.murders = new List<MurderController.Murder>(MurderController.Instance.activeMurders);
        save.iaMurders = new List<MurderController.Murder>(MurderController.Instance.inactiveMurders);

        //Vmail
        foreach (KeyValuePair<int, StateSaveData.MessageThreadSave> pair in GameplayController.Instance.messageThreads)
        {
            save.messageThreads.Add(pair.Value);
        }

        Game.Log("Saved " + save.messageThreads.Count + " vmail message threads");

        //Newspaper
        save.newspaperState = NewspaperController.Instance.currentState;

        //State
        save.storedTransPos = Player.Instance.storedTransitionPosition;

        if (Player.Instance.hideInteractable != null)
        {
            save.hideInteractable = Player.Instance.hideInteractable.id;
            save.hideRef = Player.Instance.hideReference;
        }
        else
        {
            save.hideInteractable = -1;
            save.hideRef = -1;
        }

        if (Player.Instance.phoneInteractable != null) save.phoneInteractable = Player.Instance.phoneInteractable.id;
        else save.phoneInteractable = -1;

        if (Player.Instance.computerInteractable != null) save.computerInteractable = Player.Instance.computerInteractable.id;
        else save.computerInteractable = -1;

        save.duct = -1;

        if (Player.Instance.currentDuct != null && Player.Instance.inAirVent)
        {
            save.duct = Player.Instance.currentDuct.ductID;
        }

        //Save guest passes
        foreach(KeyValuePair<NewAddress, Vector2> pair in GameplayController.Instance.guestPasses)
        {
            save.guestPasses.Add(new StateSaveData.GuestPassStateSave { id = pair.Key.id, guestPassUntil = pair.Value });
        }

        //Save crime scene cleanup pool
        if(GameplayController.Instance.crimeSceneCleanups.Count > 0)
        {
            foreach(NewGameLocation l in GameplayController.Instance.crimeSceneCleanups)
            {
                StateSaveData.CrimeSceneCleanup newEntry = new StateSaveData.CrimeSceneCleanup();

                if (l.thisAsAddress != null)
                {
                    newEntry.isStreet = false;
                    newEntry.id = l.thisAsAddress.id;
                }
                else if (l.thisAsStreet != null)
                {
                    newEntry.isStreet = true;
                    newEntry.id = l.thisAsStreet.streetID;
                }
                else continue;

                save.crimeSceneCleanup.Add(newEntry);
            }
        }

        //SAve hotel guests
        save.hotelGuests = GameplayController.Instance.hotelGuests;

        //Save broken windows
        foreach(KeyValuePair<Vector3, float> pair in GameplayController.Instance.brokenWindows)
        {
            StateSaveData.BrokenWindowSave newWindowSave = new StateSaveData.BrokenWindowSave { pos = pair.Key, brokenAt = pair.Value };
            save.brokenWindows.Add(newWindowSave);
        }

        //Save building state
        foreach(NewBuilding b in CityData.Instance.buildingDirectory)
        {
            StateSaveData.BuildingStateSav newBuildingState = new StateSaveData.BuildingStateSav();
            newBuildingState.id = b.buildingID;
            newBuildingState.alarmActive = b.alarmActive;
            newBuildingState.alarmTimer = b.alarmTimer;
            newBuildingState.targetMode = b.targetMode;
            newBuildingState.targetModeSetAt = b.targetModeSetAt;

            if (b.targetMode != NewBuilding.AlarmTargetMode.illegalActivities)
            {
                if (!GameplayController.Instance.alteredSecurityTargetsBuildings.Contains(b))
                {
                    GameplayController.Instance.alteredSecurityTargetsBuildings.Add(b);
                }
            }

            newBuildingState.wanted = b.wantedInBuilding;

            foreach(NewTile t in b.stairwells.Keys)
            {
                Elevator e;

                if(b.stairwells.TryGetValue(t, out e))
                {
                    if(e.spawnedObject != null)
                    {
                        newBuildingState.elevators.Add(new StateSaveData.ElevatorStateSave { tileID = t.tileID, yPos = e.spawnedObject.transform.position.y, floor = e.currentFloor });
                    }
                }
            }
            
            foreach(Human h in b.alarmTargets)
            {
                newBuildingState.targets.Add(h.humanID);
            }

            newBuildingState.callLog = new List<TelephoneController.PhoneCall> (b.callLog);
            newBuildingState.lostAndFound = b.lostAndFound;

            save.buildings.Add(newBuildingState);

            //Save floor state
            foreach(KeyValuePair<int, NewFloor> pair in b.floors)
            {
                if(pair.Value.alarmLockdown)
                {
                    StateSaveData.FloorStateSave newFloorState = new StateSaveData.FloorStateSave();
                    newFloorState.id = pair.Value.floorID;
                    newFloorState.alarmLockdown = pair.Value.alarmLockdown;
                    save.floors.Add(newFloorState);
                }

                //Save address sate
                foreach(NewAddress ad in pair.Value.addresses)
                {
                    if(ad.addressPreset != null && (ad.addressPreset.useOwnSecuritySystem || ad.saleNote != null || ad.vandalism.Count > 0 || ad.escalation.Count > 0))
                    {
                        StateSaveData.AddressStateSave newAddressState = new StateSaveData.AddressStateSave();
                        newAddressState.id = ad.id;
                        newAddressState.alarmActive = ad.alarmActive;
                        newAddressState.alarmTimer = ad.alarmTimer;
                        newAddressState.targetMode = ad.targetMode;
                        newAddressState.targetModeSetAt = ad.targetModeSetAt;
                        newAddressState.escalation = new List<NewGameLocation.TrespassEscalation>(ad.escalation.Values);

                        newAddressState.vandalism = new List<NewAddress.Vandalism>(ad.vandalism);

                        if(ad.saleNote != null) newAddressState.sale = ad.saleNote.id;

                        foreach (Human h in ad.alarmTargets)
                        {
                            newAddressState.targets.Add(h.humanID);
                        }

                        save.addresses.Add(newAddressState);
                    }
                }
            }
        }

        //Save company state
        foreach(Company c in CityData.Instance.companyDirectory)
        {
            if (!c.preset.recordSalesData) continue;

            StateSaveData.CompanyStateSave newCompanySave = new StateSaveData.CompanyStateSave();
            newCompanySave.id = c.companyID;
            newCompanySave.sales = new List<Company.SalesRecord>(c.sales);

            save.companies.Add(newCompanySave);
        }

        //Save room state
        foreach(NewRoom r in CityData.Instance.roomDirectory)
        {
            //Detect changes so we don't save everything...
            //if(r.explorationLevel != 0 || r.mainLightStatus != r.preset.lightsOnAtStart || r.decorEdit || r.gasLevel > 0f)
            //{
                StateSaveData.RoomStateSave newRoomState = new StateSaveData.RoomStateSave();
                newRoomState.id = r.roomID;
                newRoomState.fID = r.furnitureAssignID;
                newRoomState.iID = r.interactableAssignID;
                newRoomState.ex = r.explorationLevel;
                newRoomState.ml = r.mainLightStatus;
                newRoomState.gl = r.gasLevel;

                //Save the decor state with the save state data
                if (r.decorEdit)
                {
                    Game.Log("CityGen: Saving decor state for room " + r.name);
                    newRoomState.decorOverride = new List<CitySaveData.RoomCitySave>();
                    newRoomState.decorOverride.Add(r.GenerateSaveData());
                }
                else newRoomState.decorOverride = null;

                save.rooms.Add(newRoomState);
            //}
        }

        Game.Log("CityGen: Saved " + save.rooms.Count + "/" + CityData.Instance.roomDictionary.Count + " room states...");

        //Save citizen state
        foreach(Human h in CityData.Instance.citizenDirectory)
        {
            StateSaveData.CitizenStateSave newSave = new StateSaveData.CitizenStateSave();

            newSave.id = h.humanID;

            //Position
            newSave.pos = h.transform.position;
            newSave.rot = h.transform.rotation;

            newSave.trespassingEscalation = h.trespassingEscalation;
            newSave.currentOutfit = h.outfitController.currentOutfit;

            //Status
            newSave.nourishment = h.nourishment;
            newSave.hydration = h.hydration;
            newSave.alertness = h.alertness;
            newSave.energy = h.energy;
            newSave.excitement = h.excitement;
            newSave.chores = h.chores;
            newSave.hygiene = h.hygiene;
            newSave.bladder = h.bladder;
            newSave.heat = h.heat;
            newSave.drunk = h.drunk;
            newSave.breath = h.breath;
            newSave.poisoned = h.poisoned;
            newSave.blinded = h.blinded;

            if (h.poisoner != null)
            {
                newSave.poisoner = h.poisoner.humanID;
            }

            newSave.currentHealth = h.currentHealth;
            newSave.currentNerve = h.currentNerve;

            newSave.fsDirt = h.footstepDirt;
            newSave.fsBlood = h.footstepBlood;

            foreach(Human.Wound w in h.currentWounds)
            {
                if(w != null)
                {
                    newSave.wounds.Add(w);
                }
            }

            if(h.currentConsumables.Count > 0)
            {
                if (newSave.currentConsumable == null) newSave.currentConsumable = new List<string>();
                else newSave.currentConsumable.Clear();

                foreach(InteractablePreset p in h.currentConsumables)
                {
                    newSave.currentConsumable.Add(p.name);
                }
            }

            newSave.trash = h.trash;
            newSave.escalationLevel = h.escalationLevel;

            newSave.fingerprintLoop = h.fingerprintLoop;

            newSave.unreportable = h.unreportable;

            //AI
            if (h.ai != null)
            {
                newSave.convicted = h.ai.isConvicted;
                newSave.putDown = new List<int>();

                foreach (Interactable i in h.ai.putDownItems)
                {
                    newSave.putDown.Add(i.id);
                }

                if (h.ai.investigateLocation != null) newSave.investigateLocation = h.ai.investigateLocation.nodeCoord; //Last known node of player
                newSave.investigatePosition = h.ai.investigatePosition; //Last known position of player
                newSave.investigatePositionProjection = h.ai.investigatePositionProjection; //Projection of where the current target will be (2m ahead of now)
                newSave.lastInvestigate = h.ai.lastInvestigate;

                newSave.persuit = h.ai.persuit; //True if chasing player (with a certain amount of grace period tracking)
                newSave.seesPlayerOnPersuit = h.ai.seesOnPersuit; //Only true when actively seeing the player
                newSave.persuitChaseLogicUses = h.ai.persuitChaseLogicUses; //Each time a player is spotted, it gets one lead on to where the player has disappeared to...

                if (newSave.persuit && h.ai.persuitTarget != null && (h.ai.persuitTarget as Human) != null)
                {
                    newSave.persuitTarget = (h.ai.persuitTarget as Human).humanID;
                    newSave.persuitPlayer = (h.ai.persuitTarget as Human).isPlayer;
                }

                newSave.minimumInvestigationTimeMultiplier = h.ai.minimumInvestigationTimeMultiplier; //Can be increased depending on the severety of the crime (remember to max this value)
                newSave.reactionState = h.ai.reactionState;

                newSave.atHome = new List<int>();

                foreach(Interactable i in h.ai.putDownItems)
                {
                    newSave.atHome.Add(i.id);
                }

                newSave.ko = h.ai.ko; //True if AI is knocked-out
                newSave.koTime = h.ai.koTime;

                newSave.wallet = h.walletItems;

                newSave.res = h.ai.restrained;
                newSave.resTime = h.ai.restrainTime;

                newSave.remFromWorld = h.removedFromWorld;

                newSave.spooked = h.ai.spooked;
                newSave.spookCount = h.ai.spookCounter;

                if(h.ai.currentGoal != null)
                {
                    newSave.currentGoal = h.ai.currentGoal.GetGoalStateSave();
                }

                newSave.sightingCit = new List<int>();
                newSave.sightings = new List<Human.Sighting>();

                foreach(KeyValuePair<Human, Human.Sighting> pair in h.lastSightings)
                {
                    newSave.sightingCit.Add(pair.Key.humanID);
                    newSave.sightings.Add(pair.Value);
                }

                if(h.ai.confineLocation != null)
                {
                    if(h.ai.confineLocation.thisAsAddress != null)
                    {
                        newSave.confine = new StateSaveData.AvoidConfineStateSave { id = h.ai.confineLocation.thisAsAddress.id, st = false };
                    }
                    else
                    {
                        newSave.confine = new StateSaveData.AvoidConfineStateSave { id = h.ai.confineLocation.thisAsStreet.streetID, st = true };
                    }
                }

                if(h.ai.avoidLocations.Count > 0)
                {
                    foreach(NewGameLocation gl in h.ai.avoidLocations)
                    {
                        if (gl.thisAsAddress != null)
                        {
                            newSave.avoid.Add(new StateSaveData.AvoidConfineStateSave { id = gl.thisAsAddress.id, st = false });
                        }
                        else
                        {
                            newSave.avoid.Add(new StateSaveData.AvoidConfineStateSave { id = gl.thisAsStreet.streetID, st = true });
                        }
                    }
                }
            }

            //Dead
            newSave.death = h.death;
            newSave.ragdollSnapshot = h.animationController.GetLimbSnapshot();

            save.citizens.Add(newSave);
        }

        save.removedCityData = new List<int>(CleanupController.Instance.removedCityDataItems);

        //Save meta objects
        foreach(KeyValuePair<int, MetaObject> pair in CityData.Instance.metaObjectDictionary)
        {
            //Only save with state if not saved in city data
            if(!pair.Value.cd)
            {
                save.metas.Add(pair.Value);
            }
        }

        //Save interactable state
        foreach(Interactable i in CityData.Instance.interactableDirectory)
        {
            //Detect change...
            //Don't do this for citizens
            if(!i.preset.dontSaveWithSaveGames)
            {
                bool saveI = i.IsSaveStateEligable();

                if (saveI)
                {
                    save.interactables.Add(i);

                    //Count up dynamic prints; use this for debugging save game file sizes
                    save.dynamicPrintsCount += i.df.Count;
                    save.sceneCaptureCount += i.cap.Count + i.sCap.Count;
                    if ((i.cap.Count + i.sCap.Count) > save.sceneCapMax) save.sceneCapMax = i.cap.Count + i.sCap.Count;
                }
                else if(i.jobParent != null)
                {
                    Game.Log("Object " + i.id + " " + i.name + " will be deleted! Rem: " + i.rem + " preset: " + i.preset.dontSaveWithSaveGames);
                }

#if UNITY_EDITOR
                if (i.controller != null && UnityEditor.Selection.Contains(i.controller.gameObject))
                {
                    Game.Log("CityGen: Save selected interatable " + i.name + " " + i.id + " with game: " + saveI);
                }
#endif
            }
        }

        Game.Log("CityGen: Saved " + save.interactables.Count + "/" + CityData.Instance.interactableDirectory.Count + " interactable states...");

        //Save doors
        foreach(KeyValuePair<int, NewDoor> pair in CityData.Instance.doorDictionary)
        {
            NewDoor d = pair.Value;

            //Detect change from start
            if(!d.isClosed || d.isLocked != d.GetDefaultLockState() || d.wall.currentLockStrength != d.wall.baseLockStrength || d.wall.currentDoorStrength != d.wall.baseDoorStrength || d.ajar != 0f || d.forbiddenForPublic)
            {
                StateSaveData.DoorStateSave newSave = new StateSaveData.DoorStateSave();

                newSave.id = pair.Key;
                //newSave.isClosed = d.isClosed;
                newSave.l = d.isLocked;
                newSave.ls = d.wall.currentLockStrength;
                newSave.ds = d.wall.currentDoorStrength;
                newSave.ajar = d.ajar;
                newSave.cs = d.forbiddenForPublic;

                save.doors.Add(newSave);
            }
        }

        //Save evidence
        foreach(KeyValuePair<string, Evidence> pair in GameplayController.Instance.evidenceDictionary)
        {
            bool saveRequired = pair.Value.forceSave;

            if(pair.Value.isFound && !pair.Value.preset.discoverOnCreate)
            {
                saveRequired = true;
            }

            //Do we need to save this?
            if(pair.Value.overrideDDS != null && pair.Value.overrideDDS.Length > 0)
            {
                saveRequired = true;
            }

            //Save multi-page
            EvidenceMultiPage evMP = pair.Value as EvidenceMultiPage;

            if(evMP != null && evMP.pageContent.Count > 0)
            {
                saveRequired = true;
            }

            //Save printed
            EvidenceSurveillance evSurv = pair.Value as EvidenceSurveillance;

            if(evSurv != null)
            {
                saveRequired = true;
            }

            EvidencePrintedVmail evVmail = pair.Value as EvidencePrintedVmail;

            if (evVmail != null)
            {
                saveRequired = true;
            }

            EvidenceStickyNote evNote = pair.Value as EvidenceStickyNote;

            if(evNote != null)
            {
                saveRequired = true;
            }

            if(pair.Value.customNames.Count > 0)
            {
                saveRequired = true;
            }

            if (pair.Value.preset.useDataKeys)
            {
                foreach(KeyValuePair<Evidence.DataKey, List<Evidence.DataKey>> keyTie in pair.Value.keyTies)
                {
                    if(keyTie.Value.Count > 0)
                    {
                        saveRequired = true;
                        break;
                    }
                }
            }

            if (pair.Value.discoveryProgress.Count > 0) saveRequired = true;

            if(saveRequired)
            {
                StateSaveData.EvidenceStateSave newEvSave = new StateSaveData.EvidenceStateSave();
                newEvSave.id = pair.Key;
                newEvSave.dds = pair.Value.overrideDDS;
                newEvSave.found = pair.Value.isFound;
                newEvSave.fs = pair.Value.forceSave;
                newEvSave.customName = new List<Evidence.CustomName>(pair.Value.customNames);

                if(evMP != null)
                {
                    newEvSave.mpContent = new List<EvidenceMultiPage.MultiPageContent>(evMP.pageContent);
                }

                //The sticky note is the only instance where we actually need to save the note text
                if(evNote != null)
                {
                    newEvSave.n = pair.Value.GetNote((new Evidence.DataKey[] { Evidence.DataKey.name }).ToList());
                }

                if (pair.Value.preset.useDataKeys)
                {
                    newEvSave.keyTies = new List<StateSaveData.EvidenceDataKeyTie>();

                    foreach (KeyValuePair<Evidence.DataKey, List<Evidence.DataKey>> keyTie in pair.Value.keyTies)
                    {
                        StateSaveData.EvidenceDataKeyTie newTie = new StateSaveData.EvidenceDataKeyTie();
                        newTie.key = keyTie.Key;
                        newTie.tied = new List<Evidence.DataKey>(keyTie.Value);
                        newEvSave.keyTies.Add(newTie);
                    }
                }

                newEvSave.discovery = new List<Evidence.Discovery>(pair.Value.discoveryProgress);

                save.evidence.Add(newEvSave);
            }
        }

        //Save time evidence
        save.timeEvidence = new List<string>();

        foreach(EvidenceTime te in GameplayController.Instance.timeEvidence)
        {
            save.timeEvidence.Add(te.evID);
        }

        //Save date evidence
        save.dateEvidence = new List<string>();

        foreach(EvidenceDate da in GameplayController.Instance.dateEvidence)
        {
            save.dateEvidence.Add(da.date);
        }

        save.customStrings = new List<string>();

        //Save custom strings
        foreach(Fact f in GameplayController.Instance.factList)
        {
            if(f.isCustom)
            {
                save.customStrings.Add(f.GetSerializedString());
            }
        }

        Game.Log("CityGen: Saved " + save.evidence.Count + "/" + GameplayController.Instance.evidenceDictionary.Count + " evidence states...");

        //Save spatter
        save.spatter = new List<SpatterSimulation>(GameplayController.Instance.spatter);

        //Save furniture storage
        save.furnitureStorage = new List<CitySaveData.FurnitureClusterObjectCitySave>();

        foreach (FurnitureLocation furn in PlayerApartmentController.Instance.furnitureStorage)
        {
            if (furn.furniture == null) continue;

            CitySaveData.FurnitureClusterObjectCitySave newObj = new CitySaveData.FurnitureClusterObjectCitySave();

            newObj.id = furn.id;
            newObj.furnitureClasses = new List<string>();
            newObj.up = true;
            newObj.offset = furn.offset;

            foreach (FurnitureClass furnClass in furn.furnitureClasses)
            {
                newObj.furnitureClasses.Add(furnClass.name);
            }

            newObj.furniture = furn.furniture.name;
            if (furn.art != null) newObj.art = furn.art.name;
            newObj.matKey = furn.matKey; //Chosen when added to a room
            newObj.artMatKet = furn.artMatKey;

            newObj.scale = furn.scaleMultiplier;

            save.furnitureStorage.Add(newObj);
        }

        save.compositionData = new List<string>();
        save.compositionData.Add("Interactables: " + save.interactables.Count);
        save.compositionData.Add("Meta Objects: " + save.metas.Count);
        save.compositionData.Add("Fingerprints: " + save.scannedPrints.Count);
        save.compositionData.Add("Footprints: " + save.footprints.Count);
        save.compositionData.Add("Spatter: " + save.spatter.Count);
        save.compositionData.Add("Message Threads: " + save.messageThreads.Count);
        save.compositionData.Add("Evidence: " + save.evidence.Count);
        save.compositionData.Add("TimeEv: " + save.timeEvidence.Count);
        save.compositionData.Add("DateEv: " + save.dateEvidence.Count);

        //Save to file...
        System.Diagnostics.Stopwatch stopWatch = null;

        if (Game.Instance.devMode)
        {
            stopWatch = new System.Diagnostics.Stopwatch();
            stopWatch.Start();
        }

        if (Game.Instance.useSaveGameCompression)
        {
            bool result = await DataCompressionController.Instance.CompressAndSaveDataAsync<StateSaveData>(save, path + "b", Game.Instance.saveGameCompressionQuality);

            if (result)
            {
                Game.Log("CityGen: Successfully saved compressed save game");
            }
            else
            {
                Game.LogError("Unable to save compressed save game, using no compression instead...");

                string jsonString = JsonUtility.ToJson(save);
                await Task.Run(() =>
                {
                    using (StreamWriter streamWriter = File.CreateText(path))
                    {
                        streamWriter.Write(jsonString);
                    }
                });
            }
        }
        else
        {
            string jsonString = JsonUtility.ToJson(save);

            using (StreamWriter streamWriter = File.CreateText(path))
            {
                streamWriter.Write(jsonString);
            }
        }

        if (Game.Instance.devMode && stopWatch != null)
        {
            stopWatch.Stop();
            Game.Log("CityGen: Save data written in " + stopWatch.Elapsed.TotalSeconds);
        }

        //Save complete message
        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "Game saved"));
    }

    //Pre-load side jobs and murders as we need their references active in the dictionaries before objects are loaded...
    //This is because objects are being loaded from city data, and may be relevent; the case classes need to exist to set them up properly...
    public void PreLoadCases(ref StateSaveData load)
    {
        //Create job files where they should be before loading interactables...
        foreach (SideJobAffair job in load.affairJobs)
        {
            if (!SideJobController.Instance.allJobsDictionary.ContainsKey(job.jobID))
            {
                SideJobController.Instance.allJobsDictionary.Add(job.jobID, job);
            }
            else SideJobController.Instance.allJobsDictionary[job.jobID] = job;

            SideJob.assignJobID = Mathf.Max(job.jobID + 1, SideJob.assignJobID);
        }

        foreach (SideJobSabotage job in load.sabotageJobs)
        {
            if (!SideJobController.Instance.allJobsDictionary.ContainsKey(job.jobID))
            {
                SideJobController.Instance.allJobsDictionary.Add(job.jobID, job);
            }
            else SideJobController.Instance.allJobsDictionary[job.jobID] = job;

            SideJob.assignJobID = Mathf.Max(job.jobID + 1, SideJob.assignJobID);
        }

        foreach (SideJobStolenItem job in load.stolenItemJobs)
        {
            if (!SideJobController.Instance.allJobsDictionary.ContainsKey(job.jobID))
            {
                SideJobController.Instance.allJobsDictionary.Add(job.jobID, job);
            }
            else SideJobController.Instance.allJobsDictionary[job.jobID] = job;

            SideJob.assignJobID = Mathf.Max(job.jobID + 1, SideJob.assignJobID);
        }

        foreach (SideJobMissingPerson job in load.missingPersonJobs)
        {
            if (!SideJobController.Instance.allJobsDictionary.ContainsKey(job.jobID))
            {
                SideJobController.Instance.allJobsDictionary.Add(job.jobID, job);
            }
            else SideJobController.Instance.allJobsDictionary[job.jobID] = job;

            SideJob.assignJobID = Mathf.Max(job.jobID + 1, SideJob.assignJobID);
        }

        foreach (SideJobRevenge job in load.revengeJobs)
        {
            if (!SideJobController.Instance.allJobsDictionary.ContainsKey(job.jobID))
            {
                SideJobController.Instance.allJobsDictionary.Add(job.jobID, job);
            }
            else SideJobController.Instance.allJobsDictionary[job.jobID] = job;

            SideJob.assignJobID = Mathf.Max(job.jobID + 1, SideJob.assignJobID);
        }

        foreach (SideJobStealBriefcase job in load.briefcaseJobs)
        {
            if (!SideJobController.Instance.allJobsDictionary.ContainsKey(job.jobID))
            {
                SideJobController.Instance.allJobsDictionary.Add(job.jobID, job);
            }
            else SideJobController.Instance.allJobsDictionary[job.jobID] = job;

            SideJob.assignJobID = Mathf.Max(job.jobID + 1, SideJob.assignJobID);
        }

        foreach (SideJob job in load.basicJobs)
        {
            if (!SideJobController.Instance.allJobsDictionary.ContainsKey(job.jobID))
            {
                SideJobController.Instance.allJobsDictionary.Add(job.jobID, job);
            }
            else SideJobController.Instance.allJobsDictionary[job.jobID] = job;

            SideJob.assignJobID = Mathf.Max(job.jobID + 1, SideJob.assignJobID);
        }

        foreach (MurderController.Murder m in load.murders)
        {
            if (!MurderController.Instance.activeMurders.Contains(m))
            {
                MurderController.Instance.activeMurders.Add(m);
            }
        }

        foreach (MurderController.Murder m in load.iaMurders)
        {
            if (!MurderController.Instance.inactiveMurders.Contains(m))
            {
                MurderController.Instance.inactiveMurders.Add(m);
            }
        }
    }

    public void LoadSaveState(StateSaveData load)
    {
        //Set time
        SessionData.Instance.SetGameTime(load.gameTime, load.leapCycle);
        SessionData.Instance.gameTimeLimit = load.timeLimit;

        //Set weather
        SessionData.Instance.weatherChangeTimer = load.weatherChange;
        SessionData.Instance.SetWeather(load.desiredRain, load.desiredWind, load.desiredSnow, load.desiredLightning, 1f);
        SessionData.Instance.currentRain = load.currentRain;
        SessionData.Instance.currentWind = load.currentWind;
        SessionData.Instance.currentSnow = load.currentSnow;
        SessionData.Instance.currentLightning = load.currentLightning;
        SessionData.Instance.cityWetness = load.cityWetness;
        SessionData.Instance.citySnow = load.citySnow;

        SessionData.Instance.ExecuteWeatherChange();
        SessionData.Instance.ExecuteWetnessChange();
        SessionData.Instance.ExecuteWindChange();

        GameplayController.Instance.printsLetterLoop = load.fingerprintLoop;
        SceneRecorder.assignCapID = load.assignCaptureID;
        GameplayController.Instance.assignMessageThreadID = load.assignMessageThreadID;
        GroupsController.assignID = load.assignGroupID;
        InterfaceController.assignStickyNoteID = load.assignStickNote;
        Interactable.worldAssignID = load.assignInteractableID;
        Case.assignCaseID = load.assignCaseID;
        MurderController.Instance.assignMurderID = load.assignMurderID;
        Game.Instance.SetGameLength(load.gameLength, true, true, false);

        SessionData.Instance.SetDisplayTutorialText(load.tutorial);

        if (load.tutorial)
        {
            //SessionData.Instance.tutorialTextRead = new HashSet<string>(load.tutTextRead);
            SessionData.Instance.tutorialTextTriggered = new HashSet<string>(load.tutTextTriggered);
        }

        SessionData.Instance.UpdateTutorialNotifications();

        //Load time evidence
        foreach(string timeID in load.timeEvidence)
        {
            EvidenceCreator.Instance.GetTimeEvidence(timeID);
        }

        //Load date evidence
        foreach (string date in load.dateEvidence)
        {
            EvidenceCreator.Instance.GetDateEvidence(date);
        }

        GameplayController.Instance.SetJobDifficultyLevel(load.jobDiffLevel);

        //Load player residence
        if(load.residence > -1)
        {
            NewAddress res = null;

            if(CityData.Instance.addressDictionary.TryGetValue(load.residence, out res))
            {
                if(res != null)
                {
                    if(res.residence != null)
                    {
                        Game.Log("City Gen: Loading player's residence from save data...");
                        Player.Instance.SetResidence(res.residence);
                    }

                    if (!Player.Instance.apartmentsOwned.Contains(res))
                    {
                        Player.Instance.apartmentsOwned.Add(res);
                    }
                }
            }
            else
            {
                Game.Log("CityGen: Setting player as having no apartment...");
                Player.Instance.SetResidence(null);
            }
        }
        else
        {
            Game.Log("CityGen: Setting player as having no apartment...");
            Player.Instance.SetResidence(null);
        }

        if(load.apartmentsOwned != null)
        {
            foreach(int i in load.apartmentsOwned)
            {
                NewAddress res = null;

                if (CityData.Instance.addressDictionary.TryGetValue(i, out res))
                {
                    if (res != null && !Player.Instance.apartmentsOwned.Contains(res))
                    {
                        Player.Instance.apartmentsOwned.Add(res);
                    }

                    Player.Instance.AddLocationOfAuthorty(res);
                }
            }
        }

        Player.Instance.claimedAccidentCover = load.accidentCover;
        Player.Instance.foodHygeinePhotos = new List<int>(load.foodH);
        Player.Instance.sanitaryHygeinePhotos = new List<int>(load.sanitary);
        Player.Instance.illegalOpsPhotos = new List<int>(load.ops);
        GameplayController.Instance.playerKnowsPasswords = new List<int>(load.knowsPasswords);
        GameplayController.Instance.debt = new List<GameplayController.LoanDebt>(load.debt);

        //Load player captures
        if (load.playerSavedCaptures.Count > 0)
        {
            Player.Instance.sceneRecorder.interactable.sCap = new List<SceneRecorder.SceneCapture>(load.playerSavedCaptures);

            foreach (SceneRecorder.SceneCapture s in load.playerSavedCaptures)
            {
                //Add in non-serialized references...
                s.recorder = Player.Instance.sceneRecorder;

                //foreach (SceneRecorder.InteractableCapture ic in s.capturedObjects)
                //{
                //    ic.inter = Player.Instance.interactable;

                //    foreach (SceneRecorder.ActorCapture ac in s.actorCap)
                //    {
                //        ac.human = CityData.Instance.citizenDictionary[ac.id];
                //    }

                //    Player.Instance.sceneRecorder.interactable.sCap.Add(s);
                //}
            }

            Game.Log("Loaded " + load.playerSavedCaptures.Count + " player captures...");
        }

        //Load vmails
        Game.Log("CityGen: Loading " + load.messageThreads.Count + " vmail threads...");

        foreach (StateSaveData.MessageThreadSave thread in load.messageThreads)
        {
            Human partA = null;

            if (CityData.Instance.GetHuman(thread.participantA, out partA))
            {
                if (!GameplayController.Instance.messageThreads.ContainsKey(thread.threadID))
                {
                    GameplayController.Instance.messageThreads.Add(thread.threadID, thread);
                }
                else GameplayController.Instance.messageThreads[thread.threadID] = thread;

                if (!partA.messageThreadsStarted.Contains(thread)) partA.messageThreadsStarted.Add(thread);
                if (!partA.messageThreadFeatures.Contains(thread)) partA.messageThreadFeatures.Add(thread);

                //Game.Log("Loaded vmail message for citizen: " + CityData.Instance.citizenDictionary[thread.participantA].messageThreadsStarted.Count);

                if (thread.participantB > -1)
                {
                    Human partB = null;

                    if (CityData.Instance.GetHuman(thread.participantB, out partB))
                    {
                        if (!partB.messageThreadFeatures.Contains(thread))
                        {
                            partB.messageThreadFeatures.Add(thread);
                        }
                    }
                    else
                    {
                        Game.LogError("CityGen: Cannot find participant B " + thread.participantA + " for vmail");
                    }
                }

                if (thread.participantC > -1)
                {
                    Human partC = null;

                    if (CityData.Instance.GetHuman(thread.participantC, out partC))
                    {
                        if (!partC.messageThreadFeatures.Contains(thread))
                        {
                            partC.messageThreadFeatures.Add(thread);
                        }
                    }
                    else
                    {
                        Game.LogError("CityGen: Cannot find participant C " + thread.participantA + " for vmail");
                    }
                }

                if (thread.participantD > -1)
                {
                    Human partD = null;

                    if (CityData.Instance.GetHuman(thread.participantD, out partD))
                    {
                        if (!partD.messageThreadFeatures.Contains(thread))
                        {
                            partD.messageThreadFeatures.Add(thread);
                        }
                    }
                    else
                    {
                        Game.LogError("CityGen: Cannot find participant D " + thread.participantA + " for vmail");
                    }
                }

                //Load CC'd members of the thread
                foreach (int i in thread.cc)
                {
                    Human cc = null;

                    if (CityData.Instance.GetHuman(i, out cc))
                    {
                        if (!cc.messageThreadCCd.Contains(thread))
                        {
                            cc.messageThreadCCd.Add(thread);
                        }
                    }
                }
            }
            else
            {
                Game.LogError("CityGen: Cannot find participant A " + thread.participantA + " for vmail");
            }
        }

        //Load murders
        if (load.currentMurderer > -1) CityData.Instance.GetHuman(load.currentMurderer, out MurderController.Instance.currentMurderer);
        if (load.currentVictim > -1) CityData.Instance.GetHuman(load.currentVictim, out MurderController.Instance.currentVictim);
        if (load.murderPreset != null && load.murderPreset.Length > 0) Toolbox.Instance.LoadDataFromResources<MurderPreset>(load.murderPreset, out MurderController.Instance.murderPreset);
        if (load.chosenMO != null && load.chosenMO.Length > 0) Toolbox.Instance.LoadDataFromResources<MurderMO>(load.chosenMO, out MurderController.Instance.chosenMO);

        MurderController.Instance.pauseBetweenMurders = load.pauseBetweenMurders;
        MurderController.Instance.murderRoutineActive = load.murderRoutineActive;
        MurderController.Instance.maxDifficultyLevel = load.maxMurderDiffLevel;

        MurderController.Instance.previousMurderers = new List<Human>();

        foreach (int i in load.previousMurderers)
        {
            Human prev = null;

            if (CityData.Instance.GetHuman(i, out prev))
            {
                MurderController.Instance.previousMurderers.Add(prev);
            }
        }

        foreach (MurderController.Murder m in MurderController.Instance.activeMurders)
        {
            m.LoadSerializedData();
        }

        foreach (MurderController.Murder m in MurderController.Instance.inactiveMurders)
        {
            m.LoadSerializedData();
        }

        //Load meta objects
        foreach (MetaObject mo in load.metas)
        {
            if (!CityData.Instance.metaObjectDictionary.ContainsKey(mo.id))
            {
                CityData.Instance.metaObjectDictionary.Add(mo.id, mo);
            }
        }

        //Load interactables
        foreach (Interactable i in load.interactables)
        {
            i.wasLoadedFromSave = true;

            //We should be left with un-created interactables at this point, as existing ones should be loaded when loading the main data...
            i.MainSetupStart();
            i.OnLoad();
        }

        //Load jobs after interactables
        Game.Log("Jobs: Loading " + load.affairJobs.Count + " affair jobs...");

        foreach (SideJobAffair job in load.affairJobs)
        {
            LoadJob(job);
        }

        Game.Log("Jobs: Loading " + load.sabotageJobs.Count + " sabotage jobs...");

        foreach (SideJobSabotage job in load.sabotageJobs)
        {
            LoadJob(job);
        }

        Game.Log("Jobs: Loading " + load.stolenItemJobs.Count + " stolen item jobs...");

        foreach (SideJobStolenItem job in load.stolenItemJobs)
        {
            LoadJob(job);
        }

        Game.Log("Jobs: Loading " + load.missingPersonJobs.Count + " missing person jobs...");

        foreach (SideJobMissingPerson job in load.missingPersonJobs)
        {
            LoadJob(job);
        }

        Game.Log("Jobs: Loading " + load.revengeJobs.Count + " revenge jobs...");

        foreach (SideJobRevenge job in load.revengeJobs)
        {
            LoadJob(job);
        }

        Game.Log("Jobs: Loading " + load.briefcaseJobs.Count + " briefcase jobs...");

        foreach (SideJobStealBriefcase job in load.briefcaseJobs)
        {
            LoadJob(job);
        }

        Game.Log("Jobs: Loading " + load.basicJobs.Count + " basic jobs...");

        foreach (SideJob job in load.basicJobs)
        {
            LoadJob(job);
        }

        //Run a creation check to update all data...
        SideJobController.Instance.JobCreationCheck();

        //Load vmails //Changed to load before interactables
        //Game.Log("CityGen: Loading " + load.messageThreads.Count + " vmail threads...");

        //foreach (StateSaveData.MessageThreadSave thread in load.messageThreads)
        //{
        //    Human partA = null;

        //    if(CityData.Instance.GetHuman(thread.participantA, out partA))
        //    {
        //        if (!GameplayController.Instance.messageThreads.ContainsKey(thread.threadID))
        //        {
        //            GameplayController.Instance.messageThreads.Add(thread.threadID, thread);
        //        }
        //        else GameplayController.Instance.messageThreads[thread.threadID] = thread;

        //        if(!partA.messageThreadsStarted.Contains(thread)) partA.messageThreadsStarted.Add(thread);
        //        if (!partA.messageThreadFeatures.Contains(thread)) partA.messageThreadFeatures.Add(thread);

        //        //Game.Log("Loaded vmail message for citizen: " + CityData.Instance.citizenDictionary[thread.participantA].messageThreadsStarted.Count);

        //        if (thread.participantB > -1)
        //        {
        //            Human partB = null;

        //            if(CityData.Instance.GetHuman(thread.participantB, out partB))
        //            {
        //                if(!partB.messageThreadFeatures.Contains(thread))
        //                {
        //                    partB.messageThreadFeatures.Add(thread);
        //                }
        //            }
        //            else
        //            {
        //                Game.LogError("CityGen: Cannot find participant B " + thread.participantA + " for vmail");
        //            }
        //        }

        //        if (thread.participantC > -1)
        //        {
        //            Human partC = null;

        //            if (CityData.Instance.GetHuman(thread.participantC, out partC))
        //            {
        //                if (!partC.messageThreadFeatures.Contains(thread))
        //                {
        //                    partC.messageThreadFeatures.Add(thread);
        //                }
        //            }
        //            else
        //            {
        //                Game.LogError("CityGen: Cannot find participant C " + thread.participantA + " for vmail");
        //            }
        //        }

        //        if (thread.participantD > -1)
        //        {
        //            Human partD = null;

        //            if (CityData.Instance.GetHuman(thread.participantD, out partD))
        //            {
        //                if (!partD.messageThreadFeatures.Contains(thread))
        //                {
        //                    partD.messageThreadFeatures.Add(thread);
        //                }
        //            }
        //            else
        //            {
        //                Game.LogError("CityGen: Cannot find participant D " + thread.participantA + " for vmail");
        //            }
        //        }

        //        //Load CC'd members of the thread
        //        foreach (int i in thread.cc)
        //        {
        //            Human cc = null;

        //            if(CityData.Instance.GetHuman(i, out cc))
        //            {
        //                if(!cc.messageThreadCCd.Contains(thread))
        //                {
        //                    cc.messageThreadCCd.Add(thread);
        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        Game.LogError("CityGen: Cannot find participant A " + thread.participantA + " for vmail");
        //    }
        //}

        //Load cases
        CasePanelController.Instance.activeCases = new List<Case>(load.activeCases);
        CasePanelController.Instance.archivedCases = new List<Case>(load.archivedCases);

        foreach (Case c in CasePanelController.Instance.activeCases)
        {
            //Link jobs with cases
            if(c.caseType == Case.CaseType.sideJob && c.jobReference > -1)
            {
                if(SideJobController.Instance.allJobsDictionary.TryGetValue(c.jobReference, out c.job))
                {
                    c.job.thisCase = c; //Set reverse link
                    Game.Log("Jobs: Loading objectives for job " + c.job.jobID);
                }
            }

            //Setup inactive objectives
            List<Objective> ended = new List<Objective>(c.endedObjectives);

            foreach (Objective obj in ended)
            {
                obj.Setup(c);
            }

            foreach (Objective obj in c.inactiveCurrentObjectives)
            {
                obj.Setup(c);
            }

            //Setup active objectives
            foreach (Objective obj in c.currentActiveObjectives)
            {
                obj.Setup(c);
            }

            if(c.id == load.currentActiveCase)
            {
                MurderController.Instance.currentActiveCase = c;
            }
        }

        foreach(Case c in CasePanelController.Instance.archivedCases)
        {
            //Link jobs with cases
            if (c.caseType == Case.CaseType.sideJob && c.jobReference > -1)
            {
                if (SideJobController.Instance.allJobsDictionary.TryGetValue(c.jobReference, out c.job))
                {
                    c.job.thisCase = c; //Set reverse link
                }
            }

            if (c.id == load.currentActiveCase)
            {
                MurderController.Instance.currentActiveCase = c;
            }
        }

        //Execute loop
        MurderController.Instance.SetProcGenKillerLoop(load.pgLoop);

        //Load footprints
        foreach (GameplayController.Footprint p in load.footprints)
        {
            NewRoom getRoom = null;

            if(CityData.Instance.roomDictionary.TryGetValue(p.rID, out getRoom))
            {
                if(!GameplayController.Instance.activeFootprints.ContainsKey(getRoom))
                {
                    GameplayController.Instance.activeFootprints.Add(getRoom, new List<GameplayController.Footprint>());
                }

                GameplayController.Instance.activeFootprints[getRoom].Add(p);
                GameplayController.Instance.footprintsList.Add(p);
            }
        }

        //Load History
        GameplayController.Instance.history = new List<GameplayController.History>(load.history);

        //Load passcodes
        GameplayController.Instance.acquiredPasscodes = new List<GameplayController.Passcode>();

        //We want the actual references to citizen/room/address so parse them here
        foreach(GameplayController.Passcode code in load.passcodes)
        {
            if(code.type == GameplayController.PasscodeType.citizen)
            {
                Human found = null;

                if(CityData.Instance.GetHuman(code.id, out found))
                {
                    GameplayController.Instance.acquiredPasscodes.Add(found.passcode);
                }
            }
            else if (code.type == GameplayController.PasscodeType.address)
            {
                NewAddress found = null;

                if (CityData.Instance.addressDictionary.TryGetValue(code.id, out found))
                {
                    GameplayController.Instance.acquiredPasscodes.Add(found.passcode);
                }
            }
            else if (code.type == GameplayController.PasscodeType.room)
            {
                NewRoom found = null;

                if (CityData.Instance.roomDictionary.TryGetValue(code.id, out found))
                {
                    GameplayController.Instance.acquiredPasscodes.Add(found.passcode);
                }
            }
            else if (code.type == GameplayController.PasscodeType.interactable)
            {
                Interactable interactable = CityData.Instance.interactableDirectory.Find(item => item.id == code.id);

                if (interactable != null)
                {
                    GameplayController.Instance.acquiredPasscodes.Add(interactable.passcode);
                }
            }
        }

        //Load numbers
        GameplayController.Instance.acquiredNumbers = new List<GameplayController.PhoneNumber>(load.numbers);

        //Parse item history
        for (int i = 0; i < GameplayController.Instance.history.Count; i++)
        {
            GameplayController.History h = GameplayController.Instance.history[i];

            Evidence ev = null;

            if(GameplayController.Instance.evidenceDictionary.TryGetValue(h.evID, out ev))
            {
                if(ev.interactable != null && ev.interactable.preset.spawnable)
                {
                    GameplayController.Instance.itemOnlyHistory.Add(h);
                }
            }
        }

        //Load enforcer calls
        foreach(GameplayController.EnforcerCall call in load.enforcerCalls)
        {
            NewGameLocation loc = null;

            if(call.isStreet)
            {
                loc = CityData.Instance.streetDirectory.Find(item => item.streetID == call.id);
            }
            else
            {
                loc = CityData.Instance.addressDictionary[call.id];
            }

            GameplayController.Instance.enforcerCalls.Add(loc, call);

            loc.SetAsCrimeScene(call.isCrimeScene);

            if(call.isCrimeScene)
            {
                loc.loggedAsCrimeScene = call.logTime;
            }
        }

        //Player data
        Player.Instance.isCrouched = load.crouched;
        Player.Instance.OnCrouchedChange();

        Player.Instance.SetPosition(load.playerPos, load.playerRot);
        GameplayController.Instance.SetMoney(load.money);
        GameplayController.Instance.SetLockpicks(load.lockpicks);
        GameplayController.Instance.SetSocialCredit(load.socCredit);
        Player.Instance.SetHealth(load.health);

        Player.Instance.nourishment = load.nourishment;
        Player.Instance.hydration = load.hydration;
        Player.Instance.alertness = load.alertness;
        Player.Instance.energy = load.energy;
        Player.Instance.hygiene = load.hygiene;
        Player.Instance.heat = load.heat;
        Player.Instance.drunk = load.drunk;
        Player.Instance.sick = load.sick;
        Player.Instance.headache = load.headache;
        Player.Instance.wet = load.wet;
        Player.Instance.brokenLeg = load.brokenLeg;
        Player.Instance.bruised = load.bruised;
        Player.Instance.blackEye = load.blackEye;
        Player.Instance.blackedOut = load.blackedOut;
        Player.Instance.numb = load.numb;
        Player.Instance.poisoned = load.poisoned;
        Player.Instance.bleeding = load.bleeding;
        Player.Instance.wellRested = load.wellRested;
        Player.Instance.starchAddiction = load.starchAddiction;
        Player.Instance.syncDiskInstall = load.syncDiskInstall;
        Player.Instance.blinded = load.blinded;

        //Load player speech
        if (load.speech.Count > 0)
        {
            Player.Instance.speechController.speechQueue.AddRange(load.speech);

            //Make sure triggers contain non serialized data
            foreach(SpeechController.QueueElement element in Player.Instance.speechController.speechQueue)
            {
                foreach(Objective.ObjectiveTrigger trg in element.triggers)
                {
                    trg.SetupNonSerialized();
                }
            }

            //Start coroutine
            Player.Instance.speechController.enabled = true;
        }

        //Upgrades
        UpgradesController.Instance.upgrades.Clear();
        GameplayController.Instance.companiesSabotaged = new List<string>(load.sabotaged);
        GameplayController.Instance.booksRead = new List<string>(load.booksRead);

        //Merge saved upgrades; merge the sources with existing which will have been created on setup prior to this...
        foreach (UpgradesController.Upgrades up in load.upgrades)
        {
            UpgradesController.Instance.upgrades.Add(up);
        }

        //UpgradesController.Instance.UpdateUpgrades(); //Do this after load of inventory now

        //Keyring
        foreach (int d in load.keyring)
        {
            if(CityData.Instance.doorDictionary.ContainsKey(d))
            {
                Player.Instance.AddToKeyring(CityData.Instance.doorDictionary[d], false);
            }
        }

        foreach (int d in load.keyringInt)
        {
            Interactable i = null;

            if(CityData.Instance.savableInteractableDictionary.TryGetValue(d, out i))
            {
                Player.Instance.AddToKeyring(i, false);
            }
            else
            {
                i = CityData.Instance.interactableDirectory.Find(item => item.id == d);

                if (i != null) Player.Instance.AddToKeyring(i, false);
            }
        }

        //Load fake numbers
        foreach (StateSaveData.FakeTelephone ft in load.fakeTelephone)
        {
            TelephoneController.Instance.AddFakeNumber(ft.number, ft.source);
        }

        //Load building state
        foreach (StateSaveData.BuildingStateSav b in load.buildings)
        {
            NewBuilding foundBuilding = CityData.Instance.buildingDirectory.Find(item => item.buildingID == b.id);

            if (foundBuilding != null)
            {
                foundBuilding.SetAlarm(b.alarmActive, null, null);
                foundBuilding.alarmTimer = b.alarmTimer;
                foundBuilding.targetMode = b.targetMode;
                foundBuilding.wantedInBuilding = b.wanted;

                foreach(StateSaveData.ElevatorStateSave el in b.elevators)
                {
                    //Find the correct tile (bottom)
                    foreach(KeyValuePair<NewTile, Elevator> pair in foundBuilding.stairwells)
                    {
                        if(pair.Key.tileID == el.tileID)
                        {
                            pair.Value.LoadElevatorSaveData(el);
                        }
                    }
                }
                
                foreach(int i in b.targets)
                {
                    Human t = null;

                    if(CityData.Instance.GetHuman(i, out t))
                    {
                        foundBuilding.alarmTargets.Add(t);
                    }
                }

                foundBuilding.callLog = new List<TelephoneController.PhoneCall>(b.callLog);

                foreach(TelephoneController.PhoneCall pc in foundBuilding.callLog)
                {
                    pc.SetupNonSerializedData();
                }

                foundBuilding.lostAndFound = new List<GameplayController.LostAndFound>(b.lostAndFound);
            }

            //Load floor state
            foreach (StateSaveData.FloorStateSave fl in load.floors)
            {
                foreach (KeyValuePair<int, NewFloor> pair in foundBuilding.floors)
                {
                    if (pair.Value.floorID == fl.id)
                    {
                        pair.Value.SetAlarmLockdown(fl.alarmLockdown);
                    }
                }
            }
        }

        //Load address state
        foreach(StateSaveData.AddressStateSave ad in load.addresses)
        {
            NewAddress found = null;

            if(CityData.Instance.addressDictionary.TryGetValue(ad.id, out found))
            {
                found.SetAlarm(ad.alarmActive, null);
                found.alarmTimer = ad.alarmTimer;

                found.targetMode = ad.targetMode;
                found.targetModeSetAt = ad.targetModeSetAt;

                if (ad.targetMode != NewBuilding.AlarmTargetMode.illegalActivities)
                {
                    if (!GameplayController.Instance.alteredSecurityTargetsLocations.Contains(found))
                    {
                        GameplayController.Instance.alteredSecurityTargetsLocations.Add(found);
                    }
                }

                //Load sale note
                if (ad.sale > -1)
                {
                    CityData.Instance.savableInteractableDictionary.TryGetValue(ad.sale, out found.saleNote);
                }

                //Load vandalism
                found.vandalism = new List<NewAddress.Vandalism>(ad.vandalism);

                foreach (int i in ad.targets)
                {
                    Human t = null;

                    if (CityData.Instance.GetHuman(i, out t))
                    {
                        found.alarmTargets.Add(t);
                    }
                }

                foreach(NewGameLocation.TrespassEscalation esc in ad.escalation)
                {
                    if(esc.isPlayer)
                    {
                        found.escalation.Add(Player.Instance, esc);

                        if (!CitizenBehaviour.Instance.tempEscalationBoost.Contains(found))
                        {
                            CitizenBehaviour.Instance.tempEscalationBoost.Add(found);
                        }
                    }
                    else
                    {
                        Human getHuman = null;

                        if(CityData.Instance.GetHuman(esc.actor, out getHuman))
                        {
                            found.escalation.Add(getHuman, esc);

                            if (!CitizenBehaviour.Instance.tempEscalationBoost.Contains(found))
                            {
                                CitizenBehaviour.Instance.tempEscalationBoost.Add(found);
                            }
                        }
                    }
                }
            }
        }

        //Load room state
        foreach (StateSaveData.RoomStateSave r in load.rooms)
        {
            NewRoom foundRoom = null;

            if (CityData.Instance.roomDictionary.TryGetValue(r.id, out foundRoom))
            {
                foundRoom.SetExplorationLevel(r.ex);
                foundRoom.SetMainLights(r.ml, "load game", null, true, true); //Set light status
                foundRoom.AddGas(r.gl);

                foundRoom.furnitureAssignID = r.fID;
                foundRoom.interactableAssignID = r.iID;

                if(r.decorOverride != null && r.decorOverride.Count > 0)
                {
                    foundRoom.decorEdit = true; //The decor has been edited since the citysave data
                }
            }
        }

        //Load company state
        foreach(StateSaveData.CompanyStateSave c in load.companies)
        {
            Company foundCompany = CityData.Instance.companyDirectory.Find(item => item.companyID == c.id);

            if(foundCompany != null)
            {
                foundCompany.sales = new List<Company.SalesRecord>(c.sales);

                foreach (Company.SalesRecord sr in foundCompany.sales)
                {
                    sr.SpawnFact();
                }
            }
        }

        //Load citizen state
        foreach (StateSaveData.CitizenStateSave h in load.citizens)
        {
            Human foundHuman = null;

            if (CityData.Instance.GetHuman(h.id, out foundHuman, includePlayer: false))
            {
                foundHuman.transform.position = h.pos;
                foundHuman.transform.rotation = h.rot;
                foundHuman.UpdateGameLocation();

                if(Game.Instance.devMode && foundHuman.ai != null) foundHuman.ai.debugDestinationPosition.Add("Load in position " + foundHuman.transform.position);

                foundHuman.trespassingEscalation = h.trespassingEscalation;
                foundHuman.outfitController.SetCurrentOutfit(h.currentOutfit);

                //Status
                foundHuman.nourishment = h.nourishment;
                foundHuman.hydration = h.hydration;
                foundHuman.alertness = h.alertness;
                foundHuman.energy = h.energy;
                foundHuman.excitement = h.excitement;
                foundHuman.chores = h.chores;
                foundHuman.hygiene = h.hygiene;
                foundHuman.bladder = h.bladder;
                foundHuman.heat = h.heat;
                foundHuman.drunk = h.drunk;
                foundHuman.breath = h.breath;
                foundHuman.poisoned = h.poisoned;
                foundHuman.blinded = h.blinded;

                if(h.poisoner > -1)
                {
                    CityData.Instance.GetHuman(h.poisoner, out foundHuman.poisoner);
                }

                foundHuman.currentWounds = new List<Human.Wound>(h.wounds);
                foreach (Human.Wound w in foundHuman.currentWounds) w.Load();

                foundHuman.fingerprintLoop = h.fingerprintLoop;

                foundHuman.SetHealth(h.currentHealth);
                foundHuman.SetNerve(h.currentNerve);

                foundHuman.footstepBlood = h.fsBlood;
                foundHuman.footstepDirt = h.fsDirt;

                if (h.currentConsumable != null && h.currentConsumable.Count > 0)
                {
                    foreach (string str in h.currentConsumable)
                    {
                        InteractablePreset cc = null;
                        Toolbox.Instance.LoadDataFromResources<InteractablePreset>(str, out cc);
                        foundHuman.AddCurrentConsumable(cc);
                    }
                }

                foundHuman.trash = new List<int>(h.trash);

                //Load dead
                foundHuman.unreportable = h.unreportable;
                foundHuman.death = h.death;

                if (h.death != null && h.death.isDead)
                {
                    Human k = null;
                    CityData.Instance.GetHuman(h.death.killer, out k);

                    foundHuman.death.isDead = true;
                    foundHuman.ai.SetKO(true);
                    foundHuman.Murder(k, false, null, null);

                    if(foundHuman.death != null)
                    {
                        MurderController.Murder m = foundHuman.death.GetMurder();
                        if (m != null) m.death = foundHuman.death;
                    }

                    foundHuman.ai.deadRagdollTimer = 9999999;
                    foundHuman.animationController.SetRagdoll(false, true);

                    //Load ragdoll snapshot
                    if (h.ragdollSnapshot != null)
                    {
                        foundHuman.animationController.LoadLimbSnapshot(h.ragdollSnapshot);
                    }

                    if(foundHuman.animationController.newBoxCollider != null)
                    {
                        Destroy(foundHuman.animationController.newBoxCollider);
                    }

                    //Add to dead citizens list
                    if(!CityData.Instance.deadCitizensDirectory.Contains(foundHuman))
                    {
                        CityData.Instance.deadCitizensDirectory.Add(foundHuman);
                    }
                }

                //AI
                if (foundHuman.ai != null)
                {
                    foundHuman.ai.lastUpdated = SessionData.Instance.gameTime;
                    foundHuman.ai.isConvicted = h.convicted;

                    if (h.putDown != null)
                    {
                        foreach (int i in h.putDown)
                        {
                            Interactable found = null;

                            if (CityData.Instance.savableInteractableDictionary.TryGetValue(i, out found))
                            {
                                foundHuman.ai.putDownItems.Add(found);
                            }
                        }
                    }

                    if (h.reactionState != NewAIController.ReactionState.none)
                    {
                        NewNode investigateNode = null;
                        Human target = null;

                        if (h.persuit)
                        {
                            if (h.persuitPlayer)
                            {
                                target = Player.Instance;
                            }
                            else
                            {
                                Human citTarget = null;
                                CityData.Instance.GetHuman(h.persuitTarget, out citTarget);
                                if (citTarget != null)
                                {
                                    target = citTarget as Human;
                                }
                            }

                            foundHuman.ai.SetPersue(target, false, h.escalationLevel, true);

                            foundHuman.ai.seesOnPersuit = h.seesPlayerOnPersuit; //Only true when actively seeing the player
                            foundHuman.ai.persuitChaseLogicUses = h.persuitChaseLogicUses; //Each time a player is spotted, it gets one lead on to where the player has disappeared to...
                        }

                        if (PathFinder.Instance.nodeMap.TryGetValue(h.investigateLocation, out investigateNode))
                        {
                            foundHuman.ai.Investigate(investigateNode, h.investigatePosition, target, h.reactionState, h.minimumInvestigationTimeMultiplier, h.escalationLevel);
                        }

                        foundHuman.ai.investigatePositionProjection = h.investigatePositionProjection; //Projection of where the current target will be (2m ahead of now)
                        foundHuman.ai.lastInvestigate = h.lastInvestigate;
                    }

                    //foundHuman.ai.timesSpottedMinorCrime = h.timesSpottedMinorCrime; //Count this up as player is spotted trespassing (minor). Used to turn into a serious crime.

                    //Load current goal
                    if (h.currentGoal != null)
                    {
                        NewAIGoal foundGoal = foundHuman.ai.goals.Find(item => item.preset.name == h.currentGoal.preset && !item.preset.disableSave); //Find instance of this goal

                        if(foundGoal == null)
                        {
                            //Game.LogError("Unable to find goal " + h.currentGoal.preset + " in " + foundHuman.name + ", attempting to create...");

                            AIGoalPreset gp = Toolbox.Instance.allGoals.Find(item => item.name == h.currentGoal.preset);

                            if (gp != null && !gp.disableSave)
                            {
                                NewNode passedNode = null;
                                PathFinder.Instance.nodeMap.TryGetValue(h.currentGoal.passedNode, out passedNode);

                                Interactable passedInteractable = null;

                                if(h.currentGoal.passedInteractable > -1 && !CityData.Instance.savableInteractableDictionary.TryGetValue(h.currentGoal.passedInteractable, out passedInteractable))
                                {
                                    passedInteractable = CityData.Instance.interactableDirectory.Find(item => item.id == h.currentGoal.passedInteractable);
                                }

                                NewGameLocation passedGameLocation = null;

                                if (h.currentGoal.gameLocation > -1)
                                {
                                    if (h.currentGoal.isAddress)
                                    {
                                        passedGameLocation = CityData.Instance.addressDirectory.Find(item => item.id == h.currentGoal.gameLocation);
                                    }
                                    else
                                    {
                                        passedGameLocation = CityData.Instance.streetDirectory.Find(item => item.streetID == h.currentGoal.gameLocation);
                                    }
                                }

                                GroupsController.SocialGroup passedGroup = null;

                                if (h.currentGoal.passedGroup > -1)
                                {
                                    passedGroup = GroupsController.Instance.groups.Find(item => item.id == h.currentGoal.passedGroup);
                                }

                                foundGoal = foundHuman.ai.CreateNewGoal(gp, h.currentGoal.trigerTime, h.currentGoal.duration, passedNode, passedInteractable, passedGameLocation, passedGroup, null, h.currentGoal.var);

                                if (h.currentGoal.room > -1)
                                {
                                    CityData.Instance.roomDictionary.TryGetValue(h.currentGoal.room, out foundGoal.roomLocation);
                                }
                            }
                        }

                        if (foundGoal != null)
                        {
                            foundHuman.ai.currentGoal = foundGoal;

                            foundGoal.triggerTime = h.currentGoal.trigerTime;
                            foundGoal.duration = h.currentGoal.duration;
                            PathFinder.Instance.nodeMap.TryGetValue(h.currentGoal.passedNode, out foundGoal.passedNode);

                            CityData.Instance.savableInteractableDictionary.TryGetValue(h.currentGoal.passedInteractable, out foundGoal.passedInteractable);

                            foundGoal.priority = h.currentGoal.priority;
                            foundGoal.passedVar = h.currentGoal.var;
                            foundGoal.isActive = true;
                            foundGoal.activeTime = h.currentGoal.activeTime;

                            if(h.currentGoal.gameLocation > -1)
                            {
                                if (h.currentGoal.isAddress)
                                {
                                    foundGoal.gameLocation = CityData.Instance.addressDirectory.Find(item => item.id == h.currentGoal.gameLocation);
                                }
                                else
                                {
                                    foundGoal.gameLocation = CityData.Instance.streetDirectory.Find(item => item.streetID == h.currentGoal.gameLocation);
                                }
                            }

                            if (h.currentGoal.passedGroup > -1)
                            {
                                foundGoal.passedGroup = GroupsController.Instance.groups.Find(item => item.id == h.currentGoal.passedGroup);
                            }

                            foundGoal.jobID = h.currentGoal.jobID;

                            //Create actions
                            foreach (StateSaveData.AIActionStateSave act in h.currentGoal.actions)
                            {
                                Interactable passedInteractable = null;
                                CityData.Instance.savableInteractableDictionary.TryGetValue(act.passedInteractable, out passedInteractable);

                                NewRoom roomLocation = null;
                                if (act.passedRoom > -1) CityData.Instance.roomDictionary.TryGetValue(act.passedRoom, out roomLocation);

                                NewNode forcedNode = null;
                                if (act.forcedNode != null) PathFinder.Instance.nodeMap.TryGetValue(act.forcedNode, out forcedNode);

                                AIActionPreset actPreset = null;
                                Toolbox.Instance.LoadDataFromResources<AIActionPreset>(act.preset, out actPreset);

                                GroupsController.SocialGroup passedGroup = null;

                                if (act.passedGroup > -1)
                                {
                                    passedGroup = GroupsController.Instance.groups.Find(item => item.id == act.passedGroup);
                                }

                                NewAIAction newAction = new NewAIAction(foundGoal, actPreset, newPassedInteractable: passedInteractable, newPassedRoom: roomLocation, newForcedNode: forcedNode, newPassedGroup: passedGroup);

                                newAction.repeat = act.repeat;

                                Interactable interactable = null;
                                CityData.Instance.savableInteractableDictionary.TryGetValue(act.interactable, out interactable);
                                newAction.interactable = interactable;
                            }
                        }
                    }

                    foundHuman.walletItems = new List<Human.WalletItem>(h.wallet);

                    //Dead

                    //KO'd
                    if (h.ko)
                    {
                        foundHuman.ai.SetKO(h.ko, forced: h.ko, forcedDuration: h.koTime - SessionData.Instance.gameTime);
                    }

                    foundHuman.ai.spooked = h.spooked;
                    foundHuman.ai.spookCounter = h.spookCount;

                    //restrained
                    if (h.res)
                    {
                        foundHuman.ai.SetRestrained(h.res, h.resTime - SessionData.Instance.gameTime);
                    }

                    //Removed from world
                    if(h.remFromWorld)
                    {
                        foundHuman.RemoveFromWorld(true);
                    }

                    //Sightings
                    if(h.sightingCit != null)
                    {
                        for (int i = 0; i < h.sightingCit.Count; i++)
                        {
                            Human hu = null;

                            if(CityData.Instance.GetHuman(h.sightingCit[i], out hu))
                            {
                                if(!foundHuman.lastSightings.ContainsKey(hu))
                                {
                                    foundHuman.lastSightings.Add(hu, h.sightings[i]);
                                }
                                else foundHuman.lastSightings[hu] = h.sightings[i];
                            }
                        }
                    }

                    if(h.confine != null)
                    {
                        if(h.confine.st)
                        {
                            foundHuman.ai.confineLocation = CityData.Instance.streetDirectory.Find(item => item.streetID == h.confine.id);
                        }
                        else
                        {
                            foundHuman.ai.confineLocation = CityData.Instance.addressDirectory.Find(item => item.id == h.confine.id);
                        }
                    }

                    if(h.avoid != null)
                    {
                        foreach(StateSaveData.AvoidConfineStateSave d in h.avoid)
                        {
                            if (d.st)
                            {
                                foundHuman.ai.avoidLocations.Add(CityData.Instance.streetDirectory.Find(item => item.streetID == d.id));
                            }
                            else
                            {
                                foundHuman.ai.avoidLocations.Add(CityData.Instance.addressDirectory.Find(item => item.id == d.id));
                            }
                        }
                    }

                    //Update weapon of choice
                    foundHuman.ai.UpdateCurrentWeapon();

                    if (foundHuman.ai.isConvicted)
                    {
                        foundHuman.RemoveFromWorld(true);
                    }
                }
            }
        }

        //Load door state
        foreach (StateSaveData.DoorStateSave d in load.doors)
        {
            NewDoor foundDoor = null;

            if (CityData.Instance.doorDictionary.TryGetValue(d.id, out foundDoor))
            {
                //Game.Log("Loaded status door " + d.id);
                if(Game.Instance.collectDebugData) foundDoor.isLockedDebug.Add("Loaded save state for door ID " + d.id + ", locked: " + d.l);
                foundDoor.wall.SetCurrentLockStrength(d.ls);
                foundDoor.wall.SetDoorStrength(d.ds);
                foundDoor.SetLocked(d.l, null, false);
                foundDoor.SetOpen(d.ajar, null, true);
                if(d.cs) foundDoor.SetForbidden(d.cs);
            }
        }

        //Load evidence state
        //Load multi page evidence first...
        foreach (StateSaveData.EvidenceStateSave ev in load.evidence)
        {
            Evidence found = null;

            if (Toolbox.Instance.TryGetEvidence(ev.id, out found))
            {
                if(found != null)
                {
                    if(ev.dds != null) found.SetOverrideDDS(ev.dds);
                    found.SetFound(ev.found);
                    found.SetForceSave(ev.fs);

                    if (ev.mpContent != null && ev.mpContent.Count > 0)
                    {
                        EvidenceMultiPage mp = found as EvidenceMultiPage;
                        //List<EvidenceMultiPage.MultiPageContent> metaObjectContent = ev.mpContent.FindAll(item => item.meta != null);
                        //Game.Log("Loading multipage content: " + ev.mpContent.Count + " (" + metaObjectContent.Count + " meta objects)");

                        if(mp != null) mp.pageContent = new List<EvidenceMultiPage.MultiPageContent>(ev.mpContent);
                    }
                }
            }
        }

        foreach (StateSaveData.EvidenceStateSave ev in load.evidence)
        {
            Evidence found = null;

            if(Toolbox.Instance.TryGetEvidence(ev.id, out found))
            {
                if(found != null)
                {
                    if(ev.dds != null) found.SetOverrideDDS(ev.dds);
                    found.SetFound(ev.found);
                    found.SetForceSave(ev.fs);
                    found.customNames = new List<Evidence.CustomName>(ev.customName);

                    if(found.preset.useDataKeys)
                    {
                        foreach(StateSaveData.EvidenceDataKeyTie tie in ev.keyTies)
                        {
                            foreach(Evidence.DataKey dk in tie.tied)
                            {
                                found.MergeDataKeys(tie.key, dk);
                            }
                        }
                    }

                    foreach(Evidence.Discovery dis in ev.discovery)
                    {
                        found.AddDiscovery(dis);
                    }

                    //Load note
                    Evidence stickyNote = found as EvidenceStickyNote;

                    if(stickyNote != null)
                    {
                        stickyNote.SetNote((new Evidence.DataKey[] { Evidence.DataKey.name }).ToList(), ev.n);
                    }
                }
            }
            else
            {
                Game.LogError("Cannot find evidence ID " + ev.id);
            }
        }

        //Load active case after all evidence is loaded
        if (load.activeCase > -1) CasePanelController.Instance.SetActiveCase(CasePanelController.Instance.activeCases.Find(item => item.id == load.activeCase));

        ////Compose pinned evidence in order to generate facts connected with 'makes reference to'
        ////Move this until after story is loaded...
        //foreach (Case c in CasePanelController.Instance.activeCases)
        //{
        //    foreach(Case.CaseElement ce in c.caseElements)
        //    {
        //        if(ce.id != null && ce.id.Length > 0)
        //        {
        //            Evidence found = null;

        //            if (Toolbox.Instance.TryGetEvidence(ce.id, out found))
        //            {
        //                if(found.overrideDDS != null && found.overrideDDS.Length > 0)
        //                {
        //                    DDSSaveClasses.DDSTreeSave content;

        //                    //Get DDS content
        //                    if (Toolbox.Instance.allDDSTrees.TryGetValue(found.overrideDDS, out content))
        //                    {
        //                        //Spawn objects
        //                        foreach (DDSSaveClasses.DDSMessageSettings msg in content.messages)
        //                        {
        //                            Strings.GetTextForComponent(msg.msgID, found.interactable, found.writer, found.reciever);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        ////Load facts
        //foreach(string str in load.customStrings)
        //{
        //    EvidenceCreator.Instance.CreateFactFromSerializedString(str);
        //}

        //foreach (Case c in CasePanelController.Instance.archivedCases)
        //{
        //    foreach (Case.CaseElement ce in c.caseElements)
        //    {
        //        if (ce.id != null && ce.id.Length > 0)
        //        {
        //            Evidence found = null;

        //            if (Toolbox.Instance.TryGetEvidence(ce.id, out found))
        //            {
        //                if (found.overrideDDS != null && found.overrideDDS.Length > 0)
        //                {
        //                    DDSSaveClasses.DDSTreeSave content;

        //                    //Get DDS content
        //                    if (Toolbox.Instance.allDDSTrees.TryGetValue(found.overrideDDS, out content))
        //                    {
        //                        //Spawn objects
        //                        foreach (DDSSaveClasses.DDSMessageSettings msg in content.messages)
        //                        {
        //                            Strings.GetTextForComponent(msg.msgID, found.interactable, found.writer, found.reciever);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        //Load guest passes
        foreach (StateSaveData.GuestPassStateSave saveState in load.guestPasses)
        {
            GameplayController.Instance.AddGuestPass(CityData.Instance.addressDirectory.Find(item => item.id == saveState.id), saveState.guestPassUntil);
        }

        //Load broken windows
        foreach(StateSaveData.BrokenWindowSave saveState in load.brokenWindows)
        {
            GameplayController.Instance.brokenWindows.Add(saveState.pos, saveState.brokenAt);
        }

        //Load crime scene cleanup pool
        foreach(StateSaveData.CrimeSceneCleanup cs in load.crimeSceneCleanup)
        {
            if(cs.isStreet)
            {
                StreetController s = CityData.Instance.streetDirectory.Find(item => item.streetID == cs.id);

                if(s != null)
                {
                    NewGameLocation gl = s as NewGameLocation;

                    if(!GameplayController.Instance.crimeSceneCleanups.Contains(gl))
                    {
                        GameplayController.Instance.crimeSceneCleanups.Add(gl);
                    }
                }
            }
            else
            {
                NewAddress s = null;
                
                if (CityData.Instance.addressDictionary.TryGetValue(cs.id, out s))
                {
                    NewGameLocation gl = s as NewGameLocation;

                    if (!GameplayController.Instance.crimeSceneCleanups.Contains(gl))
                    {
                        GameplayController.Instance.crimeSceneCleanups.Add(gl);
                    }
                }
            }
        }

        //Load hotel guests
        GameplayController.Instance.hotelGuests = new List<GameplayController.HotelGuest>(load.hotelGuests);

        //Load hotel guest location of authority
        foreach(GameplayController.HotelGuest g in GameplayController.Instance.hotelGuests)
        {
            g.FromLoadGame();
        }

        //Set char controller height
        if (!Game.Instance.freeCam)
        {
            Player.Instance.SetPlayerHeight(Player.Instance.GetPlayerHeightNormal());
            Player.Instance.SetCameraHeight(GameplayControls.Instance.cameraHeightNormal);
        }

        //Load in air duct
        if (load.duct > -1)
        {
            Game.Log("Player: Loading player in vent: " + load.duct + " (" + CityData.Instance.airDuctGroupDirectory.Count + ")");
            AirDuctGroup duct = CityData.Instance.airDuctGroupDirectory.Find(item => item.ductID == load.duct);

            if(duct != null)
            {
                //if (CityData.Instance.airVentDirectory.Count <= 0) Game.LogError("There are no air vent interactables loaded for the player to crawl into!");
                //Player.Instance.OnCrawlIntoVent(CityData.Instance.airVentDirectory[0].spawned.interactable, instant: true);

                //We just want *any* air vent for this
                Interactable airVent = CityData.Instance.interactableDirectory.Find(item => item.preset.specialCaseFlag == InteractablePreset.SpecialCase.airVent);
                if(airVent != null) Player.Instance.OnCrawlIntoVent(airVent, instant: true);

                Player.Instance.SetPosition(load.playerPos, load.playerRot); //Re set position to get culling/vent location right
            }
        }

        //Load map route
        if(load.mapPathActive)
        {
            NewNode mapNode = null;

            if(PathFinder.Instance.nodeMap.TryGetValue(load.mapPathNode, out mapNode))
            {
                MapController.Instance.PlotPlayerRoute(mapNode, load.mapPathNodeSpecific);
            }
        }

        //Load sandbox
        if (load.chapter < 0)
        {
            Game.Instance.sandboxMode = true;
        }
        //Load chapter part
        else
        {
            Game.Instance.sandboxMode = false;

            //Don't teleport, as we'll load position manually after this...
            ChapterController.Instance.LoadChapter(ChapterController.Instance.allChapters[load.chapter], false);

            //Skip to correct part
            ChapterController.Instance.SkipToChapterPart(load.chapterPart, false, false);
        }

        //Compose pinned evidence in order to generate facts connected with 'makes reference to'
        foreach (Case c in CasePanelController.Instance.activeCases)
        {
            foreach (Case.CaseElement ce in c.caseElements)
            {
                if (ce.id != null && ce.id.Length > 0)
                {
                    Evidence found = null;

                    if (Toolbox.Instance.TryGetEvidence(ce.id, out found))
                    {
                        if (found != null && found.overrideDDS != null && found.overrideDDS.Length > 0)
                        {
                            DDSSaveClasses.DDSTreeSave content;

                            //Get DDS content
                            if (Toolbox.Instance.allDDSTrees.TryGetValue(found.overrideDDS, out content))
                            {
                                //Spawn objects
                                foreach (DDSSaveClasses.DDSMessageSettings msg in content.messages)
                                {
                                    if(msg.msgID != null && msg.msgID.Length > 0)
                                    {
                                        Strings.GetTextForComponent(msg.msgID, found.interactable, found.writer, found.reciever);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //Load facts
        foreach (string str in load.customStrings)
        {
            EvidenceCreator.Instance.CreateFactFromSerializedString(str);
        }

        foreach (Case c in CasePanelController.Instance.archivedCases)
        {
            foreach (Case.CaseElement ce in c.caseElements)
            {
                if (ce.id != null && ce.id.Length > 0)
                {
                    Evidence found = null;

                    if (Toolbox.Instance.TryGetEvidence(ce.id, out found))
                    {
                        if (found != null && found.overrideDDS != null && found.overrideDDS.Length > 0)
                        {
                            DDSSaveClasses.DDSTreeSave content;

                            //Get DDS content
                            if (Toolbox.Instance.allDDSTrees.TryGetValue(found.overrideDDS, out content))
                            {
                                //Spawn objects
                                foreach (DDSSaveClasses.DDSMessageSettings msg in content.messages)
                                {
                                    Strings.GetTextForComponent(msg.msgID, found.interactable, found.writer, found.reciever);
                                }
                            }
                        }
                    }
                }
            }
        }

        //Load state before loading position
        if (load.hideInteractable > -1)
        {
            Interactable hideInter = null;

            if (CityData.Instance.savableInteractableDictionary.TryGetValue(load.hideInteractable, out hideInter))
            {
                Player.Instance.OnHide(hideInter, reference: load.hideRef, instant: true);
            }
        }
        else if (load.phoneInteractable > -1)
        {
            Interactable phoneInter = null;

            if (CityData.Instance.savableInteractableDictionary.TryGetValue(load.phoneInteractable, out phoneInter))
            {
                Player.Instance.OnAnswerPhone(phoneInter);
            }
        }
        else if (load.computerInteractable > -1)
        {
            Interactable compInter = null;

            if (CityData.Instance.savableInteractableDictionary.TryGetValue(load.computerInteractable, out compInter))
            {
                Player.Instance.OnUseComputer(compInter, instant: true);
            }
        }

        Player.Instance.storedTransitionPosition = load.storedTransPos;

        //Spatter
        GameplayController.Instance.spatter = new List<SpatterSimulation>(load.spatter);

        //Load furniture storage
        foreach (CitySaveData.FurnitureClusterObjectCitySave clust in load.furnitureStorage)
        {
            List<FurnitureClass> furnitureClasses = new List<FurnitureClass>();

            foreach (string str in clust.furnitureClasses)
            {
                FurnitureClass furnClass = null;
                Toolbox.Instance.LoadDataFromResources<FurnitureClass>(str, out furnClass);
                if (furnClass != null) furnitureClasses.Add(furnClass);
            }

            FurnitureLocation loadClust = new FurnitureLocation(clust.id, null, furnitureClasses, 0, null, null, false, newUserPlaced: true, newOffset: clust.offset); //Load and force ID

            Toolbox.Instance.LoadDataFromResources<FurniturePreset>(clust.furniture, out loadClust.furniture);

            if (clust.art.Length > 0)
            {
                Toolbox.Instance.LoadDataFromResources<ArtPreset>(clust.art, out loadClust.art);
                loadClust.pickedArt = true;
            }

            loadClust.matKey = clust.matKey;
            loadClust.artMatKey = clust.artMatKet;
            loadClust.pickedMaterials = true;

            PlayerApartmentController.Instance.furnitureStorage.Add(loadClust);
        }

        //Load in
        foreach (SpatterSimulation sp in GameplayController.Instance.spatter)
        {
            sp.LoadFromSerializedData();
        }

        //Load first person items
        UpgradesController.Instance.UpdateActivation();
        UpgradeEffectController.Instance.OnSyncDiskChange(true);
        FirstPersonItemController.Instance.SetSlotSize(GameplayControls.Instance.defaultInventorySlots + Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.increaseInventory)));

        foreach (FirstPersonItemController.InventorySlot fpi in load.firstPersonItems)
        {
            if(fpi.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic)
            {
                FirstPersonItemController.Instance.slots.Add(fpi);
            }
        }

        FirstPersonItemController.Instance.SetSlotSize(GameplayControls.Instance.defaultInventorySlots + Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.increaseInventory)));

        //Load scanned fingerprints from objects
        foreach(StateSaveData.ScannedObjPrint obj in load.scannedPrints)
        {
            Interactable baseObj = null;

            if(CityData.Instance.savableInteractableDictionary.TryGetValue(obj.objID, out baseObj))
            {
                List<Interactable> printObjs = new List<Interactable>();

                foreach (int i in obj.prints)
                {
                    Interactable p = null;

                    if (CityData.Instance.savableInteractableDictionary.TryGetValue(i, out p))
                    {
                        if(!BioScreenController.Instance.scannedObjectsPrintsCache.ContainsKey(baseObj))
                        {
                            BioScreenController.Instance.scannedObjectsPrintsCache.Add(baseObj, printObjs);
                        }
                    }
                }
            }
        }

        //Load carried object
        if(load.carried > -1)
        {
            Interactable carried = null;

            if(CityData.Instance.savableInteractableDictionary.TryGetValue(load.carried, out carried))
            {
                InteractionController.Instance.PickUp(carried);
            }
        }

        UpgradesController.Instance.UpdateUpgrades();
    }

    private void LoadJob(SideJob job)
    {
        job.SetupNonSerializedData();

        //Set ID
        job.SetJobState(job.state, true);

        //If not posted, then post
        if (job.state == SideJob.JobState.generated)
        {
            job.PostJob();
        }

        job.CreateAcqusitionFacts();

        Game.Log("Jobs: Loaded job " + job.preset.name);
    }
}
