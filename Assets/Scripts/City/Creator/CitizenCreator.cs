﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//This script populates the city with citizens
//Script pass 1
public class CitizenCreator : Creator
{
    //Chunks to load per frame
    public int loadChunk = 10;

    //References
    public GameObject unemploymentHolder;
    public GameObject criminalHolder;
	public OccupationPreset unemployedPreset;
	public OccupationPreset retiredPreset;

	//Citizen Prefab
	public GameObject citizenObj;
	public Texture agentTexture;
    public Texture suspectTexture;

	//Parent
	public GameObject citizenHolder;

	//When unemployed entries need to be added, how many are retired and how many are unemployed.
	public int rUnemployed = 10;
	public int rRetired = 10;

    //Singleton pattern
    private static CitizenCreator _instance;
    public static CitizenCreator Instance { get { return _instance; } }

	void Awake ()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }

    public override void StartLoading()
    {
        Game.Log("CityGen: Populating with citizens...");

        //Generate citizens
        StartCoroutine("Populate");
	}

	IEnumerator Populate()
	{
        string seed = CityData.Instance.seed;

        if(!CityConstructor.Instance.generateNew)
        {
            int citCursor = 0;

            //Load citizens
            while (citCursor < CityConstructor.Instance.currentData.citizens.Count)
            {
                for (int u = 0; u < loadChunk; u++)
                {
                    //Create citizen
                    //Spawn citizen object
                    GameObject newBlob = Instantiate(PrefabControls.Instance.citizen, citizenHolder.transform);
                    Citizen cic = newBlob.GetComponent<Citizen>();
                    CitizenBehaviour.Instance.visibleHumans++;
                    cic.outfitController.debugOverride = false;
                    cic.GetComponent<NewAIController>().Setup(cic); //Setup AI

                    cic.Load(CityConstructor.Instance.currentData.citizens[citCursor]);

                    cic.SetVisible(false, true);

                    //Add to directory
                    if (!cic.isPlayer)
                    {
                        CityData.Instance.citizenDirectory.Add(cic);
                        CityData.Instance.citizenDictionary.Add(cic.humanID, cic);
                    }

                    citCursor++;
                    if (citCursor >= CityConstructor.Instance.currentData.citizens.Count) break;
                }

                //Calculate ~progress
                CityConstructor.Instance.loadingProgress = (float)citCursor / (float)CityConstructor.Instance.currentData.citizens.Count;

                yield return null;
            }

            //Set parners & acquaintances
            foreach(CitySaveData.HumanCitySave humanData in CityConstructor.Instance.currentData.citizens)
            {
                Human created = null;

                if(CityData.Instance.GetHuman(humanData.humanID, out created))
                {
                    //Find citizen
                    if (humanData.partner > -1)
                    {
                        //Load partner
                        Human partner = null;

                        if(CityData.Instance.GetHuman(humanData.partner, out partner))
                        {
                            created.SetPartner(partner as Citizen);
                            partner.SetPartner(created as Citizen);
                        }
                    }

                    //Find citizen
                    if (humanData.paramour > -1)
                    {
                        //Load partner
                        Human para = null;

                        if(CityData.Instance.GetHuman(humanData.paramour, out para))
                        {
                            created.paramour = para as Citizen;
                            para.paramour = created as Citizen;
                        }
                    }

                    //Load aquaintances
                    created.LoadAcquaintances(humanData);
                }
            }
        }
        else
        {
            //New system: Create citizens as a % of maximum city apartment capacity. For all citizens that don't have a job, create unemployment.
            //Get the capacity based on bedrooms...
            //As beds can sleep 2 people if in a couple, or 1 single person, create citizens based on the bedroom number and choose whether to put them in a couple from there...
            //int maximumBedCapacity = -2; //Start @ -2 because the player needs their own apartment
            int coupledCitizens = 0;
            int employedCitizens = 0;
            int unemployedCitizens = 0;

            int citizensOrCouplesToSpawn = 0;

            //Pick a certain number of residences to be kept empty and purchased by the player...
            List<ResidenceController> emptyResidences = new List<ResidenceController>();

            int emptyCount = Mathf.Max(Mathf.FloorToInt((CityData.Instance.citySize.x * CityData.Instance.citySize.y) * 0.1f), 3); //On a 5x4 this will have 3 apartments to buy

            List<ResidenceController> allVacantResidences = new List<ResidenceController>();

            foreach(ResidenceController rc in CityData.Instance.residenceDirectory)
            {
                if(rc.preset != null && rc.preset.habitable)
                {
                    allVacantResidences.Add(rc);
                }
            }

            //Sort by land value (most expensive first)
            allVacantResidences.Sort();
            allVacantResidences.Reverse();

            for (int i = 0; i < emptyCount; i++)
            {
                ResidenceController chosenRes = null;

                //The player must have an opportunity to buy one of the most expensive apartments in the city...
                if(i == 0)
                {
                    chosenRes = allVacantResidences[Toolbox.Instance.GetPsuedoRandomNumberContained(0, Mathf.Min(5, allVacantResidences.Count - 1), seed, out seed)];
                }
                //...Mid range apartent
                else if(i == 1)
                {
                    chosenRes = allVacantResidences[Toolbox.Instance.GetPsuedoRandomNumberContained(Mathf.RoundToInt(allVacantResidences.Count * 0.33f), Mathf.RoundToInt(allVacantResidences.Count * 0.66f), seed, out seed)];
                }
                //... Low end apartment
                else if(i == 2)
                {
                    chosenRes = allVacantResidences[Toolbox.Instance.GetPsuedoRandomNumberContained(Mathf.RoundToInt(allVacantResidences.Count * 0.75f), allVacantResidences.Count, seed, out seed)];
                }
                //The rest are completely random
                else
                {
                    chosenRes = allVacantResidences[Toolbox.Instance.GetPsuedoRandomNumberContained(0, allVacantResidences.Count, seed, out seed)];
                }

                if(chosenRes != null)
                {
                    allVacantResidences.Remove(chosenRes);
                    emptyResidences.Add(chosenRes);
                }
            }

            //Calculate couples to spawn based on available residences...
            foreach (ResidenceController residence in allVacantResidences)
            {
                 citizensOrCouplesToSpawn += Mathf.Max(residence.bedrooms.Count, 1); //If 0 bedrooms, then this counts as a studio flat
            }

            //Multiply by population multiplier
            citizensOrCouplesToSpawn = Mathf.FloorToInt(citizensOrCouplesToSpawn * CityData.Instance.populationMultiplier) - 6; //Minus a certain number that the player apartment can take up

            int apartmentCapacity = citizensOrCouplesToSpawn; //A copy of this variable

            //Spawn a certain number of homeless
            int homelessToSpawn = Mathf.FloorToInt((CityData.Instance.citySize.x * CityData.Instance.citySize.y) * CityControls.Instance.homelessMultiplier * CityData.Instance.populationMultiplier);
            int totalHomelessToSpawn = homelessToSpawn;

            //Collect free jobs
            List<Citizen> withoutJobs = new List<Citizen>();
            List<Occupation> freeJobs = new List<Occupation>(CityData.Instance.jobsDirectory);
            List<CompanyPreset> selfEmployedAutoCreate = new List<CompanyPreset>();

            //Collect self employed auto create jobs
            foreach(CompanyPreset j in Toolbox.Instance.allCompanyPresets)
            {
                if(j.isSelfEmployed)
                {
                    if(j.autoCreate)
                    {
                        int toCreate = Mathf.FloorToInt(withoutJobs.Count * j.cityPopRatio);
                        toCreate = Mathf.Clamp(toCreate, j.minimumNumber, j.maximumNumber);

                        for (int i = 0; i < toCreate; i++)
                        {
                            selfEmployedAutoCreate.Add(j);
                        }
                    }
                }
            }

            //Sort self employed autio create by priority
            selfEmployedAutoCreate.Sort((p1, p2) => p2.priority.CompareTo(p1.priority)); //Highest first

            //Sort jobs by fill priority
            freeJobs.Sort(Occupation.FillPriorityComparison);
            freeJobs.Reverse();

            //Create criminal jobs to fill first...
            foreach(OccupationPreset cr in Toolbox.Instance.allCriminalJobs)
            {
                for (int i = 0; i < cr.minimumPerCity; i++)
                {
                    freeJobs.Insert(0, CreateCriminal(cr));
                }
            }

            Game.Log("CityGen: >---------Citizen Generation---------<");
            Game.Log("CityGen: Use a population multiplier of " + CityData.Instance.populationMultiplier);
            Game.Log("CityGen: The city will generate " + citizensOrCouplesToSpawn + " homed singles or couples based on " + apartmentCapacity + " city apartments...");
            Game.Log("CityGen: The city will spawn " + homelessToSpawn + " homeless...");
            Game.Log("CityGen: There are " + freeJobs.Count + " job in the city...");

            //Collect apartments
            List<ResidenceController> allInhabitedResidences = new List<ResidenceController>();
            List<Citizen> citizensToHouse = new List<Citizen>();

            //Setup cursor
            int setupPhaseCursor = 0;
            int populatePhase = 0;

            //Progress
            float spawnProgress = 0f;
            float jobProgress = 0f;
            float housingProgress = 0f;
            float homelessProgress = 0f;
            float miscProgress = 0f;

            HashSet<string> fingerprintIDs = new HashSet<string>();

            //Begin phasing
            while (populatePhase < 5)
            {
                //Populate stage 0: Create the correct number of citizens & their partners
                if (populatePhase == 0)
                {
                    for (int u = 0; u < loadChunk; u++)
                    {
                        //Break fill loop
                        if (citizensOrCouplesToSpawn <= 0)
                        {
                            break;
                        }

                        //Create citizen
                        //Spawn citizen object
                        GameObject newBlob = Instantiate(PrefabControls.Instance.citizen, citizenHolder.transform);
                        Citizen cic = newBlob.GetComponent<Citizen>();
                        cic.outfitController.debugOverride = false;

                        cic.GetComponent<NewAIController>().Setup(cic); //Setup AI

                        CityData.Instance.homedDirectory.Add(cic);

                        //Select gender based on statistics...
                        cic.SetSexualityAndGender();

                        //Add to directory
                        if (!cic.isPlayer)
                        {
                            CityData.Instance.citizenDirectory.Add(cic);
                            CityData.Instance.citizenDictionary.Add(cic.humanID, cic);
                            withoutJobs.Add(cic);
                            citizensToHouse.Add(cic);
                        }

                        //Is this person part of a couple? If so spawn them
                        if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, cic.seed, out cic.seed) < SocialStatistics.Instance.seriousRelationshipsRatio)
                        {
                            GameObject newBlob2 = Instantiate(PrefabControls.Instance.citizen, citizenHolder.transform);
                            Citizen cicPartner = newBlob2.GetComponent<Citizen>();
                            cicPartner.outfitController.debugOverride = false;
                            cicPartner.GetComponent<NewAIController>().Setup(cicPartner); //Setup AI

                            //Select gender and sexuality based on already existing partner...
                            cicPartner.GenerateSuitableGenderAndSexualityForParnter(cic);

                            //Add to directory
                            if (!cicPartner.isPlayer)
                            {
                                CityData.Instance.citizenDirectory.Add(cicPartner);
                                CityData.Instance.citizenDictionary.Add(cicPartner.humanID, cicPartner);
                                withoutJobs.Add(cicPartner);
                                citizensToHouse.Add(cicPartner);
                            }

                            cic.SetPartner(cicPartner);
                            cicPartner.SetPartner(cic);

                            coupledCitizens += 2;
                        }

                        citizensOrCouplesToSpawn--;
                    }

                    //Record progress
                    spawnProgress = (float)(apartmentCapacity - citizensOrCouplesToSpawn) / (float)apartmentCapacity;

                    if (citizensOrCouplesToSpawn <= 0)
                    {
                        Game.Log("CityGen: " + CityData.Instance.citizenDirectory.Count + " homed citizens created, " + coupledCitizens + " (" + Mathf.RoundToInt(((float)coupledCitizens / (float)CityData.Instance.citizenDirectory.Count) * 100) + "%) are in a partnership");

                        populatePhase++;
                    }
                }
                //Populate stage 1: Give out jobs, if there are none then make citizen unemployed
                else if (populatePhase == 1)
                {
                    for (int u = 0; u < loadChunk; u++)
                    {
                        //Break fill loop
                        if (withoutJobs.Count <= 0)
                        {
                            break;
                        }

                        //Choose citizen
                        Citizen cic = withoutJobs[Toolbox.Instance.GetPsuedoRandomNumberContained(0, withoutJobs.Count, seed, out seed)];

                        //Auto create self employed; parnter must not be as this will use their home as their company
                        if (selfEmployedAutoCreate.Count > 0 && (cic.partner == null || (cic.partner.job != null && !cic.partner.job.preset.selfEmployed)))
                        {
                            CityConstructor.Instance.CreateSelfEmployed(selfEmployedAutoCreate[0], cic, null);
                            employedCitizens++;

                            //Calc age
                            cic.CalculateAge();

                            //We can now set personality
                            cic.SetPersonality();

                            //+1 employed
                            CityData.Instance.employedCitizens++;

                            Game.Log("CityGen: Auto create self employed: " + selfEmployedAutoCreate[0].name + " " + cic.GetCitizenName());

                            //Remove job from list
                            selfEmployedAutoCreate.RemoveAt(0);
                            withoutJobs.Remove(cic);

                            continue;
                        }

                        Occupation jobPick = null;

                        if(freeJobs.Count > 0)
                        {
                            jobPick = freeJobs[Toolbox.Instance.GetPsuedoRandomNumberContained(0, Mathf.Min(5, freeJobs.Count), seed, out seed)]; //Pick one of the top 5 jobs
                            employedCitizens++;
                        }
                        //If there are no free jobs, create an unemployed position
                        else
                        {
                            jobPick = CreateUnemployed();
                            unemployedCitizens++;
                        }

                        //Give job
                        cic.SetJob(jobPick);

                        //Calc age
                        cic.CalculateAge();

                        //We can now set personality
                        cic.SetPersonality();

                        //+1 employed
                        CityData.Instance.employedCitizens++;

                        //Remove job from list
                        freeJobs.Remove(jobPick);
                        withoutJobs.Remove(cic);
                    }

                    //Record progress
                    jobProgress = (float)(CityData.Instance.citizenDirectory.Count - withoutJobs.Count) / (float)CityData.Instance.citizenDirectory.Count;

                    if (withoutJobs.Count <= 0)
                    {
                        populatePhase++;
                        Game.Log("CityGen: " + employedCitizens + " citizens are employed (" + Mathf.RoundToInt(((float)employedCitizens / (float)CityData.Instance.citizenDirectory.Count) * 100) + "%)");

                        //Rank citizens to house by income (highest first)
                        citizensToHouse.Sort((p1, p2) => p2.societalClass.CompareTo(p1.societalClass));
                    }
                }
                //Populate stage 2: Move in citizens
                else if (populatePhase == 2)
                {
                    for (int u = 0; u < loadChunk; u++)
                    {
                        //Pick an apartment for the player
                        if (Player.Instance.home == null)
                        {
                            //The player apartment should be 2-3 floors up
                            List<ResidenceController> playerApartments = allVacantResidences.FindAll(item => Game.Instance.preferredStartingBuildings.Contains(item.building.preset) && item.address.floor.floor >= 2 && item.address.floor.floor <= 3 && item.bedrooms.Count >= 1);

                            if(playerApartments.Count <= 0)
                            {
                                playerApartments = allVacantResidences.FindAll(item => item.address.floor.floor >= 0 && item.address.floor.floor <= 4 && item.address.residence.bedrooms.Count > 0);
                            }

                            if (playerApartments.Count <= 0)
                            {
                                playerApartments = allVacantResidences.FindAll(item => item.address.floor.floor >= 0);
                            }

                            Game.Log("CityGen: Picking from " + playerApartments.Count + " player apartments");

                            ResidenceController pickedForPlayer = playerApartments[Toolbox.Instance.GetPsuedoRandomNumberContained(0, playerApartments.Count, seed, out seed)];

                            if(pickedForPlayer != null)
                            {
                                Player.Instance.SetResidence(pickedForPlayer);
                                allVacantResidences.Remove(pickedForPlayer);
                                allInhabitedResidences.Remove(pickedForPlayer);
                            }
                        }

                        //Break fill loop
                        if (citizensToHouse.Count <= 0)
                        {
                            break;
                        }

                        //Pick a citizen...
                        Citizen cic = citizensToHouse[0]; //Pick the richest citizen (couple) and move them into the most expensive apartment

                        //If this citizen already has a home, skip...
                        if (cic.home != null)
                        {
                            citizensToHouse.Remove(cic);
                            continue;
                        }

                        ResidenceController chosenHome = null;
                        bool foundHome = false;

                        //If possible, move them into a completely uninhabited apartment...
                        if (allVacantResidences.Count > 0)
                        {
                            chosenHome = allVacantResidences[0];
                            foundHome = true;
                        }
                        else if(allInhabitedResidences.Count > 0)
                        {
                            //Rank inhabited residences using 50% low resident count and 50% societal class
                            allInhabitedResidences.Sort(ResidenceController.RoommateComparison);

                            for (int i = 0; i < allInhabitedResidences.Count; i++)
                            {
                                //Don't give self employed roommates as it may mess with the company setup
                                if(cic.job != null && cic.job.preset.selfEmployed && allInhabitedResidences[i].address.owners.Exists(item => item.job != null && item.job.preset.selfEmployed))
                                {
                                    continue;
                                }

                                chosenHome = allInhabitedResidences[i];
                                foundHome = true;
                                break;
                            }
                        }
                        else
                        {
                            Game.LogError("CityGen: There are no possible places to move this citizen in. Something is wrong with your code :(");
                        }

                        //Move in (+ partner)
                        if (foundHome)
                        {
                            cic.SetResidence(chosenHome);
                            citizensToHouse.Remove(cic);
                            chosenHome.bedroomsTaken++;

                            if (cic.partner != null)
                            {
                                cic.partner.SetResidence(chosenHome);
                                citizensToHouse.Remove(cic.partner);
                            }

                            if(allVacantResidences.Contains(chosenHome))
                            {
                                CityData.Instance.inhabitedResidences++;
                                allVacantResidences.Remove(chosenHome); //Remove from vacant list
                            }

                            //Does this have at least 2 more capacity?
                            if (chosenHome.bedroomsTaken < chosenHome.bedrooms.Count)
                            {
                                if (!allInhabitedResidences.Contains(chosenHome))
                                {
                                    allInhabitedResidences.Add(chosenHome);
                                }
                            }
                            else if(allInhabitedResidences.Contains(chosenHome))
                            {
                                allInhabitedResidences.Remove(chosenHome);
                            }
                        }

                        //Break fill loop
                        if (citizensToHouse.Count <= 0)
                        {
                            Game.Log("CityGen: " + CityData.Instance.inhabitedResidences + " inhabited apartments out of " + CityData.Instance.residenceDirectory.Count);
                            break;
                        }
                    }

                    //Record progress
                    housingProgress = (float)(CityData.Instance.citizenDirectory.Count - citizensToHouse.Count) / (float)CityData.Instance.citizenDirectory.Count;

                    if (citizensToHouse.Count <= 0)
                    {
                        populatePhase++;
                    }
                }
                //Populate stage 3: Spawn homeless citizens
                else if(populatePhase == 3)
                {
                    for (int u = 0; u < loadChunk; u++)
                    {
                        //Break fill loop
                        if (homelessToSpawn <= 0)
                        {
                            break;
                        }

                        //Create citizen
                        //Spawn citizen object
                        GameObject newBlob = Instantiate(PrefabControls.Instance.citizen, citizenHolder.transform);
                        Citizen cic = newBlob.GetComponent<Citizen>();
                        cic.outfitController.debugOverride = false;
                        cic.GetComponent<NewAIController>().Setup(cic); //Setup AI

                        cic.isHomeless = true; //Homeless
                        CityData.Instance.homelessDirectory.Add(cic);
                        CityData.Instance.homlessAssign.Add(cic);

                        //Select gender based on statistics...
                        cic.SetSexualityAndGender();

                        //Give job
                        Occupation jobPick = CreateUnemployed();
                        cic.SetJob(jobPick);

                        //Calc age
                        cic.CalculateAge();

                        //Add destitute trait
                        cic.AddCharacterTrait(CitizenControls.Instance.destitute);

                        //We can now set personality
                        cic.SetPersonality();

                        //Add to directory
                        if (!cic.isPlayer)
                        {
                            CityData.Instance.citizenDirectory.Add(cic);
                            CityData.Instance.citizenDictionary.Add(cic.humanID, cic);
                            //withoutJobs.Add(cic);
                            //citizensToHouse.Add(cic);
                        }

                        homelessToSpawn--;
                    }

                    if (homelessToSpawn <= 0)
                    {
                        homelessProgress = 1f;
                        populatePhase++;
                    }
                    else
                    {
                        //Record progress
                        homelessProgress = 1f - ((float)homelessToSpawn / (float)totalHomelessToSpawn);
                    }
                }
                //Populate stage 4: Setup misc
                else if (populatePhase == 4)
                {
                    for (int u = 0; u < loadChunk; u++)
                    {
                        Citizen citizen = CityData.Instance.citizenDirectory[setupPhaseCursor];

                        citizen.SetupGeneral();
                        citizen.GenerateRoutineGoals();
                        citizen.outfitController.debugOverride = false;
                        citizen.outfitController.GenerateOutfits();

                        setupPhaseCursor++;

                        //Break fill loop
                        if (setupPhaseCursor >= CityData.Instance.citizenDirectory.Count)
                        {
                            break;
                        }
                    }

                    //Record progress
                    miscProgress = (float)setupPhaseCursor / (float)CityData.Instance.citizenDirectory.Count;

                    if (setupPhaseCursor >= CityData.Instance.citizenDirectory.Count)
                    {
                        populatePhase++;
                    }
                }

                //Calculate ~progress
                CityConstructor.Instance.loadingProgress = (spawnProgress + jobProgress + housingProgress + homelessProgress + miscProgress) / 5f;

                yield return null;
            }
        }

        //Complete!
        Game.Log("CityGen: Total population: " + CityData.Instance.citizenDirectory.Count);
        Game.Log("CityGen: >--------------------------------<");
        SetComplete();
	}

	public Occupation CreateUnemployed()
	{
        //Create a new job, copy from preset
        //OccupationController ue = unemploymentHolder.AddComponent<OccupationController>();

        Occupation ue = new Occupation();

        //Remove retired status for now
  //      int type = Toolbox.Instance.GetPsuedoRandomNumberContained(0, rUnemployed + rRetired, seedInput,);

  //      //Run a psuedo setup, as this doesn't need to be saved/have an ID...
		//if (type < rUnemployed)
		//{
		//	ue.preset = unemployedPreset;
		//	ue.employer = null;
		//	ue.paygrade = 0;

		//} else
		//{
		//	ue.preset = retiredPreset;
		//	ue.employer = null;
		//	ue.paygrade = 1;
		//}

        ue.preset = unemployedPreset;
        ue.employer = null;
        ue.paygrade = 0;

        ue.name = Strings.Get("jobs", ue.preset.name);

        CityData.Instance.unemployedDirectory.Add(ue);

		return ue;
	}

    public Occupation CreateCriminal(OccupationPreset preset)
    {
        //Create a new job, copy from preset
        Occupation cr = new Occupation();
        //OccupationController cr = criminalHolder.AddComponent<OccupationController>();
        cr.preset = preset;
        cr.employer = null;
        cr.Setup();

        cr.paygrade = preset.societalClass;

        CityData.Instance.criminalJobDirectory.Add(cr);
        CityData.Instance.jobsDirectory.Add(cr);

        return cr;
    }
}
