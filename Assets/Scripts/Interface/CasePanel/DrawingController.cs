﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DrawingController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Header("Setup Components")]
    public RectTransform container;
    public RawImage img;
    public RectTransform drawBrushRect;
    public RawImage brushImage;

    [Header("Generated Components")]
    public Texture2D drawingTex;

    [Header("State")]
    public bool isOver = false; //True when hovering
    public bool drawingActive = false;
    public bool eraserMode = false;
    bool lastPosValid = false;
    Vector2 lastValidLocalPos = Vector2.zero;

    [Header("Settings")]
    public Color brushColour = Color.white;
    public Texture2D brush;
    public Vector2 brushSize = new Vector2(4, 4);
    public bool startedDraw = false; //To start drawing, the player must click inside the rect

    [Header("Buttons")]
    public bool setupButtons = false; //Handle button behaviour through this
    public WindowExtraControlsController windowButtonsController;

    public ButtonController toggleDrawingButton;
    public ColourSelectorButtonController colourButton;
    public ButtonController eraserButton;
    public ButtonController clearButton;

    private void Awake()
    {
        SetDrawingActive(drawingActive);

        //Create drawing controls
        if(setupButtons)
        {
            windowButtonsController = this.gameObject.transform.parent.parent.parent.parent.GetComponentInChildren<WindowExtraControlsController>();
            windowButtonsController.drawingController = this;
            windowButtonsController.SetEnableDrawingControls(true);
        }
    }

    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        isOver = true;
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;

        isOver = false;
    }

    public void SetDrawingActive(bool val)
    {
        drawingActive = val;

        if (!drawingActive)
        {
            //Cancel raycast block
            img.raycastTarget = false;

            //Disable brush
            drawBrushRect.gameObject.SetActive(false);

            this.enabled = false; //Start disabled
        }
        else
        {
            img.raycastTarget = true;

            SetBrushColour(brushColour);
            SetBrushImage(brush);

            //Enable brush
            drawBrushRect.gameObject.SetActive(true);

            this.enabled = true; //Start disabled
            lastPosValid = false;
            isOver = false;
        }
    }

    //Create/Replace drawing texture
    public void ResetDrawingTexture()
    {
        //Remove existing
        if(drawingTex != null)
        {
            Destroy(drawingTex);
        }

        //Create a new texture for drawing
        drawingTex = new Texture2D(Mathf.RoundToInt(container.sizeDelta.x), Mathf.RoundToInt(container.sizeDelta.y));
        drawingTex.name = "Instanced Drawing Tex";

        //Fill with clear
        for (int l = 0; l < Mathf.RoundToInt(container.sizeDelta.x); l++)
        {
            for (int u = 0; u < Mathf.RoundToInt(container.sizeDelta.y); u++)
            {
                drawingTex.SetPixel(l, u, Color.clear);
            }
        }

        drawingTex.Apply(); //Apply changes

        //Set texture
        img.texture = drawingTex;
        img.color = Color.white; //Set colour to visible
    }

    //Set eraser mode
    public void SetEraserMode(bool val)
    {
        eraserMode = val;

        if(eraserMode)
        {
            SetBrushImage(PrefabControls.Instance.eraseBrush);
        }
        else
        {
            SetBrushImage(PrefabControls.Instance.drawingBrush);
        }
    }

    //Set the brush colour
    public void SetBrushColour(Color newCol)
    {
        brushColour = newCol;
        brushImage.color = newCol;
    }

    //Set the brush image
    public void SetBrushImage(Texture2D newBrush)
    {
        brush = newBrush;
        brushImage.texture = newBrush;
        brushSize = new Vector2(newBrush.width, newBrush.height);
        drawBrushRect.sizeDelta = brushSize;
    }

    // Update is called once per frame
    void Update()
    {
        if(drawingActive)
        {
            if (!isOver) return; //Only valid when moused over properly

            //Create texure if needed
            if(drawingTex == null)
            {
                ResetDrawingTexture();
            }

            Vector2 localMousePos = Vector2.zero;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(container, Input.mousePosition, null, out localMousePos);
            drawBrushRect.localPosition = localMousePos;

            //Texture size
            int texWidth = drawingTex.width;
            int texHeight = drawingTex.height;

            //To start drawing, the player must click inside the rect
            if (Input.GetMouseButtonDown(0))
            {
                if(!startedDraw)
                {
                    if (drawBrushRect.localPosition.x >= 0 && drawBrushRect.localPosition.x < container.sizeDelta.x)
                    {
                        if (drawBrushRect.localPosition.y >= 0 && drawBrushRect.localPosition.y < container.sizeDelta.y)
                        {
                            startedDraw = true;
                            lastPosValid = false; //Reset last valid pos
                        }
                    }
                }
            }

            //Draw with left mouse
            if(startedDraw)
            {
                if (Input.GetMouseButton(0))
                {
                    //!Because the brush's pivot is bottom right to align with the cursor, we have to invert the X axis to match the texture coordinates if using anchored coordinates!
                    //Local coordinates however, are relative to the parent rect, which should be fine.
                    //Only draw if inside the layer area
                    if (drawBrushRect.localPosition.x >= 0 && drawBrushRect.localPosition.x < container.sizeDelta.x)
                    {
                        if (drawBrushRect.localPosition.y >= 0 && drawBrushRect.localPosition.y < container.sizeDelta.y)
                        {
                            //Game.Log(drawBrushRect.localPosition);

                            //Draw a line from the current to the last valid mouse position
                            if (lastPosValid)
                            {
                                //Start at the last point, draw a line to the new point
                                Vector2 line = lastValidLocalPos;
                                float progress = 0f;
                                float distance = Vector2.Distance(lastValidLocalPos, localMousePos);

                                while (line != localMousePos)
                                {
                                    //When mouse is down copy brush pixels to the current layer's drawing texture
                                    for (int i = 0; i < brushSize.x; i++)
                                    {
                                        int texX = (int)line.x - i;
                                        if (texX >= texWidth || texX < 0) continue; //Don't draw outside of texture

                                        for (int u = 0; u < brushSize.y; u++)
                                        {
                                            //Calculate texture coordinates
                                            int texY = (int)line.y + u;
                                            if (texY >= texHeight || texY < 0)
                                            {
                                                continue; //Don't draw outside of texture
                                            }

                                            if(eraserMode)
                                            {
                                                drawingTex.SetPixel(texX, texY, Color.clear);
                                            }
                                            else
                                            {
                                                //Copy colour from the brush texture
                                                Color thisBrushColour = brushColour;
                                                thisBrushColour.a = brush.GetPixel(i, u).a; //Steal the alpha value from the actual brush texture

                                                //Add to the base
                                                Color canvasColour = drawingTex.GetPixel(texX, texY);

                                                //Calculate the colour to write
                                                float srcF = thisBrushColour.a;
                                                float destF = 1f - thisBrushColour.a;
                                                float alpha = srcF + destF * canvasColour.a;
                                                Color R = (thisBrushColour * srcF + canvasColour * canvasColour.a * destF) / alpha;
                                                R.a = alpha;

                                                drawingTex.SetPixel(texX, texY, R);
                                            }
                                        }
                                    }

                                    progress += 1f / distance;
                                    line = Vector2.Lerp(lastValidLocalPos, localMousePos, progress);
                                }
                            }

                            //Draw a dot at the current position
                            //When mouse is down copy brush pixels to the current layer's drawing texture
                            for (int i = 0; i < brushSize.x; i++)
                            {
                                int texX = (int)localMousePos.x - i;
                                if (texX >= texWidth || texX < 0) continue; //Don't draw outside of texture

                                for (int u = 0; u < brushSize.y; u++)
                                {
                                    //Calculate texture coordinates
                                    int texY = (int)localMousePos.y + u;
                                    if (texY >= texHeight || texY < 0) continue; //Don't draw outside of texture

                                    if (eraserMode)
                                    {
                                        drawingTex.SetPixel(texX, texY, Color.clear);
                                    }
                                    else
                                    {
                                        //Copy colour from the brush texture
                                        Color thisBrushColour = brushColour;
                                        thisBrushColour.a = brush.GetPixel(i, u).a; //Steal the alpha value from the actual brush texture

                                        //Add to the base
                                        Color canvasColour = drawingTex.GetPixel(texX, texY);

                                        //Calculate the colour to write
                                        float srcF = thisBrushColour.a;
                                        float destF = 1f - thisBrushColour.a;
                                        float alpha = srcF + destF * canvasColour.a;
                                        Color R = (thisBrushColour * srcF + canvasColour * canvasColour.a * destF) / alpha;
                                        R.a = alpha;

                                        drawingTex.SetPixel(texX, texY, R);
                                    }
                                }
                            }

                            drawingTex.Apply(); //Apply changes

                            //This is a valid position
                            lastPosValid = true;
                            lastValidLocalPos = localMousePos;
                        }
                        else lastPosValid = false; //Reset last valid pos
                    }
                    else lastPosValid = false; //Reset last valid pos
                }
                else
                {
                    lastPosValid = false;
                    startedDraw = false;
                }
            }
        }
        else
        {
            SetDrawingActive(false);
        }
    }
}
