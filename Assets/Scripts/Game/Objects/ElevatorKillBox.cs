using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorKillBox : MonoBehaviour
{
    public Elevator elevator;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if(elevator != null && elevator.isMoving && !elevator.isGoingUp && Player.Instance.currentHealth > 0f)
            {
                Player.Instance.AddHealth(-999999f, false);
            }
        }
    }
}
