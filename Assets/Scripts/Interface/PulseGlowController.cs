﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class PulseGlowController : MonoBehaviour
{
    public Image imageToGlow;
    public RawImage rawImageToGlow;
    public TextMeshProUGUI textToGlow;
    public bool glowActiveOnStart = false;
    public bool glowActive = false;

    public float pulseSpeed = 1f;
    private float glowState = 0f;
    private bool glowSwitch = false;

    public bool useLerpColour = true;
    public Color originalColour = Color.white;
    public Color lerpColour = Color.grey;

    void Awake()
    {
        SetGlow(glowActiveOnStart);
	}
	
    public void SetGlow(bool onOff)
    {
        if (!glowActive && onOff)
        {
            glowActive = true;
            this.enabled = true;
        }
        else if (glowActive && !onOff)
        {
            glowActive = false;
            if(imageToGlow != null) imageToGlow.color = originalColour;
            if(rawImageToGlow != null) rawImageToGlow.color = originalColour;
            if(textToGlow != null) textToGlow.color = originalColour;
            this.enabled = false;
        }
    }

    void Update()
    {
        if(!glowActive)
        {
            this.enabled = false;
            return;
        }

        if(glowState < 1f && glowSwitch)
        {
            glowState += Time.deltaTime * pulseSpeed;
        }
        else if (glowState >= 1f && glowSwitch)
        {
            glowSwitch = false;
        }

        if (glowState > 0f && !glowSwitch)
        {
            glowState -= Time.deltaTime * pulseSpeed;
        }
        else if (glowState <= 0f && !glowSwitch)
        {
            glowSwitch = true;
        }

        glowState = Mathf.Clamp01(glowState);

        if(useLerpColour)
        {
            if(imageToGlow != null)
            {
                imageToGlow.color = Color.Lerp(originalColour, lerpColour, glowState);
            }

            if (textToGlow != null)
            {
                textToGlow.color = Color.Lerp(originalColour, lerpColour, glowState);
            }

            if (rawImageToGlow != null)
            {
                rawImageToGlow.color = Color.Lerp(originalColour, lerpColour, glowState);
            }
        }
    }
}
