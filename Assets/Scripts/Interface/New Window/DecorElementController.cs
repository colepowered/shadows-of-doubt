﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DecorElementController : MonoBehaviour
{
    [Header("Components")]
    public MaterialGroupPreset preset;
    public FurniturePreset furniture;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI priceText;
    public TextMeshProUGUI sellText;
    public InfoWindow thisWindow;
    public DecorController decorController;
    public FurnishingsController furnishingsController;
    public Image mainImage;
    public FurnitureLocation worldFurnitureReference;
    public ButtonController placeButton;
    public ButtonController storageButton;
    public ButtonController sellButton;
    public Image icon;
    [Space(7)]
    public Sprite chairIcon;
    public Sprite tableIcon;
    public Sprite unitIcon;
    public Sprite electronicsIcon;
    public Sprite structuralIcon;
    public Sprite decorationIcon;
    public Sprite miscIcon;

    [Header("State")]
    public int price = 0;

    public void Setup(MaterialGroupPreset newPreset, DecorController newDecorController, InfoWindow newThisWindow)
    {
        //Game.Log("Setup shop button for " + newPreset);

        preset = newPreset;
        thisWindow = newThisWindow;
        decorController = newDecorController;

        if (preset != null)
        {
            if(newPreset.materialType == MaterialGroupPreset.MaterialType.walls)
            {
                price = preset.price * Player.Instance.currentRoom.GetWallCount();
            }
            else
            {
                price = preset.price * Player.Instance.currentRoom.nodes.Count;
            }

            mainImage.sprite = preset.decorSprite;
            //mainImage.texture = GenerateDecorIcon();
            mainImage.color = Color.white;
        }

        UpdateButtonText();
    }

    private Texture2D GenerateDecorIcon(int size = 128)
    {
        Texture2D newTex = new Texture2D(128, 128);

        Texture2D baseTex = preset.material.GetTexture("_BaseColorMap") as Texture2D;

        if(!baseTex.isReadable)
        {
            Game.LogError(baseTex.name + " is not readable!");
            return newTex;
        }

        for (int i = 0; i < size; i++)
        {
            for (int u = 0; u < size; u++)
            {
                if (i >= baseTex.width || u >= baseTex.height) continue;
                newTex.SetPixel(i, u, baseTex.GetPixel(i, u));
            }
        }

        newTex.Apply();

        return newTex;
    }

    public void SetupFurniture(FurniturePreset newFurniture, FurnishingsController newDecorController, InfoWindow newThisWindow, FurnitureLocation newWorldFurnReference)
    {
        //Game.Log("Setup shop button for " + newFurniture);

        furniture = newFurniture;
        thisWindow = newThisWindow;
        furnishingsController = newDecorController;
        worldFurnitureReference = newWorldFurnReference;

        if(storageButton != null)
        {
            storageButton.SetInteractable(false);
            storageButton.gameObject.SetActive(false);
        }

        if (furniture != null)
        {
            if (worldFurnitureReference == null)
            {
                price = furniture.cost;
            }
            else
            {
                price = 0;

                if (storageButton != null)
                {
                    storageButton.SetInteractable(true);
                    storageButton.gameObject.SetActive(true);
                }
            }

            mainImage.sprite = furniture.staticImage;
            mainImage.color = Color.white;
        }

        if (furniture.decorClass == FurniturePreset.DecorClass.chairs) icon.sprite = chairIcon;
        else if (furniture.decorClass == FurniturePreset.DecorClass.tables) icon.sprite = tableIcon;
        else if (furniture.decorClass == FurniturePreset.DecorClass.units) icon.sprite = unitIcon;
        else if (furniture.decorClass == FurniturePreset.DecorClass.electronics) icon.sprite = electronicsIcon;
        else if (furniture.decorClass == FurniturePreset.DecorClass.structural) icon.sprite = structuralIcon;
        else if (furniture.decorClass == FurniturePreset.DecorClass.decoration) icon.sprite = decorationIcon;
        else if (furniture.decorClass == FurniturePreset.DecorClass.misc) icon.sprite = miscIcon;

        VisualUpdate();
    }

    public void VisualUpdate()
    {
        UpdateButtonText();
        UpdatePurchaseAbility();
    }

    public void UpdateButtonText()
    {
        if(preset != null)
        {
            nameText.text = Strings.Get("evidence.names", preset.name);
            priceText.text = Strings.Get("ui.interface", "Select") + " " + CityControls.Instance.cityCurrency + price;
        }
        else if(furniture != null)
        {
            nameText.text = Strings.Get("evidence.names", furniture.name);

            if(worldFurnitureReference != null)
            {
                priceText.text = Strings.Get("ui.interface", "Place");

                if(sellButton != null)
                {
                    sellText.text = Strings.Get("ui.interface", "Sell") + " " + CityControls.Instance.cityCurrency + Mathf.RoundToInt(furniture.cost * 0.5f);
                    sellButton.gameObject.SetActive(true);
                    sellButton.SetInteractable(true);
                }
            }
            else
            {
                priceText.text = Strings.Get("ui.interface", "Select") + " " + CityControls.Instance.cityCurrency + price;

                if (sellButton != null)
                {
                    sellButton.gameObject.SetActive(false);
                    sellButton.SetInteractable(false);
                }

                Toolbox.Instance.SetRectSize(placeButton.rect, 120, 0, 8, 0);
                placeButton.rect.sizeDelta = new Vector2(placeButton.rect.sizeDelta.x, 52);
                placeButton.rect.anchoredPosition = new Vector2(placeButton.rect.anchoredPosition.x, 6);
            }
        }

        UpdatePurchaseAbility();
    }

    public void OnPlaceButton()
    {
        if (furnishingsController == null || (furnishingsController != null && furnishingsController.tabState == FurnishingsController.TabState.inShop))
        {
            if (price <= GameplayController.Instance.money)
            {
                if (decorController != null) decorController.SetSelected(preset);
                if (furnishingsController != null) furnishingsController.SetSelected(furniture, worldFurnitureReference, false);
            }
        }
        else if (furnishingsController != null)
        {
            bool isInStorage = false;
            if (PlayerApartmentController.Instance.furnitureStorage.Contains(worldFurnitureReference)) isInStorage = true;

            furnishingsController.SetSelected(furniture, worldFurnitureReference, !isInStorage);
        }
    }

    public void OnStorageButton()
    {
        if (worldFurnitureReference != null)
        {
            PlayerApartmentController.Instance.MoveFurnitureToStorage(worldFurnitureReference);
        }
    }

    public void OnSellButton()
    {
        if(worldFurnitureReference != null)
        {
            PlayerApartmentController.Instance.SellFurniture(worldFurnitureReference);
        }
    }

    public void UpdatePurchaseAbility()
    {
        if (furnishingsController == null || furnishingsController.tabState == FurnishingsController.TabState.inShop)
        {
            if (price > GameplayController.Instance.money)
            {
                placeButton.SetInteractable(false);
            }
            else
            {
                placeButton.SetInteractable(true);
            }
        }
        else placeButton.SetInteractable(true);

        if (storageButton != null && furnishingsController != null)
        {
            if (worldFurnitureReference == null || PlayerApartmentController.Instance.furnitureStorage.Contains(worldFurnitureReference))
            {
                storageButton.gameObject.SetActive(false);
                storageButton.SetInteractable(false);
            }
        }
    }
}
