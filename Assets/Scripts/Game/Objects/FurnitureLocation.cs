﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Linq;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

[System.Serializable]
public class FurnitureLocation
{
    public int id = 0;
    public List<FurnitureClass> furnitureClasses = new List<FurnitureClass>();
    public int angle;
    public Vector3 offset; //Applied local offset to this furniture
    public NewNode anchorNode;
    public List<NewNode> coversNodes = new List<NewNode>();
    public FurnitureClusterLocation cluster;

    public bool useFOVBLock = false;
    public Vector2 fovDirection = Vector2.zero;
    public int fovMaxDistance = 5;
    public Vector3 scaleMultiplier = Vector3.one;

    //public string nameDebug;

    //Calculated automatically
    public List<Interactable.UsagePoint> usage = new List<Interactable.UsagePoint>();
    public Dictionary<NewNode, List<Vector3>> sublocations = new Dictionary<NewNode, List<Vector3>>(); //A reference to rotated sublocations for each node this covers

    //In-game
    public FurniturePreset furniture; //Chosen furniture
    public GameObject spawnedObject; //Controlled by the node this is positioned on
    public List<MeshRenderer> meshes = new List<MeshRenderer>();
    public bool pickedMaterials = false;
    public bool createdInteractables = false;
    public bool pickedOwners = false;
    public bool pickedArt = false;

    public bool userPlaced = false;

    //Automatically calculated diaglonal
    public int diagonalAngle = 0;

    public Toolbox.MaterialKey matKey; //Chosen when added to a room
    public List<Interactable> integratedInteractables = new List<Interactable>(); //List of interactables attached to this furniture
    public int integratedIDAssign = 1; //Used to give out IDs to integrated interactables (see interactable class)
    public List<Interactable> spawnedInteractables = new List<Interactable>(); //List of interactables spawned on this furniture

    //Art
    public ArtPreset art;
    public Toolbox.MaterialKey artMatKey; //Chosen when added to a room

    //Owner map
    public List<int> loadOwners = new List<int>();
    public Dictionary<OwnerKey, int> ownerMap = new Dictionary<OwnerKey, int>();

    //Debug
    public List<MonoBehaviour> debugOwners = new List<MonoBehaviour>();

    public struct OwnerKey
    {
        public Human human;
        public NewAddress address;

        public OwnerKey(Human newHuman)
        {
            human = newHuman;
            address = null;
        }

        public OwnerKey(NewAddress newAddress)
        {
            address = newAddress;
            human = null;
        }
    }

    //Create new furniture (can be done as a prospective attempt - in this case no ID is assigned)
    public FurnitureLocation(List<FurnitureClass> newClasses, int newAngle, NewNode newAnchor, List<NewNode> newCoversNodes, bool newUseFOVBlock = false, Vector2 newFovDirection = new Vector2(), int newFOVBlockMax = 5, Vector3 newScale = new Vector3(), bool newUserPlaced = false, Vector3 newOffset = new Vector3())
    {
        userPlaced = newUserPlaced;
        offset = newOffset;

        furnitureClasses = newClasses;
        angle = newAngle;
        anchorNode = newAnchor;
        coversNodes = newCoversNodes;

        useFOVBLock = newUseFOVBlock;
        fovDirection = newFovDirection;
        fovMaxDistance = newFOVBlockMax;

        scaleMultiplier = newScale;

        if(!userPlaced) DiagonalRotation();
        CalculateWalkableSublocations();
    }

    //Assigns a new furniture ID based on this room
    public void AssignID(NewRoom fromRoom)
    {
        if(id <= 0)
        {
            id = (fromRoom.roomID * 10000) + fromRoom.furnitureAssignID;
            fromRoom.furnitureAssignID++;
        }
    }

    //Create new furniture (assigned ID with passed location class)
    public FurnitureLocation(FurnitureClusterLocation newCluster, List<FurnitureClass> newClasses, int newAngle, NewNode newAnchor, List<NewNode> newCoversNodes, bool newUseFOVBlock = false, Vector2 newFovDirection = new Vector2(), int newFOVBlockMax = 5, Vector3 newScale = new Vector3(), bool newUserPlaced = false, Vector3 newOffset = new Vector3())
    {
        //New furniture ID system
        if (newCluster != null)
        {
            id = (newCluster.anchorNode.room.roomID * 10000) + newCluster.anchorNode.room.furnitureAssignID;
            newCluster.anchorNode.room.furnitureAssignID++;
        }

        //nameDebug = newNameDebug;

        userPlaced = newUserPlaced;
        offset = newOffset;

        furnitureClasses = newClasses;
        angle = newAngle;
        anchorNode = newAnchor;
        coversNodes = newCoversNodes;
        cluster = newCluster;

        useFOVBLock = newUseFOVBlock;
        fovDirection = newFovDirection;
        fovMaxDistance = newFOVBlockMax;

        scaleMultiplier = newScale;

        if (!userPlaced) DiagonalRotation();
        CalculateWalkableSublocations();
    }

    //This constructor loads in an ID
    public FurnitureLocation(int loadID, FurnitureClusterLocation newCluster, List<FurnitureClass> newClasses, int newAngle, NewNode newAnchor, List<NewNode> newCoversNodes, bool newUseFOVBlock = false, Vector2 newFovDirection = new Vector2(), int newFOVBlockMax = 5, Vector3 newScale = new Vector3(), bool newUserPlaced = false, Vector3 newOffset = new Vector3())
    {
        id = loadID;
        //Game.Log("Load furniture id: " + loadID);

        userPlaced = newUserPlaced;
        offset = newOffset;

        furnitureClasses = newClasses;
        angle = newAngle;
        anchorNode = newAnchor;
        coversNodes = newCoversNodes;
        cluster = newCluster;

        useFOVBLock = newUseFOVBlock;
        fovDirection = newFovDirection;
        fovMaxDistance = newFOVBlockMax;

        scaleMultiplier = newScale;

        if (anchorNode != null)
        {
            if (!userPlaced) DiagonalRotation();
            CalculateWalkableSublocations();
        }
    }

    //Moves lightswitches here up by a small amount to avoid conflict with spawned objects
    public void RaiseLightswitch()
    {
        float raiseAmount = 1.375f;
        bool r = false;

        foreach (FurnitureClass c in furnitureClasses)
        {
            if (c.raiseLightswitch)
            {
                r = true;
                raiseAmount = Mathf.Max(raiseAmount, c.lightswitchYOffset);
            }
        }

        if (r)
        {
            foreach (NewWall w in anchorNode.walls)
            {
                if (w.lightswitchInteractable != null)
                {
                    Vector3 newPos = w.lightswitchInteractable.GetWorldPosition();
                    newPos.y = anchorNode.position.y + raiseAmount;

                    w.lightswitchInteractable.lPos.y = raiseAmount;
                    w.lightswitchInteractable.wPos = newPos;
                    w.lightswitchInteractable.spWPos = newPos;
                }
            }
        }
    }

    //Calculate diagonal rotation
    private void DiagonalRotation()
    {
        foreach (FurnitureClass furnClass in furnitureClasses)
        {
            if (furnClass.canFaceDiagonally)
            {
                //Get postion of walls relative to object
                if (anchorNode.walls.Count == 2)
                {
                    //Game.Log("!!!!Anchor wall count = 2 at " + anchorNode.nodeCoord);

                    Vector2 addedOffset = Vector2.zero;

                    foreach (NewWall wall in anchorNode.walls)
                    {
                        addedOffset += wall.wallOffset;
                    }

                    //Game.Log("!!!!Added offset is = " + addedOffset);

                    //We have the required offset for diagonal facing: The corner which this should face away from...
                    if (Mathf.Abs(addedOffset.x) > 0f && Mathf.Abs(addedOffset.x) < 1f && Mathf.Abs(addedOffset.y) > 0f && Mathf.Abs(addedOffset.y) < 1f)
                    {
                        Vector2 rotated = Toolbox.Instance.RotateVector2CW(addedOffset, angle);

                        //Game.Log("!!!!Rotated angle is = " + rotated);

                        if (rotated.x < 0f && rotated.y > 0f)
                        {
                            diagonalAngle -= 45;
                        }
                        else if (rotated.x > 0f && rotated.y < 0f)
                        {
                            diagonalAngle -= 45;
                        }
                        else if (rotated.x > 0f && rotated.y > 0f)
                        {
                            diagonalAngle += 45;
                        }
                        else if (rotated.x < 0f && rotated.y < 0f)
                        {
                            diagonalAngle += 45;
                        }
                    }
                }
            }
        }
    }

    public void SpawnObject(bool forceSpawnImmediate = false)
    {
        //Interactables must be created first
        if (!createdInteractables)
        {
            CreateInteractables();
        }

        if (spawnedObject != null)
        {
            return;
        }

        if (furniture == null)
        {
            Game.LogError("Furniture preset missing!");
            return;
        }

        try
        {
            spawnedObject = Toolbox.Instance.SpawnObject(furniture.prefab, anchorNode.room.transform);
            spawnedObject.transform.position = anchorNode.position + offset;
            meshes = spawnedObject.GetComponentsInChildren<MeshRenderer>().ToList();

            //Set to static shadow caster
            foreach(MeshRenderer mesh in meshes)
            {
                mesh.staticShadowCaster = true;
                
                //Scenarios where we need to enable the mesh right away...
                if (Game.Instance.freeCam || forceSpawnImmediate || furniture.allowStaticBatching) mesh.enabled = true;
                else
                {
                    mesh.enabled = !ObjectPoolingController.Instance.useRange; //Disable at start
                }
            }

            //Add to range check list for disabling meshes
            if (ObjectPoolingController.Instance.useRange && !Game.Instance.freeCam && !ObjectPoolingController.Instance.furnitureRangeToEnableDisableList.Contains(this))
            {
                ObjectPoolingController.Instance.furnitureRangeToEnableDisableList.Add(this);
            }
        }
        catch
        {
            Game.LogError("Unable to spawn furniture!");
            return;
        }

        //Spawn from ceiling...
        if (furnitureClasses[0].ceilingPiece)
        {
            spawnedObject.transform.localPosition = new Vector3(spawnedObject.transform.localPosition.x, (anchorNode.floor.defaultCeilingHeight * 0.1f), spawnedObject.transform.localPosition.z) + offset;
        }

        spawnedObject.transform.localEulerAngles = new Vector3(0, angle + diagonalAngle, 0);

        //Multiply scale
        Vector3 prefabScale = furniture.prefab.transform.localScale;
        spawnedObject.transform.localScale = new Vector3(prefabScale.x * scaleMultiplier.x, prefabScale.y * scaleMultiplier.y, prefabScale.z * scaleMultiplier.z);

        //This is static
        spawnedObject.isStatic = true; //NOTE: This has no effects in builds!

        //Name according to cluster
        if(Game.Instance.collectDebugData)
        {
            spawnedObject.transform.name = furniture.prefab.name + " " + id + ": " + " (Offset: " + offset + ") CP: " + furnitureClasses[0].ceilingPiece;

            foreach (NewNode coveredNode in coversNodes)
            {
                spawnedObject.transform.name += coveredNode.nodeCoord + ", ";
            }
        }

        //Apply colouring
        if (furniture.inheritColouringFromDecor && furniture.variations.Count > 0)
        {
            foreach(MeshRenderer rend in meshes)
            {
                MaterialsController.Instance.ApplyMaterialKey(rend, matKey);
            }
        }

        //Apply art material
        if (art != null)
        {
            for (int i = 0; i < spawnedObject.transform.childCount; i++)
            {
                Transform child = spawnedObject.transform.GetChild(i);

                if (child.tag == "Art")
                {
                    //Is this a wall picture?
                    MeshRenderer rend = child.GetComponent<MeshRenderer>();

                    Material mat = art.material;

                    //Try to load dynamic text image
                    if(art.useDynamicText)
                    {
                        if(!GameplayController.Instance.dynamicTextImages.TryGetValue(art, out mat))
                        {
                            mat = art.material;
                        }
                    }

                    if (rend != null)
                    {
                        //Found it!
                        rend.sharedMaterial = mat;
                    }
                    else
                    {
                        //Is this a decal projector?
                        DecalProjector dec = child.GetComponent<DecalProjector>();

                        if (dec != null)
                        {
                            dec.material = mat;

                            //Set size to tex size * 0.02
                            Texture tex = dec.material.GetTexture("_BaseColorMap");
                            dec.size = new Vector3(tex.width * art.pixelScaleMultiplier, tex.height * art.pixelScaleMultiplier, 0.13f);
                        }
                    }

                    break;
                }
            }
        }

        //Set light layer
        foreach(MeshRenderer rend in meshes)
        {
            Toolbox.Instance.SetLightLayer(rend, anchorNode.room.building, anchorNode.room.preset.forceStreetLightLayer);

            //Add for static batching...
            if(furniture.allowStaticBatching)
            {
                if(rend.enabled)
                {
                    MeshFilter mf = rend.gameObject.GetComponent<MeshFilter>();

                    if(mf != null)
                    {
                        //Check this doesn't contain a door script
                        DoorMovementController dmc = rend.gameObject.GetComponent<DoorMovementController>();

                        if(dmc == null)
                        {
                            anchorNode.room.AddForStaticBatching(mf.gameObject, mf.sharedMesh, rend.sharedMaterial);
                        }
                    }
                }
            }
        }

        //Link interactables
        InteractableController[] interactables = spawnedObject.GetComponentsInChildren<InteractableController>();

        foreach (InteractableController ic in interactables)
        {
            //Find matching interactable
            Interactable matching = integratedInteractables.Find(item => item.pt == (int)ic.id && item.pt != (int)InteractableController.InteractableID.none);

            if (matching != null)
            {
                //Run setup
                ic.Setup(matching);

                //Also trigger spawn in existing interactables
                matching.loadedGeometry = true;
                matching.spawnedObject = ic.gameObject;
                matching.OnSpawn();
            }
            else
            {
                Game.LogError("Unable to match furntiure " + furniture.name + " with interactable " + ic.id.ToString() + " CreatedInteractables: " + createdInteractables);
            }
        }
    }

    public void DespawnObject()
    {
        if (spawnedObject != null)
        {
            Toolbox.Instance.DestroyObject(spawnedObject);
            meshes.Clear();
        }
    }

    public void Delete(bool removeIntegratedInteractables, FurnitureClusterLocation.RemoveInteractablesOption removeSpawnedInteractables)
    {
        if (cluster != null)
        {
            cluster.DeleteFurniture(id, removeIntegratedInteractables, removeSpawnedInteractables);
        }
        else Game.LogError("Unable to delete furniture " + id + " from cluster, as cluster is null");
    }

    //Remove all spawned interactables
    public void RemoveSpawnedInteractables()
    {
        if(spawnedInteractables != null)
        {
            for (int i = 0; i < spawnedInteractables.Count; i++)
            {
                spawnedInteractables[i].SafeDelete(false);
                i--;
            }

            for (int i = 0; i < spawnedInteractables.Count; i++)
            {
                if(spawnedInteractables[i]== null)
                {
                    spawnedInteractables.RemoveAt(i);
                    i--;
                }
            }
        }
    }

    //Remove all integrated interactables
    public void RemoveIntegratedInteractables()
    {
        if(integratedInteractables != null)
        {
            for (int u = 0; u < integratedInteractables.Count; u++)
            {
                integratedInteractables[u].Delete();
                u--;
            }

            integratedInteractables.Clear();
        }

        createdInteractables = false;
        integratedIDAssign = 1; //Reset integrated id
    }

    //Create interactables from the preset. These must be created before any objects or you'll have pairing issues!
    public void CreateInteractables()
    {
        if (createdInteractables) return;
        if (furniture == null) return;

        foreach (FurniturePreset.IntegratedInteractable integrated in furniture.integratedInteractables)
        {
            if (integrated.pairToController == InteractableController.InteractableID.none) continue;

            //Attempt to load object from save data...
            if(CityConstructor.Instance != null && !CityConstructor.Instance.generateNew)
            {
                //Load interactable...
                //Edit: Don't save integrated interactables with the city data
                //Interactable inter = CityConstructor.Instance.currentData.interactables.Find(item => item.fp == id && item.fsoi <= -1 && item.pt == (int)integrated.pairToController);

                Interactable inter = null;

                //Is there a more recent save state version of this object?
                if (CityConstructor.Instance.saveState != null)
                {
                    //Look for matching ID in save state...
                    int newerVersionIndex = CityConstructor.Instance.saveState.interactables.FindIndex(item => item.fp == id && item.fsoi <= -1 && item.pt == (int)integrated.pairToController);

                    //If this is found, load from save state instead of city data...
                    if (newerVersionIndex > -1)
                    {
                        inter = CityConstructor.Instance.saveState.interactables[newerVersionIndex];

                        //Remove from save state to save loading later...
                        CityConstructor.Instance.saveState.interactables.RemoveAt(newerVersionIndex);

                        inter.wasLoadedFromSave = true;
                        integratedIDAssign++; //We have found an integrated interactable, so + to this so we can load the next one successfully...
                    }
                }

                //Successfully found in save data, load this instead...
                if(inter != null)
                {
                    inter.MainSetupStart();
                    inter.OnLoad();

                    if(!integratedInteractables.Contains(inter))
                    {
                        integratedInteractables.Add(inter);
                    }

                    continue; //Move onto next object...
                }
            }

            //Otherwise create object...
            InteractablePreset intPreset = integrated.preset;
            Human ownedBy = null;
            NewAddress ownedByAddress = null;

            //Get owned by through finding corresponding value matching letter designation
            if(integrated.belongsTo != FurniturePreset.SubObjectOwnership.everybody && integrated.belongsTo != FurniturePreset.SubObjectOwnership.nobody)
                {
                    if (!pickedOwners)
                    {
                        Game.LogError("Object: " + furniture.name + ": Trying to assign ownership to interactables, but owners have not yet been picked: " + ownerMap.Count + " " + pickedOwners);
                    }
                    else
                    {
                        int ownershipIndex = (int)integrated.belongsTo - 2;

                        foreach (KeyValuePair<OwnerKey, int> pair in ownerMap)
                        {
                            if (pair.Value == ownershipIndex)
                            {
                                //Owned by a human...
                                if(pair.Key.human != null)
                                {
                                    ownedBy = pair.Key.human;
                                    break;
                                }
                                //Owned by an address...
                                else if(pair.Key.address != null)
                                {
                                    ownedByAddress = pair.Key.address;
                                    break;
                                }
                            }
                        }

                        if (ownedBy == null && ownedByAddress == null)
                        {
                            Game.Log("Object: " + furniture.name + ": Could not find interactable owner for index " + ownershipIndex + ". Ownership list count of " + ownerMap.Count);
                        }
                    }
                }

            //Look at the prefab and find the local positions of all the controllers. We can use this for localpositions.
            List <InteractableController> controllerLocs = furniture.prefab.GetComponentsInChildren<InteractableController>(true).ToList();

            //Find their proper spawn positions...
            InteractableController correspondingController = controllerLocs.Find(item => item.id == integrated.pairToController);

            if(correspondingController == null && Game.Instance.collectDebugData)
            {
                Game.LogError(furniture.name + " Could not find controller in prefab " + furniture.prefab.name + " for pairing id " + integrated.pairToController.ToString() + " (" + integrated.preset.name + ")");

                foreach(InteractableController ic in controllerLocs)
                {
                    Game.Log("Listing controller for " + furniture.prefab.name + ": " + ic.id.ToString());
                }
            }

            //If this isn't inside a child object, always use 0,0,0 as it is a component of the furniture transform itself
            Vector3 iLocPos = Vector3.zero;
            Vector3 iLocRot = Vector3.zero;

            if(furniture.prefab == null)
            {
                Game.Log("Furniture prefab is null for " + furniture.name);
            }
            else if(correspondingController == null)
            {
                Game.Log("Unable for find corresponding controller for integrated interactable on " + furniture.name);
            }
            else
            {
                if (furniture.prefab.transform != correspondingController.transform)
                {
                    iLocPos = correspondingController.transform.localPosition;
                    iLocRot = correspondingController.transform.localEulerAngles;

                    //Find position relative to furniture parent
                    Transform hi = correspondingController.transform.parent;

                    while(hi != null && hi != furniture.prefab.transform)
                    {
                        iLocPos += hi.transform.localPosition;
                        iLocRot += hi.transform.localEulerAngles;
                        hi = hi.parent;
                    }
                }

            }

            List<Interactable.Passed> pv = null;

            //If owned by address, use the passed vars to store the data...
            if (ownedByAddress != null)
            {
                pv = new List<Interactable.Passed>();
                pv.Add(new Interactable.Passed(Interactable.PassedVarType.ownedByAddress, ownedByAddress.id));
            }

            //Position above the zero point otherwise you will have problems with calculated postion
            Interactable newInt = InteractableCreator.Instance.CreateFurnitureIntegratedInteractable(intPreset, anchorNode.room, this, ownedBy, ownedBy, null, iLocPos, iLocRot, integrated.pairToController, integrated.belongsTo, intPreset.isLight, pv);
            integratedInteractables.Add(newInt); //Add to interactables
        }

        //Copy furniture interactabes to all nodes this furniture occupies
        foreach (NewNode node in coversNodes)
        {
            foreach (Interactable interactable in integratedInteractables)
            {
                if (!node.interactables.Contains(interactable))
                {
                    node.AddInteractable(interactable);
                }
            }
        }

        createdInteractables = true;
    }

    //Assign a new owner
    public void AssignOwner(Human newOwner, bool updateIntegratedObjectOwnership)
    {
        if(newOwner == null)
        {
            Game.LogError("CityGen: Trying to assign a null owner to furniture. This probably isn't something you want.");
            return;
        }

        //Add new ownership class reference...
        if (!anchorNode.room.gameLocation.furnitureBelongsTo.ContainsKey(furnitureClasses[0].ownershipClass))
        {
            anchorNode.room.gameLocation.furnitureBelongsTo.Add(furnitureClasses[0].ownershipClass, new Dictionary<FurnitureLocation, List<Human>>());
        }

        //Add new furniture reference...
        if (!anchorNode.room.gameLocation.furnitureBelongsTo[furnitureClasses[0].ownershipClass].ContainsKey(this))
        {
            anchorNode.room.gameLocation.furnitureBelongsTo[furnitureClasses[0].ownershipClass].Add(this, new List<Human>());
        }

        //Assign owners
        anchorNode.room.gameLocation.furnitureBelongsTo[furnitureClasses[0].ownershipClass][this].Add(newOwner);

        //Within this object's preset, find corresponding IDs to assign to the new owner
        FurnitureLocation.OwnerKey k = new OwnerKey(newOwner);

        if (!ownerMap.ContainsKey(k))
        {
            //Find the 'next' owner ID to assign...
            int assign = -1;

            foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> pair in ownerMap)
            {
                assign = Mathf.Max(assign, pair.Value);
            }

            ownerMap.Add(k, assign + 1);
        }

        //Find any interactables that should belong to this.
        //USUALLY this won't be triggered as when owners are picked for the furniture, interactables aren't created yet, but it's useful for when we set a new owner after creation
        if (createdInteractables && updateIntegratedObjectOwnership)
        {
            UpdateIntegratedObjectOwnership();

            //foreach (Interactable inter in integratedInteractables)
            //{
            //    int ownershipIndex = (int)inter.pto - 2;

            //    if (ownershipIndex >= 0)
            //    {
            //        if (ownershipIndex == ownerMap[k])
            //        {
            //            inter.SetOwner(newOwner);
            //            //Game.Log("Object: Set owner for " + inter.preset.name + " " + inter.preset.pairToID.ToString() + " corresponding to owner " + assign);

            //            //Set as bed
            //            if(inter.preset.specialCaseFlag == InteractablePreset.SpecialCase.sleepPosition)
            //            {
            //                newOwner.SetBed(inter);
            //            }

            //            //Set as job position
            //            if (newOwner.job != null && newOwner.job.employer != null && newOwner.job.preset.ownsWorkPosition)
            //            {
            //                if(newOwner.job.preset.jobPostion != InteractablePreset.SpecialCase.none)
            //                {
            //                    if (newOwner.job.preset.jobPostion == inter.preset.specialCaseFlag)
            //                    {
            //                        newOwner.SetWorkFurniture(inter);
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
        }

        //if(Game.Instance.collectDebugData && newOwner != null) debugOwners.Add(newOwner);
    }

    public void AssignOwner(NewAddress newOwner, bool updateIntegratedObjectOwnership)
    {
        if (newOwner == null)
        {
            Game.LogError("CityGen: Trying to assign a null owner to furniture. This probably isn't something you want.");
            return;
        }

        ////Add new ownership class reference...
        //if (!anchorNode.room.gameLocation.furnitureBelongsTo.ContainsKey(furnitureClasses[0].ownershipClass))
        //{
        //    anchorNode.room.gameLocation.furnitureBelongsTo.Add(furnitureClasses[0].ownershipClass, new Dictionary<FurnitureLocation, List<object>>());
        //}

        ////Add new furniture reference...
        //if (!anchorNode.room.gameLocation.furnitureBelongsTo[furnitureClasses[0].ownershipClass].ContainsKey(this))
        //{
        //    anchorNode.room.gameLocation.furnitureBelongsTo[furnitureClasses[0].ownershipClass].Add(this, new List<object>());
        //}

        ////Assign owners
        //anchorNode.room.gameLocation.furnitureBelongsTo[furnitureClasses[0].ownershipClass][this].Add(newOwner);

        FurnitureLocation.OwnerKey k = new OwnerKey(newOwner);

        //Within this object's preset, find corresponding IDs to assign to the new owner
        if (!ownerMap.ContainsKey(k))
        {
            //Find the 'next' owner ID to assign...
            int assign = -1;

            foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> pair in ownerMap)
            {
                assign = Mathf.Max(assign, pair.Value);
            }

            ownerMap.Add(k, assign + 1);
        }

        if (Game.Instance.collectDebugData && newOwner != null) debugOwners.Add(newOwner);

        //Find any interactables that should belong to this.
        //USUALLY this won't be triggered as when owners are picked for the furniture, interactables aren't created yet, but it's useful for when we set a new owner after creation
        if (createdInteractables && updateIntegratedObjectOwnership) UpdateIntegratedObjectOwnership();
    }

    public void UpdateIntegratedObjectOwnership()
    {
        foreach (Interactable i in integratedInteractables)
        {
            if (i == null) continue;

            InteractableController.InteractableID pairTo = (InteractableController.InteractableID)i.pt;

            //Otherwise create object...
            InteractablePreset intPreset = i.preset;
            Human ownedBy = null;
            NewAddress ownedByAddress = null;

            FurniturePreset.IntegratedInteractable integrated = furniture.integratedInteractables.Find(item => item.pairToController == pairTo);

            if(integrated != null)
            {
                //Get owned by through finding corresponding value matching letter designation
                //Changes to save data means this has to be done on loading any game...
                if (integrated.belongsTo != FurniturePreset.SubObjectOwnership.everybody && integrated.belongsTo != FurniturePreset.SubObjectOwnership.nobody)
                {
                    if (!pickedOwners)
                    {
                        Game.LogError("Object: " + furniture.name + ": " + anchorNode.position + " Trying to assign ownership to interactables, but owners have not yet been picked: " + ownerMap.Count + " " + pickedOwners);
                    }
                    else
                    {
                        int ownershipIndex = (int)integrated.belongsTo - 2;

                        foreach (KeyValuePair<OwnerKey, int> pair in ownerMap)
                        {
                            if (pair.Value == ownershipIndex)
                            {
                                //Owned by a human...
                                if (pair.Key.human != null)
                                {
                                    ownedBy = pair.Key.human;
                                    break;
                                }
                                //Owned by an address...
                                else if (pair.Key.address != null)
                                {
                                    ownedByAddress = pair.Key.address;
                                    break;
                                }
                            }
                        }

                        if (ownedBy == null && ownedByAddress == null)
                        {
                            Game.Log("Object: " + furniture.name + ": " + anchorNode.position + "  Could not find interactable owner for index " + ownershipIndex + ". Ownership list count of " + ownerMap.Count);
                        }
                    }
                }

                //If owned by address, use the passed vars to store the data...
                if(ownedBy != null)
                {
                     i.SetOwner(ownedBy);
                    //Game.Log("Object: Set owner for " + inter.preset.name + " " + inter.preset.pairToID.ToString() + " corresponding to owner " + assign);

                    //Set as bed
                    if (i.preset.specialCaseFlag == InteractablePreset.SpecialCase.sleepPosition)
                    {
                        ownedBy.SetBed(i);
                    }

                    //Set as job position
                    if (ownedBy.job != null && ownedBy.job.employer != null && ownedBy.job.preset.ownsWorkPosition)
                    {
                        if (ownedBy.job.preset.jobPostion != InteractablePreset.SpecialCase.none)
                        {
                            if (ownedBy.job.preset.jobPostion == i.preset.specialCaseFlag)
                            {
                                ownedBy.SetWorkFurniture(i);
                            }
                        }
                    }

                    if (Game.Instance.collectDebugData) debugOwners.Add(ownedBy);
                }

                if (ownedByAddress != null)
                {
                    if (i.pv == null) i.pv = new List<Interactable.Passed>();

                    if(!i.pv.Exists(item => item.varType == Interactable.PassedVarType.ownedByAddress))
                    {
                        i.pv.Add(new Interactable.Passed(Interactable.PassedVarType.ownedByAddress, ownedByAddress.id));
                        i.objectRef = ownedByAddress;
                    }

                    i.UpdateName();
                }
            }
        }
    }

    //Get the centre world postion from average nodes
    public Vector3 GetWorldAveragePosition()
    {
        Vector3 avPos = Vector3.zero;

        foreach (NewNode node in coversNodes)
        {
            avPos += node.position;
        }

        return avPos / (float)coversNodes.Count;
    }

    //Calculated rotated walkable points
    public void CalculateWalkableSublocations()
    {
        foreach (FurnitureClass furnClass in furnitureClasses)
        {
            //if (furnClass.name == "HangingBasket")
            //{
            //    Game.Log("Created handing basket at " + anchorNode.gameLocation.name);
            //}

            foreach (FurnitureClass.FurniureWalkSubLocations subLoc in furnClass.sublocations)
            {
                //Rotate by furniture
                Vector2 rotated = Toolbox.Instance.RotateVector2CW(subLoc.offset, angle - 180);
                Vector3 offsetCoord = anchorNode.nodeCoord + new Vector3(Mathf.Round(rotated.x), Mathf.Round(rotated.y), 0);

                NewNode foundNode = coversNodes.Find(item => item.nodeCoord == offsetCoord);

                if (foundNode != null)
                {
                    if (!sublocations.ContainsKey(foundNode))
                    {
                        sublocations.Add(foundNode, new List<Vector3>());
                    }

                    //Now rotate all points within this node...
                    foreach (Vector3 pointV3 in subLoc.sublocations)
                    {
                        Vector2 rot = new Vector2(pointV3.x, pointV3.z);
                        rot = Toolbox.Instance.RotateVector2CW(rot, angle);
                        sublocations[foundNode].Add(new Vector3(rot.x, pointV3.y, rot.y));
                    }
                }
                else
                {
                    Game.Log("Unable to find walkable sublocation for " + furnClass.presetName + " + in 'covered nodes': " + offsetCoord.ToString() + " (there are " + coversNodes.Count + " covered nodes) Rotated by angle " + angle);
                }
            }
        }
    }

    //Get local locations relative to the base parent furniture object instead of whatever the subobject is parented to...
    public Vector3 GetSubObjectLocalPosition(FurniturePreset.SubObject subObj)
    {
        Vector3 ret = subObj.localPos;

        //Find the correct prefab
        GameObject findIn = furniture.prefab;
        //if (spawnedObject != null) findIn = spawnedObject;

        Transform spawnParent = findIn.transform;

        if(subObj.parent != null && subObj.parent.Length > 0)
        {
            spawnParent = Toolbox.Instance.SearchForTransform(findIn.transform, subObj.parent);
        }

        int safety = 6;

        while (spawnParent.transform.parent != null && spawnParent.transform.parent != findIn.transform && safety > 0)
        {
            ret += spawnParent.transform.parent.localPosition;
            spawnParent = spawnParent.transform.parent;
            safety--;
        }

        //ret = spawnParent.TransformPoint(subObj.localPos);
        //ret = findIn.transform.InverseTransformPoint(ret);

        return ret;
    }

    //Get local locations relative to the base parent furniture object instead of whatever the subobject is parented to...
    public Vector3 GetSubObjectLocalEuler(FurniturePreset.SubObject subObj)
    {
        Vector3 ret = subObj.localRot;

        //Find the correct prefab
        GameObject findIn = furniture.prefab;
        //if (spawnedObject != null) findIn = spawnedObject;

        Transform spawnParent = findIn.transform;

        if (subObj.parent != null && subObj.parent.Length > 0)
        {
            spawnParent = Toolbox.Instance.SearchForTransform(findIn.transform, subObj.parent);
        }

        int safety = 6;

        while(spawnParent.transform.parent != null && spawnParent.transform.parent != findIn.transform && safety > 0)
        {
            ret += spawnParent.transform.parent.localEulerAngles;
            spawnParent = spawnParent.transform.parent;
            safety--;
        }

        return ret;
    }
}
