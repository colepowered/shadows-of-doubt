﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

//Controls a checkbox.
//Script pass 1
public class Checkbox : MonoBehaviour
{
	//Settings
	public bool ticked = false;

	//References
	public Image graphic;
	public Sprite unTickedSprite;
	public Sprite tickedSprite;
	public Checkbox orCheckbox;

	void Awake()
	{
		Set(ticked);
	}

	public void Toggle()
	{
		if (ticked) ticked = false;
		else
		{
			ticked = true;
			if (orCheckbox != null) orCheckbox.Set(false);
		}

		SetImage();
	}

	public void Set(bool setTo)
	{
		ticked = setTo;
		SetImage();
	}

	public void SetImage()
	{
		if (ticked) graphic.sprite = tickedSprite;
		else graphic.sprite = unTickedSprite;
	}
}
