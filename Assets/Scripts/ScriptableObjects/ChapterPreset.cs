﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "chapter_data", menuName = "Database/Chapter Preset")]

public class ChapterPreset : SoCustomComparison
{
    [Header("Settings")]
    [Tooltip("The number of the chapter. This must be exclusive.")]
    public int chapterNumber = 0;
    [Tooltip("The prefab that contains the logic for this chapter")]
    public GameObject scriptObject;
    [Tooltip("The chapter script reference")]
    public string dictionary = "chapter.1";
    [Tooltip("Ask to enable the tutorial if this chapter is played.")]
    public bool askToEnableTutorial = false;

    [Header("Starting Time")]
    public float startingHour = 2.56f;
    public int startingDate = 2;
    public int startingMonth = 0;
    public int startingYear = 1;

    //The time data on the game's 0:00:00:0000 hour
    //Only time cycles idependent of the actual time data are needed here
    public int yearZeroLeapYearCycle = 0;
    public int dayZero = 0;

    [Header("Starting Weather")]
    [Range(0f, 1f)]
    public float rainAmount = 0f;
    [Range(0f, 1f)]
    public float windAmount = 0f;
    [Range(0f, 1f)]
    public float snowAmount = 0f;
    [Range(0f, 1f)]
    public float lightningAmount = 0f;
    public float transitionSpeed = 0.1f;

    [Header("Television schedule")]
    public BroadcastSchedule broadcastSchedule;

    [Header("Pre-Simulation")]
    [Tooltip("Simulate at fast forward until a certain point (dictated manually)")]
    public bool usePreSimulation = true;
    [EnableIf("usePreSimulation")]
    [Tooltip("The minimum amount of time to pre-simulate")]
    public float minimumPreSimLength = 23.5f;

    [Header("Bespoke Audio Events")]
    [ReorderableList]
    public List<AudioEvent> audioEvents = new List<AudioEvent>();

    [Header("Bespoke Dialog")]
    [ReorderableList]
    public List<DialogPreset> dialogEvents = new List<DialogPreset>();

    [Header("Crimes")]
    public List<MurderPreset> crimePool = new List<MurderPreset>();
    public List<MurderMO> MOPool = new List<MurderMO>();

    [Header("Chapter Parts")]
    [ReorderableList]
    [Tooltip("Included mostly for reference: You can use the chapter controller to switch between these.")]
    public List<string> partNames = new List<string>();
    [Tooltip("The part to load when the chapter is loaded. You can use this to skip parts for testing.")]
    public int startingPart = 0;

    [Button]
    public virtual void SkipToChapterPart()
    {
        ChapterController.Instance.SkipToChapterPart(startingPart, true, false);
    }
}
