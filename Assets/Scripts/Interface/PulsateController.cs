﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PulsateController : MonoBehaviour
{
    public Image img;
    public bool getNormalColourAtStart = true;
    public Color normalColour = Color.white;
    public Color pulsateColour = Color.red;
    public float speed = 5f;
    public float progress = 0f;
    public bool onoff = false;

	// Use this for initialization
	void Start ()
    {
        if (img == null) img = this.gameObject.GetComponent<Image>();

        if (getNormalColourAtStart)
        {
            normalColour = img.color;
        }

        progress = 0f;
	}
	
	// Update is called once per frame
	void Update()
    {
        //Set colour
        Color newCol = Color.Lerp(normalColour, pulsateColour, progress);
        img.color = newCol;

        if(!onoff && progress < 1f)
        {
            progress += speed * Time.deltaTime;
        }
        else if(onoff && progress > 0f)
        {
            progress -= speed * Time.deltaTime;
        }

        //Switch
        if (progress >= 1f && !onoff) onoff = true;
        else if (progress <= 0f && onoff) onoff = false;
    }

    private void OnDisable()
    {
        img.color = normalColour; //Return to normal colour
        progress = 0f;
    }
}
