﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ApartmentSalesController : MonoBehaviour
{
    public WindowContentController windowContent;
    public InfoWindow parentWindow;

    public TextMeshProUGUI dataText;
    public TextMeshProUGUI descriptionText;
    public ButtonController purchaseButton;
    public RawImage img;

    private void OnEnable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        if (windowContent == null) windowContent = this.gameObject.GetComponentInParent<WindowContentController>();
        parentWindow.OnWindowRefresh += UpdateDetails;

        UpdateDetails();
    }

    private void OnDisable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.OnWindowRefresh -= UpdateDetails;
    }

    public void UpdateDetails()
    {
        //Which land value category is this in?
        int lv = 0;
        purchaseButton.SetInteractable(false);

        if (parentWindow.passedInteractable.forSale != null)
        {
            if(parentWindow.passedInteractable.forSale.thisAsAddress != null)
            {
                img.texture = parentWindow.passedInteractable.forSale.evidenceEntry.GetPhoto(parentWindow.evidenceKeys);
                img.color = Color.white;

                dataText.text = Strings.GetTextForComponent("600d4a18-7306-4871-a68e-e7764ae62f81", parentWindow.passedInteractable, null, null, dataKeys: parentWindow.evidenceKeys); //data

                lv = Mathf.RoundToInt(parentWindow.passedInteractable.forSale.thisAsAddress.normalizedLandValue * 2f);

                if(lv <= 0)
                {
                    descriptionText.text = Strings.GetTextForComponent("3651e904-22e5-4093-9660-e59140ea6176", parentWindow.passedInteractable, null, null, dataKeys: parentWindow.evidenceKeys); //description
                }
                else if(lv == 1)
                {
                    descriptionText.text = Strings.GetTextForComponent("f1da9ff4-f0f8-42cb-b295-04d7ac7d353d", parentWindow.passedInteractable, null, null, dataKeys: parentWindow.evidenceKeys); //description
                }
                else if(lv >= 2)
                {
                    descriptionText.text = Strings.GetTextForComponent("2a8c5e45-1b74-46f6-813d-6cae5e65a396", parentWindow.passedInteractable, null, null, dataKeys: parentWindow.evidenceKeys); //description
                }

                int price = parentWindow.passedInteractable.forSale.GetPrice(false);
                purchaseButton.text.text = Strings.Get("evidence.generic", "Purchase") + " " + CityControls.Instance.cityCurrency + price;

                if(GameplayController.Instance.money >= price)
                {
                    purchaseButton.SetInteractable(true);
                }
            }
        }
    }

    public void OnPurchaseButton()
    {
        if (GameplayController.Instance.money >= parentWindow.passedInteractable.forSale.GetPrice(false))
        {
            PopupMessageController.Instance.PopupMessage("BuyApartment", true, true, RButton: "Confirm");
            PopupMessageController.Instance.OnRightButton += ConfirmApartmentPurchase;
            PopupMessageController.Instance.OnLeftButton += CancelApartmentPurchase;
        }
    }

    public void ConfirmApartmentPurchase()
    {
        PopupMessageController.Instance.OnRightButton -= ConfirmApartmentPurchase;
        PopupMessageController.Instance.OnLeftButton -= CancelApartmentPurchase;

        GameplayController.Instance.AddMoney(-parentWindow.passedInteractable.forSale.GetPrice(false), true, "Property purchase");
        PlayerApartmentController.Instance.BuyNewResidence(parentWindow.passedInteractable.forSale.residence);

        //Open window to new apartment
        SessionData.Instance.PauseGame(true);
        List<Evidence.DataKey> passKeys = new List<Evidence.DataKey>();
        passKeys.Add(Evidence.DataKey.location);
        passKeys.Add(Evidence.DataKey.photo);
        passKeys.Add(Evidence.DataKey.name);
        passKeys.Add(Evidence.DataKey.purpose);
        passKeys.Add(Evidence.DataKey.blueprints);
        InterfaceController.Instance.SpawnWindow(parentWindow.passedInteractable.forSale.evidenceEntry, Evidence.DataKey.name, passKeys);
        parentWindow.CloseWindow(false);

        //Fanfare
        InterfaceController.Instance.ApartmentPurchaseDisplay();
    }

    public void CancelApartmentPurchase()
    {
        PopupMessageController.Instance.OnRightButton -= ConfirmApartmentPurchase;
        PopupMessageController.Instance.OnLeftButton -= CancelApartmentPurchase;
    }
}
