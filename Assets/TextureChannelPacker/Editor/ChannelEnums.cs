﻿using System;

namespace Assets.TextureChannelPacker.Editor
{
    internal enum Channel : byte
    {
        Red,
        Green,
        Blue,
        Alpha
    }

    [Flags]
    internal enum ChannelToggles : byte
    {
        Red = 1,
        Green = (1 << 1),
        Blue = (1 << 2),
        Alpha = (1 << 3)
    }
}