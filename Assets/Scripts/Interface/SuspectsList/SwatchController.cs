using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwatchController : ButtonController
{
    ColourPickerController controller;

    public void Setup(Color newColor, ColourPickerController newController)
    {
        controller = newController;
        SetButtonBaseColour(newColor);
    }

    public override void OnLeftClick()
    {
        controller.OnPickNewColour(this);

        base.OnLeftClick();
    }
}
