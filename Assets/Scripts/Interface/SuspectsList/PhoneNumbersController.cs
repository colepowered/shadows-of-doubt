﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using TMPro;

public class PhoneNumbersController : MonoBehaviour
{
    public RectTransform rect;
    public WindowContentController wcc;
    public bool isSetup = false;
    public bool isMini = false;

    public TextMeshProUGUI contentsText;
    public RectTransform entryParent;

    public TMP_InputField searchInputField;
    public List<PhoneNumberEntryController> spawnedEntries = new List<PhoneNumberEntryController>();

    public void Setup(WindowContentController newContentController)
    {
        rect = this.gameObject.GetComponent<RectTransform>();

        wcc = newContentController;

        if(isMini)
        {
            SetPageSize(new Vector2(338, 738));
        }
        else
        {
            SetPageSize(new Vector2(740, 738));
        }

        if(contentsText != null) contentsText.text = Strings.Get("ui.handbook", "phonenumbersheader");

        isSetup = true;
    }

    public void SetPageSize(Vector2 newSize)
    {
        rect.sizeDelta = newSize;
        wcc.normalSize = rect.sizeDelta;
    }

    private void OnEnable()
    {
        if (isSetup)
        {
            GameplayController.Instance.OnNewEvidenceHistory += UpdateListDisplay;
            UpdateListDisplay();
        }
    }

    private void OnDisable()
    {
         GameplayController.Instance.OnNewEvidenceHistory -= UpdateListDisplay;
    }

    private void OnDestroy()
    {
         GameplayController.Instance.OnNewEvidenceHistory -= UpdateListDisplay;
    }

    //Update facts list
    public void UpdateListDisplay()
    {
        Game.Log("Interface: Updating phone numbers list...");

        //Force deselect
        if (InterfaceController.Instance.selectedElement != null) InterfaceController.Instance.selectedElement.OnDeselect();

        rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 0);

        //Compile a list of file icons that should exist
        List<GameplayController.PhoneNumber> required = new List<GameplayController.PhoneNumber>();

        foreach (GameplayController.PhoneNumber h in GameplayController.Instance.acquiredNumbers)
        {
            if (required.Count >= 40) break; //Display 40 maximum entries
            if (required.Contains(h)) continue;

            if (searchInputField.text.Length <= 0)
            {
                required.Add(h);
            }
            else
            {
                string realText = Toolbox.Instance.GetTelephoneNumberString(h.number);

                //if (h.type == GameplayController.PasscodeType.room)
                //{
                //    NewRoom room = null;

                //    if(CityData.Instance.roomDictionary.TryGetValue(h.id, out room))
                //    {
                //        realText += room.name + ", " + room.gameLocation.evidenceEntry.GetNameForDataKey(Evidence.DataKey.name);
                //    }
                //}
                //else
                //{
                //    NewAddress address = null;

                //    if(CityData.Instance.addressDictionary.TryGetValue(h.id, out address))
                //    {
                //        if(address.evidenceEntry != null) realText += address.evidenceEntry.GetNameForDataKey(Evidence.DataKey.name);
                //    }
                //}

                //realText +=  " " + h.digits[0].ToString() + h.digits[1].ToString() + h.digits[2].ToString() + h.digits[3].ToString();

                if (realText.ToLower().Contains(searchInputField.text.ToLower()))
                {
                    required.Add(h);
                }
            }
        }

        //Go through existing list
        for (int i = 0; i < spawnedEntries.Count; i++)
        {
            PhoneNumberEntryController sec = spawnedEntries[i];

            //If a null entry, this has obviously been destroyed- remove entry
            if (sec == null)
            {
                spawnedEntries.RemoveAt(i);
                i--;
                continue;
            }

            if(required.Contains(sec.number))
            {
                required.Remove(sec.number);
                sec.VisualUpdate();
            }
            else
            {
                Destroy(sec.gameObject);
                spawnedEntries.RemoveAt(i);
                i--;
                continue;
            }
        }

        //The ones left in the found files list are buttons to spawn
        foreach (GameplayController.PhoneNumber sec in required)
        {
            GameObject newButton = null;

            newButton = Instantiate(PrefabControls.Instance.phoneNumberEntry, entryParent);

            PhoneNumberEntryController newEntry = newButton.GetComponent<PhoneNumberEntryController>();

            if(isMini)
            {
                newEntry.rect.sizeDelta = new Vector2(338, newEntry.rect.sizeDelta.y);
            }

            newEntry.Setup(sec);
            spawnedEntries.Add(newEntry);
        }

        for (int i = 0; i < spawnedEntries.Count; i++)
        {
            spawnedEntries[i].transform.SetAsLastSibling();
        }

        //Update rect sizes...
        entryParent.sizeDelta = new Vector2(entryParent.sizeDelta.x, Mathf.Max(spawnedEntries.Count * 130f + 24f, 466));
        SetPageSize(new Vector2(rect.sizeDelta.x, entryParent.sizeDelta.y + 500f));
    }

    public void ClearSearchButton()
    {
        searchInputField.text = string.Empty;
    }
}
