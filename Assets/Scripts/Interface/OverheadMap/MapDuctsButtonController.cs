﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

public class MapDuctsButtonController : ButtonController
{
    public NewFloor floor;
    public Vector2 range;

    //Generated image
    public Image generatedImage;
    public Texture2D tex;

    Action UpdateMapTex;

    public void Setup(NewFloor newAddress)
    {
        //Setup actions
        UpdateMapTex += GenerateMapImage;

        base.SetupReferences();
        floor = newAddress;
        floor.mapDucts = this; //Reference to this in the location class

        if (rect.sizeDelta.x > 0 && rect.sizeDelta.y > 0)
        {
            UpdateMapImageEndOfFrame();
        }
    }

    //Update map image at end of frame (ignore duplicate requests)
    public void UpdateMapImageEndOfFrame()
    {
        //Update @ end of frame
        Toolbox.Instance.InvokeEndOfFrame(UpdateMapTex, "Update map image");
    }

    //Combine/refresh images into a single one...
    public void GenerateMapImage()
    {
        Vector2 texRes = new Vector2(Mathf.RoundToInt(rect.sizeDelta.x / MapController.Instance.mapResolutionDivision), Mathf.RoundToInt(rect.sizeDelta.y / MapController.Instance.mapResolutionDivision));

        //Remove previous
        if (tex == null)
        {
            tex = new Texture2D((int)texRes.x, (int)texRes.y);
            tex.filterMode = FilterMode.Trilinear;
            tex.name = "Generated Texture";
            generatedImage.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100f);
            generatedImage.GetComponent<RectTransform>().sizeDelta = rect.sizeDelta;
            generatedImage.color = MapController.Instance.roomBaseColor;
            generatedImage.alphaHitTestMinimumThreshold = 0.5f;
        }

        //Color32 is apparently faster than normal color
        //Reset all pixels color to transparent
        Color32 resetColor = new Color32(0, 0, 0, 0);
        Color32[] resetColorArray = tex.GetPixels32();

        for (int i = 0; i < resetColorArray.Length; i++)
        {
            resetColorArray[i] = resetColor;
        }

        tex.SetPixels32(resetColorArray);

        //Scan each node, write it's ducts
        foreach(NewAddress ad in floor.addresses)
        {
            List<AirDuctGroup.AirDuctSection> upConnections = new List<AirDuctGroup.AirDuctSection>();
            List<AirDuctGroup.AirDuctSection> downConnections = new List<AirDuctGroup.AirDuctSection>();

            //Write ducts
            foreach (NewNode node in ad.nodes)
            {
                //Write ducts
                foreach(AirDuctGroup.AirDuctSection duct in node.airDucts)
                {
                    //Set button reference
                    if(duct.mapButton == null)
                    {
                        duct.mapButton = this;
                    }

                    //Skip if not discovered
                    if (!duct.discovered) continue;

                    Texture2D mapImage = null;

                    //Find the map section to write
                    List<Vector3Int> ductOffsets;
                    List<Vector3Int> ventOffsets;
                    duct.GetNeighborSections(out ductOffsets, out _, out ventOffsets);

                    //Combine duct and vent offsets for this purpse
                    ductOffsets.AddRange(ventOffsets);

                    //List<Vector3> ductOffsets = duct.group.GetDuctOffsets(duct.node, duct); //Get offsets, search for vents

                    //Is this an up/down connection?
                    if (duct.level == 0)
                    {
                        if (ductOffsets.Exists(item => item.z < 0))
                        {
                            downConnections.Add(duct);
                        }
                    }
                    else if(duct.level == 2)
                    {
                        if (ductOffsets.Exists(item => item.z > 0))
                        {
                            upConnections.Add(duct);
                        }
                    }

                    foreach (InteriorControls.AirDuctOffset air in InteriorControls.Instance.airDuctModels)
                    {
                        if (air.offsets.Count != ductOffsets.Count) continue; //Must be the same number of offsets...

                        bool valid = true;

                        foreach (Vector3 offset in ductOffsets)
                        {
                            if (!air.offsets.Contains(offset.normalized))
                            {
                                valid = false;
                                break;
                            }
                        }

                        if (!valid) continue;
                        if (air.maps.Count <= 0) continue;

                        mapImage = air.maps[Mathf.Min((int)duct.index, air.maps.Count - 1)]; //Get prefab model from configured list
                        break;
                    }

                    if(mapImage != null)
                    {
                        //Get the 0,0 location of this node within in the image
                        Vector2 mapCoord = MapController.Instance.NodeCoordToMap(node.nodeCoord);
                        Vector2 withinImage = (mapCoord - rect.anchoredPosition - new Vector2(MapController.Instance.nodePositionMultiplier * 0.5f, MapController.Instance.nodePositionMultiplier * 0.5f)) / MapController.Instance.mapResolutionDivision;

                        //Write texture including transparency
                        for (int i = 0; i < mapImage.width; i++)
                        {
                            for (int u = 0; u < mapImage.height; u++)
                            {
                                Color setCol = mapImage.GetPixel(i, u);

                                //Skip pixel if fully transparent...
                                if (setCol.a <= 0.001f) continue;

                                Color destCol = tex.GetPixel((int)withinImage.x + i, (int)withinImage.y + u);
                                setCol = setCol * setCol.a + destCol * (1f - setCol.a); //Apply with alpha
                                tex.SetPixel((int)withinImage.x + i, (int)withinImage.y + u, setCol);
                            }
                        }
                    }
                }
            }

            //Write vents
            foreach(NewRoom room in ad.rooms)
            {
                foreach(AirDuctGroup.AirVent vent in room.airVents)
                {
                    //Set button reference
                    if (vent.mapButton == null)
                    {
                        vent.mapButton = this;
                    }

                    if (!vent.discovered) continue;

                    //Wall vents
                    if (vent.wall != null)
                    {
                        //Get the 0,0 location of this node within in the image
                        Vector2 mapCoord = MapController.Instance.NodeCoordToMap(vent.node.nodeCoord - new Vector3(vent.wall.wallOffset.x, vent.wall.wallOffset.y, 0));
                        Vector2 withinImage = (mapCoord - rect.anchoredPosition - new Vector2(MapController.Instance.nodePositionMultiplier * 0.5f, MapController.Instance.nodePositionMultiplier * 0.5f)) / MapController.Instance.mapResolutionDivision;

                        Color32[] ventImg = MapController.Instance.vent.GetPixels32();
                        tex.SetPixels32((int)withinImage.x +9, (int)withinImage.y + 9, MapController.Instance.vent.width, MapController.Instance.vent.height, ventImg);
                    }
                    //Ceiling vents
                    else if(vent.node != null)
                    {
                        //Get the 0,0 location of this node within in the image
                        Vector2 mapCoord = MapController.Instance.NodeCoordToMap(vent.node.nodeCoord);
                        Vector2 withinImage = (mapCoord - rect.anchoredPosition - new Vector2(MapController.Instance.nodePositionMultiplier * 0.5f, MapController.Instance.nodePositionMultiplier * 0.5f)) / MapController.Instance.mapResolutionDivision;

                        Color32[] ventImg = MapController.Instance.vent.GetPixels32();
                        tex.SetPixels32((int)withinImage.x + 9, (int)withinImage.y + 9, MapController.Instance.vent.width, MapController.Instance.vent.height, ventImg);
                    }
                }
            }

            //Write connections
            foreach(AirDuctGroup.AirDuctSection duct in upConnections)
            {
                //Skip if undiscovered
                if (!duct.discovered) continue;

                //Get the 0,0 location of this node within in the image
                Vector2 mapCoord = MapController.Instance.NodeCoordToMap(duct.node.nodeCoord);
                Vector2 withinImage = (mapCoord - rect.anchoredPosition - new Vector2(MapController.Instance.nodePositionMultiplier * 0.5f, MapController.Instance.nodePositionMultiplier * 0.5f)) / MapController.Instance.mapResolutionDivision;

                //Write texture including transparency
                for (int i = 0; i < MapController.Instance.ventUpwardsConnection.width; i++)
                {
                    for (int u = 0; u < MapController.Instance.ventUpwardsConnection.height; u++)
                    {
                        Color setCol = MapController.Instance.ventUpwardsConnection.GetPixel(i, u);

                        //Skip pixel if fully transparent...
                        if (setCol.a <= 0.001f) continue;

                        Color destCol = tex.GetPixel((int)withinImage.x + i, (int)withinImage.y + u);
                        setCol = setCol * setCol.a + destCol * (1f - setCol.a); //Apply with alpha
                        tex.SetPixel((int)withinImage.x + i, (int)withinImage.y + u, setCol);
                    }
                }
            }

            foreach (AirDuctGroup.AirDuctSection duct in downConnections)
            {
                //Get the 0,0 location of this node within in the image
                Vector2 mapCoord = MapController.Instance.NodeCoordToMap(duct.node.nodeCoord);
                Vector2 withinImage = (mapCoord - rect.anchoredPosition - new Vector2(MapController.Instance.nodePositionMultiplier * 0.5f, MapController.Instance.nodePositionMultiplier * 0.5f)) / MapController.Instance.mapResolutionDivision;

                //Write texture including transparency
                for (int i = 0; i < MapController.Instance.ventDownwardsConnection.width; i++)
                {
                    for (int u = 0; u < MapController.Instance.ventDownwardsConnection.height; u++)
                    {
                        Color setCol = MapController.Instance.ventDownwardsConnection.GetPixel(i, u);

                        //Skip pixel if fully transparent...
                        if (setCol.a <= 0.001f) continue;

                        Color destCol = tex.GetPixel((int)withinImage.x + i, (int)withinImage.y + u);
                        setCol = setCol * setCol.a + destCol * (1f - setCol.a); //Apply with alpha
                        tex.SetPixel((int)withinImage.x + i, (int)withinImage.y + u, setCol);
                    }
                }
            }
        }

        tex.Apply(); //Apply changes
    }
}
