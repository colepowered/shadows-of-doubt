﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.IO;
using System.Linq;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Rendering;
using UnityEditor;
using NaughtyAttributes;
using System.Threading;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class Toolbox : MonoBehaviour
{
    private bool endOfFrameInvoke = false;
    private HashSet<Action> invokeEndOfFrame = new HashSet<Action>();
    private List<string> debugInvokeEndOfFrame = new List<string>();

    public static GameObject PoolingGroup { get; private set; }

    //Lists of all scriptable objects of certain types for easy access during runtime
    public List<Human.ShoeType> allShoeTypes = new List<Human.ShoeType>();
    public List<Evidence.DataKey> allDataKeys = new List<Evidence.DataKey>();
    public List<Descriptors.EthnicGroup> allEthnicities = new List<Descriptors.EthnicGroup>();
    public List<DDSSaveClasses.TreeTriggers> allTreeTriggers = new List<DDSSaveClasses.TreeTriggers>();
    public List<Acquaintance.ConnectionType> allConnectionTypes = new List<Acquaintance.ConnectionType>();
    public List<CompanyPreset.CompanyCategory> allCompanyCategories = new List<CompanyPreset.CompanyCategory>();
    public List<RetailItemPreset> allItems = new List<RetailItemPreset>();
    public List<ArtPreset> allArt = new List<ArtPreset>();
    public List<CompanyPreset> allCompanyPresets = new List<CompanyPreset>();
    public List<OccupationPreset> allJobs = new List<OccupationPreset>();
    public List<OccupationPreset> allCriminalJobs = new List<OccupationPreset>();
    public List<StreetTilePreset> allStreetTiles = new List<StreetTilePreset>();
    public List<ClothesPreset.OutfitCategory> allOutfitCategories = new List<ClothesPreset.OutfitCategory>();
    public List<CitizenOutfitController.CharacterAnchor> allCharacterAnchors = new List<CitizenOutfitController.CharacterAnchor>();
    public List<SyncDiskPreset> allSyncDisks = new List<SyncDiskPreset>();
    public List<AmbientZone> allAmbientZones = new List<AmbientZone>();
    public List<JobPreset> allSideJobs = new List<JobPreset>();
    public List<DistrictPreset> allDistricts = new List<DistrictPreset>();
    public List<HandwritingPreset> allHandwriting = new List<HandwritingPreset>();

    //Dictionary containing references to all evidence type files
    public Dictionary<string, EvidencePreset> evidencePresetDictionary = new Dictionary<string, EvidencePreset>();

    //Dictionary containing references to all fact type files
    public Dictionary<string, FactPreset> factPresetDictionary = new Dictionary<string, FactPreset>();

    public List<GroupPreset> allGroups = new List<GroupPreset>();
    public Dictionary<string, GroupPreset> groupsDictionary = new Dictionary<string, GroupPreset>();

    //Parsing scope dictionary
    public Dictionary<string, DDSScope> scopeDictionary = new Dictionary<string, DDSScope>();
    public Dictionary<string, DDSScope> globalScopeDictionary = new Dictionary<string, DDSScope>();

    public CloudSaveData devCloudSaveConfig;

    //Dictionary of object presets
    public Dictionary<string, InteractablePreset> objectPresetDictionary = new Dictionary<string, InteractablePreset>();
    public List<InteractablePreset> placeAtGameLocationInteractables = new List<InteractablePreset>();
    public List<InteractablePreset> placePerOwnerInteractables = new List<InteractablePreset>();
    public Dictionary<SubObjectClassPreset, List<InteractablePreset>> subObjectsDictionary = new Dictionary<SubObjectClassPreset, List<InteractablePreset>>();

    public Dictionary<string, AudioEvent> voiceActedDictionary = new Dictionary<string, AudioEvent>();

    public List<MurderPreset> allMurderPresets = new List<MurderPreset>();
    public List<MurderMO> allMurderMOs = new List<MurderMO>();

    public List<CharacterTrait> allCharacterTraits = new List<CharacterTrait>();
    public List<CharacterTrait> stage0Traits = new List<CharacterTrait>();
    public List<CharacterTrait> stage1Traits = new List<CharacterTrait>();
    public List<CharacterTrait> stage2Traits = new List<CharacterTrait>();
    public List<CharacterTrait> stage3Traits = new List<CharacterTrait>();
    public List<CharacterTrait> reasons = new List<CharacterTrait>();

    public List<BookPreset> allBooks = new List<BookPreset>();

    public List<NewspaperArticle> allArticles = new List<NewspaperArticle>();

    public List<AddressPreset> allAddressPresets = new List<AddressPreset>();
    public List<RoomConfiguration> allRoomConfigs = new List<RoomConfiguration>();
    public List<DesignStylePreset> allDesignStyles;
    public List<MaterialGroupPreset> allMaterialGroups = new List<MaterialGroupPreset>();
    public List<WallFrontagePreset> allWallFrontage = new List<WallFrontagePreset>();

    public Dictionary<DesignStylePreset, List<FurniturePreset>> furnitureDesignStyleRef = new Dictionary<DesignStylePreset, List<FurniturePreset>>();
    public Dictionary<RoomClassPreset, HashSet<FurniturePreset>> furnitureRoomTypeRef = new Dictionary<RoomClassPreset, HashSet<FurniturePreset>>();

    public Dictionary<DesignStylePreset, Dictionary<MaterialGroupPreset.MaterialType, List<MaterialGroupPreset>>> materialDesignStyleRef = new Dictionary<DesignStylePreset, Dictionary<MaterialGroupPreset.MaterialType, List<MaterialGroupPreset>>>();
    public Dictionary<DesignStylePreset, Dictionary<WallFrontageClass, List<WallFrontagePreset>>> wallFrontageStyleRef = new Dictionary<DesignStylePreset, Dictionary<WallFrontageClass, List<WallFrontagePreset>>>();
    public List<FurnitureCluster> allFurnitureClusters;
    public List<FurniturePreset> allFurniture = new List<FurniturePreset>();
    public List<RoomLightingPreset> allRoomLighting;

    public List<ColourSchemePreset> allColourSchemes;
    public List<AIGoalPreset> allGoals;
    public List<DialogPreset> allDialog;
    public List<DialogPreset> defaultDialogOptions;

    public List<InteractablePreset> allWeapons = new List<InteractablePreset>();

    //Loaded on game only: All DDS
    public Dictionary<string, DDSSaveClasses.DDSBlockSave> allDDSBlocks = new Dictionary<string, DDSSaveClasses.DDSBlockSave>();
    public Dictionary<string, DDSSaveClasses.DDSMessageSave> allDDSMessages = new Dictionary<string, DDSSaveClasses.DDSMessageSave>();
    public Dictionary<string, DDSSaveClasses.DDSTreeSave> allDDSTrees = new Dictionary<string, DDSSaveClasses.DDSTreeSave>();

    public Dictionary<string, HelpContentPage> allHelpContent = new Dictionary<string, HelpContentPage>();

    public List<ClothesPreset> allClothes = new List<ClothesPreset>();
    public Dictionary<string, ClothesPreset> clothesDictionary = new Dictionary<string, ClothesPreset>();

    public List<StatusPreset> allStatuses = new List<StatusPreset>();

    public int aiSightingLayerMask = 0;
    public int interactionRayLayerMask = 0;
    public int interactionRayLayerMaskNoRoomMesh = 0;
    public int printDetectionRayLayerMask = 0;
    public int sceneCaptureLayerMask = 0;
    public int mugShotCaptureLayerMask = 0;
    public int physicalObjectsLayerMask = 0;
    public int heldObjectsObjectsLayerMask = 0;
    public int spatterLayerMask = 0;
    public int textToImageMask = 0;
    public int lightCullingMask = 0;

    private List<Descriptors.EthnicGroup> rEthnicity = new List<Descriptors.EthnicGroup>();
    public int totalEthnictiyFrequencyCount = 0;

    public char[] alphabet = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

    //Resources Cache: Type - Name - File
    private Dictionary<Type, Dictionary<string, ScriptableObject>> resourcesCache = new Dictionary<Type, Dictionary<string, ScriptableObject>>();

    //Material property reference: For getting material settings on walls, ceilings etc through the material itself
    public Dictionary<Material, MaterialGroupPreset> materialProperties = new Dictionary<Material, MaterialGroupPreset>();

    //Furniture property reference: For getting furntiure preset from the furniture mesh itself
    public Dictionary<Mesh, FurniturePreset> furnitureMeshReference = new Dictionary<Mesh, FurniturePreset>();

    //Important for generating different but predicatable random numbers
    public string lastRandomNumberKey = string.Empty;

    private char[] seedLetters = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
    private char[] seedNumbers = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

    [System.Serializable]
    public struct MaterialKey
    {
        public Material baseMaterial;
        public Color mainColour;
        public Color colour1;
        public Color colour2;
        public Color colour3;
        public float grubiness;

        public bool Equals(MaterialKey other)
        {
            return Equals(other, this);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var objectToCompareWith = (MaterialKey)obj;

            return objectToCompareWith.baseMaterial.name == baseMaterial.name && Approximately(objectToCompareWith.mainColour, mainColour) && Approximately(objectToCompareWith.colour1, colour1) &&
                   Approximately(objectToCompareWith.colour2, colour2) && Approximately(objectToCompareWith.colour3, colour3) && Mathf.Approximately(objectToCompareWith.grubiness, grubiness); ;
        }

        private bool Approximately(Color colour1, Color colour2)
        {
            return Mathf.Approximately(colour1.r, colour2.r) &&
                Mathf.Approximately(colour1.g, colour2.g) &&
                Mathf.Approximately(colour1.b, colour2.b) &&
                Mathf.Approximately(colour1.a, colour2.a);
        }

        public override int GetHashCode()
        {
            HashCode hc = new HashCode();
            hc.Add(baseMaterial.name);
            hc.Add(mainColour);
            hc.Add(colour1);
            hc.Add(colour2);
            hc.Add(colour3);
            hc.Add(grubiness);
            return hc.ToHashCode();
        }

        public static bool operator ==(MaterialKey c1, MaterialKey c2)
        {
            return c1.Equals(c2);
        }

        public static bool operator !=(MaterialKey c1, MaterialKey c2)
        {
            return !c1.Equals(c2);
        }
    }

    //Special item placement
    public struct SpecialItemPlacement
    {
        public string reference;
        public InteractablePreset preset;
        public Human belongsTo;
        public object passedObject;
    }

    [Header("Debug")]
    public Vector2 debugTimeRange1;
    public Vector2 debugTimeRange2;

    //Singleton pattern
    private static Toolbox _instance;
    public static Toolbox Instance { get { return _instance; } }

    //Init
    private void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        //List of all evidence keys
        allShoeTypes = Human.ShoeType.GetValues(typeof(Human.ShoeType)).Cast<Human.ShoeType>().ToList();
        allDataKeys = Evidence.DataKey.GetValues(typeof(Evidence.DataKey)).Cast<Evidence.DataKey>().ToList();
        //allItemDesires = RetailItemPreset.Desire.GetValues(typeof(RetailItemPreset.Desire)).Cast<RetailItemPreset.Desire>().ToList();
        allCompanyCategories = CompanyPreset.CompanyCategory.GetValues(typeof(CompanyPreset.CompanyCategory)).Cast<CompanyPreset.CompanyCategory>().ToList();
        allEthnicities = Descriptors.EthnicGroup.GetValues(typeof(Descriptors.EthnicGroup)).Cast<Descriptors.EthnicGroup>().ToList();
        allConnectionTypes = Acquaintance.ConnectionType.GetValues(typeof(Acquaintance.ConnectionType)).Cast<Acquaintance.ConnectionType>().ToList();
        allTreeTriggers = DDSSaveClasses.TreeTriggers.GetValues(typeof(DDSSaveClasses.TreeTriggers)).Cast<DDSSaveClasses.TreeTriggers>().ToList();
        allOutfitCategories = ClothesPreset.OutfitCategory.GetValues(typeof(ClothesPreset.OutfitCategory)).Cast<ClothesPreset.OutfitCategory>().ToList();
        allCharacterAnchors = CitizenOutfitController.CharacterAnchor.GetValues(typeof(CitizenOutfitController.CharacterAnchor)).Cast<CitizenOutfitController.CharacterAnchor>().ToList();

        LoadAll();
    }

    private void Start()
    {
        if (SocialStatistics.Instance != null)
        {
            SocialStatistics.Instance.ethnicityFrequencies.Sort();
            SocialStatistics.Instance.ethnicityFrequencies.Reverse();

            foreach (SocialStatistics.EthnicityFrequency freq in SocialStatistics.Instance.ethnicityFrequencies)
            {
                totalEthnictiyFrequencyCount += freq.frequency;

                for (int u = 0; u < freq.frequency; u++)
                {
                    rEthnicity.Add(freq.ethnicity);
                }
            }
        }

        //Load all DDS into dictionaries
        if (SessionData.Instance != null && !SessionData.Instance.isDialogEdit)
        {
            Game.Log("Menu: Loading DDS...");

            //Get embedded files
            for (int u = 0; u < 3; u++)
            {
                string ddsPath = "/DDS/Blocks";
                string ddsFile = ".block";

                if (u == 1)
                {
                    ddsPath = "/DDS/Messages";
                    ddsFile = ".msg";
                }
                else if (u == 2)
                {
                    ddsPath = "/DDS/Trees";
                    ddsFile = ".tree";
                }

                var embed = new DirectoryInfo(Application.streamingAssetsPath + ddsPath);
                List<FileInfo> embeddedFiles = embed.GetFiles("*" + ddsFile, SearchOption.TopDirectoryOnly).ToList();

                //Retrieve data
                foreach (FileInfo i in embeddedFiles)
                {
                    //Parse to class file
                    using (StreamReader streamReader = File.OpenText(i.FullName))
                    {
                        string jsonString = streamReader.ReadToEnd();

                        if (u == 0)
                        {
                            DDSSaveClasses.DDSBlockSave loadedBlock = JsonUtility.FromJson<DDSSaveClasses.DDSBlockSave>(jsonString);

                            if(allDDSBlocks.ContainsKey(loadedBlock.id))
                            {
                                Game.LogError("Duplicate block exists: " + loadedBlock.id + " Existing name: " + allDDSBlocks[loadedBlock.id].name + ", new block name: " + loadedBlock.name);
                            }
                            else allDDSBlocks.Add(loadedBlock.id, loadedBlock);
                        }
                        else if (u == 1)
                        {
                            DDSSaveClasses.DDSMessageSave loadedMsg = JsonUtility.FromJson<DDSSaveClasses.DDSMessageSave>(jsonString);

                            if (allDDSMessages.ContainsKey(loadedMsg.id))
                            {
                                Game.LogError("Duplicate message exists: " + loadedMsg.id + " Existing name: " + allDDSMessages[loadedMsg.id].name + ", new message name: " + loadedMsg.name);
                            }
                            else allDDSMessages.Add(loadedMsg.id, loadedMsg);
                        }
                        else if (u == 2)
                        {
                            DDSSaveClasses.DDSTreeSave loadedTree = JsonUtility.FromJson<DDSSaveClasses.DDSTreeSave>(jsonString);

                            //Create message ref dictionary for quick access during gameplay
                            loadedTree.messageRef = new Dictionary<string, DDSSaveClasses.DDSMessageSettings>();

                            foreach (DDSSaveClasses.DDSMessageSettings msg in loadedTree.messages)
                            {
                                if(!loadedTree.messageRef.ContainsKey(msg.instanceID))
                                {
                                    loadedTree.messageRef.Add(msg.instanceID, msg);
                                }
                            }

                            if (allDDSTrees.ContainsKey(loadedTree.id))
                            {
                                Game.LogError("Duplicate tree exists: " + loadedTree.id + " Existing name: " + allDDSTrees[loadedTree.id].name + ", new tree name: " + loadedTree.name);
                            }
                            else allDDSTrees.Add(loadedTree.id, loadedTree);
                        }
                    }
                }
            }

#if UNITY_EDITOR
            if(Game.Instance.devMode)
            {
                foreach(KeyValuePair<string, InteractablePreset> pair in objectPresetDictionary)
                {
                    if(pair.Value.summaryMessageSource != null && pair.Value.summaryMessageSource.Length <= 0)
                    {
                        List<DDSSaveClasses.DDSMessageSave> msgs = allDDSMessages.Values.ToList();
                        DDSSaveClasses.DDSMessageSave msg = msgs.Find(item => item.name.ToLower() == pair.Value.name.ToLower());

                        if(msg != null)
                        {
                            pair.Value.summaryMessageSource = msg.id;
                            EditorUtility.SetDirty(pair.Value);
                            Game.Log("Found description message for object " + pair.Value.name + ": " + msg.id);
                        }
                    }
                }

                foreach (KeyValuePair<string, HelpContentPage> pair in allHelpContent)
                {
                    if (pair.Value.messageID != null && pair.Value.messageID.Length <= 0)
                    {
                        List<DDSSaveClasses.DDSMessageSave> msgs = allDDSMessages.Values.ToList();
                        DDSSaveClasses.DDSMessageSave msg = msgs.Find(item => item.name.ToLower() == pair.Value.name.ToLower());

                        if (msg != null)
                        {
                            pair.Value.messageID = msg.id;
                            EditorUtility.SetDirty(pair.Value);
                            Game.Log("Found help page message for object " + pair.Value.name + ": " + msg.id);
                        }
                    }
                }
            }
#endif
        }

        //Create AI sighting layer mask
        aiSightingLayerMask = CreateLayerMask(LayerMaskMode.castAllExcept, 2, 15, 18, 23, 25, 5, 31, 26);

        //Create interaction check layer mask
        interactionRayLayerMask = CreateLayerMask(LayerMaskMode.castAllExcept, 2, 12, 15, 18, 20, 5);
        interactionRayLayerMaskNoRoomMesh = CreateLayerMask(LayerMaskMode.castAllExcept, 2, 12, 15, 18, 20, 5, 29);

        //Create print detection check layer mask
        printDetectionRayLayerMask = CreateLayerMask(LayerMaskMode.castAllExcept, 2, 15, 18, 20, 31, 5);

        //Create scene capture culling mask
        sceneCaptureLayerMask = CreateLayerMask(LayerMaskMode.castAllExcept, 5, 7, 15, 17, 18, 20, 21, 22, 24, 30, 31);
        mugShotCaptureLayerMask = CreateLayerMask(LayerMaskMode.castAllExcept, 5, 7, 15, 17, 18, 20, 21, 22, 30, 31);

        //bullets etc...
        physicalObjectsLayerMask = CreateLayerMask(LayerMaskMode.castAllExcept, 2, 15, 18, 30, 31);

        //For blood
        spatterLayerMask = CreateLayerMask(LayerMaskMode.onlyCast, 0, 7, 28, 29);

        //Text to image
        textToImageMask = CreateLayerMask(LayerMaskMode.onlyCast, 15);

        //For checking whether to cull lights
        lightCullingMask = CreateLayerMask(LayerMaskMode.onlyCast, 0, 29, 20);

        //For held objects
        heldObjectsObjectsLayerMask = CreateLayerMask(LayerMaskMode.onlyCast, 0, 7, 8, 9, 10, 11, 12, 13, 14, 24, 27, 29);
    }

    private void ProcessLoadedScriptableObject(ScriptableObject so)
    {
        Type soType = so.GetType();
        string soName = so.name;

        //Create type entry
        if (!resourcesCache.ContainsKey(soType))
        {
            resourcesCache.Add(soType, new Dictionary<string, ScriptableObject>());
        }

        //Should this be accessed by name or ID system?
        ScriptableObjectIDSystem idSys = so as ScriptableObjectIDSystem;

        if (idSys != null)
        {
            //Save as lower so not case sensitive...
            resourcesCache[soType].Add(idSys.id, so);
        }
        else
        {
            //Save as lower so not case sensitive...
            resourcesCache[soType].Add(soName, so);
        }

        //Process presetNames; automatically set them in-editor only
#if UNITY_EDITOR
        if (Game.Instance.devMode)
        {
            SoCustomComparison customComp = so as SoCustomComparison;

            if(customComp != null)
            {
                if (customComp.presetName != customComp.name)
                {
                    customComp.presetName = customComp.name;
                    EditorUtility.SetDirty(customComp);
                }
            }
        }
#endif

        //Add to lists
        if (so is AddressPreset)
        {
            allAddressPresets.Add(so as AddressPreset);
        }
        else if (so is RoomConfiguration)
        {
            allRoomConfigs.Add(so as RoomConfiguration);
        }
        ////Quickly convert to new format
        //else if (so as FurnitureClass != null)
        //{
        //    FurnitureClass c = so as FurnitureClass;
        //    List<Vector2> dirs = CityData.Instance.offsetArrayX8.ToList();

        //    foreach (FurnitureClass.FurnitureWallRule r in c.wallRules)
        //    {
        //        int ind = dirs.FindIndex(item => item == r.wallOffset) + 1;

        //        r.wallDirection = (CityData.BlockingDirection)ind;
        //    }

        //    foreach (FurnitureClass.BlockedAccess a in c.blockedAccess)
        //    {
        //        a.blocked.Clear();

        //        foreach (Vector2 v2 in a.blockedAccess)
        //        {
        //            int ind = dirs.FindIndex(item => item == v2) + 1;
        //            a.blocked.Add((CityData.BlockingDirection)ind);
        //        }
        //    }

        //    EditorUtility.SetDirty(c);
        //}
        else if (so is InteractablePreset)
        {
            InteractablePreset p = so as InteractablePreset;
            objectPresetDictionary.Add(so.name, p); //For spawning items through strings

            if (p.weapon != null)
            {
                allWeapons.Add(p);
            }

            foreach (SubObjectClassPreset presetClass in p.subObjectClasses)
            {
                if (presetClass == null)
                {
                    Game.LogError("Null subobject class detected in " + p.name);
                }

                if (!subObjectsDictionary.ContainsKey(presetClass))
                {
                    subObjectsDictionary.Add(presetClass, new List<InteractablePreset>());
                }

                subObjectsDictionary[presetClass].Add(p);
            }

            if (p.alwaysPlaceAtGameLocation && p.autoPlacement != InteractablePreset.AutoPlacement.never)
            {
                placeAtGameLocationInteractables.Add(p);
            }

            if (p.placeIfFiltersPresentInOwner && p.autoPlacement != InteractablePreset.AutoPlacement.never)
            {
                placePerOwnerInteractables.Add(p);
            }
        }
        else if (so is DesignStylePreset)
        {
            DesignStylePreset d = so as DesignStylePreset;

            allDesignStyles.Add(d);
        }
        else if (so is FurnitureCluster)
        {
            FurnitureCluster cl = so as FurnitureCluster;

#if UNITY_EDITOR
            //if (cl.limitPerBuilding) Game.Log("Cluster limit per building: " + cl.presetName);
            //if (cl.limitPerCity) Game.Log("Cluster limit per city: " + cl.presetName);

            //cl.UpdatePreCalculatedLimits(); //Edit out eventually
#endif

            allFurnitureClusters.Add(cl);
        }
        else if (so is FurnitureClass)
        {
#if UNITY_EDITOR
            //(so as FurnitureClass).UpdatePreCalculatedLimits(); //Edit out eventually

            FurnitureClass cl = so as FurnitureClass;

            if (!cl.noAccessNeeded && (int)cl.objectSize.x == 1 && (int)cl.objectSize.y == 1)
            {
                if (cl.blockedAccess.Exists(item => item.nodeOffset == Vector2.zero && item.blocked.Count >= 8))
                {
                    Game.LogError("Furniture class " + cl.name + " features 8 blocked directions at 0,0 but no 'NoAccessNeeded' flag; this may cause pathing/access/generation problems!");
                }
            }

            //if (cl.limitPerBuilding) Game.Log("Class limit per building: " + cl.name);
            //if (cl.limitPerCity) Game.Log("Class limit per city: " + cl.name);
            //if (cl.limitPerBuildingResidence) Game.Log("Limit per building residence " + cl.name);
#endif
        }
        else if (so is WindowStylePreset)
        {
            WindowStylePreset ws = so as WindowStylePreset;
            string nameLower = ws.name.ToLower();
            InterfaceController.Instance.windowDictionary.Add(nameLower, ws);
        }
        else if (so is FurniturePreset)
        {
            FurniturePreset f = so as FurniturePreset;
            allFurniture.Add(f);

            //if(f.subObjects.Exists(item => item.preset.name == "SideJobHiddenObject"))
            //{
            //    Game.Log(f.name + " contains SideJobHiddenObject");
            //}

            if (f.prefab != null)
            {
                MeshFilter mf = f.prefab.GetComponent<MeshFilter>();

                if (mf != null && !furnitureMeshReference.ContainsKey(mf.sharedMesh))
                {
                    furnitureMeshReference.Add(mf.sharedMesh, f);
                }
            }
        }
        else if (so is HelpContentPage)
        {
            allHelpContent.Add((so as HelpContentPage).name, so as HelpContentPage);
        }
        else if (so is CompanyPreset)
        {
            allCompanyPresets.Add(so as CompanyPreset);
        }
        else if (so is ColourSchemePreset)
        {
            allColourSchemes.Add(so as ColourSchemePreset);
        }
        else if (so is AIGoalPreset)
        {
            allGoals.Add(so as AIGoalPreset);
        }
        else if (so is DialogPreset)
        {
            allDialog.Add(so as DialogPreset);
        }
        else if (so is EvidencePreset)
        {
            EvidencePreset item = so as EvidencePreset;
            evidencePresetDictionary.Add(item.name.ToLower(), item);
        }
        else if (so is JobPreset)
        {
            allSideJobs.Add(so as JobPreset);
        }
        else if (so is NewspaperArticle)
        {
            allArticles.Add(so as NewspaperArticle);
        }
        else if (so is DistrictPreset)
        {
            allDistricts.Add(so as DistrictPreset);
        }
        else if (so is HandwritingPreset)
        {
            allHandwriting.Add(so as HandwritingPreset);
        }
        else if (so is FactPreset)
        {
            FactPreset item = so as FactPreset;
            factPresetDictionary.Add(item.name.ToLower(), item);
        }
        else if (so is MurderPreset)
        {
            allMurderPresets.Add(so as MurderPreset);
        }
        else if (so is MurderMO)
        {
            allMurderMOs.Add(so as MurderMO);
        }
        else if (so is RetailItemPreset)
        {
            allItems.Add(so as RetailItemPreset);

            //#if UNITY_EDITOR

            //RetailItemPreset retail = so as RetailItemPreset;

            //if(retail.itemPreset != null)
            //{
            //    retail.itemPreset.retailItem = retail;
            //    EditorUtility.SetDirty(retail.itemPreset);
            //}

            //#endif
        }
        else if (so is CharacterTrait)
        {
            allCharacterTraits.Add(so as CharacterTrait);

#if UNITY_EDITOR
            if (Game.Instance.devMode)
            {
                CharacterTrait t = so as CharacterTrait;

                if (t != null)
                {
                    if (t.featureInAfflictionPool || t.featureInInterestPool)
                    {
                        Strings.Get("descriptors", t.name); //Make sure this is in text files
                    }
                }
            }
#endif
        }
        else if (so is BookPreset)
        {
            BookPreset b = so as BookPreset;
#if UNITY_EDITOR
            if (b.bookName != b.name)
            {
                b.bookName = b.name;
                EditorUtility.SetDirty(b);
            }
#endif
            allBooks.Add(so as BookPreset);
        }
        else if (so is ArtPreset)
        {
            ArtPreset a = so as ArtPreset;

            allArt.Add(a);
        }
        else if (so is ClothesPreset)
        {
            ClothesPreset clothing = so as ClothesPreset;

            allClothes.Add(clothing);
            clothesDictionary.Add(clothing.name, clothing);
        }
        else if (so is OccupationPreset)
        {
            OccupationPreset job = so as OccupationPreset;

            allJobs.Add(job);

            if (job.isCriminal)
            {
                allCriminalJobs.Add(job);
            }
        }
        else if (so is RoomLightingPreset)
        {
            allRoomLighting.Add(so as RoomLightingPreset);
        }
        else if (so is StreetTilePreset)
        {
            allStreetTiles.Add(so as StreetTilePreset);
        }
        else if (so is MaterialGroupPreset)
        {
            MaterialGroupPreset mg = so as MaterialGroupPreset;

            if (!materialProperties.ContainsKey(mg.material))
            {
                materialProperties.Add(mg.material, mg);
            }

            allMaterialGroups.Add(mg);
        }
        else if (so is CloudSaveData)
        {
            devCloudSaveConfig = so as CloudSaveData;
        }
        else if (so is StatusPreset)
        {
            allStatuses.Add(so as StatusPreset);
        }
        else if (so is WallFrontagePreset)
        {
            WallFrontagePreset wf = so as WallFrontagePreset;
            allWallFrontage.Add(wf);
        }
        else if (so is SyncDiskPreset)
        {
            allSyncDisks.Add(so as SyncDiskPreset);
        }
        else if (so is DDSScope)
        {
            DDSScope grp = so as DDSScope;
            scopeDictionary.Add(grp.name, grp);

            if (grp.isGlobal)
            {
                globalScopeDictionary.Add(grp.name, grp);
            }
        }
        else if (so is GroupPreset)
        {
            GroupPreset grp = so as GroupPreset;
            allGroups.Add(grp);
            groupsDictionary.Add(grp.name, grp);
        }
    }

    private void LoadAll()
    {
        ////Load dictionary cache

        List<ScriptableObject> allPresets = AssetLoader.Instance.GetAllPresets();
        foreach (ScriptableObject so in allPresets)
        {
            ProcessLoadedScriptableObject(so);
        }

        //Load ambient zones
        allAmbientZones = AssetLoader.Instance.GetAllAmbientZones();

        foreach (CharacterTrait trait in allCharacterTraits)
        {
            if (!trait.isTrait)
            {
                reasons.Add(trait);
            }
            else
            {
                if (trait.pickStage == 0)
                {
                    stage0Traits.Add(trait);
                }
                else if (trait.pickStage == 1)
                {
                    stage1Traits.Add(trait);
                }
                else if (trait.pickStage == 2)
                {
                    stage2Traits.Add(trait);
                }
                else
                {
                    stage3Traits.Add(trait);
                }
            }
        }

        foreach (DialogPreset dia in allDialog)
        {
            if (dia.defaultOption)
            {
                defaultDialogOptions.Add(dia);
            }
        }

        //Load & classify furniture into design styles
        //List<FurniturePreset> furn = AssetLoader.Instance.GetAllFurniture();

        foreach (FurniturePreset f in allFurniture)
        {
            if (f.prefab != null)
            {
                MeshFilter mf = f.prefab.GetComponent<MeshFilter>();

                if (mf != null && !furnitureMeshReference.ContainsKey(mf.sharedMesh))
                {
                    furnitureMeshReference.Add(mf.sharedMesh, f);
                }
            }

            if (f.universalDesignStyle)
            {
                foreach (DesignStylePreset d in allDesignStyles)
                {
                    if (d == null)
                    {
                        Game.Log("Furniture design style blank for " + f.name);
                        continue;
                    }

                    if (!furnitureDesignStyleRef.ContainsKey(d)) furnitureDesignStyleRef.Add(d, new List<FurniturePreset>());

                    furnitureDesignStyleRef[d].Add(f);
                    //Game.Log("Adding " + f.name + " to set " + d.name + " (" + furnitureDesignStyleRef[d].Count + ")");
                }
            }
            else
            {
                foreach (DesignStylePreset d in f.designStyles)
                {
                    if (d == null)
                    {
                        Game.Log("Furniture design style blank for " + f.name);
                        continue;
                    }

                    if (!furnitureDesignStyleRef.ContainsKey(d)) furnitureDesignStyleRef.Add(d, new List<FurniturePreset>());

                    furnitureDesignStyleRef[d].Add(f);
                    //Game.Log("Adding " + f.name + " to set " + d.name + " (" + furnitureDesignStyleRef[d].Count + ")");
                }
            }

            foreach (RoomTypeFilter rt in f.allowedRoomFilters)
            {
                foreach (RoomClassPreset rtp in rt.roomClasses)
                {
                    if (!furnitureRoomTypeRef.ContainsKey(rtp))
                    {
                        furnitureRoomTypeRef.Add(rtp, new HashSet<FurniturePreset>());
                    }

                    if (!furnitureRoomTypeRef[rtp].Contains(f))
                    {
                        furnitureRoomTypeRef[rtp].Add(f);
                    }
                }
            }
        }

        foreach (MaterialGroupPreset mg in allMaterialGroups)
        {
            foreach (MaterialGroupPreset.MaterialSettings d in mg.designStyles)
            {
                if (!materialDesignStyleRef.ContainsKey(d.designStyle)) materialDesignStyleRef.Add(d.designStyle, new Dictionary<MaterialGroupPreset.MaterialType, List<MaterialGroupPreset>>());
                if (!materialDesignStyleRef[d.designStyle].ContainsKey(mg.materialType)) materialDesignStyleRef[d.designStyle].Add(mg.materialType, new List<MaterialGroupPreset>());

                materialDesignStyleRef[d.designStyle][mg.materialType].Add(mg);
            }
        }

        foreach (WallFrontagePreset f in allWallFrontage)
        {
            if (f.universalDesignStyle)
            {
                foreach (DesignStylePreset d in allDesignStyles)
                {
                    if (!wallFrontageStyleRef.ContainsKey(d)) wallFrontageStyleRef.Add(d, new Dictionary<WallFrontageClass, List<WallFrontagePreset>>());

                    foreach (WallFrontageClass c in f.classes)
                    {
                        if (!wallFrontageStyleRef[d].ContainsKey(c)) wallFrontageStyleRef[d].Add(c, new List<WallFrontagePreset>());
                        wallFrontageStyleRef[d][c].Add(f);
                    }
                }
            }
            else
            {
                foreach (DesignStylePreset d in f.designStyles)
                {
                    if (!wallFrontageStyleRef.ContainsKey(d)) wallFrontageStyleRef.Add(d, new Dictionary<WallFrontageClass, List<WallFrontagePreset>>());

                    foreach (WallFrontageClass c in f.classes)
                    {
                        if (!wallFrontageStyleRef[d].ContainsKey(c)) wallFrontageStyleRef[d].Add(c, new List<WallFrontagePreset>());
                        wallFrontageStyleRef[d][c].Add(f);
                    }
                }
            }
        }

#if UNITY_EDITOR
        PoolingGroup = new GameObject("Pooling Group"); // Keeps things neat in the scene heirarchy
#endif
        FootprintController.InitialisePool();
        SpatterSimulation.DecalSpawnData.InitialisePool();
    }

    //Round a float to x decimal places
    public float RoundToPlaces(float input, int decimals)
    {
        int mp = 1;

        for (int i = 0; i < decimals; i++)
        {
            mp *= 10;
        }

        return (float)(Mathf.RoundToInt(input * mp) / (float)mp);
    }

    //Return a string with added zeros if needed (can be used for currency, time)
    public string AddZeros(float num, int decimals)
    {
        string ret = num.ToString();

        string[] r = ret.Split('.');

        if (r.Length > 1)
        {
            int addZeros = decimals - r[1].Length;

            for (int i = 0; i < addZeros; i++)
            {
                ret += "0";
            }
        }
        else
        {
            ret += ".";

            for (int i = 0; i < decimals; i++)
            {
                ret += "0";
            }
        }

        return ret;
    }

    //Round a double to x decimal places, return float
    public float RoundToPlaces(double input, int decimals)
    {
        float floatInput = (float)input;
        int mp = 100;

        if (decimals == 1) mp = 10;
        else if (decimals == 2) mp = 100;
        else if (decimals == 3) mp = 1000;
        else if (decimals == 4) mp = 10000;

        return (float)(Mathf.RoundToInt(floatInput * mp) / (float)mp);
    }

    //Return a theoretical time for movement between one location and another. Accounts for floors & rooms also.
    public float TravelTimeEstimate(Human cc, NewNode origin, NewNode destination)
    {
        if (origin == null)
        {
            Game.LogError("Travel time estimate: Origin  is null!");
            return 0f;
        }
        else if (destination == null)
        {
            Game.LogError("Travel time estimate: Destination is null!");
            return 0f;
        }
        else if (origin == destination)
        {
            return 0f;
        }

        //Apply citizen speed difference
        float speed = CitizenControls.Instance.baseCitizenWalkSpeed;

        if (cc != null)
        {
            speed = cc.speedMultiplier * CitizenControls.Instance.baseCitizenWalkSpeed;
        }

        Vector2Int t1 = new Vector2Int(origin.tile.globalTileCoord.x, origin.tile.globalTileCoord.y);
        Vector2Int t2 = new Vector2Int(destination.tile.globalTileCoord.x, destination.tile.globalTileCoord.y);

        float tileDistance = (Vector2.Distance(t1, t2) * CityControls.Instance.travelTimeCrowFliesMultiplierEstimate);

        //same building
        if (origin.building != null && origin.building == destination.building)
        {
            //Add extra tiles for navigating the building interior
            tileDistance += Mathf.Abs(origin.floor.floor - destination.floor.floor);

            //Add extra tiles for navigating the building interior
            if (origin.floor != destination.floor)
            {
                tileDistance += 2;
            }
        }
        //Different building
        else
        {
            //Add extra tiles for navigating the building interior
            if (origin.building != null) tileDistance += 2;
            if (destination.building != null) tileDistance += 2;

            //Add climb distance
            tileDistance += origin.tile.globalTileCoord.z + destination.tile.globalTileCoord.z;
        }

        //Distance to cover in unity units
        float unityDist = tileDistance * PathFinder.Instance.tileSize.x;

        //Should roughly equate to distance / speed (unity units in 1 realtime second) = time in realtime seconds
        //Convert this amount of time to game time.
        //Add a minimum time of around 1 mins to account for initial logic behaviour delays
        float ret = (((unityDist / speed) * 0.01f) * CityControls.Instance.travelTimeMultiplier) + 0.0167f;

        //Minimum travel time is 0
        ret = Mathf.Max(ret, 0f);

        return ret;
    }

    //Add to the time travel records
    public void AddToTravelTimeRecords(Actor cc, float discrepency)
    {
        //Add to data collection
        //Send onTime statistics to the debug controller so I can keep an eye on average timing
        //Make sure it isn't a status switch and that there is a genuine time estimate
        if (Game.Instance.collectRoutineTimingInfo)
        {
            //+ is early, - is late
            Game.Instance.AddOnTimeEntry(cc, discrepency);
        }
    }

    //Return travel time in minutes
    public int TravelTimeEstimateMinutes(Citizen cc, NewNode origin, NewNode destination)
    {
        float travelTimeDecimal = TravelTimeEstimate(cc, origin, destination);

        return Mathf.RoundToInt(travelTimeDecimal * 60f);
    }

    //Generate a number using a weighted random range
    public float RandomRangeWeighted(float minimum, float maximum, float weightedValue, int stepResolution = 5)
    {
        int rangePicked = Rand(0, stepResolution + 1);

        //Narrow ranges depending on rng step
        return Rand(
            Mathf.Lerp(minimum, weightedValue, rangePicked / (float)stepResolution),
            Mathf.Lerp(maximum, weightedValue, rangePicked / (float)stepResolution)
            );
    }

    //Generate a number using a weighted random range
    public float RandomRangeWeightedSeedContained(float minimum, float maximum, float weightedValue, string input, out string output, int stepResolution = 5)
    {
        int rangePicked = Toolbox.Instance.GetPsuedoRandomNumberContained(0, stepResolution + 1, input, out output);

        //Narrow ranges depending on rng step
        return Toolbox.Instance.GetPsuedoRandomNumberContained(
            Mathf.Lerp(minimum, weightedValue, rangePicked / (float)stepResolution),
            Mathf.Lerp(maximum, weightedValue, rangePicked / (float)stepResolution), output, out output
            );
    }

    //Get distance from a path from x to y
    public float MinDistanceFromPath(NewNode pathOrigin, NewNode pathDestination, Vector3 inputPosition)
    {
        if (pathOrigin == null || pathDestination == null) return 9999f;

        //Create path
        PathFinder.PathData path = PathFinder.Instance.GetPath(pathOrigin, pathDestination, null);

        //Start with distance from origin
        float minimumDistance = Vector2.Distance(pathOrigin.position, inputPosition);

        //Choose the minimum distance from a point
        if (path.accessList != null)
        {
            for (int i = 0; i < Mathf.Max(path.accessList.Count - 1, 0); i++)
            {
                Vector3 v2 = path.accessList[i].fromNode.position;
                Vector3 next = path.accessList[i + 1].fromNode.position;
                Vector3 projectedNearest = v2;

                //Detect the vector direction between this point and the next
                float diffInX = Mathf.Abs(next.x - v2.x);
                //float diffInY = Mathf.Abs(next.y - v2.y);
                float diffInZ = Mathf.Abs(next.z - v2.z);

                //If the biggest difference in point position is x, then the route runs along this direction
                if (diffInX >= diffInZ)
                {
                    int nearestX = Mathf.RoundToInt(Mathf.Clamp(inputPosition.x, Mathf.Min(v2.x, next.x), Mathf.Max(v2.x, next.x)));

                    //Create a projected point along this line's axis, using the closest coord to the input
                    projectedNearest = new Vector3(nearestX, 0, (v2.z + next.z) / 2f);
                }
                else
                {
                    int nearestZ = Mathf.RoundToInt(Mathf.Clamp(inputPosition.z, Mathf.Min(v2.z, next.z), Mathf.Max(v2.z, next.z)));
                    projectedNearest = new Vector3((v2.x + next.x) / 2f, 0, nearestZ);
                }

                //Distance
                float dist = Vector3.Distance(projectedNearest, inputPosition);

                if (dist < minimumDistance)
                {
                    minimumDistance = dist;
                }
            }
        }

        return minimumDistance;
    }

    //Convert a rect transform's position to screen space
    public Rect RectTransformToScreenSpace(RectTransform transform)
    {
        Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
        Rect rect = new Rect(transform.position.x, Screen.height - transform.position.y, size.x, size.y);
        rect.x -= (transform.pivot.x * size.x);
        rect.y -= ((1.0f - transform.pivot.y) * size.y);
        rect.y = Screen.height - rect.y;

        return rect;
    }

    //Wait until the end of frame to invoke this function
    public void InvokeEndOfFrame(Action action, string newDebug)
    {
        if (action == null) return;

        if (invokeEndOfFrame.Contains(action)) return;
        else
        {
            invokeEndOfFrame.Add(action);
            debugInvokeEndOfFrame.Add(newDebug);
        }

        if (!endOfFrameInvoke)
        {
            endOfFrameInvoke = true;
            StartCoroutine(ExeEndOfFrame());
        }
    }

    IEnumerator ExeEndOfFrame()
    {
        bool wait = true;

        while (wait)
        {
            wait = false;

            yield return new WaitForEndOfFrame();
        }

        while (invokeEndOfFrame.Count > 0)
        {
            //Game.Log("Update: End of frame invoke: " + debugInvokeEndOfFrame[0]);
            Action act = invokeEndOfFrame.First();
            act();
            invokeEndOfFrame.Remove(act);
            debugInvokeEndOfFrame.RemoveAt(0);
        }

        invokeEndOfFrame.Clear();
        debugInvokeEndOfFrame.Clear();
        endOfFrameInvoke = false;
    }

    //Use a list of spawned button objects to update their positions
    public void UpdateButtonListPositions(List<ButtonController> buttons, float edgeMargin = 5f, float iconMargin = 4f)
    {
        //If no icons, skip this
        if (buttons.Count <= 0) return;

        //Get window dimensions
        RectTransform background = buttons[0].transform.parent.GetComponent<RectTransform>();

        float xPos = edgeMargin;
        float yPos = -edgeMargin;

        //ContentSize
        Vector2 contentSize = new Vector2(background.rect.width, 1f);

        //Position all buttons
        for (int i = 0; i < buttons.Count; ++i)
        {
            //Get button's rect transform
            RectTransform b = buttons[i].gameObject.GetComponent<RectTransform>();

            b.anchoredPosition = new Vector2(xPos, yPos);

            xPos += b.sizeDelta.x + iconMargin;

            if (xPos > (contentSize.x - b.sizeDelta.x - edgeMargin))
            {
                yPos -= b.sizeDelta.y + iconMargin;
                xPos = iconMargin;
            }

            contentSize.y = Mathf.Abs(yPos - b.sizeDelta.y - edgeMargin);
        }

        //Set the content size to the area of the buttons
        background.sizeDelta = new Vector2(background.sizeDelta.x, contentSize.y);
    }

    //Detect if two ranges are overlapping: Should work with gametime
    public bool GameTimeRangeOverlap(Vector2 range1, Vector2 range2, bool equalsIsOverlapping = true)
    {
        bool overlap = false;

        if (equalsIsOverlapping)
        {
            overlap = range1.x <= range2.y && range2.x <= range1.y;
        }
        else
        {
            overlap = range1.x < range2.y && range2.x < range1.y;
        }

        return overlap;
    }

    //Detect if two ranges are overlapping: Should work with decimal hours
    public bool DecimalTimeRangeOverlap(Vector2 range1, Vector2 range2, bool equalsIsOverlapping = true)
    {
        bool overlap = false;

        //Check for inverse decimal ranges...
        //The end range is lower than the start range. Add 24 onto end time.
        if (range1.y < range1.x)
        {
            range1.y += 24;

            range2.x += 24;
            range2.y += 24;
        }

        if (range2.y < range2.x)
        {
            range2.y += 24;

            range1.x += 24;
            range1.y += 24;
        }

        if (equalsIsOverlapping)
        {
            overlap = range1.x <= range2.y && range2.x <= range1.y;
        }
        else
        {
            overlap = range1.x < range2.y && range2.x < range1.y;
        }

        return overlap;
    }

    //Rotate a vector2
    //Positive values will rotate anti-clockwise assuming 0,0 is bottom left
    public Vector2 RotateVector2ACW(Vector2 v, float degrees)
    {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);

        return v;
    }

    public Vector2 RotateVector2CW(Vector2 v, float degrees)
    {
        float sin = Mathf.Sin(-degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(-degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);

        return v;
    }

    //Return a random ethnicity based on city statistics
    public Descriptors.EthnicGroup RandomEthnicGroup(string seed)
    {
        return rEthnicity[GetPsuedoRandomNumber(0, rEthnicity.Count, seed)];
    }

    // Get the contents of a RenderTexture into a Texture2D
    public Color GetRenderTexturePixel(RenderTexture rt)
    {
        // Remember currently active render texture
        RenderTexture currentActiveRT = RenderTexture.active;

        // Set the supplied RenderTexture as the active one
        RenderTexture.active = rt;

        // Create a new Texture2D and read the RenderTexture image into it
        Texture2D tex = new Texture2D(rt.width, rt.height);
        tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);

        // Restorie previously active render texture
        RenderTexture.active = currentActiveRT;
        return tex.GetPixel(0, 0);
    }

    //Set an object to a building's culling layer, or street layer if building is null
    public void SetLightLayer(GameObject objectWithMesh, NewBuilding building, bool includeStreetLighting = false)
    {
        //HDRP: Set mesh to light layer
        MeshRenderer[] meshes = objectWithMesh.GetComponentsInChildren<MeshRenderer>();

        foreach (MeshRenderer mesh in meshes)
        {
            SetLightLayer(mesh, building, includeStreetLighting);
        }
    }

    public void SetLightLayer(MeshRenderer meshRend, NewBuilding building, bool includeStreetLighting = false)
    {
        //Set layer to interior + sun (default)
        if (meshRend == null) return;

        int street = 0;

        if (includeStreetLighting)
        {
            street = 2;
        }

        if (building != null)
        {
            if (building.interiorLightCullingLayer == 0)
            {
                //References to values in LightLayerEnum;
                // +1 to add the default light layer (sun)
                // + 2 to include streetLighting

                meshRend.renderingLayerMask = 4 + 1 + (uint)street;
            }
            else if (building.interiorLightCullingLayer == 1)
            {
                meshRend.renderingLayerMask = 8 + 1 + (uint)street;
            }
            else if (building.interiorLightCullingLayer == 2)
            {
                meshRend.renderingLayerMask = 16 + 1 + (uint)street;
            }
            else if (building.interiorLightCullingLayer == 3)
            {
                meshRend.renderingLayerMask = 32 + 1 + (uint)street;
            }
        }
        //HDRP: Else set light layer to street lighting + sun (default). Overrides the streetlighting bool
        else
        {
            street = 2;
            meshRend.renderingLayerMask = (uint)street + 1;
        }
    }

    //Load data file from resources/data folder (case sensitvie)
    public bool LoadDataFromResources<T>(string name, out T output) where T : ScriptableObject
    {
        output = null;

        //Get the output type
        Type typeParameterType = typeof(T);

        Dictionary<string, ScriptableObject> found;

        if (resourcesCache.TryGetValue(typeParameterType, out found))
        {
            ScriptableObject foundObject;

            if (found.TryGetValue(name, out foundObject))
            {
                output = foundObject as T;
                return true;
            }
            else
            {
                Game.LogError("Resources load error: Could not find any files of type " + typeParameterType + " with name or ID " + name.ToLower() + ", scaning for name... (this could take a while)");

                foreach(KeyValuePair<string,  ScriptableObject> pair in found)
                {
                    if(pair.Value.name == name)
                    {
                        Game.Log("Resources load successful using name system. This should be re-saved for more efficiency.");
                        output = pair.Value as T;
                        return true;
                    }
                }
            }
        }
        else
        {
            Game.LogError("Resources load error: Could not find any files of type " + typeParameterType);
        }

        return false;
    }

    //Return inputs as list
    public List<T> GetList<T>(params T[] elements)
    {
        return new List<T>(elements);
    }

    public float HeuristicCostEstimate(NewNode start, NewNode goal)
    {
        return Vector2Int.Distance(start.floorCoord, goal.floorCoord);
    }

    //Construct path and count distance
    public List<NewNode> ConstructPathAccurate(Dictionary<NewNode, NewNode> cameFrom, NewNode current)
    {
        List<NewNode> totalPath = new List<NewNode>();
        NewNode previous = null;

        totalPath.Add(current);

        while (cameFrom.ContainsKey(current))
        {
            previous = current;
            current = cameFrom[current];

            //negative vector2 is used as a null, so don't add this.
            if (current != null)
            {
                //The next node to add must be different
                if (current != previous)
                {
                    totalPath.Add(current);
                }
                else continue;
            }
            else
            {
                Game.Log("Pathfinder: Current is null, invalid path");
                break;
            }
        }

        totalPath.Reverse();
        //totalPath.RemoveAt(0); //We don't need the first node

        return totalPath;
    }

    public Evidence GetOrCreateEvidenceForInteractable(InteractablePreset preset, string newID, Interactable interactable, Human belongsTo, Human writer, Human reciever, SideJob jobParent, NewGameLocation gameLocation, RetailItemPreset retailItem, List<Interactable.Passed> passedVars)
    {
        Evidence newEvidence = null;

        //Evidence already exists...
        if (GameplayController.Instance.evidenceDictionary.TryGetValue(newID, out newEvidence))
        {
            return newEvidence;
        }

        //Grab gamelocation from interactable
        if(gameLocation == null && interactable != null)
        {
            if (interactable.node != null) gameLocation = interactable.node.gameLocation;
        }

        //Attempt to find evidence...
        if (preset.useEvidence)
        {
            if (preset.useSingleton != null)
            {
                if (!SessionData.Instance.isFloorEdit)
                {
                    newEvidence = GameplayController.Instance.singletonEvidence.Find(item => item.preset == preset.useSingleton);
                }
            }
            else if (preset.findEvidence != InteractablePreset.FindEvidence.none)
            {
                if (preset.findEvidence == InteractablePreset.FindEvidence.addressBook)
                {
                    if (belongsTo == null) Game.LogError("Address book created without 'Belongs To' reference");
                    newEvidence = belongsTo.addressBook;
                }
                else if (preset.findEvidence == InteractablePreset.FindEvidence.telephone)
                {
                    newEvidence = CityData.Instance.telephone;
                }
                else if (preset.findEvidence == InteractablePreset.FindEvidence.hospitalBed)
                {
                    newEvidence = CityData.Instance.hospitalBed;
                }
                else if (preset.findEvidence == InteractablePreset.FindEvidence.companyRoster)
                {
                    newEvidence = gameLocation.thisAsAddress.company.employeeRoster;
                }
                else if (preset.findEvidence == InteractablePreset.FindEvidence.residentRoster)
                {
                    newEvidence = gameLocation.building.residentRoster;
                }
                else if (preset.findEvidence == InteractablePreset.FindEvidence.salesRecords)
                {
                    newEvidence = gameLocation.thisAsAddress.company.salesRecords;
                }
                else if (preset.findEvidence == InteractablePreset.FindEvidence.menu)
                {
                    newEvidence = gameLocation.thisAsAddress.company.menu;
                }
                else if (preset.findEvidence == InteractablePreset.FindEvidence.workID)
                {
                    newEvidence = belongsTo.workID;
                }
                else if (preset.findEvidence == InteractablePreset.FindEvidence.IDCard)
                {
                    //newEvidence = belongsTo.idCard;
                }
                else if (preset.findEvidence == InteractablePreset.FindEvidence.calendar)
                {
                    if(gameLocation == null) Game.LogError("Unable to get calendar as gamelocation is null!");
                    else
                    {
                       //Generate a calendar if there isn't one at the address
                        if (gameLocation != null && gameLocation.thisAsAddress != null)
                        {
                            if (gameLocation.thisAsAddress.calendar != null) newEvidence = gameLocation.thisAsAddress.calendar;
                            else
                            {
                                newEvidence = gameLocation.thisAsAddress.CreateCalendar();
                            }
                        }

                        if (newEvidence == null & gameLocation != null) Game.LogError("Unable to get calendar for " + gameLocation.name);
                    }
                }
            }
            else
            {
                //Pass passed ints into evidence creation
                List<object> passed = new List<object>();

                if (passedVars != null)
                {
                    foreach (Interactable.Passed pint in passedVars)
                    {
                        passed.Add(pint);
                    }
                    //Game.Log("Creating evidence for " + preset.name + " with " + passed.Count + " passed objects...");
                }
                //else Game.Log("Creating evidence for " + preset.name + " with no passed objects...");

                //Always pass this as interactable
                if (interactable != null) passed.Add(interactable);

                if (preset != null) passed.Add(preset);

                //Always pass retail item reference
                if (retailItem != null)
                {
                    passed.Add(retailItem);
                }

                //Set parent (this is required to auto add a 'found at' fact).
                Evidence parentEv = null;
                if (preset.locationIsParent && gameLocation != null) parentEv = gameLocation.evidenceEntry;

                newEvidence = EvidenceCreator.Instance.CreateEvidence(preset.spawnEvidence, newID, newController: belongsTo, newOwner: belongsTo, newWriter: writer, newReciever: reciever, passedObjects: passed, newParent: parentEv);
            }
        }

        return newEvidence;
    }

    //Get existing evidence...
    public bool TryGetEvidence(string evID, out Evidence evidence)
    {
        evidence = null;

        if (GameplayController.Instance.evidenceDictionary.TryGetValue(evID, out evidence))
        {
            return true;
        }
        else
        {
            if(evID.Length > 4)
            {
                string first4 = evID.Substring(0, 4);

                string sticky = string.Empty;
                if (evID.Length > 10) sticky = evID.Substring(0, 10);

                //Is this time or date evidence?
                if (first4 == "Date")
                {
                    string date = evID.Substring(4, evID.Length - 4);
                    evidence = EvidenceCreator.Instance.GetDateEvidence(date);
                    Game.Log("Created date evidence for missing evidence save data " + evID + " (" + date + ")");
                }
                //Is this a sticky note?
                else if(sticky == "StickyNote")
                {
                    int parseStickyNoteID = InterfaceController.assignStickyNoteID;
                    int.TryParse(evID.Substring(10, evID.Length - 10), out parseStickyNoteID);

                    evidence = EvidenceCreator.Instance.CreateEvidence("PlayerStickyNote", "StickyNote" + parseStickyNoteID, forceDiscoveryOnCreate: true) as EvidenceStickyNote;
                }
                else
                {
                    evidence = EvidenceCreator.Instance.GetTimeEvidence(evID);

                    if (evidence == null)
                    {
                        //Do some investigation...
                        string evClass = string.Empty;
                        if (evID.Length > 1) evClass = evID.Substring(0, 1);

                        if(evClass == "I")
                        {
                            string iParse = evID.Substring(1, evID.Length - 1);
                            int iID = -1;
                            int.TryParse(iParse, out iID);

                            if(iID > -1)
                            {
                                Game.Log("Parsed interactable ID " + iID + " from " + evID + ", searching...");

                                Interactable inter = CityData.Instance.interactableDirectory.Find(item => item.id == iID);

                                if(inter != null)
                                {
                                    Game.Log("Found interactable! Getting evidence...");

                                    //Make sure we have both the retailitem and gamelocation before proceeding with this (was causing load errors)...
                                    RetailItemPreset retailItem = null;
                                    if (inter.preset != null) retailItem = inter.preset.retailItem;

                                    NewGameLocation gameLoc = null;
                                    if (inter.node != null) gameLoc = inter.node.gameLocation;

                                    evidence = Toolbox.Instance.GetOrCreateEvidenceForInteractable(inter.preset, evID, inter, inter.belongsTo, inter.writer, inter.reciever, inter.jobParent, gameLoc, retailItem, inter.pv);
                                    return true;
                                }
                                else
                                {
                                    Game.Log("Seaching metaobjects...");

                                    MetaObject mObj = CityData.Instance.FindMetaObject(iID);

                                    if(mObj != null)
                                    {
                                        Game.Log("Found meta object! Getting evidence...");
                                        evidence = mObj.GetEvidence();
                                        return true;
                                    }
                                    else
                                    {
                                        Game.LogError("Unable to find interactable " + iID + " loaded interactables: " + CityData.Instance.interactableDirectory.Count);
                                    }
                                }
                            }
                            else
                            {
                                Game.LogError("Unable to parse valid interactable ID from " + evID);
                            }
                        }

                        Game.LogError("Unable to find evidence for ID " + evID);
                    }
                }
            }
        }

        if (evidence != null) return true;
        else return false;
    }

    //Spawn a spare key hidden near the entrance of the address
    public Interactable SpawnSpareKey(NewAddress ad, string loadGUID = null)
    {
        if (ad.hiddenSpareKey) return null;
        if (ad.inhabitants.Count <= 0) return null; //Can't spawn a key if nobody lives here...

        List<NewRoom> adjoiningLobbies = new List<NewRoom>();
        List<NewRoom> correspondingAddressRoom = new List<NewRoom>();
        List<NewNode> nodeEntrance = new List<NewNode>();

        List<FurnitureLocation> correspondingParents = new List<FurnitureLocation>();
        List<FurniturePreset.SubObject> validLocs = new List<FurniturePreset.SubObject>();
        List<NewRoom> validAdRoom = new List<NewRoom>();

        //Find adjoining lobbies
        foreach (NewNode.NodeAccess ent in ad.entrances)
        {
            if (ent.wall == null) continue;

            if (ent.wall.node.gameLocation != ad && ent.wall.node.gameLocation.isLobby)
            {
                if (!adjoiningLobbies.Contains(ent.wall.node.room))
                {
                    adjoiningLobbies.Add(ent.wall.node.room);
                    correspondingAddressRoom.Add(ent.wall.otherWall.node.room);
                    nodeEntrance.Add(ent.wall.node);
                }
            }
            else if (ent.wall.otherWall.node.gameLocation != ad && ent.wall.otherWall.node.gameLocation.isLobby)
            {
                if (!adjoiningLobbies.Contains(ent.wall.otherWall.node.room))
                {
                    adjoiningLobbies.Add(ent.wall.otherWall.node.room);
                    correspondingAddressRoom.Add(ent.wall.node.room);
                    nodeEntrance.Add(ent.wall.otherWall.node);
                }
            }
        }

        //Find objects with hiding places that are within 2 nodes
        for (int i = 0; i < adjoiningLobbies.Count; i++)
        {
            NewRoom room = adjoiningLobbies[i];
            NewRoom adRoom = correspondingAddressRoom[i];
            NewNode entrance = nodeEntrance[i];

            for (int f = 0; f < room.individualFurniture.Count; f++)
            {
                FurnitureLocation loc = null;

                try
                {
                    loc = room.individualFurniture[f];
                }
                catch
                {
                    continue;
                }

                if (loc == null) continue;

                //Must feature key hide preset
                if (loc.furniture.subObjects.Exists(item => item.preset == InteriorControls.Instance.keyHidingPlace))
                {
                    //Must be within 3 nodes
                    if (Vector3.Distance(loc.anchorNode.nodeCoord, entrance.nodeCoord) <= 2.1f)
                    {
                        List<FurniturePreset.SubObject> valid = new List<FurniturePreset.SubObject>();
                        valid.AddRange(loc.furniture.subObjects.FindAll(item => item.preset == InteriorControls.Instance.keyHidingPlace));

                        //Ignore existing objects...
                        for (int u = 0; u < valid.Count; u++)
                        {
                            FurniturePreset.SubObject ownCheck = valid[u];

                            //Does this have an item already?
                            Interactable exising = loc.integratedInteractables.Find(item => item.subObject == ownCheck);

                            //On the first attempt: Ignore all existing objects
                            if (exising != null)
                            {
                                continue;
                            }

                            validLocs.Add(ownCheck);
                            correspondingParents.Add(loc);
                            validAdRoom.Add(adRoom);
                        }
                    }
                }
            }
        }

        //Pick a random location
        if (validLocs.Count > 0)
        {
            int pickedIndex = Toolbox.Instance.GetPsuedoRandomNumber(0, validLocs.Count, ad.seed);
            FurniturePreset.SubObject pickedPos = validLocs[pickedIndex];
            FurnitureLocation pickedFurn = correspondingParents[pickedIndex];
            NewRoom addressRoom = validAdRoom[pickedIndex];

            int pickedLocIndex = pickedFurn.furniture.subObjects.FindIndex(item => item == pickedPos);

            List<Interactable.Passed> passed = new List<Interactable.Passed>();
            passed.Add(new Interactable.Passed(Interactable.PassedVarType.roomID, addressRoom.roomID));

            Interactable newObj = InteractableCreator.Instance.CreateFurnitureSpawnedInteractableThreadSafe(InteriorControls.Instance.key, pickedFurn.anchorNode.room, pickedFurn, pickedPos, ad.inhabitants[0], ad.inhabitants[0], null, passed, null, null);

            ad.hiddenSpareKey = true;

            return newObj;
        }

        return null;
    }

    //Get the angle from a wall offset
    public float GetAngleForOffset(Vector2 offset1)
    {
        float angle = Mathf.DeltaAngle(Mathf.Atan2(offset1.y, offset1.x) * Mathf.Rad2Deg,
                                Mathf.Atan2(0f, 0f) * Mathf.Rad2Deg) - 90f;

        while (angle < 0)
        {
            angle += 360;
        }

        return angle;
    }

    //Get the offset from an angle
    public Vector2 GetOffsetFromAngle(int angle)
    {
        //Round angle to 45 degrees
        int angleRounded = Mathf.RoundToInt(angle / 45f) * 45;
        Vector2 offset = new Vector2(0, 1);

        if (angleRounded == 45)
        {
            offset = new Vector2(1, 1);
        }
        else if (angleRounded == 90)
        {
            offset = new Vector2(1, 0);
        }
        else if (angleRounded == 135)
        {
            offset = new Vector2(1, -1);
        }
        else if (angleRounded == 180)
        {
            offset = new Vector2(0, -1);
        }
        else if (angleRounded == 225)
        {
            offset = new Vector2(-1, -1);
        }
        else if (angleRounded == 270)
        {
            offset = new Vector2(-1, 0);
        }
        else if (angleRounded == 315)
        {
            offset = new Vector2(-1, 1);
        }

        return offset;
    }

    //Get the angle between points
    public float GetAngleBetween(Vector3 origin, Vector3 lookAt)
    {
        //return Mathf.DeltaAngle(Mathf.Atan2(offset1.y, offset1.x) * Mathf.Rad2Deg,
        //                        Mathf.Atan2(offset2.y, offset2.x) * Mathf.Rad2Deg) - 90f;




        //float xDiff = offset2.x - offset1.x;
        //float yDiff = offset2.y - offset1.y;
        //float angle = Mathf.Atan2(yDiff, xDiff) * (180f / Mathf.PI);

        //while (angle < -0)
        //{
        //    angle += 360;
        //}

        //return angle;

        //Vector2 diference = offset2 - offset1;
        //float sign = (offset2.y < offset1.y) ? -1.0f : 1.0f;
        //return Vector2.Angle(Vector2.right, diference) * sign;

        Vector3 relativePos = origin - lookAt;
        Quaternion look = Quaternion.LookRotation(relativePos, Vector3.up);
        return (look.eulerAngles.y + 180f) % 360; //My forward is the opposite of unity's.
    }

    //Get the average position for a collection of nodes
    private Vector3 GetAveragePosition(List<NewNode> nodes)
    {
        Vector3 ret = Vector3.zero;

        foreach (NewNode node in nodes)
        {
            ret += node.position;
        }

        return ret / (float)nodes.Count;
    }

    //Is day x (int 0-6) a work day?
    public bool IsWorkDay(int day, Citizen cit)
    {
        if (cit.job == null) return false;
        else if (cit.job.employer == null) return false;

        bool ret = false;
        while (day >= 7)
        {
            day -= 7;
        }
        day = Mathf.Clamp(day, 0, 6);

        if (day == 0 && cit.job.shift.monday) ret = true;
        else if (day == 1 && cit.job.shift.tuesday) ret = true;
        else if (day == 2 && cit.job.shift.wednesday) ret = true;
        else if (day == 3 && cit.job.shift.thursday) ret = true;
        else if (day == 4 && cit.job.shift.friday) ret = true;
        else if (day == 5 && cit.job.shift.saturday) ret = true;
        else if (day == 6 && cit.job.shift.sunday) ret = true;

        return ret;
    }

    //Find nearest furniture from this location (excluding current room, game location). This can be used as a catch-all search, but can end up being expensive.
    public Interactable FindNearestWithAction(AIActionPreset action, NewRoom startRoom, Human person, AIActionPreset.FindSetting findSetting, bool overrideWithHome = true, HashSet<NewRoom> ignore = null, NewGameLocation restrictTo = null, NewBuilding restrictToBuilding = null, bool useSpecialCasesOnly = false, InteractablePreset.SpecialCase mustBeSpecial = InteractablePreset.SpecialCase.none, bool filterWithRoomType = false, List<RoomTypePreset> roomTypeFilter = null, bool preferUnused = true, bool enforcersAllowedEverywhere = false, float robberyPriority = 0f, List<Interactable> avoidInteractables = null, List<InteractablePreset> shopItems = null, bool printDebug = false, bool mustContainDesireCategory = false, CompanyPreset.CompanyCategory containDesireCategory = CompanyPreset.CompanyCategory.meal, bool excludeAIUsingThis = false)
    {
        //if (startRoom == null) return null;

        //Dynamic room search: Search outwards from location
        HashSet<NewRoom> openSet = new HashSet<NewRoom>();
        HashSet<NewRoom> closedSet = new HashSet<NewRoom>();
        openSet.Add(startRoom);

        int failsafe = 800; //Alter this to make this not such a performance hog.

        if(printDebug) Game.Log("FindNearestWithAction: " + action.name + " Starting with room " + startRoom.name);

        bool reachedRestrictedLocation = true;
        bool reachedRestrictedBuilding = true;
        bool exitedStartBuilding = false;

        if(restrictTo != null)
        {
            if(startRoom.gameLocation != restrictTo)
            {
                reachedRestrictedLocation = false;
            }
        }

        if (restrictToBuilding != null)
        {
            if (startRoom.gameLocation.building != restrictToBuilding)
            {
                reachedRestrictedBuilding = false;
            }
        }

        while (openSet.Count > 0 && failsafe > 0)
        {
            NewRoom current = openSet.First<NewRoom>();

            //Game.Log(ignore);
            //Game.Log(current);

            //Is what we're looking for here?
            if(current != null)
            {
                if(!reachedRestrictedLocation && restrictTo != null)
                {
                    if(current.gameLocation == restrictTo)
                    {
                        reachedRestrictedLocation = true;
                        if (printDebug) Game.Log("FindNearestWithAction: Reached resticted location");
                    }
                }

                if (!reachedRestrictedBuilding && restrictToBuilding != null)
                {
                    if (current.gameLocation.building == restrictToBuilding)
                    {
                        reachedRestrictedBuilding = true;
                        if (printDebug) Game.Log("FindNearestWithAction: Reached resticted building");
                    }
                }

                if ((ignore == null || !ignore.Contains(current)) && (!reachedRestrictedLocation || (restrictTo == null || current.gameLocation == restrictTo)) && (!reachedRestrictedBuilding || (restrictToBuilding == null || current.building == restrictToBuilding))) //Don't search @ ignored
                {
                    bool scanThis = false;

                    //Apply room type filter
                    if (!filterWithRoomType || roomTypeFilter.Contains(current.roomType))
                    {
                        if (overrideWithHome && person.home != null && current.gameLocation == person.home)
                        {
                            scanThis = true;
                        }
                        else
                        {
                            if (findSetting == AIActionPreset.FindSetting.allAreas)
                            {
                                scanThis = true;
                            }
                            else if (findSetting == AIActionPreset.FindSetting.homeOnly)
                            {
                                if (current.gameLocation == person.home && !person.home.isCrimeScene)
                                {
                                    scanThis = true;
                                }
                            }
                            else if (findSetting == AIActionPreset.FindSetting.workOnly)
                            {
                                if(person.job != null && person.job.employer != null)
                                {
                                    if (current.gameLocation == person.job.employer.placeOfBusiness || current.gameLocation == person.job.employer.address)
                                    {
                                        scanThis = true;
                                    }
                                }
                            }
                            else if (findSetting == AIActionPreset.FindSetting.nonTrespassing)
                            {
                                if (!person.IsTrespassing(current, out _, out _, enforcersAllowedEverywhere))
                                {
                                    scanThis = true;
                                }
                            }
                            else if (findSetting == AIActionPreset.FindSetting.onlyPublic)
                            {
                                bool isPlayer = false;
                                if (person != null) isPlayer = person.isPlayer;
                                scanThis = current.gameLocation.IsPublicallyOpen(isPlayer);
                            }
                        }
                    }

                    if (shopItems != null && scanThis)
                    {
                        if (current.gameLocation.thisAsAddress != null && current.gameLocation.thisAsAddress.company != null)
                        {
                            bool shopPass = true;

                            foreach (InteractablePreset sellsItem in shopItems)
                            {
                                //Scan company selling...
                                if (!current.gameLocation.thisAsAddress.company.prices.ContainsKey(sellsItem))
                                {
                                    shopPass = false;
                                    break;
                                }
                            }

                            if (!shopPass) scanThis = false;
                        }
                        else scanThis = false;
                    }

                    if (!reachedRestrictedBuilding && restrictToBuilding != null && scanThis)
                    {
                        if (exitedStartBuilding)
                        {
                            if (current.building != null && current.building != restrictToBuilding)
                            {
                                scanThis = false;
                            }
                        }
                        else if (current.gameLocation.building == null)
                        {
                            exitedStartBuilding = true;
                            if (printDebug) Game.Log("FindNearestWithAction: Has exited start building");
                        }
                    }

                    if (scanThis)
                    {
                        if (printDebug) Game.Log("FindNearestWithAction: Scanning with room " + current.name + " (" + current.actionReference.Count + " actions present)");

                        if (current.actionReference.ContainsKey(action))
                        {
                            Interactable bestChoice = null;
                            float bestRank = -9999f;

                            //There's something here that features what we're looking for...
                            foreach (Interactable obj in current.actionReference[action])
                            {
                                if (printDebug) Game.Log("FindNearestWithAction: Scanning object " + obj.name);

                                if(excludeAIUsingThis)
                                {
                                    if(obj.nextAIInteraction != null)
                                    {
                                        if (printDebug) Game.Log("FindNearestWithAction: Object " + obj.name + " has existing user");
                                        continue;
                                    }
                                }

                                //Discount if moved by player
                                if (!obj.originalPosition)
                                {
                                    if (printDebug) Game.Log("FindNearestWithAction: Object " + obj.name + " has been moved by player!");
                                    continue;
                                }

                                //Check desire category
                                if(mustContainDesireCategory)
                                {
                                    //Get menu...
                                    //Look in fridge
                                    if (obj.preset.specialCaseFlag == InteractablePreset.SpecialCase.fridge)
                                    {
                                        if(obj.furnitureParent != null)
                                        {
                                            if(!obj.furnitureParent.spawnedInteractables.Exists(item => item.preset.retailItem != null && item.preset.retailItem.desireCategory == containDesireCategory))
                                            {
                                                continue;
                                            }
                                        }
                                    }
                                    //Search for menu override
                                    else if (obj.preset.menuOverride != null)
                                    {
                                        if(!obj.preset.menuOverride.itemsSold.Exists(item => item.retailItem != null && item.retailItem.desireCategory == containDesireCategory))
                                        {
                                            continue;
                                        }
                                    }
                                    //Search for company at address
                                    else if (current.gameLocation.thisAsAddress != null && current.gameLocation.thisAsAddress.company != null)
                                    {
                                        bool containDesirePass = false;

                                        foreach(KeyValuePair<InteractablePreset, int> pair in current.gameLocation.thisAsAddress.company.prices)
                                        {
                                            if(pair.Key.retailItem != null && pair.Key.retailItem.desireCategory == containDesireCategory)
                                            {
                                                containDesirePass = true;
                                                break;
                                            }
                                        }

                                        if (!containDesirePass) continue;
                                    }
                                    //Search for self employed
                                    else if (obj.furnitureParent != null && obj.furnitureParent.ownerMap.Count > 0)
                                    {
                                        bool containDesirePass = false;

                                        foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> pair in obj.furnitureParent.ownerMap)
                                        {
                                            if(pair.Key.human != null)
                                            {
                                                if (pair.Key.human.job != null && pair.Key.human.job.employer != null && pair.Key.human.job.preset.selfEmployed)
                                                {
                                                    foreach (KeyValuePair<InteractablePreset, int> pair2 in pair.Key.human.job.employer.prices)
                                                    {
                                                        if (pair2.Key.retailItem != null && pair2.Key.retailItem.desireCategory == containDesireCategory)
                                                        {
                                                            containDesirePass = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }

                                            if (containDesirePass) break;
                                        }

                                        if (!containDesirePass) continue;
                                    }
                                }

                                if (useSpecialCasesOnly)
                                {
                                    if (obj.preset.specialCaseFlag != mustBeSpecial)
                                    {
                                        continue;
                                    }
                                }

                                //Make sure this has nobody on it or it isn't ringing
                                if(action.requiresTelephone)
                                {
                                    if (obj.t != null)
                                    {
                                        //Active caller
                                        if (obj.t.activeReceiver != null)
                                        {
                                            continue;
                                        }
                                        //Has call
                                        else if(action.requiresTelephoneNoCall && obj.t.activeCall != null && obj.t.activeCall.Count > 0)
                                        {
                                            continue;
                                        }
                                    }
                                    else continue;
                                }

                                Human user = null;
                                obj.usagePoint.TryGetUserAtSlot(action.usageSlot, out user);

                                //This list is already sorted by action rank...
                                if(!preferUnused || (user == null && obj.usagePoint.reserved == null && obj.node.occupiedSpace.Count <= 0))
                                {
                                    return obj;
                                }
                                else
                                {
                                    //Choose unused
                                    float thisRank = obj.preset.AIPriority;

                                    if(robberyPriority > 0f && obj.furnitureParent != null && obj.furnitureParent.furnitureClasses.Count > 0)
                                    {
                                        thisRank += obj.furnitureParent.furnitureClasses[0].aiRobberyPriority * robberyPriority;
                                    }

                                    if(avoidInteractables != null && avoidInteractables.Contains(obj))
                                    {
                                        thisRank -= 100;
                                    }

                                    if(user == null)
                                    {
                                        thisRank += 5 + Toolbox.Instance.Rand(0f, 1f);
                                    }

                                    if (obj.node.occupiedSpace.Count <= 0)
                                    {
                                        thisRank += 7 + Toolbox.Instance.Rand(0f, 1f);
                                    }

                                    if (obj.usagePoint.reserved  != null)
                                    {
                                        thisRank -= 6;
                                    }

                                    if(bestChoice == null || thisRank > bestRank)
                                    {
                                        bestChoice = obj;
                                        bestRank = thisRank;
                                    }
                                }
                            }

                            if(bestChoice != null)
                            {
                                return bestChoice;
                            }
                        }
                        //else
                        //{
                        //    Game.Log("FindNearestWithAction: Action " + action + " is not present in room " + current.name);
                        //}
                    }
                }

                foreach (NewNode.NodeAccess acc in current.entrances)
                {
                    if (!acc.walkingAccess) continue;

                    NewRoom other = acc.GetOtherRoom(current);

                    if (!openSet.Contains(other))
                    {
                        if (!closedSet.Contains(other))
                        {
                            openSet.Add(other);
                        }
                    }
                }
            }

            closedSet.Add(current);
            openSet.Remove(current);
            failsafe--;

            if(printDebug && failsafe <= 0) Game.Log("FindNearestWithAction: Failsafe count reached");
        }

        //Game.Log("AI: Cannot find " + action.name + " using FindNearestWithAction");
        return null;
    }

    //Find the nearest gamelocation that sells this item
    public Company FindNearestThatSells(InteractablePreset sellsItem, NewGameLocation startLocation)
    {
        //Dynamic room search: Search outwards from location
        HashSet<NewGameLocation> openSet = new HashSet<NewGameLocation>();
        HashSet<NewGameLocation> closedSet = new HashSet<NewGameLocation>();
        openSet.Add(startLocation);

        int failsafe = CityData.Instance.gameLocationDirectory.Count + 1; //Alter this to make this not such a performance hog.

        //Game.Log("FindNearestWithAction: Starting with room " + startRoom.name);

        while (openSet.Count > 0 && failsafe > 0)
        {
            NewGameLocation current = openSet.First<NewGameLocation>();

            //Is what we're looking for here?
            if (current != null)
            {
                //Search for company at address
                if (current.thisAsAddress != null && current.thisAsAddress.company != null)
                {
                    if(current.thisAsAddress.company.prices.ContainsKey(sellsItem))
                    {
                        return current.thisAsAddress.company;
                    }
                }

                foreach (NewNode.NodeAccess acc in current.entrances)
                {
                    NewGameLocation other = acc.GetOtherGameLocation(current);

                    if (!openSet.Contains(other))
                    {
                        if (!closedSet.Contains(other))
                        {
                            openSet.Add(other);
                        }
                    }
                }
            }

            closedSet.Add(current);
            openSet.Remove(current);
            failsafe--;
        }

        //Game.Log("AI: Cannot find " + action.name + " using FindNearestWithAction");
        return null;
    }

    //Get 1st, 2nd, 3rd etc
    public string GetNumbericalStringReference(int number)
    {
        string dateStr = number.ToString();

        if (number >= 11 && number <= 13)
        {
            dateStr += Strings.Get("ui.interface", "th");
        }
        else
        {
            if (dateStr.Length > 0 && dateStr.Substring(dateStr.Length - 1, 1) == "1") dateStr += Strings.Get("ui.interface", "st");
            else if (dateStr.Length > 0 && dateStr.Substring(dateStr.Length - 1, 1) == "2") dateStr += Strings.Get("ui.interface", "nd");
            else if (dateStr.Length > 0 && dateStr.Substring(dateStr.Length - 1, 1) == "3") dateStr += Strings.Get("ui.interface", "rd");
            else dateStr += Strings.Get("ui.interface", "th");
        }

        return dateStr;
    }

    public Vector2 Rotate(Vector2 aPoint, float aDegree)
    {
        return Quaternion.Euler(0, 0, aDegree) * aPoint;
    }

    //Bresenham Line Formula
    public List<Vector2> PlotLine(Vector2 point1, Vector2 point2)
    {
        List<Vector2> passed = new List<Vector2>();

        int dx = Mathf.Abs((int)point2.x - (int)point1.x), sx = point1.x < point2.x ? 1 : -1;
        int dy = -Mathf.Abs((int)point2.y - (int)point1.y), sy = point1.y < point2.y ? 1 : -1;
        int err = dx + dy, e2; /* error value e_xy */

        for (; ; )
        {  /* loop */
            passed.Add(new Vector2(point1.x, point1.y));

            if (point1.x == point2.x && point1.y == point2.y) break;
            e2 = 2 * err;
            if (e2 >= dy) { err += dy; point1.x += sx; } /* e_xy+e_x > 0 */
            if (e2 <= dx) { err += dx; point1.y += sy; } /* e_xy+e_y < 0 */
        }

        return passed;
    }

    //Clamp a quaternion
    public Quaternion ClampRotation(Quaternion q, float minimumUpDown, float maximumUpDown, float minimumLeftRight, float maximumLeftRight)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);
        angleX = Mathf.Clamp(angleX, minimumUpDown, maximumUpDown);
        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        float angleY = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.y);
        angleY = Mathf.Clamp(angleY, minimumLeftRight, maximumLeftRight);
        q.y = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleY);

        return q;
    }

    public float ClampAngle(float angle, float min, float max)
    {
        if (angle < 90 || angle > 270)
        {       // if angle in the critic region...
            if (angle > 180) angle -= 360;  // convert all angles to -180..+180
            if (max > 180) max -= 360;
            if (min > 180) min -= 360;
        }

        angle = Mathf.Clamp(angle, min, max);
        if (angle < 0) angle += 360;  // if angle negative, convert to 0..360
        return angle;
    }

    //Shuffle a list
    public void ShuffleList(ref List<CharacterTrait> list)
    {
        if (list == null || list.Count <= 1) return;

        for (int i = 0; i < list.Count; i++)
        {
            CharacterTrait temp = list[i];

            if (temp == null)
            {
                list.RemoveAt(i);
                i--;
                continue;
            }

            int randomIndex = Rand(i, list.Count);
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }
    }

    //Shuffle a list using a seed, output a new seed generated from the rng it generates
    public void ShuffleListSeedContained(ref List<CharacterTrait> list, string input, out string output)
    {
        output = input;

        if (list == null || list.Count <= 1) return;

        for (int i = 0; i < list.Count; i++)
        {
            CharacterTrait temp = list[i];

            if(temp == null)
            {
                list.RemoveAt(i);
                i--;
                continue;
            }

            int randomIndex = GetPsuedoRandomNumberContained(i, list.Count, input, out output);
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }
    }

    public void ShuffleList(ref List<Human.WalletItem> list)
    {
        if (list == null || list.Count <= 1) return;

        for (int i = 0; i < list.Count; i++)
        {
            Human.WalletItem temp = list[i];

            if (temp == null)
            {
                list.RemoveAt(i);
                i--;
                continue;
            }

            int randomIndex = Toolbox.Instance.Rand(i, list.Count);
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }
    }

    //Spawn object: Can be called from a non monobehaviour class
    public GameObject SpawnObject(GameObject newObj, Transform newParent)
    {
        return Instantiate(newObj, newParent);
    }

    public GameObject SpawnObject(GameObject newObj, Vector3 newPos, Quaternion newRot, Transform newParent)
    {
        return Instantiate(newObj, newPos, newRot, newParent);
    }

    public void DestroyObject(GameObject newObj)
    {
        if (newObj != null)
        {
            Destroy(newObj);
        }
    }

    //Spawn material: Can be called from a non monobehaviour class
    public Material SpawnMaterial(Material newObj)
    {
        return Instantiate(newObj);
    }

    public Vector3 GetLocalEulerAtRotation(Transform transform, Quaternion targetRotation)
    {
        var q = Quaternion.Inverse(transform.parent.rotation) * targetRotation;
        return q.eulerAngles;
    }

    //Get key code from string
    public List<int> GetKeyCodeFromString(string str)
    {
        List<int> ret = new List<int>();

        try
        {
            for (int i = 0; i < 4; i++)
            {
                string c = str.Substring(i, 1).ToLower();

                if (c == "a" || c == "b" || c == "c")
                {
                    ret.Add(2);
                }
                else if (c == "d" || c == "e" || c == "f")
                {
                    ret.Add(3);
                }
                else if (c == "g" || c == "h" || c == "i")
                {
                    ret.Add(4);
                }
                else if (c == "j" || c == "k" || c == "l")
                {
                    ret.Add(5);
                }
                else if (c == "m" || c == "n" || c == "o")
                {
                    ret.Add(6);
                }
                else if (c == "p" || c == "r" || c == "s" || c == "q")
                {
                    ret.Add(7);
                }
                else if (c == "t" || c == "u" || c == "v")
                {
                    ret.Add(8);
                }
                else if (c == "w" || c == "x" || c == "y" || c == "z")
                {
                    ret.Add(9);
                }
                else ret.Add(0);
            }
        }
        catch
        {

        }


        return ret;
    }

    //Generate a unique evidence identifier that can be serialized as a string then used to find specific evidence after loading (without having to save evidence itself)
    public string GenerateEvidenceIdentifier(Evidence ev)
    {
        string ret = string.Empty;

        //The first part of the string is the preset name
        ret += ev.preset.name + "|";

        //The second part is the controller (if applicable)
        if (ev.controller != null)
        {
            ret += ev.controller.name;
        }

        ret += "|";

        //The third part is the evidence parent preset
        if (ev.parent != null)
        {
            ret += ev.parent.preset.name;
        }

        return ret;
    }

    //Use this carefully: It is not predicatable across games!
    public string GenerateUniqueID()
    {
        return System.Guid.NewGuid().ToString();
    }

    //Find the closest item
    public Interactable FindClosestObjectTo(InteractablePreset objectType, Vector3 closestTo, NewBuilding constrainToBuilding, NewGameLocation constrainToLocation, NewRoom constrainToRoom, out float distance, bool publicOnly = false)
    {
        distance = 0;

        //Bedside cabinets in the bedroom
        List<Interactable> objectsOfType = new List<Interactable>();

        List<NewGameLocation> locations = new List<NewGameLocation>();
        List<NewRoom> rooms = new List<NewRoom>();

        if (constrainToLocation != null)
        {
            locations.Add(constrainToLocation);
        }
        else if (constrainToBuilding != null)
        {
            foreach(KeyValuePair<int, NewFloor> pair in constrainToBuilding.floors)
            {
                locations.AddRange(pair.Value.addresses);
            }
        }
        else
        {
            if (publicOnly)
            {
                //Ideally use places that are public and open for business
                List<Company> comps = CityData.Instance.companyDirectory.FindAll(item => item.preset != null && item.preset.publicFacing && item.openForBusinessActual && item.openForBusinessDesired && item.placeOfBusiness != null && !item.placeOfBusiness.isCrimeScene);

                //If there aren't any then just use public ones
                if (comps.Count <= 0)
                {
                    comps = CityData.Instance.companyDirectory.FindAll(item => item.preset != null && item.preset.publicFacing);
                }

                foreach (Company cc in comps)
                {
                    locations.Add(cc.placeOfBusiness);
                }
            }
            else
            {
                locations.AddRange(CityData.Instance.gameLocationDirectory); //Please don't ever do this, it'll take ages!
            }
        }

        if (constrainToRoom != null)
        {
            rooms.Add(constrainToRoom);
        }
        else if (constrainToLocation != null)
        {
            rooms.AddRange(constrainToLocation.rooms);
        }
        else
        {
            foreach (NewGameLocation ngl in locations)
            {
                rooms.AddRange(ngl.rooms);
            }
        }

        //Now search rooms
        foreach (NewRoom room in rooms)
        {
            foreach(NewNode n in room.nodes)
            {
                objectsOfType.AddRange(n.interactables.FindAll(item => item.preset == objectType));
            }
        }

        //Find closest
        Interactable closest = null;
        distance = Mathf.Infinity;

        foreach (Interactable subobj in objectsOfType)
        {
            //Disable if this has been moved by the player
            //if (!subobj.originalPosition) continue;

            Vector3 loc = subobj.GetWorldPosition();

            float dist = Vector3.Distance(loc, closestTo);
            //Game.Log(subobj.existing.name + " Find closest distance: " + dist + " to " + closestTo);

            if (dist < distance)
            {
                distance = dist;
                closest = subobj;
            }
        }

        return closest;
    }

    //Find furniture within a gamelocation
    public FurnitureLocation FindFurnitureWithinGameLocation(NewGameLocation location, FurnitureClass furnitureClass, out NewRoom room)
    {
        room = null;

        foreach (NewRoom r in location.rooms)
        {
            if (r == null) continue;

            foreach (FurnitureLocation loc in r.individualFurniture)
            {
                if (loc.furnitureClasses.Contains(furnitureClass))
                {
                    room = r;
                    return loc;
                }
            }
        }

        return null;
    }

    //Set size of a Rect
    public void SetRectSize(RectTransform trs, float left, float top, float right, float bottom)
    {
        trs.offsetMin = new Vector2(left, bottom);
        trs.offsetMax = new Vector2(-right, -top);
    }

    // Converts RectTransform.rect's local coordinates to world space
    //Optional scale pulled from the CanvasScaler. Default to using Vector2.one.
    public Rect GetWorldRect(RectTransform rt, Vector2 scale)
    {
        // Convert the rectangle to world corners and grab the top left
        Vector3[] corners = new Vector3[4];
        rt.GetWorldCorners(corners);
        Vector3 topLeft = corners[0];

        // Rescale the size appropriately based on the current Canvas scale
        Vector2 scaledSize = new Vector2(scale.x * rt.rect.size.x, scale.y * rt.rect.size.y);

        return new Rect(topLeft, scaledSize);
    }

    public enum LayerMaskMode { castAllExcept, onlyCast };

    //Creates a layer mask for use with raycasts:
    public int CreateLayerMask(LayerMaskMode castMode, params int[] aLayers)
    {
        bool aExclude = false;

        if (castMode == LayerMaskMode.castAllExcept)
        {
            aExclude = true;
        }

        int v = 0;
        foreach (var L in aLayers)
            v |= 1 << L;
        if (aExclude)
            v = ~v;
        return v;
    }

    //Find closest node to world coordinate (include option for accessibility). Use in NPC ragdoll location finding on get-up, and invalid interactable node finding
    public NewNode FindClosestValidNodeToWorldPosition(Vector3 worldPos, bool onlyAccessibleNodes = false, bool checkUpAndDown = true, bool limitToDirection = false, Vector3Int limitedDirection = new Vector3Int(), bool limitToFloor = false, int limitedToFloor = 0, bool outsideOnly = false, int safety = 200)
    {
        if (limitToFloor)
        {
            worldPos.y = limitedToFloor * PathFinder.Instance.nodeSize.z;
        }

        Vector3Int worldNodePos = CityData.Instance.RealPosToNodeInt(worldPos);

        //if(Player.Instance.currentNode != null && InteractionController.Instance.carryingObject != null) Game.Log("Trying to find closest node to WP: " + worldPos + " node: " + worldNodePos + " player node: " + Player.Instance.currentNode.nodeCoord + " node ranges X: " + PathFinder.Instance.nodeRangeX + " Y:" + PathFinder.Instance.nodeRangeY + " Z:" + PathFinder.Instance.nodeRangeZ);

        //Keep adding coordinates equal to node size in every direction until we find something...
        List<Vector3Int> openSet = new List<Vector3Int>();
        openSet.Add(worldNodePos);

        List<Vector3> closedSet = new List<Vector3>();

        while (openSet.Count > 0 && safety > 0)
        {
            Vector3Int currentX = openSet[0];

            //if (InteractionController.Instance.carryingObject != null) Game.Log("Current: " + currentX);

            //Check for node
            NewNode foundNode = null;

            if (PathFinder.Instance.nodeMap.TryGetValue(currentX, out foundNode))
            {
                if (onlyAccessibleNodes)
                {
                    if (foundNode.accessToOtherNodes.Count > 0)
                    {
                        if (!outsideOnly || (foundNode.isOutside || foundNode.room.IsOutside()))
                        {
                            //if (InteractionController.Instance.carryingObject != null) Game.Log("Found node: " + currentX + ": " + safety);
                            return foundNode;
                        }
                    }
                }
                else
                {
                    if (!outsideOnly || (foundNode.isOutside || foundNode.room.IsOutside()))
                    {
                        //if (InteractionController.Instance.carryingObject != null) Game.Log("Found node: " + currentX + ": " + safety);
                        return foundNode;
                    }
                }
            }
            //else if (InteractionController.Instance.carryingObject != null) Game.Log("Unable to find node: " + currentX);

            //Otherwise search outwards
            if (limitToDirection)
            {
                Vector3Int addPos = currentX + limitedDirection;

                addPos.x = Mathf.RoundToInt(Mathf.Clamp(addPos.x, PathFinder.Instance.nodeRangeX.x, PathFinder.Instance.nodeRangeX.y));
                addPos.y = Mathf.RoundToInt(Mathf.Clamp(addPos.y, PathFinder.Instance.nodeRangeY.x, PathFinder.Instance.nodeRangeY.y));
                addPos.z = Mathf.RoundToInt(Mathf.Clamp(addPos.z,PathFinder.Instance.nodeRangeZ.x, PathFinder.Instance.nodeRangeZ.y));

                if (!openSet.Contains(addPos))
                {
                    if (!closedSet.Contains(addPos))
                    {
                        //if (InteractionController.Instance.carryingObject != null) Game.Log("Adding to open set LD: " + addPos);
                        openSet.Add(addPos);
                    }
                }
            }
            else if (checkUpAndDown && !limitToFloor)
            {
                foreach (Vector3Int v3 in CityData.Instance.offsetArrayX6)
                {
                    Vector3Int addPos = currentX + v3;

                    addPos.x = Mathf.RoundToInt(Mathf.Clamp(addPos.x, PathFinder.Instance.nodeRangeX.x, PathFinder.Instance.nodeRangeX.y));
                    addPos.y = Mathf.RoundToInt(Mathf.Clamp(addPos.y, PathFinder.Instance.nodeRangeY.x, PathFinder.Instance.nodeRangeY.y));
                    addPos.z = Mathf.RoundToInt(Mathf.Clamp(addPos.z, PathFinder.Instance.nodeRangeZ.x, PathFinder.Instance.nodeRangeZ.y));

                    if (!openSet.Contains(addPos))
                    {
                        if (!closedSet.Contains(addPos))
                        {
                            //if (InteractionController.Instance.carryingObject != null) Game.Log("Adding to open set X6: " + addPos);
                            openSet.Add(addPos);
                        }
                    }
                }
            }
            else
            {
                foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                {
                    Vector3Int addPos = currentX + new Vector3Int(v2.x, v2.y);

                    addPos.x = Mathf.RoundToInt(Mathf.Clamp(addPos.x, PathFinder.Instance.nodeRangeX.x, PathFinder.Instance.nodeRangeX.y));
                    addPos.y = Mathf.RoundToInt(Mathf.Clamp(addPos.y, PathFinder.Instance.nodeRangeY.x, PathFinder.Instance.nodeRangeY.y));
                    addPos.z = Mathf.RoundToInt(Mathf.Clamp(addPos.z, PathFinder.Instance.nodeRangeZ.x, PathFinder.Instance.nodeRangeZ.y));

                    if (!openSet.Contains(addPos))
                    {
                        if (!closedSet.Contains(addPos))
                        {
                            //if (InteractionController.Instance.carryingObject != null) Game.Log("Adding to open set X4: " + addPos);
                            openSet.Add(addPos);
                        }
                    }
                }
            }

            //Remove current
            closedSet.Add(currentX);
            openSet.RemoveAt(0);
            safety--;
        }

        Game.LogError("Tried to find closest node to world position " + worldPos + " but failed. It could be nodes are not yet set up? Or this coordinate is rediculously far away from anything!");

        return null;
    }

    //Get material properties info from a material
    public MaterialGroupPreset GetMaterialProperties(Material mat)
    {
        MaterialGroupPreset ret = null;

        if (!materialProperties.TryGetValue(mat, out ret))
        {
            Game.Log("Unable to find specific material properties for material " + mat.name);
        }

        return ret;
    }

    //Get furniture properties info from a mesh
    public FurniturePreset GetFurnitureFromMesh(Mesh mesh)
    {
        if (mesh == null) return null;

        FurniturePreset ret = null;

        if(furnitureMeshReference != null)
        {
            if (!furnitureMeshReference.TryGetValue(mesh, out ret))
            {
                //Game.Log("Unable to find specific furniture preset for mesh " + filter.name);
            }
        }

        return ret;
    }

    //Get an interactable preset via a string name
    public InteractablePreset GetInteractablePreset(string interactableName)
    {
        InteractablePreset preset = null;

        try
        {
            preset = objectPresetDictionary[interactableName];
        }
        catch
        {
            Game.LogError("Cannot find object " + interactableName);
            return null;
        }

        return preset;
    }

    //this will transform WorldRotation to Target's local space
    public Quaternion TransformRotation(Quaternion worldRotation, Transform targetsLocal)
    {
        return Quaternion.Inverse(targetsLocal.rotation) * worldRotation; //this will transform WorldRotation to Target's local space
    }

    //this will transform from target's local rotation to world rotation
    public Quaternion InverseTransformRotation(Quaternion localRotation, Transform target)
    {
        return Quaternion.Inverse(target.rotation) * localRotation; //this will transform WorldRotation to Target's local space
    }

    //Shoot a bullet
    public void Shoot(Actor fromThis, Vector3 muzzlePoint, Vector3 aimPoint, float aimRange, float accuracy, float damage, MurderWeaponPreset weapon, bool ejectBrass, Vector3 ejectBrassPoint, bool forcePhysicsEjectBrass)
    {
        //Spawn muzzle flash
        GameObject muzzleFlash = Instantiate(PrefabControls.Instance.muzzleFlash, PrefabControls.Instance.mapContainer);
        muzzleFlash.transform.position = muzzlePoint;

        //Set muzzle dir
        Vector3 aimDir = aimPoint - muzzlePoint;
        muzzleFlash.transform.rotation = Quaternion.LookRotation(aimDir, muzzleFlash.transform.up);

        float rAngle = Mathf.Lerp(0.2f, 0f, accuracy);

        //Vector3 direction = aimDir.normalized * aimRange; //Calculate the true vector to the aim range

        Vector3 direction = aimDir.normalized + new Vector3(Toolbox.Instance.Rand(-rAngle, rAngle), Toolbox.Instance.Rand(-rAngle, rAngle), Toolbox.Instance.Rand(-rAngle, rAngle));

        //Vector3 v3 = Quaternion.AngleAxis(Toolbox.Instance.Rand(-rAngle, rAngle), Vector3.forward) * direction; //Apply variation/inaccuracy

        //direction = direction.normalized * weapon.maximumBulletRange; //Ge the vector of the full bullet range

        //Play sfx
        if (weapon.fireEvent != null) AudioController.Instance.PlayWorldOneShot(weapon.fireEvent, fromThis, fromThis.currentNode, muzzlePoint);

        //Eject brass
        if(ejectBrass && weapon.shellCasing != null)
        {
            Interactable newCasing = InteractableCreator.Instance.CreateWorldInteractable(weapon.shellCasing, fromThis as Human, fromThis as Human, null, ejectBrassPoint, Vector3.zero, null, null);

            //The coin should be spawned upon creation...
            //Set physics to simulate throw...
            if (newCasing != null)
            {
                newCasing.MarkAsTrash(true); //Remove this once we can

                Vector3 force = fromThis.transform.right * Toolbox.Instance.Rand(3.5f, 4.5f) + new Vector3(Toolbox.Instance.Rand(-0.1f, 0.1f), Toolbox.Instance.Rand(-0.1f, 0.1f), Toolbox.Instance.Rand(-0.1f, 0.1f));

                if (newCasing.controller != null)
                {
                    newCasing.controller.DropThis(false); //Activate physics

                    //Apply sideways force to ejected shell
                    newCasing.controller.rb.AddForce(force, ForceMode.VelocityChange);
                }
                else
                {
                    newCasing.ForcePhysicsActive(forcePhysicsEjectBrass, true, force, ForceMode.VelocityChange);
                }
            }
        }

        ////The Ray-hits will be in a circular area
        //float randomRadius = Toolbox.Instance.Rand(0f, Mathf.Lerp(1f, 0f, accuracy));
        //float randomAngle = Toolbox.Instance.Rand(0, 2f * Mathf.PI);

        ////Calculating the raycast direction
        //Vector3 direction = new Vector3(
        //    randomRadius * Mathf.Cos(randomAngle),
        //    randomRadius * Mathf.Sin(randomAngle),
        //    aimRange
        //);

        //Make the direction match the transform
        //muzzleTransform.rotation = Quaternion.LookRotation(direction - muzzleTransform.position, muzzleTransform.up);

        //It is like converting the Vector3.forward to transform.forward
        //direction = muzzleTransform.TransformDirection(direction.normalized) * weapon.maximumBulletRange;

        RaycastHit hit;

        if (Physics.Raycast(muzzlePoint, direction, out hit, weapon.maximumBulletRange, Toolbox.Instance.physicalObjectsLayerMask))
        {
            Vector3 hitDir = hit.point - muzzlePoint;

            Debug.DrawRay(muzzlePoint, hitDir, Color.yellow, 2f);

            Actor hitActor = hit.transform.gameObject.GetComponent<Actor>();
            if (hitActor == null) hitActor = hit.transform.gameObject.GetComponentInParent<Actor>();

            //Calc real damage: Consistent until aim range, then starts dropping off
            float dmg = damage;

            if(hit.distance > aimRange)
            {
                dmg = Mathf.Lerp(damage, 0f, (hit.distance - aimRange) / (weapon.maximumBulletRange - aimRange));
            }

            //Handle collision (no damage to self allowed as this could mess up routines)
            if (hitActor != null && hitActor != fromThis)
            {
                Debug.DrawRay(muzzlePoint, hitDir, Color.green, 2f);

                //Should I die?
                bool enableKill = false;

                //Allow kills if this is the victim
                if(fromThis.ai != null && hitActor.ai != null)
                {
                    if(fromThis.ai.killerForMurders.Exists(item => item.victim == hitActor))
                    {
                        enableKill = true;
                    }
                }

                if(hitActor.isPlayer)
                {
                    if (weapon.impactEventPlayer != null) AudioController.Instance.PlayWorldOneShot(weapon.impactEventPlayer, hitActor, hitActor.currentNode, hit.point);
                }
                else
                {
                    if (weapon.impactEventBody != null) AudioController.Instance.PlayWorldOneShot(weapon.impactEventBody, hitActor, hitActor.currentNode, hit.point);
                }

                hitActor.RecieveDamage(dmg, fromThis, hit.point, hitDir, weapon.forwardSpatter, weapon.backSpatter, SpatterSimulation.EraseMode.useDespawnTime, false, enableKill: enableKill);

                //Spawn wound
                if(weapon.entryWound != null)
                {
                    //Get closest model vert...
                    Citizen citi = hitActor as Citizen;

                    if(citi != null)
                    {
                        citi.CreateWoundClosestToPoint(hit.point, hit.normal, weapon.entryWound);
                    }
                }
            }
            else
            {
                if (weapon.impactEvent != null) AudioController.Instance.PlayWorldOneShot(weapon.impactEvent, hitActor, hitActor.currentNode, hit.point);

                //Hit window
                BreakableWindowController window = hit.transform.gameObject.GetComponent<BreakableWindowController>();

                if(window != null && !window.isBroken)
                {
                    window.BreakWindow(hit.point, direction.normalized, fromThis);
                }
                else
                {
                    //Hit interactable
                    InteractableController ic = hit.transform.gameObject.GetComponent<InteractableController>();

                    if(ic != null && ic.interactable != null)
                    {
                        //Create ricochet
                        GameObject ricochet = Instantiate(PrefabControls.Instance.ricochet, PrefabControls.Instance.mapContainer);
                        ricochet.transform.position = hit.point;
                        ricochet.transform.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);

                        //Drop unless skill upgrade
                        if(ic.isCarriedByPlayer)
                        {
                            if(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.holdingBlocksBullets) <= 0f)
                            {
                                ic.DropThis(false);
                            }
                        }
                        //Break
                        else if (ic.interactable.preset.breakable)
                        {
                            ic.BreakObject(hit.point, hit.normal, 1f, fromThis);
                        }
                    }
                    else
                    {
                        Debug.DrawRay(muzzlePoint, hitDir, Color.red, 2f);
                        //Game.Log("Hit actor not found on " + hit.transform.gameObject.name);

                        //Create ricochet
                        GameObject ricochet = Instantiate(PrefabControls.Instance.ricochet, PrefabControls.Instance.mapContainer);
                        ricochet.transform.position = hit.point;
                        ricochet.transform.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);

                        //Create bullet hole
                        if (weapon.bulletHole != null)
                        {
                            Interactable newHole = InteractableCreator.Instance.CreateWorldInteractable(weapon.bulletHole, null, null, null, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal).eulerAngles, null, null);
                            if(newHole != null) newHole.MarkAsTrash(true); //Remove this once we can
                        }
                    }
                }
            }
        }
        else
        {
            Debug.DrawRay(muzzlePoint, direction, Color.yellow, 2f);
        }
    }

    //Get a pseudo random number that is always the same, based on a string.
    public float GetPsuedoRandomNumber(float lowerRange, float upperRange, string str, bool updateLastKey = false)
    {
        int hash = str.GetHashCode(); //Get a hash code for the string.
        System.Random randomSequence = new System.Random(hash);

        double randomNumber1 = randomSequence.NextDouble();

        //Record the last generated number as a key to use next time...
        if(updateLastKey)
        {
            if(InteriorCreator.Instance != null && InteriorCreator.Instance.threadedInteriorCreationActive)
            {
                Game.LogError("Updating load state key while interior generation is ative! This shouldn't happen!");
            }

            lastRandomNumberKey = randomNumber1.ToString();
            lastRandomNumberKey = lastRandomNumberKey.Substring(2);
        }

        return Mathf.Lerp(lowerRange, upperRange, (float)randomNumber1);
    }

    public int GetPsuedoRandomNumber(int lowerRange, int upperRange, string str, bool updateLastKey = false)
    {
        int hash = str.GetHashCode(); //Get a hash code for the string.
        System.Random randomSequence = new System.Random(hash);

        double randomNumber1 = randomSequence.NextDouble();

        //Record the last generated number as a key to use next time...
        if(updateLastKey)
        {
            if (InteriorCreator.Instance != null && InteriorCreator.Instance.threadedInteriorCreationActive)
            {
                Game.LogError("Updating load state key while interior generation is ative! This shouldn't happen!");
            }

            lastRandomNumberKey = randomNumber1.ToString();
            lastRandomNumberKey = lastRandomNumberKey.Substring(2);
        }

        return Mathf.FloorToInt(Mathf.Lerp(lowerRange, upperRange, (float)randomNumber1));
    }

    //New random functions can use seed or true random...
    //Use Seed: Generates number based on seed (or the last generated number) resulting in different outcomes but that are repeatable.
    //Repeated: Uses the seed but will return the same number for a range each time.
    public float Rand(float min, float max, bool definitelyNotPartOfCityGeneration = false)
    {
        if(!definitelyNotPartOfCityGeneration && !SessionData.Instance.startedGame && CityConstructor.Instance != null && CityConstructor.Instance.loadingOperationActive && !CityConstructor.Instance.preSimActive)
        {
            Game.LogError("Generating random number without seed before game has started! If this is part of the city generation process, it could result in unpredictable cities...");
        }

        return UnityEngine.Random.Range(min, max);
    }

    public int Rand(int min, int max, bool definitelyNotPartOfCityGeneration = false)
    {
        if (!definitelyNotPartOfCityGeneration && !SessionData.Instance.startedGame && CityConstructor.Instance != null && CityConstructor.Instance.loadingOperationActive)
        {
            Game.LogError("Generating random number without seed before game has started! If this is part of the city generation process, it could result in unpredictable cities...");
        }

        return UnityEngine.Random.Range(min, max);
    }

    //Get random number from city seed and save a new key for the next random number...
    public float SeedRand(float min, float max)
    {
        float ret = min;
        if (min == max) return ret;

        if (lastRandomNumberKey.Length <= 0)
        {
            ret = GetPsuedoRandomNumber(min, max, CityData.Instance.seed, updateLastKey: true);
            Game.Log("CityGen: First random from seed: " + CityData.Instance.seed + " = " + ret);
        }
        else
        {
            ret = GetPsuedoRandomNumber(min, max, lastRandomNumberKey, updateLastKey: true);
            //Game.Log("Random from last gnerated: " + lastRandomNumberKey + " " + ret);
        }

        return ret;
    }

    public int SeedRand(int min, int max)
    {
        int ret = min;
        if (min == max) return ret;

        if (lastRandomNumberKey.Length <= 0)
        {
            ret = GetPsuedoRandomNumber(min, max, CityData.Instance.seed, updateLastKey: true);
            Game.Log("CityGen: First random from seed: " + CityData.Instance.seed + " = " + lastRandomNumberKey);
        }
        else
        {
            ret = GetPsuedoRandomNumber(min, max, lastRandomNumberKey, updateLastKey: true);
            //Game.Log("Random from last gnerated: " + lastRandomNumberKey + " " + ret);
        }

        return ret;
    }

    //Use a vector2 to generate a random range
    public float VectorToRandom(Vector2 vec)
    {
        return Rand(vec.x, vec.y);
    }

    public float VectorToRandomSeedContained(Vector2 vec, string input, out string output)
    {
        float ret = GetPsuedoRandomNumberContained(vec.x, vec.y, input, out output);
        return ret;
    }

    //Versions of the above that return the generated number for use in contained threading situations
    public float RandContained(float min, float max, string seedInput, out string seedOutput)
    {
        float ret = min;
        ret = GetPsuedoRandomNumberContained(min, max, seedInput, out seedOutput);

        return ret;
    }

    public int RandContained(int min, int max, string seedInput, out string seedOutput)
    {
        int ret = min;
        ret = GetPsuedoRandomNumberContained(min, max, seedInput, out seedOutput);

        return ret;
    }

    //Get a pseudo random number that is always the same, based on a string.
    public float GetPsuedoRandomNumberContained(float lowerRange, float upperRange, string seedInput, out string seedOutput)
    {
        seedOutput = seedInput;
        int hash = seedInput.GetHashCode(); //Get a hash code for the string.
        System.Random randomSequence = new System.Random(hash);

        double randomNumber1 = randomSequence.NextDouble();

        //Record the last generated number as a key to use next time...
        seedOutput = randomNumber1.ToString();
        seedOutput = seedOutput.Substring(2);

        return Mathf.Lerp(lowerRange, upperRange, (float)randomNumber1);
    }

    public int GetPsuedoRandomNumberContained(int lowerRange, int upperRange, string seedInput, out string seedOutput)
    {
        seedOutput = seedInput;

        int hash = 0;
        if(seedInput != null) hash = seedInput.GetHashCode(); //Get a hash code for the string.

        System.Random randomSequence = new System.Random(hash);

        double randomNumber1 = randomSequence.NextDouble();

        //Record the last generated number as a key to use next time...
        seedOutput = randomNumber1.ToString();
        seedOutput = seedOutput.Substring(2);

        return Mathf.FloorToInt(Mathf.Lerp(lowerRange, upperRange, (float)randomNumber1));
    }

    //DDS condition logic
    public bool DDSTraitConditionLogicAcquaintance(Human thisPerson, Acquaintance acquaintance, DDSSaveClasses.TraitConditionType logic, ref List<string> traitList)
    {
        Human other = null;

        if(acquaintance != null)
        {
            other = acquaintance.GetOther(thisPerson);
        }

        return DDSTraitConditionLogic(thisPerson, other, logic, ref traitList);
    }

    public bool DDSTraitConditionLogic(Human thisPerson, Human otherPerson, DDSSaveClasses.TraitConditionType logic, ref List<string> traitList)
    {
        bool pass = false;

        if (traitList.Count <= 0 || thisPerson == null || thisPerson.characterTraits == null) return true;

        //if(logic == DDSSaveClasses.TraitConditionType.IfNoneOfThese && traitList.Contains("Char-Shy"))
        //{
        //    Game.Log(thisPerson.name + " checking for not shy...");
        //}

        if (logic == DDSSaveClasses.TraitConditionType.IfAnyOfThese || logic == DDSSaveClasses.TraitConditionType.IfNoneOfThese || logic == DDSSaveClasses.TraitConditionType.IfAllOfThese)
        {
            foreach (Human.Trait trait in thisPerson.characterTraits)
            {
                if(logic == DDSSaveClasses.TraitConditionType.IfAnyOfThese && traitList.Contains(trait.trait.name))
                {
                    pass = true;
                    break;
                }
                else if(logic == DDSSaveClasses.TraitConditionType.IfNoneOfThese)
                {
                    if (traitList.Contains(trait.trait.name))
                    {
                        //if (logic == DDSSaveClasses.TraitConditionType.IfNoneOfThese && traitList.Contains("Char-Shy"))
                        //{
                        //    Game.Log("Contains " + trait.trait.name + ", so reult false");
                        //}

                        pass = false;
                        break;
                    }
                    else
                    {
                        //if (logic == DDSSaveClasses.TraitConditionType.IfNoneOfThese && traitList.Contains("Char-Shy"))
                        //{
                        //    Game.Log("Doesn't contain " + trait.trait.name + ", so true so far...");
                        //}

                        pass = true;
                    }
                }
                else if(logic == DDSSaveClasses.TraitConditionType.IfAllOfThese)
                {
                    if (!traitList.Contains(trait.trait.name))
                    {
                        pass = false;
                        break;
                    }
                    else pass = true;
                }
            }
        }
        else if (otherPerson != null)
        {
            foreach (Human.Trait trait in otherPerson.characterTraits)
            {
                if (logic == DDSSaveClasses.TraitConditionType.otherAnyOfThese && traitList.Contains(trait.trait.name))
                {
                    pass = true;
                    break;
                }
                else if (logic == DDSSaveClasses.TraitConditionType.otherNoneOfThese)
                {
                    if (traitList.Contains(trait.trait.name))
                    {
                        pass = false;
                        break;
                    }
                    else pass = true;
                }
                else if (logic == DDSSaveClasses.TraitConditionType.otherAllOfThese)
                {
                    if (!traitList.Contains(trait.trait.name))
                    {
                        pass = false;
                        break;
                    }
                    else pass = true;
                }
            }
        }
        else
        {
            pass = false;
        }

        //if (logic == DDSSaveClasses.TraitConditionType.IfNoneOfThese && traitList.Contains("Char-Shy"))
        //{
        //    Game.Log("Return " + pass);
        //}

        return pass;
    }

    //Remove when new system is working...
    public void LoadInteractableToWorld()
    {
        //InteractablePreset interPreset = null;
        //Toolbox.Instance.LoadDataFromResources<InteractablePreset>(inter.p, out interPreset);

        //FurnitureLocation furnParent = null;
        //FurniturePreset.SubObject subObject = null;

        //if (inter.f > -1)
        //{
        //    furnParent = CityConstructor.Instance.loadingFurnitureReference[inter.f];

        //    if (inter.s > -1)
        //    {
        //        subObject = furnParent.furniture.subObjects[inter.s];
        //    }
        //}

        ////Load passed object
        //object passed = null;

        ////Passed object is retail item
        //RetailItemPreset retail = null;
        //if (inter.r.Length > 0) retail = Toolbox.Instance.allItems.Find(item => item.name == inter.r);

        //if (retail != null)
        //{
        //    passed = retail;
        //}
        //else if (inter.bk.Length > 0)
        //{
        //    //Passed object is book
        //    BookPreset bookPreset = null;
        //    Toolbox.Instance.LoadDataFromResources<BookPreset>(inter.bk, out bookPreset);

        //    if (bookPreset != null)
        //    {
        //        passed = bookPreset;
        //    }
        //}
        //else if(inter.sd != null && inter.sd.Length > 0)
        //{
        //    //Passed object is syncdisk
        //    SyncDiskPreset syncPreset = null;
        //    Toolbox.Instance.LoadDataFromResources<SyncDiskPreset>(inter.sd, out syncPreset);

        //    if (syncPreset != null)
        //    {
        //        passed = syncPreset;
        //    }
        //}

        //Human belongsTo = null;
        //Human receiver = null;

        //Citizen foundCitizen = null;

        //if (CityData.Instance.citizenDictionary.TryGetValue(inter.b, out foundCitizen))
        //{
        //    belongsTo = foundCitizen as Human;
        //}
        //else if (inter.b > -1)
        //{
        //    Game.LogError("Cannot find citizen with ID " + inter.b);
        //}

        //Citizen foundRec = null;

        //if (CityData.Instance.citizenDictionary.TryGetValue(inter.re, out foundRec))
        //{
        //    receiver = foundRec as Human;
        //}

        //LightingPreset lp = null;

        ////Get light preset
        //if (inter.lp.Length > 0)
        //{
        //    Toolbox.Instance.LoadDataFromResources<LightingPreset>(inter.lp, out lp);
        //}

        //if (inter.m)
        //{
        //    NewNode lightNode = null;

        //    if(!PathFinder.Instance.nodeMap.TryGetValue(inter.ln, out lightNode))
        //    {
        //        Game.LogError("CityGen: Light Load Error: Unable to find light node at " + inter.ln);
        //    }
        //    else
        //    {
        //        if(lightNode.room == null)
        //        {
        //            Game.LogError("CityGen: Light Load Error: Node " + lightNode.nodeCoord + " has no room!");
        //        }
        //        else
        //        {
        //            //Safeguard to stop lights being spawned in stairwell
        //            //if (!lightNode.tile.isStairwell)
        //            //{
        //            Interactable newLight = InteractableCreator.Instance.CreateMainLightInteractable(interPreset, lightNode.room.transform, inter.po, Vector3.zero, lp, inter.lc, inter.lz);
        //                newLight.SetValue(inter.v);

        //                return newLight;
        //            //}
        //        }
        //    }
        //}
        //else
        //{
        //    Interactable newInter = null;

        //    //Spawn world objects...
        //    if(inter.w)
        //    {
        //        newInter = InteractableCreator.Instance.CreateWorldInteractable(interPreset, belongsTo, receiver, inter.po, inter.e, inter.pa, null);
        //    }
        //    //Spawn sub objects...
        //    else if (subObject != null)
        //    {
        //        newInter = InteractableCreator.Instance.CreateFurnitureSpawnedInteractable(interPreset, furnParent, subObject, belongsTo, receiver, inter.pa, lp, null);
        //    }
        //    //This should only be books and other stackables...
        //    else
        //    {
        //        newInter = InteractableCreator.Instance.CreateFurnitureInteractable(interPreset, furnParent, belongsTo, receiver, inter.po, inter.e, InteractableController.InteractableID.A, FurniturePreset.SubObjectOwnership.nobody, null);
        //    }

        //    if (newInter != null)
        //    {
        //        newInter.SetValue(inter.v);
        //        newInter.SetDDSOverride(inter.ovr);

        //        //Set own colour
        //        if (newInter.preset.useOwnColourSettings)
        //        {
        //            newInter.SetMaterialKey(inter.mk);
        //        }

        //        //Load multi-page evidence
        //        if (inter.fp != null)
        //        {
        //            foreach (MetaObject f in inter.fp)
        //            {
        //                if (newInter.savedContents == null) newInter.savedContents = new List<MetaObject>();
        //                if(!newInter.savedContents.Contains(f)) newInter.savedContents.Add(f);
        //                EvidenceMultiPage mp = newInter.evidence as EvidenceMultiPage;

        //                if (mp != null)
        //                {
        //                    mp.AddOrAppendContentToPage(mp.pageContent.Count + 1, Strings.Get("evidence.names", f.preset));
        //                    mp.AddContainedMetaObject(f, mp.pageContent.Count);
        //                }
        //            }
        //        }
        //    }

        //    return newInter;
        //}

        //return null;
    }

    public string ToBase26(int myNumber)
    {
        int originalNumber = myNumber;
        myNumber++;

        var array = new LinkedList<int>();

        while (myNumber > 26)
        {
            int value = myNumber % 26;

            if (value == 0)
            {
                myNumber = myNumber / 26 - 1;
                array.AddFirst(26);
            }
            else
            {
                myNumber /= 26;
                array.AddFirst(value);
            }
        }

        if (myNumber > 0)
        {
            array.AddFirst(myNumber);
        }

        string ret = new string(array.Select(s => (char)('A' + s - 1)).ToArray());
        Game.Log("Evidence: Base-24 conversion of " + originalNumber + " = " + ret);

        return ret;
    }

    public string GenerateSeed(int digits = 16, bool useSeed = false, string newSeed = "")
    {
        string seed = string.Empty;

        for (int i = 0; i < digits; i++)
        {
            float r = 0f;

            if(useSeed)
            {
                r = Toolbox.Instance.RandContained(0f, 1f, newSeed, out newSeed);
            }
            else
            {
                r = Toolbox.Instance.Rand(0f, 1f);
            }

            if (r <= 0.33f)
            {
                seed += seedLetters[Toolbox.Instance.Rand(0, seedLetters.Length)];
            }
            else if (r >= 0.66f)
            {
                seed += seedLetters[Toolbox.Instance.Rand(0, seedLetters.Length)].ToString().ToLower();
            }
            else
            {
                seed += seedNumbers[Toolbox.Instance.Rand(0, seedNumbers.Length)];
            }
        }

        return seed;
    }

    //A simple point-to-point check for trasnforms
    public bool RaycastCheck(Transform from, Transform to, float maxRange, out RaycastHit hit)
    {
        return RaycastCheck(from.position, to, maxRange, out hit);
    }

    //A simple point-to-point check for vector3
    public bool RaycastCheck(Vector3 from, Transform to, float maxRange, out RaycastHit hit)
    {
        Vector3 dir = to.transform.position - from;

        Ray ray = new Ray(from, dir);

        if (Physics.Raycast(ray, out hit, maxRange, aiSightingLayerMask, QueryTriggerInteraction.Collide))
        {
            if (hit.transform == to)
            {
                return true;
            }
        }

        return false;
    }

    //The same as above but uses collider & bounds for target position
    public bool RaycastCheck(Vector3 from, Collider to, float maxRange, out RaycastHit hit)
    {
        Vector3 dir = to.bounds.center - from;

        Ray ray = new Ray(from, dir);

        //if (Game.Instance.devMode) Debug.DrawRay(from, dir.normalized * maxRange);

        if (Physics.Raycast(ray, out hit, maxRange, aiSightingLayerMask, QueryTriggerInteraction.Collide))
        {
            if (hit.collider == to)
            {
                return true;
            }
        }

        return false;
    }

    //Set pivot without changing the position of the element
    public void SetPivot(RectTransform rectTransform, Vector2 pivot)
    {
        Vector3 deltaPosition = rectTransform.pivot - pivot;    // get change in pivot
        deltaPosition.Scale(rectTransform.rect.size);           // apply sizing
        deltaPosition.Scale(rectTransform.localScale);          // apply scaling
        deltaPosition = rectTransform.rotation * deltaPosition; // apply rotation

        rectTransform.pivot = pivot;                            // change the pivot
        rectTransform.localPosition -= deltaPosition;           // reverse the position change
    }

    //Set anchor without changing the position of the element
    public void SetAnchor(RectTransform rectTransform, Vector2 anchorMin, Vector2 anchorMax)
    {
        Vector3 rememberPos = rectTransform.position;
        rectTransform.anchorMin = anchorMin;
        rectTransform.anchorMax = anchorMax;
        rectTransform.position = rememberPos;
    }

    //Recursively get all objects in a transform
    public Transform[] GetAllTransforms(Transform t)
    {
        return t.GetComponentsInChildren<Transform>(true);
    }

    //Recursively search for a transform
    public Transform SearchForTransform(Transform parent, string search, bool printDebug = false)
    {
        if (parent.name == search) return parent;

        Transform child = null;

        // Loop through top level
        foreach (Transform t in parent)
        {
            if (printDebug) Game.Log("Transform search (" + search + "/" + parent.name + "): " + t.name);

            if (t.name == search)
            {
                child = t;
                return child;
            }
            else if (t.childCount > 0)
            {
                child = SearchForTransform(t, search, printDebug);

                if (child)
                {
                    return child;
                }
            }
        }

        return child;
    }

    //Return all tagged children in a transform
    public List<Transform> GetTagsWithinTransform(Transform parent, string tag)
    {
        List<Transform> ret = new List<Transform>();

        Transform[] allChildren = GetAllTransforms(parent);

        foreach(Transform t in allChildren)
        {
            if(t.CompareTag(tag))
            {
                ret.Add(t);
            }
        }

        return ret;
    }

    //Send vmails
    public void NewVmailThread(Human from, List<Human> otherParticipiants, string treeID, float timeStamp, int progress = 999, StateSaveData.CustomDataSource overrideDataSource = StateSaveData.CustomDataSource.sender, int newDataSourceID = -1)
    {
        Human b = null;
        Human c = null;
        Human d = null;
        List<Human> cc = new List<Human>();

        if (otherParticipiants.Count >= 1) b = otherParticipiants[0];
        if (otherParticipiants.Count >= 2) c = otherParticipiants[0];
        if (otherParticipiants.Count >= 3) d = otherParticipiants[0];

        if(otherParticipiants.Count >= 4)
        {
            for (int i = 4; i < otherParticipiants.Count; i++)
            {
                cc.Add(otherParticipiants[i]); //CC in others but they don't participate...
            }
        }

        NewVmailThread(from, b, c, d, cc, treeID, timeStamp, progress, overrideDataSource, newDataSourceID);
    }

    public StateSaveData.MessageThreadSave NewVmailThread(Human from, Human to1, Human to2, Human to3, List<Human> cc, string treeID, float timeStamp, int progress = 999, StateSaveData.CustomDataSource overrideDataSource = StateSaveData.CustomDataSource.sender, int newDataSourceID = -1)
    {
        DDSSaveClasses.DDSTreeSave tree = null;

        if(!allDDSTrees.TryGetValue(treeID, out tree))
        {
            Game.LogError("Cannot find vmail tree " + treeID);
            return null;
        }

        StateSaveData.MessageThreadSave newMessageThread = new StateSaveData.MessageThreadSave { participantA = from.humanID, msgType = DDSSaveClasses.TreeType.vmail, treeID = treeID, threadID = GameplayController.Instance.assignMessageThreadID, ds = overrideDataSource, dsID = newDataSourceID };
        GameplayController.Instance.assignMessageThreadID++;

        if (to1 != null) newMessageThread.participantB = to1.humanID;
        if (to2 != null) newMessageThread.participantC = to2.humanID;
        if (to3 != null) newMessageThread.participantD = to3.humanID;

        if(cc != null)
        {
            foreach (Human h in cc)
            {
                newMessageThread.cc.Add(h.humanID);
            }
        }

        //Run through tree and record messages
        string instanceID = tree.startingMessage;

        DDSSaveClasses.DDSMessageSettings msg = null;

        int safety = Mathf.Min(progress, tree.messageRef.Count);

        //Game.Log("Generating vmail thread with progress " + safety + " MIN(" + progress + " / " + tree.messageRef.Count + ")");

        //We need to add at least the first message...
        bool firstMessagedAdded = false;

        while (safety > 0 || !firstMessagedAdded)
        {
            if(!firstMessagedAdded)
            {
                firstMessagedAdded = true;
            }
            else
            {
                safety--;
            }

            if (tree.messageRef.TryGetValue(instanceID, out msg))
            {
                newMessageThread.messages.Add(msg.instanceID);
                newMessageThread.timestamps.Add(timeStamp);

                //Set currently talking...
                if (msg.saidBy <= 0)
                {
                    if (from != null) newMessageThread.senders.Add(from.humanID);
                    else newMessageThread.senders.Add(-1);
                }
                else if (msg.saidBy == 1)
                {
                    if (to1 != null) newMessageThread.senders.Add(to1.humanID);
                    else newMessageThread.senders.Add(-1);
                }
                else if (msg.saidBy == 2)
                {
                    if (to2 != null) newMessageThread.senders.Add(to2.humanID);
                    else newMessageThread.senders.Add(-1);
                }
                else if (msg.saidBy == 3)
                {
                    if (to3 != null) newMessageThread.senders.Add(to3.humanID);
                    else newMessageThread.senders.Add(-1);
                }

                if (msg.saidTo <= 0)
                {
                    if (from != null) newMessageThread.recievers.Add(from.humanID);
                    else newMessageThread.recievers.Add(-1);
                }
                else if (msg.saidTo == 1)
                {
                    if (to1 != null) newMessageThread.recievers.Add(to1.humanID);
                    else newMessageThread.recievers.Add(-1);
                }
                else if (msg.saidTo == 2)
                {
                    if (to2 != null) newMessageThread.recievers.Add(to2.humanID);
                    else newMessageThread.recievers.Add(-1);
                }
                else if (msg.saidTo == 3)
                {
                    if (to3 != null) newMessageThread.recievers.Add(to3.humanID);
                    else newMessageThread.recievers.Add(-1);
                }

                //Find the next message...
                List<Human.DDSRank> possibleLinks = GetMessageTreeLinkRankings(newMessageThread, msg);

                //If there are no possible links, the conversation is ended
                if (possibleLinks.Count <= 0)
                {
                    break;
                }
                else
                {
                    //Pick a time interval: this cannot be in the future! The most recent it can be is about 4 hours ago...
                    //Pick a normalized point along this remaining range...
                    if(SessionData.Instance.startedGame)
                    {
                        timeStamp = Mathf.Lerp(timeStamp, Mathf.Max(SessionData.Instance.gameTime - 4, timeStamp), Toolbox.Instance.Rand(0f, 0.75f));
                    }
                    else
                    {
                        timeStamp = Mathf.Lerp(timeStamp, Mathf.Max(SessionData.Instance.gameTime - 4, timeStamp), Toolbox.Instance.SeedRand(0f, 0.75f));
                    }

                    //Ready for the next message on the loop
                    instanceID = possibleLinks[0].linkRef.to;
                }
            }
            else
            {
                break;
            }
        }

        //Add msg thread start
        from.messageThreadsStarted.Add(newMessageThread);

        //Add feature references
        if(from != null) from.messageThreadFeatures.Add(newMessageThread);
        if (to1 != null) to1.messageThreadFeatures.Add(newMessageThread);
        if (to2 != null) to2.messageThreadFeatures.Add(newMessageThread);
        if (to3 != null) to3.messageThreadFeatures.Add(newMessageThread);

        if(cc != null)
        {
            foreach (Human h in cc)
            {
                h.messageThreadCCd.Add(newMessageThread);
            }
        }

        //Add to list
        GameplayController.Instance.messageThreads.Add(newMessageThread.threadID, newMessageThread);

        return newMessageThread;
    }

    //Returns possible tree links from this message (ranked highest first)
    public List<Human.DDSRank> GetMessageTreeLinkRankings(StateSaveData.MessageThreadSave thread, DDSSaveClasses.DDSMessageSettings thisMsg)
    {
        List<Human.DDSRank> possibleLinks = new List<Human.DDSRank>();

        Human currentlyTalking = null;
        CityData.Instance.GetHuman(thread.participantA, out currentlyTalking);

        if (thisMsg.saidBy == 1) CityData.Instance.GetHuman(thread.participantB, out currentlyTalking);
        else if (thisMsg.saidBy == 2) CityData.Instance.GetHuman(thread.participantC, out currentlyTalking);
        else if (thisMsg.saidBy == 3) CityData.Instance.GetHuman(thread.participantD, out currentlyTalking);

        string seed = thread.treeID + CityData.Instance.seed + thisMsg.instanceID;

        //We now need to process the next step of the conversation...
        foreach (DDSSaveClasses.DDSMessageLink link in thisMsg.links)
        {
            //Attempt to match the response...
            //Add a small random number to help break up ties...
            float rank = Toolbox.Instance.GetPsuedoRandomNumberContained(-0.01f, 0.01f, seed, out seed);

            if (link.useWeights)
            {
                rank += link.choiceWeight;
            }

            if (link.useKnowLike || link.useTraits)
            {
                DDSSaveClasses.DDSTreeSave tree = null;

                if (!allDDSTrees.TryGetValue(thread.treeID, out tree))
                {
                    Game.LogError("Cannot find vmail tree " + thread.treeID);
                    return possibleLinks;
                }

                //Grab the acquaintance of the corresponding message talker...
                DDSSaveClasses.DDSMessageSettings nextMessage = tree.messageRef[link.to];

                Human nextSaidBy = null;

                CityData.Instance.GetHuman(thread.participantA, out nextSaidBy);
                if (nextMessage.saidBy == 1) CityData.Instance.GetHuman(thread.participantB, out nextSaidBy);
                else if (nextMessage.saidBy == 2) CityData.Instance.GetHuman(thread.participantC, out nextSaidBy);
                else if (nextMessage.saidBy == 3) CityData.Instance.GetHuman(thread.participantD, out nextSaidBy);

                if (link.useKnowLike)
                {
                    Acquaintance nextAq = null;

                    if (currentlyTalking.FindAcquaintanceExists(nextSaidBy, out nextAq))
                    {
                        float matchDifference = Mathf.Abs(nextAq.like - link.like) + Mathf.Abs(nextAq.known - link.know); //The lower this is the better the fit 0 - 2

                        //Add 0 - 1 to rank for better matches
                        rank += (2f - matchDifference) / 2f;
                    }
                }

                //Check against traits
                if (link.useTraits)
                {
                    bool pass = DDSTraitConditionLogic(currentlyTalking, nextSaidBy, link.traitConditions, ref link.traits);

                    if (pass)
                    {
                        rank += 1f;
                    }
                }
            }

            possibleLinks.Add(new Human.DDSRank { linkRef = link, rankRef = rank });
        }

        possibleLinks.Sort((p1, p2) => p2.rankRef.CompareTo(p1.rankRef)); //Using P2 first gives highest first

        return possibleLinks;
    }

    //Progress a vmail thread...
    public void ProgressVmailThread(StateSaveData.MessageThreadSave thread, int addProgress)
    {
        DDSSaveClasses.DDSTreeSave tree = null;

        if (!allDDSTrees.TryGetValue(thread.treeID, out tree))
        {
            Game.LogError("Cannot find vmail tree " + thread.treeID);
            return;
        }

        Human from = null;
        CityData.Instance.GetHuman(thread.participantA, out from);

        //Run through timestamps and get the index of the last message sent
        //Run through tree and record messages
        string instanceID = tree.startingMessage;
        float timeStamp = -999999f;

        for (int i = 0; i < thread.timestamps.Count; i++)
        {
            if(thread.timestamps[i] > timeStamp)
            {
                timeStamp = thread.timestamps[i];
                instanceID = thread.messages[i];
            }
        }

        DDSSaveClasses.DDSMessageSettings msg = null;

        if(!tree.messageRef.TryGetValue(instanceID, out msg))
        {
            Game.LogError("Cannot find message instance ID " + instanceID);
            return;
        }

        //Find the next message...
        List<Human.DDSRank> immediateLinks = GetMessageTreeLinkRankings(thread, msg);

        //If there are no possible links, the conversation is ended
        if (immediateLinks.Count <= 0)
        {
            return;
        }
        else
        {
            //Pick a time interval: this cannot be in the future! The most recent it can be is about 4 hours ago...
            //Pick a normalized point along this remaining range...
            if(SessionData.Instance.startedGame)
            {
                timeStamp = Mathf.Lerp(timeStamp, Mathf.Max(SessionData.Instance.gameTime - 4, timeStamp), Toolbox.Instance.Rand(0f, 0.75f));
            }
            else
            {
                timeStamp = Mathf.Lerp(timeStamp, Mathf.Max(SessionData.Instance.gameTime - 4, timeStamp), Toolbox.Instance.SeedRand(0f, 0.75f));
            }

            //Ready for the next message on the loop
            instanceID = immediateLinks[0].linkRef.to;
        }

        int safety = Mathf.Min(addProgress, tree.messageRef.Count);

        while (safety > 0)
        {
            safety--;

            if (tree.messageRef.TryGetValue(instanceID, out msg))
            {
                thread.messages.Add(msg.instanceID);
                thread.timestamps.Add(timeStamp);

                //Set currently talking
                if (msg.saidBy <= 0) thread.senders.Add(from.humanID);
                else if (msg.saidBy == 1) thread.senders.Add(thread.participantB);
                else if (msg.saidBy == 2) thread.senders.Add(thread.participantC);
                else if (msg.saidBy == 3) thread.senders.Add(thread.participantD);

                if (msg.saidTo <= 0) thread.recievers.Add(from.humanID);
                else if (msg.saidTo == 1) thread.recievers.Add(thread.participantB);
                else if (msg.saidTo == 2) thread.recievers.Add(thread.participantC);
                else if (msg.saidTo == 3) thread.recievers.Add(thread.participantD);

                //Find the next message...
                List<Human.DDSRank> possibleLinks = GetMessageTreeLinkRankings(thread, msg);

                //If there are no possible links, the conversation is ended
                if (possibleLinks.Count <= 0)
                {
                    break;
                }
                else
                {
                    //Pick a time interval: this cannot be in the future! The most recent it can be is about 4 hours ago...
                    //Pick a normalized point along this remaining range...
                    if(SessionData.Instance.startedGame)
                    {
                        timeStamp = Mathf.Lerp(timeStamp, Mathf.Max(SessionData.Instance.gameTime - 4, timeStamp), Toolbox.Instance.Rand(0f, 0.5f));
                    }
                    else
                    {
                        timeStamp = Mathf.Lerp(timeStamp, Mathf.Max(SessionData.Instance.gameTime - 4, timeStamp), Toolbox.Instance.SeedRand(0f, 0.5f));
                    }

                    //Ready for the next message on the loop
                    instanceID = possibleLinks[0].linkRef.to;
                }
            }
            else
            {
                break;
            }
        }
    }

    //Get a vmail participant
    public bool GetVmailParticipant(Human initiator, DDSSaveClasses.DDSParticipant participant, List<Human> banned, out Human chosen)
    {
        chosen = null;

        int safety = 4;

        string seed = initiator.humanID.ToString();

        while ((chosen == null || banned.Contains(chosen) || chosen == initiator) && safety > 0)
        {
            safety--;

            if (participant.connection == Acquaintance.ConnectionType.anyAcquaintance && initiator.acquaintances.Count > 0)
            {
                chosen = initiator.acquaintances[RandContained(0, initiator.acquaintances.Count, seed, out seed)].with;
            }
            else if(participant.connection == Acquaintance.ConnectionType.anyone || participant.connection == Acquaintance.ConnectionType.anyoneNotPlayer)
            {
                int s = 99;

                while(chosen == null || chosen == initiator)
                {
                    chosen = CityData.Instance.citizenDictionary[RandContained(0, CityData.Instance.citizenDictionary.Count, seed, out seed)];
                    s--;
                    if (s <= 0) break;
                }
            }
            else if(participant.connection == Acquaintance.ConnectionType.relationshipMatch)
            {
                float relScore = -99999f;

                foreach(Citizen c in CityData.Instance.citizenDirectory)
                {
                    float score = Toolbox.Instance.RandContained(0f, 5f, seed, out seed);
                    if (c.partner == null) score += 10f;
                    if (c.attractedTo.Contains(initiator.gender)) score += 10f;

                    if(score > relScore)
                    {
                        relScore = score;
                        chosen = c;
                    }
                }
            }
            //'Fake' sender
            else if((int)participant.connection >= 22 && (int)participant.connection <= 33)
            {
                chosen = initiator; //Set same as initiator to get no human sender
                return true;
            }
            else
            {
                foreach(Acquaintance aq in initiator.acquaintances)
                {
                    if(aq.connections.Contains(participant.connection))
                    {
                        chosen = aq.with;
                    }

                    if(participant.connection == Acquaintance.ConnectionType.paramour)
                    {
                        if(aq.secretConnection == participant.connection)
                        {
                            chosen = aq.with;
                        }
                    }
                }
            }
        }

        if (chosen == null) return false;
        else return true;
    }

    //Select material from a design preset
    public MaterialGroupPreset SelectMaterial(RoomClassPreset roomType, float wealthLevel, DesignStylePreset designStyle, MaterialGroupPreset.MaterialType materialType, string seedInput, out string seedOutput)
    {
        List<MaterialGroupPreset> selectionList = new List<MaterialGroupPreset>();

        if(materialDesignStyleRef.ContainsKey(designStyle))
        {
            if(materialDesignStyleRef[designStyle].ContainsKey(materialType))
            {
                foreach(MaterialGroupPreset p in materialDesignStyleRef[designStyle][materialType])
                {
                    if (wealthLevel < p.minimumWealth) continue;

                    //Suitable for this room?
                    bool pass = false;

                    foreach (RoomTypeFilter filter in p.allowedRoomFilters)
                    {
                        //Matching
                        if (filter.roomClasses.Contains(roomType))
                        {
                            pass = true;
                            break;
                        }
                    }

                    if (!pass) continue;

                    MaterialGroupPreset.MaterialSettings set = p.designStyles.Find(item => item.designStyle == designStyle);

                    if(set != null)
                    {
                        for (int i = 0; i < set.weighting; i++)
                        {
                            selectionList.Add(p);
                        }
                    }
                }
            }
        }

        if(selectionList.Count <= 0)
        {
            if(materialType == MaterialGroupPreset.MaterialType.floor || materialType == MaterialGroupPreset.MaterialType.other)
            {
                if(roomType.name != "Null") Game.Log("CityGen: Using fallback floor material for " + roomType.name);
                selectionList.Add(CityControls.Instance.fallbackFloorMat);
            }
            else if(materialType == MaterialGroupPreset.MaterialType.ceiling)
            {
                if (roomType.name != "Null") Game.Log("CityGen: Using fallback ceiling material for " + roomType.name);
                selectionList.Add(CityControls.Instance.fallbackCeilingMat);
            }
            else if(materialType == MaterialGroupPreset.MaterialType.walls)
            {
                if (roomType.name != "Null") Game.Log("CityGen: Using fallback walls material for " + roomType.name);
                selectionList.Add(CityControls.Instance.fallbackWallMat);
            }
        }

        return selectionList[GetPsuedoRandomNumberContained(0, selectionList.Count, seedInput, out seedOutput)];
    }

    public WallFrontagePreset SelectWallFrontage(DesignStylePreset designStyle, WallFrontageClass frontageClass, string seed)
    {
        if(wallFrontageStyleRef.ContainsKey(designStyle))
        {
            if(wallFrontageStyleRef[designStyle].ContainsKey(frontageClass))
            {
                return wallFrontageStyleRef[designStyle][frontageClass][RandContained(0, wallFrontageStyleRef[designStyle][frontageClass].Count, seed, out _)];
            }
        }

        return null;
    }

    public float GetNormalizedLandValue(NewGameLocation location, bool print = false)
    {
        float wealthLevel = 0f;

        if (location.thisAsAddress != null)
        {
            wealthLevel = location.thisAsAddress.normalizedLandValue;
            if (print) Game.Log("Default land value: " + location.thisAsAddress.normalizedLandValue);

            //For residence; this is average wealth of citizens
            if (location.thisAsAddress.residence != null)
            {
                foreach (Human owner in location.thisAsAddress.inhabitants)
                {
                    if(owner.societalClass > wealthLevel)
                    {
                        wealthLevel = Mathf.Max(owner.societalClass);
                        if(print) Game.Log("Owner " + owner.name + " has greater societal class: " + owner.societalClass);
                    }
                }
            }

            if (location.thisAsAddress.addressPreset != null) wealthLevel = Mathf.Clamp(wealthLevel, location.thisAsAddress.addressPreset.minimumLandValue, location.thisAsAddress.addressPreset.maximumLandValue);
        }
        else if(location.nodes.Count > 0)
        {
            wealthLevel = (int)location.nodes[0].tile.cityTile.landValue / 4f;
        }

        return wealthLevel;
    }

    public float GetNormalizedLandValue(NewBuilding location)
    {
        return (int)location.cityTile.landValue / 4f;
    }

    public List<Human> GetFingerprintOwnerPool(NewRoom room, FurnitureLocation furn, Interactable inter, RoomConfiguration.PrintsSource source, Vector3 worldPos, bool forceFind)
    {
        List<Human> ownerPool = new List<Human>();

        //Always get room
        if (furn != null && room == null)
        {
            room = furn.anchorNode.room;
        }
        else if (inter != null && room == null && inter.node != null)
        {
            room = inter.node.room;
        }

        bool found = false;
        int attempts = 8;
        if (!forceFind) attempts = 1;

        while (!found && attempts > 0)
        {
            //Get who this belongs to...
            if (source == RoomConfiguration.PrintsSource.buildingResidents)
            {
                if(room != null)
                {
                    //Prefer residents located on this floor
                    NewFloor floor = room.floor;

                    if (floor != null)
                    {
                        foreach (NewAddress ad in floor.addresses)
                        {
                            ownerPool.AddRange(ad.inhabitants);
                        }
                    }
                    else if (room.building != null)
                    {
                        foreach (KeyValuePair<int, NewFloor> pair in room.building.floors)
                        {
                            foreach (NewAddress ad in pair.Value.addresses)
                            {
                                ownerPool.AddRange(ad.inhabitants);
                            }
                        }
                    }
                }

                //Set next backup source...
                source = RoomConfiguration.PrintsSource.inhabitants;
            }
            else if (source == RoomConfiguration.PrintsSource.inhabitants)
            {
                if (room != null && room.gameLocation.thisAsAddress != null)
                {
                    ownerPool.AddRange(room.gameLocation.thisAsAddress.inhabitants);
                }

                //Set next backup source...
                source = RoomConfiguration.PrintsSource.publicAll;
            }
            else if (source == RoomConfiguration.PrintsSource.inhabitantsAndCustomers)
            {
                if(room != null)
                {
                    if (room.gameLocation.thisAsAddress != null)
                    {
                        ownerPool.AddRange(room.gameLocation.thisAsAddress.inhabitants);
                    }

                    //Also add customers
                    if (room.gameLocation.thisAsAddress != null)
                    {
                        if (room.gameLocation.thisAsAddress.favouredCustomers.Count > 0)
                        {
                            ownerPool.AddRange(room.gameLocation.thisAsAddress.favouredCustomers);
                        }
                    }
                }

                //Set next backup source...
                source = RoomConfiguration.PrintsSource.publicAll;
            }
            else if (source == RoomConfiguration.PrintsSource.owners)
            {
                if (inter != null && inter.belongsTo != null)
                {
                    ownerPool.Add(inter.belongsTo);
                }
                else if (furn != null && furn.ownerMap != null && furn.ownerMap.Count > 0)
                {
                    foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> pair in furn.ownerMap)
                    {
                        if(pair.Key.human != null)
                        {
                            ownerPool.Add(pair.Key.human);
                        }
                        else if(pair.Key.address != null)
                        {
                            foreach(Human h in pair.Key.address.owners)
                            {
                                ownerPool.Add(h);
                            }
                        }
                    }
                }
                else if (room != null && room.belongsTo.Count > 0)
                {
                    ownerPool.AddRange(room.belongsTo);
                }

                //Set next backup source...
                source = RoomConfiguration.PrintsSource.inhabitants;
            }
            else if (source == RoomConfiguration.PrintsSource.writers)
            {
                if (inter != null && inter.writer != null)
                {
                    ownerPool.Add(inter.writer);
                }
                else if (furn != null && furn.ownerMap != null && furn.ownerMap.Count > 0)
                {
                    foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> pair in furn.ownerMap)
                    {
                        if(pair.Key.human != null)
                        {
                            ownerPool.Add(pair.Key.human);
                        }
                        else if (pair.Key.address != null)
                        {
                            foreach (Human h in pair.Key.address.owners)
                            {
                                ownerPool.Add(h);
                            }
                        }
                    }
                }
                else if (room != null && room.belongsTo.Count > 0)
                {
                    ownerPool.AddRange(room.belongsTo);
                }

                //Set next backup source...
                source = RoomConfiguration.PrintsSource.owners;
            }
            else if (source == RoomConfiguration.PrintsSource.receivers)
            {
                if (inter != null && inter.reciever != null)
                {
                    ownerPool.Add(inter.reciever);
                }
                else if (furn != null && furn.ownerMap != null && furn.ownerMap.Count > 0)
                {
                    foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> pair in furn.ownerMap)
                    {
                        if (pair.Key.human != null)
                        {
                            ownerPool.Add(pair.Key.human);
                        }
                        else if (pair.Key.address != null)
                        {
                            foreach (Human h in pair.Key.address.owners)
                            {
                                ownerPool.Add(h);
                            }
                        }
                    }
                }
                else if (room != null && room.belongsTo.Count > 0)
                {
                    ownerPool.AddRange(room.belongsTo);
                }

                //Set next backup source...
                source = RoomConfiguration.PrintsSource.owners;
            }
            else if (source == RoomConfiguration.PrintsSource.ownersAndWriters)
            {
                if (inter != null && (inter.reciever != null || inter.writer != null))
                {
                    if (inter.writer != null) ownerPool.Add(inter.writer);
                    if (inter.reciever != null) ownerPool.Add(inter.reciever);
                }
                else if (furn != null && furn.ownerMap != null && furn.ownerMap.Count > 0)
                {
                    foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> pair in furn.ownerMap)
                    {
                        if (pair.Key.human != null)
                        {
                            ownerPool.Add(pair.Key.human);
                        }
                        else if (pair.Key.address != null)
                        {
                            foreach (Human h in pair.Key.address.owners)
                            {
                                ownerPool.Add(h);
                            }
                        }
                    }
                }
                else if (room != null && room.belongsTo.Count > 0)
                {
                    ownerPool.AddRange(room.belongsTo);
                }

                //Set next backup source...
                source = RoomConfiguration.PrintsSource.owners;
            }
            else if (source == RoomConfiguration.PrintsSource.ownersWritersReceivers)
            {
                if (inter != null && (inter.reciever != null || inter.writer != null || inter.belongsTo != null))
                {
                    if (inter.belongsTo != null) ownerPool.Add(inter.belongsTo);
                    if (inter.writer != null) ownerPool.Add(inter.writer);
                    if (inter.reciever != null) ownerPool.Add(inter.reciever);
                }
                else if (furn != null && furn.ownerMap != null && furn.ownerMap.Count > 0)
                {
                    foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> pair in furn.ownerMap)
                    {
                        if (pair.Key.human != null)
                        {
                            ownerPool.Add(pair.Key.human);
                        }
                        else if (pair.Key.address != null)
                        {
                            foreach (Human h in pair.Key.address.owners)
                            {
                                ownerPool.Add(h);
                            }
                        }
                    }
                }
                else if (room != null && room.belongsTo.Count > 0)
                {
                    ownerPool.AddRange(room.belongsTo);
                }

                //Set next backup source...
                source = RoomConfiguration.PrintsSource.owners;
            }
            else if (source == RoomConfiguration.PrintsSource.customersAll)
            {
                if(room != null)
                {
                    if (room.gameLocation.thisAsAddress != null)
                    {
                        if (room.gameLocation.thisAsAddress.favouredCustomers.Count > 0)
                        {
                            ownerPool.AddRange(room.gameLocation.thisAsAddress.favouredCustomers);
                        }
                    }
                }

                //Set next backup source...
                source = RoomConfiguration.PrintsSource.publicAll;
            }
            else if (source == RoomConfiguration.PrintsSource.customersMale)
            {
                if (room != null && room.gameLocation.thisAsAddress != null)
                {
                    if (room.gameLocation.thisAsAddress.favouredCustomers.Count > 0)
                    {
                        List<Human> male = room.gameLocation.thisAsAddress.favouredCustomers.FindAll(item => item.gender == Human.Gender.male || (item.gender == Human.Gender.nonBinary && item.genderScale >= 0.5f));

                        if (male.Count > 0)
                        {
                            ownerPool.AddRange(male);
                        }
                    }
                }

                //Set next backup source...
                source = RoomConfiguration.PrintsSource.inhabitants;
            }
            else if (source == RoomConfiguration.PrintsSource.customersFemale)
            {
                if (room != null && room.gameLocation.thisAsAddress != null)
                {
                    if (room.gameLocation.thisAsAddress.favouredCustomers.Count > 0)
                    {
                        List<Human> male = room.gameLocation.thisAsAddress.favouredCustomers.FindAll(item => item.gender == Human.Gender.female || (item.gender == Human.Gender.nonBinary && item.genderScale < 0.5f));

                        if (male.Count > 0)
                        {
                            ownerPool.AddRange(male);
                        }
                    }
                }

                //Set next backup source...
                source = RoomConfiguration.PrintsSource.inhabitants;
            }
            else if (source == RoomConfiguration.PrintsSource.publicAll)
            {
                ownerPool.AddRange(CityData.Instance.citizenDirectory);
            }

            if (ownerPool.Count > 0) found = true;
            attempts--;
        }

        //Use backup plan of picking from random citizen...
        if (ownerPool.Count <= 0)
        {
            ownerPool.Add(CityData.Instance.citizenDirectory[Toolbox.Instance.GetPsuedoRandomNumber(0, CityData.Instance.citizenDirectory.Count, worldPos.ToString())]);
        }

        return ownerPool;
    }

    public void SpawnWindowAfterSeconds(Evidence ev, float after)
    {
        StartCoroutine(SpawnTelephoneEntryWindow(ev, after));
    }

    IEnumerator SpawnTelephoneEntryWindow(Evidence ev, float after)
    {
        float timer = 0f;

        while (timer < 2f)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        SessionData.Instance.PauseGame(true);
        InterfaceController.Instance.SpawnWindow(ev);
    }

    //Generate and save a city info file
    public CityInfoData GenerateCityInfoFile(FileInfo citySave)
    {
        CitySaveData cityData = null;

        //Parse to class file
        using (StreamReader streamReader = File.OpenText(citySave.FullName))
        {
            string jsonString = streamReader.ReadToEnd();
            cityData = JsonUtility.FromJson<CitySaveData>(jsonString);
        }

        CityInfoData newData = new CityInfoData();
        newData.cityName = cityData.cityName;
        newData.build = Game.Instance.buildID;
        newData.shareCode = Toolbox.Instance.GetShareCode(ref cityData);
        newData.citySize = cityData.citySize;
        newData.population = cityData.population;
        //newData.populationAmount = cityData.populationAmount;

        //Save to file...
        string path = citySave.FullName.Substring(0, citySave.FullName.Length - 4) + ".txt";
        string writeString = JsonUtility.ToJson(newData, true);

        using (StreamWriter streamWriter = File.CreateText(path))
        {
            streamWriter.Write(writeString);
        }

        Game.Log("Generate city info file: " + path);

        return newData;
    }

    //Get a telephone number string
    public string GetTelephoneNumberString(int number)
    {
        string numberString = number.ToString();

        //Update telephone string to include dashes.
        string str = string.Empty;
        int tCursor = 0;

        for (int i = 0; i < numberString.Length; i++)
        {
            str += numberString[i];
            tCursor++;

            if (tCursor >= 3 && i < numberString.Length - 1)
            {
                tCursor = -1;
                str += '—';
            }
        }

        return str;
    }

    //Get the number of lockpicks needed.
    public int GetLockpicksNeeded(float lockStrength)
    {
        //Lockpicks needed for this...
        bool countedCurrent = false;

        int picksNeeded = 0; //This includes current lockpick...

        while (lockStrength > 0f)
        {
            float lockpickSkill = Mathf.LerpUnclamped(GameplayControls.Instance.lockpickEffectivenessRange.x, GameplayControls.Instance.lockpickEffectivenessRange.y, UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.lockpickingEfficiencyModifier));
            float lockpickAmount = lockpickSkill;

            //First, count the current lockpick that may be partially used...
            if (!countedCurrent)
            {
                lockpickAmount = GameplayController.Instance.currentLockpickStrength * lockpickSkill;
                countedCurrent = true;
            }

            lockStrength -= lockpickAmount;
            picksNeeded++;
        }

        return picksNeeded;
    }

    //Create a time range with the actual time contained within
    public Vector2 CreateTimeRange(float actualTime, float accuracyMargin, bool limitToNow, bool round, int roundToMinutes)
    {
        float lerp = Rand(0f, 1f); //The actual time will sit within the range here

        float lowerEnd = actualTime - (lerp * accuracyMargin); //Calculate the upper range
        float upperEnd = actualTime + (1f - (lerp * accuracyMargin)); //Calculate the lower range

        //Make sure the upper end does not extend past now...
        if(limitToNow)
        {
            if(upperEnd > SessionData.Instance.gameTime)
            {
                float lowerEndAddition = upperEnd - SessionData.Instance.gameTime; //Get the difference
                lowerEnd -= lowerEndAddition; //Minus the difference and add it to the lower end to keep the same accuracy margin
                upperEnd = SessionData.Instance.gameTime; //Set the upper end to now
            }
        }

        if(round)
        {
            float roundTo = roundToMinutes * (1 / 60f);

            //Make sure these rounding methods will be inclusive of the actual value by using floor/ceiling values
            lowerEnd = Mathf.FloorToInt(lowerEnd / roundTo) * roundTo;
            upperEnd = Mathf.CeilToInt(upperEnd / roundTo) * roundTo;
        }

        return new Vector2(lowerEnd, upperEnd);
    }

    public void ScrollScrollRectOLD(CustomScrollRect scrollRect, Vector3 targetPos, bool allowHorizontal, bool allowVertical, float timeTaken = 0.2f, float extraScrollThreshold = 0.2f)
    {
        StartCoroutine(ExecuteScrollScrollRectOLD(scrollRect, targetPos, allowHorizontal, allowVertical, timeTaken, extraScrollThreshold));
    }

    IEnumerator ExecuteScrollScrollRectOLD(CustomScrollRect scrollRect, Vector3 targetPos, bool allowHorizontal, bool allowVertical, float timeTaken = 0.2f, float extraScrollThreshold = 0.2f)
    {
        float progress = 0f;
        Canvas.ForceUpdateCanvases();

        //Get the position of the object relative to the scrollrect content
        Vector3 rel = scrollRect.content.InverseTransformPoint(targetPos) + new Vector3(scrollRect.content.sizeDelta.x * scrollRect.content.pivot.x, scrollRect.content.sizeDelta.y * scrollRect.content.pivot.y, 0);
        Vector2 mapScrollPos = new Vector2(Mathf.Clamp01(rel.x / scrollRect.content.sizeDelta.x), Mathf.Clamp01(rel.y / scrollRect.content.sizeDelta.y)); //Get normalized position

        float scrollX = scrollRect.horizontalNormalizedPosition;

        if (allowHorizontal)
        {
            float extra = ((0.5f - mapScrollPos.x) * 2f) * extraScrollThreshold;
            scrollX = Mathf.Clamp(mapScrollPos.x - extra, 0f, 1f);
        }

        float scrollY = scrollRect.verticalNormalizedPosition;

        if (allowVertical)
        {
            float extra = ((0.5f - mapScrollPos.y) * 2f) * extraScrollThreshold;
            scrollY = Mathf.Clamp(mapScrollPos.y - extra, 0f, 1f);
            //Game.Log(mapScrollPos.y + " Extra Y " + extra);
        }

        Vector2 newPos = new Vector2(scrollX, scrollY);

        Game.Log("Scroll rect pos: " + newPos);

        while (progress < 1f)
        {
            progress += Time.deltaTime / timeTaken;
            scrollRect.normalizedPosition = Vector2.Lerp(scrollRect.normalizedPosition, newPos, progress);
            yield return null;
        }
    }

    //Smoothly scroll to a transform on the scroll rect
    public void ScrollRectPosition(CustomScrollRect scrollRect, RectTransform target, bool allowHorizontal, bool allowVertical, float timeTaken = 0.2f)
    {
        Canvas.ForceUpdateCanvases();

        //Override allow horizonal/vertical
        if (!scrollRect.horizontal) allowHorizontal = false;
        if (!scrollRect.vertical) allowVertical = false;

        //Viewport offset: Compensate for the anchored pivot position of the content...
        Vector2 viewportOffset = new Vector2(scrollRect.viewport.rect.width, scrollRect.viewport.rect.height) * -1f;
        viewportOffset *= (scrollRect.content.pivot - new Vector2(0.5f, 0.5f));

        //True anchored position
        Vector2 desiredAnchored = ((Vector2)scrollRect.transform.InverseTransformPoint(scrollRect.content.position)
            - (Vector2)scrollRect.transform.InverseTransformPoint(target.position)) + viewportOffset;

        //Clamp
        Vector2 clampY = new Vector2((scrollRect.content.sizeDelta.y - scrollRect.viewport.rect.height) * -(1f - scrollRect.content.pivot.y), (scrollRect.content.sizeDelta.y - scrollRect.viewport.rect.height) * scrollRect.content.pivot.y);
        Vector2 clampX = new Vector2((scrollRect.content.sizeDelta.x - scrollRect.viewport.rect.width) * -(1f - scrollRect.content.pivot.x), (scrollRect.content.sizeDelta.x - scrollRect.viewport.rect.width) * scrollRect.content.pivot.x);

        desiredAnchored = new Vector2(Mathf.Clamp(desiredAnchored.x, clampX.x, clampX.y), Mathf.Clamp(desiredAnchored.y, clampY.x, clampY.y));

        if (!allowHorizontal) desiredAnchored.x = scrollRect.content.anchoredPosition.x;
        if (!allowVertical) desiredAnchored.y = scrollRect.content.anchoredPosition.y;

        StartCoroutine(LerpScrollRect(scrollRect, desiredAnchored, timeTaken));
    }

    IEnumerator LerpScrollRect(CustomScrollRect scrollRect, Vector2 anchoredPos, float timeTaken = 0.2f)
    {
        float progress = 0f;
        Canvas.ForceUpdateCanvases();

        while (progress < 1f)
        {
            progress += Time.deltaTime / timeTaken;
            progress = Mathf.Clamp01(progress);
            scrollRect.content.anchoredPosition = Vector2.Lerp(scrollRect.content.anchoredPosition, anchoredPos, progress);
            yield return null;
        }

        scrollRect.content.anchoredPosition = Vector2.Lerp(scrollRect.content.anchoredPosition, anchoredPos, progress);
    }

    public NewNode PickNearbyNode(NewNode toThis)
    {
        HashSet<NewNode> openSet = new HashSet<NewNode>();
        HashSet<NewNode> closedSet = new HashSet<NewNode>();
        openSet.Add(toThis);

        List<NewNode> pickPool = new List<NewNode>();

        int safety = 20;

        while(openSet.Count > 0 && safety > 0)
        {
            NewNode current = openSet.FirstOrDefault();

            if(current != toThis)
            {
                pickPool.Add(current);
            }

            foreach(KeyValuePair<NewNode, NewNode.NodeAccess> pair in current.accessToOtherNodes)
            {
                if (!pair.Value.walkingAccess) continue;
                if (closedSet.Contains(pair.Key)) continue;
                if (openSet.Contains(pair.Key)) continue;

                openSet.Add(pair.Key);
            }

            openSet.Remove(current);
            closedSet.Add(current);
            safety--;

            if (pickPool.Count >= 4) break;
        }

        if (pickPool.Count > 0)
        {
            return pickPool[Toolbox.Instance.Rand(0, pickPool.Count)];
        }
        else return null;
    }

    public NewNode GetDoorSideNode(NewNode currentNode, NewDoor door)
    {
        //Room check
        if (door.wall.node.room == currentNode.room)
        {
            return door.wall.node;
        }
        else if (door.wall.otherWall.node.room == currentNode.room)
        {
            return door.wall.otherWall.node;
        }
        else
        {
            //Gamelocation check
            if (door.wall.node.gameLocation == currentNode.gameLocation)
            {
                return door.wall.node;
            }
            else if (door.wall.otherWall.node.gameLocation == currentNode.gameLocation)
            {
                return door.wall.otherWall.node;
            }
        }

        Dictionary<NewRoom, NewNode> openSet = new Dictionary<NewRoom, NewNode>();
        openSet.Add(door.wall.node.room, door.wall.node);
        openSet.Add(door.wall.otherWall.node.room, door.wall.otherWall.node);

        Dictionary<NewRoom, NewNode> closedSet = new Dictionary<NewRoom, NewNode>();

        int safety = 800;

        while(openSet.Count > 0 && safety > 0)
        {
            KeyValuePair<NewRoom, NewNode> current = openSet.FirstOrDefault();

            //Found position
            if (currentNode.room == current.Key)
            {
                return current.Value;
            }

            foreach(NewNode.NodeAccess ent in current.Key.entrances)
            {
                if (ent.door == door) continue; //Make sure to not 'go through' this door

                if(ent.walkingAccess)
                {
                    if(!openSet.ContainsKey(ent.fromNode.room))
                    {
                        if (!closedSet.ContainsKey(ent.fromNode.room))
                        {
                            openSet.Add(ent.fromNode.room, current.Value);
                        }
                    }

                    if (!openSet.ContainsKey(ent.toNode.room))
                    {
                        if (!closedSet.ContainsKey(ent.toNode.room))
                        {
                            openSet.Add(ent.toNode.room, current.Value);
                        }
                    }
                }
            }

            closedSet.Add(current.Key, current.Value);
            openSet.Remove(current.Key);
            safety--;
        }

        return door.wall.node; //If all else fails...
    }

    [Button]
    public void TestTimeRangeOverlap()
    {
        Game.Log(DecimalTimeRangeOverlap(debugTimeRange1, debugTimeRange2, true));
    }

    public void AutomaticNavigationSetup(ref List<Button> selectables, float differenceBuffer = 2f)
    {
        foreach (Button s in selectables)
        {
            Navigation nav = new Navigation();
            nav.mode = Navigation.Mode.Explicit;

            Vector3 sScreenPos = s.transform.position;

            for (int i = 0; i < 4; i++)
            {
                //Look for appropriate windows in this onscreen direction...
                Selectable bestS = null;
                float closest = Mathf.Infinity;

                foreach (Selectable w in selectables)
                {
                    if (w == s) continue; //Don't include itself...
                    Vector3 wScreenPos = w.transform.position;


                    Vector3 difference = wScreenPos - sScreenPos;
                    float dist = Vector3.Distance(wScreenPos, sScreenPos);

                    if (i == 0 && difference.x > differenceBuffer)
                    {
                        if (dist < closest)
                        {
                            bestS = w;
                            closest = dist;
                        }
                    }
                    else if (i == 1 && difference.x < -differenceBuffer)
                    {
                        if (dist < closest)
                        {
                            bestS = w;
                            closest = dist;
                        }
                    }
                    else if (i == 2 && difference.y > differenceBuffer)
                    {
                        if (dist < closest)
                        {
                            bestS = w;
                            closest = dist;
                        }
                    }
                    else if (i == 3 && difference.y < -differenceBuffer)
                    {
                        if (dist < closest)
                        {
                            bestS = w;
                            closest = dist;
                        }
                    }
                }

                if (bestS != null)
                {
                    if (i == 0)
                    {
                        nav.selectOnRight = bestS;
                    }
                    else if (i == 1)
                    {
                        nav.selectOnLeft = bestS;
                    }
                    else if (i == 2)
                    {
                        nav.selectOnUp = bestS;
                    }
                    else
                    {
                        nav.selectOnDown = bestS;
                    }
                }
            }

            s.navigation = nav;
        }
    }

    public void AddNavigationInput(Selectable selectable, Selectable newLeft = null, Selectable newRight = null, Selectable newUp = null, Selectable newDown = null, bool clearOld = false)
    {
        Navigation nav = new Navigation { mode = Navigation.Mode.Explicit };

        if(!clearOld)
        {
            nav.selectOnLeft = selectable.FindSelectableOnLeft();
            nav.selectOnRight = selectable.FindSelectableOnRight();
            nav.selectOnUp = selectable.FindSelectableOnUp();
            nav.selectOnDown = selectable.FindSelectableOnDown();
        }

        if (newLeft != null) nav.selectOnLeft = newLeft;
        if (newRight != null) nav.selectOnRight = newRight;
        if (newUp != null) nav.selectOnUp = newUp;
        if (newDown != null) nav.selectOnDown = newDown;

        if(selectable != null) selectable.navigation = nav;
    }

    public static void SetTextureImporterFormat(Texture2D texture, bool isReadable)
    {
#if UNITY_EDITOR
        if (null == texture) return;

        string assetPath = AssetDatabase.GetAssetPath(texture);
        var tImporter = AssetImporter.GetAtPath(assetPath) as TextureImporter;
        if (tImporter != null)
        {
            tImporter.isReadable = isReadable;

            AssetDatabase.ImportAsset(assetPath);
            AssetDatabase.Refresh();
        }
#endif
    }

    //Return whether actor has authority to relocate (set spawn position) of object
    public bool GetRelocateAuthority(Actor actor, Interactable obj)
    {
        //Is player at home?
        if(actor == Player.Instance)
        {
            if(obj.preset.relocateIfPlacedInPlayersHome)
            {
                if(actor.currentGameLocation == Player.Instance.home)
                {
                    return true;
                }
            }
        }

        if(obj.preset.relocationAuthority == InteractablePreset.RelocationAuthority.AIAndOwnersCanRelocate)
        {
            if (actor.ai != null || obj.belongsTo == actor) return true;
        }
        else if(obj.preset.relocationAuthority == InteractablePreset.RelocationAuthority.anyoneCanRelocate)
        {
            return true;
        }
        else if(obj.preset.relocationAuthority == InteractablePreset.RelocationAuthority.nooneCanRelocate)
        {
            return false;
        }
        else if (obj.preset.relocationAuthority == InteractablePreset.RelocationAuthority.ownerCanRelocate)
        {
            if (obj.belongsTo == actor) return true;
        }

        return false;
    }

    //Get the nearest street at ground level
    public NewNode GetNearestGroundLevelOutside(Vector3 pos)
    {
        return FindClosestValidNodeToWorldPosition(pos, false, false, false, limitToFloor: true, limitedToFloor: 0, outsideOnly: true, safety: 400);
    }

    public void HandleLaserBehaviour(SecuritySystem secSystem, GameObject laser, Light laserLight, float maxRange = 16)
    {
        RaycastHit rayHit;
        Ray r = new Ray(laser.transform.position, laser.transform.forward);

        if (Physics.Raycast(r, out rayHit, maxRange, Toolbox.Instance.aiSightingLayerMask, QueryTriggerInteraction.Collide))
        {
            //Unparent to set global scale
            Transform originalParent = laser.transform.parent;
            laser.transform.parent = null;
            laser.transform.localScale = new Vector3(laser.transform.localScale.x, laser.transform.localScale.y, rayHit.distance);
            laser.transform.parent = originalParent;

            if(laserLight != null) laserLight.range = rayHit.distance;

            laser.name = rayHit.transform.name + " (" + rayHit.distance + ")";

            //Is AI?
            Human hu = null;
            Player getPlayer = rayHit.transform.GetComponentInChildren<Player>();

            if (getPlayer != null)
            {
                hu = getPlayer;
            }
            else
            {
                Citizen getCitizen = rayHit.transform.GetComponentInChildren<Citizen>();

                if (getCitizen != null)
                {
                    hu = getCitizen;
                }
            }

            if(hu != null)
            {
                if (secSystem != null)
                {
                    if (hu.currentNode != null && hu.currentNode.gameLocation.thisAsAddress != null)
                    {
                        if (hu.currentNode.gameLocation.thisAsAddress.IsAlarmSystemTarget(hu) && (!hu.isPlayer || !Game.Instance.invisiblePlayer))
                        {
                            hu.currentNode.gameLocation.thisAsAddress.SetAlarm(true, hu);

                            if (!secSystem.seenIllegalThisCheck.Contains(hu))
                            {
                                secSystem.seenIllegalThisCheck.Add(hu);
                            }

                            if (!secSystem.seesIllegal.ContainsKey(hu))
                            {
                                secSystem.seesIllegal.Add(hu, 1);
                            }
                            else
                            {
                                secSystem.seesIllegal[hu] = 1;
                            }

                            secSystem.OnInvestigate(hu, 2);
                        }
                    }
                }
            }
        }
        else
        {
            laser.name = "Laser hit null (" + maxRange + ")";

            //Unparent to set global scale
            Transform originalParent = laser.transform.parent;
            laser.transform.parent = null;
            laser.transform.localScale = new Vector3(laser.transform.localScale.x, laser.transform.localScale.y, maxRange);
            if (laserLight != null) laserLight.range = maxRange;
            laser.transform.parent = originalParent;
        }
    }

    public Interactable GetLocalizedSnapshot(Interactable obj)
    {
        Interactable newPhoto = null;

        //Find a nearby position to capture item from...
        NewNode captureNode = obj.node;
        float bestScore = -999999f;
        string bestScoreDebug = string.Empty;

        if (obj.node.gameLocation != null)
        {
            //Build a list of nodes to evaluate
            List<NewNode> evaluateNodes = new List<NewNode>();

            foreach (NewRoom r in obj.node.gameLocation.rooms)
            {
                if (r.nodes.Count <= 0 || r.entrances.Count <= 0) continue;
                
                foreach(NewNode n in r.nodes)
                {
                    evaluateNodes.Add(n);
                }
            }

            //Expand to cover neighboring nodes - Removed for now as it could lead to confusion
            //List<NewNode> toAdd = new List<NewNode>();

            //foreach(NewNode n in evaluateNodes)
            //{
            //    foreach(KeyValuePair<NewNode, NewNode.NodeAccess> pair in n.accessToOtherNodes)
            //    {
            //        if(pair.Value.walkingAccess)
            //        {
            //            if(!evaluateNodes.Contains(pair.Key) && !toAdd.Contains(pair.Key))
            //            {
            //                toAdd.Add(pair.Key);
            //            }
            //        }
            //    }
            //}

            //evaluateNodes.AddRange(toAdd);

            Game.Log("Evaluating " + evaluateNodes.Count + " nodes...");

            foreach (NewNode n in evaluateNodes)
            {
                float score = Toolbox.Instance.Rand(0f, 5f); //Start with a random score

                float dist = Vector3.Distance(n.position, obj.node.position);
                float norm = Mathf.InverseLerp(GameplayControls.Instance.missionPhotoMinMaxDistance.x, GameplayControls.Instance.missionPhotoMinMaxDistance.y, dist);
                score += GameplayControls.Instance.missionPhotoDistanceScoreCurve.Evaluate(norm); //Add distance

                //Minus score for large furniture objects in the way
                float furnScore = 0;

                foreach(FurnitureLocation f in n.room.individualFurniture)
                {
                    if(f.coversNodes.Contains(n))
                    {
                        if(f.furniture.classes.Count > 0)
                        {
                            if(f.furniture.classes[0].discourageMissionPhotos)
                            {
                                furnScore -= 1000f;
                            }

                            if(f.furniture.classes[0].occupiesTile)
                            {
                                furnScore -= 10f;
                            }

                            if (f.furniture.classes[0].wallPiece)
                            {
                                furnScore -= 5f;
                            }
                        }
                    }
                }

                score += furnScore;

                foreach(Actor a in n.room.currentOccupants)
                {
                    if (a.currentNode == n) score -= 10f; //We ideally don't want actors standing here
                }

                //There is an unblocked path to the object according to the culling raycast
                if (DataRaycastController.Instance.NodeRaycast(n, obj.node, out _))
                {
                    score += 100f;
                }

                if (score > bestScore)
                {
                    captureNode = n;
                    bestScore = score;
                    bestScoreDebug =
                        "Score: " + bestScore + " (Distance norm: " + norm + " = " + Mathf.InverseLerp(GameplayControls.Instance.missionPhotoMinMaxDistance.x, GameplayControls.Instance.missionPhotoMinMaxDistance.y, dist) + ") furn : " + furnScore + ")";
                    Game.Log(bestScoreDebug);
                }
            }
        }

        Game.Log(bestScoreDebug);

        if (captureNode != null)
        {
            NewNode savedPlayerNode = Player.Instance.currentNode;

            Player.Instance.interactable.node = captureNode;
            Player.Instance.interactable.cvp = captureNode.position + new Vector3(0, Toolbox.Instance.Rand(1.6f, 2f), 0);

            Vector3 focusPoint = obj.node.position + new Vector3(Toolbox.Instance.Rand(-1.2f, 1.2f), Toolbox.Instance.Rand(1f, 3f), Toolbox.Instance.Rand(-1.2f, 1.2f));
            Vector3 focusDirection = (focusPoint - Player.Instance.interactable.cvp).normalized;

            Player.Instance.interactable.cve = Quaternion.LookRotation(focusDirection, Vector3.up).eulerAngles;

            //Player.Instance.sceneRecorder.RefreshCoveredArea(); //Update the covered area first... Note: This is now done in the capture class
            SceneRecorder.SceneCapture camScene = Player.Instance.sceneRecorder.ExecuteCapture(false, true);
            Game.Log("Player: New capture: " + camScene + " from " + Player.Instance.interactable.cvp + ", euler " + Player.Instance.interactable.cve);

            //Save the surveillance capture
            Player.Instance.sceneRecorder.interactable.sCap.Add(camScene);

            //Get passed vars
            Interactable.Passed passedID = new Interactable.Passed(Interactable.PassedVarType.savedSceneCapID, camScene.id);
            List<Interactable.Passed> newPassed = new List<Interactable.Passed>();
            newPassed.Add(passedID);

            newPhoto = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.surveillancePrintout, Player.Instance, Player.Instance, null, Player.Instance.transform.position, Vector3.zero, newPassed, null);

            if (newPhoto != null)
            {
                newPhoto.RemoveFromPlacement();
                newPhoto.MarkAsTrash(true);
            }

            Player.Instance.interactable.node = savedPlayerNode;
        }
        else Game.LogError("Unable to find a valid capture node!");

        return newPhoto;
    }

    public void RetroactiveSurveillanceAddition(Human who, NewNode routeFrom, NewNode routeTo, bool addReturnJourney, NewNode returnTo, float arrivalTime, float stayTime, ClothesPreset.OutfitCategory outfit)
    {
        PathFinder.PathData path = PathFinder.Instance.GetPath(routeFrom, routeTo, who);

        if (path != null)
        {
            //Scan route for cctv coverage, and record it here...
            Dictionary<NewRoom, List<NewNode>> nWnodesPool = new Dictionary<NewRoom, List<NewNode>>();
            Dictionary<NewNode, List<Interactable>> nWcameraCoverage = new Dictionary<NewNode, List<Interactable>>();

            //Detect the camera coverage along this route...
            foreach (NewNode.NodeAccess acc in path.accessList)
            {
                if (acc.toNode.gameLocation.securityCameras.Count > 0)
                {
                    foreach (Interactable cctv in acc.toNode.gameLocation.securityCameras)
                    {
                        //This node is covered by a camera, add to pool
                        if (cctv.sceneRecorder.coveredNodes.ContainsKey(acc.toNode))
                        {
                            if (!nWnodesPool.ContainsKey(acc.toNode.room))
                            {
                                nWnodesPool.Add(acc.toNode.room, new List<NewNode>());
                            }

                            nWnodesPool[acc.toNode.room].Add(acc.toNode);

                            if (!nWcameraCoverage.ContainsKey(acc.toNode))
                            {
                                nWcameraCoverage.Add(acc.toNode, new List<Interactable>());
                            }

                            nWcameraCoverage[acc.toNode].Add(cctv);
                        }
                    }
                }
            }

            //Work backwards from arrival time adding to cctv
            float backwardsTime = arrivalTime;
            HashSet<SceneRecorder.SceneCapture> addedToCaptures = new HashSet<SceneRecorder.SceneCapture>();
            NewNode lastNode = null; //Reference to the last node iterated

            for (int i = path.accessList.Count - 1; i > 0; i--)
            {
                NewNode bNode = path.accessList[i].fromNode;

                if (nWcameraCoverage.ContainsKey(bNode))
                {
                    //Calculate the facing direction here...
                    Vector3 facingDirection = Vector3.zero;

                    if(lastNode != null && bNode != null) facingDirection = lastNode.position - bNode.position;
                    facingDirection.y = 0;

                    foreach (Interactable cam in nWcameraCoverage[bNode])
                    {
                        //Are there any captures from around this time?
                        List<SceneRecorder.SceneCapture> captures = cam.cap.FindAll(item => item.t > backwardsTime - 0.0167f && item.t < backwardsTime + 0.0167f && !addedToCaptures.Contains(item));

                        if (captures.Count > 0)
                        {
                            //Add this to captures...
                            foreach (SceneRecorder.SceneCapture cap in captures)
                            {
                                if (addedToCaptures.Contains(cap)) continue; //This shouldn't happen anyway

                                //Is the actor here already? If so reposition
                                int existingIndex = cap.aCap.FindIndex(item => item.id == who.humanID);

                                if (existingIndex > -1)
                                {
                                    SceneRecorder.ActorCapture existing = cap.aCap[existingIndex];
                                    existing.o = (int)outfit;
                                    existing.pos = bNode.position;
                                    existing.rot = Quaternion.LookRotation(facingDirection).eulerAngles;
                                    existing.sp = 1;
                                }
                                else
                                {
                                    SceneRecorder.ActorCapture newCapture = new SceneRecorder.ActorCapture(who, false);
                                    newCapture.o = (int)outfit;
                                    newCapture.pos = bNode.position;
                                    newCapture.rot = Quaternion.LookRotation(facingDirection).eulerAngles;
                                    newCapture.sp = 1;
                                    newCapture.arms = 0;
                                    newCapture.main = 0;

                                    cap.aCap.Add(newCapture);
                                }

                                addedToCaptures.Add(cap); //Add to this set so we don't get multiple copies
                            }
                        }
                        else
                        {
                            //Has the camera skipped this capture? Therefore should we add one?
                            captures = cam.cap.FindAll(item => item.t > backwardsTime - (GameplayControls.Instance.captureInterval * 0.6f) && item.t < backwardsTime + (GameplayControls.Instance.captureInterval * 0.6f) && !addedToCaptures.Contains(item));

                            if(captures == null || captures.Count <= 0)
                            {
                                SceneRecorder.SceneCapture copyCandidate = null;

                                foreach(SceneRecorder.SceneCapture c in cam.cap)
                                {
                                    if (c.t > backwardsTime) continue;

                                    if(copyCandidate == null || c.t > copyCandidate.t)
                                    {
                                        copyCandidate = c;
                                    }
                                }

                                if(copyCandidate != null)
                                {
                                    SceneRecorder.SceneCapture copyCap = new SceneRecorder.SceneCapture(copyCandidate);
                                    copyCap.t = backwardsTime;
                                    copyCap.aCap = new List<SceneRecorder.ActorCapture>();

                                    SceneRecorder.ActorCapture newCapture = new SceneRecorder.ActorCapture(who, false);
                                    newCapture.o = (int)outfit;
                                    newCapture.pos = bNode.position;
                                    newCapture.rot = Quaternion.LookRotation(facingDirection).eulerAngles;
                                    newCapture.sp = 1;
                                    newCapture.arms = 0;
                                    newCapture.main = 0;

                                    copyCap.aCap.Add(newCapture);

                                    cam.cap.Add(copyCap); //Add fake capture to camera's memory
                                    cam.cap.Sort((p1, p2) => p1.t.CompareTo(p2.t)); //Sort captures by time (lowest first)
                                }
                            }
                        }
                    }
                }

                backwardsTime -= 0.005f;
                lastNode = bNode;
            }

            if(addReturnJourney)
            {
                if(returnTo == null)
                {
                    returnTo = routeFrom;
                }

                if(returnTo != routeFrom)
                {
                    path = PathFinder.Instance.GetPath(routeTo, returnTo, who);
                }

                if(path != null)
                {
                    float forwardsTime = arrivalTime + stayTime;
                    addedToCaptures = new HashSet<SceneRecorder.SceneCapture>();
                    NewNode nextNode = null; //Reference to the last node iterated

                    for (int i = path.accessList.Count - 1; i > 0; i--)
                    {
                        NewNode bNode = path.accessList[i].fromNode;
                        if(i < path.accessList.Count - 1) nextNode = path.accessList[i + 1].fromNode;

                        if (nWcameraCoverage.ContainsKey(bNode))
                        {
                            //Calculate the facing direction here...
                            Vector3 facingDirection = Vector3.zero;

                            if(nextNode != null && bNode != null) facingDirection = nextNode.position - bNode.position;
                            facingDirection.y = 0;

                            foreach (Interactable cam in nWcameraCoverage[bNode])
                            {
                                //Are there any captures from around this time?
                                List<SceneRecorder.SceneCapture> captures = cam.cap.FindAll(item => item.t > forwardsTime - 0.0167f && item.t < forwardsTime + 0.0167f && !addedToCaptures.Contains(item));

                                if (captures.Count > 0)
                                {
                                    //Add this to captures...
                                    foreach (SceneRecorder.SceneCapture cap in captures)
                                    {
                                        if (addedToCaptures.Contains(cap)) continue; //This shouldn't happen anyway

                                        //Is the actor here already? If so reposition
                                        int existingIndex = cap.aCap.FindIndex(item => item.id == who.humanID);

                                        if (existingIndex > -1)
                                        {
                                            SceneRecorder.ActorCapture existing = cap.aCap[existingIndex];
                                            existing.o = (int)outfit;
                                            existing.pos = bNode.position;
                                            existing.rot = Quaternion.LookRotation(facingDirection).eulerAngles;
                                            existing.sp = 1;
                                        }
                                        else
                                        {
                                            SceneRecorder.ActorCapture newCapture = new SceneRecorder.ActorCapture(who, false);
                                            newCapture.o = (int)outfit;
                                            newCapture.pos = bNode.position;
                                            newCapture.rot = Quaternion.LookRotation(facingDirection).eulerAngles;
                                            newCapture.sp = 1;
                                            newCapture.arms = 0;
                                            newCapture.main = 0;

                                            cap.aCap.Add(newCapture);
                                        }

                                        addedToCaptures.Add(cap); //Add to this set so we don't get multiple copies
                                    }
                                }
                                else
                                {
                                    //Has the camera skipped this capture? Therefore should we add one?
                                    captures = cam.cap.FindAll(item => item.t > forwardsTime - (GameplayControls.Instance.captureInterval * 0.6f) && item.t < forwardsTime + (GameplayControls.Instance.captureInterval * 0.6f) && !addedToCaptures.Contains(item));

                                    if (captures == null || captures.Count <= 0)
                                    {
                                        SceneRecorder.SceneCapture copyCandidate = null;

                                        foreach (SceneRecorder.SceneCapture c in cam.cap)
                                        {
                                            if (c.t > backwardsTime) continue;

                                            if (copyCandidate == null || c.t > copyCandidate.t)
                                            {
                                                copyCandidate = c;
                                            }
                                        }

                                        if (copyCandidate != null)
                                        {
                                            SceneRecorder.SceneCapture copyCap = new SceneRecorder.SceneCapture(copyCandidate);
                                            copyCap.t = backwardsTime;
                                            copyCap.aCap = new List<SceneRecorder.ActorCapture>();

                                            SceneRecorder.ActorCapture newCapture = new SceneRecorder.ActorCapture(who, false);
                                            newCapture.o = (int)outfit;
                                            newCapture.pos = bNode.position;
                                            newCapture.rot = Quaternion.LookRotation(facingDirection).eulerAngles;
                                            newCapture.sp = 1;
                                            newCapture.arms = 0;
                                            newCapture.main = 0;

                                            copyCap.aCap.Add(newCapture);

                                            cam.cap.Add(copyCap); //Add fake capture to camera's memory
                                            cam.cap.Sort((p1, p2) => p1.t.CompareTo(p2.t)); //Sort captures by time (lowest first)
                                        }
                                    }
                                }
                            }
                        }

                        forwardsTime -= 0.005f;
                    }
                }
                else Game.LogError("Unable to get path for " + routeTo.position + " to " + returnTo.position);
            }
        }
        else
        {
            Game.LogError("Unable to get path for " + routeFrom.position + " to " + routeTo.position);
        }
    }

    public void ExplodeGrenade(Interactable grenade)
    {
        if (grenade == null) return;
        float blastPower = 8.5f;
        float blastRadius = 9.5f;
        float humanDamageMP = 0.33f;

        grenade.UpdateWorldPositionAndNode(true);
        Vector3 grenPos = grenade.GetWorldPosition();
        
        if(grenade.inInventory != null)
        {
            grenPos = grenade.inInventory.transform.position;
        }

        //Blind everybody
        if(grenade.preset == InteriorControls.Instance.activeFlashbomb)
        {
            blastRadius = 15f;
            blastPower = 4f;
            humanDamageMP = 0.05f;

            GameObject newFlash = Instantiate(PrefabControls.Instance.flashBombFlash, PrefabControls.Instance.mapContainer);
            newFlash.transform.position = grenPos;

            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.flashBombDetonate, grenade.belongsTo, grenade.node, grenPos);
        }
        else if(grenade.preset == InteriorControls.Instance.activeIncapacitator)
        {
            blastRadius = 10f;
            blastPower = 8.5f;
            humanDamageMP = 0.66f;

            GameObject newFlash = Instantiate(PrefabControls.Instance.incapacitatorFlash, PrefabControls.Instance.mapContainer);
            newFlash.transform.position = grenPos;

            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.incapacitatorDetonate, grenade.belongsTo, grenade.node, grenPos);
        }

        GameplayController.Instance.activeGrenades.Remove(grenade);

        Dictionary<Human, float> affectedHumans = null;
        Dictionary<NewNode, float> affectedNodes = GetNodeCoverageFromRadius(grenade, blastRadius, out affectedHumans);

        Game.Log("Gameplay: Explode grenade " + grenade.name + " at " + grenade.wPos + ", affecting " + affectedNodes.Count + " nodes & " + affectedHumans.Count + " humans");

        //Affect objects
        foreach(KeyValuePair<NewNode, float> pair in affectedNodes)
        {
            float power = pair.Value * blastPower;

            if(power > 0.2f)
            {
                //Move objects
                foreach(Interactable i in pair.Key.interactables)
                {
                    if (i == grenade) continue;

                    if(i.preset.physicsProfile != null && i.preset.reactWithExternalStimuli && i.inInventory == null)
                    {
                        i.ForcePhysicsActive(true, true, (i.wPos - grenade.wPos).normalized * pair.Value * blastPower);
                    }
                }

                //Break windows
                foreach(NewWall w in pair.Key.walls)
                {
                    foreach (GameObject frontage in w.spawnedFrontage)
                    {
                        BreakableWindowController[] windows = frontage.GetComponentsInChildren<BreakableWindowController>();

                        foreach (BreakableWindowController win in windows)
                        {
                            Vector3 contactPos = win.GetAveragePosition();
                            win.BreakWindow(contactPos, (contactPos - grenade.node.position).normalized * power, null, noDebris: true);
                        }
                    }

                    if(w.otherWall != null)
                    {
                        foreach (GameObject frontage in w.otherWall.spawnedFrontage)
                        {
                            BreakableWindowController[] windows = frontage.GetComponentsInChildren<BreakableWindowController>();

                            foreach (BreakableWindowController win in windows)
                            {
                                Vector3 contactPos = win.GetAveragePosition();
                                win.BreakWindow(contactPos, (contactPos - grenade.node.position).normalized * power, null, noDebris: true);
                            }
                        }
                    }
                }
            }

            //Flash bomb disables security systems
            if (grenade.preset == InteriorControls.Instance.activeFlashbomb)
            {
                foreach(FurnitureLocation f in pair.Key.individualFurniture)
                {
                    foreach(Interactable i in f.integratedInteractables)
                    {
                        if (i.preset.specialCaseFlag == InteractablePreset.SpecialCase.securityCamera || i.preset.specialCaseFlag == InteractablePreset.SpecialCase.sentryGun || i.preset.specialCaseFlag == InteractablePreset.SpecialCase.otherSecuritySystem)
                        {
                            Game.Log("Flashbomb disabled " + i.name);
                            i.SetSwitchState(false, null, forceUpdate: true);
                            i.SetValue(0f);
                        }
                    }
                }
            }
        }

        //Affect humans
        foreach(KeyValuePair<Human, float> pair in affectedHumans)
        {
            if (!pair.Key.isStunned && !pair.Key.isDead)
            {
                Game.Log("Grenade dmg normalized:" + pair.Key.name + " " + pair.Value);

                //Blind everybody
                if (grenade.preset == InteriorControls.Instance.activeFlashbomb)
                {
                    pair.Key.AddBlinded(pair.Value * 10f + 0.1f);
                }

                Vector3 contactPosition = pair.Key.lookAtThisTransform.position;
                pair.Key.RecieveDamage(pair.Value * blastPower * humanDamageMP, grenade.belongsTo, contactPosition, contactPosition - grenade.wPos, null, null, ragdollForceMP: 3);
            }
        }

        grenade.Delete(); //Remove from game world
    }

    //Get node coverage stemming from an origin: Returns a dictionary with normalised proximity to origin
    public Dictionary<NewNode, float> GetNodeCoverageFromRadius(Interactable grenade, float radius, out Dictionary<Human, float> humanOutput)
    {
        Dictionary<NewNode, float> output = new Dictionary<NewNode, float>();
        humanOutput = new Dictionary<Human, float>();

        grenade.UpdateWorldPositionAndNode(true);
        NewNode origin = grenade.node;

        if (grenade.inInventory != null)
        {
            origin = grenade.inInventory.currentNode;

            humanOutput.Add(grenade.inInventory, 1f);
        }

        HashSet<NewNode> openSet = new HashSet<NewNode>();
        openSet.Add(origin);
        output.Add(origin, 1);

        int safety = 200;

        while(openSet.Count > 0 && safety > 0)
        {
            NewNode current = openSet.FirstOrDefault();

            float effect = 0;
            if (current == origin) effect = 1f;
            else effect = Mathf.Clamp01((radius - Vector3.Distance(grenade.wPos, current.position)) / radius);

            if (!output.ContainsKey(current))
            {
                output.Add(current, effect);
            }

            //Calc humans affected
            foreach(Actor occ in current.room.currentOccupants)
            {
                Human hu = occ as Human;

                if (hu != null)
                {
                    float dmg = Mathf.Clamp01((radius - Vector3.Distance(grenade.wPos, hu.transform.position)) / radius);
                    //Game.Log("Grenade: " + hu.name + ": Distance from " + grenade.wPos + " is " + Vector3.Distance(grenade.wPos, hu.transform.position));

                    if (!humanOutput.ContainsKey(hu))
                    {
                        humanOutput.Add(hu, dmg);
                    }
                    else humanOutput[hu] = Mathf.Max(humanOutput[hu], dmg);
                }
            }

            //Game.Log(current.position + " access to other nodes: " + current.accessToOtherNodes.Count);

            foreach (KeyValuePair<NewNode, NewNode.NodeAccess> pair in current.accessToOtherNodes)
            {
                if (openSet.Contains(pair.Key)) continue;
                if (output.ContainsKey(pair.Key)) continue;

                if (Vector3.Distance(pair.Key.position, grenade.wPos) > radius) continue; //Out of radius
                if (!DataRaycastController.Instance.NodeRaycast(origin, pair.Key, out _)) continue; //Must be directly visible

                if(pair.Value.accessType == NewNode.NodeAccess.AccessType.door)
                {
                    if(pair.Value.door != null)
                    {
                        if(!pair.Value.door.isClosed)
                        {
                            openSet.Add(pair.Key);
                        }
                    }
                    else openSet.Add(pair.Key);
                }
                else if(pair.Value.accessType != NewNode.NodeAccess.AccessType.window)
                {
                    openSet.Add(pair.Key);
                }
            }

            openSet.Remove(current);
            safety--;
        }

        return output;
    }

    //Rank a room by how suitable it is for a shady meeting
    public bool RankRoomShadiness(NewRoom room, out float score)
    {
        score = 0;
        if (room == null) return false;
        if (room.nodes.Count <= 0) return false;
        if (room.entrances.Count <= 0) return false;
        if (!room.gameLocation.IsPublicallyOpen(false)) return false;

        score = Toolbox.Instance.Rand(0f, 1f); //Random score to start

        //If this is a street, the lower footfall the better (score out of 10)
        if(room.gameLocation.thisAsStreet != null)
        {
            score += (1f - room.gameLocation.thisAsStreet.normalizedFootfall) * 10f;
        }
        //If this is an address, prioritise basement areas (+5, size +5)
        else
        {
            if (room.floor != null && room.floor.floor < 0)
            {
                score += 5f;
            }

            score += Mathf.Min(room.nodes.Count * 0.3f, 5);
        }

        //Add the shadiness value from the preset /10
        score += room.preset.shadinessValue;

        //The current occupants should give *some* idea of how vacant this is
        score -= room.currentOccupants.Count * 2;

        //The grubbier the better! /10
        score += room.defaultWallKey.grubiness * 10;

        return true;
    }

    //Rank a node by how suitable it is for a shady meeting
    public bool RankNodeShadiness(NewNode node, out float score)
    {
        score = 0;
        if (node.accessToOtherNodes.Count <= 0) return false;

        score = Toolbox.Instance.Rand(0f, 1f); //Random score to start

        //Boost for less furniture
        score += 10 - (node.individualFurniture.Count * 2); //Out of 10

        //Boost for less obects
        score += 10 - node.interactables.Count * 2;

        if (node.allowNewFurniture) score += 5;

        //Boost for walls, not windows
        foreach(NewWall w in node.walls)
        {
            if (w.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance) score -= 10; //Not in an entrance!
            else if (w.preset.sectionClass == DoorPairPreset.WallSectionClass.window) score -= 3; //Not by a window
            else if (w.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge) score -= 6; //Not by a window
            else
            {
                score += 1;
            }
        }

        return true;
    }

    public void TriggerBriefcaseBomb(Interactable briefcase)
    {
        if (briefcase == null) return;

        //Place on actor
        Interactable activeTracker = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.activeIncapacitator, Player.Instance, null, null, Player.Instance.transform.position + new Vector3(0, 3.5f, 0), Vector3.zero, null, null);

        if (activeTracker != null)
        {
            activeTracker.SetInInventory(Player.Instance);
            activeTracker.SetCustomState2(true, Player.Instance, forceUpdate: true); //Use custom switch 2 to arm the grenade
            activeTracker.SetValue(GameplayControls.Instance.thrownGrenadeFuse * 0.5f);
            //FirstPersonItemController.Instance.EmptySlot(BioScreenController.Instance.selectedSlot, false, true, playSound: false);
        }
    }

    //Returns mailbox door object
    public Interactable GetMailbox(Human forHuman)
    {
        if (forHuman != null)
        {
            if (forHuman.home != null && forHuman.home.residence != null)
            {
                if (forHuman.home.residence.mailbox != null)
                {
                    foreach(KeyValuePair<FurnitureLocation.OwnerKey, int> pair in forHuman.home.residence.mailbox.ownerMap)
                    {
                        if(pair.Key.human == forHuman)
                        {
                            Interactable mailBoxDoor = forHuman.home.residence.mailbox.integratedInteractables.Find(item => item.objectRef != null && (item.objectRef as NewAddress) == forHuman.home);
                            if (mailBoxDoor != null) return mailBoxDoor;
                        }
                        else if(pair.Key.address == forHuman.home)
                        {
                            Interactable mailBoxDoor = forHuman.home.residence.mailbox.integratedInteractables.Find(item => item.objectRef != null && (item.objectRef as NewAddress) == forHuman.home);
                            if (mailBoxDoor != null) return mailBoxDoor;
                        }
                    }
                }
                else Game.LogError("Unable to locate mailbox: " + forHuman.name);
            }
            else Game.LogError("Unable to locate home: " + forHuman.name);
        }
        else Game.LogError(" Unable to get person!");

        return null;
    }

    //Returns whether the storyline mission is running, and if so output the chapter details
    public bool IsStoryMissionActive(out Chapter script, out int chapter)
    {
        chapter = -1;
        script = null;

        if (Game.Instance.sandboxMode) return false;

        if(ChapterController.Instance != null)
        {
            if(ChapterController.Instance.chapterScript != null)
            {
                script = ChapterController.Instance.chapterScript;

                ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                if(intro != null)
                {
                    if (intro.completed) return false;
                    else
                    {
                        chapter = ChapterController.Instance.currentPart;
                        return true;
                    }
                }
            }
        }

        return false;
    }

    //Return a share code from input data
    public string GetShareCode(string cityName, int citySize, string version, string seed)
    {
        return RemoveCharacters(cityName, true, false, true, false) + "." + citySize + "." + VersionToNumbers(version) + "." + RemoveCharacters(seed, true, false, true, false);
    }

    public string GetShareCode(ref CitySaveData cityData)
    {
        if (cityData == null) return string.Empty;
        return GetShareCode(cityData.cityName, (int)cityData.citySize.x, (int)cityData.citySize.y, cityData.build, cityData.seed);
    }

    public string GetShareCode(string cityName, int citySizeX, int citySizeY, string version, string seed)
    {
        int sizeIndex = CityControls.Instance.citySizes.FindIndex(item => Mathf.RoundToInt(item.v2.x) == citySizeX && Mathf.RoundToInt(item.v2.y) == citySizeY);
        if (sizeIndex < 0) sizeIndex = 0;

        return RemoveCharacters(cityName, true, false, true, false) + "." + sizeIndex + "." + VersionToNumbers(version) + "." + RemoveCharacters(seed, true, false, true, false);
    }

    //Parse data from a share code, make sure it's legal
    //New share code: CityName.Size.Version + Params (',' separated).Seed
    public void ParseShareCode(string input, out string cityName, out int citySizeX, out int citySizeY, out string version, out string seed)
    {
        string[] elements = input.Split('.');
        cityName = string.Empty;
        citySizeX = 5;
        citySizeY = 5;
        version = string.Empty;
        seed = string.Empty;

        //Parse name
        if (elements.Length > 0)
        {
            cityName = RemoveCharacters(elements[0], true, false, true, false);
        }

        //Parse size
        if (elements.Length > 1)
        {
            //Force small cities
            if(Game.Instance.smallCitiesOnly)
            {
                citySizeX = 5;
                citySizeY = 5;
            }
            else
            {
                int sizeVal = 0;

                int.TryParse(elements[1], out sizeVal);
                Vector2 s = GetCitySizeFromValue(sizeVal);
                citySizeX = Mathf.RoundToInt(s.x);
                citySizeY = Mathf.RoundToInt(s.y);
            }
        }

        //Parse version and extra params
        if (elements.Length > 2)
        {
            int v = 0;
            int.TryParse(elements[2], out v);
            version = NumbersToVersion(v);
        }

        //Parse seed
        if (elements.Length > 3)
        {
            seed = RemoveCharacters(elements[3], true, false, true, false);
        }

        //If no seed found, then generate one
        if (seed == null || seed.Length <= 0)
        {
            seed = Toolbox.Instance.GenerateSeed(16);
        }
    }

    //Convert version to numbers
    public int VersionToNumbers(string version)
    {
        int ret = 0;

        string[] split = version.Split('.');

        string reComp = string.Empty;

        foreach(string str in split)
        {
            reComp += str;
        }

        int.TryParse(reComp, out ret);
        //Game.Log("Version to numbers: " + version + " = " + ret);

        return ret;
    }

    public string NumbersToVersion(int numbers)
    {
        string ret = numbers.ToString();

        if(ret.Length > 2)
        {
            ret = ret.Substring(0, 2) + "." + ret.Substring(2);
        }

        //Game.Log("Numbers to version: " + numbers + " = " + ret);

        return ret;
    }

    //Get city size from option value
    public Vector2 GetCitySizeFromValue(int val)
    {
        if(val < CityControls.Instance.citySizes.Count)
        {
            return CityControls.Instance.citySizes[val].v2;
        }
        return new Vector2(5, 5);
    }

    //Remove special characters from a string
    public string RemoveCharacters(string input, bool removeSpecialCharacters, bool removeNumbers, bool removeDots, bool removeSpaces)
    {
        string ret = input;

        if (removeSpecialCharacters)
        {
            //Only allowed: Letters, numbers, dots, white space
            ret = Regex.Replace(ret, "[^a-zA-Z0-9.\\-\\s]+", string.Empty, RegexOptions.Compiled);
            //Game.Log("Removing special characters from " + input + " = " + ret);
        }

        if (removeNumbers)
        {
            ret = Regex.Replace(ret, @"[\d-]", string.Empty, RegexOptions.Compiled);
            //Game.Log("Removing numbers from " + input + " = " + ret);
        }

        if(removeDots)
        {
            ret = Regex.Replace(ret, @"\.+", string.Empty, RegexOptions.Compiled);
            //Game.Log("Removing dots from " + input + " = " + ret);
        }

        if(removeSpaces)
        {
            ret = Regex.Replace(ret, "\\s+", string.Empty, RegexOptions.Compiled);
            //Game.Log("Removing spaces from " + input + " = " + ret);
        }

        return ret;
    }

    //Converts a vector int to a regular float vector
    public Vector3 ToVector3(Vector3Int input)
    {
        return new Vector3(input.x, input.y, input.z);
    }

    //Converts a vector int to a regular float vector
    public Unity.Mathematics.float3 ToFloat3(Vector3Int input)
    {
        return new Unity.Mathematics.float3(input.x, input.y, input.z);
    }

    //Converts a vector int to a regular float vector
    public Vector2 ToVector2(Vector2Int input)
    {
        return new Vector2(input.x, input.y);
    }

    //Get the correct likely hotel room reference for this person
    public GameplayController.HotelGuest GetHotelRoom(Human person)
    {
        if (person == null) return null;

        List<GameplayController.HotelGuest> guests = GameplayController.Instance.hotelGuests.FindAll(item => item.humanID == person.humanID);

        if (guests.Count > 0)
        {
            GameplayController.HotelGuest foundGuest = guests[0]; //Default to index 0

            //We could be referring to another hotel room, so look for additional input...
            if (guests.Count > 1)
            {
                foreach (GameplayController.HotelGuest g in guests)
                {
                    NewAddress thisRoom = g.GetAddress();

                    //This isn't an ideal solution but if this person works in a hotel, probably bias towards that...
                    if (person.job != null && person.job.employer != null && thisRoom != null)
                    {
                        if (person.job.employer.address != null && person.job.employer.address.building == thisRoom.building)
                        {
                            foundGuest = g;
                            break;
                        }
                    }

                    //Otherwise use location
                    if (person.currentBuilding == thisRoom.building)
                    {
                        foundGuest = g;
                        break;
                    }
                }
            }

            return foundGuest;
        }

        return null;
    }
}
