﻿using UnityEngine;
using System.Collections;

//Controls the windowed sub camera
//Script pass 1
public class WindowCameraController : MonoBehaviour
{
	//References
	public GameObject cameraObj;
	private Camera cam;
	public GameObject followObj;

	//Default (starting values) for the camera postion.
	public Vector2 camHeightLimit = new Vector2(1f, 20f);
	public Vector3 camPos = new Vector3(0f, 2f, 0f);

    public bool isActive = false;

	// Use this for initialization
	void Awake()
	{
		cam = cameraObj.GetComponent<Camera>();
	}

	void Start ()
	{
		//Set desired cam height to actual cam height
		camPos = cameraObj.transform.position;
        //UpdateCamPos();
	}

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            //Use the follow object's position for X & Z
            Vector3 objPos = followObj.transform.position;

            camPos.x = objPos.x;
            camPos.z = objPos.z;
            camPos.y = 2f;

            UpdateCamPos();
        }
	}

	//Call to update the camera position.
	void UpdateCamPos ()
	{
		//Clamp position to boundaries
		cameraObj.transform.position = new Vector3
			(
				Mathf.Clamp (camPos.x, CityData.Instance.boundaryLeft, CityData.Instance.boundaryRight), 
				Mathf.Clamp (camPos.y, camHeightLimit.x, camHeightLimit.y),
				Mathf.Clamp (camPos.z, CityData.Instance.boundaryDown, CityData.Instance.boundaryUp)				
			);

        cam.orthographicSize = Mathf.Clamp(camPos.y, camHeightLimit.x, camHeightLimit.y);

    }
}
