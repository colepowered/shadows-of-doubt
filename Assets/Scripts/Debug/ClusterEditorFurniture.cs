using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class ClusterEditorFurniture : MonoBehaviour
{
    public FurniturePreset furnPreset;
    public FurnitureClass furnClass;

    public void Setup(FurniturePreset newFurn)
    {
        furnPreset = newFurn;
        furnClass = furnPreset.classes[0];
    }
}
