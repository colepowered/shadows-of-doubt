﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactCustom : Fact
{

    //Constructor
    public FactCustom(FactPreset newPreset, List<Evidence> newFromEvidence, List<Evidence> newToEvidence, List<object> newPassedObjects, List<Evidence.DataKey> overrideFromKeys, List<Evidence.DataKey> overrideToKeys, bool isCustomFact) : base(newPreset, newFromEvidence, newToEvidence, newPassedObjects, overrideFromKeys, overrideToKeys, isCustomFact)
    {
        SetCustomName(Strings.Get("evidence.generic", preset.name));
    }

    public override void SetCustomName(string str)
    {
        base.SetCustomName(str);
        GenerateName();
    }

    //Generate real/object name
    public override string GenerateName()
    {
        return customName;
    }

    //Get name from the connecting evidence. Optional: Get name for a specific link
    public override string GetName(Evidence.FactLink specificLink = null)
    {
        return customName;
    }
}
