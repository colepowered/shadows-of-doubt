﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEditor;

public class SalesRecordsApp : CruncherAppContent
{
    [Header("Components")]
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI displayText;
    public ComputerOSMultiSelect list;
    public RectTransform printButton;

    [Header("State")]
    public InteractablePreset ddsPrintout;

    public enum CitizenPool { allCitizens, companyOnly, buildingOnly };

    //Called on setup
    public override void OnSetup()
    {
        list.OnNewSelection += UpdateSelected;
        list.OnChangePage += OnChangePage;
        UpdateEntries();
        OnChangePage();
        UpdateSelected();
    }

    private void OnDestroy()
    {
        list.OnNewSelection -= UpdateSelected;
        list.OnChangePage -= OnChangePage;
    }

    public void UpdateEntries()
    {
        List<ComputerOSMultiSelect.OSMultiOption> newOptions = new List<ComputerOSMultiSelect.OSMultiOption>();

        try
        {
            foreach (Company.SalesRecord c in controller.ic.interactable.node.gameLocation.thisAsAddress.company.sales)
            {
                ComputerOSMultiSelect.OSMultiOption newOption = new ComputerOSMultiSelect.OSMultiOption();
                newOption.salesRecord = c;

                newOptions.Add(newOption);
            }
        }
        catch
        {
            Game.LogError("Unable to update sales record entries!");
        }

        list.UpdateElements(newOptions);
        UpdateSelected();
    }

    public void OnChangePage()
    {
        displayText.text = Strings.Get("computer", "Displaying page") + " " + (list.page + 1) + "/" + (Mathf.FloorToInt(list.allOptions.Count / list.maxPerPage) + 1);
    }

    public void UpdateSelected()
    {
        if(list.selected != null)
        {
            printButton.gameObject.SetActive(true);
        }
        else
        {
            printButton.gameObject.SetActive(false);
        }
    }
    public void ExitButton()
    {
        controller.OnAppExit();
    }

    public void OnPrintEntry()
    {
        if (list.selected != null)
        {
            Game.Log("Print " + list.selected);

            controller.SetTimedLoading(Toolbox.Instance.Rand(0.5f, 1f));

            if (controller.printedDocument == null && controller.printTimer <= 0f && list.selected != null && list.selected.option != null && list.selected.option.salesRecord != null)
            {
                controller.printTimer = 1f;
                controller.printerParent.localPosition = new Vector3(controller.printerParent.localPosition.x, controller.printerParent.localPosition.y, -0.05f); //Move printer paren back

                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerPrint, Player.Instance, controller.ic.interactable.node, controller.ic.interactable.wPos);

                //Create receipt
                List<Interactable.Passed> passed = new List<Interactable.Passed>();
                passed.Add(new Interactable.Passed(Interactable.PassedVarType.companyID, list.selected.option.salesRecord.companyID));
                passed.Add(new Interactable.Passed(Interactable.PassedVarType.time, list.selected.option.salesRecord.time));

                for (int i = 0; i < list.selected.option.salesRecord.items.Count; i++)
                {
                    passed.Add(new Interactable.Passed(Interactable.PassedVarType.stringInteractablePreset, -1, list.selected.option.salesRecord.items[i]));
                }

                controller.printedDocument = InteractableCreator.Instance.CreateWorldInteractable(ddsPrintout, Player.Instance, list.selected.option.salesRecord.GetPunter(), list.selected.option.salesRecord.GetPunter(), controller.printerParent.position, controller.ic.transform.eulerAngles, passed, null);
                if (controller.printedDocument != null) controller.printedDocument.MarkAsTrash(true);

                //Listen for on remove...
                controller.printedDocument.OnRemovedFromWorld += OnPlayerTakePrint;
            }
            else AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerInvalidPasscode, Player.Instance, controller.ic.interactable.node, controller.ic.interactable.wPos);
        }
        else Game.Log("No selected entry!");
    }
}
