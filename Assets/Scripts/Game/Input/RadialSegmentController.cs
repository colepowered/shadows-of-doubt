﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using TMPro;

public class RadialSegmentController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public RectTransform segmentLineRect;
    public RectTransform elementLineRect;
    public RectTransform elementRect;
    public RectTransform stolenIcon;
    public TextMeshProUGUI text;
    public Image img;
    public List<CanvasRenderer> renderers = new List<CanvasRenderer>();

    [Header("Inventory")]
    public FirstPersonItemController.InventorySlot slot;

    [Header("Calculations")]
    [ReadOnly]
    [Tooltip("The space each segment takes up")]
    public float segmentAngleSpace;
    [ReadOnly]
    [Tooltip("The anlge of this slot")]
    public float angle;
    [ReadOnly]
    public float toAngle;

    public void UpdateSegment(FirstPersonItemController.InventorySlot newSlot)
    {
        slot = newSlot;

        //Calculate the space this takes up
        segmentAngleSpace = 360f / FirstPersonItemController.Instance.slots.Count;

        angle = slot.index * segmentAngleSpace;
        toAngle = angle + segmentAngleSpace;

        //Position line @ angle
        segmentLineRect.localEulerAngles = new Vector3(0, 0, angle);
        elementLineRect.localEulerAngles = new Vector3(0, 0, angle + (segmentAngleSpace * 0.5f));

        elementRect.eulerAngles = Vector3.zero;

        //elementRect.position = elementLineRect.TransformPoint(new Vector3(elementLineRect.sizeDelta.x, 0, 0));
    }

    public void OnUpdateContent()
    {
        stolenIcon.gameObject.SetActive(false); //Stolen icon disabled by default

        if(slot.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic)
        {
            Interactable slotInteractable = slot.GetInteractable();

            if (slotInteractable == null)
            {
                text.text = Strings.Get("ui.interface", "Empty", Strings.Casing.firstLetterCaptial);
                img.sprite = InterfaceControls.Instance.iconReference.Find(item => item.iconType == InterfaceControls.Icon.empty).sprite;
            }
            else
            {
                text.text = slotInteractable.GetName();

                if (slotInteractable.preset.iconOverride != null)
                {
                    img.sprite = slotInteractable.preset.iconOverride;
                }
                else if (slotInteractable.evidence != null)
                {
                    img.sprite = slotInteractable.evidence.preset.iconSpriteLarge;
                }
                else
                {
                    img.sprite = InterfaceControls.Instance.iconReference.Find(item => item.iconType == InterfaceControls.Icon.hand).sprite;
                }

                //Is stolen?
                if(StatusController.Instance.activeFineRecords.Exists(item => item.crime == StatusController.CrimeType.theft && item.objectID == slotInteractable.id))
                {
                    stolenIcon.gameObject.SetActive(true);
                }
            }
        }
        else
        {
            FirstPersonItem slotFPSItem = slot.GetFirstPersonItem();

            if(slotFPSItem != null)
            {
                text.text = Strings.Get("evidence.names", slotFPSItem.name, Strings.Casing.firstLetterCaptial);
                img.sprite = slotFPSItem.selectionIcon;
            }
            else
            {
                text.text = Strings.Get("ui.interface", "nothing", Strings.Casing.firstLetterCaptial);
                img.sprite = InterfaceControls.Instance.iconReference.Find(item => item.iconType == InterfaceControls.Icon.empty).sprite;
            }
        }
    }
}
