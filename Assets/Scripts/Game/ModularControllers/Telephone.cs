using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[System.Serializable]
public class Telephone
{
    [Header("Serialized Components")]
    public int number;
    public string numberString;
    public List<TelephoneController.PhoneCall> activeCall = new List<TelephoneController.PhoneCall>();

    [Header("Non-Serialized Components")]
    [System.NonSerialized]
    public bool setup = false; //As this is serializable, check against this flag to see if this class has been set up
    [System.NonSerialized]
    public NewGameLocation location;
    [System.NonSerialized]
    public Interactable interactable;
    [System.NonSerialized]
    public SpeechController speechController;
    [System.NonSerialized]
    public Human activeReceiver = null; //True if somebody has picked up the phone
    [System.NonSerialized]
    public AudioController.LoopingSoundInfo dialTone;
    [System.NonSerialized]
    public FMOD.Studio.EventInstance engaged;
    [System.NonSerialized]
    public EvidenceLocation locationEntry;
    [System.NonSerialized]
    public EvidenceTelephone telephoneEntry;

    //Create new telephone
    public Telephone(Interactable newTelephone)
    {
        interactable = newTelephone;
        location = interactable.node.gameLocation;
        if(!location.telephones.Contains(this)) location.telephones.Add(this);
        locationEntry = interactable.node.gameLocation.evidenceEntry;

        //Generate a new telephone numbers
        GenerateTelephoneNumber();
        CreateEvidence();

        setup = true;
    }

    //Load in an existing number
    public Telephone(Interactable newTelephone, int newNumber)
    {
        interactable = newTelephone;
        number = newNumber;

        location = interactable.node.gameLocation;
        if (!location.telephones.Contains(this)) location.telephones.Add(this);
        locationEntry = interactable.node.gameLocation.evidenceEntry;

        LoadTelephoneNumber();
        CreateEvidence();

        setup = true;
    }

    //Load a telephone number from serialized data
    public void LoadTelephoneNumber()
    {
        if (SessionData.Instance.isFloorEdit) return;

        if(!CityData.Instance.phoneDictionary.ContainsKey(number))
        {
            CityData.Instance.phoneDictionary.Add(number, this);
        }
        
        numberString = Toolbox.Instance.GetTelephoneNumberString(number);
    }

    //Assign telephone number
    public void GenerateTelephoneNumber()
    {
        if (SessionData.Instance.isFloorEdit) return;

        //Regular phone numbers start with digits 1 - 7.
        number = Toolbox.Instance.GetPsuedoRandomNumber(1000000, 6999000, location.GetReplicableSeed());

        //Add one to find a unique number
        while(CityData.Instance.phoneDictionary.ContainsKey(number) || TelephoneController.Instance.fakeTelephoneDictionary.ContainsKey(number))
        {
            number++;
        }

        CityData.Instance.phoneDictionary.Add(number, this);
        numberString = Toolbox.Instance.GetTelephoneNumberString(number);
    }

    public List<int> GetInputCode()
    {
        List<int> ret = new List<int>();
        string n = number.ToString();

        for (int i = 0; i < n.Length; i++)
        {
            int integer = 0;
            int.TryParse(n[i].ToString(), out integer);
            ret.Add(integer);
        }

        return ret;
    }

    //Create evidence
    public void CreateEvidence()
    {
        List<object> passed = new List<object>();
        passed.Add(this);
        telephoneEntry = EvidenceCreator.Instance.CreateEvidence("TelephoneNumber", "TelephoneNumber" + number, location, newParent: location.evidenceEntry, passedObjects: passed) as EvidenceTelephone;

        Fact phoneNumberFact = EvidenceCreator.Instance.CreateFact("IsTelephoneNumberFor", telephoneEntry, locationEntry);
    }

    public void StopActiveCall()
    {
        //End call
        if (activeCall.Count > 0)
        {
            //Game.Log("End call");
            TelephoneController.PhoneCall call = activeCall[0];
            activeCall[0].EndCall();

            if (call.fromNS != null)
            {
                call.fromNS.activeCall.Clear();
                if (call.fromNS.interactable != null) call.fromNS.interactable.SetCustomState3(false, null);
            }

            if (call.toNS != null)
            {
                call.toNS.activeCall.Clear();
                if(call.toNS.interactable != null) call.toNS.interactable.SetCustomState3(false, null);
            }

            //Set telephone's custom state 3
            interactable.SetCustomState3(false, null);
        }
    }

    public void SetActiveCall(TelephoneController.PhoneCall newCall)
    {
        if (newCall == null || newCall.fromNS == null) return; //Passing an invalid call...

        if (activeCall.Count <= 0 || (activeCall.Count > 0 && activeCall[0] != newCall))
        {
            //Stop existing call...
            if (activeCall.Count > 0)
            {
                StopActiveCall();
            }

            Game.Log("Phone: Adding new active call: " + newCall);
            activeCall.Add(newCall);

            if (activeCall.Count > 0)
            {
                if (dialTone != null)
                {
                    AudioController.Instance.StopSound(dialTone, AudioController.StopType.immediate, "call while picked up"); //Stop dial tone
                    dialTone = null;
                }

                //Set telephone's custom state 3
                interactable.SetCustomState3(true, null);

                //Is this already answered?
                if (activeReceiver != null && activeCall[0].toNS == this)
                {
                    //Set the active reciever...
                    if (activeCall[0].toNS == this)
                    {
                        activeCall[0].recevierNS = activeReceiver;
                        activeCall[0].receiver = activeReceiver.humanID;
                    }

                    activeCall[0].SetCallState(TelephoneController.CallState.started);
                }
                //Start outgoing active call right away...
                else if(activeCall[0].source.callType == TelephoneController.CallType.fakeOutbound)
                {
                    activeCall[0].SetCallState(TelephoneController.CallState.started);
                }
                else
                {
                    activeCall[0].SetCallState(TelephoneController.CallState.ringing);
                }
            }
        }
    }

    public void SetTelephoneAnswered(Human val)
    {
        if (activeReceiver != val)
        {
            //Don't replace an active reciever
            if (activeReceiver != null && val != null) return;

            activeReceiver = val;

            //Set telephone usage manually here
            interactable.usagePoint.TrySetUser(Interactable.UsePointSlot.defaultSlot, activeReceiver);

            //Hang up
            if (activeReceiver == null)
            {
                Game.Log("Phone: Telephone " + numberString + " hung up.");

                if (dialTone != null)
                {
                    AudioController.Instance.StopSound(dialTone, AudioController.StopType.immediate, "phone hang up"); //Stop dial tone
                    dialTone = null;
                }

                StopActiveCall();

                //Stop all other sounds...
                AudioController.Instance.StopSound(engaged, AudioController.StopType.immediate);
                //AudioController.Instance.StopSound(connecting, AudioController.StopType.immediate);
                //AudioController.Instance.StopSound(hangup, AudioController.StopType.immediate);
            }
            else
            {
                Game.Log("Phone: Telephone " + numberString + " new active receiver: " + activeReceiver);

                //Answer call
                if (activeCall.Count > 0)
                {
                    //Set the active reciever...
                    if (activeCall[0].toNS == this)
                    {
                        activeCall[0].recevierNS = activeReceiver;
                        activeCall[0].receiver = activeReceiver.humanID;
                        activeCall[0].SetCallState(TelephoneController.CallState.started);
                    }
                    else if (activeCall[0].fromNS == this)
                    {
                        if (activeCall[0].toNS != null && activeCall[0].toNS.activeReceiver != null)
                        {
                            activeCall[0].SetCallState(TelephoneController.CallState.started);
                        }
                    }
                }
                else
                {
                    if(interactable != null && interactable.sw3)
                    {
                        interactable.SetCustomState3(false, null, false);
                    }

                    //Play dial tone
                    if (activeReceiver.isPlayer)
                    {
                        if (dialTone == null)
                        {
                            Game.Log("Play dial tone...");

                            if (!Player.Instance.phoneInteractable.sw2)
                            {
                                dialTone = AudioController.Instance.PlayWorldLooping(AudioControls.Instance.dialTone, activeReceiver, activeReceiver.interactable);
                            }
                        }
                    }
                }
            }
        }
    }
}
