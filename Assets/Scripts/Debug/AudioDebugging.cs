using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class AudioDebugging : MonoBehaviour
{
    [Header("Debug Controls")]
    public bool overrideSmokeStackEmissionFrequency = false;
    [InfoBox("Controls how often the chem plant smoke plumes appear in in-game minutes (roughly). This will only take effect after the last plume.")]
    [EnableIf("overrideSmokeStackEmissionFrequency")]
    public float chemSmokeStackEmissionFrequency = 4f;

    [Space(7)]
    public bool overrideThunderDelay = false;
    [InfoBox("Controls how often thunder happens in storms")]
    [EnableIf("overrideThunderDelay")]
    public float thunderDelay = 4f;
    [InfoBox("The distance threshold at which the ThunderDistance param passes 1 instead of 0 (1 = 1m) 2D distance with world height not taken into account")]
    public float thunderDistanceThreshold = 40f;

    [Space(7)]
    [InfoBox("At what point in the closing door animation does the closeDoor event trigger? 0 = completely closed, 1 = completely open")]
    public float doorCloseTriggerPoint = 0.1f;

    [Space(7)]
    [InfoBox("A multiplier that controls how far a citizen moves before creating a footstep sound & footprint")]
    public float citizenFootstepDistanceMultiplier = 1f;

    [Header("Object Spawn")]
    [InfoBox("Spawn an object infront of the player by choosing a config using the below, then use the spawn object button.")]
    public InteractablePreset spawnObject;


    //Singleton pattern
    private static AudioDebugging _instance;
    public static AudioDebugging Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    [Button]
    public void SpawnObject()
    {
        if(spawnObject != null)
        {
            if (spawnObject.spawnable && spawnObject.prefab != null)
            {
                Interactable newObj = InteractableCreator.Instance.CreateWorldInteractable(spawnObject, Player.Instance, null, null, Player.Instance.transform.position + new Vector3(Toolbox.Instance.Rand(-1f, 1f), 0, Toolbox.Instance.Rand(-1f, 1f)), Vector3.zero, null, null);
                if (newObj != null) newObj.ForcePhysicsActive(false, false);
            }
            else Game.Log("Unable to spawn object: This object is not spawnable!");
        }
        else Game.Log("Unable to spawn object: Nothing selected!");
    }
}
