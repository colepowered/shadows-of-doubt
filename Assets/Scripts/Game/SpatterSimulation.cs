﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

[System.Serializable]
public class SpatterSimulation
{
    [Header("Serialized References")]
    public Vector3 worldOrigin;
    public Vector3 worldTarget;
    public Vector3Int nodeCoord;
    public string presetStr;
    public int roomID = -1;
    public EraseMode eraseMode = EraseMode.useDespawnTimeOnceExecuted;
    public ForceType force;
    public float spatterCountMultiplier = 1f;
    public float createdAt = 0f; //Time stamp of creation
    public bool isExecuted = false; //True after execution (room must be loaded)
    public float executedAt = 0f; //Time stamp for execution
    public float eraseModeTimeStamp = -1f; //Used for when the player is out of the building/address
    public bool stickToActors = true;
    public List<DecalSpawnData> decalsSpawned = new List<DecalSpawnData>();

    [Header("Non-Serialized References")]
    [System.NonSerialized]
    public NewRoom room;
    [System.NonSerialized]
    public SpatterPatternPreset preset;

    public enum EraseMode { neverOrManual, onceExecutedAndOutOfBuildingPlusDespawnTime, onceExecutedAndOutOfAddressPlusDespawnTime, useDespawnTime, useDespawnTimeOnceExecuted};

    //Presets for blood values
    public enum ForceType { bulletForward, bulletBack, punch};

    [System.Serializable]
    public class DecalSpawnData
    {
        public ParentID parentID;
        public int transformParentID = -1;
        public string subObjectName;

        public Vector3 worldPos;
        public Vector3 worldEuler;
        public Vector3 size;
        public DecalMaterialType materialType;

        [System.NonSerialized]
        public DecalProjector spawnedProjector;
        [System.NonSerialized]
        public Interactable i; //Save this reference to save on searching when spawn update is refreshed
        [System.NonSerialized]
        public SpatterSimulation sim;

        private const int INITIAL_POOL_SIZE = 30;
        private const float RECYCLED_Y_POSITION = -1000f;
        [System.NonSerialized]
        private static Queue<DecalProjector> decalPool = new Queue<DecalProjector>();

        public void SpawnOnTransform(Transform spawnTransform)
        {
            if(spawnedProjector == null && spawnTransform != null)
            {
                spawnedProjector = GetNewDecalProjector();

                if(spawnedProjector != null)
                {
                    spawnedProjector.transform.SetParent(spawnTransform);

                    spawnedProjector.size = size;
                    spawnedProjector.gameObject.transform.eulerAngles = worldEuler;
                    spawnedProjector.gameObject.transform.position = worldPos;

                    //Apply opacity material
                    if (materialType == DecalMaterialType.heavy)
                    {
                        spawnedProjector.material = sim.preset.heavyMaterial;
                    }
                    else if (materialType == DecalMaterialType.medium)
                    {
                        spawnedProjector.material = sim.preset.mediumMaterial;
                    }
                    else
                    {
                        spawnedProjector.material = sim.preset.lightMaterial;
                    }
                }
            }
        }

        public static void InitialisePool()
        {
            for (int i = 0; i < INITIAL_POOL_SIZE; i++)
            {
                DecalProjector decalProjector = Toolbox.Instance.SpawnObject(PrefabControls.Instance.spatterSimulation, null).GetComponent<DecalProjector>();
#if UNITY_EDITOR
                decalProjector.transform.SetParent(Toolbox.PoolingGroup.transform);
#else
                decalProjector.transform.SetParent(null);
#endif
                decalProjector.transform.position = new Vector3(0, RECYCLED_Y_POSITION, 0);
                decalPool.Enqueue(decalProjector);
            }
        }

        public static DecalProjector GetNewDecalProjector()
        {
            if (decalPool.Count > 0)
            {
                DecalProjector decalProjector = decalPool.Dequeue();
                return decalProjector;
            }
            else
            {
                DecalProjector decalProjector = Toolbox.Instance.SpawnObject(PrefabControls.Instance.spatterSimulation, null).GetComponent<DecalProjector>();
#if UNITY_EDITOR
                decalProjector.transform.SetParent(Toolbox.PoolingGroup.transform);
#else
                decalProjector.transform.SetParent(null);
#endif
                return decalProjector;
            }
        }

        public static void RecycleDecalProjector(DecalProjector decalProjector)
        {
#if UNITY_EDITOR
            decalProjector.transform.SetParent(Toolbox.PoolingGroup.transform);
#else
            decalProjector.transform.SetParent(null);
#endif
            decalProjector.transform.position = new Vector3(0, RECYCLED_Y_POSITION, 0);
            decalPool.Enqueue(decalProjector);
        }
    }

    public enum DecalMaterialType { light, medium, heavy};

    public enum ParentID { room, human, interactable, door};

    public SpatterSimulation (Human newHuman, Vector3 newLocalPosition, Vector3 newDirection, SpatterPatternPreset spatter, EraseMode newEraseMode, float newSpatterCountMultiplier = 1f, bool newStickToActors = true)
    {
        preset = spatter;
        presetStr = spatter.name;
        //humanID = newHuman.humanID;
        //localPosition = newLocalPosition;
        //localDirection = newDirection;
        eraseMode = newEraseMode;
        eraseModeTimeStamp = -1f; //Make sure this is set to negative
        spatterCountMultiplier = newSpatterCountMultiplier;
        stickToActors = newStickToActors;

        createdAt = SessionData.Instance.gameTime;

        //Calculate word position + direction
        worldOrigin = newHuman.transform.TransformPoint(newLocalPosition);
        worldTarget = newHuman.transform.TransformPoint(newLocalPosition + newDirection);

        nodeCoord = CityData.Instance.RealPosToNodeInt(worldOrigin);
        NewNode node = null;

        if(!PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out node))
        {
            node = newHuman.currentNode; //If we can't find a node through the world coordinate, use the human's current node
        }

        if (node == null) return;

        room = node.room;
        roomID = room.roomID;

        room.spatter.Add(this); //Add to room
        GameplayController.Instance.spatter.Add(this);

        //If room is loaded already then execute
        if(room.geometryLoaded)
        {
            Execute();
        }
    }

    public SpatterSimulation (Vector3 newWorldPosition, Vector3 newWorldTarget, SpatterPatternPreset spatter, EraseMode newEraseMode, float newSpatterCountMultiplier = 1f, bool newStickToActors = true)
    {
        preset = spatter;
        presetStr = spatter.name;
        eraseMode = newEraseMode;
        eraseModeTimeStamp = -1f; //Make sure this is set to negative
        spatterCountMultiplier = newSpatterCountMultiplier;
        stickToActors = newStickToActors;

        createdAt = SessionData.Instance.gameTime;

        //Calculate word position + direction
        worldOrigin = newWorldPosition;
        worldTarget = newWorldTarget;

        nodeCoord = CityData.Instance.RealPosToNodeInt(worldOrigin);
        NewNode node = null;

        if(!PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out node))
        {
            return;
        }

        room = node.room;
        roomID = room.roomID;

        room.spatter.Add(this); //Add to room
        GameplayController.Instance.spatter.Add(this);

        //If room is loaded already then execute
        if (room.geometryLoaded)
        {
            Execute();
        }
    }

    public void Execute()
    {
        isExecuted = true;
        executedAt = SessionData.Instance.gameTime;

        int rayCount = Mathf.RoundToInt(preset.spatterCount * spatterCountMultiplier);
        AnimationCurve coneCurve = preset.spreadCurve;
        Vector2 rayRanges = preset.rayLength;
        //0.1 = Walls/floor scale
        //0.05 = Furniture scale
        //0.01 = Object scale

        for (int i = 0; i < rayCount; i++)
        {
            //Apply the direction to the original position
            float angleX = coneCurve.Evaluate(Toolbox.Instance.Rand(0f, 1f)) * preset.maxAngleX;
            float angleY = coneCurve.Evaluate(Toolbox.Instance.Rand(0f, 1f)) * preset.maxAngleY;

            Vector3 randomTarget = worldTarget + new Vector3(Toolbox.Instance.Rand(-angleX, angleX), Toolbox.Instance.Rand(-angleY, angleY), Toolbox.Instance.Rand(-angleX, angleX));
            Vector3 dirPoint = randomTarget - worldOrigin;

            //Choose a ray length
            float rayLength = Toolbox.Instance.Rand(rayRanges.x, rayRanges.y);

            Ray ray = new Ray(worldOrigin, dirPoint);

            RaycastHit hitInfo;

            if (Physics.Raycast(ray, out hitInfo, rayLength, Toolbox.Instance.spatterLayerMask))
            {
                GameObject newSpatter = Toolbox.Instance.SpawnObject(PrefabControls.Instance.spatterSimulation, hitInfo.point, Quaternion.FromToRotation(Vector3.up, hitInfo.normal), hitInfo.transform);
                DecalProjector decal = newSpatter.GetComponent<DecalProjector>();

                DecalSpawnData newSpawnData = new DecalSpawnData { spawnedProjector = decal, sim = this };

                //Is this a room, door, interactable or human?
                Citizen cit = hitInfo.transform.GetComponentInParent<Citizen>();
                newSpawnData.subObjectName = hitInfo.transform.name;

                if (cit != null)
                {
                    newSpawnData.parentID = ParentID.human;
                    newSpawnData.transformParentID = cit.humanID;
                }
                else
                {
                    NewDoor door = hitInfo.transform.GetComponentInParent<NewDoor>();

                    if(door != null)
                    {
                        newSpawnData.parentID = ParentID.door;
                        newSpawnData.transformParentID = door.wall.id;
                    }
                    else
                    {
                        InteractableController ic = hitInfo.transform.GetComponentInParent<InteractableController>();

                        if(ic != null)
                        {
                            newSpawnData.parentID = ParentID.interactable;
                            if(ic.interactable != null) newSpawnData.transformParentID = ic.interactable.id;
                        }
                        else
                        {
                            NewRoom r = hitInfo.transform.GetComponentInParent<NewRoom>();

                            if (r != null)
                            {
                                newSpawnData.parentID = ParentID.room;
                                newSpawnData.transformParentID = r.roomID;
                                newSpatter.transform.SetParent(r.transform, true); //Use the position of the room
                            }
                        }
                    }
                }

                //Get relative distance
                float relDistance = hitInfo.distance / rayLength;
                float voxelSize = 0.05f;

                //Snap to nearest voxel size
                if(relDistance < 0.5f)
                {
                    voxelSize = 0.1f;
                }

                //Add a tiny amount to avoid Z fighting
                decal.size = new Vector3(voxelSize + 0.0001f, voxelSize + 0.0001f, voxelSize + 0.0001f);
                newSpawnData.size = decal.size;

                //Snap to nearest voxel position
                newSpatter.transform.position = new Vector3(Mathf.Round(newSpatter.transform.position.x / voxelSize) * voxelSize, Mathf.Round(newSpatter.transform.position.y / voxelSize) * voxelSize, Mathf.Round(newSpatter.transform.position.z / voxelSize) * voxelSize);
                newSpawnData.worldPos = newSpatter.transform.position;

                //Apply opacity material
                if(relDistance <= 0.33f)
                {
                    decal.material = preset.heavyMaterial;
                    newSpawnData.materialType = DecalMaterialType.heavy;
                }
                else if(relDistance <= 0.66f)
                {
                    decal.material = preset.mediumMaterial;
                    newSpawnData.materialType = DecalMaterialType.medium;
                }
                else
                {
                    decal.material = preset.lightMaterial;
                    newSpawnData.materialType = DecalMaterialType.light;
                }

                newSpawnData.worldEuler = newSpatter.transform.eulerAngles;
                decalsSpawned.Add(newSpawnData);

                //if(Game.Instance.devMode) Debug.DrawRay(worldOrigin, hitInfo.point - worldOrigin, Color.red, 2f);
            }
            else
            {
                //if (Game.Instance.devMode) Debug.DrawRay(worldOrigin, dirPoint, Color.white, 2f);
            }
        }
    }

    //Remove all created
    public void Remove()
    {
        while(decalsSpawned.Count > 0)
        {
            DecalSpawnData ds = decalsSpawned[0];

            if(ds != null)
            {
                if (ds.parentID == ParentID.interactable)
                {
                    if (ds.i == null) ds.i = CityData.Instance.interactableDirectory.Find(item => item.id == ds.transformParentID);

                    if (ds.i != null)
                    {
                        //Remove reference to spawn when object spawns...
                        if (ds.i.spawnedDecals != null)
                        {
                            ds.i.spawnedDecals.Remove(ds);
                        }
                    }
                }

                if (ds.spawnedProjector != null)
                {
                    DecalSpawnData.RecycleDecalProjector(ds.spawnedProjector);
                }
            }

            decalsSpawned.RemoveAt(0);
        }

        if(room != null && room.spatter != null) room.spatter.Remove(this); //Remove from room
        GameplayController.Instance.spatter.Remove(this); //Remove from global list
    }

    //Load from saved data
    public void LoadFromSerializedData()
    {
        Toolbox.Instance.LoadDataFromResources<SpatterPatternPreset>(presetStr, out preset);

        CityData.Instance.roomDictionary.TryGetValue(roomID, out room);

        UpdateSpawning();
    }

    //Try to spawn decal projectors (needs a spawned transform)
    public void UpdateSpawning()
    {
        foreach (DecalSpawnData ds in decalsSpawned)
        {
            if (ds.sim == null) ds.sim = this;

            if (ds.spawnedProjector == null)
            {
                Transform spawnTransform = null;

                if (ds.parentID == ParentID.human)
                {
                    Human h = null;

                    if (CityData.Instance.GetHuman(ds.transformParentID, out h))
                    {
                        spawnTransform = Toolbox.Instance.SearchForTransform(h.transform, ds.subObjectName);
                        if (spawnTransform == null) Game.LogError("Unable to load spatter with parent " + ds.parentID.ToString() + " id " + ds.transformParentID + " transform name: " + ds.subObjectName);
                    }
                }
                else if (ds.parentID == ParentID.door)
                {
                    NewDoor door = null;

                    if (CityData.Instance.doorDictionary.TryGetValue(ds.transformParentID, out door))
                    {
                        if (door.spawnedDoor != null) spawnTransform = door.spawnedDoor.transform;
                        else if (spawnTransform == null) Game.LogError("Unable to load spatter with parent " + ds.parentID.ToString() + " id " + ds.transformParentID + " transform name: " + ds.subObjectName);
                    }
                    else
                    {
                        Game.LogError("Unable to find door ID " + ds.transformParentID + " for spatter");
                    }
                }
                else if (ds.parentID == ParentID.interactable)
                {
                    if(ds.i == null) ds.i = CityData.Instance.interactableDirectory.Find(item => item.id == ds.transformParentID);

                    if(ds.i != null)
                    {
                        if (ds.i.spawnedObject != null)
                        {
                            spawnTransform = Toolbox.Instance.SearchForTransform(ds.i.spawnedObject.transform, ds.subObjectName, false);

                            if (spawnTransform == null)
                            {
                                Game.LogError("Unable to load spatter with parent " + ds.parentID.ToString() + " id " + ds.transformParentID + " transform name: " + ds.subObjectName);
                            }
                        }
                        //Add to reference to spawn when object spawns...
                        else
                        {
                            if (ds.i.spawnedDecals == null) ds.i.spawnedDecals = new List<DecalSpawnData>();

                            if(!ds.i.spawnedDecals.Contains(ds))
                            {
                                ds.i.spawnedDecals.Add(ds);
                            }
                        }
                    }
                }
                else if (ds.parentID == ParentID.room)
                {
                    NewRoom r = null;

                    if (CityData.Instance.roomDictionary.TryGetValue(ds.transformParentID, out r))
                    {
                        spawnTransform = r.transform;
                    }
                }

                if (spawnTransform == null)
                {
                    continue; //Skip this if we can't find the correct transform...
                }
                else
                {
                    ds.SpawnOnTransform(spawnTransform);
                }
            }
        }
    }
}
