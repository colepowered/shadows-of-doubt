﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//A controller given to every residence in the game.
//Script pass 1
public class ResidenceController : Controller, IComparable<ResidenceController>
{
    public ResidencePreset preset;

	[Header("Location")]
	public NewBuilding building;
	public NewAddress address;

    //Rooms
    public List<NewRoom> bedrooms = new List<NewRoom>();
    [System.NonSerialized]
    public int bedroomsTaken = 0; //Only used in city generation
    [System.NonSerialized]
    public FurnitureLocation mailbox = null;

    public void Setup(ResidencePreset newPreset, NewAddress newAddress)
    {
        //Game.Log("Setup residence " + newPreset.name);

        preset = newPreset;

        address = newAddress;
        building = address.building;

        //Add to residence directory
        CityData.Instance.residenceDirectory.Add(this);

        CreateEvidence();

        List<NewRoom> brooms = address.rooms.FindAll(item => item.roomType == InteriorControls.Instance.bedroomType);

        foreach(NewRoom bedroom in brooms)
        {
            AddBedroom(bedroom);
        }
    }

    public string GetResidenceString()
    {
        //Create residence string
        string numStr = address.residenceNumber.ToString();
        if (numStr.Length < 2) numStr = "0" + numStr;

        if (address != null && address.floor != null)
        {
            if (address.floor.floor < 0)
            {
                return Strings.Get("names.rooms", "Basement") + " " + numStr;
            }
            else
            {
                return address.floor.floor.ToString() + numStr;
            }
        }
        else return numStr;
    }

    //Return a complete residence number including floor, for int comparison only
    public int GetResidenceNumber()
    {
        int num = address.residenceNumber;

        if (address != null && address.floor != null)
        {
            if (address.floor.floor < 0)
            {
                num -= 100;
            }
            else
            {
                num += address.floor.floor * 100;
            }
        }

        return num;
    }

    public void AddBedroom(NewRoom newBedroom)
    {
        if(!bedrooms.Contains(newBedroom))
        {
            bedrooms.Add(newBedroom);
        }
    }

    public void Load(CitySaveData.ResidenceCitySave data, NewAddress newAddress)
    {
        address = newAddress;

        Toolbox.Instance.LoadDataFromResources<ResidencePreset>(data.preset, out preset); //Design style chosen for the address
        //bedroomsTaken = data.bedroomsTaken;

        //Add to residence directory
        CityData.Instance.residenceDirectory.Add(this);

        //For now, create evidence
        CreateEvidence();
    }

    public CitySaveData.ResidenceCitySave GenerateSaveData()
    {
        CitySaveData.ResidenceCitySave output = new CitySaveData.ResidenceCitySave();

        output.preset = preset.name;
        //output.bedroomsTaken = bedroomsTaken;
        if (mailbox != null) output.mail = mailbox.id;

        return output;
    }

    public override void CreateEvidence()
    {
        
    }

    //Default comparer(land value)
    public int CompareTo(ResidenceController other)
    {
        return this.address.normalizedLandValue.CompareTo(other.address.normalizedLandValue);
    }

    //Compare by societal class (Also include that of partner) 50% and 50% single person
    public static Comparison<ResidenceController> RoommateComparison = delegate (ResidenceController object1, ResidenceController object2)
    {
        //Get average societal class
        float thisClass = 0;

        foreach(Citizen cit in object1.address.inhabitants)
        {
            thisClass += cit.societalClass;
        }

        thisClass /= (float)object1.address.inhabitants.Count;

        //Give bonus to single people
        thisClass += 3 - object1.address.inhabitants.Count;

        float otherClass = 0;

        foreach (Citizen cit in object2.address.inhabitants)
        {
            otherClass += cit.societalClass;
        }

        otherClass /= (float)object2.address.inhabitants.Count;

        //Give bonus to single people
        otherClass += 3 - object2.address.inhabitants.Count;

        return thisClass.CompareTo(otherClass);
    };
}
