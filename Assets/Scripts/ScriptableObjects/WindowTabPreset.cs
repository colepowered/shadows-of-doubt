﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "windowtab_data", menuName = "Database/Window Tab Style")]

public class WindowTabPreset : SoCustomComparison
{
	[Header("Naming")]
	public string tabName = "New Tab";
    public Color colour = Color.white;
    public GameObject contentPrefab;

    public enum TabContentType { generated, message, facts, history, help, photoSelect, shop, objectives, callLogsIncoming, callLogsOutgoing, passcodes, phoneNumbers, resolve, results, decor, furnishings, colourPicker, floors, ceiling, materialKey, caseOptions, items, itemSelect };
    public TabContentType contentType = TabContentType.generated;

    [Header("Scripts")]
    public bool scalableContent = true;
    public bool fitToScaleX = true;
    public bool fitToScaleY = true;
    public bool zoomWithMouseWheel = true;

    [Header("Scroll")]
    public bool scrollBars = true;

    //Scroll restrictions
    public CustomScrollRect.MovementType scrollRestrcition;

    [Header("Content")]
    public string displayContentWithTag = string.Empty;
}
