﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugWalkableSublocation : MonoBehaviour
{
    public NewNode node;
    public MeshRenderer rend;
    public Material unoccupiedMat;
    public Material occupiedActualMat;
    public Material occupiedDestinationMat;
    public NewNode.NodeSpace space;

    public void Setup(NewNode newNode, NewNode.NodeSpace newSpace)
    {
        node = newNode;
        space = newSpace;
        enabled = true;
    }

    private void Update()
    {
        if(node != null && space != null)
        {
            if(space.occ == NewNode.NodeSpaceOccupancy.empty)
            {
                rend.sharedMaterial = unoccupiedMat;
            }
            else if (space.occ == NewNode.NodeSpaceOccupancy.reserved)
            {
                rend.sharedMaterial = occupiedDestinationMat;
            }
            else if(space.occ == NewNode.NodeSpaceOccupancy.position)
            {
                rend.sharedMaterial = occupiedActualMat;
            }
        }
    }
}
