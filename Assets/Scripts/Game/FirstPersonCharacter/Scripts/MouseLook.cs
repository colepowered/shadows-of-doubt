using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [Serializable]
    public class MouseLook
    {
        public bool clampVerticalRotation = true;
        public float MinimumX = -90F;
        public float MaximumX = 90F;
        public bool lockCursor = false;

        public Quaternion charMovementThisFrame;
        public Quaternion camMovementThisFrame;

        private Quaternion m_CharacterTargetRot;
        private Quaternion m_CameraTargetRot;
        private bool m_cursorIsLocked = true;

        public void Init(Transform character, Transform camera)
        {
            m_CharacterTargetRot = character.localRotation;
            m_CameraTargetRot = camera.localRotation;
        }

        public void LookRotation(Transform character, Transform camera, bool disableClamp = false)
        {
            float control = 1f;

            if (Player.Instance.transitionActive && Player.Instance.currentTransition != null)
            {
                control = Mathf.Clamp01(Player.Instance.currentTransition.mouseLookControlCurve.Evaluate(Player.Instance.transitionProgress));
            }

            Vector2 sensitivity = Game.Instance.mouseSensitivity * 0.1f;

            //Use controller sensitivity
            if(!InputController.Instance.mouseInputMode)
            {
                sensitivity = Game.Instance.controllerSensitivity * 0.1f;
            }

            float yRot = InputController.Instance.GetAxisRelative("LookHorizontal") * sensitivity.x * control * Game.Instance.axisMP.x;
            float xRot = InputController.Instance.GetAxisRelative("LookVertical") * sensitivity.y * control * Game.Instance.axisMP.y;

            m_CharacterTargetRot *= Quaternion.Euler (0f, yRot, 0f);
            m_CameraTargetRot *= Quaternion.Euler (-xRot, 0f, 0f);

            if(clampVerticalRotation && !disableClamp)
                m_CameraTargetRot = ClampRotationAroundXAxis (m_CameraTargetRot);

            //Capture rotation this frame
            Quaternion previousCharRot = character.rotation;
            Quaternion previousCamRot = camera.rotation;

            if(InputController.Instance.mouseInputMode && Game.Instance.mouseSmoothing > 0)
            {
                float sm = (51 - Game.Instance.mouseSmoothing) * Time.deltaTime;
                character.localRotation = Quaternion.Slerp (character.localRotation, m_CharacterTargetRot, sm);
                camera.localRotation = Quaternion.Slerp (camera.localRotation, m_CameraTargetRot, sm);
            }
            else if (!InputController.Instance.mouseInputMode && Game.Instance.controllerSmoothing > 0)
            {
                float sm = (51 - Game.Instance.controllerSmoothing) * Time.deltaTime;
                character.localRotation = Quaternion.Slerp(character.localRotation, m_CharacterTargetRot, sm);
                camera.localRotation = Quaternion.Slerp(camera.localRotation, m_CameraTargetRot, sm);
            }
            else
            {
                character.localRotation = m_CharacterTargetRot;
                camera.localRotation = m_CameraTargetRot;
            }

            //Apply drunk
            if(StatusController.Instance.drunkVision > 0f)
            {
                character.rotation *= Quaternion.Euler(0f, SessionData.Instance.drunkOscillation.y * StatusController.Instance.drunkVision, 0f);
                camera.rotation *= Quaternion.Euler(SessionData.Instance.drunkOscillation.x * StatusController.Instance.drunkVision, 0f, 0f);
            }

            //Apply shiver
            if(StatusController.Instance.shiverVision > 0f)
            {
                character.rotation *= Quaternion.Euler(0f, SessionData.Instance.shiverOscillation.y * StatusController.Instance.shiverVision, 0f);
                camera.rotation *= Quaternion.Euler(SessionData.Instance.shiverOscillation.x * StatusController.Instance.shiverVision, 0f, 0f);
            }

            //To get the difference in quaternions, you multiply the inverse.
            charMovementThisFrame = character.rotation * Quaternion.Inverse(previousCharRot);
            camMovementThisFrame = camera.rotation * Quaternion.Inverse(previousCamRot);

            UpdateCursorLock();
        }

        public void UpdateCursorLock()
        {
            //if the user set "lockCursor" we check & properly lock the cursos
            //if (lockCursor) InternalLockUpdate();
        }

        //private void InternalLockUpdate()
        //{
        //    if(Input.GetKeyUp(KeyCode.Escape))
        //    {
        //        m_cursorIsLocked = false;
        //    }
        //    else if(Input.GetMouseButtonUp(0))
        //    {
        //        m_cursorIsLocked = true;
        //    }

        //    InputController.Instance.SetCursorLock(m_cursorIsLocked);
        //}

        public Quaternion ClampRotationAroundXAxis(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan (q.x);

            angleX = Mathf.Clamp (angleX, MinimumX, MaximumX);

            q.x = Mathf.Tan (0.5f * Mathf.Deg2Rad * angleX);

            return q;
        }

        public Quaternion ClampRotationAroundYAxis(Quaternion q)
        {
            q.x /= q.w;
            q.y /= q.w;
            q.z /= q.w;
            q.w = 1.0f;

            float angleY = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.y);

            angleY = Mathf.Clamp(angleY, MinimumX, MaximumX);

            q.y = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleY);

            return q;
        }

    }
}
