﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class TooltipController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Header("Init")]
	public bool tooltipEnabled = true;
    public bool handleOwnBehaviour = true; //If true this tooltip will handle it's own opening and closing
    [Tooltip("If this isn't null, the tooltip will spawn parented to this layer instead of the tooltip canvas.")]
    public RectTransform parentOverride;

    [Header("Text")]
    //Use dictionary to get tooltip
    public bool useMainDictionaryEntry = false;
    public string mainDictionary = "ui.tooltips";
    public string mainDictionaryKey = string.Empty;

    public bool useDetailDictionaryEntry = false;
    public string detailDictionary = "ui.tooltips";
    public string detailDictionaryKey = string.Empty;

    public string mainText = string.Empty;
    public string detailText = string.Empty;

    [Header("State")]
	public bool isOver = false;

    [Header("Delay")]
    [Tooltip("Added onto the default spawn time (seconds)")]
    public float additionalSpawnDelay = 0f;
	public float moTimer = 0f;
    public GameObject spawnedTooltip = null;
    public TextMeshProUGUI tooltipText;
	public float fadeIn = 0f;

    public CanvasRenderer rend;
    public CanvasRenderer textRend;
    
	public Vector2 pos = new Vector2(0f, -0.5f);
    public bool useCursorPos = false;
    public Vector2 cursorPosOffset = new Vector2(10f, -7f);
    public bool limitWidth = false;
    public int extendTooltipWidth = 0;

    private Outline outline;
    public bool enableOutlineMouseOver = true;
	private Image img;
	public Sprite mouseOverSprite;

	private Sprite originalSprite;

	public ContextMenuController contextMenuBelongingToThis;

    [Tooltip("While active, constantly update the tooltip postion")]
    public bool updateTooltipPosition = false;

    //Currently active tooltip
    public static TooltipController activeTooltip = null;

    //Events
    //Triggered when tooltip is about to launch
    public delegate void BeforeTooltipSpawn();
    public event BeforeTooltipSpawn OnBeforeTooltipSpawn;

    void Start()
    {
        outline = this.gameObject.GetComponent<Outline>();
        img = this.gameObject.GetComponent<Image>();

        if (img != null)
        {
            originalSprite = img.sprite;
        }

		contextMenuBelongingToThis = this.gameObject.GetComponent<ContextMenuController>();

		GetText();
	}

	public void OnPointerEnter(PointerEventData eventData)
	{
        if(InputController.Instance.mouseInputMode) SetPointerOver(true);
	}

	public virtual void OnPointerExit(PointerEventData eventData)
	{
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;
        if (InputController.Instance.mouseInputMode) SetPointerOver(false);
	}

    //Triggered by buttons: This is useful when using a controller as a subsistute for the above
    public virtual void OnButtonHover()
    {
        SetPointerOver(true);
    }

    //Triggered by buttons: This is useful when using a controller as a subsistute for the above
    public virtual void OnButtonExitHover()
    {
        SetPointerOver(false);
    }

    public virtual void SetPointerOver(bool val)
    {
        if(isOver != val)
        {
            isOver = val;

            if(val)
            {
                if (InterfaceControls.Instance.enableTooltips && tooltipEnabled && handleOwnBehaviour)
                {
                    moTimer = 0f;
                    fadeIn = 0f;
                    OnMouseEnterCustom();

                    StartCoroutine("MouseOver");

                    //Enable outline
                    if (outline != null && enableOutlineMouseOver) outline.enabled = true;

                    //Enable mouse over texture
                    if (mouseOverSprite != null) img.sprite = mouseOverSprite;
                }
            }
            else
            {
                StopCoroutine("MouseOver");

                if (handleOwnBehaviour)
                {
                    ForceClose();
                }
            }
        }
    }

    public virtual void OnMouseEnterCustom()
    {
        return;
    }

    public virtual void GetText()
    {
        ForceClose();

        if (useMainDictionaryEntry)
        {
            if (mainDictionaryKey.Length > 0)
            {
                mainText = Strings.Get(mainDictionary, mainDictionaryKey);
            }
        }

        if (useDetailDictionaryEntry)
        {
            if (detailDictionaryKey.Length > 0)
            {
                detailText = Strings.Get(detailDictionary, detailDictionaryKey);
            }
        }
    }

	IEnumerator MouseOver()
	{
		while(isOver)
		{
			moTimer += Time.deltaTime;

            //Spawn tooltip
            if (mainText != null && detailText != null && mainText.Length + detailText.Length > 0)
            {
                if (spawnedTooltip == null)
                {
                    contextMenuBelongingToThis = this.gameObject.GetComponent<ContextMenuController>();

                    //Check for no spawned context menu
                    if (contextMenuBelongingToThis == null || contextMenuBelongingToThis.spawnedMenu == null)
                    {
                        if (moTimer > InterfaceControls.Instance.toolTipDelay + additionalSpawnDelay)
                        {
                            //If there is an existing tooltip, close it
                            if (activeTooltip != null) activeTooltip.ForceClose();

                            OpenTooltip();
                        }
                    }
                }
                else
                {
                    //Update position
                    if(updateTooltipPosition)
                    {
                        //Position relative to parent
                        RectTransform thisRect = this.gameObject.GetComponent<RectTransform>();
                        RectTransform rect = spawnedTooltip.GetComponent<RectTransform>();

                        spawnedTooltip.transform.SetParent(thisRect);

                        if (useCursorPos && InputController.Instance.mouseInputMode)
                        {
                            Vector2 localCursor = new Vector2(pos.x * thisRect.sizeDelta.x, pos.y * thisRect.sizeDelta.y);
                            RectTransformUtility.ScreenPointToLocalPointInRectangle(thisRect, Input.mousePosition, null, out localCursor);
                            rect.localPosition = localCursor;
                            rect.position += new Vector3(cursorPosOffset.x, cursorPosOffset.y, 0);
                        }
                        else
                        {
                            rect.position = this.transform.TransformPoint(new Vector2(pos.x * thisRect.sizeDelta.x, pos.y * thisRect.sizeDelta.y));
                        }

                        //Set the parent to the container
                        if(parentOverride == null)
                        {
                            spawnedTooltip.transform.SetParent(PrefabControls.Instance.tooltipsContainer);
                        }
                        else
                        {
                            spawnedTooltip.transform.SetParent(parentOverride);
                        }
                    }

                    //Fade in
                    if (fadeIn < 1f)
                    {
                        fadeIn += Time.deltaTime * InterfaceControls.Instance.toolTipFadeInSpeed;
                        fadeIn = Mathf.Clamp01(fadeIn);
                        rend.SetAlpha(fadeIn);
                        textRend.SetAlpha(fadeIn);
                        if (tooltipText != null) tooltipText.ForceMeshUpdate(); //Force update for workaround to sprite alpha problems
                    }
                }
            }

			if(!tooltipEnabled)
			{
                ForceClose();
                break;
			}

            //If this is spawned but not the active tooltip, remove
            if(spawnedTooltip != null && activeTooltip != this)
            {
                ForceClose();
                break;
            }

			yield return null;
		}

        //Remove upon end
        ForceClose();
	}

    public void OpenTooltip()
    {
        //Game.Log("Open tooltip " + mainText + " " + mainDictionaryKey);

        //Set this as active
        activeTooltip = this;

        //Fire event
        if (OnBeforeTooltipSpawn != null) OnBeforeTooltipSpawn();

        //if (spawnedTooltip != null) Destroy(spawnedTooltip);

        //Spawn and position using this transform before moving to the tooltips container...
        spawnedTooltip = Instantiate(PrefabControls.Instance.tooltip, this.transform);

        //Get the text
        tooltipText = spawnedTooltip.GetComponentInChildren<TextMeshProUGUI>();
        tooltipText.color = InterfaceControls.Instance.defaultTextColour;

        //Detail text is supplementary info that is displayed beneath the main text
        if (detailText.Length > 0)
        {
            tooltipText.text = ("<b>" + mainText + "</b>" + " <line-height=130%>" + '\n' + "<line-height=100%>" + "<alpha=#AA>" + detailText).Trim();
        }
        else
        {
            tooltipText.text = ("<b>" + mainText + "</b>").Trim();
        }

        //Make tooltip the right size
        RectTransform rect = spawnedTooltip.GetComponent<RectTransform>();

        if (!limitWidth)
        {
            rect.sizeDelta = new Vector2(Mathf.Min(InterfaceControls.Instance.tooltipWidth, tooltipText.preferredWidth + 20f) + extendTooltipWidth, rect.sizeDelta.y);
        }
        else
        {
            rect.sizeDelta = new Vector2(InterfaceControls.Instance.tooltipWidth + extendTooltipWidth, rect.sizeDelta.y);
        }

        //Set height after width so preffered value is correct
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, tooltipText.preferredHeight + 22);

        //Position relative to parent
        RectTransform thisRect = this.gameObject.GetComponent<RectTransform>();

        if (useCursorPos && InputController.Instance.mouseInputMode)
        {
            Vector2 localCursor = new Vector2(pos.x * thisRect.sizeDelta.x, pos.y * thisRect.sizeDelta.y);
            RectTransformUtility.ScreenPointToLocalPointInRectangle(thisRect, ClampToWindow(Input.mousePosition), null, out localCursor);
            rect.localPosition = localCursor;
            rect.position += new Vector3(cursorPosOffset.x, cursorPosOffset.y, 0);
        }
        else
        {
            rect.position = this.transform.TransformPoint(new Vector2(pos.x * thisRect.sizeDelta.x, pos.y * thisRect.sizeDelta.y));
        }

        //Set the parent to the container
        spawnedTooltip.transform.SetParent(PrefabControls.Instance.tooltipsContainer);

        //Now change parent to the top level canvas, so nothing appears above the tooltip
        spawnedTooltip.transform.SetAsLastSibling();

        //Set 1,1 scale
        rect.localScale = Vector3.one;

        //Set 0,0 rotation
        rect.localEulerAngles = Vector3.zero;

        CustomPositioning();

        //Clamp onscreen
        rect.localPosition = new Vector2(Mathf.Clamp(rect.localPosition.x, Screen.width * -0.5f, Screen.width * 0.5f - rect.sizeDelta.x),
                                        Mathf.Clamp(rect.localPosition.y, Screen.height * -0.5f + rect.sizeDelta.y, Screen.height * 0.5f));

        //Setup tooltip active script
        ActiveTooltip att = spawnedTooltip.GetComponent<ActiveTooltip>();
        att.ttc = this;
        att.setupComplete = true;

        //Set renderer
        rend = spawnedTooltip.GetComponent<CanvasRenderer>();
        textRend = tooltipText.GetComponent<CanvasRenderer>();
        fadeIn = 0f;
        rend.SetAlpha(0f);
        textRend.SetAlpha(0f);

        //Custom func
        OnMouseOverCustom();
    }

    Vector2 ClampToWindow(Vector2 rawPointerPosition)
    {
        Vector3[] canvasCorners = new Vector3[4];
        PrefabControls.Instance.tooltipsCanvas.GetWorldCorners(canvasCorners);

        float clampedX = Mathf.Clamp(rawPointerPosition.x, canvasCorners[0].x, canvasCorners[2].x - spawnedTooltip.GetComponent<RectTransform>().sizeDelta.x);
        float clampedY = Mathf.Clamp(rawPointerPosition.y, canvasCorners[0].y, canvasCorners[2].y);

        Vector2 newPointerPosition = new Vector2(clampedX, clampedY);
        return newPointerPosition;
    }

    public virtual void CustomPositioning()
    {
        return;
    }



    private void OnDisable()
    {
        //Remove upon end
        if (spawnedTooltip != null)
        {
            ForceClose();
        }
    }

    private void OnDestroy()
    {
        //Remove upon end
        if (spawnedTooltip != null)
        {
            ForceClose();
        }
    }

    public static void RemoveActiveTooltip()
    {
        //If there is an existing tooltip, close it
        if (TooltipController.activeTooltip != null) TooltipController.activeTooltip.ForceClose();
    }

    public void ForceClose()
	{
        //Game.Log("Close tooltip: " + mainText + " " + mainDictionaryKey);

        isOver = false;
		moTimer = 0f;
		fadeIn = 0f;
		StopCoroutine("MouseOver");

        //Destroy tooltip
        if (spawnedTooltip != null)
        {
            Destroy(spawnedTooltip);
        }

        OnMouseOffCustom();

		//Disable outline
		if(outline != null && enableOutlineMouseOver) outline.enabled = false;

		//Restore texture
		if(mouseOverSprite != null) img.sprite = originalSprite;

        //Set null active
        if(activeTooltip == this) activeTooltip = null;
	}

	public virtual void OnMouseOverCustom()
	{
		return;
	}

	public virtual void OnMouseOffCustom()
	{
		return;
	}
}
