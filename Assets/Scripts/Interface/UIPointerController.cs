﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPointerController : MonoBehaviour
{
    public RectTransform rect;
    public CanvasRenderer rend;
    public float alpha = 0f;
    public float fadeIn = 0f;
    public Objective objective;
    public Image img;
    public float distance = 0f;
    public NewNode node;

    public void Setup(Objective newObj)
    {
        if (rend == null) rend = this.gameObject.GetComponent<CanvasRenderer>();
        objective = newObj;
        img.sprite = objective.sprite;
        rend.SetAlpha(0f);

        //Find node so we can detect same rooms...
        Vector3Int nPos = CityData.Instance.RealPosToNodeInt(Player.Instance.lookAtThisTransform.position);
        PathFinder.Instance.nodeMap.TryGetValue(nPos, out node);
    }

    void Update()
    {
        if(fadeIn < 1f)
        {
            fadeIn += Time.deltaTime;
            fadeIn = Mathf.Clamp01(fadeIn);
            //rend.SetAlpha(fadeIn * alpha);
        }

        Vector3 currentPosition = objective.queueElement.pointerPosition;

        //Update distance
        distance = Vector3.Distance(Player.Instance.lookAtThisTransform.position, currentPosition);

        float distanceRel = 1f - Mathf.Clamp01(distance / 20f);

        float sameRoom = 0f;
        if (node != null && Player.Instance.currentRoom == node.room) sameRoom = 1f;

        alpha = (distanceRel * 0.4f) + (sameRoom * 0.4f) + 0.2f;
        rend.SetAlpha(fadeIn * alpha);

        float size = Mathf.Lerp(InterfaceControls.Instance.uiPointerDistanceRange.x, InterfaceControls.Instance.uiPointerDistanceRange.y, distanceRel);
        rect.sizeDelta = new Vector2(size, size);

        //Get angle from player...
        Vector3 forwardRelative = Vector3.zero;
        float angleBetween = 0;

        forwardRelative = currentPosition - Player.Instance.transform.position;
        angleBetween = Vector3.SignedAngle(forwardRelative, Player.Instance.transform.forward, Vector3.up);

        //Display on screen position or in the anchor display...
        if (Mathf.Abs(angleBetween) < 75f)
        {
            img.enabled = true;
            Vector3 screenPoint = CameraController.Instance.cam.WorldToScreenPoint(currentPosition);
            Vector2 rectPos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(InterfaceController.Instance.firstPersonUI, screenPoint, null, out rectPos);
            rect.localPosition = rectPos;
        }
        else
        {
            img.enabled = false;
        }
    }

    public void Remove()
    {
        Destroy(this.gameObject);
    }
}
