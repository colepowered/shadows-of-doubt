﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class Descriptors
{
    [System.NonSerialized]
    public Human citizen;

    public enum Age { youngAdult, adult, old};
    //public Age ageEnum = Age.adult;

    //public int age = 25;
    //public int ageGroup = 0;
    public float visualDistinctiveness = 0.5f;
    //public string debugEthnicties; //Debug

    //Build type (uiversal)
    public enum BuildType { skinny, average, overweight, muscular };
    public BuildType build = BuildType.average;

    //Height
    public enum Height { veryShort, hShort, hAverage, tall, veryTall };
    public Height height = Height.hAverage;

    public float heightCM = 175f;
    public float weightKG = 50f;
    public int shoeSize = 10;
    public Human.ShoeType footwear = Human.ShoeType.normal;


    //Ethnicity
    public enum EthnicGroup { westEuropean, eastEuropean, scandinavian, mediterranean, hispanic, african, indian, chinese, japanese, korean, nativeAmerican, middleEastern, australian, africanAmerican, islander, northAmerican, southAmerican};

    [System.Serializable]
    public class EthnicitySetting : IComparable<EthnicitySetting>
    {
        public EthnicGroup group;
        public float ratio;
        public SocialStatistics.EthnicityStats stats;

        //Default comparer (ratio)
        public int CompareTo(EthnicitySetting otherObject)
        {
            return this.ratio.CompareTo(otherObject.ratio);
        }
    }

    public List<EthnicitySetting> ethnicities = new List<EthnicitySetting>();
    public Color skinColour = Color.white;

    //Hair (ethnicity specific)
    public enum HairColour { black, brown, blonde, ginger, red, blue, green, purple, pink, grey, white };
    public HairColour hairColourCategory = HairColour.black;
    public Color hairColour = Color.black;

    public enum HairStyle { bald, shortHair, longHair };
    public HairStyle hairType = HairStyle.shortHair;

    public enum EyeColour { blueEyes, brownEyes, greenEyes, greyEyes };
    public EyeColour eyeColour = EyeColour.blueEyes;

    [System.Serializable]
    public struct FacialFeaturesSetting
    {
        public FacialFeature feature;
        public int id;
    }

    public enum FacialFeature {scaring, beard, moustache, piercing, tattoo, glasses, mole};
    public List<FacialFeaturesSetting> facialFeatures = new List<FacialFeaturesSetting>();

    public Descriptors(Human newCitizen)
    {
        citizen = newCitizen;
        visualDistinctiveness = Toolbox.Instance.GetPsuedoRandomNumberContained(0.4f, 0.6f, newCitizen.seed, out newCitizen.seed);

        //Generate ehtnicity
        GenerateEthnicity();

        //Generate hair and eyes
        GenerateEyes();
        GenerateHair();

        //Generate build
        GenerateBuild();

        //Generate height: Fast way of getting a weighted random
        heightCM = Mathf.RoundToInt(Toolbox.Instance.RandomRangeWeightedSeedContained(SocialStatistics.Instance.heightMinMax.x, SocialStatistics.Instance.heightMinMax.y, SocialStatistics.Instance.averageHeight, newCitizen.seed, out newCitizen.seed, 6));

        if(heightCM < 153)
        {
            height = Height.veryShort;
        }
        else if(heightCM < 168)
        {
            height = Height.hShort;
        }
        else if(heightCM < 183)
        {
            height = Height.hAverage;
        }
        else if (heightCM < 198)
        {
            height = Height.tall;
        }
        else
        {
            height = Height.veryTall;
        }

        //Generate weight
        float hMP = 2f;

        if (build == BuildType.skinny)
        {
            hMP = 0.3f + Toolbox.Instance.GetPsuedoRandomNumberContained(-0.1f, 0.1f, newCitizen.seed, out newCitizen.seed);
            visualDistinctiveness += 0.1f;
        }
        else if (build == BuildType.average)
        {
            hMP = 0.4f + Toolbox.Instance.GetPsuedoRandomNumberContained(-0.1f, 0.1f, newCitizen.seed, out newCitizen.seed);
        }
        else if (build == BuildType.overweight)
        {
            hMP = 0.5f + Toolbox.Instance.GetPsuedoRandomNumberContained(-0.1f, 0.1f, newCitizen.seed, out newCitizen.seed);
            visualDistinctiveness += 0.125f;
        }
        else if (build == BuildType.muscular)
        {
            hMP = 0.5f + Toolbox.Instance.GetPsuedoRandomNumberContained(-0.1f, 0.1f, newCitizen.seed, out newCitizen.seed);
            visualDistinctiveness += 0.2f;
        }

        weightKG = Mathf.RoundToInt(heightCM * hMP);

        float heightVSAverage = (float)heightCM / SocialStatistics.Instance.averageHeight; //Taller = higher value
        float weightVSAverage = (float)weightKG / SocialStatistics.Instance.averageWeight; //Heavier = higher value

        //Set health based on average height & weight
        citizen.SetMaxHealth(heightVSAverage * weightVSAverage);

        //Set combat heft based on average height & weight
        citizen.SetCombatHeft(heightVSAverage * weightVSAverage * CitizenControls.Instance.citizenCombatHeftMultiplier);

        //Set recovery rate
        citizen.SetRecoveryRate(CitizenControls.Instance.citizenBaseRecoveryRate);

        //Set combat skill
        citizen.SetCombatSkill(Toolbox.Instance.GetPsuedoRandomNumberContained(CitizenControls.Instance.citizenBaseCombatSkillRange.x, CitizenControls.Instance.citizenBaseCombatSkillRange.y, newCitizen.seed, out newCitizen.seed));

        //Base nerve is based on combat skill
        citizen.SetMaxNerve(citizen.combatSkill, true);

        GenerateFootwearPreference();

        //Generate shoe size
        float shoeSizeNormalized = Mathf.Clamp01((heightVSAverage * weightVSAverage) - 0.5f + (citizen.genderScale - 0.5f));
        float shoeSizeWeight = Mathf.Lerp(CitizenControls.Instance.shoeSizeRange.x, CitizenControls.Instance.shoeSizeRange.y, shoeSizeNormalized); //The expected shoe size

        shoeSize = Mathf.RoundToInt(Toolbox.Instance.RandomRangeWeightedSeedContained(CitizenControls.Instance.shoeSizeRange.x, CitizenControls.Instance.shoeSizeRange.y, shoeSizeWeight, newCitizen.seed, out newCitizen.seed, 6));
        CityData.Instance.averageShoeSize += shoeSize;

        GenerateFacialFeatures();

        //Visual distinctiveness is based on how 'extreme' physical stats are
        visualDistinctiveness = Mathf.Clamp01(visualDistinctiveness);

        //Add appropriate physical traits...
        if(hairType == HairStyle.bald)
        {
            citizen.AddCharacterTrait(CitizenControls.Instance.bald);
        }
        else if(hairType == HairStyle.shortHair)
        {
            citizen.AddCharacterTrait(CitizenControls.Instance.shortHair);
        }
        else if(hairType == HairStyle.longHair)
        {
            citizen.AddCharacterTrait(CitizenControls.Instance.longHair);
        }
    }

    private void GenerateEthnicity()
    {
        //Assign ethnicities
        while(true)
        {
            //Must have at least 1 ethnicity
            EthnicitySetting newEth = new EthnicitySetting();
            newEth.group = Toolbox.Instance.RandomEthnicGroup(citizen.seed);
            newEth.ratio = 1f;
            newEth.stats = SocialStatistics.Instance.ethnicityStats.Find(item => item.group == newEth.group);
            if(!ethnicities.Exists(item => item.group == newEth.group)) ethnicities.Add(newEth);
            //debugEthnicties = newEth.group.ToString()+" ";

            //Chance of additional ethnicity
            if(ethnicities.Count <= 1)
            {
                if (Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, citizen.seed, out citizen.seed) <= SocialStatistics.Instance.chanceOf2ndEthnicity)
                {
                    continue;
                }
            }

            break;
        }

        //Assign ethnicity ratios
        if(ethnicities.Count == 2)
        {
            //The first ethnicity will be dominant, and a minimum of 50%
            ethnicities[0].ratio = Toolbox.Instance.GetPsuedoRandomNumberContained(50, 90, citizen.seed, out citizen.seed) / 100f;
            ethnicities[1].ratio = 1f - ethnicities[0].ratio;
        }

        //Assign traits
        citizen.AddCharacterTrait(ethnicities[0].stats.ethTraits[Toolbox.Instance.GetPsuedoRandomNumberContained(0, ethnicities[0].stats.ethTraits.Count, citizen.seed, out citizen.seed)]);

        GenerateNameAndSkinColour();
    }

    public void GenerateNameAndSkinColour()
    {
        bool setDominant = false;
        EthnicGroup nameGroupFirst = EthnicGroup.westEuropean;
        EthnicGroup nameGroupSur = EthnicGroup.westEuropean;

        //Set skin colour
        foreach (EthnicitySetting eth in ethnicities)
        {
            //Pick a colour from the range in this ethnicity
            Color pick = Color.Lerp(eth.stats.skinColourRange1, eth.stats.skinColourRange2, Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, citizen.seed, out citizen.seed));

            //Apply it to the existing colour using lerp
            if (setDominant)
            {
                skinColour = Color.Lerp(skinColour, pick, eth.ratio); //Add skin colour to existing

                //Change of overriding name group
                if (eth.ratio >= Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, citizen.seed, out citizen.seed))
                {
                    nameGroupFirst = eth.group; //Set first ethnictiy group
                }

                if (eth.ratio >= Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, citizen.seed, out citizen.seed))
                {
                    nameGroupSur = eth.group; //Set surname ethnictiy group
                }
            }
            else
            {
                skinColour = pick; //Set dominatant skin colour
                nameGroupFirst = eth.group; //Set first ethnictiy group
                nameGroupSur = eth.group; //Set surname ethnictiy group

                setDominant = true;
            }

            //Overrride names
            if (eth.stats.overrideFirst) nameGroupFirst = eth.stats.overrideNameFirst;
            if (eth.stats.overrideSur) nameGroupSur = eth.stats.overrideNameSur;
        }

        //Generate name
        string nameFileString = "male";

        if (citizen.gender == Human.Gender.female)
        {
            nameFileString = "female";
        }

        int safetyAttempts = 99;

        string prefixOnly;
        string mainOnly;
        string suffixOnly;
        bool notNeeded;

        citizen.firstName = NameGenerator.Instance.GenerateName(null, 0f, "names." + nameGroupFirst.ToString() + ".first." + nameFileString, 1f, null, 0f, out prefixOnly, out mainOnly, out suffixOnly, out notNeeded, out _, citizen.seed);
        citizen.surName = NameGenerator.Instance.GenerateName(null, 0f, "names." + nameGroupSur.ToString() + ".sur", 1f, null, 0f, out prefixOnly, out mainOnly, out suffixOnly, out notNeeded, out _, citizen.seed);

        while(CityData.Instance.citizenDirectory.Exists(item => item != citizen && item.firstName == citizen.firstName && item.surName == citizen.surName) && safetyAttempts > 0)
        {
            Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, citizen.seed, out citizen.seed);
            citizen.firstName = NameGenerator.Instance.GenerateName(null, 0f, "names." + nameGroupFirst.ToString() + ".first." + nameFileString, 1f, null, 0f, out prefixOnly, out mainOnly, out suffixOnly, out notNeeded, out _, citizen.seed);
            citizen.surName = NameGenerator.Instance.GenerateName(null, 0f, "names." + nameGroupSur.ToString() + ".sur", 1f, null, 0f, out prefixOnly, out mainOnly, out suffixOnly, out notNeeded, out _, citizen.seed);
            safetyAttempts--;
        }

        citizen.citizenName = citizen.firstName + " " + citizen.surName;
        citizen.casualName = Strings.Get("names." + nameGroupFirst.ToString() + ".first." + nameFileString, citizen.firstName, getAlternate: true); //The casual name will be the alternate
        citizen.gameObject.name = citizen.citizenName;
        citizen.name = citizen.citizenName;
    }

    private void GenerateEyes()
    {
        List<EyeColour> eyePool = new List<EyeColour>();

        foreach(EthnicitySetting eth in ethnicities)
        {
            for (int u = 0; u < Mathf.RoundToInt(eth.stats.blueEyesRatio * eth.ratio); u++)
            {
                eyePool.Add(EyeColour.blueEyes);
            }

            for (int u = 0; u < Mathf.RoundToInt(eth.stats.brownEyesRatio * eth.ratio); u++)
            {
                eyePool.Add(EyeColour.brownEyes);
            }

            for (int u = 0; u < Mathf.RoundToInt(eth.stats.greenEyesRatio * eth.ratio); u++)
            {
                eyePool.Add(EyeColour.greenEyes);
            }

            for (int u = 0; u < Mathf.RoundToInt(eth.stats.greyEyesRatio * eth.ratio); u++)
            {
                eyePool.Add(EyeColour.greyEyes);
            }
        }

        eyeColour = eyePool[Toolbox.Instance.GetPsuedoRandomNumberContained(0, eyePool.Count, citizen.seed, out citizen.seed)];
    }

    private void GenerateHair()
    {
        List<HairColour> hairColourPool = new List<HairColour>();
        List<HairStyle> hairTypePool = new List<HairStyle>();

        int hBlack = 0;
        int hBrown = 0;
        int hBlonde = 0;
        int hGinger = 0;
        int hRed = 0;
        int hBlue = 0;
        int hGreen = 0;
        int hPurple = 0;
        int hPink = 0;
        int hGrey = 0;
        int hWhite = 0;

        int hBald = 0;
        int hShort = 0;
        int hLong = 0;

        //Older people more likely to have grey hair (must be over 33)
        float ageRatio = Mathf.Clamp01(((float)(citizen.GetAge() - 33) * 3f) / 100f);

        foreach (EthnicitySetting eth in ethnicities)
        {
            hBlack += Mathf.RoundToInt(eth.stats.blackHairRatio * eth.ratio * (1f - ageRatio)); //Chances of natural hair colours are deminished with age
            hBrown += Mathf.RoundToInt(eth.stats.brownHairRatio * eth.ratio * (1f - ageRatio));
            hBlonde += Mathf.RoundToInt(eth.stats.blondeHairRatio * eth.ratio * (1f - ageRatio));
            hGinger += Mathf.RoundToInt(eth.stats.gingerHairRatio * eth.ratio * (1f - ageRatio));
            hRed += Mathf.RoundToInt(eth.stats.RedHairRatio * eth.ratio);
            hBlue += Mathf.RoundToInt(eth.stats.blueHairRatio * eth.ratio);
            hGreen += Mathf.RoundToInt(eth.stats.greenHairRatio * eth.ratio);
            hPurple += Mathf.RoundToInt(eth.stats.purpleHairRatio * eth.ratio);
            hPink += Mathf.RoundToInt(eth.stats.pinkHairRatio * eth.ratio);
            hGrey += Mathf.RoundToInt(eth.stats.greyHairRatio * eth.ratio) + Mathf.RoundToInt(ageRatio * 25); //Increate amount as age increases
            hWhite += Mathf.RoundToInt(eth.stats.whiteHairRatio * eth.ratio) + Mathf.RoundToInt(ageRatio * 25); //Increate amount as age increases

            //Do I dye my hair?
            //Extraversion + Emotionality + Creativity
            float dyeMultiplier = (citizen.extraversion + citizen.emotionality + citizen.creativity) / 3f;
            hRed += Mathf.RoundToInt(SocialStatistics.Instance.RedHairRatio * dyeMultiplier);
            hBlue += Mathf.RoundToInt(SocialStatistics.Instance.blueHairRatio * dyeMultiplier);
            hGreen += Mathf.RoundToInt(SocialStatistics.Instance.greenHairRatio * dyeMultiplier);
            hPurple += Mathf.RoundToInt(SocialStatistics.Instance.purpleHairRatio * dyeMultiplier);
            hPink += Mathf.RoundToInt(SocialStatistics.Instance.pinkHairRatio * dyeMultiplier);

            //Length/Type
            if(citizen.gender == Human.Gender.male)
            {
                hBald += Mathf.RoundToInt(eth.stats.baldHairRatioMale * eth.ratio) + Mathf.RoundToInt(ageRatio * 20); //Increate amount as age increases
                hShort += Mathf.RoundToInt(eth.stats.shortHairRatioMale * eth.ratio);
                hLong = Mathf.RoundToInt(eth.stats.longHairRatioMale * eth.ratio);

                //Type
                //hStraight += Mathf.RoundToInt(eth.stats.straightHairRatioMale * eth.ratio);
                //hCurly += Mathf.RoundToInt(eth.stats.curlyHairRatioMale * eth.ratio);
                //hBalding += Mathf.RoundToInt(eth.stats.baldHairRatioMale * eth.ratio) + Mathf.RoundToInt(ageRatio * 20); //Increate amount as age increases
                //hMessy += Mathf.RoundToInt(eth.stats.messyHairRatioMale * eth.ratio);
                //hStyled += Mathf.RoundToInt(eth.stats.styledHairRatioMale * eth.ratio);
                //hMohawk += Mathf.RoundToInt(eth.stats.mohawkHairRatioMale * eth.ratio * (1f - ageRatio)); //Chances of natural hair colours are deminished with age
                //hAfro += Mathf.RoundToInt(eth.stats.afroHairRatioMale * eth.ratio * (1f - ageRatio)); //Chances of natural hair colours are deminished with age
            }
            else if (citizen.gender == Human.Gender.female)
            {
                hBald += Mathf.RoundToInt(eth.stats.baldHairRatioFemale * eth.ratio);
                hShort += Mathf.RoundToInt(eth.stats.shortHairRatioFemale * eth.ratio);
                hLong = Mathf.RoundToInt(eth.stats.longHairRatioFemale * eth.ratio);

                //Type
                //hStraight += Mathf.RoundToInt(eth.stats.straightHairRatioFemale * eth.ratio);
                //hCurly += Mathf.RoundToInt(eth.stats.curlyHairRatioFemale * eth.ratio);
                //hBalding += Mathf.RoundToInt(eth.stats.baldHairRatioFemale * eth.ratio) + Mathf.RoundToInt(ageRatio * 5); //Increate amount as age increases
                //hMessy += Mathf.RoundToInt(eth.stats.messyHairRatioFemale * eth.ratio);
                //hStyled += Mathf.RoundToInt(eth.stats.styledHairRatioFemale * eth.ratio);
                //hMohawk += Mathf.RoundToInt(eth.stats.mohawkHairRatioFemale * eth.ratio * (1f - ageRatio)); //Chances of natural hair colours are deminished with age
                //hAfro += Mathf.RoundToInt(eth.stats.afroHairRatioFemale * eth.ratio);
            }
        }

        for (int u = 0; u < Mathf.Max(hBlack, 1); u++)
        {
            hairColourPool.Add(HairColour.black);
        }

        for (int u = 0; u < Mathf.Max(hBrown, 1); u++)
        {
            hairColourPool.Add(HairColour.brown);
        }

        for (int u = 0; u < Mathf.Max(hBlonde, 1); u++)
        {
            hairColourPool.Add(HairColour.blonde);
        }

        for (int u = 0; u < Mathf.Max(hGinger, 1); u++)
        {
            hairColourPool.Add(HairColour.ginger);
        }

        for (int u = 0; u < Mathf.Max(hRed, 1); u++)
        {
            hairColourPool.Add(HairColour.red);
        }

        for (int u = 0; u < Mathf.Max(hBlue, 1); u++)
        {
            hairColourPool.Add(HairColour.blue);
        }

        for (int u = 0; u < Mathf.Max(hGreen, 1); u++)
        {
            hairColourPool.Add(HairColour.green);
        }

        for (int u = 0; u < Mathf.Max(hPurple, 1); u++)
        {
            hairColourPool.Add(HairColour.purple);
        }

        for (int u = 0; u < Mathf.Max(hPink, 1); u++)
        {
            hairColourPool.Add(HairColour.pink);
        }

        for (int u = 0; u < Mathf.Max(hGrey, 1); u++)
        {
            hairColourPool.Add(HairColour.grey);
        }

        for (int u = 0; u < Mathf.Max(hWhite, 1); u++)
        {
            hairColourPool.Add(HairColour.white);
        }

        for (int u = 0; u < Mathf.Max(hBald, 1); u++)
        {
            hairTypePool.Add(HairStyle.bald);
        }

        for (int u = 0; u < Mathf.Max(hShort, 1); u++)
        {
            hairTypePool.Add(HairStyle.shortHair);
        }

        for (int u = 0; u < Mathf.Max(hLong, 1); u++)
        {
            hairTypePool.Add(HairStyle.longHair);
        }

        hairColourCategory = hairColourPool[Toolbox.Instance.GetPsuedoRandomNumberContained(0, hairColourPool.Count, citizen.seed, out citizen.seed)];
        hairType = hairTypePool[Toolbox.Instance.GetPsuedoRandomNumberContained(0, hairTypePool.Count, citizen.seed, out citizen.seed)];

        SocialStatistics.HairSetting hairSetting = SocialStatistics.Instance.hairColourSettings.Find(item => item.colour == hairColourCategory);
        hairColour = Color.Lerp(hairSetting.hairColourRange1, hairSetting.hairColourRange2, Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, citizen.seed, out citizen.seed));
    }

    private void GenerateBuild()
    {
        //verySkinny(5), skinny(15), average (35), overweight(20), obese(5), athletic(10), muscley(10)
        //int rVSkinny = Mathf.Max(Mathf.RoundToInt(SocialStatistics.Instance.verySkinnyRatio * (0.1f + citizen.conscientiousness)), 0);
        int rSkinny = Mathf.Max(Mathf.RoundToInt(SocialStatistics.Instance.skinnyRatio * (0.6f + citizen.conscientiousness)), 0);
        int rAverage = Mathf.Max(SocialStatistics.Instance.averageRatio, 0);
        int rOverweight = Mathf.Max(Mathf.RoundToInt(SocialStatistics.Instance.overweightRatio * (1.6f - citizen.conscientiousness)), 0);
        //int rObese = Mathf.Max(Mathf.RoundToInt(SocialStatistics.Instance.obeseRatio * (1.1f - citizen.conscientiousness)), 0);
        //int rAthletic = Mathf.Max(Mathf.RoundToInt(SocialStatistics.Instance.athleticRatio * (citizen.conscientiousness)), 0);
        int rMuscley = Mathf.Max(Mathf.RoundToInt(SocialStatistics.Instance.muscleyRatio * (0.5f + citizen.conscientiousness)), 0);

        //More chance of being muscley if identifies as male (swap with athletic)
        //if (citizen.gender == Human.Gender.male || citizen.gender == Human.Gender.nonBinary)
        //{
        //    rMuscley = Mathf.Max(rMuscley + 2, 0);
        //    rAthletic = Mathf.Max(rAthletic - 2, 0);
        //}

        //Les chance of being muscles if identifies as female (swap with athletic)
        //if (citizen.gender == Human.Gender.female || citizen.gender == Human.Gender.nonBinary)
        //{
        //    rMuscley = Mathf.Max(rMuscley - 2, 0);
        //    rAthletic = Mathf.Max(rAthletic + 2, 0);
        //}

        List<BuildType> rBuildType = new List<BuildType>();

        //for (int u = 0; u < rVSkinny; u++)
        //{
        //    rBuildType.Add(BuildType.verySkinny);
        //}

        for (int u = 0; u < rSkinny; u++)
        {
            rBuildType.Add(BuildType.skinny);
        }

        for (int u = 0; u < rAverage; u++)
        {
            rBuildType.Add(BuildType.average);
        }

        for (int u = 0; u < rOverweight; u++)
        {
            rBuildType.Add(BuildType.overweight);
        }

        //for (int u = 0; u < rObese; u++)
        //{
        //    rBuildType.Add(BuildType.obese);
        //}

        //for (int u = 0; u < rAthletic; u++)
        //{
        //    rBuildType.Add(BuildType.athletic);
        //}

        for (int u = 0; u < rMuscley; u++)
        {
            rBuildType.Add(BuildType.muscular);
        }

        build = rBuildType[Toolbox.Instance.GetPsuedoRandomNumberContained(0, rBuildType.Count, citizen.seed, out citizen.seed)];
    }

    private void GenerateFacialFeatures()
    {
        List<FacialFeature> featuresPool = new List<FacialFeature>();

        //Scaring- more likely to occur with age and lower societal class
        int scaringLikihood = Mathf.RoundToInt((1f - citizen.societalClass) * citizen.GetAge() * (SocialStatistics.Instance.scaringRatio/100f));

        if(Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, citizen.seed, out citizen.seed) < scaringLikihood)
        {
            FacialFeaturesSetting newFeature = new FacialFeaturesSetting();
            newFeature.feature = FacialFeature.scaring;
            newFeature.id = 0;
            facialFeatures.Add(newFeature);
        }

        //Facial hair - only if identifies as male or nb
        if(citizen.gender == Human.Gender.male || citizen.gender == Human.Gender.nonBinary)
        {
            if(Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, citizen.seed, out citizen.seed) < SocialStatistics.Instance.menWithBeards)
            {
                FacialFeaturesSetting newFeature = new FacialFeaturesSetting();
                newFeature.feature = FacialFeature.beard;
                newFeature.id = 0;
                facialFeatures.Add(newFeature);
            }

            if (Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, citizen.seed, out citizen.seed) < SocialStatistics.Instance.menWithMoustaches)
            {
                FacialFeaturesSetting newFeature = new FacialFeaturesSetting();
                newFeature.feature = FacialFeature.moustache;
                newFeature.id = 0;
                facialFeatures.Add(newFeature);
            }
        }

        //Piercing & tattoo- more likely to occur with lower age and lower societal class
        int piercingLikihood = Mathf.RoundToInt((1f - citizen.societalClass) * (100f - citizen.GetAge()) * (SocialStatistics.Instance.piercingRatio/100f));

        if (Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, citizen.seed, out citizen.seed) < piercingLikihood)
        {
            FacialFeaturesSetting newFeature = new FacialFeaturesSetting();
            newFeature.feature = FacialFeature.piercing;
            newFeature.id = 0;
            facialFeatures.Add(newFeature);
        }

        int tattooLikihood = Mathf.RoundToInt((1f - citizen.societalClass) * (100f - citizen.GetAge()) * (SocialStatistics.Instance.TattooRatio/100f));

        if (Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, citizen.seed, out citizen.seed) < tattooLikihood)
        {
            FacialFeaturesSetting newFeature = new FacialFeaturesSetting();
            newFeature.feature = FacialFeature.tattoo;
            newFeature.id = 0;
            facialFeatures.Add(newFeature);
        }

        //Glasses wearers
        if (Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, citizen.seed, out citizen.seed) < SocialStatistics.Instance.glassesRatio)
        {
            FacialFeaturesSetting newFeature = new FacialFeaturesSetting();
            newFeature.feature = FacialFeature.glasses;
            newFeature.id = 0;
            facialFeatures.Add(newFeature);
        }

        //Moles
        if (Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, citizen.seed, out citizen.seed) < SocialStatistics.Instance.moleRatio)
        {
            FacialFeaturesSetting newFeature = new FacialFeaturesSetting();
            newFeature.feature = FacialFeature.mole;
            newFeature.id = 0;
            facialFeatures.Add(newFeature);
        }
    }

    private void GenerateFootwearPreference()
    {
        List<Human.ShoeType> pool = new List<Human.ShoeType>();

        foreach(Human.ShoeType shoe in Toolbox.Instance.allShoeTypes)
        {
            if (shoe == Human.ShoeType.barefoot) continue; //Ignore barefoot

            int feminineMasculine = 5;
            int humility = 5;
            int emotionality = 5;
            int extraversion = 5;
            int agreeableness = 5;
            int conscientiousness = 5;
            int creativity = 5;

            if(shoe == Human.ShoeType.heel)
            {
                feminineMasculine = 0;
                extraversion = 7;
            }
            else if(shoe == Human.ShoeType.boots)
            {
                feminineMasculine = 7;
                conscientiousness = 8;
            }

            //Get matching stats...
            int c_femMasc = 10 - Mathf.RoundToInt(Mathf.Abs(feminineMasculine - (citizen.genderScale * 10f)));
            int c_humility = 10 - Mathf.RoundToInt(Mathf.Abs(humility - (citizen.humility * 10f)));
            int c_emotionality = 10 - Mathf.RoundToInt(Mathf.Abs(emotionality - (citizen.emotionality * 10f)));
            int c_extraversion = 10 - Mathf.RoundToInt(Mathf.Abs(extraversion - (citizen.extraversion * 10f)));
            int c_agreeableness = 10 - Mathf.RoundToInt(Mathf.Abs(agreeableness - (citizen.agreeableness * 10f)));
            int c_conscientiousness = 10 - Mathf.RoundToInt(Mathf.Abs(conscientiousness - (citizen.conscientiousness * 10f)));
            int c_creativity = 10 - Mathf.RoundToInt(Mathf.Abs(creativity - (citizen.creativity * 10f)));

            //Add and divide by 6 to give a score out of 10
            int hexacoScore = Mathf.Max(Mathf.FloorToInt((float)(c_humility + c_emotionality + c_extraversion + c_agreeableness + c_conscientiousness + c_creativity + c_femMasc) / 7f), 1);

            for (int u = 0; u < hexacoScore; u++)
            {
                pool.Add(shoe);
            }
        }

        footwear = pool[Toolbox.Instance.GetPsuedoRandomNumberContained(0, pool.Count, citizen.seed, out citizen.seed)];

        //Add appropriate physical traits...
        if (footwear == Human.ShoeType.normal)
        {
            citizen.AddCharacterTrait(CitizenControls.Instance.shoesNormal);
        }
        else if (footwear == Human.ShoeType.heel)
        {
            citizen.AddCharacterTrait(CitizenControls.Instance.shoesHeels);
        }
        else if (footwear == Human.ShoeType.boots)
        {
            citizen.AddCharacterTrait(CitizenControls.Instance.shoesBoots);
        }
    }

    //Descriptor comparison
    public static float DescriptorComparison(Descriptors comp1, Descriptors comp2)
    {
        return 1f;
    }
}
