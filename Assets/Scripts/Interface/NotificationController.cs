﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using TMPro;

public class NotificationController : MonoBehaviour
{
    [Header("Components")]
    public TextMeshProUGUI numberText;
    public JuiceController juice;
    public RectTransform glowRect;
    public Image glowImg;
    public RectTransform HUDNotificationsIcon;

    [Header("State")]
    private float time = 0f;
    public int notifications = 0;

    private void OnEnable()
    {
        UpdateNotifications();
    }

    public void AddNotification(int val)
    {
        notifications += val;
        UpdateNotifications();
    }

    public void SetNotifications(int val)
    {
        notifications = val;
        UpdateNotifications();
    }

    public void UpdateNotifications()
    {
        try
        {
            if (notifications > 0)
            {
                if(this.gameObject != null) this.gameObject.SetActive(true);
                numberText.text = notifications.ToString();
                if (HUDNotificationsIcon != null) HUDNotificationsIcon.gameObject.SetActive(true);
            }
            else
            {
                if (this.gameObject != null) this.gameObject.SetActive(false);
                if (HUDNotificationsIcon != null) HUDNotificationsIcon.gameObject.SetActive(false);
            }
        }
        catch
        {

        }

    }

    private void Update()
    {
        time += Time.deltaTime * 0.5f;
        if (time >= 1f) time = 0f;
        float scaleEv = InterfaceControls.Instance.notificationGlowCurve.Evaluate(time);
        glowRect.localScale = new Vector3(scaleEv, scaleEv, scaleEv);
        glowImg.color = Color.Lerp(InterfaceControls.Instance.notificationColorMin, InterfaceControls.Instance.notificationColorMax, scaleEv);
    }
}
