﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using NaughtyAttributes;

public class StringController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public RectTransform rect;
    public RectTransform fromRect;
    public RectTransform toRect;
    public Image img;
    public Color pulsateColor;

    public Dictionary<Fact, TooltipController> tooltips = new Dictionary<Fact, TooltipController>();

    //List of connections this represents
    public CasePanelController.StringConnection connection;

    public ContextMenuController contextMenu;

    public JuiceController juice;

    public List<Evidence.FactLink> debugLinks = new List<Evidence.FactLink>();

    //Animations
    //public RectTransform leftRightAnimRect;
    //public Image leftRightImg;
    //public bool fromToActive = false;
    //public int fromToIncrimination = 0;

    //public RectTransform rightLeftAnimRect;
    //public Image rightLeftImg;
    //public bool toFromActive = false;
    //public int toFromIncrimination = 0;

    public float cumulativeReliability = 0f;

    //Tooltips
    public bool isOver = false;

    public float additionalSpawnDelay = 0f;
    public float moTimer = 0f;
    [System.NonSerialized]
    public float fadeIn = 0f;

    public void Setup(CasePanelController.StringConnection newConnection)
    {
        rect = this.gameObject.GetComponent<RectTransform>();
        //outline = this.gameObject.GetComponent<Outline>();
        connection = newConnection;
        fromRect = connection.from.GetComponent<RectTransform>();
        toRect = connection.to.GetComponent<RectTransform>();
        img = this.gameObject.GetComponent<Image>();

        name = connection.from.name + " -> " + connection.to.name;

        //Add tooltips
        int tooltipXPos = 0;

        foreach(Fact fct in connection.facts)
        {
            TooltipController ttc = this.gameObject.AddComponent<TooltipController>();
            ttc.useCursorPos = true;
            ttc.limitWidth = true;
            ttc.handleOwnBehaviour = false; //Because this needs multiple tooltips, handle them from this class
            ttc.cursorPosOffset = new Vector2(10 + tooltipXPos, -7);
            ttc.extendTooltipWidth = 50;
            tooltips.Add(fct, ttc);

            tooltipXPos += Mathf.RoundToInt(InterfaceControls.Instance.tooltipWidth) + 32;

            if(fct.isCustom)
            {
                contextMenu.disabledItems.Clear(); //Allow removal of custom string, or renaming
            }
        }

        //Listen for movement of either controller
        connection.from.OnMoved += UpdatePosition;
        connection.to.OnMoved += UpdatePosition;

        //Listen for colour changes
        foreach(Evidence.FactLink link in connection.links)
        {
            //link.OnColourChanged += UpdateStringColour;
        }

        //Add to connected strings
        if (!connection.from.connectedStrings.Contains(this)) connection.from.connectedStrings.Add(this);
        if (!connection.to.connectedStrings.Contains(this)) connection.to.connectedStrings.Add(this);

        //Close tooltip on context menu open
        contextMenu.OnOpenMenu += ForceCloseTooltip;

        UpdatePosition();
        UpdateStringColour();
        UpdateHidden();
    }

    public void UpdatePosition()
    {
        if (rect == null)
        {
            try
            {
                rect = this.gameObject.GetComponent<RectTransform>();
            }
            catch
            {
                Game.Log("Trying to access a destroyed object.");
                return;
            }
        }
        if (fromRect == null) fromRect = connection.from.GetComponent<RectTransform>();
        if(toRect == null) toRect = connection.to.GetComponent<RectTransform>();

        //Set position to objects - offset
        Vector3 differenceVector = toRect.localPosition - fromRect.localPosition;

        //Calculate with based on whether this is a file link
        rect.sizeDelta = new Vector2(differenceVector.magnitude, rect.sizeDelta.y);

        //Set angle
        float angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
        rect.rotation = Quaternion.Euler(0, 0, angle);

        rect.localPosition = new Vector2(fromRect.localPosition.x, fromRect.localPosition.y + 70f);
    }

    public void ForceCloseTooltip()
    {
        //Remove upon end
        foreach (KeyValuePair<Fact, TooltipController> pair in tooltips)
        {
            pair.Value.ForceClose();
        }
    }

    private void OnDestroy()
    {
        connection.from.OnMoved -= UpdatePosition;
        connection.to.OnMoved -= UpdatePosition;

        //Listen for colour changes
        foreach (Evidence.FactLink link in connection.links)
        {
            //link.OnColourChanged -= UpdateStringColour;
        }

        //Close tooltip on context menu open
        contextMenu.OnOpenMenu -= ForceCloseTooltip;

        ////Listen for changes in the fact's connections' data keys
        //foreach (Fact f in connection.facts)
        //{
        //    f.OnConnectingEvidenceChangeDataKey -= UpdateTooltipText;
        //    f.OnNewName -= UpdateTooltipText;
        //}

        //Remove from connected strings
        if (connection.from.connectedStrings.Contains(this)) connection.from.connectedStrings.Remove(this);
        if (connection.to.connectedStrings.Contains(this)) connection.to.connectedStrings.Remove(this);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (InterfaceControls.Instance.enableTooltips)
        {
            isOver = true;
            moTimer = 0f;
            fadeIn = 0f;

            StartCoroutine("MouseOver");

            //Enable outline
            //if (outline != null) outline.enabled = true;
        }
    }

    IEnumerator MouseOver()
    {
        while (isOver && contextMenu.spawnedMenu == null)
        {
            moTimer += Time.deltaTime;

            if (moTimer > InterfaceControls.Instance.toolTipDelay + additionalSpawnDelay)
            {
                //If there is an existing tooltip, close it
                //if (TooltipController.activeTooltip != null) TooltipController.activeTooltip.ForceClose();

                //Spawn tooltips/update text
                foreach (KeyValuePair<Fact, TooltipController> pair in tooltips)
                {
                    if(pair.Value.spawnedTooltip == null)
                    {
                        UpdateTooltipText(pair.Key);
                        pair.Value.OpenTooltip();
                        pair.Value.spawnedTooltip.GetComponent<Image>().raycastTarget = false; //Set no raycast target because we need to check for mouse over this only
                        fadeIn = 0f;
                    }
                }

                //Fade in
                if (fadeIn < 1f)
                {
                    fadeIn += Time.deltaTime * InterfaceControls.Instance.toolTipFadeInSpeed;
                    fadeIn = Mathf.Clamp01(fadeIn);

                    //Spawn tooltips/update text
                    foreach (KeyValuePair<Fact, TooltipController> pair in tooltips)
                    {
                        pair.Value.rend.SetAlpha(fadeIn);
                        pair.Value.textRend.SetAlpha(fadeIn);

                        //Workaround: Bug where setting the alpha doesn't update the alpha of the sprites, so we must force and update
                        pair.Value.tooltipText.ForceMeshUpdate();
                    }
                }
            }

            yield return null;
        }

        //Remove upon end
        foreach(KeyValuePair<Fact, TooltipController> pair in tooltips)
        {
            pair.Value.ForceClose();
        }

        fadeIn = 0f;
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;

        isOver = false;
        StopCoroutine("MouseOver");
        fadeIn = 0f;

        foreach (KeyValuePair<Fact, TooltipController> pair in tooltips)
        {
            pair.Value.ForceClose();
        }
    }

    public void UpdateTooltipText(Fact fact)
    {
        TooltipController tooltip = tooltips[fact];
        Game.Log("Interface: Updating tooltip text for fact " + fact.preset.name);

        string main = fact.GetName();
        string detail = string.Empty; //"<font=\"pf_arma_five 8pt\">";

        //Add auto created to mouse over text
        if(fact.isCustom)
        {
            detail += Strings.Get("evidence.generic", "string_customfact");
        }
        else
        {
            detail += Strings.Get("evidence.generic", "string_autofact");
        }

        tooltip.mainText = main.Trim();
        tooltip.detailText = detail.Trim();
    }

    public void UpdateStringColour()
    {
        InterfaceControls.EvidenceColours getColour = InterfaceControls.EvidenceColours.red;

        if (CasePanelController.Instance.activeCase != null)
        {
            bool foundEntry = false;

            //Game.Log("Serching " + CasePanelController.Instance.activeCase.stringColours.Count + " string colours...");

            foreach (Case.StringColours sc in CasePanelController.Instance.activeCase.stringColours)
            {
               // Game.Log("Searching " + connection.links + " links...");

                foreach (Evidence.FactLink link in connection.links)
                {
                    if (sc.fromEv == link.thisEvidence.evID)
                    {
                        //Game.Log("Matching from ev");

                        bool toEvMatch = true;

                        foreach (Evidence ev in link.destinationEvidence)
                        {
                            if (!sc.toEv.Contains(ev.evID))
                            {
                                toEvMatch = false;
                                break;
                            }
                        }

                        if (toEvMatch)
                        {
                            //Game.Log("Matching to ev");

                            bool keysPass = false;

                            foreach (Evidence.DataKey dk in link.thisKeys)
                            {
                                if (sc.fromDK.Contains(dk))
                                {
                                    keysPass = true;
                                    break;
                                }
                            }

                            if(keysPass)
                            {
                                keysPass = false;

                                foreach (Evidence.DataKey dk in link.destinationKeys)
                                {
                                    if (sc.toDK.Contains(dk))
                                    {
                                        keysPass = true;
                                        break;
                                    }
                                }

                                //We've found a duplicate!
                                if (keysPass)
                                {
                                    getColour = (InterfaceControls.EvidenceColours)sc.colIndex;
                                    Game.Log("Interface: Found entry for this string " + getColour);
                                    foundEntry = true;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (foundEntry) break;
            }
        }

        InterfaceControls.PinColours pinColourRef = InterfaceControls.Instance.pinColours.Find(item => item.colour == getColour);
        img.color = pinColourRef.actualColour;

        //Update juice colours
        foreach(JuiceController.JuiceElement element in juice.elements)
        {
            element.originalColour = img.color;
        }

        juice.pulsateColour = img.color * pulsateColor;
    }

    [Button]
    public bool UpdateHidden()
    {
        //Game.Log("Update string hidden: " + name);

        if (CasePanelController.Instance.activeCase != null)
        {
            List<Fact> hiddenFacts = new List<Fact>();

            foreach (Fact fact in connection.facts)
            {
                string id = fact.GetIdentifier();

                if(CasePanelController.Instance.activeCase.hiddenConnections.Contains(id))
                {
                    hiddenFacts.Add(fact);
                }
            }

            List<Fact> nonHidden = new List<Fact>(connection.facts);

            foreach(Fact l in hiddenFacts)
            {
                nonHidden.Remove(l);
            }

            if (nonHidden.Count > 0)
            {
                this.gameObject.SetActive(true);
                //Game.Log("Return hidden false (Hidden: " + hiddenFacts.Count + "/" + connection.facts.Count + ")");
                return false;
            }
            else
            {
                this.gameObject.SetActive(false);
                //Game.Log("Return hidden true (Hidden: " + hiddenFacts.Count + "/" + connection.facts.Count + ")");
                return true;
            }
        }

        //Game.Log("Return hidden false");
        return false;
    }

    //Change evidence colours
    public void SetColour(InterfaceControls.EvidenceColours col)
    {
        if(CasePanelController.Instance.activeCase != null)
        {
            Game.Log("Interface: Set string colour " + col + " to " + connection.links.Count + " links");

            foreach(Evidence.FactLink link in connection.links)
            {
                CasePanelController.Instance.activeCase.AddNewStringColour(link, col);
            }

            UpdateStringColour();
        }
    }

    public void RenameCustomLink()
    {
        //Set existing colour...
        //bool foundColour = false;

        //InterfaceControls.EvidenceColours getColour = InterfaceControls.EvidenceColours.red;

        //Use player-set colours as a priority
        foreach (Evidence.FactLink link in connection.links)
        {
            //if (!foundColour)
            //{
            //    getColour = link.GetColour();
            //    foundColour = true;
            //}
            ////Use player-set colours as a priority
            //else if (link.playerSetColour)
            //{
            //    getColour = link.GetColour();
            //    foundColour = true;
            //}
        }

        //PopupMessageController.Instance.colour = getColour;
        PopupMessageController.Instance.PopupMessage("CustomFactRename", true, true, "Cancel", "Continue", anyButtonClosesMsg: true, newPauseState: PopupMessageController.AffectPauseState.no, true, Strings.Get("evidence.generic", "customlink"), enableColourPicker: true);
        PopupMessageController.Instance.OnRightButton += OnContinueFactName;
    }

    public void OnContinueFactName()
    {
        foreach(Fact f in connection.facts)
        {
            if(f.isCustom)
            {
                FactCustom fc = f as FactCustom;

                if (fc != null)
                {
                    fc.SetCustomName(PopupMessageController.Instance.inputField.text);
                }
            }
        }

        SetColour(PopupMessageController.Instance.colourPicker.GetCurrentSelectedEvidenceColourValue());

        PopupMessageController.Instance.OnRightButton -= OnContinueFactName;
    }

    public void RemoveCustomLink()
    {
        PopupMessageController.Instance.OnRightButton -= OnContinueFactName;

        //Find the custom fact
        List<Fact> customFacts = connection.facts.FindAll(item => item.isCustom);

        while(customFacts.Count > 0)
        {
            Fact customF = customFacts[0];

            for (int i = 0; i < customF.fromEvidence.Count; i++)
            {
                customF.fromEvidence[i].RemoveFactLink(customF);
            }

            for (int i = 0; i < customF.toEvidence.Count; i++)
            {
                customF.toEvidence[i].RemoveFactLink(customF);
            }

            GameplayController.Instance.factList.Remove(customF); //Remove from fact list

            customFacts.RemoveAt(0);
        }

        //Force update strings
        CasePanelController.Instance.UpdateStrings();
    }

    //Context menu
    public void SetColourRed()
    {
        SetColour(InterfaceControls.EvidenceColours.red);
    }

    public void SetColourBlue()
    {
        SetColour(InterfaceControls.EvidenceColours.blue);
    }

    public void SetColourYellow()
    {
        SetColour(InterfaceControls.EvidenceColours.yellow);
    }

    public void SetColourGreen()
    {
        SetColour(InterfaceControls.EvidenceColours.green);
    }

    public void SetColourPurple()
    {
        SetColour(InterfaceControls.EvidenceColours.purple);
    }

    public void SetColourWhite()
    {
        SetColour(InterfaceControls.EvidenceColours.white);
    }

    public void SetColourBlack()
    {
        SetColour(InterfaceControls.EvidenceColours.black);
    }

    public void Hide()
    {
        if (CasePanelController.Instance.activeCase != null)
        {
            Game.Log("Interface: Hide string " + name);

            foreach (Fact f in connection.facts)
            {
                CasePanelController.Instance.activeCase.SetHidden(f, true);
            }
        }
    }

    [Button]
    public void UpdateDebugFactLinks()
    {
        debugLinks = connection.links;
    }

    [Button]
    public void DisplayFactIdentifier()
    {
        if (connection != null)
        {
            foreach(Fact f in connection.facts)
            {
                Game.Log(f.GetIdentifier());
            }
        }
    }
}
