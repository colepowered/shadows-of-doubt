﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "trait_data", menuName = "Database/Character Trait")]

public class CharacterTrait : SoCustomComparison
{
    [Header("Type")]
    [Tooltip("If true this is a trait that can be picked, if false, this is a 'reason'.")]
    public bool isTrait = true;
    [Tooltip("If true, a 'reason' trait will be immediately picked to accompany this trait")]
    public bool needsReson = false;
    [Tooltip("If true, this trait requires a partner")]
    public bool requiresPartner = false;
    [Tooltip("If true, this trait requires NO partner")]
    public bool requiresSingle = false;
    [Tooltip("If true, this trait requires a citizen to have a home")]
    public bool requiresHome = false;
    [Tooltip("If true, this trait requires a job")]
    public bool requiresEmployment = false;
    [Tooltip("If true, this needs a date")]
    public bool needsDate = false;
    [Tooltip("Appears in the 'random interest' pool when acquiring information on the citizen.")]
    public bool featureInInterestPool = false;
    [Tooltip("Appears in the 'random affliction' pool when acquiring information on the citizen.")]
    public bool featureInAfflictionPool = false;
    [EnableIf("needsDate")]
    [Tooltip("This event happened when their age was...")]
    public Vector2 ageDateRange = new Vector2(5, 10);
    [EnableIf("needsDate")]
    [Tooltip("Use couples anniversary date")]
    public bool useCouplesAnniversary = false;
    [Tooltip("This is a password (special case)")]
    public bool isPassword = false;
    [Tooltip("Disabled from being assigned automatically")]
    public bool disabled = false;
    [Tooltip("Is this considered a positive/neutral/negative trait?")]
    public PosNeg postiveNegative = PosNeg.neutral;

    public enum PosNeg { postive, neutral, negative };

    [Header("Pick")]
    [Range(0, 3)]
    [EnableIf("isTrait")]
    [Tooltip("When is this anylised to see if it is picked or not? 0 = first, 2 = last")]
    public int pickStage = 0;

    [Range(0f, 1f)]
    [EnableIf("isTrait")]
    [Tooltip("Chance of assigning this trait completely at random on citizen creation")]
    public float primeBaseChance = 0;

    public enum RuleType { ifAnyOfThese, ifAllOfThese, ifNoneOfThese, ifPartnerAnyOfThese };

    [System.Serializable]
    public class TraitPickRule
    {
        public RuleType rule = RuleType.ifAnyOfThese;

        public List<CharacterTrait> traitList = new List<CharacterTrait>();
        [ShowIf("isTrait")]
        [Tooltip("If this isn't true then it won't be picked for application at all.")]
        public bool mustPassForApplication = true;
        [ShowIf("isTrait")]
        [Range(-1f, 1f)]
        [Tooltip("If the rules match, then apply this to the base chance...")]
        public float baseChance = 0;
        [HideIf("isTrait")]
        [Range(0, 10)]
        [Tooltip("Since only one reason is picked, this chance is vs other valid chances...")]
        public int reasonChance = 5;
    }

    [System.Serializable]
    public class SpecialItemPlacementRule
    {
        public InteractablePreset preset;
        [Range(0f, 1f)]
        public float chance = 1f;
    }

    [ReorderableList]
    public List<TraitPickRule> pickRules = new List<TraitPickRule>();

    [Header("Match")]
    [Range(0f, 1f)]
    [Tooltip("Importance of matching this to base HEXACO personality. This is added to the base chance of either prime or secondary.")]
    public float matchChance = 0;

    [Space(5)]
    public bool useHumilityMatch = false;
    [Tooltip("Honesty-Humility (H): sincere, honest, faithful, loyal, modest/unassuming versus sly, deceitful, greedy, pretentious, hypocritical, boastful, pompous")]
    [Range(0f, 1f)]
    [EnableIf("useHumilityMatch")]
    public float matchHumility = 0f;

    public bool useEmotionalityMatch = false;
    [Tooltip("Emotionality (E): emotional, oversensitive, sentimental, fearful, anxious, vulnerable versus brave, tough, independent, self-assured, stable")]
    [Range(0f, 1f)]
    [EnableIf("useEmotionalityMatch")]
    public float matchEmotionality = 0f;

    public bool useExtraversionMatch = false;
    [Tooltip("Extraversion (X): outgoing, lively, extraverted, sociable, talkative, cheerful, active versus shy, passive, withdrawn, introverted, quiet, reserved")]
    [Range(0f, 1f)]
    [EnableIf("useExtraversionMatch")]
    public float matchExtraversion = 0f;

    public bool useAgreeablenessMatch = false;
    [Tooltip("Agreeableness (A): patient, tolerant, peaceful, mild, agreeable, lenient, gentle versus ill-tempered, quarrelsome, stubborn, choleric")]
    [Range(0f, 1f)]
    [EnableIf("useAgreeablenessMatch")]
    public float matchAgreeableness = 0f;

    public bool useConscientiousnessMatch = false;
    [Tooltip("Conscientiousness (C): organized, disciplined, diligent, careful, thorough, precise versus sloppy, negligent, reckless, lazy, irresponsible, absent-minded")]
    [Range(0f, 1f)]
    [EnableIf("useConscientiousnessMatch")]
    public float matchConscientiousness = 0f;

    public bool useCreativityMatch = false;
    [Tooltip("Openness to Experience (O): intellectual, creative, unconventional, innovative, ironic versus shallow, unimaginative, conventional")]
    [Range(0f, 1f)]
    [EnableIf("useCreativityMatch")]
    public float matchCreativity = 0f;

    public bool useSocietalClassMatch = false;
    [Range(0f, 1f)]
    [EnableIf("useSocietalClassMatch")]
    public float matchSocietalClass = 0f;

    [Header("Effects")]
    [Tooltip("Honesty-Humility (H): sincere, honest, faithful, loyal, modest/unassuming versus sly, deceitful, greedy, pretentious, hypocritical, boastful, pompous")]
    [Range(-1f, 1f)]
    public float effectHumility = 0f;
    [Tooltip("Emotionality (E): emotional, oversensitive, sentimental, fearful, anxious, vulnerable versus brave, tough, independent, self-assured, stable")]
    [Range(-1f, 1f)]
    public float effectEmotionality = 0f;
    [Tooltip("Extraversion (X): outgoing, lively, extraverted, sociable, talkative, cheerful, active versus shy, passive, withdrawn, introverted, quiet, reserved")]
    [Range(-1f, 1f)]
    public float effectExtraversion = 0f;
    [Tooltip("Agreeableness (A): patient, tolerant, peaceful, mild, agreeable, lenient, gentle versus ill-tempered, quarrelsome, stubborn, choleric")]
    [Range(-1f, 1f)]
    public float effectAgreeableness = 0f;
    [Tooltip("Conscientiousness (C): organized, disciplined, diligent, careful, thorough, precise versus sloppy, negligent, reckless, lazy, irresponsible, absent-minded")]
    [Range(-1f, 1f)]
    public float effectConscientiousness = 0f;
    [Tooltip("Openness to Experience (O): intellectual, creative, unconventional, innovative, ironic versus shallow, unimaginative, conventional")]
    [Range(-1f, 1f)]
    public float effectCreativity = 0f;

    [Space(7)]
    public float maxHealthModifier = 0f;
    public float recoveryRateModifier = 0f;
    public float combatSkillModifier = 0f;
    public float combatHeftModifier = 0f;
    public float maxNerveModifier = 0f;
    public float breathRecoveryModifier = 0f;

    [Header("Limits")]
    [Tooltip("Honesty-Humility (H): sincere, honest, faithful, loyal, modest/unassuming versus sly, deceitful, greedy, pretentious, hypocritical, boastful, pompous")]
    [MinMaxSlider(0f, 1f)]
    public Vector2 limitHumility = new Vector2(0f, 1f);
    [Tooltip("Emotionality (E): emotional, oversensitive, sentimental, fearful, anxious, vulnerable versus brave, tough, independent, self-assured, stable")]
    [MinMaxSlider(0f, 1f)]
    public Vector2 limitEmotionality = new Vector2(0f, 1f);
    [Tooltip("Extraversion (X): outgoing, lively, extraverted, sociable, talkative, cheerful, active versus shy, passive, withdrawn, introverted, quiet, reserved")]
    [MinMaxSlider(0f, 1f)]
    public Vector2 limitExtraversion = new Vector2(0f, 1f);
    [Tooltip("Agreeableness (A): patient, tolerant, peaceful, mild, agreeable, lenient, gentle versus ill-tempered, quarrelsome, stubborn, choleric")]
    [MinMaxSlider(0f, 1f)]
    public Vector2 limitAgreeableness = new Vector2(0f, 1f);
    [Tooltip("Conscientiousness (C): organized, disciplined, diligent, careful, thorough, precise versus sloppy, negligent, reckless, lazy, irresponsible, absent-minded")]
    [MinMaxSlider(0f, 1f)]
    public Vector2 limitConscientiousness = new Vector2(0f, 1f);
    [Tooltip("Openness to Experience (O): intellectual, creative, unconventional, innovative, ironic versus shallow, unimaginative, conventional")]
    [MinMaxSlider(0f, 1f)]
    public Vector2 limitCreativity = new Vector2(0f, 1f);

    [Header("Slang Pool")]
    [Tooltip("Affect the slang usage...")]
    [Range(-1f, 1f)]
    public float slangUsageModifier = 0f;
    [ReorderableList]
    [Tooltip("A default slang greeting to be used on anyone in a casual manor")]
    public List<string> slangGreetingDefault;
    [ReorderableList]
    [Tooltip("Similar to above, but male specific (eg. 'bro')")]
    public List<string> slangGreetingMale;
    [ReorderableList]
    [Tooltip("Similar to above, but female specific")]
    public List<string> slangGreetingFemale;
    [ReorderableList]
    [Tooltip("Slang greeting for a lover")]
    public List<string> slangGreetingLover;
    [ReorderableList]
    [Tooltip("Slang curse words")]
    public List<string> slangCurse;
    [ReorderableList]
    [Tooltip("Slang cursing noun word")]
    public List<string> slangCurseNoun;
    [ReorderableList]
    [Tooltip("Slang praising noun word")]
    public List<string> slangPraiseNoun;

    [Header("Culture")]
    [Tooltip("Does this affect the number of books this person should have?")]
    public int preferredBookCountModifier = 0;
    public int sightingLimitMemoryModifier = 0;
}
