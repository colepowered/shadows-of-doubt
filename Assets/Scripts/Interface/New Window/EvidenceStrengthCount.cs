﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EvidenceStrengthCount : CountController
{
    //public DetailInstance detailInstance;

    public int multiplierCount = 0;
    public Image multiplierCountImg;
    public Sprite multiplierCount0Sprite;
    public Sprite multiplierCount1Sprite;
    public Sprite multiplierCount2Sprite;
    public Sprite multiplierCount3Sprite;

    public int bonusCount = 0;
    public Image bonusImage;
    public Sprite plusSprite;
    public Sprite minusSprite;

    public Image adjEffectDisplay;
    public TooltipController adjTooltip;

    //public void Setup(DetailInstance newDetInstance)
    //{
    //    detailInstance = newDetInstance;

    //    adjTooltip = adjEffectDisplay.gameObject.GetComponent<TooltipController>();
    //}

    public void SetMultiplier(int newVal)
    {
        multiplierCount = newVal;

        if (multiplierCount <= 0) multiplierCountImg.sprite = multiplierCount0Sprite;
        else if (multiplierCount == 1) multiplierCountImg.sprite = multiplierCount1Sprite;
        else if (multiplierCount == 2) multiplierCountImg.sprite = multiplierCount2Sprite;
        else if (multiplierCount >= 3) multiplierCountImg.sprite = multiplierCount3Sprite;

        VisibilityCheck();
    }

    public void SetBonus(int newVal)
    {
        bonusCount = newVal;

        if (bonusCount == 0)
        {
            bonusImage.gameObject.SetActive(false);
        }
        else
        {
            bonusImage.gameObject.SetActive(true);
        }

        if (bonusCount > 0)
        {
            bonusImage.sprite = plusSprite;
        }
        else bonusImage.sprite = minusSprite;
    }
}
