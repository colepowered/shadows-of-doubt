﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MessageContentController : MonoBehaviour
{
    public TextMeshProUGUI messageText;
    public ProgressBarController progressBar;
    public RectTransform minigame;
    public RectTransform rect;

    private void Awake()
    {
        rect = this.gameObject.GetComponent<RectTransform>();
    }
}
