﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Linq;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using System.Xml;

[System.Serializable]
public class FurnitureClusterLocation : IComparable<FurnitureClusterLocation>
{
    [Header("Cluster Setup")]
    //For keeping track of the furniture locations in this cluster
    public Dictionary<NewNode, List<FurnitureLocation>> clusterObjectMap = new Dictionary<NewNode, List<FurnitureLocation>>();
    [System.NonSerialized]
    public List<FurnitureLocation> clusterList = new List<FurnitureLocation>();

    public FurnitureCluster cluster; //Cluster preset
    public NewNode anchorNode; //Reference to the anchor node
    public int angle; //The angle this object should be facing
    public float ranking = 0f;

    [Header("In-Game")]
    public bool loadedGeometry = false; //Controlled by the node this is positioned on

    public enum RemoveInteractablesOption { keep, remove, moveToStorage };

    //Constructor
    public FurnitureClusterLocation(NewNode newAnchor, FurnitureCluster newPreset, int newAngle, float newRank)
    {
        cluster = newPreset;
        anchorNode = newAnchor;
        angle = newAngle;
        ranking = newRank;
    }

    //Load furniture to world
    public void LoadFurnitureToWorld(bool forceSpawnImmediate = false)
    {
        if (!loadedGeometry)
        { 
            foreach (KeyValuePair<NewNode, List<FurnitureLocation>> pair in clusterObjectMap)
            {
                foreach (FurnitureLocation newFurn in pair.Value)
                {
                    ObjectPoolingController.Instance.MarkAsToLoad(newFurn, forceSpawnImmediate);
                    //anchorNode.room.AddForStaticBatching(newFurn); //Add to room's static batching NOTE: This has been replaced by implementation in the OnSpawn furniture function
                }
            }

            loadedGeometry = true;
        }
        else if(forceSpawnImmediate)
        {
            //Make sure furniture meshes are visible right away...
            foreach (KeyValuePair<NewNode, List<FurnitureLocation>> pair in clusterObjectMap)
            {
                foreach (FurnitureLocation newFurn in pair.Value)
                {
                    foreach (MeshRenderer mesh in newFurn.meshes)
                    {
                        mesh.enabled = true;
                    }
                }
            }
        }
    }

    //Unload furniture
    public void UnloadFurniture(bool removeIntegratedInteractables, RemoveInteractablesOption removeSpawnedInteractables)
    {
        if (loadedGeometry)
        {
            foreach (KeyValuePair<NewNode, List<FurnitureLocation>> pair in clusterObjectMap)
            {
                foreach (FurnitureLocation newFurn in pair.Value)
                {
                    ObjectPoolingController.Instance.MarkAsNotNeeded(newFurn);
                    newFurn.DespawnObject();

                    if(removeIntegratedInteractables)
                    {
                        newFurn.RemoveIntegratedInteractables();
                    }

                    //For items spawned on this furniture; remove them?
                    if (removeSpawnedInteractables == RemoveInteractablesOption.remove)
                    {
                        newFurn.RemoveSpawnedInteractables();
                    }
                    //For items spawned on this furniture; move them to storage...
                    else if (removeSpawnedInteractables == RemoveInteractablesOption.moveToStorage)
                    {
                        for (int i = 0; i < newFurn.spawnedInteractables.Count; i++)
                        {
                            PlayerApartmentController.Instance.MoveItemToStorage(newFurn.spawnedInteractables[i]);

                            //newFurn.spawnedInteractables.RemoveAt(i); //Removed from this reference so not needed?
                            //i--;
                        }
                    }
                }
            }

            loadedGeometry = false;
        }
    }

    public void DeleteCluster(bool removeIntegratedInteractables, RemoveInteractablesOption removeSpawnedInteractables)
    {
        List<FurnitureLocation> allFurn = new List<FurnitureLocation>(clusterList);

        foreach(FurnitureLocation l in allFurn)
        {
            DeleteFurniture(l.id, removeIntegratedInteractables, removeSpawnedInteractables);
        }

        anchorNode.room.furniture.Remove(this);

        ObjectPoolingController.Instance.UpdateObjectRanges();
    }

    //Delete furniture
    public void DeleteFurniture(int deleteID, bool removeIntegratedInteractables, RemoveInteractablesOption removeSpawnedInteractables)
    {
        foreach(KeyValuePair<NewNode, List<FurnitureLocation>> pair in clusterObjectMap)
        {
            for (int i = 0; i < pair.Value.Count; i++)
            {
                if(pair.Value[i].id == deleteID)
                {
                    ObjectPoolingController.Instance.MarkAsNotNeeded(pair.Value[i]);

                    if (removeIntegratedInteractables && pair.Value[i] != null)
                    {
                        pair.Value[i].RemoveIntegratedInteractables();
                    }

                    if (removeSpawnedInteractables == RemoveInteractablesOption.remove && pair.Value[i] != null)
                    {
                        pair.Value[i].RemoveSpawnedInteractables();
                    }
                    else if(removeSpawnedInteractables == RemoveInteractablesOption.moveToStorage && pair.Value[i] != null)
                    {
                        for (int u = 0; u < pair.Value[i].spawnedInteractables.Count; u++)
                        {
                            PlayerApartmentController.Instance.MoveItemToStorage(pair.Value[i].spawnedInteractables[u]);
                            //pair.Value[i].spawnedInteractables.RemoveAt(u); //Removed from this reference so not needed?
                            //u--;
                        }
                    }

                    anchorNode.room.individualFurniture.Remove(pair.Value[i]);
                    anchorNode.room.decorEdit = true; //We need to save this to state save data if anything has been deleted; this flag does that

                    ObjectPoolingController.Instance.MarkAsNotNeeded(pair.Value[i]);
                    pair.Value[i].DespawnObject();

                    pair.Value.RemoveAt(i);
                    i--;
                }
            }
        }

        for (int i = 0; i < clusterList.Count; i++)
        {
            if (clusterList[i].id == deleteID)
            {
                clusterList.RemoveAt(i);
                i--;
            }
        }

        ObjectPoolingController.Instance.UpdateObjectRanges();
    }

    //Default comparer (ranking)
    public int CompareTo(FurnitureClusterLocation otherObject)
    {
        return this.ranking.CompareTo(otherObject.ranking);
    }

    //Generate save data
    public CitySaveData.FurnitureClusterCitySave GenerateSaveData()
    {
        CitySaveData.FurnitureClusterCitySave output = new CitySaveData.FurnitureClusterCitySave();

        output.cluster = cluster.name;
        output.anchorNode = anchorNode.nodeCoord;
        output.angle = angle;
        output.ranking = ranking;

        foreach(FurnitureLocation cluster in clusterList)
        {
            if (cluster.furniture == null) continue;

            CitySaveData.FurnitureClusterObjectCitySave newObj = new CitySaveData.FurnitureClusterObjectCitySave();

            newObj.id = cluster.id;
            newObj.offset = cluster.offset;
            newObj.furnitureClasses = new List<string>();
            newObj.up = cluster.userPlaced;

            foreach(FurnitureClass furnClass in cluster.furnitureClasses)
            {
                newObj.furnitureClasses.Add(furnClass.name);
            }

            newObj.angle = cluster.angle;
            newObj.anchorNode = cluster.anchorNode.nodeCoord;

            newObj.coversNodes = new List<Vector3Int>();

            foreach(NewNode node in cluster.coversNodes)
            {
                newObj.coversNodes.Add(node.nodeCoord);
            }

            newObj.furniture = cluster.furniture.name;
            if (cluster.art != null) newObj.art = cluster.art.name;
            newObj.matKey = cluster.matKey; //Chosen when added to a room
            newObj.artMatKet = cluster.artMatKey;

            newObj.fovDirection = cluster.fovDirection;
            newObj.useFOVBLock = cluster.useFOVBLock;
            newObj.fovMaxDistance = cluster.fovMaxDistance;

            newObj.scale = cluster.scaleMultiplier;

            foreach(KeyValuePair<FurnitureLocation.OwnerKey, int> pair in cluster.ownerMap)
            {
                if(pair.Key.human != null)
                {
                    newObj.owners.Add(pair.Key.human.humanID);
                }
                else if(pair.Key.address != null)
                {
                    newObj.owners.Add(-pair.Key.address.id); //Use the - range to differentiate between human/address references
                }
            }

            output.objs.Add(newObj);
        }

        return output;
    }
}
