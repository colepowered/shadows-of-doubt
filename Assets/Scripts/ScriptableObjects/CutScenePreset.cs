using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "cutscene_data", menuName = "Database/Cut Scene")]

public class CutScenePreset : SoCustomComparison
{
    [Header("Timeline")]
    public List<CutSceneElement> elementList = new List<CutSceneElement>();

    [Space(7)]

    public bool fadeIn = true;
    [ShowIf("fadeIn")]
    public float fadeInTime = 2;
    public bool fadeOut = true;
    [ShowIf("fadeOut")]
    public float fadeOutTime = 2;

    [Space(7)]
    public Sprite displayImage;
    public float imageFadeIn;
    public float imageFadeInSpeed = 1f;
    public float imageFadeOut;
    public float imageFadeOutSpeed = 0f;

    [Header("Settings")]
    public bool disableAISpeech = true;

    [System.Serializable]
    public class CutSceneElement
    {
        public string name;
        public bool disable;
        public ElementType elementType;
        [Space(5)]
        public List<CameraMovement> movement;
        public AnimationCurve lerpPositionGraph;
        public AnimationCurve lerpRotationGraph;
        [Space(7)]
        public string ddsMessage;
        public float messageDelay;
    }

    public OnEndScene onEnd;

    public enum ElementType { newShot, ddsMessage };
    public enum OnEndScene { resumeGameplay, startGame, endGame };

    public enum AnchorType { blockCorner, middle };

    [System.Serializable]
    public class CameraMovement
    {
        public float atDuration;
        public Vector3 camPos;
        public Vector3 camEuler;
        public AnchorType anchor = AnchorType.blockCorner;

        public bool overridePosGraph = false;
        [ShowIf("overridePosGraph")]
        public AnimationCurve lerpPositionGraphOverride;

        public bool overrideRotGraph = false;
        [ShowIf("overrideRotGraph")]
        public AnimationCurve lerpRotationGraphOverride;
    }

    [Button]
    public void RecordCurrentPositionToNewShot()
    {
#if UNITY_EDITOR
        if(elementList.Count > 0)
        {
            CutSceneElement lastShot = null;

            for (int i = 0; i < elementList.Count; i++)
            {
                CutSceneElement el = elementList[i];

                if(el.elementType == ElementType.newShot)
                {
                    lastShot = el;
                }
            }

            if(lastShot != null)
            {
                Vector3 anchorPos = CityData.Instance.CityTileToRealpos(Vector2.zero); //Position of 0, 0

                lastShot.movement.Add(new CameraMovement { camPos = Player.Instance.transform.position - anchorPos, camEuler = CameraController.Instance.cam.transform.eulerAngles, anchor = AnchorType.blockCorner });
                UnityEditor.EditorUtility.SetDirty(this);
            }
        }
#endif
    }
}
