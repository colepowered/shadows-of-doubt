﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "district_data", menuName = "Database/District Preset")]

public class DistrictPreset : SoCustomComparison
{
	[Header("Generation")]
	[Tooltip("How important is it to generate this district first")]
	public Vector2 generationPriority;
	[Tooltip("Can there be more than 1 of these districts?")]
	public bool limitToOne = true;
	[Tooltip("Distict size as ratio of the city")]
	[Range(0.1f, 0.5f)]
	public float cityRatio = 0.2f;
	[Tooltip("Hard minimum size")]
	public int minimumSize = 1;
	[Tooltip("Hard maximum size")]
	public int maximumSize = 6;
	[Tooltip("This district must be located on the coast")]
	public bool mustBeOnCoast = false;
	[Tooltip("How important is it that this district is located near the centre of the city?")]
	[Range(-0.5f, 0.5f)]
	public float centreWeighting = 0f;

	[Header("Naming")]
    [Tooltip("Chance of alliteration with prefix. This will add words with the same letter to the suffix to increase the chances of picking them by this amount")]
    [Range(0, 15)]
    public int aliterationWeight = 1;
    [Space(5)]
    [Range(0f, 1f)]
    public float prefixOrSuffixChance = 0.8f;
    [Tooltip("Use this name list to pick a prefix")]
    [ReorderableList]
    public List<string> prefixList = new List<string>();
    [Range(0f, 1f)]
    public float mainChance = 1f;
    [Tooltip("Use this name list to pick a main name")]
    [ReorderableList]
    public List<string> mainNamingList = new List<string>();
	[Tooltip("Append a random selection of this suffix list to the name")]
    [ReorderableList]
    public List<string> suffixList = new List<string>();

    [Header("Composition")]
	public BuildingPreset.Density minimumDensity = BuildingPreset.Density.low;
	public BuildingPreset.Density maximumDensity = BuildingPreset.Density.veryHigh;
	public BuildingPreset.LandValue minimumLandValue = BuildingPreset.LandValue.veryLow;
	public BuildingPreset.LandValue maximumLandValue = BuildingPreset.LandValue.veryHigh;

	[Space(7)]
	[Tooltip("Affect the ethnicity of the citizens in this district...")]
	public bool affectEthnicity = false;
	[EnableIf("affectEthnicity")]
	public List<SocialStatistics.EthnicityFrequency> ethnicityFrequencyModifiers = new List<SocialStatistics.EthnicityFrequency>();

	[Header("Environment")]
	public SessionData.SceneProfile sceneProfile = SessionData.SceneProfile.outdoors;
	[Tooltip("Change street light area colours")]
	public bool alterStreetAreaLighting = true;
	[EnableIf("alterStreetAreaLighting")]
	public List<Color> possibleColours = new List<Color>();
	[Tooltip("This is used in combination with the following to adjust street area lighting")]
	[EnableIf("alterStreetAreaLighting")]
	public AffectStreetAreaLights lightOperation = AffectStreetAreaLights.lerp;
	[EnableIf("alterStreetAreaLighting")]
	public float lightAmount = 1f;
	[Tooltip("This is added to brightness")]
	[EnableIf("alterStreetAreaLighting")]
	public float brightnessModifier = 0f;

	public enum AffectStreetAreaLights { lerp, multiply, add};

	[Header("Debug")]
	public DistrictPreset copyFrom;

	[Button]
	public void CopyFrom()
	{
		if(copyFrom != null)
		{
			ethnicityFrequencyModifiers = new List<SocialStatistics.EthnicityFrequency>(copyFrom.ethnicityFrequencyModifiers);
			aliterationWeight = copyFrom.aliterationWeight;
			prefixOrSuffixChance = copyFrom.prefixOrSuffixChance;
			prefixList = new List<string>(copyFrom.prefixList);
			mainChance = copyFrom.mainChance;
			mainNamingList = new List<string>(copyFrom.mainNamingList);
			suffixList = new List<string>(copyFrom.suffixList);

#if UNITY_EDITOR
			UnityEditor.EditorUtility.SetDirty(this);
#endif
		}
	}
}
