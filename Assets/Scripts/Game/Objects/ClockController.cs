﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClockController : MonoBehaviour
{
    public InteractableController ic;
    public Transform hourHand;
    public Transform minuteHand;
    public Animator hourlyAnimation;
    [ReadOnly]
    public float animateTimer = 0f;

    private void Start()
    {
        if(ic == null) ic = this.gameObject.GetComponentInParent<InteractableController>();

        if(hourlyAnimation != null)
        {
            SessionData.Instance.OnHourChange += OnHourChange;
        }
    }

    private void OnDestroy()
    {
        if (hourlyAnimation != null)
        {
            hourlyAnimation.SetBool("Animate", false);
            SessionData.Instance.OnHourChange -= OnHourChange;
        }
    }

    public void OnHourChange()
    {
        if(hourlyAnimation != null)
        {
            hourlyAnimation.SetBool("Animate", true);

            if (ic.interactable.preset.chimeEqualToHour)
            {
                //Send delayed sounds...
                int hour = Mathf.FloorToInt(SessionData.Instance.decimalClock);
                animateTimer = ic.interactable.preset.chimeDelay * hour;
            }
            //Just chime once
            else
            {
                animateTimer = ic.interactable.preset.chimeDelay;
            }
        }
    }

    void Update()
    {
        float hour = SessionData.Instance.decimalClock;
        if (hour > 12f) hour = SessionData.Instance.decimalClock - 12f; //12H format
        hourHand.localEulerAngles = new Vector3(0, 0, (hour / 12f) * 360f);

        float minutes = SessionData.Instance.decimalClock - Mathf.Floor(SessionData.Instance.decimalClock);
        minuteHand.localEulerAngles = new Vector3(0, 0, minutes * 360f);

        if(animateTimer > 0f)
        {
            animateTimer -= Time.deltaTime;

            if(animateTimer <= 0f)
            {
                hourlyAnimation.SetBool("Animate", false);
            }
        }
    }
}
