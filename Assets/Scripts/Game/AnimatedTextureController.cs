﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class AnimatedTextureController : MonoBehaviour
{
    [Header("Components")]
    public Renderer animatedRenderer;

    [Header("Settings")]
    [Tooltip("How long it takes the animation to complete")]
    public float animationCycleTime;
    [Tooltip("Count of X/Y frames within the base texture.")]
    public Vector2 texTileCount;
    [Tooltip("Play this when active")]
    public bool playOnStart;
    [Tooltip("Destroy self on end")]
    public bool destroyOnEnd = true;
    [Tooltip("Destroy if inactive")]
    public bool destroyIfInactive = true;

    [Header("Billboarding")]
    [Tooltip("Always face the camera")]
    public bool billboardingOn = false;
    [EnableIf("billboardingOn")]
    [Tooltip("Only face towards camera when the animation starts playing...")]
    public bool faceOnStartOnly = false;
    [EnableIf("billboardingOn")]
    public SpecialCase specialCase = SpecialCase.fireSmoke;

    public enum SpecialCase { fireSmoke};

    [Header("Extra VFX")]
    public bool alterEmission = true;
    [ColorUsageAttribute(true, true)]
    public Color startingEmission;
    [ColorUsageAttribute(true, true)]
    public Color midEmission;
    [ColorUsageAttribute(true, true)]
    public Color endEmission;
    [Space(5)]
    public bool alterScale = true;
    public Transform parentScaleTransform;
    public AnimationCurve scaleX;
    public AnimationCurve scaleY;
    public AnimationCurve scaleZ;

    [Header("Audio")]
    public AudioEvent triggerAudio;
    public bool useSpeedOfSound = true;

    [Header("State")]
    private float animtionTimer = 0f;
    public float animationProgress = 0f; //Normalized progress
    public bool isPlaying;
    public bool loop;
    public float nextFrameTimer = 0f;
    public int spriteCursorX = -1;
    public int spriteCursorY = -1;

    [Button]
    public virtual void Play()
    {
        if (!isPlaying)
        {
            if(triggerAudio != null)
            {
                if(useSpeedOfSound)
                {
                    //Calculate distance
                    float dist = Vector3.Distance(CameraController.Instance.transform.position, this.transform.position);

                    //Calculate speed of sound delay
                    float soundDelay = dist / AudioController.Instance.speedOfSound;

                    AudioController.Instance.PlayOneShotDelayed(soundDelay, triggerAudio, null, null, this.transform.position);
                }
                else
                {
                    AudioController.Instance.PlayWorldOneShot(triggerAudio, null, null, this.transform.position);
                }
            }

            nextFrameTimer = 0f;
            spriteCursorX = -1;
            spriteCursorY = (int)texTileCount.y;

            animtionTimer = 0f;
            animationProgress = 0f;

            if(billboardingOn && faceOnStartOnly)
            {
                Billboard();
            }

            isPlaying = true;
            this.enabled = true;
        }
    }

    [Button]
    public virtual void Stop()
    {
        //Game.Log ("STOP: isPlaying="+isPlaying+"  stopSignal="+stopSignal);
        if (isPlaying)
        {
            isPlaying = false;
            this.enabled = false;
        }
    }

    private void OnDisable()
    {
        if(destroyIfInactive)
        {
            Destroy(this.gameObject);
        }
    }

    // ----------
    protected virtual void Awake()
    {
        if (animatedRenderer == null)
        {
            animatedRenderer = this.gameObject.GetComponent<Renderer>();

            if(animatedRenderer == null)
            {
                animatedRenderer = this.gameObject.GetComponentInChildren<Renderer>();
            }
        }

        ApplyScale(new Vector2(1.0f / texTileCount.x, 1.0f / texTileCount.y));
        isPlaying = false;
    }

    protected virtual void Start()
    {
        if (playOnStart)
            Play();
    }

    void Update()
    {
        //Play loop
        if(isPlaying)
        {
            animtionTimer += Time.deltaTime;
            animationProgress = Mathf.Clamp01(animtionTimer / animationCycleTime);

            if(nextFrameTimer > 0f)
            {
                nextFrameTimer -= Time.deltaTime;
            }

            //Load the next frame
            if(nextFrameTimer <= 0f)
            {
                //Try and progress to the next x coordinate
                spriteCursorX++;

                //Next row
                if(spriteCursorX >= texTileCount.x)
                {
                    spriteCursorY--;
                    spriteCursorX = 0;

                    if(spriteCursorY <= -1)
                    {
                        //Animation is finished...
                        if (destroyOnEnd)
                        {
                            Destroy(this.gameObject);
                        }

                        if (loop)
                        {
                            //Reset animation
                            spriteCursorX = -1;
                            spriteCursorY = (int)texTileCount.y;
                            nextFrameTimer = 0;

                            if (billboardingOn && faceOnStartOnly)
                            {
                                Billboard();
                            }

                            animtionTimer = 0f;
                            animationProgress = 0f;
                        }
                        else
                        {
                            Stop();
                        }
                    }
                }

                if (spriteCursorX >= 0 && spriteCursorY >= 0)
                {
                    ApplyOffset(new Vector2(1.0f / texTileCount.x * spriteCursorX, 1.0f / texTileCount.y * spriteCursorY));
                }

                nextFrameTimer += animationCycleTime / (texTileCount.x * texTileCount.y); //Set delay for next frame
            }

            //Do extra VFX
            if(alterEmission)
            {
                Color setEm = startingEmission;

                if (animationProgress <= 0.5f)
                {
                    setEm = Color.Lerp(startingEmission, midEmission, animationProgress * 2f);
                }
                else
                {
                    setEm = Color.Lerp(midEmission, endEmission, (animationProgress - 0.5f) * 2f);
                }

                if(animatedRenderer.material.HasProperty("_EmissiveColor"))
                {
                    animatedRenderer.material.SetColor("_EmissiveColor", setEm);
                }
            }

            if(alterScale)
            {
                parentScaleTransform.localScale = new Vector3(scaleX.Evaluate(animationProgress), scaleY.Evaluate(animationProgress), scaleZ.Evaluate(animationProgress));
            }
        }

        //Billboard the sprite
        if (billboardingOn && !faceOnStartOnly)
        {
            Billboard();
        }
    }

    void Billboard()
    {
        transform.rotation = CameraController.Instance.cam.transform.rotation;

        if(specialCase == SpecialCase.fireSmoke)
        {
            //Rotate Y first
            Vector3 loc = new Vector3(0, transform.localEulerAngles.y, 0);

            //Now rotate X
            transform.rotation = Quaternion.Slerp(Quaternion.identity, CameraController.Instance.cam.transform.rotation, 0.4f);

            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, loc.y, 0f);
        }
    }

    protected virtual void ApplyOffset(Vector2 offset)
    {
        if (animatedRenderer != null)
        {
            if (animatedRenderer.material.HasProperty("_BaseColorMap"))
            {
                animatedRenderer.material.SetTextureOffset("_BaseColorMap", offset);
            }
        }
    }

    protected virtual void ApplyScale(Vector2 scale)
    {
        if (animatedRenderer != null)
        {
            if (animatedRenderer.material.HasProperty("_BaseColorMap"))
            {
                animatedRenderer.material.SetTextureScale("_BaseColorMap", scale);
            }
        }
    }
}
