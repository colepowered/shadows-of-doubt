﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;

//This script contains a function that populates the map with buildings.
//Script pass 1
public class RoomsLoader : Creator
{
    public int connectionChunk = 75;
    public int cullTreeChunk = 2;

    [System.NonSerialized]
    public List<LoaderThread> threads = new List<LoaderThread>();

    public class LoaderThread
    {
        public Coroutine thread;
        public NewRoom room;
        public bool isDone = false;
    }

    //Singleton pattern
    private static RoomsLoader _instance;
    public static RoomsLoader Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public override void StartLoading()
    {
        Game.Log("CityGen: Connect rooms...");
        StartCoroutine("Load");
    }

    IEnumerator Load()
    {
        int cursor = 0;

        //Clear GL connections: Start from scratch
        foreach (NewGameLocation gl in CityData.Instance.gameLocationDirectory)
        {
            gl.entrances.Clear();
        }

        //Connect nodes: Do this on main thread
        while (cursor < CityData.Instance.roomDirectory.Count)
        {
            for (int i = 0; i < connectionChunk; i++)
            {
                //Break loop if complete
                if (cursor >= CityData.Instance.roomDirectory.Count) break;

                CityData.Instance.roomDirectory[cursor].ConnectNodes();

                //DEBUG: CHECK Entrances
                //There are entrances appearing in gamelocations that are not added to the rooms!
                //I don't understand why this is happening, but for now simply add the entrance to the room...
                foreach (NewNode.NodeAccess acc in CityData.Instance.roomDirectory[cursor].gameLocation.entrances)
                {
                    if (!acc.fromNode.room.entrances.Exists(item => item.fromNode == acc.fromNode && item.toNode == acc.toNode))
                    {
                        //Game.Log("An entrance in a gamelocation was not found in it's room! " + acc.fromNode.name + " > " + acc.toNode.name);
                        acc.fromNode.room.AddEntrance(acc.fromNode, acc.toNode);
                    }
                }

                cursor++;
            }

            CityConstructor.Instance.loadingProgress = ((float)cursor / (float)CityData.Instance.roomDirectory.Count) * 0.1f;
            yield return null;
        }

        //For the culling tree generation to go at full speed we first need to generate building directional trees
        foreach(NewBuilding b in CityData.Instance.buildingDirectory)
        {
            b.CalculateDirectionalCullingTrees();
        }

        cursor = 0;
        //int gcCounter = 0;

        int phase2Chunk = connectionChunk;
        if (CityConstructor.Instance.generateNew) phase2Chunk = cullTreeChunk;

        //To make sure optimization works, sort rooms by YPos (highest first)
        //CityData.Instance.roomDirectory.Sort((p2, p1) => p1.worldPos.y.CompareTo(p2.worldPos.y));

        threads = new List<LoaderThread>();

        //Generate culling trees
        while (cursor < CityData.Instance.roomDirectory.Count || threads.Count > 0)
        {
            if(CityConstructor.Instance.generateNew)
            {
                //Multi-threaded loading
                if(cursor < CityData.Instance.roomDirectory.Count)
                {
                    for (int i = threads.Count; i < Game.Instance.maxThreads; i++)
                    {
                        //Break loop if complete
                        if (cursor >= CityData.Instance.roomDirectory.Count) break;

                        LoaderThread newThread = new LoaderThread { room = CityData.Instance.roomDirectory[cursor] };
                        newThread.thread = StartCoroutine(ThreadedRoomConnect(newThread));
                        cursor++;
                    }
                }
            }
            else
            {
                for (int i = 0; i < phase2Chunk; i++)
                {
                    //Break loop if complete
                    if (cursor >= CityData.Instance.roomDirectory.Count) break;

                    //Load culling trees here...
                    CityData.Instance.roomDirectory[cursor].LoadCullingTree();

                    //Generate entrance weights & job pathing data
                    if (CityData.Instance.roomDirectory[cursor].gameLocation.thisAsAddress != null)
                    {
                        if (!CityData.Instance.roomDirectory[cursor].gameLocation.thisAsAddress.generatedEntranceWeights)
                        {
                            //Precompute entrance weights...
                            foreach (NewNode.NodeAccess access in CityData.Instance.roomDirectory[cursor].gameLocation.entrances)
                            {
                                access.PreComputeEntranceWeights();
                            }

                            CityData.Instance.roomDirectory[cursor].gameLocation.thisAsAddress.generatedEntranceWeights = true;

                            //Generate job system data
                            if (Game.Instance.useJobSystem)
                            {
                                CityData.Instance.roomDirectory[cursor].gameLocation.thisAsAddress.GenerateJobPathingData();
                            }
                        }
                    }

                    cursor++;
                }
            }

            CityConstructor.Instance.loadingProgress = ((float)cursor / (float)CityData.Instance.roomDirectory.Count) * 0.9f + 0.1f;
            yield return null;
        }

        //Generate weights for street
        foreach (NewNode.NodeAccess access in PathFinder.Instance.streetEntrances)
        {
            access.PreComputeEntranceWeights();
        }

        //Clear building cache
        foreach (NewBuilding b in CityData.Instance.buildingDirectory)
        {
            b.directionalCullingTrees = null;
        }

        //Generate job data for streets
        if (Game.Instance.useJobSystem)
        {
            PathFinder.Instance.GenerateJobPathingData();
        }

        //Complete!
        SetComplete();
    }

    //Threaded room connection
    IEnumerator ThreadedRoomConnect(LoaderThread loaderReference)
    {
        //Game.Log("CityGen: Started new thread for room cursor " + roomCursor);
        loaderReference.isDone = false;

        threads.Add(loaderReference);
        Game.Log("CityGen: Room connect threads: " + threads.Count);

        NewRoom room = loaderReference.room;

        Thread thread = new Thread(() =>
        {
            //Generate culling tree
            room.GenerateCullingTree();

            loaderReference.isDone = true;
        });

        thread.Start();

        //Do nothing on each frame until the thread is done
        while (!loaderReference.isDone || thread.IsAlive)
        {
            yield return null;
        }

        threads.Remove(loaderReference); //Remove thread reference
        Game.Log("CityGen: Room connect threads: " + threads.Count);
    }
}
