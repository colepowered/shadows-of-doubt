﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using NaughtyAttributes;
using System.Runtime.ExceptionServices;
using UnityEngine.Rendering;
using UnityEditor;
using System.Linq;

public class SceneCapture : MonoBehaviour
{
    [Header("Capture")]
    public RenderTexture evidenceRenderTexturePrefab;
    public RenderTexture surveillanceRenderTexturePrefab;
    public float evidenceFoV = 70f;
    public float surveillanceFov = 90f;

    //The scene the player is currently looking at
    [System.NonSerialized]
    public SceneRecorder.SceneCapture currrentlyViewing;

    [Header("Cache")]
    [Tooltip("The max number of cached evidence photos")]
    public int maxEvidenceCache = 32;
    [ReadOnly]
    public int cachedEvidencePhotos = 0;
    public Dictionary<Evidence, PhotoCache> cachedRenders = new Dictionary<Evidence, PhotoCache>();

    [Space(7)]
    public int maxSurveillanceCache = 16;
    [ReadOnly]
    public int cachedSurveillancePhotos = 0;
    public Dictionary<SceneRecorder.SceneCapture, PhotoCache> cachedSurveillance = new Dictionary<SceneRecorder.SceneCapture, PhotoCache>();

    [System.Serializable]
    public class PhotoCache
    {
        public Texture2D img;
        public float lastUsed;
        public List<ActorScreenPosition> actorSP = new List<ActorScreenPosition>();
        //public List<Human> visibleHumans;
    }

    public struct ActorScreenPosition
    {
        public Human human;
        public Vector2 screenPoint;
    }

    [Header("Photo Room")]
    public GameObject photoRoomParent;
    public Transform cameraTransform;
    public Transform itemTransform;

    public enum PostProcessingProfile { captureNormal, captureCCTV };

    //Singleton pattern
    private static SceneCapture _instance;
    public static SceneCapture Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        photoRoomParent.SetActive(false); //Disable photo room
    }

    public Texture2D CapturePhotoOfEvidence(Evidence ev, bool forceNew = false)
    {
        if (!SessionData.Instance.startedGame) return null; //Game must be started for this to work properly...

        Game.Log("Interface: Request capture for " + ev.GetNameForDataKey(Evidence.DataKey.name) + "...");

        if(cachedRenders.ContainsKey(ev) && !forceNew)
        {
            Game.Log("Interface: Capture is cached already...");
            cachedRenders[ev].lastUsed = SessionData.Instance.gameTime; //Update last used
            return cachedRenders[ev].img;
        }

        //Is there enough room in the cache?
        if (cachedRenders.Count >= maxEvidenceCache)
        {
            Game.Log("Interface: Max cache exceeded, removing oldest render...");

            //Find the oldest render
            Evidence oldestKey = null;
            float lastUsed = Mathf.Infinity;

            foreach (KeyValuePair<Evidence, PhotoCache> pair in cachedRenders)
            {
                if (pair.Value.lastUsed < lastUsed)
                {
                    oldestKey = pair.Key;
                    lastUsed = pair.Value.lastUsed;
                }
            }

            if (oldestKey != null)
            {
                cachedRenders.Remove(oldestKey);
                cachedEvidencePhotos = cachedRenders.Count;
            }
        }

        //Calculate relative position and convert it to world position...
        Vector3 worldPos = Vector3.zero;
        Vector3 worldEuler = Vector3.zero;
        NewNode nearestNode = null;

        Transform objectMove = null;
        Vector3 objectMovePos = Vector3.zero;
        Quaternion objectMoveRot = Quaternion.identity;
        bool objectVis = true;
        int objectLayer = 0;
        Transform objectParent = null;

        //bool citizenModelVis = false;
        bool citizenOutlineActive = false;
        Human actor = ev.controller as Human;

        //Hide elements
        List<Transform> hide = new List<Transform>();

        bool basicMode = false;

        //Use some specific rules to get cam positions...
        if (ev.preset.captureRules == EvidencePreset.CaptureRules.building)
        {
            NewBuilding building = ev.controller as NewBuilding;

            if(building != null)
            {
                building.SetDisplayBuildingModel(true, false); //Make sure building is displayed

                Transform camTransform = building.transform;
                if (building.buildingModelBase != null) camTransform = building.buildingModelBase.transform;

                if (building.preset.overrideEvidencePhotoSettings)
                {
                    Game.Log("Interface: Render using overriden cam pos " + building.preset.relativeCamPhotoPos + building.preset.relativeCamPhotoEuler);
                    worldPos = camTransform.TransformPoint(building.preset.relativeCamPhotoPos);
                    worldEuler = camTransform.eulerAngles + building.preset.relativeCamPhotoEuler;
                }
                else
                {
                    worldPos = camTransform.TransformPoint(ev.preset.relativeCamPhotoPos);
                    worldEuler = camTransform.eulerAngles + ev.preset.relativeCamPhotoEuler;
                }
            }
        }
        else if(ev.preset.captureRules == EvidencePreset.CaptureRules.location)
        {
            NewGameLocation location = ev.controller as NewGameLocation;

            if(location != null)
            {
                NewAddress ad = location.thisAsAddress;

                if(ad != null && ad.building != null)
                {
                    ad.building.SetDisplayBuildingModel(true, false); //Make sure building is displayed

                    Transform camTransform = ad.building.transform;

                    if (ad.building.buildingModelBase != null) camTransform = ad.building.buildingModelBase.transform;

                    if (ad.building.preset.overrideEvidencePhotoSettings)
                    {
                        Game.Log("Interface: Render using overriden cam pos " + ad.building.preset.relativeCamPhotoPos + ad.building.preset.relativeCamPhotoEuler);
                        worldPos = camTransform.TransformPoint(ad.building.preset.relativeCamPhotoPos);
                        worldEuler = camTransform.eulerAngles + ad.building.preset.relativeCamPhotoEuler;
                    }
                    else
                    {
                        worldPos = camTransform.TransformPoint(ev.preset.relativeCamPhotoPos);
                        worldEuler = camTransform.eulerAngles + ev.preset.relativeCamPhotoEuler;
                    }
                }


                //nearestNode = ad.nodes[0];

                ////Use fallback of building
                //Transform camTransform = ad.building.transform;
                //if (ad.building.buildingModelBase != null) camTransform = ad.building.buildingModelBase.transform;

                //if (ad.building.preset.overrideEvidencePhotoSettings)
                //{
                //    worldPos = camTransform.TransformPoint(ad.building.preset.relativeCamPhotoPos);
                //    worldEuler = camTransform.eulerAngles + ad.building.preset.relativeCamPhotoEuler;
                //}
                //else
                //{
                //    worldPos = camTransform.TransformPoint(ev.preset.relativeCamPhotoPos);
                //    worldEuler = camTransform.eulerAngles + ev.preset.relativeCamPhotoEuler;
                //}

                //if (ad != null && ad.entrances.Count > 0)
                //{
                //    NewDoor door = null;

                //    if(ad.streetAccess != null && ad.streetAccess.door != null)
                //    {
                //        door = ad.streetAccess.door;
                //    }
                //    else
                //    {
                //        foreach (NewNode.NodeAccess acc in ad.entrances)
                //        {
                //            if (acc.door != null)
                //            {
                //                door = acc.door;
                //                break;
                //            }
                //        }
                //    }

                //    if(door != null)
                //    {
                //        if(ad.preset.overrideEvidencePhotoSettings)
                //        {
                //            Game.Log("Interface: Render using overriden cam pos " + ad.preset.relativeCamPhotoPos + ad.preset.relativeCamPhotoEuler);
                //            worldPos = door.transform.TransformPoint(ad.preset.relativeCamPhotoPos);
                //            worldEuler = door.transform.eulerAngles + ad.preset.relativeCamPhotoEuler;
                //        }
                //        else
                //        {
                //            worldPos = door.transform.TransformPoint(ev.preset.relativeCamPhotoPos);
                //            worldEuler = door.transform.eulerAngles + ev.preset.relativeCamPhotoEuler;
                //        }
                //    }
                //}
            }
        }
        else if(ev.preset.captureRules == EvidencePreset.CaptureRules.item)
        {
            if(ev.interactable != null)
            {
                nearestNode = ev.interactable.node;

                if(ev.interactable.controller != null)
                {
                    if (!ev.interactable.preset.overrideEvidencePhotoSettings)
                    {
                        worldPos = ev.interactable.controller.transform.TransformPoint(ev.preset.relativeCamPhotoPos);
                        worldEuler = ev.interactable.controller.transform.eulerAngles + ev.preset.relativeCamPhotoEuler;
                    }
                    else
                    {
                        worldPos = ev.interactable.controller.transform.TransformPoint(ev.interactable.preset.relativeCamPhotoPos);
                        worldEuler = ev.interactable.controller.transform.eulerAngles + ev.interactable.preset.relativeCamPhotoEuler;
                    }
                }
                else
                {
                    //If this hasn't spawned yet we need to use a matrix to calculate the offset positions...
                    Vector3 locScale = Vector3.one;
                    if (ev.interactable.preset.prefab != null) locScale = ev.interactable.preset.prefab.transform.localScale;

                    if (ev.interactable.furnitureParent != null)
                    {
                        Vector3 prefabScale = ev.interactable.furnitureParent.furniture.prefab.transform.localScale;
                        locScale = new Vector3(ev.interactable.furnitureParent.scaleMultiplier.x * prefabScale.x, ev.interactable.furnitureParent.scaleMultiplier.y * prefabScale.y, ev.interactable.furnitureParent.scaleMultiplier.z * prefabScale.z);
                    }

                    //This will transform the point from the original transform location
                    Matrix4x4 m = Matrix4x4.TRS(ev.interactable.wPos, Quaternion.Euler(ev.interactable.wEuler), locScale);

                    if (!ev.interactable.preset.overrideEvidencePhotoSettings)
                    {
                        worldPos = m.MultiplyPoint3x4(ev.preset.relativeCamPhotoPos);
                        worldEuler = ev.interactable.wEuler + ev.preset.relativeCamPhotoEuler;
                    }
                    else
                    {
                        worldPos = m.MultiplyPoint3x4(ev.interactable.preset.relativeCamPhotoPos);
                        worldEuler = ev.interactable.wEuler + ev.interactable.preset.relativeCamPhotoEuler;
                    }
                }
            }
        }
        else if(ev.preset.captureRules == EvidencePreset.CaptureRules.citizen)
        {
            basicMode = true;

            //Enable photo room
            photoRoomParent.SetActive(true);

            if(actor != null)
            {
                if (actor.outline != null)
                {
                    citizenOutlineActive = actor.outline.outlineActive;
                    actor.outline.SetOutlineActive(false);
                }

                worldPos = cameraTransform.position;
                worldEuler = cameraTransform.eulerAngles;
                nearestNode = actor.currentNode;

                //Use actor head mesh
                if(actor.outfitController == null)
                {
                    Game.LogError("Unable to find character's outfit controller (" + actor.name + ")");
                }

                objectMove = actor.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.Head);

                if (actor.outfitController != null && actor.outfitController.lod != null)
                {
                    actor.outfitController.lod.ForceLOD(0);
                }

                if(objectMove != null)
                {
                    Game.Log("Found head anchor (" + actor.name + ") present on layer " + objectMove.gameObject.layer);

                    //Move head
                    objectLayer = objectMove.gameObject.layer;
                    objectVis = objectMove.gameObject.activeSelf;
                    objectMoveRot = objectMove.transform.rotation;
                    objectMovePos = objectMove.transform.position;
                    objectParent = objectMove.transform.parent;

                    objectMove.SetParent(null);
                    objectMove.gameObject.layer = 0;
                    objectMove.gameObject.SetActive(true);
                    objectMove.rotation = itemTransform.rotation * Quaternion.Euler(ev.preset.relativeCamPhotoEuler);
                    objectMove.position = itemTransform.position - ev.preset.relativeCamPhotoPos;
                }

                //Reset eyes
                if(actor.outfitController != null)
                {
                    actor.outfitController.rightPupil.localScale = new Vector3(0.02f, 0.02f, 0.02f);
                    actor.outfitController.leftPupil.localScale = new Vector3(0.02f, 0.02f, 0.02f);
                    actor.outfitController.rightPupil.localPosition = new Vector3(0.045f, 0, 0);
                    actor.outfitController.leftPupil.localPosition = new Vector3(-0.045f, 0, 0);
                }

                //Hide elements
                hide.AddRange(Toolbox.Instance.GetTagsWithinTransform(objectMove.transform, "Interactable"));

                //Set orthographic
                CameraController.Instance.cam.orthographic = true;
            }
        }

        if (objectMove != null)
        {
            //Hide elements
            hide.AddRange(Toolbox.Instance.GetTagsWithinTransform(objectMove.transform, "SceneCapHide"));
            hide.Remove(objectMove);

            for (int i = 0; i < hide.Count; i++)
            {
                Transform t = hide[i];

                if(t.gameObject.activeSelf)
                {
                    t.gameObject.SetActive(false);
                }
                else
                {
                    hide.RemoveAt(i);
                    i--;
                }
            }
        }

        PhotoCache newCache = new PhotoCache();

        newCache.img = CaptureScene(worldPos, worldEuler, Toolbox.Instance.mugShotCaptureLayerMask, ev.preset.changeTimeOfDay, ev.preset.captureTimeOfDay, evidenceRenderTexturePrefab, evidenceFoV, passNode: nearestNode, useCaptureLight: ev.preset.useCaptureLight, captureProfile: PostProcessingProfile.captureNormal, basicMode: basicMode);
        newCache.lastUsed = SessionData.Instance.gameTime;

        cachedRenders.Add(ev, newCache);
        cachedEvidencePhotos++;

        if(photoRoomParent.activeSelf)
        {
            photoRoomParent.SetActive(false);
        }

        if (CameraController.Instance.cam.orthographic)
        {
            CameraController.Instance.cam.orthographic = false;
        }

        //Restore hidden
        if (hide != null)
        {
            foreach(Transform t in hide)
            {
                t.gameObject.SetActive(true);
            }
        }

        if (actor != null)
        {
            //actor.SetModelParentVisibility(citizenModelVis, "Restoring after evidence capture");
            if (actor.outline != null) actor.outline.SetOutlineActive(citizenOutlineActive);

            if (actor.outfitController != null && actor.outfitController.lod != null)
            {
                actor.outfitController.lod.ForceLOD(-1);
            }
        }

        if(objectMove != null)
        {
            objectMove.gameObject.SetActive(objectVis);
            if (objectParent != null) objectMove.SetParent(objectParent);
            objectMove.gameObject.layer = objectLayer;

            objectMove.position = objectMovePos;
            objectMove.rotation = objectMoveRot;
        }

        Player.Instance.UpdateCurrentBuildingModelVisibility();

        return newCache.img;
    }

    public Texture2D GetSurveillanceScene(SceneRecorder.SceneCapture scene, bool saveToCache = true)
    {
        if (scene == null)
        {
            Game.LogError("Gameplay: Scene is null for GetSurveillanceScene!");
            return null;
        }

        SceneRecorder recorder = scene.recorder;
        //visibleHumans = new List<Human>();

        if (recorder == null)
        {
            Game.LogError("Gameplay: Recorder is null for GetSurveillanceScene!");
            return null;
        }

        if (cachedSurveillance.ContainsKey(scene))
        {
            Game.Log("Gameplay: Capture is cached already for " + scene + " " + SessionData.Instance.TimeStringOnDay(scene.t, true, true) + "...");
            cachedSurveillance[scene].lastUsed = SessionData.Instance.gameTime; //Update last used

            //Load actor screen positions
            foreach(ActorScreenPosition sp in cachedSurveillance[scene].actorSP)
            {
                sp.human.lastUsedCCTVScreenPoint = sp.screenPoint;
            }

            //visibleHumans = cachedSurveillance[scene].visibleHumans;
            return cachedSurveillance[scene].img;
        }

        //Is there enough room in the cache?
        if (cachedSurveillance.Count >= maxSurveillanceCache)
        {
            Game.Log("Gameplay: Max cache exceeded (" + maxSurveillanceCache +"), removing oldest render...");

            //Find the oldest render
            SceneRecorder.SceneCapture oldestKey = null;
            float lastUsed = Mathf.Infinity;

            foreach (KeyValuePair<SceneRecorder.SceneCapture, PhotoCache> pair in cachedSurveillance)
            {
                if (pair.Value.lastUsed < lastUsed)
                {
                    oldestKey = pair.Key;
                    lastUsed = pair.Value.lastUsed;
                }
            }

            if (oldestKey != null)
            {
                cachedSurveillance.Remove(oldestKey);
                cachedSurveillancePhotos = cachedSurveillance.Count;
            }
        }

        Game.Log("Gameplay: Grabbing capture for surveillance scene...");
        Game.Log("Gameplay: Loading data from scene capture...");

        List<SceneRecorder.RoomCapture> originalLightState = new List<SceneRecorder.RoomCapture>();

        //Set lights to captured state if different to current; save original state to revert back to
        foreach (SceneRecorder.RoomCapture rCap in scene.rCap)
        {
            NewRoom r = rCap.GetRoom();

            if (r != null && r.mainLightStatus != rCap.light)
            {
                SceneRecorder.RoomCapture originalR = new SceneRecorder.RoomCapture { id = rCap.id, light = r.mainLightStatus };
                originalLightState.Add(originalR);
                r.SetMainLights(rCap.light, "Capture", forceInstant: true);
            }
        }

        //Record door states and set new ones...
        List<SceneRecorder.DoorCapture> originalDoorState = new List<SceneRecorder.DoorCapture>();

        //Set door positions
        foreach (SceneRecorder.DoorCapture d in scene.dCap)
        {
            NewDoor getDoor = d.GetDoor();

            if (getDoor != null)
            {
                originalDoorState.Add(new SceneRecorder.DoorCapture(getDoor));
                getDoor.SetOpen(d.a, null, true);
            }
        }

        //Load object proxies
        foreach (SceneRecorder.InteractableCapture i in scene.oCap)
        {
            i.Load();
        }

        //Record object states and set new ones...
        List<SceneRecorder.InteractableStateCapture> originalObjectState = new List<SceneRecorder.InteractableStateCapture>();

        foreach (SceneRecorder.InteractableStateCapture st in scene.oSCap)
        {
            Interactable i = st.GetInteractable();

            if (i != null)
            {
                if (Player.Instance.computerInteractable != null && Player.Instance.computerInteractable == i) continue; //Ignore computer interactable as this could mess things up!

                if (i.sw0 != st.sw)
                {
                    originalObjectState.Add(new SceneRecorder.InteractableStateCapture(i));
                    i.SetSwitchState(st.sw, null, false, false, true);
                }
            }
        }

        //We should now have everything saved that needs to be saved. We can now position things for the scene replication...
        PhotoCache newCache = new PhotoCache();

        Game.Log("Gameplay: Scene capture execution...");
        newCache.img = CaptureScene(scene.GetCaptureWorldPosition(), scene.GetCaptureWorldRotation(), Toolbox.Instance.sceneCaptureLayerMask, true, scene.GetDecimalClock(), surveillanceRenderTexturePrefab, ref scene.aCap, out newCache.actorSP, surveillanceFov, null, useCaptureLight: false, captureProfile: PostProcessingProfile.captureCCTV);
        newCache.lastUsed = SessionData.Instance.gameTime;

        if(saveToCache)
        {
            cachedSurveillance.Add(scene, newCache);
            cachedSurveillancePhotos++;
        }

        Game.Log("Gameplay: Resetting data from screen capture...");

        //Now reset everything!
        foreach (SceneRecorder.RoomCapture rCap in originalLightState)
        {
            NewRoom r = rCap.GetRoom();
            r.SetMainLights(rCap.light, "Capture Reset", forceInstant: true);
        }

        //Reset door positions
        foreach (SceneRecorder.DoorCapture d in originalDoorState)
        {
            NewDoor getDoor = d.GetDoor();

            if (getDoor != null)
            {
                getDoor.SetOpen(d.a, null, true);
            }
        }

        //Reset object states
        foreach(SceneRecorder.InteractableStateCapture st in originalObjectState)
        {
            st.Load();
        }

        //Unload object proxies
        foreach (SceneRecorder.InteractableCapture i in scene.oCap)
        {
            i.Unload();
        }

        Game.Log("Gameplay: Successfully captured scene " + SessionData.Instance.TimeStringOnDay(scene.t, true, true));
        Player.Instance.UpdateCulling();

        return newCache.img;
    }

    public Texture2D CaptureScene(Vector3 pos, Vector3 euler, int layerMask, bool changeTimeOfDay, float decimalClock, RenderTexture renderPrefab, float fov = 70f, List<Interactable> forceHide = null, NewNode passNode = null, bool useCaptureLight = true, bool basicMode = false, bool ignoreEarlyCapError = false, PostProcessingProfile captureProfile = PostProcessingProfile.captureNormal)
    {
        List<SceneRecorder.ActorCapture> humanRef = new List<SceneRecorder.ActorCapture>();
        return CaptureScene(pos, euler, layerMask, changeTimeOfDay, decimalClock, renderPrefab, ref humanRef, out _, fov, forceHide, passNode, useCaptureLight, basicMode, ignoreEarlyCapError, captureProfile);
    }

    public Texture2D CaptureScene(Vector3 pos, Vector3 euler, int layerMask, bool changeTimeOfDay, float decimalClock, RenderTexture renderPrefab, ref List<SceneRecorder.ActorCapture> humanRef, out List<ActorScreenPosition> actorScreenPointCapture, float fov = 70f, List<Interactable> forceHide = null, NewNode passNode = null, bool useCaptureLight = true, bool basicMode = false, bool ignoreEarlyCapError = false, PostProcessingProfile captureProfile = PostProcessingProfile.captureNormal)
    {
        if(!SessionData.Instance.startedGame && !ignoreEarlyCapError)
        {
            Game.LogError("Requesting scene capture before game has started: This could result in some blank captures...");
        }

        Game.Log("Capturing scene with profile " + captureProfile.ToString() + " Pos: " + pos + " euler: " + euler);

        RenderTexture renderTextureInstance = Instantiate(renderPrefab) as RenderTexture;

        actorScreenPointCapture = new List<ActorScreenPosition>();

        int camLayer = CameraController.Instance.cam.cullingMask;
        CameraController.Instance.cam.cullingMask = layerMask;

        float savedFoV = CameraController.Instance.cam.fieldOfView;
        CameraController.Instance.cam.fieldOfView = fov;

        //Turn off 3D UI //Causes problems with arms anim controller and should be covered by the layer mask anyway...
        //InterfaceControls.Instance.Interface3DCanvas.gameObject.SetActive(false);

        //Position main camera at cam point, render to texture.
        //Save cam pos
        Vector3 savedCamPos = CameraController.Instance.cam.transform.position;
        Vector3 savedCamEuler = CameraController.Instance.cam.transform.eulerAngles;

        //Activate scene profile for capture
        if(captureProfile == PostProcessingProfile.captureNormal)
        {
            CityControls.Instance.captureSceneNormal.volume.priority = 999;
            CityControls.Instance.captureSceneNormal.volume.weight = 1;
            CityControls.Instance.captureSceneNormal.objectRef.SetActive(true);
        }
        else if(captureProfile == PostProcessingProfile.captureCCTV)
        {
            CityControls.Instance.captureSceneCCTV.volume.priority = 999;
            CityControls.Instance.captureSceneCCTV.volume.weight = 1;
            CityControls.Instance.captureSceneCCTV.objectRef.SetActive(true);
        }

        //Save DoF
        //bool saveDOF = SessionData.Instance.dof.active;
        //SessionData.Instance.dof.active = false;

        //Save grain
        //bool saveGrain = SessionData.Instance.grain.active;
        //SessionData.Instance.grain.active = false;

        //Save vignette
        //bool saveVig = SessionData.Instance.vignette.active;
        //SessionData.Instance.vignette.active = false;

        //Save colour adjustments
        //bool saveColourAdjustments = SessionData.Instance.colour.active;
        //SessionData.Instance.colour.active = false;

        //Save flashlight
        bool saveFlashlight = FirstPersonItemController.Instance.flashlight;
        FirstPersonItemController.Instance.flashLightObject.SetActive(false);

        //Enable cap light
        if(useCaptureLight)
        {
            FirstPersonItemController.Instance.captureLightObject.SetActive(true);
        }

        //Save post processing
        //SessionData.SceneProfile saveSceneProfile = SessionData.Instance.currentProfile;

        //Turn off motion blur
        //SessionData.Instance.motionBlur.active = false;

        //Turn off bloom
        //SessionData.Instance.bloom.active = false;

        //Set to cam pos
        CameraController.Instance.cam.transform.position = pos;
        CameraController.Instance.cam.transform.eulerAngles = euler;

        float saveDecimalClock = SessionData.Instance.decimalClock;

        //Update culling
        if (!basicMode)
        {
            if (passNode != null)
            {
                Game.Log("Updating culling for " + passNode.room.name + " (capture) " + passNode.position + ", " + pos);
                GameplayController.Instance.UpdateCullingForRoom(passNode.room, false, false, null, immediateLoad: true);
            }
            else
            {
                passNode = Toolbox.Instance.FindClosestValidNodeToWorldPosition(pos);

                if (passNode != null && passNode.room != null)
                {
                    Game.Log("Updating culling for " + passNode.room.name + " (capture)" + passNode.position + ", " + pos);
                    GameplayController.Instance.UpdateCullingForRoom(passNode.room, false, false, null, immediateLoad: true);
                }
            }

            SessionData.Instance.ExecuteSyncPhysics(SessionData.PhysicsSyncType.both);

            ObjectPoolingController.Instance.ExecuteUpdateObjectRanges(true); //Force update of object ranges to immediately spawn things

            //This is now not needed as we have special pp profiles for capture
            //Update post processing
            //if (passNode != null && passNode.room != null)
            //{
            //    SessionData.Instance.SetSceneProfile(passNode.room.GetEnvironment(), true);
            //}

            //Set world visuals
            if (changeTimeOfDay)
            {
                SessionData.Instance.SetSceneVisuals(decimalClock);
            }

            if (forceHide != null)
            {
                foreach (Interactable i in forceHide)
                {
                    if (i.controller != null)
                    {
                        i.controller.gameObject.SetActive(false);
                    }
                }
            }

            //Capture normalized actor screen positions
            if (humanRef != null)
            {
                foreach (SceneRecorder.ActorCapture act in humanRef)
                {
                    Game.Log("Gameplay: Loading actor capture " + act.id);
                    act.Load();

                    if (act.poser == null) continue;

                    //Get and record normalized screen postion...
                    Vector3 screenPoint = CameraController.Instance.cam.WorldToScreenPoint(act.poser.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.Head).transform.position); //Get point above their head and convert it to a screen position
                    Vector2 sp = new Vector2(screenPoint.x / CameraController.Instance.cam.pixelWidth, screenPoint.y / CameraController.Instance.cam.pixelHeight);
                    //Game.Log("Calculate normalized screen position for " + act.humanID + " : " + sp + " (" + screenPoint + ")");

                    Human h = act.GetHuman();

                    actorScreenPointCapture.Add(new ActorScreenPosition { human = h, screenPoint = sp });

                    //This is a really hacky way of recording the last screen position for actors, but here we are...
                    h.lastUsedCCTVScreenPoint = sp;
                }
            }
        }

        //Turn off custom passes
        foreach(UnityEngine.Rendering.HighDefinition.CustomPassVolume custom in SessionData.Instance.customPasses)
        {
            if (custom == null) continue;

            foreach (UnityEngine.Rendering.HighDefinition.CustomPass cp in custom.customPasses)
            {
                cp.enabled = false;
            }
        }

        //Turn off direction arrow
        MapController.Instance.directionalArrowContainer.gameObject.SetActive(false);

        RenderTexture.active = renderTextureInstance;

        //Capture & render
        CameraController.Instance.cam.targetTexture = renderTextureInstance;
        CameraController.Instance.cam.Render();

        //Turn on 3D UI //Causes problems with arms anim controller and should be covered by the layer mask anyway...
        //InterfaceControls.Instance.Interface3DCanvas.gameObject.SetActive(true);

        //Reset everything
        CameraController.Instance.cam.transform.position = savedCamPos;
        CameraController.Instance.cam.transform.eulerAngles = savedCamEuler;
        CameraController.Instance.cam.targetTexture = null;

        CameraController.Instance.cam.fieldOfView = savedFoV;
        CameraController.Instance.cam.cullingMask = camLayer;

        if (forceHide != null)
        {
            foreach (Interactable i in forceHide)
            {
                if (i.controller != null)
                {
                    i.controller.gameObject.SetActive(true);
                }
            }
        }

        //Activate scene profile for capture
        if (captureProfile == PostProcessingProfile.captureNormal)
        {
            CityControls.Instance.captureSceneNormal.objectRef.SetActive(false);
            //CityControls.Instance.captureSceneNormal.volume.priority = 0;
            //CityControls.Instance.captureSceneNormal.volume.weight = 0;
        }
        else if (captureProfile == PostProcessingProfile.captureCCTV)
        {
            CityControls.Instance.captureSceneCCTV.objectRef.SetActive(false);
            //CityControls.Instance.captureSceneCCTV.volume.priority = 0;
            //CityControls.Instance.captureSceneCCTV.volume.weight = 0;
        }

        //Load post process
        //SessionData.Instance.SetSceneProfile(saveSceneProfile, true);

        //Reactivate motion blur
        //PlayerPrefsController.GameSetting findSetting = PlayerPrefsController.Instance.gameSettingControls.Find(item => item.identifier.ToLower() == "motionblur"); //Case insensitive find
        //SessionData.Instance.motionBlur.active = Convert.ToBoolean(findSetting.intValue);

        //Reactivate bloom
        //PlayerPrefsController.GameSetting findSettingBloom = PlayerPrefsController.Instance.gameSettingControls.Find(item => item.identifier.ToLower() == "bloom"); //Case insensitive find
        //SessionData.Instance.bloom.active = Convert.ToBoolean(findSettingBloom.intValue);

        //SessionData.Instance.dof.active = saveDOF;
        //SessionData.Instance.grain.active = saveGrain;
        //SessionData.Instance.colour.active = saveColourAdjustments;
        //SessionData.Instance.vignette.active = saveVig;

        if(!basicMode)
        {
            //Reset visuals
            if (changeTimeOfDay)
            {
                SessionData.Instance.SetSceneVisuals(saveDecimalClock);
            }

            Player.Instance.OnRoomChange(); //Forcing this will reset the building model display (you may be able to get rid of this once building culling is tied to culling trees...)
        }

        //Reset flashlight
        FirstPersonItemController.Instance.flashLightObject.SetActive(saveFlashlight);

        //Disable cap light
        FirstPersonItemController.Instance.captureLightObject.SetActive(false);

        //Unload posers
        if(humanRef != null)
        {
            foreach (SceneRecorder.ActorCapture a in humanRef)
            {
                a.Unload();
            }
        }

        //Turn on custom passes
        foreach (UnityEngine.Rendering.HighDefinition.CustomPassVolume custom in SessionData.Instance.customPasses)
        {
            if (custom == null) continue;

            foreach (UnityEngine.Rendering.HighDefinition.CustomPass cp in custom.customPasses)
            {
                cp.enabled = true;
            }
        }

        //Turn on direction arrow
        MapController.Instance.directionalArrowContainer.gameObject.SetActive(MapController.Instance.displayDirectionArrow);

        //Write to texture 2D
        Texture2D currentShot =
            new Texture2D(renderTextureInstance.width, renderTextureInstance.height, TextureFormat.RGB24, false);
        // false, meaning no need for mipmaps
        currentShot.ReadPixels(new Rect(0, 0, renderTextureInstance.width, renderTextureInstance.height), 0, 0);
        currentShot.name = "New Capture";
        currentShot.Apply();

        RenderTexture.active = null; //can help avoid errors 

        Game.Log("Gameplay: Captured scene for " + pos + " and euler " + euler);
        renderTextureInstance.Release();
        Destroy(renderTextureInstance);

        CameraController.Instance.cam.Render(); //Render normal scene again: This appears to not be needed in editor, but is in the build?

        return currentShot;
    }

    [Button]
    public void ClearRenderCache()
    {
        cachedRenders.Clear();
        cachedEvidencePhotos = 0;

        cachedSurveillance.Clear();
        cachedSurveillancePhotos = 0;
    }
}
