﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class StateElementController : ButtonController
{
    public TextMeshProUGUI mainText;
    public TextMeshProUGUI detailText;
    public TextMeshProUGUI fineText;
    public int displayedFine = 0;
    public int fineTotal = 0;

    public JuiceController iconJuice;
    public RectTransform progressBar;
    public Image progressBarImg;
    public StatusPreset preset;

    public StatusController.StatusInstance statusInstance;

    List<StatusController.StatusCount> counts = new List<StatusController.StatusCount>();
    public List<CanvasRenderer> renderElements = new List<CanvasRenderer>();

    //Minimize to icon
    public bool minimized = false;
    public float minimizeTimer = 2f;
    public float widthResizingProgress = 1f;

    //Removing animation
    public bool removing = false;
    public float removalTimer = 1f;
    public RectTransform xIcon;
    public CanvasRenderer xIconRend;

    //Maximize
    public float maximizeTimer = 2f;
    public bool maximized = false;
    public float heightResizingProgress = 0f;
    public float maximizedHeight = 42f;

    //Special cases
    public bool isWanted = false;

    public void Setup(StatusController.StatusInstance newInstance)
    {
        statusInstance = newInstance;
        preset = statusInstance.preset;
        SetupReferences();

        if(!preset.enableProgressBar)
        {
            progressBar.gameObject.SetActive(false); //Disable progress bar...
        }

        //Add location to datasource...
        //if(statusInstance.building != null)
        //{
        //    dataSource = new Dictionary<string, EvidenceLinkData>();
        //    dataSource.Add("building", new EvidenceLinkData(statusInstance.building.name));
        //}

        detailText.canvasRenderer.SetAlpha(0f); //Set detail text not visible yet
        VisualUpdate();

        SetMinimized(false); //All statuses are not minimized to begin with...
        if(maximized) SetMaximized(false); //All statuses are maximized to begin with...

        if(preset.displayTotalFineWhenMinimized)
        {
            fineText.canvasRenderer.SetAlpha(0f); //Set not visible yet
            fineText.gameObject.SetActive(true);
        }
    }

    void OnEnable()
    {
        VisualUpdate();
    }

    public override void VisualUpdate()
    {
        if (preset == null) return;

        //Update counts
        StatusController.Instance.activeStatusCounts.TryGetValue(statusInstance, out counts);

        //Count fine...
        fineTotal = 0;

        //Sort counts into their configuration groups...
        Dictionary<StatusPreset.StatusCountConfig, List<StatusController.StatusCount>> sortedCounts = null;
        Dictionary<StatusPreset.StatusCountConfig, int> sortedValues = null;

        if (preset.listCountsInDetailText || preset.displayFineTotalInMainText || preset.displayTotalFineWhenMinimized)
        {
            sortedCounts = new Dictionary<StatusPreset.StatusCountConfig, List<StatusController.StatusCount>>();
            sortedValues = new Dictionary<StatusPreset.StatusCountConfig, int>();

            for (int i = 0; i < counts.Count; i++)
            {
                StatusController.StatusCount count = counts[i];

                if (!sortedCounts.ContainsKey(count.statusCountConfig))
                {
                    sortedCounts.Add(count.statusCountConfig, new List<StatusController.StatusCount>());
                    sortedValues.Add(count.statusCountConfig, 0);
                }

                sortedCounts[count.statusCountConfig].Add(count);
                int fine = count.GetPenaltyAmount();
                fineTotal += fine;
                sortedValues[count.statusCountConfig] += fine;
            }
        }

        string extraTextIDStr = string.Empty;

        if(isWanted)
        {
            extraTextIDStr = "_w";
        }

        //Set text
        mainText.text = Strings.Get("ui.states", preset.name + extraTextIDStr, useGenderReference: true, genderReference: Player.Instance);

        if (preset.displayCountCountsInMainText)
        {
            mainText.text += " (" + counts.Count + ")";
        }

        if (preset.displayFineTotalInMainText)
        {
            mainText.text += " " + CityControls.Instance.cityCurrency + fineTotal;
        }

        detailText.text = string.Empty;

        bool forcePulsate = false;

        if(preset.includeDescription)
        {
            if(preset.replaceDescriptionBasedOnCounts)
            {
                detailText.text += Strings.Get("ui.states", preset.name + "_description" + "_" + counts[0].statusCountConfig.name, useGenderReference: true, genderReference: Player.Instance);
            }
            else
            {
                detailText.text += Strings.Get("ui.states", preset.name + "_description" + extraTextIDStr, useGenderReference: true, genderReference: Player.Instance);
            }

            if (preset.displayAddressInDetailText && statusInstance.address != null)
            {
                detailText.text += ": " + statusInstance.address.name;
            }

            if (preset.displayBuildingInDetailText && statusInstance.building != null)
            {
                detailText.text += ": " + statusInstance.building.name;
            }

            if (preset.listCountsInDetailText)
            {
                foreach(KeyValuePair<StatusPreset.StatusCountConfig, List<StatusController.StatusCount>> pair in sortedCounts)
                {
                    string countText = string.Empty;

                    countText = Strings.Get("ui.states", pair.Key.name + "_description", useGenderReference: true, genderReference: Player.Instance);

                    string underlineStart = string.Empty;
                    string underlineEnd = string.Empty;

                    if(pair.Value.Exists(item => item.fineRecord.confirmed))
                    {
                        forcePulsate = true;
                        underlineStart = "<u>";
                        underlineEnd = "</u>";
                    }

                    detailText.text += "\n\n<margin-left=5%><size=90%><line-height=80%>-"+underlineStart + countText + underlineEnd + " (" + CityControls.Instance.cityCurrency + sortedValues[pair.Key] + ")";
                }
            }
        }

        maximizedHeight = StatusController.Instance.elementDefaultHeight + detailText.preferredHeight + 14f;
        detailText.canvasRenderer.SetAlpha(StatusController.Instance.detailTextFadeInCurve.Evaluate(heightResizingProgress));

        //Set colours
        Color col = GetColour();
        icon.sprite = preset.icon;
        if (isWanted) icon.sprite = preset.alternateIcon;
        icon.color = col;
        mainText.color = col;
        juice.flashColour = col;
        juice.pulsateColour = col;
        if (progressBarImg != null) progressBarImg.color = col;

        if ((preset.pulseBackground || isWanted || forcePulsate) && !juice.pulsateActive)
        {
            juice.Pulsate(true);
        }

        //Set pulsing icon
        if ((preset.pulseIcon || isWanted))
        {
            iconJuice.elements[0].originalColour = GetColour();
            iconJuice.pulsateColour = GetColour() + preset.pulseIconAdditiveColour;
            if(!iconJuice.pulsateActive) iconJuice.Pulsate(true);
        }
    }

    public void SetRemove(bool val)
    {
        if(removing != val)
        {
            removing = val;
            removalTimer = 1f;

            detailText.gameObject.SetActive(!removing);
            juice.enabled = !removing;
            iconJuice.enabled = !removing;

            if (!removing)
            {
                if (xIcon != null) Destroy(xIcon.gameObject);

                //Reset visibility
                foreach (CanvasRenderer r in renderElements)
                {
                    r.SetAlpha(1f);
                }

                detailText.canvasRenderer.SetAlpha(StatusController.Instance.detailTextFadeInCurve.Evaluate(heightResizingProgress));
            }
            else
            {
                //Spawn X
                GameObject newObj = Instantiate(PrefabControls.Instance.statusRemovedIcon, icon.transform);
                newObj.GetComponent<Image>().color = mainText.color;
                xIcon = newObj.GetComponent<RectTransform>();
                xIconRend = newObj.GetComponent<CanvasRenderer>();
            }
        }
    }

    public Color GetColour()
    {
        Color ret = preset.color;

        if(isWanted)
        {
            ret = preset.alternateColour;
        }

        if(preset.overrrideColorWithCount)
        {
            StatusPreset.StatusCountConfig c = null;

            foreach(StatusController.StatusCount count in counts)
            {
                if(c == null || count.statusCountConfig.penalty > c.penalty)
                {
                    c = count.statusCountConfig;
                    ret = count.statusCountConfig.colour;
                }
            }
        }

        if(preset.fadeToWhite && StatusController.Instance.activeStatusCounts.ContainsKey(statusInstance) && StatusController.Instance.activeStatusCounts[statusInstance].Count > 0)
        {
            ret = Color.Lerp(Color.white, ret, StatusController.Instance.activeStatusCounts[statusInstance][0].amount);
        }

        return ret;
    }

    public void SetMinimized(bool val)
    {
        if(minimized != val)
        {
            minimized = val;

            if(!minimized)
            {
                minimizeTimer = 2f;
            }
        }
    }

    public void SetMaximized(bool val)
    {
        if(maximized != val)
        {
            maximized = val;

            if(maximized)
            {
                maximizeTimer = 2f;
            }
        }
    }
}
