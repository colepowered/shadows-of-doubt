﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeonLetterFlickerController : MonoBehaviour
{
    public CityControls.NeonMaterial neonMat;
    public bool state = false;
    public AudioController.LoopingSoundInfo loop;

    public Vector3 soundOffset = new Vector3(0f, 0f, 0.2f);

    public NewNode closestStreetNode;
    public Vector3 nodeWorldPos;

    private void OnEnable()
    {
        //find the closest street node
        if (closestStreetNode == null)
        {
            closestStreetNode = Toolbox.Instance.FindClosestValidNodeToWorldPosition(this.transform.TransformPoint(soundOffset), limitToDirection: true, limitedDirection: new Vector3Int(0, 0, -1));
            nodeWorldPos = closestStreetNode.position;
        }
    }

    private void Update()
    {
        if (closestStreetNode == null) return;

        //Detect on/off
        if(neonMat.flickerInterval && !state)
        {
            //Stop a previous sound
            if (loop != null) AudioController.Instance.StopSound(loop, AudioController.StopType.triggerCue, "Neon switch off");

            state = true;

            //Play sound
            loop = AudioController.Instance.PlayWorldLoopingStatic(neonMat.flickerAudio, null, closestStreetNode, this.transform.TransformPoint(soundOffset));
        }
        //Playing (send brightness)
        else if(neonMat.flickerInterval && state)
        {
            loop.audioEvent.setParameterByName("Brightness", neonMat.brightness);
        }
        //Key off
        else if(!neonMat.flickerInterval && state)
        {
            if (loop != null) AudioController.Instance.StopSound(loop, AudioController.StopType.triggerCue, "Neon switch off");
            loop = null;
            state = false;
        }
    }

    private void OnDisable()
    {
        //Fade off
        if (loop != null) AudioController.Instance.StopSound(loop, AudioController.StopType.fade, "Neon disabled");
        loop = null;
        state = false;
    }
}
