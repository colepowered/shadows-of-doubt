﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelController : MonoBehaviour
{
    public virtual void OnOpen()
    {
        return;
    }

    public virtual void OnClose()
    {
        return;
    }
}
