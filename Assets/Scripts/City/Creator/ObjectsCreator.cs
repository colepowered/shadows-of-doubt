﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This script contains a function that populates the map with buildings.
//Script pass 1
public class ObjectsCreator : Creator
{
    //Chunks to load per frame
    public int loadChunk = 100;

    //Singleton pattern
    private static ObjectsCreator _instance;
    public static ObjectsCreator Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public override void StartLoading()
    {
        Game.Log("CityGen: Load objects...");
        StartCoroutine("Load");
    }

    IEnumerator Load()
    {
        int cursor = 0;

        //Load meta objects
        foreach(MetaObject mo in CityConstructor.Instance.currentData.metas)
        {
            if(!CityData.Instance.metaObjectDictionary.ContainsKey(mo.id))
            {
                CityData.Instance.metaObjectDictionary.Add(mo.id, mo);
            }
        }

        //Connect nodes
        while (cursor < CityConstructor.Instance.currentData.interactables.Count)
        {
            for (int i = 0; i < loadChunk; i++)
            {
                //Break loop if complete
                if (cursor >= CityConstructor.Instance.currentData.interactables.Count)
                {
                    break;
                }

                //Load interactable...
                Interactable inter = CityConstructor.Instance.currentData.interactables[cursor];
                bool deleted = false;

                //Is there a more recent save state version of this object?
                if(CityConstructor.Instance.saveState != null)
                {
                    //Has this been destroyed in this state save?
                    if(CityConstructor.Instance.saveState.removedCityData.Contains(inter.id))
                    {
                        inter.Delete(); //Remove
                        deleted = true;
                    }
                    else
                    {
                        //Look for matching ID in save state...
                        int newerVersionIndex = CityConstructor.Instance.saveState.interactables.FindIndex(item => item.id == inter.id);

                        //If this is found, load from save state instead of city data...
                        if(newerVersionIndex > -1)
                        {
                            inter = CityConstructor.Instance.saveState.interactables[newerVersionIndex];

                            //Game.Log("Loaded a newer version of " + inter.name);

                            //Remove from save state to save loading later...
                            CityConstructor.Instance.saveState.interactables.RemoveAt(newerVersionIndex);

                            inter.wasLoadedFromSave = true;
                        }
                    }
                }

                //Load if not already loaded...
                if(!deleted)
                {
                    if(!CityData.Instance.savableInteractableDictionary.ContainsKey(inter.id))
                    {
                        inter.MainSetupStart();
                        inter.OnLoad();
                    }
                    else if(inter.furnitureParent == null)
                    {
                        //This can happen when a furniture interactable that belongs to someone is loaded. Should be ok as it will search the save data and load that...
                        //Game.LogError("Trying to load an already-created interactable " + inter.name + " ID " + inter.id + " existing: " + CityData.Instance.savableInteractableDictionary[inter.id].p + " load: " + inter.p);
                    }
                }

                cursor++;
            }

            CityConstructor.Instance.loadingProgress = ((float)cursor / (float)CityConstructor.Instance.currentData.interactables.Count);

            yield return null;
        }

        //Complete!
        SetComplete();
    }
}
