﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScrollHighlightController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Vector2 scrollPositionPathmap = Vector2.zero;
    
    public void OnPointerEnter(PointerEventData data)
    {
        CameraController.Instance.NewHighlightScroll(scrollPositionPathmap);
    }

    public void OnPointerExit(PointerEventData data)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(data, this.transform)) return;

        CameraController.Instance.CancelHighlightScroll();
    }
}
