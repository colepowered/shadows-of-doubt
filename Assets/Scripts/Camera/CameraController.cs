﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using System;
using Unity.Collections;
using System.Collections.Generic;
using Unity.Jobs;

//A script that controls the scrolling camera.
//Script pass 1
public class CameraController : MonoBehaviour
{
    [Header("References")]
    public GameObject cameraObj;
    public GameObject container;
    public Camera cam;
    public UnityEngine.Rendering.HighDefinition.HDAdditionalCameraData hdrpCam;

    [Header("Fade")]
    public bool fadeActive = false;
    public Image fadeImage;

    [Header("Editor Movement Settings")]
    //Default (starting values) for the camera postion.
    public Vector2 camHeightLimit = new Vector2(1f, 20f);
    public float heightRatio = 0f;
    public Vector3 defaultCameraEuler = new Vector3(90f, 0f, 0f);

    [Space(5)]
    public float scrollSensitivity = 0.5f;
    public float camScrollHeightModifier = 0.1f;

    [Space(5)]
    public float rotateSensitivity = 0.5f;

    [Space(5)]
    public float zoomSensitivity = 2f;

    [Header("Smoothing Speeds")]
    public float smoothRotateSpeed = 25f;
    public float smoothZoomSpeed = 25f;
    public float highlightScrollSpeed = 25f;

    [Header("Camera Boundary")]
    public float isoCamBoundaryMultiplier = 1.5f;
    public float topCamBoundaryMultiplier = 1f;
    //private float camBoundary = 1f;

    [Header("Highlight scroll")]
    public bool highlightScrollActive = false;
    public bool highlightScrollCancelFlag = false;
    public Vector3 originalCameraPosition = Vector3.zero;
    public Vector3 highlightScroll = Vector2.zero;
    public GameObject highlightScrollMarker;
    public float highlightTileHeight = 0f;

    //Actions - used as callback for end of frame
    public Action SetCameraPerspective;
    public Action SetCameraOrthographic;
    public Action EnableFogController;

    //Actions
    Action checkUnpositioned;

    //Singleton pattern
    private static CameraController _instance;
    public static CameraController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        //Get camera component
        cam = cameraObj.GetComponent<Camera>();
        //SetOrthographic();
    }

    //Toggle camera view type
    //public void ToggleViewType()
    //{
    //    if(isometricCamera)
    //    {
    //        SetTopDown();
    //    }
    //    else
    //    {
    //        SetIsometric();
    //    }
    //}

    //Set TopDown
    //public void SetTopDown()
    //{
    //    //Counter the containers rotation of 90
    //    //cameraRotateDesired.x = 0f;

    //    //Type 0 is top down
    //    isometricCamera = false;

    //    //Set a multiplier for the scroll bounds
    //    camBoundary = topCamBoundaryMultiplier;
    //}

    //Set Isometric
    //public void SetIsometric()
    //{
    //    //Counter the containers rotation of 90
    //    //cameraRotateDesired.x = -45f;

    //    //Type 1 is isometric
    //    isometricCamera = true;

    //    //Set a multiplier for the scroll bounds
    //    camBoundary = isoCamBoundaryMultiplier;
    //}

    //Static function to just move the camera's postion
    //public void SetCameraPosition(Vector3 pos)
    //{
    //    //UpdateCamPos(pos, moveZoom, conMoveR, camMoveR);
    //}

    //Highlight scroll
    public void NewHighlightScroll(Vector2 newScrollPosPathmap)
    {
        //Get height of tile
        highlightTileHeight = CityData.Instance.GetTileHeight(newScrollPosPathmap);

        //Pathmap to realpos
        Vector3 realPos = CityData.Instance.TileToRealpos(new Vector3Int(Mathf.RoundToInt(newScrollPosPathmap.x), Mathf.RoundToInt(newScrollPosPathmap.y), 0));
        highlightScroll = new Vector3(realPos.x, container.transform.position.y, realPos.z);

        //Disable cancel
        highlightScrollCancelFlag = false;
    }

    //Cancel but scroll camera back to original position
    public void CancelHighlightScroll()
    {
        if (highlightScrollActive)
        {
            highlightScrollCancelFlag = true;
        }
    }

    //Cancel without resuming original camera position
    public void ImmediateCancelHighlightScroll()
    {
        if (highlightScrollActive)
        {
            if (highlightScrollMarker != null) Destroy(highlightScrollMarker);
            highlightScroll = container.transform.position;
            highlightScrollCancelFlag = false;
            highlightScrollActive = false;
        }
    }

    public void SetupFPS()
    {
        CameraController.Instance.cam.enabled = true;
        CameraController.Instance.cam.fieldOfView = Game.Instance.fov;

        //Close UI
        InterfaceController.Instance.RemoveAllMouseInteractionComponents();

        //Set parent
        cam.transform.SetParent(PrefabControls.Instance.camHeightParent);

        //Set char controller height
        if(!Player.Instance.inAirVent && !Game.Instance.freeCam && (CityConstructor.Instance == null || CityConstructor.Instance.saveState == null))
        {
            Player.Instance.SetPlayerHeight(Player.Instance.GetPlayerHeightNormal());
            Player.Instance.SetCameraHeight(GameplayControls.Instance.cameraHeightNormal);
        }

        //Enable fps script
        Player.Instance.enabled = true;
        Player.Instance.fpsMode = true;

        //Enable first person gameworld interface
        //InterfaceController.Instance.firstPersonUI.gameObject.SetActive(true);

        //Force update of groundmap to update groundmap-based culling systems
        Player.Instance.OnCityTileChange();

        //Unpause game
        if (!SessionData.Instance.play)
        {
            SessionData.Instance.ResumeGame();
        }

        //Enable player movement
        Player.Instance.EnablePlayerMovement(true);
        Player.Instance.EnablePlayerMouseLook(true);

        cam.transform.localEulerAngles = Vector3.zero;
        //cam.transform.localPosition = new Vector3(0, (GameplayControls.Instance.fpsCameraHeightMultiplierNormal * Player.Instance.charController.height) - (Player.Instance.charController.height * 0.5f), 0);
        Player.Instance.fps.InitialiseController(true);

        //If not paused, disable cursor
        if (SessionData.Instance.play)
        {
            InterfaceController.Instance.SetDesktopMode(false, true);
        }
    }

    public void DisableFPSMode()
    {
        //fpsMode = false;

        ////Close UI
        //InterfaceController.Instance.RemoveAllMouseInteractionComponents();

        ////Set parent
        //cam.transform.SetParent(PrefabControls.Instance.topDownCamContainer.transform, true);

        ////Cancel interaction mode
        //if (Player.Instance.interactionMode) Player.Instance.SetInteractionMode(false, null, null);

        ////Disable fps script
        //Player.Instance.enabled = false;
        //Player.Instance.fpsMode = false;

        ////Disable player movement
        //Player.Instance.EnablePlayerMovement(false);
        //Player.Instance.EnablePlayerMouseLook(false);

        ////This will cancel all invokes on this script
        //Player.Instance.CancelInvoke();

        ////Disable first person gameworld interface
        //InterfaceController.Instance.firstPersonUI.gameObject.SetActive(false);

        ////Cancel interaction cursor
        //Player.Instance.DisplayInteractionCursor(false);

        ////Enable buildings
        ////if (Player.Instance.previousBuilding != null) Player.Instance.previousBuilding.SetModelCulling(false);
        ////if (Player.Instance.currentBuilding != null) Player.Instance.currentBuilding.SetModelCulling(false);

        ////Pause game
        //if (SessionData.Instance.play)
        //{
        //    SessionData.Instance.PauseGame(true);
        //}

        ////Disable this script
        //Player.Instance.enabled = false;

        ////Entering overhead mode enables interaction
        //InterfaceController.Instance.SetDesktopMode(true, true, true);

        //if (SessionData.Instance.isFloorEdit)
        //{
        //    //Set mouse lock
        //    Player.Instance.EnablePlayerMouseLook(true);
        //}
    }

    //Fade camera to colour
    public void FadeCamera(float fadeSpeed)
    {
        if (fadeImage == null) return;
        StopCoroutine("CameraFade");
        StartCoroutine(CameraFade(true, fadeSpeed));
    }

    //Unfade camera
    public void UnFadeCamera(float fadeSpeed)
    {
        if (fadeImage == null) return;
        StopCoroutine("CameraFade");
        StartCoroutine(CameraFade(false, fadeSpeed));
    }

    IEnumerator CameraFade(bool fade = true, float fadeSpeed = 1f)
    {
        if(fade)
        {
            fadeImage.gameObject.SetActive(true);
            fadeActive = true;
        }

        //Tween
        float snapProgress = 0f;

        //Pickup previous progress
        if(fade) snapProgress = fadeImage.canvasRenderer.GetAlpha();
        else snapProgress = 1f - fadeImage.canvasRenderer.GetAlpha();

        while (snapProgress < 1f)
        {
            //snapProgress += (fpsModeSnapSpeed + (snapProgress * fadeSpeed)) * Time.deltaTime;
            snapProgress = Mathf.Clamp01(Mathf.CeilToInt(snapProgress * 100f) / 100f);

            if (fade)
            {
                fadeImage.canvasRenderer.SetAlpha(snapProgress);
            }
            else
            {
                fadeImage.canvasRenderer.SetAlpha(1f - snapProgress);
            }

            yield return null;
        }

        if(!fade)
        {
            fadeImage.gameObject.SetActive(false);
            fadeActive = false;
        }
    }

    struct LightRaycastData
    {
        public float MaxRange;
        public float LightMultiplier;
        public int Phase;
        public bool IsReverseCheck;

        public LightRaycastData(float maxRange, float lightMultiplier, int phase, bool isReverseCheck)
        {
            MaxRange = maxRange;
            LightMultiplier = lightMultiplier;
            Phase = phase;
            IsReverseCheck = isReverseCheck;
        }
    }

    List<LightRaycastData> lightRaycastDataCollection = new List<LightRaycastData>();
    List<RaycastCommand> raycastCommands = new List<RaycastCommand>();

    //Only called on scripts attached to the camera
    public float GetPlayerLightLevel()
    {
        //Color averageColor = Color.clear;

        //foreach(InterfaceControls.LightMeasuringPoint lm in InterfaceControls.Instance.playerLightMeasuring)
        //{
        //    Color playerLight = Toolbox.Instance.GetRenderTexturePixel(lm.instantiatedRenderTex);
        //    averageColor += playerLight / (float)InterfaceControls.Instance.playerLightMeasuring.Count;
        //}

        //float lightLevel = Mathf.Clamp01((averageColor.grayscale * 1.3f) - 0.2f);

        //Game.Log("Light level = " + lightLevel);

        ////Measure player light levels
        //return lightLevel;

        //Get player light level using custom/raycasting system...
        //Start with the room's ambient light level...
        float ambientMultiplier = 1f;

        if(!Player.Instance.isOnStreet)
        {
            ambientMultiplier = GameplayControls.Instance.interiorAmbientLightMultiplier;
        }

        float ambientLevel = Mathf.Clamp01(GameplayControls.Instance.stealthAmbientLightLevel.Evaluate(SessionData.Instance.dayProgress) * ambientMultiplier);

        //Game.Log("Player ambient light level: " + ambientLevel);

        //Secondly, find surrounding lights...
        float mainLightsLevel = 0f;
        int lightHitCount = 0;

        float secondaryLightsLevel = 0f;
        int secondaryHitCount = 0;

        float sunlightLevel = 0f;

        lightRaycastDataCollection.Clear();
        raycastCommands.Clear();

        foreach (NewRoom room in GameplayController.Instance.roomsVicinity)
        {
            //Scan for main lights
            if (room.mainLightStatus)
            {
                foreach (Interactable light in room.mainLights)
                {
                    //Light must be drawn
                    if (light.lightController != null)
                    {
                        if(light.lightController.isOn && !light.lightController.isUnscrewed && !light.lightController.closedBreaker)
                        {
                            //Send two raycasts: One to head, one to floor
                            for (int i = 0; i < 2; i++)
                            {
                                Vector3 originPoint = light.lightController.transform.position;

                                Vector3 destinationPoint = Vector3.zero;

                                if (i == 0)
                                {
                                    destinationPoint = cam.transform.position;
                                }
                                else
                                {
                                    destinationPoint = Player.Instance.transform.position;
                                    //destinationPoint = Player.Instance.lightMeasure.transform.position;
                                }

                                float maxRange = light.lightController.lightComponent.range;

                                LightRaycastData lightRaycastData = new LightRaycastData(maxRange, light.lightController.intensity * 0.006f, 0, false);
                                lightRaycastDataCollection.Add(lightRaycastData);
                                RaycastCommand raycastCommand = new RaycastCommand(light.wPos, destinationPoint - light.wPos, maxRange, Toolbox.Instance.aiSightingLayerMask, 1);
                                raycastCommands.Add(raycastCommand);
                            }
                        }
                    }
                }
            }

            //Scan for secondary lights
            foreach (Interactable light in room.secondaryLights)
            {
                //Light must be drawn
                if (light.lightController != null && light.lightController.isOn && !light.lightController.isUnscrewed && !light.lightController.closedBreaker)
                {
                    //Send two raycasts: One to head, one to floor
                    for (int i = 0; i < 2; i++)
                    {
                        Vector3 originPoint = light.lightController.transform.position;

                        Vector3 destinationPoint = Vector3.zero;

                        if (i == 0)
                        {
                            destinationPoint = cam.transform.position;
                        }
                        else
                        {
                            destinationPoint = Player.Instance.transform.position;
                            //destinationPoint = Player.Instance.lightMeasure.transform.position;
                        }

                        //RaycastHit hit;
                        float maxRange = light.lightController.lightComponent.range;

                        LightRaycastData lightRaycastData = new LightRaycastData(maxRange, light.lightController.intensity * 0.006f, 1, false);
                        lightRaycastDataCollection.Add(lightRaycastData);
                        RaycastCommand raycastCommand = new RaycastCommand(light.wPos, destinationPoint - light.wPos, maxRange, Toolbox.Instance.aiSightingLayerMask, 1);
                        raycastCommands.Add(raycastCommand);
                    }
                }
            }
        }

        //Add sunlight...
        //Send two raycasts: One to head, one to floor
        for (int i = 0; i < 2; i++)
        {
            Vector3 originPoint = CityControls.Instance.sunPosition.position;

            Vector3 destinationPoint = Vector3.zero;

            if (i == 0)
            {
                destinationPoint = cam.transform.position;
            }
            else
            {
                destinationPoint = Player.Instance.transform.position;
                //destinationPoint = Player.Instance.lightMeasure.transform.position;
            }

            //RaycastHit hit;
            float maxRange = 2000f;

            LightRaycastData lightRaycastData = new LightRaycastData(maxRange, 0, 2, false);
            lightRaycastDataCollection.Add(lightRaycastData);
            RaycastCommand raycastCommand = new RaycastCommand(originPoint, destinationPoint - originPoint, maxRange, Toolbox.Instance.aiSightingLayerMask, 1);
            raycastCommands.Add(raycastCommand);

            LightRaycastData lightRaycastDataReverse = new LightRaycastData(maxRange, 0, 2, true);
            lightRaycastDataCollection.Add(lightRaycastDataReverse);
            RaycastCommand raycastCommandReverse = new RaycastCommand(destinationPoint, originPoint - destinationPoint, maxRange, Toolbox.Instance.aiSightingLayerMask, 1);
            raycastCommands.Add(raycastCommandReverse);
        }

        NativeArray<RaycastHit> results = new NativeArray<RaycastHit>(raycastCommands.Count, Allocator.TempJob);
        NativeArray<RaycastCommand> commands = new NativeArray<RaycastCommand>(raycastCommands.ToArray(), Allocator.TempJob);

        JobHandle handle = RaycastCommand.ScheduleBatch(commands, results, 2, default(JobHandle));
        handle.Complete();

        bool checkReverse = false;
        for (int i = 0; i < results.Length; i++)
        {
            if (results[i].collider != null)
            {
                if (lightRaycastDataCollection[i].Phase == 0)
                {
                    try
                    {
                        if (results[i].collider.transform.CompareTag("Player") || (results[i].collider.transform.parent != null && results[i].collider.transform.parent.CompareTag("Player")))
                        {
                            //Get max distance ratio
                            float dist = 1f - results[i].distance / lightRaycastDataCollection[i].MaxRange;

                            //Multiply by intensity
                            mainLightsLevel += Mathf.Clamp01(dist * lightRaycastDataCollection[i].LightMultiplier);

                            lightHitCount++;
                        }
                    }
                    catch
                    {

                    }
                }
                else if (lightRaycastDataCollection[i].Phase == 1)
                {
                    //This needs to hit player to add any light value at all...
                    if (results[i].collider.transform.CompareTag("Player") || (results[i].collider.transform.parent != null && results[i].collider.transform.parent.CompareTag("Player")))
                    {
                        //Get max distance ratio
                        float dist = 1f - results[i].distance / lightRaycastDataCollection[i].MaxRange;

                        //Multiply by intensity
                        secondaryLightsLevel += Mathf.Clamp01(dist * lightRaycastDataCollection[i].LightMultiplier);

                        secondaryHitCount++;
                    }
                }
                else if (lightRaycastDataCollection[i].Phase == 2)
                {
                    if (lightRaycastDataCollection[i].IsReverseCheck && checkReverse)
                    {
                        checkReverse = false;
                        if (results[i].collider.transform.CompareTag("Transparent"))
                        {
                            sunlightLevel += GameplayControls.Instance.stealthSunLightLevel.Evaluate(SessionData.Instance.dayProgress);
                        }
                    }
                    else
                    {
                        //This needs to hit player to add any light value at all...
                        if (results[i].collider.transform.CompareTag("Player") || (results[i].collider.transform.parent != null && results[i].collider.transform.parent.CompareTag("Player")))
                        {
                            //If indoors
                            if (Player.Instance.currentRoom.gameLocation.thisAsAddress != null)
                            {
                                checkReverse = true;
                            }
                            else
                            {
                                sunlightLevel += GameplayControls.Instance.stealthSunLightLevel.Evaluate(SessionData.Instance.dayProgress);
                            }
                        }
                    }
                }
            }
            else if (lightRaycastDataCollection[i].Phase == 2 && lightRaycastDataCollection[i].IsReverseCheck && checkReverse)
            {
                sunlightLevel += GameplayControls.Instance.stealthSunLightLevel.Evaluate(SessionData.Instance.dayProgress);
            }
        }

        mainLightsLevel = Mathf.Clamp01(mainLightsLevel);
        secondaryLightsLevel = Mathf.Clamp01(secondaryLightsLevel);
        sunlightLevel = Mathf.Clamp01(sunlightLevel);

        //Game.Log("Player main light level: " + mainLightsLevel + " (" + lightHitCount + ")");
        //Game.Log("Player secondary light level: " + secondaryLightsLevel + " (" + secondaryHitCount + ")");
        //Game.Log("Player sun light level: " + sunlightLevel);

        float lightLevel = Mathf.Clamp01(ambientLevel + mainLightsLevel + secondaryLightsLevel + sunlightLevel);

        //Game.Log("Player: Light level: " + lightLevel);

        results.Dispose();
        commands.Dispose();

        return lightLevel;
    }
}
