﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceKey : Evidence
{
    public NewRoom keyTo; //Provides keys to this address

    //Constructor
    public EvidenceKey(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        int roomID = (int)(newPassedObjects[0] as Interactable.Passed).value;
        CityData.Instance.roomDictionary.TryGetValue(roomID, out keyTo);
    }
}
