using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using NaughtyAttributes;
using UnityEngine.Rendering.HighDefinition;

/// <summary>
/// DLSS and FSR are set to use optimal scaling in the HDRP settings asset so it should work automatically when enabled without setting the scale.
/// If DLSS is not available, it will fallback to FSR.
/// Options exposed to turn it on/off and adjust the quality.
/// </summary>
public class DynamicResolutionController : MonoBehaviour
{
    public enum DLSSQuality
    {
        MaximumPerformance,
        Balanced,
        MaximumQuality,
        UltraPerformance
    };

    public List<HDAdditionalCameraData> AllHDCameras;

    [SerializeField]
    private bool dynamicResolutionEnabled = false;
    public bool DynamicResolutionEnabled
    {
        get { return dynamicResolutionEnabled; }
        set
        {
            SetDynamicResolutionEnabled(value);
        }
    }

    [SerializeField]
    private bool dlssEnabled = false;
    public bool DLSSEnabled
    {
        get { return dlssEnabled; }
        set
        {
            SetDLSSEnabled(value);
        }
    }

    //Singleton pattern
    private static DynamicResolutionController _instance;
    public static DynamicResolutionController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    void Start()
    {
        //// Enable these to test!
        //DynamicResolutionEnabled = true;
        //DLSSEnabled = true;
    }

    public void SetDynamicResolutionEnabled(bool enable)
    {
        Game.Log("Menu: Dynamic resolution enabled: " + enable);

        dynamicResolutionEnabled = enable;
        foreach (HDAdditionalCameraData camera in AllHDCameras)
        {
            camera.allowDynamicResolution = enable;
        }
    }

    public void SetDLSSEnabled(bool enable)
    {
        Game.Log("Menu: DLSS enabled: " + enable);

        dlssEnabled = enable;
        foreach (HDAdditionalCameraData camera in AllHDCameras)
        {
            if (enable)
            {
                camera.allowDynamicResolution = true;
            }

            camera.allowDeepLearningSuperSampling = enable;
            camera.deepLearningSuperSamplingUseOptimalSettings = true;
        }
    }

    public void SetDLSSQualityMode(DLSSQuality quality)
    {
        foreach (HDAdditionalCameraData camera in AllHDCameras)
        {
            uint qualityID = ConvertDLSSQualityValue(quality);
            camera.deepLearningSuperSamplingQuality = qualityID;
        }
    }

    private uint ConvertDLSSQualityValue(DLSSQuality quality)
    {
        switch (quality)
        {
            case DLSSQuality.MaximumPerformance:
                return 0;
            case DLSSQuality.Balanced:
                return 1;
            case DLSSQuality.MaximumQuality:
                return 2;
            case DLSSQuality.UltraPerformance:
                return 3;
            default:
                return 1;
        }
    }
}

