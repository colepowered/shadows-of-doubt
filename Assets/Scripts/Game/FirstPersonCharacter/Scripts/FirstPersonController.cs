using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;
using Unity.Collections;
using System.Collections.Generic;

namespace UnityStandardAssets.Characters.FirstPerson
{
    public class FirstPersonController : MonoBehaviour
    {
        public bool enableMovement = true;
        public bool enableLook = true;
        public bool isMoving = false;
        public bool movementChange = false;
        public bool enableHeadBob = true;
        public bool ghostMovement = false;
        public bool clipping = true;
        public bool syncTransforms = false; //Is a manual sync transforms update needed? Set to true when new geometry is loaded...
        public Player playerScript;

        [SerializeField] public bool m_IsWalking;
        public float m_WalkSpeed;
        public float m_RunSpeed;
        public float speed;
        [SerializeField] private float m_RunstepLenghten;
        [SerializeField] public float m_StickToGroundForce;
        [SerializeField] public float m_GravityMultiplier;
        [SerializeField] public MouseLook m_MouseLook;
        [SerializeField] private bool m_UseFovKick;
        [SerializeField] private FOVKick m_FovKick = new FOVKick();
        [SerializeField] public CurveControlledBob m_HeadBob = new CurveControlledBob();
        public bool m_UseJumpBob;
        [SerializeField] public LerpControlledBob m_JumpBob = new LerpControlledBob();
        public float m_StepInterval;

        private bool rightFootNext = true;

        [SerializeField] private Transform leanPivot;
        [SerializeField] private float leanSpeed = 10f;
        [SerializeField] private float maxLeanAngle = 18f;
        [SerializeField] private float maxLeanMovement = 0.2f;

        public Camera m_Camera;
        private bool m_Jump;
        private float m_YRotation;
        private Vector2 m_Input;
        public Vector3 m_MoveDir = Vector3.zero;
        public CharacterController m_CharacterController;
        public CollisionFlags m_CollisionFlags;
        private bool m_PreviouslyGrounded;
        public Vector3 m_OriginalCameraPosition;
        public float m_StepCycle;
        public float m_NextStep;
        public bool m_Jumping;

        public int leanState = 0; //-1 = left, 1 = right
        public float leanProgress = 0f;
        public float currentLeanAngle = 0f;
        public float currentLeanMovement = 0f;

        public List<CameraJolt> activeJolts = new List<CameraJolt>();

        public float lastY = 0f;
        public float fallCount = 0f;

        private Vector3 previousMovement;
        public Vector3 movementThisUpdate;

        private RaycastHit[] hitInfoArray = new RaycastHit[1];

        public class CameraJolt
        {
            public CameraJolt(Vector3 newDirection, float newSpeed)
            {
                direction = newDirection;
                speed = newSpeed;
            }

            public Vector3 direction;
            public float progress;
            public float speed;
        } 

        private void Start()
        {
            //InitialiseController(true); //This is done on game load
        }

        // Use this for initialization
        public void InitialiseController(bool setOriginalCamPosition, bool initMouslook = true)
        {
            if (!CameraController.Instance.cam.enabled) CameraController.Instance.cam.enabled = true;
            m_CharacterController = GetComponent<CharacterController>();
            m_Camera = Camera.main;
            if(setOriginalCamPosition) m_OriginalCameraPosition = m_Camera.transform.localPosition;
            m_FovKick.Setup(m_Camera);
            m_HeadBob.Setup(m_Camera, m_StepInterval, m_OriginalCameraPosition);
            m_StepCycle = 0f;
            m_NextStep = m_StepCycle/2f;
            m_Jumping = false;
			if(initMouslook) m_MouseLook.Init(transform , m_Camera.transform);
            fallCount = 0f;
            lastY = transform.position.y;
            syncTransforms = true;

            Player.Instance.UpdateMovementPhysics(true);
        }

        // Update is called once per frame
        private void Update()
        {
            if(enableLook) RotateView();

            // the jump state needs to read here to make sure it is not missed
            if(SessionData.Instance.play && (CutSceneController.Instance == null || !CutSceneController.Instance.cutSceneActive))
            {
                if (!m_Jump && InputController.Instance.player != null)
                {
                    m_Jump = InputController.Instance.player.GetButtonDown("Jump");
                }

                if (Game.Instance.updateMovementEveryFrame)
                {
                    UpdateMovement();
                }

                //Calculate fall damage here
                if(!m_CharacterController.isGrounded && !ghostMovement)
                {
                    float fallAdd = Mathf.Max(lastY - transform.position.y, 0) * 0.1f;
                    fallCount += fallAdd;
                    //fallCount = Mathf.Clamp01(fallCount);

                    //if(fallAdd > 0f) Game.Log("+" + fallAdd + " (" + fallCount + ")");
                }

                lastY = transform.position.y;

                if (!m_PreviouslyGrounded && m_CharacterController.isGrounded)
                {
                    if(m_UseJumpBob) StartCoroutine(m_JumpBob.DoBobCycle());
                    PlayLandingSound();
                    m_MoveDir.y = 0f;
                    m_Jumping = false;

                    if(!ghostMovement)
                    {
                        bool trip = false;
                        float fallDamage = Mathf.Max(fallCount - 0.4f, 0f) * GameplayControls.Instance.fallDamageMultiplier * (1f + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.fallDamageModifier));

                        Game.Log("Player: Fall: " + fallCount + ", fall damage from mp: " + fallDamage);

                        //Play impact sound
                        if(fallCount >= 0.2f) AudioController.Instance.PlayerPlayerImpactSound(fallCount);

                        //Apply fall damage
                        if (!Game.Instance.disableFallDamage)
                        {
                            //Chance of broken leg
                            if (fallDamage > 0.6f && Toolbox.Instance.Rand(0f, 1f) < fallDamage)
                            {
                                Player.Instance.AddBrokenLeg(Toolbox.Instance.Rand(0.8f, 1f));
                            }
                        }

                        //Chance of trip
                        if (fallDamage > 0f)
                        {
                            Player.Instance.Trip(fallDamage, true, false);
                            trip = true;
                        }

                        //Chance of player tripping on land (drunk)
                        if (!trip && Toolbox.Instance.Rand(0f, 1f) < StatusController.Instance.tripChanceDrunk)
                        {
                            bool forwards = false;
                            if (Toolbox.Instance.Rand(0f, 1f) < 0.5f) forwards = true;
                            Player.Instance.Trip(Toolbox.Instance.Rand(0.05f, 0.1f), forwards);
                            trip = true;
                        }

                        if (!trip)
                        {
                            //Apply a jolt to the camera down
                            JoltCamera(Vector3.right, fallCount * 12f, 1.6f);
                        }
                    }

                    fallCount = 0f;
                }

                if (!m_CharacterController.isGrounded && !m_Jumping && m_PreviouslyGrounded)
                {
                    m_MoveDir.y = 0f;
                }

                m_PreviouslyGrounded = m_CharacterController.isGrounded;
            }
        }

        private void PlayLandingSound()
        {
            Game.Log("Player: Landed");

            //Footstep sound
            AudioController.Instance.PlayWorldFootstep(playerScript.footstepEvent, playerScript);

            //m_AudioSource.clip = m_LandSound;
            //m_AudioSource.Play();
            m_NextStep = m_StepCycle + .5f;
        }

        private void FixedUpdate()
        {
            if(!Game.Instance.updateMovementEveryFrame)
            {
                UpdateMovement();
            }
        }

        //Check if the player is out of world (through physics glitches) and correct if needed
        public void PlayerOutOfWorldCheck()
        {
            //Fall through floor/way up in the sky failsafe
            if (float.IsNaN(this.transform.position.x) || float.IsNaN(transform.position.y) || float.IsNaN(transform.position.z) || this.transform.position.y < -30f || this.transform.position.y > 200f)
            {
                NewNode teleportNode = Player.Instance.currentNode;

                if (teleportNode == null) teleportNode = Player.Instance.previousNode;

                if (teleportNode == null)
                {
                    teleportNode = Toolbox.Instance.FindClosestValidNodeToWorldPosition(this.transform.position);
                }

                if (teleportNode != null)
                {
                    Game.LogError("Player has reached too far up or down! Teleporting them to safety...");
                    Player.Instance.Teleport(teleportNode, null);
                    Player.Instance.SetHealth(Player.Instance.currentHealth - 0.1f); //Lose health, this way if we are caught in a loop the player will eventually die...
                }
                else
                {
                    Game.LogError("Player has reached too far up or down! Unable to get node for teleporting, so killing them off (sorry)");
                    Player.Instance.SetHealth(0f);
                    Player.Instance.TriggerPlayerKO(transform.forward, 0f);
                }
            }
        }

        public void UpdateMovement()
        {
            //Don't do this if game isn't started
            if (!SessionData.Instance.startedGame) return;

            previousMovement = this.transform.position;

            float leftLeanClamp = -maxLeanMovement;
            float rightLeanClamp = maxLeanMovement;

            float deltaTime = Time.fixedDeltaTime;
            if (Game.Instance.updateMovementEveryFrame) deltaTime = Time.smoothDeltaTime;

            //Don't do this if controller is inactive
            if (m_CharacterController.enabled)
            {
                //Handle lean
                //Perform a physics raycast check to stop the player from leaning through walls...
                if (leanState < 0)
                {
                    Ray leanRayLeft = new Ray(m_Camera.transform.position, -this.transform.right);
                    RaycastHit leftHit;

                    Physics.SyncTransforms();

                    if (Physics.Raycast(leanRayLeft, out leftHit, maxLeanMovement + 0.22f + m_CharacterController.skinWidth, Toolbox.Instance.physicalObjectsLayerMask))
                    {
                        leftLeanClamp = Mathf.Max(-Mathf.Max(0, leftHit.distance - 0.22f - m_CharacterController.skinWidth), -maxLeanMovement);

                        //leanLeftProgressClamp = -Mathf.Clamp(leftHit.distance / maxLeanMovement, 0, 1);
                        //Game.Log("Left lean raycast hit: " + leftHit.transform.name + " (" + leftHit.distance + ") max ratio: " + leanLeftProgressClamp);
                    }
                }
                else if (leanState > 0)
                {
                    Ray leanRayRight = new Ray(m_Camera.transform.position, this.transform.right);
                    RaycastHit rightHit;

                    Physics.SyncTransforms();

                    if (Physics.Raycast(leanRayRight, out rightHit, maxLeanMovement + 0.22f + m_CharacterController.skinWidth, Toolbox.Instance.physicalObjectsLayerMask))
                    {
                        rightLeanClamp = Mathf.Min(Mathf.Max(0, rightHit.distance - 0.22f - m_CharacterController.skinWidth), maxLeanMovement);

                        //leanRightProgressClamp = Mathf.Clamp(rightHit.distance / maxLeanMovement, 0, 1);
                        //Game.Log("Right lean raycast hit: " + rightHit.transform.name + " (" + rightHit.distance + ") max ratio: " + leanRightProgressClamp);
                    }
                }

                //Handle movement
                float speed = 0;

                if (enableMovement)
                {
                    GetInput(out speed);

                    //Game.Log("Player: Input speed: " + speed);

                    //Apply transition control
                    if (Player.Instance.transitionActive)
                    {
                        if (Player.Instance.currentTransition != null && !Player.Instance.currentTransition.retainMovementControl)
                        {
                            speed = 0;
                            //Game.Log("Player: Transition " + Player.Instance.currentTransition + " does not maintain movement control (speed = 0)");
                        }
                        else
                        {
                            float control = Mathf.Clamp01(Player.Instance.currentTransition.controlCurve.Evaluate(Player.Instance.transitionProgress));
                            speed *= control;
                            //Game.Log("Player: Transition " + Player.Instance.currentTransition + " movement control mod (speed = " + speed + ")");
                        }
                    }
                    //Apply hurt movment penalty; up to 50%
                    else if(Player.Instance.hurt > 0f)
                    {
                        speed *= Mathf.Clamp01(1f - (Player.Instance.hurt * 0.6f));
                        //Game.Log("Player: Speed modified by player hurt amount " + Player.Instance.hurt + " (speed = " + speed + ")");
                    }

                    //Add a small amount of automatic random movement while drunk
                    Vector3 drunkDir = Vector3.zero;
                    Vector2 drunkMovement = Vector2.zero;
                
                    if(StatusController.Instance.drunkControls > 0f)
                    {
                        //Force movement
                        drunkMovement = SessionData.Instance.drunkOscillation * StatusController.Instance.drunkControls * 0.01f;

                        //Screw with directions
                        drunkDir = new Vector3(SessionData.Instance.drunkOscillation.x, SessionData.Instance.drunkOscillation.y, SessionData.Instance.drunkOscillation.y) * StatusController.Instance.drunkControls * 0.16f;
                    }

                    //always move along the camera forward as it is the direction that it being aimed at
                    Vector3 desiredMove = (transform.forward + drunkDir) * m_Input.y + (transform.right + drunkDir) * m_Input.x;

                    //Game.Log("Player: Apply speed to movement: " + speed);

                    //get a normal for the surface that is being touched to move along it
                    if (!ghostMovement)
                    {
                        Physics.SphereCastNonAlloc(transform.position, m_CharacterController.radius, Vector3.down, hitInfoArray,
                                           m_CharacterController.height * 0.5f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
                        if (hitInfoArray[0].collider != null)
                        {
                            desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfoArray[0].normal).normalized;
                        }
                    }
                    else
                    {
                        desiredMove = ((m_Camera.transform.forward * m_Input.y) + (m_Camera.transform.right * m_Input.x)).normalized;
                        m_MoveDir.y = desiredMove.y * speed;
                    }

                    m_MoveDir.x = desiredMove.x * speed + drunkMovement.x;
                    m_MoveDir.z = desiredMove.z * speed + drunkMovement.y;

                    //if(speed > 0f) Game.Log("Move: " + desiredMove.normalized + " (speed: " + speed + " * delta: " + deltaTime + " = " + (speed * deltaTime) + ")");
                }
                else
                {
                    m_MoveDir.x = 0f;
                    m_MoveDir.y = 0f;
                    m_MoveDir.z = 0f;
                }

                //Apply gravity
                if (!ghostMovement)
                {
                    if (m_CharacterController.isGrounded)
                    {
                        m_MoveDir.y = -m_StickToGroundForce;

                        if (m_Jump && !StatusController.Instance.disabledJump && !Player.Instance.transitionActive)
                        {
                            m_MoveDir.y = GameplayControls.Instance.jumpHeight;
                            PlayJumpSound();
                            m_Jump = false;
                            m_Jumping = true;
                        }
                    }
                    else
                    {
                        //Force lean to reset
                        leanState = 0;
                        m_MoveDir += Physics.gravity * m_GravityMultiplier * deltaTime; //Apply gravity movement
                    }
                }

                //Move with or without collisions
                if (clipping)
                {
                    //Make sure transforms are synced before collision based movement
                    if(syncTransforms)
                    {
                        Physics.SyncTransforms();
                        syncTransforms = false;
                    }

                    Vector3 prevPos = this.transform.position;
                    //Game.Log("Player: Apply movement: " + m_MoveDir);
                    m_CollisionFlags = m_CharacterController.Move(m_MoveDir * deltaTime);

                    //Apply the same movement to the transition's original position to enable movement during transition
                    if(Player.Instance.transitionActive)
                    {
                        Player.Instance.originalPlayerPosition += this.transform.position - prevPos;
                    }
                }
                else
                {
                    Vector3 totalMovement = m_MoveDir * deltaTime;
                    this.transform.position += totalMovement;
                }

                Player.Instance.UpdateMovementPhysics();
            }
            else leanState = 0;

            //Apply Lean
            if (leanProgress > (float)leanState)
            {
                leanProgress -= (leanSpeed + Player.Instance.extraLeanSpeed) * deltaTime;

                if (leanState == 0 && leanProgress < 0f) leanProgress = 0;
                leanProgress = Mathf.Clamp(leanProgress, -1f, 1f);
            }
            else if (leanProgress < (float)leanState)
            {
                leanProgress += (leanSpeed + Player.Instance.extraLeanSpeed) * deltaTime;
                if (leanState == 0 && leanProgress > 0f) leanProgress = 0;
                leanProgress = Mathf.Clamp(leanProgress, -1f, 1f);
            }

            //Lean pivot
            currentLeanAngle = GameplayControls.Instance.leanCurve.Evaluate(leanProgress) * -maxLeanAngle;
            currentLeanMovement = Mathf.Clamp(GameplayControls.Instance.leanCurve.Evaluate(leanProgress) * maxLeanMovement, leftLeanClamp, rightLeanClamp);

            leanPivot.localPosition = new Vector3(currentLeanMovement, 0, 0);
            leanPivot.localRotation = Quaternion.AngleAxis(currentLeanAngle, Vector3.forward);

            //Apply jolts
            if (activeJolts.Count > 0)
            {
                Vector3 joltApplication = Vector3.zero;

                for (int i = 0; i < activeJolts.Count; i++)
                {
                    CameraJolt jolt = activeJolts[i];

                    joltApplication += jolt.direction * GameplayControls.Instance.joltCurve.Evaluate(jolt.progress);

                    jolt.progress += deltaTime * jolt.speed;

                    if(jolt.progress >= 1f)
                    {
                        activeJolts.RemoveAt(i);
                        i--;
                    }
                }

                leanPivot.localEulerAngles += joltApplication;
            }

            isMoving = false;

            if ((Mathf.Abs(m_Input.x) > 0f || Mathf.Abs(m_Input.y) > 0f) && (m_CollisionFlags == CollisionFlags.Below || m_CollisionFlags == CollisionFlags.CollidedBelow || m_CollisionFlags == CollisionFlags.None))
            {
                isMoving = true;
            }

            ProgressStepCycle(speed, deltaTime);
            UpdateCameraPosition(speed);

            m_MouseLook.UpdateCursorLock();

            //Update movement this frame
            movementThisUpdate = this.transform.position - previousMovement;

            //Trigger basement footstep on movement
            if(isMoving != movementChange && Player.Instance.transform.position.y < CityControls.Instance.basementWaterLevel)
            {
                movementChange = isMoving;
                if(isMoving) AudioController.Instance.PlayWorldFootstep(playerScript.footstepEvent, playerScript, rightFootNext);
            }

            //Out of world check
            PlayerOutOfWorldCheck();
        }

        //Pass to this; a direction (that will be normalized), an amplitude and a speed
        public void JoltCamera(Vector3 direction, float amplitude, float speed)
        {
            amplitude = Mathf.Min(amplitude, 70f); //Cap amplitude

            direction = direction.normalized * amplitude;

            activeJolts.Add(new CameraJolt(direction, speed));
        }

        private void PlayJumpSound()
        {

        }

        private void ProgressStepCycle(float speed, float deltaTime)
        {
            if (Player.Instance.crouchTransitionActive || InteractionController.Instance.lockedInInteraction != null) return; //Don't do this on crouch transition

            //Detect movement
            if ( m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
            {
                if (Player.Instance.inAirVent)
                {
                    m_StepCycle += 8f * deltaTime;
                }
                else
                {
                    m_StepCycle += (m_CharacterController.velocity.magnitude + (speed * (m_IsWalking ? 1f : m_RunstepLenghten))) *
                    deltaTime;
                }
            }

            if (!(m_StepCycle > m_NextStep))
            {
                return;
            }

            m_NextStep = m_StepCycle + m_StepInterval;

            AudioEvent audioEvent = null;

            if(Player.Instance.inAirVent)
            {
                audioEvent = AudioControls.Instance.playerFootstepDuct;
            }
            else if (m_CharacterController.isGrounded)
            {
                audioEvent = playerScript.footstepEvent;
            }

            if(audioEvent != null) AudioController.Instance.PlayWorldFootstep(audioEvent, playerScript, rightFootNext);

            //Alternate left/right
            rightFootNext = !rightFootNext;

            //Update the footstep sound indicator
            InterfaceController.Instance.footstepAudioIndicator.SetSoundEvent(audioEvent, false);
            InterfaceController.Instance.footstepAudioIndicator.UpdateCurrentEvent();

            //Juice
            if (InterfaceController.Instance.footstepAudioIndicator.juice != null && InterfaceController.Instance.footstepAudioIndicator.currentListeners.Count > 0)
            {
                InterfaceController.Instance.footstepAudioIndicator.juice.Nudge(new Vector2(1.05f, 1.05f), Vector2.zero);
            }
        }

        private void UpdateCameraPosition(float speed)
        {
            Vector3 newCameraPosition;

            if (Player.Instance.crouchTransitionActive || InteractionController.Instance.lockedInInteraction != null)
            {
                return;
            }

            if (m_CharacterController.velocity.magnitude > 0 && m_CharacterController.isGrounded)
            {
                if(enableHeadBob)
                {
                    m_Camera.transform.localPosition =
                                                        DoHeadBob(m_HeadBob, m_CharacterController.velocity.magnitude +
                                                                          (speed * (m_IsWalking ? 1f : m_RunstepLenghten)), GameplayControls.Instance.headBobMultiplier);
                }

                newCameraPosition = m_Camera.transform.localPosition;

                if(m_UseJumpBob) newCameraPosition.y = m_Camera.transform.localPosition.y - m_JumpBob.Offset();
            }
            else
            {
                newCameraPosition = m_Camera.transform.localPosition;
                if (m_UseJumpBob) newCameraPosition.y = m_OriginalCameraPosition.y - m_JumpBob.Offset();
            }

            m_Camera.transform.localPosition = newCameraPosition;
        }

        private void GetInput(out float speed)
        {
            // Read input
            float horizontal = InputController.Instance.GetAxisRelative("MoveHorizontal");
            float vertical = InputController.Instance.GetAxisRelative("MoveVertical");

            speed = Mathf.Clamp01(Vector2.Distance(Vector2.zero, new Vector2(horizontal, vertical) * 1.24f));
            //Game.Log("horz: " + horizontal + " vert: " + vertical + " speed: " + speed);

            //Get lean
            leanState = 0;

            if(InputController.Instance != null && InputController.Instance.player != null && !Player.Instance.inAirVent && InteractionController.Instance.carryingObject ==null && !Player.Instance.playerKOInProgress && !Player.Instance.transitionActive && !CutSceneController.Instance.cutSceneActive && (PlayerApartmentController.Instance == null || !PlayerApartmentController.Instance.furniturePlacementMode))
            {
                if (InputController.Instance.player.GetButton("LeanLeft"))
                {
                    leanState = -1;
                }
                else if (InputController.Instance.player.GetButton("LeanRight"))
                {
                    leanState = 1;
                }
            }

            leanState = Mathf.Clamp(leanState + Player.Instance.forcedLeanState, -1, 1); //Apply forced lean state

            bool waswalking = m_IsWalking;

            bool inBasement = false;

            if(Player.Instance.transform.position.y < CityControls.Instance.basementWaterLevel)
            {
                inBasement = true;
            }

            // On standalone builds, walk/run speed is modified by a key press.
            // keep track of whether or not the character is walking or running
            if (!playerScript.isCrouched && !playerScript.inAirVent && InputController.Instance.player != null && !StatusController.Instance.disabledSprint && !inBasement)
            {
                m_IsWalking = !InputController.Instance.player.GetButton("Sprint");
            }
            else m_IsWalking = true;

            // set the desired speed to be walking or running
            if(playerScript.inAirVent)
            {
                speed *= m_WalkSpeed * 0.3f;
                //if(m_IsWalking) Game.Log("Player: Speed = Speed (" + horizontal + ", " + vertical + ") * " + m_WalkSpeed + " * 0.3 = " + speed);
            }
            else
            {
                if(m_IsWalking)
                {
                    speed *= m_WalkSpeed;
                    //Game.Log("Player: Speed = Speed (" + horizontal + ", " + vertical + ") * " + m_WalkSpeed + " = " + speed);
                }
                else
                {
                    speed = m_RunSpeed;
                    //Game.Log("Player: Speed = Run Speed = " + speed);
                }

                if (inBasement)
                {
                    speed *= 0.66f; //Basement speed penalty
                }
            }

            //Apply speed multiplier
            speed *= StatusController.Instance.movementSpeedMultiplier;

            //Apply global control
            speed *= Game.Instance.movementSpeed;

            m_Input = new Vector2(horizontal, vertical);

            // normalize input if it exceeds 1 in combined length:
            if (m_Input.sqrMagnitude > 1)
            {
                m_Input.Normalize();
            }

            //Game.Log("Input: " + m_Input + ", speed: " + speed);

            // handle speed change to give an fov kick
            // only if the player is going to a run, is running and the fovkick is to be used
            if (m_IsWalking != waswalking && m_UseFovKick && m_CharacterController.velocity.sqrMagnitude > 0)
            {
                StopAllCoroutines();
                StartCoroutine(!m_IsWalking ? m_FovKick.FOVKickUp() : m_FovKick.FOVKickDown());
            }
        }

        private void RotateView()
        {
            //Game.Log("Look rotation");

            if(!Player.Instance.transitionActive && CameraController.Instance.cam.enabled)
            {
                m_MouseLook.LookRotation(transform, m_Camera.transform);
            }
        }

        private void OnControllerColliderHit(ControllerColliderHit hit)
        {
            Rigidbody body = hit.collider.attachedRigidbody;
            //dont move the rigidbody if the character is on top of it
            if (m_CollisionFlags == CollisionFlags.Below)
            {
                return;
            }

            if (body == null || body.isKinematic)
            {
                return;
            }
            body.AddForceAtPosition(m_CharacterController.velocity*0.1f, hit.point, ForceMode.Impulse);
        }

        public Vector3 DoHeadBob(CurveControlledBob bob, float speed, float multiplier)
        {
            float bobX = bob.Bobcurve.Evaluate(bob.m_CyclePositionX);
            float bobY = bob.Bobcurve.Evaluate(bob.m_CyclePositionY);

            foreach (KeyValuePair<AnimationCurve, float> pair in StatusController.Instance.affectHeadBobs)
            {
                bobX += pair.Key.Evaluate(bob.m_CyclePositionX) * pair.Value;
                bobY += pair.Key.Evaluate(bob.m_CyclePositionY) * pair.Value;
            }

            float xPos = m_OriginalCameraPosition.x + (bobX * bob.HorizontalBobRange * multiplier);
            float yPos = m_OriginalCameraPosition.y + (bobY * bob.VerticalBobRange * multiplier);

            bob.m_CyclePositionX += (speed * Time.deltaTime) / bob.m_BobBaseInterval;
            bob.m_CyclePositionY += ((speed * Time.deltaTime) / bob.m_BobBaseInterval) * bob.VerticaltoHorizontalRatio;

            if (bob.m_CyclePositionX > bob.m_Time)
            {
                bob.m_CyclePositionX = bob.m_CyclePositionX - bob.m_Time;
            }
            if (bob.m_CyclePositionY > bob.m_Time)
            {
                bob.m_CyclePositionY = bob.m_CyclePositionY - bob.m_Time;
            }

            if(float.IsNaN(xPos))
            {
                xPos = 0;
            }

            if(float.IsNaN(yPos))
            {
                yPos = 0;
            }

            return new Vector3(xPos, yPos, 0f);
        }
    }
}
