using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using TMPro;

public class RemapController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public ButtonController primaryControlButton;
    public TextMeshProUGUI controlDescriptionText;
    public TextMeshProUGUI primaryText;

    //[Header("State")]

    //[Header("Configuration")]

    public void OnSetAlternateButton()
    {
        Game.Log("Menu: OnSetAlternateButton...");
        Rewired.Demos.SimpleControlRemappingSOD.Instance.OnInputFieldClicked(this);
    }
}
