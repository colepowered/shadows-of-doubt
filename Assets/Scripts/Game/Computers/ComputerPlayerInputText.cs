﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Data;

public class ComputerPlayerInputText : CruncherAppContent
{
    public TextMeshProUGUI text;
    public string startingTextKey;
    public string fullTextKey; //Key for grabbing the original text
    private string fullText;
    public float keystrokes = 0;
    public int charsDisplayed = 0;
    public bool displayCursor = false;
    public float cursorTimer = 0f;
    private string revealedText;

    public override void OnSetup()
    {
        base.OnSetup();

        //Grab load text
        if(fullTextKey.Length > 0)
        {
            fullText = Strings.Get("computer", fullTextKey);
        }

        //Set starting text
        if (startingTextKey.Length > 0)
        {
            revealedText = Strings.Get("computer", startingTextKey);
            text.text = revealedText;
        }
    }

    private void Update()
    {
        //Add a keystroke @ x per second
        keystrokes += Time.deltaTime * 100f;

        //Detect player input for faster booting
        if (controller.playerControlled)
        {
            //Listen for player input
            if (InputController.Instance.player.GetAnyButtonDown())
            {
                keystrokes += Toolbox.Instance.Rand(100, 120);
            }
        }

        bool updateRevealed = false;

        if (charsDisplayed < keystrokes)
        {
            charsDisplayed++;

            //Reveal the text
            revealedText = fullText.Substring(0, Mathf.Min(charsDisplayed, fullText.Length));
            updateRevealed = true;
        }

        //Animate the text cursor
        if (cursorTimer < 0.8f)
        {
            cursorTimer += Time.deltaTime;

            if (cursorTimer >= 0.8f)
            {
                displayCursor = !displayCursor;
                updateRevealed = true;

                cursorTimer = 0f;
            }
        }

        //Update the text
        if(updateRevealed && text != null)
        {
            if (displayCursor)
            {
                text.text = revealedText + "_";
            }
            else
            {
                text.text = revealedText;
            }
        }

        //When all text is revealed, end app
        if (revealedText.Length >= fullText.Length)
        {
            controller.OnAppExit();
        }
    }
}
