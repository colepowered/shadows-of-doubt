﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;
using System.Linq;

public class EvidenceWalletControls : MonoBehaviour
{
    public InfoWindow parentWindow;
    public Sprite moneySprite;
    public Sprite cardSprite;
    public Sprite keySprite;

    public static List<EvidenceWalletControls> allItems = new List<EvidenceWalletControls>();

    [Header("Wallet")]
    public ButtonController button;
    public TextMeshProUGUI buttonText;
    [System.NonSerialized]
    public Human.WalletItem itemRef;

    private void OnEnable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.OnWindowRefresh += CheckEnabled;

        CheckEnabled();
    }

    private void OnDisable()
    {
        if(parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.OnWindowRefresh -= CheckEnabled;
    }

    //Update wallet item
    public void CheckEnabled()
    {
        Game.Log("Interface: Check for location key in parent window...");
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();

        //Get wallet item reference...
        allItems = this.gameObject.transform.parent.parent.GetComponentsInChildren<EvidenceWalletControls>().ToList();

        Game.Log("Found " + allItems.Count + " items");
        VisualUpdate(allItems.IndexOf(this));
    }

    public void VisualUpdate(int walletIndex)
    {
        if (parentWindow == null || parentWindow.passedInteractable == null || parentWindow.passedInteractable.belongsTo == null) return;

        Game.Log("Set wallet index: " + walletIndex);

        if(walletIndex > -1 && walletIndex < parentWindow.passedInteractable.belongsTo.walletItems.Count)
        {
            itemRef = parentWindow.passedInteractable.belongsTo.walletItems[walletIndex];

            if(itemRef != null)
            {
                if (itemRef.itemType == Human.WalletItemType.money && itemRef.money > 0)
                {
                    buttonText.text = Strings.Get("ui.interface", "Take Money") + " [" + CityControls.Instance.cityCurrency + Mathf.RoundToInt(itemRef.money) + "]";
                    button.background.sprite = moneySprite;
                }
                else if (itemRef.itemType == Human.WalletItemType.key)
                {
                    buttonText.text = Strings.Get("ui.interface", "Take Key");
                    button.background.sprite = keySprite;
                }
                else if (itemRef.itemType == Human.WalletItemType.evidence)
                {
                    button.background.sprite = cardSprite;
                    buttonText.text = string.Empty;
                }
                else
                {
                    buttonText.text = string.Empty;
                }
            }
        }
        else
        {
            Game.Log("Unable to get wallet index of " + walletIndex);
            itemRef = null;
        }

        if (itemRef == null || itemRef.itemType == Human.WalletItemType.nothing || (itemRef.itemType == Human.WalletItemType.money && itemRef.money <= 0))
        {
            button.SetInteractable(false);
            this.gameObject.SetActive(false);
            buttonText.text = string.Empty;
        }
        else
        {
            button.SetInteractable(true);
            this.gameObject.SetActive(true);
        }
    }

    public void OnButtonPress()
    {
        if(itemRef != null && parentWindow != null && parentWindow.passedInteractable != null && parentWindow.isWorldInteraction)
        {
            Game.Log("On wallet button: " + itemRef.itemType.ToString());

            //Trigger response
            if (itemRef.itemType != Human.WalletItemType.nothing && parentWindow.passedInteractable.inInventory != null && parentWindow.passedInteractable.inInventory.ai != null && Toolbox.Instance.Rand(0f, 1f) <= GameplayControls.Instance.stealTriggerChance)
            {
                parentWindow.passedInteractable.inInventory.ai.SetPersue(Player.Instance, true, 1, true, CitizenControls.Instance.punchedResponseRange);
            }

            if (itemRef.itemType == Human.WalletItemType.evidence)
            {
                MetaObject getMeta = CityData.Instance.FindMetaObject(itemRef.meta);
                if(getMeta != null) InterfaceController.Instance.SpawnWindow(getMeta.GetEvidence(false), Evidence.DataKey.name);
            }
            else if(itemRef.itemType == Human.WalletItemType.key)
            {
                //Give keys for key lock doors
                foreach(NewRoom room in parentWindow.passedInteractable.belongsTo.home.rooms)
                {
                    foreach (NewNode.NodeAccess acc in room.entrances)
                    {
                        if(acc.wall != null)
                        {
                            if (acc.wall.door != null)
                            {
                                if (acc.wall.door.preset.lockType == DoorPreset.LockType.key)
                                {
                                    Player.Instance.AddToKeyring(acc.wall.door, true);
                                }
                            }
                        }
                    }
                }

                StatusController.Instance.AddFineRecord(Player.Instance.currentNode.gameLocation.thisAsAddress, null, StatusController.CrimeType.theft, true, forcedPenalty: 100);
                itemRef.itemType = Human.WalletItemType.nothing; //Set type to nothing
            }
            else if(itemRef.itemType == Human.WalletItemType.money)
            {
                GameplayController.Instance.AddMoney(Mathf.RoundToInt(itemRef.money), true, "stolen from wallet");
                StatusController.Instance.AddFineRecord(Player.Instance.currentNode.gameLocation.thisAsAddress, null, StatusController.CrimeType.theft, true, forcedPenalty: itemRef.money * 10);
                itemRef.money = 0;
                itemRef.itemType = Human.WalletItemType.nothing; //Set type to nothing
            }

            CheckEnabled();
        }
    }
}
