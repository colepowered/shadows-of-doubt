﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CitizenControls : MonoBehaviour
{
    [Header("Movement")]
    [Tooltip("The base speed of a citizen")]
    public float baseCitizenWalkSpeed = 1.85f;
    [Tooltip("Citizen run speed multiplier")]
    public float baseCitizenRunSpeed = 6f;
    [Tooltip("Acceleration Curve")]
    public AnimationCurve acceleration;
    [Tooltip("Decceleration Curve")]
    public AnimationCurve decceleration;
    [Tooltip("Ranges for random speed multiplier")]
    public Vector2 movementSpeedMultiplierRange = new Vector2(0.9f, 1.1f);
    [Tooltip("Speed citizen turns to face a new direction")]
    public float citizenFaceSpeed = 20f;
    [Tooltip("Speed of citizen rotational movement towards a new direction")]
    public float citizenRotationalMovementSpeed = 22f;
    [Tooltip("Range of speed of citizen turning head to face something")]
    public Vector2 citizenLookAtSpeed = new Vector2(1f, 3f);
    [Tooltip("How far a citizen moves per footstep.")]
    public float citizenFootstepDistance = 1f;
    [Tooltip("Amount of movement speed to lose while drunk")]
    public float drunkMovementPenalty = 0.3f;
    [Tooltip("Chance of falling over while drunk")]
    public float drunkFallChance = 0.5f;
    [Tooltip("Capsule collider changes thickness depending on movement")]
    public Vector2 capsuleMovementThickness = new Vector2(0.12f, 0.31f);

    [Space(7)]
    [Tooltip("When not rendered, citizens move at this times their usual speed")]
    public float offscreenMovementSpeedMultiplier = 1.25f;
    [Tooltip("When not rendered, citizens move at this times their usual speed in stairwells")]
    public float offscreenStairwellMovementSpeedMultiplier = 1.85f;

    [Header("Visuals")]
    [Tooltip("The average scale of the average in-game height (175.4). This is slightly shorter than the player who is slightly above average height.")]
    public float baseScale = 0.85f;

    [Header("Dialog")]
    [Tooltip("How high above a citizens head is th speech bubble?")]
    public float speechBubbleHeight = 0.28f;
    public DialogPreset askAboutJob;
    [Tooltip("There maximum number of existing speech bubbles onscreen for new one line dialog to be triggered")]
    public int maxSpeechBubbles = 2;

    [Header("Bank")]
    public AnimationCurve societalClassSavingsCurve;
    [Tooltip("Each of the following essentially boost the soc class by 0.1")]
    [ReorderableList]
    public List<CharacterTrait> savingsBoostTrait = new List<CharacterTrait>();
    [Tooltip("Each of the following essentially reduce the soc class by 0.1")]
    [ReorderableList]
    public List<CharacterTrait> savingsDebuffTrait = new List<CharacterTrait>();

    [Header("Behaviour")]
    public DialogPreset telephoneGreeting;
    public DialogPreset identifyNumberDialog;
    public DialogPreset lastCallerDialog;
    public DialogPreset policeDialog;
    [Tooltip("If no valid conversation is found, use this one")]
    public string fallbackTelephoneConversation;

    [Tooltip("Minimum investigation time: The minimum time the AI is to keep the investigate goal at maximum priority (in-game time)")]
    public float minimumInvestigateTime = 0.25f;
    [Tooltip("How quickly the AI gains one 'persuit lead' (this added per second)")]
    public float persuitChaseLogicAdditionPerSecond = 0.66f;
    [Tooltip("The maximum number of persuit logic leads the AI can acculumate")]
    public int maxChaseLogic = 4;
    [Tooltip("Persuit response addition from shortest distance to longest distance")]
    public Vector2 persuitTimerThreshold = new Vector2(16, 2);
    [Tooltip("When target is not in range, how fast to forget them")]
    public float persuitForgetThreshold = 1f;
    [Tooltip("When heard an illegal sound, how fast to forget...")]
    public float hearingForgetThreshold = 0.2f;
    [Tooltip("The multiplier for the minimum investiation time if the player is persued")]
    public float persuitMinInvestigationTimeMP = 1.5f;
    [Tooltip("The multiplier for the minimum investiation time if the citizen investiates a sighting")]
    public float sightingMinInvestigationTimeMP = 1.25f;
    [Tooltip("The multiplier for the minimum investiation time if the citizen investiates a sound")]
    public float soundMinInvestigationTimeMP = 1f;

    [Space(7)]
    [Tooltip("How much time passes after a sighting of highest rank before a citizen stops looking @ it")]
    public float lookAtGracePeriod = 0.133f;

    [Space(7)]
    [Tooltip("If somebody in the same room is punched, trigger citizens in the same address within this range to respond...")]
    public float punchedResponseRange = 11f;

    [Header("Sightings")]
    [Tooltip("How many different citizens does can this person remember?")]
    public int defaultMemoryLimit = 100;

    [Header("Combat")]
    [Tooltip("Citizens recover this amount of health (normalized) over time (game time 1 hour)")]
    public float citizenBaseRecoveryRate = 0.1f;
    [Tooltip("The starting value for citizen combat skill (how fast attacks are)")]
    public Vector2 citizenBaseCombatSkillRange = new Vector2(0.5f, 1.5f);
    [Tooltip("Multiplier for combat heft: See descriptors for how this works")]
    public float citizenCombatHeftMultiplier = 0.25f;
    [Tooltip("Minimum range for throwing an object")]
    public float throwMinRange = 2f;
    [Tooltip("Minimum range for throwing an object")]
    public float throwMaxRange = 8f;

    [Space(7)]
    [Tooltip("Shock damage on impact is multiplied by this")]
    public float nerveDamageShockMultiplier = 1f;
    [Tooltip("Nerve damage by a weapon draw is multiplied by this")]
    public float nerveWeaponDrawMultiplier = 1f;
    [Tooltip("Nerve impacted when an alarm goes off")]
    public float nerveAlarm = -0.15f;
    [Tooltip("Nerve impacted after alarm switch")]
    public float nerveAlarmSwitched = 0.1f;
    [Tooltip("Nerve recovery rate as a fraction of health recovery")]
    public float nerveRecoveryRateMultiplier = 0.3f;

    [Space(7)]
    public float doorBargeKOForceMultiplier = 3f;
    [Tooltip("How the force multiplier scales with extra damage received")]
    public float damageRecieveForceMultiplier = 1f;

    [Header("Get up Limb Snapshot")]
    [Tooltip("How long it takes to transition from ragdoll landing position to the start of the get-up animation (seconds)")]
    public float ragdollTransitionTime = 0.68f;
    [Tooltip("The length of the get up animation or longer")]
    public float getUpTimer = 2f;

    [System.Serializable]
    public class LimbPos
    {
        public CitizenOutfitController.CharacterAnchor anchor;
        public Vector3 localPosition;
        public Quaternion localRotation;
    }

    [System.Serializable]
    public class ManualAnimation
    {
        public float timeline;
        public List<LimbPos> limbData = new List<LimbPos>();
    }

    public List<ManualAnimation> getUpManualAnimation;

    [Header("Skills")]
    [Tooltip("How quickly stealth skill is applied when standing still")]
    public float stealthSkillApplicationRate = 1f;
    [Tooltip("How quickly stealth skill is cancelled when moving")]
    public float stealthSkillCancelRate = 5f;

    [Header("LookAt Head Clamping")]
    public float leftExtent = -70f;
    public float rightExtent = 70f;
    public float upExtent = -18f;
    public float downExtent = 18f;

    [Header("Animation")]
    [Tooltip("Animation offset of the citizens lower torso with a scale of 1...")]
    public float sittingYOffset = 0f;
    [Tooltip("Animation offset of the citizens arms with a scale of 1...")]
    public float armsStandingYOffset = 0f;

    [Header("Fingerprints")]
    public Texture2D unknownPrint;
    [ReorderableList]
    public List<Texture2D> prints;

    [Header("Traits")]
    public CharacterTrait destitute;
    public CharacterTrait litterBug;
    public CharacterTrait likesTheRain;
    public CharacterTrait shoesNormal;
    public CharacterTrait shoesBoots;
    public CharacterTrait shoesHeels;
    public CharacterTrait coffeeLiker;
    public CharacterTrait teaLiker;

    [Header("Physical Traits")]
    public CharacterTrait bald;
    public CharacterTrait shortHair;
    public CharacterTrait longHair;
    public Vector2 shoeSizeRange = new Vector2(4f, 16f);

    [Header("AI")]
    [Tooltip("Subdivisions for AI navigation when inside nodes")]
    public List<Vector3> nodeLocalSubdivisions = new List<Vector3>();

    [Header("Starting Inventory")]
    public List<StartingInventory> citizenStartingInventory = new List<StartingInventory>();

    [System.Serializable]
    public class StartingInventory
    {
        public string name;
        public List<InteractablePreset> presets = new List<InteractablePreset>();
        public float baseChance;

        public List<MurderPreset.MurdererModifierRule> modifiers = new List<MurderPreset.MurdererModifierRule>();
    }

    [Header("Misc")]
    public InteractablePreset citizenInteractable;
    public InteractablePreset handInteractable;
    public AIActionPreset sleep;
    public MatchPreset matchWithPhoto;
    public MatchPreset weakVisualSighting;
    public MatchPreset mediumVisualSighting;
    public MatchPreset strongVisualSighting;
    public CharacterTrait randomPassword;
    public InteractablePreset deadBodySearchInteractable;
    public InteractablePreset entryWound;
    public InteractablePreset exitWound;
    public InteractablePreset toothbrush;
    public InteractablePreset addressBook;
    public GameObject umbrella;
    public SpatterPatternPreset vomitSpatter;

    [Header("Debug")]
    public CitizenOutfitController debugSelectCitizen;

    //Singleton pattern
    private static CitizenControls _instance;
    public static CitizenControls Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

    }

    [Button]
    public void ClearManualAnimation()
    {
        getUpManualAnimation.Clear();
    }

    [Button]
    public void AddManualKeyframe()
    {
        if (debugSelectCitizen != null)
        {
            ManualAnimation newKeyframe = new ManualAnimation();

            foreach (CitizenOutfitController.AnchorConfig anchor in debugSelectCitizen.anchorConfig)
            {
                LimbPos newPos = new LimbPos();
                newPos.anchor = anchor.anchor;
                newPos.localPosition = anchor.trans.localPosition;
                newPos.localRotation = anchor.trans.localRotation;

                newKeyframe.limbData.Add(newPos);
            }

            getUpManualAnimation.Add(newKeyframe);
#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif

            Game.Log("Added a manual keyframe to get up animation");
        }
    }
}
