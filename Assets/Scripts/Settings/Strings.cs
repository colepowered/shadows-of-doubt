﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;
using System.Text;
using NaughtyAttributes;
using UnityEditor;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.EventSystems;
using UnityEngine.Windows;
using UnityEngine.UIElements;
using System.Reflection;

//A script containing static references to string tables.
public class Strings : MonoBehaviour
{
    public static bool textFilesLoaded = false;
    public static bool backupENGLoaded = false;
    public static LocInput loadedLanguage;

    //[Header("Word Ranking")]
    //public string currentDictionary;
    //private TextAsset loaded;
    //List<string> loadedDictLines = new List<string>();
    //[ReadOnly]
    //public string dictionaryPath;
    //[ReadOnly]
    //public int currentIndex = 0;
    //public string currentRef;
    //public string currentName;
    //public string currentNickname;
    //[Range(1, 10)]
    //public int frequency = 1;

    public class DisplayString
    {
        public string displayStr;
        public string alternateStr;
        public int lineNumber;
    }

    //Dictionary of lists- for random selection
    public class RandomDisplayString
    {
        public string displayStr;
        public string alternateStr;
        public bool needsSuffixForShortName = false; //If 1 a short name made of this must include the suffix
    }

    //Dictionary of dictionaries method- use an identifier string to load the actual text.
    public static Dictionary<string, Dictionary<string, DisplayString>> stringTable = new Dictionary<string, Dictionary<string, DisplayString>>();
    public static Dictionary<string, string> dictionaryPathnames = new Dictionary<string, string>();

    public static Dictionary<string, Dictionary<string, DisplayString>> stringTableENG = new Dictionary<string, Dictionary<string, DisplayString>>();

    private static FileInfo templateFile;

    public enum Casing { asIs, firstLetterCaptial, pascalCase, upperCase, lowerCase};

    public enum LinkSetting { automatic, forceLinks, forceNoLinks};

	public static Dictionary<string, List<RandomDisplayString>> randomEntryLists = new Dictionary<string, List<RandomDisplayString>>();
    public static Dictionary<string, List<RandomDisplayString>> randomEntryListsENG = new Dictionary<string, List<RandomDisplayString>>();

    [Header("Localisation Output")]
    public List<string> localisationIgnoreFileList = new List<string>();
    public List<string> localisationIgnoreDirectoryList = new List<string>();
    [Tooltip("If the below string is present in the notes section then skip the line")]
    public bool useIgnoreFlagInNotes = true;
    [EnableIf("useIgnoreFlagInNotes")]
    public string ignoreFlag = "LOC_IGNORE";
    [Tooltip("Add this many extra line breaks between entries in the output")]
    [Range(0, 5)]
    public int extraLineBreaks = 3;
    [Space(7)]
    [Tooltip("If two or more identical content strings are detected in english, write them to a single entry for output")]
    public bool condenseIdenticalEnglishContentIntoOneKey = true; //TODO
    [Space(7)]
    [Tooltip("If true: Only output changes since this the below date...")]
    public bool onlyOuputChangesSince = false;
    [EnableIf("onlyOuputChangesSince")]
    public string outputSinceDate;

    [Header("Localisation Input")]
    public string localisationInputFile;
    public string inputDate;
    public string templateInputFile;
    [Tooltip("The last column should be ignored when checking for content")]
    public bool inputFeaturesLastColumnLineNumbers = true;
    [Tooltip("Write this if the localized text is missing. If detected, this will revert to the ENG string if english string")]
    public static string missingString = "<empty>";
    public List<LocInput> fileInputConfig = new List<LocInput>();
    [Tooltip("These will be added to the character output")]
    public string customUsedCharacters = string.Empty;
    [Tooltip("ASCII characters will be added to the character output")]
    public bool includeDefaultAsciiCharacters = true;

    [Header("Localisation Corrections")]
    public string localisationCorrectionsInputFile;
    public string correctionsInputDate;
    public SystemLanguage correctionsLanguage;
    [Tooltip("If true this will detect lines immediately below keys as gender variants of the keys with the following format: Main = M, 1 = Female, 2 = NB")]
    public bool useGenderVariationFormatting = true;
    [Tooltip("The column index in which the correct new translation is found")]
    public int columnContent = 3;
    [Tooltip("If the corrections file features a file that doesn't exist in the existing text, create it")]
    public bool createMissingFiles = true;
    [Tooltip("If the corrections file features a key that doesn't exist in the existing text, create it")]
    public bool createMissingKey = true;

    [Header("Debug")]
    public string findBlock;

    [System.Serializable]
    public class LocInput
    {
        public string languageCode;
        public int documentColumn;
        public SystemLanguage systemLanguage;
        [Tooltip("Display 'Mr' etc after the name")]
        public bool swapCitizenTitleOrder = false;
        public bool staticKillerMoniker = true;
    }

    public class LinkData
    {
        public int id;
        public static int assignID = 1;
        public Evidence evidence;
        public List<Evidence.DataKey> dataKeys;
        public List<int> inputCode;
        public string helpPage;

        public LinkData(Evidence newEvidence, List<Evidence.DataKey> overrideKeys)
        {
            id = assignID;
            assignID++;

            evidence = newEvidence;
            dataKeys = overrideKeys;

            if(!Strings.Instance.evidenceLinkDictionary.ContainsKey(evidence))
            {
                Strings.Instance.evidenceLinkDictionary.Add(evidence, new List<LinkData>());
            }

            Strings.Instance.evidenceLinkDictionary[evidence].Add(this);
            Strings.Instance.linkIDReference.Add(id, this);
        }

        public LinkData(Telephone newTelephone = null)
        {
            id = assignID;
            assignID++;
            evidence = newTelephone.telephoneEntry;
            inputCode = newTelephone.GetInputCode();

            Strings.Instance.linkDictionary.Add(newTelephone as object, this);
            Strings.Instance.linkIDReference.Add(id, this);
        }

        public LinkData(List<int> newInputCode = null)
        {
            id = assignID;
            assignID++;
            inputCode = newInputCode;

            Strings.Instance.linkDictionary.Add(inputCode as object, this);
            Strings.Instance.linkIDReference.Add(id, this);
        }

        public LinkData(string newHelpPage)
        {
            id = assignID;
            assignID++;

            Strings.Instance.linkIDReference.Add(id, this);

            helpPage = newHelpPage;
        }

        public void OnLink()
        {
            //Game.Log("On Link. Active code input: " + InterfaceController.Instance.activeCodeInput);

            //Play SFX
            AudioController.Instance.Play2DSound(AudioControls.Instance.inLineLink);

            //Spawn window
            if (inputCode != null && InterfaceController.Instance.activeCodeInput != null && InterfaceController.Instance.activeCodeInput.digits == inputCode.Count)
            {
                //Set input tab active
                InterfaceController.Instance.activeCodeInput.parentWindow.SetActiveContent(InterfaceController.Instance.activeCodeInput.windowContent);
                InterfaceController.Instance.activeCodeInput.OnInputCode(inputCode);
            }
            else if (evidence != null)
            {
                if(Game.Instance.devMode)
                {
                    string keyDist = string.Empty;

                    if(dataKeys != null)
                    {
                        foreach (Evidence.DataKey k in dataKeys)
                        {
                            keyDist += ", " + k.ToString();
                        }
                    }

                    Game.Log("OnLink: " + evidence + " keys: " + keyDist);
                }

                InterfaceController.Instance.SpawnWindow(evidence, passedEvidenceKeys: dataKeys);
            }
            else if(helpPage != null && helpPage.Length > 0)
            {
                //If window isn't spawned, spawn one
                if (HelpController.Instance == null)
                {
                    InterfaceController.Instance.ToggleNotebook(helpPage, true);
                }
                else
                {
                    HelpController.Instance.DisplayHelpPage(helpPage);
                }
            }
        }

        //Remove old link references if there are too many...
        private void OldLinkCheck()
        {
            if(Instance.linkDictionary.Count > 1000)
            {

            }
        }
    }

    Dictionary<object, LinkData> linkDictionary = new Dictionary<object, LinkData>();
    Dictionary<Evidence, List<LinkData>> evidenceLinkDictionary = new Dictionary<Evidence, List<LinkData>>();
    public Dictionary<int, LinkData> linkIDReference = new Dictionary<int, LinkData>();

    //Singleton pattern
    private static Strings _instance;
    public static Strings Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        //Make sure player prefs are loaded first!
        if(!PlayerPrefsController.Instance.loadedPlayerPrefs)
        {
            PlayerPrefsController.Instance.LoadPlayerPrefs();
        }

        //Parse text files to dictionaries
        LoadTextFiles();
    }

	public void LoadTextFiles()
	{
        //We're usually going to be using the streaming assets path, unless this is the standalone DDS editor (when we want to use the exe folder)
        string saveLoadPath = Application.streamingAssetsPath;

        if (SessionData.Instance.isDialogEdit && !Application.isEditor)
        {
            saveLoadPath = Path.GetFullPath(".");
        }

        //Clear dictionary
        stringTable.Clear();
        dictionaryPathnames.Clear();
        stringTableENG.Clear();
        randomEntryLists.Clear();
        randomEntryListsENG.Clear();

        textFilesLoaded = false;
        backupENGLoaded = false;

        if(Game.Instance.forceEnglish)
        {
            Game.Instance.language = "English";
        }
        else if(!PlayerPrefsController.Instance.playedBefore)
        {
            Game.Log("Menu: Game has not been run before, autodetecting language...");

            LocInput detectedLanguage = fileInputConfig.Find(item => item.systemLanguage == Application.systemLanguage);

            if(detectedLanguage != null)
            {
                Game.Log("Menu: ... Detected " + detectedLanguage.languageCode);
                Game.Instance.language = detectedLanguage.languageCode;
                PlayerPrefs.SetString("language", detectedLanguage.languageCode); //Save to player prefs
            }
        }

        Game.Log("Menu: Loading text files (" + Game.Instance.language + ")");
        loadedLanguage = fileInputConfig.Find(item => item.systemLanguage == SystemLanguage.English);

        //If this is not loading the ENG text, load it now as a backup...
        if (Game.Instance.language != "English")
        {
            //The setting needs to be in a localised language
            if(fileInputConfig.Exists(item => item.languageCode == Game.Instance.language))
            {
                loadedLanguage = fileInputConfig.Find(item => item.languageCode == Game.Instance.language);

                Game.Log("CityGen: Loading English backup strings...");
                DirectoryInfo embedENG = new DirectoryInfo(saveLoadPath + "/Strings/English/");
                List<FileInfo> embeddedFilesENG = embedENG.GetFiles("*.csv", SearchOption.AllDirectories).ToList();

                foreach (FileInfo file in embeddedFilesENG)
                {
                    string fileName = file.Name.Substring(0, file.Name.Length - 4);

                    //Add ignored localization files to the language's dictionary...
                    bool addENG = false;

                    foreach (string str in localisationIgnoreDirectoryList)
                    {
                        if (file.FullName.Contains(str))
                        {
                            Game.Log("Loading ignored localization reference from ENG: " + fileName + " (ignore dir " + str + ")");
                            addENG = true;
                        }
                    }

                    if (!addENG)
                    {
                        //Ignore list...
                        if (localisationIgnoreFileList.Contains(fileName))
                        {
                            Game.Log("Loading ignored localization reference from ENG: " + fileName);
                            addENG = true;
                        }
                    }

                    if (!stringTableENG.ContainsKey(fileName))
                    {
                        stringTableENG.Add(fileName, new Dictionary<string, DisplayString>());
                    }

                    //Parse to class file
                    using (StreamReader streamReader = System.IO.File.OpenText(file.FullName))
                    {
                        int currentLineIndex = 0;

                        //Skip the first 3 lines
                        for (int i = 0; i < 3; i++)
                        {
                            streamReader.ReadLine();
                            currentLineIndex++;
                        }

                        while (!streamReader.EndOfStream)
                        {
                            string line = streamReader.ReadLine();

                            string key = string.Empty;
                            string display = string.Empty;
                            string alt = string.Empty;
                            int frequency = 0;
                            bool suffix = false;

                            ParseLine(line, out key, out _, out display, out alt, out frequency, out suffix, out _);

                            //Convert to display safe
                            display = ConvertLineBreaksToDisplay(display);
                            alt = ConvertLineBreaksToDisplay(alt);

                            if (key.Length > 0)
                            {
                                LoadIntoDictionaryENG(fileName, currentLineIndex, key, display, alt, frequency, suffix);

                                if(addENG)
                                {
                                    if (!stringTable.ContainsKey(fileName))
                                    {
                                        stringTable.Add(fileName, new Dictionary<string, DisplayString>());
                                    }

                                    //Save pathname
                                    if (!dictionaryPathnames.ContainsKey(fileName))
                                    {
                                        dictionaryPathnames.Add(fileName, file.FullName);
                                    }

                                    //Game.Log("Loading ENG because of ignored localization: " + fileName);
                                    LoadIntoDictionary(fileName, currentLineIndex, key, display, alt, frequency, suffix);
                                }
                            }

                            currentLineIndex++;
                        }
                    }
                }

                backupENGLoaded = true;
            }
            else
            {
                Game.Instance.language = "English";
            }
        }

        //New version: Load text files from streaming assets folder
        //Get embedded files
        DirectoryInfo embed = new DirectoryInfo(saveLoadPath + "/Strings/" + Game.Instance.language + "/");
        List<FileInfo> embeddedFiles = embed.GetFiles("*.csv", SearchOption.AllDirectories).ToList();

        int backupENGused = 0;

        foreach (FileInfo file in embeddedFiles)
        {
            //Game.Log("Loading file: " + file.FullName);

            string fileName = file.Name.Substring(0, file.Name.Length - 4);

            //Save pathname
            if(!dictionaryPathnames.ContainsKey(fileName))
            {
                dictionaryPathnames.Add(fileName, file.FullName);
            }

            //Find template file
            if(templateFile == null)
            {
                if(fileName == "template")
                {
                    templateFile = file;
                    continue;
                }
            }

            if (!stringTable.ContainsKey(fileName))
            {
                stringTable.Add(fileName, new Dictionary<string, DisplayString>());
            }

            //Parse to class file
            using (StreamReader streamReader = System.IO.File.OpenText(file.FullName))
            {
                int currentLineIndex = 0;

                //Skip the first 3 lines
                for (int i = 0; i < 3; i++)
                {
                    streamReader.ReadLine();
                    currentLineIndex++;
                }

                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();

                    string key = string.Empty;
                    string display = string.Empty;
                    string alt = string.Empty;
                    int frequency = 0;
                    bool suffix = false;
         
                    ParseLine(line, out key, out _, out display, out alt, out frequency, out suffix, out _);

                    //Convert to display safe
                    display = ConvertLineBreaksToDisplay(display);
                    alt = ConvertLineBreaksToDisplay(alt);

                    if (key.Length > 0)
                    {
                        //Is this a missing localization?
                        if(display == Strings.missingString)
                        {
                            //Fall back to english...
                            display = GetENG(fileName, key, false);
                            alt = GetENG(fileName, key, true);
                            backupENGused++;
                        }

                        LoadIntoDictionary(fileName, currentLineIndex, key, display, alt, frequency, suffix);
                    }

                    currentLineIndex++;
                }
            }
        }

        if(Game.Instance.language != "English")
        {
            Game.Log("CityGen: Backup English keys used for " + Game.Instance.language + ": " + backupENGused);
        }

        textFilesLoaded = true;

        //Write a timestamp for everything: WARNING: DON'T DO THIS UNLESS YOU'RE SURE IT'S NEEDED!!!
        //foreach(KeyValuePair<string, Dictionary<string, DisplayString>> pair1 in stringTable)
        //{
        //    foreach (KeyValuePair<string, DisplayString> pair2 in pair1.Value)
        //    {
        //        WriteTimestamp(pair1.Key, pair2.Value);
        //    }
        //}
    }

    public static void ParseLine(string input, out string key, out string notes, out string display, out string alt, out int frequency, out bool suffix, out string misc, bool useFieldQuotations = true)
    {
        key = string.Empty;
        notes = string.Empty;
        display = string.Empty;
        alt = string.Empty;
        frequency = 0;
        suffix = false;
        misc = string.Empty;

        if (input == null) return;

        List<string> lineOutput = null;

        if(useFieldQuotations)
        {
            //Used this technqiue to split the string: https://stackoverflow.com/questions/3147836/c-sharp-regex-split-commas-outside-quotes
            char separatorChar = ',';
            System.Text.RegularExpressions.Regex regx = new System.Text.RegularExpressions.Regex(separatorChar + "(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
            lineOutput = regx.Split(input).ToList();
        }
        else
        {
            lineOutput = input.Split(new char[] { ',' }, StringSplitOptions.None).ToList();
        }

        //The first column is the game string access key: Use lower case to make sure there are no casing errors.
        if (lineOutput.Count > 0)
        {
            for (int i = 0; i < lineOutput.Count; i++)
            {
                //Remove quotations
                if (lineOutput[i].Length >= 2 && lineOutput[i][0] == '"' && lineOutput[i][lineOutput[i].Length - 1] == '"')
                {
                    lineOutput[i] = lineOutput[i].Substring(1, lineOutput[i].Length - 2);
                }
            }

            key = lineOutput[0].ToLower();
            if (lineOutput.Count > 1) notes = lineOutput[1];
            if (lineOutput.Count > 2) display = lineOutput[2];
            if (lineOutput.Count > 3) alt = lineOutput[3];
            if (lineOutput.Count > 4)
            {
                int.TryParse(lineOutput[4], out frequency);
                //Game.Log(lineOutput[4] + " = " + frequency);
            }
            if (lineOutput.Count > 5 && lineOutput[5].Length > 0) suffix = true;
            if (lineOutput.Count > 6) misc = lineOutput[6];
        }
    }

    //Load to internal dictionary
    private static void LoadIntoDictionary(string fileName, int lineNo, string key, string display, string alternate, int frequency, bool suffix)
    {
        if (key == "")
        {
            return;
        }
        else if (display == "")
        {
            return;
        }

        //Add to regular dictionary
        if (stringTable.ContainsKey(fileName))
        {
            if (!stringTable[fileName].ContainsKey(key))
            {
                //Game.Log("Add to string table: " + fileName + " " + key);
                stringTable[fileName].Add(key, new DisplayString { displayStr = display, alternateStr = alternate, lineNumber = lineNo});
            }
            else
            {
                Game.LogError("CityGen: Duplicate " + key + " found in " + fileName + ".");
                return;
            }
        }

        if (frequency > 0)
        {
            if (!randomEntryLists.ContainsKey(fileName))
            {
                randomEntryLists.Add(fileName, new List<RandomDisplayString>());
            }

            frequency = Mathf.Clamp(frequency, 0, 100); //Hard limit appearance amount

            //Create class (only if type of name)
            RandomDisplayString rw = new RandomDisplayString { displayStr = display, alternateStr = alternate, needsSuffixForShortName = suffix };

            for (int i = 0; i < frequency; i++)
            {
                randomEntryLists[fileName].Add(rw);
            }
        }
    }

    //Load to internal dictionary (ENG Backup)
    private static void LoadIntoDictionaryENG(string fileName, int lineNo, string key, string display, string alternate, int frequency, bool suffix)
    {
        if (key == "")
        {
            return;
        }
        else if (display == "")
        {
            return;
        }

        //Add to regular dictionary
        if (stringTableENG.ContainsKey(fileName))
        {
            if (!stringTableENG[fileName].ContainsKey(key))
            {
                stringTableENG[fileName].Add(key, new DisplayString { displayStr = display, alternateStr = alternate, lineNumber = lineNo });
            }
            else
            {
                Game.LogError("CityGen: Duplicate " + key + " found in " + fileName + ".");
                return;
            }
        }

        if (frequency > 0)
        {
            if (!randomEntryListsENG.ContainsKey(fileName))
            {
                randomEntryListsENG.Add(fileName, new List<RandomDisplayString>());
            }

            frequency = Mathf.Clamp(frequency, 0, 100); //Hard limit appearance amount

            //Create class (only if type of name)
            RandomDisplayString rw = new RandomDisplayString { displayStr = display, alternateStr = alternate, needsSuffixForShortName = suffix };

            for (int i = 0; i < frequency; i++)
            {
                randomEntryListsENG[fileName].Add(rw);
            }
        }
    }

    public static string Get(string dictionary, string key, Casing casing = Casing.asIs, bool getAlternate = false, bool forceNoWrite = false, bool useGenderReference = false, Human genderReference = null)
	{
        //Load text files
        if (!textFilesLoaded) Strings.Instance.LoadTextFiles();

        //If keys are zero, abort
        if (dictionary.Length <= 0 || key.Length <= 0)
        {
            Game.LogError("CityGen: Invalid key length for key " + key + " dictionary: " + dictionary);
            return string.Empty;
        }

		string output = string.Empty;

		//All keys are lower case, so make sure they are here too...
		string dictLC = dictionary.ToLower();
		string keyLC = key.ToLower();

        if(stringTable.ContainsKey(dictLC))
        {
            DisplayString disp = null;

            //Attempt to get gendered...
            if (useGenderReference && genderReference != null)
            {
                if (genderReference.gender == Human.Gender.female)
                {
                    string femaleKey = keyLC + "_F";

                    if (stringTable[dictLC].ContainsKey(femaleKey))
                    {
                        disp = stringTable[dictLC][femaleKey];
                    }
                }
                else if (genderReference.gender == Human.Gender.nonBinary)
                {
                    string maleKey = keyLC + "_NB";

                    if (stringTable[dictLC].ContainsKey(maleKey))
                    {
                        disp = stringTable[dictLC][maleKey];
                    }
                }
            }

            if(stringTable[dictLC].ContainsKey(keyLC))
            {
                if(disp == null) disp = stringTable[dictLC][keyLC];

                if (getAlternate)
                {
                    output = disp.alternateStr;
                }
                else
                {
                    output = disp.displayStr;
                }
            }
            else
            {
                //If this is another language, attempt to get from the ENG dictionary...
                if(Game.Instance.language != "English")
                {
                    if (stringTableENG.ContainsKey(dictLC))
                    {
                        if (stringTableENG[dictLC].ContainsKey(keyLC))
                        {
                            disp = stringTableENG[dictLC][keyLC];

                            if (getAlternate)
                            {
                                output = disp.alternateStr;
                            }
                            else
                            {
                                output = disp.displayStr;
                            }
                        }
                    }
                }

                if(output.Length <= 0)
                {
                    Game.Log("CityGen: Unable to load entry " + keyLC + " from dictionary " + dictLC);

                    //foreach(KeyValuePair<string, DisplayString> pair in stringTable[dictLC])
                    //{
                    //    Game.Log(pair.Key);
                    //}

                    //If write to file is enabled, attempt to load the file and add the entry to it.
                    if (Game.Instance.writeUnfoundToTextFiles && !forceNoWrite && Game.Instance.language == "English")
                    {
                        #if UNITY_EDITOR
                        WriteToDictionary(dictLC, keyLC, string.Empty, key, string.Empty, 0, false, string.Empty);

                        //Attempt one more time to get it
                        return Get(dictionary, key, casing, getAlternate, true);
                        #endif
                    }
                    else
                    {
                        Game.Log("Invalid String Address");
                        output = "<InvalidStringAddress>";
                    }
                }
            }
        }
        else
        {
            Game.LogError("CityGen: Dictionary " + dictLC + " appears to be missing: Please create the file.");
        }

        //Work around for line breaks
        //output.Replace("\\n", "\n");

        return ApplyCasing(output, casing);
	}

    //Write timestamp to misc
    public static void WriteTimestamp(string dictionary, DisplayString disp)
    {
        //If keys are zero, abort
        if (dictionary.Length <= 0)
        {
            return;
        }

        //All keys are lower case, so make sure they are here too...
        string dictLC = dictionary.ToLower();

        if (stringTable.ContainsKey(dictLC))
        {
            //Write access timestamp
            if (disp.lineNumber > 2)
            {
                string[] arrLine = System.IO.File.ReadAllLines(dictionaryPathnames[dictLC]);

                string line = GetLineFromFile(dictLC, disp.lineNumber);

                //Parse and re-enter line
                string key2 = string.Empty;
                string notes = string.Empty;
                string display = string.Empty;
                string alt = string.Empty;
                int frequency = 0;
                bool suffix = false;
                string misc = string.Empty;

                ParseLine(line, out key2, out notes, out display, out alt, out frequency, out suffix, out misc);

                string freq = string.Empty;
                if (frequency > 0) freq = frequency.ToString();

                string suff = string.Empty;
                if (suffix) suff = "Y";

                misc = DateTime.Now.ToString("HH:mm dd/MM/yyyy");

                string compiledString = "\"" + key2 + "\"" + "," + "\"" + notes + "\"" + "," + "\"" + display + "\"" + "," + "\"" + alt + "\"" + "," + freq + "," + "\"" + suff + "\"" + "," + "\"" + misc + "\"";

                arrLine[disp.lineNumber] = compiledString;
                System.IO.File.WriteAllLines(dictionaryPathnames[dictLC], arrLine);
            }
        }
    }

    //For retrieving for the backup ENG dictionary
    public static string GetENG(string dictionary, string key, bool getAlternate = false)
    {
        //If keys are zero, abort
        if (dictionary.Length <= 0 || key.Length <= 0)
        {
            Game.LogError("CityGen: Invalid key length for key " + key + " dictionary: " + dictionary);
            return string.Empty;
        }

        string output = string.Empty;

        //All keys are lower case, so make sure they are here too...
        string dictLC = dictionary.ToLower();
        string keyLC = key.ToLower();

        if (stringTableENG.ContainsKey(dictLC))
        {
            if (stringTableENG[dictLC].ContainsKey(keyLC))
            {
                DisplayString disp = stringTableENG[dictLC][keyLC];

                if (getAlternate)
                {
                    output = disp.alternateStr;
                }
                else
                {
                    output = disp.displayStr;
                }
            }
            else
            {
                Game.Log("CityGen: Unable to load entry " + keyLC + " from dictionary " + dictLC);
            }
        }
        else
        {
            Game.LogError("CityGen: Dictionary " + dictLC + " appears to be missing: Please create the file.");
        }

        //Work around for line breaks
        //output.Replace("\\n", "\n");

        return output;
    }

    public static string GetLineFromFile(string dictionary, int lineNumber)
    {
        using (StreamReader streamReader = System.IO.File.OpenText(dictionaryPathnames[dictionary]))
        {
            for (int i = 0; i < lineNumber; i++)
            {
                streamReader.ReadLine();
            }

            return streamReader.ReadLine();
        }
    }

    public static string ApplyCasing(string input, Casing casing = Casing.asIs)
    {
        //Apply casing
        if (casing == Casing.lowerCase)
        {
            input = input.ToLower();
        }
        else if (casing == Casing.upperCase)
        {
            input = input.ToUpper();
        }
        else if (casing == Casing.firstLetterCaptial)
        {
            if (input.Length <= 1)
            {
                input = input.ToUpper();
            }
            else
            {
                input = input.ToLower();
                input = input.Substring(0, 1).ToUpper() + input.Substring(1, input.Length - 1);
            }
        }
        else if (casing == Casing.pascalCase)
        {
            // If there are 0 or 1 characters, just return the string.
            if (input == null) return input;
            if (input.Length < 2) input = input.ToUpper();

            // Split the string into words.
            string[] words = input.Split(
                new char[] { },
                StringSplitOptions.RemoveEmptyEntries);

            // Combine the words.
            string result = string.Empty;

            for (int i = 0; i < words.Length; i++)
            {
                string word = words[i];

                result +=
                word.Substring(0, 1).ToUpper() +
                word.Substring(1);

                if (i < words.Length - 1) result += " "; //Add space between words
            }

            input = result;
        }

        return input;
    }

    //Write to dictionary
    public static void WriteToDictionary(string dictionaryName, string key, string notes, string display, string alternate = "", int frequency = 0, bool requiresSuffix = false, string misc = "" )
    {
        string dict = dictionaryName.ToLower();

        if(dictionaryPathnames.ContainsKey(dict))
        {
            Game.Log("CityGen: Writing new key to dictionary " + dictionaryName + ": " + key);

            string freq = string.Empty;
            if (frequency > 0) freq = frequency.ToString();

            string suffix = string.Empty;
            if (requiresSuffix) suffix = "Y";

            //if(Game.Instance.timestampStringAccess)
            //{
                misc = DateTime.Now.ToString("HH:mm dd/MM/yyyy"); //Always write the date this was added to the dictionary
            //}

            string compiledString = "\"" + key + "\"" + "," + "\"" + notes + "\"" + "," + "\"" + display + "\"" + "," + "\"" + alternate + "\"" + "," + freq + "," + "\"" + suffix + "\"" + "," + "\"" + misc + "\"";

            using (StreamWriter sw = System.IO.File.AppendText(dictionaryPathnames[dict]))
            {
                sw.WriteLine(compiledString);
            }

            //Also load to internal files
            LoadIntoDictionary(dict, System.IO.File.ReadLines(dictionaryPathnames[dict]).Count() - 1, key, display, alternate, frequency, requiresSuffix);
        }
    }

    public static void RemoveFromDictionary(string dictionaryName, string key)
    {
        string dict = dictionaryName.ToLower();

        if (dictionaryPathnames.ContainsKey(dict))
        {
            Game.Log("CityGen: Removing key from dictionary " + dictionaryName + ": " + key);

            //Find the key
            string keyLC = key.ToLower();

            if (stringTable.ContainsKey(dict))
            {
                if (stringTable[dict].ContainsKey(keyLC))
                {
                    DisplayString disp = stringTable[dict][keyLC];

                    List<string> quotelist = System.IO.File.ReadAllLines(dictionaryPathnames[dict]).ToList();
                    quotelist.RemoveAt(disp.lineNumber);
                    System.IO.File.WriteAllLines(dictionaryPathnames[dict], quotelist.ToArray());
                }
            }
        }
    }

    public static string GetRandom(string dictionary, out bool needsSuffixForShortName, out string alternate, string useCustomSeed = "")
	{
        //Load text files
        if (!textFilesLoaded) Strings.Instance.LoadTextFiles();

        return GetRandom(dictionary, string.Empty, 0, out needsSuffixForShortName, out alternate, useCustomSeed);
	}

	public static string GetRandom(string dictionary, string alliterationStr, int alliterationWeight, out bool needsSuffixForShortName, out string alternate, string useCustomSeed = "")
	{
        //Load text files
        if (!textFilesLoaded) Strings.Instance.LoadTextFiles();

        alternate = string.Empty;
        needsSuffixForShortName = false;

		//If keys are zero, abort
		if (dictionary.Length <= 0) return "<InvalidKeyLength>";

        RandomDisplayString output = null;

		//All keys are lower case, so make sure they are here too...
		string dictLC = dictionary.ToLower();

		List<RandomDisplayString> useOriginal = null;

		//List to use
		try
		{
			useOriginal = randomEntryLists[dictLC];
		}
		catch
		{
			Game.Log("CityGen: Cannot find " + dictLC + " random word list");
			return string.Empty;
		}

        if(useOriginal.Count <= 0)
        {
            Game.Log("CityGen: Found list " + dictLC + " but it's empty!");
        }

		//Choose a random entry
		int random = 0;

		//If using alliteration weight, make a copy of the word list and add extra words that start with x letter
		if(alliterationWeight > 0 && alliterationStr.Length > 0)
		{
			List<RandomDisplayString> useListCopy = new List<RandomDisplayString>();

			foreach(RandomDisplayString str in randomEntryLists[dictLC])
			{
				if(str.displayStr.Length <= 0) continue;

				//Add one for regular copy
				useListCopy.Add(str);

				//Add x extra for weighting
				if(str.displayStr.Substring(0, 1) == alliterationStr)
				{
                    for (int n = 0; n < alliterationWeight; n++)
                    {
                        useListCopy.Add(str);
                    }
				}
			}

            if (useCustomSeed.Length > 0) random = Toolbox.Instance.RandContained(0, useListCopy.Count, useCustomSeed, out useCustomSeed);
            else random = Toolbox.Instance.Rand(0, useListCopy.Count);

            try
			{
                output = useListCopy[random];
                needsSuffixForShortName = useListCopy[random].needsSuffixForShortName;
			}
			catch
			{
                Game.Log("CityGen: No alliteration for" + dictionary);
                return "<RandomInvalidStringAddress-Alliteration>";
			}
		}
		else
		{
            if (useCustomSeed.Length > 0) random = Toolbox.Instance.RandContained(0, useOriginal.Count, useCustomSeed, out useCustomSeed);
            else random = Toolbox.Instance.Rand(0, useOriginal.Count);

            try
			{
                output = useOriginal[random];
                needsSuffixForShortName = useOriginal[random].needsSuffixForShortName;
            }
			catch
			{
				return "<RandomInvalidStringAddress-NoAlliteration>";
			}
		}

        alternate = output.alternateStr;
        return output.displayStr;
	}

	//Method to split a string cleanly (by char)
	public static string[] CleanSplit(string input, char del, bool trimElements, bool removeEmpty = true)
	{
        //input = input.Trim();
        StringSplitOptions splitOptions = StringSplitOptions.RemoveEmptyEntries;
        if (!removeEmpty) splitOptions = StringSplitOptions.None;

        char[] deliminators = { del };

		string[] parsed = input.Split(deliminators, splitOptions);

		if(trimElements)
		{
			for(int n = 0; n < parsed.Length; ++n)
			{
				parsed[n] = parsed[n].Trim();
			}
		}

		return parsed;
	}

	//Method to split a string cleanly (by string)
	public static string[] CleanSplit(string input, string[] del, bool trimElements)
	{
		//input = input.Trim();

		string[] parsed = input.Split(del, StringSplitOptions.None);

		if(trimElements)
		{
			for(int n = 0; n < parsed.Length; ++n)
			{
				parsed[n] = parsed[n].Trim();
			}
		}

		return parsed;
	}

    //Change line breaks to a save-safe format
    public static string ConvertLineBreaksToSaveSafe(string input)
    {
        input = input.Replace("\n", "\\n");
        input = input.Replace("\r", "\\r");
        input = input.Replace("\t", "\\t");

        return input;
    }

    //Change line breaks to a display-safe format
    public static string ConvertLineBreaksToDisplay(string input)
    {
        input = input.Replace("\\n", "\n");
        input = input.Replace("\\r", "\r");
        input = input.Replace("\\t", "\t");

        return input;
    }

    public static string GetTextForComponent(string msgID, object obj, Human from = null, Human to = null, string lineBreaks = "\n", bool skipFirstBlock = false, object additionalObject = null, LinkSetting linkSetting = LinkSetting.automatic, List<Evidence.DataKey> dataKeys = null)
    {
        if (msgID == null || msgID.Length <= 0)
        {
            Game.LogError("No DDS message found: Null or message ID of length 0");
        }

        Acquaintance aq = null;

        if (to != null && from != null)
        {
            from.FindAcquaintanceExists(to, out aq);
        }

        List<string> say = null;

        if (from == null)
        {
            //Game.Log("Evidence: Evidence writer is null, using player...");
            say = Player.Instance.ParseDDSMessage(msgID, aq, out _, passedObject: obj);
        }
        else
        {
            say = from.ParseDDSMessage(msgID, aq, out _, passedObject: obj);
        }

        StringBuilder sb = new StringBuilder();

        //The first message should skip the first block (subject)
        int startBlock = 0;
        if (skipFirstBlock && say.Count > 1) startBlock = 1;

        for (int i = startBlock; i < say.Count; i++)
        {
            string composed = ComposeText(Get("dds.blocks", say[i], useGenderReference: true, genderReference: from), obj, additionalObject: additionalObject, linkSetting: linkSetting, evidenceKeys: dataKeys);

            if (i < say.Count - 1)
            {
                composed += lineBreaks; //Add line break
            }

            sb.Append(composed);
        }

        return sb.ToString();
    }

    [Button]
    public void OutputTextForLoc()
    {
        //Load all text files...
        //We're usually going to be using the streaming assets path, unless this is the standalone DDS editor (when we want to use the exe folder)
        string saveLoadPath = Application.streamingAssetsPath;

        //New version: Load text files from streaming assets folder
        //Get embedded files
        DirectoryInfo embed = new DirectoryInfo(saveLoadPath + "/Strings/English/");
        List<FileInfo> embeddedFiles = embed.GetFiles("*.csv", SearchOption.AllDirectories).ToList();

        //Create a new output file...
        string outputPath = saveLoadPath + "/Strings/Localization/SoD_Localization_All.csv";

        List<string> outputList = new List<string>();
        outputList.Add("\"" + "Key" + "\"" + "," + "\"" + "ENGLISH" + "\"" + "," + "\"" + "FRENCH" + "\"" + "," + "\"" + "GERMAN" + "\"" + "," + "\"" + "SPANISH" + "\"" + ", " +"\"" + "BRAZILIAN PORTUGUESE" + "\"" + ", " + "\"" + "SIMPLIFIED CHINESE" + "\"" + "," + "\"" + "TRADITIONAL CHINESE" + "\"" + "," + "\"" + "JAPANESE" + "\"" + "," + "\"" + "RUSSIAN" + "\"");
        outputList.Add(string.Empty);
        outputList.Add(string.Empty);

        int wordCount = 0;

        //Use this opportunity to anylise duplicates...
        Dictionary<string, int> duplicateCheck = new Dictionary<string, int>();
        int duplicates = 0;
        int ignored = 0;

        foreach (FileInfo file in embeddedFiles)
        {
            string fileName = file.Name.Substring(0, file.Name.Length - 4);

            bool skip = false;

            foreach (string str in localisationIgnoreDirectoryList)
            {
                if (file.FullName.Contains(str))
                {
                    Debug.Log("Skipping: " + fileName + " (ignore dir " + str + ")");
                    skip = true;
                }
            }

            if (!skip)
            {
                //Ignore list...
                if (localisationIgnoreFileList.Contains(fileName))
                {
                    Debug.Log("Skipping File: " + fileName + "...");
                    skip = true;
                    continue;
                }
                else
                {
                    Debug.Log("Processing File: " + fileName + "...");
                }
            }
            else continue;

            //Parse to class file
            using (StreamReader streamReader = System.IO.File.OpenText(file.FullName))
            {
                int currentLineIndex = 0;

                //Skip the first 3 lines
                for (int i = 0; i < 3; i++)
                {
                    streamReader.ReadLine();
                    currentLineIndex++;
                }

                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();
                    bool skipLine = false;

                    string key = string.Empty;
                    string display = string.Empty;
                    string notes = string.Empty;
                    string misc = string.Empty;

                    ParseLine(line, out key, out notes, out display, out _, out _, out _, out misc);

                    //Ignore line...
                    if(useIgnoreFlagInNotes)
                    {
                        if(notes.ToLower().Contains(ignoreFlag.ToLower()))
                        {
                            Debug.Log("Ignoring line with key " + key + "...");
                            skipLine = true;
                            ignored++;
                        }
                    }

                    if(onlyOuputChangesSince && !skipLine)
                    {
                        DateTime editedAt = new DateTime();

                        if (DateTime.TryParseExact(misc, "HH:mm dd/MM/yyyy", new System.Globalization.CultureInfo("en-GB"), System.Globalization.DateTimeStyles.None, out editedAt))
                        {
                            DateTime outputAfter = new DateTime();

                            if (DateTime.TryParseExact(outputSinceDate, "HH:mm dd/MM/yyyy", new System.Globalization.CultureInfo("en-GB"), System.Globalization.DateTimeStyles.None, out outputAfter))
                            {
                                if(editedAt > outputAfter)
                                {
                                    Debug.Log(editedAt.ToString() + " edited after " + outputAfter.ToString() +", including output...");
                                }
                                else
                                {
                                    skipLine = true;
                                    ignored++;
                                }
                            }
                            else
                            {
                                Debug.LogError("Unable to parse date/time: " + outputSinceDate + " this will NOT be included in the output...");
                                skipLine = true;
                                ignored++;
                            }
                        }
                        else
                        {
                            Debug.LogError("Unable to parse date/time: " + misc + " this will NOT be included in the output...");
                            skipLine = true;
                            ignored++;
                        }
                    }

                    if(!skip && !skipLine)
                    {
                        bool addNew = true;
                        int existingLineNo = -1;

                        if (duplicateCheck.TryGetValue(display, out existingLineNo))
                        {
                            duplicates++;

                            //Append this key to the original copy
                            if(condenseIdenticalEnglishContentIntoOneKey)
                            {
                                //Parse the original key, add the new one
                                //char separatorChar = ',';
                                //System.Text.RegularExpressions.Regex regx = new System.Text.RegularExpressions.Regex(separatorChar + "(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                                string[] lineOutput = outputList[existingLineNo].Split(new char[] { '\"', ',' }, StringSplitOptions.RemoveEmptyEntries);

                                //Get the existing key and add this one
                                string combinedKeys = lineOutput[0].ToLower() + "<" + fileName + ":" + key + ">";

                                outputList[existingLineNo] = "\"" + combinedKeys + "\"" + "," + "\"" + display + "\"";
                                Debug.Log("Combined keys: " + combinedKeys + "...");

                                addNew = false;
                            }
                        }

                        if(addNew)
                        {
                            //Count words
                            string[] words = display.Split(new char[] { ' ', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                            wordCount += words.Length;

                            string compiledString = "\"" + "<" + fileName + ":" + key + ">" + "\"" + "," + "\"" + display + "\"";

                            //Add extra line breaks before next entry...
                            for (int i = 0; i < extraLineBreaks; i++)
                            {
                                outputList.Add(string.Empty);
                            }

                            outputList.Add(compiledString);

                            if (!duplicateCheck.ContainsKey(display))
                            {
                                duplicateCheck.Add(display, outputList.Count - 1);
                            }
                        }
                    }

                    currentLineIndex++;
                }
            }
        }

        Debug.Log("Outputting " + wordCount + " words to file at " + outputPath + ". There are " + duplicates + " duplicate entries. There are " + ignored + " ignored keys.");
        duplicateCheck = new Dictionary<string, int>();

        System.IO.File.WriteAllLines(outputPath, outputList);
    }

    [Button]
    public void ImportNonEnglish()
    {
        Debug.Log("Importing non english from localization input file...");
        List<string> debugOutput = new List<string>();

        DateTime now;

        if (!DateTime.TryParse(inputDate, out now))
        {
            Debug.Log("Unable to parse input date!");
            debugOutput.Add("Unable to parse input date!");
            return;
        }

        //We're usually going to be using the streaming assets path, unless this is the standalone DDS editor (when we want to use the exe folder)
        string saveLoadPath = Application.streamingAssetsPath;

        List<string> templateLines = new List<string>(); //The template file stored as lines

        //Entire non-english content to be written: Language/File/Key = content (whole line including key)
        Dictionary<string, Dictionary<string, Dictionary<string, string>>> nonEnglishContent = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();
        int lineCount = 0;
        List<string> genderVariations = new List<string>();
        List<string> genderVariationFiles = new List<string>();

        //Load the template file
        string templatePath = saveLoadPath + "/Strings/Localization/" + templateInputFile;

        //Read the input file
        using (StreamReader streamReader = System.IO.File.OpenText(templatePath))
        {
            //Read each line
            while (!streamReader.EndOfStream)
            {
                string line = streamReader.ReadLine();
                templateLines.Add(line);
            }
        }

        if(templateLines.Count <= 0)
        {
            Debug.Log("Invalid template file!");
            return;
        }

        Debug.Log("Writing localization content at " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString());
        debugOutput.Add("Writing localization content at " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString());

        //Create path to the input file
        string inputPath = saveLoadPath + "/Strings/Localization/" + localisationInputFile;

        //Read the input file
        using (StreamReader streamReader = System.IO.File.OpenText(inputPath))
        {
            int currentLineIndex = 0;

            //Skip the first line
            for (int i = 0; i < 1; i++)
            {
                streamReader.ReadLine();
                currentLineIndex++;
            }

            //Record the last valid keys
            List<string> lastKeys = new List<string>();

            int genderVariation = 0;

            //Read each line
            while (!streamReader.EndOfStream)
            {
                string line = streamReader.ReadLine();

                //Parse the line into string list
                //Used this technqiue to split the string: https://stackoverflow.com/questions/3147836/c-sharp-regex-split-commas-outside-quotes
                char separatorChar = ',';
                System.Text.RegularExpressions.Regex regx = new System.Text.RegularExpressions.Regex(separatorChar + "(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                List<string> lineOutput = regx.Split(line).ToList();

                //Check to see if we have content on this line (IMPORTANT: Skip the last column as it features a line number reference
                bool lineContent = false;
                int checkColumnsForContent = lineOutput.Count;
                if (inputFeaturesLastColumnLineNumbers) checkColumnsForContent = checkColumnsForContent - 1;

                for (int i = 0; i < checkColumnsForContent; i++)
                {
                    if(lineOutput[i].Length > 0)
                    {
                        lineContent = true;
                    }
                }

                //The first column is the game string access key: Use lower case to make sure there are no casing errors.
                if (lineContent && lineOutput.Count > 0) //Proceed if we have anything on this line...
                {
                    for (int i = 0; i < lineOutput.Count; i++)
                    {
                        //Remove quotations
                        if (lineOutput[i].Length >= 2 && lineOutput[i][0] == '"' && lineOutput[i][lineOutput[i].Length - 1] == '"')
                        {
                            lineOutput[i] = lineOutput[i].Substring(1, lineOutput[i].Length - 2);
                        }
                    }

                    //Get the key (always first column)
                    string allKeys = lineOutput[0].ToLower();
                    List<string> keysWithFiles = new List<string>();

                    //Parse the keys
                    string currentKey = string.Empty;
                    bool keyOpen = false;

                    for (int i = 0; i < allKeys.Length; i++)
                    {
                        char currentChar = allKeys[i];

                        //Open a new key
                        if(currentChar == '<' && !keyOpen)
                        {
                            keyOpen = true;
                        }
                        //Close a key
                        else if(currentChar == '>' && keyOpen)
                        {
                            keysWithFiles.Add(currentKey);
                            currentKey = string.Empty;
                            keyOpen = false;
                        }
                        //Add to an open key
                        else if(keyOpen)
                        {
                            currentKey += currentChar;
                        }
                    }

                    //We should now have the new keys for this line (if any), record them as the last ones used in order to make sense of F/NB variations that might appear below it
                    if (keysWithFiles.Count > 0)
                    {
                        debugOutput.Add("New set of keys found on line " + line);
                        genderVariation = 0; //Reset gender variation to 0 (neutral)
                        lastKeys.Clear();
                        lastKeys.AddRange(keysWithFiles);
                    }
                    else
                    {
                        genderVariation++;
                        debugOutput.Add("Gender variation ++ : " + genderVariation);
                    }

                    //Loop through each key assigned to this line (referenced here as key:name)
                    foreach(string keyWithFile in lastKeys)
                    {
                        //Parse the key and the name
                        string fileName = string.Empty;
                        string key = string.Empty;

                        for (int i = 0; i < keyWithFile.Length; i++)
                        {
                            //We've found the first break; use this to determin the filename...
                            if(keyWithFile[i] == ':')
                            {
                                fileName = keyWithFile.Substring(0, i);
                                key = keyWithFile.Substring(i + 1);

                                if (genderVariation == 1) key += "_F";
                                else if (genderVariation == 2) key += "_NB";

                                break;
                            }
                        }

                        //If key is empty, skip...
                        if (key.Length > 0)
                        {
                            //Parse the rest of this line, skipping the english content...
                            for (int i = 2; i <= fileInputConfig.Count; i++)
                            {
                                //Skip null lines
                                if (lineOutput[i] == null || lineOutput[i].Length <= 0) continue;

                                //Find the settings for the correct language
                                LocInput columnInput = fileInputConfig.Find(item => item.documentColumn == i);

                                if (columnInput != null)
                                {
                                    //Check to see if we have an existing key in loaded content, if we do then compare dates...
                                    if (nonEnglishContent.ContainsKey(columnInput.languageCode))
                                    {
                                        if(nonEnglishContent[columnInput.languageCode].ContainsKey(fileName))
                                        {
                                            //Key exists...
                                            if(nonEnglishContent[columnInput.languageCode][fileName].ContainsKey(key))
                                            {
                                                //Parse the existing key...
                                                string date;

                                                ParseLine(nonEnglishContent[columnInput.languageCode][fileName][key], out _, out _, out _, out _, out _, out _, out date);

                                                //Compare this date to the input date in the settings...
                                                date = date.Trim();
                                                DateTime lastWritten;

                                                if(date != null && date.Length > 0)
                                                {
                                                    if (DateTime.TryParse(date, out lastWritten))
                                                    {
                                                        //Skip this if the written is newer than now
                                                        if(lastWritten > now)
                                                        {
                                                            Debug.Log("Existing key " + columnInput.languageCode + ", " + fileName + ", " + key + " is newer (" + lastWritten.ToString() + ") than the input date (" + now.ToString() + "), skipping...");
                                                            debugOutput.Add("Existing key " + columnInput.languageCode + ", " + fileName + ", " + key + " is newer (" + lastWritten.ToString() + ") than the input date (" + now.ToString() + "), skipping...");
                                                            continue;
                                                        }
                                                        else if(lastWritten < now)
                                                        {
                                                            debugOutput.Add("Existing key " + columnInput.languageCode + ", " + fileName + ", " + key + " is older (" + lastWritten.ToString() + ") than the input date (" + now.ToString() + "), overwriting with new content...");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Debug.Log("Unable to parse date: " + date + " (Key: " + columnInput.languageCode + ", " + fileName + ", " + key + ")");
                                                        debugOutput.Add("Unable to parse date: " + date + " (Key: " + columnInput.languageCode + ", " + fileName + ", " + key + ")");
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    //Write to content
                                    if(!nonEnglishContent.ContainsKey(columnInput.languageCode))
                                    {
                                        nonEnglishContent.Add(columnInput.languageCode, new Dictionary<string, Dictionary<string, string>>());
                                    }

                                    if (!nonEnglishContent[columnInput.languageCode].ContainsKey(fileName))
                                    {
                                        nonEnglishContent[columnInput.languageCode].Add(fileName, new Dictionary<string, string>());
                                    }

                                    if (!nonEnglishContent[columnInput.languageCode][fileName].ContainsKey(key))
                                    {
                                        nonEnglishContent[columnInput.languageCode][fileName].Add(key, string.Empty);
                                    }

                                    string output = lineOutput[i];

                                    //If this is empty, replace with <empty>
                                    if (output == null || output.Length <= 0)
                                    {
                                        output = missingString;
                                        debugOutput.Add("Missing: " + columnInput.languageCode + ", " + fileName + ", " + key + " - Replacing with missing string - " + missingString);
                                    }

                                    //Convert to game's internal string system
                                    string compiledString = "\"" + key + "\"" + "," + "\"" + string.Empty + "\"" + "," + "\"" + output + "\"" + "," + "\"" + string.Empty + "\"" + "," + string.Empty + "," + "\"" + string.Empty + "\"" + "," + "\"" + inputDate + "\"";

                                    //Record gender variations
                                    if(genderVariation > 0)
                                    {
                                        genderVariations.Add(fileName + ":" + key + " (" + columnInput.languageCode + ")");

                                        if(!genderVariationFiles.Contains(fileName))
                                        {
                                            genderVariationFiles.Add(fileName);
                                        }
                                    }

                                    nonEnglishContent[columnInput.languageCode][fileName][key] = compiledString;
                                    lineCount++;
                                }
                                else
                                {
                                    Debug.Log("Skipping column " + i + "/" + lineOutput.Count + " as there is no configuration...");
                                    debugOutput.Add("Skipping column " + i + "/" + lineOutput.Count + " as there is no configuration...");
                                    continue;
                                }
                            }
                        }
                    }
                }
                else
                {
                    debugOutput.Add("No contents found on line " + currentLineIndex);
                }

                currentLineIndex++;
            }
        }

        //Write all content to files...
        foreach (KeyValuePair<string, Dictionary<string, Dictionary<string, string>>> languagePair in nonEnglishContent)
        {
            foreach (KeyValuePair<string, Dictionary<string, string>> filePair in languagePair.Value)
            {
                //Get the file path for this...
                string filePath = saveLoadPath + "/Strings/" + languagePair.Key + "/" + filePair.Key + ".csv";

                if (filePair.Key != "names.rooms")
                {
                    if (filePair.Key.Length >= 5 && filePair.Key.Substring(0, 5) == "names")
                    {
                        Debug.Log("Skipping file: " + filePath + " (" + filePair.Value.Count + " keys)...");
                        debugOutput.Add("Skipping file: " + filePath + " (" + filePair.Value.Count + " keys)...");
                        continue;
                    }
                }

                //Start with templated lines
                List<string> lines = new List<string>(templateLines);

                foreach (KeyValuePair<string, string> keyPair in filePair.Value)
                {
                    lines.Add(keyPair.Value);
                }

                //Save to file
                Debug.Log("Writing: " + filePath + " (" + filePair.Value.Count + " keys)...");
                debugOutput.Add("Writing: " + filePath + " (" + filePair.Value.Count + " keys)...");
                System.IO.File.WriteAllLines(filePath, lines);
            }
        }

        Debug.Log("Written " + lineCount + " keys across " + nonEnglishContent.Count + " lanuages (exluding ENG)...");
        debugOutput.Add("Written " + lineCount + " keys across " + nonEnglishContent.Count + " lanuages (exluding ENG)...");

        Debug.Log("Written " + genderVariations.Count + " gender variations of keys across " + genderVariationFiles.Count + " files...");
        debugOutput.Add("Written " + genderVariations.Count + " gender variations of keys across " + genderVariationFiles.Count + " files...");

        //List gender variation files
        foreach(string str in genderVariationFiles)
        {
            debugOutput.Add(str);
        }

        Debug.Log("Localization update complete!");
        debugOutput.Add("Localization update complete!");

        //Write debug output
        string debugPath = saveLoadPath + "/Strings/Localization/Input/OutputDebug.txt";

        StreamWriter writer = new StreamWriter(debugPath, false);

        for (int i = 0; i < debugOutput.Count; i++)
        {
            writer.WriteLine(debugOutput[i]);
        }

        writer.Close();

        string genderVarPath = saveLoadPath + "/Strings/Localization/Input/GenderVariationKeys.txt";

        StreamWriter genderWriter = new StreamWriter(genderVarPath, false);

        for (int i = 0; i < genderVariations.Count; i++)
        {
            genderWriter.WriteLine(genderVariations[i]);
        }

        genderWriter.Close();
    }

    public static Human GetVmailSender(StateSaveData.MessageThreadSave msgThread, int msgIndex, out string nameString)
    {
        nameString = string.Empty;
        Human ret = null;

        if (CityData.Instance.GetHuman(msgThread.senders[msgIndex], out ret, true))
        {
            nameString = ret.GetCitizenName();
            Game.Log("Got vmail sender " + nameString + " from ID " + msgThread.senders[msgIndex]);
        }
        else Game.Log("Failed to get vmail sender from ID " + msgThread.senders[msgIndex]);

        DDSSaveClasses.DDSTreeSave tree = Toolbox.Instance.allDDSTrees[msgThread.treeID];
        DDSSaveClasses.DDSMessageSettings msgSettings = tree.messageRef[msgThread.messages[msgIndex]];
        DDSSaveClasses.DDSParticipant part = tree.participantA;
        if (msgSettings.saidBy == 1) part = tree.participantB;
        else if (msgSettings.saidBy == 2) part = tree.participantC;
        else if (msgSettings.saidBy == 3) part = tree.participantD;

        if((int)part.connection == 36)
        {
            nameString = Strings.ComposeText("|story.partnerfullname|", Player.Instance, LinkSetting.forceNoLinks);
        }
        else if ((int)part.connection >= 22 && (int)part.connection <= 33)
        {
            nameString = Strings.Get("computer", part.connection.ToString());
        }

        if(nameString.Length <= 0)
        {
            nameString = Strings.Get("computer", "Unknown");
        }

        return ret;
    }

    public static Human GetVmailReciever(StateSaveData.MessageThreadSave msgThread, int msgIndex, out string nameString)
    {
        nameString = string.Empty;
        Human ret = null;

        if (CityData.Instance.GetHuman(msgThread.recievers[msgIndex], out ret, true))
        {
            nameString = ret.GetCitizenName();
            Game.Log("Got vmail receiver " + nameString + " from ID " + msgThread.recievers[msgIndex]);
        }
        else Game.Log("Failed to get vmail receiver from ID " + msgThread.senders[msgIndex]);

        DDSSaveClasses.DDSTreeSave tree = Toolbox.Instance.allDDSTrees[msgThread.treeID];
        DDSSaveClasses.DDSMessageSettings msgSettings = tree.messageRef[msgThread.messages[msgIndex]];
        DDSSaveClasses.DDSParticipant part = tree.participantA;
        if (msgSettings.saidTo == 1) part = tree.participantB;
        else if (msgSettings.saidTo == 2) part = tree.participantC;
        else if (msgSettings.saidTo == 3) part = tree.participantD;

        if ((int)part.connection == 36)
        {
            nameString = Strings.ComposeText("|story.partnerfullname|", Player.Instance, LinkSetting.forceNoLinks);
        }
        else if ((int)part.connection >= 22 && (int)part.connection <= 33)
        {
            nameString = Strings.Get("computer", part.connection.ToString());
        }

        if (nameString.Length <= 0)
        {
            nameString = Strings.Get("computer", "Unknown");
        }

        return ret;
    }

    //Parses replacable text
    public static string ComposeText(string input, object baseObject, LinkSetting linkSetting = LinkSetting.automatic, List<Evidence.DataKey> evidenceKeys = null, object additionalObject = null, bool forceKnownCitizenGender = false)
    {
        //Load text files
        if (!textFilesLoaded) Strings.Instance.LoadTextFiles();

        //Parse tags to input content from dictionary
        //Get the whole element
        List<string> parsed = input.Split(new char[] { '|' }, StringSplitOptions.None).ToList(); //Don't remove empties

        //String builder
        StringBuilder sb = new StringBuilder();

        bool escapeChar = false;
        //if (input.Length > 0 && input.Substring(0, 1) == "|") escapeChar = true; //Test for immediate escape character

        //Override base scopes
        if(baseObject != null)
        {
            VMailApp.VmailParsingData thread = baseObject as VMailApp.VmailParsingData;

            if(thread != null)
            {
                if(thread.thread.dsID > -1)
                {
                    if(thread.thread.ds == StateSaveData.CustomDataSource.groupID)
                    {
                        Game.Log("DDS: Override with group ID " + thread.thread.dsID);
                        GroupsController.SocialGroup grp = GroupsController.Instance.groups.Find(item => item.id == thread.thread.dsID);

                        if (grp != null)
                        {
                            Game.Log("DDS: Overwrite group as input object: " + grp.id);
                            baseObject = grp;
                        }
                        else Game.Log("DDS: Unable to find group " + grp.id);
                    }
                }
            }
        }

        //Search to see if any of the parsed entries are dictionary references, if not ignore
        for (int n = 0; n < parsed.Count; n++)
        {
            string item = parsed[n];

            //To save scanning for the proper text items, put a limit on the dictionary references
            if (escapeChar)
            {
                if (item.Length > 0)
                {
                    //Check for capitalization rules...
                    bool capitalize = false;
                    bool allCaps = false;
                    if (char.IsUpper(item[0])) capitalize = true;

                    //Check for all caps
                    if (capitalize)
                    {
                        //If last char is caps then use all caps
                        if (char.IsUpper(item[item.Length - 1])) allCaps = true;
                    }

                    //Detect base scope
                    DDSScope baseScope = null;
                    Human humanBase = baseObject as Human;
                    VMailApp.VmailParsingData vmail = baseObject as VMailApp.VmailParsingData;
                    Interactable itemBase = baseObject as Interactable;
                    MurderController.Murder murderBase = baseObject as MurderController.Murder;
                    NewGameLocation locationBase = baseObject as NewGameLocation;
                    Evidence evidenceBase = baseObject as Evidence;
                    SideJob sideJob = baseObject as SideJob;
                    SyncDiskPreset syncDisk = baseObject as SyncDiskPreset;
                    GroupsController.SocialGroup socGroup = baseObject as GroupsController.SocialGroup;

                    bool knowCitizenGender = false;

                    if (humanBase != null || vmail != null)
                    {
                        baseScope = GameplayControls.Instance.humanScope;
                        Game.Log("DDS: Base scope is 'human'...");
                        knowCitizenGender = true;
                    }
                    else if(itemBase != null)
                    {
                        baseScope = GameplayControls.Instance.itemScope;
                        Game.Log("DDS: Base scope is 'item'...");
                        knowCitizenGender = true;
                    }
                    else if(murderBase != null)
                    {
                        baseScope = GameplayControls.Instance.murderScope;
                        Game.Log("DDS: Base scope is 'murder'...");
                    }
                    else if (locationBase != null)
                    {
                        baseScope = GameplayControls.Instance.locationScope;
                        Game.Log("DDS: Base scope is 'location'...");
                    }
                    else if (sideJob != null)
                    {
                        baseScope = GameplayControls.Instance.sideJobScope;
                        Game.Log("DDS: Base scope is 'side job'...");
                    }
                    else if(syncDisk != null)
                    {
                        baseScope = GameplayControls.Instance.syncDiskScope;
                        Game.Log("DDS: Base scope is 'sync disk'...");
                    }
                    else if (socGroup != null)
                    {
                        baseScope = GameplayControls.Instance.groupScope;
                        Game.Log("DDS: Base scope is 'group'...");
                    }
                    else if (evidenceBase != null)
                    {
                        if(evidenceBase.meta != null)
                        {
                            baseScope = GameplayControls.Instance.itemScope;
                            Game.Log("DDS: Base scope is 'item' (meta)...");
                            knowCitizenGender = true;
                        }
                        else
                        {
                            baseScope = GameplayControls.Instance.evidenceScope;
                            Game.Log("DDS: Base scope is 'evidence'...");
                        }
                    }
                    else
                    {
                        Game.Log("DDS: Base scope is 'null'...");
                    }

                    if (forceKnownCitizenGender) knowCitizenGender = true;

                    //Parse this
                    try
                    {
                        item = ScopeParser(item, baseScope, baseObject, linkSetting, evidenceKeys, additionalObject, knowCitizenGender);
                    }
                    catch
                    {
                        Game.LogError("Unable to parse text");
                    }

                    if (allCaps || (capitalize && item.Length <= 1))
                    {
                        item = item.ToUpper();
                    }
                    else if (capitalize)
                    {
                        item = item.Substring(0, 1).ToUpper() + item.Substring(1, item.Length - 1);
                    }
                }
            }

            //If this is an escape phase, the next entry must not be, and vice versa
            //Is this an escape char?
            escapeChar = !escapeChar;

            //Stringbuilder method
            sb.Append(item);
        }

        //Work around for line breaks
        //sb.Replace("\\n", "\n"); //This shouldn't be needed as it happens when loading into dictionary

        return sb.ToString();
    }

    //Used to parse scopes in DDS
    public static string ScopeParser(string input, DDSScope baseScope, object baseObject, LinkSetting linkSetting = LinkSetting.automatic, List<Evidence.DataKey> evidenceKeys = null, object additionalObject = null, bool knowCitizenGender = false)
    {
        string ret = string.Empty;

        Game.Log("DDS: Scope parser: " + input + " in base object " + baseObject + "...");

        string[] scopes = input.Split('.');
        DDSScope currentScope = baseScope;
        object currentObject = baseObject;
        Evidence baseEvidence = null; //Needed for link context

        //Does this need to return as a link? If automatic, figure it out based on context...
        if(linkSetting == LinkSetting.automatic && baseScope != null)
        {
            if (baseScope.name == "object")
            {
                linkSetting = LinkSetting.forceLinks;
                baseEvidence = GetEvidenceFromBaseScope(baseObject);
            }
            else if(baseScope.name == "citizen")
            {
                Human human = baseObject as Human;
                VMailApp.VmailParsingData thread = null;
                Interactable note = null;

                if (human == null)
                {
                    thread = baseObject as VMailApp.VmailParsingData;

                    if (thread == null)
                    {
                        note = baseObject as Interactable;

                        if (note != null)
                        {
                            linkSetting = LinkSetting.forceLinks;
                            baseEvidence = GetEvidenceFromBaseScope(baseObject);
                        }
                    }
                    else
                    {
                        linkSetting = LinkSetting.forceLinks;
                        baseEvidence = GetEvidenceFromBaseScope(baseObject);
                    }
                }
                else
                {
                    linkSetting = LinkSetting.forceNoLinks;
                }
            }
            else if (baseScope.name == "murder")
            {
                linkSetting = LinkSetting.forceLinks;
            }
            else if (baseScope.name == "evidence")
            {
                linkSetting = LinkSetting.forceLinks;
                baseEvidence = GetEvidenceFromBaseScope(baseObject);
            }
            else if (baseScope.name == "location")
            {
                linkSetting = LinkSetting.forceLinks;
            }
            else if (baseScope.name == "company")
            {
                linkSetting = LinkSetting.forceLinks;
            }
            else linkSetting = LinkSetting.forceNoLinks;
        }

        for (int i = 0; i < scopes.Length; i++)
        {
            string scope = scopes[i];

            //The last entry will reference a value
            if(i == scopes.Length - 1)
            {
                string currentScopeName = string.Empty;
                if (currentScope != null) currentScopeName = currentScope.name;

                ret = GetContainedValue(baseObject, currentScopeName, scope, currentObject, baseEvidence, linkSetting, evidenceKeys, additionalObject, knowCitizenGender);
            }
            //The first entries will switch the scope
            else
            {
                currentScope = GetContainedScope(baseScope, currentScope, scope, currentObject, out currentObject, additionalObject);
            }
        }

        Game.Log("DDS: Scope parser return: " + ret);

        return ret;
    }

    public static DDSScope GetContainedScope(DDSScope baseScope, DDSScope currentScope, string newScope, object inputObject, out object outputObject, object additionalObject)
    {
        outputObject = inputObject;
        DDSScope output = null;
        newScope = newScope.ToLower();

        //Scan contained scopes...
        if(currentScope != null)
        {
            DDSScope.ContainedScope contained = currentScope.containedScopes.Find(item => item.name == newScope);

            if(contained != null)
            {
                output = contained.type;

                //Search for the correct output object
                outputObject = GetScopeObject(baseScope, outputObject, currentScope.name, contained.name, additionalObject: additionalObject);
            }
            else if(newScope == currentScope.name)
            {
                Game.Log("DDS: Required scope " + newScope + " is same as current scope " + currentScope.name);
                output = currentScope;
                outputObject = inputObject;
            }
        }

        //Scan global scopes...
        if(output == null)
        {
            if(Toolbox.Instance.globalScopeDictionary.TryGetValue(newScope, out output))
            {
                //Search for the correct output object
                outputObject = GetScopeObject(baseScope, outputObject, output.name, newScope, additionalObject: additionalObject);
            }
        }

        if(output == null)
        {
            if(currentScope != null)
            {
                Game.LogError("Unable to get contained scope " + newScope + " within scope " + currentScope.name);
            }
            else
            {
                Game.LogError("Unable to get contained scope " + newScope + " without a preceeding scope!");
            }
        }

        if(output != null)
        {
            Game.Log("DDS: Switch scope > " + output.name + ", object: " + outputObject);
        }
        else
        {
            Game.Log("DDS: Switch scope > null, object: " + outputObject);
        }

        return output;
    }

    public static object GetScopeObject(DDSScope baseScope, object inputObject, string withinScope, string newType, List<Evidence.DataKey> evidenceKeys = null, object additionalObject = null)
    {
        object ret = null;
        withinScope = withinScope.ToLower();
        newType = newType.ToLower();

        //Failsafe; class can contain itself, return itself...
        if(newType == withinScope)
        {
            return inputObject;
        }

        try
        {
            //Search for the correct output object
            if (withinScope == "citizen")
            {
                //This input could be a human, vmail or note (object)

                Human human = inputObject as Human;
                VMailApp.VmailParsingData thread = null;
                Interactable note = null;

                if (human == null)
                {
                    thread = inputObject as VMailApp.VmailParsingData;

                    if (thread == null)
                    {
                        note = inputObject as Interactable;

                        if(note == null)
                        {
                            Game.LogError("Value error: Unable to convert input object to Human: " + inputObject);
                            return ret;
                        }
                        else
                        {
                            human = note.belongsTo; //TODO: This will need to be changed to specific receiver
                        }
                    }
                    else
                    {
                        human = Strings.GetVmailSender(thread.thread, thread.messageIndex, out _);
                    }
                }

                if (newType == "job")
                {
                    ret = human.job;
                }
                else if (newType == "home")
                {
                    ret = human.home;
                }
                else if (newType == "receiver")
                {
                    if(thread != null)
                    {
                        ret = GetVmailReciever(thread.thread, thread.messageIndex, out _);
                    }
                    else if(note != null)
                    {
                        ret = note.reciever;
                    }
                    else if(additionalObject != null && (additionalObject as Human) != null)
                    {
                        ret = additionalObject;
                    }
                    else
                    {
                        if(human.isPlayer && InteractionController.Instance.talkingTo != null && InteractionController.Instance.talkingTo.isActor != null)
                        {
                            ret = InteractionController.Instance.talkingTo.isActor;
                        }
                        else if(!human.isPlayer && InteractionController.Instance.talkingTo != null && InteractionController.Instance.talkingTo.isActor == human)
                        {
                            ret = Player.Instance;
                        }
                        else if (human.inConversation && human.currentConversation != null)
                        {
                             ret = human.currentConversation.currentlyTalkingTo;
                        }
                    }
                }
                else if (newType == "location")
                {
                    ret = human.currentGameLocation;
                }
                else if (newType == "sightinglocation")
                {
                    Human sightingOf = null;

                    if (thread != null)
                    {
                        sightingOf = GetVmailReciever(thread.thread, thread.messageIndex, out _);
                    }
                    else if (note != null)
                    {
                        sightingOf = note.reciever;
                    }
                    else if (additionalObject != null && (additionalObject as Human) != null)
                    {
                        sightingOf = additionalObject as Human;
                    }
                    else
                    {
                        if (human.inConversation && human.currentConversation != null)
                        {
                            sightingOf = human.currentConversation.currentlyTalkingTo;
                        }
                    }

                    if(sightingOf != null)
                    {
                        if(human.lastSightings.ContainsKey(sightingOf))
                        {
                            NewNode foundNode = null;

                            if(PathFinder.Instance.nodeMap.TryGetValue(human.lastSightings[sightingOf].node, out foundNode))
                            {
                                ret = foundNode.gameLocation;
                            }
                        }
                    }
                }
                else if (newType == "sightingtime")
                {
                    Human sightingOf = null;

                    if (thread != null)
                    {
                        sightingOf = GetVmailReciever(thread.thread, thread.messageIndex, out _);
                    }
                    else if (note != null)
                    {
                        sightingOf = note.reciever;
                    }
                    else if (additionalObject != null && (additionalObject as Human) != null)
                    {
                        sightingOf = additionalObject as Human;
                    }
                    else
                    {
                        if (human.inConversation && human.currentConversation != null)
                        {
                            sightingOf = human.currentConversation.currentlyTalkingTo;
                        }
                    }

                    if (sightingOf != null)
                    {
                        if (human.lastSightings != null && human.lastSightings.ContainsKey(sightingOf))
                        {
                            ret = human.lastSightings[sightingOf].time;
                        }
                    }
                }
                else if (newType == "sightingclosest")
                {
                    Human sightingOf = null;

                    if (thread != null)
                    {
                        sightingOf = GetVmailReciever(thread.thread, thread.messageIndex, out _);
                    }
                    else if (note != null)
                    {
                        sightingOf = note.reciever;
                    }
                    else if (additionalObject != null && (additionalObject as Human) != null)
                    {
                        sightingOf = additionalObject as Human;
                    }
                    else
                    {
                        if (human.inConversation && human.currentConversation != null)
                        {
                            sightingOf = human.currentConversation.currentlyTalkingTo;
                        }
                    }

                    if (sightingOf != null)
                    {
                        if (human.lastSightings.ContainsKey(sightingOf))
                        {
                            NewNode pos = null;

                            if (PathFinder.Instance.nodeMap.TryGetValue(human.lastSightings[sightingOf].node, out pos))
                            {
                                ret = null;
                                float bestDist = Mathf.Infinity;

                                //Scan all entraces to other locations...
                                foreach (NewNode.NodeAccess acc in pos.gameLocation.entrances)
                                {
                                    if (!acc.walkingAccess) continue;

                                    if (acc.toNode.gameLocation != pos.gameLocation)
                                    {
                                        if (acc.toNode.gameLocation.thisAsStreet != null || acc.toNode.gameLocation.thisAsAddress.company != null || acc.toNode.gameLocation.thisAsAddress.residence != null)
                                        {
                                            float dist = Vector3.Distance(acc.worldAccessPoint, pos.position);

                                            if (dist < bestDist)
                                            {
                                                ret = acc.toNode.gameLocation;
                                                bestDist = dist;
                                            }
                                        }
                                    }
                                    else if (acc.fromNode.gameLocation != pos.gameLocation)
                                    {
                                        if (acc.fromNode.gameLocation.thisAsStreet != null || acc.fromNode.gameLocation.thisAsAddress.company != null || acc.fromNode.gameLocation.thisAsAddress.residence != null)
                                        {
                                            float dist = Vector3.Distance(acc.worldAccessPoint, pos.position);

                                            if (dist < bestDist)
                                            {
                                                ret = acc.fromNode.gameLocation;
                                                bestDist = dist;
                                            }
                                        }
                                    }
                                }

                                //Failsafe, scan promimity to all companies...
                                if(ret == null)
                                {
                                    foreach(Company c in CityData.Instance.companyDirectory)
                                    {
                                        if(c.placeOfBusiness != null)
                                        {
                                            foreach(NewNode.NodeAccess acc in c.placeOfBusiness.entrances)
                                            {
                                                if (!acc.walkingAccess) continue;

                                                if (acc.toNode.nodeCoord.z == pos.nodeCoord.z)
                                                {
                                                    float dist = Vector3.Distance(acc.worldAccessPoint, pos.position);

                                                    if (dist < bestDist)
                                                    {
                                                        ret = acc.toNode.gameLocation;
                                                        bestDist = dist;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (newType == "a")
                {
                    if (human.inConversation && human.currentConversation != null)
                    {
                        ret = human.currentConversation.participantA;
                    }
                }
                else if (newType == "b")
                {
                    if (human.inConversation && human.currentConversation != null)
                    {
                        ret = human.currentConversation.participantB;
                    }
                }
                else if (newType == "c")
                {
                    if (human.inConversation && human.currentConversation != null)
                    {
                        ret = human.currentConversation.participantC;
                    }
                }
                else if (newType == "d")
                {
                    if (human.inConversation && human.currentConversation != null)
                    {
                        ret = human.currentConversation.participantD;
                    }
                }
                else if (newType == "partner")
                {
                    ret = human.partner;
                }
                else if (newType == "paramour")
                {
                    if (human.paramour != null) ret = human.paramour;
                    else ret = human.partner;
                }
                else if (newType == "doctor")
                {
                    ret = human.GetDoctor();
                }
                else if (newType == "friend")
                {
                    float likeMost = -99999f;

                    foreach (Acquaintance aq in human.acquaintances)
                    {
                        if (aq.known >= 0.33f)
                        {
                            float val = aq.known + aq.like;

                            if (val > likeMost)
                            {
                                ret = aq.with;
                                likeMost = val;
                            }
                        }
                    }
                }
                else if (newType == "enemy")
                {
                    float likeLeast = 99999f;

                    foreach (Acquaintance aq in human.acquaintances)
                    {
                        if (aq.known >= 0.2f)
                        {
                            float val = aq.known + aq.like;

                            if (val < likeLeast)
                            {
                                ret = aq.with;
                                likeLeast = val;
                            }
                        }
                    }
                }
                else if (newType == "neighbour")
                {
                    float likeMost = -99999f;

                    foreach (Acquaintance aq in human.acquaintances)
                    {
                        if (aq.connections.Contains(Acquaintance.ConnectionType.neighbor))
                        {
                            float val = aq.known + aq.like;

                            if (val > likeMost)
                            {
                                ret = aq.with;
                                likeMost = val;
                            }
                        }
                    }
                }
                else if (newType == "landlord")
                {
                    ret = human.GetLandlord();
                }
                else if (newType == "faveatery")
                {
                    NewAddress ad = null;

                    if (human.favouritePlaces.TryGetValue(CompanyPreset.CompanyCategory.meal, out ad))
                    {
                        ret = ad;
                    }
                }
                else if (newType == "favrecreation")
                {
                    NewAddress ad = null;

                    if (human.favouritePlaces.TryGetValue(CompanyPreset.CompanyCategory.recreational, out ad))
                    {
                        ret = ad;
                    }
                }
                else if (newType == "group1")
                {
                    if (human.groups.Count > 0)
                    {
                        List<GroupsController.SocialGroup> grps = human.groups.FindAll(item => Toolbox.Instance.groupsDictionary.ContainsKey(item.preset) && Toolbox.Instance.groupsDictionary[item.preset].groupType == GroupPreset.GroupType.interestGroup);

                        if (grps.Count > 0)
                        {
                            //Sort so we get a predictable outcome
                            grps.Sort((p1, p2) => p1.id.CompareTo(p2.id));

                            ret = grps[0];
                        }
                    }
                }
                else if (newType == "group2")
                {
                    if (human.groups.Count > 1)
                    {
                        List<GroupsController.SocialGroup> grps = human.groups.FindAll(item => Toolbox.Instance.groupsDictionary.ContainsKey(item.preset) && Toolbox.Instance.groupsDictionary[item.preset].groupType == GroupPreset.GroupType.interestGroup);

                        if (grps.Count > 1)
                        {
                            //Sort so we get a predictable outcome
                            grps.Sort((p1, p2) => p1.id.CompareTo(p2.id));

                            ret = grps[1];
                        }
                    }
                }
                else if (newType == "murder")
                {
                    if (human.death != null && human.death.isDead)
                    {
                        ret = human.death.GetMurder();
                    }
                }
                else if(newType == "sidejob")
                {
                    if(additionalObject != null)
                    {
                        SideJob sj = additionalObject as SideJob;

                        if(sj != null)
                        {
                            ret = sj;
                        }
                    }
                }
            }
            else if (withinScope == "city")
            {
                if (newType == "time")
                {
                    ret = SessionData.Instance.gameTime;
                }
                else if (newType == "player")
                {
                    ret = Player.Instance;
                }
            }
            else if (withinScope == "company")
            {
                Company company = inputObject as Company;

                if (company == null)
                {
                    Game.LogError("Value error: Unable to convert input object to Company: " + inputObject);
                    return ret;
                }

                if (newType == "location")
                {
                    ret = company.address;
                }
                else if (newType == "owner")
                {
                    ret = company.director;
                }
                else if (newType == "receptionist")
                {
                    ret = company.receptionist;
                }
                else if (newType == "security")
                {
                    ret = company.security;
                }
                else if (newType == "janitor")
                {
                    ret = company.janitor;
                }
                else if (newType == "employee")
                {
                    //Use a seed to pick a predictable employee
                    List<Occupation> employees = company.companyRoster.FindAll(item => item.employee != null && item.employee != company.director);

                    if(employees.Count > 0)
                    {
                        string seed = company.companyID + company.name;
                        ret = employees[Toolbox.Instance.GetPsuedoRandomNumber(0, employees.Count, seed)].employee;
                    }
                }
            }
            else if (withinScope == "controls")
            {

            }
            else if (withinScope == "group")
            {
                GroupsController.SocialGroup group = inputObject as GroupsController.SocialGroup;

                if (group == null)
                {
                    Game.LogError("Value error: Unable to convert input object to Group: " + inputObject);
                    return ret;
                }

                if (newType == "leader")
                {
                    if (group.members.Count > 0)
                    {
                        Human h = null;

                        if (CityData.Instance.GetHuman(group.members[0], out h))
                        {
                            ret = h;
                        }
                    }
                }
                else if (newType == "member2")
                {
                    if (group.members.Count > 1)
                    {
                        Human h = null;

                        if (CityData.Instance.GetHuman(group.members[1], out h))
                        {
                            ret = h;
                        }
                    }
                }
                else if (newType == "member3")
                {
                    if (group.members.Count > 2)
                    {
                        Human h = null;

                        if (CityData.Instance.GetHuman(group.members[2], out h))
                        {
                            ret = h;
                        }
                    }
                }
                else if (newType == "member4")
                {
                    if (group.members.Count > 3)
                    {
                        Human h = null;

                        if (CityData.Instance.GetHuman(group.members[3], out h))
                        {
                            ret = h;
                        }
                    }
                }
                else if (newType == "member5")
                {
                    if (group.members.Count > 4)
                    {
                        Human h = null;

                        if (CityData.Instance.GetHuman(group.members[4], out h))
                        {
                            ret = h;
                        }
                    }
                }
                else if (newType == "member6")
                {
                    if (group.members.Count > 5)
                    {
                        Human h = null;

                        if (CityData.Instance.GetHuman(group.members[5], out h))
                        {
                            ret = h;
                        }
                    }
                }
                else if (newType == "member7")
                {
                    if (group.members.Count > 6)
                    {
                        Human h = null;

                        if (CityData.Instance.GetHuman(group.members[6], out h))
                        {
                            ret = h;
                        }
                    }
                }
                else if (newType == "member8")
                {
                    if (group.members.Count > 6)
                    {
                        Human h = null;

                        if (CityData.Instance.GetHuman(group.members[7], out h))
                        {
                            ret = h;
                        }
                    }
                }
                else if (newType == "meetingplace")
                {
                    ret = group.GetMeetingPlace();
                }
                else if (newType == "nextmeetingtime")
                {
                    ret = group.GetNextMeetingTime();
                }
            }
            else if (withinScope == "job")
            {
                Occupation job = inputObject as Occupation;

                if (job == null)
                {
                    Game.LogError("Value error: Unable to convert input object to Job: " + inputObject);
                    return ret;
                }

                if (newType == "employee")
                {
                    ret = job.employee;
                }
                else if (newType == "employer")
                {
                    ret = job.employer;
                }
            }
            else if (withinScope == "killer")
            {
                if (newType == "lastmurder")
                {
                    if(MurderController.Instance.activeMurders.Count > 0)
                    {
                        for (int i = 0; i < MurderController.Instance.activeMurders.Count; i++)
                        {
                            if(MurderController.Instance.activeMurders[i].death != null && MurderController.Instance.activeMurders[i].death.reported)
                            {
                                if(ret == null || MurderController.Instance.activeMurders[i].death.time > (ret as MurderController.Murder).time)
                                {
                                    ret = MurderController.Instance.activeMurders[i];
                                }
                            }
                        }
                    }
                    
                    if(ret == null && MurderController.Instance.inactiveMurders.Count > 0)
                    {
                        for (int i = 0; i < MurderController.Instance.inactiveMurders.Count; i++)
                        {
                            if (MurderController.Instance.inactiveMurders[i].death != null && MurderController.Instance.inactiveMurders[i].death.reported)
                            {
                                if (ret == null || MurderController.Instance.inactiveMurders[i].death.time > (ret as MurderController.Murder).time)
                                {
                                    ret = MurderController.Instance.inactiveMurders[i];
                                }
                            }
                        }
                    }
                }
                if (newType == "nextmurder")
                {
                    if (MurderController.Instance.activeMurders.Count > 0)
                    {
                        for (int i = 0; i < MurderController.Instance.activeMurders.Count; i++)
                        {
                            if (MurderController.Instance.activeMurders[i].death == null)
                            {
                                ret = MurderController.Instance.activeMurders[i];
                                break;
                            }
                        }
                    }
                }
                else if (newType == "location")
                {
                    if(MurderController.Instance.currentMurderer != null)
                    {
                        ret = MurderController.Instance.currentMurderer.currentGameLocation;
                    }
                }
            }
            else if (withinScope == "location")
            {
                NewGameLocation location = inputObject as NewGameLocation;
                NewAddress address = location.thisAsAddress;
                StreetController street = location.thisAsStreet;

                if (location == null && address == null && street == null)
                {
                    Game.LogError("Value error: Unable to convert input object to Location: " + inputObject);
                    return ret;
                }

                if (newType == "company")
                {
                    if (address != null) ret = address.company;
                }
                else if (newType == "owner")
                {
                    if(address != null)
                    {
                        if(address.company != null)
                        {
                            ret = address.company.director;
                        }

                        if(ret == null && address.owners.Count > 0)
                        {
                            ret = address.owners[0];
                        }

                        if (ret == null && address.inhabitants.Count > 0)
                        {
                            ret = address.inhabitants[0];
                        }
                    }
                }
                else if (newType == "landlord")
                {
                    if(address != null && address.residence != null && address.inhabitants.Count > 0)
                    {
                        ret = address.inhabitants[0].GetLandlord();
                    }
                }
                else if (newType == "group1")
                {
                    List<GroupsController.SocialGroup> meetHere = new List<GroupsController.SocialGroup>();

                    foreach(GroupsController.SocialGroup soc in GroupsController.Instance.groups)
                    {
                        if (Toolbox.Instance.groupsDictionary.ContainsKey(soc.preset) && Toolbox.Instance.groupsDictionary[soc.preset].groupType != GroupPreset.GroupType.interestGroup) continue;

                        if(soc.GetMeetingPlace() == location)
                        {
                            meetHere.Add(soc);
                        }
                    }

                    if(meetHere.Count > 0)
                    {
                        //Order so we get a predicatble result
                        meetHere.Sort((p1, p2) => p1.id.CompareTo(p2.id));

                        ret = meetHere[0];
                    }
                }
                else if (newType == "group2")
                {
                    List<GroupsController.SocialGroup> meetHere = new List<GroupsController.SocialGroup>();

                    foreach (GroupsController.SocialGroup soc in GroupsController.Instance.groups)
                    {
                        if (Toolbox.Instance.groupsDictionary.ContainsKey(soc.preset) && Toolbox.Instance.groupsDictionary[soc.preset].groupType != GroupPreset.GroupType.interestGroup) continue;

                        if (soc.GetMeetingPlace() == location)
                        {
                            meetHere.Add(soc);
                        }
                    }

                    if (meetHere.Count > 1)
                    {
                        //Order so we get a predicatble result
                        meetHere.Sort((p1, p2) => p1.id.CompareTo(p2.id));

                        ret = meetHere[1];
                    }
                }
                else if (newType == "group3")
                {
                    List<GroupsController.SocialGroup> meetHere = new List<GroupsController.SocialGroup>();

                    foreach (GroupsController.SocialGroup soc in GroupsController.Instance.groups)
                    {
                        if (Toolbox.Instance.groupsDictionary.ContainsKey(soc.preset) && Toolbox.Instance.groupsDictionary[soc.preset].groupType != GroupPreset.GroupType.interestGroup) continue;

                        if (soc.GetMeetingPlace() == location)
                        {
                            meetHere.Add(soc);
                        }
                    }

                    if (meetHere.Count > 2)
                    {
                        //Order so we get a predicatble result
                        meetHere.Sort((p1, p2) => p1.id.CompareTo(p2.id));

                        ret = meetHere[2];
                    }
                }
                else if (newType == "group4")
                {
                    List<GroupsController.SocialGroup> meetHere = new List<GroupsController.SocialGroup>();

                    foreach (GroupsController.SocialGroup soc in GroupsController.Instance.groups)
                    {
                        if (Toolbox.Instance.groupsDictionary.ContainsKey(soc.preset) && Toolbox.Instance.groupsDictionary[soc.preset].groupType != GroupPreset.GroupType.interestGroup) continue;

                        if (soc.GetMeetingPlace() == location)
                        {
                            meetHere.Add(soc);
                        }
                    }

                    if (meetHere.Count > 3)
                    {
                        //Order so we get a predicatble result
                        meetHere.Sort((p1, p2) => p1.id.CompareTo(p2.id));

                        ret = meetHere[3];
                    }
                }
            }
            else if (withinScope == "murder")
            {
                MurderController.Murder murder = inputObject as MurderController.Murder;

                if (murder == null)
                {
                    Game.LogError("Value error: Unable to convert input object to Murder: " + inputObject);
                    return ret;
                }

                Human.Death death = murder.death;

                if (newType == "victim")
                {
                    ret = murder.victim;
                }
                else if (newType == "killer")
                {
                    ret = murder.murderer;
                }
                else if (newType == "weapon")
                {
                    if (murder.weapon != null) ret = murder.weapon;
                }
                else if (newType == "location")
                {
                    if (death != null) ret = death.GetDeathLocation();
                }
                else if (newType == "time")
                {
                    if (death != null) ret = death.time;
                }
                else if (newType == "discovercitizen")
                {
                    if (death != null) ret = death.GetDiscoverer();
                }
                else if (newType == "discovertime")
                {
                    if (death != null) ret = death.discoveredAt;
                }
                else if (newType == "timefrom")
                {
                    if (death != null) ret = death.timeOfDeathRange.x;
                }
                else if (newType == "timeto")
                {
                    if (death != null) ret = death.timeOfDeathRange.y;
                }
                else if (newType == "callingcard")
                {
                    if (murder.callingCard != null) ret = murder.callingCard;
                }
            }
            else if (withinScope == "object")
            {
                Interactable inter = inputObject as Interactable;

                if(inter != null)
                {
                    if (newType == "owner")
                    {
                        ret = inter.belongsTo;
                    }
                    else if (newType == "writer")
                    {
                        ret = inter.writer;
                    }
                    else if (newType == "receiver")
                    {
                        ret = inter.reciever;
                    }
                    else if (newType == "other")
                    {
                        ret = inter.reciever;
                    }
                    else if (newType == "location")
                    {
                        if(inter.node != null) ret = inter.node.gameLocation;
                    }
                    else if (newType == "purchasedfrom")
                    {
                        //This requires receipt evidence
                        EvidenceReceipt receipt = inter.evidence as EvidenceReceipt;

                        if (receipt != null)
                        {
                            ret = receipt.soldHere;
                        }
                        else
                        {
                            Game.LogError("Scope error: Trying to get purchased place from non-receipt evidence");
                        }
                    }
                    else if(newType == "purchasedtime")
                    {
                        //This requires receipt evidence
                        EvidenceReceipt receipt = inter.evidence as EvidenceReceipt;

                        if (receipt != null)
                        {
                            ret = receipt.purchasedTime;
                        }
                        else
                        {
                            Game.LogError("Scope error: Trying to get purchased time from non-receipt evidence");
                        }
                    }
                    else if (newType == "sidejob")
                    {
                        ret = inter.jobParent;

                        if (ret == null && additionalObject != null)
                        {
                            SideJob sj = additionalObject as SideJob;

                            if (sj != null)
                            {
                                ret = sj;
                            }
                        }
                    }
                    else if (newType == "lastcall")
                    {
                        if(inter.t != null && inter.t.location != null && inter.t.location.building != null)
                        {
                            TelephoneController.PhoneCall lastCall = null;
                            float lastTime = -999999f;

                            foreach(TelephoneController.PhoneCall pc in inter.t.location.building.callLog)
                            {
                                if(pc.to == inter.t.number)
                                {
                                    if(pc.time > lastTime)
                                    {
                                        lastCall = pc;
                                        lastTime = pc.time;
                                    }
                                }
                            }

                            if(lastCall != null)
                            {
                                ret = lastCall.fromNS.interactable;
                            }
                        }
                    }
                    else if (newType == "lastcalltime")
                    {
                        if (inter.t != null && inter.t.location != null && inter.t.location.building != null)
                        {
                            TelephoneController.PhoneCall lastCall = null;
                            float lastTime = -999999f;

                            foreach (TelephoneController.PhoneCall pc in inter.t.location.building.callLog)
                            {
                                if (pc.to == inter.t.number)
                                {
                                    if (pc.time > lastTime)
                                    {
                                        lastCall = pc;
                                        lastTime = pc.time;
                                    }
                                }
                            }

                            if (lastCall != null)
                            {
                                ret = lastTime;
                            }
                        }
                    }
                    else if(newType == "group")
                    {
                        ret = inter.group;
                    }
                    else if (newType == "forsale")
                    {
                        ret = inter.forSale;
                    }
                }
                else
                {
                    Evidence ev = inputObject as Evidence;

                    if(ev.meta != null)
                    {
                        if (newType == "owner")
                        {
                            if (ev.meta.owner > -1)
                            {
                                Human h = null;

                                if(CityData.Instance.GetHuman(ev.meta.owner, out h))
                                {
                                    ret = h;
                                }
                            }
                        }
                        else if (newType == "writer")
                        {
                            if (ev.meta.writer > -1)
                            {
                                Human h = null;

                                if (CityData.Instance.GetHuman(ev.meta.writer, out h))
                                {
                                    ret = h;
                                }
                            }
                        }
                        else if (newType == "receiver")
                        {
                            if (ev.meta.reciever > -1)
                            {
                                Human h = null;

                                if (CityData.Instance.GetHuman(ev.meta.reciever, out h))
                                {
                                    ret = h;
                                }
                            }
                        }
                        else if (newType == "other")
                        {
                            if (ev.meta.reciever > -1)
                            {
                                Human h = null;

                                if (CityData.Instance.GetHuman(ev.meta.reciever, out h))
                                {
                                    ret = h;
                                }
                            }
                        }
                        else if (newType == "location")
                        {
                            NewNode foundNode = null;

                            if (PathFinder.Instance.nodeMap.TryGetValue(ev.meta.n, out foundNode))
                            {
                                ret = foundNode.gameLocation;
                            }
                        }
                        else if (newType == "purchasedfrom")
                        {
                            //This requires receipt evidence
                            EvidenceReceipt receipt = ev as EvidenceReceipt;

                            if (receipt != null)
                            {
                                ret = receipt.soldHere;
                            }
                            else
                            {
                                Game.LogError("Scope error: Trying to get purchased place from non-receipt evidence");
                            }
                        }
                        else if (newType == "purchasedtime")
                        {
                            //This requires receipt evidence
                            EvidenceReceipt receipt = ev as EvidenceReceipt;

                            if (receipt != null)
                            {
                                ret = receipt.purchasedTime;
                            }
                            else
                            {
                                Game.LogError("Scope error: Trying to get purchased time from non-receipt evidence");
                            }
                        }
                    }
                    else
                    {
                        Game.LogError("Value error: Unable to convert input object to Object/MetaObject: " + inputObject);
                        return ret;
                    }

                }

            }
            else if (withinScope == "random")
            {
                string seed = CityData.Instance.seed;
                if (baseScope != null) seed += baseScope.name;

                if (inputObject != null)
                {
                    Human hu = inputObject as Human;

                    if(hu != null)
                    {
                        seed = hu.humanID.ToString();
                    }
                    else
                    {
                        Interactable i = inputObject as Interactable;

                        if(i != null)
                        {
                            seed = i.id.ToString();
                        }
                    }
                }

                if (newType == "citizen")
                {
                    ret = CityData.Instance.citizenDirectory[Toolbox.Instance.GetPsuedoRandomNumber(0, CityData.Instance.citizenDirectory.Count, seed)];
                }
                else if (newType == "address")
                {
                    ret = CityData.Instance.addressDirectory[Toolbox.Instance.GetPsuedoRandomNumber(0, CityData.Instance.addressDirectory.Count, seed)];
                }
                else if (newType == "street")
                {
                    ret = CityData.Instance.streetDirectory[Toolbox.Instance.GetPsuedoRandomNumber(0, CityData.Instance.streetDirectory.Count, seed)];
                }
                else if (newType == "residence")
                {
                    ret = CityData.Instance.residenceDirectory[Toolbox.Instance.GetPsuedoRandomNumber(0, CityData.Instance.residenceDirectory.Count, seed)];
                }
                else if (newType == "park")
                {
                    List<NewAddress> parks = CityData.Instance.addressDirectory.FindAll(item => item.addressPreset.name == "Park");
                    if(parks.Count > 0) ret = parks[Toolbox.Instance.GetPsuedoRandomNumber(0, parks.Count, seed)];
                }
                else if (newType == "company")
                {
                    ret = CityData.Instance.companyDirectory[Toolbox.Instance.GetPsuedoRandomNumber(0, CityData.Instance.companyDirectory.Count, seed)];
                }
                else if (newType == "eatery")
                {
                    List<Company> eatery = CityData.Instance.companyDirectory.FindAll(item => item.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.meal));
                    if (eatery.Count > 0) ret = eatery[Toolbox.Instance.GetPsuedoRandomNumber(0, eatery.Count, seed)];
                }
                else if (newType == "shop")
                {
                    List<Company> retail = CityData.Instance.companyDirectory.FindAll(item => item.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.retail));
                    if (retail.Count > 0) ret = retail[Toolbox.Instance.GetPsuedoRandomNumber(0, retail.Count, seed)];
                }
                //else if (newType == "industrial")
                //{
                //    List<Company> industrial = CityData.Instance.companyDirectory.FindAll(item => item.preset.companyCategories.Contains(CompanyPreset.CompanyCategory.industrial));
                //    if (industrial.Count > 0) ret = industrial[Toolbox.Instance.GetPsuedoRandomNumber(0, industrial.Count, seed)];
                //}
                else if (newType == "diner")
                {
                    List<Company> diner = CityData.Instance.companyDirectory.FindAll(item => item.preset.name == "AmericanDiner");
                    if (diner.Count > 0) ret = diner[Toolbox.Instance.GetPsuedoRandomNumber(0, diner.Count, seed)];
                }
                else if (newType == "bar")
                {
                    List<Company> bar = CityData.Instance.companyDirectory.FindAll(item => item.preset.name == "Bar");
                    if (bar.Count > 0) ret = bar[Toolbox.Instance.GetPsuedoRandomNumber(0, bar.Count, seed)];
                }
                else if (newType == "launderette")
                {
                    List<Company> launderette = CityData.Instance.companyDirectory.FindAll(item => item.preset.name == "Launderette");
                    if (launderette.Count > 0) ret = launderette[Toolbox.Instance.GetPsuedoRandomNumber(0, launderette.Count, seed)];
                }
                else if (newType == "syncclinic")
                {
                    List<Company> sync = CityData.Instance.companyDirectory.FindAll(item => item.preset.name == "SyncClinic");
                    if (sync.Count > 0) ret = sync[Toolbox.Instance.GetPsuedoRandomNumber(0, sync.Count, seed)];
                }
                else if (newType == "police")
                {
                    List<Company> police = CityData.Instance.companyDirectory.FindAll(item => item.preset.name == "EnforcerBranch");
                    if (police.Count > 0) ret = police[Toolbox.Instance.GetPsuedoRandomNumber(0, police.Count, seed)];
                }
                else if (newType == "hospital")
                {
                    List<Company> hospital = CityData.Instance.companyDirectory.FindAll(item => item.preset.name == "HospitalWing");
                    if (hospital.Count > 0) ret = hospital[Toolbox.Instance.GetPsuedoRandomNumber(0, hospital.Count, seed)];
                }
                else if (newType == "weaponsdealer")
                {
                    List<Company> weaponsDealer = CityData.Instance.companyDirectory.FindAll(item => item.preset.name == "WeaponsDealer");
                    if (weaponsDealer.Count > 0) ret = weaponsDealer[Toolbox.Instance.GetPsuedoRandomNumber(0, weaponsDealer.Count, seed)];
                }
                else if (newType == "blackmarkettrader")
                {
                    List<Company> bmTrader = CityData.Instance.companyDirectory.FindAll(item => item.preset.name == "BlackmarketTrader");
                    if (bmTrader.Count > 0) ret = bmTrader[Toolbox.Instance.GetPsuedoRandomNumber(0, bmTrader.Count, seed)];
                }
                else if (newType == "loanshark")
                {
                    List<Company> loanShark = CityData.Instance.companyDirectory.FindAll(item => item.preset.name == "LoanShark");
                    if (loanShark.Count > 0) ret = loanShark[Toolbox.Instance.GetPsuedoRandomNumber(0, loanShark.Count, seed)];
                }
                else if (newType == "blackmarketsyncclinic")
                {
                    List<Company> bmSyncClinic = CityData.Instance.companyDirectory.FindAll(item => item.preset.name == "BlackmarketSyncClinic");
                    if (bmSyncClinic.Count > 0) ret = bmSyncClinic[Toolbox.Instance.GetPsuedoRandomNumber(0, bmSyncClinic.Count, seed)];
                }
                else if (newType == "gamblingden")
                {
                    List<Company> gamblingDen = CityData.Instance.companyDirectory.FindAll(item => item.preset.name == "GamblingDen");
                    if (gamblingDen.Count > 0) ret = gamblingDen[Toolbox.Instance.GetPsuedoRandomNumber(0, gamblingDen.Count, seed)];
                }
            }
            else if (withinScope == "sidejob")
            {
                SideJob job = inputObject as SideJob;

                if (job == null)
                {
                    Game.LogError("Value error: Unable to convert input object to Side Job: " + inputObject);
                    return ret;
                }

                if(newType == "sidejob")
                {
                    ret = job;
                }
                else if (newType == "poster")
                {
                    ret = job.poster;
                }
                else if (newType == "purp")
                {
                    ret = job.purp;
                }
                else if (newType == "posttime")
                {
                
                }
                else if (newType == "post")
                {
                    ret = job.post;
                }
                else if(newType == "callphone")
                {
                    if(job.chosenGooseChasePhone != null)
                    {
                        ret = job.chosenGooseChasePhone;
                    }
                    else
                    {
                        Game.LogError("Job: Unable to get goose chace phone interactable " + job.gooseChasePhone + " from job " + job.jobID);
                    }
                }
                else if (newType == "meet")
                {
                    if (job.chosenMeetingPoint != null)
                    {
                        ret = job.chosenMeetingPoint;
                    }
                    else
                    {
                        Game.LogError("Job: Unable to get meeting point interactable " + job.meetingPoint + " from job " + job.jobID);
                    }
                }
                else if (newType == "calltime")
                {
                    ret = job.gooseChaseCallTime;
                }
                else if (newType == "stolenitem")
                {
                    if(job.activeJobItems.ContainsKey(JobPreset.JobTag.A))
                    {
                        Game.Log("DDS: Returning item A from job " + job.jobID);
                        ret = job.activeJobItems[JobPreset.JobTag.A];
                    }
                    else
                    {
                        Game.Log("DDS: Unable to retrieve stolen item from sidejob " + job.jobID + "! Cannot find item A within items " + job.activeJobItems.Count);
                    }
                }
                else if (newType == "stolentimefrom")
                {
                    SideJobStolenItem sabo = job as SideJobStolenItem;

                    if (sabo != null)
                    {
                        ret = sabo.theftTimeFrom;
                    }
                }
                else if (newType == "stolentimeto")
                {
                    SideJobStolenItem sabo = job as SideJobStolenItem;

                    if (sabo != null)
                    {
                        ret = sabo.theftTimeTo;
                    }
                }
                else if(newType == "submission")
                {
                    if(job.thisCase != null)
                    {
                        if(job.thisCase.handIn.Count > 0)
                        {
                            //Get the nearest hand-in
                            Interactable closestHandIn = job.thisCase.GetClosestHandIn();

                            if(closestHandIn != null) ret = closestHandIn.node.gameLocation;
                        }
                    }
                }
                else if(newType == "extraperson1")
                {
                    ret = job.GetExtraPerson1();
                }
            }
            else if (withinScope == "story")
            {
                if(ChapterController.Instance != null && ChapterController.Instance.chapterScript != null)
                {
                    ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                    if (intro == null)
                    {
                        Game.LogError("Value error: Unable to convert input object to Chapter: " + inputObject);
                        return ret;
                    }

                    if (intro != null)
                    {
                        if (newType == "associate")
                        {
                            ret = intro.killer;
                        }
                        else if (newType == "kidnapper")
                        {
                            ret = intro.kidnapper;
                        }
                        else if (newType == "notewriter")
                        {
                            ret = intro.noteWriter;
                        }
                        else if (newType == "restaurant")
                        {
                            ret = intro.restaurant;
                        }
                        else if (newType == "workplace")
                        {
                            ret = intro.kidnapper.job.employer.address;
                        }
                        else if (newType == "meettime")
                        {
                            ret = intro.meetTime;
                        }
                        else if(newType == "lockpicksneeded")
                        {
                            ret = Toolbox.Instance.GetLockpicksNeeded(intro.playersStorageBox.val);
                        }
                        else if (newType == "routeraddress")
                        {
                            ret = intro.chosenRouterAddress;
                        }
                        else if (newType == "bar")
                        {
                            ret = intro.killerBar;
                        }
                        else if (newType == "redgummeet")
                        {
                            ret = intro.killerBar;
                        }
                        else if (newType == "weaponsdealer")
                        {
                            ret = intro.weaponSeller.company;
                        }
                        else if (newType == "playersapartment")
                        {
                            ret = intro.apartment;
                        }
                        else if (newType == "flophouse")
                        {
                            ret = intro.slophouse;
                        }
                        else if (newType == "flophouseowner")
                        {
                            ret = intro.slophouseOwner;
                        }
                    }
                }
            }
            else if (withinScope == "evidence")
            {
                Evidence ev = inputObject as Evidence;

                if(ev != null)
                {
                    if (newType == "object")
                    {
                        ret = ev.interactable;
                    }
                    else if (newType == "timefrom")
                    {
                        EvidenceTime evT = ev as EvidenceTime;

                        if(evT != null)
                        {
                            ret = evT.timeFrom;
                        }
                    }
                    else if (newType == "timeto")
                    {
                        EvidenceTime evT = ev as EvidenceTime;

                        if (evT != null)
                        {
                            ret = evT.timeTo;
                        }
                    }
                    else if (newType == "writer")
                    {
                        ret = ev.writer;
                    }
                    else if (newType == "receiver")
                    {
                        ret = ev.reciever;
                    }
                    else if (newType == "belongsto")
                    {
                        ret = ev.belongsTo;
                    }
                    else if (newType == "telephonefrom")
                    {
                        EvidenceTelephoneCall evCall = ev as EvidenceTelephoneCall;

                        if(evCall != null)
                        {
                            if(evCall.callFrom != null)
                            {
                                ret = evCall.callFrom.interactable;
                            }
                        }
                    }
                    else if (newType == "telephoneto")
                    {
                        EvidenceTelephoneCall evCall = ev as EvidenceTelephoneCall;

                        if (evCall != null)
                        {
                            if (evCall.callTo != null)
                            {
                                ret = evCall.callTo.interactable;
                            }
                        }
                    }
                }
            }
            else if (withinScope == "time")
            {

            }
            else
            {
                Game.LogError("Scope not found: " + withinScope);
            }
        }
        catch
        {
            Game.LogError("Error while getting scope object!");
            return ret;
        }

        if (ret == null)
        {
            Game.LogError("GetScopeObject: Unable to retrieve scope " + newType + " within scope " + withinScope);
        }

        return ret;
    }

    public static string GetContainedValue(object baseObject, string withinScope, string newValue, object inputObject, Evidence baseEvidence, LinkSetting linkSetting = LinkSetting.automatic, List<Evidence.DataKey> evidenceKeys = null, object additionalObject = null, bool knowCitizenGender = false)
    {
        string output = string.Empty;
        withinScope = withinScope.ToLower();
        string lowerValue = newValue.ToLower();

        LinkData link = null;

        //Search for the correct output object
        if (withinScope == "citizen")
        {
            Human human = inputObject as Human;

            if (human == null)
            {
                VMailApp.VmailParsingData thread = inputObject as VMailApp.VmailParsingData;

                if (thread == null)
                {
                    Interactable note = inputObject as Interactable;

                    if (note != null)
                    {
                        human = note.belongsTo; //TODO: This will need to be changed to specific receiver
                    }
                }
                else
                {
                    human = Strings.GetVmailSender(thread.thread, thread.messageIndex, out _);
                }

                if(human == null)
                {
                    Game.LogError("Value error: Unable to convert input object to Human: " + inputObject);
                    return output;
                }
            }

            if (lowerValue == "fullname" || lowerValue == "name")
            {
                output = human.GetCitizenName();

                if (linkSetting == LinkSetting.forceLinks)
                {
                    if(baseEvidence != null) link = AddOrGetLink(human.evidenceEntry, baseEvidence.GetMergedDiscoveryLinkKeysFor(human.evidenceEntry, Evidence.DataKey.name)); //Get appropriate keys for this based on preset merging
                }
            }
            else if (lowerValue == "firstname")
            {
                output = human.GetFirstName();

                if (linkSetting == LinkSetting.forceLinks)
                {
                    if (baseEvidence != null) link = AddOrGetLink(human.evidenceEntry, baseEvidence.GetMergedDiscoveryLinkKeysFor(human.evidenceEntry, Evidence.DataKey.firstName));
                }
            }
            else if (lowerValue == "surname")
            {
                output = human.GetSurName();

                if (linkSetting == LinkSetting.forceLinks)
                {
                    if (baseEvidence != null) link = AddOrGetLink(human.evidenceEntry, baseEvidence.GetMergedDiscoveryLinkKeysFor(human.evidenceEntry, Evidence.DataKey.surname));
                }
            }
            else if (lowerValue == "casualname")
            {
                output = human.GetCasualName();

                if (linkSetting == LinkSetting.forceLinks)
                {
                    if (baseEvidence != null) link = AddOrGetLink(human.evidenceEntry, baseEvidence.GetMergedDiscoveryLinkKeysFor(human.evidenceEntry, Evidence.DataKey.firstName));
                }
            }
            else if (lowerValue == "formalname")
            {
                string title = string.Empty;

                if (human.gender == Human.Gender.male)
                {
                    title = Get("descriptors", "Mr");
                }
                else if (human.gender == Human.Gender.female)
                {
                    title = Get("descriptors", "Ms");
                }
                else
                {
                    title = Get("descriptors", "Mx");
                }

                output = title + " " + human.GetInitialledName();

                //Swap these around...
                if(Strings.loadedLanguage != null && Strings.loadedLanguage.swapCitizenTitleOrder)
                {
                    output = human.GetInitialledName() + " " + title;
                }

                if (linkSetting == LinkSetting.forceLinks)
                {
                    if (baseEvidence != null) link = AddOrGetLink(human.evidenceEntry, baseEvidence.GetMergedDiscoveryLinkKeysFor(human.evidenceEntry, Evidence.DataKey.initialedName));
                }
            }
            else if(lowerValue == "signature")
            {
                string citHandwriting = string.Empty;
                if (human.handwriting != null) citHandwriting = "<font=\"" + human.handwriting.fontAsset.name + "\">";

                output = citHandwriting + human.GetInitialledName() + "</font>";

                if (linkSetting == LinkSetting.forceLinks)
                {
                    if (baseEvidence != null) link = AddOrGetLink(human.evidenceEntry, baseEvidence.GetMergedDiscoveryLinkKeysFor(human.evidenceEntry, Evidence.DataKey.initialedName));
                }
            }
            else if (lowerValue == "initial")
            {
                string fName = human.GetFirstName();

                if(fName != null && fName.Length > 0)
                {
                    output = fName.Substring(0, 1);
                }

                if (linkSetting == LinkSetting.forceLinks)
                {
                    if (baseEvidence != null) link = AddOrGetLink(human.evidenceEntry, baseEvidence.GetMergedDiscoveryLinkKeysFor(human.evidenceEntry, Evidence.DataKey.firstNameInitial));
                }
            }
            else if (lowerValue == "initials")
            {
                output = human.GetInitials();

                if (linkSetting == LinkSetting.forceLinks)
                {
                    if (baseEvidence != null) link = AddOrGetLink(human.evidenceEntry, baseEvidence.GetMergedDiscoveryLinkKeysFor(human.evidenceEntry, Evidence.DataKey.initials));
                }
        }
            else if (lowerValue == "gender")
            {
                output = Get("descriptors", human.gender.ToString(), useGenderReference: knowCitizenGender, genderReference: human);
            }
            else if (lowerValue == "build")
            {
                output = Get("descriptors", human.descriptors.build.ToString(), useGenderReference: knowCitizenGender, genderReference: human);
            }
            else if (lowerValue == "haircolour")
            {
                output = Get("descriptors", human.descriptors.hairColourCategory.ToString(), useGenderReference: knowCitizenGender, genderReference: human);
            }
            else if (lowerValue == "hairtype")
            {
                output = Get("descriptors", human.descriptors.hairType.ToString(), useGenderReference: knowCitizenGender, genderReference: human);
            }
            else if (lowerValue == "eyecolour")
            {
                output = Get("descriptors", human.descriptors.eyeColour.ToString(), useGenderReference: knowCitizenGender, genderReference: human);
            }
            else if (lowerValue == "age")
            {
                output = human.GetAge().ToString();
            }
            else if (lowerValue == "agegroup")
            {
                output = Get("descriptors", human.GetAgeGroup().ToString(), useGenderReference: knowCitizenGender, genderReference: human);
            }
            else if (lowerValue == "height")
            {
                output = Get("descriptors", human.descriptors.height.ToString(), useGenderReference: knowCitizenGender, genderReference: human);
            }
            else if (lowerValue == "passcode")
            {
                List<int> digits = human.passcode.GetDigits();

                for (int i = 0; i < digits.Count; i++)
                {
                    output += digits[i];
                }

                if (linkSetting == LinkSetting.forceLinks) link = AddOrGetLink(digits);
            }
            else if (lowerValue == "passcode1")
            {
                output = human.passcode.GetDigit(0).ToString();
            }
            else if (lowerValue == "passcode2")
            {
                output = human.passcode.GetDigit(1).ToString();
            }
            else if (lowerValue == "passcode3")
            {
                output = human.passcode.GetDigit(2).ToString();
            }
            else if (lowerValue == "passcode4")
            {
                output = human.passcode.GetDigit(3).ToString();
            }
            else if (lowerValue == "relnoun")
            {
                //Uses base scope to determin this...
                Human baseHuman = baseObject as Human;
                float known = 0f;

                if (baseHuman != null)
                {
                    //Get relationship
                    Acquaintance aq = null;
                    baseHuman.FindAcquaintanceExists(human, out aq);

                    if (aq != null)
                    {
                        //Return slang/lover pet name
                        if (aq.connections.Contains(Acquaintance.ConnectionType.lover) || aq.secretConnection == Acquaintance.ConnectionType.paramour)
                        {
                            output = GetTextForComponent("143675c5-4f0b-411b-8881-04df117c3c35", baseHuman, baseHuman, null);
                        }
                        //If boss it's a 50/50 split between boss and sir/mam
                        else if (aq.connections.Contains(Acquaintance.ConnectionType.boss))
                        {
                            output = Get("misc", "boss");
                        }
                        else
                        {
                            known = aq.known;
                        }
                    }
                }

                if (output == null || output.Length <= 0)
                {
                    //Otherwise this is determined by slang use paramter
                    //0 = will use formal greeting, even for friends
                    //0.5 = will use non-gender specific greeting
                    //1 = will use gender specific greetings
                    float slangScale = Mathf.Clamp01((known + baseHuman.slangUsage) * (baseHuman.slangUsage * 2f));

                    //Only applies if gendered
                    if (slangScale <= 0.33f && human.gender == Human.Gender.male)
                    {
                        output = Get("misc", "sir");
                    }
                    else if (slangScale <= 0.33f && human.gender == Human.Gender.female)
                    {
                        output = Get("misc", "ma'am");
                    }
                    else
                    {
                        output = GetTextForComponent("ace5b902-65ef-4bde-ae3c-62c788870304", baseHuman, baseHuman, null);
                    }
                }
            }
            else if (lowerValue == "noungeneral")
            {
                output = GetTextForComponent("ace5b902-65ef-4bde-ae3c-62c788870304", human, human, null);
                //output = human.slangGreetingDefault;
            }
            else if (lowerValue == "nounlover")
            {
                output = GetTextForComponent("143675c5-4f0b-411b-8881-04df117c3c35", human, human, null);
                //output = human.slangGreetingLover;
            }
            else if (lowerValue == "curse")
            {
                output = GetTextForComponent("cbb38590-25bc-430d-89b8-03cf3ede9946", human, human, null);
                //output = human.slangCurse;
            }
            else if(lowerValue == "bloodtype")
            {
                output = Get("descriptors", human.bloodType.ToString(), useGenderReference: knowCitizenGender, genderReference: human);
            }
            else if (lowerValue == "heshe")
            {
                if (human.gender == Human.Gender.male)
                {
                    output = Get("descriptors", "he");
                }
                else if (human.gender == Human.Gender.female)
                {
                    output = Get("descriptors", "she");
                }
                else
                {
                    output = Get("descriptors", "they");
                }
            }
            else if (lowerValue == "himher")
            {
                if (human.gender == Human.Gender.male)
                {
                    output = Get("descriptors", "him");
                }
                else if (human.gender == Human.Gender.female)
                {
                    output = Get("descriptors", "her");
                }
                else
                {
                    output = Get("descriptors", "them");
                }
            }
            else if (lowerValue == "hishers")
            {
                if (human.gender == Human.Gender.male)
                {
                    output = Get("descriptors", "his");
                }
                else if (human.gender == Human.Gender.female)
                {
                    output = Get("descriptors", "hers");
                }
                else
                {
                    output = Get("descriptors", "their");
                }
            }
            else if (lowerValue == "birthgender")
            {
                output = Get("descriptors", human.birthGender.ToString());
            }
            else if (lowerValue == "dateofbirth")
            {
                output = human.birthday;

            }
            else if (lowerValue == "birthweight")
            {
                float bWeight = Toolbox.Instance.GetPsuedoRandomNumber(5f, 11f, human.citizenName + human.humanID);
                bWeight = Toolbox.Instance.RoundToPlaces(bWeight, 1);
                output = bWeight + Get("descriptors", "lb", Strings.Casing.lowerCase);
            }
            else if (lowerValue == "favitem1")
            {
                //Favourite items
                RetailItemPreset fav1 = null;
                int fav1Rank = -999999;
                RetailItemPreset fav2 = null;
                int fav2Rank = -999999;

                foreach (KeyValuePair<RetailItemPreset, int> pair in human.itemRanking)
                {
                    if (fav1 == null || pair.Value > fav1Rank)
                    {
                        fav1 = pair.Key;
                        fav1Rank = pair.Value;
                    }
                    else if (fav2 == null || pair.Value > fav2Rank)
                    {
                        fav2 = pair.Key;
                        fav2Rank = pair.Value;
                    }
                }

                if (fav1 != null) output = Get("evidence.names", fav1.itemPreset.name);
            }
            else if (lowerValue == "favitem2")
            {
                //Favourite items
                RetailItemPreset fav1 = null;
                int fav1Rank = -999999;
                RetailItemPreset fav2 = null;
                int fav2Rank = -999999;

                foreach (KeyValuePair<RetailItemPreset, int> pair in human.itemRanking)
                {
                    if (fav1 == null || pair.Value > fav1Rank)
                    {
                        fav1 = pair.Key;
                        fav1Rank = pair.Value;
                    }
                    else if (fav2 == null || pair.Value > fav2Rank)
                    {
                        fav2 = pair.Key;
                        fav2Rank = pair.Value;
                    }
                }

                if (fav2 != null) output = Get("evidence.names", fav2.itemPreset.name);
            }
            else if (lowerValue == "acquaintance")
            {
                //Uses base scope to determin this...
                Human baseHuman = baseObject as Human;

                if (baseHuman != null)
                {
                    //Get relationship
                    Acquaintance aq = null;
                    baseHuman.FindAcquaintanceExists(human, out aq);

                    if (aq != null)
                    {
                        output = Get("evidence.generic", aq.connections[0].ToString());
                    }
                    else
                    {
                        output = Get("evidence.generic", "stranger");
                    }
                }
            }
            else if (lowerValue == "namecipher")
            {
                //Name cipher
                int cipher = Toolbox.Instance.GetPsuedoRandomNumber(0, 1, human.humanID + human.citizenName);

                //Anagram
                if (cipher == 0 && human.GetFirstName().Length > 0 && human.GetSurName().Length > 0)
                {
                    //Ciphered name
                    string inital = human.GetFirstName();
                    if (inital != null && inital.Length > 0) inital = inital.Substring(0, 1);
                    inital += human.GetSurName();

                    string[] nameElements = inital.Split(' ');
                    string ciperNameNoSpaces = string.Empty;

                    for (int i = 0; i < nameElements.Length; i++)
                    {
                        ciperNameNoSpaces += nameElements[i];
                    }

                    //Give correct placements of certain letters...
                    int giveCorrect = Mathf.Max(Mathf.CeilToInt(ciperNameNoSpaces.Length * 0.33f), 1);
                    List<int> correctPlacements = new List<int>(); //The give/correct letter placements

                    //Create annagram...
                    List<char> cipherLettersRemain = new List<char>(ciperNameNoSpaces);

                    //Pick correct placements
                    for (int i = 0; i < giveCorrect; i++)
                    {
                        int pl = Toolbox.Instance.GetPsuedoRandomNumber(0, ciperNameNoSpaces.Length, human.humanID + human.citizenName + correctPlacements.Count);

                        int safety = 99;

                        while (correctPlacements.Contains(pl) && safety > 0)
                        {
                            pl++;
                            if (pl >= ciperNameNoSpaces.Length) pl = 0;
                            safety--;
                        }

                        if (!correctPlacements.Contains(pl))
                        {
                            correctPlacements.Add(pl);
                            char ch = ciperNameNoSpaces[pl];
                            cipherLettersRemain.Remove(ch);
                        }
                    }

                    string anagramClueString = string.Empty;
                    string anagramLetters = string.Empty;

                    for (int i = 0; i < ciperNameNoSpaces.Length; i++)
                    {
                        //Display space in the right place
                        if (i == 1)
                        {
                            anagramClueString += ". ";
                        }

                        //Add correct placements
                        if (correctPlacements.Contains(i))
                        {
                            anagramClueString += ciperNameNoSpaces[i];
                        }
                        else
                        {
                            anagramClueString += "_ ";

                            int anaIndex = Toolbox.Instance.GetPsuedoRandomNumber(0, cipherLettersRemain.Count, human.humanID + human.citizenName + cipherLettersRemain.Count);
                            int safety = 99;

                            while (anaIndex == i && safety > 0)
                            {
                                anaIndex = Toolbox.Instance.GetPsuedoRandomNumber(0, cipherLettersRemain.Count, human.humanID + human.citizenName + cipherLettersRemain.Count);
                                safety--;
                            }

                            char ana = cipherLettersRemain[anaIndex];
                            anagramLetters += ana;
                            anagramLetters += ' ';
                            cipherLettersRemain.Remove(ana);
                        }
                    }

                    string anagramClueComplete = anagramLetters.ToUpper() + "\n" + anagramClueString.ToUpper();

                    output = anagramClueComplete;
                }
            }
            else if (lowerValue == "killernamecipher")
            {
                //Name cipher
                int cipher = Toolbox.Instance.GetPsuedoRandomNumber(0, 1, human.humanID + human.citizenName);

                //Anagram
                if (cipher == 0 && human.GetFirstName().Length > 0 && human.GetSurName().Length > 0)
                {
                    //Ciphered name
                    string inital = human.GetFirstName();
                    if (inital != null && inital.Length > 0) inital = inital.Substring(0, 1);
                    inital += human.GetSurName();

                    string[] nameElements = inital.Split(' ');
                    string ciperNameNoSpaces = string.Empty;

                    for (int i = 0; i < nameElements.Length; i++)
                    {
                        ciperNameNoSpaces += nameElements[i];
                    }

                    //Give correct placements of certain letters...
                    List<MurderController.Murder> m = new List<MurderController.Murder>();
                    List<MurderController.Murder> m2 = new List<MurderController.Murder>();

                    //Search for murder parent in base object
                    if(baseObject != null)
                    {
                        Interactable objParent = baseObject as Interactable;

                        if(objParent != null)
                        {
                            Game.Log("Killer name cipher: Found base object interactable");

                            if (objParent.murderParent != null)
                            {
                                Game.Log("Killer name cipher: Found murder parent on object");

                                if (objParent.murderParent.time > 0)
                                {
                                    //Find all murders that happened before this murder by this killer
                                    m = MurderController.Instance.activeMurders.FindAll(item => item.murdererID == human.humanID && item.time < objParent.murderParent.time);
                                    m2 = MurderController.Instance.inactiveMurders.FindAll(item => item.murdererID == human.humanID && item.time < objParent.murderParent.time);

                                    Game.Log("Killer name cipher: Found valid murder time & " + (m.Count + m2.Count) + " murders that happened prior to this one");
                                }
                            }
                        }
                    }

                    int giveCorrect = Mathf.Clamp(m.Count + m2.Count, 0, ciperNameNoSpaces.Length - 1); //Reveal a character every murder
                    List<int> correctPlacements = new List<int>(); //The give/correct letter placements

                    //Create annagram...
                    List<char> cipherLettersRemain = new List<char>(ciperNameNoSpaces);

                    //Pick correct placements
                    for (int i = 0; i < giveCorrect; i++)
                    {
                        int pl = Toolbox.Instance.GetPsuedoRandomNumber(0, ciperNameNoSpaces.Length, human.humanID + human.citizenName + correctPlacements.Count);

                        int safety = 99;

                        while (correctPlacements.Contains(pl) && safety > 0)
                        {
                            pl++;
                            if (pl >= ciperNameNoSpaces.Length) pl = 0;
                            safety--;
                        }

                        if (!correctPlacements.Contains(pl))
                        {
                            correctPlacements.Add(pl);
                            char ch = ciperNameNoSpaces[pl];
                            cipherLettersRemain.Remove(ch);
                        }
                    }

                    string anagramClueString = string.Empty;
                    string anagramLetters = string.Empty;

                    for (int i = 0; i < ciperNameNoSpaces.Length; i++)
                    {
                        //Display space in the right place
                        if (i == 1)
                        {
                            anagramClueString += ". ";
                        }

                        //Add correct placements
                        if (correctPlacements.Contains(i))
                        {
                            anagramClueString += ciperNameNoSpaces[i];
                        }
                        else
                        {
                            anagramClueString += "_ ";

                            int anaIndex = Toolbox.Instance.GetPsuedoRandomNumber(0, cipherLettersRemain.Count, human.humanID + human.citizenName + cipherLettersRemain.Count);
                            int safety = 99;

                            while (anaIndex == i && safety > 0)
                            {
                                anaIndex = Toolbox.Instance.GetPsuedoRandomNumber(0, cipherLettersRemain.Count, human.humanID + human.citizenName + cipherLettersRemain.Count);
                                safety--;
                            }

                            char ana = cipherLettersRemain[anaIndex];
                            anagramLetters += ana;
                            anagramLetters += ' ';
                            cipherLettersRemain.Remove(ana);
                        }
                    }

                    string anagramClueComplete = anagramLetters.ToUpper() + "\n" + anagramClueString.ToUpper();

                    output = anagramClueComplete;
                }
            }
            else if (lowerValue == "investigateroom")
            {
                if(human.ai != null)
                {
                    if(human.ai.investigateLocation != null)
                    {
                        //Set the investigation room name
                        output = Get("names.rooms", human.ai.investigateLocation.room.preset.name, Strings.Casing.lowerCase);
                    }
                    else if(human.currentRoom != null)
                    {
                        //Set the investigation room name
                        output = Get("names.rooms", human.currentRoom.preset.name, Strings.Casing.lowerCase);
                    }
                }
            }
            else if (lowerValue == "investigateobject")
            {
                if (human.ai != null && human.ai.investigateObject != null)
                {
                    output = Get("evidence.names", human.ai.investigateObject.preset.name, Strings.Casing.lowerCase);
                }
            }
            else if (lowerValue == "tamperedobject")
            {
                if (human.ai != null && human.ai.tamperedObject != null)
                {
                    output = Get("evidence.names", human.ai.tamperedObject.preset.name, Strings.Casing.lowerCase);
                }
            }
            else if (lowerValue == "summary" && human.evidenceEntry != null)
            {
                output = human.evidenceEntry.GetSummary(evidenceKeys);
            }
            else if (lowerValue == "weightkg")
            {
                output = human.descriptors.weightKG.ToString();
            }
            else if (lowerValue == "heightcm")
            {
                output = human.descriptors.heightCM.ToString();
            }
            else if (lowerValue == "shoesize")
            {
                output = human.descriptors.shoeSize.ToString();
            }
            else if (lowerValue == "shoetype")
            {
                output = Get("evidence.generic", human.descriptors.footwear.ToString(), Strings.Casing.lowerCase, useGenderReference: knowCitizenGender, genderReference: human);
            }
            else if (lowerValue == "shoetype")
            {
                output = human.GetBloodTypeString();
            }
            else if(lowerValue == "interest")
            {
                List<Human.Trait> validTraits = human.characterTraits.FindAll(item => item.trait.featureInInterestPool);

                if (validTraits.Count > 0)
                {
                    Human.Trait chosenTrait = validTraits[Toolbox.Instance.GetPsuedoRandomNumber(0, validTraits.Count, CityData.Instance.seed + human.humanID + human.citizenName)];
                    output = Strings.Get("descriptors", chosenTrait.trait.name, Strings.Casing.firstLetterCaptial);
                }
            }
            else if(lowerValue == "affliction")
            {
                List<Human.Trait> validTraits = human.characterTraits.FindAll(item => item.trait.featureInAfflictionPool);

                if (validTraits.Count > 0)
                {
                    Human.Trait chosenTrait = validTraits[Toolbox.Instance.GetPsuedoRandomNumber(0, validTraits.Count, CityData.Instance.seed + human.humanID + human.citizenName)];
                    output = Strings.Get("descriptors", chosenTrait.trait.name, Strings.Casing.firstLetterCaptial);
                }
            }
            else if(lowerValue == "sightingdirection")
            {
                Human sightingOf = null;

                //if (thread != null)
                //{
                //    sightingOf = GetVmailReciever(thread.thread, thread.messageIndex, out _);
                //}
                //else if (note != null)
                //{
                //    sightingOf = note.reciever;
                //}
                if (additionalObject != null && (additionalObject as Human) != null)
                {
                    sightingOf = additionalObject as Human;
                }
                else
                {
                    if (human.inConversation && human.currentConversation != null)
                    {
                        sightingOf = human.currentConversation.currentlyTalkingTo;
                    }
                }

                if (sightingOf != null)
                {
                    if (human.lastSightings.ContainsKey(sightingOf))
                    {
                        NewGameLocation newLoc = null;
                        Vector2 dir = human.GetSightingDirection(human.lastSightings[sightingOf], out newLoc);

                        if(newLoc != null)
                        {
                            output = newLoc.name;
                        }
                        else
                        {
                            if(dir.x < 0)
                            {
                                output = Strings.Get("misc", "west");
                            }
                            else if(dir.x > 0)
                            {
                                output = Strings.Get("misc", "east");
                            }
                            else if(dir.y < 0)
                            {
                                output = Strings.Get("misc", "south");
                            }
                            else if(dir.y > 0)
                            {
                                output = Strings.Get("misc", "north");
                            }
                        }
                    }
                }
            }
            else if(lowerValue == "hotelroombuilding")
            {
                GameplayController.HotelGuest foundGuest = Toolbox.Instance.GetHotelRoom(human);

                if (foundGuest != null)
                {
                    NewAddress r = foundGuest.GetAddress();
                    if (r != null && r.building != null) output = r.building.name;
                }
            }
            else if (lowerValue == "hotelroomnumber")
            {
                GameplayController.HotelGuest foundGuest = Toolbox.Instance.GetHotelRoom(human);

                if (foundGuest != null)
                {
                    NewAddress r = foundGuest.GetAddress();

                    if (r != null && r.residence != null)
                    {
                        output = r.residence.GetResidenceString();
                    }
                }
            }
            else if (lowerValue == "hotelroomcost")
            {
                GameplayController.HotelGuest foundGuest = Toolbox.Instance.GetHotelRoom(human);

                if (foundGuest != null)
                {
                    NewAddress r = foundGuest.GetAddress();

                    if (r != null)
                    {
                        output = foundGuest.roomCost.ToString();
                    }
                }
            }
            else if (lowerValue == "hotelbill")
            {
                GameplayController.HotelGuest foundGuest = Toolbox.Instance.GetHotelRoom(human);

                if (foundGuest != null)
                {
                    foundGuest.bill.ToString();
                }
            }
        }
        else if (withinScope == "city")
        {
            if (lowerValue == "name")
            {
                output = CityData.Instance.cityName;
            }
            else if (lowerValue == "population")
            {
                output = CityData.Instance.citizenDirectory.Count.ToString();
            }
            else if (lowerValue == "unemployment")
            {
                output = Mathf.RoundToInt((float)CityData.Instance.unemployedDirectory.Count / (float)CityData.Instance.citizenDirectory.Count).ToString();
            }
            else if (lowerValue == "homeless")
            {
                output = Mathf.RoundToInt((float)CityData.Instance.homelessDirectory.Count / (float)CityData.Instance.citizenDirectory.Count).ToString();
            }
            else if (lowerValue == "currency")
            {
                output = CityControls.Instance.cityCurrency;
            }
            else if (lowerValue == "hotelcostupper")
            {
                output = CityControls.Instance.hotelCostUpper.ToString();
            }
            else if (lowerValue == "hotelcostlower")
            {
                output = CityControls.Instance.hotelCostLower.ToString();
            }
        }
        else if (withinScope == "company")
        {
            Company company = inputObject as Company;

            if (company == null)
            {
                Game.LogError("Value error: Unable to convert input object to Company: " + inputObject);
                return output;
            }

            if (lowerValue == "name")
            {
                output = company.name;
                if (linkSetting == LinkSetting.forceLinks && company.address != null) link = AddOrGetLink(company.address.evidenceEntry);
            }
            else if (lowerValue == "openhours")
            {
                //Add opening hours
                if (company.retailOpenHours.x == 0f && company.retailOpenHours.y == 24f)
                {
                    output += Get("evidence.generic", "24 hours");
                }
                else
                {
                    output += SessionData.Instance.DecimalToClockString(company.retailOpenHours.x, false) + " - " + SessionData.Instance.DecimalToClockString(company.retailOpenHours.y, false);
                }
            }
            else if (lowerValue == "opendays")
            {
                //Add opening days
                //Use syntax, open all week except x...
                if (company.daysOpen.Count >= 7)
                {
                    output += Get("evidence.generic", "every day");
                }
                else if (company.daysOpen.Count >= 5)
                {
                    output += Get("evidence.generic", "every day") + " " + Get("evidence.generic", "except") + " ";

                    for (int i = 0; i < company.daysClosed.Count; i++)
                    {
                        output += Get("ui.interface", company.daysClosed[i].ToString());
                        if (i < company.daysClosed.Count - 1) output += "& ";
                    }
                }
                else
                {
                    for (int i = 0; i < company.daysClosed.Count; i++)
                    {
                        output += Get("ui.interface", company.daysClosed[i].ToString());
                        if (i < company.daysClosed.Count - 1) output += ", ";
                    }
                }
            }
            else if (lowerValue == "item1")
            {
                List<InteractablePreset> items = company.prices.Keys.ToList();

                if(items.Count > 0)
                {
                    output = Get("evidence.names", items[0].name);
                }
            }
            else if (lowerValue == "item2")
            {
                List<InteractablePreset> items = company.prices.Keys.ToList();

                if (items.Count > 1)
                {
                    output = Get("evidence.names", items[1].name);
                }
            }
            else if (lowerValue == "item3")
            {
                List<InteractablePreset> items = company.prices.Keys.ToList();

                if (items.Count > 2)
                {
                    output = Get("evidence.names", items[2].name);
                }
            }
            else if (lowerValue == "item4")
            {
                List<InteractablePreset> items = company.prices.Keys.ToList();

                if (items.Count > 3)
                {
                    output = Get("evidence.names", items[3].name);
                }
            }
            else if (lowerValue == "item5")
            {
                List<InteractablePreset> items = company.prices.Keys.ToList();

                if (items.Count > 4)
                {
                    output = Get("evidence.names", items[4].name);
                }
            }
            else if (lowerValue == "item6")
            {
                List<InteractablePreset> items = company.prices.Keys.ToList();

                if (items.Count > 5)
                {
                    output = Get("evidence.names", items[5].name);
                }
            }
            else if (lowerValue == "item7")
            {
                List<InteractablePreset> items = company.prices.Keys.ToList();

                if (items.Count > 6)
                {
                    output = Get("evidence.names", items[6].name);
                }
            }
            else if (lowerValue == "item8")
            {
                List<InteractablePreset> items = company.prices.Keys.ToList();

                if (items.Count > 7)
                {
                    output = Get("evidence.names", items[7].name);
                }
            }
            else if (lowerValue == "item9")
            {
                List<InteractablePreset> items = company.prices.Keys.ToList();

                if (items.Count > 8)
                {
                    output = Get("evidence.names", items[8].name);
                }
            }
            else if (lowerValue == "item10")
            {
                List<InteractablePreset> items = company.prices.Keys.ToList();

                if (items.Count > 9)
                {
                    output = Get("evidence.names", items[9].name);
                }
            }
            else if (lowerValue == "loansharkloan")
            {
                output = GameplayControls.Instance.defaultLoanAmount.ToString();
            }
            else if (lowerValue == "loansharkextra")
            {
                output = GameplayControls.Instance.defaultLoanExtra.ToString();
            }
            else if (lowerValue == "loansharkpayment")
            {
                GameplayController.LoanDebt debt = GameplayController.Instance.debt.Find(item => item.companyID == company.companyID);

                if(debt != null)
                {
                    output = debt.GetRepaymentAmount().ToString();
                }
                else
                {
                    output = GameplayControls.Instance.defaultLoanRepayment.ToString();
                }
            }
            else if (lowerValue == "loansharkdebt")
            {
                GameplayController.LoanDebt debt = GameplayController.Instance.debt.Find(item => item.companyID == company.companyID);

                if (debt != null)
                {
                    output = debt.debt.ToString();
                }
                else
                {
                    output = "0";
                }
            }
            else if (lowerValue == "loansharknextdue")
            {
                GameplayController.LoanDebt debt = GameplayController.Instance.debt.Find(item => item.companyID == company.companyID);

                if (debt != null)
                {
                    int day = 0;
                    SessionData.Instance.ParseTimeData(debt.nextPaymentDueBy, out _, out day, out _, out _, out _);

                    //Minus one day to represent 'end of x'
                    day--;
                    if (day < 0) day = 6;
                    output = Strings.Get("ui.interface", ((SessionData.WeekDay)day).ToString());
                }
            }
        }
        else if (withinScope == "controls")
        {
            //Do we need to return the icon?
            string[] sp = lowerValue.Split('_');
            bool returnIcon = false;
            if (sp.Length > 1) returnIcon = true;
            string controlString = sp[0];

            if(returnIcon)
            {
                Rewired.ActionElementMap map = InputController.Instance.player.controllers.maps.GetFirstElementMapWithAction(controlString, true);

                if (map != null)
                {
                    string spriteCategory = "desktop";
                    string spriteName = map.elementIdentifierName;
                    string extraKeyText = string.Empty;

                    if (map.controllerMap.controllerType == Rewired.ControllerType.Keyboard)
                    {
                        spriteCategory = "desktop";

                        //Display a keyboard key
                        if (spriteName.Length <= 1)
                        {
                            spriteName = "Keyboard Key";
                            extraKeyText = map.elementIdentifierName.ToUpper() + "  ";
                        }
                        //Detect F keys
                        else if (spriteName.Length <= 3)
                        {
                            string f = spriteName.Substring(0, 1);

                            if (f == "F")
                            {
                                string number = spriteName.Substring(1, spriteName.Length - 1);

                                if (int.TryParse(number, out _))
                                {
                                    spriteName = "Keyboard Key";
                                    extraKeyText = map.elementIdentifierName.ToUpper() + "  ";
                                }
                            }
                        }
                    }
                    else if (map.controllerMap.controllerType == Rewired.ControllerType.Mouse)
                    {
                        spriteCategory = "desktop";
                    }
                    else
                    {
                        //Game.Log("Controller name: " + map.controllerMap.controller.name + " type " + map.controllerMap.controllerType);

                        //Default to xbox controller icons
                        spriteCategory = "controller";

                        //Convert switch stick button reference...
                        if (spriteName == "Left Stick" || spriteName == "Right Stick")
                        {
                            spriteName += " Button";
                        }

                        //Convert axis to display a singular icon
                        if (spriteName == "Left Stick X" || spriteName == "Left Stick Y")
                        {
                            spriteName = "Left Stick";
                        }
                        else if (spriteName == "Right Stick X" || spriteName == "Right Stick Y")
                        {
                            spriteName = "Right Stick";
                        }

                        //For nintendo controllers use a different version of A, B, X, Y with different arrows, as they are in different positions...
                        //Switch pro controller
                        if (map.controllerMap.controller.name == "Pro Controller")
                        {
                            if (spriteName == "X" || spriteName == "Y" || spriteName == "A" || spriteName == "B")
                            {
                                spriteName += "_Nintendo";
                            }
                        }
                    }

                    output =  "<sprite=\"" + spriteCategory + "\" name=\"" + spriteName + "\">" + extraKeyText;
                }
                else
                {
                    Game.Log("DDS: Unable to find control icon " + controlString);
                    output = string.Empty;
                }
            }
            else
            {
                Rewired.ActionElementMap map = InputController.Instance.player.controllers.maps.GetFirstElementMapWithAction(controlString, true);

                if (map != null)
                {
                    output = map.elementIdentifierName.ToUpper();
                }
                else
                {
                    Game.Log("DDS: Unable to find control icon " + controlString);
                    output = string.Empty;
                }
            }
        }
        else if (withinScope == "group")
        {
            GroupsController.SocialGroup group = inputObject as GroupsController.SocialGroup;

            if (group == null)
            {
                Game.LogError("Value error: Unable to convert input object to Group: " + inputObject);
                return output;
            }

            if (lowerValue == "name")
            {
                output = Get("misc", group.preset);
            }
            else if (lowerValue == "time")
            {
                output = SessionData.Instance.DecimalToClockString(group.decimalStartTime, false);
            }
            else if (lowerValue == "days")
            {
                for (int i = 0; i < group.weekDays.Count; i++)
                {
                    output += Strings.Get("ui.interface", group.weekDays[i].ToString());
                    if (i < group.weekDays.Count - 1) output += ", ";
                }
            }
        }
        else if (withinScope == "job")
        {
            Occupation job = inputObject as Occupation;

            if (job == null)
            {
                Game.LogError("Value error: Unable to convert input object to Job: " + inputObject);
                return output;
            }

            if (lowerValue == "title")
            {
                output = Get("jobs", job.preset.name, useGenderReference: knowCitizenGender, genderReference: job.employee);
            }
            else if (lowerValue == "salary")
            {
                output = CityControls.Instance.cityCurrency + Mathf.RoundToInt(job.salary * 1000);
            }
            else if (lowerValue == "hours")
            {
                output = job.GetWorkingHoursString();
            }
            else if (lowerValue == "start")
            {
                output = SessionData.Instance.DecimalToClockString(job.startTimeDecimalHour, false);
            }
            else if (lowerValue == "end")
            {
                output = SessionData.Instance.DecimalToClockString(job.startTimeDecimalHour + job.workHours, false);
            }
        }
        else if (withinScope == "killer")
        {
            if (lowerValue == "moniker")
            {
                output = Strings.Get("misc", "static_killer_ref_1");

                if (MurderController.Instance.currentMurderer != null)
                {
                    MurderController.Murder m = MurderController.Instance.activeMurders.Find(item => item.murderer == MurderController.Instance.currentMurderer);
                    if(m == null) m = MurderController.Instance.inactiveMurders.Find(item => item.murderer == MurderController.Instance.currentMurderer);

                    if (m != null) output = m.GetMonkier();
                }
            }
        }
        else if (withinScope == "location")
        {
            NewGameLocation location = inputObject as NewGameLocation;

            if (location == null)
            {
                Game.LogError("Value error: Unable to convert input object to Location: " + inputObject);
                return output;
            }

            if (location != null)
            {
                NewAddress address = location.thisAsAddress;
                StreetController street = location.thisAsStreet;

                if (lowerValue == "name")
                {
                    output = location.name;

                    if (linkSetting == LinkSetting.forceLinks)
                    {
                        if (baseEvidence != null) link = AddOrGetLink(location.evidenceEntry, baseEvidence.GetMergedDiscoveryLinkKeysFor(location.evidenceEntry, Evidence.DataKey.name));
                    }
                }
                else if (lowerValue == "building")
                {
                    if (address != null)
                    {
                        output = address.building.name;

                        if (linkSetting == LinkSetting.forceLinks)
                        {
                            if (baseEvidence != null) link = AddOrGetLink(location.building.evidenceEntry, baseEvidence.GetMergedDiscoveryLinkKeysFor(location.building.evidenceEntry, Evidence.DataKey.name));
                        }
                    }
                    else if(street != null)
                    {
                        output = " ";
                    }
                }
                else if (lowerValue == "district")
                {
                    if(location.district != null) output = location.district.name;
                }
                else if (lowerValue == "street")
                {
                    if(street != null)
                    {
                        output = street.name;

                        if (linkSetting == LinkSetting.forceLinks)
                        {
                            if (baseEvidence != null) link = AddOrGetLink(street.evidenceEntry, baseEvidence.GetMergedDiscoveryLinkKeysFor(street.evidenceEntry, Evidence.DataKey.name));
                        }
                    }
                    else if(address != null && address.building != null && address.building.street != null)
                    {
                        output = address.building.street.name;

                        if (linkSetting == LinkSetting.forceLinks)
                        {
                            if (baseEvidence != null) link = AddOrGetLink(address.building.street.evidenceEntry, baseEvidence.GetMergedDiscoveryLinkKeysFor(address.building.street.evidenceEntry, Evidence.DataKey.name));
                        }
                    }
                }
                else if (lowerValue == "passcode")
                {
                    if(address != null && address.passcode != null)
                    {
                        List<int> digits = address.passcode.GetDigits();

                        for (int i = 0; i < digits.Count; i++)
                        {
                            output += digits[i];
                        }

                        if (linkSetting == LinkSetting.forceLinks) link = AddOrGetLink(digits);
                    }
                }
                else if (lowerValue == "telephone")
                {
                    if(location.telephones.Count > 0)
                    {
                        output = location.telephones[0].numberString;

                        if (linkSetting == LinkSetting.forceLinks) link = AddOrGetLink(location.telephones[0]);
                    }
                }
                else if (lowerValue == "type")
                {
                    if(address != null)
                    {
                        output = Get("names.rooms", address.addressPreset.name);
                    }
                    else
                    {
                        output = Get("names.rooms", "street");
                    }
                }
                else if (lowerValue == "price")
                {
                    output = CityControls.Instance.cityCurrency + location.GetPrice(false).ToString();
                }
                else if (lowerValue == "bedrooms")
                {
                    if(address != null)
                    {
                        List<NewRoom> bedrooms = address.rooms.FindAll(item => item.preset.roomType == InteriorControls.Instance.bedroomType);

                        if(bedrooms.Count <= 0)
                        {
                            output = Get("names.rooms", "Studio");
                        }
                        else
                        {
                            output = Get("names.rooms", "Bedrooms") + ": " + bedrooms.Count.ToString();
                        }
                    }
                }
                else if (lowerValue == "floor")
                {
                    if(address != null)
                    {
                        if(address.floor != null)
                        {
                            output = Get("names.rooms", "floor_" + address.floor.floor);
                        }
                    }
                }
                else if (lowerValue == "sqm")
                {
                    output = location.GetSQM(false).ToString();
                }
                else if (lowerValue == "password")
                {
                    if (location.thisAsAddress != null) output = location.thisAsAddress.GetPassword();
                    else output = string.Empty;
                }
            }
        }
        else if (withinScope == "murder")
        {
            MurderController.Murder m = inputObject as MurderController.Murder;

            if (lowerValue == "moniker")
            {
                if (m != null)
                {
                    output = m.GetMonkier();
                }
            }
            else if (lowerValue == "method")
            {
                if(m != null)
                {
                    if(m.weaponPreset != null)
                    {
                        MurderController.MurderMethod meth = MurderController.Instance.methodTypes.Find(item => item.type == m.weaponPreset.weapon.type);

                        if(meth != null)
                        {
                            output = Strings.ComposeText(Strings.Get("dds.blocks", meth.blockDDS), m, LinkSetting.forceNoLinks);
                        }
                    }
                }
            }
            else if (lowerValue == "methodnoadverb")
            {
                if (m != null)
                {
                    if (m.weaponPreset != null)
                    {
                        MurderController.MurderMethod meth = MurderController.Instance.methodTypes.Find(item => item.type == m.weaponPreset.weapon.type);

                        if (meth != null)
                        {
                            string wAdverb = Strings.Get("dds.blocks", meth.blockDDS);
                            wAdverb = wAdverb.Replace("|adverb|", "");
                            output = Strings.ComposeText(wAdverb.Trim(), m, LinkSetting.forceNoLinks);
                        }
                    }
                    else Game.Log("DDS: Unable to get murder weapon preset");
                }
                else Game.Log("DDS: Unable to get murder class with input object " + inputObject.GetType());
            }
            else if (lowerValue == "adverb")
            {
                if(m != null)
                {
                    output = Strings.GetTextForComponent("7ab5765e-a2a6-4c08-a1de-370c26da9794", m);
                }
            }
            else if (lowerValue == "message")
            {
                if (m != null)
                {
                    output = m.graffitiMsg;
                }
            }
        }
        else if (withinScope == "object")
        {
            Interactable interactable = inputObject as Interactable;

            if(interactable != null)
            {
                if (lowerValue == "name")
                {
                    output = interactable.GetName().ToLower();
                }
                else if (lowerValue == "purchaseditems")
                {
                    //This requires receipt evidence
                    EvidenceReceipt receipt = interactable.evidence as EvidenceReceipt;

                    if(receipt != null)
                    {
                        for (int i = 0; i < receipt.purchased.Count; i++)
                        {
                            float price = receipt.soldHere.prices[receipt.purchased[i]];
                            output += "\n\n" + Strings.Get("evidence.names", receipt.purchased[i].name) + "<pos=70%>" + CityControls.Instance.cityCurrency + Toolbox.Instance.AddZeros(Toolbox.Instance.RoundToPlaces(price, 2), 2);
                        }
                    }
                    else
                    {
                        Game.LogError("Value error: Trying to get purchased items from non-receipt evidence");
                    }
                }
                else if (lowerValue == "purchasedprice")
                {
                    output = CityControls.Instance.cityCurrency;
                }
                else if (lowerValue == "telephone")
                {
                    if(interactable.t != null)
                    {
                        output = interactable.t.numberString;

                        if (linkSetting == LinkSetting.forceLinks) link = AddOrGetLink(interactable.t);
                    }
                }
                else if(lowerValue == "summary" && interactable.evidence != null)
                {
                    output = interactable.evidence.GetSummary(evidenceKeys);
                }
                else if (lowerValue == "lostitemtype")
                {
                    if(interactable.pv != null)
                    {
                        Interactable.Passed passed = interactable.pv.Find(item => item.varType == Interactable.PassedVarType.lostItemPreset);

                        if(passed != null)
                        {
                            output = Strings.Get("evidence.names", passed.str);
                        }
                    }
                }
                else if (lowerValue == "lostitembuilding")
                {
                    if (interactable.pv != null)
                    {
                        Interactable.Passed passed = interactable.pv.Find(item => item.varType == Interactable.PassedVarType.lostItemBuilding);

                        if (passed != null)
                        {
                            NewBuilding building = CityData.Instance.buildingDirectory.Find(item => item.buildingID == (int)passed.value);

                            if(building != null)
                            {
                                output = building.name;
                            }
                        }
                    }
                }
                else if (lowerValue == "lostitemreward")
                {
                    if (interactable.pv != null)
                    {
                        Interactable.Passed passed = interactable.pv.Find(item => item.varType == Interactable.PassedVarType.lostItemReward);

                        if (passed != null)
                        {
                            output = Mathf.RoundToInt(passed.value).ToString();
                        }
                    }
                }
                else if (lowerValue == "lostitemfloorx")
                {
                    if (interactable.pv != null)
                    {
                        Interactable.Passed passedFloor = interactable.pv.Find(item => item.varType == Interactable.PassedVarType.lostItemFloorX);

                        if (passedFloor != null)
                        {
                            output = Mathf.RoundToInt(passedFloor.value).ToString();
                        }
                    }
                }
                else if (lowerValue == "lostitemfloory")
                {
                    if (interactable.pv != null)
                    {
                        Interactable.Passed passedFloor = interactable.pv.Find(item => item.varType == Interactable.PassedVarType.lostItemFloorY);

                        if (passedFloor != null)
                        {
                            output = Mathf.RoundToInt(passedFloor.value).ToString();
                        }
                    }
                }

                //Special case for if this is a printed v-mail: Use the scope of the writer
                if (output == null || output.Length <= 0)
                {
                    return GetContainedValue(baseObject, "citizen", newValue, interactable.writer, baseEvidence, linkSetting, evidenceKeys, additionalObject, knowCitizenGender);
                }
            }
            else
            {
                Evidence ev = inputObject as Evidence;

                if(ev != null && ev.meta != null)
                {
                    if (lowerValue == "name")
                    {
                        output = Strings.Get("evidence.names", ev.meta.preset);
                    }
                    else if (lowerValue == "purchaseditems")
                    {
                        //This requires receipt evidence
                        EvidenceReceipt receipt = ev as EvidenceReceipt;

                        if (receipt != null)
                        {
                            for (int i = 0; i < receipt.purchased.Count; i++)
                            {
                                float price = receipt.soldHere.prices[receipt.purchased[i]];
                                output += "\n\n" + Strings.Get("evidence.names", receipt.purchased[i].name) + "<pos=70%>" + CityControls.Instance.cityCurrency + Toolbox.Instance.AddZeros(Toolbox.Instance.RoundToPlaces(price, 2), 2);
                            }
                        }
                        else
                        {
                            Game.LogError("Value error: Trying to get purchased items from non-receipt evidence");
                        }
                    }
                    else if (lowerValue == "purchasedprice")
                    {
                        output = CityControls.Instance.cityCurrency;
                    }
                    else if (lowerValue == "summary")
                    {
                        output = ev.GetSummary(evidenceKeys);
                    }
                }
                else
                {
                    if(inputObject == null)
                    {
                        Game.LogError("Value error: Unable to convert input object to Interactable: Null");
                    }
                    else
                    {
                        Game.LogError("Value error: Unable to convert input object to Interactable: " + inputObject.GetType().ToString());
                    }

                    return output;
                }
            }
        }
        else if (withinScope == "random")
        {
            string seed = CityData.Instance.seed;

            Interactable interactable = inputObject as Interactable;

            if (interactable != null)
            {
                seed += interactable.id;
            }
            else
            {
                SideJob job = inputObject as SideJob;

                if (job != null)
                {
                    seed += job.jobID;
                }
                else
                {
                    Human human = inputObject as Human;

                    if(human != null)
                    {
                        seed += human.humanID;
                    }
                    else
                    {
                        if (inputObject != null) seed += inputObject.GetHashCode();
                    }
                }
            }

            Game.Log("DDS: Random seed: " + seed);

            //if (baseObject != null) seed += baseObject.GetHashCode();

            if (lowerValue == "fullname")
            {
                List<Descriptors.EthnicGroup> groups = new List<Descriptors.EthnicGroup>();

                foreach (SocialStatistics.EthnicityFrequency freq in SocialStatistics.Instance.ethnicityFrequencies)
                {
                    for (int i = 0; i < freq.frequency; i++)
                    {
                        groups.Add(freq.ethnicity);
                    }
                }

                Descriptors.EthnicGroup nameGroup = groups[Toolbox.Instance.GetPsuedoRandomNumber(0, groups.Count, seed)];

                //Generate name
                string nameFileString = "male";

                if (Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, seed) > 0.5f)
                {
                    nameFileString = "female";
                }

                output = NameGenerator.Instance.GenerateName(null, 0f, "names." + nameGroup.ToString() + ".first." + nameFileString, 1f, null, 0f, out _, out _, out _, out _, out _, useCustomSeed: seed);
            }
            else if (lowerValue == "casualname")
            {
                List<Descriptors.EthnicGroup> groups = new List<Descriptors.EthnicGroup>();

                foreach (SocialStatistics.EthnicityFrequency freq in SocialStatistics.Instance.ethnicityFrequencies)
                {
                    for (int i = 0; i < freq.frequency; i++)
                    {
                        groups.Add(freq.ethnicity);
                    }
                }

                Descriptors.EthnicGroup nameGroup = groups[Toolbox.Instance.GetPsuedoRandomNumber(0, groups.Count, seed)];

                output = NameGenerator.Instance.GenerateName(null, 0f, "names." + nameGroup.ToString() + ".sur", 1f, null, 0f, out _, out _, out _, out _, out _, useCustomSeed: seed);
            }
            else if (lowerValue == "number5")
            {
                output = Toolbox.Instance.GetPsuedoRandomNumber(1, 6, seed).ToString();
            }
            else if (lowerValue == "number10")
            {
                output = Toolbox.Instance.GetPsuedoRandomNumber(1, 11, seed).ToString();
            }
            else if (lowerValue == "number20")
            {
                output = Toolbox.Instance.GetPsuedoRandomNumber(1, 21, seed).ToString();
            }
            else if (lowerValue == "number50")
            {
                output = Toolbox.Instance.GetPsuedoRandomNumber(1, 51, seed).ToString();
            }
            else if (lowerValue == "number100")
            {
                output = Toolbox.Instance.GetPsuedoRandomNumber(1, 101, seed).ToString();
            }
        }
        else if (withinScope == "sidejob")
        {
            SideJob job = inputObject as SideJob;

            if (job == null)
            {
                Game.LogError("Value error: Unable to convert input object to Side Job: " + inputObject);
                return output;
            }

            if (lowerValue == "reward")
            {
                if(job.rewardSyncDisk != null && job.rewardSyncDisk.Length > 0)
                {
                    output = Get("missions.postings", "a rare sync disk") + "- " + Get("evidence.syncdisks", job.rewardSyncDisk);
                    if (job.reward > 0) output += " & ";
                }

                if(job.reward > 0) output += CityControls.Instance.cityCurrency + job.reward;
            }
            else if (lowerValue == "type")
            {
                output = Get("missions.postings", job.presetStr);
            }
            else if (lowerValue == "motive")
            {
                output = Get("missions.postings", job.motiveStr);
            }
            else if(lowerValue == "fakenumber")
            {
                output = job.fakeNumberStr;

                //Parse digits
                List<int> digits = new List<int>();

                for (int i = 0; i < job.fakeNumberStr.Length; i++)
                {
                    int outp = 0;

                    if(int.TryParse(job.fakeNumberStr[i].ToString(), out outp))
                    {
                        digits.Add(outp);
                    }
                }

                if (linkSetting == LinkSetting.forceLinks) link = AddOrGetLink(digits);
            }
            else if (lowerValue == "revengeamount")
            {
                if(job.thisCase != null)
                {
                    Case.ResolveQuestion q = job.thisCase.resolveQuestions.Find(item => item.inputType == Case.InputType.revengeObjective && item.revengeObjective != null && item.revengeObjective.Length > 0);
                    if (q != null) output = Mathf.RoundToInt(q.revengeObjPassed).ToString();
                    else Game.Log("DDS: " + job.resolveQuestions.Count + " questions exist...");
                }
            }
            else if(lowerValue == "meetitem")
            {
                if(job != null)
                {
                    try
                    {
                        InteractablePreset p = InteriorControls.Instance.meetupConsumables[job.meetingConsumableIndex];

                        if(p != null)
                        {
                            output = Strings.Get("evidence.names", p.name);
                        }
                    }
                    catch
                    {

                    }
                }
            }
            else if(lowerValue == "stolenitemroom")
            {
                if(job != null)
                {
                    SideJobStolenItem st = job as SideJobStolenItem;

                    if(st != null)
                    {
                        NewRoom r = null;

                        if(CityData.Instance.roomDictionary.TryGetValue(st.stolenItemRoom, out r))
                        {
                            output = Strings.Get("names.rooms", r.preset.name, Casing.lowerCase);
                        }
                    }
                }
            }
            else if(lowerValue == "secretlocation")
            {
                if(job != null)
                {
                    if(job.secretLocationFurniture != 0)
                    {
                        NewNode n = null;

                        if(PathFinder.Instance.nodeMap.TryGetValue(job.secretLocationNode, out n))
                        {
                            FurnitureLocation furn = n.room.individualFurniture.Find(item => item.id == job.secretLocationFurniture);

                            if(furn != null)
                            {
                                if(Strings.loadedLanguage != null && Strings.loadedLanguage.systemLanguage != SystemLanguage.English)
                                {
                                    output = Strings.Get("evidence.names", furn.furniture.name) + " (" + Strings.Get("names.rooms", n.room.preset.name, Casing.lowerCase) + ", " + n.gameLocation.name + ")";
                                }
                                else output = Strings.Get("evidence.names", furn.furniture.name) + " " + Strings.Get("missions.postings", "in the") + " " + Strings.Get("names.rooms", n.room.preset.name, Casing.lowerCase) + ", " + n.gameLocation.name;
                            }
                        }
                    }
                }
            }
        }
        else if (withinScope == "story")
        {
            if(ChapterController.Instance != null && ChapterController.Instance.chapterScript != null)
            {
                ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                if(intro != null)
                {
                    if(lowerValue == "lockpicksneeded")
                    {
                        output = (intro.lockpicksNeeded - 2).ToString();
                    }
                    else if (lowerValue == "partnerfirstname")
                    {
                        output = "Sam";
                    }
                    else if (lowerValue == "partnerfullname")
                    {
                        output = "Sam Merriweather";
                    }
                }
            }
        }
        else if (withinScope == "time")
        {
            float time = Convert.ToSingle(inputObject);

            float decimalHour = 0f;
            int dayInt = 0;
            int dateInt = 0;
            int monthInt = 0;
            int yearInt = 0;

            SessionData.Instance.ParseTimeData(time, out decimalHour, out dayInt, out dateInt, out monthInt, out yearInt);

            if (lowerValue == "timeofday")
            {
                if(decimalHour > 3f && decimalHour < 12f)
                {
                    output = Get("misc", "evening");
                }
                else if(decimalHour > 3f && decimalHour < 17f)
                {
                    output = Get("misc", "afternoon");
                }
                else
                {
                    output = Get("misc", "evening");
                }
            }
            else if (lowerValue == "time")
            {
                output = SessionData.Instance.DecimalToClockString(decimalHour, false);
            }
            else if (lowerValue == "time24")
            {
                output = SessionData.Instance.DecimalToClockString(decimalHour, false);
            }
            else if (lowerValue == "time12")
            {
                float formatted = SessionData.Instance.FloatMinutes12H(decimalHour);

                string am = Get("ui.interface", "AM");
                if (decimalHour > 12) am = Get("ui.interface", "PM");

                output = SessionData.Instance.MinutesToClockString(formatted, false) + am;
            }
            else if (lowerValue == "time12rounded15")
            {
                decimalHour = (Mathf.RoundToInt((decimalHour * 100) / 25f) * 25f) / 100f;

                float formatted = SessionData.Instance.FloatMinutes12H(decimalHour);

                string am = Get("ui.interface", "AM");
                if (decimalHour > 12) am = Get("ui.interface", "PM");

                output = SessionData.Instance.MinutesToClockString(formatted, false) + am;
            }
            else if (lowerValue == "day")
            {
                output = Get("ui.interface", ((SessionData.WeekDay)dayInt).ToString());
            }
            else if (lowerValue == "date")
            {
                output = SessionData.Instance.ShortDateString(time, true);
            }
            else if (lowerValue == "datelong")
            {
                output = SessionData.Instance.LongDateString(time, true, false, true, false, true, true, false, true);
            }
            else if (lowerValue == "year")
            {
                output = yearInt.ToString();
            }
        }
        else if(withinScope == "syncdisk")
        {
            SyncDiskPreset disk = inputObject as SyncDiskPreset;

            int option = 0;
            int level = 0;

            if(additionalObject != null)
            {
                int[] optionLevel = additionalObject as int[];

                if(optionLevel != null)
                {
                    if (optionLevel.Length > 0) option = optionLevel[0];
                    if (optionLevel.Length > 1) level = optionLevel[1];
                }
            }

            if(disk != null)
            {
                if (lowerValue == "value")
                {
                    if(option == 0) output = Mathf.RoundToInt(disk.mainEffect1Value).ToString();
                    else if(option == 1) output = Mathf.RoundToInt(disk.mainEffect2Value).ToString();
                    else if (option == 2) output = Mathf.RoundToInt(disk.mainEffect3Value).ToString();
                }
                else if (lowerValue == "upgradevalue")
                {
                    if (option == 0)
                    {
                        if(level == 0 && disk.option1UpgradeValues.Count > 0) output = Mathf.RoundToInt(disk.option1UpgradeValues[0]).ToString();
                        else if (level == 1 && disk.option1UpgradeValues.Count > 1) output = Mathf.RoundToInt(disk.option1UpgradeValues[1]).ToString();
                        else if (level == 2 && disk.option1UpgradeValues.Count > 2) output = Mathf.RoundToInt(disk.option1UpgradeValues[2]).ToString();
                    }
                    else if (option == 1)
                    {
                        if (level == 0 && disk.option2UpgradeValues.Count > 0) output = Mathf.RoundToInt(disk.option2UpgradeValues[0]).ToString();
                        else if (level == 1 && disk.option2UpgradeValues.Count > 1) output = Mathf.RoundToInt(disk.option2UpgradeValues[1]).ToString();
                        else if (level == 2 && disk.option2UpgradeValues.Count > 2) output = Mathf.RoundToInt(disk.option2UpgradeValues[2]).ToString();
                    }
                    else if (option == 1)
                    {
                        if (level == 0 && disk.option3UpgradeValues.Count > 0) output = Mathf.RoundToInt(disk.option3UpgradeValues[0]).ToString();
                        else if (level == 1 && disk.option3UpgradeValues.Count > 1) output = Mathf.RoundToInt(disk.option3UpgradeValues[1]).ToString();
                        else if (level == 2 && disk.option3UpgradeValues.Count > 2) output = Mathf.RoundToInt(disk.option3UpgradeValues[2]).ToString();
                    }
                }
                else if (lowerValue == "value%")
                {
                    if (option == 0) output = Mathf.RoundToInt(disk.mainEffect1Value * 100).ToString();
                    else if (option == 1) output = Mathf.RoundToInt(disk.mainEffect2Value * 100).ToString();
                    else if (option == 2) output = Mathf.RoundToInt(disk.mainEffect3Value * 100).ToString();
                }
                else if (lowerValue == "upgradevalue%")
                {
                    if (option == 0)
                    {
                        if (level == 0 && disk.option1UpgradeValues.Count > 0) output = Mathf.RoundToInt(disk.option1UpgradeValues[0] * 100).ToString();
                        else if (level == 1 && disk.option1UpgradeValues.Count > 1) output = Mathf.RoundToInt(disk.option1UpgradeValues[1] * 100).ToString();
                        else if (level == 2 && disk.option1UpgradeValues.Count > 2) output = Mathf.RoundToInt(disk.option1UpgradeValues[2] * 100).ToString();
                    }
                    else if (option == 1)
                    {
                        if (level == 0 && disk.option2UpgradeValues.Count > 0) output = Mathf.RoundToInt(disk.option2UpgradeValues[0] * 100).ToString();
                        else if (level == 1 && disk.option2UpgradeValues.Count > 1) output = Mathf.RoundToInt(disk.option2UpgradeValues[1] * 100).ToString();
                        else if (level == 2 && disk.option2UpgradeValues.Count > 2) output = Mathf.RoundToInt(disk.option2UpgradeValues[2] * 100).ToString();
                    }
                    else if (option == 1)
                    {
                        if (level == 0 && disk.option3UpgradeValues.Count > 0) output = Mathf.RoundToInt(disk.option3UpgradeValues[0] * 100).ToString();
                        else if (level == 1 && disk.option3UpgradeValues.Count > 1) output = Mathf.RoundToInt(disk.option3UpgradeValues[1] * 100).ToString();
                        else if (level == 2 && disk.option3UpgradeValues.Count > 2) output = Mathf.RoundToInt(disk.option3UpgradeValues[2] * 100).ToString();
                    }
                }
                else if(lowerValue == "uninstallcost")
                {
                    output = Mathf.RoundToInt(disk.uninstallCost).ToString();
                }
            }
        }
        else if (withinScope == "evidence")
        {
            Evidence ev = inputObject as Evidence;

            if (ev == null)
            {
                Game.LogError("Value error: Unable to convert input object to Evidence: " + inputObject);
                return output;
            }

            if (lowerValue == "name")
            {
                output = ev.name;
            }
            else if (lowerValue == "note")
            {
                List<Evidence.DataKey> noteKeys = new List<Evidence.DataKey>();
                noteKeys.Add(Evidence.DataKey.name);
                output = ev.GetNoteComposed(noteKeys);
            }
            else if (lowerValue == "date")
            {
                EvidenceDate dEv = ev as EvidenceDate;
                if (dEv != null) output = dEv.date;
            }
            else if (lowerValue == "duration")
            {
                EvidenceTime tEv = ev as EvidenceTime;
                if (tEv != null) output = tEv.duration;
            }
        }
        else
        {
            Evidence ev = inputObject as Evidence;

            if (ev == null)
            {
                Game.LogError("Value error: Unable to convert input object to Evidence: " + inputObject);
                return output;
            }

            if (ev != null && lowerValue == "summary")
            {
                output = ev.GetSummary(evidenceKeys);
            }
            else Game.LogError("Scope not found: '" + withinScope + "'");
        }

        if(output == null || output.Length <= 0)
        {
            Game.LogError("Unable to retrieve value '" + lowerValue + "' in scope: " + withinScope);
        }
        //Make this a link...
        else if(linkSetting == LinkSetting.forceLinks && link != null)
        {
            //Adjust link text to contain the reference
            output = "<link=" + link.id.ToString() + ">" + output + "</link>";
        }

        return output;
    }

    //Returns evidence from the base object
    public static Evidence GetEvidenceFromBaseScope(object baseObject)
    {
        //Detect base scope
        Evidence baseEvidence = null;
        Human humanBase = baseObject as Human;
        VMailApp.VmailParsingData vmail = baseObject as VMailApp.VmailParsingData;
        Interactable itemBase = baseObject as Interactable;
        Human.Death murderBase = baseObject as Human.Death;
        NewGameLocation locationBase = baseObject as NewGameLocation;
        Evidence evidenceBase = baseObject as Evidence;
        SideJob sideJob = baseObject as SideJob;

        if (humanBase != null)
        {
            baseEvidence = humanBase.evidenceEntry;
            Game.Log("DDS: Base evidence is 'human'...");
        }
        else if(vmail != null)
        {
            Human sender = Strings.GetVmailSender(vmail.thread, vmail.messageIndex, out _);
            if(sender != null) baseEvidence = sender.evidenceEntry;
            Game.Log("DDS: Base evidence is 'human' (from vmail)...");
        }
        else if (itemBase != null)
        {
            baseEvidence = itemBase.evidence;
            Game.Log("DDS: Base evidence is 'item'...");
        }
        else if (locationBase != null)
        {
            baseEvidence = locationBase.evidenceEntry;
            Game.Log("DDS: Base evidence is 'location'...");
        }
        else if (evidenceBase != null)
        {
            if (evidenceBase.meta != null)
            {
                baseEvidence = evidenceBase;
                Game.Log("DDS: Base evidence is 'item' (meta)...");
            }
            else
            {
                baseEvidence = evidenceBase;
                Game.Log("DDS: Base evidence is 'evidence'...");
            }
        }
        else
        {
            Game.Log("DDS: Base evidence is 'null'...");
        }

        return baseEvidence;
    }

    public static LinkData AddOrGetLink(Evidence newEvidence, List<Evidence.DataKey> overrideKeys = null)
    {
        List<LinkData> output = null;

        if(newEvidence == null)
        {
            Game.LogError("Trying to create link from null evidence!");
            return null;
        }

        if(overrideKeys == null)
        {
            overrideKeys = new List<Evidence.DataKey>();
            overrideKeys.Add(Evidence.DataKey.name);
        }

        if(!Strings.Instance.evidenceLinkDictionary.TryGetValue(newEvidence, out output))
        {
            if(output != null)
            {
                foreach(LinkData ld in output)
                {
                    if(ld.dataKeys.SequenceEqual(overrideKeys))
                    {
                        return ld;
                    }
                }
            }
        }

        return new LinkData(newEvidence, overrideKeys);
    }

    public static LinkData AddOrGetLink(Telephone newTelephone)
    {
        LinkData output = null;

        if (!Strings.Instance.linkDictionary.TryGetValue(newTelephone as object, out output))
        {
            output = new LinkData(newTelephone);
        }

        return output;
    }

    public static LinkData AddOrGetLink(List<int> newInputCode)
    {
        foreach(KeyValuePair<object, LinkData> pair in Instance.linkDictionary)
        {
            List<int> list = pair.Key as List<int>;

            if(list != null)
            {
                if(list.SequenceEqual(newInputCode))
                {
                    return pair.Value;
                }
            }
        }

        return new LinkData(newInputCode);
    }

    public static string GetMainTextFromInteractable(Interactable interactable, LinkSetting linkSetting = LinkSetting.automatic)
    {
        string ret = string.Empty;

        //Game.Log("Getting main text from interactable " + interactable.name);

        if (interactable.evidence != null)
        {
            string ddsTree = interactable.evidence.preset.ddsDocumentID;
            if (interactable.evidence.overrideDDS != null && interactable.evidence.overrideDDS.Length > 0) ddsTree = interactable.evidence.overrideDDS;

            DDSSaveClasses.DDSTreeSave content;

            //Get DDS content
            if (!Toolbox.Instance.allDDSTrees.TryGetValue(ddsTree, out content))
            {
                Game.Log("Misc Error: Cannot find content for  tree " + ddsTree + " (" + interactable.evidence.preset.name + "). Original doc ID: " + interactable.evidence.preset.ddsDocumentID);
                return ret;
            }
            else
            {
                //Game.Log("Checking " + content.messages.Count + " messages...");
                int returnedMsg = 0;

                foreach (DDSSaveClasses.DDSMessageSettings msg in content.messages)
                {
                    //Find element: Is this text or image? Only display the 'use pages' message here...
                    if (msg.msgID != null && msg.msgID.Length > 0 && msg.usePages)
                    {
                        //Game.Log("Found message with 'use pages': " + msg.msgID);

                        //Override message text
                        object passed = interactable.evidence;
                        if (interactable != null) passed = interactable;
                        ret = Strings.GetTextForComponent(msg.msgID, passed, interactable.evidence.writer, interactable.evidence.reciever, linkSetting: linkSetting);
                        returnedMsg++;
                        break;
                    }
                }

                if (returnedMsg <= 0)
                {
                    foreach (DDSSaveClasses.DDSMessageSettings msg in content.messages)
                    {
                        //Find element: Is this text or image? Only display the 'use pages' message here...
                        if (msg.msgID != null && msg.msgID.Length > 0)
                        {
                            //Game.Log("Found message without 'use pages': " + msg.msgID);

                            //Override message text
                            object passed = interactable.evidence;
                            if (interactable != null) passed = interactable;
                            ret = Strings.GetTextForComponent(msg.msgID, passed, interactable.evidence.writer, interactable.evidence.reciever, linkSetting: linkSetting);
                            returnedMsg++;
                            break;
                        }
                    }
                }
            }
        }
        else
        {
            Game.Log("Misc Error: Interactable " + interactable.name + " has no evidence, cannot get the main text...");
        }

        return ret;
    }

    [Button]
    public void FindBlockInMessages()
    {
        if(findBlock != null && findBlock.Length > 0)
        {
            foreach(KeyValuePair<string, DDSSaveClasses.DDSMessageSave> pair in Toolbox.Instance.allDDSMessages)
            {
                if(pair.Value.blocks.Exists(item => item.blockID == findBlock))
                {
                    Game.Log("Block " + findBlock + " exists in message: " + pair.Value.name + " (" + pair.Value.id + "). Other blocks in this message include: ");

                    foreach(DDSSaveClasses.DDSBlockCondition b in pair.Value.blocks)
                    {
                        Game.Log("..." + b.blockID);
                    }
                }
            }
        }
    }

    [Button]
    public void OutputAllCharacters()
    {
        HashSet<char> characterOutputAll = new HashSet<char>();
        HashSet<char> characterOutputLatin = new HashSet<char>();
        HashSet<char> characterOutputNonLatin = new HashSet<char>();

        //Create character output file
        string outputCharPathAll = Application.streamingAssetsPath + "/Strings/Localization/UniqueCharacters_ALL.txt";
        string outputCharPathLatin = Application.streamingAssetsPath + "/Strings/Localization/UniqueCharacters_LATIN.txt";
        string outputCharPathNonLatin = Application.streamingAssetsPath + "/Strings/Localization/UniqueCharacters_NON-LATIN.txt";

        //Read all text files
        foreach (LocInput lang in fileInputConfig)
        {
            DirectoryInfo embed = new DirectoryInfo(Application.streamingAssetsPath + "/Strings/" + lang.languageCode + "/");
            List<FileInfo> embeddedFiles= embed.GetFiles("*.csv", SearchOption.AllDirectories).ToList();

            foreach (FileInfo file in embeddedFiles)
            {
                //Parse to class file
                using (StreamReader streamReader = System.IO.File.OpenText(file.FullName))
                {
                    int currentLineIndex = 0;

                    //Skip the first 3 lines
                    for (int i = 0; i < 3; i++)
                    {
                        streamReader.ReadLine();
                        currentLineIndex++;
                    }

                    while (!streamReader.EndOfStream)
                    {
                        string line = streamReader.ReadLine();

                        string key = string.Empty;
                        string display = string.Empty;
                        string alt = string.Empty;
                        int frequency = 0;
                        bool suffix = false;

                        ParseLine(line, out key, out _, out display, out alt, out frequency, out suffix, out _);

                        //Convert to display safe
                        display = ConvertLineBreaksToDisplay(display);
                        alt = ConvertLineBreaksToDisplay(alt);

                        //Count words...
                        if (display.Length > 0)
                        {
                            //Add unique chars...
                            foreach (char c in display)
                            {
                                if (!characterOutputAll.Contains(c))
                                {
                                    characterOutputAll.Add(c);
                                }

                                if(CheckForNotLatin(c))
                                {
                                    if (!characterOutputNonLatin.Contains(c))
                                    {
                                        characterOutputNonLatin.Add(c);
                                    }
                                }
                                else
                                {
                                    if (!characterOutputLatin.Contains(c))
                                    {
                                        characterOutputLatin.Add(c);
                                    }
                                }
                            }
                        }

                        if (alt.Length > 0)
                        {
                            //Add unique chars...
                            foreach (char c in alt)
                            {
                                if (!characterOutputAll.Contains(c))
                                {
                                    characterOutputAll.Add(c);
                                }

                                if (CheckForNotLatin(c))
                                {
                                    if (!characterOutputNonLatin.Contains(c))
                                    {
                                        characterOutputNonLatin.Add(c);
                                    }
                                }
                                else
                                {
                                    if (!characterOutputLatin.Contains(c))
                                    {
                                        characterOutputLatin.Add(c);
                                    }
                                }
                            }
                        }

                        currentLineIndex++;
                    }
                }
            }
        }

        //Write all chars to a string
        string usedCharsAll = string.Empty;
        string usedCharsLatin = string.Empty;
        string usedCharsNonLatin = string.Empty;

        //Add all ascii characters
        if(includeDefaultAsciiCharacters)
        {
            var allAscii = Enumerable.Range('\x1', 127).ToArray();

            foreach (char c in allAscii)
            {
                if (c == '\n' || c == '\r' || c == ' ') continue;

                if (!usedCharsAll.Contains(c))
                {
                    usedCharsAll += c;
                }

                if (CheckForNotLatin(c))
                {
                    if (!usedCharsNonLatin.Contains(c))
                    {
                        usedCharsNonLatin += c;
                    }
                }
                else
                {
                    if (!usedCharsLatin.Contains(c))
                    {
                        usedCharsLatin += c;
                    }
                }
            }
        }

        foreach (char c in characterOutputAll)
        {
            if (c == '\n' || c == '\r' || c == ' ') continue;

            if (!usedCharsAll.Contains(c))
            {
                usedCharsAll += c;
            }

            if (CheckForNotLatin(c))
            {
                if (!usedCharsNonLatin.Contains(c))
                {
                    usedCharsNonLatin += c;
                }
            }
            else
            {
                if (!usedCharsLatin.Contains(c))
                {
                    usedCharsLatin += c;
                }
            }
        }

        //Manually add characters
        foreach(char c in customUsedCharacters)
        {
            if(!usedCharsAll.Contains(c))
            {
                usedCharsAll += c;
            }

            if(CheckForNotLatin(c))
            {
                if (!usedCharsNonLatin.Contains(c))
                {
                    usedCharsNonLatin += c;
                }
            }
            else
            {
                if (!usedCharsLatin.Contains(c))
                {
                    usedCharsLatin += c;
                }
            }
        }

        List<string> charStrings = new List<string>();
        charStrings.Add(usedCharsAll);

        System.IO.File.WriteAllLines(outputCharPathAll, charStrings);

        Debug.Log(usedCharsAll.Length + " unique characters file saved in " + outputCharPathAll);

        System.IO.File.WriteAllLines(outputCharPathAll, charStrings);

        Debug.Log(usedCharsAll.Length + " unique characters file saved in " + outputCharPathAll);

        charStrings = new List<string>();
        charStrings.Add(usedCharsLatin);

        charStrings = new List<string>();
        charStrings.Add(usedCharsNonLatin);

        System.IO.File.WriteAllLines(outputCharPathNonLatin, charStrings);

        Debug.Log(usedCharsNonLatin.Length + " unique characters file saved in " + outputCharPathNonLatin);
    }

    bool CheckForNotLatin(char c)
    {
        bool boolToReturn = false;

        int code = (int)c;
        // for lower and upper cases respectively
        if ((code > 96 && code < 123) || (code > 64 && code < 91))
            boolToReturn = true;
        // visit http://www.dotnetperls.com/ascii-table for more codes

        return !boolToReturn;
    }

    [Button]
    public void ImportCorrections()
    {
        Debug.Log("Importing localization corrections input file...");
        List<string> debugOutput = new List<string>();

        DateTime corrDate;

        if (!DateTime.TryParse(correctionsInputDate, out corrDate))
        {
            Debug.Log("Unable to parse input date!");
            debugOutput.Add("Unable to parse input date!");
            return;
        }

        //Load all content for this language
        loadedLanguage = fileInputConfig.Find(item => item.systemLanguage == correctionsLanguage);

        if (loadedLanguage == null)
        {
            Debug.Log("Cannot find existing content for language " + loadedLanguage.languageCode.ToString());
            return;
        }

        List<string> templateLines = new List<string>(); //The template file stored as lines

        //Entire non-english content to be written: Language/File/Key = content (whole line including key)
        List<string> genderVariations = new List<string>();
        List<string> genderVariationFiles = new List<string>();

        //Load the template file
        string templatePath = Application.streamingAssetsPath + "/Strings/Localization/" + templateInputFile;

        //Read the input file
        using (StreamReader streamReader = System.IO.File.OpenText(templatePath))
        {
            //Read each line
            while (!streamReader.EndOfStream)
            {
                string line = streamReader.ReadLine();
                templateLines.Add(line);
            }
        }

        if (templateLines.Count <= 0)
        {
            Debug.Log("Invalid template file!");
            return;
        }

        //Entire non-english content to be written: Language/File/Key = content (whole line including key)
        Dictionary<string, Dictionary<string, string>> nonEnglishContent = new Dictionary<string, Dictionary<string, string>>();

        //Get embedded files
        DirectoryInfo embed = new DirectoryInfo(Application.streamingAssetsPath + "/Strings/" + loadedLanguage.languageCode + "/");
        List<FileInfo> embeddedFiles = embed.GetFiles("*.csv", SearchOption.AllDirectories).ToList();

        foreach (FileInfo file in embeddedFiles)
        {
            Debug.Log("Loading file: " + file.FullName);

            string fileName = file.Name.Substring(0, file.Name.Length - 4);

            if (!nonEnglishContent.ContainsKey(fileName))
            {
                nonEnglishContent.Add(fileName, new Dictionary<string, string>());
            }

            Debug.Log("Reading file: " + fileName);

            //Parse to class file
            using (StreamReader streamReader = System.IO.File.OpenText(file.FullName))
            {
                int currentLineIndex = 0;

                //Skip the first 3 lines
                for (int i = 0; i < 3; i++)
                {
                    streamReader.ReadLine();
                    currentLineIndex++;
                }

                while (!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine();

                    string key = string.Empty;
                    string display = string.Empty;
                    string alt = string.Empty;
                    int frequency = 0;
                    bool suffix = false;

                    ParseLine(line, out key, out _, out display, out alt, out frequency, out suffix, out _);

                    //Convert to display safe
                    display = ConvertLineBreaksToDisplay(display);
                    alt = ConvertLineBreaksToDisplay(alt);

                    if (key.Length > 0)
                    {
                        if (nonEnglishContent[fileName].ContainsKey(key))
                        {
                            Debug.Log("Dictionary in " + fileName + " already contains key: " + key + ", skipping...");
                        }
                        else nonEnglishContent[fileName].Add(key, line);
                    }
                    else Debug.Log("Found key of length 0 in " + fileName + ", skipping this line...");

                    currentLineIndex++;
                }
            }
        }

        Debug.Log("Loaded " + loadedLanguage.languageCode + " files. Loading corrections input file...");

        string saveLoadPath = Application.streamingAssetsPath;

        //Create path to the input file
        string inputPath = saveLoadPath + "/Strings/Localization/" + localisationCorrectionsInputFile;

        List<string> filesToWrite = new List<string>();

        //Read the input file
        using (StreamReader streamReader = System.IO.File.OpenText(inputPath))
        {
            int currentLineIndex = 0;

            //Record the last valid keys
            List<string> lastKeys = new List<string>();

            int genderVariation = 0;

            //Read each line
            while (!streamReader.EndOfStream)
            {
                string line = streamReader.ReadLine();

                //Parse the line into string list
                //Used this technqiue to split the string: https://stackoverflow.com/questions/3147836/c-sharp-regex-split-commas-outside-quotes
                char separatorChar = ',';
                System.Text.RegularExpressions.Regex regx = new System.Text.RegularExpressions.Regex(separatorChar + "(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
                List<string> lineOutput = regx.Split(line).ToList();

                //Check to see if we have content on this line
                bool lineContent = false;
                int checkColumnsForContent = lineOutput.Count;

                for (int i = 0; i < checkColumnsForContent; i++)
                {
                    if (lineOutput[i].Length > 0)
                    {
                        lineContent = true;
                    }
                }

                //The first column is the game string access key: Use lower case to make sure there are no casing errors.
                if (lineContent && lineOutput.Count > 0) //Proceed if we have anything on this line...
                {
                    for (int i = 0; i < lineOutput.Count; i++)
                    {
                        //Remove quotations
                        if (lineOutput[i].Length >= 2 && lineOutput[i][0] == '"' && lineOutput[i][lineOutput[i].Length - 1] == '"')
                        {
                            lineOutput[i] = lineOutput[i].Substring(1, lineOutput[i].Length - 2);
                        }
                    }

                    //Get the key (always first column)
                    string allKeys = lineOutput[0].ToLower();
                    List<string> keysWithFiles = new List<string>();

                    //Parse the keys
                    string currentKey = string.Empty;
                    bool keyOpen = false;

                    for (int i = 0; i < allKeys.Length; i++)
                    {
                        char currentChar = allKeys[i];

                        //Open a new key
                        if (currentChar == '<' && !keyOpen)
                        {
                            keyOpen = true;
                        }
                        //Close a key
                        else if (currentChar == '>' && keyOpen)
                        {
                            keysWithFiles.Add(currentKey);
                            currentKey = string.Empty;
                            keyOpen = false;
                        }
                        //Add to an open key
                        else if (keyOpen)
                        {
                            currentKey += currentChar;
                        }
                    }

                    //We should now have the new keys for this line (if any), record them as the last ones used in order to make sense of F/NB variations that might appear below it
                    if (keysWithFiles.Count > 0)
                    {
                        debugOutput.Add("Set of keys found on line " + line);
                        genderVariation = 0; //Reset gender variation to 0 (neutral)
                        lastKeys.Clear();
                        lastKeys.AddRange(keysWithFiles);
                    }
                    else
                    {
                        if(useGenderVariationFormatting)
                        {
                            genderVariation++;
                            debugOutput.Add("Gender variation ++ : " + genderVariation);
                        }
                    }

                    //Loop through each key assigned to this line (referenced here as key:name)
                    foreach (string keyWithFile in lastKeys)
                    {
                        //Parse the key and the name
                        string fileName = string.Empty;
                        string key = string.Empty;

                        for (int i = 0; i < keyWithFile.Length; i++)
                        {
                            //We've found the first break; use this to determin the filename...
                            if (keyWithFile[i] == ':')
                            {
                                fileName = keyWithFile.Substring(0, i);
                                key = keyWithFile.Substring(i + 1);

                                if (useGenderVariationFormatting)
                                {
                                    if (genderVariation == 1) key += "_F";
                                    else if (genderVariation == 2) key += "_NB";
                                }

                                break;
                            }
                        }

                        //Debug.Log("Attempting to find key: " + fileName + ": " + key + " for replacement...");

                        //If key is empty, skip...
                        if (key.Length > 0)
                        {
                            //Skip irrelevent lines
                            //if (lineOutput[3] == null || lineOutput[3].Length <= 0) continue;

                            //Check to see if we have an existing key in loaded content, if we do then compare dates...
                            if (nonEnglishContent.ContainsKey(fileName))
                            {
                                //Key exists...
                                if (nonEnglishContent[fileName].ContainsKey(key))
                                {
                                    //Parse the existing key...
                                    string date;

                                    ParseLine(nonEnglishContent[fileName][key], out _, out _, out _, out _, out _, out _, out date);

                                    //Compare this date to the input date in the settings...
                                    date = date.Trim();
                                    DateTime lastWritten;

                                    if (date != null && date.Length > 0)
                                    {
                                        if (DateTime.TryParse(date, out lastWritten))
                                        {
                                            //Skip this if the written is newer than now
                                            if (lastWritten >= corrDate)
                                            {
                                                Debug.Log("Existing key " + loadedLanguage.languageCode + ", " + fileName + ", " + key + " is newer (" + lastWritten.ToString() + ") than the input date (" + corrDate.ToString() + "), skipping...");
                                                debugOutput.Add("Existing key " + loadedLanguage.languageCode + ", " + fileName + ", " + key + " is newer (" + lastWritten.ToString() + ") than the input date (" + corrDate.ToString() + "), skipping...");
                                                continue;
                                            }
                                            else if (lastWritten < corrDate)
                                            {
                                                Debug.Log("Existing key " + loadedLanguage.languageCode + ", " + fileName + ", " + key + " is older (" + lastWritten.ToString() + ") than the input date (" + corrDate.ToString() + "), overwriting with new content: " + lineOutput[columnContent]);
                                                debugOutput.Add("Existing key " + loadedLanguage.languageCode + ", " + fileName + ", " + key + " is older (" + lastWritten.ToString() + ") than the input date (" + corrDate.ToString() + "), overwriting with new content: " + lineOutput[columnContent]);
                                            }
                                        }
                                        else
                                        {
                                            Debug.Log("Unable to parse date: " + date + " (Key: " + loadedLanguage.languageCode + ", " + fileName + ", " + key + ")");
                                            debugOutput.Add("Unable to parse date: " + date + " (Key: " + loadedLanguage.languageCode + ", " + fileName + ", " + key + ")");
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        Debug.Log("Unable to parse date: " + date + " (Key: " + loadedLanguage.languageCode + ", " + fileName + ", " + key + ")");
                                        debugOutput.Add("Unable to parse date: " + date + " (Key: " + loadedLanguage.languageCode + ", " + fileName + ", " + key + ")");
                                        continue;
                                    }
                                }
                                else
                                {
                                    if(createMissingKey)
                                    {
                                        Debug.Log("Missing key: " + fileName + ": " + key + ", this will be appended to the file");
                                        debugOutput.Add("Missing key: " + fileName + ": " + key + ", this will be appended to the file");
                                    }
                                    else
                                    {
                                        Debug.Log("Missing key: " + fileName + ": " + key + ", this will be skipped");
                                        debugOutput.Add("Missing key: " + fileName + ": " + key + ", this will be skipped");
                                        continue;
                                    }
                                }
                            }
                            else
                            {
                                if(createMissingFiles)
                                {
                                    Debug.Log("Missing file: " + fileName + ", this will be created");
                                    debugOutput.Add("Missing file: " + fileName + ", this will be created");
                                }
                                else
                                {
                                    Debug.Log("Missing file: " + fileName + ", this will be skipped");
                                    debugOutput.Add("Missing file: " + fileName + ", this will be skipped");
                                    continue;
                                }
                            }

                            //Write to content
                            if (!nonEnglishContent.ContainsKey(fileName))
                            {
                                nonEnglishContent.Add(fileName, new Dictionary<string, string>());
                            }

                            if (!nonEnglishContent[fileName].ContainsKey(key))
                            {
                                nonEnglishContent[fileName].Add(key, string.Empty);
                            }

                            string output = lineOutput[columnContent];

                            //If this is empty, replace with <empty>
                            if (output == null || output.Length <= 0)
                            {
                                continue;
                                //output = missingString;
                                //debugOutput.Add("Missing: " + correctionsLanguage + ", " + fileName + ", " + key + " - Replacing with missing string - " + missingString);
                            }

                            if(!filesToWrite.Contains(fileName))
                            {
                                filesToWrite.Add(fileName);
                            }

                            //Convert to game's internal string system
                            string compiledString = "\"" + key + "\"" + "," + "\"" + string.Empty + "\"" + "," + "\"" + output + "\"" + "," + "\"" + string.Empty + "\"" + "," + string.Empty + "," + "\"" + string.Empty + "\"" + "," + "\"" + correctionsInputDate + "\"";

                            //Record gender variations
                            //if (genderVariation > 0)
                            //{
                            //    genderVariations.Add(fileName + ":" + key + " (" + columnInput.languageCode + ")");

                            //    if (!genderVariationFiles.Contains(fileName))
                            //    {
                            //        genderVariationFiles.Add(fileName);
                            //    }
                            //}

                            nonEnglishContent[fileName][key] = compiledString;
                            //lineCount++;
                        }
                    }
                }
                else
                {
                    debugOutput.Add("No contents found on line " + currentLineIndex);
                }

                currentLineIndex++;
            }
        }

        //Write all content to files...
        foreach (KeyValuePair<string, Dictionary<string, string>> filePair in nonEnglishContent)
        {
            if (!filesToWrite.Contains(filePair.Key)) continue; //Don't bother to write these files unless they have been modified...

            //Get the file path for this...
            string filePath = saveLoadPath + "/Strings/" + loadedLanguage.languageCode + "/" + filePair.Key + ".csv";

            if (filePair.Key != "names.rooms")
            {
                if (filePair.Key.Length >= 5 && filePair.Key.Substring(0, 5) == "names")
                {
                    Debug.Log("Skipping file: " + filePath + " (" + filePair.Value.Count + " keys)...");
                    debugOutput.Add("Skipping file: " + filePath + " (" + filePair.Value.Count + " keys)...");
                    continue;
                }
            }

            //Start with templated lines
            List<string> lines = new List<string>(templateLines);

            foreach (KeyValuePair<string, string> keyPair in filePair.Value)
            {
                lines.Add(keyPair.Value);
            }

            //Save to file
            Debug.Log("Writing: " + filePath + " (" + filePair.Value.Count + " keys)...");
            debugOutput.Add("Writing: " + filePath + " (" + filePair.Value.Count + " keys)...");
            System.IO.File.WriteAllLines(filePath, lines);
        }

        //Debug.Log("Written " + lineCount + " keys across " + nonEnglishContent.Count + " lanuages (exluding ENG)...");
        //debugOutput.Add("Written " + lineCount + " keys across " + nonEnglishContent.Count + " lanuages (exluding ENG)...");

        //Debug.Log("Written " + genderVariations.Count + " gender variations of keys across " + genderVariationFiles.Count + " files...");
        //debugOutput.Add("Written " + genderVariations.Count + " gender variations of keys across " + genderVariationFiles.Count + " files...");

        ////List gender variation files
        //foreach (string str in genderVariationFiles)
        //{
        //    debugOutput.Add(str);
        //}

        Debug.Log("Localization update complete!");
        debugOutput.Add("Localization update complete!");

        //Write debug output
        string debugPath = saveLoadPath + "/Strings/Localization/Input/OutputDebug.txt";

        StreamWriter writer = new StreamWriter(debugPath, false);

        for (int i = 0; i < debugOutput.Count; i++)
        {
            writer.WriteLine(debugOutput[i]);
        }

        writer.Close();
    }
}
