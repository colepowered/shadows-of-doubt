﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class EvidenceDNA : Evidence
{
    public Citizen citizenController;

    public static int DNAAssign = -1;
    public static int DNAAssignLoop = 0;

    //Constructor
    public EvidenceDNA(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        citizenController = newController as Citizen;
    }

    public override void OnDiscovery()
    {
        ////Generate a set name for the prints on discovery
        //if (medicalData.DNAID < 0)
        //{
        //    medicalData.DNAID = DNAAssign;
        //    medicalData.DNALetterLoop = DNAAssignLoop;

        //    DNAAssign++;

        //    if (DNAAssign > 25)
        //    {
        //        DNAAssign = 0;
        //        DNAAssignLoop++;
        //    }

        //    medicalData.DNASetName = string.Empty;

        //    for (int i = 0; i < medicalData.DNALetterLoop + 1; i++)
        //    {
        //        medicalData.DNASetName += GameplayController.Instance.doeLetters[DNAAssign];
        //    }
        //}

        base.OnDiscovery();

        //Listen for key changes in citizen
        citizenController.evidenceEntry.OnDataKeyChange += UpdateName;
    }

    //public override string GenerateName()
    //{
    //    string ret = citizenController.evidenceEntry.GetNameForDataKey(Evidence.DataKey.dna) + "'s " + Strings.Get("evidence.generic", preset.name);
    //    return ret;
    //}
}
