﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;

public class SuspectListEntryController : MonoBehaviour
{
    public RectTransform rect;
    public ButtonController button;
    public Evidence evidence;

    public GameplayController.History key;

    public TextMeshProUGUI nameText;
    public TextMeshProUGUI timeText;
    public RawImage evidenceImage;

    public void Setup(GameplayController.History sec)
    {
        key = sec;

        GameplayController.Instance.evidenceDictionary.TryGetValue(key.evID, out evidence);
        //evidence.OnDataKeyChange += UpdateEvidenceIdentifiers;

        VisualUpdate();

        evidence.OnNewName += VisualUpdate;
        evidence.OnNoteAdded += VisualUpdate;
        evidence.OnDataKeyChange += VisualUpdate;

        button.OnPress += OpenEvidence;
    }

    public void OpenEvidence(ButtonController press)
    {
        InterfaceController.Instance.SpawnWindow(evidence, passedEvidenceKeys: key.keys, passedInteractable: evidence.interactable);
    }

    public void VisualUpdate()
    {
        evidenceImage.texture = evidence.GetPhoto(key.keys);

        button.icon.sprite = evidence.GetIcon();

        nameText.text = evidence.GetNameForDataKey(key.keys);

        string locText = string.Empty;

        if(key.locationID > 0)
        {
            NewAddress add = null;

            if(CityData.Instance.addressDictionary.TryGetValue(key.locationID, out add))
            {
                locText = add.evidenceEntry.GetNameForDataKey(Evidence.DataKey.name);
            }
        }

        if(key.lastAccess > -1)
        {
            timeText.text = SessionData.Instance.TimeString(key.lastAccess, false) + ", " + SessionData.Instance.LongDateString(key.lastAccess, true, true, true, true, true, true, false, true) +"\n" + locText;
        }
        else
        {
            timeText.text = locText;
        }
    }

    private void OnDestroy()
    {
        button.OnPress -= OpenEvidence;

        evidence.OnNewName -= VisualUpdate;
        evidence.OnNoteAdded -= VisualUpdate;
        evidence.OnDataKeyChange -= VisualUpdate;
        //evidence.OnDataKeyChange -= UpdateEvidenceIdentifiers;
    }
}
