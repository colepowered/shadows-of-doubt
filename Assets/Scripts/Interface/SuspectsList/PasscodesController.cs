﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using TMPro;

public class PasscodesController : MonoBehaviour
{
    public RectTransform rect;
    public WindowContentController wcc;
    public bool isSetup = false;
    public bool isMini = false;

    public TextMeshProUGUI contentsText;
    public RectTransform entryParent;

    public TMP_InputField searchInputField;
    public List<PasscodesEntryController> spawnedEntries = new List<PasscodesEntryController>();

    public void Setup(WindowContentController newContentController)
    {
        rect = this.gameObject.GetComponent<RectTransform>();

        wcc = newContentController;

        if(isMini)
        {
            SetPageSize(new Vector2(338, 738));
        }
        else
        {
            SetPageSize(new Vector2(740, 738));
        }

        if(contentsText != null) contentsText.text = Strings.Get("ui.handbook", "passcodesheader");

        isSetup = true;
    }

    public void SetPageSize(Vector2 newSize)
    {
        Game.Log("Set history size: " + newSize);
        rect.sizeDelta = newSize;
        wcc.normalSize = rect.sizeDelta;
    }

    private void OnEnable()
    {
        if (isSetup)
        {
            GameplayController.Instance.OnNewEvidenceHistory += UpdateListDisplay;
            UpdateListDisplay(); //Do this when tab enabled
        }
    }

    private void OnDisable()
    {
         GameplayController.Instance.OnNewEvidenceHistory -= UpdateListDisplay;
    }

    private void OnDestroy()
    {
         GameplayController.Instance.OnNewEvidenceHistory -= UpdateListDisplay;
    }

    //Update facts list
    public void UpdateListDisplay()
    {
        Game.Log("Interface: Updating passcodes list...");

        //Force deselect
        if (InterfaceController.Instance.selectedElement != null) InterfaceController.Instance.selectedElement.OnDeselect();

        rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 0);

        //Compile a list of file icons that should exist
        List<GameplayController.Passcode> required = new List<GameplayController.Passcode>();

        foreach (GameplayController.Passcode h in GameplayController.Instance.acquiredPasscodes)
        {
            if (required.Count >= 40) break; //Display 40 maximum entries
            if (required.Contains(h)) continue;
            if (h.digits.Count < 4) continue;

            if (searchInputField.text.Length <= 0)
            {
                required.Add(h);
            }
            else
            {
                string realText = string.Empty;

                if (h.type == GameplayController.PasscodeType.room)
                {
                    NewRoom room = null;

                    if(CityData.Instance.roomDictionary.TryGetValue(h.id, out room))
                    {
                        realText += room.GetName() + ", " + room.gameLocation.evidenceEntry.GetNameForDataKey(Evidence.DataKey.name);
                    }
                }
                else
                {
                    NewAddress address = null;

                    if(CityData.Instance.addressDictionary.TryGetValue(h.id, out address))
                    {
                        if(address.evidenceEntry != null) realText += address.evidenceEntry.GetNameForDataKey(Evidence.DataKey.name);
                    }
                }

                realText +=  " " + h.GetDigit(0).ToString() + h.GetDigit(1).ToString() + h.GetDigit(2).ToString() + h.GetDigit(3).ToString();

                if (realText.ToLower().Contains(searchInputField.text.ToLower()))
                {
                    required.Add(h);
                }
            }
        }

        //Go through existing list
        for (int i = 0; i < spawnedEntries.Count; i++)
        {
            PasscodesEntryController sec = spawnedEntries[i];

            //If a null entry, this has obviously been destroyed- remove entry
            if (sec == null)
            {
                spawnedEntries.RemoveAt(i);
                i--;
                continue;
            }

            if(required.Contains(sec.passcode))
            {
                required.Remove(sec.passcode);
                sec.VisualUpdate();
            }
            else
            {
                Destroy(sec.gameObject);
                spawnedEntries.RemoveAt(i);
                i--;
                continue;
            }
        }

        //The ones left in the found files list are buttons to spawn
        foreach (GameplayController.Passcode sec in required)
        {
            GameObject newButton = null;

            if (isMini)
            {
                newButton = Instantiate(PrefabControls.Instance.passcodesEntryMini, entryParent);
            }
            else
            {
                newButton = Instantiate(PrefabControls.Instance.passcodesEntry, entryParent);
            }

            PasscodesEntryController newEntry = newButton.GetComponent<PasscodesEntryController>();

            newEntry.Setup(sec);
            spawnedEntries.Add(newEntry);
        }

        for (int i = 0; i < spawnedEntries.Count; i++)
        {
            spawnedEntries[i].transform.SetAsLastSibling();
        }

        //Update rect sizes...
        entryParent.sizeDelta = new Vector2(entryParent.sizeDelta.x, Mathf.Max(spawnedEntries.Count * 130f + 24f, 466));
        SetPageSize(new Vector2(rect.sizeDelta.x, entryParent.sizeDelta.y + 400f));
    }

    public void ClearSearchButton()
    {
        searchInputField.text = string.Empty;
    }
}
