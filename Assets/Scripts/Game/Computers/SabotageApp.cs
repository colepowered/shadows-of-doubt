﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEditor;

public class SabotageApp : CruncherAppContent
{
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI percentageText;
    public RectTransform progressBar;
    public RectTransform progressBarFill;
    public float progress = 0f;

    // Update is called once per frame
    void Update()
    {
        progress += Time.deltaTime * Toolbox.Instance.Rand(0.5f, 1f);
        progress = Mathf.Clamp01(progress);
        progressBarFill.sizeDelta = new Vector2(progressBar.sizeDelta.x * progress, progressBarFill.sizeDelta.y);
        percentageText.text = Mathf.CeilToInt(progress * 100f) + "%";

        if(progress >= 1f)
        {
            //Payment
            if (controller.ic.interactable.node.gameLocation.thisAsAddress != null)
            {
                if (controller.ic.interactable.node.gameLocation.thisAsAddress.company != null)
                {
                    if(!GameplayController.Instance.companiesSabotaged.Contains(controller.ic.interactable.node.gameLocation.thisAsAddress.company.name))
                    {
                        int amount = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.installMalware));

                        if (controller.ic.interactable.node.gameLocation.thisAsAddress.owners.Contains(controller.loggedInAs))
                        {
                            amount = Mathf.RoundToInt(amount * (1f + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.malwareOwnerBonus)));
                            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "corpsabotage_success"), InterfaceControls.Icon.money, colourOverride: true, col: InterfaceControls.Instance.messageGreen);
                        }
                        else
                        {
                            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "corpsabotage_success_bonus"), InterfaceControls.Icon.money, colourOverride: true, col: InterfaceControls.Instance.messageGreen);
                        }

                        GameplayController.Instance.AddMoney(Mathf.Max(amount, 1), true, "corpsabotage");
                        GameplayController.Instance.companiesSabotaged.Add(controller.ic.interactable.node.gameLocation.thisAsAddress.company.name);
                    }
                    else
                    {
                        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "corpsabotage_fail"), InterfaceControls.Icon.money, colourOverride: true, col: InterfaceControls.Instance.messageRed);
                    }
                }
            }

            controller.OnAppExit();
        }
    }
}
