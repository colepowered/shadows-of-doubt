﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ComputerAutoTextController : MonoBehaviour
{
    void Start()
    {
        TextMeshProUGUI text = this.gameObject.GetComponent<TextMeshProUGUI>();
        text.text = Strings.Get("computer", text.text);
    }
}
