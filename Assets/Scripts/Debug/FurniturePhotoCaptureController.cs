using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;
using UnityEngine.UI;
using UnityEditor;

public class FurniturePhotoCaptureController : MonoBehaviour
{
    [Header("Components")]
    public Transform spawnParent;
    public GameObject spawnedObject;
    public Camera captureCam;

    [Header("Settings")]
    public int resolution = 128;

    [InfoBox("Don't forget to turn film grain off before capturing, unless you want it featured")]

    [Header("Capture")]
    [ShowAssetPreview]
    public Sprite captured;
    public InteractablePreset prefabOverrideObject;
    public GameObject prefabOverride;
    [Range(0.1f, 3f)]
    public float scale = 1f;
    [ReadOnly]
    public Vector3 itemPos;
    [ReadOnly]
    public Vector3 itemEuler;
    [Space(7)]
    public Vector2 captureIndex;
    public FurniturePreset captureSingle;

    [Button]
    public void LoadIndex()
    {
        int index = (int)captureIndex.x;

        List<FurniturePreset> allPresets = GetValidPresets();

        if (index >= 0 && index < allPresets.Count)
        {
            captureSingle = allPresets[index];
            LoadSingle();
        }
    }

    private List<FurniturePreset> GetValidPresets()
    {
        List<FurniturePreset> valid = null;

#if UNITY_EDITOR

        List<FurniturePreset> allPresets = AssetLoader.Instance.GetAllFurniture();
        valid = allPresets.FindAll(item => item.purchasable && item.prefab != null);

        captureIndex.y = valid.Count;
#endif

        return valid;
    }

    [Button]
    public void LoadSingle()
    {
#if UNITY_EDITOR
        if(captureSingle != null && captureSingle.purchasable && captureSingle.prefab != null)
        {
            captured = captureSingle.staticImage;

            itemPos = captureSingle.imagePos;
            itemEuler = captureSingle.imageRot;
            scale = captureSingle.imageScale;
            prefabOverride = captureSingle.imagePrefabOverride;
            if (prefabOverrideObject != null) prefabOverride = prefabOverrideObject.prefab;
            if (scale == 0f) scale = 1f;

            //Destroy existing
            if (spawnedObject != null) DestroyImmediate(spawnedObject);

            GameObject spawn = captureSingle.prefab;
            if (prefabOverride != null) spawn = prefabOverride;

            spawnedObject = Instantiate(spawn, spawnParent);
            spawnedObject.transform.localPosition = itemPos;
            spawnedObject.transform.localEulerAngles = itemEuler;
            spawnedObject.transform.localScale = new Vector3(scale, scale, scale);
        }
#endif
    }

    [Button]
    public void UpdatePositions()
    {
#if UNITY_EDITOR
        if (spawnedObject != null)
        {
            itemPos = spawnedObject.transform.localPosition;
            itemEuler = spawnedObject.transform.localEulerAngles;
            scale = spawnedObject.transform.localScale.x;

            spawnedObject.transform.localPosition = itemPos;
            spawnedObject.transform.localEulerAngles = itemEuler;
            spawnedObject.transform.localScale = new Vector3(scale, scale, scale);
        }
#endif
    }

    [Button]
    public void NextIndex()
    {
        captureIndex.x++;
        LoadIndex();
    }

    [Button]
    public void PreviousIndex()
    {
        captureIndex.x--;
        if (captureIndex.y < 0) captureIndex.y = 0;
        LoadIndex();
    }

    [Button]
    public void CaptureSingle()
    {
#if UNITY_EDITOR

        if (captureSingle == null) return;

        string prefabName = string.Empty;

        if (spawnedObject == null)
        {
            GameObject spawn = captureSingle.prefab;
            if (prefabOverrideObject != null) prefabOverride = prefabOverrideObject.prefab;
            if (prefabOverride != null) spawn = prefabOverride;
            spawnedObject = Instantiate(spawn, spawnParent);
        }

        prefabName = spawnedObject.name.Substring(0, spawnedObject.name.Length - 7);

        spawnedObject.transform.localPosition = itemPos;
        spawnedObject.transform.localEulerAngles = itemEuler;
        spawnedObject.transform.localScale = new Vector3(scale, scale, scale);

        RenderTexture tempRT = new RenderTexture(resolution, resolution, 24);
        // the 24 can be 0,16,24, formats like
        // RenderTextureFormat.Default, ARGB32 etc.

        captureCam.targetTexture = tempRT;
        captureCam.Render();

        RenderTexture.active = tempRT;
        Texture2D virtualPhoto =
            new Texture2D(resolution, resolution, TextureFormat.RGB24, false);
        // false, meaning no need for mipmaps
        virtualPhoto.ReadPixels(new Rect(0, 0, resolution, resolution), 0, 0);

        RenderTexture.active = null; //can help avoid errors 
        captureCam.targetTexture = null;
        // consider ... Destroy(tempRT);

        byte[] bytes;
        bytes = virtualPhoto.EncodeToPNG();

        string path = Application.dataPath + "/Textures/Interface/NewGraphics/FurniturePhotos/" + prefabName + ".png";
        Debug.Log("Writing: " + path);

        System.IO.File.WriteAllBytes(
            path, bytes);
        // virtualCam.SetActive(false); ... no great need for this.

        //AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate); //Force update
        AssetDatabase.Refresh();

        //path = Application.dataPath + "/Textures/Interface/NewGraphics/EvidencePhotos/" + prefabName + ".png";
        path = "Assets/Textures/Interface/NewGraphics/FurniturePhotos/" + prefabName + ".png";

        //Change settings
        TextureImporter importer = (TextureImporter)AssetImporter.GetAtPath(path);

        if(importer != null)
        {
            importer.textureType = TextureImporterType.Sprite;
            importer.mipmapEnabled = false;
            importer.filterMode = FilterMode.Trilinear;
            importer.textureShape = TextureImporterShape.Texture2D;
            importer.SaveAndReimport();
        }
        else
        {
            Debug.LogError("Cannot find texture importer at " + path);
        }

        //Set sprite
        Sprite newSprite = AssetDatabase.LoadAssetAtPath<Sprite>(path);
        captureSingle.staticImage = newSprite;
        captureSingle.imagePos = itemPos;
        captureSingle.imageRot = itemEuler;
        captureSingle.imageScale = scale;
        captureSingle.imagePrefabOverride = prefabOverride;
        EditorUtility.SetDirty(captureSingle);

        captured = newSprite;

        AssetDatabase.Refresh();

        Editor[] ed = (Editor[])Resources.FindObjectsOfTypeAll<Editor>();
        for (int i = 0; i < ed.Length; i++)
        {
            if (ed[i].GetType() == this.GetType())
            {
                ed[i].Repaint();
                return;
            }
        }
#endif
    }

    [Button]
    public void CaptureAllSpawnableInteractables()
    {
#if UNITY_EDITOR

        //Load all interactables to resources...
        List<FurniturePreset> allPresets = GetValidPresets();

        Debug.Log("All valid presets: " + allPresets.Count);

        HashSet<GameObject> prefabsCaptured = new HashSet<GameObject>();

        int captured = 0;

        foreach(FurniturePreset p in allPresets)
        {
            //Already captured this model, use the same image...
            //if(prefabsCaptured.Contains(p.prefab))
            //{
            //    //Set sprite
            //    string path = "Assets/Textures/Interface/NewGraphics/EvidencePhotos/" + p.prefab.name + ".png";
            //    Sprite newSprite = AssetDatabase.LoadAssetAtPath<Sprite>(path);
            //    captureSingle.staticImage = newSprite;
            //    EditorUtility.SetDirty(captureSingle);
            //}
            //else
            //{
                captureSingle = p;
            LoadSingle();
                CaptureSingle();

                prefabsCaptured.Add(p.prefab);
                captured++;
            //}

            Debug.Log(captured + " images captured");

            //Destroy existing
            if (spawnedObject != null) DestroyImmediate(spawnedObject);
        }
#endif
    }
}
