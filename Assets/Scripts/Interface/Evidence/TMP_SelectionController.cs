﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using System.Text;

#pragma warning disable 0618 // Disabled warning due to SetVertices being deprecated until new release with SetMesh() is available.

public class TMP_SelectionController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerUpHandler
{
    public TextMeshProUGUI m_TextMeshPro;
    private Canvas m_Canvas;
    private Camera m_Camera;

    // Flags
    private bool isHoveringObject;
    //private int m_selectedWord = -1;
    private int m_selectedLink = -1;
    //private int m_lastIndex = -1;

    private Matrix4x4 m_matrix;

    private TMP_MeshInfo[] m_cachedMeshInfoVertexData;

    public List<LinkButtonController> linkButtons = new List<LinkButtonController>();

    //Hover colours
    public Color hoverColour = Color.red;
    private Color hoverOriginal = Color.black;
    public Color highlightColour = Color.yellow;

    private bool originalUseGradient = false;
    private TMP_ColorGradient originalGradient;

    //For keeping track which links are marked
    public Dictionary<int, bool> markedLinks = new Dictionary<int, bool>();
    private int lastPage = 0;

    void Awake()
    {
        m_TextMeshPro = gameObject.GetComponent<TextMeshProUGUI>();

        m_Canvas = InterfaceControls.Instance.hudCanvas;

        // Get a reference to the camera if Canvas Render Mode is not ScreenSpace Overlay.
        if (m_Canvas.renderMode == RenderMode.ScreenSpaceOverlay)
            m_Camera = null;
        else
            m_Camera = m_Canvas.worldCamera;

        UpdateOriginalFontSettings();
    }

    public void UpdateOriginalFontSettings()
    {
        //Get original colour
        hoverOriginal = m_TextMeshPro.color;
        originalUseGradient = m_TextMeshPro.enableVertexGradient;
        originalGradient = m_TextMeshPro.colorGradientPreset;
    }

    void OnEnable()
    {
        // Subscribe to event fired when text object has been regenerated.
        TMPro_EventManager.TEXT_CHANGED_EVENT.Add(ON_TEXT_CHANGED);
    }

    void OnDisable()
    {
        // UnSubscribe to event fired when text object has been regenerated.
        TMPro_EventManager.TEXT_CHANGED_EVENT.Remove(ON_TEXT_CHANGED);
    }

    void ON_TEXT_CHANGED(Object obj)
    {
        if (obj == m_TextMeshPro)
        {
            // Update cached vertex data.
            m_cachedMeshInfoVertexData = m_TextMeshPro.textInfo.CopyMeshInfoVertexData();
        }
    }

    //Updating the links only works late
	void Start()
	    {
	        StartCoroutine(LateStart());
	    }
	 
	IEnumerator LateStart()
	{
	    yield return new WaitForEndOfFrame();

	    //Find marked links (set all to false at awake)
	    for (int i = 0; i < m_TextMeshPro.textInfo.linkCount; i++)
        {
		    TMP_LinkInfo link = m_TextMeshPro.textInfo.linkInfo[i];

            //Parse link ID to int
            int linkID = 0;
            if(int.TryParse(link.GetLinkID(), out linkID))
            {
                if(!markedLinks.ContainsKey(linkID))
                {
                    markedLinks.Add(linkID, false);
                }
            }
        }

	    UpdateLinkDiscovery();
	}

    void LateUpdate()
    {
        if (isHoveringObject)
		{
            // Check if mouse intersects with any links.
            int linkIndex = TMP_TextUtilities.FindIntersectingLink(m_TextMeshPro, Input.mousePosition, m_Camera);

            //If currently over no link, but one is selected, restore and deselect
            if(linkIndex == -1 && m_selectedLink != -1) EndHover(m_selectedLink);

            //New link hover if it's not null(-1)
			if(linkIndex != -1 && linkIndex != m_selectedLink) NewHover(linkIndex);
		}

        //Detect page change
        if(m_TextMeshPro.pageToDisplay != lastPage)
        {
            RefreshLinkButtons(true);
        }
    }

    public void NewHover(int linkIndex)
    {
        //Restore previous link
		if ((linkIndex == -1 && m_selectedLink != -1) || (linkIndex != m_selectedLink && m_selectedLink != -1))
        {
        	EndHover(m_selectedLink);
        }

		// Handle new Link selection.
        if (linkIndex != -1 && linkIndex != m_selectedLink)
        {
            m_selectedLink = linkIndex;

            TMP_LinkInfo linkInfo = m_TextMeshPro.textInfo.linkInfo[linkIndex];

            // Iterate through each of the characters of the link
            for (int i = 0; i < linkInfo.linkTextLength; i++)
            {
                int characterIndex = linkInfo.linkTextfirstCharacterIndex + i;

                //If this character is whitespace, then skip
				if(!m_TextMeshPro.textInfo.characterInfo[characterIndex].isVisible) continue;

                // Get the index of the material / sub text object used by this character.
                int meshIndex = m_TextMeshPro.textInfo.characterInfo[characterIndex].materialReferenceIndex;

                int vertexIndex = m_TextMeshPro.textInfo.characterInfo[characterIndex].vertexIndex;

                // Get a reference to the vertex color
                Color32[] vertexColors = m_TextMeshPro.textInfo.meshInfo[meshIndex].colors32;

                //Color32 c = vertexColors[vertexIndex + 0].Tint(0.75f);

                vertexColors[vertexIndex + 0] = hoverColour;
                vertexColors[vertexIndex + 1] = hoverColour;
                vertexColors[vertexIndex + 2] = hoverColour;
                vertexColors[vertexIndex + 3] = hoverColour;
                }

                // Update Geometry
                m_TextMeshPro.UpdateVertexData(TMP_VertexDataUpdateFlags.All);
            }
    }

    public void EndHover(int linkIndex)
    {
		if (linkIndex == -1 || linkIndex > m_TextMeshPro.textInfo.linkCount - 1) return;

		TMP_LinkInfo linkInfo = m_TextMeshPro.textInfo.linkInfo[m_selectedLink];

        // Iterate through each of the characters of the word.
        for (int i = 0; i < linkInfo.linkTextLength; i++)
        {
            int characterIndex = linkInfo.linkTextfirstCharacterIndex + i;

			//If this character is whitespace, then skip
			if(!m_TextMeshPro.textInfo.characterInfo[characterIndex].isVisible) continue;

            // Get the index of the material / sub text object used by this character.
            int meshIndex = m_TextMeshPro.textInfo.characterInfo[characterIndex].materialReferenceIndex;

            // Get the index of the first vertex of this character.
            int vertexIndex = m_TextMeshPro.textInfo.characterInfo[characterIndex].vertexIndex;

            // Get a reference to the vertex color
            Color32[] vertexColors = m_TextMeshPro.textInfo.meshInfo[meshIndex].colors32;

            //Color32 c = vertexColors[vertexIndex + 0].Tint(1.33333f);

            vertexColors[vertexIndex + 0] = hoverOriginal;
			vertexColors[vertexIndex + 1] = hoverOriginal;
			vertexColors[vertexIndex + 2] = hoverOriginal;
			vertexColors[vertexIndex + 3] = hoverOriginal;

            m_TextMeshPro.enableVertexGradient = originalUseGradient;
            m_TextMeshPro.colorGradientPreset = originalGradient;
            }

            // Update Geometry
            m_TextMeshPro.UpdateVertexData(TMP_VertexDataUpdateFlags.All);

            m_selectedLink = -1;
    }

    public void UpdateLinkDiscovery()
    {
		//Game.Log("update link discovery " + m_TextMeshPro.textInfo.linkCount);

		for (int i = 0; i < m_TextMeshPro.textInfo.linkCount; i++)
        {
			TMP_LinkInfo link = m_TextMeshPro.textInfo.linkInfo[i];

        	//Parse link ID to int
        	int linkID = 0;
        	int.TryParse(link.GetLinkID(), out linkID);

        	//Get the opening tag for marking
			string openMarkedTag = "<mark=#" + ColorUtility.ToHtmlStringRGBA(InterfaceControls.Instance.markedLinkColour) + ">";
        }

        //Replace pseudo tagged text
		StringBuilder sb2 = new StringBuilder();
		sb2.Append(m_TextMeshPro.text);

		sb2.Replace("|u", "<u>");
		sb2.Replace("u|", "</u>");

		m_TextMeshPro.SetText(sb2);

		m_TextMeshPro.ForceMeshUpdate();

        RefreshLinkButtons(true);
    }

    //Refresh link buttons
    public void RefreshLinkButtons(bool updateNavigation = true)
    {
        //Game.Log("Update link buttons for " + m_TextMeshPro.textInfo.linkCount + " links...");

        for (int i = 0; i < linkButtons.Count; i++)
        {
            Destroy(linkButtons[i].gameObject);
        }

        linkButtons = new List<LinkButtonController>();

        for (int i = 0; i < m_TextMeshPro.textInfo.linkCount; i++)
        {
            TMP_LinkInfo link = m_TextMeshPro.textInfo.linkInfo[i];

            //Game.Log("Displayed page number: " + m_TextMeshPro.pageToDisplay);
            //Game.Log("Starting char is on page " + (m_TextMeshPro.textInfo.characterInfo[link.linkTextfirstCharacterIndex].pageNumber + 1));

            if (m_TextMeshPro.pageToDisplay != m_TextMeshPro.textInfo.characterInfo[link.linkTextfirstCharacterIndex].pageNumber + 1) continue; //Skip if different page

            //The following are local coordinates...
            Vector3 topLeft = m_TextMeshPro.textInfo.characterInfo[link.linkTextfirstCharacterIndex].topLeft;
            Vector3 bottomLeft = m_TextMeshPro.textInfo.characterInfo[link.linkTextfirstCharacterIndex].bottomLeft;

            Vector3 topRight = m_TextMeshPro.textInfo.characterInfo[link.linkTextfirstCharacterIndex].topRight;
            Vector3 bottomRight = m_TextMeshPro.textInfo.characterInfo[link.linkTextfirstCharacterIndex].bottomRight;

            float height = topLeft.y - bottomLeft.y;
            float width = topRight.x - topLeft.x;

            int linkIndex = 1;
            int lineNo = m_TextMeshPro.textInfo.characterInfo[link.linkTextfirstCharacterIndex].lineNumber;
            int charIndex = link.linkTextfirstCharacterIndex + linkIndex;
            bool remainingButton = false;

            //Loop through link to find end and/or line breaks...
            while (linkIndex < link.linkTextLength)
            {
                //Game.Log("linkIndex " + linkIndex + "/" + link.linkTextLength + " for " + link.GetLinkText());
                Vector3 rT = m_TextMeshPro.textInfo.characterInfo[charIndex].topRight;
                Vector3 rB = m_TextMeshPro.textInfo.characterInfo[charIndex].bottomRight;

                //This is to the right of the last character: This is on the same line...
                if (m_TextMeshPro.textInfo.characterInfo[charIndex].lineNumber == lineNo && m_TextMeshPro.pageToDisplay == m_TextMeshPro.textInfo.characterInfo[charIndex].pageNumber + 1)
                {
                    //Continue...
                    remainingButton = true;

                    height = Mathf.Max(height, rT.y - rB.y); //Search for the max char height for the link height
                    width = rT.x - topLeft.x;

                    //Game.Log("...Character is on same line...");
                }
                //This is to the left of the last character: This is on a new line...
                else if(remainingButton)
                {
                    //Draw a button extending up until now
                    DrawButton(bottomLeft, width, height, link.GetLinkID(), link.GetLinkText());
                    remainingButton = true;

                    //Start anew on this line...
                    topLeft = m_TextMeshPro.textInfo.characterInfo[charIndex].topLeft;
                    bottomLeft = m_TextMeshPro.textInfo.characterInfo[charIndex].bottomLeft;

                    height = topLeft.y - bottomLeft.y;
                    width = rT.x - topLeft.x;
                    lineNo = m_TextMeshPro.textInfo.characterInfo[charIndex].lineNumber;

                    //Game.Log("...Character is on different line: Draw button");
                }

                //Stop here if invisible...
                if (m_TextMeshPro.pageToDisplay != m_TextMeshPro.textInfo.characterInfo[charIndex].pageNumber + 1)
                {
                    //Game.Log("...Character is on different page, skipping...");
                    break;
                }

                topRight = rT;
                bottomRight = rB;

                linkIndex++;
                charIndex++;
            }

            //Do we have a button left to draw?
            if(remainingButton)
            {
                //Game.Log("...Reached end with remaining button: Draw button");
                DrawButton(bottomLeft, width, height, link.GetLinkID(), link.GetLinkText());
                remainingButton = false;
            }
        }

        lastPage = m_TextMeshPro.pageToDisplay;

        if(updateNavigation)
        {
            //Refresh window controller nav
            InfoWindow window = this.transform.GetComponentInParent<InfoWindow>();

            if(window != null)
            {
                window.UpdateControllerNavigationEndOfFrame();
            }
        }
    }

    private LinkButtonController DrawButton(Vector3 bottomLeft, float width, float height, string linkID, string buttonName)
    {
        GameObject newButton = Instantiate(PrefabControls.Instance.linkButton, this.transform);

        LinkButtonController lbc = newButton.GetComponent<LinkButtonController>();
        lbc.Setup(linkID, this);
        lbc.rect.localPosition = bottomLeft - new Vector3(0, 4f);
        lbc.rect.sizeDelta = new Vector2(width, height + 8f);
        newButton.name = buttonName;

        linkButtons.Add(lbc);

        return lbc;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Game.Log("OnPointerEnter()");
        isHoveringObject = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;

        //Game.Log("OnPointerExit()");
        isHoveringObject = false;
        EndHover(m_selectedLink);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        // Check if Mouse intersects any words and if so assign a random color to that word.
        int linkIndex = TMP_TextUtilities.FindIntersectingLink(m_TextMeshPro, Input.mousePosition, m_Camera);
        if (linkIndex != -1)
        {
            TMP_LinkInfo linkInfo = m_TextMeshPro.textInfo.linkInfo[linkIndex];

            //Game.Log(TMP_TextUtilities.GetSimpleHashCode("id_02"));

			//Parse link ID to int
        	int linkID = 0;
        	int.TryParse(linkInfo.GetLinkID(), out linkID);

            Strings.LinkData foundLink = null;

            if (Strings.Instance.linkIDReference.TryGetValue(linkID, out foundLink))
            {
                foundLink.OnLink();
            }
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        //Game.Log("OnPointerUp()");
    }

    void RestoreCachedVertexAttributes(int index)
    {
        if (index == -1 || index > m_TextMeshPro.textInfo.characterCount - 1) return;

        // Get the index of the material / sub text object used by this character.
        int materialIndex = m_TextMeshPro.textInfo.characterInfo[index].materialReferenceIndex;

        // Get the index of the first vertex of the selected character.
        int vertexIndex = m_TextMeshPro.textInfo.characterInfo[index].vertexIndex;

        // Restore Vertices
        // Get a reference to the cached / original vertices.
        Vector3[] src_vertices = m_cachedMeshInfoVertexData[materialIndex].vertices;

        // Get a reference to the vertices that we need to replace.
        Vector3[] dst_vertices = m_TextMeshPro.textInfo.meshInfo[materialIndex].vertices;

        // Restore / Copy vertices from source to destination
        dst_vertices[vertexIndex + 0] = src_vertices[vertexIndex + 0];
        dst_vertices[vertexIndex + 1] = src_vertices[vertexIndex + 1];
        dst_vertices[vertexIndex + 2] = src_vertices[vertexIndex + 2];
        dst_vertices[vertexIndex + 3] = src_vertices[vertexIndex + 3];

        // Restore Vertex Colors
        // Get a reference to the vertex colors we need to replace.
        Color32[] dst_colors = m_TextMeshPro.textInfo.meshInfo[materialIndex].colors32;

        // Get a reference to the cached / original vertex colors.
        Color32[] src_colors = m_cachedMeshInfoVertexData[materialIndex].colors32;

        // Copy the vertex colors from source to destination.
        dst_colors[vertexIndex + 0] = src_colors[vertexIndex + 0];
        dst_colors[vertexIndex + 1] = src_colors[vertexIndex + 1];
        dst_colors[vertexIndex + 2] = src_colors[vertexIndex + 2];
        dst_colors[vertexIndex + 3] = src_colors[vertexIndex + 3];

        // Restore UV0S
        // UVS0
        Vector2[] src_uv0s = m_cachedMeshInfoVertexData[materialIndex].uvs0;
        Vector2[] dst_uv0s = m_TextMeshPro.textInfo.meshInfo[materialIndex].uvs0;
        dst_uv0s[vertexIndex + 0] = src_uv0s[vertexIndex + 0];
        dst_uv0s[vertexIndex + 1] = src_uv0s[vertexIndex + 1];
        dst_uv0s[vertexIndex + 2] = src_uv0s[vertexIndex + 2];
        dst_uv0s[vertexIndex + 3] = src_uv0s[vertexIndex + 3];

        // UVS2
        Vector2[] src_uv2s = m_cachedMeshInfoVertexData[materialIndex].uvs2;
        Vector2[] dst_uv2s = m_TextMeshPro.textInfo.meshInfo[materialIndex].uvs2;
        dst_uv2s[vertexIndex + 0] = src_uv2s[vertexIndex + 0];
        dst_uv2s[vertexIndex + 1] = src_uv2s[vertexIndex + 1];
        dst_uv2s[vertexIndex + 2] = src_uv2s[vertexIndex + 2];
        dst_uv2s[vertexIndex + 3] = src_uv2s[vertexIndex + 3];


        // Restore last vertex attribute as we swapped it as well
        int lastIndex = (src_vertices.Length / 4 - 1) * 4;

        // Vertices
        dst_vertices[lastIndex + 0] = src_vertices[lastIndex + 0];
        dst_vertices[lastIndex + 1] = src_vertices[lastIndex + 1];
        dst_vertices[lastIndex + 2] = src_vertices[lastIndex + 2];
        dst_vertices[lastIndex + 3] = src_vertices[lastIndex + 3];

        // Vertex Colors
        src_colors = m_cachedMeshInfoVertexData[materialIndex].colors32;
        dst_colors = m_TextMeshPro.textInfo.meshInfo[materialIndex].colors32;
        dst_colors[lastIndex + 0] = src_colors[lastIndex + 0];
        dst_colors[lastIndex + 1] = src_colors[lastIndex + 1];
        dst_colors[lastIndex + 2] = src_colors[lastIndex + 2];
        dst_colors[lastIndex + 3] = src_colors[lastIndex + 3];

        // UVS0
        src_uv0s = m_cachedMeshInfoVertexData[materialIndex].uvs0;
        dst_uv0s = m_TextMeshPro.textInfo.meshInfo[materialIndex].uvs0;
        dst_uv0s[lastIndex + 0] = src_uv0s[lastIndex + 0];
        dst_uv0s[lastIndex + 1] = src_uv0s[lastIndex + 1];
        dst_uv0s[lastIndex + 2] = src_uv0s[lastIndex + 2];
        dst_uv0s[lastIndex + 3] = src_uv0s[lastIndex + 3];

        // UVS2
        src_uv2s = m_cachedMeshInfoVertexData[materialIndex].uvs2;
        dst_uv2s = m_TextMeshPro.textInfo.meshInfo[materialIndex].uvs2;
        dst_uv2s[lastIndex + 0] = src_uv2s[lastIndex + 0];
        dst_uv2s[lastIndex + 1] = src_uv2s[lastIndex + 1];
        dst_uv2s[lastIndex + 2] = src_uv2s[lastIndex + 2];
        dst_uv2s[lastIndex + 3] = src_uv2s[lastIndex + 3];

        // Need to update the appropriate 
        m_TextMeshPro.UpdateVertexData(TMP_VertexDataUpdateFlags.All);
    }
}
