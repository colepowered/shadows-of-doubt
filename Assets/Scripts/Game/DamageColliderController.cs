﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageColliderController : MonoBehaviour
{
    public Collider coll;
    public Actor attacker;
    public Actor target;
    public float damage = 0.1f;
    public Human enableKill = null;
    public MurderWeaponPreset weapon;

    public void Setup(Actor newAttacker, Actor newTarget, float newDamage, Human newEnableKill, MurderWeaponPreset newWeapon)
    {
        Game.Log("Damage collider spawned for " + newAttacker + " aiming for " + newTarget + " dmg: " + newDamage);
        attacker = newAttacker;
        target = newTarget;
        damage = newDamage;
        enableKill = newEnableKill;
        weapon = newWeapon;

        //Apply damage outgoing from player multiplier
        if(attacker.isPlayer)
        {
            damage *= StatusController.Instance.damageOutgoingMultiplier;
        }
    }

    //Works if this is a trigger: Does NOT work with player's character controller
    void OnCollisionEnter(Collision other)
    {
        Game.Log("OnCollisionEnter: " + other.gameObject.name);

        //Try and find an actor class...
        Actor hit = other.gameObject.GetComponent<Actor>();
        if (hit == null) hit = other.gameObject.GetComponentInParent<Actor>();

        ContactPoint contact = other.GetContact(0);

        if (hit != null)
        {
            ProcessHit(hit, contact.point, contact.normal);
        }
        else
        {
            if(weapon.impactEvent != null) AudioController.Instance.PlayWorldOneShot(weapon.impactEvent, null, null, contact.point);

            //Hit window
            BreakableWindowController wbc = other.gameObject.GetComponent<BreakableWindowController>();

            if(wbc != null)
            {
                //A rough but hopefully good enough approximation of the hit world position
                //Vector3 approxHitLocation = this.transform.TransformPoint(new Vector3(0, -1.1f, 0));

                wbc.BreakWindow(contact.point, -this.transform.up, attacker);
            }
            else
            {
                //Hit interactable
                InteractableController[] ic = other.gameObject.GetComponentsInChildren<InteractableController>();

                for (int i = 0; i < ic.Length; i++)
                {
                    InteractableController obj = ic[i];

                    if(obj.interactable.preset.physicsProfile != null && obj.interactable.preset.reactWithExternalStimuli)
                    {
                        obj.SetPhysics(true, attacker);

                        if(obj.rb != null)
                        {
                            Vector3 force = -this.transform.up * GameplayControls.Instance.throwForce;
                            obj.rb.AddForce(force, ForceMode.VelocityChange);
                        }
                    }
                }
            }
        }
    }

    //Works with player's character controller
    void OnControllerColliderHit(ControllerColliderHit other)
    {
        Game.Log("OnControllerColliderHit: " + other.gameObject.name);
        ProcessHit(Player.Instance, other.point, other.normal);
    }

    void OnTriggerEnter(Collider other)
    {
        Game.Log("OnTriggerEnter: " + other.gameObject.name);
        //Try and find an actor class...
        Actor hit = other.gameObject.GetComponent<Actor>();
        if (hit == null) hit = other.gameObject.GetComponentInParent<Actor>();

        if(hit != null && hit.isPlayer)
        {
            ProcessHit(hit, this.transform.position, CameraController.Instance.cam.transform.forward * -1f);
        }
    }

    private void ProcessHit(Actor hit, Vector3 contactPoint, Vector3 contactNormal)
    {
        if (hit == attacker) return; //Ignore self

        //Is kill allowed?
        bool kill = false;

        if (enableKill == hit)
        {
            kill = true;
        }

        //Chance to hit other based on combat skill
        if (hit != target)
        {
            if (Toolbox.Instance.Rand(0f, 1f) <= attacker.combatSkill) return;
        }

        //We've hit somebody!
        //Deal damage
        Game.Log("Dmg Collider: " + attacker.name + " dealt " + damage + " to " + hit.name + " (kill allowed: " + kill + ")");

        //A rough but hopefully good enough approximation of the hit world position
        //Vector3 approxHitLocation = this.transform.TransformPoint(new Vector3(0, -1.1f, 0));

        //SFX
        if (hit.isPlayer)
        {
            if (weapon.impactEventPlayer != null) AudioController.Instance.PlayWorldOneShot(weapon.impactEventPlayer, hit, hit.currentNode, contactPoint);
        }
        else
        {
            if (weapon.impactEventBody != null) AudioController.Instance.PlayWorldOneShot(weapon.impactEventBody, hit, hit.currentNode, contactPoint);
        }

        //Spawn wound
        if (weapon.entryWound != null)
        {
            //Get closest model vert...
            Citizen citi = hit as Citizen;

            if (citi != null && !citi.isPlayer)
            {
                citi.CreateWoundClosestToPoint(contactPoint, contactNormal, weapon.entryWound);
            }
        }

        //Apply poision
        if (weapon != null)
        {
            if (weapon.applyPoison > 0f)
            {
                Citizen citi = hit as Citizen;

                if (citi != null)
                {
                    citi.AddPoisoned(weapon.applyPoison, attacker as Human);
                }
            }
        }

        hit.RecieveDamage(damage, attacker, contactPoint, -this.transform.up, weapon.forwardSpatter, weapon.backSpatter, SpatterSimulation.EraseMode.useDespawnTime, enableKill: kill); //Use this downwards as the direction of impact

        if (hit.currentHealth > 0f)
        {
            if (hit.ai != null)
            {
                hit.ai.AITick(true); //Force priority update
            }
        }

        //Remove this
        Destroy(this.gameObject);
    }
}
