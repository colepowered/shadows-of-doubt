﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NaughtyAttributes;

public class GameMessageController : MonoBehaviour
{
    public RectTransform rect;
    public string displayMessage;
    public TextMeshProUGUI messageText;
    public Image img;
    public JuiceController juice;
    public RectTransform lensFlare;

    public bool isKeyMergeMessage = false;
    public ProgressBarController keyMergeProgress;

    public bool isSocialCreditMessage = false;
    public int originalCredit = 0;

    public Sprite checkedSprite;

    public Image puzzleBG;
    public Image namePiece;
    public Image photoPiece;
    public Image voicePiece;
    public Image fingerprintPiece;

    public TextMeshProUGUI socialCreditLevelText;

    public enum PingOnComplete { none, lockpicks, money};
    public PingOnComplete ping = PingOnComplete.none;

    public float progress = 0f;
    public float delayProgress = 0f;
    public float fadeProgress = 0f;
    public float revealProgress = 0f;
    public float keyTieProgress = 0f;
    public float socCreditProgress = 0f;

    private int tiedKeysValue = 0;

    [ReorderableList]
    public List<CanvasRenderer> renderers = new List<CanvasRenderer>();

    public RectTransform moveToTargetOnDestroy;

    private void OnEnable()
    {
        messageText.text = string.Empty;
        lensFlare.gameObject.SetActive(false);
        rect.sizeDelta = new Vector2(0, rect.sizeDelta.y);
    }

    public void Setup(Sprite graphic, string message, RectTransform moveToTarget, bool colourOverride = false, Color col = new Color(), PingOnComplete newPing = PingOnComplete.none, Evidence keyTieEvidence = null, List<Evidence.DataKey> newTiedKeys = null, int value = 0)
    {
        //Setup
        displayMessage = message;
        if(img != null) img.sprite = graphic;
        messageText.text = string.Empty;
        moveToTargetOnDestroy = moveToTarget;
        ping = newPing;

        if (colourOverride)
        {
            if(img != null) img.color = col;
        }

        juice.Flash(2, colourOverride, col);

        if (isKeyMergeMessage)
        {
            keyMergeProgress.barMax = keyTieEvidence.preset.GetValidProfileKeys().Count;
            keyMergeProgress.SetValue(value);

            List<Evidence.DataKey> keysUpdate = keyTieEvidence.GetTiedKeys(newTiedKeys);
            tiedKeysValue = keyTieEvidence.preset.GetProfileKeyCount(keysUpdate);

            List<Evidence.DataKey> uniqueKeys = keysUpdate.FindAll(item => keyTieEvidence.preset.IsKeyUnique(item));
            string uK = string.Empty;

            if (namePiece != null) namePiece.gameObject.SetActive(false);
            if (photoPiece != null) photoPiece.gameObject.SetActive(false);
            if (voicePiece != null) voicePiece.gameObject.SetActive(false);
            if (fingerprintPiece != null) fingerprintPiece.gameObject.SetActive(false);

            if (uniqueKeys.Count > 0)
            {
                bool previous = false;
                uK = "\n" + Strings.Get("ui.gamemessage", "Linked to") + "\n";

                foreach (Evidence.DataKey dk in uniqueKeys)
                {
                    if (previous) uK += ", ";
                    uK += Strings.Get("evidence.generic", dk.ToString(), Strings.Casing.firstLetterCaptial);
                    previous = true;

                    if(dk == Evidence.DataKey.name)
                    {
                        if (namePiece != null) namePiece.gameObject.SetActive(true);
                    }
                    else if(dk == Evidence.DataKey.photo)
                    {
                        if (photoPiece != null) photoPiece.gameObject.SetActive(true);
                    }
                    else if(dk == Evidence.DataKey.voice)
                    {
                        if (voicePiece != null) voicePiece.gameObject.SetActive(true);
                    }
                    else if(dk == Evidence.DataKey.fingerprints)
                    {
                        if (fingerprintPiece != null) fingerprintPiece.gameObject.SetActive(true);
                    }
                }
            }

            displayMessage = keyTieEvidence.GetNameForDataKey(keysUpdate) +": " + Strings.Get("ui.gamemessage", "New Information") + " (" + tiedKeysValue + "/" + keyTieEvidence.preset.GetValidProfileKeys().Count + ")" + uK;
        }
        else if(isSocialCreditMessage)
        {
            originalCredit = value;
            SocialScoreVisualUpdate(value);
        }

        //else
        //{
        //    Toolbox.Instance.SetRectSize(messageText.rectTransform, 12, 12, 12, 12);
        //    if(puzzleBG != null) puzzleBG.gameObject.SetActive(false);
        //    if (puzzleBG != null) namePiece.gameObject.SetActive(false);
        //    if (puzzleBG != null) photoPiece.gameObject.SetActive(false);
        //    if (puzzleBG != null) voicePiece.gameObject.SetActive(false);
        //}
    }

    public void SocialScoreVisualUpdate(int points)
    {
        int currentLevel = GameplayController.Instance.GetSocialCreditLevel(points);
        int nextLevel = currentLevel + 1;
        socialCreditLevelText.text = currentLevel.ToString();

        int currentLevelThreshold = GameplayController.Instance.GetSocialCreditThresholdForLevel(currentLevel);
        int nextLevelThreshold  = GameplayController.Instance.GetSocialCreditThresholdForLevel(nextLevel);

        keyMergeProgress.barMin = 0;
        keyMergeProgress.barMax = nextLevelThreshold - currentLevelThreshold;
        int dispPoints = points - currentLevelThreshold;
        keyMergeProgress.SetValue(dispPoints); //Set current to previous value

        displayMessage = Strings.Get("ui.gamemessage", "Social Credit Score") + ": " + dispPoints + "/" + keyMergeProgress.barMax;

        Game.Log("display for points: " + points + ", current level: " + currentLevel + " (" + currentLevelThreshold + ")" + ", next level: " + nextLevel + " (" + nextLevelThreshold + "), original credit: " + originalCredit + " applied: " + dispPoints + "/" + keyMergeProgress.barMax);
    } 

    // Update is called once per frame
    void Update ()
    {
        if(revealProgress < 1f)
        {
            revealProgress += Time.deltaTime / 0.18f;
            revealProgress = Mathf.Clamp01(revealProgress);

            foreach (CanvasRenderer rend in renderers)
            {
                if(rend != null) rend.SetAlpha(revealProgress);
            }
        }

        //Animate
        if (progress < displayMessage.Length)
        {
            progress += Time.deltaTime * InterfaceControls.Instance.gameMessageTextRevealSpeed;
            messageText.text = displayMessage.Substring(0, Mathf.Min(Mathf.RoundToInt(progress), displayMessage.Length));

            float keyExtra = 0f;
            if (isKeyMergeMessage) keyExtra = 100f;
            else if (isSocialCreditMessage) keyExtra = 100f;
            rect.sizeDelta = new Vector2(Mathf.Max(messageText.preferredWidth + 66f + keyExtra, 66), rect.sizeDelta.y);

            lensFlare.gameObject.SetActive(true);
        }
        else
        {
            delayProgress += Time.deltaTime;

            float extraTime = 0f;
            if (isKeyMergeMessage) extraTime = 0.1f;
            else if (isSocialCreditMessage) extraTime = 0.4f;

            if (delayProgress >= InterfaceControls.Instance.gameMessageDestroyDelay + extraTime)
            {
                fadeProgress += Time.deltaTime / 0.25f; //Time to remove
                fadeProgress = Mathf.Clamp01(fadeProgress);

                if(moveToTargetOnDestroy != null)
                {
                    rect.position = Vector3.Lerp(rect.position, moveToTargetOnDestroy.position, fadeProgress);
                }

                foreach(CanvasRenderer rend in renderers)
                {
                    rend.SetAlpha(Mathf.Clamp01(1f - fadeProgress * 1.25f));
                }

                if(fadeProgress > 0.85f)
                {
                    if(ping == PingOnComplete.money)
                    {
                        InterfaceController.Instance.PingMoney();
                        ping = PingOnComplete.none;
                    }
                    else if(ping == PingOnComplete.lockpicks)
                    {
                        InterfaceController.Instance.PingLockpicks();
                        ping = PingOnComplete.none;
                    }

                    if (fadeProgress >= 1f)
                    {
                        Destroy(this.gameObject);
                    }
                }
            }
        }

        if (isKeyMergeMessage)
        {
            if(revealProgress >= 1f)
            {
                if (keyTieProgress < 1f)
                {
                    keyTieProgress += Time.deltaTime / 1f;
                    keyTieProgress = Mathf.Clamp01(keyTieProgress);
                }
            }

            keyMergeProgress.SetValue(tiedKeysValue * keyTieProgress);
        }

        if (isSocialCreditMessage)
        {
            if(revealProgress >= 1f)
            {
                if (socCreditProgress < 1f)
                {
                    socCreditProgress += Time.deltaTime / 1f;
                    socCreditProgress = Mathf.Clamp01(socCreditProgress);
                    SocialScoreVisualUpdate(originalCredit + (Mathf.CeilToInt((GameplayController.Instance.socialCredit - originalCredit) * socCreditProgress)));
                }
                else
                {
                    SocialScoreVisualUpdate(GameplayController.Instance.socialCredit);
                }
            }
        }
    }
}
