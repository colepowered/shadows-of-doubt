﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System;
using System.Security.AccessControl;

public class TelephoneController : MonoBehaviour
{
    [Header("Telecoms")]
    public List<PhoneCall> activeCalls = new List<PhoneCall>();

    private float gameTimeLastLoop = 0f;

    //private float phoneAnswerTimer = 0f;

    public Dictionary<Interactable, FMOD.Studio.EventInstance> engagedEvents = new Dictionary<Interactable, FMOD.Studio.EventInstance>();

    public enum CallState { dialing, denied, ringing, started, ended};
    public enum CallType { dds, audioEvent, player, fakeOutbound };

    public Dictionary<int, CallSource> fakeTelephoneDictionary = new Dictionary<int, CallSource>();

    [System.Serializable]
    public class CallSource
    {
        public CallType callType;
        public string dds;
        public string audio;
        public string dialog;
        public int job = -1; //Override data source

        [NonSerialized]
        public AudioEvent audioEvent;
        [NonSerialized]
        public DialogPreset dialogGreeting;

        public CallSource(CallType newType, string newDDS)
        {
            callType = newType;
            dds = newDDS;
        }

        public CallSource(CallType newType, AudioEvent newAudioEvent)
        {
            callType = newType;
            audio = newAudioEvent.name;
            audioEvent = newAudioEvent;
        }

        public CallSource(CallType newType, DialogPreset newGreeting)
        {
            callType = newType;
            dialogGreeting = newGreeting;
            if (dialogGreeting == null) dialogGreeting = CitizenControls.Instance.telephoneGreeting;
            dialog = dialogGreeting.name;
        }

        public CallSource(CallType newType, DialogPreset newGreeting, SideJob newJob)
        {
            callType = newType;
            dialogGreeting = newGreeting;
            if (dialogGreeting == null) dialogGreeting = CitizenControls.Instance.telephoneGreeting;
            dialog = dialogGreeting.name;
            job = newJob.jobID;
        }
    }

    [System.Serializable]
    public class PhoneCall
    {
        public int from;
        public int to = -1;
        public float time;
        public float duration;
        public int caller = -2;
        public int receiver = -2;
        public int intendedReceiver = -2;
        public CallSource source;
        public CallState previousSate = CallState.denied;
        public CallState state = CallState.dialing;
        public float ringTime = 0.1f;
        public bool specRecevier = false; //If true this will hang up if it's the wrong reciever

        public float dialingTimer = 0f;
        public float ringDelay = 0f;

        //Non serialized
        [NonSerialized]
        public Telephone fromNS;
        [NonSerialized]
        public Telephone toNS;
        [NonSerialized]
        public Human callerNS;
        [NonSerialized]
        public Human recevierNS;
        [NonSerialized]
        public Human intendedReceiverNS;
        [NonSerialized]
        public AudioController.LoopingSoundInfo lineRingingLoop;
        [NonSerialized]
        public AudioController.LoopingSoundInfo lineActiveLoopCaller;
        [NonSerialized]
        public AudioController.LoopingSoundInfo lineActiveLoopReceiver;
        [NonSerialized]
        public FMOD.Studio.EventInstance callAudioInstance;
        [System.NonSerialized]
        public FMOD.Studio.EventInstance connecting;
        [System.NonSerialized]
        public FMOD.Studio.EventInstance hangUpCaller;
        [System.NonSerialized]
        public FMOD.Studio.EventInstance hangUpReciever;

        public PhoneCall(Telephone newFrom, Telephone newTo, float newTime, Human newCaller, Human newIntendedReceiver, CallSource newCallSource, float newMaxRingTime = 0.1f, bool newSpecificRecevier = false)
        {
            if(newFrom != null) from = newFrom.number;
            if(newTo != null) to = newTo.number;
            time = newTime;
            if(newCaller != null) caller = newCaller.humanID;
            if(newIntendedReceiver != null) intendedReceiver = newIntendedReceiver.humanID;
            source = newCallSource;
            ringTime = newMaxRingTime;
            specRecevier = newSpecificRecevier;

            fromNS = newFrom;
            toNS = newTo;
            callerNS = newCaller;
            intendedReceiverNS = newIntendedReceiver;
        }

        public void SetCallState(CallState newState)
        {
            if(newState != state)
            {
                previousSate = state;
                state = newState;

                //Game.Log("Gameplay: " + SessionData.Instance.decimalClock + ": Call from " + fromNS.name + " to " + toNS.name + " state: " + state + " (Occupants: " + toNS.currentOccupants.Count + ")");

                //Start/stop ringing
                if (state == CallState.ringing)
                {
                    if(toNS != null)
                    {
                        toNS.interactable.SetSwitchState(true, null);
                    }

                    //Play line loop for caller
                    if (lineRingingLoop == null && callerNS != null)
                    {
                        if(callerNS.isPlayer)
                        {
                            lineRingingLoop = AudioController.Instance.PlayWorldLooping(AudioControls.Instance.phoneLineRing, callerNS, callerNS.interactable);
                        }
                    }
                }
                else
                {
                    //Stop line loop
                    if (lineRingingLoop != null)
                    {
                        AudioController.Instance.StopSound(lineRingingLoop, AudioController.StopType.fade, "Phone stop ringing");
                        lineRingingLoop = null;
                    }

                    if(toNS != null)
                    {
                        toNS.interactable.SetSwitchState(false, null);
                    }

                    if(state == CallState.started)
                    {
                        //Set the active call
                        if (recevierNS != null && recevierNS.isPlayer)
                        {
                            Game.Log("Set player active call: " + source.callType);
                            Player.Instance.activeCall = this;

                            if(TelephoneController.Instance.OnPlayerCall != null)
                            {
                                TelephoneController.Instance.OnPlayerCall();
                            }
                        }

                        //Play line loop
                        if (lineActiveLoopCaller == null && callerNS != null)
                        {
                            if(callerNS.isPlayer)
                            {
                                lineActiveLoopCaller = AudioController.Instance.PlayWorldLooping(AudioControls.Instance.phoneLineActive, callerNS, callerNS.interactable);
                            }
                        }

                        if (lineActiveLoopReceiver == null && recevierNS != null)
                        {
                            if(recevierNS.isPlayer)
                            {
                                lineActiveLoopReceiver = AudioController.Instance.PlayWorldLooping(AudioControls.Instance.phoneLineActive, recevierNS, recevierNS.interactable);
                            }
                        }

                        if(callerNS != null && recevierNS != null)
                        {
                            callerNS.UpdateLastSighting(recevierNS, true);
                            recevierNS.UpdateLastSighting(callerNS, true);
                        }

                        //Check receiver...
                        if (specRecevier)
                        {
                            if(intendedReceiverNS != null && intendedReceiverNS != recevierNS)
                            {
                                Game.Log("Not the right intended reciever, ending call...");
                                EndCall();
                                return;
                            }
                        }

                        //Start convo...
                        if(source.callType == CallType.player || source.callType == CallType.fakeOutbound)
                        {
                            if (source.dialogGreeting == null)
                            {
                                Toolbox.Instance.LoadDataFromResources<DialogPreset>(source.dialog, out source.dialogGreeting);
                            }

                            if (Player.Instance.phoneInteractable == null) Game.Log("Player phone interactable is null!");

                            if(recevierNS != null)
                            {
                                InteractionController.Instance.SetDialog(true, recevierNS.interactable, true, Player.Instance.phoneInteractable);
                            }
                            else
                            {
                                InteractionController.Instance.SetDialog(true, Player.Instance.interactable, true, Player.Instance.phoneInteractable);
                            }

                            if(source.job < 0)
                            {
                                Interactable saysToInter = Player.Instance.phoneInteractable;
                                if (recevierNS != null) saysToInter = recevierNS.interactable;

                                Game.Log("Player phone executing source greeting: " + source.dialogGreeting);

                                try
                                {
                                    DialogController.Instance.ExecuteDialog(new EvidenceWitness.DialogOption { preset = source.dialogGreeting }, saysToInter, Player.Instance.currentNode, Player.Instance);
                                }
                                catch
                                {
                                    Game.LogError("Unable to execute telephone greeting");
                                    EndCall();
                                    return;
                                }
                            }
                            else
                            {
                                SideJob job = null;
                                Interactable saysToInter = Player.Instance.interactable;
                                if (recevierNS != null) saysToInter = recevierNS.interactable;

                                if(source == null)
                                {
                                    Game.LogError("Call source is null!");
                                }
                                else if(source.dialogGreeting == null)
                                {
                                    Game.LogError("Call source dialog greeting is null!");
                                }
                                else
                                {
                                    //try
                                    //{
                                        if(!SideJobController.Instance.allJobsDictionary.TryGetValue(source.job, out job))
                                        {
                                            DialogController.Instance.ExecuteDialog(new EvidenceWitness.DialogOption { preset = source.dialogGreeting }, saysToInter, Player.Instance.currentNode, Player.Instance);
                                        }
                                        else
                                        {
                                            Game.Log("Jobs: Executing dialog from job " + job.preset + ": " + source.dialogGreeting);
                                            DialogController.Instance.ExecuteDialog(new EvidenceWitness.DialogOption { preset = source.dialogGreeting, jobRef = job }, saysToInter, Player.Instance.currentNode, Player.Instance);
                                        }
                                    //}
                                    //catch
                                    //{
                                    //    Game.LogError("Unable to execute dialog from job");
                                    //    EndCall();
                                    //    return;
                                    //}
                                }
                            }
                        }
                        else if(source.callType == CallType.dds)
                        {
                            List<Human> p = new List<Human>();
                            p.Add(recevierNS);

                            //Execute
                            callerNS.ExecuteConversationTree(Toolbox.Instance.allDDSTrees[source.dds], p);
                        }
                        else if(source.callType == CallType.audioEvent)
                        {
                            //Find audio event in resources...
                            if(source.audioEvent == null)
                            {
                                Toolbox.Instance.LoadDataFromResources<AudioEvent>(source.audio, out source.audioEvent);
                            }

                            if (source.audioEvent != null)
                            {
                                if(recevierNS != null)
                                {
                                    float lineVol = 1f;
                                    if (!recevierNS.isPlayer) lineVol = 0.4f;
                                    callAudioInstance = AudioController.Instance.PlayWorldOneShot(source.audioEvent, recevierNS, recevierNS.currentNode, recevierNS.lookAtThisTransform.position, volumeOverride: lineVol);
                                }
                            }
                            else Game.LogError("Cannot find audio event for telephone call: " + source.audio);
                        }
                    }
                    else if(state == CallState.ended)
                    {
                        //Game.Log("Call ended, callerNS = " + callerNS + " fromNS = " + fromNS);

                        //Stop line active loop
                        if (lineActiveLoopCaller != null)
                        {
                            AudioController.Instance.StopSound(lineActiveLoopCaller, AudioController.StopType.fade, "Phone call ended");
                            lineActiveLoopCaller = null;
                        }

                        if (lineActiveLoopReceiver != null)
                        {
                            AudioController.Instance.StopSound(lineActiveLoopReceiver, AudioController.StopType.fade, "Phone call ended");
                            lineActiveLoopReceiver = null;
                        }

                        //Play hand up sound
                        if (previousSate == CallState.started)
                        {
                            if (callerNS != null)
                            {
                                //If this is the player, restart the dial tone
                                if (callerNS == Player.Instance && fromNS.activeReceiver == Player.Instance)
                                {
                                    hangUpCaller = AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.hangUp, callerNS, callerNS.currentNode, callerNS.lookAtThisTransform.position);

                                    //Restart dial tone if picked up but not at dial state
                                    if (Player.Instance.phoneInteractable.sw1 && !Player.Instance.phoneInteractable.sw2)
                                    {
                                        fromNS.dialTone = AudioController.Instance.PlayWorldLooping(AudioControls.Instance.dialTone, Player.Instance, Player.Instance.interactable);
                                    }
                                }
                            }

                            if (recevierNS != null)
                            {
                                //If this is the player, restart the dial tone
                                if (recevierNS == Player.Instance && toNS.activeReceiver == Player.Instance)
                                {
                                    hangUpReciever = AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.hangUp, recevierNS, recevierNS.currentNode, recevierNS.lookAtThisTransform.position);

                                    //Restart dial tone
                                    if (Player.Instance.phoneInteractable.sw1 && !Player.Instance.phoneInteractable.sw2)
                                    {
                                        toNS.dialTone = AudioController.Instance.PlayWorldLooping(AudioControls.Instance.dialTone, Player.Instance, Player.Instance.interactable);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public void EndCall()
        {
            if(fromNS != null && toNS != null) Game.Log("Phone: End telephone call from " + fromNS.numberString + " to " + toNS.numberString);

            //Clear remote speech queue
            //if(InteractionController.Instance.remoteOverride != null)
            //{
            //    InteractionController.Instance.remoteOverride.speechController.speechQueue.Clear();
            //}

            //if(recevierNS != null)
            //{
            //    if(recevierNS.ai != null)
            //    {
            //        //Cancel answering goals
            //        NewAIAction answerAction = recevierNS.ai.currentAction;

            //        if (answerAction != null)
            //        {
            //            if(answerAction.preset == RoutineControls.Instance.answerTelephone)
            //            {
            //                answerAction.Complete();
            //            }
            //        }
            //    }
            //}

            //Stop audio
            AudioController.Instance.StopSound(connecting, AudioController.StopType.immediate);
            //AudioController.Instance.StopSound(hangUpReciever, AudioController.StopType.immediate);

            if (source.callType == CallType.audioEvent)
            {
                callAudioInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
                callAudioInstance.release();
            }

            //Stop any answer call actions
            if(toNS != null)
            {
                foreach (Human h in toNS.location.currentOccupants)
                {
                    if (h.ai == null) continue;

                    if (h.ai.currentGoal != null)
                    {
                        List<NewAIAction> answerActions = h.ai.currentGoal.actions.FindAll(item => item.preset == RoutineControls.Instance.answerTelephone);

                        foreach (NewAIAction act in answerActions)
                        {
                            act.Complete();
                        }
                    }
                }
            }

            if (callerNS != null)
            {
                if(callerNS.ai != null)
                {
                    NewAIAction callAction = callerNS.ai.currentAction;

                    if(callAction != null)
                    {
                        if (callAction.preset == RoutineControls.Instance.makeCall)
                        {
                            callAction.Complete();
                        }
                    }
                }
            }

            if (callerNS == Player.Instance || recevierNS == Player.Instance)
            {
                //Game.Log("Caller or recveiver is player...");

                if (Player.Instance.interactingWith != null)
                {
                    Actor otherActor = Player.Instance.interactingWith.objectRef as Actor;
                    if (otherActor != null) otherActor.SetInteracting(null);
                }

                Player.Instance.SetInteracting(null);

                //Go back to pick up phone state...
                if(Player.Instance.phoneInteractable != null)
                {
                    Player.Instance.phoneInteractable.SetCustomState1(true, Player.Instance, true);
                    Player.Instance.phoneInteractable.SetCustomState2(false, Player.Instance, true);
                    Player.Instance.phoneInteractable.SetCustomState3(false, null);
                }

                //Load dialog options: Only do this if custom state 2 is active...
                InteractionController.Instance.SetDialog(false, null);
                //if (Player.Instance.phoneInteractable.sw2)
                //{
                //    InteractionController.Instance.SetDialog(true, Player.Instance.phoneInteractable);
                //}
                //else
                //{
                //    InteractionController.Instance.SetDialog(false, null);
                //}
            }

            TelephoneController.Instance.RemoveActiveCall(this);
            SetCallState(CallState.ended);
        }

        public void SetupNonSerializedData()
        {
            if(!CityData.Instance.phoneDictionary.TryGetValue(from, out fromNS))
            {
                Game.LogError("Unable to get 'from' telephone: " + from);
            }

            if(!CityData.Instance.phoneDictionary.TryGetValue(to, out toNS))
            {
                Game.LogError("Unable to get 'to' telephone: " + to);
            }

            CityData.Instance.GetHuman(caller, out callerNS);
            CityData.Instance.GetHuman(receiver, out recevierNS);
            CityData.Instance.GetHuman(intendedReceiver, out intendedReceiverNS);
        }
    }

    [Header("Debug")]
    public int debugNumber;

    //Events
    public delegate void PlayerCall();
    public event PlayerCall OnPlayerCall;

    //Singleton pattern
    private static TelephoneController _instance;
    public static TelephoneController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    //Create telephone call (using number)
    public PhoneCall CreateNewCall(int from, int to, Human caller, Human intendedReceiver, CallSource callSource, float maxRingTime = 0.1f, bool specificRecevier = false)
    {
        Telephone fromPhone = null;

        if(CityData.Instance.phoneDictionary.TryGetValue(from, out fromPhone))
        {
            Telephone toPhone = null;

            if (CityData.Instance.phoneDictionary.TryGetValue(to, out toPhone))
            {
                return CreateNewCall(fromPhone, toPhone, caller, intendedReceiver, callSource, maxRingTime, specificRecevier);
            }
        }

        return null;
    }

    //Create telephone call
    public PhoneCall CreateNewCall(Telephone from, Telephone to, Human caller, Human intendedReceiver, CallSource callSource, float maxRingTime = 0.1f, bool specificRecevier = false)
    {
        if(from == to)
        {
            Game.LogError("Cannot make call to the same phone!");
            return null;
        }
        else if(caller == intendedReceiver && caller != null)
        {
            Game.LogError("Cannot make call to the same person!");
            return null;
        }

        PhoneCall newCall = new PhoneCall(from, to, SessionData.Instance.gameTime, caller, intendedReceiver, callSource, maxRingTime, specificRecevier);

        //Store call log in building...
        if(from != null)
        {
            if(from.location.building != null)
            {
                if(from.location.building.callLog.Count >= GameplayControls.Instance.buildingCallLogMax)
                {
                    //Remove earliest call log
                    from.location.building.callLog.RemoveAt(0);
                }

                from.location.building.callLog.Add(newCall);
            }

            //Stop dial tone
            if (from.dialTone != null)
            {
                Game.Log("Stop dial tone");
                AudioController.Instance.StopSound(from.dialTone, AudioController.StopType.immediate, "call while picked up"); //Stop dial tone
                from.dialTone = null;
            }
        }

        if(to != null)
        {
            if(to.location.building != null)
            {
                if (to.location.building.callLog.Count >= GameplayControls.Instance.buildingCallLogMax)
                {
                    //Remove earliest call log
                    to.location.building.callLog.RemoveAt(0);
                }

                to.location.building.callLog.Add(newCall);
            }
        }

        //Add to active calls...
        AddActiveCall(newCall);

        if(newCall.callerNS != null && newCall.callerNS.isPlayer)
        {
            newCall.connecting = AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.phoneConnect, newCall.callerNS, newCall.callerNS.currentNode, newCall.callerNS.lookAtThisTransform.position);
        }

        return newCall;
    }

    public void OnPlayerCalls()
    {
        //Fire event
        if(OnPlayerCall != null)
        {
            OnPlayerCall();
        }
    }

    public void AddFakeNumber(int number, CallSource source)
    {
        if(!fakeTelephoneDictionary.ContainsKey(number))
        {
            fakeTelephoneDictionary.Add(number, source);
        }
    }

    public void RemoveFakeNumber(int number)
    {
        if(fakeTelephoneDictionary.ContainsKey(number))
        {
            fakeTelephoneDictionary.Remove(number);
        }
    }

    //Add active phone
    public void AddActiveCall(PhoneCall newCall)
    {
        if (!activeCalls.Contains(newCall))
        {
            activeCalls.Add(newCall);
            gameTimeLastLoop = SessionData.Instance.gameTime;
            this.enabled = true;

            if (newCall.callerNS != null && newCall.callerNS.isPlayer)
            {
                Game.Log("Set player active call");
                Player.Instance.activeCall = newCall;
            }

            //A reciever won't be triggered here as this is called when phone rings...
        }
    }

    //Remove active phone
    public void RemoveActiveCall(PhoneCall newCall)
    {
        activeCalls.Remove(newCall);

        if (newCall.callerNS != null && newCall.callerNS.isPlayer)
        {
            Game.Log("Phone: Remove player active call");
            Player.Instance.activeCall = null;
        }
        else if(newCall.recevierNS != null && newCall.recevierNS.isPlayer)
        {
            Game.Log("Phone: Remove player active call");
            Player.Instance.activeCall = null;
        }
    }

    private void Update()
    {
        if(SessionData.Instance.play)
        {
            float timePassedThisLoop = SessionData.Instance.gameTime - gameTimeLastLoop;

            //Telephones
            if (activeCalls.Count > 0)
            {
                //phoneAnswerTimer += Time.deltaTime * Mathf.Max(activeCalls.Count * 2f, 1); //Cycle through more if we have more calls...

                for (int i = 0; i < activeCalls.Count; i++)
                {
                    PhoneCall call = activeCalls[i];

                    //progress from dial to calling
                    if(call.state == CallState.dialing)
                    {
                        call.dialingTimer += timePassedThisLoop;

                        if (call.dialingTimer > 0.0037f)
                        {
                            //Is the phone at the other end engaged?
                            if (call.toNS != null && call.toNS.activeCall.Count > 0)
                            {
                                if(call.callerNS != null && call.callerNS.isPlayer)
                                {
                                    InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "telephone engaged"), InterfaceControls.Icon.citizen);
                                }

                                Game.Log("Gameplay: Call " + call.fromNS.numberString + " to " + call.toNS.numberString + " is engaged...");
                                call.EndCall();
                                
                                //Play engaged sound...
                                if(call.fromNS != null && call.callerNS != null && call.callerNS.isPlayer)
                                {
                                    call.fromNS.engaged = AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.phoneLineEngaged, call.callerNS, call.callerNS.currentNode, call.callerNS.lookAtThisTransform.position);
                                }
                            }
                            else
                            {
                                if(call.toNS != null) call.toNS.SetActiveCall(call);
                                if(call.fromNS != null) call.fromNS.SetActiveCall(call);
                            }
                        }
                    }
                    //Monitor ring time
                    else if(call.state == CallState.ringing)
                    {
                        if (call.ringDelay > 1f)
                        {
                            if (call.toNS != null)
                            {
                                //If intended receiver is here, use them...
                                bool chosen = false;
                                List<Human> eligableHumans = new List<Human>();

                                foreach (Human h in call.toNS.location.currentOccupants)
                                {
                                    if (h.isDead) continue;
                                    if (h.ai == null) continue;
                                    if (!h.canListen) continue;
                                    if (call.toNS.location.thisAsAddress != null && call.toNS.location.thisAsAddress.company != null && call.toNS.location.thisAsAddress.company.publicFacing) continue; //Don't answer calls to public facing companies

                                    if (h.locationsOfAuthority.Contains(call.toNS.location) || (h.isEnforcer && h.isOnDuty))
                                    {
                                        //If intended receiver is here, use them...
                                        if (h == call.intendedReceiverNS)
                                        {
                                            h.ai.AnswerPhone(call.toNS);
                                            chosen = true;
                                            break;
                                        }
                                        //If one is a job poster than prioritise...
                                        else if (CasePanelController.Instance.activeCase != null && CasePanelController.Instance.activeCase.job != null && CasePanelController.Instance.activeCase.job.poster == h)
                                        {
                                            h.ai.AnswerPhone(call.toNS);
                                            chosen = true;
                                            break;
                                        }
                                        else
                                        {
                                            eligableHumans.Add(h);
                                        }
                                    }
                                }

                                if (!chosen)
                                {
                                    foreach (Human h in eligableHumans)
                                    {
                                        h.ai.AnswerPhone(call.toNS);
                                    }
                                }

                                //phoneAnswerTimer = 0f;
                            }
                            //To nobody...
                            else
                            {
                                call.SetCallState(CallState.started);
                                //phoneAnswerTimer = 0f;
                            }
                        }
                        else call.ringDelay += Time.deltaTime;

                        //Give up after x time
                        if (/*call.source.callType != CallType.player &&*/ call.source.callType != CallType.fakeOutbound)
                        {
                            call.ringTime -= timePassedThisLoop;

                            //Give up
                            if (call.ringTime <= 0f)
                            {
                                call.EndCall();
                            }
                        }
                    }
                    else if(call.state == CallState.started)
                    {
                        //If audio event, end after finished...
                        if(call.source.callType == CallType.audioEvent)
                        {
                            FMOD.Studio.PLAYBACK_STATE state = FMOD.Studio.PLAYBACK_STATE.STOPPED;
                            call.callAudioInstance.getPlaybackState(out state);

                            if(state == FMOD.Studio.PLAYBACK_STATE.STOPPED)
                            {
                                call.EndCall();
                            }
                        }

                        //if (phoneAnswerTimer > 1f)
                        //{
                        //    foreach (Human h in call.toNS.currentOccupants)
                        //    {
                        //        if (h.isDead) continue;
                        //        if (h.ai == null) continue;
                        //        if (!h.canListen) continue;
                        //        if (h == call.recevierNS) continue;

                        //        //Cancel answering action
                        //        if(h.ai.currentGoal != null)
                        //        {
                        //            NewAIAction answerAction = h.ai.currentGoal.actions.Find(item => item.preset == RoutineControls.Instance.answerTelephone);

                        //            if (answerAction != null)
                        //            {
                        //                if (answerAction.preset == RoutineControls.Instance.answerTelephone)
                        //                {
                        //                    answerAction.Complete();
                        //                }
                        //            }
                        //        }
                        //    }

                        //    phoneAnswerTimer = 0f;
                        //}
                    }
                }

                //if (phoneAnswerTimer > 1f) phoneAnswerTimer = 0f;
            }
            else
            {
                //phoneAnswerTimer = 0;
                this.enabled = false;
            }

            gameTimeLastLoop = SessionData.Instance.gameTime;
        }
    }

    [Button]
    public void FindTelephoneByNumber()
    {
        Telephone toPhone = null;

        if (CityData.Instance.phoneDictionary.TryGetValue(debugNumber, out toPhone))
        {
            Game.Log("Found telephone at " + toPhone.interactable.GetWorldPosition() + " (" + toPhone.interactable.node.room.GetName() + ", " + toPhone.interactable.node.gameLocation.name + ")");
        }
        else Game.Log("Unable to find telephone of " + debugNumber);
    }

    [Button]
    public void FindTelephonesAtPlayerLocation()
    {
        if(Player.Instance.currentGameLocation != null)
        {
            foreach(KeyValuePair<int, Telephone> pair in CityData.Instance.phoneDictionary)
            {
                if(pair.Value.interactable != null)
                {
                    if(pair.Value.interactable.node.gameLocation == Player.Instance.currentGameLocation)
                    {
                        Game.Log("Found telephone " + pair.Key + " at " + pair.Value.interactable.GetWorldPosition());
                    }
                }
            }
        }
    }
}
