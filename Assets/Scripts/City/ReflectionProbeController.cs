﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReflectionProbeController : MonoBehaviour
{
    public ReflectionProbe probe;

    public void Setup()
    {
        probe.RenderProbe(null);
    }
}
