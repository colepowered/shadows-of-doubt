﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ComputerOSMultiSelectElement : ComputerOSUIComponent
{
    public ComputerOSMultiSelect multiSelect;
    public RectTransform rect;
    public TextMeshProUGUI elementText;
    public TextMeshProUGUI elementText2;
    public ComputerOSMultiSelect.OSMultiOption option;
    public Image backgroundImage;
    public Color backgroundColourNormal = Color.white;
    public Color backgroundColourSelected = Color.cyan;
    public bool selected = false;


    public void Setup(ComputerOSMultiSelect.OSMultiOption newOpt, ComputerOSMultiSelect newMulti)
    {
        multiSelect = newMulti;
        option = newOpt;

        if(option.salesRecord != null)
        {
            try
            {
                elementText.text = SessionData.Instance.TimeAndDate(option.salesRecord.time, false, true, true) + "\n" + option.salesRecord.GetPunter().GetInitialledName();

                elementText2.text = string.Empty;

                Company comp = CityData.Instance.companyDirectory.Find(item => item.companyID == option.salesRecord.companyID);

                for (int i = 0; i < option.salesRecord.items.Count; i++)
                {
                    string it = option.salesRecord.items[i];
                    InteractablePreset p = Toolbox.Instance.GetInteractablePreset(it);
                    float price = 0;

                    if (comp != null && comp.prices.ContainsKey(p))
                    {
                        price = comp.prices[p];
                    }

                    elementText2.text += "<align=\"left\">" + Strings.Get("evidence.names", p.name) + "  <align=\"right\">" + CityControls.Instance.cityCurrency + Toolbox.Instance.RoundToPlaces(price, 2);
                    elementText2.text += "\n";
                }
            }
            catch
            {
                Game.LogError("Unable to display sales record!");
            }
        }
        else
        {
            string[] listSplit = option.text.Split('\n');
            if (listSplit.Length > 0) elementText.text = listSplit[0];
            if (listSplit.Length > 1) elementText2.text = listSplit[1];
        }
    }

    //Select on click
    public override void OnLeftClick()
    {
        multiSelect.SetSelected(this);

        base.OnLeftClick();
    }
}
