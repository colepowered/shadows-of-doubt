﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class BasicAnimationController : SwitchSyncBehaviour
{
    public bool isSetup = false;
    public InteractableController controller;

    public DoorMovementPreset preset;
    public bool oscillate = false;
    public Transform animatedTransform;
    [ReadOnly]
    [Tooltip("This animator has a warm-up time")]
    public float normalizedSpeed = 0f;
    [ReadOnly]
    public float progress = 0f;
    [ReadOnly]
    public bool inOut = false;

    [Space(7)]
    public Vector3 closedLocalPos = Vector3.zero;
    public Vector3 openLocalPos = Vector3.zero;
    public Vector3 closedLocalEuler = Vector3.zero;
    public Vector3 openLocalEuler = Vector3.zero;
    public Vector3 closedLocalScale = Vector3.one;
    public Vector3 openLocalScale = Vector3.one;

    private void Start()
    {
        if (!isSetup)
        {
            Setup();
        }
    }

    public void Setup()
    {
        //Setup relation positons
        closedLocalPos = animatedTransform.localPosition + preset.closedRelativePos;
        openLocalPos = animatedTransform.localPosition + preset.openRelativePos;
        closedLocalEuler = animatedTransform.localEulerAngles + preset.closedRelativeEuler;
        openLocalEuler = animatedTransform.localEulerAngles + preset.openRelativeEuler;
        closedLocalScale = animatedTransform.localScale + preset.closedRelativeScale;
        openLocalScale = animatedTransform.localScale + preset.openRelativeScale;

        isSetup = true;
    }

    public override void SetOn(bool val)
    {
        base.SetOn(val);

        if(val)
        {
            this.enabled = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(isOn && normalizedSpeed < 1f)
        {
            normalizedSpeed += Time.deltaTime;
        }
        else if(!isOn && normalizedSpeed > 0f)
        {
            normalizedSpeed -= Time.deltaTime;

            //Turn off
            if(normalizedSpeed <= 0f)
            {
                this.enabled = false;
                return;
            }
        }

        if (oscillate)
        {
            if(!inOut)
            {
                progress += Time.deltaTime * preset.doorOpenSpeed * normalizedSpeed;

                if(progress >= 1f)
                {
                    inOut = true;
                }
            }
            else
            {
                progress -= Time.deltaTime * preset.doorOpenSpeed * normalizedSpeed;

                if (progress <= 0f)
                {
                    inOut = false;
                }
            }
        }
        else
        {
            progress += Time.deltaTime * preset.doorOpenSpeed * normalizedSpeed;
            if (progress >= 1f) progress -= 1f; //Loop
        }

        float lerpValue = preset.animationCurve.Evaluate(progress);

        animatedTransform.localEulerAngles = Vector3.Lerp(closedLocalEuler, openLocalEuler, lerpValue);
        animatedTransform.localPosition = Vector3.Lerp(closedLocalPos, openLocalPos, lerpValue);
        animatedTransform.localScale = Vector3.Lerp(closedLocalScale, openLocalScale, lerpValue);
    }
}
