﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Text;

public class DialogButtonController : ButtonController
{
    public EvidenceWitness.DialogOption option;
    public bool selectable = true;

    public void Setup(EvidenceWitness.DialogOption newPreset)
    {
        base.SetupReferences();
        option = newPreset;

        UpdateButtonText();
    }

    public override void UpdateButtonText()
    {
        if (option == null)
        {
            //Game.LogError("Dialog option missing!");
            return;
        }

        if (option.preset == null)
        {
            //Game.LogError("Dialog option preset missing!");
            return;
        }

        //Setup text
        string costStart = string.Empty;
        string costEnd = string.Empty;

        //Apply cost
        Actor talkingTo = null;
        if (InteractionController.Instance.talkingTo != null) talkingTo = InteractionController.Instance.talkingTo.isActor;

        int cost = option.preset.GetCost(talkingTo, Player.Instance);

        if(option.preset.specialCase == DialogPreset.SpecialCase.medicalCosts)
        {
            cost = Mathf.RoundToInt(option.preset.GetCost(talkingTo, Player.Instance) * (1f - UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.reduceMedicalCosts)));
        }

        if (cost > 0)
        {
            if (GameplayController.Instance.money < cost)
            {
                costStart = "<s>";
                costEnd = "</s>";
            }

            if(option.preset.usePercentageCost)
            {
                int perc = option.preset.cost;
                if (option.preset.specialCase == DialogPreset.SpecialCase.medicalCosts) perc = Mathf.RoundToInt(option.preset.cost * (1f - UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.reduceMedicalCosts)));
                costEnd += "[" + perc + "%]";
            }
        }

        StringBuilder sb = new StringBuilder();

        sb.Append(option.preset.preceedingSyntax);

        if (option.preset.displayAsIllegal)
        {
            sb.Append("<color=#" + InterfaceControls.Instance.interactionControlTextIllegalHex + ">");
        }

        sb.Append(costStart + Strings.GetTextForComponent(option.preset.msgID, Player.Instance, additionalObject: option.jobRef) + costEnd);

        //Add cost
        if (cost > 0)
        {
            sb.Append(" [" + CityControls.Instance.cityCurrency + cost.ToString() + "]");
        }

        text.text = sb.ToString();
    }

    public void SetSelectable(bool val)
    {
        selectable = val;
        button.interactable = selectable;

        if(selectable)
        {
            text.alpha = 1f;
        }
        else
        {
            text.alpha = 0.75f;
        }
    }
}
