﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactPurchased : Fact
{
    public Company.SalesRecord sale;

    //Constructor
    public FactPurchased(FactPreset newPreset, List<Evidence> newFromEvidence, List<Evidence> newToEvidence, List<object> newPassedObjects, List<Evidence.DataKey> overrideFromKeys, List<Evidence.DataKey> overrideToKeys, bool isCustomFact) : base(newPreset, newFromEvidence, newToEvidence, newPassedObjects, overrideFromKeys, overrideToKeys, isCustomFact)
    {
        sale = newPassedObjects[0] as Company.SalesRecord; //Sales record is passed at #0
    }

    public override string GenerateNameSuffix()
    {
        if (sale == null) return string.Empty;
        return " " + Strings.Get("evidence.generic", "at") + " " + SessionData.Instance.ShortDateString(sale.time, false) + " " + SessionData.Instance.GameTimeToClock24String(sale.time, false);
    }
}
