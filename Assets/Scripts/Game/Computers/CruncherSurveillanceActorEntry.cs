﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CruncherSurveillanceActorEntry : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public RawImage headshotImg;
    public bool loadedHeadshot = false;
    public ComputerOSUIComponent component;
    public RectTransform namePopup;
    public TextMeshProUGUI popupText;

    public SurveillanceApp appParent;
    public Human human;
    public bool isOver = false;

    public void Setup(SurveillanceApp newParent, Human newHuman)
    {
        appParent = newParent;
        human = newHuman;
        SetOnOver(false, true);
        LoadHeadshot();
    }

    public void LoadHeadshot()
    {
        if(!loadedHeadshot)
        {
            //Get human headshot
            List<Evidence.DataKey> keys = new List<Evidence.DataKey>();
            keys.Add(Evidence.DataKey.photo);
            headshotImg.texture = human.evidenceEntry.GetPhoto(keys);

            loadedHeadshot = true;
        }
    }

    public void SetOnOver(bool val, bool forceUpdate = false)
    {
        if(val != isOver || forceUpdate)
        {
            isOver = val;

            UpdateText();

            if(isOver)
            {
                //Load position
                SceneRecorder.ActorCapture actorCap = appParent.currentScene.aCap.Find(item => item.id == human.humanID);

                if (actorCap != null)
                {
                    Human h = actorCap.GetHuman();
                    appParent.locator.localPosition = new Vector2(h.lastUsedCCTVScreenPoint.x * appParent.captureRect.sizeDelta.x, h.lastUsedCCTVScreenPoint.y * appParent.captureRect.sizeDelta.y);

                    //Game.Log("Load locator for " + actorCap.id + " position from " + actorCap.human.lastUsedCCTVScreenPoint + ": " + appParent.locator.localPosition);

                    appParent.locator.gameObject.SetActive(true);
                }
                else
                {
                    Game.Log("Unable to find actor cap with human ID " + human.humanID);
                }
            }

            namePopup.gameObject.SetActive(isOver);
            appParent.UpdateTimelineFlagging();
        }
    }

    public void UpdateText()
    {
        //Is this person familiar to the security system?
        popupText.text = human.evidenceEntry.GetNameForDataKey(Evidence.DataKey.photo);

        if(isOver)
        {
            if (human.job != null && human.job.employer != null)
            {
                if (appParent.controller.ic != null && appParent.controller.ic.interactable.node.gameLocation == human.job.employer.address)
                {
                    human.evidenceEntry.MergeDataKeys(Evidence.DataKey.photo, Evidence.DataKey.name);
                    human.evidenceEntry.MergeDataKeys(Evidence.DataKey.photo, Evidence.DataKey.work);
                    human.evidenceEntry.AddDiscovery(Evidence.Discovery.jobDiscovery);

                    popupText.text = human.evidenceEntry.GetNameForDataKey(Evidence.DataKey.photo) + '\n' + Strings.Get("jobs", human.job.preset.name, Strings.Casing.firstLetterCaptial);
                }
            }
        }

        namePopup.sizeDelta = new Vector2(namePopup.sizeDelta.x, popupText.preferredHeight + 0.02f);
    }

    public void Press()
    {
        appParent.SelectActor(this);
    }
}
