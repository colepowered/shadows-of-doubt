﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CityDirectoryController : MonoBehaviour
{
    public WindowContentController windowContent;
    public InfoWindow parentWindow;
    public TextMeshProUGUI descriptionText;

    private void OnEnable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        if (windowContent == null) windowContent = this.gameObject.GetComponentInParent<WindowContentController>();
        parentWindow.passedEvidence.OnDataKeyChange += CheckEnabled;
        parentWindow.OnWindowRefresh += CheckEnabled;

        CheckEnabled();
    }

    private void OnDisable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange -= CheckEnabled;
        parentWindow.OnWindowRefresh -= CheckEnabled;
    }

    public void CheckEnabled()
    {
        descriptionText.text = string.Empty;

        for (int i = 0; i < Toolbox.Instance.alphabet.Length; i++)
        {
            char character = Toolbox.Instance.alphabet[i];

            if (windowContent.tabController.preset.displayContentWithTag.StartsWith(character.ToString()))
            {
                descriptionText.text = CityData.Instance.cityDirText[i].Trim() + '\n' + CityData.Instance.cityDirText[i + 1].Trim() + '\n' + CityData.Instance.cityDirText[i + 2].Trim();
                break;
            }
        }
    }
}
