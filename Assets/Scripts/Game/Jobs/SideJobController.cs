﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;
using UnityEngine.Analytics;

public class SideJobController : MonoBehaviour
{
    public bool enableJobs = true;

    [Header("Job Tracking")]
    public List<JobTracking> jobTracking = new List<JobTracking>();

    [Header("Exempt Citizens")]
    public List<Human> exemptFromPosters = new List<Human>();
    public List<Human> exemptFromPurps = new List<Human>();

    public Dictionary<Human, List<SideJob>> exemptFromPostersJobs = new Dictionary<Human, List<SideJob>>();
    public Dictionary<Human, List<SideJob>> exemptFromPurpsJobs = new Dictionary<Human, List<SideJob>>();

    //All jobs reference
    public Dictionary<int, SideJob> allJobsDictionary = new Dictionary<int, SideJob>();

    [System.Serializable]
    public class JobTracking
    {
        public string name;
        public JobPreset preset;
        public List<SideJob> activeJobs = new List<SideJob>();
        public List<SideJob> endedJobs = new List<SideJob>();
        public int desiredActiveInstances = 0;
    }

    [System.Serializable]
    public class JobPickData
    {
        public MotivePreset motive;
        public Citizen poster;
        public Citizen purp;
        public float score = 0;
    }

    //Used when invoking triggers for side jobs
    [System.NonSerialized]
    public SideJob invokedSideJob;
    [System.NonSerialized]
    public Objective invokedObjective;

    [Header("Debug")]
    public int debugJobID;

    //Singleton pattern
    private static SideJobController _instance;
    public static SideJobController Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        foreach (JobPreset p in Toolbox.Instance.allSideJobs)
        {
            JobTracking tracking = jobTracking.Find(item => item.preset == p && !item.preset.disabled);

            //Make sure we have a tracking class. Create one if we don't.
            if (tracking == null)
            {
                JobTracking newClass = new JobTracking();
                newClass.name = p.name;
                newClass.preset = p;
                jobTracking.Add(newClass);
                //Game.Log("Jobs: Setting up job " + p.name + "...");

                foreach (KeyValuePair<int, SideJob> pair in allJobsDictionary)
                {
                    if (pair.Value.preset == p)
                    {
                        if (pair.Value.state == SideJob.JobState.ended)
                        {
                            if(!newClass.endedJobs.Contains(pair.Value)) newClass.endedJobs.Add(pair.Value);
                        }
                        else
                        {
                            if(!newClass.activeJobs.Contains(pair.Value)) newClass.activeJobs.Add(pair.Value);
                        }
                    }
                }
            }
        }
    }

    //Check to see if jobs need to be created and create them if so...
    public void JobCreationCheck()
    {
        if (!enableJobs) return;
        if (!SessionData.Instance.startedGame) return; //Don't run this before start

        //Game.Log("Jobs: Job creation check...");

        //Check all side jobs types...
        foreach(JobTracking tracking in jobTracking)
        {
            JobPreset p = tracking.preset;

            if (p.disabled) continue;

            //Update desired job count...
            tracking.desiredActiveInstances = p.GetFrequencyForSocialCreditLevel();

            //If there are enough jobs, don't do anything, but if there are less, attempt to create some...
            if(tracking.activeJobs.Count < tracking.desiredActiveInstances)
            {
                Game.Log("Jobs: " + p.name + " has lower active jobs than desired (" + tracking.activeJobs.Count + "/" + tracking.desiredActiveInstances + ")");

                List<JobPickData> jobPickData = new List<JobPickData>();

                //Search for a purp...
                foreach(Citizen cit in CityData.Instance.citizenDirectory)
                {
                    if (exemptFromPurps.Contains(cit)) continue; //Exempt
                    if (cit.isDead || cit.isPlayer) continue;

                    //The criteria for being a purp is met, now we need a poster...
                    foreach(MotivePreset m in p.purpetratorMotives)
                    {
                        if (cit.isHomeless && !m.allowHomelessPurps) continue;
                        if ((cit.job == null || cit.job.employer == null) && !m.allowJoblessPurps) continue;

                        int purpScore = 0; //Used for ranking a prupetrator for this motive

                        //Check if the purp passes the motive rules...
                        if (!MotivePass(ref m.purpTraitModifiers, cit, out purpScore)) continue;

                        //Purp must have one of these jobs
                        if(m.usePurpJobs && m.purpJobs.Count > 0)
                        {
                            if (!m.purpJobs.Contains(cit.job.preset)) continue;
                        }

                        //Game.Log("Jobs: ... " + cit.name + " passes motive check... Acquaintances: " + cit.acquaintances.Count);

                        //We now have a valid purp...
                        //Scan acquaintance combinations for posters...
                        if(m.usePosterConnections)
                        {
                            foreach (Acquaintance aq in cit.acquaintances)
                            {
                                if ((aq.with as Citizen) == null)
                                {
                                    // Game.Log("Jobs: 1");
                                    continue;
                                }

                                if (exemptFromPosters.Contains(aq.with as Citizen))
                                {
                                    //Game.Log("Jobs: 2");
                                    continue;
                                }

                                if (aq.with.isHomeless && !m.allowHomelessPosters) continue;
                                if ((aq.with.job == null || aq.with.job.employer == null) && !m.allowJoblessPosters) continue;

                                //Check acceptable connection...
                                bool connCheck = false;

                                foreach(Acquaintance.ConnectionType conn in aq.connections)
                                {
                                    if (m.acceptableConnections.Contains(conn))
                                    {
                                        connCheck = true;
                                        break;
                                    }
                                    else if (m.acceptableConnections.Contains(Acquaintance.ConnectionType.anyAcquaintance))
                                    {
                                        connCheck = true;
                                        break;
                                    }
                                    else if (m.acceptableConnections.Contains(Acquaintance.ConnectionType.friendOrWork) && (aq.connections.Contains(Acquaintance.ConnectionType.friend) || aq.connections.Contains(Acquaintance.ConnectionType.workTeam) || aq.connections.Contains(Acquaintance.ConnectionType.workOther) || aq.connections.Contains(Acquaintance.ConnectionType.familiarWork)))
                                    {
                                        connCheck = true;
                                        break;
                                    }
                                    else if (m.acceptableConnections.Contains(Acquaintance.ConnectionType.knowsName) && aq.dataKeys.Contains(Evidence.DataKey.name))
                                    {
                                        connCheck = true;
                                        break;
                                    }
                                }

                                if(!connCheck && m.acceptableConnections.Contains(aq.secretConnection))
                                {
                                    connCheck = true;
                                }

                                if (!connCheck) continue;

                                int posterScore = 0;

                                if (m.usePosterTraits)
                                {
                                    if (!MotivePass(ref m.posterTraitModifiers, aq.with as Citizen, out posterScore)) continue;
                                }

                                if(!m.allowEnforcers)
                                {
                                    if (cit.isEnforcer) continue;
                                }

                                if (m.purpMustLiveAtDifferentAddressToPoster)
                                {
                                    if (cit.home == aq.with.home) continue;
                                }
                                else posterScore -= 2;

                                //Valid job pick scenario
                                //Game.Log("Jobs: ... Creating new pick scenario...");
                                JobPickData newPick = new JobPickData();
                                newPick.motive = m;
                                newPick.purp = cit;
                                newPick.poster = aq.with as Citizen;
                                newPick.score = purpScore + posterScore + Toolbox.Instance.Rand(0, 0.25f);

                                if (cit.home != null && aq.with.home != null)
                                {
                                    if (cit.home.building == aq.with.home.building)
                                    {
                                        newPick.score -= p.penaltyForPurpAndPosterSameBuilding;
                                    }
                                }

                                jobPickData.Add(newPick);
                            }
                        }
                        else
                        {
                            foreach(Citizen h in CityData.Instance.citizenDirectory)
                            {
                                if (h == cit) continue;

                                if (h.isHomeless && !m.allowHomelessPosters) continue;
                                if ((h.job == null || h.job.employer == null) && !m.allowJoblessPosters) continue;

                                if (exemptFromPosters.Contains(h))
                                {
                                    continue;
                                }

                                int posterScore = 0;

                                if (m.usePosterTraits)
                                {
                                    if (!MotivePass(ref m.posterTraitModifiers, h, out posterScore)) continue;
                                }

                                if (!m.allowEnforcers)
                                {
                                    if (cit.isEnforcer) continue;
                                }

                                if (m.purpMustLiveAtDifferentAddressToPoster)
                                {
                                    if (cit.home == h.home) continue;
                                }
                                else posterScore -= 2;

                                //Valid job pick scenario
                                //Game.Log("Jobs: ... Creating new pick scenario...");
                                JobPickData newPick = new JobPickData();
                                newPick.motive = m;
                                newPick.purp = cit;
                                newPick.poster = h;
                                newPick.score = purpScore + posterScore + Toolbox.Instance.Rand(0, 0.25f);

                                if (cit.home != null && h.home != null)
                                {
                                    if (cit.home.building == h.home.building)
                                    {
                                        newPick.score -= p.penaltyForPurpAndPosterSameBuilding;
                                    }
                                }

                                jobPickData.Add(newPick);
                            }
                        }
                    }
                }

                //Rank job
                jobPickData.Sort((p1, p2) => p2.score.CompareTo(p1.score)); //Using P2 first gives highest first

                //Create a job per checking loop...
                if(jobPickData.Count > 0)
                {
                    JobPickData picked = jobPickData[0];

                    bool immediatePost = false;

                    if(tracking.activeJobs.Count < p.immediatePostCountThreshold)
                    {
                        immediatePost = true;
                    }

                    //Get the class to create using the subclass string
                    string searchString = "SideJob";
                    if (p.subClass.Length > 0) searchString = "SideJob" + p.subClass;

                    //Collection of parameters to pass to the created class
                    object[] passed = { p, picked, immediatePost };

                    SideJob newJob = null;

                    //Create this class by string
                    //try
                    //{
                        Game.Log("Jobs: Creating new job: " + searchString + " (" + picked.motive.name + ")");
                        newJob = System.Activator.CreateInstance(System.Type.GetType(searchString), passed) as SideJob;
                        if(newJob != null && !tracking.activeJobs.Contains(newJob)) tracking.activeJobs.Add(newJob);
                    //}
                    //catch
                    //{
                    //    Game.LogError("Unable to create side job " + searchString);
                    //}
                }
                else
                {
                    Game.Log("Jobs: No compatible job matches for " + p.presetName);
                }
            }
            //If too many jobs, remove them if outside of player vicinity
            else if(tracking.activeJobs.Count > tracking.desiredActiveInstances && SessionData.Instance.startedGame)
            {
                for (int i = 0; i < tracking.activeJobs.Count; i++)
                {
                    SideJob j = tracking.activeJobs[i];

                    if(!j.accepted)
                    {
                        if(j.post != null && !j.post.rPl)
                        {
                            if(j.post.node != null)
                            {
                                //Must be out of player's vicinity to remove
                                if(Player.Instance.currentGameLocation != null && j.post.node.gameLocation != Player.Instance.currentGameLocation && j.post.node.tile.cityTile != Player.Instance.currentCityTile)
                                {
                                    Game.Log("Jobs: Removing job " + j.preset.name + " as there are too many jobs (" + tracking.activeJobs.Count + ") for this social credit level (" + tracking.desiredActiveInstances + ")");
                                    j.End();

                                    if(tracking.activeJobs.Count <= tracking.desiredActiveInstances)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Do job loop
            for (int i = 0; i < tracking.activeJobs.Count; i++)
            {
                tracking.activeJobs[i].GameWorldLoop();
            }
        }
    }

    public void AddExemptFromPostersJob(Human cit, SideJob job)
    {
        if(job != null)
        {
            if(!exemptFromPostersJobs.ContainsKey(cit))
            {
                exemptFromPostersJobs.Add(cit, new List<SideJob>());
            }

            if(!exemptFromPostersJobs[cit].Contains(job))
            {
                exemptFromPostersJobs[cit].Add(job);
            }
        }

        if(!exemptFromPosters.Contains(cit))
        {
            exemptFromPosters.Add(cit);
        }
    }

    public void AddExemptFromPurpJob(Human cit, SideJob job)
    {
        if (job != null)
        {
            if (!exemptFromPurpsJobs.ContainsKey(cit))
            {
                exemptFromPurpsJobs.Add(cit, new List<SideJob>());
            }

            if (!exemptFromPurpsJobs[cit].Contains(job))
            {
                exemptFromPurpsJobs[cit].Add(job);
            }
        }

        if (!exemptFromPurps.Contains(cit))
        {
            exemptFromPurps.Add(cit);
        }
    }

    public void RemoveExemptFromPosters(Human cit, SideJob job)
    {
        if (exemptFromPostersJobs.ContainsKey(cit))
        {
            exemptFromPostersJobs[cit].Remove(job);

            if(exemptFromPostersJobs[cit].Count <= 0)
            {
                exemptFromPosters.Remove(cit);
            }
        }
        else
        {
            exemptFromPosters.Remove(cit);
        }
    }

    public void RemoveExemptFromPurps(Human cit, SideJob job)
    {
        if (exemptFromPurpsJobs.ContainsKey(cit))
        {
            exemptFromPurpsJobs[cit].Remove(job);

            if (exemptFromPurpsJobs[cit].Count <= 0)
            {
                exemptFromPurpsJobs.Remove(cit);
            }
        }
        else
        {
            exemptFromPurps.Remove(cit);
        }
    }

    private bool MotivePass(ref List<MotivePreset.ModifierRule> rules, Citizen cit, out int score)
    {
        score = 0;

        //Check picking rules
        bool passRules = true;

        foreach (MotivePreset.ModifierRule rule in rules)
        {
            bool pass = false;

            if (rule.rule == CharacterTrait.RuleType.ifAnyOfThese)
            {
                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (cit.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = true;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifAllOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (!cit.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifNoneOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (cit.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifPartnerAnyOfThese)
            {
                if (cit.partner != null)
                {
                    foreach (CharacterTrait searchTrait in rule.traitList)
                    {
                        if (cit.partner.characterTraits.Exists(item => item.trait == searchTrait))
                        {
                            pass = true;
                            break;
                        }
                    }
                }
                else pass = false;
            }

            if (pass)
            {
                score += rule.score;
            }
            else if (rule.mustPassForApplication)
            {
                score = 0;
                passRules = false;
            }
        }

        return passRules;
    }

    //Trigger completion of a side job objective
    public void SideJobObjectiveComplete(SideJob job, Objective objective)
    {
        invokedSideJob = job;
        invokedObjective = objective;

        Game.Log("Invoking side job method: " + objective.queueElement.entryRef);

        try
        {
            Invoke(objective.queueElement.entryRef, 0f);
        }
        catch
        {
            Game.LogError("Could not invoke Side Job method: " + objective.queueElement.entryRef);
        }
    }

    //Below are triggered by side job objectives...
    public void CallPoster()
    {
        Game.Log("Jobs: CallPoster");
        //if (invokedSideJob.phase <= 0) invokedSideJob.phase = 1;
    }

    public void CallFake()
    {
        Game.Log("Jobs: CallFake");

        //SideJobSabotage sabo = invokedSideJob as SideJobSabotage;
        //if(sabo != null) sabo.callTime = SessionData.Instance.gameTime + 0.1f;

        //invokedSideJob.phase = 1;
    }

    public void SabotageRecoverInfo()
    {
        Game.Log("Jobs: SabotageRecoverInfo");

        //invokedSideJob.OnAcquireJobInfo(invokedSideJob.preset.additionalDialog[1]);
        //invokedSideJob.phase = 4;
    }

    [Button]
    public void ListSpawnedItemsForJob()
    {
        SideJob findJob = null;

        if (allJobsDictionary.TryGetValue(debugJobID, out findJob))
        {
            foreach (KeyValuePair<JobPreset.JobTag, Interactable> pair in findJob.activeJobItems)
            {
                Game.Log("Job item " + pair.Value.GetName() + " (" + pair.Value.id + ") exists with tag " + pair.Key.ToString() + " for job " + findJob.jobID + " at " + pair.Value.GetWorldPosition());
            }

            if (findJob.activeJobItems.Count <= 0)
            {
                Game.Log("No active job items have been spawned for job " + debugJobID);
            }

        }
        else Game.Log("Unable to find job " + debugJobID);
    }
}
