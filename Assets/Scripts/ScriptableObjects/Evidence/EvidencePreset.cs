﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using NaughtyAttributes;

//A scriptable object that can be used to make evidence presets.
//Script pass 1
[CreateAssetMenu(fileName = "evidence_data", menuName = "Database/Evidence/Evidence Preset")]

public class EvidencePreset : SoCustomComparison
{
    public enum CaptureRules { building, location, item, citizen};

    [Header("Setup")]
    [Tooltip("Spawn this subclass. If left empty it will use the base class.")]
    public string subClass = string.Empty;
    [Tooltip("The window style this evidence should use.")]
    public WindowStylePreset windowStyle;
    [Tooltip("Should this evidence use data key instances? If false, all keys will be tied together on creation.")]
    public bool useDataKeys = false;
    [EnableIf("useDataKeys")]
    public List<DataKeyControls.DataKeySettings> validKeys = new List<DataKeyControls.DataKeySettings>();
    [EnableIf("useDataKeys")]
    [Tooltip("The below keys act as if merged when retrieved")]
    public List<DataKeyAutomaticTies> passiveTies = new List<DataKeyAutomaticTies>();
    [EnableIf("useDataKeys")]
    public bool notifyOfTies = false;
    [Tooltip("Item Evidence class only: Should the person who this belongs to be featured in the name?")]
    public bool useBelongsToInName = false;
    [Tooltip("Does only one instance of this evidence exist?")]
    public bool isSingleton = false;
    [Tooltip("If true this does appear in history when inspected")]
    public bool disableHistory = false;
    [Tooltip("Allow this evidence to be given custom names")]
    public bool allowCustomNames = true;
    [Tooltip("If true this will be marked as discovered on any interaction, as opposed to just world interactions")]
    public bool markAsDiscoveredOnAnyInteraction = false;
    [Tooltip("If true this will always and only be able to be a world interaction")]
    public bool forceWorldInteraction = false;
    [Tooltip("Use window focus mode (black screen behind the window)")]
    public bool useWindowFocusMode = false;

    public enum BelongsToSetting { self, partner, paramour, boss, doctor, landlord};

    [Header("Graphics")]
    [Tooltip("The icon for this evidence")]
    public Sprite iconSpriteLarge;
    public Texture2D defaultNullImage;
    [Tooltip("Take in-game shot of this item for use in evidence (only used if photo key present)")]
    public bool useInGamePhoto = false;
    [Tooltip("Instead of 'this' use a photo of the person this belongs to")]
    public bool useWriter = false;
    [EnableIf("useInGamePhoto")]
    public Vector3 relativeCamPhotoPos = Vector3.zero;
    [EnableIf("useInGamePhoto")]
    public Vector3 relativeCamPhotoEuler = Vector3.zero;
    [EnableIf("useInGamePhoto")]
    public CaptureRules captureRules = CaptureRules.building;
    [EnableIf("useInGamePhoto")]
    public bool changeTimeOfDay = false;
    [EnableIf("useInGamePhoto")]
    public float captureTimeOfDay = 12f;
    public bool useCaptureLight = true;
    [Tooltip("Use image from a CCTV capture")]
    [DisableIf("useInGamePhoto")]
    public bool useSurveillanceCapture = false;

    [BoxGroup("Facts")]
    [Tooltip("Item evidence only: The 'belongs to' reference is set to this relation.")]
    public BelongsToSetting itemOwner = BelongsToSetting.self;
    [BoxGroup("Facts")]
    [Tooltip("Item evidence only: The 'belongs to' reference is set to this relation.")]
    public BelongsToSetting itemWriter = BelongsToSetting.self;
    [BoxGroup("Facts")]
    [Tooltip("Item evidence only: The 'subject' reference is set to this relation.")]
    public BelongsToSetting itemReceiver = BelongsToSetting.self;
    [BoxGroup("Facts")]
    [Tooltip("Automatically create these facts...")]
    [ReorderableList]
    public List<EvidenceFactSetup> factSetup = new List<EvidenceFactSetup>();
    [BoxGroup("Facts")]
    [Tooltip("Automatically add a link to these facts (doesn't have to feature this evidence)")]
    [ReorderableList]
    public List<FactLinkSetup> addFactLinks = new List<FactLinkSetup>();


    public enum Subject { self, writer, receiver, parent, interactable, interactableLocation };

    [System.Serializable]
    public class EvidenceFactSetup
    {
        public FactPreset preset;
        public Subject link;
        [Tooltip("Item evidence only: Only create the belongsTo fact if this is placed in an owned position.")]
        public bool onlyIfInOwnedPosition = false;
        [Tooltip("Create this fact on discovery")]
        public bool createOnDiscovery = true;
        [Tooltip("Force discovery of this fact when this is created")]
        public bool forceDiscoveryOnCreation = true;
        [Tooltip("When creating the above, switch the from (this) and to (link) evidence.")]
        public bool switchFindingFactToFrom = false;
    }

    [System.Serializable]
    public class FactLinkSetup
    {
        public FactLinkSubject subject;
        public string factDictionary;
        public Evidence.DataKey key;
        public bool discovery = true;
    }

    [System.Serializable]
    public class DataKeyAutomaticTies
    {
        public Evidence.DataKey mainKey;
        public List<Evidence.DataKey> mergeAtStart = new List<Evidence.DataKey>();
    }

    public enum FactLinkSubject { writer, receiver};


    [BoxGroup("Discovery")]
    [Tooltip("Discover this evidence when it is created.")]
    public bool discoverOnCreate = false;
    [BoxGroup("Discovery")]
    [Tooltip("On discovery, merge these keys (this evidence)")]
    [ReorderableList]
    public List<MergeKeysSetup> keyMergeOnDiscovery = new List<MergeKeysSetup>();
    [BoxGroup("Discovery")]
    [Tooltip("Conditions for discovery of this evidence (ANY of these)")]
    [ReorderableList]
    public List<Evidence.Discovery> discoveryTriggers = new List<Evidence.Discovery>();
    [BoxGroup("Discovery")]
    [Tooltip("Apply these discoveries on discovery")]
    [ReorderableList]
    public List<DiscoveryApplication> applicationOnDiscover = new List<DiscoveryApplication>();

    [System.Serializable]
    public class MergeKeysSetup
    {
        public Subject link;
        public List<Evidence.DataKey> mergeKeys;
    }

    [System.Serializable]
    public class DiscoveryApplication
    {
        public Subject link;
        public Evidence.Discovery applyDiscoveryTrigger;
    }

    [Header("Content")]
    [Tooltip("Use this ID for content (do the rest in DDS editor)")]
    public string ddsDocumentID = "715b743b-ee3e-4d93-99c5-fc5b1882b2f0";

    [Header("Matching")]
    [Tooltip("Some matching types below will only match to-from a match parent.")]
    public bool isMatchParent = false;
    [Tooltip("List of match types for auto-creating matches")]
    public List<MatchPreset> matchTypes = new List<MatchPreset>();

    [Header("Evidence Folder")]
    public bool enableSummary = true;
    public bool enableFacts = true;

    public enum PinnedStyle { polaroid, stickNote};
    [Tooltip("The type of pinned evidence style")]
    public PinnedStyle pinnedStyle = PinnedStyle.polaroid;
    [Tooltip("Colour multiplier for pinned evidence background")]
    public Color pinnedBackgroundColour = Color.white;

    public List<Evidence.DataKey> GetValidProfileKeys()
    {
        List<Evidence.DataKey> valid = new List<Evidence.DataKey>();
        
        foreach(DataKeyControls.DataKeySettings ks in validKeys)
        {
            if (ks.countTowardsProfile) valid.Add(ks.key);
        }

        return valid;
    }

    public List<Evidence.DataKey> GetUniqueProfileKeys()
    {
        List<Evidence.DataKey> valid = new List<Evidence.DataKey>();

        foreach (DataKeyControls.DataKeySettings ks in validKeys)
        {
            if (ks.uniqueKey) valid.Add(ks.key);
        }

        return valid;
    }

    public bool IsKeyValid(Evidence.DataKey key, out bool countTowardsProfile)
    {
        DataKeyControls.DataKeySettings setting = validKeys.Find(item => item.key == key);
        countTowardsProfile = false;

        if (setting != null)
        {
            countTowardsProfile = setting.countTowardsProfile;
            return true;
        }
        else return false;
    }

    public bool IsKeyUnique(Evidence.DataKey key)
    {
        if (!useDataKeys) return true; //Test this addition; otherwise it was bringing up errors in evidence that didn't use keys...

        DataKeyControls.DataKeySettings setting = validKeys.Find(item => item.key == key);

        if (setting != null)
        {
            return setting.uniqueKey;
        }
        else return false;
    }

    public int GetProfileKeyCount(List<Evidence.DataKey> keyList)
    {
        int ret = 0;

        foreach(Evidence.DataKey key in keyList)
        {
            bool count = false;

            if(IsKeyValid(key, out count))
            {
                if (count) ret++;
            }
        }

        return ret;
    }
}
