﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//Controller for a draggable destination of the UI.
//Script pass 1
public class DraggableDestination : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	//References
	public bool isOver = false;

	//Button?
	public Button but;

	//Events
	public delegate void DragDestination(GameObject dragObj, string tag);
    public event DragDestination OnDragged;

    //Accept these tags
    public List<string> acceptedTags = new List<string>();

    //Hover Colour
    private Image graphic;
    public Color originalColour = Color.white;
    public bool useHoverColours = true;
    public Color hoverAcceptColour = Color.yellow;

    void Awake()
    {
    	graphic = this.gameObject.GetComponent<Image>();
    	but = this.gameObject.GetComponent<Button>();
    	originalColour = graphic.color;
    }

	public void OnPointerEnter(PointerEventData eventData)
    {
        //Return if button is not interactable
        if (but != null && !but.interactable)
    	{
    		isOver = false;
            return;
    	}

    	//Check validity before confirming legitimate mouse over
    	//Dragged object?
		if (InterfaceController.Instance.dragged != null)
	    {
            //Does a tag match?
            bool tagMatch = false;

            if (InterfaceController.Instance.draggedTag.Length > 0)
            {
                if (acceptedTags.Contains(InterfaceController.Instance.draggedTag))
                {
                    tagMatch = true;
                }
            }

	    	//If a tag matches, confirm the mouse over
	    	if (tagMatch)
	    	{
        		isOver = true;
        		if(useHoverColours) graphic.color = hoverAcceptColour;
        	}
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;

        isOver = false;
        if (useHoverColours) graphic.color = originalColour;
    }

	//Check for on mouse up...
	void Update()
    {
    	if (isOver)
    	{
	    	if(Input.GetMouseButtonUp(0))
	    	{
	    		//Fire Event   	
	    		if(OnDragged != null)
				OnDragged(InterfaceController.Instance.dragged, InterfaceController.Instance.draggedTag);
				if (useHoverColours) graphic.color = originalColour;
	    	}
	    }
    }
}
