﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "musiccue_data", menuName = "Audio/Music Cue")]

public class MusicCue : SoCustomComparison
{
    [Header("Setup")]
    public string fmodGUID;
    public bool disabled = false;
    public bool debug = false;

    [Header("Track Settings")]
    [Tooltip("If true, only play this track once per game")]
    public bool playOnce = false;
    [Tooltip("If true then when it's appropraite to play this ambient, it will stop a previously playing one.")]
    public bool interrupt = false;
    [Tooltip("If true then this track will stop when the ambient state is switched to something that's not compatible")]
    public bool stopOnIncompatibleStateSwitch = false;
    [Tooltip("If true this track will avoid repetition as much as possible by playing other tracks of the same priority when available")]
    public bool avoidRepetition = true;
    [Range(0, 4)]
    [Tooltip("The game will choose between the highest available priority tracks. Higher plays first.")]
    public int ambientPriority = 2;

    public List<MusicTrigger> triggers = new List<MusicTrigger>();

    public enum MusicTriggerGameState { any, menu, inGame };

    public enum MusicTriggerPlayerState { any, safe, trespass, combat };

    public enum MusicTriggerPlayerLocation { any, outdoors, indoors, playersApartment };

    public enum MusicTriggerEvent { none, newMurderCase, caseComplete, caseFailed, caseUnsolved, socialCreditLevelUp, resolveScreen, arriveAtCrimeScene };

    [System.Serializable]
    public class MusicTrigger
    {
        public MusicTriggerGameState onGameState = MusicTriggerGameState.any;
        public MusicTriggerPlayerState onPlayerSate = MusicTriggerPlayerState.any;
        public MusicTriggerPlayerLocation onPlayerLocation = MusicTriggerPlayerLocation.any;

        [Space(7)]
        public MusicTriggerEvent onEvent = MusicTriggerEvent.none;
        [Range(0f, 1f)]
        public float eventTriggerChance = 1f;
        public bool triggerOnlyOnEvents = false;

        [Space(7)]
        [Tooltip("If true this will be triggered regardless of the time between tracks")]
        public bool ignoreSilentTimeBetweenTracks = false;

        [Space(7)]

        public bool onlyInDistricts = false;
        [EnableIf("onlyInDistricts")]
        public List<DistrictPreset> compatibleDistricts = new List<DistrictPreset>();

        public bool excludeDistricts = false;
        [EnableIf("excludeDistricts")]
        public List<DistrictPreset> excludedDistricts = new List<DistrictPreset>();

        [Space(7)]

        public bool onlyInBuildings = false;
        [EnableIf("onlyInBuildings")]
        public List<BuildingPreset> compatibleBuildings = new List<BuildingPreset>();

        public bool excludeBuildings = false;
        [EnableIf("excludeBuildings")]
        public List<BuildingPreset> excludedBuildings = new List<BuildingPreset>();

        [Space(7)]

        public bool onlyInLocations = false;
        [EnableIf("onlyInLocations")]
        public List<AddressPreset> compatibleAddressTypes = new List<AddressPreset>();

        public bool excludeLocations = false;
        [EnableIf("excludeLocations")]
        public List<AddressPreset> excludedAddressTypes = new List<AddressPreset>();

        [Space(7)]

        public bool onlyDuringStatuses = false;
        [EnableIf("onlyDuringStatuses")]
        public List<StatusPreset> compatibleStatuses = new List<StatusPreset>();

        public bool excludeStatuses = false;
        [EnableIf("excludeStatuses")]
        public List<StatusPreset> excludedStatuses = new List<StatusPreset>();

        [Space(7)]
        public bool useDecorGrimeRange = false;
        [MinMaxSlider(0f, 1f)]
        public Vector2 grimeRange = new Vector2(0, 1);

        [Space(7)]
        [InfoBox("Play on these floor ranges, if empty then it will play on any floor")]
        public List<Vector2> floorRanges = new List<Vector2>();
        [InfoBox("Play at this time (24hr clock so 0 = midnight, 12 = mid day etc). If empty then it will play at any time")]
        public List<Vector2> timeRanges = new List<Vector2>();
    }
}
