﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using System.Linq;
using Rewired.Data;

[Serializable]
public class Interactable : IEquatable<Interactable>
{
    //New class values
    [Header("Serializable")]
    //Basic Data
    public string name;
    public int id; //Numerical object ID
    [NonSerialized]
    public static int worldAssignID = 100000000; //ID assigner (start here to give space for intereactables that spawn tied to rooms)

    //Positional Data
    public Vector3 wPos; //World position
    public Vector3 wEuler; //World euler
    public Vector3 lPos; //Local position
    public Vector3 lEuler; //Local euler
    public Vector3 spWPos; //Spawn world position
    public Vector3 spWEuler; //Spawn world euler
    public Vector3Int spNode; //Spawn node
    public bool spR = false; //Is the spawn position on this object relevent? IE will the object be classed as moved, or will the player get the choice to 'put it back' etc

    //Configuration data
    public string p; //Preset name
    public List<Passed> pv; //Passed variables

    public int fp = -1; //Furniture parent ID
    public int fsoi = -1; //Furniture sub object index
    public int dp = -1; //Door parent
    public Toolbox.MaterialKey mk; //Material key - chosen when added to a room
    public LightConfiguration lcd; //Light configuration data - chosen for lights
    public string lp; //Lighting preset name
    public string bp; //Book preset name
    public string sdp; //Sync disk preset name
    public string dds; //Use to override this with a different DDS setup...
    public int pt = 0; //Used to pair to a furniture interactable. Covnerts to an enum

    //Ownership
    public int w = -1; //Writer
    public int r = -1; //Receiver
    public int b = -1; //Belongs to
    public int inv = -1; //If currently in inventory of a human

    //State data
    public float val = 0; //The value of this object

    public bool ins = false; //True if this has been inspected. This is used for objective checking. Also true if this multipage has been searched...
    public float lma; //Time this was last moved
    public float cs; //How much of this consumable is leftover
    public bool wo = false; //Should this be parented to a room?
    public int opp;

    public string bo; //Book preset
    public string sd; //Sync disk

    public bool sw0 = false; //Switch status: If this interactable is a switch, this bool will be available for reference here
    public bool sw1 = false; //A custom state used for additional interaction switches
    public bool sw2 = false; //A custom state used for additional interaction switches
    public bool sw3 = false; //A custom state used for additional interaction switches
    public bool locked = false; //A lock state
    public bool phy = false; //Physics pickup state

    public bool drm = false; //Is distance recognition mode enabled for this interactable?
    public int lzs = -1; //The size of the area to light in nodes
    public bool ml = false; //If true, add to the room's main lights
    public int pto; //Pair to owner
    //public FurniturePreset.SubObjectOwnership pairToOwner = FurniturePreset.SubObjectOwnership.nobody;
    public List<InteractablePreset.SubSpawnSlot> ssp; //Sub spawn positions

    //New versions/system of below
    public float mtr = -1; //Marked to remove, when safe to do so this will be removed from the game world completely

    //Various flags used for figuring out if we should save the state of this object...
    public bool cr = false; //Has this object been created after game start? Ie not in the city files...
    public bool rem = false; //Has this object been removed since start?
    public bool rPl = false; //Removed from placement; don't spawn this object
    public bool spCh = false; //Has the spawn position changed?
    public bool force = false; //Misc force the save state?

    //Dynamic fingerprints
    public List<DynamicFingerprint> df = new List<DynamicFingerprint>();

    //Saved print
    public List<SavedPrint> print = new List<SavedPrint>();

    //Surveillance captures
    public List<SceneRecorder.SceneCapture> cap = new List<SceneRecorder.SceneCapture>();
    public List<SceneRecorder.SceneCapture> sCap = new List<SceneRecorder.SceneCapture>();

    //Use naming evidence key
    public int nEvKey = -1;

    public float lhc; //Last hourly chime of a clock
    public bool ft = false; //Forced object to be spawned with colliders as triggers

    //Own passcode
    public GameplayController.Passcode passcode = null;

    [Header("Non Serialized")]
    [NonSerialized]
    public bool mov = false; //Has this been moved?
    [NonSerialized]
    public Telephone t = null;
    [NonSerialized]
    public string seed; //Used for RNG on generation only
    [NonSerialized]
    public bool save = false; //True if this is saved with city or save state. If false it is created with game load.
    [NonSerialized]
    public bool isTampered = false; //True if this object is noticably out of position
    [NonSerialized]
    public float distanceFromSpawn = 0f; //Distance from the spawn
    [NonSerialized]
    public bool originalPosition = true; //True if this is in it's spawned/original position ie the player has moved it.
    [NonSerialized]
    public Vector3 cvp; //Capture camera view postion
    [NonSerialized]
    public Vector3 cve; //Capture camera view euler
    [NonSerialized]
    public Evidence evidence;
    [NonSerialized]
    public SceneRecorder sceneRecorder; //Used to capture images (CCTV etc).
    [NonSerialized]
    public Transform spawnParent;
    [NonSerialized]
    public Transform parentTransform;
    [NonSerialized]
    public Human inInventory; //The interactable can either be on a node, or in an inventory
    [NonSerialized]
    public InteractablePreset preset; //Reference to the preset containing several parameters
    [NonSerialized]
    public FurnitureLocation furnitureParent; //This interactable is attached to this furniture
    [NonSerialized]
    public FurniturePreset.SubObject subObject; //If spawned on furniture, this is a reference to the sub object position class...
    [NonSerialized]
    public SideJob jobParent; //If to do with a side job, the reference is here...
    [NonSerialized]
    public MurderController.Murder murderParent; //If to do with a murder, the reference is here...
    [NonSerialized]
    public SpeechController speechController; //Speech controller reference;
    [NonSerialized]
    public InteractableController controller; //Reference to the physical interactable controller when spawned
    [NonSerialized]
    public LightController lightController; //Reference to the light controller when spawned
    [NonSerialized]
    public Interactable lockInteractable;
    [NonSerialized]
    public Interactable thisDoor; //If a lock, this is a reference to the door it opens.
    [NonSerialized]
    public object passwordSource;
    [NonSerialized]
    public GameObject spawnedObject;
    [NonSerialized]
    public NewNode node; //Current node coordinate
    [NonSerialized]
    public NewNode spawnNode;
    [NonSerialized]
    public NewRoom worldObjectRoomParent; //Will effectively be parented to this room
    [NonSerialized]
    public UsagePoint usagePoint; //Used for AI interaction (generated once loaded)
    [NonSerialized]
    public NewAIAction nextAIInteraction;
    [NonSerialized]
    public LightingPreset isLight; //When spawned, this will setup light controller
    [NonSerialized]
    public object objectRef; //Polymorphic object reference
    [NonSerialized]
    public Human writer;
    [NonSerialized]
    public Human reciever;
    [NonSerialized]
    public Human belongsTo;
    [NonSerialized]
    public Actor isActor; //If this is the iteractable of a character, use it's data to track position
    [NonSerialized]
    public BookPreset book;
    [NonSerialized]
    public SyncDiskPreset syncDisk;
    [NonSerialized]
    public GroupsController.SocialGroup group;
    [NonSerialized]
    public float recentCallCheck = 0; //When the player last checked for recent calls (0 = never checked, + = recent call discovered, - = no recent calls)
    [NonSerialized]
    private Transform ceilingFan;
    [NonSerialized]
    public NewAddress forSale; //Reference to the address for sale
    [NonSerialized]
    public List<Human> proxy; //Used for proxy detonation
    [NonSerialized]
    public List<SpatterSimulation.DecalSpawnData> spawnedDecals = null;

    //Audio
    [NonSerialized]
    public AudioController.LoopingSoundInfo actionLoop;

    [NonSerialized]
    public bool loadedGeometry = false; //Controlled by the node this is positioned on
    [NonSerialized]
    public Dictionary<InteractablePreset.InteractionKey, InteractableCurrentAction> currentActions = new Dictionary<InteractablePreset.InteractionKey, InteractableCurrentAction>();
    [NonSerialized]
    public List<InteractablePreset.InteractionAction> highlightActions = new List<InteractablePreset.InteractionAction>(); //Set action highlights
    [NonSerialized]
    public List<InteractablePreset.InteractionAction> disabledActions = new List<InteractablePreset.InteractionAction>(); //Temporary disabled actions
    [NonSerialized]
    public Dictionary<AIActionPreset, InteractablePreset.InteractionAction> aiActionReference = new Dictionary<AIActionPreset, InteractablePreset.InteractionAction>(); //Quick reference for AI to identify which action to use
    [NonSerialized]
    public float readingDelay = 0;
    [NonSerialized]
    public Dictionary<AIActionPreset, AudioEvent> actionAudioEventOverrides = new Dictionary<AIActionPreset, AudioEvent>(); //Override an action's audio events with this (must be added manually)
    [NonSerialized]
    public List<AudioController.LoopingSoundInfo> loopingAudio = new List<AudioController.LoopingSoundInfo>(); //References to any started looping sounds
    [NonSerialized]
    private bool isSetup = false; //True once world pos has been set once...
    [NonSerialized]
    public bool wasLoadedFromSave = false; //Debug: Was loaded from save game data
    [NonSerialized]
    public bool printDebug = false;

    [System.Serializable]
    public class LightConfiguration
    {
        public Color colour;
        public float intensity;
        public float flickerColourMultiplier;
        public float pulseSpeed;
        public float intervalTime;
        public bool flicker;
        public float range;
    }

    [System.Serializable]
    public class SavedPrint
    {
        public Vector3 worldPos;
        public int interactableID;
    }

    [System.Serializable]
    public class DynamicFingerprint
    {
        public int id;
        public float created;
        public string seed;
        public PrintLife life = PrintLife.timed;
    }

    public enum PrintLife { timed, manualRemoval };

    public class InteractableCurrentAction
    {
        public InteractablePreset.InteractionAction currentAction = null;
        public bool display = false;
        public bool enabled = false;
        public string overrideInteractionName;
        public bool forcePositioning = false;
        public ControlDisplayController.ControlPositioning forcePosition = ControlDisplayController.ControlPositioning.neutral;

        //Action highlights
        public bool highlight = false;
    }

    [System.Serializable]
    public class UsagePoint
    {
        public InteractablePreset.AIUseSetting useSetting; //Detail reference (inside class preset)
        [System.NonSerialized]
        public Interactable interactable;
        public NewNode node;
        public Dictionary<UsePointSlot, Human> users = new Dictionary<UsePointSlot, Human>();

        //public Human user; //Human currently using
        [System.NonSerialized]
        public GroupsController.SocialGroup reserved = null; //If the person using is reserving this node for them or their friends...

        [SerializeField]
        private Vector3 useageWorldPosition; //World position of the usage point
        public Vector3 worldLookAtPoint; //Point in world space that the user faces towards

        //Debug
        public Human debugDefaultSlot;
        public Human debugSlot1;
        public Human debugSlot2;
        public List<string> slotLog = new List<string>();

        public UsagePoint(InteractablePreset.AIUseSetting newPreset, Interactable newInteractable, NewNode newNode)
        {
            useSetting = newPreset;
            interactable = newInteractable;
            node = newNode;

            PositionUpdate();
        }

        //Call this if the interactable has moved
        public void PositionUpdate()
        {
            //If this is spawned, we can use the actual object position
            if (interactable.controller != null)
            {
                useageWorldPosition = interactable.controller.transform.TransformPoint(useSetting.usageOffset);
                worldLookAtPoint = interactable.controller.transform.TransformPoint(useSetting.facingOffset);
            }
            else
            {
                //If this hasn't spawned yet we need to use a matrix to calculate the offset positions...
                //Get scale
                Vector3 locScale = Vector3.one;
                if (interactable.preset.prefab != null) locScale = interactable.preset.prefabLocalScale;

                //This will transform the point from the original transform location
                Matrix4x4 m = Matrix4x4.TRS(interactable.wPos, Quaternion.Euler(interactable.wEuler), locScale);

                useageWorldPosition = m.MultiplyPoint3x4(useSetting.usageOffset);
                worldLookAtPoint = m.MultiplyPoint3x4(useSetting.facingOffset);
            }

            //Use Node's floor
            if (useSetting.useNodeFloorPosition && interactable.node != null)
            {
                useageWorldPosition.y = interactable.node.position.y;
            }
        }

        //Get the usage poisiton in world space, factoring in relative position options
        public Vector3 GetUsageWorldPosition(Vector3 userPos, Actor actor)
        {
            Vector3 ret = useageWorldPosition;

            if(useSetting.useDoorBehaviour && actor != null)
            {
                //New system: Check using room system for which side we're on...
                NewDoor door = interactable.objectRef as NewDoor;

                //Override the user position with the location of the room or gamelocation the actor is in...
                if(door != null && actor.ai != null && actor.ai.currentAction != null && actor.ai.currentAction.forcedNode == null)
                {
                    userPos = Toolbox.Instance.GetDoorSideNode(actor.currentNode, door).position;
                }

                //UnityEngine.Game.Log("Using door behaviour to get usage position for " + interactable.preset.name + " user pos: " + userPos);

                //Use the room to determin which side of the door to open...
                Vector3 localToDoor = Vector3.zero;

                //Use spawn point as the door's point (use a matrix)
                Vector3 locScale = Vector3.one;
                if (interactable.preset.prefab != null) locScale = interactable.preset.prefab.transform.localScale;

                if (interactable.furnitureParent != null)
                {
                    Vector3 prefabScale = interactable.furnitureParent.furniture.prefab.transform.localScale;
                    locScale = new Vector3(interactable.furnitureParent.scaleMultiplier.x * prefabScale.x, interactable.furnitureParent.scaleMultiplier.y * prefabScale.y, interactable.furnitureParent.scaleMultiplier.z * prefabScale.z);
                }

                //This will transform the point from the original transform location
                Matrix4x4 m = Matrix4x4.TRS(interactable.wPos, Quaternion.Euler(interactable.wEuler), locScale).inverse; //Using inverse should give word > local?
                localToDoor = m.MultiplyPoint3x4(userPos);

                //UnityEngine.Game.Log("...User position local to door is " + localToDoor + "(world: " + interactable.worldPosition + " Euler: " + interactable.worldEuler + ")");

                //If the actor is behind the interactable, then flip the Z axis of the use point
                if (localToDoor.z < 0)
                {
                    //We only need to flip the Z local coordinate of the use point offset, not the whole thing.
                    ret = GetPositionWithInvertedZ();
                    //UnityEngine.Game.Log("...User is behind door, getting inverted Z use position (normal: " + useageWorldPosition + ") (Inverted: " + ret + ")");
                }
            }

            //Apply an offset so character is always sitting at the correct height relative to their size...
            if(useSetting.useSittingOffset && actor != null && actor.modelParent != null)
            {
                //If scale is 1 then position is 0
                //Find the difference between lower torso height...
                float lowerTorsoDiff = CitizenControls.Instance.sittingYOffset * actor.modelParent.transform.localScale.y - CitizenControls.Instance.sittingYOffset;

                //Minus this difference to the floor...
                ret.y -= lowerTorsoDiff;
            }
            //Apply an offset so character is at the correct height relative to their size...
            if (useSetting.useArmsStandingOffset && actor != null && actor.modelParent != null)
            {
                //If scale is 1 then position is 0
                //Find the difference between lower torso height...
                float armsDiff = CitizenControls.Instance.armsStandingYOffset * actor.modelParent.transform.localScale.y - CitizenControls.Instance.armsStandingYOffset;

                //Minus this difference to the floor...
                ret.y -= armsDiff;
            }

            return ret;
        }

        //Get the position of this if the local Z coordinate was inverted
        private Vector3 GetPositionWithInvertedZ()
        {
            Vector3 invertedOffset = useSetting.usageOffset;
            invertedOffset.z *= -1f;

            Vector3 worldCoordWithInvertedZ = Vector3.zero;

            //If this is spawned, we can use the actual object position
            if (interactable.controller != null)
            {
                worldCoordWithInvertedZ = interactable.controller.transform.TransformPoint(invertedOffset);
            }
            else
            {
                //If this hasn't spawned yet we need to use a matrix to calculate the offset positions...
                //Get scale
                Vector3 locScale = Vector3.one;
                if (interactable.preset.prefab != null) locScale = interactable.preset.prefab.transform.localScale;

                if (interactable.furnitureParent != null)
                {
                    Vector3 prefabScale = interactable.furnitureParent.furniture.prefab.transform.localScale;
                    locScale = new Vector3(interactable.furnitureParent.scaleMultiplier.x * prefabScale.x, interactable.furnitureParent.scaleMultiplier.y * prefabScale.y, interactable.furnitureParent.scaleMultiplier.z * prefabScale.z);
                }

                //This will transform the point from the original transform location
                Matrix4x4 m = Matrix4x4.TRS(interactable.wPos, Quaternion.Euler(interactable.wEuler), locScale);
                worldCoordWithInvertedZ = m.MultiplyPoint3x4(invertedOffset);
            }

            //Use Node's floor
            if (useSetting.useNodeFloorPosition && interactable.node != null)
            {
                worldCoordWithInvertedZ.y = interactable.node.position.y;
            }

            return worldCoordWithInvertedZ;
        }

        //Use this to set user(s) or reserve for users
        public bool TrySetUser(UsePointSlot slot, Human newUser, string debug = "")
        {
            Human existing = null;
            TryGetUserAtSlot(slot, out existing);

            //If currently occupied, trying to set a new user and this is a different user to the existing one
            if (existing != null && newUser != null && existing != newUser)
            {
                if ((Game.Instance.devMode && Game.Instance.collectDebugData))
                {
                    if (slotLog.Count > 50)
                    {
                        slotLog.RemoveAt(0);
                    }

                    slotLog.Add("Unable to set slot " + slot.ToString() + " to " + newUser.GetCitizenName() + ": " + debug);
                }

                return false;
            }
            else
            {
                if (!users.ContainsKey(slot))
                {
                    users.Add(slot, newUser);
                }
                else users[slot] = newUser;

                if((Game.Instance.devMode && Game.Instance.collectDebugData))
                {
                    if (slotLog.Count > 50)
                    {
                        slotLog.RemoveAt(0);
                    }

                    if (newUser != null) slotLog.Add("Setting slot " + slot.ToString() + " to " + newUser.GetCitizenName() + ": " + debug);
                    else slotLog.Add("Setting slot " + slot.ToString() + " to empty" + ": " + debug);

                    if (slot == UsePointSlot.defaultSlot)
                    {
                        debugDefaultSlot = newUser;
                    }
                    else if (slot == UsePointSlot.slot1)
                    {
                        debugSlot1 = newUser;
                    }
                    else if (slot == UsePointSlot.slot2)
                    {
                        debugSlot2 = newUser;
                    }
                }

                return true;
            }
        }

        public void SetReserved(GroupsController.SocialGroup group)
        {
            if(reserved != null && reserved != group)
            {
                if (reserved.reserved == null) reserved.reserved = new List<Interactable>();

                foreach (Interactable i in reserved.reserved)
                {
                    i.usagePoint.reserved = null;
                }

                if (reserved != null && reserved.reserved != null)
                {
                    reserved.reserved.Clear(); //Clear reserved interactables reference
                }
            }

            reserved = group;
        }

        public bool TryGetUserAtSlot(UsePointSlot slot, out Human user)
        {
            user = null;

            if (users.TryGetValue(slot, out user))
            {
                if (user == null) return false;
                else return true;
            }
            else return false;
        }

        public void RemoveUserFromAllSlots(Human user)
        {
            List<UsePointSlot> allKeys = new List<UsePointSlot>(users.Keys);

            foreach(UsePointSlot key in allKeys)
            {
                if(users.ContainsKey(key))
                {
                    if(users[key] == user)
                    {
                        users[key] = null;
                    }
                }
            }
        }
    }

    public enum UsePointSlot { defaultSlot, slot1, slot2};

    //Passed variables
    public enum PassedVarType { jobID, humanID, noteID, roomID, addressID, time, savedSceneCapID, menuIndex, vmailThreadID, consumableAmount, companyID, stringInteractablePreset, isTrash, jobTag, groupID, ddsOverride, metaObjectID, murderID, decal, decalDynamicText, ownedByAddress, vmailThreadMsgIndex, phoneNumber, lostItemPreset, lostItemBuilding, lostItemReward, lostItemFloorX, lostItemFloorY};

    [System.Serializable]
    public class Passed
    {
        public PassedVarType varType;
        public float value = -1;
        public string str;

        public Passed(PassedVarType newType, float newVal, string newStr = null)
        {
            varType = newType;
            value = newVal;
            str = newStr;
        }
    }

    //Events
    public delegate void SwitchChange();
    public event SwitchChange OnSwitchChange;

    public delegate void State1Change();
    public event State1Change OnState1Change;

    //Triggered on removing an item from the gameword
    public delegate void Deleted(Interactable destroyed);
    public event Deleted OnDelete;

    public delegate void RemovedFromWorld();
    public event RemovedFromWorld OnRemovedFromWorld;

    //Default constructor: Use with the interactable creator to set everything up
    public Interactable(InteractablePreset newPreset)
    {
        preset = newPreset;
        p = preset.presetName;
    }

    bool IEquatable<Interactable>.Equals(Interactable other)
    {
        return other.GetHashCode() == GetHashCode();
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    //Used to setup misc references (run before OnLoad, OnCreate). Both created & loaded run this.
    public void MainSetupStart()
    {
        isSetup = false;

        //Add to directory
        CityData.Instance.interactableDirectory.Add(this);

        if(id != 0)
        {
            Interactable existing;

            if(CityData.Instance.savableInteractableDictionary.TryGetValue(id, out existing))
            {
                Game.LogError("Interactable " + id + " already exists (Existing: " + existing.GetName() + " at " + existing.GetWorldPosition() + " Loaded from state save: " + existing.wasLoadedFromSave + ", New: " + p + " at " + GetWorldPosition() +") (" + CityData.Instance.interactableDirectory.Count + " interactables with ID assign of " + Interactable.worldAssignID + ")");
            }
            else
            {
                CityData.Instance.savableInteractableDictionary.Add(id, this);
            }
        }

        //Parse passed variables. These are needed for save data, so might as well use them here...
        UpdatePassedVariables();
    }

    //Handle/load from passed variables
    public void UpdatePassedVariables()
    {
        if (pv != null)
        {
            for (int i = 0; i < pv.Count; i++)
            {
                Passed p = pv[i];

                if (p.varType == PassedVarType.jobID)
                {
                    if (SideJobController.Instance.allJobsDictionary.TryGetValue((int)p.value, out jobParent))
                    {
                        Game.Log("Jobs: Interactable " + id + " assigned job ID " + p.value);
                    }
                    else
                    {
                        Game.LogError("Jobs: Unable to find job with ID " + (int)p.value + ", current jobs loaded: " + SideJobController.Instance.allJobsDictionary.Count + ". If no jobs are loaded, this is probably an object created in city data that has been assigned to a mission...");
                    }
                }
                else if (p.varType == PassedVarType.murderID)
                {
                    murderParent = MurderController.Instance.activeMurders.Find(item => item.murderID == (int)p.value);
                    if (murderParent == null) murderParent = MurderController.Instance.inactiveMurders.Find(item => item.murderID == (int)p.value);

                    if (murderParent == null)
                    {
                        Game.LogError("Murder: Unable to find murder with ID " + (int)p.value + ", current murders loaded: " + (MurderController.Instance.activeMurders.Count + MurderController.Instance.inactiveMurders.Count) + ". If no murders are loaded, this is probably an object created in city data that has been assigned to a murder...");
                    }
                }
                else if (p.varType == PassedVarType.consumableAmount)
                {
                    cs = p.value;
                }
                else if (p.varType == PassedVarType.isTrash)
                {
                    MarkAsTrash(true, true, p.value);
                }
                else if (p.varType == PassedVarType.jobTag)
                {
                    //Add to active job items. This always needs job id to be assigned first...
                    if (jobParent != null)
                    {
                        if (jobParent.activeJobItems == null) jobParent.activeJobItems = new Dictionary<JobPreset.JobTag, Interactable>();

                        if (jobParent.activeJobItems.ContainsKey((JobPreset.JobTag)Mathf.RoundToInt(p.value)))
                        {
                            Game.LogError("Jobs: Item tag " + (JobPreset.JobTag)Mathf.RoundToInt(p.value) + " already existings for job! This interactable: " + id + ", exisiting interactable: " + jobParent.activeJobItems[(JobPreset.JobTag)Mathf.RoundToInt(p.value)].id);
                        }
                        else
                        {
                            Game.Log("Jobs: Loading in job item " + (JobPreset.JobTag)Mathf.RoundToInt(p.value) + " for job " + jobParent.jobID + " (" + id + ")");
                            jobParent.activeJobItems.Add((JobPreset.JobTag)Mathf.RoundToInt(p.value), this);
                        }
                    }
                    else if (murderParent != null)
                    {
                        if (murderParent.activeMurderItems == null) murderParent.activeMurderItems = new Dictionary<JobPreset.JobTag, Interactable>();

                        if (murderParent.activeMurderItems.ContainsKey((JobPreset.JobTag)Mathf.RoundToInt(p.value)))
                        {
                            Game.LogError("Murder: Item tag " + (JobPreset.JobTag)Mathf.RoundToInt(p.value) + " already existings for murder!  This interactable: " + id + ", exisiting interactable: " + murderParent.activeMurderItems[(JobPreset.JobTag)Mathf.RoundToInt(p.value)].id);
                        }
                        else
                        {
                            Game.Log("Jobs: Loading in murder item " + (JobPreset.JobTag)Mathf.RoundToInt(p.value) + " (" + id + ")");
                            murderParent.activeMurderItems.Add((JobPreset.JobTag)Mathf.RoundToInt(p.value), this);
                        }
                    }
                    else
                    {
                        Game.LogError("Job/Murder tagged items are being assiged before murder ID. Does this murder exist? This is probably an object created in city data that has been assigned to a murder...");
                    }
                }
                else if (p.varType == PassedVarType.groupID)
                {
                    group = GroupsController.Instance.groups.Find(item => item.id == p.value);
                }
                else if (p.varType == PassedVarType.addressID)
                {
                    CityData.Instance.addressDictionary.TryGetValue(Mathf.RoundToInt(p.value), out forSale);
                }
                else if (p.varType == PassedVarType.decal)
                {
                    ArtPreset foundArt = null;

                    if (Toolbox.Instance.LoadDataFromResources<ArtPreset>(p.str, out foundArt))
                    {
                        objectRef = foundArt;
                    }
                }
                else if (p.varType == PassedVarType.ownedByAddress)
                {
                    NewAddress foundOwned = null;

                    if (CityData.Instance.addressDictionary.TryGetValue(Mathf.RoundToInt(p.value), out foundOwned))
                    {
                        objectRef = foundOwned;
                    }
                }
            }
        }
    }

    //When creating this object for the first time
    public void OnCreate()
    {
        //Create seed
        seed = id.ToString();

        //Add prefab euler (precalculated to be compatible with multithreading)
        if (preset.spawnable && preset.prefab != null)
        {
            lEuler += preset.prefabLocalEuler;
        }

        //if(Game.Instance.devMode && Game.Instance.collectDebugData)
        //{
        //    if (CityConstructor.Instance != null && !SessionData.Instance.startedGame && (int)CityConstructor.Instance.loadState > 15)
        //    {
        //        Game.Log("CityGen: Creating an interactable after interior generation: " + preset.presetName + " (" + id + ")");
        //    }
        //}

        //Generate a value
        SetValue(Toolbox.Instance.RandContained(Mathf.FloorToInt(preset.value.x), Mathf.CeilToInt(preset.value.y) + 1, seed, out seed));

        //Set consumable state
        cs = preset.consumableAmount;

        //Get colour
        if (preset.useOwnColourSettings)
        {
            //Create a key
            Toolbox.MaterialKey newKey = new Toolbox.MaterialKey();
            newKey.baseMaterial = null;

            //Main colour
            if (preset.mainColour != InteractablePreset.InteractableColourSetting.none)
            {
                if (preset.mainColour == InteractablePreset.InteractableColourSetting.randomDecorColour && node != null)
                {
                    newKey.mainColour = MaterialsController.Instance.GetColourFromScheme(node.room.colourScheme, MaterialGroupPreset.MaterialColour.any, node.room, node.building);
                }
                else if (preset.mainColour == InteractablePreset.InteractableColourSetting.ownersFavColour && belongsTo != null)
                {
                    newKey.mainColour = SocialStatistics.Instance.favouriteColoursPool[belongsTo.favColourIndex];
                }
                //Pick from a pool of colours
                else if (preset.mainColour == InteractablePreset.InteractableColourSetting.randomColour)
                {
                    newKey.mainColour = SocialStatistics.Instance.favouriteColoursPool[Toolbox.Instance.RandContained(0, SocialStatistics.Instance.favouriteColoursPool.Count, seed, out seed)];
                }
                else if (preset.mainColour == InteractablePreset.InteractableColourSetting.syncDisk && syncDisk != null)
                {
                    GameplayControls.SyncDiskColour diskColour = GameplayControls.Instance.syncDiskColours.Find(item => item.category == syncDisk.manufacturer);

                    if (diskColour != null)
                    {
                        newKey.mainColour = diskColour.mainColour;
                    }
                }
            }

            //Custom 1
            if (preset.customColour1 != InteractablePreset.InteractableColourSetting.none)
            {
                if (preset.customColour1 == InteractablePreset.InteractableColourSetting.randomDecorColour && node != null)
                {
                    newKey.colour1 = MaterialsController.Instance.GetColourFromScheme(node.room.colourScheme, MaterialGroupPreset.MaterialColour.any, node.room, node.building);
                }
                else if (preset.customColour1 == InteractablePreset.InteractableColourSetting.ownersFavColour && belongsTo != null)
                {
                    newKey.colour1 = SocialStatistics.Instance.favouriteColoursPool[belongsTo.favColourIndex];
                }
                //Pick from a pool of colours
                else if (preset.customColour1 == InteractablePreset.InteractableColourSetting.randomColour)
                {
                    newKey.colour1 = SocialStatistics.Instance.favouriteColoursPool[Toolbox.Instance.RandContained(0, SocialStatistics.Instance.favouriteColoursPool.Count, seed, out seed)];
                }
                else if (preset.mainColour == InteractablePreset.InteractableColourSetting.syncDisk && syncDisk != null)
                {
                    GameplayControls.SyncDiskColour diskColour = GameplayControls.Instance.syncDiskColours.Find(item => item.category == syncDisk.manufacturer);

                    if (diskColour != null)
                    {
                        newKey.colour1 = diskColour.colour1;
                    }
                }
            }

            //Custom 2
            if (preset.customColour2 != InteractablePreset.InteractableColourSetting.none)
            {
                if (preset.customColour2 == InteractablePreset.InteractableColourSetting.randomDecorColour && node != null)
                {
                    newKey.colour2 = MaterialsController.Instance.GetColourFromScheme(node.room.colourScheme, MaterialGroupPreset.MaterialColour.any, node.room, node.building);
                }
                else if (preset.customColour2 == InteractablePreset.InteractableColourSetting.ownersFavColour && belongsTo != null)
                {
                    newKey.colour2 = SocialStatistics.Instance.favouriteColoursPool[belongsTo.favColourIndex];
                }
                //Pick from a pool of colours
                else if (preset.customColour2 == InteractablePreset.InteractableColourSetting.randomColour)
                {
                    newKey.colour2 = SocialStatistics.Instance.favouriteColoursPool[Toolbox.Instance.RandContained(0, SocialStatistics.Instance.favouriteColoursPool.Count, seed, out seed)];
                }
                else if (preset.mainColour == InteractablePreset.InteractableColourSetting.syncDisk && syncDisk != null)
                {
                    GameplayControls.SyncDiskColour diskColour = GameplayControls.Instance.syncDiskColours.Find(item => item.category == syncDisk.manufacturer);

                    if (diskColour != null)
                    {
                        newKey.colour2 = diskColour.colour2;
                    }
                }
            }

            //Custom 3
            if (preset.customColour3 != InteractablePreset.InteractableColourSetting.none)
            {
                if (preset.customColour3 == InteractablePreset.InteractableColourSetting.randomDecorColour && node != null)
                {
                    newKey.colour3 = MaterialsController.Instance.GetColourFromScheme(node.room.colourScheme, MaterialGroupPreset.MaterialColour.any, node.room, node.building);
                }
                else if (preset.customColour3 == InteractablePreset.InteractableColourSetting.ownersFavColour && belongsTo != null)
                {
                    newKey.colour3 = SocialStatistics.Instance.favouriteColoursPool[belongsTo.favColourIndex];
                }
                //Pick from a pool of colours
                else if (preset.customColour3 == InteractablePreset.InteractableColourSetting.randomColour)
                {
                    newKey.colour3 = SocialStatistics.Instance.favouriteColoursPool[Toolbox.Instance.RandContained(0, SocialStatistics.Instance.favouriteColoursPool.Count, seed, out seed)];
                }
                else if (preset.mainColour == InteractablePreset.InteractableColourSetting.syncDisk && syncDisk != null)
                {
                    GameplayControls.SyncDiskColour diskColour = GameplayControls.Instance.syncDiskColours.Find(item => item.category == syncDisk.manufacturer);

                    if (diskColour != null)
                    {
                        newKey.colour2 = diskColour.colour2;
                    }
                }
            }

            if(preset.inheritGrubValue)
            {
                newKey.grubiness = node.room.defaultWallKey.grubiness;
            }

            SetMaterialKey(newKey);
        }

        //Copy subspawn positions
        if (preset.subSpawnPositions.Count > 0)
        {
            ssp = new List<InteractablePreset.SubSpawnSlot>(preset.subSpawnPositions);
        }

        //Set starting states
        if (!ml) sw0 = preset.startingSwitchState;
        sw1 = preset.startingCustomState1;
        sw2 = preset.startingCustomState2;
        sw3 = preset.startingCustomState3;
        locked = preset.startingLockState;

        //Mark as trash
        if(preset.markAsTrashOnCreate)
        {
            MarkAsTrash(true);
        }

        //Distance recognition
        drm = preset.distanceRecognitionEnabled;

        //Update the name
        UpdateName();

        //Created after save game data
        if (SessionData.Instance.startedGame || (CityConstructor.Instance != null && (int)CityConstructor.Instance.loadState > 19))
        {
            cr = true;
        }

        //Update position (+ assign node)
        UpdateWorldPositionAndNode(true); //This will also assign the interactable to the correct node

        //Set breaker references...
        if (preset.specialCaseFlag == InteractablePreset.SpecialCase.breakerSecurity)
        {
            if (node.gameLocation.thisAsAddress != null && node.gameLocation.thisAsAddress.addressPreset.useOwnBreakerBox)
            {
                node.gameLocation.thisAsAddress.SetBreakerSecurity(this);
            }
            else if(node.gameLocation.thisAsAddress != null && node.gameLocation.floor != null)
            {
                node.gameLocation.floor.SetBreakerSecurity(this);
            }
        }
        else if (preset.specialCaseFlag == InteractablePreset.SpecialCase.breakerDoors)
        {
            if (node.gameLocation.thisAsAddress != null && node.gameLocation.thisAsAddress.addressPreset.useOwnBreakerBox)
            {
                node.gameLocation.thisAsAddress.SetBreakerDoors(this);
            }
            else if (node.gameLocation.thisAsAddress != null && node.gameLocation.floor != null)
            {
                node.gameLocation.floor.SetBreakerDoors(this);
            }
        }
        else if (preset.specialCaseFlag == InteractablePreset.SpecialCase.breakerLights)
        {
            if (node.gameLocation.thisAsAddress != null && node.gameLocation.thisAsAddress.addressPreset.useOwnBreakerBox)
            {
                node.gameLocation.thisAsAddress.SetBreakerLights(this);
            }
            else if (node.gameLocation.thisAsAddress != null && node.gameLocation.floor != null)
            {
                node.gameLocation.floor.SetBreakerLights(this);
            }
        }

        MainSetupEnd();
    }

    //When loading this object from JSON
    public void OnLoad()
    {
        seed = id.ToString();

        //The following are not created when serialized from JSON, so create them here...
        currentActions = new Dictionary<InteractablePreset.InteractionKey, InteractableCurrentAction>();
        highlightActions = new List<InteractablePreset.InteractionAction>(); //Set action highlights
        disabledActions = new List<InteractablePreset.InteractionAction>(); //Temporary disabled actions
        aiActionReference = new Dictionary<AIActionPreset, InteractablePreset.InteractionAction>(); //Quick reference for AI to identify which action to use
        actionAudioEventOverrides = new Dictionary<AIActionPreset, AudioEvent>(); //Override an action's audio events with this (must be added manually)
        loopingAudio = new List<AudioController.LoopingSoundInfo>(); //References to any started looping sounds
        if (df == null) df = new List<DynamicFingerprint>();
        if (cap == null) cap = new List<SceneRecorder.SceneCapture>();
        if (sCap == null) sCap = new List<SceneRecorder.SceneCapture>();

        //Game.Log("Loaded " + name + " ID " + id);

        //Parse preset
        if (!Toolbox.Instance.objectPresetDictionary.TryGetValue(p, out preset))
        {
            Game.LogError("Interactables unable to find preset for " + p);
        }

        //Parse owners/receivers
        if(b > 0 && belongsTo == null)
        {
            if(CityData.Instance.GetHuman(b, out belongsTo))
            {
                SetOwner(belongsTo, false);
            }
        }

        if (r > 0 && reciever == null)
        {
            if (CityData.Instance.GetHuman(r, out reciever))
            {
                SetReciever(reciever);
            }
        }

        if (w > 0 && writer == null)
        {
            if (CityData.Instance.GetHuman(w, out writer))
            {
                SetWriter(writer);
            }
        }

        //Parse passed objects
        if (bo != null && bo.Length > 0 && book == null)
        {
            Toolbox.Instance.LoadDataFromResources<BookPreset>(bo, out book);
        }

        //if (ri != null && ri.Length > 0 && retailItem == null)
        //{
        //    Toolbox.Instance.LoadDataFromResources<RetailItemPreset>(ri, out retailItem);
        //}

        if (sd != null && sd.Length > 0 && syncDisk == null)
        {
            Toolbox.Instance.LoadDataFromResources<SyncDiskPreset>(sd, out syncDisk);
        }

        //Load value
        SetValue(val);

        //Parse furniture parent
        if(!wo && fp > -1 && furnitureParent == null)
        {
            if(CityConstructor.Instance.loadingFurnitureReference.TryGetValue(fp, out furnitureParent))
            {
                //This is an integrated interactable if fsoi is -1
                if (fsoi <= -1)
                {
                    if (!furnitureParent.integratedInteractables.Contains(this))
                    {
                        furnitureParent.integratedInteractables.Add(this);
                    }
                }
            }
            else
            {
                Game.LogError("Unable to find furniture parent: " + fp + " (loading reference contains: " + CityConstructor.Instance.loadingFurnitureReference.Count + ") Interactable: " + name + " world pos: " + wPos + " [If this is contained within evidence board it could be waiting to be deleted by the game]");

                //NewRoom roomOne = null;

                //if(CityData.Instance.roomDictionary.TryGetValue(1, out roomOne))
                //{
                //    string ids = string.Empty;

                //    foreach(FurnitureLocation loc in roomOne.individualFurniture)
                //    {
                //        ids += ", " + loc.id;
                //    }

                //    Game.LogError("Room 1 contains " + roomOne.furniture.Count + " furniture clusters, with individual IDs: " + ids);
                //}
            }
        }

        //Parse sub object
        if(!wo && fsoi > -1 && furnitureParent != null && furnitureParent.furniture != null && subObject == null)
        {
            if(fsoi >= furnitureParent.furniture.subObjects.Count)
            {
                Game.LogError("Furniture sub object out of range! Are you sure this is the correct furniture? " + fsoi + "/" + furnitureParent.furniture.subObjects.Count + " spawning " + name + " on " + furnitureParent.furniture.name);
            }
            else
            {
                subObject = furnitureParent.furniture.subObjects[fsoi];
            }
        }

        //Parse door parent
        if(dp > -1 && parentTransform == null)
        {
            NewDoor foundDoor = null;

            if(CityData.Instance.doorDictionary.TryGetValue(dp, out foundDoor))
            {
                parentTransform = foundDoor.transform;
            }
            else
            {
                Game.LogError("Unable to find door: " + dp);
            }
        }

        //Set assign ID to highest
        worldAssignID = Mathf.Max(id + 1, worldAssignID);

        //Set in inventory
        if (inv > 0)
        {
            if(CityData.Instance.GetHuman(inv, out inInventory))
            {
                SetInInventory(inInventory);
            }
            else
            {
                Game.LogError("Unable to find receiver citizen ID " + inv + " for object " + preset.name + ". Are you trying to load this object before citizens are created?");
            }
        }

        //Dynamic fignerprints
        if(df != null && df.Count > 0)
        {
            if (!GameplayController.Instance.objectsWithDynamicPrints.Contains(this))
            {
                GameplayController.Instance.objectsWithDynamicPrints.Add(this);
            }
        }

        //Update position (+ assign node)
        //if (ml) Game.Log("Main light load: " + wPos + "/" + lPos + "/" + spWPos);

        //Load spawn position
        PathFinder.Instance.nodeMap.TryGetValue(spNode, out spawnNode);

        UpdateWorldPositionAndNode(false); //This will also assign the interactable to the correct node

        //Parse light
        if (lp != null && lp.Length > 0 && isLight == null)
        {
            Toolbox.Instance.LoadDataFromResources<LightingPreset>(lp, out isLight);
            SetAsLight(isLight, lzs, ml, lcd);
        }

        //Has this been moved?
        if (wPos != spWPos || wEuler != spWEuler)
        {
            mov = true;
        }
        else mov = false;

        //Mark to remove (add to trash)
        if(mtr > 0f)
        {
            MarkAsTrash(true, true, mtr);
        }

        //if (ml) Game.Log("Main light load after: " + wPos + "/" + lPos + "/" + spWPos);

        MainSetupEnd();
    }

    //Run after MainSetupStart, OnCreate or OnLoad. Both created & loaded run this.
    public void MainSetupEnd()
    {
        //Attempt to find evidence...
        if (evidence == null && preset.useEvidence)
        {
            NewGameLocation loc = null;
            if (node != null) loc = node.gameLocation;
            evidence = Toolbox.Instance.GetOrCreateEvidenceForInteractable(preset, "I" + id, this, belongsTo, writer, reciever, jobParent, loc, preset.retailItem, pv);

            //Override DDS
            if (dds != null && dds.Length > 0)
            {
                SetDDSOverride(dds);
            }
        }

        //Set reference to interactable (this shouldn't be needed but do it anyway)
        if (evidence != null)
        {
            evidence.interactable = this;
            evidence.interactablePreset = preset;

            //if(group != null)
            //{
            //    evidence.dataSource = group.dataSource; //Copy group's datasource
            //}
        }

        //If clock then listen for hour change
        if (preset.isClock)
        {
            SessionData.Instance.OnHourChange += OnHourChange;
        }

        //Add as hospital bed
        if (preset.specialCaseFlag == InteractablePreset.SpecialCase.hospitalBed)
        {
            if (!GameplayController.Instance.hospitalBeds.Contains(this))
            {
                GameplayController.Instance.hospitalBeds.Add(this);
            }
        }
        else if(preset.specialCaseFlag == InteractablePreset.SpecialCase.caseTray)
        {
            if(!CityData.Instance.caseTrays.Contains(this))
            {
                CityData.Instance.caseTrays.Add(this);
            }
        }

        //Add to parent's spawned interactables
        if (preset.spawnable && subObject != null)
        {
            furnitureParent.spawnedInteractables.Add(this);
        }
        else if(furnitureParent != null && subObject == null)
        {
            furnitureParent.integratedInteractables.Add(this);
        }

        //Load switch states
        if(wasLoadedFromSave && preset.dontLoadSwitchStates)
        {
            ResetToDefaultSwitchState();
        }
        else
        {
            SetSwitchState(sw0, null, false, forceUpdate: true);
            SetCustomState1(sw1, null, false, forceUpdate: true);
            SetCustomState2(sw2, null, false, forceUpdate: true);
            SetCustomState3(sw3, null, false, forceUpdate: true);
            SetLockedState(locked, null, false, forceUpdate: true);
        }

        SetPhysicsPickupState(phy, null, false, forceUpdate: true);

        UpdateCurrentActions();

        //Create reference dictionary
        foreach (InteractablePreset.InteractionAction action in preset.GetActions())
        {
            if (action.usableByAI)
            {
                aiActionReference.Add(action.action, action);
            }
        }

        //Spawn lock interactable
        if (preset.includeLock != null)
        {
            if (furnitureParent == null) Game.LogError("Unable to create lock for " + GetName() + " (" + id +") as furniture parent is null: " + fp);
            else
            {
                Vector3 relPos = preset.lockOffset;
                Vector3 relEuler = Vector3.zero;

                lockInteractable = InteractableCreator.Instance.CreateInteractableLock(preset.includeLock, furnitureParent, belongsTo, relPos, relEuler, InteractableController.InteractableID.A);
                lockInteractable.thisDoor = this;
                lockInteractable.SetLockedState(locked, null);
                lockInteractable.SetPasswordSource(passwordSource);
                lockInteractable.SetValue(Toolbox.Instance.GetPsuedoRandomNumber(Mathf.Clamp01(preset.lockStrength.x), Mathf.Clamp01(preset.lockStrength.y), seed)); //Value in this case is lock strength
                SetValue(lockInteractable.val);

                //Game.Log("Creating lock for " + GetName() + " (" + id + ") + : " + lockInteractable.GetWorldPosition());

                //Do this again to set the password source
                SetOwner(belongsTo, false);

                //Immediately spawn lock? Locks have their own spawning processs...
                if(loadedGeometry)
                {
                    SpawnLock();
                }
            }
        }
        else if(preset.isSelfLock)
        {
            lockInteractable = this;
            lockInteractable.thisDoor = this;
            //lockInteractable.SetLockedState(locked, null);
            lockInteractable.SetPasswordSource(passwordSource);
            SetValue(Toolbox.Instance.GetPsuedoRandomNumber(Mathf.Clamp01(preset.lockStrength.x), Mathf.Clamp01(preset.lockStrength.y), seed)); //Value in this case is lock strength

            //Do this again to set the password source
            SetOwner(belongsTo, false);
        }

        //Create usage point
        usagePoint = new UsagePoint(preset.useSetting, this, node);

        //Add special cases
        if (node != null)
        {
            if (preset.specialCaseFlag != InteractablePreset.SpecialCase.none)
            {
                if (!node.room.specialCaseInteractables.ContainsKey(preset.specialCaseFlag))
                {
                    node.room.specialCaseInteractables.Add(preset.specialCaseFlag, new List<Interactable>());
                }

                if(!node.room.specialCaseInteractables[preset.specialCaseFlag].Contains(this))
                {
                    node.room.specialCaseInteractables[preset.specialCaseFlag].Add(this);
                }
            }

            //Add security door reference
            if (preset.specialCaseFlag == InteractablePreset.SpecialCase.securityDoor)
            {
                if(node != null)
                {
                    if(node.floor != null)
                    {
                        node.floor.AddSecurityDoor(this);
                    }
                    else Game.LogError("Cannot place security door at node " + node.nodeCoord + " with no floor!");
                }
            }
            //Add alarm reference
            else if (preset.specialCaseFlag == InteractablePreset.SpecialCase.alarmSystem)
            {
                if(node != null)
                {
                    if(node.gameLocation != null)
                    {
                        if(node.gameLocation.thisAsAddress != null)
                        {
                            node.gameLocation.thisAsAddress.alarms.Add(this);
                        }

                        if (node.building != null)
                        {
                            node.building.alarms.Add(this);
                        }
                    }

                    SetValue(GetSecurityStrength()); //Set this for sabotage value
                }
            }
            //Add sentry gun
            else if (preset.specialCaseFlag == InteractablePreset.SpecialCase.sentryGun)
            {
                if(node != null)
                {
                    //Set camera view point and euler
                    cvp = wPos + new Vector3(0f, (furnitureParent.anchorNode.floor.defaultCeilingHeight * 0.1f) - 0.56f, 0f);
                    cve = wEuler + new Vector3(0, 0 * furnitureParent.scaleMultiplier.x, 0) + new Vector3(20, 0, 0);

                    if (node.gameLocation != null)
                    {
                        if (node.gameLocation.thisAsAddress != null)
                        {
                            node.gameLocation.thisAsAddress.AddSentryGun(this);
                        }

                        if (node.building != null)
                        {
                            node.building.AddSentryGun(this);
                        }
                    }

                    SetValue(GetSecurityStrength()); //Set this for sabotage value
                }
            }
            //Add other security
            else if (preset.specialCaseFlag == InteractablePreset.SpecialCase.otherSecuritySystem || preset.specialCaseFlag == InteractablePreset.SpecialCase.gasReleaseSystem)
            {
                if (node != null)
                {
                    if (node.gameLocation != null)
                    {
                        if (node.gameLocation.thisAsAddress != null)
                        {
                            node.gameLocation.thisAsAddress.AddOtherSecurity(this);
                        }

                        if (node.building != null)
                        {
                            node.building.AddOtherSecurity(this);
                        }
                    }

                    SetValue(GetSecurityStrength()); //Set this for sabotage value
                }
            }
            //Add security cam
            else if (preset.specialCaseFlag == InteractablePreset.SpecialCase.securityCamera)
            {
                //Set camera view point and euler
                float heightExtra = 0f;
                Vector3 extraEuler = Vector3.zero;

                if (node.building != null && furnitureParent != null && furnitureParent.anchorNode != null && furnitureParent.anchorNode.floor != null)
                {
                    heightExtra = (furnitureParent.anchorNode.floor.defaultCeilingHeight * 0.1f) - 0.57f; //Position from ceiling
                    extraEuler = new Vector3(20, 0, 0);
                }

                cvp = wPos + new Vector3(0f, heightExtra, 0f);
                cve = wEuler + new Vector3(0, 0 * furnitureParent.scaleMultiplier.x, 0) + extraEuler;

                if(!SessionData.Instance.isFloorEdit)
                {
                    sceneRecorder = new SceneRecorder(this);
                    if(node.building != null) node.building.AddSecurityCamera(this);
                    node.gameLocation.AddSecurityCamera(this);
                    CityData.Instance.surveillanceDirectory.Add(sceneRecorder);

                    //Update recorder refernce in captures
                    foreach(SceneRecorder.SceneCapture s in cap)
                    {
                        s.recorder = sceneRecorder;
                    }

                    foreach (SceneRecorder.SceneCapture s in sCap)
                    {
                        s.recorder = sceneRecorder;
                    }
                }

                force = true; //State needs to be saved to save captures...

                SetValue(GetSecurityStrength()); //Set this for sabotage value
            }
            //Add burnign barrel
            else if (preset.specialCaseFlag == InteractablePreset.SpecialCase.burningBarrel)
            {
                GameplayController.Instance.burningBarrels.Add(this);
            }
            //Add active codebreaker
            else if (preset.specialCaseFlag == InteractablePreset.SpecialCase.codebreaker)
            {
                GameplayController.Instance.activeGadgets.Add(this);
            }
            //Add active door wedge
            else if (preset.specialCaseFlag == InteractablePreset.SpecialCase.doorWedge)
            {
                GameplayController.Instance.activeGadgets.Add(this);
            }
            //Telephone
            else if (preset.specialCaseFlag == InteractablePreset.SpecialCase.telephone)
            {
                //Create new telephone
                Passed telephoneNumber = null;
                if(pv != null) telephoneNumber = pv.Find(item => item.varType == PassedVarType.phoneNumber);

                if (telephoneNumber == null)
                {
                    t = new Telephone(this); //Create new telephone & number

                    //Game.Log("Created new telephone for " + id + " number: " + t.number + " at " + node.gameLocation.name);

                    //Save the telephone number as a passed variable, this will save with the object
                    if (pv == null) pv = new List<Passed>();
                    pv.Add(new Passed(PassedVarType.phoneNumber, t.number));

                    force = true; //Force the saving of the state data
                }
                else
                {
                    //Game.Log("Loading new telephone for " + id + " number: " + telephoneNumber.value + " at " + node.gameLocation.name);

                    //Create a new telephone class and load in the saved number
                    if(t == null) t = new Telephone(this, Mathf.RoundToInt(telephoneNumber.value));
                }

                speechController = node.gameLocation.gameObject.AddComponent<SpeechController>();
                speechController.phoneLine = t;
                speechController.interactable = this;

                //Sync switch state with telephone engaged
                if (t.activeReceiver == null)
                {
                    if (t.activeCall.Count > 0)
                    {
                        SetSwitchState(true, null);
                    }
                    else
                    {
                        SetSwitchState(false, null);
                    }
                }
                else SetSwitchState(false, null);
            }
        }

        if (preset.specialCaseFlag == InteractablePreset.SpecialCase.fingerprint)
        {
            if (!GameplayController.Instance.confirmedPrints.ContainsKey(print[0].worldPos))
            {
                Game.Log("Player: Creating new fingerprint evidence at " + print[0].worldPos);
                GameplayController.Instance.confirmedPrints.Add(print[0].worldPos, this);
            }
        }
        else if (preset.specialCaseFlag == InteractablePreset.SpecialCase.footprint)
        {
            if (!GameplayController.Instance.confirmedFootprints.ContainsKey(print[0].worldPos))
            {
                Game.Log("Player: Creating new footprint evidence at " + print[0].worldPos);
                GameplayController.Instance.confirmedFootprints.Add(print[0].worldPos, this);
            }
        }

        //Steam controller: Set this before setting the starting swithc states
        if (preset.affectRoomSteamLevel && node != null && node.room.steamControllingInteractables != null)
        {
            node.room.steamControllingInteractables.Add(this);
        }

        //If this plays a looping sound then we need to force-update the switch on starting the game
        if (!SessionData.Instance.startedGame && preset.switchSFX.Exists(item => item.isLoop))
        {
            CityConstructor.Instance.updateSwitchState.Add(this);
        }

        //Grab owner's address and save this (needed incase owner's address is changed)
       if (preset.specialCaseFlag == InteractablePreset.SpecialCase.addressBook)
        {
            if (belongsTo != null && belongsTo.home != null)
            {
                if (pv == null) pv = new List<Passed>();
                pv.Add(new Passed(PassedVarType.addressID, belongsTo.home.id));
            }
        }

        //Game.Log("Object " + name + " is at original postion: " + originalPosition + " (" + wPos + " spawned at " + spWPos + ") Distance: " + distanceFromSpawn);

        isSetup = true;

        //Object has been removed from game world...
        if(rem)
        {
            SafeDelete();
            //Delete();
        }
        else if(rPl)
        {
            RemoveFromPlacement();
        }
    }

    //A simple check to see if this should spawn...
    public void SpawnCheck()
    {
        //Load if geometry is already loaded
        if (node != null && node.room != null && node.room.geometryLoaded)
        {
            if (furnitureParent != null)
            {
                if (furnitureParent.spawnedObject != null)
                {
                    LoadInteractableToWorld();
                }
            }
            else LoadInteractableToWorld();
        }
    }

    //Generate light data: Decide on colour, intesity etc
    public void GenerateLightData()
    {
        lcd = new LightConfiguration();

        Color chosenColour = Color.white;

        //Decide colour
        if (node != null && node.room != null && node.room.preset.cleanness <= 4)
        {
            chosenColour = Color.Lerp(isLight.warmColours[Toolbox.Instance.RandContained(0, isLight.warmColours.Count, seed, out seed)].colourOne,
                            isLight.warmColours[Toolbox.Instance.RandContained(0, isLight.warmColours.Count, seed, out seed)].colourTwo, Toolbox.Instance.RandContained(0f, 1f, seed, out seed));
        }
        else if (node != null && node.room != null && node.room.preset.cleanness >= 6)
        {
            chosenColour = Color.Lerp(isLight.coolColours[Toolbox.Instance.RandContained(0, isLight.coolColours.Count, seed, out seed)].colourOne,
            isLight.coolColours[Toolbox.Instance.RandContained(0, isLight.coolColours.Count, seed, out seed)].colourTwo, Toolbox.Instance.RandContained(0f, 1f, seed, out seed));
        }
        else
        {
            Color col1 = Color.Lerp(isLight.coolColours[Toolbox.Instance.RandContained(0, isLight.coolColours.Count, seed, out seed)].colourOne,
            isLight.warmColours[Toolbox.Instance.RandContained(0, isLight.warmColours.Count, seed, out seed)].colourOne, Toolbox.Instance.RandContained(0.33f, 0.66f, seed, out seed));

            Color col2 = Color.Lerp(isLight.coolColours[Toolbox.Instance.RandContained(0, isLight.coolColours.Count, seed, out seed)].colourTwo,
            isLight.warmColours[Toolbox.Instance.RandContained(0, isLight.warmColours.Count, seed, out seed)].colourTwo, Toolbox.Instance.RandContained(0.33f, 0.66f, seed, out seed));

            chosenColour = Color.Lerp(col1, col2, 0.5f);
        }

        lcd.colour = chosenColour;

        lcd.range = isLight.defaultRange;

        //Get intensity
        float intense = isLight.defaultIntensity;

        float intensityMP = 1f;
        if (node.room.lowerRoom != null) intensityMP = 1.33f;

        if (lzs < 0)
        {
            intense = (node.room.nodes.Count * intensityMP) * 100f - 50f;
        }
        else
        {
            intense = (lzs * intensityMP) * 100f - 50f;
        }

        if (node.room.preset.wellLit)
        {
            intense *= 1.5f;
            lcd.range *= 3f;
        }

        lcd.intensity = Mathf.Clamp(intense, isLight.intensityRange.x, isLight.intensityRange.y);

        lcd.flickerColourMultiplier = Toolbox.Instance.RandContained(isLight.flickerMultiplierRange.x, isLight.flickerMultiplierRange.y, seed, out seed);
        lcd.pulseSpeed = Toolbox.Instance.RandContained(isLight.flickerPulseRange.x, isLight.flickerPulseRange.y, seed, out seed);
        lcd.intervalTime = Toolbox.Instance.RandContained(isLight.flickerNormalityIntervalRange.x, isLight.flickerNormalityIntervalRange.y, seed, out seed);

        //Setup flicker
        float chanceExtra = 0f;

        if (node.room != null)
        {
            if (node.room.gameLocation.floor != null)
            {
                if (node.room.gameLocation.floor.floor <= CityControls.Instance.lowestFloor)
                {
                    chanceExtra = CityControls.Instance.lowestFloorIncreaseFlickerChance;
                }
            }
        }

        if (isLight.chanceOfFlicker + chanceExtra >= Toolbox.Instance.RandContained(0f, 1f, seed, out seed))
        {
            lcd.flicker = true;
        }
        else
        {
            lcd.flicker = false;
        }
    }

    //Set material key
    public void SetMaterialKey(Toolbox.MaterialKey newMatKey)
    {
        mk = newMatKey;

        if(controller != null)
        {
            foreach(MeshRenderer rend in controller.meshes)
            {
                MaterialsController.Instance.ApplyMaterialKey(rend, mk);
            }
        }
        else if(spawnedObject != null)
        {
            MaterialsController.Instance.ApplyMaterialKey(spawnedObject, mk);
        }
    }

    //Set the object ref polymorphic reference
    public void SetPolymorphicReference(object newRef)
    {
        objectRef = newRef;
    }

    //Set the value of this
    public void SetValue(float newValue)
    {
        val = newValue;
        if (preset == null) return;
        if (preset.isMoney) UpdateName(); //Update the name if money as it needs to be named after the amount.

        //Set door to jammed here...
        if(preset.specialCaseFlag == InteractablePreset.SpecialCase.doorWedge)
        {
            NewDoor d = null;

            if(CityData.Instance.doorDictionary.TryGetValue((int)val, out d))
            {
                objectRef = d;
                d.SetJammed(true, this);
            }
        }
        //else if(preset.specialCaseFlag == InteractablePreset.SpecialCase.codebreaker)
        //{
        //    objectRef = CityData.Instance.interactableDirectory.Find(item => item.id == (int)val);
        //    if (objectRef == null) Game.LogError("Codebreaker cannot find object reference " + (int)val);
        //}

        if(preset.specialCaseFlag == InteractablePreset.SpecialCase.breakerLights || preset.specialCaseFlag == InteractablePreset.SpecialCase.breakerDoors || preset.specialCaseFlag == InteractablePreset.SpecialCase.breakerSecurity)
        {
            if(val >= 1f)
            {
                //Game.Log("Debug: Set breaker open " + val);
                GameplayController.Instance.closedBreakers.Remove(this);
                if(!sw0) SetSwitchState(true, null, true, true, false);
            }
            else
            {
                //Game.Log("Debug: Set breaker closed " + val);

                //If breaker is off, make sure it's 'active'
                if (!GameplayController.Instance.closedBreakers.Contains(this))
                {
                    GameplayController.Instance.closedBreakers.Add(this);
                }

                if(sw0) SetSwitchState(false, null, true, true, false);
            }

            if (controller != null && controller.doorMovement != null) controller.doorMovement.SetOpen(val, null);
        }

        if(thisDoor != null)
        {
            thisDoor.val = val;
        }
        else if(lockInteractable != null)
        {
            lockInteractable.val = val;
        }
    }

    //Set a note/dds override
    public void SetDDSOverride(string newTreeID)
    {
        if(newTreeID.Length > 0)
        {
            dds = newTreeID;

            if (pv == null)
            {
                pv = new List<Passed>();
                pv.Add(new Passed(PassedVarType.ddsOverride, 0, newTreeID));
            }
            else
            {
                Passed passedDDS = pv.Find(item => item.varType == PassedVarType.ddsOverride);

                if (passedDDS != null)
                {
                    passedDDS.str = newTreeID;
                }
                else
                {
                    pv.Add(new Passed(PassedVarType.ddsOverride, 0, newTreeID));
                }
            }

            if (evidence != null)
            {
                evidence.SetOverrideDDS(dds);
            }
            else
            {
                Game.LogError("Trying to set DDS override but there is not (yet) any evidence: " + name);
            }
        }
    }

    //Assign an interactable ID (world object) Starts at 100000000 to avoid conflict with assigning IDs of room objects, which are comprised of room IDs * 1000
    //This is done to avoid thread race conditions in generation time outputting different IDs for objects (essentially non-predictable generation)
    //Threads should use another version of ID assign to avoid race conditions (see below)
    public void AssignIDWorld()
    {
        id = worldAssignID;

        //If this exists already, add to ID to avoid conflicts
        while(CityData.Instance.savableInteractableDictionary.ContainsKey(id) || CityData.Instance.metaObjectDictionary.ContainsKey(id))
        {
            id++;
        }

        worldAssignID = id + 1;

        //if(CityConstructor.Instance != null && CityConstructor.Instance.loadState == CityConstructor.LoadState.generateInteriors)
        //{
        //    Game.LogError("Trying to assign a world id to an interactable while multi-threading!");
        //}
    }

    //Assigns an ID based on room ID then counts up as it is filled; this makes this process thread-safe as threads are used to process on a room-by-room basis
    //Each room can support 1000 objects
    public void AssignRoomBasedID(NewRoom r)
    {
        id = (r.roomID * 1000) + r.interactableAssignID;

        //If we run out of space in the room's 1000 limit, we can safely assume this object is being created after the generation of the game;
        //Therefore we can actually use world ID instead...
        if (r.interactableAssignID >= 1000 || CityData.Instance.savableInteractableDictionary.ContainsKey(id) || CityData.Instance.metaObjectDictionary.ContainsKey(id))
        {
            Game.LogError("Error trying to assign room id of " + id + " to room " + r.roomID + ", so using world ID instead... (This should be fine if the game has started)");
            AssignIDWorld();
        }
        else
        {
            r.interactableAssignID++;

            if (Game.Instance.devMode && Game.Instance.collectDebugData)
            {
                r.itemsPlaced += ", " + preset.presetName;
            }
        }
    }

    //Assigns an ID based on the parent's furniture ID; this is used to reliably recreate versions of furniture integrated interactables (not saved with the city but may be saved with state data)
    //Use negative ID values for this to avoid conflicts with the two above ID ranges
    //Each piece of furniture can support 29 integrated interactables as the counter starts at 1, so the overall assigned ID here is always negative
    public void AssignFurnitureBasedID(FurnitureLocation f)
    {
        if (f == null)
        {
            Game.LogError("Unable to assign furntiure integrated ID as a null furniture reference has been passed!");
            return;
        }

        id = -((f.id * 30) + f.integratedIDAssign);
        f.integratedIDAssign++;
    }

    //Move the interactable within the world: Only do this if NO furniture parent
    public void MoveInteractable(Vector3 newWorldPos, Vector3 newEulerAngle, bool updateSpawnPosition)
    {
        if(!wo) ConvertToWorldObject(false);

        wPos = newWorldPos;
        wEuler = newEulerAngle;
        lPos = newWorldPos;
        lEuler = newEulerAngle;

        mov = true;

        UpdateWorldPositionAndNode(updateSpawnPosition); //Don't update spawn position

        //Set capture update needed
        //if (preset.includeInSceneCaptures && node != null)
        //{
        //    node.room.fullCaptureNeeded = true;
        //}
    }

    //Set a new parent and/or world position & rotation: Only do this if NO furniture parent
    public void SetNewPositionAndParent(Transform newParent, Vector3 newLocalPos, Vector3 newLocalEuler, bool updateSpawnPosition)
    {
        furnitureParent = null;
        parentTransform = newParent;
        lPos = newLocalPos;
        lEuler = newLocalEuler;

        //Update physical positions
        if(controller != null)
        {
            controller.transform.SetParent(newParent, true);
            controller.transform.localPosition = lPos;
            controller.transform.localEulerAngles = lEuler;
        }

        UpdateWorldPositionAndNode(updateSpawnPosition); //Don't update spawn position

        //Set capture update needed
        //if (preset.includeInSceneCaptures && node != null)
        //{
        //    node.room.fullCaptureNeeded = true;
        //}
    }

    public void SetNewPosition(Vector3 newLocalPos, Vector3 newLocalEuler, bool updateSpawnPosition)
    {
        lPos = newLocalPos;
        lEuler = newLocalEuler;

        //Update physical positions
        if (controller != null)
        {
            controller.transform.localPosition = lPos;
            controller.transform.localEulerAngles = lEuler;
        }

        UpdateWorldPositionAndNode(updateSpawnPosition); //Don't update spawn position

        //Set capture update needed
        //if (preset.includeInSceneCaptures && node != null)
        //{
        //    node.room.fullCaptureNeeded = true;
        //}
    }

    //Get an accurate position
    public Vector3 GetWorldPosition(bool useSpawnedPosition = true)
    {
        if(controller != null && useSpawnedPosition)
        {
            return controller.transform.position;
        }

        return wPos;
    }

    //Call when the parent transform moves or the interactable's parent changes in order to keep accurate position
    public Vector3 UpdateWorldPositionAndNode(bool updateSpawnPosition)
    {
        NewNode newNode = null;

        if (isActor != null)
        {
            wPos = isActor.transform.position;
            wEuler = isActor.transform.eulerAngles;
            newNode = isActor.currentNode; //Node rules: Use the actor's node always
        }
        else if(inInventory != null)
        {
            wPos = inInventory.transform.position;
            wEuler = inInventory.transform.eulerAngles;
            newNode = inInventory.currentNode; //Node rules: Use the actor's node always
        }
        else if (wo)
        {
            //wPos = lPos;
            //wEuler = lEuler;

            Vector3 colliderPos = wPos;

            //Use collider position to dictate room parents...
            if(controller != null && controller.coll != null)
            {
                colliderPos = controller.coll.bounds.center;
            }

            //If this reaches below the bottom of the world, then destroy
            if ((wPos.y <= -50f || wPos.y > 1000f) && (InteractionController.Instance.carryingObject == null || InteractionController.Instance.carryingObject.interactable != this))
            {
                if (controller != null)
                {
                    controller.SetPhysics(false); //Make sure physics is off
                }

                Game.Log("Removing " + name + " as it is ouside of gameworld: " + wPos);
                SafeDelete();
                return Vector3.zero;
            }
            //Auto-attempt at finding node
            else if (!PathFinder.Instance.nodeMap.TryGetValue(CityData.Instance.RealPosToNodeInt(colliderPos), out newNode))
            {
                newNode = Toolbox.Instance.FindClosestValidNodeToWorldPosition(colliderPos, false, safety: 2000);

                float distance = 99999f;

                if (newNode != null)
                {
                    distance = Vector3.Distance(colliderPos, newNode.position);

                    string physicsDebug = string.Empty;

                    if(controller != null)
                    {
                        physicsDebug = "Physics on: " + controller.physicsOn + ", RB: " + controller.rb;
                    }

                    Game.Log(name + " " + id + ": Found closest node to world position " + colliderPos + " manually: " + newNode.position + " distance: " + distance + ". This could be very slow and should be corrected. " + physicsDebug);
                }
                else
                {
                    Game.LogError("Unable to find node for " + name + " at " + colliderPos);
                }

                if(distance > 35f && (InteractionController.Instance.carryingObject == null || InteractionController.Instance.carryingObject.interactable != this))
                {
                    Game.LogError("Distance is too far away from anything (" + distance + "), removing interactable " + preset.name + " " + name + " " + id + ": If this is important, make sure this doesn't happen!");
                    SafeDelete();
                }
            }

            //Set proper parents
            if (newNode != null)
            {
                //trigger change of room (only do this if not held)
                if (worldObjectRoomParent != newNode.room)// && (controller == null || InteractionController.Instance.carryingObject == null || InteractionController.Instance.carryingObject != controller))
                {
                    //Remove from previous
                    if (worldObjectRoomParent != null)
                    {
                        worldObjectRoomParent.worldObjects.Remove(this);
                    }

                    worldObjectRoomParent = newNode.room;
                    parentTransform = newNode.room.transform;

                    //Change physical parent
                    if (controller != null && preset.apartmentPlacementMode == InteractablePreset.ApartmentPlacementMode.physics)
                    {
                        parentTransform = newNode.room.transform; //Moved this in here as it's not compatible with multithreading

                        //Game.Log(name + " set parent transform to " + parentTransform);
                        controller.transform.SetParent(parentTransform, true);
                    }

                    if (!worldObjectRoomParent.worldObjects.Contains(this))
                    {
                        worldObjectRoomParent.worldObjects.Add(this);
                    }
                }
            }

            //Update true local position, euler should remain the same as world
            if (parentTransform != null)
            {
                lPos = parentTransform.InverseTransformPoint(wPos);
            }
        }
        else if (parentTransform != null && furnitureParent == null)
        {
            wPos = parentTransform.TransformPoint(lPos);
            wEuler = parentTransform.eulerAngles + lEuler;

            //If this reaches below the bottom of the world, then destroy
            if (wPos.y <= -100f)
            {
                if (controller != null)
                {
                    controller.SetPhysics(false); //Make sure physics is off

                    //Remove object completely
                    SafeDelete();

                    return Vector3.zero;
                }
            }

            //Auto-attempt at finding node
            if (!PathFinder.Instance.nodeMap.TryGetValue(CityData.Instance.RealPosToNodeInt(wPos), out newNode))
            {
                //Use furniture
                if (furnitureParent != null)
                {
                    //Pick node from closest furniture node
                    newNode = furnitureParent.anchorNode;
                    float distance = Mathf.Infinity;

                    foreach (NewNode f in furnitureParent.coversNodes)
                    {
                        float dist = Vector3.Distance(node.position, wPos);

                        if (dist < distance)
                        {
                            newNode = f;
                            distance = dist;
                        }
                    }
                }
                else
                {
                    newNode = Toolbox.Instance.FindClosestValidNodeToWorldPosition(wPos, false);

                    if (newNode != null)
                    {
                        Game.Log("Found closest node to world position " + wPos + " manually: " + newNode.position + " distance: " + Vector3.Distance(wPos, newNode.position) + ". This could be very slow and should be corrected");
                    }
                    else
                    {
                        Game.LogError("Unable to find node for " + name + " at " + wPos);
                    }
                }
            }
        }
        //Node rules are slightly different for furniture parents: Pick the closest node to current position
        else if (furnitureParent != null)
        {
            //As we now have positions relative to the furniture object, we can use a matrix to rotate
            //Find world position and rotation using the furniture parent
            Matrix4x4 m = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(new Vector3(0, furnitureParent.angle + furnitureParent.diagonalAngle, 0)), Vector3.one);

            wPos = m.MultiplyPoint3x4(lPos) + furnitureParent.anchorNode.position;
            wEuler = new Vector3(0, furnitureParent.angle + furnitureParent.diagonalAngle, 0) + lEuler;

            //Pick node from closest world node but keep furniture floor postion
            newNode = Toolbox.Instance.FindClosestValidNodeToWorldPosition(wPos, false, limitToFloor: true, limitedToFloor: (int)furnitureParent.anchorNode.nodeCoord.z);

            //Pick node from closest furniture node
            if (newNode == null)
            {
                Game.Log(name + " unable to find a close by node, using furniture node area...");

                newNode = furnitureParent.anchorNode;

                if (furnitureParent.coversNodes.Count > 0)
                {
                    float distance = Mathf.Infinity;

                    foreach (NewNode f in furnitureParent.coversNodes)
                    {
                        float dist = Vector3.Distance(f.position, wPos);

                        if (dist < distance)
                        {
                            newNode = f;
                            distance = dist;
                        }
                    }
                }
            }
        }
        else
        {
            //newNode = Toolbox.Instance.FindClosestValidNodeToWorldPosition(wPos, false);
        }

        //Update node
        if (isActor == null)
        {
            if(newNode == null)
            {
                //Game.LogError("Unable to find node for " + preset.name + " parent transform: " + parentTransform + " furn parent: " + furnitureParent);
            }

            //If this is different to the previous, remove it first...
            //Don't do this for characters
            if (newNode != node)
            {
                if (node != null)
                {
                    node.RemoveInteractable(this);
                }

                if (newNode != null)
                {
                    node = newNode; //Set node
                    newNode.AddInteractable(this);
                }
            }

            //Update physical position
            if (spawnedObject != null)
            {
                //Game.Log("Set pos 2: " + locPosition);
                spawnedObject.transform.localPosition = lPos;
                spawnedObject.transform.localEulerAngles = lEuler;
            }

            //Update usage point location
            if(usagePoint != null)
            {
                usagePoint.PositionUpdate();
            }
        }
        else
        {
            node = isActor.currentNode;
        }

        if(Game.Instance.collectDebugData && controller != null)
        {
            controller.debugLocalEuler = lEuler;
            controller.debugLocalPos = lPos;

            if (node != null)
            {
                controller.debugNodeCoord = node.nodeCoord;
                controller.debugRoom = node.room;
            }

            controller.debugInteractablePredictedWorldPos = wPos;
        }

        if(updateSpawnPosition)
        {
            //Can only do this if the object has a saved idenifier...
            if (save)
            {
                if (isSetup) spCh = true; //Set save flag
            }

            spawnNode = node;
            if (node != null) spNode = node.nodeCoord;
            spawnParent = parentTransform;
            spWPos = wPos;
            spWEuler = wEuler;
            originalPosition = true; //This is now the original position
            distanceFromSpawn = 0; //Reset distance from spawn
            SetTampered(false);
        }
        else
        {
            //While active, measure distance from spawn
            distanceFromSpawn = Vector3.Distance(wPos, spWPos);

            if(distanceFromSpawn > 0.01f)
            {
                SetOriginalPosition(false, false);
            }
            else
            {
                originalPosition = true; //This is now the original position
                distanceFromSpawn = 0; //Reset distance from spawn
            }

            if (preset.physicsProfile != null && preset.tamperEnabled)
            {
                if (distanceFromSpawn >= GameplayControls.Instance.physicsTamperDistance + preset.physicsProfile.tamperDistanceModifier)
                {
                    if (!isTampered)
                    {
                        SetTampered(true);
                    }
                }
                else if (isTampered)
                {
                    SetTampered(false);
                }
            }
        }

        //Update looping audio
        if(loopingAudio != null)
        {
            foreach(AudioController.LoopingSoundInfo loop in loopingAudio)
            {
                loop.UpdateWorldPosition(wPos, node);
            }
        }

        if (isSetup && (wPos != spWPos || wEuler != spWEuler))
        {
            mov = true;
        }
        else mov = false;

        //debugGeneralSetup = true;

        return wPos;
    }

    //Set password
    public void SetPasswordSource(object newPSource)
    {
        passwordSource = newPSource;
        //Game.Log("Set password source for " + id + "  newPSource");

        //Set isUsed flag so we know when to write password notes.
        if (newPSource == this) passcode.used = true;
        else if (newPSource as NewAddress != null) (newPSource as NewAddress).passcode.used = true;
        else if (newPSource as NewRoom != null) (newPSource as NewRoom).passcode.used = true;
        else if (newPSource as Human != null) (newPSource as Human).passcode.used = true;

        if(Game.Instance.collectDebugData && controller != null)
        {
            controller.debugPasswordSource = passwordSource;
        }
    }

    //Set a new owner
    public void SetOwner(Human newOwner, bool updateName = true)
    {
        belongsTo = newOwner;
        if(updateName) UpdateName();

        if (lockInteractable != null && lockInteractable != this)
        {
            //Game.Log("...Lock interactable has been created for " + id);

            //Only set the password source if it hasn't been set yet. Doors will set this first, and this would override that.
            if(passwordSource == null)
            {
                lockInteractable.SetOwner(belongsTo);

                //Set the password source
                if (preset.passwordSource == RoomConfiguration.RoomPasswordPreference.interactableBelongsTo && belongsTo != null)
                {
                    SetPasswordSource(belongsTo);
                    lockInteractable.SetPasswordSource(belongsTo);

                    ////Copy from belongs to...
                    //passcode = new GameplayController.Passcode(GameplayController.PasscodeType.interactable);
                    //passcode.id = id;
                    //passcode.used = true;
                    //passcode.digits = new List<int>(belongsTo.passcode.digits);

                    //SetPasswordSource(this);
                    //lockInteractable.SetPasswordSource(this);
                }
                else if (preset.passwordSource == RoomConfiguration.RoomPasswordPreference.thisRoom && node != null)
                {
                    SetPasswordSource(node.room);
                    lockInteractable.SetPasswordSource(node.room);
                }
                else if (preset.passwordSource == RoomConfiguration.RoomPasswordPreference.thisAddress && node != null)
                {
                    SetPasswordSource(node.room.gameLocation);
                    lockInteractable.SetPasswordSource(node.room.gameLocation);
                }
            }
        }

        if(newOwner != null)
        {
            b = newOwner.humanID;

            if (preset.specialCaseFlag == InteractablePreset.SpecialCase.sleepPosition)
            {
                newOwner.SetBed(this);
            }

            if (newOwner.job != null)
            {
                if (newOwner.job.preset.ownsWorkPosition)
                {
                    if (newOwner.job.preset.jobPostion != InteractablePreset.SpecialCase.none)
                    {
                        if (newOwner.job.preset.jobPostion == preset.specialCaseFlag)
                        {
                            newOwner.SetWorkFurniture(this);
                        }
                    }
                }
            }
        }
        else
        {
            b = -1;
        }

        if (Game.Instance.collectDebugData && controller != null)
        {
            controller.debugOwnedBy = belongsTo;
            controller.debugWrittenBy = writer;
            controller.debugReceivedBy = reciever;

            if(furnitureParent != null)
            {
                controller.debugFurnitureOwnedBy = furnitureParent.debugOwners;
            }
        }
    }

    //Set writer
    public void SetWriter(Human newWriter)
    {
        writer = newWriter;

        //if(preset.name == "Note")
        //{
        //    Game.Log(name + " Set writer to " + writer + " id: " + id);
        //}

        if (writer != null)
        {
            w = writer.humanID;
        }
        else
        {
            w = -1;
        }
    }

    //Set reciver
    public void SetReciever(Human newReciever)
    {
        reciever = newReciever;

        if (reciever != null)
        {
            r = reciever.humanID;
        }
        else
        {
            r = -1;
        }
    }

    public string GetName()
    {
        string ret = string.Empty;

        if (nEvKey > -1 && evidence != null)
        {
            ret = evidence.GetNameForDataKey((Evidence.DataKey)nEvKey);
        }
        //Auto name from preset
        else if (preset.autoName)
        {
            if (preset.includeBelongsTo)
            {
                if (belongsTo != null)
                {
                    if (preset.useApartmentName && belongsTo.home != null)
                    {
                        ret = belongsTo.home.name + " ";
                    }
                    else
                    {
                        if (preset.useNameShorthand)
                        {
                            ret = belongsTo.GetInitialledName() + " ";
                        }
                        else
                        {
                            ret = belongsTo.GetCitizenName() + " ";
                        }
                    }
                }
                else if (preset.useApartmentName)
                {
                    //The object ref should hold a reference to the address this belongs to...
                    if (objectRef != null)
                    {
                        NewAddress b = objectRef as NewAddress;

                        if (b != null)
                        {
                            ret = b.name + " ";
                        }
                    }
                }
            }

            //Special cases...
            if (preset.isMoney)
            {
                ret = CityControls.Instance.cityCurrency + val.ToString();
            }
            else if (book != null)
            {
                DDSSaveClasses.DDSMessageSave msg = null;

                if (Toolbox.Instance.allDDSMessages.TryGetValue(book.ddsMessage, out msg))
                {
                    DDSSaveClasses.DDSBlockCondition nameBlock = msg.blocks.Find(item => item.alwaysDisplay);

                    if (nameBlock != null) ret = Strings.Get("dds.blocks", nameBlock.blockID, forceNoWrite: true);
                }
            }
            else
            {
                ret += Strings.Get("evidence.names", preset.presetName);
            }
        }
        else
        {
            //Is door?
            NewDoor d = objectRef as NewDoor;

            if(d != null)
            {
                NewRoom playerOtherSideRoom = d.wall.node.room;

                if (playerOtherSideRoom == Player.Instance.currentRoom)
                {
                    playerOtherSideRoom = d.wall.otherWall.node.room;
                }

                if (playerOtherSideRoom.gameLocation != Player.Instance.currentGameLocation)
                {
                    ret = playerOtherSideRoom.gameLocation.evidenceEntry.GetNameForDataKey(Evidence.DataKey.location);
                }
                else
                {
                    ret = playerOtherSideRoom.GetName();
                }
            }
        }

        return ret;
    }

    //Get and update name
    public void UpdateName(bool assignNewNamingEvidenceKey = false, Evidence.DataKey newKey = Evidence.DataKey.name)
    {
        if(assignNewNamingEvidenceKey)
        {
            nEvKey = (int)newKey;
        }

        name = GetName();

        //Copy name to controller
        if (controller != null)
        {
            controller.name = name;
            controller.gameObject.name = name;

            if (InteractionController.Instance.currentLookingAtInteractable == controller)
            {
                InteractionController.Instance.UpdateInteractionText();
            }
        }
    }

    //Set as in inventory
    public void SetInInventory(Human newActor)
    {
        if(node != null)
        {
            node.RemoveInteractable(this);
            node.room.tamperedInteractables.Remove(this);
        }

        node = null;

        if (Game.Instance.collectDebugData)
        {
            if (newActor == Player.Instance) Game.Log("Player: Set in inventory: " + this.name);
            else if (newActor != null) newActor.SelectedDebug("Set in inventory: " + this.name, Actor.HumanDebug.actions);
        }

        //Remove from previous inventory
        if (newActor != inInventory && inInventory != null)
        {
            //Add fingerprints
            AddNewDynamicFingerprint(inInventory, PrintLife.timed);
            AddNewDynamicFingerprint(inInventory, PrintLife.timed);

            //Game.Log("Remove " + name + " from inventory of " + inInventory.name);
            inInventory.inventory.Remove(this);
            
            if(inInventory.ai != null && inInventory.ai.currentWeapon == this)
            {
                inInventory.ai.UpdateCurrentWeapon();
            }
        }

        if(newActor != null)
        {
            //Game.Log("Add " + name + " from inventory of " + newActor.name);
            newActor.inventory.Add(this);

            SetOriginalPosition(false, true);

            //Switch weapons
            if(newActor.ai != null)
            {
                newActor.ai.UpdateCurrentWeapon();
            }
        }

        inInventory = newActor;

        if(inInventory != null)
        {
            //Once this has moved, it is no longer part of a furniture spawn...
            //fp = -1;
            //fsoi = -1;
            ConvertToWorldObject(false);

            inv = inInventory.humanID;
            RemoveFromPlacement();

            //Add fingerprints
            if(SessionData.Instance.startedGame)
            {
                AddNewDynamicFingerprint(inInventory, PrintLife.timed);
                AddNewDynamicFingerprint(inInventory, PrintLife.timed);
            }

            if (inInventory.ai != null && preset.inventoryCarryItem) inInventory.ai.UpdateHeldItems(AIActionPreset.ActionStateFlag.none);
        }

        //Trigger tracking device
        if (preset.specialCaseFlag == InteractablePreset.SpecialCase.tracker)
        {
            //Pick a colour
            List<Interactable> trackers = CityData.Instance.interactableDirectory.FindAll(item => item.preset.specialCaseFlag == InteractablePreset.SpecialCase.tracker);
            trackers.Sort((p1, p2) => p1.id.CompareTo(p2.id)); //lowest first
            int index = trackers.IndexOf(this);
            index = Mathf.Clamp(index % PrefabControls.Instance.motionTrackerColors.Count, 0, PrefabControls.Instance.motionTrackerColors.Count - 1);

            Color col = PrefabControls.Instance.motionTrackerColors[index];

            MapController.Instance.AddNewTrackedObject(inInventory.transform, PrefabControls.Instance.mapCharacterMarker, new Vector2(22, 22), col, true, inInventory.evidenceEntry);

            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "Tracking citizen..."), InterfaceControls.Icon.location);
        }
    }

    //Place an item after being in inventory
    public void SetAsNotInventory(NewNode newNode)
    {
        Human wasInInventory = null;

        //Remove from players inventory
        if(inInventory != null)
        {
            wasInInventory = inInventory;

            //Disable tracking device
            if (preset.specialCaseFlag == InteractablePreset.SpecialCase.tracker)
            {
                MapController.Instance.RemoveTrackedObject(inInventory.transform);
            }

            if (inInventory.isPlayer)
            {
                for (int i = 0; i < FirstPersonItemController.Instance.slots.Count; i++)
                {
                    FirstPersonItemController.InventorySlot slot = FirstPersonItemController.Instance.slots[i];

                    if(slot.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic && slot.GetInteractable() == this)
                    {
                        FirstPersonItemController.Instance.EmptySlot(slot, false, false, false, playSound: false);
                    }
                }
            }
        }

        if(node != null) node.RemoveInteractable(this);
        node = newNode;
        SetInInventory(null);
        if (newNode != null) newNode.AddInteractable(this);
        inInventory = null;
        inv = 0;
        rPl = false;

        //Switch weapons
        if (wasInInventory != null && wasInInventory.ai != null)
        {
            wasInInventory.ai.UpdateCurrentWeapon();
        }

        //Game.Log("Set " + name + " as not in inventory, at node in " + newNode.room.name + " room parent: " + worldObjectRoomParent.name);
    }

    //Update which actions are currently active
    public void UpdateCurrentActions()
    {
        //There's no reason to do this unless the player is looking @ this...
        if(InteractionController.Instance.lockedInInteraction != this)
        {
            if (InteractionController.Instance.currentLookingAtInteractable == null || InteractionController.Instance.currentLookingAtInteractable.interactable != this)
            {
                //Game.Log("Object: Cancel looking at (player is looking at something else/null: " + Player.Instance.currentLookingAtInteractable + ")");
                return;
            }
        }

        //Game.Log("Object: Update actions for interactable " + name);

        List<InteractablePreset.InteractionAction> actionList = null;

        //If locked-in interaction, use that list
        if(InteractionController.Instance.lockedInInteraction == this)
        {
            actionList = preset.GetActions(InteractionController.Instance.lockedInInteractionRef + 1);
        }
        //Otherwise use the normal list
        else
        {
            actionList = preset.GetActions();
        }

        //Sort list by priority
        actionList.Sort((p1, p2) => p2.action.inputPriority.CompareTo(p1.action.inputPriority)); //Using P2 first gives highest first

        //Create a blank list for each key
        foreach (InteractablePreset.InteractionKey key in InteractionController.Instance.allInteractionKeys)
        {
            if(!currentActions.ContainsKey(key))
            {
                currentActions.Add(key, new InteractableCurrentAction());
            }

            currentActions[key].enabled = false;
            currentActions[key].display = false;
        }

        for (int i = 0; i < actionList.Count; i++)
        {
            InteractablePreset.InteractionAction pre = actionList[i];

            if (pre.GetInteractionKey() == InteractablePreset.InteractionKey.none) continue; //Hidden action, skip...

            //Find the correct entry in current actions
            InteractableCurrentAction currentAction = currentActions[pre.GetInteractionKey()];
            if (currentAction == null) continue;

            //If this is already enabled with prior action, then don't overwrite...
            if (currentAction.enabled)
            {
                //Game.Log("... Already enabled, skipping");
                continue;
            }

            //If this uses strikethrough, it will be shown regardless
            if (pre.useStrikethrough)
            {
                currentAction.display = true; //Display as strikethrough if disabled this way
            }

            //This action is force disabled...
            //Actions can be disabled on the interactable...
            if (disabledActions.Contains(pre))
            {
                if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " In disabled actions (interactable)");
                currentAction.enabled = false; //Disable this
                currentAction.currentAction = pre; //Set as current if displayed
                continue;
            }

            //Disable certain actions if this KO'd
            if (isActor != null)
            {
                if(isActor.ai != null)
                {
                    if(isActor.ai.ko || isActor.isAsleep || isActor.isStunned)
                    {
                        if(currentAction.currentAction != null && currentAction.currentAction.interactionName == "Talk To")
                        {
                            if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " disabled because " + isActor.ai.ko + " " +  isActor.ai.restrained + " " + isActor.isAsleep + " " + isActor.isStunned);
                            currentAction.enabled = false; //Disable this
                            currentAction.currentAction = pre; //Set as current if displayed
                            continue;
                        }
                    }
                }
            }

            //...Or banned completely by the player class
            if (Player.Instance.disabledActions.Contains(pre.interactionName.ToLower()))
            {
                if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " In disabled actions (player)");
                currentAction.enabled = false; //Disable this
                currentAction.currentAction = pre; //Set as current if displayed
                continue;
            }

            //Display while locked in interaction. Do this before the strikethrough check.
            if (!pre.availableWhileLockedIn && InteractionController.Instance.lockedInInteraction != null)
            {
                if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available while locked in");
                currentAction.enabled = false; //Disable this
                currentAction.currentAction = pre; //Set as current if displayed
                continue;
            }

            if (!pre.availableWhileJumping && !Player.Instance.fps.m_CharacterController.isGrounded)
            {
                if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available while jumping");
                currentAction.enabled = false; //Disable this
                currentAction.currentAction = pre; //Set as current if displayed
                continue;
            }

            if (!pre.availableWhileIllegal)
            {
                if (Player.Instance.illegalStatus)
                {
                    if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available while illegal (player illegal state)");
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
                else if(!pre.availableWhileWitnessesToIllegal && Player.Instance.witnessesToIllegalActivity.Count > 0)
                {
                    bool allow = true;

                    foreach(Actor a in Player.Instance.witnessesToIllegalActivity)
                    {
                        if (a.isAsleep || a.isDead || a.isStunned || a.isMachine) continue;

                        if(a.ai != null)
                        {
                            if (a.ai.restrained) continue;
                        }

                        allow = false;
                    }

                    if(!allow)
                    {
                        if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available while illegal (witness to illegal activity)");
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
            }

            if (pre.actionCost > 0)
            {
                if (GameplayController.Instance.money < pre.actionCost)
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }

            if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.onlyAvailableInFastForward)
            {
                if (!Player.Instance.spendingTimeMode)
                {
                    if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Time isn't in fast forward");
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.specialCase != InteractablePreset.InteractionAction.SpecialCase.availableInFastForward)
            {
                if (Player.Instance.spendingTimeMode)
                {
                    if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Time is in fast forward");
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }

            if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.onlyIfDeadAsleepOrUncon)
            {
                if (isActor != null)
                {
                    if(!isActor.isAsleep && !isActor.isDead && !isActor.isStunned )
                    {
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.onlyInNormalTimeAndAwakeNonDialog)
            {
                if (Player.Instance.spendingTimeMode || Player.Instance.playerKOInProgress || InteractionController.Instance.dialogMode)
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.nonDialog)
            {
                if (InteractionController.Instance.dialogMode)
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.search)
            {
                if (isActor != null)
                {
                    if(isActor.ai != null && isActor.escalationLevel > 0 && !isActor.ai.restrained)
                    {
                        if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " invalid: " + isActor.name + " has escalation level " + isActor.escalationLevel);
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                    else if (isActor.isMoving)
                    {
                        if(pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " invalid: " + isActor.name + " is moving");
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                    else if (!Player.Instance.stealthMode && (!isActor.isAsleep && !isActor.isDead && !isActor.isStunned && (isActor.ai == null || !isActor.ai.restrained)))
                    {
                        if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " invalid: Stealth: " + Player.Instance.stealthMode + " asleep: " + isActor.isAsleep + " dead: " + isActor.isDead + " ko: " + isActor.isStunned);
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.nonCombat)
            {
                if (isActor != null)
                {
                    if (isActor.ai != null && isActor.escalationLevel > 0)
                    {
                        if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " invalid: " + isActor.name + " has escalation level " + isActor.escalationLevel);
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.nonCombatOrRestrained)
            {
                if (isActor != null)
                {
                    if (isActor.ai != null && ((isActor.escalationLevel > 0 && !isActor.ai.restrained) || (isActor.ai.inFleeState && !isActor.ai.restrained)))
                    {
                        if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " invalid: " + isActor.name + " has escalation level " + isActor.escalationLevel);
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.knockOnDoor)
            {
                //The polymorphic reference should be a door controller
                NewDoor door = objectRef as NewDoor;

                if(door != null)
                {
                    NewAddress otherSide = null;

                    //Trigger knock in address controller (other side to actor)
                    if (door.wall.otherWall.node.gameLocation.thisAsAddress != null)
                    {
                        if (Player.Instance.currentRoom == door.wall.node.room)
                        {
                            otherSide = door.wall.otherWall.node.gameLocation.thisAsAddress;
                        }
                        else
                        {
                            otherSide = door.wall.node.gameLocation.thisAsAddress;
                        }
                    }

                    if(otherSide != null)
                    {
                        //Disable if not residence or company
                        if(otherSide.residence == null && otherSide.company == null)
                        {
                            currentAction.enabled = false; //Disable this
                            currentAction.currentAction = pre; //Set as current if displayed
                            continue;
                        }
                    }
                    else
                    {
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if(pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.originalPlace)
            {
                if(!originalPosition && spR)
                {
                    if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " invalid: Object original position is false! " + distanceFromSpawn + ": " + spWPos + " != " + wPos);
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.onlyIfRestrained)
            {
                if(isActor != null && isActor.ai != null && isActor.ai.restrained)
                {

                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.onlyIfNotRestrained)
            {
                if (isActor != null && isActor.ai != null && !isActor.ai.restrained && !isActor.isDead)
                {

                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.ifInventoryItemDrawn)
            {
                if (BioScreenController.Instance.selectedSlot != null && BioScreenController.Instance.selectedSlot.GetInteractable() != null && BioScreenController.Instance.selectedSlot.GetInteractable().preset.isInventoryItem)
                {

                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.caseFormsNeeded)
            {
                if(CasePanelController.Instance.activeCases.Exists(item => item.caseStatus == Case.CaseStatus.handInNotCollected))
                {

                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.activeCaseHandInReady)
            {
                if(CasePanelController.Instance.activeCase != null && CasePanelController.Instance.activeCase.caseStatus == Case.CaseStatus.handInCollected && CasePanelController.Instance.activeCase.handInValid && (CasePanelController.Instance.activeCase.handIn.Contains(id) || ((objectRef as NewDoor) != null && CasePanelController.Instance.activeCase.handIn.Contains(-(objectRef as NewDoor).wall.id))))
                {

                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.onlyIfSick)
            {
                if (Player.Instance.sick > 0f)
                {

                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.onlyIfMultiPageHasPages)
            {
                if(evidence != null)
                {
                    EvidenceMultiPage mp = evidence as EvidenceMultiPage;

                    if(mp != null)
                    {
                        if(mp.pageContent.Count > 0)
                        {

                        }
                        else
                        {
                            currentAction.enabled = false; //Disable this
                            currentAction.currentAction = pre; //Set as current if displayed
                            continue;
                        }
                    }
                    else
                    {
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.furniturePlacement)
            {
                if (!PlayerApartmentController.Instance.furniturePlacementMode || PlayerApartmentController.Instance.furnPlacement == null)
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.decorItemPlacement)
            {
                if (InteractionController.Instance.carryingObject != null)
                {
                    if (InteractionController.Instance.carryingObject.apartmentPlacementIsValid)
                    {
                        //Success
                    }
                    else
                    {
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }

            if (pre.action.unavailableWhenItemSelected && BioScreenController.Instance.selectedSlot != null)
            {
                if (BioScreenController.Instance.selectedSlot.interactableID > -1 || BioScreenController.Instance.selectedSlot.isStatic != FirstPersonItemController.InventorySlot.StaticSlot.holster)
                {
                    if (pre.action.unavailableWhenItemsSelected == null || pre.action.unavailableWhenItemsSelected.Count <= 0)
                    {
                        if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Unavailable while items drawn");
                        //Game.Log("... Unavailable while items drawn");
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                    else if (pre.action.unavailableWhenItemsSelected.Contains(BioScreenController.Instance.selectedSlot.GetFirstPersonItem()))
                    {
                        if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Unavailable while items drawn");
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
            }

            if (pre.action.onlyAvailableWhenItemSelected)
            {
                if (BioScreenController.Instance.selectedSlot != null && !pre.action.availableWhenItemsSelected.Contains(BioScreenController.Instance.selectedSlot.GetFirstPersonItem()))
                {
                    if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Unavailable because item isn't drawn");
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }

            //Check active if switch states
            bool switchPass = true;

            foreach(InteractablePreset.IfSwitchState st in pre.onlyActiveIf)
            {
                if(st.switchState == InteractablePreset.Switch.switchState)
                {
                    if (sw0 != st.boolIs)
                    {
                        switchPass = false;
                        break;
                    }
                }
                else if (st.switchState == InteractablePreset.Switch.lockState)
                {
                    if (locked != st.boolIs)
                    {
                        switchPass = false;
                        break;
                    }
                }
                else if (st.switchState == InteractablePreset.Switch.custom1)
                {
                    if (sw1 != st.boolIs)
                    {
                        switchPass = false;
                        break;
                    }
                }
                else if (st.switchState == InteractablePreset.Switch.custom2)
                {
                    if (sw2 != st.boolIs)
                    {
                        switchPass = false;
                        break;
                    }
                }
                else if (st.switchState == InteractablePreset.Switch.custom3)
                {
                    if (sw3 != st.boolIs)
                    {
                        switchPass = false;
                        break;
                    }
                }
                else if (st.switchState == InteractablePreset.Switch.lockedIn)
                {
                    bool lockedIn = false;
                    if (InteractionController.Instance.lockedInInteraction != null) lockedIn = true;

                    if (lockedIn != st.boolIs)
                    {
                        switchPass = false;
                        break;
                    }
                }
                else if (st.switchState == InteractablePreset.Switch.carryPhysicsObject)
                {
                    if (phy != st.boolIs)
                    {
                        switchPass = false;
                        break;
                    }
                }
                else if (st.switchState == InteractablePreset.Switch.sprinting)
                {
                    if (Player.Instance.isRunning != st.boolIs)
                    {
                        switchPass = false;
                        break;
                    }
                }
                else if (st.switchState == InteractablePreset.Switch.ko)
                {
                    if (isActor != null && isActor.isStunned != st.boolIs)
                    {
                        switchPass = false;
                        break;
                    }
                }
            }

            //We don't need to update the action display if it's disabled anyway...
            if (!switchPass)
            {
                if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " invalid: Unavailable switches");
                //Game.Log("... Unavailable switches");
                currentAction.enabled = false; //Disable this
                if (currentAction.display) currentAction.currentAction = pre; //Set as current if displayed
                continue;
            }

            if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " is valid");

            //if(currentAction.currentAction != null)
            //{
            //    Game.Log("Set current action for " + currentAction.currentAction.key.ToString());
            //}

            //Below means actions are valid
            //If we've reached here, then this is a valid action for this key
            currentAction.display = true;
            currentAction.enabled = true;
            currentAction.currentAction = pre;
        }

        //Game.Log(enablePrimary.ToString() + enableSecondary.ToString() + enableAlternate.ToString());
        //Game.Log(displayPrimary.ToString() + displaySecondary.ToString() + displayAlternate.ToString());

        InteractionController.Instance.OnPlayerLookAtChange(); //Force look @ change
    }

    //Update the switch state of the interactable
    public virtual void SetSwitchState(bool val, Actor interactor, bool playSFX = true, bool forceUpdate = false, bool forceInstantLights = false)
    {
        if(sw0 != val || forceUpdate)
        {
            sw0 = val;

            if(controller != null)
            {
                controller.debugSwitchState = sw0;
                controller.UpdateSwitchSync(); //Update the switch synced objects
            }

            //If this is a light, the switch state is on/off
            if (lightController != null && preset.lightswitch == InteractablePreset.Switch.switchState)
            {
                lightController.SetOn(sw0, forceInstantLights);
            }

            UpdateCurrentActions();

            //Watch for reset
            if(preset.resetSwitchStates)
            {
                if(sw0 != preset.startingSwitchState)
                {
                    if (!GameplayController.Instance.switchRessetingObjects.ContainsKey(this))
                    {
                        GameplayController.Instance.switchRessetingObjects.Add(this, SessionData.Instance.gameTime);
                    }
                    else GameplayController.Instance.switchRessetingObjects[this] = SessionData.Instance.gameTime;
                }
            }

            //Play SFX (make sure we trigger this if it contains looped sfx)
            if(playSFX || preset.switchSFX.Exists(item => item.isLoop))
            {
                //If this is a door, use both nodes as audio sources
                List<NewNode> doorNodes = null;

                if (objectRef != null)
                {
                    NewDoor door = objectRef as NewDoor;
                    if (door != null)
                    {
                        doorNodes = door.bothNodesForAudioSource;
                    }
                }

                foreach (InteractablePreset.IfSwitchStateSFX aud in preset.switchSFX)
                {
                    if (aud.switchState == InteractablePreset.Switch.switchState)
                    {
                        UpdateSwitchStateAudio(aud, sw0, doorNodes, interactor);
                    }
                }
            }

            //Trigger door movement
            if(controller != null)
            {
                if(controller.doorMovement != null)
                {
                    if(sw0 && controller.doorMovement.desiredTransition != 1f)
                    {
                        controller.doorMovement.SetOpen(1f, interactor);
                    }
                    else if(!sw0 && controller.doorMovement.desiredTransition != 0f)
                    {
                        controller.doorMovement.SetOpen(0f, interactor);
                    }

                    if(controller.secondaryDoorMovement != null)
                    {
                        if (sw0 && controller.secondaryDoorMovement.desiredTransition != 1f)
                        {
                            controller.secondaryDoorMovement.SetOpen(1f, interactor);
                        }
                        else if (!sw0 && controller.secondaryDoorMovement.desiredTransition != 0f)
                        {
                            controller.secondaryDoorMovement.SetOpen(0f, interactor);
                        }
                    }

                    if (controller.thirdDoorMovement != null)
                    {
                        if (sw0 && controller.thirdDoorMovement.desiredTransition != 1f)
                        {
                            controller.thirdDoorMovement.SetOpen(1f, interactor);
                        }
                        else if (!sw0 && controller.thirdDoorMovement.desiredTransition != 0f)
                        {
                            controller.thirdDoorMovement.SetOpen(0f, interactor);
                        }
                    }
                }

                //Trigger sentry gun/camera
                if (controller.securitySystem != null)
                {
                    //Cameras just need power on to be active
                    if (preset.specialCaseFlag == InteractablePreset.SpecialCase.securityCamera)
                    {
                        controller.securitySystem.SetActive(sw0, true);
                    }
                    //Sentries must have power and switch 1 (alarm)
                    else if (preset.specialCaseFlag == InteractablePreset.SpecialCase.sentryGun)
                    {
                        if (sw0 && sw1)
                        {
                            controller.securitySystem.SetActive(true, true);
                        }
                        else controller.securitySystem.SetActive(false, true);
                    }
                }

                //Set lightswitch materials
                if (preset.specialCaseFlag == InteractablePreset.SpecialCase.lightswitch && controller.meshes.Count > 1)
                {
                    if (sw0)
                    {
                        controller.meshes[0].sharedMaterial = InteriorControls.Instance.newLightswitchMaterial;
                        controller.meshes[1].sharedMaterial = InteriorControls.Instance.newLightswithSwitchMaterial;
                    }
                    else
                    {
                        controller.meshes[0].sharedMaterial = InteriorControls.Instance.pulsingLightswitch;
                        controller.meshes[1].sharedMaterial = InteriorControls.Instance.pulsingLightswitchSwitch;
                    }
                }
            }

            //Set steam
            if (preset.affectRoomSteamLevel && node != null)
            {
                node.room.SetSteam(sw0);
            }

            //Breaker
            if(preset.specialCaseFlag == InteractablePreset.SpecialCase.breakerLights)
            {
                if(node != null && node.gameLocation.floor != null)
                {
                    foreach(NewAddress ad in node.gameLocation.floor.addresses)
                    {
                        if(ad.GetBreakerLights() == this)
                        {
                            foreach (NewRoom r in ad.rooms)
                            {
                                foreach (Interactable i in r.mainLights)
                                {
                                    i.SetCustomState2(!sw0, null);
                                }
                            }
                        }
                    }
                }
            }
            else if(preset.specialCaseFlag == InteractablePreset.SpecialCase.breakerSecurity)
            {
                if (node != null && node.gameLocation.floor != null)
                {
                    foreach (NewAddress ad in node.gameLocation.floor.addresses)
                    {
                        if (ad.GetBreakerSecurity() == this)
                        {
                            foreach (Interactable i in ad.securityCameras)
                            {
                                i.SetSwitchState(sw0, null);
                            }

                            foreach (Interactable i in ad.sentryGuns)
                            {
                                i.SetSwitchState(sw0, null);
                            }

                            foreach (Interactable i in ad.otherSecurity)
                            {
                                i.SetSwitchState(sw0, null);
                            }
                        }
                    }
                }
            }
            else if(preset.specialCaseFlag == InteractablePreset.SpecialCase.breakerDoors)
            {
                if (node != null && node.gameLocation.floor != null)
                {
                    bool addressOnly = false;

                    foreach (NewAddress ad in node.gameLocation.floor.addresses)
                    {
                        if (ad.breakerDoors == this)
                        {
                            node.gameLocation.floor.SetAlarmLockdown(!sw0, ad);
                            addressOnly = true;
                        }
                    }

                    if(!addressOnly) node.gameLocation.floor.SetAlarmLockdown(!sw0);
                }
            }
            else if(preset.specialCaseFlag == InteractablePreset.SpecialCase.stovetopKettle)
            {
                //Add to active kettles
                if(sw0)
                {
                    if(!GameplayController.Instance.activeKettles.Contains(this))
                    {
                        GameplayController.Instance.activeKettles.Add(this);
                    }
                }
            }
            
            //Security
            if(preset.specialCaseFlag == InteractablePreset.SpecialCase.securityCamera || preset.specialCaseFlag == InteractablePreset.SpecialCase.sentryGun || preset.specialCaseFlag == InteractablePreset.SpecialCase.otherSecuritySystem || preset.specialCaseFlag == InteractablePreset.SpecialCase.gasReleaseSystem)
            {
                if(!sw0 && !GameplayController.Instance.turnedOffSecurity.Contains(this))
                {
                    GameplayController.Instance.turnedOffSecurity.Add(this);
                }
                else if (sw0)
                {
                    GameplayController.Instance.turnedOffSecurity.Remove(this);
                }
            }

            //Affect special loop params
            UpdateLoopingAudioParams();

            //Fire event
            if (OnSwitchChange != null)
            {
                OnSwitchChange();
            }
        }
    }

    //Update the custom state of the interactable
    public virtual void SetCustomState1(bool val, Actor interactor, bool playSFX = true, bool forceUpdate = false, bool forceInstantLights = false)
    {
        if(sw1 != val || forceUpdate)
        {
            sw1 = val;

            if (controller != null)
            {
                controller.debugState1 = sw1;
                controller.UpdateSwitchSync(); //Update the switch synced objects
            }

            //If this is a light, the switch state is on/off
            if (lightController != null)
            {
                if(preset.lightswitch == InteractablePreset.Switch.custom1) lightController.SetOn(sw1, forceInstantLights);
                lightController.SetUnscrewed(sw1, forceInstantLights);
            }

            UpdateCurrentActions();

            //Watch for reset
            if (preset.resetSwitchStates)
            {
                if (sw1 != preset.startingCustomState1)
                {
                    if (!GameplayController.Instance.switchRessetingObjects.ContainsKey(this))
                    {
                        GameplayController.Instance.switchRessetingObjects.Add(this, SessionData.Instance.gameTime);
                    }
                    else GameplayController.Instance.switchRessetingObjects[this] = SessionData.Instance.gameTime;
                }
            }

            //Play SFX
            if (playSFX || preset.switchSFX.Exists(item => item.isLoop))
            {
                //If this is a door, use both nodes as audio sources
                List<NewNode> doorNodes = null;

                if (objectRef != null)
                {
                    NewDoor door = objectRef as NewDoor;
                    if (door != null)
                    {
                        doorNodes = door.bothNodesForAudioSource;
                    }
                }

                foreach (InteractablePreset.IfSwitchStateSFX aud in preset.switchSFX)
                {
                    if(aud.switchState == InteractablePreset.Switch.custom1)
                    {
                        UpdateSwitchStateAudio(aud, sw1, doorNodes, interactor);
                    }
                }
            }

            //Trigger sentry gun/camera
            if (controller != null && controller.securitySystem != null)
            {
                //Sentries must have power and switch 1 (alarm)
                if (preset.specialCaseFlag == InteractablePreset.SpecialCase.sentryGun)
                {
                    if (sw0 && sw1)
                    {
                        controller.securitySystem.SetActive(true, true);
                    }
                    else controller.securitySystem.SetActive(false, true);
                }
            }

            //Trigger wall tracking mode
            if(preset.specialCaseFlag == InteractablePreset.SpecialCase.tracker)
            {
                if(sw1)
                {
                    GameplayController.Instance.AddMotionTracker(this, 3);
                }
                else
                {
                    GameplayController.Instance.RemoveMotionTracker(this);
                }
            }
            //Trigger proxy mode
            else if(preset.specialCaseFlag == InteractablePreset.SpecialCase.grenade)
            {
                if (sw1)
                {
                    GameplayController.Instance.AddProxyDetonator(this, 3f);
                }
                else
                {
                    GameplayController.Instance.RemoveProxyDetonator(this);
                }
            }

            if (OnState1Change != null)
            {
                OnState1Change();
            }
        }
    }

    //Update the custom state of the interactable
    public virtual void SetCustomState2(bool val, Actor interactor, bool playSFX = true, bool forceUpdate = false, bool forceInstantLights = false)
    {
        if(sw2 != val || forceUpdate)
        {
            sw2 = val;
            UpdateCurrentActions();

            //Watch for reset
            if (preset.resetSwitchStates)
            {
                if (sw2 != preset.startingCustomState2)
                {
                    if (!GameplayController.Instance.switchRessetingObjects.ContainsKey(this))
                    {
                        GameplayController.Instance.switchRessetingObjects.Add(this, SessionData.Instance.gameTime);
                    }
                    else GameplayController.Instance.switchRessetingObjects[this] = SessionData.Instance.gameTime;
                }
            }

            if (controller != null)
            {
                controller.UpdateSwitchSync(); //Update the switch synced objects
            }

            //If this is a light, the switch state is on/off
            if (lightController != null)
            {
                if (preset.lightswitch == InteractablePreset.Switch.custom2) lightController.SetOn(sw2, forceInstantLights);
                lightController.SetClosedBreaker(sw2, false);
            }

            //Add active grenades
            if(preset.specialCaseFlag == InteractablePreset.SpecialCase.grenade)
            {
                if(sw2)
                {
                    if (!GameplayController.Instance.activeGrenades.Contains(this))
                    {
                        GameplayController.Instance.activeGrenades.Add(this);
                    }
                }
                else
                {
                    GameplayController.Instance.activeGrenades.Remove(this);
                    SetValue(GameplayControls.Instance.proxyGrenadeFuse); //Reset fuse
                    recentCallCheck = 0f;
                }
            }

            //Play SFX
            if (playSFX || preset.switchSFX.Exists(item => item.isLoop))
            {
                //If this is a door, use both nodes as audio sources
                List<NewNode> doorNodes = null;

                if (objectRef != null)
                {
                    NewDoor door = objectRef as NewDoor;
                    if (door != null)
                    {
                        doorNodes = door.bothNodesForAudioSource;
                    }
                }

                foreach (InteractablePreset.IfSwitchStateSFX aud in preset.switchSFX)
                {
                    if (aud.switchState == InteractablePreset.Switch.custom2)
                    {
                        UpdateSwitchStateAudio(aud, sw2, doorNodes, interactor);
                    }
                }
            }
        }
    }

    //Update the custom state of the interactable
    public virtual void SetCustomState3(bool val, Actor interactor, bool playSFX = true, bool forceUpdate = false, bool forceInstantLights = false)
    {
        if(sw3 != val || forceUpdate)
        {
            sw3 = val;
            UpdateCurrentActions();

            //Watch for reset
            if (preset.resetSwitchStates)
            {
                if (sw3 != preset.startingCustomState3)
                {
                    if (!GameplayController.Instance.switchRessetingObjects.ContainsKey(this))
                    {
                        GameplayController.Instance.switchRessetingObjects.Add(this, SessionData.Instance.gameTime);
                    }
                    else GameplayController.Instance.switchRessetingObjects[this] = SessionData.Instance.gameTime;
                }
            }

            if (controller != null)
            {
                controller.UpdateSwitchSync(); //Update the switch synced objects
            }

            if(lightController != null)
            {
                if (preset.lightswitch == InteractablePreset.Switch.custom3) lightController.SetOn(sw3, forceInstantLights);
            }

            //Play SFX
            if (playSFX || preset.switchSFX.Exists(item => item.isLoop))
            {
                //If this is a door, use both nodes as audio sources
                List<NewNode> doorNodes = null;

                if (objectRef != null)
                {
                    NewDoor door = objectRef as NewDoor;
                    if (door != null)
                    {
                        doorNodes = door.bothNodesForAudioSource;
                    }
                }

                foreach (InteractablePreset.IfSwitchStateSFX aud in preset.switchSFX)
                {
                    if (aud.switchState == InteractablePreset.Switch.custom3)
                    {
                        UpdateSwitchStateAudio(aud, sw3, doorNodes, interactor);
                    }
                }
            }
        }
    }

    //Update the lock state of the interactable
    public virtual void SetLockedState(bool val, Actor interactor, bool playSFX = true, bool forceUpdate = false)
    {
        if(locked != val || forceUpdate)
        {
            locked = val;

            //Watch for reset
            if (preset.resetSwitchStates)
            {
                if (locked != preset.startingLockState)
                {
                    if (!GameplayController.Instance.switchRessetingObjects.ContainsKey(this))
                    {
                        GameplayController.Instance.switchRessetingObjects.Add(this, SessionData.Instance.gameTime);
                    }
                    else GameplayController.Instance.switchRessetingObjects[this] = SessionData.Instance.gameTime;
                }
            }

            if (controller != null)
            {
                controller.UpdateSwitchSync(); //Update the switch synced objects
            }

            //Game.Log(name + " IsLocked = " + isLocked);

            UpdateCurrentActions();

            //Sync locked state with doors
            if(objectRef != null)
            {
                NewDoor door = objectRef as NewDoor;

                if(door != null)
                {
                    door.SetLocked(locked, interactor);
                }
            }

            //Sync locked state with doors
            if (lockInteractable != null && lockInteractable != this)
            {
                lockInteractable.SetLockedState(locked, interactor);
            }

            if (preset.useMaterialChanges && spawnedObject != null)
            {
                if (locked && preset.lockOnMaterial != null)
                {
                    spawnedObject.GetComponent<MeshRenderer>().material = preset.lockOnMaterial;
                    //Game.Log("Material changed to " + preset.lockOnMaterial);
                }
                else if (!locked && preset.lockOffMaterial != null)
                {
                    spawnedObject.GetComponent<MeshRenderer>().material = preset.lockOffMaterial;
                    //Game.Log("Material changed to " + preset.lockOffMaterial);
                }
            }

            //If this is a lock, play arm animation
            if (thisDoor != null)
            {
                thisDoor.SetLockedState(locked, interactor);

                if (controller != null && locked)
                {
                    if(controller.flash != null)
                    {
                        controller.flash.Flash(3);
                    }
                }
            }

            //Play SFX
            if (playSFX || preset.switchSFX.Exists(item => item.isLoop))
            {
                //If this is a door, use both nodes as audio sources
                List<NewNode> doorNodes = null;

                if (objectRef != null)
                {
                    NewDoor door = objectRef as NewDoor;
                    if (door != null)
                    {
                        doorNodes = door.bothNodesForAudioSource;
                    }
                }

                foreach (InteractablePreset.IfSwitchStateSFX aud in preset.switchSFX)
                {
                    if (aud.switchState == InteractablePreset.Switch.lockState)
                    {
                        UpdateSwitchStateAudio(aud, locked, doorNodes, interactor);
                    }
                }
            }

            //Set capture update needed
            //if (preset.includeInSceneCaptures)
            //{
            //    if (node != null && node.room != null) node.room.fullCaptureNeeded = true;
            //}
        }
    }

    //Update the physics carried state of the interactable
    public virtual void SetPhysicsPickupState(bool val, Actor interactor, bool playSFX = true, bool forceUpdate = false)
    {
        if (phy != val || forceUpdate)
        {
            phy = val;
            UpdateCurrentActions();

            if (controller != null)
            {
                controller.UpdateSwitchSync(); //Update the switch synced objects
            }

            //Play SFX
            if (playSFX || preset.switchSFX.Exists(item => item.isLoop))
            {
                //If this is a door, use both nodes as audio sources
                List<NewNode> doorNodes = null;

                if (objectRef != null)
                {
                    NewDoor door = objectRef as NewDoor;
                    if (door != null)
                    {
                        doorNodes = door.bothNodesForAudioSource;
                    }
                }

                foreach (InteractablePreset.IfSwitchStateSFX aud in preset.switchSFX)
                {
                    if (aud.switchState == InteractablePreset.Switch.carryPhysicsObject)
                    {
                        UpdateSwitchStateAudio(aud, phy, doorNodes, interactor);
                    }
                }
            }
        }
    }

    //Reset to original switch config according to preset
    public void ResetToDefaultSwitchState()
    {
        SetSwitchState(preset.startingSwitchState, null);
        SetSwitchState(preset.startingCustomState1, null);
        SetSwitchState(preset.startingCustomState2, null);
        SetSwitchState(preset.startingCustomState3, null);
        SetSwitchState(preset.startingLockState, null);
    }

    //Update switch state audio
    public void UpdateSwitchStateAudio(InteractablePreset.IfSwitchStateSFX aud, bool swState, List<NewNode> doorNodes, Actor interactor)
    {
        bool syncBedPass = true;
        bool neonPass = true;
        bool inventoryPass = true;

        if(aud.onlyIfInSyncBed)
        {
            if (Player.Instance.hideInteractable != null && Player.Instance.hideInteractable.preset.specialCaseFlag == InteractablePreset.SpecialCase.syncBed)
            {
                syncBedPass = true;
            }
            else syncBedPass = false;
        }

        if (aud.onlyIfNotInSyncBed)
        {
            if (Player.Instance.hideInteractable == null || Player.Instance.hideInteractable.preset.specialCaseFlag != InteractablePreset.SpecialCase.syncBed)
            {
                syncBedPass = true;
            }
            else syncBedPass = false;
        }

        if(aud.onlyIfNeonSign)
        {
            neonPass = false;

            if(objectRef != null)
            {
                NewDoor d = objectRef as NewDoor;

                if(d != null)
                {
                    if(d.featuresNeonSign)
                    {
                        neonPass = true;
                    }
                }
            }
        }

        if(inInventory != null)
        {
            inventoryPass = false;
        }

        //Matching state
        if (aud.boolIs == swState && syncBedPass && neonPass && inventoryPass && !rPl)
        {
            if (aud.isLoop)
            {
                //Music player: Loop tracks
                if(aud.isMusicPlayer)
                {
                    if (!GameplayController.Instance.activeMusicPlayers.Contains(this)) GameplayController.Instance.activeMusicPlayers.Add(this);
                    UpdateMusicPlayer();
                }
                else
                {
                    if (!loopingAudio.Exists(item => item.eventPreset == aud.triggerAudio))
                    {
                        //Game.Log("Audio: Playing Switch State looped audio from interactable " + preset.name + " (" + aud.triggerAudio.name + ")");
                        AudioController.LoopingSoundInfo startLoop = AudioController.Instance.PlayWorldLooping(aud.triggerAudio, interactor, this, pauseWhenGamePaused: true, isBroadcast: aud.isBroadcast, newSwitchInfo: aud);
                        if (startLoop != null) loopingAudio.Add(startLoop);

                        //Pass special params...
                        if (aud.passOpenParam || aud.passCSParam || aud.passDoorDirParam)
                        {
                            UpdateLoopingAudioParams();
                        }
                    }
                    else
                    {
                        //Game.Log("Audio: loop already exists");
                    }
                }
            }
            else
            {
                AudioController.Instance.PlayWorldOneShot(aud.triggerAudio, interactor, node, wPos, additionalSources: doorNodes, interactable: this);
            }
        }
        //Stop looping sounds
        else
        {
            if (aud.isLoop && loopingAudio != null)
            {
                if(aud.isMusicPlayer)
                {
                    List<AudioController.LoopingSoundInfo> foundLoops = loopingAudio.FindAll(item => preset.musicTracks.Contains(item.eventPreset));

                    foreach(AudioController.LoopingSoundInfo fl in foundLoops)
                    {
                        AudioController.Instance.StopSound(fl, aud.stop, "Music player off");
                        loopingAudio.Remove(fl);
                    }

                    GameplayController.Instance.activeMusicPlayers.Remove(this);
                }
                else
                {
                    //Game.Log("Audio: Stopping Switch State looped audio from interactable " + preset.name + " (" + aud.triggerAudio.name + ")");
                    AudioController.LoopingSoundInfo foundLoop = loopingAudio.Find(item => item.eventPreset == aud.triggerAudio);

                    if (foundLoop != null)
                    {
                        AudioController.Instance.StopSound(foundLoop, aud.stop, "Switch is different to state (" + val + ")");
                        loopingAudio.Remove(foundLoop);
                    }
                }
            }
        }
    }

    public void MusicPlayerNextTrack(int add)
    {
        SetValue(Mathf.RoundToInt(val + add));
        if (val >= preset.musicTracks.Count) SetValue(0);
        if (val < 0) SetValue(preset.musicTracks.Count - 1);
        UpdateMusicPlayer();
    }

    public void UpdateMusicPlayer()
    {
        //Loop around
        int trackNo = Mathf.RoundToInt(val);
        if (trackNo >= preset.musicTracks.Count) trackNo = 0;
        if (trackNo < 0) trackNo = preset.musicTracks.Count - 1;

        try
        {
            AudioEvent trackToPlay = preset.musicTracks[trackNo];

            if(trackToPlay != null)
            {
                //Stop any previous track
                foreach(AudioController.LoopingSoundInfo loop in loopingAudio)
                {
                    if (loop.eventPreset == trackToPlay)
                    {
                        //Is the track not playing? Next track...
                        if(loop.UpdatePlayState() != FMOD.Studio.PLAYBACK_STATE.PLAYING)
                        {
                            SetValue(Mathf.RoundToInt(val + 1));
                            if (val >= preset.musicTracks.Count) SetValue(0);
                            if (val < 0) SetValue(preset.musicTracks.Count - 1);
                            trackNo = Mathf.RoundToInt(val);

                            try
                            {
                                trackToPlay = preset.musicTracks[trackNo];
                            }
                            catch
                            {
                                //Game.LogError("Music player does not feature track at index " + trackNo);
                            }
                        }
                    }
                    else if(preset.musicTracks.Contains(loop.eventPreset))
                    {
                        AudioController.Instance.StopSound(loop, AudioController.StopType.immediate, "Music player different track");
                        loopingAudio.Remove(loop);
                    }
                }

                //Trigger playing of new track
                if (!loopingAudio.Exists(item => item.eventPreset == trackToPlay))
                {
                    //Game.Log("Music player starting new track " + trackNo);
                    AudioController.LoopingSoundInfo startLoop = AudioController.Instance.PlayWorldLooping(trackToPlay, null, this, pauseWhenGamePaused: true);
                    if (startLoop != null) loopingAudio.Add(startLoop);
                }
            }
        }
        catch
        {
            //Game.LogError("Music player does not feature track at index " + trackNo);
        }
    }

    //Update looping audio params
    public void UpdateLoopingAudioParams()
    {
        if (loopingAudio == null) loopingAudio = new List<AudioController.LoopingSoundInfo>();

        foreach(AudioController.LoopingSoundInfo loop in loopingAudio)
        {
            bool occUpdate = false;

            if (loop.interactableLoopInfo != null)
            {
                if(loop.interactableLoopInfo.passOpenParam)
                {
                    float passed = 0;
                    
                    if(sw0)
                    {
                        passed = 1f;
                    }

                    //Override with animated state
                    if(controller != null && controller.doorMovement != null)
                    {
                        passed = controller.doorMovement.currentTransition;
                    }

                    if (controller != null && controller.secondaryDoorMovement != null)
                    {
                        passed = controller.secondaryDoorMovement.currentTransition;
                    }

                    if (controller != null && controller.thirdDoorMovement != null)
                    {
                        passed = controller.thirdDoorMovement.currentTransition;
                    }

                    loop.audioEvent.setParameterByName("Open", passed);
                    FMOD.Studio.PLAYBACK_STATE playState = FMOD.Studio.PLAYBACK_STATE.STOPPED;
                    loop.audioEvent.getPlaybackState(out playState);
                    occUpdate = true;
                    //Game.Log("Audio: Passing open param of " + " to " + loop.eventPreset.name + " on " + name + " (" + playState + ")");
                }

                if (loop.interactableLoopInfo.passCSParam)
                {
                    loop.audioEvent.setParameterByName("ConsumableState", cs);
                    FMOD.Studio.PLAYBACK_STATE playState = FMOD.Studio.PLAYBACK_STATE.STOPPED;
                    loop.audioEvent.getPlaybackState(out playState);
                    occUpdate = true;
                    //Game.Log("Audio: Passing open param of " + " to " + loop.eventPreset.name + " on " + name + " (" + playState + ")");
                }

                if(loop.interactableLoopInfo.passDoorDirParam)
                {
                    if(controller != null)
                    {
                        if(controller.doorMovement != null)
                        {
                            if(controller.doorMovement.isAnimating)
                            {
                                if(controller.doorMovement.isOpening)
                                {
                                    loop.audioEvent.setParameterByName("DoorDirection", 1);
                                    //Game.Log("Door is opening (1)");
                                }
                                else
                                {
                                    loop.audioEvent.setParameterByName("DoorDirection", 0);
                                    //Game.Log("Door is closing (0)");
                                }
                            }
                        }
                    }
                }

                if(occUpdate)
                {
                    loop.UpdateOcclusion(true);
                }
            }
        }
    }
    
    //Executed upon interaction (2x variants; one with input reference from the player, the other a direct action reference useful for AI)
    public void OnInteraction(InteractablePreset.InteractionKey input, Actor who)
    {
        InteractableCurrentAction currentAction = null;

        if(currentActions.TryGetValue(input, out currentAction))
        {
            //Do a failsafe enabled check
            if(currentAction.enabled)
            {
                OnInteraction(currentAction.currentAction, who);
            }
        }
    }

    public void OnInteraction(InteractablePreset.InteractionAction action, Actor who, bool allowDelays = true, float additionalDelay = 0f)
    {
        if (action == null) return; //Return if null

        //Add to queued actions: Only if visible and ai
        if(allowDelays && who != null && who.ai != null && (action.aiUsageDelay + additionalDelay) > 0f && who.visible && !who.isDead)
        {
            if(!who.ai.queuedActions.Exists(item => item.actionSetting == action && item.interactable == this))
            {
                //Game.Log(who.name + ": Adding queued action " + action.interactionName + " " + name + " delay: " + action.aiUsageDelay + additionalDelay);
                who.ai.queuedActions.Add(new NewAIController.QueuedAction { actionSetting = action, delay = action.aiUsageDelay + additionalDelay, interactable = this });
                return;
            }
        }

        //Cost
        if(action.actionCost > 0 && who != null && who.isPlayer)
        {
            GameplayController.Instance.AddMoney(-action.actionCost, true, "Action cost");
        }

        //Ilegal (Needs to happen before execution as some legality settings depend on the furniture it's spawned on)
        if(who != null && who.isPlayer)
        {
            if (action.actionIsIllegal)
            {
                if (InteractionController.Instance.GetValidPlayerActionIllegal(this, Player.Instance.currentNode))
                {
                    InteractionController.Instance.SetIllegalActionActive(true);
                }
            }

            //Deselect item
            if (action.action.holsterCurrentItemOnAction)
            {
                if (BioScreenController.Instance.selectedSlot != null && BioScreenController.Instance.selectedSlot.isStatic != FirstPersonItemController.InventorySlot.StaticSlot.holster)
                {
                    BioScreenController.Instance.SelectSlot(FirstPersonItemController.Instance.slots.Find(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.holster));
                }
            }
        }

        //Execute
        ActionController.Instance.ExecuteAction(action.action, this, node, who);

        //Leave a dynamic fingerprint
        if(who != null && !who.isPlayer)
        {
            //75% chance of leaving a print
            if(Toolbox.Instance.Rand(0f, 1f) <= 0.75f)
            {
                AddNewDynamicFingerprint(who as Human, PrintLife.timed);
            }
        }

        //Feedback animation
        //if(who != null && who.isPlayer)
        //{
        //    InteractionController.InteractionSetting current = null;

        //    if(InteractionController.Instance.currentInteractions.TryGetValue(action.GetInteractionKey(), out current))
        //    {
        //        if(current.uiReference != null)
        //        {
        //            current.uiReference.juiceController.Flash(1, true, current.uiReference.text.color, speed: 6f);
        //            current.uiReference.juiceController.Nudge(new Vector2(1.05f, 1.05f), new Vector2(-2, -2)); //Nudge
        //        }
        //    }
        //}

        //Toggle hide
        if(action.isHidingPlace && who != null && (!action.onlyHidingPlaceIfPublic || !who.isTrespassing))
        {
            who.SetHiding(true, this);
        }

        //Effect switch states
        foreach(InteractablePreset.SwitchState st in action.effectSwitchStates)
        {
            if(st.switchState == InteractablePreset.Switch.switchState)
            {
                //Attempt to open while locked
                if (lockInteractable != null)
                {
                    //Locked: Can't alter switch state when locked
                    if (locked && (belongsTo == Player.Instance || belongsTo != who))
                    {
                        if (node != null && preset.attemptedOpenSound != null)
                        {
                            //If this is a door, use both nodes as audio sources
                            List<NewNode> doorNodes = null;

                            if(objectRef != null)
                            {
                                NewDoor door = objectRef as NewDoor;
                                if(door != null)
                                {
                                    doorNodes = door.bothNodesForAudioSource;
                                }
                            }

                            AudioController.Instance.PlayWorldOneShot(preset.attemptedOpenSound, who, node, wPos, additionalSources: doorNodes, interactable: this);
                        }

                        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "Locked"), InterfaceControls.Icon.lockpick);
                        Game.Log("Cannot affect switch state of interactable " + name + " because it is locked.");

                        return;
                    }
                }

                SetSwitchState(st.boolIs, who);
            }
            else if (st.switchState == InteractablePreset.Switch.lockState)
            {
                SetLockedState(st.boolIs, who);
            }
            else if (st.switchState == InteractablePreset.Switch.custom1)
            {
                SetCustomState1(st.boolIs, who);
            }
            else if (st.switchState == InteractablePreset.Switch.custom2)
            {
                SetCustomState2(st.boolIs, who);
            }
            else if (st.switchState == InteractablePreset.Switch.custom3)
            {
                SetCustomState3(st.boolIs, who);
            }
            else if (st.switchState == InteractablePreset.Switch.carryPhysicsObject)
            {
                SetPhysicsPickupState(st.boolIs, who);
            }
        }

        //Play sfx
        if (controller != null && action.soundEvent != null && action.playOnTrigger)
        {
            //If this is a door, use both nodes as audio sources
            List<NewNode> doorNodes = null;

            if (objectRef != null)
            {
                NewDoor door = objectRef as NewDoor;
                if (door != null)
                {
                    doorNodes = door.bothNodesForAudioSource;
                }
            }

            NewNode audioNode = node;
            Actor audioActor = who;
            Vector3 audioPostion = wPos;

            if (audioNode == null)
            {
                PathFinder.Instance.nodeMap.TryGetValue(CityData.Instance.RealPosToNodeInt(audioPostion), out audioNode);
            }

            if(action.soundEvent.debug) Game.Log("Audio: Audio from interactable action " + action.interactionName + ": Who= " + audioActor + " node= " + audioNode + " worldPos= " + audioPostion);
            AudioController.Instance.PlayWorldOneShot(action.soundEvent, audioActor, audioNode, audioPostion, additionalSources: doorNodes, interactable: this);
        }

        //Set as illegally activated
        if(node != null)
        {
            if (action.action.tamperAction)
            {
                if (who != null && who.isTrespassing)
                {
                    if (!node.room.tamperedInteractables.Contains(this))
                    {
                        node.room.tamperedInteractables.Add(this);

                        //Add tamper peril
                        if(who.isPlayer)
                        {
                            int tamperCount = 0;

                            foreach(NewRoom room in node.room.gameLocation.rooms)
                            {
                                tamperCount += room.tamperedInteractables.Count;
                            }

                            if(tamperCount > GameplayControls.Instance.tamperGrace)
                            {
                                //if(node.room.gameLocation.thisAsAddress != null) StatusController.Instance.AddFineRecord(node.room.gameLocation.thisAsAddress, null, StatusController.CrimeType.tampering);
                                //GameplayController.Instance.AddPerilFine(GameplayController.PerilType.tampering, node.room.gameLocation);
                            }
                        }
                    }
                }
                else node.room.tamperedInteractables.Remove(this); //If not tresspassing, it's been replaced by being deliberately on
            }

            if (action.action.tamperResetAction)
            {
                if(node.room.tamperedInteractables.Contains(this))
                {
                    node.room.tamperedInteractables.Remove(this);

                    //Remove tamper peril (only if player resets)
                    if (who.isPlayer)
                    {
                        //GameplayController.PerilFine tamper = GameplayController.Instance.perilFines.Find(item => item.perilType == GameplayController.PerilType.tampering);

                        //if (tamper != null)
                        //{
                            int tamperCount = 0;

                            foreach (NewRoom room in node.room.gameLocation.rooms)
                            {
                                tamperCount += room.tamperedInteractables.Count;
                            }

                            if (tamperCount <= GameplayControls.Instance.tamperGrace)
                            {
                            //GameplayController.PerilCount tamperC = tamper.counts.Find(item => item.gameLocation == node.room.gameLocation);

                            //if (tamperC != null)
                            //{
                            //    tamper.counts.Remove(tamperC);
                            //    GameplayController.Instance.UpdatePeril();
                            //}
                            //if (node.room.gameLocation.thisAsAddress != null) StatusController.Instance.RemoveFineRecord(node.room.gameLocation.thisAsAddress, null, StatusController.CrimeType.tampering);
                            }
                        //}
                    }
                }
            }
        }
    }

    //Load this interactable to world
    public void LoadInteractableToWorld(bool respawn = false, bool forceSpawnImmediate = false)
    {
        if (preset.spawnable)
        {
            if (spawnedObject != null)
            {
                if (respawn)
                {
                    DespawnObject();
                }
                else if(inInventory == null && !rPl)
                {
                    ObjectPoolingController.Instance.MarkAsToLoad(this);
                    return;
                }
            }

            //Only spawn if not in inventory and not removed from world
            if (inInventory == null && !rPl)
            {
                if(forceSpawnImmediate)
                {
                    SpawnObject(out _);
                }
                else
                {
                    ObjectPoolingController.Instance.MarkAsToLoad(this);
                }
            }
        }
    }

    //Set the spawn position to be relevent or not
    public void SetSpawnPositionRelevent(bool val)
    {
        spR = val;
    }

    //Triggered when called by the object pooler (load only x objects per frame)
    public void SpawnObject(out bool wasPooled)
    {
        //Game.Log("Spawn object: " + GetName() + ": " + id);

        wasPooled = false;
        if (controller != null || preset == null) return; //TEST THIS: Ignore if exsiting...

        spawnParent = null;

        if (parentTransform == null && worldObjectRoomParent != null) parentTransform = worldObjectRoomParent.transform; //Load room parent

        //Find the parent transform using furniture
        if (furnitureParent != null)
        {
            //The furniture must be spawned first...
            if (furnitureParent.spawnedObject == null)
            {
                Game.Log("Misc Error: Trying to spawn interactable with furniture parent before an interactable spawned upon it: " + preset.name + " on node " + node.nodeCoord + " world: " + wPos + " local: " + lPos + " on furniture " + furnitureParent.furniture.name + " on node " + furnitureParent.anchorNode.nodeCoord + " world: " + furnitureParent.anchorNode.position);
                return;
            }

            //Find the parent transform if needed
            spawnParent = furnitureParent.spawnedObject.transform;

            if (subObject != null && subObject.parent.Length > 0)
            {
                spawnParent = Toolbox.Instance.SearchForTransform(furnitureParent.spawnedObject.transform, subObject.parent);

                if (spawnParent == null && Game.Instance.devMode && Game.Instance.printDebug)
                {
                    Game.LogError("SubObject not found for " + preset.name + " in furniture " + furnitureParent.furniture.name + " Parent name = " + subObject.parent);

                    Transform[] allTransforms = Toolbox.Instance.GetAllTransforms(furnitureParent.spawnedObject.transform);

                    foreach (Transform t in allTransforms)
                    {
                        Game.Log("Listing children transform for " + furnitureParent.furniture.name + ": " + t.name + " (children: " + t.childCount + ")");
                    }
                }
            }
        }
        else
        {
            if (parentTransform == null && worldObjectRoomParent != null) parentTransform = worldObjectRoomParent.transform;
            spawnParent = parentTransform;
        }

        bool reloadedPooledSelf = false; //Has this just been activated from the pool (ie we don't need to apply/set up anything)?
        spawnedObject = ObjectPoolingController.Instance.GetInteractableObject(this, out reloadedPooledSelf, out wasPooled);

        if(spawnedObject == null)
        {
            return;
        }

        spawnedObject.transform.localEulerAngles = lEuler;
        spawnedObject.transform.localPosition = lPos;

        //Link interactables
        List<InteractableController> interactables = new List<InteractableController>(spawnedObject.GetComponentsInChildren<InteractableController>());
        InteractableController matching = interactables.Find(item => item.id == (InteractableController.InteractableID)pt && pt != (int)InteractableController.InteractableID.none);

        if (matching != null)
        {
            //Run setup
            matching.Setup(this);
        }
        else
        {
            Game.Log("Misc Error: Unable to find a matching interactable controller for " + preset.name + " id: " + ((InteractableController.InteractableID)pt).ToString());
        }

        //Only do the following if this is a freshly-spawned or different through the object pool
        if (!reloadedPooledSelf)
        {
            //Change material
            if (preset.inheritColouringFromDecor)
            {
                if (preset.shareColoursWithFurniture != FurniturePreset.ShareColours.none)
                {
                    //Find other with this color sharing designation...
                    FurnitureLocation other = node.room.individualFurniture.Find(item => item.furniture.shareColours == preset.shareColoursWithFurniture);

                    if (other != null)
                    {
                        if(controller != null)
                        {
                           foreach (MeshRenderer rend in controller.meshes)
                           {
                                MaterialsController.Instance.ApplyMaterialKey(rend, other.matKey);
                           }
                        }
                        else MaterialsController.Instance.ApplyMaterialKey(spawnedObject, other.matKey);
                    }
                }
                else
                {
                    if (controller != null)
                    {
                        foreach (MeshRenderer rend in controller.meshes)
                        {
                            MaterialsController.Instance.ApplyMaterialKey(rend, node.room.miscKey);
                        }
                    }
                    else MaterialsController.Instance.ApplyMaterialKey(spawnedObject, node.room.miscKey);
                }
            }
            //Apply own colour key
            else if (preset.useOwnColourSettings)
            {
                if (controller != null)
                {
                    foreach (MeshRenderer rend in controller.meshes)
                    {
                        MaterialsController.Instance.ApplyMaterialKey(rend, mk);
                    }
                }
                else MaterialsController.Instance.ApplyMaterialKey(spawnedObject, mk);
            }

            if (preset.useMaterialChanges && controller != null && controller.meshes.Count > 0)
            {
                if (locked && preset.lockOnMaterial != null)
                {
                    controller.meshes[0].material = preset.lockOnMaterial;
                    //Game.Log("Material changed to " + preset.lockOnMaterial);
                }
                else if (!locked && preset.lockOffMaterial != null)
                {
                    controller.meshes[0].material = preset.lockOffMaterial;
                    //Game.Log("Material changed to " + preset.lockOffMaterial);
                }
            }

            //Set lightswitch materials
            if (preset.specialCaseFlag == InteractablePreset.SpecialCase.lightswitch && controller != null && controller.meshes.Count > 1)
            {
                if (sw0)
                {
                    controller.meshes[0].sharedMaterial = InteriorControls.Instance.newLightswitchMaterial;
                    controller.meshes[1].sharedMaterial = InteriorControls.Instance.newLightswithSwitchMaterial;
                }
                else
                {
                    controller.meshes[0].sharedMaterial = InteriorControls.Instance.pulsingLightswitch;
                    controller.meshes[1].sharedMaterial = InteriorControls.Instance.pulsingLightswitchSwitch;
                }
            }

            //Use book/record setup
            if (book != null)
            {
                MeshFilter currentFilter = spawnedObject.GetComponent<MeshFilter>();

                if (currentFilter.sharedMesh != book.bookMesh)
                {
                    currentFilter.sharedMesh = book.bookMesh;

                    //Make sure collider is the correct size...
                    spawnedObject.GetComponent<BoxCollider>().center = currentFilter.sharedMesh.bounds.center;
                    spawnedObject.GetComponent<BoxCollider>().size = currentFilter.sharedMesh.bounds.size;
                }

                spawnedObject.GetComponent<MeshRenderer>().sharedMaterial = book.bookMaterial;
            }

            //Set light layer
            if (node != null)
            {
                bool streetLightLayer = false;
                if (node.room.preset != null) streetLightLayer = node.room.preset.forceStreetLightLayer;

                if(controller != null)
                {
                    foreach(MeshRenderer mr in controller.meshes)
                    {
                        Toolbox.Instance.SetLightLayer(mr, node.building, streetLightLayer);
                    }
                }
                else Toolbox.Instance.SetLightLayer(spawnedObject, node.building, streetLightLayer);
            }
        }

        loadedGeometry = true;
        ObjectPoolingController.Instance.interactableLoadList.Remove(this); //Remove from toload list

        OnSpawn();
    }

    //Set to unrendered: Will set invisible and send the object to the pool (if enabled).
    public void UnloadInteractable()
    {
        //If this is still waiting to be spawned, make sure it's removed from the load list
        ObjectPoolingController.Instance.MarkAsNotNeeded(this);

        if (preset.spawnable && loadedGeometry && ObjectPoolingController.Instance.usePooling)
        {
            ObjectPoolingController.Instance.PoolInteractable(this);
            loadedGeometry = false;
        }
    }

    //Despawn interactable from world
    public void DespawnObject()
    {
        if(InteractionController.Instance.carryingObject != null && InteractionController.Instance.carryingObject.interactable == this)
        {
            Game.LogError("Despawning a carried object: " + name);
        }

        if(preset == null)
        {
            //Parse preset
            if (!Toolbox.Instance.objectPresetDictionary.TryGetValue(p, out preset))
            {
                Game.LogError("Interactables unable to find preset for " + p);
            }
        }

        if (preset.spawnable)
        {
            //If this is still waiting to be spawned, make sure it's removed from the load list
            ObjectPoolingController.Instance.MarkAsNotNeeded(this);

            if (spawnedObject != null)
            {
                Toolbox.Instance.DestroyObject(spawnedObject);
                ObjectPoolingController.Instance.interactablesLoaded--;
            }

            loadedGeometry = false;
        }
    }

    public void OnSpawn()
    {
        if (isLight != null)
        {
            if(node == null)
            {
                UpdateWorldPositionAndNode(false);

                if(node == null)
                {
                    //Game.LogError("Object: Object " + preset.name + " node is not set... " + debugGeneralSetup);
                    return;
                }
            }

            //Spawn ceiling light
            if (ceilingFan != null) Toolbox.Instance.DestroyObject(ceilingFan.gameObject);

            if (node.room.ceilingFans > -1 && ml)
            {
                ceilingFan = Toolbox.Instance.SpawnObject(node.room.mainLightPreset.ceilingFans[node.room.ceilingFans], spawnedObject.transform).transform;
                ceilingFan.transform.localPosition = Vector3.zero;
                Toolbox.Instance.SetLightLayer(ceilingFan.gameObject, node.building, node.room.preset.forceStreetLightLayer);
            }

            if (controller != null)
            {
                lightController = controller.lightController;
                if(lightController == null) lightController = controller.gameObject.GetComponentInChildren<LightController>();
            }

            if(lightController != null)
            {
                lightController.Setup(node.room, this, lcd, isLight, lightZoneSize: lzs, newCeilingFan: ceilingFan); //Pass the size of the light zone for auto-setting intensity
                lightController.SetUnscrewed(sw1, true);

                if(preset.lightswitch == InteractablePreset.Switch.switchState) lightController.SetOn(sw0, true); //On/off == switch state
                else if (preset.lightswitch == InteractablePreset.Switch.custom1) lightController.SetOn(sw1, true); //On/off == switch state
                else if (preset.lightswitch == InteractablePreset.Switch.custom2) lightController.SetOn(sw2, true); //On/off == switch state
                else if (preset.lightswitch == InteractablePreset.Switch.custom3) lightController.SetOn(sw3, true); //On/off == switch state
            }
            else Game.Log("Misc Error: No controller for " + spawnedObject + " (needed for light)");
        }

        SpawnLock();

        //Spawn decals
        if(spawnedDecals != null && spawnedObject != null)
        {
            foreach(SpatterSimulation.DecalSpawnData ds in spawnedDecals)
            {
                if(ds.spawnedProjector == null)
                {
                    Transform t = Toolbox.Instance.SearchForTransform(spawnedObject.transform, ds.subObjectName); ;

                    if(t != null)
                    {
                        ds.SpawnOnTransform(t);
                    }
                    else
                    {
                        Game.LogError("Unable to load spatter with parent " + ds.parentID.ToString() + " id " + ds.transformParentID + " transform name: " + ds.subObjectName);
                    }
                }
            }
        }
    }

    //Spawn a lock and pair with the lock interactable if needed
    public void SpawnLock()
    {
        if (lockInteractable != null && lockInteractable != this)
        {
            //Game.Log("Spawning lock for " + id);

            if (lockInteractable.spawnedObject != null)
            {
                Toolbox.Instance.DestroyObject(spawnedObject);
            }

            Transform spawnParent = spawnedObject.transform;
            if (controller != null && controller.lockParentOverride != null) spawnParent = controller.lockParentOverride;

            lockInteractable.spawnedObject = Toolbox.Instance.SpawnObject(preset.includeLock.prefab, spawnParent);
            lockInteractable.spawnedObject.transform.localPosition = preset.lockOffset;
            lockInteractable.loadedGeometry = true;

            InteractableController lockController = lockInteractable.spawnedObject.GetComponent<InteractableController>();
            Toolbox.Instance.SetLightLayer(lockInteractable.spawnedObject, node.building);

            lockController.Setup(lockInteractable);

            if (preset.includeLock.useMaterialChanges)
            {
                if (lockInteractable.locked && lockInteractable.preset.lockOnMaterial != null)
                {
                    lockInteractable.spawnedObject.GetComponent<MeshRenderer>().material = lockInteractable.preset.lockOnMaterial;
                    //Game.Log("Material changed to " + lockInteractable.preset.lockOnMaterial);
                }
                else if (!lockInteractable.locked && lockInteractable.preset.lockOffMaterial != null)
                {
                    lockInteractable.spawnedObject.GetComponent<MeshRenderer>().material = lockInteractable.preset.lockOffMaterial;
                    //Game.Log("Material changed to " + lockInteractable.preset.lockOffMaterial);
                }
            }
        }
    }

    //Mark as trash; this will be removed when trash reaches the maximum value, plus it is safe to do so...
    public void MarkAsTrash(bool val, bool forceTime = false, float forcedTime = 0f)
    {
        if(val)
        {
            if(forceTime)
            {
                mtr = forcedTime;

                if (!CleanupController.Instance.trash.Contains(this))
                {
                    bool inserted = false;

                    for (int i = 0; i < CleanupController.Instance.trash.Count; i++)
                    {
                        Interactable current = CleanupController.Instance.trash[i];

                        if (mtr <= current.mtr)
                        {
                            CleanupController.Instance.trash.Insert(i, this);
                            inserted = true;
                            break;
                        }
                    }

                    if(!inserted)
                    {
                        CleanupController.Instance.trash.Add(this);
                    }
                }
            }
            else
            {
                mtr = SessionData.Instance.gameTime + 0.0001f;

                if(CleanupController.Instance != null)
                {
                    if (!CleanupController.Instance.trash.Contains(this))
                    {
                        CleanupController.Instance.trash.Add(this);
                    }
                }
            }
        }
        else
        {
            mtr = -1;

            if (CleanupController.Instance != null)
            {
                CleanupController.Instance.trash.Remove(this);
            }
        }
    }

    //Remove from the game world visually; removes from furniture and despawns the object
    public void RemoveFromPlacement()
    {
        rPl = true;

        if (node != null)
        {
            node.RemoveInteractable(this); //Remove from current position
            node.room.tamperedInteractables.Remove(this);
        }

        if(spawnNode != null)
        {
            spawnNode.RemoveInteractable(this); //Remove from current position
            spawnNode.room.tamperedInteractables.Remove(this);
        }

        GameplayController.Instance.interactablesMoved.Remove(this); //Remove from moved
        ObjectPoolingController.Instance.MarkAsNotNeeded(this);

        //It is possible for this to be deleted before the preset is loaded, so if it's missing, try to load it first...
        if(preset == null)
        {
            Toolbox.Instance.LoadDataFromResources<InteractablePreset>(p, out preset);
        }

        if (preset.isClock)
        {
            SessionData.Instance.OnHourChange -= OnHourChange;
        }

        if (preset != null)
        {
            if (preset.specialCaseFlag == InteractablePreset.SpecialCase.tracker)
            {
                GameplayController.Instance.RemoveMotionTracker(this);
            }
            else if(preset.specialCaseFlag == InteractablePreset.SpecialCase.grenade)
            {
                GameplayController.Instance.RemoveProxyDetonator(this);
                GameplayController.Instance.activeGrenades.Remove(this);
            }
        }

        if (furnitureParent != null)
        {
            //Remove from all potential nodes in furniture
            foreach (NewNode node in furnitureParent.coversNodes)
            {
                node.RemoveInteractable(this);
            }

            //Remove from furniture reference
            furnitureParent.integratedInteractables.Remove(this);
            furnitureParent.spawnedInteractables.Remove(this);
        }

        if (worldObjectRoomParent != null)
        {
            worldObjectRoomParent.worldObjects.Remove(this);
            worldObjectRoomParent = null;
        }

        DespawnObject();

        if(loopingAudio != null)
        {
            for (int i = 0; i < loopingAudio.Count; i++)
            {
                AudioController.Instance.StopSound(loopingAudio[i], AudioController.StopType.immediate, "Remove object from world");
                loopingAudio.RemoveAt(i);
            }
        }

        if (OnRemovedFromWorld != null)
        {
            OnRemovedFromWorld();
        }

        UpdateLoopingAudioParams();
    }

    //Delete but only if this isn't going to disrupt the player's caseboard, history or other important references
    public void SafeDelete(bool removeFromInventory = false)
    {
         //Remove from inventory
        if (removeFromInventory && inInventory != null)
        {
            SetAsNotInventory(null);
        }

        RemoveFromPlacement();
        MarkAsTrash(true);

        if (IsSafeToDelete())
        {
            Delete();
        }
        else if(inInventory == null)
        {
            //Trigger mission destroy
            if (jobParent != null)
            {
                foreach (KeyValuePair<JobPreset.JobTag, Interactable> pair in jobParent.activeJobItems)
                {
                    if (pair.Value == this)
                    {
                        jobParent.OnDestroyMissionObject(pair.Value);
                    }
                }
            }
        }
    }

    //Remove from the game world completely
    public void Delete()
    {
        rem = true; //Marks as removed

        //Remove from inventory
        if (inInventory != null)
        {
            SetAsNotInventory(null);
        }

        RemoveFromPlacement(); //Remove visually

        if (isLight != null && node != null && node.room != null)
        {
            node.room.secondaryLights.Remove(this);
            node.room.mainLights.Remove(this);
        }

        if (worldObjectRoomParent != null)
        {
            worldObjectRoomParent.worldObjects.Remove(this);
            worldObjectRoomParent = null;
        }

        if (jobParent != null)
        {
            List<JobPreset.JobTag> removeTags = new List<JobPreset.JobTag>();

            foreach (KeyValuePair<JobPreset.JobTag, Interactable> pair in jobParent.activeJobItems)
            {
                if (pair.Value == this)
                {
                    jobParent.OnDestroyMissionObject(pair.Value);
                    removeTags.Add(pair.Key);
                }
            }

            foreach (JobPreset.JobTag t in removeTags)
            {
                jobParent.activeJobItems.Remove(t);
            }
        }

        if(mtr > -1)
        {
            if(CleanupController.Instance != null) CleanupController.Instance.trash.Remove(this);
        }

        //Remove evidence traces
        if(evidence != null)
        {
            //Close any active windows purtaining to this
            List<InfoWindow> activeWindows = InterfaceController.Instance.activeWindows.FindAll(item => item.passedEvidence == evidence);

            for (int i = 0; i < activeWindows.Count; i++)
            {
                activeWindows[i].CloseWindow(false);
            }

            //Unpin any boards featuring this
            if(!SessionData.Instance.isFloorEdit)
            {
                foreach (Case c in CasePanelController.Instance.activeCases)
                {
                    for (int i = 0; i < c.caseElements.Count; i++)
                    {
                        Case.CaseElement ce = c.caseElements[i];

                        if (ce.id == evidence.evID)
                        {
                            c.caseElements.RemoveAt(i);

                            //Listen for evidence changes...
                            evidence.OnDataKeyChange -= CasePanelController.Instance.UpdatePinned;
                            evidence.OnDiscoverConnectedFact -= CasePanelController.Instance.UpdateStrings;
                            evidence.OnDiscoverChild -= CasePanelController.Instance.UpdateStrings;

                            i--;
                            continue;
                        }
                    }
                }

                foreach (Case c in CasePanelController.Instance.archivedCases)
                {
                    for (int i = 0; i < c.caseElements.Count; i++)
                    {
                        Case.CaseElement ce = c.caseElements[i];

                        if (ce.id == evidence.evID)
                        {
                            c.caseElements.RemoveAt(i);

                            //Listen for evidence changes...
                            evidence.OnDataKeyChange -= CasePanelController.Instance.UpdatePinned;
                            evidence.OnDiscoverConnectedFact -= CasePanelController.Instance.UpdateStrings;
                            evidence.OnDiscoverChild -= CasePanelController.Instance.UpdateStrings;

                            i--;
                            continue;
                        }
                    }
                }

                //Remove from history
                List<GameplayController.History> removeHistory = GameplayController.Instance.history.FindAll(item => item.evID == evidence.evID);

                foreach(GameplayController.History h in removeHistory)
                {
                    GameplayController.Instance.history.Remove(h);
                    GameplayController.Instance.itemOnlyHistory.Remove(h);
                }

                //Remove evidence...
                GameplayController.Instance.evidenceDictionary.Remove(evidence.evID);
            }

        }

        //Remove special case reference
        if (preset.specialCaseFlag != InteractablePreset.SpecialCase.none && node != null && node.room != null)
        {
            if (node.room.specialCaseInteractables.ContainsKey(preset.specialCaseFlag))
            {
                node.room.specialCaseInteractables[preset.specialCaseFlag].Remove(this);
            }
        }

        //If this has been created with the city data, send it to this list which keeps record of the items removed
        if (!cr)
        {
            CleanupController.Instance.removedCityDataItems.Add(id);
        }

        //Remove the reference to this if is created after the start of the game, ie not in the citysave data (we need to save it again to tell the game it's gone).
        Game.Log("Object: Deleting object " + name + " id " + id + " from interactable directory...");
        CityData.Instance.interactableDirectory.Remove(this); //Remove reference
        CityData.Instance.savableInteractableDictionary.Remove(id);

        GameplayController.Instance.activeGadgets.Remove(this); //Remove active gadget

        //Remove wound
        if(objectRef != null)
        {
            Human.Wound wound = objectRef as Human.Wound;

            if(wound != null)
            {
                Human woundHuman = null;

                if(CityData.Instance.GetHuman(wound.humanID, out woundHuman))
                {
                    woundHuman.currentWounds.Remove(wound);
                }
            }
        }

        //Fire event
        if (OnDelete != null)
        {
            OnDelete(this);
        }
    }

    //Return whether this object is considered 'safe' to delete
    public bool IsSafeToDelete()
    {
        //Must not be spawned or away from player (unless removed from placement)
        if(controller == null || node == null || (rPl || !node.room.isVisible))
        {
            //Must not contain job reference, be present in history, or be in inventory
            if (inInventory == null && (evidence == null || !GameplayController.Instance.history.Exists(item => item.evID == evidence.evID)))
            {
                //Must not feature in jobs or the job is finished
                if(jobParent == null || jobParent.state == SideJob.JobState.ended || !jobParent.activeJobItems.Values.Contains(this))
                {
                    //Must feature no evidence or no active window references
                    if (!InterfaceController.Instance.activeWindows.Exists(item => (evidence != null && item.passedEvidence == evidence) || item.passedInteractable == this))
                    {
                        bool casePass = true;

                        //Must feature no evidence or not be pinned on a case
                        if (evidence != null)
                        {
                            foreach (Case c in CasePanelController.Instance.activeCases)
                            {
                                if (c.caseElements.Exists(item => item.id == evidence.evID))
                                {
                                    casePass = false;
                                    break;
                                }
                            }

                            //Depending on whether we're intending to allow access to archived cases; this may be disabled...
                            if(casePass)
                            {
                                foreach (Case c in CasePanelController.Instance.archivedCases)
                                {
                                    if (c.caseElements.Exists(item => item.id == evidence.evID))
                                    {
                                        casePass = false;
                                        break;
                                    }
                                }
                            }
                        }

                        if (casePass)
                        {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    //Should you save this in save state data?
    public bool IsSaveStateEligable()
    {
        if (preset.dontSaveWithSaveGames && (!wo || (wo && !preset.onlySaveWithSaveGamesIfWorldObject))) return false;

        //If removed from the game we don't have to save; objects created with city data now have their own deletion record
        if (rem)
        {
            return false;
        }

        if (force) return true; //Misc force the save state?
        if (cr) return true; //Has this object been created after game start? Ie not in the city files...
        if (spCh) return true; //Has the spawn position changed?
        if (mov && wo) return true; //Has this been moved? Must be a world object
        if (df.Count > 0) return true; //Features dynamic fingerprints
        if (inInventory != null) return true; //Is in an inventory

        //Check states against default
        if(!preset.dontSaveSwitchStates)
        {
            if (sw0 != preset.startingSwitchState) return true;
            if (sw1 != preset.startingCustomState1) return true;
            if (sw2 != preset.startingCustomState2) return true;
            if (sw3 != preset.startingCustomState3) return true;
            if (locked != preset.startingLockState) return true;
        }

        if (phy) return true;

        //Physics on
        if (controller != null)
        {
            if (controller.physicsOn) return true;
        }

        return false;
    }

    public string GetReasonForSaveStateEligable()
    {
        if (force) return "Forced save"; //Misc force the save state?
        if (cr) return "Created after start"; //Has this object been created after game start? Ie not in the city files...
        if (spCh) return "Spawn position changed"; //Has the spawn position changed?
        if (mov && wo) return "Has been moved"; //Has this been moved?
        if (df.Count > 0) return "Features dynamic prints"; //Features dynamic fingerprints
        if (inInventory != null) return "Is in inventory"; //Is in an inventory

        //Check states against default
        if (sw0 != preset.startingSwitchState) return "SW0 is different from starting state";
        if (sw1 != preset.startingCustomState1) return "SW1 is different from starting state";
        if (sw2 != preset.startingCustomState2) return "SW2 is different from starting state";
        if (sw3 != preset.startingCustomState3) return "SW3 is different from starting state";
        if (locked != preset.startingLockState) return "Locked is different from starting state";
        if (phy) return "Physics pick up state";

        //Physics on
        if (controller != null)
        {
            if (controller.physicsOn) return "Physics on";
        }

        return "Isn't savable";
    }

    //Set this as a light
    public void SetAsLight(LightingPreset newLightPreset, int newLightZoneSize, bool newIsMainLight, LightConfiguration preconfiguredLight)
    {
        //if(newIsMainLight)
        //{
        //    Game.Log("LZ: " + newLightZoneSize + " : " + node.room.name + " : " + wPos.y + " / " + lPos.y + " / " + spWPos.y);
        //}

        isLight = newLightPreset;
        lzs = newLightZoneSize;
        ml = newIsMainLight;
        lcd = preconfiguredLight;

        //If this is not given, generate new light data
        if (lcd == null)
        {
            GenerateLightData();
        }

        if (node == null)
        {
            Game.LogError("Unable to Set as Light due to no node! " + name);
            return;
        }

        if (ml)
        {
            node.room.AddMainLight(this);
        }
        else node.room.AddSecondaryLight(this);
    }

    //Set the next AI interaction, so we can stop the AI doing 2 things at once
    public void SetNextAIInteraction(NewAIAction newAction, NewAIController ai)
    {
        //Remove old action if not this one
        if(ai.nextAIAction != null && ai.nextAIAction != this)
        {
            ai.nextAIAction.nextAIInteraction = null;
            ai.nextAIAction = null;
        }

        nextAIInteraction = newAction;
        
        if(newAction != null)
        {
            newAction.goal.aiController.nextAIAction = this;
        }
    }

    //If this interactable has a door movement, pass close/open finish events
    public void OnDoorMovementClosed()
    {
        if(!locked && preset.armLockOnClose && lockInteractable != null)
        {
            InteractablePreset.InteractionAction act = lockInteractable.preset.GetActions().Find(item => item.effectSwitchStates.Exists(item2 => item2.switchState == InteractablePreset.Switch.lockState && item2.boolIs));
            lockInteractable.OnInteraction(act, null);
        }
    }

    public void OnDoorMovementOpened()
    {

    }

    //Get password from the password source
    public List<int> GetPasswordFromSource(out List<string> notePlacements)
    {
        notePlacements = new List<string>();

        if(passwordSource == null)
        {
            Game.Log("No password source for " + name);
        }

        Interactable thisInteractable = passwordSource as Interactable;

        if(thisInteractable != null)
        {
            Game.Log("Password source is interactable " + thisInteractable.id + " (" + thisInteractable.GetName() + ")");
            if ((Game.Instance.devMode && Game.Instance.collectDebugData)) notePlacements.Add(thisInteractable.passcode.GetNotePlacements());
            return thisInteractable.passcode.GetDigits();
        }

        Human thisHuman = passwordSource as Human;

        if (thisHuman != null)
        {
            Game.Log("Password source is human " + thisHuman.humanID);
            if ((Game.Instance.devMode && Game.Instance.collectDebugData)) notePlacements.Add(thisHuman.passcode.GetNotePlacements());
            return thisHuman.passcode.GetDigits();
        }

        NewRoom thisRoom = passwordSource as NewRoom;

        if (thisRoom != null)
        {
            Game.Log("Password source is room " + thisRoom.roomID);
            if ((Game.Instance.devMode && Game.Instance.collectDebugData)) notePlacements.Add(thisRoom.passcode.GetNotePlacements());
            return thisRoom.passcode.GetDigits();
        }

        NewAddress thisAddress = passwordSource as NewAddress;

        if (thisAddress != null)
        {
            Game.Log("Password source is address " + thisAddress.id);
            if ((Game.Instance.devMode && Game.Instance.collectDebugData)) notePlacements.Add(thisAddress.passcode.GetNotePlacements());
            return thisAddress.passcode.GetDigits();
        }

        Game.Log("Returned no password: 0000");

        int[] returnNull = new int[] { 0, 0, 0, 0 };

        return returnNull.ToList();
    }

    //Get password from the password source
    public GameplayController.Passcode GetPasswordSource()
    {
        if (passwordSource == null)
        {
            Game.Log("No password source for " + name);
        }
        else Game.Log("Password source is " + passwordSource);

        Interactable thisInteractable = passwordSource as Interactable;

        if (thisInteractable != null)
        {
            return thisInteractable.passcode;
        }

        Human thisHuman = passwordSource as Human;

        if (thisHuman != null)
        {
            return thisHuman.passcode;
        }

        NewRoom thisRoom = passwordSource as NewRoom;

        if (thisRoom != null)
        {
            return thisRoom.passcode;
        }

        NewAddress thisAddress = passwordSource as NewAddress;

        if (thisAddress != null)
        {
            return thisAddress.passcode;
        }

        return null;
    }

    public void AddPasswordSourceToAcquired()
    {
        GameplayController.Passcode passcode = null;

        if (passwordSource == null)
        {
            Game.Log("No password source for " + name);
        }
        else Game.Log("Password source is " + passwordSource);

        Interactable thisInteractable = passwordSource as Interactable;

        if (thisInteractable != null)
        {
            passcode = thisInteractable.passcode;
        }

        Human thisHuman = passwordSource as Human;

        if (thisHuman != null)
        {
            passcode = thisHuman.passcode;
        }

        NewRoom thisRoom = passwordSource as NewRoom;

        if (thisRoom != null)
        {
            passcode = thisRoom.passcode;
        }

        NewAddress thisAddress = passwordSource as NewAddress;

        if (thisAddress != null)
        {
            passcode = thisAddress.passcode;
        }

        if(passcode != null)
        {
            GameplayController.Instance.AddPasscode(passcode);
        }
    }


    //Set highlighting of a certain action
    public void SetActionHighlight(string newString, bool val)
    {
        Game.Log("Object: Looking to highlight action: " + newString);

        //Find action
        InteractablePreset.InteractionAction act = preset.GetActions().Find(item => item.interactionName.ToLower() == newString.ToLower());

        //If null this action may be in the locked-in interaction list
        if(act == null)
        {
            act = preset.GetActions(1).Find(item => item.interactionName.ToLower() == newString.ToLower());

            if(act == null)
            {
                act = preset.GetActions(2).Find(item => item.interactionName.ToLower() == newString.ToLower());
            }
        }

        if (act == null)
        {
            Game.Log(newString + " action not found");
            return; //If we don't have an action at this point then return.
        }

        if(val)
        {
            if (!highlightActions.Contains(act)) highlightActions.Add(act);
        }
        else
        {
            highlightActions.Remove(act);
        }

        InteractionController.Instance.UpdateInteractionText();
    }

    //Force disable an action
    public void SetActionDisable(string newString, bool val)
    {
        Game.Log("Object: Disable " + name + " : " + newString + "...");

        //Find action
        InteractablePreset.InteractionAction act = preset.GetActions().Find(item => item.interactionName.ToLower() == newString.ToLower());

        //If null this action may be in the locked-in interaction list
        if (act == null)
        {
            act = preset.GetActions(1).Find(item => item.interactionName.ToLower() == newString.ToLower());

            if (act == null)
            {
                act = preset.GetActions(2).Find(item => item.interactionName.ToLower() == newString.ToLower());
            }
        }

        if (act == null)
        {
            Game.Log("Object: ...Cannot find action " + newString + ", aborting...");
            return; //If we don't have an action at this point then return.
        }

        if (val)
        {
            if (!disabledActions.Contains(act))
            {
                Game.Log("Object: Adding " + act.interactionName + " to disabled actions");
                disabledActions.Add(act);
            }
        }
        else
        {
            Game.Log("Object: Removing " + act.interactionName + " from disabled actions");
            disabledActions.Remove(act);
        }

        UpdateCurrentActions(); //Force action update
    }

    //Set in original positions (setting true will reset this object physically too)
    public void SetOriginalPosition(bool newVal, bool setGameTime = true)
    {
        if(originalPosition != newVal)
        {
            originalPosition = newVal;

            //Reset to the original/spawn position
            if (originalPosition)
            {
                //Get correct (reset) spawn parent...
                wPos = spWPos;
                wEuler = spWEuler;

                //Convert back to furniture object...
                if(wo && fp > -1)
                {
                    ResetToFurnitureObject(true);
                }
                else UpdateWorldPositionAndNode(false);

                //if (spawnParent != null)
                //{
                //    SetNewPositionAndParent(spawnParent, spawnParent.InverseTransformPoint(spWPos), spWEuler - spawnParent.transform.eulerAngles, false);
                //    Game.Log("Setting original position relative to spawn parent " + spawnParent.name + "; spawn pos " + spWPos + " local = " + lPos + ", world = " + wPos);
                //}
                //else
                //{
                //    MoveInteractable(spWPos, spWEuler, false);
                //    Game.Log("Setting original position of world object:" + spWPos + " local = " + lPos + ", world = " + wPos);
                //}

                SetTampered(false);
                distanceFromSpawn = 0; //Reset distance from spawn

                //Remove vandal reference
                if(spawnNode != null && spawnNode.gameLocation != null && spawnNode.gameLocation.thisAsAddress != null)
                {
                    spawnNode.gameLocation.thisAsAddress.RemoveVandalism(this);
                }

                //Reboot physics
                if (controller != null)
                {
                    if(controller.rb != null)
                    {
                        controller.rb.MovePosition(spWPos);
                        controller.rb.MoveRotation(Quaternion.Euler(spWEuler));
                        controller.rb.velocity = Vector3.zero;
                        controller.rb.angularVelocity = Vector3.zero;
                        SessionData.Instance.ExecuteSyncPhysics(SessionData.PhysicsSyncType.now);
                    }
                }

                GameplayController.Instance.interactablesMoved.Remove(this);
            }
            else
            {
                if (setGameTime) lma = SessionData.Instance.gameTime;

                //Add to moved interatables 
                if(spR && !GameplayController.Instance.interactablesMoved.Contains(this))
                {
                    GameplayController.Instance.interactablesMoved.Add(this);
                }
            }
        }
    }

    public void SetTampered(bool val)
    {
        if (isTampered != val && spawnNode != null && spawnNode.room != null)
        {
            isTampered = val;

            //Set as illegally activated
            if (isTampered)
            {
                if (Player.Instance.isTrespassing)
                {
                    if (!spawnNode.room.tamperedInteractables.Contains(this))
                    {
                        spawnNode.room.tamperedInteractables.Add(this);

                        //Add tamper peril
                        int tamperCount = 0;

                        foreach (NewRoom room in spawnNode.room.gameLocation.rooms)
                        {
                            tamperCount += room.tamperedInteractables.Count;
                        }

                        if (tamperCount > GameplayControls.Instance.tamperGrace)
                        {
                            //if (node.room.gameLocation.thisAsAddress != null) StatusController.Instance.AddFineRecord(spawnNode.room.gameLocation.thisAsAddress, null, StatusController.CrimeType.tampering);
                            //StatusController.Instance.AddFineRecord(null, this, StatusController.CrimeType.tampering);
                            //GameplayController.Instance.AddPerilFine(GameplayController.PerilType.tampering, spawnNode.room.gameLocation);
                        }
                    }
                }
                else spawnNode.room.tamperedInteractables.Remove(this); //If not tresspassing, it's been replaced by being deliberately on
            }
            else
            {
                if (spawnNode.room.tamperedInteractables.Contains(this))
                {
                    spawnNode.room.tamperedInteractables.Remove(this);

                    //Remove tamper peril (only if player resets)
                    //GameplayController.PerilFine tamper = GameplayController.Instance.perilFines.Find(item => item.perilType == GameplayController.PerilType.tampering);

                    //if (tamper != null)
                    //{
                        int tamperCount = 0;

                        foreach (NewRoom room in spawnNode.room.gameLocation.rooms)
                        {
                            tamperCount += room.tamperedInteractables.Count;
                        }

                        if (tamperCount <= GameplayControls.Instance.tamperGrace)
                        {
                        //GameplayController.PerilCount tamperC = tamper.counts.Find(item => item.gameLocation == spawnNode.room.gameLocation);

                        //if (tamperC != null)
                        //{
                        //    tamper.counts.Remove(tamperC);
                        //    GameplayController.Instance.UpdatePeril();
                        //}
                            //if (node.room.gameLocation.thisAsAddress != null) StatusController.Instance.RemoveFineRecord(spawnNode.room.gameLocation.thisAsAddress, null, StatusController.CrimeType.tampering);
                        }
                    //}
                }
            }

            //Set capture update needed
            //if (preset.includeInSceneCaptures)
            //{
            //    node.room.fullCaptureNeeded = true;
            //}
        }
    }

    //Add new dynamic fingerprint
    public void AddNewDynamicFingerprint(Human from, Interactable.PrintLife life)
    {
        if (from == null) return;
        if (!preset.fingerprintsEnabled) return;
        if (!preset.enableDynamicFingerprints) return;
        if (df == null) df = new List<DynamicFingerprint>();
        if (from.isPlayer) return;

        if (preset.overrideMaxDynamicFingerprints)
        {
            if(df.Count >= preset.maxDynamicFingerprints) return; //Abort creating new print
        }
        else if (df.Count >= GameplayControls.Instance.maxDynamicPrintsPerObject) return; //Abort creating new print

        DynamicFingerprint newPrint = new DynamicFingerprint { id = from.humanID, created = SessionData.Instance.gameTime, seed = Toolbox.Instance.GenerateSeed(4), life = life };
        df.Add(newPrint);

        if(!GameplayController.Instance.objectsWithDynamicPrints.Contains(this))
        {
            GameplayController.Instance.objectsWithDynamicPrints.Add(this);
        }
    }

    public void RemoveDynamicPrint(DynamicFingerprint print)
    {
        if (df == null) df = new List<DynamicFingerprint>();
        df.Remove(print);
    }

    //Triggered if this has the isClock designation
    public void OnHourChange()
    {
        //Play hourly chime...
        if(preset.hourlyChime != null && SessionData.Instance.gameTime > lhc && SessionData.Instance.startedGame)
        {
            lhc = SessionData.Instance.gameTime;

            //Remove existing
            for (int i = 0; i < AudioController.Instance.delayedSound.Count; i++)
            {
                if(AudioController.Instance.delayedSound[i].worldPosition == wPos)
                {
                    AudioController.Instance.delayedSound.RemoveAt(i);
                    i--;
                }
            }

            if (preset.chimeEqualToHour)
            {
                //Game.Log("Audio: Trigger hourly clock chime for " + name + " " + id + "...");

                //Send delayed sounds...
                float formatted = SessionData.Instance.FloatMinutes24H(SessionData.Instance.decimalClock);

                //Get hours only
                int hours = Mathf.FloorToInt(formatted);

                while (hours > 12)
                {
                    hours -= 12; //12 hour clock chime
                }

                for (int i = 0; i < hours; i++)
                {
                    //Game.Log("Audio: Adding delayed hourly clock chime for " + name + " " + id + ": " + preset.chimeDelay * i);
                    AudioController.Instance.PlayOneShotDelayed(preset.chimeDelay * i, preset.hourlyChime, null, node, wPos);
                }
            }
            //Just chime once
            else
            {
                AudioController.Instance.PlayWorldOneShot(preset.hourlyChime, null, node, wPos, this);
            }
        }
    }

    //On lockpick
    public void OnLockpick()
    {
        //Set know door status
        if (!locked)
        {
            Game.Log("locked: " + locked);
            return;
        }

        //Set as locked in interaction
        InteractionController.Instance.SetLockedInInteractionMode(this);

        Player.Instance.TransformPlayerController(GameplayControls.Instance.lockpickEnter, GameplayControls.Instance.lockpickExit, this, null, false);

        //Set ineraction action
        InteractionController.Instance.SetInteractionAction(0f, val, Mathf.LerpUnclamped(GameplayControls.Instance.lockpickEffectivenessRange.x, GameplayControls.Instance.lockpickEffectivenessRange.y, UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.lockpickingEfficiencyModifier)), "lockpicking", true, true, controller.transform);
        Player.Instance.isLockpicking = true;

        //Listen for progress change
        InteractionController.Instance.OnInteractionActionProgressChange += OnLockpickProgressChange;

        //Listen for complete lockpick
        InteractionController.Instance.OnInteractionActionCompleted += OnCompleteLockpick;

        //Listen for return from locked in 
        InteractionController.Instance.OnReturnFromLockedIn += OnReturnFromLockpick;

        //Play sfx looping
        AudioController.Instance.StopSound(actionLoop, AudioController.StopType.immediate, "Stop lockpick");
        actionLoop = AudioController.Instance.PlayWorldLooping(AudioControls.Instance.lockpick, Player.Instance, this);

        //Add fine
        if(Player.Instance.home != Player.Instance.currentNode.gameLocation && !Player.Instance.apartmentsOwned.Contains(Player.Instance.currentNode.gameLocation))
        {
            StatusController.Instance.AddFineRecord(Player.Instance.currentNode.gameLocation.thisAsAddress, this, StatusController.CrimeType.breakingAndEntering);
        }
    }

    public void OnLockpickProgressChange(float amountChangeThisFrame, float amountToal)
    {
        //Game.Log("Player: On locking use: " + amountChangeThisFrame);
        SetValue(val - amountChangeThisFrame); //Set actual lock strength

        //Update UI
        InteractionController.Instance.DisplayInteractionCursor(InteractionController.Instance.displayingInteraction, true); //Force update but don't change

        //If lock strength is 0, it is picked (unlock)
        if (val <= 0f)
        {
            Game.Log("Set " + name + " unlocked");
            SetLockedState(false, Player.Instance, true, true);
            if(thisDoor != null) thisDoor.SetLockedState(false, Player.Instance, true, true);
        }

        //Use up lockpick
        GameplayController.Instance.UseLockpick(amountChangeThisFrame);

        //Check we still have lockpicks to continue...
        if (GameplayController.Instance.lockPicks <= 0)
        {
            InteractionController.Instance.SetLockedInInteractionMode(null);
            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "not_enough_lockpicks"), InterfaceControls.Icon.lockpick, null, colourOverride: true, col: InterfaceControls.Instance.messageRed, ping: GameMessageController.PingOnComplete.lockpicks);
        }
    }

    public void OnCompleteLockpick()
    {
        //Stop SFX
        AudioController.Instance.StopSound(actionLoop, AudioController.StopType.fade, "Complete lockpick");
        actionLoop = null;

        InteractionController.Instance.OnInteractionActionProgressChange -= OnLockpickProgressChange;
        //InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromLockpick;
        InteractionController.Instance.OnInteractionActionCompleted -= OnCompleteLockpick;
        Player.Instance.isLockpicking = false;

        //Return from lock in
        InteractionController.Instance.SetLockedInInteractionMode(null);

        //Remove fine record
        StatusController.Instance.RemoveFineRecord(null, this, StatusController.CrimeType.breakingAndEntering, true);
    }

    public void OnReturnFromLockpick()
    {
        //Stop SFX
        AudioController.Instance.StopSound(actionLoop, AudioController.StopType.fade, "Return from lockpick");
        actionLoop = null;

        InteractionController.Instance.OnInteractionActionProgressChange -= OnLockpickProgressChange;
        InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromLockpick;
        InteractionController.Instance.OnInteractionActionCompleted -= OnCompleteLockpick;
        Player.Instance.isLockpicking = false;

        Player.Instance.ReturnFromTransform();

        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, GameplayController.Instance.lockPicks + " " + Strings.Get("ui.gamemessage", "lockpick_deplete"), InterfaceControls.Icon.lockpick, null, moveToOnDestroy: InterfaceController.Instance.moneyNotificationIcon, ping: GameMessageController.PingOnComplete.lockpicks);

        //Remove fine record
        StatusController.Instance.RemoveFineRecord(null, this, StatusController.CrimeType.breakingAndEntering, true);
    }

    //Force the object to spawn and enable physics. Force spawn location: If location is not rendered, spawn it first. Switching on physics should keep it spawned...
    public void ForcePhysicsActive(bool forceSpawnLocation, bool applyForce, Vector3 force = new Vector3(), ForceMode forceMode = ForceMode.VelocityChange, bool useThrowingForce = false)
    {
        Game.Log("Object: Forcing physics active on " + name + "...");

        if(controller == null && node != null)
        {
            if(node.room != null && node.room.gameObject != null && !node.room.gameObject.activeSelf)
            {
                if(forceSpawnLocation)
                {
                    Game.Log("Object: ... Force spawn location " + node.room.name);
                    node.room.SetVisible(true, true, "Force physics spawn", immediateLoad: true);
                }
            }

            if(node.room != null && node.room.gameObject != null && node.room.gameObject.activeSelf)
            {
                LoadInteractableToWorld(false, true);

                controller.DropThis(useThrowingForce); //Activate physics

                if(applyForce && !useThrowingForce)
                {
                    controller.rb.AddForce(force, ForceMode.VelocityChange);
                }
            }
            //Otherwise similate drop by spawning on ground...
            else
            {
                Game.Log("Object: ... Spawning on ground...");
                Vector2 c = (UnityEngine.Random.insideUnitCircle * 0.5f);
                Vector3 r = node.position;

                //Spawn in a small radius around this...
                if (node.walkableNodeSpace.Count > 0)
                {
                    List<NewNode.NodeSpace> nodeSpace = new List<NewNode.NodeSpace>(node.walkableNodeSpace.Values);
                    r = nodeSpace[Toolbox.Instance.Rand(0, nodeSpace.Count)].position + new Vector3(c.x, 0, c.y);
                }
                else
                {
                    r += new Vector3(c.x, 0, c.y);
                }

                //Basic rotation...
                Vector3 rot = new Vector3(0, Toolbox.Instance.Rand(0f, 360f));

                MoveInteractable(r, rot, true);
            }
        }
        else
        {
            controller.DropThis(useThrowingForce); //Activate physics

            if (applyForce && !useThrowingForce)
            {
                controller.rb.AddForce(force, ForceMode.VelocityChange);
            }
        }
    }

    public void ConvertToFurnitureSpawnedObject(FurnitureLocation newFurniture, FurniturePreset.SubObject newSubObject, bool updatePosition = true, bool updateSpawnPosition = true)
    {
        wo = false;

        if (worldObjectRoomParent != null)
        {
            worldObjectRoomParent.worldObjects.Remove(this);
            worldObjectRoomParent = null;
        }

        furnitureParent = newFurniture;
        fp = newFurniture.id;
        subObject = newSubObject;
        fsoi = newFurniture.furniture.subObjects.IndexOf(subObject); //Get the sub object index
        lPos = subObject.localPos;
        lEuler = subObject.localRot;

        //Game.Log("Convert object to furnitire, local pos: " + lPos + " from fsoi " + fsoi);

        //Add to parent's spawned interactables
        if (subObject != null)
        {
            if(!furnitureParent.spawnedInteractables.Contains(this))
            {
                furnitureParent.spawnedInteractables.Add(this);
            }
        }

        if(updatePosition) UpdateWorldPositionAndNode(updateSpawnPosition);

        //Load in immediately if needed
        if(furnitureParent != null && furnitureParent.spawnedObject != null)
        {
            LoadInteractableToWorld(false, false);
        }
    }

    public void ConvertToWorldObject(bool updatePosition = true)
    {
        //Game.Log("Converting " + name + " to world object...");

        //Remove from furniture reference
        if(furnitureParent != null)
        {
            furnitureParent.integratedInteractables.Remove(this);
            furnitureParent.spawnedInteractables.Remove(this);
        }

        wo = true;
        //fp = -1;
        //fsoi = -1;
        furnitureParent = null;
        subObject = null;

        if(updatePosition) UpdateWorldPositionAndNode(false);
    }

    public void ResetToFurnitureObject(bool updatePosition = true)
    {
        Game.Log("Resetting " + name + " to furniture object at furniture ID " + fp + " and fsoi " + fsoi +"...");
        wo = false;

        if (worldObjectRoomParent != null)
        {
            worldObjectRoomParent.worldObjects.Remove(this);
            worldObjectRoomParent = null;
        }

        DespawnObject();

        //Parse furniture parent...
        if (fp > -1 && furnitureParent == null)
        {
            //There is no dictionary for furniture ID, so we have to look for it...
            NewNode searchNode = null;
            Vector3Int nPos = CityData.Instance.RealPosToNodeInt(spWPos);

            if(PathFinder.Instance.nodeMap.TryGetValue(nPos, out searchNode))
            {
                furnitureParent = searchNode.room.individualFurniture.Find(item => item.id == fp);

                //Search furniture within gamelocation
                if (furnitureParent == null)
                {
                    foreach (NewRoom r in searchNode.gameLocation.rooms)
                    {
                        furnitureParent = r.individualFurniture.Find(item => item.id == fp);
                        if (furnitureParent != null) break;
                    }
                }
            }

            //Search ALL furniture; this is expensive and hopefully won't ever be needed...
            if(furnitureParent == null)
            {
                foreach(NewRoom r in CityData.Instance.roomDirectory)
                {
                    furnitureParent = r.individualFurniture.Find(item => item.id == fp);
                    if (furnitureParent != null) break;
                }
            }

            if (furnitureParent != null)
            {
                //This is an integrated interactable if fsoi is -1
                if (fsoi <= -1)
                {
                    if (!furnitureParent.integratedInteractables.Contains(this))
                    {
                        furnitureParent.integratedInteractables.Add(this);
                    }
                }
            }
            else
            {
                Game.LogError("Unable to find furniture parent: " + fp);
            }
        }

        //Parse sub object
        if (fsoi > -1 && furnitureParent != null && furnitureParent.furniture != null && subObject == null)
        {
            if (fsoi >= furnitureParent.furniture.subObjects.Count)
            {
                Game.LogError("Furniture sub object out of range! Are you sure this is the correct furniture? " + fsoi);
            }
            else
            {
                subObject = furnitureParent.furniture.subObjects[fsoi];
            }
        }

        if (furnitureParent != null && subObject != null)
        {
            Game.Log("...Found furniture parent and sub object index...");
            ConvertToFurnitureSpawnedObject(furnitureParent, subObject, updatePosition, false);
        }

        //Load if geometry is already loaded
        if (updatePosition && node != null && node.room != null && node.room.geometryLoaded)
        {
            if (furnitureParent != null)
            {
                if (furnitureParent.spawnedObject != null)
                {
                    LoadInteractableToWorld();
                }
            }
            else LoadInteractableToWorld();
        }
    }

    //Returns true if this is classed as litter or used up
    public bool IsLitter()
    {
        if (preset.isLitter) return true;

        if (preset.isInventoryItem && preset.consumableAmount > 0f && cs <= 0f) return true;

        return false;
    }

    //Is this classed as a valid pickup target for this citizen if it's on the floor?
    public bool PickUpTarget(Human pickerUpper)
    {
        if (pickerUpper.isStunned || pickerUpper.isDead || pickerUpper.currentNerve <= 0.05f) return false;
        if (inInventory != null) return false;
        if (rem) return false;
        if (rPl) return false;
        if (phy) return false;
        if (!preset.spawnable || preset.prefab == null) return false;
        if (controller != null && controller.isCarriedByPlayer) return false;
        if (controller != null && controller.physicsOn) return false;
        if (!preset.AIWillCorrectPosition) return false;
        if (FirstPersonItemController.Instance.slots.Exists(item => item.interactableID > -1 && item.interactableID == id)) return false;

        //Has been moved
        if (preset.physicsProfile != null && preset.tamperEnabled && isTampered)
        {
            //Contains pick up option
            if (aiActionReference.ContainsKey(RoutineControls.Instance.pickupFromFloor))
            {
                //Is money
                if (preset.isMoney) return true;

                //Is consumable
                if (preset.isInventoryItem && preset.consumableAmount > 0f && cs > 0f && belongsTo != pickerUpper && pickerUpper.locationsOfAuthority.Contains(node.gameLocation))
                {
                    return true;
                }

                //Is litter
                if (IsLitter())
                {
                    return !pickerUpper.isLitterBug;
                }

                //Otherwise original position must not be location of authority
                if(pickerUpper.locationsOfAuthority.Contains(spawnNode.gameLocation))
                {
                    return true;
                }

                return false;
            }
        }

        return false;
    }

    public void RemoveManuallyCreatedFingerprints()
    {
        for (int u = 0; u < df.Count; u++)
        {
            Interactable.DynamicFingerprint p = df[u];

            if (p != null && p.life == Interactable.PrintLife.manualRemoval)
            {
                RemoveDynamicPrint(p);
                u--;
            }
        }
    }

    //Return the interaction distance for this object
    public float GetReachDistance()
    {
        return (GameplayControls.Instance.interactionRange * (1f + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.reachModifier))) + preset.rangeModifier;
    }

    public float GetSecurityStrength()
    {
        if(node != null && node.gameLocation != null)
        {
            return Toolbox.Instance.GetNormalizedLandValue(node.gameLocation) * GameplayControls.Instance.sabotageLandValueMP;
        }

        return Toolbox.Instance.GetPsuedoRandomNumber(0f, 0.5f, wPos + id.ToString()) * GameplayControls.Instance.sabotageLandValueMP;
    }

    //Does this object react to external stimuli? (Physics)
    public bool IsInteractablePhysicsObject()
    {
        bool ret = false;

        if(preset != null)
        {
            if (preset.physicsProfile != null)
            {
                if(preset.reactWithExternalStimuli)
                {
                    ret = true;
                }
            }
        }

        return ret;
    }
}
