﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using UnityEngine.Rendering.HighDefinition;
using UnityEditor;
using System.Threading;
using System;
using System.IO;

//Used to alter the game in various ways for debugging
//Script pass 1
public class Game : MonoBehaviour
{
    [Header("Build")]
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Build version")]
    public string buildID = "20";
    [OnValueChanged("OnBuildValueChanged")]
    public bool displayBuildInMenu = true;
    [Space(7)]
    public string buildDescription = string.Empty;
    public string customTags = string.Empty;
    public string steamScriptPath;
    [OnValueChanged("OnBuildValueChanged")]
    public bool updateAbove = false;
    public string lastCompatibleCities = "30.00";

    [Header("Build Settings")]
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Enable dev options on main menu & collect debugging info on a range of gameobjects")]
    public bool devMode = false;
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Print to console")]
    public bool printDebug = true;
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Collect debug data")]
    public bool collectDebugData = false;
    [OnValueChanged("OnBuildValueChanged")]
    [EnableIf("printDebug")]
    [Range(0, 2)]
    [Tooltip("Set the debug prints level: 0 is nothing, 2 is maximum")]
    public int debugPrintLevel = 2;
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Enable bug reporting")]
    public bool enableBugReporting = false;
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Enable feedback form link button")]
    public bool enableFeedbackFormLink = false;
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Force the game to be english")]
    public bool forceEnglish = false;
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Skip intro")]
    public bool skipIntro = false;

    [Header("Demo Limitations")]
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Is this a demo build? Limit time")]
    public bool timeLimited = false;
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Limit time to this in minutes")]
    public float timeLimit = 60f;
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Start the timer after exit of apartment in story mode")]
    public bool startTimerAfterApartmentExit = true;
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Pause the timer on caseboard or menu")]
    public bool pauseTimerOnGamePause = false;
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Disable save games")]
    public bool disableSaveLoadGames = false;
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Disable sandbox mode")]
    public bool disableSandbox = false;
    [OnValueChanged("OnBuildValueChanged")]
    [Tooltip("Disable city generation")]
    public bool disableCityGeneration = false;
    [OnValueChanged("OnBuildValueChanged")]
    public bool smallCitiesOnly = false;
    [OnValueChanged("OnBuildValueChanged")]
    public bool displayBetaMessage = false;

    [Header("Game")]
    [Tooltip("Use compression to store save games")]
    public bool useSaveGameCompression = false;
    [EnableIf("useSaveGameCompression")]
    [Range(0, 10)]
    public int saveGameCompressionQuality = 9;
    [Tooltip("Use compression to store generated city data")]
    public bool useCityDataCompression = false;
    [EnableIf("useCityDataCompression")]
    [Range(0, 10)]
    public int cityDataCompressionQuality = 9;
    [Tooltip("How many threads to spawn on certain loading/generation routines")]
    public int maxThreads = 16;
    [Tooltip("Automatically write unfound text entries to .csv files")]
    public bool writeUnfoundToTextFiles = true;
    [Tooltip("Start the game without loading a story chapter")]
    public bool sandboxMode = false;
    [DisableIf("sandboxMode")]
    [Tooltip("Load this chapter")]
    public int loadChapter = 0;
    [Tooltip("If true, the player movement will update in the regular Update() loop, if false it will update in FixedUpdate()")]
    public bool updateMovementEveryFrame = false;
    [Tooltip("Default amount of saturation: Needed as we modify it with loosing health etc")]
    public float defaultSaturationAmount = 10f;
    [Tooltip("If true this will display control tips")]
    public bool displayExtraControlHints = true;
    [Tooltip("If true this will display objective markers")]
    public bool objectiveMarkers = true;
    [Tooltip("General game difficulty")]
    [Range(0, 3)]
    public int gameDifficulty = 1;
    public List<float> difficultyIncomingDamageMultipliers = new List<float>();
    [Tooltip("Game length")]
    [Range(0, 4)]
    public int gameLength = 2;
    public List<int> gameLengthMaxLevels = new List<int>();
    [Tooltip("Force side missions to be a certain difficulty")]
    public bool forceSideJobDifficulty = false;
    [Range(0, 6)]
    public int forcedJobDifficulty = 2;
    [Tooltip("Resume the game after pinning evidence item")]
    public bool resumeAfterPin = false;
    [Tooltip("Close windows with world interaction status on resume game")]
    public bool closeInteractionsOnResume = true;
    [Tooltip("Enables the directional arrow")]
    public bool enableDirectionalArrow = true;
    public float sandboxStartTime = 9f;
    [Tooltip("Enables the murderer in sandbox mode")]
    public bool enableMurdererInSandbox = true;
    public float weatherChangeFrequency = 0.5f;
    [Tooltip("Enable room loading thresholds: When a certain number of rooms are loaded, unload uneeded ones down to a certain cached number")]
    public bool roomCacheLimit = true;
    public bool allowNudity = false;
    public bool disableSnow = false;
    public bool disableTrespass = false;
    public bool debugMurdererOnStart = false;
    [Tooltip("Starts the game outside the door of the apartment in story mode")]
    public bool demoChapterSkip = false;
    public bool demoMode = false;
    [Tooltip("Gives access to all hospital doors")]
    public bool allHospitalAccess = false;
    [Tooltip("Auto pause the game after x seconds of no input")]
    public bool autoPause = false;
    [EnableIf("autoPause")]
    public int autoPauseSeconds = 90;
    [Tooltip("Ask to restart the game from this save game if left for x seconds of no input")]
    public bool demoAutoReset = false;
    [EnableIf("demoAutoReset")]
    public int resetSeconds = 90;
    [EnableIf("demoAutoReset")]
    public int resetChapterPart = 23;
    [EnableIf("demoAutoReset")]
    public string resetSaveGameName;
    public float textSpeed = 1f;
    [Tooltip("Disables closing of case board: Used sometimes by the tutorial")]
    [ReadOnly]
    public bool disableCaseBoardClose = false;
    [Tooltip("Allows/disables licensed music")]
    public bool allowLicensedMusic = true;
    [Tooltip("Override all passcodes")]
    public bool overridePasscodes = false;
    public int overriddenPasscode = 1000;
    [Tooltip("Max delta time")]
    public float maxDeltaTime = 1.05f;

    //For detecting if on main thread...
    [NonSerialized]
    public Thread mainThread; 

    [Header("Statuses")]
    public bool coldStatusEnabled = true;
    public bool smellyStatusEnabled = true;
    public bool headacheStatusEnabled = true;
    public bool injuryStatusEnabled = true;
    public bool tiredStatusEnabled = true;
    public bool hungerStatusEnabled = true;
    public bool hydrationStatusEnabled = true;
    public bool numbStatusEnabled = true;
    public bool bleedingStatusEnabled = true;
    public bool wetStatusEnabled = true;
    public bool sickStatusEnabled = true;
    public bool drunkStatusEnabled = true;
    public bool starchAddictionEnabled = true;
    public bool poisonStatusEnabled = true;
    public bool blindedStatusEnabled = true;
    [Space(7)]
    public bool energizedStatusEnabled = true;
    public bool hydratedStatusEnabled = true;
    public bool focusedStatusEnabled = true;
    public bool wellRestedStatusEnabled = true;

    [Header("Controls")]
    [Tooltip("Enables mouse/keyboard control by default on startup; disable for console builds")]
    public Vector2 mouseSensitivity = new Vector2(20, 20);
    public Vector2 controllerSensitivity = new Vector2(20, 20);
    public Vector2 axisMP = new Vector2(1, 1);
    public bool controlAutoSwitch = true;
    public int mouseSmoothing = 10;
    public int controllerSmoothing = 40;
    public float movementSpeed = 1f;
    public int scrollSensitivity = 50;

    [Header("Player")]
    public string playerFirstName = "Fred";
    public string playerSurname = "Melrose";
    public Human.Gender playerGender = Human.Gender.male;
    public Human.Gender partnerGender = Human.Gender.male;
    public Color playerSkinColour;
    public int playerBirthDay = 19;
    public int playerBirthMonth = 12;
    public int playerBirthYear = 1947;

    [Header("Words")]
    [Tooltip("The game's language reference string")]
    public string language = "English";
    public int wordCountTotal = 0;

    [Button]
    public void WordCount()
    {
        wordCountTotal = 0;

        char sep = ' ';

        foreach (KeyValuePair<string, Dictionary<string, Strings.DisplayString>> mainDict in Strings.stringTable)
        {
            foreach (KeyValuePair<string, Strings.DisplayString> pair in mainDict.Value)
            {
                string[] words = Strings.CleanSplit(pair.Value.displayStr, sep, true, true);
                wordCountTotal += words.Length;

                words = Strings.CleanSplit(pair.Value.alternateStr, sep, true, true);
                wordCountTotal += words.Length;
            }
        }
    }

    [Header("Streets")]
    public bool displayStreetChunks = false;
    public bool displayStreetAndJunctionChunks = false;
    [EnableIf("displayStreetAndJunctionChunks")]
    public bool displayTrafficSimulationResults = false;
    public Material debugtrafficSimMaterial;
    [DisableIf("displayStreetAndJunctionChunks")]
    public bool displayStreets = false;
    public Material debugStreetMaterial;
    public List<Color> streetDebugColours = new List<Color>();

    [Header("City")]
    [Tooltip("Display roads")]
    public bool debugDisplayRoads = false;
    [Tooltip("Player can open any door")]
    public bool keysToTheCity = true;
    [Tooltip("Disable generation of furniture")]
    public bool disableFurniture = false;
    [Tooltip("Destroy this when the game is started")]
    public Transform debugContainer;
    [Tooltip("Enable culling debug for new cull system")]
    public bool enableCullingDebug = false;

    [Header("Citizens")]
    [Tooltip("Statistic for keeping track of how early/late citizens are on average compared with due time. Records guess accuracy.")]
    public bool collectRoutineTimingInfo = true;
    //Plus values are late, minus are early
    public float guessAverageOnTime = 0f;
    public int guessDataEntries = 0;
    public float guessEarlyPercent = 0f;
    public float guessLatePercent = 0f;
    [System.NonSerialized]
    public float guessCumulativeOnTime = 0f;
    [System.NonSerialized]
    public int guessEarlyEntries = 0;
    [System.NonSerialized]
    public int guessLateEntries = 0;
    public Vector2 boundaries = Vector2.zero;
    [Tooltip("If enabled, citizens won't react to getting attacked by the player")]
    public bool noReactOnAttack = false;

    [Header("Pathfinding")]
    [Tooltip("If pathfinding fails, load objects to present the closed set for debugging")]
    public bool debugPathfinding = false;
    [Tooltip("Use the job system for pathfinding")]
    public bool useJobSystem = true;
    [Tooltip("Cache external routes for the pathfinding system")]
    public bool useExternalRouteCaching = true;
    [Tooltip("Cache internal routes for the pathfinding system")]
    public bool useInternalRouteCaching = true;
    [Tooltip("Force street pathing to be run on the main thread, this appears to be faster due to the data passed to the worker?")]
    public bool forceStreetPathsOnMainThread = true;
    [Tooltip("If true the maximum cached path numbers will be ignored. The game could run out of memory eventually, so be careful!")]
    public bool unlimitedPathCaching = false;
    [Tooltip("The maximum number of cached external paths")]
    public int maxExternalCachedPaths = 1000;
    [Tooltip("The maximum number of cached paths in an internal address")]
    public int maxInternalCachedPaths = 12;
    [Tooltip("The maximum number of cached street paths")]
    public int maxStreetCachedPaths = 1000;
    [Tooltip("Enable dynamic rerouting")]
    public bool dynamicReRouting = true;

    [Space(7)]
    public List<string> pathfinderDebugLog = new List<string>();

    [Header("Evidence")]
    [Tooltip("Discover all evidence as soon as it is created")]
    public bool discoverAllEvidence = false;

    [Header("Map")]
    [Tooltip("Discover all rooms")]
    public bool discoverAllRooms = false;

    [Header("Gameplay")]
    [Tooltip("Player is always in illegal area")]
    public bool everywhereIllegal = false;
    [Tooltip("Player is always hidden for AI")]
    public bool invisiblePlayer = false;
    [Tooltip("Player cannot be heard by AI")]
    public bool inaudiblePlayer = false;
    [Tooltip("Player cannot be killed")]
    public bool invinciblePlayer = false;
    [Tooltip("Plotting a route will teleport the player to that location isntead")]
    public bool routeTeleport = false;
    [Tooltip("Give all upgrades available in the city")]
    public bool giveAllUpgrades = false;
    [Tooltip("Disable fall damage")]
    public bool disableFallDamage = false;
    [Tooltip("Pause the AI completely")]
    public bool pauseAI = false;
    [Tooltip("Free camera mode")]
    public bool freeCam = false;
    [Tooltip("Fast forward")]
    public bool fastForward = false;
    [Tooltip("Disable negatives statuses while in story mode")]
    public bool disableSurvivalStatusesInStory = true;

    public bool sandboxStartingApartment = false;
    public int sandboxStartingMoney = 100;
    public int sandboxStartingLockpicks = 5;
    [Tooltip("Build types where the player apartment will be located")]
    public List<BuildingPreset> preferredStartingBuildings = new List<BuildingPreset>();

    [Header("Difficulty: Prices")]
    public float jobRewardMultiplier = 1f;
    public float jobPenaltyMultiplier = 1f;
    public float housePriceMultiplier = 1f;

    [Header("Graphics")]
    [Tooltip("Switch indoor lights to non-shadow casting when player is in a different groundmap area")]
    public bool noShadowsWhenPlayerIsInDifferentGoundmapLocation = true;
    [Tooltip("Spawn & setup reflection probes")]
    public bool useReflectionProbes = true;
    [Tooltip("Use a special raindrop material/shader on the streets")]
    public bool enableRaindrops = true;
    [Tooltip("Use a special raindrop material/shader on the windows")]
    public bool enableRainyWindows = true;
    public int fov = 70;
    public bool depthBlur = true;
    public float motionBlurIntensity = 1f;
    public float motionBlurGameSpeedModifier = 1f;
    public float bloomIntensity = 0.151f;
    public bool shadowsOnCitizenLOD = true;
    public bool vsync = false;
    public bool enableFrameCap = false;
    public int frameCap = 60;
    public bool flickeringLights = false;
    public bool enableRuntimeStaticBatching = true;
    [Tooltip("Use quads instead of decal projectors for footprints")]
    public bool useQuadsForFootprints = false;

    [Space(7)]
    [Range(0.4f, 2f)]
    public float lightFadeDistanceMultiplier = 1f;
    [Range(0.4f, 2f)]
    public float shadowFadeDistanceMultiplier = 1f;

    [Header("Shadows")]
    [Tooltip("Same as above but for the sun")]
    public int sunShadowUpdateFrequency = 1;
    [ReadOnly]
    public int lastShadowsUpdatedCount = 0;

    [Tooltip("Override shadow mode on all light controllers...")]
    public bool overrideLightControllerShadowMode = false;
    [EnableIf("overrideLightControllerShadowMode")]
    public LightingPreset.ShadowMode shadowModeOverride = LightingPreset.ShadowMode.everyFrame;

    public int dynamicShadowUpdateFrames = 3;
    public int maxUpdateDynamicShadowsPerFrame = 5;

    [Space(7)]
    public bool overrideLightCullingMode = false;
    public LightingPreset.CullMode lightCullOverride = LightingPreset.CullMode.occlusion;

    [Header("Meshes")]
    [Tooltip("Combining meshes will take longer on load and increase memory, but also increase performance...")]
    public bool combineAirDuctMeshes = true;
    [Tooltip("Combining meshes will take longer on load and increase memory, but also increase performance...")]
    public bool combineRoomMeshes = true;
    public UnityEngine.Rendering.ShadowCastingMode roomWallShadowMode = UnityEngine.Rendering.ShadowCastingMode.On;
    public UnityEngine.Rendering.ShadowCastingMode roomFloorShadowMode = UnityEngine.Rendering.ShadowCastingMode.On;
    public UnityEngine.Rendering.ShadowCastingMode roomCeilingShadowMode = UnityEngine.Rendering.ShadowCastingMode.TwoSided;
    public UnityEngine.Rendering.ShadowCastingMode airDuctShadowMode = UnityEngine.Rendering.ShadowCastingMode.On;

    [Header("Interface")]
    public int uiScale = 100;

    [Header("Editor")]
    public bool selectCitizenOnLookAt = false;
    public int base26Test;
    public bool screenshotMode = false;
    public bool screenshotModeAllowDialog = false;

    public void SetScreenshotMode(bool val, bool allowDialog = false)
    {
        screenshotMode = val;
        screenshotModeAllowDialog = allowDialog;

        foreach(RectTransform rect in InterfaceControls.Instance.screenshotModeToggleObjects)
        {
            if (rect == null) continue;

            if(screenshotMode && screenshotModeAllowDialog)
            {
                if (InterfaceControls.Instance.screenShotModeAllowDialogObjects.Contains(rect)) continue;
            }

            rect.gameObject.SetActive(!screenshotMode);
        }

        if(!screenshotMode)
        {
            try
            {
                InteractionController.Instance.DisplayInteractionCursor(InteractionController.Instance.displayingInteraction, forceUpdate: true); //Force interaction display update
            }
            catch
            {

            }
        }
    }

    [Header("Debug")]
    public List<Actor> debugHuman = new List<Actor>();
    public bool debugHumanMovement = true;
    public bool debugHumanActions = true;
    public bool debugHumanAttacks = true;
    public bool debugHumanUpdates = true;
    public bool debugHumanMisc = true;
    public bool debugHumanSight = true;
    public int debugFindWall;
    public int debugAddressID = 0;
    public int debugCitizenID = 0;
    public int debugPhotoTestID = 0;

    [System.Serializable]
    public class DebugCitizenWeapons
    {
        public MurderWeaponPreset weapon;
        public int count;
        public float percentage;
    }

    public List<DebugCitizenWeapons> debugWeaponsSurvey = new List<DebugCitizenWeapons>();

    //Singleton pattern
    private static Game _instance;
    public static Game Instance { get { return _instance; } }

	void Awake ()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }

        mainThread = System.Threading.Thread.CurrentThread;
        Time.maximumDeltaTime = maxDeltaTime;
    }

    //+ is early, - is late
    public void AddOnTimeEntry(Actor cc, float newOnTime)
    {
        guessCumulativeOnTime += newOnTime;
        guessDataEntries++;

        guessAverageOnTime = guessCumulativeOnTime / (float)guessDataEntries;

        if (newOnTime >= 0f)
        {
            guessEarlyEntries++;
        }
        else
        {
            guessLateEntries++;
        }

        guessEarlyPercent = (float)guessEarlyEntries / (float)guessDataEntries * 100f;
        guessLatePercent = (float)guessLateEntries / (float)guessDataEntries * 100f;

        boundaries = new Vector2(Mathf.Min(boundaries.x, newOnTime), Mathf.Max(boundaries.y, newOnTime));
    }

    public void AIInAddressFullyRested()
    {
        foreach(NewRoom r in Player.Instance.currentGameLocation.rooms)
        {
            foreach(Human h in r.currentOccupants)
            {
                h.AddEnergy(1f);
            }
        }
    }

    public void AIInAddressNeedShower()
    {
        foreach (NewRoom r in Player.Instance.currentGameLocation.rooms)
        {
            foreach (Human h in r.currentOccupants)
            {
                h.AddHygiene(-1f);
            }
        }
    }

    public void AIInAddressNeedFun()
    {
        foreach (NewRoom r in Player.Instance.currentGameLocation.rooms)
        {
            foreach (Human h in r.currentOccupants)
            {
                h.AddExcitement(-1f);
            }
        }
    }

    public void DebugButton()
    {
        foreach(InfoWindow win in InterfaceController.Instance.activeWindows)
        {
            if(win.item != null)
            {
                foreach(Evidence.DataKey key in Toolbox.Instance.allDataKeys)
                {
                    win.passedEvidence.MergeDataKeys(key, Evidence.DataKey.name);
                }
            }
        }

        ////Open murderer
        //InterfaceController.Instance.SpawnWindow(SyndicateController.Instance.criminalTeams[0].members[0].evidenceEntry, Evidence.DataKey.name);

            ////Open current victim
            //InterfaceController.Instance.SpawnWindow(SyndicateController.Instance.motive.victimList[0].evidenceEntry, Evidence.DataKey.name);

            //for (int i = 0; i < analogs.Count; i++)
            //{
            //    //analogs[i].Setup(analogs[i].evidenceEntry);
            //}

            ////Get average memory count
            //int allMems = 0;

            //for (int i = 0; i < CityData.Instance.citizenDirectory.Count; i++)
            //{
            //    allMems += CityData.Instance.citizenDirectory[i].memoryEvents.Count;
            //}

            //averageMemoryCount = (float)allMems / CityData.Instance.citizenDirectory.Count;

            //SessionData.Instance.UpdateUIDay();
    }

    [Button]
    public void ResetRoutineCollectionData()
    {
        guessAverageOnTime = 0f;
        guessDataEntries = 0;
        guessEarlyPercent = 0f;
        guessLatePercent = 0f;
        guessCumulativeOnTime = 0f;
        guessEarlyEntries = 0;
        guessLateEntries = 0;
        boundaries = Vector2.zero;
    }

    [Button]
    public void AddRandomCitizenToAwareness()
    {
        Human random = CityData.Instance.citizenDirectory[Toolbox.Instance.Rand(0, CityData.Instance.citizenDirectory.Count)];
        if (random == null) Game.Log("Random is null!");
        InterfaceController.Instance.AddAwarenessIcon(InterfaceController.AwarenessType.actor, InterfaceController.AwarenessBehaviour.alwaysVisible, random as Actor, null, Vector3.zero, InterfaceControls.Instance.spotted, 10);
    }

    public void ForceEnableMovement()
    {
        Player.Instance.pausedRememberPlayerMovement = true;

        //Re-enable movement
        Player.Instance.EnablePlayerMovement(true);
        Player.Instance.EnablePlayerMouseLook(true);

        //Return from locked-in interaction
        Player.Instance.fps.m_StickToGroundForce = 7;
        Player.Instance.fps.m_GravityMultiplier = 2;
        Player.Instance.EnableCharacterController(true);
    }

    public void SetRaindrops(bool val)
    {
        enableRaindrops = val;

        if(SessionData.Instance.startedGame)
        {
            foreach(StreetController st in CityData.Instance.streetDirectory)
            {
                foreach (KeyValuePair<MeshRenderer, StreetTilePreset.StreetSectionModel> pair in st.loadedModelReference)
                {
                    //Change to rain material
                    if (enableRaindrops && pair.Value.rainMaterial != null)
                    {
                        pair.Key.sharedMaterial = pair.Value.rainMaterial;
                    }
                    else
                    {
                        pair.Key.sharedMaterial = pair.Value.normalMaterial;
                    }
                }
            }
        }

        //Set rain texture rain amount
        if (enableRaindrops)
        {
            foreach (Material mat in SessionData.Instance.raindropMaterials)
            {
                mat.SetFloat("_Rain", SessionData.Instance.currentRain);
            }
        }
    }

    public void SetRainWindows(bool val)
    {
        if(val != enableRainyWindows)
        {
            enableRainyWindows = val;

            if(CityData.Instance != null)
            {
                foreach(NewRoom r in CityData.Instance.roomDirectory)
                {
                    foreach(NewNode n in r.nodes)
                    {
                        foreach(NewWall w in n.walls)
                        {
                            foreach (NewWall.FrontageSetting frontage in w.frontagePresets)
                            {
                                if (frontage.mainTransform == null) continue;

                                //Switch the material now...
                                if (enableRainyWindows && (w.node.room.IsOutside() || w.otherWall.node.room.IsOutside()))
                                {
                                    foreach (Transform tr in frontage.mainTransform)
                                    {
                                        if (tr.tag == "RainWindowGlass")
                                        {
                                            MeshRenderer rend = tr.gameObject.GetComponent<MeshRenderer>();
                                            if (rend != null) rend.sharedMaterial = frontage.preset.rainyGlass;
                                        }
                                    }
                                }
                                else if (frontage.preset.regularGlass != null)
                                {
                                    foreach (Transform tr in frontage.mainTransform)
                                    {
                                        if (tr.tag == "RainWindowGlass")
                                        {
                                            MeshRenderer rend = tr.gameObject.GetComponent<MeshRenderer>();
                                            if (rend != null) rend.sharedMaterial = frontage.preset.regularGlass;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }

    public void SetFOV(int val)
    {
        fov = val;

        if(CameraController.Instance != null && CameraController.Instance.cam != null)
        {
            CameraController.Instance.cam.fieldOfView = fov;
        }
    }

    public void SetObjectiveMarkers(bool val)
    {
        objectiveMarkers = val;

        foreach(Objective obj in InterfaceController.Instance.displayedObjectives)
        {
            if (obj.pointer != null) obj.pointer.Remove(); //Remove world pointer
        }
    }

    public void SetDirectionalArrow(bool val)
    {
        enableDirectionalArrow = val;

        if (enableDirectionalArrow)
        {
            MapController.Instance.directionalArrowContainer.SetActive(MapController.Instance.displayDirectionArrow);
        }
        else
        {
            MapController.Instance.directionalArrowContainer.SetActive(false);
        }
    }

    public void SetAwarenessIndicator(bool val)
    {
        InterfaceController.Instance.backgroundTransform.gameObject.SetActive(val);
    }

    public void SetDepthBlur(bool val)
    {
        depthBlur = val;
        InterfaceController.Instance.UpdateDOF();
    }

    public void SetSandboxStartTime(float val)
    {
        sandboxStartTime = val;
    }

    public void SetGameDifficulty(int val)
    {
        gameDifficulty = val;
    }

    public void SetGameLength(int val, bool updateSocialCredits, bool updateDropdown, bool updateSavedValue)
    {
        gameLength = val;
        Game.Log("CityGen: Set game length: " + val);

        if(updateSocialCredits) BioScreenController.Instance.SetMaxSocialCreditLevels(gameLengthMaxLevels[gameLength]);
        if (updateDropdown) MainMenuController.Instance.gameLengthDropdown.dropdown.SetValueWithoutNotify(gameLength);
        if(updateSavedValue) MainMenuController.Instance.gameLengthDropdown.dropdown.value = gameLength;
    }

    public void SetEnableColdStatus(bool val)
    {
        coldStatusEnabled = val;
        if(SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableSmellyStatus(bool val)
    {
        smellyStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableHeadacheStatus(bool val)
    {
        headacheStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableBleedingStatus(bool val)
    {
        bleedingStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableInjuryStatus(bool val)
    {
        injuryStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableHungerStatus(bool val)
    {
        hungerStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableHydrationStatus(bool val)
    {
        hydrationStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableWetStatus(bool val)
    {
        wetStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableSickStatus(bool val)
    {
        sickStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableNumbStatus(bool val)
    {
        numbStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableTiredStatus(bool val)
    {
        tiredStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableDrunkStatus(bool val)
    {
        drunkStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableEnergizedStatus(bool val)
    {
        energizedStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableHydratedStatus(bool val)
    {
        hydratedStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableFocusedStatus(bool val)
    {
        focusedStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetEnableWellRestedStatus(bool val)
    {
        wellRestedStatusEnabled = val;
        if (SessionData.Instance.startedGame) StatusController.Instance.ForceStatusCheck();
    }

    public void SetSandboxStartingApartment(bool val)
    {
        sandboxStartingApartment = val;
    }

    public void SetSandboxStartingMoney(int val)
    {
        sandboxStartingMoney = val;
    }

    public void SetSandboxStartingLockpicks(int val)
    {
        sandboxStartingLockpicks = val;
    }

    public void SetForceSideJobDifficulty(bool val)
    {
        forceSideJobDifficulty = val;
    }

    public void SetForcedSideJobDifficulty(int val)
    {
        forcedJobDifficulty = val;
    }

    public void SetPauseAI(bool val)
    {
        pauseAI = val;
    }

    public void SetFreeCamMode(bool val)
    {
        freeCam = val;

        if(freeCam)
        {
            Player.Instance.EnableGhostMovement(true, false);
            Player.Instance.SetPlayerHeight(0.01f, false);
            Player.Instance.SetCameraHeight(0f);
        }
        else
        {
            Player.Instance.SetPlayerHeight(Player.Instance.GetPlayerHeightNormal());
            Player.Instance.SetCameraHeight(GameplayControls.Instance.cameraHeightNormal);
            Player.Instance.EnableGhostMovement(false, true);
        }
    }

    public void SetFastForward(bool val)
    {
        fastForward = val;

        if (fastForward)
        {
            SessionData.Instance.SetTimeSpeed(SessionData.TimeSpeed.veryFast);
        }
        else
        {
            SessionData.Instance.SetTimeSpeed(SessionData.TimeSpeed.normal);
        }
    }

    //TODO: Switch to quality settings?
    public void SetDrawDistance(float val)
    {
        //Game.Log("LOD Bias: " + val);
        //QualitySettings.lodBias = 1;
    }

    public void SetLightDistance(float val)
    {
        lightFadeDistanceMultiplier = Mathf.Clamp(val, 0.4f, 2f);
        shadowFadeDistanceMultiplier = Mathf.Clamp(val, 0.4f, 2f);

        if(SessionData.Instance.startedGame)
        {
            LightController[] lightControllers = UnityEngine.Object.FindObjectsOfType<LightController>(true);

            foreach (LightController v in lightControllers)
            {
                v.UpdateFadeDistances();
            }
        }
    }

    public void SetMurders(bool val)
    {
        enableMurdererInSandbox = val;

        if(MurderController.Instance != null && Game.Instance.sandboxMode && SessionData.Instance.startedGame)
        {
            MurderController.Instance.SetProcGenKillerLoop(enableMurdererInSandbox);
        }
    }

    //Possible TODO
    public void SetUIScale(int val)
    {
        //uiScale = val;
        //float floatScale = uiScale / 100f;

        //if(InterfaceController.Instance.uiScaling != null)
        //{
        //    foreach(RectTransform rect in InterfaceController.Instance.uiScaling)
        //    {
        //        if (rect == null) continue;
        //        rect.localScale = new Vector3(floatScale, floatScale, floatScale);
        //    }
        //}
    }

    public void SetAAMode(int newMode)
    {
        HDAdditionalCameraData cd = CameraController.Instance.cam.gameObject.GetComponent<HDAdditionalCameraData>();

        if (cd != null)
        {
            Game.Log("Menu: Set AA Mode: " + (HDAdditionalCameraData.AntialiasingMode)newMode);
            cd.antialiasing = (HDAdditionalCameraData.AntialiasingMode)newMode;
        }
        else Game.LogError("Could not find HDAdditionalCameraData on camera!");
    }

    public void SetAAQuality(int newQuality)
    {
        HDAdditionalCameraData cd = CameraController.Instance.cam.gameObject.GetComponent<HDAdditionalCameraData>();

        if (cd != null)
        {
            Game.Log("Menu: Set AA Quality: " + (HDAdditionalCameraData.SMAAQualityLevel)newQuality);
            cd.SMAAQuality = (HDAdditionalCameraData.SMAAQualityLevel)newQuality;
            cd.TAAQuality = (HDAdditionalCameraData.TAAQualityLevel)newQuality;
        }
        else Game.LogError("Could not find HDAdditionalCameraData on camera!");
    }

    public void SetDithering(bool newVal)
    {
        HDAdditionalCameraData cd = CameraController.Instance.cam.gameObject.GetComponent<HDAdditionalCameraData>();

        if (cd != null)
        {
            Game.Log("Menu: Set Dithering: " + newVal);
            cd.dithering = newVal;
        }
        else Game.LogError("Could not find HDAdditionalCameraData on camera!");
    }

    public void SetVsync(bool newVal)
    {
        vsync = newVal;

        if (vsync)
        {
            QualitySettings.vSyncCount = 1;

            //if(enableFrameCap)
            //{
            //    SetEnableFrameCap(false);

            //    PlayerPrefs.SetInt("enableFrameCap", 0); //Save to player prefs

            //    PlayerPrefsController.GameSetting findSetting = PlayerPrefsController.Instance.gameSettingControls.Find(item => item.identifier == "enableFrameCap"); //Case insensitive find
            //    findSetting.intValue = 0;

            //    PlayerPrefsController.Instance.OnToggleChanged("enableFrameCap", false);
            //}
        }
        else
        {
            QualitySettings.vSyncCount = 0;
        }

        Game.Log("Set Vsync count: " + QualitySettings.vSyncCount);
    }

    public void SetEnableFrameCap(bool newVal)
    {
        enableFrameCap = newVal;

        if(enableFrameCap)
        {
            //if (vsync)
            //{
            //    SetVsync(false);

            //    PlayerPrefs.SetInt("vsync", 0); //Save to player prefs

            //    PlayerPrefsController.GameSetting findSetting = PlayerPrefsController.Instance.gameSettingControls.Find(item => item.identifier == "vsync"); //Case insensitive find
            //    findSetting.intValue = 0;

            //    PlayerPrefsController.Instance.OnToggleChanged("vsync", false);
            //}
        }

        SetFrameCap(frameCap);
    }

    public void SetFrameCap(int newVal)
    {
        frameCap = newVal;

        if(enableFrameCap)
        {
            Application.targetFrameRate = frameCap;
        }
        else
        {
            Application.targetFrameRate = -1;
        }
    }

    public void SetPasscodeOverrideToggle(bool newVal)
    {
        overridePasscodes = newVal;
    }

    public void SetPasscodeOverride(int newPasscode)
    {
        overriddenPasscode = newPasscode;
    }

    public void SetFlickingLights(bool newVal)
    {
        flickeringLights = newVal;
    }

    public static void Log(object print, int level = 2)
    {
        if(Instance.printDebug && Instance.debugPrintLevel >= level)
        {
            Debug.Log(print);
        }
    }

    public static void LogError(object print, int level = 2)
    {
        if (Instance.devMode && Instance.printDebug && Instance.debugPrintLevel >= level)
        {
            Debug.LogError(print);
        }
    }

    public void SetAllowLicensedMusic(bool val)
    {
        allowLicensedMusic = val;

        if(!allowLicensedMusic)
        {
            List<AudioController.LoopingSoundInfo> loopsToStop = new List<AudioController.LoopingSoundInfo>();

            foreach(AudioController.LoopingSoundInfo loop in AudioController.Instance.loopingSounds)
            {
                if(loop != null && loop.eventPreset != null && loop.eventPreset.isLicensed)
                {
                    loopsToStop.Add(loop);
                }
            }

            foreach(AudioController.LoopingSoundInfo loop in loopsToStop)
            {
                AudioController.Instance.StopSound(loop, AudioController.StopType.immediate, "stop licensed music/sound");
            }
        }
    }

    [Button]
    public void Base26Test()
    {
        int myNumber = base26Test + 1;

        var array = new LinkedList<int>();

        while (myNumber > 26)
        {
            int value = myNumber % 26; //Gives 0 - 25

            if (value == 0)
            {
                myNumber = myNumber / 26 - 1;
                array.AddFirst(26);
            }
            else
            {
                myNumber /= 26;
                array.AddFirst(value);
            }
        }

        if (myNumber >= 0)
        {
            array.AddFirst(myNumber);
        }

        Game.Log(new string(array.Select(s => (char)('A' + s - 1)).ToArray()));
    }

    [Button]
    public void Give1000Crows()
    {
        GameplayController.Instance.AddMoney(1000, true, "cheat");
    }

    [Button]
    public void Give100Lockpicks()
    {
        GameplayController.Instance.AddLockpicks(100, false);
    }

    [Button]
    public void ResetHealth()
    {
        Player.Instance.ResetHealthToMaximum();
    }

    [Button]
    public void KOPlayer()
    {
        Player.Instance.AddHealth(-99999, false);
    }

    [Button]
    public void TestCurrentDetainedStatus()
    {
        StatusController.Instance.GetCurrentDetainedStatus();
    }

    [Button]
    public void VictimsRankTest()
    {
        Game.Log(Mathf.InverseLerp(GameplayControls.Instance.worstCaseVictimCount, GameplayControls.Instance.bestCaseVictimCount, 1));
        Game.Log(Mathf.InverseLerp(GameplayControls.Instance.worstCaseVictimCount, GameplayControls.Instance.bestCaseVictimCount, 2));
        Game.Log(Mathf.InverseLerp(GameplayControls.Instance.worstCaseVictimCount, GameplayControls.Instance.bestCaseVictimCount, 3));
        Game.Log(Mathf.InverseLerp(GameplayControls.Instance.worstCaseVictimCount, GameplayControls.Instance.bestCaseVictimCount, 4));
        Game.Log(Mathf.InverseLerp(GameplayControls.Instance.worstCaseVictimCount, GameplayControls.Instance.bestCaseVictimCount, 5));
    }

    [Button]
    public void GiveAllUpgrades()
    {
        foreach(SyncDiskPreset disk in Toolbox.Instance.allSyncDisks)
        {
            if(disk.interactable != null)
            {
                Interactable newDisk = InteractableCreator.Instance.CreateWorldInteractable(disk.interactable, Player.Instance, null, null, Player.Instance.transform.position + new Vector3(Toolbox.Instance.Rand(-1f, 1f), 0, Toolbox.Instance.Rand(-1f, 1f)), Vector3.zero, null, passedObject: disk);
                if (newDisk != null) newDisk.ForcePhysicsActive(false, false);
            }
        }
    }

    [Button]
    public void GiveSocialCredit()
    {
        GameplayController.Instance.AddSocialCredit(50, true, "cheat");
    }

    [Button]
    public void CompleteSideJob()
    {
        if(CasePanelController.Instance.activeCase != null)
        {
            if(CasePanelController.Instance.activeCase.job != null)
            {
                CasePanelController.Instance.activeCase.job.Complete();
            }
        }
    }

    [Button]
    public void DisplayAnswersToCurrentSideJob()
    {
        if (CasePanelController.Instance.activeCase != null)
        {
            if (CasePanelController.Instance.activeCase.job != null)
            {
                CasePanelController.Instance.activeCase.job.DebugDisplayAnswers();
            }
        }
    }

    public void OnBuildValueChanged()
    {
        buildDescription = buildID;

        //Add custom tags here
        if (customTags != null && customTags.Length > 0)
        {
            string[] s = customTags.Split(' ');

            foreach(string str in s)
            {
                buildDescription += " [" + str + "]";
            }
        }

        if (devMode) buildDescription += " [Dev]";
        if (printDebug) buildDescription += " [Debug]";
        if (collectDebugData) buildDescription += " [CollData]";
        if (enableBugReporting) buildDescription += " [BugRep]";
        if (enableFeedbackFormLink) buildDescription += " [Feedback]";

        if (timeLimited) buildDescription += " [TimeLimit: " + timeLimit + "]";
        if (disableSaveLoadGames) buildDescription += " [NoLoadSave]";
        if (disableSandbox) buildDescription += " [NoSandbox]";
        if (disableCityGeneration) buildDescription += " [NoGeneration]";

        if (smallCitiesOnly) buildDescription += " [SmallCities]";
        if (displayBetaMessage) buildDescription += " [BetaMsg]";

        if (forceEnglish) buildDescription += " [ForceENG]";

        //Write to the steam script path
        if(steamScriptPath != null && steamScriptPath.Length > 0)
        {
            if(File.Exists(steamScriptPath))
            {
                var lines = File.ReadAllLines(steamScriptPath);
                lines[3] = "      \"Desc\" \"" + buildDescription + "\" // internal description for this build";
                File.WriteAllLines(steamScriptPath, lines);
            }
        }
    }

    [Button]
    public void DebugFindWall()
    {
        foreach (NewGameLocation l in CityData.Instance.gameLocationDirectory)
        {
            foreach(NewRoom r in l.rooms)
            {
                foreach(NewNode n in r.nodes)
                {
                    foreach (NewWall w in n.walls)
                    {
                        if(w.id == debugFindWall)
                        {
                            Game.Log("Found wall " + debugFindWall + " at " + w.position + " euler: " + w.localEulerAngles + " door " + w.door);
                            break;
                        }
                    }
                }
            }
        }
    }

    [Button]
    public void DebugAddressID()
    {
        NewAddress add = CityData.Instance.addressDirectory.Find(item => item.id == debugAddressID);

        if(add != null)
        {
#if UNITY_EDITOR
            Selection.SetActiveObjectWithContext(add.gameObject, add.gameObject);
#endif
            Game.Log(add.name + " at " + add.floor.floor + " in " + add.building.name);
        }
    }

    [Button]
    public void DebugCitizenID()
    {
        Human cit = CityData.Instance.citizenDirectory.Find(item => item.humanID == debugCitizenID);

        if (cit != null)
        {
            Game.Log(cit.GetCitizenName());
        }
    }

    [Button]
    public void ShotgunTest()
    {
        for (int i = 0; i < 12; i++)
        {
            Toolbox.Instance.Shoot(Player.Instance, CameraController.Instance.cam.transform.position, CameraController.Instance.cam.transform.forward, 16, 0.1f, 0.1f, GameplayControls.Instance.sentryGunWeapon, false, CameraController.Instance.cam.transform.position, false);
        }
    }

    //Picks a random item and takes a mission style photo of it
    [Button]
    public void MissionPhotoTest()
    {
        Interactable chosenItem = null;

        if (debugPhotoTestID != 0)
        {
            chosenItem = CityData.Instance.interactableDirectory.Find(item => item.id == debugPhotoTestID);
        }

        if (chosenItem == null)
        {
            List<Interactable> getItem = CityData.Instance.interactableDirectory.FindAll(item => item.inInventory == null && item.preset.spawnable && item.preset.prefab != null && item.node != null && item.node.gameLocation.thisAsStreet != null);

            //This is now done with dialog trigger
            chosenItem = getItem[Toolbox.Instance.Rand(0, getItem.Count)];
        }

        Game.Log("Player: Chosen to capture item " + chosenItem.GetName() + " " + chosenItem.id + " at " + chosenItem.GetWorldPosition());

        Interactable hiddenItemPhoto = Toolbox.Instance.GetLocalizedSnapshot(chosenItem);

        Player.Instance.Teleport(chosenItem.node, null); //Teleport player

        if (hiddenItemPhoto != null)
        {
            ActionController.Instance.Inspect(hiddenItemPhoto, Player.Instance.currentNode, Player.Instance);
        }
        else Game.LogError("Unable to generate hidden item photo");
    }

    [Button]
    public void FindProsthetics()
    {
        foreach(Citizen cit in CityData.Instance.citizenDirectory)
        {
            foreach(Human.Trait t in cit.characterTraits)
            {
                if(t.trait.name == "Affliction-LostLeftArm" || t.trait.name == "Affliction-LostLeftArmWhole" || t.trait.name == "Affliction-LostRightArm" || t.trait.name == "Affliction-LostRightArmWhole" || t.trait.name == "Affliction-LostRightLeg" || t.trait.name == "Affliction-LostLeftLeg")
                {
                    Game.Log(cit.citizenName + " (homeless: " + cit.isHomeless + ", job: " + cit.job.preset.name +")");
                }
            }
        }
    }

    [Button]
    public void TeleportPlayerStreetStart()
    {
        //Find player starting position
        List<StreetController> validStreets = CityData.Instance.streetDirectory.FindAll(item => item.entrances.Count > 0 && item.rooms.Count > 0 && item.nodes.Count >= 8);
        SessionData.Instance.startingNode = Player.Instance.FindSafeTeleport(validStreets[Toolbox.Instance.Rand(0, validStreets.Count)]);

        Player.Instance.Teleport(SessionData.Instance.startingNode, null, false);
    }

    [Button]
    public void ToggleCitizenColliders()
    {
        foreach(Citizen cit in CityData.Instance.citizenDirectory)
        {
            if (cit.ai != null && cit.ai.capCollider != null) cit.ai.capCollider.enabled = !cit.ai.capCollider.enabled;
        }
    }

    [Button]
    public void GiveRandomJolt()
    {
        Player.Instance.fps.JoltCamera(new Vector3(Toolbox.Instance.Rand(-1f, 1f), Toolbox.Instance.Rand(-1f, 1f), Toolbox.Instance.Rand(-1f, 1f)), 45f, 1f);
    }

    [Button]
    public void TripPlayer()
    {
        Player.Instance.Trip(0f, false);
    }

    //Test time range generation (can delete once accurate)
    [Button]
    public void TestTimeRange()
    {
        Vector2 tr = Toolbox.Instance.CreateTimeRange(SessionData.Instance.gameTime, GameplayControls.Instance.timeOfDeathAccuracy, false, true, 15);

        Game.Log(SessionData.Instance.DecimalToClockString(tr.x, false) + " - " + SessionData.Instance.DecimalToClockString(tr.y, false) + " (" + SessionData.Instance.DecimalToClockString(SessionData.Instance.gameTime, false) + ")");
    }

    [Button]
    public void DeletePlayerPrefs()
    {
        PopupMessageController.Instance.PopupMessage("DeletePrefs", true, true, RButton: "Confirm");
        PopupMessageController.Instance.OnRightButton += DeletePlayerPrefsConfirm;
        PopupMessageController.Instance.OnLeftButton += DeletePlayerPrefsCancel;
    }

    public void DeletePlayerPrefsConfirm()
    {
        PopupMessageController.Instance.OnRightButton -= DeletePlayerPrefsConfirm;
        PopupMessageController.Instance.OnLeftButton -= DeletePlayerPrefsCancel;

        PlayerPrefs.DeleteAll();

        AudioController.Instance.StopAllSounds();
        UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
    }

    public void DeletePlayerPrefsCancel()
    {
        PopupMessageController.Instance.OnRightButton -= DeletePlayerPrefsConfirm;
        PopupMessageController.Instance.OnLeftButton -= DeletePlayerPrefsCancel;
    }

    [Button]
    public void ForcePlayerDirtyDeath()
    {
        Player.Instance.currentHealth = 0;
        Player.Instance.TriggerPlayerKO(transform.forward, 0f, true);
    }

    [Button]
    public void TurnOffAllDynamicOcclusion()
    {
        var foundRenderObjects = FindObjectsOfType<MeshRenderer>(true);

        foreach(MeshRenderer mr in foundRenderObjects)
        {
            mr.allowOcclusionWhenDynamic = false;
        }
    }

    [Button]
    public void TurnOffAllLODS()
    {
        var foundRenderObjects = FindObjectsOfType<LODGroup>(true);

        foreach (LODGroup mr in foundRenderObjects)
        {
            Destroy(mr);
        }
    }
}
