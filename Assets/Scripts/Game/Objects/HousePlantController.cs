﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.UIElements;

public class HousePlantController : MonoBehaviour
{
    [Header("Configuration")]
    public Vector3 spawnLocalPosition = Vector3.zero;
    public Vector2 sizeScale = new Vector2(0.85f, 1.15f);

    [Header("Deterministic Values")]
    public int poolIndex = 0;
    public float scaleIndex = 0f;
    public float rotation = 0f;
    public float colourLerp = 0f;

    [Header("State")]
    public GameObject spawnedPlant;
    public bool isLoaded = false;

    void OnEnable()
    {
        //Get determined values
        if(!isLoaded && this != null && InteriorControls.Instance != null && InteriorControls.Instance.housePlantPool.Count > 0)
        {
            string seed = this.transform.position.ToString();
            poolIndex = Mathf.Clamp(Toolbox.Instance.GetPsuedoRandomNumber(0, InteriorControls.Instance.housePlantPool.Count, seed), 0, InteriorControls.Instance.housePlantPool.Count - 1);
            scaleIndex = Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, seed);
            rotation = Toolbox.Instance.GetPsuedoRandomNumber(0f, 360f, seed);
            colourLerp = scaleIndex;

            spawnedPlant = Instantiate(InteriorControls.Instance.housePlantPool[poolIndex], this.transform);
            float scaleMp = Mathf.Lerp(sizeScale.x, sizeScale.y, scaleIndex);
            spawnedPlant.transform.localScale = new Vector3(scaleMp, scaleMp, scaleMp);
            spawnedPlant.transform.localPosition = spawnLocalPosition;
            spawnedPlant.transform.localEulerAngles = new Vector3(0, rotation, 0);

            MeshRenderer rend = spawnedPlant.GetComponent<MeshRenderer>();
            rend.material.SetColor("_BaseColor", Color.Lerp(InteriorControls.Instance.housePlantColour1, InteriorControls.Instance.housePlantColour2, colourLerp));

            //Set light layer
            Vector3Int nodeCoord = CityData.Instance.RealPosToNodeInt(this.transform.position);
            NewNode foundNode = null;

            if(PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out foundNode))
            {
                Toolbox.Instance.SetLightLayer(spawnedPlant, foundNode.building);
            }

            isLoaded = true;
        }
    }
}
