﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "ambient_data", menuName = "Audio/Ambient Zone")]

public class AmbientZone : SoCustomComparison
{
	[Header("Audio")]
	public AudioEvent mainEvent;

    [Header("Occlusion")]
    [Tooltip("If true, this zone can be active and heard outside of the assigned room.")]
    public bool useOcclusion = true;
    [EnableIf("useOcclusion")]
    public float maxRange = 10f;
    [EnableIf("useOcclusion")]
    [Tooltip("If true this sound can penetrate closed doors")]
    public bool canPenetrateClosedDoors = true;
    [Space(7)]
    [EnableIf("useOcclusion")]
    [Tooltip("Overrides default occlusion value sound in the audio controller")]
    public bool overrideOcclusionModifier = false;
    [EnableIf("overrideOcclusionModifier")]
    [Tooltip("Each occlusion unit will decrease volume by this amount...")]
    public float occlusionUnitVolumeModifier = -0.1f;

    [Header("Special Cases")]
    public bool isAirDuctAmbience = false;
    //[EnableIf("playInAirDuct")]
    //public float otherRoomVolumePenalty = -0.5f;

    [Header("Params")]
    [Tooltip("Pass time of day")]
    public bool passTimeOfDay = false;
    [Tooltip("Pass walla amount")]
    public bool passWalla = false;
    [Tooltip("Pass player in vent")]
    public bool passPlayerInVent = true;
    [Tooltip("Pass player vent ext/int")]
    public bool passPlayerVentExtInt = false;
    [Tooltip("Pass the player's distance to the nearest vent")]
    public bool passDistanceToVent = false;
    [Tooltip("Pass rain")]
    public bool passRain = false;
    [Tooltip("Pass basement")]
    public bool passBasement = false;
    [Tooltip("Pass combination of height and wind speed")]
    public bool passHeightWindSpeed = false;
    [Tooltip("The range to sample crowds")]
    [EnableIf("passWalla")]
    public float maxWallaRange = 10f;
    [Tooltip("The number of people present per node for maximum walla")]
    [EnableIf("passWalla")]
    public float maxWallaCrowd = 10f;
}
