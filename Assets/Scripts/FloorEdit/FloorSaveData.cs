﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FloorSaveData
{
    //Class Data
    public string floorName = "newFloor";
    public Vector2 size = new Vector2(1, 1);
    public int defaultFloorHeight = 0;
    public int defaultCeilingHeight = 42;

    //Containers
    public List<AddressSaveData> a_d = new List<AddressSaveData>(); //Address data
    public List<TileSaveData> t_d = new List<TileSaveData>(); //Tile data
}

[System.Serializable]
public class AddressSaveData
{
    //Class Data
    public string p_n; //Preset name
    public Color e_c = Color.cyan; //Editor colour

    //Variations
    public List<AddressLayoutVariation> vs = new List<AddressLayoutVariation>(); //variations
}

[System.Serializable]
public class AddressLayoutVariation
{
    public List<RoomSaveData> r_d = new List<RoomSaveData>(); //Room data
}

[System.Serializable]
public class RoomSaveData
{
    public int id = 0;
    public List<NodeSaveData> n_d = new List<NodeSaveData>(); //Node data
    public string l; //Layout
    //public List<int> op_e = new List<int>(); //Open plan elements by index
    //public string p; //Preset index
}

[System.Serializable]
public class TileSaveData
{
    //Class Data
    public Vector2Int f_c; //Coordinate within floor
    public bool i_e = false; //True if this is an entrance
    public bool m_e = false; //True if this is a main entrance
    public bool s_t = false; //True if this is a stairwell
    public int s_r = 0; //Rotation of the stairwell
    public bool e_l = false; //True if this is an elevator
    public int e_r = 0; //Rotation of the elevator
}

[System.Serializable]
public class NodeSaveData
{
    //Class Data
    public Vector2Int f_c; //Floor Coord: Coordinate local to the floor (not representitive of actual world position)
    public int f_h = 0; //Floor Height: The default floor height, nodes will inherit this value
    //public int c_h = 42; //Ceiling Height: The default ceiling height, nodes will inherit this value
    public NewNode.FloorTileType f_t; //Floor Type: Parameters of the floor
    public string f_r; //Forced room: Forced room references as addressconfig.roomconfig

    //Containers
    public List<WallSaveData> w_d = new List<WallSaveData>(); //Wall data
}

[System.Serializable]
public class WallSaveData
{
    //Class Data
    public Vector2 w_o; //Wall offset: 0.5 +/- in either direction
    public string p_n; //Preset ID
}
