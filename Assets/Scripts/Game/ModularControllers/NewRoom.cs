﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using NaughtyAttributes;
using UnityEngine.UI;

public class NewRoom : Controller, IComparable<NewRoom>, IEquatable<NewRoom>
{
    public string name;

    [Header("Location")]
    public NewBuilding building; //Reference to the building
    public NewFloor floor; //Reference to the floor
    public NewGameLocation gameLocation; //Reference to the address
    public NewRoom lowerRoom; //If this is a room above a room with no ceiling, link it passively here

    [Header("Room Contents")]
    public int furnitureAssignID = 1;
    public int interactableAssignID = 1;
    public HashSet<NewNode> nodes = new HashSet<NewNode>();
    public List<RoomConfiguration> openPlanElements = new List<RoomConfiguration>();
    public List<NewNode.NodeAccess> entrances = new List<NewNode.NodeAccess>(); //Key == Other node, Value == access data. //Used to keep track of which other rooms this can connect to
    public List<RoomDivider> roomDividers = new List<RoomDivider>(); //Similar to above but exclusively for dividers: Used for generation only
    public List<LightZoneData> lightZones = new List<LightZoneData>(); //List of lists of the room divided into rectangles for ceiling lights.
    public Vector3 middleRoomPosition; //world position of the middle of the largest light zone
    public List<NewRoom> commonRooms = new List<NewRoom>(); //List of rooms that share the same light system (assigned when decor is generated).
    public HashSet<Actor> currentOccupants = new HashSet<Actor>(); //Updated list of occupants
    public GameObject streetObjectContainer; //Streets only - contains street objects
    public HashSet<Interactable> tamperedInteractables = new HashSet<Interactable>(); //List of interactables that are illegally turned on by the player (Eg. TVs)
    public List<NewNode> noAccessNodes = new List<NewNode>(); //List of nodes with no access at all. Used for reference when determining furniture validity.
    public HashSet<Interactable> worldObjects = new HashSet<Interactable>(); //List of physics objects that are temporarily parented to this room
    public List<LightController> lightCulling = new List<LightController>(); //List of lights with light culling enabled
    public List<Human.ConversationInstance> activeConversations = new List<Human.ConversationInstance>(); //Conversations active in this room
    public List<NewWall> windows = new List<NewWall>(); //List of windows leading to outside
    public List<AudioController.LoopingSoundInfo> audibleLoopingSounds = new List<AudioController.LoopingSoundInfo>(); //List of registered sounds that can be heard in this room. When AI enters, you need to update occlusion.
    //public HashSet<Interactable> thrownItems = new HashSet<Interactable>(); //True if this room contains thrown items worth picking up
    public Dictionary<FurniturePreset.FurnitureGroup, int> furnitureGroups = new Dictionary<FurniturePreset.FurnitureGroup, int>(); //Assigned furniture groups...                                                                                                                         //Heat sources
    public List<Interactable> heatSources = new List<Interactable>(); //Heat sources
    public List<PipeConstructor.PipeGroup> pipes = new List<PipeConstructor.PipeGroup>();

    [System.Serializable]
    public class RoomDivider
    {
        public NewRoom fromRoom;
        public NewRoom toRoom;
        public List<NewWall> dividerWalls; //If this is a divider, list of divider walls
    }

    [Header("Details")]
    public int roomFloorID = -1; //This must be saved as in some cases it can determin parent
    public static int assignRoomFloorID = 1;
    public int roomID = -1;
    public static int assignRoomID = 1;
    public string seed;

    public RoomTypePreset roomType;
    public RoomConfiguration preset;
    //public Vector2 roomBoundsSize = Vector2.zero; //Dimensions in vector2 format (bounds)
    //public Vector3 averageNodePos = Vector3.zero;
    //public Vector3 averageWorldPos = Vector3.zero;
    public Vector3 worldPos; //Centre of room world position
    public Vector2 boundsSize; //Bounds size of all nodes
    public bool geometryLoaded = false; //True if the walls, floor and ceiling objects exist in the game world
    public bool reachableFromEntrance = false; //Used in address generation to heck if this room can be accessed
    public bool isOutsideWindow = false; //If true this room is the outside area outside a building
    public bool isNullRoom = false; //Is this a null room?
    public bool isBaseNullRoom = false; //Is this the base null room?
    public bool featuresStairwell = false; //True if this room contains a stairwell
    public bool uniqueCeilingMaterial = false; //True if there is a unique ceiling material assigned (required for ceiling emission boost)
    public bool containsDead = false; //Flag for quickly checking if dead citizens are here
    public bool decorEdit = false; //True if the decor has been changed and is different to the citysave data
    public bool isVisible = false;
    public bool musicPlaying = false; //Is music playing in this room?
    public float musicStartedAt = 0;

    [Header("Decor")]
    public bool allowCoving = false; //If true this room will feature coving
    public MaterialGroupPreset floorMaterial;
    public Toolbox.MaterialKey floorMatKey;
    public Material floorMat;

    public MaterialGroupPreset ceilingMaterial;
    public Toolbox.MaterialKey ceilingMatKey;
    public Material ceilingMat;

    public MaterialGroupPreset defaultWallMaterial;
    public Toolbox.MaterialKey defaultWallKey;
    public Material wallMat;

    public bool hasBeenDecorated = false; //Flag to see if this room has been decorated

    public Toolbox.MaterialKey miscKey;

    public ColourSchemePreset colourScheme;

    [Header("Lights")]
    public RoomLightingPreset mainLightPreset; //Chosen main light preset
    public bool mainLightStatus = false; //State of the main lights
    public bool secondaryLightStatus = false; //True if one of the secondary lights is on
    public List<NewWall> lightswitches = new List<NewWall>(); //Reference to wall tiles with lightswitches
    public List<Interactable> lightswitchInteractables = new List<Interactable>();
    public List<Interactable> mainLights = new List<Interactable>();
    public List<Interactable> secondaryLights = new List<Interactable>();
    public bool enabledLights = true; //If false main lights are disabled
    public List<NewWall> windowsWithUVData = new List<NewWall>();
    public int ceilingFans = -1;

    private bool hasHash;
    private int hash;

    bool IEquatable<NewRoom>.Equals(NewRoom other)
    {
        return other.GetHashCode() == GetHashCode();
    }

    public override int GetHashCode()
    {
        if (!hasHash)
        {
            hash = base.GetHashCode();
            hasHash = true;
        }
        return hash;
    }

    [System.Serializable]
    public class LightZoneData
    {
        public NewRoom room;
        public List<NewNode> nodeList;
        public Vector3 centreWorldPosition;
        public Vector3 lightSpawnPosition; //Similar to centre position but avoids no ceilings and air ducts
        public Vector2 worldSize;
        public NewNode centreNode;
        public Light spawnedAreaLight;
        public HDAdditionalLightData aAdditional;
        public bool allowLight = true; //If no ceiling then block light
        public bool bestPosFound = false;
        public List<string> debug = new List<string>();

        public Color areaLightColour = Color.white;
        public float areaLightBrightness;

        public class LightNodeRank
        {
            public NewNode node;
            public float rank;
        }

        public LightZoneData(NewRoom newRoom, List<NewNode> newNodeList)
        {
            room = newRoom;
            nodeList = newNodeList;

            //Calculate centre, size, centre node etc...
            //Find the centre of the zone...
            centreWorldPosition = Vector3.zero;
            Vector2 minMaxX = new Vector2(999999, -999999);
            Vector2 minMaxY = new Vector2(999999, -999999);
            float divideAv = 0f;

            foreach (NewNode node in nodeList)
            {
                centreWorldPosition += node.position;
                minMaxX = new Vector2(Mathf.Min(minMaxX.x, node.position.x), Mathf.Max(minMaxX.y, node.position.x));
                minMaxY = new Vector2(Mathf.Min(minMaxY.x, node.position.z), Mathf.Max(minMaxY.y, node.position.z));
                divideAv++;
            }

            //We now have the true middle..
            if (divideAv > 0) centreWorldPosition /= divideAv;

            //Find the size
            worldSize = new Vector2(minMaxX.y - minMaxX.x, minMaxY.y - minMaxY.x);

            //Find centre node...
            float cDist = 99999f;

            foreach (NewNode n in nodeList)
            {
                float dist = Vector3.Distance(n.position, centreWorldPosition);

                if (dist < cDist)
                {
                    cDist = dist;
                    centreNode = n;
                }
            }

            //Create or load params
            if (room.preset.useAdditionalAreaLights & (CityConstructor.Instance == null || CityConstructor.Instance.generateNew))
            {
                //Set the colour
                areaLightColour = room.preset.areaLightColor;

                //Set the brightness
                areaLightBrightness = room.preset.areaLightBrightness;
                debug.Add("Set to room preset area light brightness: " + areaLightBrightness);

                if (room.preset.useDistrictSettingsAsBase && room.gameLocation.district != null && room.gameLocation.district.preset.alterStreetAreaLighting)
                {
                    string seed = CityData.Instance.seed + room.roomID + nodeList.Count;
                    Color newColour = room.gameLocation.district.preset.possibleColours[Toolbox.Instance.RandContained(0, room.gameLocation.district.preset.possibleColours.Count, seed, out seed)];

                    if (room.gameLocation.district.preset.lightOperation == DistrictPreset.AffectStreetAreaLights.lerp)
                    {
                        areaLightColour = Color.Lerp(areaLightColour, newColour, room.gameLocation.district.preset.lightAmount);
                    }
                    else if (room.gameLocation.district.preset.lightOperation == DistrictPreset.AffectStreetAreaLights.multiply)
                    {
                        areaLightColour = areaLightColour * (newColour * room.gameLocation.district.preset.lightAmount);
                    }
                    else if (room.gameLocation.district.preset.lightOperation == DistrictPreset.AffectStreetAreaLights.add)
                    {
                        areaLightColour = areaLightColour + (newColour * room.gameLocation.district.preset.lightAmount);
                    }

                    areaLightBrightness += room.gameLocation.district.preset.brightnessModifier;
                    debug.Add("Add district modifier to brightness: " + room.gameLocation.district.preset.brightnessModifier);
                }
            }
        }

        private void FindBestLightPosition()
        {
            if (room.mainLightPreset == null && !room.preset.useAdditionalAreaLights)
            {
                //Game.Log("Cannot allow lights as there is no main light preset");
                allowLight = false; //Cannot allow light if no presets...
                return;
            }

            //debug.Add("Finding best light position...");

            List<LightNodeRank> possibleLightNodes = new List<LightNodeRank>();

            int noCeiling = 0; //Count areas of no ceiling
            allowLight = true;

            foreach (NewNode node in nodeList)
            {
                if (room.gameLocation.thisAsStreet != null && room.preset.useAdditionalAreaLights)
                {
                    //For streets, anything is passable as we just want a floating area light...
                }
                else if ((node.floorType != NewNode.FloorTileType.floorAndCeiling && node.floorType != NewNode.FloorTileType.CeilingOnly))
                {
                    noCeiling++;
                    continue;
                }
                //Don't allow a light to spawn on ducts...
                else if (room.gameLocation.floor.defaultCeilingHeight >= 44 && node.airDucts.Exists(item => item.level >= 1))
                {
                    noCeiling++;
                    continue;
                }
                //Don't allow a light to spawn on ducts...
                else if (room.gameLocation.floor.defaultCeilingHeight < 44 && node.airDucts.Exists(item => item.level == 1))
                {
                    noCeiling++;
                    continue;
                }
                //Don't allow if any furniture ceiling items here...
                else if (node.individualFurniture.Exists(item => item.furnitureClasses[0].ceilingPiece && item.furnitureClasses[0].blocksCeiling))
                {
                    noCeiling++;
                    continue;
                }

                //debug.Add("Adding " + node.nodeCoord + " with rank " + Vector3.Distance(node.position, centreWorldPosition));
                possibleLightNodes.Add(new LightNodeRank { node = node, rank = Vector3.Distance(node.position, centreWorldPosition) });
            }

            //Only allow light if at least 2 ceiling nodes
            if (noCeiling + 1 >= nodeList.Count)
            {
                //Game.Log("Disallowing lights on zone " + room.name + " as not enough free nodes (" + (noCeiling + 1) + " >= " + nodeList.Count + ")");
                allowLight = false;
            }

            if (allowLight && possibleLightNodes.Count > 0)
            {
                //Sort list by LOWEST rank, ie closer to middle...
                possibleLightNodes.Sort((p1, p2) => p1.rank.CompareTo(p2.rank));

                //Grab all nodes with the same highest rank
                List<NewNode> highestRanked = new List<NewNode>();

                highestRanked.Add(possibleLightNodes[0].node);
                float highestRank = possibleLightNodes[0].rank;
                possibleLightNodes.RemoveAt(0);

                //debug.Add("Highest rank is " + possibleLightNodes[0].node.nodeCoord + " with rank of " + possibleLightNodes[0].rank);

                for (int i = 0; i < possibleLightNodes.Count; i++)
                {
                    LightNodeRank current = possibleLightNodes[i];

                    //If within range then add to list
                    if (current.rank <= highestRank + 0.01f)
                    {
                        bool posCheck = false;
                        //debug.Add("Found other with rank of " + current.rank);

                        //Do a position check...
                        foreach (NewNode n in highestRanked)
                        {
                            float dist = Vector3.Distance(current.node.nodeCoord, n.nodeCoord);

                            //Must be within one node...
                            if (dist <= 1.5f)
                            {
                                posCheck = true;
                                break;
                            }
                        }

                        if (posCheck)
                        {
                            highestRanked.Add(current.node);
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                //We now have the highest ranked nodes that are adjacent to each other, use their average for the light position...
                lightSpawnPosition = Vector3.zero;

                foreach (NewNode n in highestRanked)
                {
                    lightSpawnPosition += n.position;
                }

                lightSpawnPosition /= (float)highestRanked.Count;
            }
            else
            {
                allowLight = false;
            }

            bestPosFound = true;
        }

        //Do this on CityGen for main lights as they are interactables that will be saved. Area lights are created on spawn of geometry.
        public void CreateMainLight()
        {
            //Pick the best position for a light...
            if (!bestPosFound) FindBestLightPosition();
            if (!allowLight) return;

            //You now have everything you need to spawn a light!
            //Convert position to local space
            //Vector3 worldPos = room.transform.InverseTransformPoint(lightSpawnPosition);

            int ceilingHeight = 42;
            if (centreNode.floor != null) ceilingHeight = centreNode.floor.defaultCeilingHeight;
            if (centreNode.room.roomType.overrideFloorHeight) ceilingHeight = centreNode.room.roomType.floorHeight;

            //Set Y position to ceiling height
            Vector3 worldPos = new Vector3(lightSpawnPosition.x, lightSpawnPosition.y + ((ceilingHeight - centreNode.floorHeight - 1) * 0.1f), lightSpawnPosition.z);
            //localPos.y = centreNode.ceilingHeight * 0.1f;

            if (room.mainLightPreset == null)
            {
                Game.Log("No light presets for " + room.name);
                return;
            }

            //Face the direction of the longest side of the zone
            Vector3 eul = Vector3.zero;
            if (worldSize.x > worldSize.y) eul = new Vector3(0, 90, 0);

            //Spawn light interactable
            Interactable newLight =
                InteractableCreator.Instance.CreateMainLightInteractable(
                room.mainLightPreset.lightObjects[Toolbox.Instance.GetPsuedoRandomNumber(0, room.mainLightPreset.lightObjects.Count, centreWorldPosition + room.roomID.ToString())],
                room,
                worldPos,
                eul,
                room.mainLightPreset.lightingPreset,
                null,
                nodeList.Count
                );

            //Set the light node (we will need this to load the correct parent)
            //newLight.lightNodeCoord = centreNode.nodeCoord;
        }

        //Returns true on success
        public bool CreateAreaLight()
        {
            //if (spawnedAreaLight != null) return; //Light already exists...

            //Pick the best position for a light...
            if (!bestPosFound) FindBestLightPosition();
            if (!allowLight)
            {
                return false;
            }

            if (nodeList.Count < room.preset.minimumLightZoneSizeForAreaLights) return false;

            GameObject newAreaLight = Instantiate(InteriorControls.Instance.roomAreaLight, room.transform);

            int ceilingHeight = 42;
            if (centreNode.floor != null) ceilingHeight = centreNode.floor.defaultCeilingHeight;
            if(centreNode.room.roomType.overrideFloorHeight) ceilingHeight = centreNode.room.roomType.floorHeight;

            newAreaLight.transform.localPosition = room.transform.InverseTransformPoint(lightSpawnPosition + new Vector3(0, (ceilingHeight - centreNode.floorHeight - 1) * 0.1f, 0)) + room.preset.areaLightOffset;

            spawnedAreaLight = newAreaLight.GetComponentInChildren<Light>();
            aAdditional = spawnedAreaLight.transform.GetComponent<HDAdditionalLightData>();

            //Set to correct light layer
            if (room == null || room.building == null)
            {
                //Set to street layer (1)
                aAdditional.lightlayersMask = LightLayerEnum.LightLayer1;
            }
            else if (room.building != null)
            {
                //Set to building's light layer
                if (room.building.interiorLightCullingLayer == 0)
                {
                    aAdditional.lightlayersMask = LightLayerEnum.LightLayer2;
                }
                else if (room.building.interiorLightCullingLayer == 1)
                {
                    aAdditional.lightlayersMask = LightLayerEnum.LightLayer3;
                }
                else if (room.building.interiorLightCullingLayer == 2)
                {
                    aAdditional.lightlayersMask = LightLayerEnum.LightLayer4;
                }
                else if (room.building.interiorLightCullingLayer == 3)
                {
                    aAdditional.lightlayersMask = LightLayerEnum.LightLayer5;
                }

                //Add to building's interior light list
                //room.building.allInteriorMainLights.Add(this);
            }

            //Set the size
            Vector2 lightSize = worldSize * room.preset.areaLightCoverageMultiplier;

            aAdditional.SetAreaLightSize(new Vector2(Mathf.Max(lightSize.x, 0.1f), Mathf.Max(lightSize.y, 0.1f)));

            //Set the colour
            spawnedAreaLight.color = areaLightColour;

            //Set the brightness
            aAdditional.intensity = areaLightBrightness;

            //Set the range
            aAdditional.range = room.preset.areaLightRange;

            //Set the dimmer
            aAdditional.shadowDimmer = room.preset.areaLightShadowDimmer;

            //Helps with light bleed
            aAdditional.penumbraTint = true;

            //Set shadow tints
            if(room.preset.areaLightingShadowTint)
            {
                if(room.preset.overrideAreaLightShadowTint)
                {
                    aAdditional.shadowTint = room.preset.areaLightShadowTintOverride;
                }
                else
                {
                    aAdditional.shadowTint = room.GetShadowTint(areaLightColour, room.preset.areaLightingShadowTintIntensity);
                }
            }

            //Set on or off with main lights
            spawnedAreaLight.gameObject.SetActive(room.mainLightStatus);

            return true;
        }

        public void RemoveAreaLight()
        {
            if(spawnedAreaLight != null) Destroy(spawnedAreaLight.gameObject);
            spawnedAreaLight = null;
        }
    }

    //Used as a key for seeing if objects are available for static batching
    public struct StaticBatchKey
    {
        public Mesh mesh;
        public Material mat;

        public bool Equals(StaticBatchKey other)
        {
            return Equals(other, this);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var objectToCompareWith = (StaticBatchKey)obj;

            return objectToCompareWith.mesh == mesh && objectToCompareWith.mat == mat;
        }

        public override int GetHashCode()
        {
            HashCode hc = new HashCode();
            hc.Add(mesh);
            hc.Add(mat);
            return hc.ToHashCode();
        }

        public static bool operator ==(StaticBatchKey c1, StaticBatchKey c2)
        {
            return c1.Equals(c2);
        }

        public static bool operator !=(StaticBatchKey c1, StaticBatchKey c2)
        {
            return !c1.Equals(c2);
        }
    }

    public Color GetShadowTint(Color lightColour, float intensity)
    {
        Color ret = Color.black;

        if (defaultWallKey.colour3.a >= 0.1f) ret = Color.Lerp(ret, defaultWallKey.colour3, intensity);
        if (defaultWallKey.colour2.a >= 0.1f) ret = Color.Lerp(ret, defaultWallKey.colour2, intensity);
        if (defaultWallKey.colour1.a >= 0.1f) ret = Color.Lerp(ret, defaultWallKey.colour1, intensity);
        ret = Color.Lerp(ret, lightColour, intensity);

        return ret;
    }

    //[Header("Procedural Debug Data")]
    //public float randomRanking = 0f;
    //public float exteriorWallRanking = 0f;
    //public float exteriorWindowRanking = 0f;
    //public float floorSpaceRanking = 0f;
    //public float uniformShapeRanking = 0f;

    public List<GenerationController.OverrideData> overrideData = new List<GenerationController.OverrideData>();

    //public float totalRanking = 0f;
    //[Space(7)]
    //public float overridenApplication = 0f;
    //public float overridenTotal = 0f;

    //True if any actors are within this room
    private bool actorUpdate = false;

    [Header("Occlusion")]
    public Dictionary<NewRoom, List<CullTreeEntry>> cullingTree = new Dictionary<NewRoom, List<CullTreeEntry>>();
    public HashSet<int> doorCheckSet = new HashSet<int>(); //Doors to listen to if the player is in this room

    public HashSet<NewRoom> nonAudioOccludedRooms = new HashSet<NewRoom>(); //List of rooms with permentanly open entrances
    public HashSet<NewDoor> openDoors = new HashSet<NewDoor>(); //List of rooms with open doors
    public HashSet<NewDoor> closedDoors = new HashSet<NewDoor>(); //List of rooms with closed doors
    public HashSet<NewRoom> adjacentRooms = new HashSet<NewRoom>();
    public HashSet<NewRoom> aboveRooms = new HashSet<NewRoom>(); //List of rooms above this
    public HashSet<NewRoom> belowRooms = new HashSet<NewRoom>(); //List of rooms below this
    public NewRoom atriumTop; //If this is part of an atrium, this is the top room...
    public List<NewRoom> atriumRooms = new List<NewRoom>(); //Rooms above/below this that are visible through floors

    public GameObject combinedWalls; //Game object holding the entire combined walls mesh
    public MeshRenderer combinedWallRend; //The renderer for the above

    public Dictionary<NewBuilding, GameObject> additionalWalls = new Dictionary<NewBuilding, GameObject>();

    public GameObject combinedFloor; //Game object holding the entire combined walls mesh
    public MeshRenderer combinedFloorRend; //The renderer for the above

    public GameObject combinedCeiling; //Game object holding the entire combined walls mesh
    public MeshRenderer combinedCeilingRend; //The renderer for the above

    //Ambient sound modifier: -1 for each of these to occlusion values
    public int ambientSoundLevel = 0;

    //Serialized saved culling data
    private List<CitySaveData.CullTreeSave> ct;
    private List<int> above;
    private List<int> below;
    private List<int> adj;
    private List<int> occ;

    [Header("Furniture")]
    public List<FurnitureClusterLocation> furniture = null;
    public List<FurnitureLocation> individualFurniture = null;

    //For static batching
    Dictionary<StaticBatchKey, List<GameObject>> staticBatchDictionary = new Dictionary<StaticBatchKey, List<GameObject>>();

    //Decor generation cache
    [NonSerialized]
    public Dictionary<FurnitureClass, List<FurniturePreset>> pickFurnitureCache = null;
    [NonSerialized]
    public Dictionary<Vector3, NewNode> localizedRoomNodeMaps = null;

    [Header("Footprints")]
    public bool footprintUpdateQueued = false;
    public List<FootprintController> spawnedFootprints = new List<FootprintController>();

    [Header("AI Navigation")]
    public Dictionary<NewNode, List<NewNode>> blockedAccess = new Dictionary<NewNode, List<NewNode>>(); //Blocked access due to furniture
    public Dictionary<AIActionPreset, List<Interactable>> actionReference = new Dictionary<AIActionPreset, List<Interactable>>(); //A reference to objects in this room and their actions.
    public Dictionary<InteractablePreset.SpecialCase, List<Interactable>> specialCaseInteractables = new Dictionary<InteractablePreset.SpecialCase, List<Interactable>>(); //For listing various special interactables

    [Header("Ownership")]
    private List<int> loadBelongsTo = new List<int>();
    public List<Human> belongsTo = new List<Human>(); //If this room belongs to certain people

    [Header("Exploration")]
    [Tooltip("Is this room shown on the map?")]
    public int explorationLevel = 0;
    public List<RectTransform> mapDoors = new List<RectTransform>();

    [Header("Air Vents")]
    public List<AirDuctGroup.AirVent> airVents = new List<AirDuctGroup.AirVent>(); //Placeholder for now- how many air ducts in this room. TODO: Chance to a list later.
    public List<AirDuctGroup> ductGroups = new List<AirDuctGroup>(); //Duct groups adjoining onto this room

    [Header("Passwords")]
    public GameplayController.Passcode passcode;

    [Header("Crime Scene Elements")]
    public List<SpatterSimulation> spatter = new List<SpatterSimulation>(); //Spatter simulations to be run on load geometry

    //Pathfinding key- use value operator for comparisons
    public struct PathKey : IEquatable<PathKey>
    {
        public NewNode origin;
        public NewNode destination;
        private bool hasHash;
        private int hash;

        public PathKey(NewNode locOne, NewNode locTwo)
        {
            origin = locOne;
            destination = locTwo;
            hasHash = false;
            hash = 0;
        }

        public override int GetHashCode()
        {
            if (!hasHash)
            {
                hash = HashCode.Combine(origin, destination);
                hasHash = true;
            }
            return hash;
        }

        bool IEquatable<PathKey>.Equals(PathKey other)
        {
            return other.GetHashCode() == GetHashCode();
        }
    }

    [Header("Environment")]
    public GameObject environmentalContainer;
    public Volume environmentVolume;
    public List<Interactable> steamControllingInteractables = new List<Interactable>(); //Interactables that affect steam
    public bool steamOn = false; //Is a steam source on in this room
    public float steamLastSwitched = 0f; //Used to store when the steam was last turned on or off (can determin steam level from this without an update loop)
    public SteamController steamController; //For controlling steam atmosphere within a room
    public List<BugController> spawnedBugs = new List<BugController>();
    public float gasLevel = 0f; //If this room is filled with toxic gas
    public float lastRoomGassed = 0f;

    [Header("Debug")]
    public GenerationDebugController debugController;
    //public List<string> cullingTreeDebug = new List<string>();
    //public List<string> secondaryEntrancesDebug = new List<string>();
    //public List<string> debugInteractables = new List<string>();
    //public List<string> debugLightswitch = new List<string>();

    //Actions - used as callback for end of frame
    public Action UpdateEmission;
    public bool completedTreeCull = false;
    public List<string> debugLightswitches = new List<string>();
    public int cullingDebugLoadReference = 0;
    private List<CullingDebugController> spawnPathDebug = new List<CullingDebugController>();
    public string debugCulling;
    public bool loadedCullTreeFromSave = false;
    public List<InteractableController> mainLightObjects = new List<InteractableController>();
    public List<string> debugDecor = new List<string>();
    private List<GameObject> exteriorWindowDebug = new List<GameObject>();
    private List<GameObject> nodeDebug = new List<GameObject>();
    public List<string> debugAddActions = new List<string>();

    //TODO: Remove these (debug only)
    public string clustersPlaced;
    public string itemsPlaced;
    public int poolSizeOnPlacement;
    public string palcementKey1;
    public string palcementKey2;
    public string palcementKey3;
    public string palcementKey4;
    public string palcementKey5;
    public string palcementKey51;
    public string palcementKey52;
    public string palcementKey6;
    public string keyAtStart;

    [Space(7)]
    private GameObject sublocationParent;
    private List<GameObject> sublocationDebugObjects = new List<GameObject>();

    [System.Serializable]
    public class CullTreeEntry
    {
        public List<int> requiredOpenDoors = null;

        public CullTreeEntry(List<int> newRequiredDoors/*, List<string> newDebugFloorDiff*/)
        {
            //room = newRoom;
            //if(room != null) name = room.name;

            //nodePath = newNodePath;
            requiredOpenDoors = newRequiredDoors;
            //debug = newDebugFloorDiff;

            //if (nodePath != null)
            //{
            //    for (int i = 0; i < nodePath.Count; i++)
            //    {
            //        NewNode foundNode = null;

            //        if (PathFinder.Instance.nodeMap.TryGetValue(nodePath[i], out foundNode))
            //        {
            //            debugNodeFloors.Add(foundNode.floorType);
            //        }
            //        else debugNodeFloors.Add(NewNode.FloorTileType.none);
            //    }
            //}

            ////Add debug reference
            //if (DebugControls.Instance.enableCullingDebug && parentRoom != null)
            //{
            //    debugID = assignDebugID;
            //    assignDebugID++;

            //    parentRoom.debugCulling.Add(this);
            //}
        }
    }

    public void SetupLayoutOnly(NewGameLocation newAddress, RoomTypePreset newRoomType, int loadFloorRoomID = -1)
    {
        if (loadFloorRoomID <= -1)
        {
            roomFloorID = assignRoomFloorID;
            assignRoomFloorID++;
        }
        else roomFloorID = loadFloorRoomID;

        roomID = assignRoomID;
        assignRoomID++;

        if (Game.Instance.devMode && Game.Instance.collectDebugData) keyAtStart = newAddress.seed;

        //Create a predictable random seed
        if(SessionData.Instance.isFloorEdit)
        {
            seed = Toolbox.Instance.GenerateUniqueID();
        }
        else seed = roomID + newAddress.district.districtID + CityData.Instance.seed + newRoomType.name + (roomID * roomID);

        if (newAddress.building != null)
        {
            if (newAddress.floor != null)
            {
                seed += newAddress.floor.floor;
                seed += newAddress.floor.addresses.Count;
            }

            seed += newAddress.building.buildingID;
        }

        //startSeed = seed;

        SetType(newRoomType);

        //Setup actions
        UpdateEmission += UpdateEmissionTex;

        newAddress.AddNewRoom(this);
        this.transform.SetParent(gameLocation.gameObject.transform);
        this.transform.localPosition = Vector3.zero;

        passcode = new GameplayController.Passcode(GameplayController.PasscodeType.room);
        passcode.id = roomID;

        //Is this in the floor editor? If so display this now
        if (SessionData.Instance.isFloorEdit)
        {
            SetVisible(true, true, "Created at start");
        }

        if (!CityData.Instance.roomDictionary.ContainsKey(roomID))
        {
            CityData.Instance.roomDictionary.Add(roomID, this);
        }

        if (!CityData.Instance.roomDirectory.Contains(this))
        {
            CityData.Instance.roomDirectory.Add(this);
        }
    }

    public void SetupAll(NewGameLocation newAddress, RoomConfiguration newPreset, int loadFloorRoomID = -1)
    {
        if (Game.Instance.devMode && Game.Instance.collectDebugData) keyAtStart = newAddress.seed;
        SetupLayoutOnly(newAddress, newPreset.roomType, loadFloorRoomID);
        SetConfiguration(newPreset);
    }

    public void SetConfiguration(RoomConfiguration newPreset)
    {
        preset = newPreset;
        SetType(preset.roomType);

        if (SessionData.Instance.isFloorEdit) seed = Toolbox.Instance.GenerateUniqueID();

        //Set null room
        if (preset == CityControls.Instance.nullDefaultRoom)
        {
            isNullRoom = true;
        }

        //Add a bedroom reference to the residence controller
        if (gameLocation != null)
        {
            if (gameLocation.thisAsAddress != null)
            {
                if (gameLocation.thisAsAddress.residence != null)
                {
                    if (preset.roomType == InteriorControls.Instance.bedroomType)
                    {
                        gameLocation.thisAsAddress.residence.AddBedroom(this);
                    }
                }
            }
        }

        //Force inside/outside
        if (preset.forceOutside != RoomConfiguration.OutsideSetting.dontChange)
        {
            foreach (NewNode n in nodes)
            {
                if (preset.forceOutside == RoomConfiguration.OutsideSetting.forceInside)
                {
                    n.SetAsOutside(false);
                }
                else if (preset.forceOutside == RoomConfiguration.OutsideSetting.forceOutside)
                {
                    n.SetAsOutside(true);
                }
            }
        }

        //Copy variables from preset
        allowCoving = preset.allowCoving;

        if (SessionData.Instance.isFloorEdit) SetRoomName();

        if (gameLocation.thisAsAddress != null)
        {
            if (building != null)
            {
                if (preset == CityControls.Instance.nullDefaultRoom)
                {
                    isOutsideWindow = true;
                }
            }
        }

        //Set default materials
        SetFloorMaterial(CityControls.Instance.defaultFloorMaterialGroup, CityControls.Instance.defaultFloorMaterialGroup.variations[0]);
        SetCeilingMaterial(CityControls.Instance.defaultCeilingMaterialGroup, CityControls.Instance.defaultFloorMaterialGroup.variations[0]);
        SetWallMaterialDefault(CityControls.Instance.defaultWallMaterialGroup, CityControls.Instance.defaultFloorMaterialGroup.variations[0]);
        if ((Game.Instance.devMode && Game.Instance.collectDebugData)) debugDecor.Add("Setting Default Wall Material: " + CityControls.Instance.defaultWallMaterialGroup.material.name);

        //Is this in the floor editor? If so display this now
        if (SessionData.Instance.isFloorEdit)
        {
            SetVisible(true, true, "Created at start");
        }

        //Set lights on by default
        SetMainLights(preset.lightsOnAtStart, "OnStart " + preset.lightsOnAtStart, forceUpdate: true);

        //Setup environment
        SetupEnvrionment();
    }

    public void SetType(RoomTypePreset newRoomType)
    {
        if (SessionData.Instance.isFloorEdit && FloorEditController.Instance.roomSelection == this) Game.Log("Set room type: " + newRoomType.ToString());
        roomType = newRoomType;
    }

    public void Load(CitySaveData.RoomCitySave data, NewGameLocation newGameLoc)
    {
        name = data.name;
        this.transform.name = name;

        //Setup actions
        UpdateEmission += UpdateEmissionTex;

        foreach (string str in data.openPlanElements)
        {
            RoomConfiguration include = null;
            Toolbox.Instance.LoadDataFromResources<RoomConfiguration>(str, out include); //Design style chosen for the address
            if (include != null) openPlanElements.Add(include);
        }

        roomFloorID = data.floorID; //This must be saved as in some cases it can determin parent
        roomID = data.id;
        assignRoomID = Mathf.Max(assignRoomID, roomID + 1); //Make sure other rooms won't overwrite this ID
        furnitureAssignID = data.fID;
        interactableAssignID = data.iID;

        isBaseNullRoom = data.isBaseNullRoom;
        passcode = data.password;
        ceilingFans = data.cf;

        newGameLoc.AddNewRoom(this);

        if (isBaseNullRoom)
        {
            newGameLoc.nullRoom = this;
        }

        Toolbox.Instance.LoadDataFromResources<RoomConfiguration>(data.preset, out preset); //Design style chosen for the address
        SetConfiguration(preset); //Moved to below after load of decor data

        reachableFromEntrance = data.reachableFromEntrance; //Used in address generation to heck if this room can be accessed
        isOutsideWindow = data.isOutsideWindow; //If true this room is the outside area outside a building
        allowCoving = data.allowCoving; //If true this room will feature coving

        Toolbox.Instance.LoadDataFromResources<MaterialGroupPreset>(data.floorMaterial, out floorMaterial);
        floorMatKey = data.floorMatKey;

        Toolbox.Instance.LoadDataFromResources<MaterialGroupPreset>(data.ceilingMaterial, out ceilingMaterial);
        ceilingMatKey = data.ceilingMatKey;

        Toolbox.Instance.LoadDataFromResources<MaterialGroupPreset>(data.defaultWallMaterial, out defaultWallMaterial);
        defaultWallKey = data.defaultWallKey;

        miscKey = data.miscKey;

        if (data.colourScheme != null && data.colourScheme.Length > 0)
        {
            Toolbox.Instance.LoadDataFromResources<ColourSchemePreset>(data.colourScheme, out colourScheme);
        }

        if (data.mainLightPreset != null && data.mainLightPreset.Length > 0)
        {
            Toolbox.Instance.LoadDataFromResources<RoomLightingPreset>(data.mainLightPreset, out mainLightPreset);
        }

        hasBeenDecorated = true;

        if ((Game.Instance.devMode && Game.Instance.collectDebugData)) debugDecor.Add("Setting loaded materials: " + defaultWallMaterial + ", " + floorMat + ", " + ceilingMaterial);

        middleRoomPosition = data.middle;

        loadBelongsTo.AddRange(data.owners);

        this.transform.SetParent(gameLocation.gameObject.transform);
        this.transform.localPosition = Vector3.zero;
        SetMainLights(preset.lightsOnAtStart, "OnLoad " + preset.lightsOnAtStart, forceUpdate: true);

        //A null room has been loaded in an taken this room's saved ID, reset the ID of the created room. It should be loaded in following this.
        if (CityData.Instance.roomDictionary.ContainsKey(roomID))
        {
            if (CityData.Instance.roomDictionary[roomID] != this)
            {
                NewRoom takenByRoom = CityData.Instance.roomDictionary[roomID];

                //Game.Log("Room " + roomID + " ID taken by setup null room. Reassigning created room so loaded room can have this ID...");

                CityData.Instance.roomDirectory.Remove(takenByRoom);
                CityData.Instance.roomDictionary.Remove(roomID);
                takenByRoom.roomID = -1;

                CityData.Instance.roomDictionary.Add(roomID, this);
                CityData.Instance.roomDirectory.Add(this);
            }
        }
        else
        {
            CityData.Instance.roomDictionary.Add(roomID, this);
            CityData.Instance.roomDirectory.Add(this);
        }

        //Add nodes: They should already physically exist because of tile creation, but make sure to load them!
        foreach (CitySaveData.NodeCitySave node in data.nodes)
        {
            //Find the existing node data
            NewNode existingNode = null;

            if (PathFinder.Instance.nodeMap.TryGetValue(node.nc, out existingNode))
            {
                existingNode.Load(node, this);
            }
        }

        //Load air vents
        foreach (CitySaveData.AirVentSave vent in data.airVents)
        {
            AirDuctGroup.AirVent newVent = new AirDuctGroup.AirVent(vent);
            newVent.ventID = vent.id;
            newVent.room = this;
            if (vent.node != null) PathFinder.Instance.nodeMap.TryGetValue(vent.node, out newVent.node);
            if (vent.rNode != null) PathFinder.Instance.nodeMap.TryGetValue(vent.rNode, out newVent.roomNode);

            if (vent.wall > -1)
            {
                if (CityConstructor.Instance.loadingWallsReference.ContainsKey(vent.wall))
                {
                    newVent.wall = CityConstructor.Instance.loadingWallsReference[vent.wall];

                    airVents.Add(newVent);
                    LoadVent(newVent);
                }
                else Game.LogError("Unable to load vent " + vent.id + " as unable to find wall ID " + vent.wall);
            }
            else
            {
                //This must be a ceiling vent
                airVents.Add(newVent);
                LoadVent(newVent);
                //Game.LogError("Unable to load vent " + vent.id + " because it features a wall ID of " + vent.wall);
            }
        }

        //Light zones
        foreach (CitySaveData.LightZoneSave zone in data.lightZones)
        {
            List<NewNode> newZone = new List<NewNode>();

            //Find the existing node data
            foreach (Vector3Int v3 in zone.n)
            {
                NewNode existingNode = null;

                if (PathFinder.Instance.nodeMap.TryGetValue(v3, out existingNode))
                {
                    newZone.Add(existingNode);
                }
            }

            LightZoneData z = new LightZoneData(this, newZone);
            z.areaLightColour = zone.areaLightColour;
            z.areaLightBrightness = zone.areaLightBright;

            lightZones.Add(z);
        }

        //Load furniture
        //Game.Log("room " + name + " at " + gameLocation.name + " has " + data.f.Count + " furniture clusters");

        foreach (CitySaveData.FurnitureClusterCitySave cluster in data.f)
        {
            //Parse data...
            FurnitureCluster furnPreset = null;
            Toolbox.Instance.LoadDataFromResources<FurnitureCluster>(cluster.cluster, out furnPreset);

            if(PathFinder.Instance.nodeMap.ContainsKey(cluster.anchorNode))
            {
                FurnitureClusterLocation newFurn = new FurnitureClusterLocation(PathFinder.Instance.nodeMap[cluster.anchorNode], furnPreset, cluster.angle, cluster.ranking);

                //Populate cluster
                //Game.Log("room " + name + " at " + gameLocation.name + " has " + furn.objs.Count + " in furniture cluster");

                foreach (CitySaveData.FurnitureClusterObjectCitySave furn in cluster.objs)
                {
                    List<FurnitureClass> furnitureClasses = new List<FurnitureClass>();

                    foreach (string str in furn.furnitureClasses)
                    {
                        FurnitureClass furnClass = null;
                        Toolbox.Instance.LoadDataFromResources<FurnitureClass>(str, out furnClass);
                        if (furnClass != null) furnitureClasses.Add(furnClass);
                    }

                    List<NewNode> coversNodes = new List<NewNode>();

                    foreach (Vector3Int v3 in furn.coversNodes)
                    {
                        coversNodes.Add(PathFinder.Instance.nodeMap[v3]);
                    }

                    FurnitureLocation loadFurn = new FurnitureLocation(furn.id, newFurn, furnitureClasses, furn.angle, PathFinder.Instance.nodeMap[furn.anchorNode], coversNodes, furn.useFOVBLock, furn.fovDirection, furn.fovMaxDistance, furn.scale, newUserPlaced: furn.up, newOffset: furn.offset); //Load and force ID

                    Toolbox.Instance.LoadDataFromResources<FurniturePreset>(furn.furniture, out loadFurn.furniture);

                    if (furn.art != null && furn.art.Length > 0)
                    {
                        Toolbox.Instance.LoadDataFromResources<ArtPreset>(furn.art, out loadFurn.art);
                        loadFurn.pickedArt = true;
                    }

                    loadFurn.matKey = furn.matKey;
                    loadFurn.artMatKey = furn.artMatKet;
                    loadFurn.pickedMaterials = true;

                    //Add to main cluster map
                    foreach (NewNode coversNode in loadFurn.coversNodes)
                    {
                        if (!newFurn.clusterObjectMap.ContainsKey(coversNode))
                        {
                            newFurn.clusterObjectMap.Add(coversNode, new List<FurnitureLocation>());
                        }

                        newFurn.clusterObjectMap[coversNode].Add(loadFurn);
                        if (!newFurn.clusterList.Contains(loadFurn)) newFurn.clusterList.Add(loadFurn);
                    }

                    //Load owners
                    loadFurn.loadOwners.AddRange(furn.owners);
                    loadFurn.pickedOwners = true;

                    //Add to loading reference
                    if (!CityConstructor.Instance.loadingFurnitureReference.ContainsKey(loadFurn.id))
                    {
                        //Game.Log("Adding furniture load ref: " + loadFurn.id);
                        CityConstructor.Instance.loadingFurnitureReference.Add(loadFurn.id, loadFurn);
                    }
                }

                //Add furniture
                AddFurniture(newFurn, false);
                //furniture.Add(newFurn);
            }
            else
            {
                Game.LogError("Unable to find node for furniture cluster save! " + cluster.anchorNode + " Attempting to scan node map... " + PathFinder.Instance.nodeMap.Count);

                if (Game.Instance.devMode && Game.Instance.collectDebugData)
                {
                    foreach(KeyValuePair<Vector3, NewNode> pair in PathFinder.Instance.nodeMap)
                    {
                        if(Mathf.RoundToInt(pair.Key.x) == Mathf.RoundToInt(cluster.anchorNode.x))
                        {
                            if (Mathf.RoundToInt(pair.Key.y) == Mathf.RoundToInt(cluster.anchorNode.y))
                            {
                                if (Mathf.RoundToInt(pair.Key.z) == Mathf.RoundToInt(cluster.anchorNode.z))
                                {
                                    Game.LogError("Found the proper node!! Something isn't right here..." + Mathf.RoundToInt(pair.Key.x) + " " + Mathf.RoundToInt(pair.Key.y) + " " + Mathf.RoundToInt(pair.Key.z));
                                }
                            }
                        }
                    }
                }
            }
        }

        ct = data.cullTree;
        above = data.above;
        below = data.below;
        adj = data.adj;
        occ = data.occ;

        //Failsafe for getting middle room position...
        if (middleRoomPosition == Vector3.zero)
        {
            UpdateWorldPositionAndBoundsSize();
            middleRoomPosition = worldPos;

            //if (nodes.Count > 0)
            //{
            //    NewNode[] asArray = nodes.ToArray();
            //    NewNode randomLine = asArray[Toolbox.Instance.GetPsuedoRandomNumber(0, asArray.Length, seed)];
            //    middleRoomPosition = randomLine.position;
            //}
            //else
            //{
            //    UpdateWorldPositionAndBoundsSize();
            //    middleRoomPosition = worldPos;
            //}
        }

        //Setup environment
        SetupEnvrionment();
    }

    //This must be done after all rooms are loaded
    public void LoadCullingTree()
    {
        if (ct == null)
        {
            Game.LogError("Room " + roomID + " has not been loaded...");
            return;
        }

        foreach (CitySaveData.CullTreeSave cull in ct)
        {
            NewRoom r = null;
            if (cull.r < 0) continue; //Skip -1 references

            if(!CityData.Instance.roomDictionary.TryGetValue(cull.r, out r))
            {
                Game.LogError("Unable to find room id " + cull.r + " in room dictionary...");
                continue;
            }

            CullTreeEntry newCull = new CullTreeEntry(cull.d);

            if (!cullingTree.ContainsKey(r))
            {
                cullingTree.Add(r, new List<CullTreeEntry>());
            }

            cullingTree[r].Add(newCull);

            //Add to door listening set...
            if (cull.d != null)
            {
                foreach (int d in cull.d)
                {
                    if (!doorCheckSet.Contains(d))
                    {
                        doorCheckSet.Add(d);
                    }
                }
            }

            //Add to debug
            //newCull.name = r.name;
            //ewCull.loadedID = cull.r;
            //debugCulling.Add(newCull);
        }

        //Game.Log("Loaded " + cullingTree.Count + " entries for " + name);

        foreach (int r in above)
        {
            aboveRooms.Add(CityData.Instance.roomDictionary[r]);
        }

        foreach (int r in below)
        {
            belowRooms.Add(CityData.Instance.roomDictionary[r]);
        }

        foreach (int r in adj)
        {
            adjacentRooms.Add(CityData.Instance.roomDictionary[r]);
        }

        foreach (int r in occ)
        {
            nonAudioOccludedRooms.Add(CityData.Instance.roomDictionary[r]);
        }

        loadedCullTreeFromSave = true;

        ct = null;
        above = null;
        below = null;
        adj = null;
        occ = null;
    }

    //Generate a colour scheme and update all materials
    public void UpdateColourSchemeAndMaterials()
    {
        //Ignore styling if this is outside
        if (gameLocation.isOutside) return;

        string randomKey = roomID.ToString();

        List<ColourSchemePreset> possibleColourSchemes = new List<ColourSchemePreset>();

        bool styleFound = false;

        if (!SessionData.Instance.isFloorEdit)
        {
            if (preset.decorSetting == RoomConfiguration.DecorSetting.borrowFromBuilding)
            {
                colourScheme = building.colourScheme;

                defaultWallKey = building.defaultWallKey;
                SetWallMaterialDefault(building.defaultWallMaterial, null, false);

                floorMatKey = building.floorMatKey;
                SetFloorMaterial(building.floorMaterial, null, false);

                ceilingMatKey = building.ceilingMatKey;
                SetCeilingMaterial(building.ceilingMaterial, null, false);

                styleFound = true;
                hasBeenDecorated = true;
                if ((Game.Instance.devMode && Game.Instance.collectDebugData)) debugDecor.Add("New Decor: Borrowed from building (" + building.defaultWallMaterial.name + ")");
            }
            else if (preset.decorSetting == RoomConfiguration.DecorSetting.borrowFromBelow)
            {
                foreach (NewNode n in nodes)
                {
                    //Search below...
                    Vector3Int searchCoord = n.nodeCoord + new Vector3Int(0, 0, -1);
                    NewNode foundNode = null;

                    if (PathFinder.Instance.nodeMap.TryGetValue(searchCoord, out foundNode))
                    {
                        if (foundNode.room.hasBeenDecorated && !foundNode.room.preset.excludeFromOthersCopyingDecorStyle)
                        {
                            colourScheme = foundNode.room.colourScheme;

                            defaultWallKey = foundNode.room.defaultWallKey;
                            SetWallMaterialDefault(foundNode.room.defaultWallMaterial, null, false);

                            floorMatKey = foundNode.room.floorMatKey;
                            SetFloorMaterial(foundNode.room.floorMaterial, null, false);

                            ceilingMatKey = foundNode.room.ceilingMatKey;
                            SetCeilingMaterial(foundNode.room.ceilingMaterial, null, false);

                            styleFound = true;
                            hasBeenDecorated = true;
                            if ((Game.Instance.devMode && Game.Instance.collectDebugData)) debugDecor.Add("New Decor: Borrowed from below (" + foundNode.room.defaultWallMaterial.name + ")");
                            break;
                        }
                    }
                }
            }
        }

        //Inherit style from adjoining
        if (preset.decorSetting != RoomConfiguration.DecorSetting.ownStyle && !styleFound)
        {
            //Game.Log("Borrow from adjoining...");

            if (entrances.Count > 0)
            {
                List<NewRoom> openSet = new List<NewRoom>();
                List<NewRoom> closedSet = new List<NewRoom>();
                openSet.Add(this);
                int safety = 99;

                while (openSet.Count > 0 && safety > 0)
                {
                    //Sort open set by most important rooms
                    openSet.Sort();
                    openSet.Reverse();

                    NewRoom current = openSet[0];

                    //Is this decorated?
                    if (current.hasBeenDecorated && !current.preset.excludeFromOthersCopyingDecorStyle)
                    {
                        //Game.Log("Decorated adjoining room found");

                        colourScheme = current.colourScheme;

                        defaultWallKey = current.defaultWallKey;
                        SetWallMaterialDefault(current.defaultWallMaterial, null, false);

                        floorMatKey = current.floorMatKey;
                        SetFloorMaterial(current.floorMaterial, null, false);

                        ceilingMatKey = current.ceilingMatKey;
                        SetCeilingMaterial(current.ceilingMaterial, null, false);

                        styleFound = true;
                        hasBeenDecorated = true;
                        return;
                    }
                    else if (current.entrances.Count > 0)
                    {
                        foreach (NewNode.NodeAccess access in current.entrances)
                        {
                            //Same game location
                            if (access.toNode.gameLocation == gameLocation)
                            {
                                if (!openSet.Contains(access.toNode.room) && !closedSet.Contains(access.toNode.room))
                                {
                                    openSet.Add(access.toNode.room);
                                }
                            }
                        }
                    }

                    closedSet.Add(current);
                    openSet.RemoveAt(0);
                    safety--;
                }
            }
        }

        //Chance of override
        if (preset.chanceOfOverrideMatIfGroundFloor > 0f || preset.chanceOfOverrideMatIfStairwell > 0f || preset.chanceOfOverrideMatIfBasement > 0f)
        {
            bool overrideDecor = false;
            bool overrideBasement = false;

            if (preset.chanceOfOverrideMatIfGroundFloor > 0f && floor.floor == 0)
            {
                if (!building.triedGroundFloorRandom && preset.chanceOfOverrideMatIfGroundFloor >= Toolbox.Instance.RandContained(0f, 1f, randomKey, out randomKey))
                {
                    building.groundFloorOverride = true;
                }

                building.triedGroundFloorRandom = true;
                overrideDecor = building.groundFloorOverride;
            }
            else if (preset.chanceOfOverrideMatIfBasement > 0f && floor.floor < 0)
            {
                if (!building.triedBasementRandom && preset.chanceOfOverrideMatIfBasement >= Toolbox.Instance.RandContained(0f, 1f, randomKey, out randomKey))
                {
                    building.basementFloorOverride = true;
                }

                building.triedBasementRandom = true;
                overrideBasement = building.basementFloorOverride;
            }

            if (preset.chanceOfOverrideMatIfStairwell > 0f)
            {
                foreach (NewNode node in nodes)
                {
                    if (node.tile.isStairwell)
                    {
                        if (preset.chanceOfOverrideMatIfStairwell >= Toolbox.Instance.RandContained(0f, 1f, randomKey, out randomKey))
                        {
                            overrideDecor = true;
                        }

                        break;
                    }
                }
            }

            if (overrideDecor)
            {
                if (preset.floorOverrides.Count > 0 || building.floorMaterialOverride != null)
                {
                    //Do we need to pick a new building floor override material?
                    if (building.floorMaterialOverride == null)
                    {
                        building.floorMaterialOverride = preset.floorOverrides[Toolbox.Instance.RandContained(0, preset.floorOverrides.Count, randomKey, out randomKey)];
                    }

                    SetFloorMaterial(building.floorMaterialOverride, null, false);
                    if ((Game.Instance.devMode && Game.Instance.collectDebugData)) debugDecor.Add("New Decor: Building override from preset (" + building.floorMaterialOverride.name + ")");
                }

                if (preset.wallOverrides.Count > 0 || building.defaultWallMaterialOverride != null)
                {
                    if (building.defaultWallMaterialOverride == null)
                    {
                        building.defaultWallMaterialOverride = preset.wallOverrides[Toolbox.Instance.RandContained(0, preset.wallOverrides.Count, randomKey, out randomKey)];
                    }

                    SetWallMaterialDefault(building.defaultWallMaterialOverride, null, false);
                    if ((Game.Instance.devMode && Game.Instance.collectDebugData)) debugDecor.Add("New Decor: Building override from preset (" + building.defaultWallMaterialOverride.name + ")");
                }

                if (preset.ceilingOverrides.Count > 0 || building.ceilingMaterialOverride != null)
                {
                    if (building.ceilingMaterialOverride == null)
                    {
                        building.ceilingMaterialOverride = preset.ceilingOverrides[Toolbox.Instance.RandContained(0, preset.ceilingOverrides.Count, randomKey, out randomKey)];
                    }

                    SetCeilingMaterial(building.ceilingMaterialOverride, null, false);
                    if ((Game.Instance.devMode && Game.Instance.collectDebugData)) debugDecor.Add("New Decor: Building override from preset (" + building.ceilingMaterialOverride.name + ")");
                }
            }
            else if (overrideBasement)
            {
                if (preset.floorOverrides.Count > 0 || building.basementFloorMaterialOverride != null)
                {
                    //Do we need to pick a new building floor override material?
                    if (building.basementFloorMaterialOverride == null)
                    {
                        building.basementFloorMaterialOverride = preset.floorOverrides[Toolbox.Instance.RandContained(0, preset.floorOverrides.Count, randomKey, out randomKey)];
                    }

                    SetFloorMaterial(building.basementFloorMaterialOverride, null, false);
                    if ((Game.Instance.devMode && Game.Instance.collectDebugData)) debugDecor.Add("New Decor: Building override from preset (" + building.basementFloorMaterialOverride.name + ")");
                }

                if (preset.wallOverrides.Count > 0 || building.basementDefaultWallMaterialOverride != null)
                {
                    if (building.basementDefaultWallMaterialOverride == null)
                    {
                        building.basementDefaultWallMaterialOverride = preset.wallOverrides[Toolbox.Instance.RandContained(0, preset.wallOverrides.Count, randomKey, out randomKey)];
                    }

                    SetWallMaterialDefault(building.basementDefaultWallMaterialOverride, null, false);
                    if ((Game.Instance.devMode && Game.Instance.collectDebugData)) debugDecor.Add("New Decor: Building override from preset (" + building.basementDefaultWallMaterialOverride.name + ")");
                }

                if (preset.ceilingOverrides.Count > 0 || building.basementCeilingMaterialOverride != null)
                {
                    if (building.basementCeilingMaterialOverride == null)
                    {
                        building.basementCeilingMaterialOverride = preset.ceilingOverrides[Toolbox.Instance.RandContained(0, preset.ceilingOverrides.Count, randomKey, out randomKey)];
                    }

                    SetCeilingMaterial(building.basementCeilingMaterialOverride, null, false);
                    if ((Game.Instance.devMode && Game.Instance.collectDebugData)) debugDecor.Add("New Decor: Building override from preset (" + building.basementCeilingMaterialOverride.name + ")");
                }
            }
        }

        //Own style
        if (!styleFound)
        {
            if ((Game.Instance.devMode && Game.Instance.collectDebugData)) debugDecor.Add("New Decor: Generate own decor style...");

            if(preset.forceColourSchemes.Count > 0)
            {
                possibleColourSchemes.AddRange(preset.forceColourSchemes);
            }
            else
            {
                foreach (ColourSchemePreset colourScheme in Toolbox.Instance.allColourSchemes)
                {
                    if (gameLocation.designStyle == null) Game.Log("this as address");

                    //Weight by design style with a hint of owner's creativity (10%)
                    int modernity = 10 - Mathf.RoundToInt(Mathf.Abs(colourScheme.modernity - (gameLocation.designStyle.modernity * 0.9f + gameLocation.thisAsAddress.averageCreativity)));

                    //Weight by room type...
                    int cleanness = 10 - Mathf.RoundToInt(Mathf.Abs(colourScheme.cleanness - preset.cleanness));

                    //Weight by personality... 75% Extraversion,  25% -Agreeableness
                    int loudness = 10 - Mathf.RoundToInt(Mathf.Abs(colourScheme.loudness - ((gameLocation.thisAsAddress.averageExtraversion * 7.5f) + ((1f - gameLocation.thisAsAddress.averageAgreeableness) * 2.5f))));

                    //Weight by personality... 70% Emotionality, 30% Creativity
                    int emotive = 10 - Mathf.RoundToInt(Mathf.Abs(colourScheme.emotive - ((gameLocation.thisAsAddress.averageEmotionality * 7f) + (gameLocation.thisAsAddress.averageCreativity * 3f))));

                    //Add and divide by 8 to give a score out of 5
                    int av = Mathf.FloorToInt((float)(modernity + cleanness + loudness + emotive) / 8f);

                    for (int i = 0; i < av; i++)
                    {
                        possibleColourSchemes.Add(colourScheme);
                    }
                }
            }

            if (possibleColourSchemes.Count <= 0)
            {
                possibleColourSchemes.Add(CityControls.Instance.fallbackColourScheme);
            }

            //You should now have a list proportionally weighted towards personality...
            colourScheme = possibleColourSchemes[Toolbox.Instance.RandContained(0, possibleColourSchemes.Count, randomKey, out randomKey)];

            float wealthLevel = Toolbox.Instance.GetNormalizedLandValue(gameLocation);

            //Pick variation for this material
            MaterialGroupPreset chosen = Toolbox.Instance.SelectMaterial(preset.roomClass, wealthLevel, gameLocation.designStyle, MaterialGroupPreset.MaterialType.walls, randomKey, out randomKey);
            MaterialGroupPreset.MaterialVariation chosenVar = chosen.variations[Toolbox.Instance.RandContained(0, chosen.variations.Count, randomKey, out randomKey)];
            SetWallMaterialDefault(chosen, chosenVar);

            MaterialGroupPreset chosen2 = Toolbox.Instance.SelectMaterial(preset.roomClass, wealthLevel, gameLocation.designStyle, MaterialGroupPreset.MaterialType.ceiling, randomKey, out randomKey);
            MaterialGroupPreset.MaterialVariation chosenVar2 = chosen2.variations[Toolbox.Instance.RandContained(0, chosen2.variations.Count, randomKey, out randomKey)];
            SetCeilingMaterial(chosen2, chosenVar2);

            MaterialGroupPreset chosen3 = Toolbox.Instance.SelectMaterial(preset.roomClass, wealthLevel, gameLocation.designStyle, MaterialGroupPreset.MaterialType.floor, randomKey, out randomKey);
            MaterialGroupPreset.MaterialVariation chosenVar3 = chosen3.variations[Toolbox.Instance.RandContained(0, chosen3.variations.Count, randomKey, out randomKey)];
            SetFloorMaterial(chosen3, chosenVar3);

            //Decorated flag
            hasBeenDecorated = true;
        }

        //Generate a key for lighting
        miscKey = MaterialsController.Instance.GenerateMaterialKey(InteriorControls.Instance.defaultVariation, colourScheme, this, false);
    }

    //Tiles may be split among rooms
    public void AddNewNode(NewNode newNode)
    {
        if (!nodes.Contains(newNode))
        {
            //Remove existing
            if (newNode.room != null) newNode.room.RemoveNode(newNode);
            if (newNode.gameLocation != null) newNode.gameLocation.RemoveNode(newNode);

            nodes.Add(newNode);

            //Override floor/ceiling height with room settings
            if (roomType.overrideFloorHeight)
            {
                newNode.SetFloorHeight(roomType.floorHeight);
            }
            else if(floor != null) newNode.SetFloorHeight(floor.defaultFloorHeight);

            //Set location info
            newNode.room = this;
            newNode.gameLocation = gameLocation;
            newNode.floor = floor;
            newNode.building = building;
            //newNode.name = newNode.room.name + roomID+ " Node " + newNode.nodeCoord.ToString();
            //newNode.name = newNode.name;

            //Add to gamelocation
            gameLocation.nodes.Add(newNode);

            //Tree update
            foreach (NewWall wall in newNode.walls)
            {
                //Set entrances
                //Add/remove entrances
                if (wall.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
                {
                    if (wall.parentWall.node.room != null)
                    {
                        wall.parentWall.node.room.AddEntrance(wall.parentWall.node, wall.childWall.node);
                    }

                    if (wall.childWall.node.room != null)
                    {
                        wall.childWall.node.room.AddEntrance(wall.childWall.node, wall.parentWall.node);
                    }
                }
                else
                {
                    if (wall.parentWall.node.room != null)
                    {
                        wall.parentWall.node.room.RemoveEntrance(wall.parentWall.node, wall.childWall.node);
                    }

                    if (wall.childWall.node.room != null)
                    {
                        wall.childWall.node.room.RemoveEntrance(wall.childWall.node, wall.parentWall.node);
                    }
                }
            }

            //Don't do this if being loaded...
            if (SessionData.Instance.isFloorEdit || SessionData.Instance.isTestScene || CityConstructor.Instance.generateNew)
            {
                GenerationController.Instance.UpdateGeometryFloor(floor, "NewRoom");
            }

            //Phsyically parent
            //newNode.transform.SetParent(this.transform, true);
            //newNode.parentDebug.Add("Parent to room " + this.preset.room.ToString() + " " + gameLocation.name.ToString());
            if (SessionData.Instance.isFloorEdit) SetRoomName();

            //Is this part of a staircase or elevator? If so then parent the objects to this
            if (newNode.tile.isStairwell && newNode.tile.stairwell != null)
            {
                newNode.tile.stairwell.transform.SetParent(this.transform, true);
            }
            else if (newNode.tile.isInvertedStairwell && newNode.tile.elevator != null)
            {
                newNode.tile.elevator.transform.SetParent(this.transform, true);
            }

            //Check for lower rooms
            if (newNode.floorType == NewNode.FloorTileType.CeilingOnly || newNode.floorType == NewNode.FloorTileType.noneButIndoors)
            {
                int safety = 99;
                int counter = 1;

                while (counter < safety)
                {
                    Vector3Int checkBelow = newNode.nodeCoord + new Vector3Int(0, 0, -counter);
                    NewNode belowNode = null;

                    if (PathFinder.Instance.nodeMap.TryGetValue(checkBelow, out belowNode))
                    {
                        if (belowNode.floorType == NewNode.FloorTileType.floorOnly)
                        {
                            //Found the below
                            SetLowerRoom(belowNode.room);
                            break;
                        }
                        else if (belowNode.floorType == NewNode.FloorTileType.noneButIndoors)
                        {
                            //Disable coving
                            belowNode.room.allowCoving = false;

                            //Keep searching
                            counter++;
                            continue;
                        }
                        else break;
                    }
                    else break;
                }
            }

            //Force inside/outside
            if(preset != null)
            {
                if (preset.forceOutside == RoomConfiguration.OutsideSetting.forceInside)
                {
                    newNode.SetAsOutside(false);
                }
                else if (preset.forceOutside == RoomConfiguration.OutsideSetting.forceOutside)
                {
                    newNode.SetAsOutside(true);
                }
            }

        }
        else
        {
            //newNode.parentDebug.Add("Tried to parent to room that this is already a part of");
            //Game.LogError("Tried to add to room that this is already in");
        }
    }

    public string GetName()
    {
        string ret = string.Empty;

        if (gameLocation.thisAsAddress != null)
        {
            if (gameLocation.isLobby)
            {
                ret = gameLocation.name;
            }
            else if (preset != null)
            {
                ret = gameLocation.name + " " + Strings.Get("names.rooms", preset.name);
            }
        }
        else
        {
            ret = gameLocation.name;
        }

        return ret;
    }

    public void SetRoomName()
    {
        //Set name
        if (gameLocation.thisAsAddress != null)
        {
            if (gameLocation.isLobby)
            {
                name = gameLocation.name;
            }
            else if(preset != null)
            {
                name = gameLocation.name + " " + Strings.Get("names.rooms", preset.name);
            }
        }
        else
        {
            name = gameLocation.name;
        }

        gameObject.name = name;
    }

    //Remove a node
    public void RemoveNode(NewNode newNode)
    {
        if (nodes.Contains(newNode))
        {
            //Clear connections
            newNode.accessToOtherNodes.Clear();

            foreach (NewWall wall in newNode.walls)
            {
                //Set entrances
                //Add/remove entrances
                if (wall.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
                {
                    try
                    {
                        if (wall.parentWall.node.room != null && wall.childWall.node.room != null)
                        {
                            wall.parentWall.node.room.RemoveEntrance(wall.parentWall.node, wall.childWall.node);
                        }

                        if (wall.childWall.node.room != null && wall.parentWall.node.room != null)
                        {
                            wall.childWall.node.room.RemoveEntrance(wall.childWall.node, wall.parentWall.node);
                        }
                    }
                    catch
                    {

                    }
                }
            }

            nodes.Remove(newNode);

            //Revert Override floor/ceiling height with room settings
            if (roomType.overrideFloorHeight)
            {
                newNode.SetFloorHeight(gameLocation.floor.defaultFloorHeight);
            }

            //Also remove from gamelocation
            gameLocation.RemoveNode(newNode);

            //Set location info
            newNode.room = null;
            newNode.gameLocation = null;
            newNode.floor = null;
            newNode.building = null;

            //Only do this if not being loaded/in editor
            if (SessionData.Instance.isFloorEdit || SessionData.Instance.isTestScene || CityConstructor.Instance.generateNew)
            {
                GenerationController.Instance.UpdateGeometryFloor(floor, "NewRoom");
            }

            if(SessionData.Instance.isFloorEdit) SetRoomName();
        }
    }

    //Update central average world position and the bounds size
    public void UpdateWorldPositionAndBoundsSize()
    {
        int lowestX = 9999;
        int lowestY = 9999;
        int highestX = -1;
        int highestY = -1;

        worldPos = Vector3.zero;

        foreach (NewNode node in nodes)
        {
            lowestX = (int)Mathf.Min(lowestX, node.nodeCoord.x);
            lowestY = (int)Mathf.Min(lowestY, node.nodeCoord.y);
            highestX = (int)Mathf.Max(highestX, node.nodeCoord.x);
            highestY = (int)Mathf.Max(highestY, node.nodeCoord.y);

            worldPos += node.position;
        }

        //Get average world position
        if (nodes.Count > 0) worldPos /= nodes.Count;

        //Dimensions = the difference
        boundsSize = new Vector2(highestX - lowestX + 1, highestY - lowestY + 1);
    }

    //Add an open-plan element
    public void AddOpenPlanElement(RoomConfiguration newElement)
    {
        if (!openPlanElements.Contains(newElement))
        {
            openPlanElements.Add(newElement);
        }
    }

    //Set materials
    public void SetFloorMaterial(MaterialGroupPreset newMat, MaterialGroupPreset.MaterialVariation newVar, bool getNewKey = true)
    {
        floorMaterial = newMat;
        if (getNewKey) floorMatKey = MaterialsController.Instance.GenerateMaterialKey(newVar, colourScheme, this, true);

        //Set existing floor
        if (combinedFloor == null)
        {
            foreach (NewNode nod in nodes)
            {
                nod.room.floorMat = MaterialsController.Instance.SetMaterialGroup(nod.spawnedFloor, newMat, floorMatKey);
            }
        }
        else
        {
            floorMat = MaterialsController.Instance.SetMaterialGroup(combinedFloor, newMat, floorMatKey);
        }
    }

    public void SetCeilingMaterial(MaterialGroupPreset newMat, MaterialGroupPreset.MaterialVariation newVar, bool getNewKey = true)
    {
        ceilingMaterial = newMat;
        if (getNewKey) ceilingMatKey = MaterialsController.Instance.GenerateMaterialKey(newVar, colourScheme, this, true);

        //If there's no combined mesh here apply to all nodes invidually
        if (combinedCeiling == null)
        {
            if (ceilingMat != null && preset.boostCeilingEmission) Destroy(ceilingMat);

            foreach (NewNode nod in nodes)
            {
                if (ceilingMat == null)
                {
                    ceilingMat = MaterialsController.Instance.SetMaterialGroup(nod.spawnedCeiling, newMat, ceilingMatKey, preset.boostCeilingEmission);

                    uniqueCeilingMaterial = preset.boostCeilingEmission;

                    //Boost ceiling emission
                    if (preset.boostCeilingEmission && ceilingMat != null && uniqueCeilingMaterial)
                    {
                        if (mainLightStatus)
                        {
                            Color areaLightColour = Color.white;
                            if (lightZones.Count > 0) areaLightColour = lightZones[0].areaLightColour;
                            ceilingMat.SetColor("_EmissiveColor", preset.ceilingEmissionBoost * areaLightColour);
                        }
                        else
                        {
                            ceilingMat.SetColor("_EmissiveColor", Color.black);
                        }
                    }
                }
                else
                {
                    MaterialsController.Instance.ApplyMaterial(nod.spawnedCeiling, ceilingMat);
                }
            }
        }
        else
        {
            if (ceilingMat != null && preset.boostCeilingEmission) Destroy(ceilingMat);

            ceilingMat = MaterialsController.Instance.SetMaterialGroup(combinedCeiling, newMat, ceilingMatKey, preset.boostCeilingEmission);
            uniqueCeilingMaterial = preset.boostCeilingEmission;

            //Boost ceiling emission
            if (preset.boostCeilingEmission && ceilingMat != null && uniqueCeilingMaterial)
            {
                if (mainLightStatus)
                {
                    Color areaLightColour = Color.white;
                    if (lightZones.Count > 0) areaLightColour = lightZones[0].areaLightColour;
                    ceilingMat.SetColor("_EmissiveColor", preset.ceilingEmissionBoost * areaLightColour);
                }
                else
                {
                    ceilingMat.SetColor("_EmissiveColor", Color.black);
                }
            }
        }
    }

    public void SetWallMaterialDefault(MaterialGroupPreset newMat, MaterialGroupPreset.MaterialVariation newVar, bool getNewKey = true)
    {
        defaultWallMaterial = newMat;
        if (getNewKey) defaultWallKey = MaterialsController.Instance.GenerateMaterialKey(newVar, colourScheme, this, true);

        //Set existing walls
        if (combinedWalls == null)
        {
            foreach (NewNode nod in nodes)
            {
                foreach (NewWall wall in nod.walls)
                {
                    wall.SetWallMaterial(newMat, defaultWallKey);
                }
            }
        }
        else
        {
            //No floor replacement
            if (nodes.Where(item => item.floorType == NewNode.FloorTileType.CeilingOnly || item.floorType == NewNode.FloorTileType.noneButIndoors || item.tile.isStairwell || item.tile.isInvertedStairwell).FirstOrDefault() != null)
            {
                if (newMat != null && newMat.noFloorReplacement != null)
                {
                    newMat = newMat.noFloorReplacement;
                }
            }

            wallMat = MaterialsController.Instance.SetMaterialGroup(combinedWalls, newMat, defaultWallKey);
        }
    }

    public void ToggleMainLights(Actor actor = null)
    {
        if (mainLightStatus)
        {
            SetMainLights(false, "Toggle false", actor);
        }
        else
        {
            SetMainLights(true, "Toggle true", actor);
        }
    }

    public void SetMainLights(bool newVal, string debug, Actor actor = null, bool forceInstant = false, bool forceUpdate = false)
    {
        //debugLightswitch.Add(debug);
        bool isDifferent = false;

        if (newVal != mainLightStatus || forceUpdate)
        {
            isDifferent = true;
        }

        mainLightStatus = newVal;
        if ((Game.Instance.devMode && Game.Instance.collectDebugData)) debugLightswitches.Add("TURN " + newVal + " : " + debug);
        //debugLightswitch.Add(name + "(" + roomID + ") " + debug);

        //Update lights
        foreach (Interactable light in mainLights)
        {
            light.SetSwitchState(mainLightStatus, actor, forceInstantLights: forceInstant);
        }

        //Update lightswitches
        foreach (NewWall wall in lightswitches)
        {
            wall.lightswitchInteractable.SetSwitchState(mainLightStatus, actor, forceInstantLights: forceInstant);
        }

        //Update area lights
        if (preset.useAdditionalAreaLights)
        {
            foreach (LightZoneData lz in lightZones)
            {
                if (lz.spawnedAreaLight != null)
                {
                    lz.spawnedAreaLight.gameObject.SetActive(mainLightStatus);
                }
            }
        }

        //Boost ceiling emission
        if (preset.boostCeilingEmission && ceilingMat != null && uniqueCeilingMaterial)
        {
            if (mainLightStatus)
            {
                Color areaLightColour = Color.white;
                if (lightZones.Count > 0) areaLightColour = lightZones[0].areaLightColour;
                ceilingMat.SetColor("_EmissiveColor", preset.ceilingEmissionBoost * areaLightColour);
            }
            else
            {
                ceilingMat.SetColor("_EmissiveColor", Color.black);
            }
        }

        //Only trigger this if it's a new value
        if (isDifferent)
        {
            UpdateEmissionEndOfFrame();
        }

        foreach (NewRoom room in commonRooms)
        {
            if (room == this) continue;
            room.mainLightStatus = newVal;
            //room.debugLightswitch.Add("TURN " + newVal);
            //room.debugLightswitch.Add(name + "(" + roomID + ") " + debug);

            //Update lights
            foreach (Interactable light in room.mainLights)
            {
                light.SetSwitchState(room.mainLightStatus, actor, forceInstantLights: forceInstant);
            }

            //Update lightswitches
            foreach (NewWall wall in room.lightswitches)
            {
                wall.lightswitchInteractable.SetSwitchState(room.mainLightStatus, actor, forceInstantLights: forceInstant);
            }

            //Only trigger this if it's a new value
            if (isDifferent)
            {
                room.UpdateEmissionEndOfFrame();
            }
        }

        //Suspicious action: Switching lights on or off with people in the room will trigger suspicion...
        if (actor != null && lightswitches.Count > 0)
        {
            if (actor.isPlayer)
            {
                if (Player.Instance.illegalStatus)
                {
                    //Game.Log("Set main lights by player");

                    List<NewRoom> roomsWithSuspicion = new List<NewRoom>(commonRooms);
                    roomsWithSuspicion.Add(this);

                    foreach (NewNode.NodeAccess acc in entrances)
                    {
                        if (!acc.walkingAccess) continue;

                        if (acc.accessType == NewNode.NodeAccess.AccessType.openDoorway || acc.accessType == NewNode.NodeAccess.AccessType.bannister || (acc.door != null && !acc.door.isClosed))
                        {
                            if (!roomsWithSuspicion.Contains(acc.toNode.room))
                            {
                                roomsWithSuspicion.Add(acc.toNode.room);
                            }
                        }
                    }

                    foreach (NewRoom sus in roomsWithSuspicion)
                    {
                        foreach (Actor person in sus.currentOccupants)
                        {
                            if (person.isDead) continue;

                            if (person.ai != null)
                            {
                                //Update investigation position if player isn't already seen
                                if (!person.ai.seesOnPersuit)
                                {
                                    NewWall lightswitchWall = lightswitches[Toolbox.Instance.Rand(0, lightswitches.Count, definitelyNotPartOfCityGeneration: true)];

                                    if (lightswitchWall != null && lightswitchWall.node != null)
                                    {
                                        Game.Log("...Trigger investigation with lightswitch");
                                        if (Game.Instance.devMode) Debug.DrawRay(person.lookAtThisTransform.position, lightswitchWall.node.position - person.lookAtThisTransform.position, Color.blue, 1.5f);

                                        try
                                        {
                                            person.ai.Investigate(lightswitchWall.node, lightswitchWall.node.position, null, NewAIController.ReactionState.investigatingSight, CitizenControls.Instance.sightingMinInvestigationTimeMP, Player.Instance.trespassingEscalation);
                                        }
                                        catch
                                        {
                                            //It's possible somehow for the above to modify the current occupants... Possibly come back to look at this
                                            Game.LogError("Unable to trigger AI investigation into light switch usage!");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void SetSecondaryLight(bool newVal, bool forceUpdate = false)
    {
        bool isDifferent = false;

        if (newVal != secondaryLightStatus || forceUpdate)
        {
            isDifferent = true;
        }

        secondaryLightStatus = newVal;

        //Only trigger this if it's a new value
        if (isDifferent)
        {
            UpdateEmissionEndOfFrame();
        }

        foreach (NewRoom room in commonRooms)
        {
            if (room == this) continue;
            room.secondaryLightStatus = newVal;

            if (isDifferent)
            {
                room.UpdateEmissionEndOfFrame();
            }
        }
    }

    //Write to building's emission texture at end of frame
    public void UpdateEmissionEndOfFrame()
    {
        //At the end of frame...
        Toolbox.Instance.InvokeEndOfFrame(UpdateEmission, "");
    }

    public void UpdateEmissionTex()
    {
        if (building != null && building.preset != null && building.preset.emissionMapLit != null && building.emissionTextureInstanced != null)
        {
            bool textureChanged = false;

            if (mainLightStatus)
            {
                //Edit colour sightly to reflect wall colour...
                Color lerpedColour = Color.Lerp(Color.white, defaultWallKey.colour1, 0.52f);

                foreach (NewWall window in windowsWithUVData)
                {
                    //Copy from lit map to the instanced texture
                    Color[] toCopy = building.preset.emissionMapLit.GetPixels((int)window.windowUV.originPixel.x, (int)window.windowUV.originPixel.y, (int)window.windowUV.rectSize.x, (int)window.windowUV.rectSize.y);

                    for (int i = 0; i < toCopy.Length; i++)
                    {
                        toCopy[i] *= lerpedColour;
                    }

                    building.emissionTextureInstanced.SetPixels((int)window.windowUV.originPixel.x, (int)window.windowUV.originPixel.y, (int)window.windowUV.rectSize.x, (int)window.windowUV.rectSize.y, toCopy);

                    textureChanged = true;
                }
            }
            else if (secondaryLightStatus)
            {
                //Edit colour sightly to reflect wall colour...
                Color lerpedColour = Color.Lerp(Color.gray, defaultWallKey.colour1, 0.52f);

                foreach (NewWall window in windowsWithUVData)
                {
                    //Copy from lit map to the instanced texture
                    Color[] toCopy = building.preset.emissionMapLit.GetPixels((int)window.windowUV.originPixel.x, (int)window.windowUV.originPixel.y, (int)window.windowUV.rectSize.x, (int)window.windowUV.rectSize.y);

                    for (int i = 0; i < toCopy.Length; i++)
                    {
                        toCopy[i] *= lerpedColour;
                    }

                    building.emissionTextureInstanced.SetPixels((int)window.windowUV.originPixel.x, (int)window.windowUV.originPixel.y, (int)window.windowUV.rectSize.x, (int)window.windowUV.rectSize.y, toCopy);

                    textureChanged = true;
                }
            }
            else
            {
                foreach (NewWall window in windowsWithUVData)
                {
                    //Game.Log("Copy window tex");

                    //Copy from unlit map to the instanced texture
                    try
                    {
                        Color[] toCopy = building.emissionTextureUnlit.GetPixels((int)window.windowUV.originPixel.x, (int)window.windowUV.originPixel.y, (int)window.windowUV.rectSize.x, (int)window.windowUV.rectSize.y);
                        building.emissionTextureInstanced.SetPixels((int)window.windowUV.originPixel.x, (int)window.windowUV.originPixel.y, (int)window.windowUV.rectSize.x, (int)window.windowUV.rectSize.y, toCopy);
                    }
                    catch
                    {
                        Game.LogError("Error with window texture! Check this is the correct size: " + building.preset.name);
                    }

                    textureChanged = true;
                }
            }

            //If the building's emission texture has been changed by the above, queue it for application...
            if (textureChanged)
            {
                if (!CitizenBehaviour.Instance.buildingEmissionTexturesToUpdate.Contains(building))
                {
                    CitizenBehaviour.Instance.buildingEmissionTexturesToUpdate.Add(building);
                }
            }
        }

    }

    public void AddMainLight(Interactable newLight)
    {
        if (!mainLights.Contains(newLight))
        {
            mainLights.Add(newLight);
            newLight.SetSwitchState(mainLightStatus, null, false, true);
        }
    }

    public void AddSecondaryLight(Interactable newLight)
    {
        if (!secondaryLights.Contains(newLight))
        {
            secondaryLights.Add(newLight);
        }
    }

    public void AddEntrance(NewNode fromNode, NewNode toNode, bool forceAccessType = false, NewNode.NodeAccess.AccessType forcedAccessType = NewNode.NodeAccess.AccessType.adjacent, bool forceWalkable = false)
    {
        if (!entrances.Exists(item => item.fromNode == fromNode && item.toNode == toNode))
        {
            //The doorway is always the parent wall: This only exists if there's a wall inbetween obvs.
            NewDoor foundDoor = null;
            NewWall foundwall = fromNode.walls.Find(item => item.otherWall != null && item.otherWall.node == toNode && item.parentWall.preset.sectionClass != DoorPairPreset.WallSectionClass.wall && item.parentWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventUpper && item.parentWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventLower && item.parentWall.preset.sectionClass != DoorPairPreset.WallSectionClass.ventTop);

            if (foundwall != null)
            {
                foundDoor = foundwall.parentWall.door;
                foundwall = foundwall.parentWall;
            }

            NewNode.NodeAccess newEntrance = new NewNode.NodeAccess(fromNode, toNode, foundwall, foundDoor, forceAccessType, forcedAccessType, forceWalkable);
            entrances.Add(newEntrance);

            //if (fromNode.gameLocation.thisAsStreet != null)
            //{
            //    if (entrances.Exists(item => item.fromNode == fromNode && item.toNode == toNode))
            //    {
            //        Game.Log("Add entrance to room " + fromNode.name); //This should also be adding to the room's entrance list as above, but it's not for some reason?
            //    }
            //}

            //Add entrance to other address
            if (fromNode.gameLocation != toNode.gameLocation)
            {
                fromNode.gameLocation.AddEntrance(fromNode, toNode, forceAccessType, forcedAccessType, forceWalkable);
            }
        }
    }

    public void RemoveEntrance(NewNode fromNode, NewNode toNode)
    {
        if (Mathf.RoundToInt(fromNode.nodeCoord.x) == 2 && Mathf.RoundToInt(fromNode.nodeCoord.y) == 44 && Mathf.RoundToInt(fromNode.nodeCoord.z) == 0)
        {
            Game.LogError("REMOVED ENTRANCE " + fromNode.name);
        }

        if (Mathf.RoundToInt(fromNode.nodeCoord.x) == 18 && Mathf.RoundToInt(fromNode.nodeCoord.y) == 39 && Mathf.RoundToInt(fromNode.nodeCoord.z) == 0)
        {
            Game.LogError("REMOVED ENTRANCE " + fromNode.name);
        }

        NewNode.NodeAccess entrance = entrances.Find(item => item.fromNode == fromNode && item.toNode == toNode);

        if (entrance != null)
        {
            entrance.walkingAccess = false;

            if (!SessionData.Instance.isFloorEdit)
            {
                if (gameLocation.entrances.Exists(item => item.fromNode == fromNode && item.toNode == toNode))
                {
                    Vector3 worldPos = Vector3.zero;
                    if (entrance != null) worldPos = entrance.worldAccessPoint;
                    Game.LogError("Entrance being removed from room that is still present in gamelocation: " + fromNode.room.name + " & " + toNode.room.name + " World access: " + worldPos + " node: " + fromNode.position);
                }
            }

            entrances.Remove(entrance);
        }

        ////DEBUG
        //int countRoomEnt = 0;

        //foreach (NewRoom room in gameLocation.rooms)
        //{
        //    countRoomEnt += room.entrances.Count;
        //}

        //if (gameLocation.entrances.Count != countRoomEnt)
        //{
        //    Game.LogError("Number of entrances in GL is different from the count in it's rooms!");
        //}
    }

    //Set Culling
    public void SetVisible(bool val, bool forceUpdate, string newDebug, bool immediateLoad = false)
    {
        //if(Player.Instance.inAirVent) Game.Log(name + " " + roomID + ", " + gameLocation.name + " SetVisible: " + val + " " + newDebug + " immediate load: " + immediateLoad);

        if(Game.Instance.devMode && Game.Instance.collectDebugData) debugCulling = val.ToString() + ": " + newDebug;

        if (this.gameObject == null) return;
        if (this.gameObject.activeSelf != isVisible) forceUpdate = true; //Workaround?

        if (isVisible != val || forceUpdate)
        {
            isVisible = val;
            this.gameObject.SetActive(isVisible);

            //Set inhabitants visible/invisible
            foreach (Actor a in currentOccupants)
            {
                a.SetVisible(isVisible);
            }

            //Enable/disable bugs
            foreach (BugController b in spawnedBugs)
            {
                b.enabled = isVisible;
            }

            if (val)
            {
                //Spawn if needed
                if (!geometryLoaded)
                {
                    GenerationController.Instance.LoadGeometryRoom(this);
                    //cullingTreeDebug.Add("...Load geometry...");
                }

                //Add to visible rooms reference
                CityData.Instance.visibleRooms.Add(this);

                //Set explored to minimum of 1
                if (!Player.Instance.inAirVent)
                {
                    if (explorationLevel < 1)
                    {
                        SetExplorationLevel(1);
                    }
                }

                //Load furnishings/objects
                foreach (FurnitureClusterLocation furnLoc in furniture)
                {
                    if (!furnLoc.loadedGeometry)
                    {
                        furnLoc.LoadFurnitureToWorld(forceSpawnImmediate: immediateLoad);
                    }
                }

                ExecuteStaticBatching(); //Combine static batches

                foreach (NewNode node in nodes)
                {
                    for (int i = 0; i < node.interactables.Count; i++)
                    {
                        node.interactables[i].LoadInteractableToWorld(forceSpawnImmediate: immediateLoad);
                    }
                }

                foreach (Interactable ls in lightswitchInteractables)
                {
                    ls.LoadInteractableToWorld(forceSpawnImmediate: immediateLoad);
                }

                //Execute blood spatter
                foreach (SpatterSimulation sp in spatter)
                {
                    if (!sp.isExecuted)
                    {
                        sp.Execute();
                    }
                    else
                    {
                        sp.UpdateSpawning();
                    }
                }

                UpdateLightCulling();

                foreach(PipeConstructor.PipeGroup pg in pipes)
                {
                    pg.SetVisible(true);
                }
            }
            else
            {
                //Remove from visible rooms reference
                CityData.Instance.visibleRooms.Remove(this);

                //Unload interactables...
                foreach (NewNode node in nodes)
                {
                    for (int i = 0; i < node.interactables.Count; i++)
                    {
                        Interactable toPool = node.interactables[i];

                        if (toPool.loadedGeometry)
                        {
                            toPool.UnloadInteractable();
                        }
                    }
                }

                foreach (Interactable ls in lightswitchInteractables)
                {
                    if (ls.loadedGeometry)
                    {
                        ls.UnloadInteractable();
                    }
                }

                foreach(PipeConstructor.PipeGroup pg in pipes)
                {
                    if(!SessionData.Instance.pipesToUnload.Contains(pg))
                    {
                        SessionData.Instance.pipesToUnload.Add(pg);
                        Toolbox.Instance.InvokeEndOfFrame(SessionData.Instance.UnloadPipes, "Unload pipes on room deactivate");
                    }
                }
            }

            if(SessionData.Instance.startedGame) UpdateFootprints(!val);

            //Disable lights if far away enough from player in terms of city tiles or height (unless stairwell top/bottom)
            //bool lightsEnable = true;

            //if (!SessionData.Instance.isFloorEdit && !SessionData.Instance.isTestScene && preset.autoDisableLightsOutOfVicinity)
            //{
            //    bool stairwellPass = true;

            //    if (preset.onlyAutoDisableInNonStairwell)
            //    {
            //        foreach (Interactable inter in mainLights)
            //        {
            //            if (inter.node.tile.isStairwell)
            //            {
            //                stairwellPass = false;
            //                break;
            //            }
            //        }
            //    }

            //    if (stairwellPass)
            //    {
            //        if (nodes.Count > 0)
            //        {
            //            if (Mathf.Abs(nodes.FirstOrDefault().nodeCoord.z - Player.Instance.currentNode.nodeCoord.z) > 2)
            //            {
            //                lightsEnable = false;
            //            }
            //        }

            //        //Vicinity check: Only do this if interior
            //        if (lightsEnable)
            //        {
            //            if (building != null)
            //            {
            //                if (!building.cityTile.isInPlayerVicinity) lightsEnable = false;
            //            }
            //        }
            //    }
            //}

            //EnableLight(lightsEnable); //Set lights on or off

            if (!SessionData.Instance.isFloorEdit && GenerationController.Instance.spawnedRooms.Count > ObjectPoolingController.Instance.maxRoomCache && Game.Instance.roomCacheLimit)
            {
                GenerationController.Instance.UnloadOldestRooms();
            }
        }

        //Below here, this will run every time this fuction is called instead of just when the room is set visible/invisible for the first time
        if(val)
        {
            //Are any doors in this room parented to other rooms? If so then parent them to this to make them visible...
            foreach (NewNode.NodeAccess access in entrances)
            {
                if (access.door != null)
                {
                    //cullingTreeDebug.Add("...Found doorway");

                    //Parent room is not loaded (by default set to inactive)
                    if (!access.door.parentedWall.node.room.geometryLoaded)
                    {
                        GenerationController.Instance.LoadGeometryRoom(access.door.parentedWall.node.room);
                        access.door.parentedWall.node.room.SetVisible(false, false, "Parented visibility", immediateLoad: immediateLoad);
                    }

                    //Door is inactive
                    if (!access.door.transform.parent.gameObject.activeInHierarchy)
                    {
                        if (access.door.wall.childWall.node.room == this)
                        {
                            //cullingTreeDebug.Add("...Parent Change to " + access.doorway.wall.childWall.room.name);
                            access.door.ParentToRoom(access.door.wall.childWall.node.room);
                        }
                        else if (access.door.wall.parentWall.node.room == this)
                        {
                            //cullingTreeDebug.Add("...Parent Change" + access.doorway.wall.parentWall.room.name);
                            access.door.ParentToRoom(access.door.wall.parentWall.node.room);
                        }
                    }

                    //Close on cull
                    if (access.door.preset.closeBehaviour == DoorPreset.ClosingBehaviour.closeOnCull)
                    {
                        if (!access.door.isClosed)
                        {
                            access.door.SetOpen(0f, null, true);
                        }
                    }
                }
            }
        }
        else
        {
            //Are any doors in this room parented to other rooms? If so then parent them to this to make them visible...
            foreach (NewNode.NodeAccess access in entrances)
            {
                if (access.door != null)
                {
                    //Door is parented to this room currently...
                    if(access.door.playerRoom == this)
                    {
                        if (access.door.wall.childWall.node.room.isVisible)
                        {
                            //cullingTreeDebug.Add("...Parent Change to " + access.doorway.wall.childWall.room.name);
                            access.door.ParentToRoom(access.door.wall.childWall.node.room);
                        }
                        else if (access.door.wall.parentWall.node.room.isVisible)
                        {
                            //cullingTreeDebug.Add("...Parent Change" + access.doorway.wall.parentWall.room.name);
                            access.door.ParentToRoom(access.door.wall.parentWall.node.room);
                        }
                    }
                }
            }
        }
    }

    //Potentially add this furniture for static batching (to be combined after)
    public void AddForStaticBatching(FurnitureLocation loc)
    {
        if(Game.Instance.enableRuntimeStaticBatching && loc.furniture.allowStaticBatching && loc.spawnedObject != null)
        {
            MeshFilter[] mf = loc.spawnedObject.GetComponentsInChildren<MeshFilter>(true);

            foreach(MeshFilter m in mf)
            {
                if(m != null && m.sharedMesh != null)
                {
                    MeshRenderer mr = m.gameObject.GetComponent<MeshRenderer>();

                    if (mr != null && mr.sharedMaterial != null)
                    {
                        AddForStaticBatching(m.gameObject, m.sharedMesh, mr.sharedMaterial);
                    }
                }
            }
        }
    }

    //Potentially add this wall frontage for static batching (to be combined after)
    public void AddForStaticBatching(GameObject obj, Mesh objectMesh, Material objectMat)
    {
        if (Game.Instance.enableRuntimeStaticBatching)
        {
            //Create static batch key
            StaticBatchKey newKey = new StaticBatchKey { mesh = objectMesh, mat = objectMat };

            if (!staticBatchDictionary.ContainsKey(newKey))
            {
                staticBatchDictionary.Add(newKey, new List<GameObject>());
                staticBatchDictionary[newKey].Add(obj);
            }
            else
            {
                if (!staticBatchDictionary[newKey].Contains(obj))
                {
                    staticBatchDictionary[newKey].Add(obj);
                }
            }
        }
    }

    //Execute combine of compatible game objects in this room
    public void ExecuteStaticBatching()
    {
        if(Game.Instance.enableRuntimeStaticBatching)
        {
            foreach(KeyValuePair<StaticBatchKey, List<GameObject>> pair in staticBatchDictionary)
            {
                for (int i = 0; i < pair.Value.Count; i++)
                {
                    if(pair.Value[i] == null)
                    {
                        pair.Value.RemoveAt(i);
                        i--;
                    }
                }

                if(pair.Value.Count > 1)
                {
                    try
                    {
                        StaticBatchingUtility.Combine(pair.Value.ToArray(), pair.Value[0]);
                        //Game.Log("Runtime static batching combined " + pair.Value.Count + " objects using mesh " + pair.Key.mesh.name + " and material " + pair.Key.mat.name);
                    }
                    catch
                    {
                        Game.LogError("Error trying to execute static batching...");
                    }
                }
            }

            staticBatchDictionary.Clear(); //Clean up
        }
    }

    public void UpdateLightCulling()
    {
        //Execute light culling
        foreach (LightController lc in lightCulling)
        {
            //Always draw if player is in room
            if (Player.Instance.currentRoom == this)
            {
                lc.SetCulled(false);
                continue;
            }

            if (lc == null)
            {
                lc.SetCulled(true);
                continue;
            }

            //Draw a ray from the light to the player, if it hits, display the light, if not then don't.
            Ray ray = new Ray(lc.transform.position, (CameraController.Instance.cam.transform.position + new Vector3(0, -0.3f, 0)) - lc.transform.position);
            float rayDistance = lc.hdrpLightData.fadeDistance + 1;

            //Debug.DrawRay(CameraController.Instance.transform.position, ray.direction, Color.cyan, 2f);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, rayDistance, Toolbox.Instance.lightCullingMask, queryTriggerInteraction: QueryTriggerInteraction.Collide))
            {
                //The player can directly see this light
                if (hit.transform.CompareTag("Player") || (hit.transform.parent != null && hit.transform.parent.CompareTag("Player")))
                {
                    Game.Log(lc.preset.name + " hits player to disable culling...");
                    lc.SetCulled(false);
                    continue;
                }
            }

            bool touchesPlayerRoom = false;

            //Raycast to current room nodes...
            //if (Player.Instance.currentRoom != null)
            //{
            //    foreach(NewNode n in Player.Instance.currentRoom.nodes)
            //    {
            //        Ray nodeRay = new Ray(lc.transform.position, (n.position - lc.transform.position));

            //        RaycastHit nodeHit;

            //        if (Physics.Raycast(nodeRay, out nodeHit, rayDistance, Toolbox.Instance.lightCullingMask, queryTriggerInteraction: QueryTriggerInteraction.Collide))
            //        {
            //            if (Player.Instance.currentRoom.combinedFloorMesh != null && hit.transform == Player.Instance.currentRoom.combinedFloorMesh.transform)
            //            {
            //                Game.Log(lc.preset.name + " hits floor mesh in player current room to disable culling...");
            //                lc.SetCulled(false);
            //                touchesPlayerRoom = true;
            //                break;
            //            }

            //            if (Player.Instance.currentRoom.combinedWallsMesh != null && hit.transform == Player.Instance.currentRoom.combinedWallsMesh.transform)
            //            {
            //                Game.Log(lc.preset.name + " hits walls mesh in player current room to disable culling...");
            //                lc.SetCulled(false);
            //                touchesPlayerRoom = true;
            //                break;
            //            }
            //        }
            //    }

            //    //Check adjacent rooms...
            //    if(!touchesPlayerRoom)
            //    {
            //        foreach(NewRoom adj in GameplayController.Instance.roomsVicinity)
            //        {
            //            foreach (NewNode n in adj.nodes)
            //            {
            //                Ray nodeRay = new Ray(lc.transform.position, (n.position - lc.transform.position));

            //                RaycastHit nodeHit;

            //                if (Physics.Raycast(nodeRay, out nodeHit, rayDistance, Toolbox.Instance.lightCullingMask, queryTriggerInteraction: QueryTriggerInteraction.Collide))
            //                {
            //                    if (adj.combinedFloorMesh != null && hit.transform == adj.combinedFloorMesh.transform)
            //                    {
            //                        Game.Log(lc.preset.name + " hits floor mesh in player adjacent room to disable culling...");
            //                        lc.SetCulled(false);
            //                        touchesPlayerRoom = true;
            //                        break;
            //                    }

            //                    if (adj.combinedWallsMesh != null && hit.transform == adj.combinedWallsMesh.transform)
            //                    {
            //                        Game.Log(lc.preset.name + " hits walls mesh in player adjacent room to disable culling...");
            //                        lc.SetCulled(false);
            //                        touchesPlayerRoom = true;
            //                        break;
            //                    }
            //                }
            //            }

            //            if (touchesPlayerRoom) break;
            //        }
            //    }
            //}

            //Check adjacent rooms...
            if (!touchesPlayerRoom)
            {
                foreach (NewRoom adj in GameplayController.Instance.roomsVicinity)
                {
                    foreach (NewNode n in adj.nodes)
                    {
                        if (Vector3.Distance(n.position, CameraController.Instance.cam.transform.position) > rayDistance) continue;

                        Ray nodeRay = new Ray(lc.transform.position, (n.position - lc.transform.position));

                        RaycastHit nodeHit;

                        if (Physics.Raycast(nodeRay, out nodeHit, rayDistance, Toolbox.Instance.lightCullingMask, queryTriggerInteraction: QueryTriggerInteraction.Collide))
                        {
                            if (adj.combinedFloor != null && hit.transform == adj.combinedFloor.transform)
                            {
                                Game.Log(lc.preset.name + " hits floor mesh in player adjacent room to disable culling...");
                                lc.SetCulled(false);
                                touchesPlayerRoom = true;
                                break;
                            }
                            else if (adj.combinedWalls != null && hit.transform == adj.combinedWalls.transform)
                            {
                                Game.Log(lc.preset.name + " hits walls mesh in player adjacent room to disable culling...");
                                lc.SetCulled(false);
                                touchesPlayerRoom = true;
                                break;
                            }
                        }
                    }

                    if (touchesPlayerRoom) break;
                }
            }

            if (touchesPlayerRoom)
            {
                continue;
            }
            else
            {
                lc.SetCulled(true);
            }
        }
    }

    //Triggers a footprint update of this room after x frames
    public void QueueFootprintUpdate()
    {
        if(!this.gameObject.activeSelf)
        {
            footprintUpdateQueued = false;
            return;
        }

        footprintUpdateQueued = true;
    }

    //Update spawned footprints within the room
    public void UpdateFootprints(bool forceRemoveAll = false)
    {
        if (!isVisible) return; //Don't update if invisible

        HashSet<GameplayController.Footprint> required = null;

        if (GameplayController.Instance.activeFootprints.ContainsKey(this) && !forceRemoveAll)
        {
            required = new HashSet<GameplayController.Footprint>(GameplayController.Instance.activeFootprints[this]);
        }
        else required = new HashSet<GameplayController.Footprint>();

        //Game.Log("Update footprints: " + name + ": " + required.Count);

        for (int i = 0; i < spawnedFootprints.Count; i++)
        {
            FootprintController fp = spawnedFootprints[i];

            if (required.Contains(fp.footprint))
            {
                required.Remove(fp.footprint);
            }
            else
            {
                FootprintController.RecycleFootprint(fp);
                spawnedFootprints.RemoveAt(i);
                i--;
                continue;
            }
        }

        foreach (GameplayController.Footprint fp in required)
        {
            FootprintController cont = FootprintController.GetNewFootprint();
            cont.transform.SetParent(this.transform);
            cont.Setup(fp);
            spawnedFootprints.Add(cont);
        }

        footprintUpdateQueued = false;
    }

    //Disable lights: At distance to increase performance, force lights to off
    public void EnableLight(bool val)
    {
        enabledLights = val;

        foreach (Interactable inter in mainLights)
        {
            if (inter.lightController != null)
            {
                inter.lightController.gameObject.SetActive(enabledLights);
            }
        }
    }

    //Connect nodes within this room: Also fills entrance list
    public void ConnectNodes()
    {
        //Game.Log("Connecting nodes for " + name + " (Make sure furniture blocking is set up before doing this)");

        //If editor, clear access first
        if (SessionData.Instance.isFloorEdit)
        {
            foreach (NewNode node in nodes)
            {
                node.accessToOtherNodes.Clear();
                //node.accessDebug.Clear();
            }
        }

        //Update bounds and world position
        UpdateWorldPositionAndBoundsSize();

        entrances.Clear();

        //Don't do this for outside spaces that aren't streets
        if ((gameLocation as NewAddress) != null)
        {
            if ((gameLocation as NewAddress).preset == CityControls.Instance.outsideLayoutConfig)
            {
                return;
            }
        }

        //Finally connect other nodes...
        foreach (NewNode node in nodes)
        {
            //Stairwells need to be calculated first...
            if (node.tile.isStairwell && !node.isConnected)
            {
                node.tile.ConnectStairwell();
            }

            //Calculate walk points
            node.UpdateWalkableSublocations();

            if (node.isConnected) continue; //Skip if already connected (ie stairwell)

            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX8)
            {
                Vector3Int offset = new Vector3Int(node.nodeCoord.x + v2.x, node.nodeCoord.y + v2.y, node.nodeCoord.z);
                NewNode foundNode = null;

                if (PathFinder.Instance.nodeMap.TryGetValue(offset, out foundNode))
                {
                    //Check for blocking (furniture access)
                    if (node.room.blockedAccess.ContainsKey(node))
                    {
                        if (node.room.blockedAccess[node].Contains(foundNode))
                        {
                            continue;
                        }
                    }

                    bool diagonal = false;

                    //Diagonal vectors are only valid if there are no walls on the sandwiching vectors...
                    if (Mathf.Abs(v2.x) + Mathf.Abs(v2.y) == 2)
                    {
                        Vector2 diagOffset = new Vector2(v2.x * 0.5f, v2.y * 0.5f);
                        diagonal = true;

                        //You need to check both this node and the other node for the inverse offsets...
                        Vector2 offset1 = new Vector2(0, diagOffset.y);
                        Vector2 offset2 = new Vector2(diagOffset.x, 0);

                        if (node.walls.Exists(item => item.wallOffset == offset1 || item.wallOffset == offset2))
                        {
                            continue; //Skip this vector search
                        }

                        offset1 = new Vector2(0, -diagOffset.y);
                        offset2 = new Vector2(-diagOffset.x, 0);

                        if (foundNode.walls.Exists(item => item.wallOffset == offset1 || item.wallOffset == offset2))
                        {
                            continue; //Skip this vector search
                        }
                    }

                    //Is there a wall here?
                    NewWall wallExists = node.walls.Find(item => item.wallOffset * 2 == v2);

                    if (wallExists != null)
                    {
                        //This is a window
                        if (wallExists.preset.sectionClass == DoorPairPreset.WallSectionClass.window || wallExists.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge)
                        {

                        }
                        else
                        {
                            //Is not an extrance, this this cannot be connected
                            if (wallExists.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance)
                            {
                                continue;
                            }
                        }
                    }

                    //Is the found node part of a stairwell?
                    if (foundNode.tile.isStairwell)
                    {
                        if (diagonal) continue; //No diagonal linking here

                        //Is there an upper link below? If so connect to that...
                        Vector3Int offsetBelow = new Vector3Int(foundNode.nodeCoord.x, foundNode.nodeCoord.y, foundNode.nodeCoord.z - 1);
                        NewNode belowNode = null;

                        if (PathFinder.Instance.nodeMap.TryGetValue(offsetBelow, out belowNode))
                        {
                            if (belowNode.stairwellUpperLink)
                            {
                                node.AddAccessToOtherNode(belowNode, forceAccessType: true, forcedAccessType: NewNode.NodeAccess.AccessType.adjacent/*, newDebug: "Connection to stairwell upper link 1"*/);
                                continue; //Added this, may screw things up??
                            }
                            else
                            {
                                if(belowNode.tile.isStairwell)
                                {
                                    //Game.LogError("Unable node below staircase at " + belowNode.position + ", but below is a stairwell... Could this just not be linked yet?");

                                    //Setup stairwell
                                    belowNode.tile.ConnectStairwell();

                                    if(belowNode.stairwellUpperLink)
                                    {
                                        //Game.LogError("The stairwell link is now correct...");

                                        node.AddAccessToOtherNode(belowNode, forceAccessType: true, forcedAccessType: NewNode.NodeAccess.AccessType.adjacent/*, newDebug: "Connection to stairwell upper link 2"*/);
                                        continue; //Added this, may screw things up??
                                    }
                                }
                                else
                                {
                                    //Game.LogError("Unable node below staircase at " + belowNode.position + " is not listed as a stairwell upper link...");
                                }
                            }
                        }

                        //This must be the lower node in order to connect to it...
                        if (!foundNode.stairwellLowerLink) continue;
                    }

                    //If no wall exists or it's an extrance then connect the nodes
                    node.AddAccessToOtherNode(foundNode/*, newDebug: "Connection by default (stairwell: " + node.tile.isStairwell + " connected: " + node.isConnected + ")"*/);
                }
            }
        }

        //Connect up/down using vertical space (don't do this in the above as it skips stiarwells)
        foreach (NewNode node in nodes)
        {
            //Connect above & below...
            if (gameLocation.thisAsAddress != null)
            {
                //Connect above
                if (node.floorType == NewNode.FloorTileType.floorOnly || node.floorType == NewNode.FloorTileType.noneButIndoors)
                {
                    Vector3Int offset = new Vector3Int(node.nodeCoord.x, node.nodeCoord.y, node.nodeCoord.z + 1);
                    NewNode foundNode = null;

                    if (PathFinder.Instance.nodeMap.TryGetValue(offset, out foundNode))
                    {
                        if (foundNode.gameLocation.thisAsAddress != null)
                        {
                            if (foundNode.floorType == NewNode.FloorTileType.noneButIndoors || foundNode.floorType == NewNode.FloorTileType.CeilingOnly)
                            {
                                if(!node.tile.isStairwell && !foundNode.tile.isStairwell)
                                {
                                    //If no wall exists or it's an extrance then connect the nodes
                                    node.AddAccessToOtherNode(foundNode);
                                    foundNode.AddAccessToOtherNode(node);
                                }

                                if (!node.room.atriumRooms.Contains(foundNode.room))
                                {
                                    node.room.atriumRooms.Add(foundNode.room);
                                }
                            }
                        }

                        //Attempt to find atrium top...
                        if (atriumTop == null) atriumTop = foundNode.room;
                        int seekCursor = 1;

                        while (seekCursor < 20)
                        {
                            Vector3Int aboveOffset = new Vector3Int(node.nodeCoord.x, node.nodeCoord.y, node.nodeCoord.z + seekCursor);

                            if (PathFinder.Instance.nodeMap.TryGetValue(aboveOffset, out foundNode))
                            {
                                //Found top
                                if (foundNode.floorType == NewNode.FloorTileType.CeilingOnly)
                                {
                                    atriumTop = foundNode.room;

                                    if (!node.room.atriumRooms.Contains(foundNode.room))
                                    {
                                        node.room.atriumRooms.Add(foundNode.room);
                                    }

                                    break;
                                }
                                //Found another atrium layer...
                                else if (foundNode.floorType == NewNode.FloorTileType.floorOnly || foundNode.floorType == NewNode.FloorTileType.noneButIndoors)
                                {
                                    //Make sure all atrium lights are visible
                                    if(foundNode.room.mainLightPreset != null && foundNode.room.mainLightPreset.lightingPreset != null && foundNode.room.mainLightPreset.lightingPreset.isAtriumLight)
                                    {
                                        if (!node.room.atriumRooms.Contains(foundNode.room))
                                        {
                                            node.room.atriumRooms.Add(foundNode.room);
                                        }
                                    }

                                    //Add atrium rooms visible above a certain distance
                                    if(nodes.Count / 38f >= seekCursor)
                                    {
                                        if (!node.room.atriumRooms.Contains(foundNode.room))
                                        {
                                            node.room.atriumRooms.Add(foundNode.room);
                                        }
                                    }

                                    seekCursor++;
                                }
                                else break;
                            }
                            else break;
                        }
                    }
                }

                //Connect below
                if (node.floorType == NewNode.FloorTileType.CeilingOnly || node.floorType == NewNode.FloorTileType.noneButIndoors)
                {
                    Vector3Int offset = new Vector3Int(node.nodeCoord.x, node.nodeCoord.y, node.nodeCoord.z - 1);
                    NewNode foundNode = null;

                    if (PathFinder.Instance.nodeMap.TryGetValue(offset, out foundNode))
                    {
                        if (foundNode.gameLocation.thisAsAddress != null)
                        {
                            if (foundNode.floorType == NewNode.FloorTileType.noneButIndoors || foundNode.floorType == NewNode.FloorTileType.floorOnly)
                            {
                                if(!node.tile.isStairwell && !foundNode.tile.isStairwell)
                                {
                                    //If no wall exists or it's an extrance then connect the nodes
                                    node.AddAccessToOtherNode(foundNode);
                                    foundNode.AddAccessToOtherNode(node);
                                }

                                if (!node.room.atriumRooms.Contains(foundNode.room))
                                {
                                    node.room.atriumRooms.Add(foundNode.room);
                                }
                            }
                        }
                    }
                }
            }

            //Is the access in this node limited to vertical space? If so mark it as inaccessable...
            bool limited = true;

            foreach (KeyValuePair<NewNode, NewNode.NodeAccess> acc in node.accessToOtherNodes)
            {
                if (acc.Value.walkingAccess)
                {
                    limited = false;
                    break;
                }
            }

            if (limited)
            {
                //node.walkableTile = false;
                node.isInaccessable = true;
                node.noAccess = true;
            }
        }
    }

    //Apply blocked access from furniture etc after furniture is chosen (only needed on city generation)
    public void ApplyBlockedAccess()
    {
        //Finally connect other nodes...
        foreach (NewNode node in nodes)
        {
            //Calculate walk points
            node.UpdateWalkableSublocations();

            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX8)
            {
                Vector3Int offset = new Vector3Int(node.nodeCoord.x + v2.x, node.nodeCoord.y + v2.y, node.nodeCoord.z);
                NewNode foundNode = null;

                if (PathFinder.Instance.nodeMap.TryGetValue(offset, out foundNode))
                {
                    //Check for blocking (furniture access)
                    if (node.room != null && node.room.blockedAccess != null && node.room.blockedAccess.ContainsKey(node))
                    {
                        try
                        {
                            if (node.room.blockedAccess[node].Contains(foundNode))
                            {
                                node.RemoveAccessToOtherNode(foundNode);
                            }
                        }
                        catch
                        {

                        }
                    }
                }
            }
        }
    }

    //Generate culling tree
    //This is now only done on generation
    //To make sure this is fully optimized, run from the top of a building down
    public void GenerateCullingTree(bool debugMode = false)
    {
        if (completedTreeCull && !debugMode) return;

        //System.Diagnostics.Stopwatch timeRecord = new System.Diagnostics.Stopwatch();
        //timeRecord.Start();

        if (debugMode)
        {
            cullingTree.Clear();
            doorCheckSet.Clear();
        }

        //Add self
        if (!cullingTree.ContainsKey(this))
        {
            cullingTree.Add(this, new List<CullTreeEntry>());
            cullingTree[this].Add(new CullTreeEntry(null));
        }

        //Don't run this for indoor null spaces
        if ((preset.roomType != CityControls.Instance.nullDefaultRoom || gameLocation.thisAsStreet != null) && nodes.Count > 0 && !isOutsideWindow)
        {
            //Useful flags for optizations...
            int checkRoomCount = 0;
            int checkEntrancePairCount = 0;
            int savedByExisting = 0;

            bool thisIsStreet = false;
            if (gameLocation.thisAsStreet != null) thisIsStreet = true;

            bool thisIsOutside = IsOutside();

            //Keep track of visible buildings; we don't have to draw any that aren't...
            HashSet<NewBuilding> visibleBuildings = new HashSet<NewBuilding>();
            if (building != null) visibleBuildings.Add(building);

            //Loop entrances for this room...
            //We can save time by automatically adding adjacent rooms...
            foreach (NewNode.NodeAccess entrance in entrances)
            {
                NewRoom adjRoom = entrance.toNode.room;

                //Ignore already visible...
                if (cullingTree.ContainsKey(adjRoom))
                {
                    //Ignore already visible, but potentially add to this if there is a different combination of required open doors...
                    //Skip if the existing room doesn't require any doors to be open...
                    if (cullingTree[adjRoom] != null && cullingTree[adjRoom][0].requiredOpenDoors == null || cullingTree[adjRoom][0].requiredOpenDoors.Count <= 0)
                    {
                        continue;
                    }
                }

                //Does this need required doors to be open?
                List<int> reqDoors = new List<int>();

                if (entrance.door != null && !entrance.door.preset.isTransparent)
                {
                    reqDoors.Add(entrance.door.wall.id);
                }

                bool add = false;

                //Add to this tree...
                if (!cullingTree.ContainsKey(adjRoom))
                {
                    cullingTree.Add(adjRoom, new List<CullTreeEntry>());

                    //We can add this as we know there are no other entries...
                    add = true;
                }
                else
                {
                    //If the current path requires no open doors, replace all lists with *just* this one
                    if (reqDoors.Count <= 0)
                    {
                        if (cullingTree[adjRoom].Count > 0) cullingTree[adjRoom].Clear();
                        add = true;
                    }
                    //Otherwise, only add if the current entry in the list DOESN'T feature an entry with no conditional doors already...
                    else
                    {
                        add = true;

                        //Check contents of list are different...
                        for (int u = 0; u < cullingTree[adjRoom].Count; u++)
                        {
                            CullTreeEntry ce = cullingTree[adjRoom][u];

                            //There is an existing entry of no doors, forget about other door combinations...
                            if (ce.requiredOpenDoors == null || ce.requiredOpenDoors.Count <= 0)
                            {
                                add = false;
                                break;
                            }

                            //Otherwise check for a duplicate list of doors sing sequence equal
                            if (ce.requiredOpenDoors.SequenceEqual(reqDoors))
                            {
                                add = false;
                                break;
                            }
                        }
                    }
                }

                if (add)
                {
                    if (debugMode)
                    {
                        SpawnDebugCullingObject(entrance.worldAccessPoint, adjRoom, entrance, reqDoors, CullingDebugController.CullDebugType.adjacent);
                    }

                    cullingTree[adjRoom].Add(new CullTreeEntry(reqDoors));

                    //Add any atrium top...
                    if (adjRoom.atriumTop != null)
                    {
                        if (!cullingTree.ContainsKey(adjRoom.atriumTop))
                        {
                            cullingTree.Add(adjRoom.atriumTop, new List<CullTreeEntry>());
                        }
                        else cullingTree[adjRoom.atriumTop].Clear();

                        cullingTree[adjRoom.atriumTop].Add(new CullTreeEntry(reqDoors));

                        if (debugMode)
                        {
                            SpawnDebugCullingObject(adjRoom.atriumTop.nodes.First().position, adjRoom.atriumTop, entrance, reqDoors, CullingDebugController.CullDebugType.atriumTop, adjRoom);
                        }
                    }

                    //Add to door listening set...
                    if (reqDoors != null)
                    {
                        foreach (int d in reqDoors)
                        {
                            if (!doorCheckSet.Contains(d))
                            {
                                doorCheckSet.Add(d);
                            }
                        }
                    }
                }

                //Add seen buildings from this window/entrance...
                if (gameLocation.thisAsStreet == null)
                {
                    foreach (NewBuilding b in CityData.Instance.buildingDirectory)
                    {
                        if (!visibleBuildings.Contains(b))
                        {
                            //Do a FoV check...
                            //Is this within FoV
                            Vector3 targetDir = b.worldPosition - entrance.toNode.position;
                            Vector3 diff = (entrance.toNode.nodeCoord - entrance.fromNode.nodeCoord);
                            float angleToOther = Vector3.Angle(targetDir, diff.normalized); //Local angle

                            if (angleToOther >= -CullingControls.Instance.visibleBuildingFoV && angleToOther <= CullingControls.Instance.visibleBuildingFoV) // 180° FOV
                            {
                                visibleBuildings.Add(b);
                            }
                        }
                    }
                }
                else
                {
                    foreach (NewBuilding b in CityData.Instance.buildingDirectory)
                    {
                        if (!visibleBuildings.Contains(b))
                        {
                            visibleBuildings.Add(b);
                        }
                    }
                }
            }

            //Is this part of a building? We can save a load of work by adding pre-calculated window trees...
            if (!thisIsStreet && building != null && floor != null && floor.floor > 0)
            {
                foreach (NewNode.NodeAccess entrance in entrances)
                {
                    //One must be outside, the other inside...
                    if (entrance.toNode.room.IsOutside())
                    {
                        //Which way does this entrance face?
                        Vector3 diff = (entrance.toNode.nodeCoord - entrance.fromNode.nodeCoord);
                        Vector2 facing = diff.normalized;

                        //Does this entrance have a pre-calculated tree to add?
                        if (building != null && building.directionalCullingTrees != null && building.directionalCullingTrees.ContainsKey(facing))
                        {
                            foreach (KeyValuePair<NewRoom, List<CullTreeEntry>> pair in building.directionalCullingTrees[facing])
                            {
                                if (!cullingTree.ContainsKey(pair.Key))
                                {
                                    cullingTree.Add(pair.Key, pair.Value);

                                    //Add to door listening set...
                                    for (int i = 0; i < pair.Value.Count; i++)
                                    {
                                        CullTreeEntry cte = null;

                                        try
                                        {
                                            cte = pair.Value[i];
                                        }
                                        catch
                                        {
                                            continue;
                                        }

                                        if (cte != null && cte.requiredOpenDoors != null)
                                        {
                                            foreach (int d in cte.requiredOpenDoors)
                                            {
                                                if (!doorCheckSet.Contains(d))
                                                {
                                                    doorCheckSet.Add(d);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            Game.Log("CityGen: Added precalculated entries for building " + building.buildingID + " facing " + facing + " entries: " + building.directionalCullingTrees[facing].Count);
                        }
                    }
                }
            }

            //Do an inverse check as it's more efficient. Scan every other room's entrance for this...
            foreach (NewRoom otherRoom in CityData.Instance.roomDirectory)
            {
                //Don't run for this room; we've already added itself
                if (otherRoom == this) continue;
                if (otherRoom.nodes.Count <= 0) continue;

                //Useful flags for optizations...
                bool otherIsStreet = false;
                if (otherRoom.gameLocation.thisAsStreet != null) otherIsStreet = true;

                bool otherIsOutside = otherRoom.IsOutside();

                //Don't run this for null rooms (that aren't the street)
                if (otherRoom.preset.roomType == CityControls.Instance.nullDefaultRoom && !otherIsStreet) continue;
                if (otherRoom.isOutsideWindow) continue;

                //Street optimizations...
                //This is street...
                if (thisIsStreet)
                {
                    //Only check other streets, ground floor rooms and forced outdoors areas...
                    if (!otherIsStreet)
                    {
                        if (!otherIsOutside /*otherRoom.preset.forceOutside != RoomConfiguration.OutsideSetting.forceOutside*/)
                        {
                            if(Mathf.Abs(otherRoom.floor.floor) == 1)
                            {
                                bool skipDueToFloor = true;

                                foreach(NewNode.NodeAccess acc in otherRoom.entrances)
                                {
                                    if(acc.accessType == NewNode.NodeAccess.AccessType.bannister)
                                    {
                                        if(acc.toNode.room.atriumRooms.Count > 0)
                                        {
                                            skipDueToFloor = false;
                                            break;
                                        }
                                    }
                                }

                                if (skipDueToFloor) continue;
                            }
                            //Don't scan above or below floor 1...
                            else if (otherRoom.floor.floor != 0)
                            {
                                continue;
                            }
                        }
                        //Automatically add other outdoor areas on buildings we can see...
                        else
                        {
                            if(otherRoom.building != null)
                            {
                                if(visibleBuildings.Contains(otherRoom.building))
                                {
                                    float distance = Vector3.Distance(worldPos, otherRoom.worldPos);

                                    if (distance <= CullingControls.Instance.fromOutsideToOutsideDistanceMax)
                                    {
                                        if (!cullingTree.ContainsKey(otherRoom))
                                        {
                                            cullingTree.Add(otherRoom, new List<CullTreeEntry>());
                                        }
                                        else cullingTree[otherRoom].Clear();

                                        cullingTree[otherRoom].Add(new CullTreeEntry(null));

                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }
                //If both aren't street...
                else if (!thisIsStreet && !otherIsStreet)
                {
                    //Don't run if in different buildings, unless outside
                    if (building != otherRoom.building && !otherIsOutside /*otherRoom.preset.forceOutside != RoomConfiguration.OutsideSetting.forceOutside*/)
                    {
                        //One must be above or below ground floor in order to skip...
                        if (floor.floor != 0 || otherRoom.floor.floor != 0)
                        {
                            continue;
                        }
                    }
                    else if(building == otherRoom.building)
                    {
                        //Both outside in the same building; auto add
                        if(otherIsOutside && thisIsOutside && Mathf.Abs(floor.floor - otherRoom.floor.floor) <= 5)
                        {
                            if (!cullingTree.ContainsKey(otherRoom))
                            {
                                cullingTree.Add(otherRoom, new List<CullTreeEntry>());
                                List<int> dList = new List<int>();
                                cullingTree[otherRoom].Add(new CullTreeEntry(dList));

                                continue;
                            }
                        }
                    }
                }

                bool sameFloor = false;
                if (floor != null && otherRoom.floor != null && floor.floor == otherRoom.floor.floor) sameFloor = true;

                if (cullingTree.ContainsKey(otherRoom) && cullingTree[otherRoom] != null && cullingTree[otherRoom].Count > 0)
                {
                    try
                    {
                        //Ignore already visible, but potentially add to this if there is a different combination of required open doors...
                        //Skip if the existing room doesn't require any doors to be open...
                        if (cullingTree[otherRoom][0].requiredOpenDoors == null || cullingTree[otherRoom][0].requiredOpenDoors.Count <= 0)
                        {
                            savedByExisting++;
                            continue;
                        }
                    }
                    catch
                    {

                    }
                }

                checkRoomCount++;

                bool breakRoomLoop = false;

                //New method
                //Loop entrances for this room
                foreach (NewNode.NodeAccess entrance in entrances)
                {
                    if (entrance == null) continue;

                    //If both are streets, we can ignore everything apart from street-to-street type entrances
                    if (thisIsStreet && otherIsStreet)
                    {
                        if (entrance.accessType != NewNode.NodeAccess.AccessType.streetToStreet)
                        {
                            continue;
                        }
                    }

                    //Distance checking
                    float distance = Vector3.Distance(entrance.worldAccessPoint, otherRoom.worldPos);

                    //To inside
                    if (!otherIsStreet && otherIsOutside) //otherRoom.preset.forceOutside != RoomConfiguration.OutsideSetting.forceOutside)
                    {
                        //From outside
                        if (thisIsStreet || thisIsOutside)//preset.forceOutside == RoomConfiguration.OutsideSetting.forceOutside)
                        {
                            if (distance > CullingControls.Instance.fromOutsideToInsideDistanceMax) continue;
                        }
                        //From inside
                        else
                        {
                            distance = Vector3.Distance(new Vector3(entrance.worldAccessPoint.x, 0, entrance.worldAccessPoint.z), new Vector3(otherRoom.worldPos.x, 0, otherRoom.worldPos.z));
                            if (distance > CullingControls.Instance.fromInsideToInsideDistanceMax) continue;
                        }
                    }
                    //To outside
                    else
                    {
                        //From outside
                        if (thisIsStreet || thisIsOutside)//preset.forceOutside == RoomConfiguration.OutsideSetting.forceOutside)
                        {
                            if (distance > CullingControls.Instance.fromOutsideToOutsideDistanceMax) continue;
                        }
                        //From inside
                        else
                        {
                            if (distance > CullingControls.Instance.fromInsideToOutsideDistanceMax) continue;
                        }
                    }

                    //We can ignore if both sides of the entrance are the same room...
                    if (entrance.fromNode.room == entrance.toNode.room)
                    {
                        continue;
                    }

                    //If this isn't a street and above the ground floor, we can ignore other streets unless through a window...
                    if (!thisIsStreet && otherIsStreet && floor != null && floor.floor != 0 && thisIsOutside) //preset.forceOutside != RoomConfiguration.OutsideSetting.forceOutside)
                    {
                        if (entrance.accessType != NewNode.NodeAccess.AccessType.window)
                        {
                            continue;
                        }
                    }

                    //Interior to interiors cannot be connected via window unless looking down on ground floor
                    if (!thisIsStreet && !otherIsStreet && !otherIsOutside)
                    {
                        if (entrance.accessType == NewNode.NodeAccess.AccessType.window && otherRoom.floor.floor != 0)
                        {
                            //if (!otherIsOutside /*otherRoom.preset.forceOutside != RoomConfiguration.OutsideSetting.forceOutside*/)
                            //{
                                continue;
                            //}
                        }
                    }

                    //Is this within FoV
                    Vector3 targetDir = otherRoom.worldPos - entrance.worldAccessPoint;
                    float angleToOther = Vector3.Angle(targetDir, (entrance.toNode.position - entrance.fromNode.position).normalized); //Local angle

                    if (angleToOther < -CullingControls.Instance.visibleRoomFoV || angleToOther > CullingControls.Instance.visibleRoomFoV)
                    {
                        continue; //Out of FoV
                    }

                    //Loop entrances for other room...
                    foreach (NewNode.NodeAccess otherEntrance in otherRoom.entrances)
                    {
                        if (otherEntrance == null) continue;

                        //We can ignore if both sides of the entrance are the same room...
                        if (otherEntrance.fromNode.room == otherEntrance.toNode.room)
                        {
                            continue;
                        }

                        //If both are streets, we can ignore everything apart from street-to-street type entrances
                        if (thisIsStreet && otherIsStreet)
                        {
                            if (otherEntrance.accessType != NewNode.NodeAccess.AccessType.streetToStreet)
                            {
                                continue;
                            }
                        }

                        //Entrances are within FoV...
                        checkEntrancePairCount++;
                        //Check from the outside/to node to the other room's inside/from node

                        //Do a FoV check...
                        //Is this within FoV
                        targetDir = otherEntrance.fromNode.nodeCoord - entrance.toNode.nodeCoord;

                        Vector3 diff = (entrance.toNode.nodeCoord - entrance.fromNode.nodeCoord);
                        angleToOther = Vector3.Angle(targetDir, diff.normalized); //Local angle

                        if (angleToOther >= -CullingControls.Instance.visibleRoomFoV && angleToOther <= CullingControls.Instance.visibleRoomFoV)
                        {
                            //FoV Check is passed...
                            if (entrance.toNode == null || otherEntrance.fromNode == null) continue;

                            //If entrances are close enough, then automatically add...
                            if(((sameFloor || (thisIsOutside && otherIsOutside)) && thisIsStreet == otherIsStreet && Vector3.Distance(entrance.worldAccessPoint, otherEntrance.worldAccessPoint) <= 5f) | ((!sameFloor /*|| thisIsStreet != otherIsStreet*/) && Vector3.Distance(entrance.worldAccessPoint, otherEntrance.worldAccessPoint) <= 2f))
                            {
                                if (!cullingTree.ContainsKey(otherRoom))
                                {
                                    cullingTree.Add(otherRoom, new List<CullTreeEntry>());
                                    List<int> dList = new List<int>();

                                    if (entrance.door != null && entrance.door.wall != null)
                                    {
                                        try
                                        {
                                            dList.Add(entrance.door.wall.id);
                                        }
                                        catch
                                        {
                                            Game.Log("Failed to add entrance to DList");
                                        }
                                    }

                                    if (otherEntrance.door != null && otherEntrance.door.wall != null)
                                    {
                                        try
                                        {
                                            dList.Add(otherEntrance.door.wall.id);
                                        }
                                        catch
                                        {
                                            Game.Log("Failed to add entrance to DList");
                                        }
                                    }

                                    cullingTree[otherRoom].Add(new CullTreeEntry(dList));
                                }
                            }
                            else
                            {
                                //Now do a data raycast and add all visible rooms on the way...
                                List<DataRaycastController.NodeRaycastHit> pointList;

                                //I don't think we need a seperate system adding the target as it should be included in the path list output..
                                DataRaycastController.Instance.EntranceRaycast(entrance, otherEntrance, out pointList);

                                if(debugMode) Game.Log("Checking entrance " + entrance.name + " and " + otherEntrance.name + ": " + DataRaycastController.Instance.EntranceRaycast(entrance, otherEntrance, out pointList) + " length: " + pointList.Count);

                                //Add all rooms on resulting path...
                                List<NewRoom> revList = new List<NewRoom>();

                                //Start at 1st as 0 will always be self
                                for (int i = 1; i < pointList.Count; i++)
                                {
                                    if (debugMode) Game.Log(i + "/" + pointList.Count);

                                    //Normal direction...
                                    DataRaycastController.NodeRaycastHit v3 = pointList[i];
                                    NewNode foundNode = null;

                                    if (PathFinder.Instance.nodeMap.TryGetValue(v3.coord, out foundNode))
                                    {
                                        if (foundNode.room != null)
                                        {
                                            if (foundNode.room == this) continue;
                                            bool add = false;
                                            if (debugMode) Game.Log("Add: False");

                                            CullingDebugController.CullDebugType newCullType = CullingDebugController.CullDebugType.succeededOvr;

                                            //Add to this tree...
                                            if (!cullingTree.ContainsKey(foundNode.room))
                                            {
                                                cullingTree.Add(foundNode.room, new List<CullTreeEntry>());
                                                add = true;
                                                newCullType = CullingDebugController.CullDebugType.succeededNew;
                                                if (debugMode) Game.Log("Add: True: No existing " + foundNode.room.name);
                                            }
                                            else
                                            {
                                                //If the current path requires no open doors, replace all lists with *just* this one
                                                if (v3.conditionalDoors == null || v3.conditionalDoors.Count <= 0)
                                                {
                                                    if (cullingTree[foundNode.room].Count > 0) cullingTree[foundNode.room].Clear();
                                                    add = true;
                                                    if (debugMode) Game.Log("Add: True: Requies no open doors");
                                                }
                                                //Otherwise, only add if the current entry in the list DOESN'T feature an entry with no conditional doors already...
                                                else
                                                {
                                                    add = true;
                                                    if (debugMode) Game.Log("Add: True: Existing is found: " + foundNode.room.name);

                                                    //Check contents of list are different...
                                                    for (int u = 0; u < cullingTree[foundNode.room].Count; u++)
                                                    {
                                                        CullTreeEntry ce = null;

                                                        try
                                                        {
                                                            ce = cullingTree[foundNode.room][u];

                                                        }
                                                        catch
                                                        {
                                                            continue;
                                                        }

                                                        if (ce == null) continue;

                                                        //There is an existing entry of no doors, forget about other door combinations...
                                                        if (ce.requiredOpenDoors == null || ce.requiredOpenDoors.Count <= 0)
                                                        {
                                                            add = false;
                                                            if (debugMode) Game.Log("Add: False: Existing has no door requirements");
                                                            break;
                                                        }

                                                        //Otherwise check for a duplicate list of doors sing sequence equal
                                                        if (ce.requiredOpenDoors.SequenceEqual(v3.conditionalDoors))
                                                        {
                                                            if (debugMode)
                                                            {
                                                                Game.Log("Add: False: Duplicate door requirements found: ");

                                                                foreach (int inte in v3.conditionalDoors)
                                                                {
                                                                    Game.Log(inte);
                                                                }
                                                            }

                                                            add = false;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }

                                            if (add)
                                            {
                                                if (debugMode)
                                                {
                                                    SpawnDebugCullingObject(foundNode.position, foundNode.room, entrance, v3.conditionalDoors, newCullType, otherEntrance: otherEntrance);
                                                }

                                                cullingTree[foundNode.room].Add(new CullTreeEntry(v3.conditionalDoors));

                                                //Add any atrium top...
                                                if (foundNode.room.atriumTop != null)
                                                {
                                                    if (!cullingTree.ContainsKey(foundNode.room.atriumTop))
                                                    {
                                                        cullingTree.Add(foundNode.room.atriumTop, new List<CullTreeEntry>());
                                                    }

                                                    cullingTree[foundNode.room.atriumTop].Clear();
                                                    cullingTree[foundNode.room.atriumTop].Add(new CullTreeEntry(v3.conditionalDoors));

                                                    if (debugMode)
                                                    {
                                                        SpawnDebugCullingObject(foundNode.room.atriumTop.nodes.First().position, foundNode.room.atriumTop, entrance, v3.conditionalDoors, CullingDebugController.CullDebugType.atriumTop, foundNode.room);
                                                    }
                                                }

                                                //Add to door listening set...
                                                if (v3.conditionalDoors != null)
                                                {
                                                    foreach (int d in v3.conditionalDoors)
                                                    {
                                                        if (!doorCheckSet.Contains(d))
                                                        {
                                                            doorCheckSet.Add(d);
                                                        }
                                                    }
                                                }

                                                //If this addition is the original 'other' room...
                                                //If this addition had no conditional doors, then we can break this loop
                                                if (foundNode.room == otherRoom)
                                                {
                                                    if (v3.conditionalDoors == null || v3.conditionalDoors.Count <= 0)
                                                    {
                                                        breakRoomLoop = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                pointList = null;
                            }
                        }

                        if (breakRoomLoop)
                        {
                            break;
                        }
                    }

                    if (breakRoomLoop)
                    {
                        break;
                    }
                }
            }

            //Calculate audio occlusion...
            List<NewRoom> openSetRooms = new List<NewRoom>(); //Room data and distance from source
            List<NewNode.NodeAccess> openSetEntrances = new List<NewNode.NodeAccess>();
            List<int> openSetDist = new List<int>();

            //Set audio occlusion
            openSetRooms.Clear();
            openSetRooms.Add(this);
            openSetDist.Clear();
            openSetDist.Add(0);
            openSetEntrances.Clear();
            openSetEntrances.Add(null);
            nonAudioOccludedRooms.Clear();

            while (openSetRooms.Count > 0)
            {
                NewRoom current = openSetRooms[0];
                int distanceFromSource = openSetDist[0]; //Distance from source in room count
                NewNode.NodeAccess currentEnt = openSetEntrances[0]; //Reference to the entrance that got here

                if (distanceFromSource >= 3) break;

                //If this is rendered doors only, don't search for further rooms
                //Search for open doorways (normal entrances without doors)
                foreach (NewNode.NodeAccess access in current.entrances)
                {
                    //Get floor difference...
                    int floorDiff = Mathf.Abs((int)nodes.FirstOrDefault().nodeCoord.z - (int)access.toNode.nodeCoord.z);

                    //An open doorway: An entrance has the cull adjacent room flag off. These are typically for portals where the adjacent room would be visible.
                    if (access.accessType == NewNode.NodeAccess.AccessType.openDoorway || access.accessType == NewNode.NodeAccess.AccessType.bannister)
                    {
                        if (!openSetRooms.Contains(access.toNode.room) && !nonAudioOccludedRooms.Contains(access.toNode.room))
                        {
                            openSetRooms.Add(access.toNode.room);
                            openSetDist.Add(distanceFromSource + 1);
                            openSetEntrances.Add(access);
                        }
                    }
                    //Verical space:
                    //Unless you're in the room with the vertical connection, limit this to rooms immediately above or below
                    else if (access.accessType == NewNode.NodeAccess.AccessType.verticalSpace)
                    {
                        if (!openSetRooms.Contains(access.toNode.room) && !nonAudioOccludedRooms.Contains(access.toNode.room))
                        {
                            openSetRooms.Add(access.toNode.room);
                            openSetDist.Add(distanceFromSource + 1);
                            openSetEntrances.Add(access);
                        }
                    }
                }

                if (current != this)
                {
                    nonAudioOccludedRooms.Add(current);
                }

                openSetRooms.RemoveAt(0);
                openSetDist.RemoveAt(0);
                openSetEntrances.RemoveAt(0);
            }

            //Get above and below rooms
            aboveRooms.Clear();
            belowRooms.Clear();
            adjacentRooms.Clear();

            if (gameLocation.thisAsAddress != null)
            {
                foreach (NewNode node in nodes)
                {
                    Vector3Int aboveCoord = node.nodeCoord + new Vector3Int(0, 0, 1);
                    Vector3Int belowCoord = node.nodeCoord + new Vector3Int(0, 0, -1);

                    NewNode foundNode = null;

                    if (PathFinder.Instance.nodeMap.TryGetValue(aboveCoord, out foundNode))
                    {
                        if (!foundNode.room.isOutsideWindow && foundNode.gameLocation.thisAsAddress != null)
                        {
                            if (!aboveRooms.Contains(foundNode.room))
                            {
                                aboveRooms.Add(foundNode.room);
                            }
                        }
                    }

                    if (PathFinder.Instance.nodeMap.TryGetValue(belowCoord, out foundNode))
                    {
                        if (!foundNode.room.isOutsideWindow && foundNode.gameLocation.thisAsAddress != null)
                        {
                            if (!belowRooms.Contains(foundNode.room))
                            {
                                belowRooms.Add(foundNode.room);
                            }
                        }
                    }

                    foreach (NewWall wall in node.walls)
                    {
                        if (wall.node != node)
                        {
                            if (!wall.node.room.isOutsideWindow && wall.node.gameLocation.thisAsAddress != null)
                            {
                                if (!adjacentRooms.Contains(wall.node.room))
                                {
                                    adjacentRooms.Add(wall.node.room);
                                }
                            }
                        }
                        else
                        {
                            if (!wall.otherWall.node.room.isOutsideWindow && wall.otherWall.node.gameLocation.thisAsAddress != null)
                            {
                                if (!adjacentRooms.Contains(wall.otherWall.node.room))
                                {
                                    adjacentRooms.Add(wall.otherWall.node.room);
                                }
                            }
                        }
                    }
                }
            }

            float ratio = (float)checkRoomCount / (float)CityData.Instance.roomDirectory.Count;
            float savedByExistingRatio = (float)savedByExisting / (float)CityData.Instance.roomDirectory.Count;


            //timeRecord.Stop();
            //Game.Log("CityGen: Generated culling tree for " + name + " (Street: " + thisIsStreet + ", Rooms: " + checkRoomCount + " (" + Mathf.RoundToInt(ratio * 100) + "%), Saved by Existing: " + savedByExisting + " (" + Mathf.RoundToInt(savedByExistingRatio * 100) + "%), Entrance Pairs: " + checkEntrancePairCount + ") Time: " + timeRecord.Elapsed.ToString() + "s, entries: " + cullingTree.Count);
        }
        else
        {
            //timeRecord.Stop();
            //Game.Log("CityGen: Skipping null & non-street room or isOutsideWindow " + name + " Time: " + timeRecord.Elapsed.ToString() + "s, entries: " + cullingTree.Count);
        }

        //Mark this flag
        completedTreeCull = true;
    }

    //Spawn debug culling object
    private void SpawnDebugCullingObject(Vector3 worldPos, NewRoom room, NewNode.NodeAccess parentEntrance, List<int> depDoors, CullingDebugController.CullDebugType newType, NewRoom atriumTopOf = null, NewNode.NodeAccess otherEntrance = null)
    {
        if (!Game.Instance.devMode) return;
        int existing = spawnPathDebug.FindIndex(item => item.transform.position == worldPos);

        //Remove existing
        if (existing > -1)
        {
            Toolbox.Instance.DestroyObject(spawnPathDebug[existing].gameObject);
            spawnPathDebug.RemoveAt(existing);
        }

        List<NewDoor> dependentDoors = new List<NewDoor>();

        if (depDoors != null)
        {
            foreach (int d in depDoors)
            {
                dependentDoors.Add(CityData.Instance.doorDictionary[d]);
            }
        }

        //Remove previous
        GameObject newD = Toolbox.Instance.SpawnObject(PrefabControls.Instance.debugNodeDisplay, this.transform);
        CullingDebugController cdc = newD.GetComponent<CullingDebugController>();
        cdc.Setup(room, parentEntrance, dependentDoors, newType, atriumTopOf, otherEntrance);
        spawnPathDebug.Add(cdc);
        newD.transform.position = worldPos;
    }

    //Set lower room
    public void SetLowerRoom(NewRoom newRoom)
    {
        lowerRoom = newRoom;
        lowerRoom.allowCoving = false; //Cancel coving
    }

    public void AddOccupant(Actor newOcc)
    {
        if (!currentOccupants.Contains(newOcc))
        {
            currentOccupants.Add(newOcc);

            //Set shadow update mode active
            actorUpdate = true;

            //Update occlusion
            for (int i = 0; i < audibleLoopingSounds.Count; i++)
            {
                audibleLoopingSounds[i].UpdateOcclusion();
            }

            if (newOcc.isDead) containsDead = true;
        }
    }

    public void RemoveOccupant(Actor remOcc)
    {
        if (currentOccupants.Contains(remOcc))
        {
            currentOccupants.Remove(remOcc);

            if (currentOccupants.Count <= 0)
            {
                actorUpdate = false;
                containsDead = false;
                //fullCaptureNeeded = true; //Full capture needed if everyone has left the room
            }
            else if(containsDead)
            {
                bool noDead = true;

                foreach(Actor a in currentOccupants)
                {
                    if (a.isDead)
                    {
                        noDead = false;
                        break;
                    }
                }

                if (noDead) containsDead = false;
            }
        }
    }

    //Add furniture to this room
    public void AddFurniture(FurnitureClusterLocation newFurn, bool generateNew, bool addPathBlocking = true, bool immediateSpawn = false)
    {
        if (!furniture.Contains(newFurn))
        {
            if(Game.Instance.devMode && Game.Instance.collectDebugData)
            {
                if (clustersPlaced != null && clustersPlaced.Length > 0) clustersPlaced += ", ";
                clustersPlaced += newFurn.cluster.presetName;
            }

            FurnitureLocation lastPlacedObject = null;
            string randomKey = seed;

            foreach (KeyValuePair<NewNode, List<FurnitureLocation>> pair in newFurn.clusterObjectMap)
            {
                foreach (FurnitureLocation obj in pair.Value)
                {
                    if (generateNew)
                    {
                        //Pick furniture
                        if (obj.furniture == null)
                        {
                            foreach (FurnitureClass furnClass in obj.furnitureClasses)
                            {
                                randomKey = Toolbox.Instance.RandContained(0, 999999, randomKey, out randomKey).ToString();

                                obj.furniture = GenerationController.Instance.PickFurniture(furnClass, this, randomKey);

                                if (obj.furniture != null)
                                {
                                    break;
                                }
                                else
                                {
                                    Game.Log("CityGen: No furniture found with design style");
                                }
                            }

                            if (obj.furnitureClasses == null || obj.furnitureClasses.Count <= 0)
                            {
                                Game.Log("CityGen: No furniture classes found for " + newFurn.cluster.name);
                            }

                            if (obj.furniture == null)
                            {
                                continue;
                            }
                        }

                        //Pick art
                        if (obj.furniture.isArt && !obj.pickedArt)
                        {
                            obj.art = GenerationController.Instance.PickArt(obj.furniture.artOrientation, this);
                            gameLocation.artPieces.Add(obj.art);
                            obj.pickedArt = true;
                        }

                        //Pick materials
                        if (!obj.pickedMaterials)
                        {
                            if (obj.furniture.inheritColouringFromDecor && obj.furniture.variations.Count > 0)
                            {
                                bool foundMaterialKey = false;

                                if (obj.furniture.shareColours != FurniturePreset.ShareColours.none)
                                {
                                    //Find other with this color sharing designation...
                                    FurnitureLocation other = individualFurniture.Find(item => item.furniture.shareColours == obj.furniture.shareColours && item != obj);

                                    if (other != null)
                                    {
                                        obj.matKey = other.matKey;
                                        foundMaterialKey = true;
                                    }
                                }

                                if (!foundMaterialKey)
                                {
                                    obj.matKey = MaterialsController.Instance.GenerateMaterialKey(obj.furniture.variations[Toolbox.Instance.RandContained(0, obj.furniture.variations.Count, randomKey, out randomKey)], colourScheme, this, obj.furniture.inheritGrubFromDecor);
                                    foundMaterialKey = true;
                                }
                            }

                            obj.pickedMaterials = true;
                        }

                        //Pick owners: Move to below
                        if (!obj.pickedOwners)
                        {
                            obj.pickedOwners = true;

                            if (obj.furnitureClasses[0].copyFromPreviouslyPlacedInCluster && lastPlacedObject != null)
                            {
                                //Game.Log("Copy from last placed object...");

                                foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> ownerPair in lastPlacedObject.ownerMap)
                                {
                                    if(ownerPair.Key.human != null) obj.AssignOwner(ownerPair.Key.human, false);
                                    else obj.AssignOwner(ownerPair.Key.address, false);
                                }

                                obj.UpdateIntegratedObjectOwnership();
                            }
                            //Assign homeless owners...
                            else if (obj.furnitureClasses[0].assignHomelessOwners)
                            {
                                if (CityData.Instance.homlessAssign.Count > 0)
                                {
                                    int assignIndex = Toolbox.Instance.RandContained(0, CityData.Instance.homlessAssign.Count, randomKey, out randomKey);
                                    obj.AssignOwner(CityData.Instance.homlessAssign[assignIndex], true);
                                    //CityData.Instance.homlessAssign.RemoveAt(assignIndex); //May cause issues with race conditions
                                }
                            }
                            else if (obj.furnitureClasses[0].assignBelongsToOwners > 0)
                            {
                                if(obj.furnitureClasses[0].assignMailbox && gameLocation.building != null)
                                {
                                    List<NewAddress> validAssignees = new List<NewAddress>();

                                    foreach (KeyValuePair<int, NewFloor> floorPair in gameLocation.building.floors)
                                    {
                                        foreach (NewAddress add in floorPair.Value.addresses)
                                        {
                                            if (add.residence != null && add.residence.mailbox == null)
                                            {
                                                validAssignees.Add(add);
                                            }
                                        }
                                    }

                                    //Sort by residence number...
                                    validAssignees.Sort((p1, p2) => p1.residence.GetResidenceNumber().CompareTo(p2.residence.GetResidenceNumber()));

                                    //Now assign to new furniture...
                                    int assigneeCursor = 0;

                                    for (int i = 0; i < obj.furnitureClasses[0].assignBelongsToOwners; i++)
                                    {
                                        if (assigneeCursor < validAssignees.Count)
                                        {
                                            //Look for couples
                                            NewAddress thisAssignee = validAssignees[assigneeCursor];

                                            //Skip if already has mailbox...
                                            while (thisAssignee.residence == null || thisAssignee.residence.mailbox != null)
                                            {
                                                assigneeCursor++;
                                                if (assigneeCursor >= validAssignees.Count) break;

                                                thisAssignee = validAssignees[assigneeCursor];

                                                continue;
                                            }

                                            if (assigneeCursor >= validAssignees.Count) break;

                                            thisAssignee.residence.mailbox = obj;
                                            obj.AssignOwner(thisAssignee, true);

                                            assigneeCursor++;
                                        }
                                    }
                                }
                                else
                                {
                                    //Add new ownership class reference...
                                    if (!gameLocation.furnitureBelongsTo.ContainsKey(obj.furnitureClasses[0].ownershipClass))
                                    {
                                        gameLocation.furnitureBelongsTo.Add(obj.furnitureClasses[0].ownershipClass, new Dictionary<FurnitureLocation, List<Human>>());
                                    }

                                    //Make a list of valid inhabitants that need to be assigned one of these types...
                                    List<Human> validAssignees = new List<Human>();

                                    if(obj.furnitureClasses[0].ownershipSource == FurnitureClass.OwnershipSource.addressInhabitants && gameLocation.thisAsAddress != null)
                                    {
                                        validAssignees.AddRange(gameLocation.thisAsAddress.inhabitants);
                                    }
                                    else if (obj.furnitureClasses[0].ownershipSource == FurnitureClass.OwnershipSource.buildingResidences && gameLocation.building != null)
                                    {
                                        foreach (KeyValuePair<int, NewFloor> floorPair in gameLocation.building.floors)
                                        {
                                            foreach (NewAddress add in floorPair.Value.addresses)
                                            {
                                                if (add.residence != null && add.residence.mailbox == null)
                                                {
                                                    foreach (Human h in add.owners)
                                                    {
                                                        if (validAssignees.Exists(item => item.home == h.home)) continue;
                                                        validAssignees.Add(h);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    //Remove citizens who already own furniture in this class
                                    foreach (KeyValuePair<FurnitureLocation, List<Human>> ownedFurniture in gameLocation.furnitureBelongsTo[obj.furnitureClasses[0].ownershipClass])
                                    {
                                        foreach (Human h in ownedFurniture.Value)
                                        {
                                            validAssignees.Remove(h);
                                        }
                                    }

                                    //Only pick from room owners...
                                    if (obj.furnitureClasses[0].onlyPickFromRoomOwners)
                                    {
                                        if (belongsTo.Count > 0)
                                        {
                                            for (int i = 0; i < validAssignees.Count; i++)
                                            {
                                                if (!belongsTo.Contains(validAssignees[i]))
                                                {
                                                    validAssignees.RemoveAt(i);
                                                    i--;
                                                }
                                            }
                                        }
                                    }

                                    //If this contains a work position, remove assignees with preferred rooms
                                    if (obj.furniture.isWorkPosition)
                                    {
                                        List<Human> topList = new List<Human>();

                                        for (int i = 0; i < validAssignees.Count; i++)
                                        {
                                            Human human = validAssignees[i];

                                            //If the preferred room matches, send this citizen to the top of the list
                                            if (human.job != null)
                                            {
                                                //Citizen must own work position, be in preferred room and feature the correct integrated interactable
                                                if (human.job.preset.ownsWorkPosition && human.job.preset.preferredRooms.Contains(preset) && obj.furniture.integratedInteractables.Exists(item => item.preset.specialCaseFlag == human.job.preset.jobPostion))
                                                {
                                                    topList.Add(human);
                                                }
                                                //If incompatible room then remove from valid lis
                                                else if (human.job.preset.preferredRooms.Count > 0)
                                                {
                                                    validAssignees.RemoveAt(i);
                                                    i--;
                                                }
                                            }
                                        }

                                        //Insert @ top of list
                                        foreach (Human h in topList)
                                        {
                                            validAssignees.Remove(h);
                                            validAssignees.Insert(0, h);
                                        }
                                    }

                                    validAssignees.Sort();
                                    validAssignees.Reverse();

                                    //Now assign to new furniture...
                                    Human lastAssignee = null;
                                    int assigneeCursor = 0;

                                    for (int i = 0; i < obj.furnitureClasses[0].assignBelongsToOwners; i++)
                                    {
                                        if (assigneeCursor < validAssignees.Count)
                                        {
                                            //Look for couples
                                            Human thisAssignee = validAssignees[assigneeCursor];

                                            if (lastAssignee != null && obj.furnitureClasses[0].preferCouples)
                                            {
                                                if (lastAssignee.partner != null)
                                                {
                                                    if (validAssignees.Contains(lastAssignee.partner))
                                                    {
                                                        thisAssignee = lastAssignee.partner;
                                                    }
                                                    else break; //If partner isn't in asignees, then break.
                                                }
                                            }

                                            obj.AssignOwner(thisAssignee, false);
                                            lastAssignee = thisAssignee;

                                            assigneeCursor++;
                                        }
                                    }

                                    if(obj.furnitureClasses[0].assignBelongsToOwners > 0) obj.UpdateIntegratedObjectOwnership();
                                }
                            }

                            //Keep referenced to last placed object
                            if (obj != lastPlacedObject)
                            {
                                lastPlacedObject = obj;
                            }
                        }

                        //Assign group ID to room
                        if (obj.furniture.furnitureGroup != FurniturePreset.FurnitureGroup.none)
                        {
                            if (!furnitureGroups.ContainsKey(obj.furniture.furnitureGroup))
                            {
                                furnitureGroups.Add(obj.furniture.furnitureGroup, obj.furniture.groupID);
                            }
                        }

                        //Add objects to pool
                        foreach(InteractablePreset inter in obj.furniture.spawnObjectsOnPlacement)
                        {
                            if(Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, CityData.Instance.seed + inter.presetName + roomID + obj.anchorNode.nodeCoord.ToString()) <= obj.furniture.spawnObjectOnChance)
                            {
                                gameLocation.AddToPlacementPool(inter, null, null, null);
                            }
                        }

                        if (obj.furnitureClasses.Count > 0 && obj.furnitureClasses.Exists(item => item.raiseLightswitch))
                        {
                            obj.RaiseLightswitch();
                        }

                        obj.AssignID(this);
                    }

                    if (Game.Instance.devMode && Game.Instance.collectDebugData && SessionData.Instance.startedGame)
                    {
                        Game.Log("Decor: Adding new furniture: " + obj.furnitureClasses[0].name + " to " + GetName() + " (Generate new: " + generateNew + ", id: " + obj.id + ")");
                    }

                    if (obj == null) continue;

                    if (obj.furniture == null)
                    {
                        Game.LogError("Furniture " + obj.id + " at " + name + ", " + gameLocation.name + " has no preset reference...");
                        continue;
                    }

                    //Add references to node & room
                    if (!individualFurniture.Contains(obj))
                    {
                        individualFurniture.Add(obj);
                    }

                    //Add furniture to node
                    if (!pair.Key.individualFurniture.Contains(obj))
                    {
                        pair.Key.individualFurniture.Add(obj);
                    }

                    //Add reference to wall
                    if(obj.furniture.classes == null || obj.furniture.classes.Count <= 0 || obj.furniture.classes[0] == null)
                    {
                        Game.LogError(obj.furniture.name + " has no assigned furniture class!");
                    }
                    else if (obj.furniture.classes[0].wallPiece)
                    {
                        foreach (FurnitureClass.FurnitureWallRule wallRule in obj.furniture.classes[0].wallRules)
                        {
                            Vector2Int o = CityData.Instance.GetOffsetFromDirection(wallRule.wallDirection);

                            Vector2 rotatedNodeOffset = Toolbox.Instance.RotateVector2CW(wallRule.nodeOffset, obj.angle);
                            Vector3Int tileOffsetCoord = obj.anchorNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(rotatedNodeOffset.x), Mathf.RoundToInt(rotatedNodeOffset.y), 0);

                            //Scan for rear wall
                            Vector2 rotatedWallOffset = Toolbox.Instance.RotateVector2CW(o, obj.angle) * 0.5f;
                            rotatedWallOffset = new Vector2(Mathf.Round(rotatedWallOffset.x * 2f) / 2f, Mathf.Round(rotatedWallOffset.y * 2f) / 2f); //Set to 0-0.5

                            NewNode foundWallNode = null;

                            if (PathFinder.Instance.nodeMap.TryGetValue(tileOffsetCoord, out foundWallNode))
                            {
                                NewWall foundWall = foundWallNode.walls.Find(item => item.wallOffset == rotatedWallOffset);

                                if (foundWall != null)
                                {
                                    foundWall.placedWallFurn = true;

                                    //if (!foundWall.individualFurniture.Contains(obj))
                                    //{
                                    //    foundWall.individualFurniture.Add(obj);
                                    //}
                                }
                            }
                        }
                    }

                    //Set no pass through
                    foreach (FurnitureClass furnClass in obj.furnitureClasses)
                    {
                        if (furnClass.noPassThrough)
                        {
                            pair.Key.noPassThrough = true;
                        }

                        if (furnClass.noAccessNeeded)
                        {
                            //pair.Key.noAccess = true;
                            noAccessNodes.Add(pair.Key);
                        }
                    }

                    //Is this a job board? If so add a reference to it in citydata
                    if (obj.furniture.isJobBoard)
                    {
                        if (!CityData.Instance.jobBoardsDirectory.Contains(obj))
                        {
                            //Game.Log("Jobs: Add job board to list...");
                            CityData.Instance.jobBoardsDirectory.Add(obj);
                        }
                    }

                    //Create interactables: Can't do this in loading thread, as it must be the main
                    //if(CityConstructor.Instance != null && CityConstructor.Instance.generateNew)
                    //{
                    //    if (!obj.createdInteractables)
                    //    {
                    //        obj.CreateInteractables();
                    //    }
                    //}

                    //Block nodes
                    if(addPathBlocking)
                    {
                        foreach (NewNode node in obj.coversNodes)
                        {
                            foreach (FurnitureClass furnClass in obj.furnitureClasses)
                            {
                                if (furnClass.occupiesTile)
                                {
                                    //if(!node.allowNewFurniture)
                                    //{
                                    //    Game.Log("Node " + node.nodeCoord + " doesn't allow furniture, but we're trying to spawn here...");
                                    //}

                                    node.SetAllowNewFurniture(false);
                                }
                            }
                        }

                        //Block node access
                        AddFurnitureBlockedAccess(obj);

                        //Add fov block
                        AddFOVBlock(obj);
                    }

                    //Alter street area lights
                    if (generateNew && obj.furniture.alterAreaLighting)
                    {
                        foreach (LightZoneData lz in lightZones)
                        {
                            if (obj.furniture.possibleColours.Count > 0)
                            {
                                string seed = CityData.Instance.seed + roomID + lz.nodeList.Count;
                                Color newColour = obj.furniture.possibleColours[Toolbox.Instance.RandContained(0, obj.furniture.possibleColours.Count, seed, out seed)];

                                if (obj.furniture.lightOperation == DistrictPreset.AffectStreetAreaLights.lerp)
                                {
                                    lz.areaLightColour = Color.Lerp(lz.areaLightColour, newColour, obj.furniture.lightAmount);
                                }
                                else if (obj.furniture.lightOperation == DistrictPreset.AffectStreetAreaLights.multiply)
                                {
                                    lz.areaLightColour = lz.areaLightColour * (newColour * obj.furniture.lightAmount);
                                }
                                else if (obj.furniture.lightOperation == DistrictPreset.AffectStreetAreaLights.add)
                                {
                                    lz.areaLightColour = lz.areaLightColour + (newColour * obj.furniture.lightAmount);
                                }

                                //Set the colour
                                if (lz.spawnedAreaLight != null) lz.spawnedAreaLight.color = lz.areaLightColour;
                            }

                            lz.areaLightBrightness += obj.furniture.brightnessModifier;
                            lz.debug.Add("Area light modification from furniture: " + obj.furniture.brightnessModifier);

                            //Set the brightness
                            if (lz.spawnedAreaLight != null) lz.aAdditional.intensity = lz.areaLightBrightness;
                        }
                    }
                }
            }

            furniture.Add(newFurn);

            //Set the breaker box references to 0, to avoid additional boxes spawning...
            if (newFurn.cluster.isBreakerBox)
            {
                if (gameLocation.thisAsAddress != null && gameLocation.thisAsAddress.addressPreset != null)
                {
                    //Does this address need it's own breaker box?
                    if (gameLocation.thisAsAddress.addressPreset.useOwnBreakerBox && gameLocation.thisAsAddress.breakerLightsID <= -1)
                    {
                        gameLocation.thisAsAddress.breakerLightsID = 0;
                    }
                    //Does this floor need a breaker box?
                    else
                    {
                        if (gameLocation.floor != null && gameLocation.floor.breakerLightsID <= -1)
                        {
                            gameLocation.floor.breakerLightsID = 0;
                        }
                    }
                }
            }

            //Game.Log("Added " + newFurn);

            //Load if geometry is already loaded
            if (geometryLoaded)
            {
                newFurn.LoadFurnitureToWorld(immediateSpawn);
            }

            if (SessionData.Instance.startedGame)
            {
                ObjectPoolingController.Instance.UpdateObjectRanges();
                decorEdit = true; //Force the decor edit flag so this is saved if we have started the game...
            }
        }
        else
        {
            Game.Log("This furniture already exists here: " + newFurn);
        }
    }

    //Add custom furniture from a new placement
    public FurnitureLocation AddFurnitureCustom(PlayerApartmentController.FurniturePlacement newPlacement)
    {
        if (newPlacement == null || newPlacement.anchorNode == null || newPlacement.preset == null) return null;

        FurnitureClusterLocation newLocation = new FurnitureClusterLocation(newPlacement.anchorNode, PlayerApartmentController.Instance.nullCluster, 0, 0);

        Game.Log("Decor: Adding new furniture (new) " + newPlacement.preset.name + " with an offset of " + newPlacement.offset);

        //Add singular furniture location to list
        FurnitureLocation obj = new FurnitureLocation(newLocation, newPlacement.preset.classes, newPlacement.angle, newPlacement.anchorNode, newPlacement.coversNodes, false, Vector3.zero, 0, Vector3.one, newUserPlaced: true, newOffset: newPlacement.offset);

        //Fill in furniture settings (type, colour etc)
        obj.furniture = newPlacement.preset;
        obj.pickedMaterials = true;
        obj.matKey = newPlacement.materialKey;

        if(newPlacement.art != null)
        {
            obj.art = newPlacement.art;
            obj.pickedArt = true;
        }

        //Add to cluster map
        foreach (NewNode coversNode in obj.coversNodes)
        {
            if (!newLocation.clusterObjectMap.ContainsKey(coversNode))
            {
                newLocation.clusterObjectMap.Add(coversNode, new List<FurnitureLocation>());
            }

            newLocation.clusterObjectMap[coversNode].Add(obj);
            if (!newLocation.clusterList.Contains(obj)) newLocation.clusterList.Add(obj);
        }

        AddFurniture(newLocation, true, addPathBlocking: false, immediateSpawn: true);

        return obj;
    }

    //Add custom furniture from an existing placement
    public FurnitureLocation AddFurnitureCustom(FurnitureLocation newPlacement)
    {
        if (newPlacement == null)
        {
            Game.Log("Decor: Existing furniture class is null!");
            return null;
        }
        else if (newPlacement.anchorNode == null)
        {
            Game.Log("Decor: Existing furniture has null anchor node!");
            return null;
        }

        Game.Log("Decor: Adding new furniture (existing) " + newPlacement.furniture.name + " at anchor " + newPlacement.anchorNode.position + " with an offset of " + newPlacement.offset);

        FurnitureClusterLocation newLocation = new FurnitureClusterLocation(newPlacement.anchorNode, PlayerApartmentController.Instance.nullCluster, 0, 0);
        newPlacement.cluster = newLocation;
        newLocation.loadedGeometry = false;

        //Add to cluster map
        foreach (NewNode coversNode in newPlacement.coversNodes)
        {
            if (!newLocation.clusterObjectMap.ContainsKey(coversNode))
            {
                newLocation.clusterObjectMap.Add(coversNode, new List<FurnitureLocation>());
            }

            newLocation.clusterObjectMap[coversNode].Add(newPlacement);
            if (!newLocation.clusterList.Contains(newPlacement)) newLocation.clusterList.Add(newPlacement);
        }

        AddFurniture(newLocation, false, addPathBlocking: false, immediateSpawn: true);

        return newPlacement;
    }

    //Add blocked access from a furniture piece
    public void AddFurnitureBlockedAccess(FurnitureLocation obj)
    {
        //Game.Log("Adding furniture blocked access for " + obj);

        //Cycle access block
        foreach (FurnitureClass furnClass in obj.furnitureClasses)
        {
            foreach (FurnitureClass.BlockedAccess blocked in furnClass.blockedAccess)
            {
                if (blocked.disabled) continue;

                Vector2 rotated = Toolbox.Instance.RotateVector2CW(blocked.nodeOffset, obj.angle);
                Vector3Int offsetCoord = obj.anchorNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(rotated.x), Mathf.RoundToInt(rotated.y), 0);
                NewNode foundNode = null;

                //This is the node for which to apply the blocks...
                if (PathFinder.Instance.nodeMap.TryGetValue(offsetCoord, out foundNode))
                {
                    foreach (CityData.BlockingDirection d in blocked.blocked)
                    {
                        Vector2 blockOffset = CityData.Instance.GetOffsetFromDirection(d);

                        //Scan for blocked nodes
                        Vector2 blockedOffsetRotated = Toolbox.Instance.RotateVector2CW(blockOffset, obj.angle);
                        Vector3Int blockedCoord = foundNode.nodeCoord + new Vector3Int(Mathf.RoundToInt(blockedOffsetRotated.x), Mathf.RoundToInt(blockedOffsetRotated.y), 0);
                        NewNode blockedNode = null;

                        if (PathFinder.Instance.nodeMap.TryGetValue(blockedCoord, out blockedNode))
                        {
                            //Game.Log("*Add blocked node for " + obj.furniture.name + " at " + obj.anchorNode.nodeCoord + " = " + blockedNode.nodeCoord);

                            //Add blocked in both directions...
                            try
                            {
                                if (!foundNode.room.blockedAccess.ContainsKey(foundNode))
                                {
                                    foundNode.room.blockedAccess.Add(foundNode, new List<NewNode>());
                                }

                                foundNode.room.blockedAccess[foundNode].Add(blockedNode);
                            }
                            catch
                            {

                            }

                            //Add blocked in both directions...
                            try
                            {
                                if (!blockedNode.room.blockedAccess.ContainsKey(blockedNode))
                                {
                                    blockedNode.room.blockedAccess.Add(blockedNode, new List<NewNode>());
                                }

                                blockedNode.room.blockedAccess[blockedNode].Add(foundNode);
                            }
                            catch
                            {

                            }
                        }
                    }
                }

                //Apply external diagonal blocking
                if (blocked.blockExteriorDiagonals)
                {
                    if (blocked.blocked.Contains(CityData.BlockingDirection.behindLeft))
                    {
                        Vector2 rotatedOff1 = Toolbox.Instance.RotateVector2CW(new Vector2(-1, 0), obj.angle);
                        Vector3Int offsetCoordOff1 = offsetCoord + new Vector3Int(Mathf.RoundToInt(rotatedOff1.x), Mathf.RoundToInt(rotatedOff1.y), 0);
                        NewNode foundNodeOff1 = null;

                        //This is the node for which to apply the blocks...
                        if (PathFinder.Instance.nodeMap.TryGetValue(offsetCoordOff1, out foundNodeOff1))
                        {
                            Vector2 rotatedOff2 = Toolbox.Instance.RotateVector2CW(new Vector2(0, -1), obj.angle);
                            Vector3Int offsetCoordOff2 = offsetCoord + new Vector3Int(Mathf.RoundToInt(rotatedOff2.x), Mathf.RoundToInt(rotatedOff2.y), 0);
                            NewNode foundNodeOff2 = null;

                            //This is the node for which to apply the blocks...
                            if (PathFinder.Instance.nodeMap.TryGetValue(offsetCoordOff2, out foundNodeOff2))
                            {
                                //Add blocked in both directions...
                                try
                                {
                                    if (!foundNodeOff1.room.blockedAccess.ContainsKey(foundNodeOff1))
                                    {
                                        foundNodeOff1.room.blockedAccess.Add(foundNodeOff1, new List<NewNode>());
                                    }

                                    foundNodeOff1.room.blockedAccess[foundNodeOff1].Add(foundNodeOff2);
                                }
                                catch
                                {

                                }

                                //Add blocked in both directions...
                                try
                                {
                                    if (!foundNodeOff2.room.blockedAccess.ContainsKey(foundNodeOff2))
                                    {
                                        foundNodeOff2.room.blockedAccess.Add(foundNodeOff2, new List<NewNode>());
                                    }

                                    foundNodeOff2.room.blockedAccess[foundNodeOff2].Add(foundNodeOff1);
                                }
                                catch
                                {

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //Add fov block from furniture
    private void AddFOVBlock(FurnitureLocation obj)
    {
        if (obj.useFOVBLock)
        {
            Vector2 rotatedDir = Toolbox.Instance.RotateVector2CW(obj.fovDirection, obj.angle); //The rotated direction, add this every node
            //Game.Log("FOV block with direction " + rotatedDir);

            foreach (NewNode node in obj.coversNodes)
            {
                Vector3Int cursor = node.nodeCoord + new Vector3Int(Mathf.RoundToInt(rotatedDir.x), Mathf.RoundToInt(rotatedDir.y), 0);

                for (int i = 0; i < obj.fovMaxDistance; i++)
                {
                    NewNode foundNode = null;

                    //This is the node for which to apply the blocks...
                    if (PathFinder.Instance.nodeMap.TryGetValue(cursor, out foundNode))
                    {
                        //Game.Log("FOV found node at " + cursor);

                        if (foundNode.room == this)
                        {
                            //Hit object
                            if (!foundNode.allowNewFurniture)
                            {
                                //Game.Log("...Not allowed furniture, breaking");
                                break;
                            }
                            else
                            {
                                foundNode.SetAllowNewFurniture(false);
                                cursor += new Vector3Int(Mathf.RoundToInt(rotatedDir.x), Mathf.RoundToInt(rotatedDir.y), 0);
                            }
                        }
                        else
                        {
                            //Game.Log("...Differnt room, breaking");
                            break;
                        }
                    }
                    else break;
                }
            }
        }
    }

    //Add an air vent to this room: Return false if unable to place...
    public bool AddRandomAirVent(NewAddress.AirVent ventType)
    {
        //Game.Log("Adding air vent to " + this.name);
        AirDuctGroup.AirVent newVent = null;

        //Pick location...
        if (ventType == NewAddress.AirVent.ceiling)
        {
            //Find a place on the ceiling. It can't be where an exiting vent is or light fitting...
            List<NewNode> validLocations = new List<NewNode>();

            foreach (NewNode node in nodes)
            {
                //Get the ceiling coordinate in vent space...
                Vector3Int ductCoord = new Vector3Int(node.nodeCoord.x, node.nodeCoord.y, node.nodeCoord.z * 3 + 2);
                if (!building.validVentSpace.ContainsKey(ductCoord)) continue; //Must be valid space...

                if (mainLights.Exists(item => item.node == node)) continue; //Light fitting
                if (airVents.Exists(item => item.node == node)) continue; //Existing vent

                //Avoid being on the same node as a wall vent
                bool wallCheck = true;

                foreach (NewWall wall in node.walls)
                {
                    if (wall.otherWall.node.room.airVents.Exists(item => item.wall == wall.otherWall || item.wall == wall))
                    {
                        //Existing vent
                        wallCheck = false;
                        break;
                    }
                }

                if (!wallCheck) continue;
                
                //Favour corners, add one entry per wall present.
                for (int i = 0; i < node.walls.Count + 1; i++)
                {
                    validLocations.Add(node);
                }
            }

            if (validLocations.Count <= 0)
            {
                //Game.LogError("No valid ceiling vent locations for " + this.name);
                return false; //No valid locations :(
            }

            newVent = new AirDuctGroup.AirVent(ventType, this);
            newVent.node = validLocations[Toolbox.Instance.GetPsuedoRandomNumber(0, validLocations.Count, seed)];
            newVent.debugNode = newVent.node.position;

            //Block the opposite vent space from becoming a duct
            Vector3Int oppSpace = new Vector3Int(newVent.node.nodeCoord.x, newVent.node.nodeCoord.y, newVent.node.nodeCoord.z * 3 + 1);
            if (building.validVentSpace.ContainsKey(oppSpace)) building.validVentSpace.Remove(oppSpace);
        }
        else if (ventType == NewAddress.AirVent.wallLower || ventType == NewAddress.AirVent.wallUpper)
        {
            List<NewWall> validLocations = new List<NewWall>();
            List<Vector3Int> validCoords = new List<Vector3Int>();

            foreach (NewNode node in nodes)
            {
                if (node.tile.isStairwell) continue; //Skip placement on stairwells

                //This is trickier- Must not be adjacent to outside or another address
                foreach (NewWall wall in node.walls)
                {
                    if (wall.preset.sectionClass != DoorPairPreset.WallSectionClass.wall) continue;
                    if (airVents.Exists(item => item.wall == wall || item.wall != wall.otherWall)) continue; //Existing vent
                    if (wall.otherWall.node.room.airVents.Exists(item => item.wall == wall || item.wall != wall.otherWall)) continue; //Existing vent
                    if(wall.otherWall.node.room.airVents.Exists(item => item.ventType == NewAddress.AirVent.ceiling && (item.node == wall.otherWall.node || item.node == wall.node))) continue; //Existing vent

                    //Get the ceiling coordinate in vent space...
                    if (ventType == NewAddress.AirVent.wallUpper)
                    {
                        Vector3Int ductCoord = new Vector3Int(wall.otherWall.node.nodeCoord.x, wall.otherWall.node.nodeCoord.y, wall.otherWall.node.nodeCoord.z * 3 + 1);
                        if (!building.validVentSpace.ContainsKey(ductCoord)) continue; //Must be valid space...

                        //The inside node can have no other vents in this node space.
                        if (wall.otherWall.node.walls.Exists(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.ventUpper)) continue;
                        if (wall.node.walls.Exists(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.ventUpper)) continue;

                        validCoords.Add(new Vector3Int(wall.node.nodeCoord.x, wall.node.nodeCoord.y, wall.node.nodeCoord.z * 3 + 1)); //Add the room side to the valid coords so we can remove it
                    }
                    else if (ventType == NewAddress.AirVent.wallLower)
                    {
                        Vector3Int ductCoord = new Vector3Int(wall.otherWall.node.nodeCoord.x, wall.otherWall.node.nodeCoord.y, wall.otherWall.node.nodeCoord.z * 3 + 0);
                        if (!building.validVentSpace.ContainsKey(ductCoord)) continue; //Must be valid space...

                        //The inside node can have no other vents in this node space.
                        if (wall.otherWall.node.walls.Exists(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.ventLower)) continue;
                        if (wall.node.walls.Exists(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.ventLower)) continue;

                        validCoords.Add(new Vector3Int(wall.node.nodeCoord.x, wall.node.nodeCoord.y, wall.node.nodeCoord.z * 3 + 0)); //Add the room side to the valid coords so we can remove it
                    }

                    validLocations.Add(wall);
                }
            }

            if (validLocations.Count <= 0)
            {
                Game.Log("CityGen: No valid wall vent locations for " + this.name);
                return false; //No valid locations :(
            }

            newVent = new AirDuctGroup.AirVent(ventType, this);

            int chosenIndex = Toolbox.Instance.GetPsuedoRandomNumber(0, validLocations.Count, seed);
            newVent.wall = validLocations[chosenIndex];
            newVent.node = newVent.wall.otherWall.node; //The node is the other wall, ie we want the node to be the 'inside'.
            newVent.roomNode = newVent.wall.node;
            newVent.debugNode = newVent.node.position;
            newVent.debugRoomNode = newVent.roomNode.position;

            //Remove valid space from opposite the vent
            if (building.validVentSpace.ContainsKey(validCoords[chosenIndex]))
            {
                building.validVentSpace.Remove(validCoords[chosenIndex]);
            }

            //Change wall preset (needed so multiple vents are not placed here)
            if (ventType == NewAddress.AirVent.wallLower)
            {
                if (newVent.wall.preset.sectionClass == DoorPairPreset.WallSectionClass.wall)
                {
                    newVent.wall.SetDoorPairPreset(InteriorControls.Instance.wallVentLower, false);
                }

                //Block the opposite vent space from becoming a duct
                Vector3Int oppSpace = new Vector3Int(newVent.wall.node.nodeCoord.x, newVent.wall.node.nodeCoord.y, newVent.wall.node.nodeCoord.z * 3 + 1);
                if (building.validVentSpace.ContainsKey(oppSpace)) building.validVentSpace.Remove(oppSpace);
            }
            else if (ventType == NewAddress.AirVent.wallUpper)
            {
                if (newVent.wall.preset.sectionClass == DoorPairPreset.WallSectionClass.wall)
                {
                    newVent.wall.SetDoorPairPreset(InteriorControls.Instance.wallVentUpper, false);
                }

                //Block the opposite vent space from becoming a duct
                Vector3Int oppSpace = new Vector3Int(newVent.wall.node.nodeCoord.x, newVent.wall.node.nodeCoord.y, newVent.wall.node.nodeCoord.z * 3 + 0);
                if (building.validVentSpace.ContainsKey(oppSpace)) building.validVentSpace.Remove(oppSpace);
            }
        }

        //Add to reference list
        if (newVent != null)
        {
            airVents.Add(newVent);
            LoadVent(newVent);
            return true;
        }
        else return false;
    }

    //Load a vent
    private void LoadVent(AirDuctGroup.AirVent vent)
    {
        if (vent.ventType == NewAddress.AirVent.ceiling)
        {
            vent.node.SetCeilingVent(true);
        }
        else if (vent.ventType == NewAddress.AirVent.wallLower && vent.wall.parentWall != null)
        {
            vent.wall.SetDoorPairPreset(InteriorControls.Instance.wallVentLower);
        }
        else if (vent.ventType == NewAddress.AirVent.wallUpper && vent.wall.parentWall != null)
        {
            vent.wall.SetDoorPairPreset(InteriorControls.Instance.wallVentUpper);
        }
    }

    //Add a duct group
    public void AddDuctGroup(AirDuctGroup newGroup)
    {
        if (!ductGroups.Contains(newGroup))
        {
            ductGroups.Add(newGroup);
        }
    }

    //Add owner
    public void AddOwner(Human newOwner)
    {
        if (!belongsTo.Contains(newOwner))
        {
            //Add new ownership class reference...
            if (!gameLocation.thisAsAddress.roomsBelongTo.ContainsKey(preset.roomType))
            {
                gameLocation.thisAsAddress.roomsBelongTo.Add(preset.roomType, new Dictionary<NewRoom, List<Human>>());
            }

            //Add new furniture reference...
            if (!gameLocation.thisAsAddress.roomsBelongTo[preset.roomType].ContainsKey(this))
            {
                gameLocation.thisAsAddress.roomsBelongTo[preset.roomType].Add(this, new List<Human>());
            }

            //Assign owners
            gameLocation.thisAsAddress.roomsBelongTo[preset.roomType][this].Add(newOwner);

            belongsTo.Add(newOwner);

            //if (passcode != null && passcode.digits.Count > 0)
            //{
            //    if (!newOwner.dataSource.ContainsKey("roompassword_" + preset.name.ToLower()))
            //    {
            //        EvidenceLinkData pw = new EvidenceLinkData(passcode.digits[0].ToString() + passcode.digits[1].ToString() + passcode.digits[2].ToString() + passcode.digits[3].ToString());
            //        newOwner.dataSource.Add("roompassword_" + preset.name.ToLower(), pw);
            //    }
            //}

            //Game.Log(name + " in " + gameLocation.name + " add owner " + newOwner.name);
        }
    }

    //Load owners
    public void LoadOwners()
    {
        //Load room owners
        foreach (int loadCit in loadBelongsTo)
        {
            if (loadCit > -1)
            {
                Human o = null;

                if(CityData.Instance.GetHuman(loadCit, out o))
                {
                    AddOwner(o);
                }
            }
            //else
            //{
            //    AddOwner(Player.Instance);
            //}
        }

        //Load furntiture owners
        foreach (FurnitureLocation furn in individualFurniture)
        {
            //if (!furn.createdInteractables)
            //{
            //    Game.Log("Create interactables!"); 
            //    furn.CreateInteractables();
            //}

            foreach (int loadCit in furn.loadOwners)
            {
                if (loadCit > 0)
                {
                    Human o = null;

                    if(CityData.Instance.GetHuman(loadCit, out o))
                    {
                        furn.AssignOwner(o, false);
                    }
                }
                else if(loadCit < 0)
                {
                    furn.AssignOwner(CityData.Instance.addressDictionary[-loadCit], false);
                }
            }

            //New: Do this after so owners are loaded correctly
            if (!furn.createdInteractables)
            {
                furn.CreateInteractables();
            }
            else if (furn.loadOwners.Count > 0) furn.UpdateIntegratedObjectOwnership();
        }
    }

    //Pick password: Pick a password, after all possible ones completed
    public void PickPassword()
    {
        //If random is chosen, make sure to leave a note...
        if (passcode.used)
        {
            //Game.Log(name + " in " + gameLocation.name + " room password is used...");

            HashSet<NewRoom> avoidRooms = null;
            avoidRooms = new HashSet<NewRoom>();
            avoidRooms.Add(this);

            if (belongsTo != null && belongsTo.Count > 0)
            {
                bool addressNote = false;
                passcode.digits.Clear();
                passcode.digits.AddRange(belongsTo[0].passcode.digits); //Copy password from citizen.

                if (!addressNote)
                {
                    //Place a password note in an owned place
                    float chanceOfOwnedPlacement = 0.5f;

                    //If forgetful, 100%
                    if (belongsTo[0].characterTraits.Exists(item => item.trait.name == "Quirk-Forgetful"))
                    {
                        chanceOfOwnedPlacement = 1f;
                    }

                    if (Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, seed) <= chanceOfOwnedPlacement)
                    {
                        Interactable newNote1 = belongsTo[0].WriteNote(Human.NoteObject.note, "65a356f3-72da-4378-b0c5-9d6ad2f6de98", belongsTo[0], gameLocation, 3, InteractablePreset.OwnedPlacementRule.ownedOnly, 3, null, printDebug: false);

                        if (newNote1 != null)
                        {
                            passcode.notes.Add(newNote1.id);
                        }
                        else
                        {
                            Game.Log("Error placing note outside of room! Trying again... " + this.name);

                            newNote1 = belongsTo[0].WriteNote(Human.NoteObject.note, "65a356f3-72da-4378-b0c5-9d6ad2f6de98", belongsTo[0], gameLocation, 3, InteractablePreset.OwnedPlacementRule.ownedOnly, 3, avoidRooms, printDebug: false);

                            if (newNote1 != null)
                            {
                                passcode.notes.Add(newNote1.id);
                            }
                        }
                    }

                    //Add a password note to either the physical word or vmails...
                    bool placedPasswordNote = false;
                    List<string> possibleNotes = new List<string>();
                    List<InteractablePreset.OwnedPlacementRule> correspondingOwnedSetting = new List<InteractablePreset.OwnedPlacementRule>();
                    List<Human> correspondingRecievers = new List<Human>();

                    //Add all employees note...
                    if (gameLocation.thisAsAddress != null && gameLocation.thisAsAddress.company != null)
                    {
                        possibleNotes.Add("5012b526-cfb0-4ff3-b39c-4effb810e86a");
                        correspondingOwnedSetting.Add(InteractablePreset.OwnedPlacementRule.nonOwnedOnly);
                        correspondingRecievers.Add(belongsTo[0]);
                    }

                    //Add a possible receptionist note...
                    if (gameLocation.thisAsAddress != null && gameLocation.thisAsAddress.company != null && gameLocation.thisAsAddress.company.receptionist != null)
                    {
                        possibleNotes.Add("85e316f2-528c-4354-a620-24992f8cc9e2");
                        correspondingOwnedSetting.Add(InteractablePreset.OwnedPlacementRule.ownedOnly);
                        correspondingRecievers.Add(gameLocation.thisAsAddress.company.receptionist);
                    }

                    //Add a possible janitor note...
                    if (gameLocation.thisAsAddress != null && gameLocation.thisAsAddress.company != null && gameLocation.thisAsAddress.company.janitor != null)
                    {
                        possibleNotes.Add("d9e2526d-37f8-4e9e-9da9-a68f54065dd0");
                        correspondingOwnedSetting.Add(InteractablePreset.OwnedPlacementRule.ownedOnly);
                        correspondingRecievers.Add(gameLocation.thisAsAddress.company.janitor);
                    }

                    while (!placedPasswordNote && possibleNotes.Count > 0)
                    {
                        int chosen = Toolbox.Instance.GetPsuedoRandomNumber(0, possibleNotes.Count, seed);

                        //Place a reminder note in the main office
                        Interactable newNote2 = belongsTo[0].WriteNote(Human.NoteObject.note, possibleNotes[chosen], correspondingRecievers[chosen], gameLocation, 0, correspondingOwnedSetting[chosen], 0, avoidRooms, printDebug: false);

                        if (newNote2 != null)
                        {
                            passcode.notes.Add(newNote2.id);
                            placedPasswordNote = true;
                            break;
                        }
                        else
                        {
                            Game.Log("Error placing note outside of room! Trying again... " + this.name);
                            newNote2 = belongsTo[0].WriteNote(Human.NoteObject.note, possibleNotes[chosen], correspondingRecievers[chosen], gameLocation, 0, correspondingOwnedSetting[chosen], 0, avoidRooms, printDebug: false);

                            if (newNote2 != null)
                            {
                                passcode.notes.Add(newNote2.id);
                                placedPasswordNote = true;
                                break;
                            }
                        }

                        possibleNotes.RemoveAt(chosen);
                        correspondingOwnedSetting.RemoveAt(chosen);
                        correspondingRecievers.RemoveAt(chosen);
                    }
                }
            }
            else
            {
                Game.Log("CityGen: Cannot pick a password for room that belongs to nobody: " + preset.name);
            }
        }
    }

    //Setup environment
    public void SetupEnvrionment()
    {
        //if (preset.environmentProfile != null && environmentalContainer == null)
        //{
        //    //Create a collider that covers the whole room
        //    environmentalContainer = new GameObject();
        //    environmentalContainer.name = "EnvironmentalContainer";
        //    environmentalContainer.transform.SetParent(this.transform);
        //    environmentalContainer.transform.localPosition = Vector3.zero;

        //    //Loop each node, create a box collider for each
        //    foreach (NewNode node in nodes)
        //    {
        //        BoxCollider newColl = environmentalContainer.AddComponent<BoxCollider>();
        //        newColl.isTrigger = true;
        //        newColl.size = new Vector3(PathFinder.Instance.nodeSize.x, (node.floor.defaultCeilingHeight * 0.1f) - (node.floorHeight * 0.1f), PathFinder.Instance.nodeSize.y);
        //        newColl.center = environmentalContainer.transform.InverseTransformPoint(node.position) + new Vector3(0, newColl.size.y * 0.5f, 0);
        //    }

        //    //Apply a volume
        //    environmentVolume = environmentalContainer.gameObject.AddComponent<Volume>();
        //    environmentVolume.priority = 3; //Higher than building
        //    environmentVolume.blendDistance = 1f;
        //    environmentVolume.profile = preset.environmentProfile;
        //}

        //Add room to ambient zone reference...
        if (preset.ambientZone != null)
        {
            if (AudioController.Instance.ambientZoneReference.TryGetValue(preset.ambientZone, out _))
            {
                AudioController.Instance.ambientZoneReference[preset.ambientZone].rooms.Add(this);
            }
            else
            {
                Game.LogError("Ambient zone " + preset.ambientZone.name + " not loaded!");
            }
        }
    }

    //Set exploration level
    public void SetExplorationLevel(int newLevel)
    {
        if (newLevel != explorationLevel)
        {
            int moneyForLoc = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.moneyForLocations));

            if (newLevel > explorationLevel && explorationLevel <= 0 && moneyForLoc > 0f)
            {
                GameplayController.Instance.AddMoney(moneyForLoc, false, "moneyforblueprints");
            }

            explorationLevel = newLevel;

            //Tie keys
            if(explorationLevel >= 1)
            {
                if(gameLocation.evidenceEntry != null)
                {
                    gameLocation.evidenceEntry.OnPlayerArrival();
                }
            }

            //Display doors
            foreach (RectTransform rectT in mapDoors)
            {
                if (explorationLevel >= 1)
                {
                    if (!rectT.gameObject.activeSelf)
                    {
                        rectT.gameObject.SetActive(true);
                    }
                }
                else
                {
                    if (rectT.gameObject.activeSelf)
                    {
                        rectT.gameObject.SetActive(false);
                    }
                }
            }

            //Set exploration level of rooms joined by divider/open doorway
            foreach (NewNode.NodeAccess acc in entrances)
            {
                if (acc.accessType == NewNode.NodeAccess.AccessType.adjacent || acc.accessType == NewNode.NodeAccess.AccessType.bannister || acc.accessType == NewNode.NodeAccess.AccessType.openDoorway || acc.accessType == NewNode.NodeAccess.AccessType.streetToStreet)
                {
                    NewRoom otherRoom = acc.GetOtherRoom(this);

                    if (otherRoom.explorationLevel < explorationLevel)
                    {
                        otherRoom.SetExplorationLevel(explorationLevel);
                    }
                }
            }

            //Update duct maps if ducts are visible here
            if (explorationLevel >= 2)
            {
                foreach (NewNode node in nodes)
                {
                    foreach (AirDuctGroup.AirDuctSection duct in node.airDucts)
                    {
                        //Duct is below ceiling level
                        if (duct.level < 2)
                        {
                            duct.SetDiscovered(true);
                        }
                    }
                }

                foreach (AirDuctGroup.AirVent vent in airVents)
                {
                    vent.SetDiscovered(true);
                }
            }

            //Update duct map if there are vents in this room
            if (airVents.Count > 0)
            {
                MapController.Instance.AddDuctUpdateCall(floor.mapDucts);
            }

            if (gameLocation.mapButton != null)
            {
                MapController.Instance.AddUpdateCall(gameLocation.mapButton);
            }
        }
    }

    //Should dynamic shadows be updated?
    public bool TestForDynamicShadowsUpdate()
    {
        //Search for moving actors
        if (actorUpdate)
        {
            foreach (Human h in currentOccupants)
            {
                if (h.isPlayer) continue; //Ignore player
                else if (h.isAsleep) continue; //Ignore sleeping
                else if (h.isDead) continue; //Ignore dead

                return true;
            }
        }

        //Search for animating doors
        foreach (NewNode.NodeAccess acc in entrances)
        {
            if (acc.door != null)
            {
                if (acc.door.animating) return true;
            }
        }

        //Search for physics objects
        foreach (Interactable i in worldObjects)
        {
            if (i.controller != null)
            {
                //Look for active movement
                if (i.controller.physicsOn)
                {
                    return true;
                }
            }
        }

        //Search for animating things
        foreach (NewNode n in nodes)
        {
            foreach (Interactable i in n.interactables)
            {
                if (i.controller != null)
                {
                    if (i.controller.doorMovement != null)
                    {
                        if (i.controller.doorMovement.isAnimating)
                        {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    ////Update light shadows
    //public void UpdateShadowMaps()
    //{
    //    foreach (Interactable lightI in mainLights)
    //    {
    //        if (!lightI.lightController.isOn) continue; //Skip if off

    //        if (lightI.lightController.useShadows && lightI.lightController.preset.shadowMode == LightingPreset.ShadowMode.dynamicSystem)
    //        {
    //            lightI.lightController.hdrpLightData.RequestShadowMapRendering();
    //            Game.Instance.lastShadowsUpdatedCount++;
    //        }
    //    }

    //    foreach (Interactable lightI in secondaryLights)
    //    {
    //        if (lightI.lightController == null) Game.Log("No light controller for " + lightI.preset.name + " please check config/preset");
    //        if (!lightI.lightController.isOn) continue; //Skip if off

    //        if (lightI.lightController.useShadows && lightI.lightController.preset.shadowMode == LightingPreset.ShadowMode.dynamicSystem)
    //        {
    //            lightI.lightController.hdrpLightData.RequestShadowMapRendering();
    //            Game.Instance.lastShadowsUpdatedCount++;
    //        }
    //    }
    //}

    //Default comparer (cycle priority)
    public int CompareTo(NewRoom otherObject)
    {
        return this.roomType.cyclePriority.CompareTo(otherObject.roomType.cyclePriority);
    }

    public CitySaveData.RoomCitySave GenerateSaveData()
    {
        CitySaveData.RoomCitySave output = new CitySaveData.RoomCitySave();

        output.name = name;

        //Get nodes
        foreach (NewNode node in nodes)
        {
            output.nodes.Add(node.GenerateSaveData());
        }

        foreach (RoomConfiguration con in openPlanElements)
        {
            output.openPlanElements.Add(con.name);
        }

        foreach (NewRoom room in commonRooms)
        {
            output.commonRooms.Add(room.roomID);
        }

        foreach (AirDuctGroup.AirVent vent in airVents)
        {
            CitySaveData.AirVentSave newVent = new CitySaveData.AirVentSave { ventType = vent.ventType, id = vent.ventID };
            if (vent.node != null) newVent.node = vent.node.nodeCoord;
            if (vent.wall != null) newVent.wall = vent.wall.id;
            if (vent.roomNode != null) newVent.rNode = vent.roomNode.nodeCoord;
            output.airVents.Add(newVent);
        }

        //Save light zones
        foreach (LightZoneData lzd in lightZones)
        {
            CitySaveData.LightZoneSave newZone = new CitySaveData.LightZoneSave();

            foreach (NewNode n in lzd.nodeList)
            {
                newZone.n.Add(n.nodeCoord);
            }

            newZone.areaLightBright = lzd.areaLightBrightness;
            newZone.areaLightColour = lzd.areaLightColour;

            output.lightZones.Add(newZone);
        }

        output.id = roomID;
        output.fID = furnitureAssignID;
        output.iID = interactableAssignID;
        output.floorID = roomFloorID;
        output.preset = preset.name;
        output.reachableFromEntrance = reachableFromEntrance; //Used in address generation to heck if this room can be accessed
        output.isOutsideWindow = isOutsideWindow; //If true this room is the outside area outside a building
        output.allowCoving = allowCoving; //If true this room will feature coving
        output.floorMaterial = floorMaterial.name;
        output.floorMatKey = floorMatKey;
        output.ceilingMaterial = ceilingMaterial.name;
        output.ceilingMatKey = ceilingMatKey;
        output.defaultWallMaterial = defaultWallMaterial.name;
        output.defaultWallKey = defaultWallKey;
        output.miscKey = miscKey;
        if (colourScheme != null) output.colourScheme = colourScheme.name;
        if (mainLightPreset != null) output.mainLightPreset = mainLightPreset.name; //Chosen main light preset
        output.isBaseNullRoom = isBaseNullRoom;
        output.middle = middleRoomPosition;
        output.password = passcode;
        output.cf = ceilingFans;

        //Save owners
        foreach (Human human in belongsTo)
        {
            output.owners.Add(human.humanID);
        }

        //Save furniture
        foreach (FurnitureClusterLocation furn in furniture)
        {
            output.f.Add(furn.GenerateSaveData());
        }

        //Save cull tree
        foreach (KeyValuePair<NewRoom, List<CullTreeEntry>> pair in cullingTree)
        {
            foreach (CullTreeEntry c in pair.Value)
            {
                CitySaveData.CullTreeSave cullSave = new CitySaveData.CullTreeSave { r = pair.Key.roomID };

                //Save doors as wall IDs
                cullSave.d = c.requiredOpenDoors;
                output.cullTree.Add(cullSave);
            }
        }

        //Save above, below adjacent, non audio-occluded
        foreach (NewRoom r in aboveRooms)
        {
            output.above.Add(r.roomID);
        }

        foreach (NewRoom r in belowRooms)
        {
            output.below.Add(r.roomID);
        }

        foreach (NewRoom r in adjacentRooms)
        {
            output.adj.Add(r.roomID);
        }

        foreach (NewRoom r in nonAudioOccludedRooms)
        {
            output.occ.Add(r.roomID);
        }

        return output;
    }

    //Get a random node from this room
    public NewNode GetRandomNode()
    {
        System.Random randomizer = new System.Random();
        NewNode[] asArray = nodes.ToArray();
        return asArray[randomizer.Next(asArray.Length)];
    }

    //Get entrance to this room
    public NewNode GetRandomEntranceNode()
    {
        NewNode.NodeAccess r = null;

        if (entrances.Count > 0) r = entrances[Toolbox.Instance.Rand(0, entrances.Count)];
        else return GetRandomNode(); //If no entrances, get a random node instead

        return r.fromNode;
    }

    //Get whether this is currently forbidden for an actor...
    public bool IsAccessAllowed(Human human)
    {
        string debug;
        bool ret = !human.IsTrespassing(this, out _, out debug); //I think these two are functionally the same?? If so, remove below...
        if(Game.Instance.collectDebugData) Game.Log("Player: " + debug);

        return ret;
    }

    //Remove all furniture & items
    public void RemoveAllInhabitantFurniture(bool removeSkipAddressInhabitantsFurniture, FurnitureClusterLocation.RemoveInteractablesOption spawnedOnFurnitureRemovalOption)
    {
        List<FurnitureClusterLocation> allFurn = new List<FurnitureClusterLocation>(furniture);

        foreach (FurnitureClusterLocation furn in allFurn)
        {
            if(removeSkipAddressInhabitantsFurniture || !furn.cluster.skipIfNoAddressInhabitants)
            {
                furn.DeleteCluster(true, spawnedOnFurnitureRemovalOption);
                furn.UnloadFurniture(true, spawnedOnFurnitureRemovalOption);

                furniture.Remove(furn);
            }
        }

        foreach (NewNode node in nodes)
        {
            List<Interactable> worldInteractables = node.interactables.FindAll(item => item.wo && !mainLights.Contains(item));

            for (int i = 0; i < worldInteractables.Count; i++)
            {
                if (PlayerApartmentController.Instance.itemStorage.Contains(worldInteractables[i])) continue; //Don't remove if in storage

                //Remove world objects
                worldInteractables[i].SafeDelete(false);
            }

            node.SetAllowNewFurniture(true);
            node.noPassThrough = false;

            foreach (NewWall wall in node.walls)
            {
                //wall.individualFurniture.Clear();
                wall.placedWallFurn = false;

                if (wall.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance && !wall.preset.divider)
                {
                    node.SetAllowNewFurniture(false);
                }
            }

            //Remove null entries
            for (int i = 0; i < node.interactables.Count; i++)
            {
                if(node.interactables[i] == null)
                {
                    node.interactables.RemoveAt(i);
                    i--;
                }
            }

            //Reset overwrite limit
            node.overwriteLimit = 999;
        }

        blockedAccess = new Dictionary<NewNode, List<NewNode>>();
        //actionReference = new Dictionary<AIActionPreset, List<Interactable>>();
    }

    //Set steam on or off
    public void SetSteam(bool val)
    {
        //If on, set bool to on
        if (val)
        {
            if (!steamOn)
            {
                steamOn = true;
                steamLastSwitched = SessionData.Instance.gameTime;
                if (steamController != null) steamController.SteamStateChanged();
            }
        }
        //If off, all others must be off too...
        else
        {
            if (!steamControllingInteractables.Exists(item => item.sw0))
            {
                if (steamOn)
                {
                    steamOn = false;
                    steamLastSwitched = SessionData.Instance.gameTime;
                    if (steamController != null) steamController.SteamStateChanged();
                }
            }
        }
    }

    //Return if this room is classed as outside
    public bool IsOutside()
    {
        if(preset != null)
        {
            if (preset.forceOutside == RoomConfiguration.OutsideSetting.forceInside) return false;
            else if (preset.forceOutside == RoomConfiguration.OutsideSetting.forceOutside) return true;
        }

        if(gameLocation.IsOutside())
        {
            if (isOutsideWindow) return true;
        }

        return false;
    }

    [Button]
    public void DisplaySublocations()
    {
        if (sublocationParent != null) Destroy(sublocationParent);

        sublocationParent = new GameObject();
        sublocationParent.transform.SetParent(this.transform);
        sublocationParent.transform.localPosition = Vector3.zero;

        foreach (NewNode node in nodes)
        {
            foreach (KeyValuePair<Vector3, NewNode.NodeSpace> pair in node.walkableNodeSpace)
            {
                GameObject newWalkable = Instantiate(PrefabControls.Instance.walkPointSphere, sublocationParent.transform);
                newWalkable.transform.position = pair.Value.position;
                if ((Game.Instance.devMode && Game.Instance.collectDebugData)) sublocationDebugObjects.Add(newWalkable);

                //Setup with corresponding space
                DebugWalkableSublocation sl = newWalkable.GetComponent<DebugWalkableSublocation>();
                sl.Setup(node, pair.Value);
            }

            foreach (Interactable i in node.interactables)
            {
                if (i.usagePoint != null)
                {
                    GameObject newUsage = Instantiate(PrefabControls.Instance.usePointSphere, sublocationParent.transform);
                    newUsage.name = i.name;

                    Vector3 usagePosition = i.usagePoint.GetUsageWorldPosition(node.position, null);
                    newUsage.transform.position = new Vector3(usagePosition.x, i.wPos.y, usagePosition.z);

                    newUsage.transform.rotation = Quaternion.LookRotation(i.usagePoint.worldLookAtPoint - newUsage.transform.position, Vector3.up);

                    if ((Game.Instance.devMode && Game.Instance.collectDebugData)) sublocationDebugObjects.Add(newUsage);
                }
            }
        }
    }

    [Button]
    public void RemoveSublocationsDisplay()
    {
        if (sublocationParent != null) Destroy(sublocationParent);
    }

    //Debug teleport player to location
    [Button("Teleport Player")]
    public void DebugTeleportPlayerToLocation()
    {
        System.Random randomizer = new System.Random();
        NewNode[] asArray = nodes.ToArray();
        NewNode randomLine = asArray[randomizer.Next(asArray.Length)];
        Player.Instance.Teleport(randomLine, null);
    }

    [Button]
    public void DebugCullingDisplay()
    {
        while (spawnPathDebug.Count > 0)
        {
            Toolbox.Instance.DestroyObject(spawnPathDebug[0].gameObject);
            spawnPathDebug.RemoveAt(0);
        }

        GenerateCullingTree(true);
    }

    [Button]
    public void GetMainLightData()
    {
        mainLightObjects.Clear();

        foreach (Interactable i in mainLights)
        {
            if (i.controller != null)
            {
                mainLightObjects.Add(i.controller);
            }
        }
    }

    [Button]
    public void ToggleExteriorWindowDebug()
    {
        if (exteriorWindowDebug.Count > 0)
        {
            while (exteriorWindowDebug.Count > 0)
            {
                Destroy(exteriorWindowDebug[0]);
                exteriorWindowDebug.RemoveAt(0);
            }
        }
        else
        {


            foreach (NewWall window in windowsWithUVData)
            {
                Vector3 localPos = (window.windowUV.localMeshPositionLeft + window.windowUV.localMeshPositionRight) / 2f;

                GameObject windowDebugObject = Instantiate(PrefabControls.Instance.pathfindNodeDebug, building.buildingModelBase.transform);
                windowDebugObject.transform.localPosition = (window.windowUV.localMeshPositionLeft + window.windowUV.localMeshPositionRight) / 2f;

                windowDebugObject.transform.SetParent(this.transform, true);

                windowDebugObject.name = "wHP: " + window.windowUVHorizonalPosition + " floor: " + window.windowUV.floor + " side: " + window.windowUV.side + " horz: " + window.windowUV.horizonal;

                exteriorWindowDebug.Add(windowDebugObject);
            }
        }
    }

    [Button]
    public void TestFurniturePlacementBlockingCheck()
    {
        Dictionary<NewNode, List<NewNode>> newTest = new Dictionary<NewNode, List<NewNode>>();
        List<NewNode> t = new List<NewNode>();
        List<string> output = new List<string>();
        Game.Log(GenerationController.Instance.IsFurniturePlacementValid(this, ref newTest, ref t, ref t, true, out output));

        foreach(string str in output)
        {
            Game.Log(str);
        }
    }

    [Button("Test Furniture Placement Blocking Check (Ignore No Passthrough)")]
    public void TestFurniturePlacementBlockingCheckIgnoreNoPassthrough()
    {
        Dictionary<NewNode, List<NewNode>> newTest = new Dictionary<NewNode, List<NewNode>>();
        List<NewNode> t = new List<NewNode>();
        List<string> output = new List<string>();
        Game.Log(GenerationController.Instance.IsFurniturePlacementValid(this, ref newTest, ref t, ref t, true, out output, ignoreNoPassThrough: true));

        foreach (string str in output)
        {
            Game.Log(str);
        }
    }

    [Button]
    public void DisplayNodePositions()
    {
        foreach(NewNode node in nodes)
        {
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.SetParent(this.transform, true);

            DebugNode nodeD = cube.AddComponent<DebugNode>();
            nodeD.Setup(node);

            cube.transform.position = node.position;
            cube.name = node.nodeCoord.ToString() + ": Tile: " + node.tile.globalTileCoord + " " + node.floorType.ToString() + " Use opt ceiling: " + node.tile.useOptimizedCeiling;
            nodeDebug.Add(cube);
        }
    }

    [Button]
    public void RemoveNodePositions()
    {
        while(nodeDebug.Count > 0)
        {
            Destroy(nodeDebug[0]);
            nodeDebug.RemoveAt(0);
        }
    }

    public int GetWallCount()
    {
        int ret = 0;

        foreach(NewNode n in nodes)
        {
            ret += n.walls.Count;
        }

        return ret;
    }

    [Button]
    public void GetAIActions()
    {
        foreach(KeyValuePair<AIActionPreset, List<Interactable>> pair in actionReference)
        {
            Game.Log(pair.Key.name + " (" + pair.Value.Count + ")");
        }
    }

    [Button]
    public void GetInteractables()
    {
        foreach(NewNode n in nodes)
        {
            foreach(Interactable i in n.interactables)
            {
                Game.Log(i.name + " (" + i.id + ")");
            }
        }
    }

    [Button]
    public void ListContainedInteractables()
    {
        foreach (NewNode node in nodes)
        {
            for (int i = 0; i < node.interactables.Count; i++)
            {
                Game.Log("Contained in " + name + ": " + node.interactables[i].name);
            }
        }
    }

    [Button]
    public void ListActionReferences()
    {
        foreach(KeyValuePair<AIActionPreset, List<Interactable>> pair in actionReference)
        {
            foreach(Interactable i in pair.Value)
            {
                Game.Log(pair.Key.ToString() + ": " + i.GetName() + " " + i.id + " at " + i.GetWorldPosition());
            }
        }
    }

    public SessionData.SceneProfile GetEnvironment()
    {
        if(!SessionData.Instance.isFloorEdit)
        {
            if(preset != null && preset.overrideAddressEnvironment)
            {
                if(defaultWallKey.grubiness > 0.5f)
                {
                    return preset.sceneDirty;
                }
                else return preset.sceneClean;
            }
            else if(gameLocation != null && gameLocation.thisAsAddress != null && gameLocation.thisAsAddress.addressPreset != null && gameLocation.thisAsAddress.addressPreset.overrideBuildingEnvironment)
            {
                return gameLocation.thisAsAddress.addressPreset.sceneProfile;
            }
            else if(building != null && building.preset.overrideDistrictEnvironment)
            {
                return building.preset.sceneProfile;
            }
            else if(gameLocation != null && gameLocation.district != null)
            {
                return gameLocation.district.preset.sceneProfile;
            }
        }

        return SessionData.SceneProfile.outdoors;
    }

    public void AddGas(float amount)
    {
        gasLevel += amount;
        gasLevel = Mathf.Clamp01(gasLevel);
        //Game.Log("Gameplay: " + name + " gas level: " + gasLevel);

        if(gasLevel > 0f && !GameplayController.Instance.gasRooms.Contains(this))
        {
            GameplayController.Instance.gasRooms.Add(this);
        }
        else if(gasLevel <= 0f)
        {
            GameplayController.Instance.gasRooms.Remove(this);
        }

        //Allow a break between gas releases
        if(gasLevel >= 0.95f)
        {
            lastRoomGassed = SessionData.Instance.gameTime;
        }
    }

    [Button]
    public void RebuildCullingTree()
    {
        completedTreeCull = false;

        while (spawnPathDebug.Count > 0)
        {
            Toolbox.Instance.DestroyObject(spawnPathDebug[0].gameObject);
            spawnPathDebug.RemoveAt(0);
        }

        Game.Log("Build culling tree for " + name);
        GenerateCullingTree(true);
        Player.Instance.UpdateCullingOnEndOfFrame();
    }

    [Button]
    public void IsThisOutside()
    {
        Game.Log(IsOutside());
    }

    [Button]
    public void SpawnModularRoomElements()
    {
        //Spawn all combinable bits
        MeshPoolingController.Instance.SpawnModularRoomElements(this, true, out _, out _, out _, out _);
    }
}
