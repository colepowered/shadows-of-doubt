﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;

//Use a class for called-upon timing info so child memories can use a reference to it for ease of use
public class EventTime
{
    public TimelineEvent parentMemory;

    public bool forcedAccuracy = false; //Flag that forced accuracy to the below
    public int forcedAccuracyToMinutes = 0; //Forced accuracy to this many minutes

    public bool forcedRange = false; //Flag that forces this to be a time range
    public Vector2 forcedTimeRange = Vector2.zero; //Forced time range

    public float timeStart = 0f; //The rounded called upon start time, calculated based on memory accuracy
    public float timeEnd = 0f; //The rounded called upon end time, calculated based on memory accuracy
    public float timeMidPoint = 0f; //The mid point between the two boundaries
    public Vector2 timeRange; //Time range of the above in Vector2 form
    public string accurateString = string.Empty; //Time string for an accurate time (likely used for debug)
    public string startString = string.Empty; //Time string for above
    public string endString = string.Empty; //Time string for above
    public float roundedTo = 0.1f; //The called upon times have been rounded to this
    public enum RecallAccuracy { veryLow, low, med, high, veryHigh };
    public RecallAccuracy recallAccuracy; //A general description of the recalled accuracy

    //Events
    //Triggered when the called upon time is updated
    public delegate void OnCalledUponTimeUpdate();
    public event OnCalledUponTimeUpdate OnCalledUponTimeUpdated;

    public EventTime(TimelineEvent newParent, bool forceAccuracy = false, int forceAccuracyToMinutes = 0, bool forceRange = false, float forcedFrom = 0f, float forcedTo = 0f)
    {
        parentMemory = newParent;
        forcedAccuracy = forceAccuracy;
        forcedAccuracyToMinutes = forceAccuracyToMinutes;
        forcedRange = forceRange;
        forcedTimeRange = new Vector2(forcedFrom, forcedTo);

        CalculateTimings();

        //Listen for accuracy updates
        if(!forceAccuracy && !forceRange)
        {
            parentMemory.OnRecallAccuracyChange += CalculateTimings;
        }
    }

    //Calculate/recalculate timings
    public void CalculateTimings()
    {
        //Force time range
        if(forcedRange)
        {
            roundedTo = (float)5 / 60f;
            timeStart = forcedTimeRange.x;
            timeEnd = forcedTimeRange.y;
        }
        //Force accuracy- round to this
        else if (forcedAccuracy)
        {
            roundedTo = (float)forcedAccuracyToMinutes / 60f;
            timeStart = Mathf.FloorToInt(parentMemory.happenedAt / roundedTo) * roundedTo;
            timeEnd = timeStart + roundedTo;
        }
        //If not, use the parent event accuracy
        else
        {
            //If you use a rounded floor value, the output will be the from time, add the rounded to get the to time
            if (parentMemory.timeAccuracy > 0.8f)
            {
                //Round to 5 mins
                recallAccuracy = EventTime.RecallAccuracy.veryHigh;
                roundedTo = SocialControls.Instance.accuracy1 / 60f;
                timeStart = Mathf.FloorToInt(parentMemory.happenedAt / roundedTo) * roundedTo;
                timeEnd = timeStart + roundedTo;
            }
            else if (parentMemory.timeAccuracy > 0.6f && parentMemory.timeAccuracy <= 0.8f)
            {
                //Round to 10 mins
                recallAccuracy = EventTime.RecallAccuracy.high;
                roundedTo = SocialControls.Instance.accuracy2 / 60f;
                timeStart = Mathf.FloorToInt(parentMemory.happenedAt / roundedTo) * roundedTo;
                timeEnd = timeStart + roundedTo;
            }
            else if (parentMemory.timeAccuracy > 0.4f && parentMemory.timeAccuracy <= 0.6f)
            {
                //Round to 15 mins
                recallAccuracy = EventTime.RecallAccuracy.med;
                roundedTo = SocialControls.Instance.accuracy3 / 60f;
                timeStart = Mathf.FloorToInt(parentMemory.happenedAt / roundedTo) * roundedTo;
                timeEnd = timeStart + roundedTo;
            }
            else if (parentMemory.timeAccuracy > 0.2f && parentMemory.timeAccuracy <= 0.4f)
            {
                //Round to 30 mins
                recallAccuracy = EventTime.RecallAccuracy.low;
                roundedTo = SocialControls.Instance.accuracy4 / 60f;
                timeStart = Mathf.FloorToInt(parentMemory.happenedAt / roundedTo) * roundedTo;
                timeEnd = timeStart + roundedTo;
            }
            else
            {
                //Round to 60 mins
                recallAccuracy = EventTime.RecallAccuracy.veryLow;
                roundedTo = SocialControls.Instance.accuracy5 / 60f;
                timeStart = Mathf.FloorToInt(parentMemory.happenedAt / roundedTo) * roundedTo;
                timeEnd = timeStart + roundedTo;
            }
        }

        //Create strings for time reference
        startString = SessionData.Instance.GameTimeToClock24String(timeStart, true);
        endString = SessionData.Instance.GameTimeToClock24String(timeEnd, true);
        if(parentMemory != null) accurateString = SessionData.Instance.GameTimeToClock24String(parentMemory.happenedAt, true);
        
        timeRange = new Vector2(timeStart, timeEnd);
        timeMidPoint = (timeEnd + timeStart) * 0.5f;

        //Fire event
        if (OnCalledUponTimeUpdated != null) OnCalledUponTimeUpdated();
    }
}
