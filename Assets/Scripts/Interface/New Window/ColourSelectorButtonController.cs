﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ColourSelectorButtonController : ButtonController, IPointerDownHandler, IPointerUpHandler
{
    public RectTransform selector;
    public List<Button> colourButtons = new List<Button>();
    public Color selectedColour = Color.white;

    public delegate void ChangeColour();
    public event ChangeColour OnChangeColour;

    public override void VisualUpdate()
    {
        base.VisualUpdate();

        background.color = selectedColour;
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        //Show selector
        selector.gameObject.SetActive(true);

        //Close tooltip
        if(tooltip != null)
        {
            tooltip.ForceClose();
        }
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        //Set to selected
        foreach(GameObject go in eventData.hovered)
        {
            Button hoverButton = go.GetComponent<Button>();

            if(hoverButton != null)
            {
                if(colourButtons.Contains(hoverButton))
                {
                    selectedColour = hoverButton.image.color;

                    //Play SFX
                    AudioController.Instance.Play2DSound(AudioControls.Instance.mapControlButton);

                    VisualUpdate();

                    //Fire event
                    if (OnChangeColour != null)
                    {
                        OnChangeColour();
                    }

                    return;
                }
            }
        }

        //Hide selector
        selector.gameObject.SetActive(false);
    }
}
