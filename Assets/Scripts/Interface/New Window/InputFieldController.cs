using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using TMPro;
using UnityEngine.Video;
using UnityEditor;
using UnityEngine.UI;

public class InputFieldController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public ButtonController inputNameButton;
    public ButtonController selectButton;
    public TextMeshProUGUI titleText;
    public Image checkbox;
    public Sprite tickSprite;
    public Sprite crossSprite;
    public Sprite emptySprite;
    public ProgressBarController progress;
    public RectTransform rewardedGraphic;

    [Header("State")]
    public bool resultsMode = false;
    public Case belongsToCase;
    public Case.ResolveQuestion question;
    [System.NonSerialized]
    public Evidence inputtedEvidence;
    public Color invalidInputColor = Color.red;
    public Color validInputColor = Color.white;

    public void Setup(Case.ResolveQuestion newQuestion, Case newCase, bool newResultsMode = false)
    {
        resultsMode = newResultsMode;
        belongsToCase = newCase;
        question = newQuestion;
        question.inputField = this;

        titleText.text = question.GetText(belongsToCase, true, true);
        progress.rect.gameObject.SetActive(false);

        Game.Log("Jobs: Setting up input field for " + question.name);

        string correct = string.Empty;

        for (int i = 0; i < question.correctAnswers.Count; i++)
        {
            string str = question.correctAnswers[i];

            if (question.inputType == Case.InputType.citizen)
            {
                int number = -1;
                int.TryParse(str, out number);

                Human h = CityData.Instance.citizenDirectory.Find(item => item.humanID == number);
                if (h != null) correct += h.GetCitizenName();
                if(i < question.correctAnswers.Count - 1) correct += ", ";
            }
            else if (question.inputType == Case.InputType.location)
            {
                int number = -1;
                int.TryParse(str, out number);

                NewGameLocation l = CityData.Instance.gameLocationDirectory.Find(item => (item.thisAsAddress != null && item.thisAsAddress.id == number) || (item.thisAsStreet != null && item.thisAsStreet.streetID == number));
                if (l != null) correct += l.name;
                if (i < question.correctAnswers.Count - 1) correct += ", ";
            }
            else if (question.inputType == Case.InputType.item)
            {
                Evidence ev = null;

                if(GameplayController.Instance.evidenceDictionary.TryGetValue(str, out ev))
                {
                    correct += ev.name;
                    if (i < question.correctAnswers.Count - 1) correct += ", ";
                }
            }
        }

        if(question.inputType == Case.InputType.revengeObjective)
        {
            inputNameButton.gameObject.SetActive(false);
            selectButton.gameObject.SetActive(false);

            RevengeObjective rev = question.GetRevengeObjective();

            if(rev != null)
            {
                if (rev.specialConditions.Contains(RevengeObjective.SpecialConditions.trackProgressFromAddressQuestion))
                {
                    progress.rect.gameObject.SetActive(true);
                    Game.Log("Jobs: Setting progress bar for " + question.name + ": " + question.progress);
                    progress.SetValue(question.progress);
                    question.OnProgressChange += ProgressChange;
                }
                else Game.Log("Jobs: Unable to get 'track progress from address question' in case revenge objective " + question.name);
            }
            else Game.Log("Jobs: Unable to get revenge objective " + question.name);
        }
        else if (question.inputType == Case.InputType.objective || question.inputType == Case.InputType.arrestPurp)
        {
            inputNameButton.gameObject.SetActive(false);
            selectButton.gameObject.SetActive(false);
        }
        else
        {
            //Remove input buttons if in results mode...
            if (resultsMode)
            {
                inputNameButton.SetInteractable(false);

                //selectButton.icon.gameObject.SetActive(true); //Enable tick/cross icon
                //selectButton.text.alignment = TextAlignmentOptions.Right;

                if(question.isCorrect)
                {
                    checkbox.sprite = tickSprite;
                    rewardedGraphic.gameObject.SetActive(true);

                    //selectButton.icon.sprite = tickSprite;
                    //selectButton.text.text = CityControls.Instance.cityCurrency + question.reward;
                    //selectButton.tooltip.mainDictionaryKey = "correct answer";
                }
                else
                {
                    checkbox.sprite = crossSprite;
                    rewardedGraphic.gameObject.SetActive(false);

                    //selectButton.icon.sprite = crossSprite;

                    if(question.penalty != 0)
                    {
                        //selectButton.text.text = "- " + CityControls.Instance.cityCurrency + question.penalty;
                        //selectButton.text.color = Color.red;
                    }
                    else
                    {
                        //selectButton.text.text = string.Empty;
                    }

                    if(!question.isOptional)
                    {
                        //selectButton.icon.color = Color.red;
                        //selectButton.text.color = Color.red;
                        //selectButton.tooltip.mainDictionaryKey = "incorrect answer";
                    }
                    else
                    {
                        if(belongsToCase.isSolved)
                        {
                            //Set tooltip to correct answer(s)
                            //selectButton.tooltip.useMainDictionaryEntry = false;
                            //selectButton.tooltip.mainText = Strings.Get("ui.tooltips", "incorrect answer") + ". " + Strings.Get("ui.tooltips", "correct reveal") + ": " + correct;
                        }
                        else
                        {
                            //selectButton.tooltip.mainDictionaryKey = "incorrect answer";
                        }
                    }
                }
            }
            else
            {
                selectButton.icon.gameObject.SetActive(false); //Disable tick/cross icon
                selectButton.text.text= Strings.Get("ui.interface", "Select from Board...");

                Game.Log("Chapter: New input field: " + titleText.text + " correct answer(s): " + correct);
            }
        }

        //Item can only be picked...
        if(question.inputType == Case.InputType.item)
        {
            inputNameButton.SetInteractable(false);

            //Load inputted interactable
            if(question.inputtedEvidence != null && question.inputtedEvidence.Length > 0)
            {
                Evidence ev = null;

                if(GameplayController.Instance.evidenceDictionary.TryGetValue(question.inputtedEvidence, out ev))
                {
                    SetSelectedEvidence(ev);
                }
            }
        }

        inputNameButton.text.text = question.input; //Load existing input

        OnInputEdited();
    }

    public void ProgressChange(Case.ResolveQuestion q)
    {
        Game.Log("Jobs: Setting progress bar for " + question.name + ": " + question.progress);
        progress.SetValue(question.progress);
    }

    public void OpenTextInputButton()
    {
        if(!resultsMode)
        {
            PopupMessageController.Instance.PopupMessage("input_" + question.inputType.ToString(), true, true, RButton: "Confirm", enableInputField: true);
            PopupMessageController.Instance.inputField.text = question.input;
            PopupMessageController.Instance.OnLeftButton += OnInputTextPopupCancel;
            PopupMessageController.Instance.OnRightButton += OnInputTextPopupConfirm;
        }
    }

    public void OnInputTextPopupCancel()
    {
        PopupMessageController.Instance.OnLeftButton -= OnInputTextPopupCancel;
        PopupMessageController.Instance.OnRightButton -= OnInputTextPopupConfirm;
    }

    public void OnInputTextPopupConfirm()
    {
        PopupMessageController.Instance.OnLeftButton -= OnInputTextPopupCancel;
        PopupMessageController.Instance.OnRightButton -= OnInputTextPopupConfirm;

        inputNameButton.text.text = PopupMessageController.Instance.inputField.text;
        OnInputEdited();
    }

    private void OnDestroy()
    {
        question.OnProgressChange -= ProgressChange;
        PopupMessageController.Instance.OnLeftButton -= OnInputTextPopupCancel;
        PopupMessageController.Instance.OnRightButton -= OnInputTextPopupConfirm;
    }

    public void OnInputEdited()
    {
        if(!resultsMode)
        {
            question.input = inputNameButton.text.text; //Save to class
            Game.Log("Chapter: Saving question input " + inputNameButton.text.text + " to question " + question.name);

            question.UpdateValid(belongsToCase);

            //UpdateCheckbox(); //This is included in above

            if (belongsToCase != null)
            {
                belongsToCase.ValidationCheck();

                if(belongsToCase.job != null)
                {
                    belongsToCase.job.HandleObjectiveProgress();
                }
            }

            question.UpdateCorrect(belongsToCase); //Update correct answer for question
        }
    }

    public void UpdateCheckbox()
    {
        if (question.isValid)
        {
            checkbox.sprite = tickSprite;
            inputNameButton.SetButtonBaseColour(validInputColor);
        }
        else
        {
            checkbox.sprite = emptySprite;
            inputNameButton.SetButtonBaseColour(invalidInputColor);
        }
    }

    public void OnSelectButton()
    {
        if(!resultsMode)
        {
            CasePanelController.Instance.SetPickModeActive(true, this);
        }
    }

    public void SetSelectedEvidence(Evidence newI)
    {
        if(newI != null)
        {
            inputtedEvidence = newI;
            question.inputtedEvidence = newI.evID;

            if (inputtedEvidence.interactable != null)
            {
                question.input = inputtedEvidence.interactable.GetName();
                inputNameButton.text.text = inputtedEvidence.interactable.GetName();
            }
            else
            {
                question.input = inputtedEvidence.GetNameForDataKey(Evidence.DataKey.name);
                inputNameButton.text.text = question.input;
            }
        }
        else
        {
            question.inputtedEvidence = string.Empty;
            question.input = "...";
            inputNameButton.text.text = "...";
        }

        OnInputEdited();
    }

    public void OnPick(Evidence newSelection, List<Evidence.DataKey> keys)
    {
        if(!resultsMode)
        {
            EvidenceCitizen cit = newSelection as EvidenceCitizen;

            if (cit != null && CasePanelController.Instance.pickForField.question.inputType == Case.InputType.citizen)
            {
                inputNameButton.text.text = cit.witnessController.GetCitizenName();
            }
            else
            {
                EvidenceLocation loc = newSelection as EvidenceLocation;

                if (loc != null && CasePanelController.Instance.pickForField.question.inputType == Case.InputType.location)
                {
                    inputNameButton.text.text = loc.locationController.name;
                }
                else if (CasePanelController.Instance.pickForField.question.inputType == Case.InputType.item)
                {
                    SetSelectedEvidence(newSelection);
                }
            }

            CasePanelController.Instance.SetPickModeActive(false, null);

            OnInputEdited();
        }
    }
}
