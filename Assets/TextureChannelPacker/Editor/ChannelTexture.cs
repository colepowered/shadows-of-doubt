﻿using UnityEngine;

namespace Assets.TextureChannelPacker.Editor
{
    internal class ChannelTexture
    {
        public bool Inverted { get; set; }
        public int Width { get { return (Texture == null) ? 0 : Texture.width; } }
        public int Height { get { return (Texture == null) ? 0 : Texture.height; } }
        public Channel ExtractionChannel { get; set; }
        public string FilePath { get; set; }
        public Texture2D Texture { get; set; }

        public ChannelTexture()
        {
            DefaultInitialization();
        }

        public ChannelTexture(Texture2D texture)
        {
            DefaultInitialization();
            Texture = texture;
        }

        private void DefaultInitialization()
        {
            Texture = null;
            ExtractionChannel = Channel.Red;
            FilePath = "<browse for a texture>";
            Inverted = false;
        }
    }
}