﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceWound : Evidence
{
    //Constructor
    public EvidenceWound(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
    }

    //Override data sources: This needs it's own, not the owners like in EvidenceItem
    public override void BuildDataSources()
    {
        //base.BuildDataSources();
    }
}
