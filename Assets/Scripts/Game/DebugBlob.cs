﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugBlob : MonoBehaviour
{
	public Vector2 pos;
	public bool left = false;
	public bool right = false;
	public bool up = false;
	public bool down = false;
}
