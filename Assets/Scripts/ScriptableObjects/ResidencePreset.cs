﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "residence_data", menuName = "Database/Residence Preset")]

public class ResidencePreset : SoCustomComparison
{
    [Header("Settings")]
    [Tooltip("Are NPCs allowed to live here?")]
    public bool habitable = true;
    [Tooltip("Is this residence automatically put up for sale?")]
    public bool enableForSale = true;
    [Tooltip("Furnish this room even if uninhabited")]
    public bool furnitureIfUnihabited = false;
    [Tooltip("Is this a hotel room that the player can go to when they rent a room?")]
    public bool isHotelRoom = false;
}
