﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;
using TMPro;

public class SteamAPIController : MonoBehaviour
{
	protected Callback<SteamNetworkingIdentity> m_GameOverlayActivated;
	public TextMeshProUGUI steam64IDtext;

	void Update()
	{
		if (SteamManager.Initialized)
		{
			try
			{
				Game.Log("Steam manager initialized: " + SteamFriends.GetPersonaName());

				CSteamID id = SteamUser.GetSteamID();
				steam64IDtext.text = id.ToString();
				this.enabled = false;
			}
			catch
			{
				Game.LogError("Unable to get steam ID");
			}
		}
	}
}
