﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceBirthdayCard : Evidence
{
    public Citizen birthdayCitizen;
    public Human from;
    public Acquaintance relationship;

    //Constructor
    public EvidenceBirthdayCard(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        birthdayCitizen = newController as Citizen;

        Interactable.Passed pass = newPassedObjects[0] as Interactable.Passed; //Pass the citizen who sent this
        int getFrom = (int)pass.value;
       
        CityData.Instance.GetHuman(getFrom, out from);

        birthdayCitizen.FindAcquaintanceExists(from, out relationship);

        //Now add facts...
        AddFactLink(birthdayCitizen.factDictionary["Birthday"], Evidence.DataKey.name, false);

        foreach (Fact conFact in relationship.connectionFacts)
        {
            AddFactLink(conFact, Evidence.DataKey.name, false);
        }

        if(relationship.connectionFacts.Count <= 0)
        {
            Game.LogError(relationship.from.GetCitizenName() + " to " + relationship.with.GetCitizenName() + " features no connection facts!");
        }
    }

    public override void BuildDataSources()
    {
        base.BuildDataSources();

        //Create data source
        //if(dataSource == null) dataSource = new Dictionary<string, EvidenceLinkData>();

        //If the connection is at work, we also know they work here now...
        try
        {
            if (relationship.connections.Contains(Acquaintance.ConnectionType.boss) || relationship.connections.Contains(Acquaintance.ConnectionType.familiarWork) || relationship.connections.Contains(Acquaintance.ConnectionType.workOther) || relationship.connections.Contains(Acquaintance.ConnectionType.workTeam) || relationship.secretConnection == Acquaintance.ConnectionType.paramour)
            {
                AddFactLink(birthdayCitizen.factDictionary["WorksAt"], Evidence.DataKey.name, false);

                //dataSource.Add("work", new EvidenceLinkData(" at " + birthdayCitizen.job.employer.name, ev: birthdayCitizen.job.employer.address.evidenceEntry));
            }
            //Else we know they live here...
            else
            {
                AddFactLink(birthdayCitizen.factDictionary["LivesAt"], Evidence.DataKey.name, false);
            }

            //Create belongs to fact
            AddFactLink(EvidenceCreator.Instance.CreateFact("BelongsTo", this, birthdayCitizen.evidenceEntry), Evidence.DataKey.name, true);

            //Create sent fact
            AddFactLink(EvidenceCreator.Instance.CreateFact("SentBy", this, from.evidenceEntry), Evidence.DataKey.name, true);

            //dataSource.Add("to", new EvidenceLinkData(birthdayCitizen.casualName, ev: birthdayCitizen.evidenceEntry));
            //dataSource.Add("connection", new EvidenceLinkData(Strings.Casing.asIs, "evidence.generic", relationship.connection.ToString()));
            //dataSource.Add("from", new EvidenceLinkData(from.citizenName, ev: from.evidenceEntry));
        }
        catch
        {

        }

    }

    public override string GenerateName()
    {
        return Strings.Get("evidence.generic", preset.name);
    }
}
