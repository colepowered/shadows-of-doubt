﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class LoadResolutionController : MonoBehaviour
{
    //Load resolution, save default resolution if first time running...
    private void Awake()
    {
        Resolution currentRes = Screen.currentResolution;
        FullScreenMode currentMode = Screen.fullScreenMode;

        Screen.SetResolution(PlayerPrefs.GetInt("width", currentRes.width), PlayerPrefs.GetInt("height", currentRes.height), (FullScreenMode)PlayerPrefs.GetInt("fullscreen", (int)currentMode), PlayerPrefs.GetInt("refresh", currentRes.refreshRate));
    }
}
