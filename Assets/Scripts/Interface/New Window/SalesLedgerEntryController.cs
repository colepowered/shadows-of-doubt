﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class SalesLedgerEntryController : MonoBehaviour
{
    public RectTransform rect;
    public SalesLedgerContentController salesLedger;
    public Company.SalesRecord salesRecord;
    public TextMeshProUGUI descriptionText;
    public TextMeshProUGUI timeText;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI priceText;

    public void Setup(Company.SalesRecord newRecord, SalesLedgerContentController newSalesLedger)
    {
        salesLedger = newSalesLedger;
        salesRecord = newRecord;

        //Name
        Human punter = salesRecord.GetPunter();
        Strings.LinkData nameLink = Strings.AddOrGetLink(punter.evidenceEntry, (new Evidence.DataKey[] { Evidence.DataKey.initialedName }).ToList() );
        nameText.text = "<link=" + nameLink.id.ToString() + ">" + punter.GetInitialledName() + "</link>";

        //Description
        descriptionText.text = string.Empty;

        float yPos = 38f;

        Company comp = salesRecord.GetCompany();

        for (int i = 0; i < salesRecord.items.Count; i++)
        {
            string it = salesRecord.items[i];
            InteractablePreset p = Toolbox.Instance.GetInteractablePreset(it);
            float price = 0;

            if(comp.prices.ContainsKey(p))
            {
                price = comp.prices[p];
            }

            descriptionText.text += "<align=\"left\">" + Strings.Get("evidence.names", p.name) + "  <align=\"right\">" + CityControls.Instance.cityCurrency + Toolbox.Instance.RoundToPlaces(price, 2);
            descriptionText.text += "\n";
            descriptionText.rectTransform.sizeDelta = new Vector2(descriptionText.rectTransform.sizeDelta.x, descriptionText.rectTransform.sizeDelta.y + 20);
            yPos += 20f;
        }

        rect.sizeDelta = new Vector2(rect.sizeDelta.x, yPos);

        //Time
        EvidenceTime GetTimeEv = EvidenceCreator.Instance.GetTimeEvidence(salesRecord.time, salesRecord.time, parentID: salesLedger.parentWindow.passedEvidence.evID);

        Strings.LinkData timeLink = Strings.AddOrGetLink(GetTimeEv);
        timeText.text = "<link=" + timeLink.id.ToString() + ">" + SessionData.Instance.ShortDateString(salesRecord.time, false) + " " + SessionData.Instance.GameTimeToClock24String(salesRecord.time, false) + "</link>";

        //Price
        priceText.text = CityControls.Instance.cityCurrency + Toolbox.Instance.RoundToPlaces(salesRecord.cost, 2);
    }
}
