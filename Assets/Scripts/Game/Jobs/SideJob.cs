﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Reflection;
using UnityEngine.EventSystems;
using System;
using UnityEditor;

[System.Serializable]
public class SideJob
{
    [Header("Serialized Data")]
    public string presetStr;
    public string motiveStr;
    public int jobID = 0;
    public static int assignJobID = 1;
    public JobState state = JobState.generated;
    public bool postImmediately = false;
    public int startingScenario = 0;
    public string intro;
    public string handIn;
    public bool accepted = false; //Accepted by the player
    public int caseID = -1; //Reference to created case...
    public int phase = 0;
    public int postID = -1;
    public int gooseChasePhone = 0;
    public int gooseChaseFromPhone = 0;
    public bool knowHandInLocation = false;
    public float gooseChaseCallTime = 0f;
    public bool gooseChaseCallTriggered = false;
    public int meetingPoint = 0;
    public int meetingConsumableIndex = 0;
    public int secretLocationFurniture = 0;
    public Vector3Int secretLocationNode;
    public bool failed = false;
    public List<Case.ResolveQuestion> resolveQuestions = new List<Case.ResolveQuestion>(); //Generated on job creation and copied to case (unavilable after acceptance)

    public enum JobState { generated, posted, ended };

    //Details
    public int posterID = -1;
    public int purpID = -1;
    public int reward = 0;
    public string rewardSyncDisk;

    //Generated number
    public int fakeNumber = -1;
    public string fakeNumberStr;
    public string jobInfoDialogMsg; //Reference to the dialog received for job info
    public List<JobPreset.BasicLeadPool> appliedBasicLeads = new List<JobPreset.BasicLeadPool>();
    public List<Evidence.DataKey> leadKeys = new List<Evidence.DataKey>(); //First lead key(s) purtaining to the above

    //Confine location
    public List<ConfineLocation> confine = new List<ConfineLocation>();

    //Dialog
    public List<AddedDialog> dialog = new List<AddedDialog>();

    //Assigned dialog
    [System.Serializable]
    public class AddedDialog
    {
        public int humanID;
        public string dialogRef;
        public int roomID = -1;
        public Evidence.DataKey key;

        [System.NonSerialized]
        public EvidenceWitness.DialogOption option;

        public Human GetHuman()
        {
            Human ret = null;
            CityData.Instance.GetHuman(humanID, out ret);
            return ret;
        }

        public DialogPreset GetDialog()
        {
            //Get dialog
            DialogPreset ret = Toolbox.Instance.allDialog.Find(item => item.name.ToLower() == dialogRef.ToLower());
            return ret;
        }

        public NewRoom GetRoom()
        {
            NewRoom ret = null;
            CityData.Instance.roomDictionary.TryGetValue(roomID, out ret);
            return ret;
        }
    }

    [System.Serializable]
    public class ConfineLocation
    {
        public int id;
        public int address;
    }

    [Header("Non Serialized")]
    [System.NonSerialized]
    public int phaseChange = -1; //Detects phase changes
    [System.NonSerialized]
    public JobPreset preset;
    [System.NonSerialized]
    public MotivePreset motive;
    [System.NonSerialized]
    JobPreset.IntroConfig chosenIntro;
    [System.NonSerialized]
    JobPreset.HandInConfig chosenHandIn;
    [System.NonSerialized]
    public Human poster;
    [System.NonSerialized]
    public Human purp;
    [System.NonSerialized]
    public Interactable post; //The job post itself. Once spawned, this will link to this job automatically once loaded in.
    [System.NonSerialized]
    public Dictionary<JobPreset.JobTag, Interactable> activeJobItems = new Dictionary<JobPreset.JobTag, Interactable>(); //Items that have been spawned because of this job...
    [System.NonSerialized]
    public Case thisCase; //Reference to the case
    [System.NonSerialized]
    public Dictionary<string, List<Objective>> objectiveReference = new Dictionary<string, List<Objective>>();
    //[System.NonSerialized]
    //public Dictionary<Human, List<EvidenceWitness.DialogOption>> addedDialog = new Dictionary<Human, List<EvidenceWitness.DialogOption>>(); //For referencing manually-added dialog- make sure to remove when case is over...
    [System.NonSerialized]
    public Interactable hiddenItemPhoto = null;
    [System.NonSerialized]
    public Interactable chosenGooseChasePhone = null;
    [System.NonSerialized]
    public Interactable chosenMeetingPoint = null;
    [System.NonSerialized]
    public TelephoneController.PhoneCall gooseChaseCall;
    [System.NonSerialized]
    SideMissionIntroPreset.SideMissionObjectiveBlock currentBlock = null;
    [System.NonSerialized]
    private bool triggerHandIn = false;

    //[System.NonSerialized]
    //Objective.ObjectiveTrigger submitTrigger = null;

    //Event for changing objectives
    public delegate void ObjectivesChange();
    public event ObjectivesChange OnObjectivesChanged;

    public delegate void AcquireJobInfo();
    public event AcquireJobInfo AcquireInfo;

    //Create a new Job...
    public SideJob(JobPreset newPreset, SideJobController.JobPickData newData, bool immediatePost)
    {
        jobID = assignJobID;
        assignJobID++;

        if(!SideJobController.Instance.allJobsDictionary.ContainsKey(jobID))
        {
            SideJobController.Instance.allJobsDictionary.Add(jobID, this);
        }

        preset = newPreset;
        presetStr = newPreset.name;

        motive = newData.motive;
        motiveStr = motive.name;

        poster = newData.poster;
        posterID = poster.humanID;

        purp = newData.purp;
        purpID = purp.humanID;

        postImmediately = immediatePost;

        GenerateFakeNumber();

        //Pick starting scenario
        startingScenario = Toolbox.Instance.Rand(0, preset.startingScenarios.Count);

        //Add to active jobs
        SetJobState(JobState.generated, true);

        //Pick any basci pool leads options
        PickPoolLeadOptions();

        //Only do this on spawn: Spawn items
        SpawnItems(ref preset.spawnItems);

        ChooseIntro();
        ChooseHandIn();

        if(preset.generateHidingLocation)
        {
            GenerateHidingLocation();
        }

        //Post the job or start a citizen to do it...
        PostJob();

        //Generate resolve questions
        GenerateResolveQuestions(true);
    }

    public virtual void GenerateFakeNumber()
    {
        if (fakeNumberStr == null || fakeNumberStr.Length <= 0)
        {
            string s = CityData.Instance.seed + motiveStr + posterID * postID * purpID;
            fakeNumber = Toolbox.Instance.GetPsuedoRandomNumber(8000000, 8999000, s);

            //Add one to find a unique number
            while (CityData.Instance.phoneDictionary.ContainsKey(fakeNumber) || TelephoneController.Instance.fakeTelephoneDictionary.ContainsKey(fakeNumber))
            {
                fakeNumber++;
            }

            fakeNumberStr = Toolbox.Instance.GetTelephoneNumberString(fakeNumber);
        }
    }

    public virtual void ChooseIntro()
    {
        List<JobPreset.IntroConfig> pool = new List<JobPreset.IntroConfig>();

        foreach(JobPreset.IntroConfig intr in preset.compatibleIntros)
        {
            for (int i = 0; i < intr.frequency; i++)
            {
                pool.Add(intr);
            }
        }

        if(pool.Count > 0)
        {
            chosenIntro = pool[Toolbox.Instance.Rand(0, pool.Count)];
            intro = chosenIntro.preset.name;
        }
    }

    public virtual void ChooseHandIn()
    {
        List<JobPreset.HandInConfig> pool = new List<JobPreset.HandInConfig>();

        foreach (JobPreset.HandInConfig intr in preset.compatibleHandIns)
        {
            for (int i = 0; i < intr.frequency; i++)
            {
                pool.Add(intr);
            }
        }

        if (pool.Count > 0)
        {
            chosenHandIn = pool[Toolbox.Instance.Rand(0, pool.Count)];
            handIn = chosenHandIn.preset.name;
        }
    }

    public virtual void SpawnItems(ref List<JobPreset.StartingSpawnItem> spawnThese)
    {
        List<JobPreset.StartingSpawnItem> toSpawn = new List<JobPreset.StartingSpawnItem>(spawnThese);
        List<JobPreset.StartingSpawnItem> successsfullySpawned = new List<JobPreset.StartingSpawnItem>();

        int safety = 999;

        while(toSpawn.Count > 0 && safety > 0)
        {
            JobPreset.StartingSpawnItem spawn = toSpawn[0];

            //Check compatible with motive
            if(!SpawnItemIsValid(spawn, ref successsfullySpawned, !spawn.useOrGroup))
            {
                toSpawn.RemoveAt(0);
                safety--;
                continue;
            }

            List<JobPreset.StartingSpawnItem> orGroupPool = null;

            //Only spawn one of these type...
            if (spawn.useOrGroup)
            {
                orGroupPool = new List<JobPreset.StartingSpawnItem>();
                List<JobPreset.StartingSpawnItem> groupSinglePool = toSpawn.FindAll(item => item.useOrGroup && item.orGroup == spawn.orGroup && SpawnItemIsValid(item, ref successsfullySpawned, false));

                //Create a pool using chances...
                for (int u = 0; u < groupSinglePool.Count; u++)
                {
                    for (int l = 0; l < Mathf.RoundToInt(groupSinglePool[u].chanceRatio); l++)
                    {
                        orGroupPool.Add(groupSinglePool[u]);
                    }
                }

                spawn = orGroupPool[Toolbox.Instance.Rand(0, orGroupPool.Count)]; //Change the object to one of these...
            }

            //Spawn object...
            if(spawn.spawnItem != null)
            {
                Game.Log("Jobs: " + presetStr + ": Spawn item " + spawn.name + " for job " + preset.name + " tag: " + spawn.itemTag.ToString() + ", use if " + spawn.useIf + ": " + spawn.ifTag.ToString() + ", use or group " + spawn.useOrGroup + ": " + spawn.orGroup.ToString() +"...");
                Interactable newItem = SpawnJobItem(spawn.spawnItem, spawn.where, spawn.belongsTo, spawn.writer, spawn.receiver, spawn.security, spawn.ownershipRule, spawn.priority, spawn.itemTag, spawn.findExisting);

                successsfullySpawned.Add(spawn);
            }

            //Start VMAIL thread
            if(spawn.vmailThread != null && spawn.vmailThread.Length > 0)
            {
                Human belongsTo = null;

                if (spawn.belongsTo == JobPreset.LeadCitizen.poster) belongsTo = poster;
                else if (spawn.belongsTo == JobPreset.LeadCitizen.purp) belongsTo = purp;
                else if (spawn.belongsTo == JobPreset.LeadCitizen.purpsParamour) belongsTo = purp.paramour;

                Toolbox.Instance.NewVmailThread(belongsTo, new List<Human>(), spawn.vmailThread, SessionData.Instance.gameTime + Toolbox.Instance.Rand(-48f, -12f), Mathf.RoundToInt(Toolbox.Instance.Rand(spawn.vmailProgressThreshold.x, spawn.vmailProgressThreshold.y)));
            }

            //Remove from pool...
            toSpawn.Remove(spawn);
            
            if(orGroupPool != null)
            {
                foreach(JobPreset.StartingSpawnItem sp in orGroupPool)
                {
                    toSpawn.Remove(sp);
                }
            }

            safety--;
        }
    }

    private bool SpawnItemIsValid(JobPreset.StartingSpawnItem spawn, ref List<JobPreset.StartingSpawnItem> successsfullySpawned, bool useChance)
    {
        if (useChance)
        {
            float chance = spawn.chance;

            //Check picking rules
            bool passRules = true;

            if(spawn.useTraits)
            {
                foreach (JobPreset.JobModifierRule rule in spawn.traitModifiers)
                {
                    bool pass = false;

                    Human cit = null;
                    if (rule.who == JobPreset.LeadCitizen.poster) cit = poster;
                    else if (rule.who == JobPreset.LeadCitizen.purp) cit = purp;
                    else if (rule.who == JobPreset.LeadCitizen.purpsParamour) cit = purp.paramour;

                    if(cit != null)
                    {
                        if (rule.rule == CharacterTrait.RuleType.ifAnyOfThese)
                        {
                            foreach (CharacterTrait searchTrait in rule.traitList)
                            {
                                if (cit.characterTraits.Exists(item => item.trait == searchTrait))
                                {
                                    pass = true;
                                    break;
                                }
                            }
                        }
                        else if (rule.rule == CharacterTrait.RuleType.ifAllOfThese)
                        {
                            pass = true;

                            foreach (CharacterTrait searchTrait in rule.traitList)
                            {
                                if (!cit.characterTraits.Exists(item => item.trait == searchTrait))
                                {
                                    pass = false;
                                    break;
                                }
                            }
                        }
                        else if (rule.rule == CharacterTrait.RuleType.ifNoneOfThese)
                        {
                            pass = true;

                            foreach (CharacterTrait searchTrait in rule.traitList)
                            {
                                if (cit.characterTraits.Exists(item => item.trait == searchTrait))
                                {
                                    pass = false;
                                    break;
                                }
                            }
                        }
                        else if (rule.rule == CharacterTrait.RuleType.ifPartnerAnyOfThese)
                        {
                            if (cit.partner != null)
                            {
                                foreach (CharacterTrait searchTrait in rule.traitList)
                                {
                                    if (cit.partner.characterTraits.Exists(item => item.trait == searchTrait))
                                    {
                                        pass = true;
                                        break;
                                    }
                                }
                            }
                            else pass = false;
                        }
                    }

                    if (pass)
                    {
                        chance += rule.chanceModifier;
                    }
                    else if (rule.mustPassForApplication)
                    {
                        passRules = false;
                    }
                }
            }

            if(!passRules || Toolbox.Instance.Rand(0f, 1f) > chance) return false;
        }

        //Disable on difficulties
        if (spawn.disableOnDifficulties.Contains(preset.difficultyTag)) return false;

        //Check compatible with motive
        if (!spawn.compatibleWithAllMotives && !spawn.compatibleWithMotives.Contains(motive))
        {
            return false;
        }

        //Only spawn if a certain other item has been spawned already...
        if (spawn.useIf)
        {
            if (!successsfullySpawned.Exists(item => item.itemTag == spawn.itemTag))
            {
                return false;
            }
        }

        return true;
    }

    //Called when this job is active
    public virtual void GameWorldLoop()
    {
        //Handle objective progress
        HandleObjectiveProgress();

        //Hand objectives
        ObjectiveStateLoop();
    }

    public virtual void HandleObjectiveProgress()
    {
        if (thisCase != null)
        {
            bool checkValid = false;

            foreach (Case.ResolveQuestion q in thisCase.resolveQuestions)
            {
                if (q.inputType == Case.InputType.revengeObjective && q.revengeObjective != null && q.revengeObjective.Length > 0)
                {
                    RevengeObjective rev = q.GetRevengeObjective();

                    if (rev != null)
                    {
                        if (rev.specialConditions.Contains(RevengeObjective.SpecialConditions.trackProgressFromAddressQuestion))
                        {
                            //Parse locations
                            NewGameLocation thisLocation = GetGameLocationFromQuestionInput(q);

                            Game.Log("Jobs: Attempting to parse progress from location: " + thisLocation);

                            if (thisLocation != null)
                            {
                                int getID = 0;
                                if (thisLocation.thisAsAddress != null) getID = thisLocation.thisAsAddress.id;
                                else if (thisLocation.thisAsStreet != null) getID = thisLocation.thisAsStreet.streetID;

                                //Create method info
                                MethodInfo method = rev.GetType().GetMethod(rev.answerMethod);

                                if (method != null)
                                {
                                    object[] passed = { 0, getID, 0 };
                                    object ret = method.Invoke(rev, passed);

                                    //The returned should be a bool
                                    float amount = 0f;

                                    if (float.TryParse(ret.ToString(), out amount))
                                    {
                                        Game.Log("Jobs: Managed to parse float from " + ret.ToString() + " (" + amount + "/" + (float)q.revengeObjPassed + ")");
                                    }
                                    else
                                    {
                                        Game.Log("Jobs: Unable to parse float from " + ret.ToString());
                                    }

                                    q.SetProgress(amount / (float)q.revengeObjPassed);
                                    Game.Log("Jobs: Progress for " + q.name + " is " + q.progress);
                                }
                                else
                                {
                                    Game.Log("Jobs: Unable to return method from " + rev.answerMethod);
                                    q.SetProgress(0f);
                                }
                            }
                            else
                            {
                                q.SetProgress(0f);
                            }
                        }
                    }
                    else
                    {
                        Game.Log("Jobs: Unable to get revenge objective: " + q.revengeObjective);
                    }

                    checkValid = true;
                }
            }

            if (checkValid)
            {
                //Check for valid status as this won't depend on player input...
                thisCase.ValidationCheck();
            }
        }
    }

    public NewGameLocation GetGameLocationFromQuestionInput(Case.ResolveQuestion question)
    {
        //Parse locations
        NewGameLocation thisLocation = null;

        //With D0 difficulty, we already know the address...
        if (preset.difficultyTag == JobPreset.DifficultyTag.D0)
        {
            thisLocation = GetGameLocation(question.location);
        }
        else
        {
            Case.ResolveQuestion addressQuestion = thisCase.resolveQuestions.Find(item => item.inputType == Case.InputType.location);

            if (addressQuestion != null && addressQuestion.input != null && addressQuestion.input.Length > 0)
            {
                //Parse locations
                thisLocation = CityData.Instance.gameLocationDirectory.Find(item => item.name.ToLower() == addressQuestion.input.ToLower());
            }
            else Game.Log("Jobs: Unable to get a valid address for address question input");
        }

        return thisLocation;
    }

    public Human GetCitizenFromQuestionInput(Case.ResolveQuestion question)
    {
        //Parse locations
        Human thisPerson = null;

        //With D0 difficulty, we already know the address...
        if (preset.difficultyTag == JobPreset.DifficultyTag.D0)
        {
            thisPerson = GetTarget(question.target);
        }
        else
        {
            Case.ResolveQuestion citizenQuestion = thisCase.resolveQuestions.Find(item => item.inputType == Case.InputType.citizen);

            if (citizenQuestion != null && citizenQuestion.input != null && citizenQuestion.input.Length > 0)
            {
                //Parse locations
                thisPerson = CityData.Instance.citizenDirectory.Find(item => item.GetCitizenName().ToLower() == citizenQuestion.input.ToLower());
            }
            else Game.Log("Jobs: Unable to get a valid citizen for name question input");
        }

        return thisPerson;
    }

    public virtual void ObjectiveStateLoop()
    {
        //Objectives state loops
        if (thisCase != null && thisCase.isActive && state != JobState.ended && (InteractionController.Instance.talkingTo == null || (currentBlock != null && currentBlock.enableUpdateWhileTalking)))
        {
            bool phaseChanged = false; //A flag we can use to only run things once

            //Only run this once
            if (phase != phaseChange)
            {
                phaseChange = phase;

                phaseChanged = true;
                Game.Log("Jobs: Phase change detected: " + phase);
            }

            //Phases are split up by job state
            //Get the block of the current phase
            currentBlock = null;

            if (thisCase.caseStatus == Case.CaseStatus.handInNotCollected)
            {
                if(phase < chosenIntro.preset.blocks.Count) currentBlock = chosenIntro.preset.blocks[phase];
            }
            else if(thisCase.caseStatus == Case.CaseStatus.handInCollected)
            {
                try
                {
                    if(phase - chosenIntro.preset.blocks.Count < preset.additional.Count) currentBlock = preset.additional[phase - chosenIntro.preset.blocks.Count];
                    else if(phase - chosenIntro.preset.blocks.Count - preset.additional.Count < chosenHandIn.preset.blocks.Count) currentBlock = chosenHandIn.preset.blocks[phase - chosenIntro.preset.blocks.Count - preset.additional.Count];
                }
                catch
                {
                    Game.LogError("Unable to get block for " + phase);
                }
            }

            if (currentBlock != null)
            {
                //Skip this phase
                if(currentBlock.disableOnDifficulties.Contains(preset.difficultyTag))
                {
                    phase++;
                    return;
                }

                if(currentBlock.onlyCompatibleWithHandIns.Count > 0)
                {
                    if (!currentBlock.onlyCompatibleWithHandIns.Exists(item => item.name.ToLower() == handIn.ToLower()))
                    {
                        phase++;
                        return;
                    }
                }

                if (currentBlock.onlyCompativleWithIntros.Count > 0)
                {
                    if (!currentBlock.onlyCompativleWithIntros.Exists(item => item.name.ToLower() == intro.ToLower()))
                    {
                        phase++;
                        return;
                    }
                }

                //Write stuff you only want to happen once here...
                if (phaseChanged)
                {
                    Game.Log("Jobs: " + presetStr + " (" + jobID + ") enters phase " + phase + ": " + currentBlock.elementType);

                    //Spawn any items
                    if (currentBlock.spawnItems != null && currentBlock.spawnItems.Count > 0)
                    {
                        SpawnItems(ref currentBlock.spawnItems);
                    }

                    if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.playerCallsNumber)
                    {
                        //Get dialog
                        JobPreset.DialogReference dialogPreset = preset.dialogReferences.Find(item => item.name.ToLower() == currentBlock.dialogReference.ToLower());

                        if (dialogPreset != null)
                        {
                            //Generate fake number to call
                            GenerateFakeNumber();

                            TelephoneController.Instance.AddFakeNumber(fakeNumber, new TelephoneController.CallSource(TelephoneController.CallType.player, dialogPreset.dialog, this));

                            TelephoneController.Instance.fakeTelephoneDictionary[fakeNumber].dialogGreeting = dialogPreset.dialog;
                            TelephoneController.Instance.fakeTelephoneDictionary[fakeNumber].dialog = dialogPreset.dialog.name;

                            Objective.ObjectiveTrigger callFake = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.makeCall, fakeNumber.ToString(), newJob: this);

                            //When advancing phase, make sure we also pass the current phase
                            AddObjective(currentBlock.name, callFake, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextSideJobPhase, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.hand, delay: currentBlock.objectiveDelay);
                        }
                        else Game.LogError("Could not find job dialog index " + currentBlock.dialogReference);
                    }
                    else if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.acquireInformation)
                    {
                        //Get dialog
                        JobPreset.DialogReference dialogPreset = preset.dialogReferences.Find(item => item.name.ToLower() == currentBlock.dialogReference.ToLower());

                        if (dialogPreset != null)
                        {
                            OnAcquireJobInfo(dialogPreset.dialog);
                        }
                        else Game.LogError("Could not find job dialog index " + currentBlock.dialogReference);

                        //Automatic phase change
                        phase++;
                    }
                    else if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.askStaff)
                    {
                        //Get dialog
                        JobPreset.DialogReference dialogPreset = preset.dialogReferences.Find(item => item.name.ToLower() == currentBlock.dialogReference.ToLower());

                        if (dialogPreset != null && post != null)
                        {
                            if (post.node != null)
                            {
                                if (post.node.gameLocation != null)
                                {
                                    if (post.node.gameLocation.thisAsAddress != null)
                                    {
                                        if (post.node.gameLocation.thisAsAddress.company != null)
                                        {
                                            List<Occupation> serviceJobs = post.node.gameLocation.thisAsAddress.company.companyRoster.FindAll(item => item.employee != null && item.preset.canAskAboutJob);

                                            foreach (Occupation occ in serviceJobs)
                                            {
                                                Game.Log("Jobs: Adding dialog " + dialogPreset.dialog.name + " to " + occ.employee.name);
                                                AddDialogOption(occ.employee, Evidence.DataKey.photo, dialogPreset.dialog);
                                            }
                                        }

                                        Objective.ObjectiveTrigger askStaff = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.onDialogSuccess, dialogPreset.dialog.name, newJob: this);

                                        //When advancing phase, make sure we also pass the current phase
                                        AddObjective(currentBlock.name, askStaff, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextSideJobPhase, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.citizen, delay: currentBlock.objectiveDelay);
                                    }
                                    else Game.LogError("Job post " + post.id + " is not at an address! Location: " + post.GetWorldPosition());
                                }
                                else Game.LogError("Job post " + post.id + " has not been assigned a GameLocation. Location: " + post.GetWorldPosition());
                            }
                            else
                            {
                                Game.LogError("Job post " + post.id + " has not been assigned a Node. Location: " + post.GetWorldPosition() + " Attempting to assign now...");
                                post.UpdateWorldPositionAndNode(false);
                                if (post.node != null) Game.Log("Job post " + post.id + " node assign successful!");
                            }
                        }
                        else Game.LogError("Could not find job dialog index " + currentBlock.dialogReference);
                    }

                    else if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.spawnItems)
                    {
                        //Automatic phase change
                        phase++;
                    }
                    else if(currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.photoOfItemLocation)
                    {
                        Interactable item = GetItem(currentBlock.tagReference);

                        if (item != null)
                        {
                            //This is now done with dialog trigger
                            if (hiddenItemPhoto == null) hiddenItemPhoto = Toolbox.Instance.GetLocalizedSnapshot(item);

                            if (hiddenItemPhoto != null)
                            {
                                ActionController.Instance.Inspect(hiddenItemPhoto, Player.Instance.currentNode, Player.Instance);
                                CasePanelController.Instance.PinToCasePanel(thisCase, hiddenItemPhoto.evidence, Evidence.DataKey.photo, true);
                            }
                            else Game.LogError("Unable to generate hidden item photo");

                            //Automatic phase change
                            phase++;
                        }
                        else Game.LogError("Could not find item with tag " + currentBlock.tagReference);
                    }
                    else if(currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.openedBriefcase)
                    {
                        Interactable item = GetItem(currentBlock.tagReference);

                        if(item != null)
                        {
                            Objective.ObjectiveTrigger inspectItem = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.raiseFirstPersonItem, "", newInteractable: item);

                            //When advancing phase, make sure we also pass the current phase
                            AddObjective(currentBlock.name, inspectItem, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextSideJobPhase, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.lookingGlass, delay: currentBlock.objectiveDelay);

                            Game.Log("Jobs: Hidden item exists (" + item.id + ") at " + item.GetWorldPosition());
                        }
                        else Game.LogError("Could not find item with tag " + currentBlock.tagReference);
                    }
                    else if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.playerHasCamera)
                    {
                        Objective.ObjectiveTrigger hasFPSItem = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.hasFPSInventory, "camera");

                        //When advancing phase, make sure we also pass the current phase
                        AddObjective(currentBlock.name, hasFPSItem, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextSideJobPhase, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.camera, delay: currentBlock.objectiveDelay);
                    }
                    else if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.playerHasHandcuffs)
                    {
                        Objective.ObjectiveTrigger hasFPSItem = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.hasFPSInventory, "handcuffs");

                        //When advancing phase, make sure we also pass the current phase
                        AddObjective(currentBlock.name, hasFPSItem, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextSideJobPhase, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.handcuffs, delay: currentBlock.objectiveDelay);
                    }
                    else if(currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.setGooseChaseCall)
                    {
                        if(gooseChasePhone == 0)
                        {
                            //Pick a payphone...
                            List<Telephone> allPhones = new List<Telephone>(CityData.Instance.phoneDictionary.Values);

                            List<Telephone> payPhones = allPhones.FindAll(item => item.interactable != null && item.interactable.preset.isPayphone && item.interactable.node.gameLocation.IsPublicallyOpen(true) && item.interactable.node.gameLocation != Player.Instance.currentGameLocation);
                            
                            if(payPhones.Count > 0)
                            {
                                Telephone chosenPhone = payPhones[Toolbox.Instance.Rand(0, payPhones.Count)];

                                if(chosenPhone != null)
                                {
                                    chosenGooseChasePhone = chosenPhone.interactable;
                                    gooseChasePhone = chosenPhone.interactable.id; //Save chosen
                                    Game.Log("Jobs: Setting goose chase phone as " + gooseChasePhone);

                                    //Calculate time...
                                    PathFinder.PathData route = PathFinder.Instance.GetPath(Player.Instance.currentNode, chosenGooseChasePhone.node, Player.Instance);

                                    if(route != null)
                                    {
                                        float secondsPerNode = 0.5f;
                                        gooseChaseCallTime = SessionData.Instance.gameTime + 0.085f + (route.accessList.Count / 60f / 60f * secondsPerNode);
                                    }
                                }
                                else
                                {
                                    Game.Log("Jobs: Unable to pick goose chase phone!");
                                }
                            }
                            else
                            {
                                Game.Log("Jobs: Unable to pick goose chase phone!");
                            }
                        }

                        if(chosenGooseChasePhone != null)
                        {
                            //Set call time
                            gooseChaseCallTriggered = false;

                            //Add get to phone objective
                            Objective.ObjectiveTrigger getToPhone = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToNode, "", newNode: chosenGooseChasePhone.node);
                            AddObjective(currentBlock.name, getToPhone, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.run, delay: currentBlock.objectiveDelay);
                        }
                    }
                    else if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.setGooseChaseCallIndoorOnly)
                    {
                        if (gooseChasePhone == 0)
                        {
                            //Pick a payphone...
                            List<Telephone> allPhones = new List<Telephone>(CityData.Instance.phoneDictionary.Values);
                            List<Telephone> payPhones = allPhones.FindAll(item => item.interactable != null && item.interactable.preset.isPayphone && item.interactable.node.gameLocation.thisAsAddress != null && item.interactable.node.gameLocation.thisAsAddress.company != null && item.interactable.node.gameLocation.IsPublicallyOpen(true) && item.interactable.node.gameLocation != post.node.gameLocation);
                            Telephone chosenPhone = payPhones[Toolbox.Instance.Rand(0, payPhones.Count)];

                            if (chosenPhone != null)
                            {
                                chosenGooseChasePhone = chosenPhone.interactable;
                                gooseChasePhone = chosenPhone.interactable.id; //Save chosen

                                //Calculate time...
                                PathFinder.PathData route = PathFinder.Instance.GetPath(Player.Instance.currentNode, chosenGooseChasePhone.node, Player.Instance);

                                if (route != null)
                                {
                                    float secondsPerNode = 0.5f;
                                    gooseChaseCallTime = SessionData.Instance.gameTime + 0.085f + (route.accessList.Count / 60f / 60f * secondsPerNode);
                                }
                            }
                        }

                        if (chosenGooseChasePhone != null)
                        {
                            //Set call time
                            gooseChaseCallTriggered = false;

                            //Add get to phone objective
                            Objective.ObjectiveTrigger getToPhone = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToNode, "", newNode: chosenGooseChasePhone.node);
                            AddObjective(currentBlock.name, getToPhone, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.run, delay: currentBlock.objectiveDelay);
                        }
                    }
                    else if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.setMeeting)
                    {
                        //Pick meet point
                        if (meetingPoint == 0)
                        {
                            List<Interactable> pool = new List<Interactable>();

                            foreach(NewGameLocation gl in CityData.Instance.gameLocationDirectory)
                            {
                                //If a company, make sure this is open 24hrs
                                if(gl.thisAsAddress != null)
                                {
                                    if(gl.thisAsAddress.company != null)
                                    {
                                        if(gl.thisAsAddress.company.preset.workHours.presetName != "247")
                                        {
                                            continue;
                                        }
                                    }
                                }

                                if(gl.IsPublicallyOpen(true))
                                {
                                    foreach(NewRoom r in gl.rooms)
                                    {
                                        foreach(NewNode n in r.nodes)
                                        {
                                            List<Interactable> v = n.interactables.FindAll(item => item.furnitureParent != null && item.node.gameLocation != Player.Instance.currentGameLocation && currentBlock.validItems.Contains(item.preset) && currentBlock.validFurniture.Contains(item.furnitureParent.furniture) && !item.usagePoint.TryGetUserAtSlot(Interactable.UsePointSlot.defaultSlot, out _));
                                            if(v.Count > 0) pool.AddRange(v);
                                        }
                                    }
                                }
                            }

                            if(pool.Count > 0) chosenMeetingPoint = pool[Toolbox.Instance.Rand(0, pool.Count)];

                            if(chosenMeetingPoint != null)
                            {
                                Game.Log("Jobs: Choosing new meeting point at " + chosenMeetingPoint.GetName() + " " + chosenMeetingPoint.GetWorldPosition());

                                meetingPoint = chosenMeetingPoint.id;
                                gooseChaseCallTime = SessionData.Instance.gameTime;
                                meetingConsumableIndex = Toolbox.Instance.Rand(0, InteriorControls.Instance.meetupConsumables.Count);
                            }
                            else
                            {
                                Game.LogError("Jobs: Unable to find meeting point for job " + jobID);
                            }
                        }

                        if (chosenMeetingPoint != null)
                        {
                            //Get dialog
                            JobPreset.DialogReference dialogPreset = preset.dialogReferences.Find(item => item.name.ToLower() == currentBlock.dialogReference.ToLower());

                            if(dialogPreset != null)
                            {
                                AddDialogOption(poster, Evidence.DataKey.voice, dialogPreset.dialog);

                                Game.Log("Jobs: Attempting to teleport poster " + poster.citizenName + "...");
                                poster.Teleport(poster.FindSafeTeleport(chosenMeetingPoint.node.gameLocation), chosenMeetingPoint.usagePoint);

                                Game.Log("Jobs: ... Poster " + poster.citizenName + " teleported to " + poster.transform.position);

                                poster.outfitController.SetCurrentOutfit(ClothesPreset.OutfitCategory.outdoorsCasual);
                                poster.ai.CreateNewGoal(RoutineControls.Instance.missionMeetUpSpecific, SessionData.Instance.gameTime, 1f, newPassedInteractable: chosenMeetingPoint);

                                //Remove all current consumables
                                if(poster.trash.Count > 0)
                                {
                                    Interactable nearestBin = Toolbox.Instance.FindNearestWithAction(RoutineControls.Instance.disposal, poster.currentRoom, poster, AIActionPreset.FindSetting.onlyPublic);

                                    //Get rid of all binnable trash...
                                    if (nearestBin != null)
                                    {
                                        ActionController.Instance.Dispose(nearestBin, nearestBin.node, poster);
                                    }

                                    //Get rid of all trash here...
                                    for (int i = 0; i < poster.trash.Count; i++)
                                    {
                                        poster.trash.RemoveAt(i);
                                        i--;
                                    }
                                }

                                poster.AddCurrentConsumable(InteriorControls.Instance.meetupConsumables[meetingConsumableIndex]);
                                poster.animationController.SetArmsBoolState(CitizenAnimationController.ArmsBoolSate.armsConsuming);

                                Objective.ObjectiveTrigger meetPerson = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.onDialogSuccess, dialogPreset.dialog.name, newJob: this);

                                //When advancing phase, make sure we also pass the current phase
                                AddObjective(currentBlock.name, meetPerson, usePointer: false, pointerPosition: chosenMeetingPoint.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nextSideJobPhase, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.citizen, delay: currentBlock.objectiveDelay);
                            }
                        }
                    }
                    else if(currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.setHomeMeeting)
                    {
                        //Get dialog
                        JobPreset.DialogReference dialogPreset = preset.dialogReferences.Find(item => item.name.ToLower() == currentBlock.dialogReference.ToLower());

                        if (dialogPreset != null)
                        {
                            foreach(Human h in poster.home.inhabitants)
                            {
                                AddDialogOption(h, Evidence.DataKey.voice, dialogPreset.dialog);
                            }

                            //Wait @ home
                            poster.Teleport(poster.FindSafeTeleport(poster.home), null);
                            Interactable nearestChair = Toolbox.Instance.FindNearestWithAction(RoutineControls.Instance.sit, poster.home.rooms[0], poster, AIActionPreset.FindSetting.homeOnly);
                            poster.ai.CreateNewGoal(RoutineControls.Instance.missionMeetUpSpecific, SessionData.Instance.gameTime, 3f, newPassedInteractable: nearestChair);

                            Objective.ObjectiveTrigger goToHome = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.goToAddress, "", newGameLocation: poster.home, newJob: this);

                            //When advancing phase, make sure we also pass the current phase
                            AddObjective(currentBlock.name, goToHome, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nothing, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.citizen, delay: currentBlock.objectiveDelay);

                            //Automatic phase change
                            phase++;
                        }
                    }
                    else if(currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.setupHomeInvestigation)
                    {
                        Player.Instance.AddToKeyring(poster.home, true);

                        GameplayController.Instance.AddGuestPass(poster.home, 12f);

                        //Automatic phase change
                        phase++;
                    }
                    else if(currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.tailBriefcase)
                    {
                        Objective.ObjectiveTrigger tailBriefcase = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.sideMissionMeetTriggered, "");
                        AddObjective(currentBlock.name, tailBriefcase, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextSideJobPhase, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.lookingGlass, delay: currentBlock.objectiveDelay);
                    }
                    else if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.playerHasItemInPossession)
                    {
                        Interactable getItem = GetItem(currentBlock.tagReference);

                        if(getItem != null)
                        {
                            Objective.ObjectiveTrigger acquireItem = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.itemInInventory, "", newInteractable: getItem);
                            AddObjective(currentBlock.name, acquireItem, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextSideJobPhase, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.robbery, delay: currentBlock.objectiveDelay);
                        }
                        else
                        {
                            Game.Log("Jobs: Cannot find item with tag " + currentBlock.tagReference);
                        }
                    }
                    else if(currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.leaveItemAtSecretLocation)
                    {
                        if (secretLocationFurniture == 0)
                        {
                            Game.LogError("Jobs: Secret location was not generated at start! Please enable this on the jobs preset...");
                            GenerateHidingLocation();
                        }

                        Interactable getItem = GetItem(currentBlock.tagReference);

                        if (getItem != null)
                        {
                            Objective.ObjectiveTrigger hideItem = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.itemIsPlacedAtSecretLocation, "", newInteractable: getItem, newJob: this);
                            AddObjective(currentBlock.name, hideItem, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextSideJobPhase, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.hand, delay: currentBlock.objectiveDelay);
                        }                     
                    }
                    else if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.destroyItem)
                    {
                        Interactable getItem = GetItem(currentBlock.tagReference);

                        if (getItem != null)
                        {
                            Objective.ObjectiveTrigger destroyItem = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.destroyItem, "", newInteractable: getItem);
                            AddObjective(currentBlock.name, destroyItem, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextSideJobPhase, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.hand, delay: currentBlock.objectiveDelay);
                        }
                    }
                    else if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.placeItemInPosterMailbox)
                    {
                        Interactable getItem = GetItem(currentBlock.tagReference);

                        if (getItem != null)
                        {
                            //Find poster mailbox
                            Interactable mailboxDoor = Toolbox.Instance.GetMailbox(poster);

                            if (mailboxDoor != null)
                            {
                                mailboxDoor.SetLockedState(false, null);

                                //Is not in inventory, is close to door...
                                Objective.ObjectiveTrigger hideItem = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.itemIsNear, "", newInteractable: getItem, newJob: this, newPosition: mailboxDoor.GetWorldPosition());
                                AddObjective(currentBlock.name, hideItem, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextSideJobPhase, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.hand, delay: currentBlock.objectiveDelay);
                            }
                            else Game.Log("Jobs: Unable to get mailbox!");
                        }
                    }
                    else if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.placeItemOfTypeInPosterMailbox)
                    {
                        Interactable getItem = GetItem(currentBlock.tagReference);

                        if (getItem != null)
                        {
                            //Find poster mailbox
                            Interactable mailboxDoor = Toolbox.Instance.GetMailbox(poster);

                            if (mailboxDoor != null)
                            {
                                mailboxDoor.SetLockedState(false, null);

                                //Is not in inventory, is close to door...
                                Objective.ObjectiveTrigger hideItem = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.itemOfTypeIsNear, "", newInteractable: getItem, newJob: this, newPosition: mailboxDoor.GetWorldPosition());
                                AddObjective(currentBlock.name, hideItem, usePointer: false, onCompleteAction: Objective.OnCompleteAction.nextSideJobPhase, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.hand, delay: currentBlock.objectiveDelay);
                            }
                            else Game.Log("Jobs: Unable to get mailbox!");
                        }
                    }

                    if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.handDossier)
                    {
                        Interactable getBriefcase = GetItem(currentBlock.tagReference);
                            
                        if(getBriefcase != null)
                        {
                            getBriefcase.MoveInteractable(Player.Instance.transform.position, Vector3.zero, true);
                            getBriefcase.SetSpawnPositionRelevent(false); //Set spawn to irrelevent so there's no 'put back' action
                            FirstPersonItemController.Instance.PickUpItem(getBriefcase, true, false, true, true);

                            //Automatic phase change
                            phase++;
                        }
                    }
                }

                //Continual stuff happens here...
                if(currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.setGooseChaseCall || currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.setGooseChaseCallIndoorOnly)
                {
                    if(!gooseChaseCallTriggered)
                    {
                        //Trigger the call
                        if(SessionData.Instance.gameTime >= gooseChaseCallTime)
                        {
                            if (chosenGooseChasePhone != null)
                            {
                                //Pick a from phone
                                List<Telephone> allPhones = new List<Telephone>(CityData.Instance.phoneDictionary.Values);
                                List<Telephone> payPhones = allPhones.FindAll(item => item.interactable != null && item.interactable.preset.isPayphone && item.interactable != chosenGooseChasePhone && !item.interactable.sw1);
                                Telephone chosenFromPhone = payPhones[Toolbox.Instance.Rand(0, payPhones.Count)];

                                if (chosenFromPhone != null && !chosenFromPhone.interactable.sw1)
                                {
                                    //Get dialog
                                    JobPreset.DialogReference dialogPreset = preset.dialogReferences.Find(item => item.name.ToLower() == currentBlock.dialogReference.ToLower());

                                    if (dialogPreset != null)
                                    {
                                        gooseChaseCall = TelephoneController.Instance.CreateNewCall(chosenFromPhone, chosenGooseChasePhone.t, null, Player.Instance, new TelephoneController.CallSource(TelephoneController.CallType.player, dialogPreset.dialog, this), 0.075f, true);

                                        gooseChaseFromPhone = chosenFromPhone.interactable.id;
                                        chosenFromPhone.interactable.SetCustomState1(true, null);

                                        gooseChaseCallTriggered = true;
                                        OnGooseChaseCallTriggered();
                                        TelephoneController.Instance.OnPlayerCall += OnPlayerCall;

                                        Objective.ObjectiveTrigger answerCall = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.answerPhone, "", newInteractable: chosenGooseChasePhone);
                                        AddObjective("Answer", answerCall, usePointer: true, pointerPosition: chosenGooseChasePhone.GetWorldPosition(), onCompleteAction: Objective.OnCompleteAction.nextSideJobPhase, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.hand, delay: 2f);
                                    }
                                }
                            }
                            else Game.Log("Error: Chosen goose chase phone is null!");
                        }
                    }
                    else
                    {
                        //Detect if the player has missed the call...
                        //Check if player didn't catch the call...
                        if (gooseChaseCall != null)
                        {
                            if(gooseChaseCall.recevierNS != null && !gooseChaseCall.recevierNS.isPlayer)
                            {
                                OnGooseChaseEnd();
                                TriggerFail("Missed Call");
                            }
                            else if(gooseChaseCall.state == TelephoneController.CallState.ended)
                            {
                                OnGooseChaseEnd();
                                TriggerFail("Missed Call");
                            }
                        }
                    }
                }

                //Hand in: Wait for valid questions
                if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.postSubmission)
                {
                    if(!triggerHandIn && thisCase != null && thisCase.handInValid)
                    {
                        //Get the nearest hand-in
                        Interactable closestHandIn = thisCase.GetClosestHandIn();

                        if (closestHandIn != null)
                        {
                            triggerHandIn = true;
                            Objective.ObjectiveTrigger submit = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.submitCase, "Submit case sidejob", newProgressAdd: 1f);
                            AddObjective(currentBlock.name, submit, false, pointerPosition: closestHandIn.GetWorldPosition(), useIcon: InterfaceControls.Icon.location, onCompleteAction: Objective.OnCompleteAction.submitSideJob, delay: currentBlock.objectiveDelay);
                        }
                    }
                }
                else if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.submitToPoster)
                {
                    if(!triggerHandIn)
                    {
                        //Get dialog
                        JobPreset.DialogReference dialogPreset = preset.dialogReferences.Find(item => item.name.ToLower() == currentBlock.dialogReference.ToLower());

                        if (dialogPreset != null)
                        {
                            triggerHandIn = true;

                            AddDialogOption(poster, Evidence.DataKey.photo, dialogPreset.dialog);

                            Objective.ObjectiveTrigger askStaff = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.onDialogSuccess, dialogPreset.dialog.name, newJob: this);

                            //When advancing phase, make sure we also pass the current phase
                            AddObjective(currentBlock.name, askStaff, usePointer: false, onCompleteAction: Objective.OnCompleteAction.submitSideJob, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.citizen, delay: currentBlock.objectiveDelay);
                        }
                        else Game.LogError("Could not find job dialog index " + currentBlock.dialogReference);
                    }
                }
                else if (currentBlock.elementType == SideMissionIntroPreset.SideMissionElementType.telephoneSubmission)
                {
                    if (!triggerHandIn)
                    {
                        //Get dialog
                        JobPreset.DialogReference dialogPreset = preset.dialogReferences.Find(item => item.name.ToLower() == currentBlock.dialogReference.ToLower());

                        if(dialogPreset != null)
                        {
                            thisCase.ValidationCheck();

                            if(thisCase != null && thisCase.handInValid)
                            {
                                //Generate fake number to call
                                GenerateFakeNumber();

                                TelephoneController.Instance.AddFakeNumber(fakeNumber, new TelephoneController.CallSource(TelephoneController.CallType.player, dialogPreset.dialog, this));

                                TelephoneController.Instance.fakeTelephoneDictionary[fakeNumber].dialogGreeting = dialogPreset.dialog;
                                TelephoneController.Instance.fakeTelephoneDictionary[fakeNumber].dialog = dialogPreset.dialog.name;

                                Objective.ObjectiveTrigger callFake = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.makeCall, fakeNumber.ToString(), newJob: this);

                                //When advancing phase, make sure we also pass the current phase
                                AddObjective(currentBlock.name, callFake, usePointer: false, onCompleteAction: Objective.OnCompleteAction.submitSideJob, chapterString: phase.ToString(), useIcon: InterfaceControls.Icon.hand, delay: currentBlock.objectiveDelay);
                            }
                            else
                            {
                                Game.LogError("Cannot progress as case hand in is not valid");
                            }
                        }
                        else
                        {
                            Game.LogError("Could not find job dialog reference " + currentBlock.dialogReference + " in " + preset.name);
                        }
                    }
                }
            }

            if(phaseChanged)
            {
                DisplayResolveObjectivesCheck(); //Check to see if we should display resolve questions...
            }
        }
    }

    public void GenerateHidingLocation()
    {
        List<FurnitureLocation> furnPool = new List<FurnitureLocation>();

        //Chose a secret place to leave item with tag...
        foreach (NewRoom r in CityData.Instance.roomDirectory)
        {
            if (r.nodes.Count <= 0) continue;
            if (r.entrances.Count <= 0) continue;
            if (!r.gameLocation.IsPublicallyOpen(true)) continue;

            //Must contain secret location furniture
            foreach (FurnitureLocation f in r.individualFurniture)
            {
                if (GameplayControls.Instance.secretLocationFurniture.Contains(f.furniture))
                {
                    //The room must contain only one of these to avoid confusion...
                    if (!r.individualFurniture.Exists(item => item.furniture == f.furniture && item != f))
                    {
                        furnPool.Add(f);
                    }
                }
            }
        }

        if (furnPool.Count > 0)
        {
            FurnitureLocation chosenFurn = furnPool[Toolbox.Instance.Rand(0, furnPool.Count)];
            secretLocationFurniture = chosenFurn.id; //Record ID
            secretLocationNode = chosenFurn.anchorNode.nodeCoord;
        }
    }

    public void OnPlayerCall()
    {
        Game.Log("Jobs: On player active call...");

        if (Player.Instance.activeCall != null && gooseChaseCall != null)
        {
            if (Player.Instance.activeCall.from == gooseChaseCall.from && Player.Instance.activeCall.to == gooseChaseCall.to)
            {
                OnGooseChaseSuccess();
                OnGooseChaseEnd();
            }
            else Game.Log("Jobs: Active call is not goose chase call");
        }
    }

    public virtual void OnGooseChaseCallTriggered()
    {

    }

    public virtual void OnGooseChaseSuccess()
    {
        TelephoneController.Instance.OnPlayerCall -= OnPlayerCall;
    }

    public virtual void OnGooseChaseEnd()
    {
        TelephoneController.Instance.OnPlayerCall -= OnPlayerCall;

        //Disenage chosen from phone
        if (gooseChaseFromPhone == 0)
        {
            Interactable ph = null;

            if(CityData.Instance.savableInteractableDictionary.TryGetValue(gooseChaseFromPhone, out ph))
            {
                ph.SetCustomState1(false, null);
            }
        }
    }

    public virtual Human GetExtraPerson1()
    {
        return null;
    }

    public virtual void SubmitCase()
    {
        //Set to submitted
        if (!GameplayController.Instance.caseProcessing.ContainsKey(CasePanelController.Instance.activeCase))
        {
            GameplayController.Instance.caseProcessing.Add(CasePanelController.Instance.activeCase, SessionData.Instance.gameTime);
        }

        CasePanelController.Instance.activeCase.SetStatus(Case.CaseStatus.submitted);

        //Display message
        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "Case Submitted"), InterfaceControls.Icon.resolve);
    }

    public Interactable SpawnJobItem(InteractablePreset spawnItem, JobPreset.JobSpawnWhere spawnWhere, JobPreset.LeadCitizen spawnBelongsTo, JobPreset.LeadCitizen spawnWriter, JobPreset.LeadCitizen spawnReceiver, int security, InteractablePreset.OwnedPlacementRule ownedRule, int priority, JobPreset.JobTag itemTag, bool tryFindExisting)
    {
        Human belongsTo = null;
        Human writer = null;
        Human receiver = null;

        if (spawnBelongsTo == JobPreset.LeadCitizen.poster) belongsTo = poster;
        else if (spawnBelongsTo == JobPreset.LeadCitizen.purp) belongsTo = purp;
        else if (spawnBelongsTo == JobPreset.LeadCitizen.purpsParamour) belongsTo = purp.paramour;

        if (spawnWriter == JobPreset.LeadCitizen.poster) writer = poster;
        else if (spawnWriter == JobPreset.LeadCitizen.purp) writer = purp;
        else if (spawnWriter == JobPreset.LeadCitizen.purpsParamour) writer = purp.paramour;

        if (spawnReceiver == JobPreset.LeadCitizen.poster) receiver = poster;
        else if (spawnReceiver == JobPreset.LeadCitizen.purp) receiver = purp;
        else if (spawnReceiver == JobPreset.LeadCitizen.purpsParamour) receiver = purp.paramour;

        Interactable spawned = null;

        List<Interactable.Passed> passed = new List<Interactable.Passed>();
        passed.Add(new Interactable.Passed(Interactable.PassedVarType.jobID, jobID));
        passed.Add(new Interactable.Passed(Interactable.PassedVarType.jobTag, (int)itemTag));

        NewGameLocation spawnLoc = GetGameLocation(spawnWhere);

        NewNode closestTo = null;

        //Try to spawn closest to goose chase phone
        if(spawnWhere == JobPreset.JobSpawnWhere.nearbyGooseChase && chosenGooseChasePhone != null)
        {
            closestTo = chosenGooseChasePhone.node;
        }

        if(spawnLoc != null)
        {
            if (tryFindExisting)
            {
                spawned = FindExisting(spawnItem, spawnLoc, belongsTo, writer, receiver, itemTag);
            }

            if (spawned == null) spawned = spawnLoc.PlaceObject(spawnItem, belongsTo, writer, receiver, out _, passedVars: passed, forceSecuritySettings: true, forcedSecurity: security, forcedOwnership: ownedRule, forcedPriority: priority, placeClosestTo: closestTo);
        }
        else
        {
            Game.Log("Jobs: Unable to spawn item " + spawnItem.name + " for job " + presetStr + ": Missing location!");
        }

        if (spawned != null)
        {
            Game.Log("Jobs: Successfully spawned item " + spawned.name + " " + spawned.id + " (" + itemTag +") for job " + presetStr + " at " + spawned.wPos + " (" + spawned.node.room.name + ")");

            //This is also called by the interactble setup itself, so don't panic if the below doesn't trigger (it shouldn't)
            if(!activeJobItems.ContainsKey(itemTag))
            {
                activeJobItems.Add(itemTag, spawned);
            }
        }
        else
        {
            Game.Log("Jobs: Unable to spawn item " + spawnItem.name + " for job " + presetStr + " at location " + spawnLoc);
        }

        return spawned;
    }

    public Interactable FindExisting(InteractablePreset what, NewGameLocation location, Human belongsTo, Human writer, Human receiver, JobPreset.JobTag itemTag)
    {
        Interactable ret = null;

        foreach(NewRoom r in location.rooms)
        {
            foreach(NewNode n in r.nodes)
            {
                foreach(Interactable i in n.interactables)
                {
                    if(i.preset == what)
                    {
                        if(i.belongsTo == belongsTo)
                        {
                            if(i.writer == writer)
                            {
                                if(i.reciever == receiver)
                                {
                                    if(ret.pv != null)
                                    {
                                        //Cannot be an existing job item
                                        if(ret.pv.Exists(item => item.varType == Interactable.PassedVarType.jobID) || ret.pv.Exists(item => item.varType == Interactable.PassedVarType.jobTag))
                                        {
                                            continue;
                                        }
                                    }

                                    ret = i;

                                    //Add job tags
                                    ret.pv.Add(new Interactable.Passed(Interactable.PassedVarType.jobID, jobID));
                                    ret.pv.Add(new Interactable.Passed(Interactable.PassedVarType.jobTag, (int)itemTag));
                                    ret.jobParent = this;

                                    Game.Log("Jobs: Found item " + ret.GetName() + " " + ret.id + " for job " + presetStr + " " + jobID);

                                    break;
                                }
                            }
                        }
                    }
                }

                if (ret != null) break;
            }

            if (ret != null) break;
        }

        return ret;
    }

    public void SetJobState(JobState newState, bool forceUpdate = false)
    {
        if(state != newState || forceUpdate)
        {
            state = newState;
            Game.Log("Jobs: Set job " + preset.name + " " + jobID + " state to " + state);

            if(state == JobState.generated || state == JobState.posted)
            {
                //Add to active jobs...
                SideJobController.JobTracking tracking = SideJobController.Instance.jobTracking.Find(item => item.preset == preset);

                tracking.endedJobs.Remove(this);

                if(!tracking.activeJobs.Contains(this))
                {
                    tracking.activeJobs.Add(this);
                }

                if(motive.posterIsExemptFromPostingOtherJobs)
                {
                    SideJobController.Instance.AddExemptFromPostersJob(poster, this);
                }

                if (motive.posterIsExemptFromPurpingOtherJobs)
                {
                    SideJobController.Instance.AddExemptFromPurpJob(poster, this);
                }

                if (motive.purpIsExemptFromPostingOtherJobs)
                {
                    SideJobController.Instance.AddExemptFromPostersJob(purp, this);
                }

                if (motive.purpIsExemptFromPurpingOtherJobs)
                {
                    SideJobController.Instance.AddExemptFromPurpJob(purp, this);
                }
            }
            else if(state == JobState.ended)
            {
                //Add to ended jobs...
                SideJobController.JobTracking tracking = SideJobController.Instance.jobTracking.Find(item => item.preset == preset);

                tracking.activeJobs.Remove(this);

                if (!tracking.endedJobs.Contains(this))
                {
                    tracking.endedJobs.Add(this);
                }

                //Check for exemptions
                if (motive.posterIsExemptFromPostingOtherJobs)
                {
                    SideJobController.Instance.RemoveExemptFromPosters(poster, this);
                }

                if (motive.posterIsExemptFromPurpingOtherJobs)
                {
                    SideJobController.Instance.RemoveExemptFromPurps(poster, this);
                }

                if (motive.purpIsExemptFromPostingOtherJobs)
                {
                    SideJobController.Instance.RemoveExemptFromPosters(purp, this);
                }

                if (motive.purpIsExemptFromPurpingOtherJobs)
                {
                    SideJobController.Instance.RemoveExemptFromPurps(purp, this);
                }
            }
        }
    }

    public void SetupNonSerializedData()
    {
        phaseChange = -1;

        if (activeJobItems == null) activeJobItems = new Dictionary<JobPreset.JobTag, Interactable>();
        if (objectiveReference == null) objectiveReference = new Dictionary<string, List<Objective>>();

        if (!CityData.Instance.savableInteractableDictionary.TryGetValue(postID, out post))
        {
            Game.Log("Misc Error: Unable to find post for job: " + postID);
        }

        Toolbox.Instance.LoadDataFromResources<JobPreset>(presetStr, out preset);
        Toolbox.Instance.LoadDataFromResources<MotivePreset>(motiveStr, out motive);
        CityData.Instance.GetHuman(posterID, out poster);
        CityData.Instance.GetHuman(purpID, out purp);

        //Load dialog additions
        foreach(AddedDialog ad in dialog)
        {
            //Setup serialized...
            Human person = ad.GetHuman();
            DialogPreset dia = ad.GetDialog();
            NewRoom room = ad.GetRoom();

            if(person != null && dia != null) ad.option = person.evidenceEntry.AddDialogOption(ad.key, dia, this, room);
        }

        if (thisCase == null && caseID > -1) thisCase = CasePanelController.Instance.activeCases.Find(item => item.id == caseID);

        if(gooseChasePhone != 0)
        {
            if (!CityData.Instance.savableInteractableDictionary.TryGetValue(gooseChasePhone, out chosenGooseChasePhone))
            {
                Game.LogError("Jobs: Unable to retrieve goose chance phone at " + gooseChasePhone + ", attempting to find outside of dictionary...");
                chosenGooseChasePhone = CityData.Instance.interactableDirectory.Find(item => item.id == gooseChasePhone);
            }
        }

        if (meetingPoint != 0)
        {
            if(!CityData.Instance.savableInteractableDictionary.TryGetValue(meetingPoint, out chosenMeetingPoint))
            {
                Game.LogError("Jobs: Unable to retrieve meeting point at " + meetingPoint + ", attempting to find outside of dictionary...");
                chosenMeetingPoint = CityData.Instance.interactableDirectory.Find(item => item.id == meetingPoint);
            }
        }

        //Load chosen intro
        if (intro != null && intro.Length > 0)
        {
            foreach(JobPreset.IntroConfig intr in preset.compatibleIntros)
            {
                if(intr.preset.name.ToLower() == intro.ToLower())
                {
                    chosenIntro = intr;
                    break;
                }
            }

            if (chosenIntro == null) Game.LogError("Unable to get intro " + intro);
        }

        if (handIn != null && handIn.Length > 0)
        {
            foreach (JobPreset.HandInConfig h in preset.compatibleHandIns)
            {
                if (h.preset.name.ToLower() == handIn.ToLower())
                {
                    chosenHandIn = h;
                    break;
                }
            }

            if (chosenHandIn == null) Game.LogError("Unable to get hand in " + handIn);
        }

        GenerateFakeNumber();
    }

    //Trigger completion of job
    public virtual void Complete()
    {
        OnRewarded();

        //Set new difficulty level
        GameplayController.Instance.SetJobDifficultyLevel(Mathf.Max(GameplayController.Instance.jobDifficultyLevel, preset.GetDifficultyValue()));

        End();
    }

    public virtual void End()
    {
        TelephoneController.Instance.RemoveFakeNumber(fakeNumber); //Remove the fake number

        List<Objective> objectives = new List<Objective>();

        foreach(KeyValuePair<string, List<Objective>> pair in objectiveReference)
        {
            objectives.AddRange(pair.Value);
        }

        foreach (Objective obj in objectives)
        {
            if (!obj.isComplete)
            {
                foreach (Objective.ObjectiveTrigger trigger in obj.queueElement.triggers)
                {
                    if (!trigger.triggered)
                    {
                        if (trigger.triggerType == Objective.ObjectiveTriggerType.onCompleteJob)
                        {
                            trigger.Trigger(true); //Passing as setup will stop the objectives from affecting anything
                        }
                    }
                }

                obj.CheckingLoop(); //Run the checking loop to complete
            }
        }

        //Cancel any objectives remaining...
        foreach (Objective obj in objectives)
        {
            if (!obj.isComplete)
            {
                obj.Cancel();
            }
        }

        for (int i = 0; i < dialog.Count; i++)
        {
            AddedDialog ad = dialog[i];
            Human h = ad.GetHuman();

            if(h != null && ad.option != null)
            {
                List<Evidence.DataKey> toRemoveKeys = new List<Evidence.DataKey>();
                List<EvidenceWitness.DialogOption> toRemove = new List<EvidenceWitness.DialogOption>();

                foreach (KeyValuePair<Evidence.DataKey, List<EvidenceWitness.DialogOption>> opt in h.evidenceEntry.dialogOptions)
                {
                    foreach (EvidenceWitness.DialogOption o in opt.Value)
                    {
                        if(ad.option == o)
                        {
                            toRemoveKeys.Add(opt.Key);
                            toRemove.Add(o);
                        }
                    }
                }

                for (int r = 0; r < toRemoveKeys.Count; r++)
                {
                    h.evidenceEntry.dialogOptions[toRemoveKeys[r]].Remove(toRemove[r]);
                }
            }
        }

        dialog.Clear();

        //Be sure to remove post
        if(post != null)
        {
            post.RemoveFromPlacement();
        }

        //Mark any spawned items as trash
        foreach(KeyValuePair<JobPreset.JobTag, Interactable> pair in activeJobItems)
        {
            if (pair.Value != null)
            {
                pair.Value.MarkAsTrash(true);
            }
        }

        SetJobState(JobState.ended);
    }

    public virtual void OnRewarded()
    {
        if (rewardSyncDisk != null && rewardSyncDisk.Length > 0)
        {
            SyncDiskPreset rewardDisk = Toolbox.Instance.allSyncDisks.Find(item => item.name == rewardSyncDisk);

            if (rewardDisk != null)
            {
                bool foundValidRewardLocation = false;
                Interactable spawnedReward = null;
                Vector3 worldLoc = Player.Instance.transform.position + new Vector3(Toolbox.Instance.Rand(-0.2f, 0.2f), 0, Toolbox.Instance.Rand(-0.2f, 0.2f));

                if (preset.physicalRewardLocation == JobPreset.RewardLocation.postersMailbox)
                {
                    Interactable mailboxDoor = Toolbox.Instance.GetMailbox(poster);

                    if (mailboxDoor != null)
                    {
                        //Unlock the door
                        mailboxDoor.SetLockedState(false, null, false, true);

                        spawnedReward = mailboxDoor.node.gameLocation.PlaceObject(rewardDisk.interactable, poster, poster, Player.Instance, out _, passedObject: rewardDisk, forceSecuritySettings: true, forcedOwnership: InteractablePreset.OwnedPlacementRule.ownedOnly, forcedPriority: 3, ignoreLimits: true);

                        if (spawnedReward != null)
                        {
                            foundValidRewardLocation = true;
                            spawnedReward.SetOwner(Player.Instance); //Set ownership to player so this isn't stealing...

                            Objective.ObjectiveTrigger pickUpReward = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.itemInInventory, "", newInteractable: spawnedReward);
                            AddObjective("Pick up your reward " + preset.physicalRewardLocation.ToString(), pickUpReward, true, pointerPosition: spawnedReward.GetWorldPosition(), useIcon: InterfaceControls.Icon.star, onCompleteAction: Objective.OnCompleteAction.nothing);
                        }
                    }
                    else Game.LogError("Jobs: Unable to locate poster's mailbox: " + poster.name);
                }
                else if (preset.physicalRewardLocation == JobPreset.RewardLocation.playersMailbox)
                {
                    Interactable mailboxDoor = Toolbox.Instance.GetMailbox(Player.Instance);

                    if (mailboxDoor != null)
                    {
                        //Unlock the door
                        mailboxDoor.SetLockedState(false, null, false, true);

                        spawnedReward = mailboxDoor.node.gameLocation.PlaceObject(rewardDisk.interactable, poster, poster, Player.Instance, out _, passedObject: rewardDisk, forceSecuritySettings: true, forcedOwnership: InteractablePreset.OwnedPlacementRule.ownedOnly, forcedPriority: 3, ignoreLimits: true);

                        if (spawnedReward != null)
                        {
                            foundValidRewardLocation = true;
                            spawnedReward.SetOwner(Player.Instance); //Set ownership to player so this isn't stealing...

                            Objective.ObjectiveTrigger pickUpReward = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.itemInInventory, "", newInteractable: spawnedReward);
                            AddObjective("Pick up your reward " + preset.physicalRewardLocation.ToString(), pickUpReward, true, pointerPosition: spawnedReward.GetWorldPosition(), useIcon: InterfaceControls.Icon.star, onCompleteAction: Objective.OnCompleteAction.nothing);
                        }
                    }
                    else Game.LogError("Jobs: Unable to locate poster's mailbox: " + poster.name);
                }

                //Place here if the first two locations fail...
                if(preset.physicalRewardLocation == JobPreset.RewardLocation.cityHallDesk || spawnedReward == null)
                {
                    Interactable handIn = thisCase.GetClosestHandIn();

                    if(handIn != null)
                    {
                        if(handIn.furnitureParent != null)
                        {
                            List<FurniturePreset.SubObject> getLocs = handIn.furnitureParent.furniture.subObjects.FindAll(item => item.preset.name == "MissionReward");

                            if (getLocs.Count > 0)
                            {
                                FurniturePreset.SubObject chosenSO = null;

                                List<FurniturePreset.SubObject> freeSpace = new List<FurniturePreset.SubObject>();

                                foreach(FurniturePreset.SubObject i in getLocs)
                                {
                                    if(!handIn.furnitureParent.spawnedInteractables.Exists(item => item.subObject == i))
                                    {
                                        freeSpace.Add(i);
                                    }
                                }

                                if (freeSpace.Count > 0) chosenSO = freeSpace[Toolbox.Instance.Rand(0, freeSpace.Count)];
                                else chosenSO = getLocs[Toolbox.Instance.Rand(0, getLocs.Count)];

                                spawnedReward = InteractableCreator.Instance.CreateFurnitureSpawnedInteractable(rewardDisk.interactable, handIn.furnitureParent, chosenSO, Player.Instance, null, null, null, null, passedObject: rewardDisk);

                                if(spawnedReward != null)
                                {
                                    foundValidRewardLocation = true;
                                    spawnedReward.SetOwner(Player.Instance); //Set ownership to player so this isn't stealing...

                                    Objective.ObjectiveTrigger pickUpReward = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.itemInInventory, "", newInteractable: spawnedReward);
                                    AddObjective("Pick up your reward " + JobPreset.RewardLocation.cityHallDesk.ToString(), pickUpReward, true, pointerPosition: spawnedReward.GetWorldPosition(), useIcon: InterfaceControls.Icon.star, onCompleteAction: Objective.OnCompleteAction.nothing);
                                }
                            }
                        }
                    }
                }

                if(spawnedReward == null)
                {
                    spawnedReward = InteractableCreator.Instance.CreateWorldInteractable(rewardDisk.interactable, Player.Instance, null, null, worldLoc, Vector3.zero, null, passedObject: rewardDisk);

                    if(spawnedReward != null && !foundValidRewardLocation)
                    {
                        spawnedReward.SetOwner(Player.Instance); //Set ownership to player so this isn't stealing...
                        spawnedReward.SetSpawnPositionRelevent(false); //Set spawn to irrelevent so there's no 'put back' action

                        //If unable to pick up then drop on floor...
                        if (!FirstPersonItemController.Instance.PickUpItem(spawnedReward, true, false))
                        {
                            spawnedReward.ForcePhysicsActive(false, false);
                        }
                    }
                }

                //Spawn objective
                if(spawnedReward != null && spawnedReward.inInventory == null)
                {
                    Objective.ObjectiveTrigger pickUpReward = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.itemInInventory, "", newInteractable: spawnedReward);

                    if(!foundValidRewardLocation)
                    {
                        AddObjective("Pick up your reward", pickUpReward, true, pointerPosition: spawnedReward.GetWorldPosition(), useIcon: InterfaceControls.Icon.star, onCompleteAction: Objective.OnCompleteAction.nothing);
                    }
                }
            }
        }


    }

    //Either immediately spawn job post or send citizen to post it...
    public virtual void PostJob()
    {
        Game.Log("Jobs: Posting job " + presetStr + " (Boards: " + CityData.Instance.jobBoardsDirectory.Count + ") by poster " + poster.name);
        FurnitureLocation jobBoard = null;

        List<FurnitureLocation> validPool = new List<FurnitureLocation>();

        //Focus on empty boards first...
        //Try and populate all boards with at least 1 job.
        foreach (FurnitureLocation board in CityData.Instance.jobBoardsDirectory)
        {
            if (board.spawnedInteractables.FindAll(item => item.jobParent != null).Count <= 0)
            {
                validPool.Add(board);

                //Add extra chances based on proximity to poster...
                float distance = 0;
                if (poster.home != null) distance = Vector2.Distance(board.anchorNode.tile.cityTile.cityCoord, poster.home.building.cityTile.cityCoord);
                int proxyChance = Mathf.Clamp(4 - Mathf.RoundToInt(distance), 0, 4);

                for (int i = 0; i < proxyChance; i++)
                {
                    validPool.Add(board);
                }
            }
        }

        //If there are valid empty boards, then great! Use one...
        if (validPool.Count > 0)
        {
            jobBoard = validPool[Toolbox.Instance.Rand(0, validPool.Count)];
        }

        if(jobBoard == null)
        {
            Game.Log("Jobs: No empty job boards found. Using a populated one...");

            //If this is reached, it means there are no empty boards. In this case we should pick the closest as long as there's room...
            foreach (FurnitureLocation board in CityData.Instance.jobBoardsDirectory)
            {
                //This is automatically an option if empty...
                if (board.spawnedInteractables.FindAll(item => item.jobParent != null).Count < 5)
                {
                    validPool.Add(board);

                    //Add extra chances based on proximity to poster...
                    float distance = 0f;
                    if (poster.home != null) distance = Vector2.Distance(board.anchorNode.tile.cityTile.cityCoord, poster.home.building.cityTile.cityCoord);
                    int proxyChance = Mathf.Clamp(4 - Mathf.RoundToInt(distance), 0, 4);

                    for (int i = 0; i < proxyChance; i++)
                    {
                        validPool.Add(board);
                    }
                }
            }

            //If there are valid empty boards, then great! Use one...
            if (validPool.Count > 0)
            {
                jobBoard = validPool[Toolbox.Instance.Rand(0, validPool.Count)];
            }
        }

        if(jobBoard == null)
        {
            Game.Log("Jobs: No empty job boards found. Picking a random city-wide one (" + CityData.Instance.jobBoardsDirectory.Count + ")");

            //If we're at this point then just pick a random board...
            if (CityData.Instance.jobBoardsDirectory.Count > 0)
            {
                jobBoard = CityData.Instance.jobBoardsDirectory[Toolbox.Instance.Rand(0, CityData.Instance.jobBoardsDirectory.Count)];
            }
        }

        //We should have found a job board at this point!
        //Spawn a job post immediately...
        if(postImmediately)
        {
            List<FurniturePreset.SubObject> placePool = new List<FurniturePreset.SubObject>();

            for (int i = 0; i < jobBoard.furniture.subObjects.Count; i++)
            {
                FurniturePreset.SubObject so = jobBoard.furniture.subObjects[i];
                if (!preset.jobPosting.subObjectClasses.Contains(so.preset)) continue; //class must match...
                if (jobBoard.spawnedInteractables.Exists(item => item.subObject == so)) continue; //item is already here...
                placePool.Add(so);
            }

            //chosen position
            if(placePool.Count > 0)
            {
                FurniturePreset.SubObject chosenLocation = placePool[Toolbox.Instance.Rand(0, placePool.Count)];

                List<Interactable.Passed> spawnPass = new List<Interactable.Passed>();
                spawnPass.Add(new Interactable.Passed(Interactable.PassedVarType.jobID, jobID));

                //Immediately create post...
                post = InteractableCreator.Instance.CreateFurnitureSpawnedInteractable(preset.jobPosting, jobBoard, chosenLocation, poster, poster, null, spawnPass, null, null, ddsOverride: preset.startingScenarios[startingScenario].dds);
                postID = post.id;
                Game.Log("Jobs: Immediately posted job at " + jobBoard.anchorNode.gameLocation.name);

                SetJobState(JobState.posted);
            }
            else
            {
                Game.Log("Jobs: Placement pool empty!");
            }
        }
        else
        {
            //Tell citizen to post the job...
            NewAIGoal newPostGoal = poster.ai.CreateNewGoal(RoutineControls.Instance.postJob, 0f, 0f, newPassedGameLocation: jobBoard.anchorNode.gameLocation);
            newPostGoal.jobID = jobID; //Make sure to pass job ID, this will be used later!
            poster.ai.AITick();
            Game.Log("Jobs: Citizen " + poster.name + " will manually post job at " + jobBoard.anchorNode.gameLocation.name);
        }
    }

    //Accept the job
    public virtual void AcceptJob()
    {
        if(!accepted && CasePanelController.Instance.activeCases.Count < GameplayControls.Instance.maxCases)
        {
            Game.Log("Jobs: Job " + preset.name + " accepted by player!");

            //Get starting leads
            List<JobPreset.StartingLead> startingLeads = new List<JobPreset.StartingLead>();

            if(startingScenario > -1 && preset.startingScenarios.Count > startingScenario)
            {
                startingLeads = preset.startingScenarios[startingScenario].leads;
            }
            else
            {
                Game.LogError("Unable to get starting lead at index " + startingScenario + " for job " + preset.name);
            }

            //Create a new case board
            thisCase = CasePanelController.Instance.CreateNewCase(Case.CaseType.sideJob, Case.CaseStatus.handInNotCollected, true, Strings.Get("missions.postings", preset.caseName));

            if(thisCase != null)
            {
                accepted = true;

                caseID = thisCase.id;
                thisCase.job = this;
                thisCase.jobReference = jobID;

                //Add leads
                ApplyLeads(ref startingLeads);

                //Pin job note
                CasePanelController.Instance.PinToCasePanel(CasePanelController.Instance.activeCase, post.evidence, Evidence.DataKey.name, true);

                //Copy resolve questions to case
                thisCase.resolveQuestions = new List<Case.ResolveQuestion>(resolveQuestions);
                resolveQuestions.Clear();

                //Update the resolve answers
                UpdateResolveAnswers();

                SetHandIn();

                SessionData.Instance.TutorialTrigger("sidejobs");
            }
            else
            {
                Game.LogError("Unable to create new case for side job " + preset.name);
            }
        }
        else
        {
            Game.LogError("Too many active cases to accept side job " + preset.name);
        }
    }

    public virtual void SetHandIn()
    {
        //Add hand-in location
        thisCase.handIn.Clear();

        if(chosenHandIn.preset.postersDoor)
        {
            foreach (NewNode.NodeAccess n in poster.home.entrances)
            {
                if (n.door != null)
                {
                    if (n.door.peekInteractable != null)
                    {
                        thisCase.handIn.Add(-n.door.wall.id); //As the peek interactables have no proper interactable ID, use the MINUS value of the wall ID instead...
                    }
                }
            }
        }

        if(chosenHandIn.preset.cityHall)
        {
            foreach (Interactable i in CityData.Instance.caseTrays)
            {
                thisCase.handIn.Add(i.id);
            }
        }
    }

    //Same as above but using just one trigger
    public virtual void AddObjective(string entryRef, Objective.ObjectiveTrigger trigger, bool usePointer = false, Vector3 pointerPosition = new Vector3(), InterfaceControls.Icon useIcon = InterfaceControls.Icon.lookingGlass, Objective.OnCompleteAction onCompleteAction = Objective.OnCompleteAction.nextChapterPart, float delay = 0f, bool removePrevious = false, string chapterString = "", bool isSilent = false, bool allowCrouchPromt = false)
    {
        //Only add objective if it's not already created (helps with save games)
        if(thisCase != null)
        {
            Game.Log("Jobs: Job " + jobID + " Adding objective: " + entryRef);
            thisCase.AddObjective(entryRef, trigger, usePointer, pointerPosition, useIcon, onCompleteAction, delay, removePrevious, chapterString, isSilent, allowCrouchPromt, this);
        }
        else
        {
            Game.LogError("Jobs: Trying to create objective " + entryRef + " for job " + jobID +" but no case is assigned to the job!");
        }
    }

    //Called when a new objective is added or it's status has changed...
    public virtual void OnObjectiveChange()
    {
        if(OnObjectivesChanged != null)
        {
            OnObjectivesChanged();
        }
    }

    //Add a dialogue option manually
    public virtual void AddDialogOption(Human person, Evidence.DataKey key, DialogPreset newPreset, NewRoom roomRef = null)
    {
        EvidenceWitness.DialogOption option = person.evidenceEntry.AddDialogOption(key, newPreset, this, roomRef);

        if(option != null)
        {
            AddedDialog newD = new AddedDialog();
            newD.humanID = person.humanID;
            newD.dialogRef = newPreset.name;
            newD.key = key;
            if(roomRef != null)newD.roomID = roomRef.roomID;

            dialog.Add(newD);
        }
    }

    public virtual void OnAcquireJobInfo(DialogPreset dialog)
    {
        Game.Log("Jobs: Acquire job info through dialog...");

        string passedMsg = string.Empty;

        if(dialog != null)
        {
            AIActionPreset.AISpeechPreset resp = dialog.responses.Find(item => item.isSuccessful);
            if (resp != null) passedMsg = resp.ddsMessageID;
        }

        OnAcquireJobInfo(passedMsg);
    }

    //Triggered when the player discovers details about the job (eg phone or ask behind the bar)
    public virtual void OnAcquireJobInfo(string infoDialogMessage)
    {
        if (!knowHandInLocation)
        {
            Game.Log("Jobs: OnAcquireJobInfo");

            knowHandInLocation = true;
            jobInfoDialogMsg = infoDialogMessage;

            thisCase.SetStatus(Case.CaseStatus.handInCollected);

            //Create suspect fact
            CreateAcqusitionFacts();

            ApplyLeads(ref preset.informationAcquisitionLeads);

            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "Job information updated!"), InterfaceControls.Icon.resolve);

            //Fire event
            if (AcquireInfo != null)
            {
                AcquireInfo();
            }
        }
    }

    public void CreateAcqusitionFacts()
    {
        if(knowHandInLocation)
        {
            //Create suspect fact
            foreach (JobPreset.FactCreation f in preset.createFactsOnInformationAcquisition)
            {
                Evidence from = GetEvidence(f.from);
                Evidence to = GetEvidence(f.to);

                List<Evidence.DataKey> fromKeyOverride = null;
                List<Evidence.DataKey> toKeyOverride = null;

                if(f.overrideFromKeys)
                {
                    fromKeyOverride = new List<Evidence.DataKey>();

                    if (f.featureKeysFromLeadPool && leadKeys.Count > 0)
                    {
                        fromKeyOverride.AddRange(leadKeys);
                    }
                    else fromKeyOverride.AddRange(f.fromKeys);
                }

                if (f.overrideToKeys)
                {
                    toKeyOverride = new List<Evidence.DataKey>();

                    if (f.featureKeysFromLeadPoolTo && leadKeys.Count > 0)
                    {
                        toKeyOverride.AddRange(leadKeys);
                    }
                    else toKeyOverride.AddRange(f.toKeys);
                }

                Fact newFact = EvidenceCreator.Instance.CreateFact(f.factPreset.name, from, to, forceDiscoveryOnCreate: true, overrideFromKeys: fromKeyOverride, overrideToKeys: toKeyOverride);
                if (newFact != null) newFact.SetFound(true);
            }
        }
    }

    private void PickPoolLeadOptions()
    {
        bool keyChosen = false; //Chose at least one key piece of data...

        for (int u = 0; u < preset.leadPoolData; u++)
        {
            if (purp == null) continue;

            //Chose a basic lead not already chosen
            List<JobPreset.BasicLeadPool> leadPool = JobPreset.BasicLeadPool.GetValues(typeof(JobPreset.BasicLeadPool)).Cast<JobPreset.BasicLeadPool>().ToList();

            foreach (JobPreset.BasicLeadPool existing in appliedBasicLeads)
            {
                if (leadPool.Contains(existing))
                {
                    leadPool.Remove(existing);
                }
            }

            //Check basic leads are valid for application
            for (int i = 0; i < leadPool.Count; i++)
            {
                JobPreset.BasicLeadPool lp = leadPool[i];

                if (lp == JobPreset.BasicLeadPool.jobTitle || lp == JobPreset.BasicLeadPool.salary)
                {
                    if (purp.job == null || purp.job.employer == null)
                    {
                        leadPool.RemoveAt(i);
                        i--;
                    }
                }
                else if (lp == JobPreset.BasicLeadPool.partnerFirstName)
                {
                    if (purp.partner == null)
                    {
                        leadPool.RemoveAt(i);
                        i--;
                    }
                }
                else if (lp == JobPreset.BasicLeadPool.partnerJobTitle)
                {
                    if (purp.partner == null || purp.partner.job == null || purp.partner.job.employer == null)
                    {
                        leadPool.RemoveAt(i);
                        i--;
                    }
                }
                else if (lp == JobPreset.BasicLeadPool.socialClub)
                {
                    if (!purp.groups.Exists(item => item.GetPreset().groupType == GroupPreset.GroupType.interestGroup))
                    {
                        leadPool.RemoveAt(i);
                        i--;
                    }
                }
                else if (lp == JobPreset.BasicLeadPool.partnerSocialClub)
                {
                    if (purp.partner == null || !purp.partner.groups.Exists(item => item.GetPreset().groupType == GroupPreset.GroupType.interestGroup))
                    {
                        leadPool.RemoveAt(i);
                        i--;
                    }
                }
                else if (lp == JobPreset.BasicLeadPool.randomInterest)
                {
                    if (!purp.characterTraits.Exists(item => item.trait.featureInInterestPool))
                    {
                        leadPool.RemoveAt(i);
                        i--;
                    }
                }
                else if (lp == JobPreset.BasicLeadPool.randomAffliction)
                {
                    if (!purp.characterTraits.Exists(item => item.trait.featureInAfflictionPool))
                    {
                        leadPool.RemoveAt(i);
                        i--;
                    }
                }
                else if (lp == JobPreset.BasicLeadPool.notableFeatures)
                {
                    if (!purp.characterTraits.Exists(item => item.trait.name == "Affliction-ShortSighted" || item.trait.name == "Affliction-FarSighted" || item.trait.name == "Quirk-FacialHair"))
                    {
                        leadPool.RemoveAt(i);
                        i--;
                    }
                }
            }

            //Chose a random basic lead
            if (leadPool.Count > 0)
            {
                JobPreset.BasicLeadPool chosenL = leadPool[Toolbox.Instance.Rand(0, leadPool.Count)];

                if(!keyChosen)
                {
                    List<JobPreset.BasicLeadPool> keyInfo = leadPool.FindAll(item => item == JobPreset.BasicLeadPool.hair || item == JobPreset.BasicLeadPool.eyeColour || item == JobPreset.BasicLeadPool.build || item == JobPreset.BasicLeadPool.fingerprint || item == JobPreset.BasicLeadPool.age || item == JobPreset.BasicLeadPool.jobTitle || item == JobPreset.BasicLeadPool.firstNameInitial || item == JobPreset.BasicLeadPool.notableFeatures);

                    if(keyInfo.Count > 0)
                    {
                        chosenL = keyInfo[Toolbox.Instance.Rand(0, keyInfo.Count)];
                        keyChosen = true;
                    }
                }

                appliedBasicLeads.Add(chosenL);

                if (chosenL == JobPreset.BasicLeadPool.age)
                {
                    leadKeys.Add(Evidence.DataKey.age);
                }
                else if (chosenL == JobPreset.BasicLeadPool.build)
                {
                    leadKeys.Add(Evidence.DataKey.build);
                }
                else if (chosenL == JobPreset.BasicLeadPool.eyeColour)
                {
                    leadKeys.Add(Evidence.DataKey.eyes);
                }
                else if (chosenL == JobPreset.BasicLeadPool.fingerprint)
                {
                    leadKeys.Add(Evidence.DataKey.fingerprints);
                }
                else if (chosenL == JobPreset.BasicLeadPool.firstNameInitial)
                {
                    leadKeys.Add(Evidence.DataKey.firstNameInitial);
                }
                else if (chosenL == JobPreset.BasicLeadPool.hair)
                {
                    leadKeys.Add(Evidence.DataKey.hair);
                }
                else if (chosenL == JobPreset.BasicLeadPool.height)
                {
                    leadKeys.Add(Evidence.DataKey.height);
                }
                else if (chosenL == JobPreset.BasicLeadPool.jobTitle)
                {
                    leadKeys.Add(Evidence.DataKey.jobTitle);
                }
                else if (chosenL == JobPreset.BasicLeadPool.partnerFirstName)
                {
                    leadKeys.Add(Evidence.DataKey.partnerFirstName);
                }
                else if (chosenL == JobPreset.BasicLeadPool.partnerJobTitle)
                {
                    leadKeys.Add(Evidence.DataKey.partnerJobTitle);
                }
                else if (chosenL == JobPreset.BasicLeadPool.randomInterest)
                {
                    leadKeys.Add(Evidence.DataKey.randomInterest);
                }
                else if (chosenL == JobPreset.BasicLeadPool.shoeSize)
                {
                    leadKeys.Add(Evidence.DataKey.shoeSize);
                }
                else if (chosenL == JobPreset.BasicLeadPool.socialClub)
                {
                    leadKeys.Add(Evidence.DataKey.randomSocialClub);
                }
                else if (chosenL == JobPreset.BasicLeadPool.partnerSocialClub)
                {
                    leadKeys.Add(Evidence.DataKey.partnerSocialClub);
                }
                else if (chosenL == JobPreset.BasicLeadPool.notableFeatures)
                {
                    leadKeys.Add(Evidence.DataKey.glasses);
                    leadKeys.Add(Evidence.DataKey.facialHair);
                }
                else if (chosenL == JobPreset.BasicLeadPool.salary)
                {
                    leadKeys.Add(Evidence.DataKey.salary);
                }
                else if (chosenL == JobPreset.BasicLeadPool.bloodType)
                {
                    leadKeys.Add(Evidence.DataKey.bloodType);
                }
                else if (chosenL == JobPreset.BasicLeadPool.randomAffliction)
                {
                    leadKeys.Add(Evidence.DataKey.randomAffliction);
                }
                else if (chosenL == JobPreset.BasicLeadPool.handwriting)
                {
                    leadKeys.Add(Evidence.DataKey.handwriting);
                }
            }
        }
    }

    private void ApplyLeads(ref List<JobPreset.StartingLead> leads)
    {
        //Get leads from lead pool selections
        foreach(JobPreset.BasicLeadPool chosenL in appliedBasicLeads)
        {
            if (chosenL == JobPreset.BasicLeadPool.age)
            {
                foreach(Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.age, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.build)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.build, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.eyeColour)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.eyes, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.fingerprint)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.fingerprints, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.firstNameInitial)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.firstNameInitial, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.hair)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.hair, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.height)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.height, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.jobTitle)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.jobTitle, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.partnerFirstName)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.partnerFirstName, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.partnerJobTitle)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.partnerJobTitle, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.randomInterest)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.randomInterest, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.shoeSize)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.shoeSize, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.socialClub)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.randomSocialClub, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.partnerSocialClub)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.partnerSocialClub, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.notableFeatures)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.glasses, key);
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.facialHair, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.salary)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.salary, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.bloodType)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.bloodType, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.randomAffliction)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.randomAffliction, key);
            }
            else if (chosenL == JobPreset.BasicLeadPool.handwriting)
            {
                foreach (Evidence.DataKey key in leadKeys) purp.evidenceEntry.MergeDataKeys(Evidence.DataKey.handwriting, key);
            }
        }

        foreach (JobPreset.StartingLead lead in leads)
        {
            Evidence ev = GetEvidence(lead.leadEvidence);

            if (ev != null)
            {
                //Merge keys...
                foreach (Evidence.DataKey dk in lead.mergeKeys)
                {
                    foreach (Evidence.DataKey dk2 in lead.mergeKeys)
                    {
                        if (dk == dk2) continue;
                        ev.MergeDataKeys(dk, dk2);
                    }
                }

                List<Evidence.DataKey> leadEvKeys = new List<Evidence.DataKey>(lead.keys);
                if (lead.useKeyFromLeadPool && leadKeys.Count > 0) leadEvKeys.AddRange(leadKeys);

                //Add fact link
                foreach(string str in lead.factsReveal)
                {
                    Fact foundFact = null;
                    EvidenceCitizen cit = ev as EvidenceCitizen;

                    if(cit != null && cit.witnessController != null)
                    {
                        if (cit.witnessController.factDictionary.TryGetValue(str, out foundFact))
                        {
                            if (!foundFact.isFound)
                            {
                                foundFact.SetFound(true);
                            }

                            post.evidence.AddFactLink(foundFact, leadEvKeys, false);
                        }
                    }
                }

                //Apply discovery triggers
                foreach (Evidence.Discovery dis in lead.discoveryApplication)
                {
                    ev.AddDiscovery(dis);
                }

                if(lead.autoPin)
                {
                    if (thisCase != null)
                    {
                        CasePanelController.Instance.PinToCasePanel(thisCase, ev, leadEvKeys, forceAutoPin: true);
                    }
                }
            }
        }
    }

    private Evidence GetEvidence(JobPreset.LeadEvidence lead)
    {
        Evidence ev = null;

        if (lead == JobPreset.LeadEvidence.poster && poster != null) ev = poster.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.purp && purp != null) ev = purp.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.purpsParamour && purp != null && purp.paramour != null) ev = purp.paramour.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.postersHome && poster != null && poster.home != null) ev = poster.home.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.purpsHome && purp != null && purp.home != null) ev = purp.home.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.purpsParamourHome && purp != null && purp.paramour != null && purp.paramour.home != null) ev = purp.paramour.home.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.postersWorkplace && poster != null && poster.job != null && poster.job.employer != null && poster.job.employer.address != null) ev = poster.job.employer.address.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.purpsWorkplace && purp != null && purp.job != null && purp.job.employer != null && purp.job.employer.address != null) ev = purp.job.employer.address.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.purpsParamourWorkplace && purp != null && purp.paramour != null && purp.paramour.job != null && purp.paramour.job.employer != null && purp.paramour.job.employer.address != null) ev = purp.paramour.job.employer.address.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.postersBuilding && poster != null && poster.home != null) ev = poster.home.building.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.purpsBuilding && purp != null && purp.home != null) ev = purp.home.building.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.purpsParamourBuilding && purp != null && purp.paramour != null && purp.paramour.home != null) ev = purp.paramour.home.building.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.post && post != null && post.evidence != null) ev = post.evidence;
        else if (lead == JobPreset.LeadEvidence.posterTelephone && poster != null && poster.home != null && poster.home.telephones != null && poster.home.telephones.Count > 0) ev = poster.home.telephones[0].telephoneEntry;
        else if (lead == JobPreset.LeadEvidence.purpsTelephone && purp != null && purp.home != null && purp.home.telephones != null && purp.home.telephones.Count > 0) ev = purp.home.telephones[0].telephoneEntry;
        else if (lead == JobPreset.LeadEvidence.purpsParamourTelephone && purp != null && purp.paramour != null && purp.paramour.home != null && purp.paramour.home.telephones != null && purp.paramour.home.telephones.Count > 0) ev = purp.paramour.home.telephones[0].telephoneEntry;
        else if (lead == JobPreset.LeadEvidence.postersWorkplaceBuilding && poster != null && poster.job != null && poster.job.employer != null && poster.job.employer.address != null) ev = poster.job.employer.address.building.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.purpsWorkplaceBuilding && purp != null && purp.job != null && purp.job.employer != null && purp.job.employer.address != null) ev = purp.job.employer.address.building.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.purpsParamourWorkplaceBuilding && purp != null && purp.paramour != null && purp.paramour.job != null && purp.paramour.job.employer != null && purp.paramour.job.employer.address != null) ev = purp.paramour.job.employer.address.building.evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.extraPerson1 && GetExtraPerson1() != null) ev = GetExtraPerson1().evidenceEntry;
        else if (lead == JobPreset.LeadEvidence.itemA && GetItem(JobPreset.JobTag.A) != null) ev = GetItem(JobPreset.JobTag.A).evidence;
        else if (lead == JobPreset.LeadEvidence.itemB && GetItem(JobPreset.JobTag.B) != null) ev = GetItem(JobPreset.JobTag.B).evidence;
        else if (lead == JobPreset.LeadEvidence.itemC && GetItem(JobPreset.JobTag.C) != null) ev = GetItem(JobPreset.JobTag.C).evidence;
        else if (lead == JobPreset.LeadEvidence.itemD && GetItem(JobPreset.JobTag.D) != null) ev = GetItem(JobPreset.JobTag.D).evidence;
        else if (lead == JobPreset.LeadEvidence.itemE && GetItem(JobPreset.JobTag.E) != null) ev = GetItem(JobPreset.JobTag.E).evidence;

        return ev;
    }

    //Create resolve questions
    public virtual void GenerateResolveQuestions(bool setRewardType)
    {
        reward = 0;
        int mainReward = 0;
        resolveQuestions.Clear();

        foreach(Case.ResolveQuestion rq in preset.resolveQuestions)
        {
            if(rq.onlyCompatibleWithHandIns.Count > 0)
            {
                if (!rq.onlyCompatibleWithHandIns.Exists(item => item.name.ToLower() == handIn.ToLower()))
                {
                    continue;
                }
            }

            if (rq.onlyCompatibleWithIntros.Count > 0)
            {
                if (!rq.onlyCompatibleWithIntros.Exists(item => item.name.ToLower() == intro.ToLower()))
                {
                    continue;
                }
            }

            Case.ResolveQuestion newRq = new Case.ResolveQuestion();
            newRq.name = rq.name;
            newRq.inputType = rq.inputType;
            newRq.tag = rq.tag;
            newRq.rewardRange = rq.rewardRange;
            newRq.isOptional = rq.isOptional;
            newRq.automaticAnswers = new List<Case.AutoCorrectAnswer>(rq.automaticAnswers);
            newRq.icon = rq.icon;
            newRq.useAlternateName = rq.useAlternateName;
            newRq.useName = rq.useName;
            newRq.displayObjective = rq.displayObjective;
            newRq.displayOnlyAtPhase = rq.displayOnlyAtPhase;
            newRq.displayAtPhase = rq.displayAtPhase;
            newRq.objectiveDelay = rq.objectiveDelay;
            newRq.target = rq.target;
            newRq.location = rq.location;

            float rewardMP = 1f;

            //Pick revenge objective & target, location
            if (rq.inputType == Case.InputType.revengeObjective)
            {
                RevengeObjective rev = GetRevengeObjective(newRq);

                if (rev != null)
                {
                    newRq.revengeObjective = rev.name;
                    newRq.icon = rev.icon;

                    float rewardLerp = Toolbox.Instance.Rand(0f, 1f);
                    newRq.revengeObjPassed = Mathf.RoundToInt(Mathf.Lerp(rev.passedNumberRange.x, rev.passedNumberRange.y, rewardLerp) / 50f) * 50;
                    rewardMP = Mathf.Lerp(rev.rewardMultiplier.x, rev.rewardMultiplier.y, rewardLerp);

                    Human cit = null;

                    if (newRq.target == JobPreset.LeadCitizen.poster) cit = poster;
                    else if (newRq.target == JobPreset.LeadCitizen.purp) cit = purp;
                    else if (newRq.target == JobPreset.LeadCitizen.purpsParamour) cit = purp.paramour;
                    
                    if(cit != null)
                    {
                        newRq.revengeObjTarget = cit.humanID;

                        NewGameLocation loc = GetGameLocation(newRq.location);

                        if (loc != null && loc.thisAsAddress != null)
                        {
                            newRq.revengeObjLoc = loc.thisAsAddress.id;
                        }
                    }

                    //Change name to revenge objective
                    if(newRq.useName == Case.RevengeObjectiveName.D0)
                    {
                        newRq.name = rev.d0Name;
                        Game.Log("Objective: Using D0 name for " + rev.name + " on job " + jobID + ": " + newRq.name);
                    }
                    else if(newRq.useName == Case.RevengeObjectiveName.D1)
                    {
                        newRq.name = rev.d1Name;
                        Game.Log("Objective: Using D1 name for " + rev.name + " on job " + jobID + ": " + newRq.name);
                    }
                    else if(newRq.useName == Case.RevengeObjectiveName.IDTarget)
                    {
                        newRq.name = rev.idTargetName;
                        Game.Log("Objective: Using IDTarget name for " + rev.name + " on job " + jobID + ": " + newRq.name);
                    }
                }
            }
            else if(rq.inputType == Case.InputType.objective || rq.inputType == Case.InputType.arrestPurp)
            {
                //This field is used as a reference for these input types
                newRq.revengeObjective = rq.revengeObjective;
            }

            //Pick reward
            if (rewardSyncDisk == null || rewardSyncDisk.Length <= 0)
            {
                newRq.reward = Mathf.RoundToInt((Toolbox.Instance.VectorToRandom(rq.rewardRange) * Game.Instance.jobRewardMultiplier * rewardMP * GameplayControls.Instance.sideJobDifficultyRewardMultiplier.Evaluate(GetDifficulty())) / 50f) * 50;
                newRq.reward = Mathf.RoundToInt(newRq.reward * (1f + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.sideJobPayModifier)));
            }
            else newRq.reward = 0;

            newRq.penalty = Mathf.RoundToInt((Toolbox.Instance.VectorToRandom(rq.penaltyRange) * Game.Instance.jobPenaltyMultiplier * GameplayControls.Instance.sideJobDifficultyRewardMultiplier.Evaluate(GetDifficulty())) / 50f) * 50;

            reward += newRq.reward;

            if(!newRq.isOptional)
            {
                mainReward += newRq.reward;
            }

            resolveQuestions.Add(newRq);
        }

        //Is there a reward for a sync disk? About a 30% chance
        if (setRewardType && (rewardSyncDisk == null || rewardSyncDisk.Length <= 0))
        {
            if (Toolbox.Instance.Rand(0f, 1f) > 0.7f)
            {
                List<SyncDiskPreset> diskPool = new List<SyncDiskPreset>();

                foreach (SyncDiskPreset d in Toolbox.Instance.allSyncDisks)
                {
                    if (d.disabled) continue;
                    if (!d.canBeSideJobReward) continue;
                    if (!preset.allowBlackMarketSyncDiskRewards && d.manufacturer == SyncDiskPreset.Manufacturer.BlackMarket) continue;

                    if (reward >= d.price)
                    {
                        if (UpgradesController.Instance.upgrades.Exists(item => item.preset == d)) continue; //Not applied
                        if (FirstPersonItemController.Instance.slots.Exists(item => item.GetInteractable() != null && item.GetInteractable().syncDisk == d)) continue; //Not in inventory

                        diskPool.Add(d);
                    }
                }

                if(diskPool.Count > 0)
                {
                    rewardSyncDisk = diskPool[Toolbox.Instance.Rand(0, diskPool.Count)].name;

                    //Change reward to 0 for main Q's
                    foreach(Case.ResolveQuestion rQ in resolveQuestions)
                    {
                        if(rQ.isOptional)
                        {
                            rQ.reward = 0;
                        }
                    }
                }
            }
        }
    }

    private RevengeObjective GetRevengeObjective(Case.ResolveQuestion resolveQ)
    {
        if(preset.revengeObjectives.Count <= 0)
        {
            Game.Log("Misc Error: No revenge objectives set for job " + preset.name);
            return null;
        }

        List<RevengeObjective> matchingWithFrequency = new List<RevengeObjective>();

        foreach (RevengeObjective cp in preset.revengeObjectives)
        {
            if (cp.disabled) continue;

            NewGameLocation add = GetGameLocation(resolveQ.location);
            if (add == null) continue; //Needs a valid location

            int traitScore = 0;
            int hexacoScore = 0;

            foreach (RevengeObjective.SpecialConditions sc in cp.specialConditions)
            {
                if(sc == RevengeObjective.SpecialConditions.mustHaveWindows)
                {
                    if (add != null)
                    {
                        //Must have at least 3 windows
                        if (add.entrances.FindAll(item => item.accessType == NewNode.NodeAccess.AccessType.window).Count <= 2)
                        {
                            continue;
                        }
                    }
                    else continue;
                }
            }

            if (cp.useTraits)
            {
                if (poster != null && cp.characterTraitsPoster.Count > 0)
                {
                    poster.outfitController.GetChance(poster, ref cp.characterTraitsPoster, out traitScore);
                }
                else
                {
                    Game.Log("Misc Error: No poster assigned to job " + preset.name + ", unable to calculate trait scores for poster. " + jobID);
                }

                if (purp != null && cp.characterTraitsPurp.Count > 0)
                {
                    int purpScore = 0;
                    poster.outfitController.GetChance(purp, ref cp.characterTraitsPurp, out purpScore);
                    traitScore += purpScore;
                }
                else
                {
                    Game.Log("Misc Error: No purp assigned to job " + preset.name + ", unable to calculate trait scores for purp. " + jobID);
                }
            }

            //Get matching stats...
            if (cp.useHEXACO)
            {
                if(poster != null)
                {
                    int c_femMasc = 10 - Mathf.RoundToInt(Mathf.Abs(cp.feminineMasculine - (poster.genderScale * 10f)));
                    int c_humility = 10 - Mathf.RoundToInt(Mathf.Abs(cp.humility - (poster.humility * 10f)));
                    int c_emotionality = 10 - Mathf.RoundToInt(Mathf.Abs(cp.emotionality - (poster.emotionality * 10f)));
                    int c_extraversion = 10 - Mathf.RoundToInt(Mathf.Abs(cp.extraversion - (poster.extraversion * 10f)));
                    int c_agreeableness = 10 - Mathf.RoundToInt(Mathf.Abs(cp.agreeableness - (poster.agreeableness * 10f)));
                    int c_conscientiousness = 10 - Mathf.RoundToInt(Mathf.Abs(cp.conscientiousness - (poster.conscientiousness * 10f)));
                    int c_creativity = 10 - Mathf.RoundToInt(Mathf.Abs(cp.creativity - (poster.creativity * 10f)));

                    //Add and divide by 6 to give a score out of 10
                    hexacoScore = Mathf.Max(Mathf.FloorToInt((float)(c_humility + c_emotionality + c_extraversion + c_agreeableness + c_conscientiousness + c_creativity + c_femMasc) / 7f), 1);
                }
                else
                {
                    Game.Log("Misc Error: No poster assigned to job " + preset.name + ", unable to calculate HEXACO scores for poster. " + jobID);
                }
            }

            int totalScore = cp.baseChance + traitScore + hexacoScore;

            for (int u = 0; u < totalScore; u++)
            {
                matchingWithFrequency.Add(cp);
            }
        }

        if (matchingWithFrequency.Count > 0)
        {
            return matchingWithFrequency[Toolbox.Instance.Rand(0, matchingWithFrequency.Count)];
        }
        else
        {
            Game.Log("Misc Error: No revenge objective was returned for side job " + preset.name);
        }

        return null;
    }

    private NewGameLocation GetGameLocation(JobPreset.JobSpawnWhere spawnWhere)
    {
        NewGameLocation ret = null;

        if ((spawnWhere == JobPreset.JobSpawnWhere.posterHome && poster != null && poster.home != null) || (spawnWhere == JobPreset.JobSpawnWhere.posterWork && poster.home != null && poster.job.employer == null))
        {
            ret = poster.home;
        }
        else if (spawnWhere == JobPreset.JobSpawnWhere.posterWork && poster != null && poster.job.employer != null)
        {
            ret = poster.job.employer.address;
            if (poster.job.employer.address == null && poster.home != null) ret = poster.home;
        }
        else if ((spawnWhere == JobPreset.JobSpawnWhere.purpHome && purp != null && purp.home != null) || (spawnWhere == JobPreset.JobSpawnWhere.purpWork && purp.home != null && purp.job.employer == null))
        {
            ret = purp.home;
        }
        else if (spawnWhere == JobPreset.JobSpawnWhere.purpWork && purp != null && purp.job.employer != null)
        {
            ret = purp.job.employer.address;
            if (purp.job.employer.address == null && purp.home != null) ret = purp.home;
        }
        else if ((spawnWhere == JobPreset.JobSpawnWhere.purpsParamourHome && purp.paramour != null && purp.paramour.home != null) || (spawnWhere == JobPreset.JobSpawnWhere.purpsParamourWork && purp.paramour.home != null && purp.paramour.job.employer == null))
        {
            ret = purp.paramour.home;
        }
        else if (spawnWhere == JobPreset.JobSpawnWhere.purpsParamourWork && purp.paramour != null && purp.paramour.job.employer != null)
        {
            ret = purp.paramour.job.employer.address;
            if (purp.paramour.job.employer.address == null && purp.paramour.home != null) ret = purp.paramour.home;
        }
        else if(spawnWhere == JobPreset.JobSpawnWhere.hiddenItemPlace)
        {
            //Must be somewhere public
            List<NewGameLocation> pool = new List<NewGameLocation>();

            foreach(StreetController str in CityData.Instance.streetDirectory)
            {
                //Search for correct furntiure spawning...
                foreach (NewRoom r in str.rooms)
                {
                    foreach (FurnitureClusterLocation furn in r.furniture)
                    {
                        foreach (FurnitureLocation obj in furn.clusterList)
                        {
                            List<FurniturePreset.SubObject> subObjs = obj.furniture.subObjects.FindAll(item => item.preset == InteriorControls.Instance.sideJobHiddenItemClass);
                            List<Interactable> existing = obj.spawnedInteractables.FindAll(item => subObjs.Contains(item.subObject));
                            int count = subObjs.Count - existing.Count;

                            //Add a chance for each spawn point
                            if(count > 0)
                            {
                                for (int i = 0; i < count; i++)
                                {
                                    pool.Add(str);
                                }
                            }
                        }
                    }
                }
            }

            foreach (NewGameLocation ad in CityData.Instance.addressDirectory)
            {
                if(ad.IsPublicallyOpen(true))
                {
                    //Search for correct furntiure spawning...
                    foreach (NewRoom r in ad.rooms)
                    {
                        foreach (FurnitureClusterLocation furn in r.furniture)
                        {
                            foreach (FurnitureLocation obj in furn.clusterList)
                            {
                                List<FurniturePreset.SubObject> subObjs = obj.furniture.subObjects.FindAll(item => item.preset == InteriorControls.Instance.sideJobHiddenItemClass);
                                List<Interactable> existing = obj.spawnedInteractables.FindAll(item => subObjs.Contains(item.subObject));
                                int count = subObjs.Count - existing.Count;

                                //Add a chance for each spawn point
                                if (count > 0)
                                {
                                    for (int i = 0; i < count; i++)
                                    {
                                        pool.Add(ad);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(pool.Count > 0)
            {
                ret = pool[Toolbox.Instance.Rand(0, pool.Count)];
            }
        }
        else if(spawnWhere == JobPreset.JobSpawnWhere.nearbyGooseChase)
        {
            if(chosenGooseChasePhone != null)
            {
                //If possible, spawn in same game location...
                foreach (NewRoom r in chosenGooseChasePhone.node.gameLocation.rooms)
                {
                    foreach (FurnitureClusterLocation furn in r.furniture)
                    {
                        foreach (FurnitureLocation obj in furn.clusterList)
                        {
                            List<FurniturePreset.SubObject> subObjs = obj.furniture.subObjects.FindAll(item => item.preset == InteriorControls.Instance.sideJobHiddenItemClass);
                            List<Interactable> existing = obj.spawnedInteractables.FindAll(item => subObjs.Contains(item.subObject));
                            int count = subObjs.Count - existing.Count;

                            //Add a chance for each spawn point
                            if (count > 0)
                            {
                                return chosenGooseChasePhone.node.gameLocation;
                            }
                        }
                    }
                }

                //Must be somewhere public
                List<NewGameLocation> pool = new List<NewGameLocation>();

                foreach (StreetController str in CityData.Instance.streetDirectory)
                {
                    //Search for correct furntiure spawning...
                    foreach (NewRoom r in str.rooms)
                    {
                        foreach (FurnitureClusterLocation furn in r.furniture)
                        {
                            foreach (FurnitureLocation obj in furn.clusterList)
                            {
                                List<FurniturePreset.SubObject> subObjs = obj.furniture.subObjects.FindAll(item => item.preset == InteriorControls.Instance.sideJobHiddenItemClass);
                                List<Interactable> existing = obj.spawnedInteractables.FindAll(item => subObjs.Contains(item.subObject));
                                int count = subObjs.Count - existing.Count;

                                //Add a chance for each spawn point
                                if (count > 0)
                                {
                                    for (int i = 0; i < count; i++)
                                    {
                                        pool.Add(str);
                                    }
                                }
                            }
                        }
                    }
                }

                foreach (NewGameLocation ad in CityData.Instance.addressDirectory)
                {
                    if (ad.IsPublicallyOpen(true))
                    {
                        //Search for correct furntiure spawning...
                        foreach (NewRoom r in ad.rooms)
                        {
                            foreach (FurnitureClusterLocation furn in r.furniture)
                            {
                                foreach (FurnitureLocation obj in furn.clusterList)
                                {
                                    List<FurniturePreset.SubObject> subObjs = obj.furniture.subObjects.FindAll(item => item.preset == InteriorControls.Instance.sideJobHiddenItemClass);
                                    List<Interactable> existing = obj.spawnedInteractables.FindAll(item => subObjs.Contains(item.subObject));
                                    int count = subObjs.Count - existing.Count;

                                    //Add a chance for each spawn point
                                    if (count > 0)
                                    {
                                        for (int i = 0; i < count; i++)
                                        {
                                            pool.Add(ad);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (pool.Count > 0)
                {
                    float score = Mathf.Infinity;
                    NewGameLocation bestLoc = null;

                    foreach(NewGameLocation gl in pool)
                    {
                        if (gl.anchorNode == null) continue;
                        float dist = Vector3.Distance(chosenGooseChasePhone.node.position, gl.anchorNode.position);

                        if(dist < score)
                        {
                            score = dist;
                            bestLoc = gl;
                        }
                    }

                    ret = bestLoc;
                }
            }
            else
            {
                Game.Log("Jobs: Cannot get nearbygoose chase location as the phone reference is null!");
            }
        }

        return ret;
    }

    private Human GetTarget(JobPreset.LeadCitizen who)
    {
        Human ret = null;

        if (who == JobPreset.LeadCitizen.poster) ret = poster;
        else if (who == JobPreset.LeadCitizen.purp) ret = purp;
        else if (who == JobPreset.LeadCitizen.purpsParamour) ret = purp.paramour;

        return ret;
    }

    public Interactable GetItem(JobPreset.JobTag tag)
    {
        Interactable ret = null;
        activeJobItems.TryGetValue(tag, out ret);
        return ret;
    }

    public virtual void UpdateResolveAnswers()
    {
        //Return debug information
        Game.Log("Jobs: " + presetStr + " updating resolve answers...");

        foreach (Case.ResolveQuestion q in thisCase.resolveQuestions)
        {
            //Set automatic answers
            foreach(Case.AutoCorrectAnswer auto in q.automaticAnswers)
            {
                if(poster != null)
                {
                    if(auto == Case.AutoCorrectAnswer.poster)
                    {
                        if (!q.correctAnswers.Contains(poster.humanID.ToString())) q.correctAnswers.Add(poster.humanID.ToString());
                    }
                    else if(auto == Case.AutoCorrectAnswer.posterHome && poster.home != null)
                    {
                        if (!q.correctAnswers.Contains(poster.home.id.ToString())) q.correctAnswers.Add(poster.home.id.ToString());
                    }
                    else if(auto == Case.AutoCorrectAnswer.posterWork && poster.job != null && poster.job.employer != null && poster.job.employer.address != null)
                    {
                        if (!q.correctAnswers.Contains(poster.job.employer.address.id.ToString())) q.correctAnswers.Add(poster.job.employer.address.id.ToString());
                    }
                    else if(auto == Case.AutoCorrectAnswer.posterPhoto || auto == Case.AutoCorrectAnswer.posterHomePhoto || auto == Case.AutoCorrectAnswer.posterWorkPhoto)
                    {
                        List<Interactable> surveillance = CityData.Instance.interactableDirectory.FindAll(item => item.preset == InteriorControls.Instance.surveillancePrintout);

                        foreach(Interactable s in surveillance)
                        {
                            if(s.evidence != null)
                            {
                                EvidenceSurveillance ev = s.evidence as EvidenceSurveillance;

                                if(ev != null && ev.savedCapture != null)
                                {
                                    if(auto == Case.AutoCorrectAnswer.posterPhoto)
                                    {
                                        if(ev.savedCapture.aCap.Exists(item => item.id == poster.humanID))
                                        {
                                            if (!q.correctAnswers.Contains(ev.evID)) q.correctAnswers.Add(ev.evID);
                                        }
                                    }
                                    else
                                    {
                                        NewGameLocation capLocation = ev.savedCapture.GetCaptureGamelocation();

                                        if (auto == Case.AutoCorrectAnswer.posterHomePhoto && capLocation == poster.home)
                                        {
                                            if (!q.correctAnswers.Contains(ev.evID)) q.correctAnswers.Add(ev.evID);
                                        }
                                        else if (auto == Case.AutoCorrectAnswer.posterWorkPhoto && poster.job != null && poster.job.employer != null && capLocation == poster.job.employer.address)
                                        {
                                            if (!q.correctAnswers.Contains(ev.evID)) q.correctAnswers.Add(ev.evID);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (purp != null)
                {
                    if (auto == Case.AutoCorrectAnswer.purp)
                    {
                        if (!q.correctAnswers.Contains(purp.humanID.ToString())) q.correctAnswers.Add(purp.humanID.ToString());
                    }
                    else if (auto == Case.AutoCorrectAnswer.purpHome && purp.home != null)
                    {
                        if (!q.correctAnswers.Contains(purp.home.id.ToString())) q.correctAnswers.Add(purp.home.id.ToString());
                    }
                    else if (auto == Case.AutoCorrectAnswer.purpWork && purp.job != null && purp.job.employer != null && purp.job.employer.address != null)
                    {
                        if (!q.correctAnswers.Contains(purp.job.employer.address.id.ToString())) q.correctAnswers.Add(purp.job.employer.address.id.ToString());
                    }
                    else if (auto == Case.AutoCorrectAnswer.purpPhoto || auto == Case.AutoCorrectAnswer.purpHomePhoto || auto == Case.AutoCorrectAnswer.purpWorkPhoto)
                    {
                        List<Interactable> surveillance = CityData.Instance.interactableDirectory.FindAll(item => item.preset == InteriorControls.Instance.surveillancePrintout);

                        foreach (Interactable s in surveillance)
                        {
                            if (s.evidence != null)
                            {
                                EvidenceSurveillance ev = s.evidence as EvidenceSurveillance;

                                if (ev != null && ev.savedCapture != null)
                                {
                                    if (auto == Case.AutoCorrectAnswer.purpPhoto)
                                    {
                                        if (ev.savedCapture.aCap.Exists(item => item.id == purp.humanID))
                                        {
                                            if (!q.correctAnswers.Contains(ev.evID)) q.correctAnswers.Add(ev.evID);
                                        }
                                    }
                                    else
                                    {
                                        NewGameLocation capLocation = ev.savedCapture.GetCaptureGamelocation();

                                        if (auto == Case.AutoCorrectAnswer.purpHomePhoto && capLocation == purp.home)
                                        {
                                            if (!q.correctAnswers.Contains(ev.evID)) q.correctAnswers.Add(ev.evID);
                                        }
                                        else if (auto == Case.AutoCorrectAnswer.purpWorkPhoto && purp.job != null && purp.job.employer != null && capLocation == purp.job.employer.address)
                                        {
                                            if (!q.correctAnswers.Contains(ev.evID)) q.correctAnswers.Add(ev.evID);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (purp.paramour != null)
                    {
                        if (auto == Case.AutoCorrectAnswer.purpsParamour)
                        {
                            if (!q.correctAnswers.Contains(purp.paramour.humanID.ToString())) q.correctAnswers.Add(purp.paramour.humanID.ToString());
                        }
                        else if (auto == Case.AutoCorrectAnswer.purpsParamourHome && purp.paramour.home != null)
                        {
                            if (!q.correctAnswers.Contains(purp.paramour.home.id.ToString())) q.correctAnswers.Add(purp.paramour.home.id.ToString());
                        }
                        else if (auto == Case.AutoCorrectAnswer.purpsParamourWork && purp.paramour.job != null && purp.paramour.job.employer != null && purp.paramour.job.employer.address != null)
                        {
                            if (!q.correctAnswers.Contains(purp.paramour.job.employer.address.id.ToString())) q.correctAnswers.Add(purp.paramour.job.employer.address.id.ToString());
                        }
                        else if (auto == Case.AutoCorrectAnswer.purpsParamourPhoto || auto == Case.AutoCorrectAnswer.purpsParamourHomePhoto || auto == Case.AutoCorrectAnswer.purpsParamourWorkPhoto)
                        {
                            List<Interactable> surveillance = CityData.Instance.interactableDirectory.FindAll(item => item.preset == InteriorControls.Instance.surveillancePrintout);

                            foreach (Interactable s in surveillance)
                            {
                                if (s.evidence != null)
                                {
                                    EvidenceSurveillance ev = s.evidence as EvidenceSurveillance;

                                    if (ev != null && ev.savedCapture != null)
                                    {
                                        if (auto == Case.AutoCorrectAnswer.purpsParamourPhoto)
                                        {
                                            if (ev.savedCapture.aCap.Exists(item => item.id == purp.paramour.humanID))
                                            {
                                                if (!q.correctAnswers.Contains(ev.evID)) q.correctAnswers.Add(ev.evID);
                                            }
                                        }
                                        else
                                        {
                                            NewGameLocation capLocation = ev.savedCapture.GetCaptureGamelocation();

                                            if (auto == Case.AutoCorrectAnswer.purpsParamourHomePhoto && capLocation == purp.paramour.home)
                                            {
                                                if (!q.correctAnswers.Contains(ev.evID)) q.correctAnswers.Add(ev.evID);
                                            }
                                            else if (auto == Case.AutoCorrectAnswer.purpsParamourWorkPhoto && purp.paramour.job != null && purp.paramour.job.employer != null && capLocation == purp.paramour.job.employer.address)
                                            {
                                                if (!q.correctAnswers.Contains(ev.evID)) q.correctAnswers.Add(ev.evID);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if(auto == Case.AutoCorrectAnswer.spawnedItemTag)
                {
                    Interactable getItem = GetItem(q.tag);

                    if(getItem != null)
                    {
                        Evidence itemEv = getItem.evidence;

                        if(itemEv != null)
                        {
                            if (!q.correctAnswers.Contains(itemEv.evID)) q.correctAnswers.Add(itemEv.evID);
                        }
                        else if (!q.correctAnswers.Contains(getItem.id.ToString())) q.correctAnswers.Add(getItem.id.ToString());
                    }
                }
                else if (auto == Case.AutoCorrectAnswer.spawnedItemA)
                {
                    Interactable getItem = GetItem(JobPreset.JobTag.A);

                    if (getItem != null)
                    {
                        Evidence itemEv = getItem.evidence;

                        if (itemEv != null)
                        {
                            if (!q.correctAnswers.Contains(itemEv.evID)) q.correctAnswers.Add(itemEv.evID);
                        }
                        else if (!q.correctAnswers.Contains(getItem.id.ToString())) q.correctAnswers.Add(getItem.id.ToString());
                    }
                }
                else if (auto == Case.AutoCorrectAnswer.spawnedItemB)
                {
                    Interactable getItem = GetItem(JobPreset.JobTag.B);

                    if (getItem != null)
                    {
                        Evidence itemEv = getItem.evidence;

                        if (itemEv != null)
                        {
                            if (!q.correctAnswers.Contains(itemEv.evID)) q.correctAnswers.Add(itemEv.evID);
                        }
                        else if (!q.correctAnswers.Contains(getItem.id.ToString())) q.correctAnswers.Add(getItem.id.ToString());
                    }
                }
                else if (auto == Case.AutoCorrectAnswer.spawnedItemC)
                {
                    Interactable getItem = GetItem(JobPreset.JobTag.C);

                    if (getItem != null)
                    {
                        Evidence itemEv = getItem.evidence;

                        if (itemEv != null)
                        {
                            if (!q.correctAnswers.Contains(itemEv.evID)) q.correctAnswers.Add(itemEv.evID);
                        }
                        else if (!q.correctAnswers.Contains(getItem.id.ToString())) q.correctAnswers.Add(getItem.id.ToString());
                    }
                }
                else if (auto == Case.AutoCorrectAnswer.spawnedItemD)
                {
                    Interactable getItem = GetItem(JobPreset.JobTag.D);

                    if (getItem != null)
                    {
                        Evidence itemEv = getItem.evidence;

                        if (itemEv != null)
                        {
                            if (!q.correctAnswers.Contains(itemEv.evID)) q.correctAnswers.Add(itemEv.evID);
                        }
                        else if (!q.correctAnswers.Contains(getItem.id.ToString())) q.correctAnswers.Add(getItem.id.ToString());
                    }
                }
                else if (auto == Case.AutoCorrectAnswer.spawnedItemE)
                {
                    Interactable getItem = GetItem(JobPreset.JobTag.E);

                    if (getItem != null)
                    {
                        Evidence itemEv = getItem.evidence;

                        if (itemEv != null)
                        {
                            if (!q.correctAnswers.Contains(itemEv.evID)) q.correctAnswers.Add(itemEv.evID);
                        }
                        else if (!q.correctAnswers.Contains(getItem.id.ToString())) q.correctAnswers.Add(getItem.id.ToString());
                    }
                }
                else if (auto == Case.AutoCorrectAnswer.spawnedItemF)
                {
                    Interactable getItem = GetItem(JobPreset.JobTag.F);

                    if (getItem != null)
                    {
                        Evidence itemEv = getItem.evidence;

                        if (itemEv != null)
                        {
                            if (!q.correctAnswers.Contains(itemEv.evID)) q.correctAnswers.Add(itemEv.evID);
                        }
                        else if (!q.correctAnswers.Contains(getItem.id.ToString())) q.correctAnswers.Add(getItem.id.ToString());
                    }
                }
            }

            //Update validity and correctness...
            q.UpdateValid(thisCase);
            q.UpdateCorrect(thisCase, false);
        }
    }

    public float GetDifficulty()
    {
        float diff = 0f;

        if(preset.difficultyTag == JobPreset.DifficultyTag.D1)
        {
            diff = 1;
        }
        else if(preset.difficultyTag == JobPreset.DifficultyTag.D2A || preset.difficultyTag == JobPreset.DifficultyTag.D2B)
        {
            diff = 2;
        }
        else if(preset.difficultyTag == JobPreset.DifficultyTag.D3)
        {
            diff = 3;
        }
        else if(preset.difficultyTag == JobPreset.DifficultyTag.D4A || preset.difficultyTag == JobPreset.DifficultyTag.D4B || preset.difficultyTag == JobPreset.DifficultyTag.D4C)
        {
            diff = 4;
        }
        else if(preset.difficultyTag == JobPreset.DifficultyTag.D5)
        {
            diff = 5;
        }
        else if(preset.difficultyTag == JobPreset.DifficultyTag.D6)
        {
            diff = 6;
        }

        foreach(JobPreset.BasicLeadPool lead in appliedBasicLeads)
        {
            if(lead == JobPreset.BasicLeadPool.eyeColour)
            {
                diff += 0.7f;
            }
            else if(lead == JobPreset.BasicLeadPool.height)
            {
                diff += 0.7f;
            }
            else if(lead == JobPreset.BasicLeadPool.age)
            {
                diff += 0.7f;
            }
            else if(lead == JobPreset.BasicLeadPool.jobTitle)
            {
                diff += -0.2f;
            }
            else if(lead == JobPreset.BasicLeadPool.randomInterest)
            {
                diff += 0.7f;
            }
            else if(lead == JobPreset.BasicLeadPool.randomAffliction)
            {
                diff += 0.6f;
            }
            else if(lead == JobPreset.BasicLeadPool.socialClub)
            {
                diff += -0.5f;
            }
            else if(lead == JobPreset.BasicLeadPool.partnerSocialClub)
            {
                diff += -0.1f;
            }
            else if(lead == JobPreset.BasicLeadPool.salary)
            {
                diff += 0.5f;
            }
            else if(lead == JobPreset.BasicLeadPool.bloodType)
            {
                diff += 0.5f;
            }
            else if(lead == JobPreset.BasicLeadPool.handwriting)
            {
                diff += 0.5f;
            }
        }

        return diff;
    }

    public void AddConfineLocation(Human who, NewAddress where)
    {
        if(who != null && who.ai != null && where != null)
        {
            who.ai.SetConfineLocation(where);
            confine.Add(new ConfineLocation { id = who.humanID, address = where.id });
        }
    }

    public void RemoveConfineLocation(Human who, NewAddress where)
    {
        if(who != null && who.ai != null && where != null)
        {
            who.ai.SetConfineLocation(null);
            ConfineLocation cf = confine.Find(item => item.id == who.humanID && item.address == where.id);
            if(cf != null) confine.Remove(cf);
        }
    }

    public virtual void DisplayResolveObjectivesCheck()
    {
        if(thisCase != null)
        {
            Game.Log("Jobs: Checking for display " + thisCase.resolveQuestions.Count + " resolve questions at phase " + phase + "...");

            //Create objectives for case
            if(knowHandInLocation)
            {
                foreach (Case.ResolveQuestion q in thisCase.resolveQuestions)
                {
                    if (!q.displayObjective) continue;

                    //Only display at phase
                    if(q.displayOnlyAtPhase)
                    {
                        Game.Log("Display " + q.name + " at phase " + q.displayAtPhase);
                        if (phase < q.displayAtPhase) continue;
                    }

                    if (q.inputType == Case.InputType.objective) continue; //Skip if this already depends on an objective to trigger...

                    //Check against queue; the name will also be checked for duplicates when created proper
                    if(!Player.Instance.speechController.speechQueue.Exists(item => item.entryRef == q.name))
                    {
                        Game.Log("Jobs: Adding auto objective (resolve question): " + q.name + "...");

                        //Game.Log("Creating resolve q objective: " + q.name);
                        Objective.ObjectiveTrigger completedJob = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.onCompleteJob, "complete");
                        thisCase.AddObjective(q.name, completedJob, false, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: q.icon, delay: q.objectiveDelay, jobRef: this);
                        q.OnProgressChange += thisCase.OnQuestionProgressChange;
                    }
                }
            }
        }
    }

    public virtual void TriggerFail(string reason)
    {
        if(!failed)
        {
            failed = true;
            Game.Log("Jobs: " + preset.name + " job failed! " + reason);
            InterfaceController.Instance.ExecuteMissionFailedDisplay(thisCase);
            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.ComposeText(Strings.Get("missions.postings", reason), this, Strings.LinkSetting.forceNoLinks), newIcon: InterfaceControls.Icon.cross);
            End();
        }
    }

    public virtual void OnDestroyMissionObject(Interactable destroyed)
    {
        //Trigger fail upon object destroy...
        if(currentBlock != null)
        {
            foreach (JobPreset.JobTag tag in currentBlock.triggerFailIfItemDestroyed)
            {
                Interactable failIf = null;

                if(activeJobItems.TryGetValue(tag, out failIf))
                {
                    if(destroyed == failIf)
                    {
                        TriggerFail("An important item was destroyed!");
                    }
                }
            }
        }
    }

    public virtual void DebugDisplayAnswers()
    {
        if (poster != null) Game.Log("Jobs: Poster is " + poster.name);
        if (purp != null) Game.Log("Jobs: Purp is " + purp.name);
    }
}
