using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class DebugBlockingSelector : MonoBehaviour
{
    public WalkableRecorder.TileSetup tile;
    public CityData.BlockingDirection dir;
    public MeshRenderer rend;
    public bool blocked = false;
    public WalkableRecorder recorder;
    public Vector2 offset;

    public void Setup(WalkableRecorder.TileSetup newTile, CityData.BlockingDirection newDir, WalkableRecorder newRecorder, Vector2 newOffset)
    {
        tile = newTile;
        dir = newDir;
        offset = newOffset;
        recorder = newRecorder;
        rend = this.gameObject.GetComponent<MeshRenderer>();
    }

    [Button("Set Blocked")]
    public void SetB()
    {
        SetBlocked(true);
    }

    [Button("Set Unblocked")]
    public void SetUB()
    {
        SetBlocked(false);
    }

    private void SetBlocked(bool val)
    {
        blocked = val;

        if(blocked)
        {
            rend.sharedMaterial = recorder.blockedMaterial;
        }
        else
        {
            rend.sharedMaterial = recorder.nonBlockedMaterial;
        }
    }
}
