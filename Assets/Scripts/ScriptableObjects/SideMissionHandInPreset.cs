﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "sidejobhandin_data", menuName = "Database/Side Job Hand-In Preset")]

public class SideMissionHandInPreset : SoCustomComparison
{
    [Header("Rewards")]
    public int rewardModifier = 0;

    [Header("Location")]
    public bool postersDoor = false;
    public bool cityHall = false;

    [Header("Elements")]
    public List<SideMissionIntroPreset.SideMissionObjectiveBlock> blocks = new List<SideMissionIntroPreset.SideMissionObjectiveBlock>();
}
