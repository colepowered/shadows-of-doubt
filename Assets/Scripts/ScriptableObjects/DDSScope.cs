﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using System.Globalization;

//A scriptable object that can be used to make occupation presets.
//Script pass 1
[CreateAssetMenu(fileName = "scope_data", menuName = "Database/DDS Scope")]

public class DDSScope : SoCustomComparison
{
    public enum SpecialCase { none};

    [Header("Setup")]
    public Color colour = Color.white;
    [Tooltip("This can be accessed from any scope")]
    public bool isGlobal = false;
    public SpecialCase specialCase = SpecialCase.none;

    [System.Serializable]
    public class ContainedScope
    {
        public string name;
        public DDSScope type;
    }

    [Header("Content")]
    public List<ContainedScope> containedScopes = new List<ContainedScope>();
    public List<string> containedValues = new List<string>();
}
