﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NewTile
{
    [Header("ID")]
    public int tileID = -1;
    public static int assignID = 0;

    [Header("Transform")]
    public string name;
    public Vector3 position;
    public Transform parent;

    [Header("Location")]
    public NewBuilding building; //Reference to the building
    public NewFloor floor; //Reference to the floor
    public CityTile cityTile; //Reference to city tile
    public Vector2Int floorCoord; //Coordinate within floor
    public Vector3Int globalTileCoord; //Coordinate within all tiles
    public PathFinder.StreetChunk streetChunk; //Used when building streets (only available on city creation)

    [Header("Tile Contents")]
    public List<NewNode> nodes = new List<NewNode>();
    public NewNode anchorNode; //Each location has an anchor node for the pathfinder to use to go to an address (using specific nodes for this would result in a lot of coppied paths)

    [Header("Tile")]
    public bool isSetup = false; //True once setup has been called
    public bool isLoaded = false; //True if this tile has been loaded from city save data
    public bool isOutside = false; //True if this tile is outside, false if it's indoors
    public bool isObstacle = false; //True if the whole tile is considered an obstacle
    //public bool isAlley = false; //True if the all nodes are considered an alley
    //public bool isBackstreet = false; //True if this is street space behind a building
    public bool isMapCorner = false;
    public bool isEdge = false; //True if this reperesents the edge of a groundmap tile (eg road space)
    public int rotation = 0; //Rotation of this tile relative to the address

    public bool isEntrance = false; //True if this is an entrance
    public bool isMainEntrance = false; //True if this is a main entrance
    public NewTile entrancePair; //The 'other' entrance tile
    public bool isStairwell = false; //True if this is a stairwell
    public int stairwellRotation = 0; //Rotation of the stairwell
    public bool isInvertedStairwell = false; //True if this is an inverted stairwell
    public int elevatorRotation = 0; //Rotation of the elevator
    public bool isTop = false; //If true this is the top of the elevator/stairs
    public bool isBottom = false; //If true this is the bottom of the elevator/stairs

    [Header("Roads")]
    public StreetController streetController; //If part of a street

    [Header("Optimization")]
    public bool useOptimizedFloor = false;
    public bool useOptimizedCeiling = false;

    [Header("Spawned Objects")]
    public GameObject entranceArrow;
    public GameObject stairwell;
    public GameObject elevator;
    public Elevator stairwellAssign;

    //Setup for interior tile
    public void SetupInterior(NewFloor newFloor, Vector2Int newCoord, bool newIsEdge)
    {
        floor = newFloor;
        building = floor.building;
        parent = newFloor.gameObject.transform;

        floorCoord = newCoord;
        isEdge = newIsEdge;
        SetAsOutside(isEdge);

        //Calculate the tile coordinate (use this for finding this within all tiles)
        //The building's coord returns the centre while the floor's coord counts from the bottom left
        globalTileCoord = new Vector3Int(building.globalTileCoords.x - Mathf.FloorToInt(CityControls.Instance.tileMultiplier * 0.5f) + floorCoord.x, building.globalTileCoords.y - Mathf.FloorToInt(CityControls.Instance.tileMultiplier * 0.5f) + floorCoord.y, floor.floor);
        //this.transform.localPosition = new Vector3((CityControls.Instance.cityTileSize.x * -0.5f) + (PathFinder.Instance.tileSize.x * 0.5f) + (floorCoord.x * PathFinder.Instance.tileSize.x), 0, (CityControls.Instance.cityTileSize.y * -0.5f) + (PathFinder.Instance.tileSize.y * 0.5f) + (floorCoord.y * PathFinder.Instance.tileSize.y));

        //Set position: It's important that this is done here because things will be parented to this.
        position = CityData.Instance.TileToRealpos(globalTileCoord);

        //Add to floor's tile map
        floor.tileMap.Add(floorCoord, this);

        //Set city tile
        cityTile = floor.building.cityTile;

        CommonSetup();
    }

    //Setup for exterior tile
    public void SetupExterior(CityTile newCityTile, Vector3Int newCityCoord)
    {
        //Add to city tile
        newCityTile.AddOutsideTile(this);
        cityTile = newCityTile;
        isEdge = true; //All exterior tiles are edge tiles

        globalTileCoord = newCityCoord;

        //Set position: It's important that this is done here because things will be parented to this.
        position = CityData.Instance.TileToRealpos(globalTileCoord);

        parent = newCityTile.gameObject.transform;
        SetAsOutside(true);

        CheckOffMap(); //Run this to find out if this is in the corner of the map
        CommonSetup();
    }

    //Run by both of the above
    private void CommonSetup()
    {
        if (isSetup) return;

        if(tileID <= -1)
        {
            //Set ID
            tileID = assignID;
            assignID++;

            //Set name
            name = "Tile " + Mathf.RoundToInt(globalTileCoord.x) + "," + Mathf.RoundToInt(globalTileCoord.y) + "," + Mathf.RoundToInt(globalTileCoord.z) + " edge: " + isEdge; //Name after coordinate
            //this.transform.name = name;
        }

        //Add to pathfinder's map
        if (!PathFinder.Instance.tileMap.ContainsKey(globalTileCoord))
        {
            PathFinder.Instance.tileMap.Add(globalTileCoord, this);
        }

        //Spawn nodes
        //Setup nodes unless this is an edge tile and in-game
        for (int i = 0; i < CityControls.Instance.nodeMultiplier; i++)
        {
            for (int n = 0; n < CityControls.Instance.nodeMultiplier; n++)
            {
                Vector2Int v2 = new Vector2Int(i, n);

                //Skip some nodes above street level
                if(isEdge && !SessionData.Instance.isTestScene)
                {
                    if(globalTileCoord.z != 0)
                    {
                        if(floorCoord.x <= 0 && v2.x < CityControls.Instance.nodeMultiplier - 1)
                        {
                            continue;
                        }
                        else if (floorCoord.x >= CityControls.Instance.tileMultiplier - 1 && v2.x > 0)
                        {
                            continue;
                        }
                        else if (floorCoord.y <= 0 && v2.y < CityControls.Instance.nodeMultiplier - 1)
                        {
                            continue;
                        }
                        else if (floorCoord.y >= CityControls.Instance.tileMultiplier - 1 && v2.y > 0)
                        {
                            continue;
                        }
                    }
                }

                //GameObject newNd = Instantiate(PrefabControls.Instance.node, this.transform);
                //NewNode newNode = newNd.GetComponent<NewNode>();

                NewNode newNode = new NewNode();

                //If floor is null, this must be part of a street...
                if (floor == null || floor.lobbyAddress == null)
                {
                    newNode.Setup(this, streetController, v2);
                }
                else
                {
                    if (isEdge)
                    {
                        //Game.Log("Add to null");
                        newNode.Setup(this, floor.outsideAddress, v2);
                    }
                    else
                    {
                        newNode.Setup(this, floor.lobbyAddress, v2);
                    }
                }

                //Automatically set the centre as the anchor node
                if (i == Mathf.FloorToInt(CityControls.Instance.nodeMultiplier * 0.5f) && n == Mathf.FloorToInt(CityControls.Instance.nodeMultiplier * 0.5f))
                {
                    anchorNode = newNode;
                }
            }
        }

        isSetup = true;
    }

    //Load
    public void LoadPathfindTileData(CitySaveData.TileCitySave data)
    {
        SetAsOutside(data.isOutside);
        globalTileCoord = data.globalTileCoord;
        tileID = data.tileID;
        isOutside = data.isOutside; //True if this tile is outside, false if it's indoors
        isObstacle = data.isObstacle; //True if the whole tile is considered an obstacle
        //isAlley = data.isAlley; //True if the all nodes are considered an alley
        //isBackstreet = data.isBackstreet; //True if this is street space behind a building
        isEdge = data.isEdge; //True if this reperesents the edge of a groundmap tile (eg road space)
    }

    public void LoadExterior(CitySaveData.TileCitySave data)
    {
        tileID = data.tileID;
        floorCoord = data.floorCoord; //Coordinate within floor
        globalTileCoord = data.globalTileCoord; //Coordinate within all tiles
        SetAsOutside(data.isOutside);
        isObstacle = data.isObstacle; //True if the whole tile is considered an obstacle
        //isAlley = data.isAlley; //True if the all nodes are considered an alley
        //isBackstreet = data.isBackstreet; //True if this is street space behind a building
        isEdge = data.isEdge; //True if this reperesents the edge of a groundmap tile (eg road space)
        rotation = data.rotation; //Rotation of this tile relative to the address
        isEntrance = data.isEntrance; //True if this is an entrance
        isMainEntrance = data.isMainEntrance; //True if this is a main entrance
        isStairwell = data.isStairwell; //True if this is a stairwell
        stairwellRotation = data.stairwellRotation; //Rotation of the stairwell
        isInvertedStairwell = data.isElevator; //True if this is an elevator
        elevatorRotation = data.elevatorRotation; //Rotation of the elevator
        isTop = data.isTop; //If true this is the top of the elevator/stairs
        isBottom = data.isBottom; //If true this is the bottom of the elevator/stairs
        //useOptimizedFloor = data.useOptimizedFloor;
        //useOptimizedCeiling = data.useOptimizedCeiling;

        //Add to city tile
        cityTile.AddOutsideTile(this);
        isEdge = true; //All exterior tiles are edge tiles

        globalTileCoord = data.globalTileCoord;

        //Set position: It's important that this is done here because things will be parented to this.
        position = CityData.Instance.TileToRealpos(globalTileCoord);

        parent = cityTile.gameObject.transform;
        SetAsOutside(true);

        CheckOffMap(); //Run this to find out if this is in the corner of the map
        CommonSetup();
        isLoaded = true;
    }

    public void LoadInterior(CitySaveData.TileCitySave data)
    {
        tileID = data.tileID;
        floorCoord = data.floorCoord; //Coordinate within floor
        globalTileCoord = data.globalTileCoord; //Coordinate within all tiles
        SetAsOutside(data.isOutside); //True if this tile is outside, false if it's indoors
        isObstacle = data.isObstacle; //True if the whole tile is considered an obstacle
        //isAlley = data.isAlley; //True if the all nodes are considered an alley
        isMainEntrance = data.isMainEntrance; //True if this is a main entrance
        isEdge = data.isEdge; //True if this reperesents the edge of a groundmap tile (eg road space)
        rotation = data.rotation; //Rotation of this tile relative to the address
        isTop = data.isTop; //If true this is the top of the elevator/stairs
        isBottom = data.isBottom; //If true this is the bottom of the elevator/stairs
        //useOptimizedFloor = data.useOptimizedFloor;
        //useOptimizedCeiling = data.useOptimizedCeiling;

        //Set position: It's important that this is done here because things will be parented to this.
        position = CityData.Instance.TileToRealpos(globalTileCoord);
        parent = cityTile.gameObject.transform;

        SetAsEntrance(data.isEntrance, data.isMainEntrance);

        SetAsStairwell(data.isStairwell, false, data.isElevator);
        SetStairwellRotation(data.stairwellRotation);

        CommonSetup();
        isLoaded = true;
    }

    //Add a node
    public void AddNewNode(NewNode newNode)
    {
        if (!nodes.Contains(newNode))
        {
            //Remove existing
            if (newNode.tile != null) newNode.tile.RemoveNode(newNode);

            nodes.Add(newNode);

            //Set location info
            newNode.tile = this;
            newNode.floor = floor;
            newNode.building = building;
            newNode.SetAsObstacle(isObstacle);
            //newNode.SetAsAlley(isAlley);
            //newNode.SetAsBackstreet(isBackstreet);
            newNode.SetAsOutside(isOutside);

            //Physically parent
            //newNode.transform.SetParent(this.transform, true);
            //newNode.parentDebug.Add("Parent to tile");
        }
    }

    //Remove a node
    public void RemoveNode(NewNode newNode)
    {
        if (nodes.Contains(newNode))
        {
            nodes.Remove(newNode);

            //Set location info
            newNode.tile = null;
            newNode.room = null;
            newNode.gameLocation = null;
            newNode.floor = null;
            newNode.building = null;
        }
    }

    public void SetRotation(int newRot)
    {
        rotation = newRot;

        foreach(NewNode node in nodes)
        {
            if(node.spawnedFloor != null) node.spawnedFloor.transform.localEulerAngles = new Vector3(0, rotation, 0);
        }
    }

    //If being set by a tool, use the set flag to assign the pair in-editor
    public void SetAsEntrance(bool val, bool mainEntrance, bool set = false)
    {
        isEntrance = val;

        if (val)
        {
            isMainEntrance = mainEntrance;

            if(SessionData.Instance.isFloorEdit)
            {
                if(isEdge)
                {
                    //Spawn arrow
                    Toolbox.Instance.DestroyObject(entranceArrow);
                    entranceArrow = Toolbox.Instance.SpawnObject(PrefabControls.Instance.entranceArrow, parent);
                    entranceArrow.transform.position = position;

                    if (isMainEntrance)
                    {
                        entranceArrow.GetComponent<MeshRenderer>().material.SetColor("_BaseColor", Color.red);
                    }
                    else
                    {
                        entranceArrow.GetComponent<MeshRenderer>().material.SetColor("_BaseColor", Color.blue);
                    }
                }
            }
        }
        else
        {
            isMainEntrance = false;

            if(entranceArrow != null)
            {
                Toolbox.Instance.DestroyObject(entranceArrow);
            }
        }

        //Game.Log("Set tile " + globalTileCoord + " as entrance: " + val + " main: " + mainEntrance);
        
        //Find the inside tile that corresponds to this
        if(SessionData.Instance.isFloorEdit && set)
        {
            if(isEntrance || (!isEntrance && entrancePair != null))
            {
                foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                {
                    Vector2Int check = v2 + floorCoord;

                    if (floor != null)
                    {
                        if (floor.tileMap.ContainsKey(check))
                        {
                            if (!floor.tileMap[check].isEdge)
                            {
                                if (isEntrance)
                                {
                                    entrancePair = floor.tileMap[check];
                                    entrancePair.entrancePair = this;
                                    entrancePair.isEntrance = isEntrance;
                                    entrancePair.isMainEntrance = isMainEntrance;

                                    //Face arrow towards the entrance pair
                                    if (entranceArrow != null)
                                    {
                                        entranceArrow.transform.LookAt(entrancePair.position);
                                    }
                                }
                                else
                                {
                                    if (entrancePair != null)
                                    {
                                        entrancePair.isEntrance = false;
                                        entrancePair.isMainEntrance = false;
                                        entrancePair.entrancePair = null;

                                        if (entranceArrow != null)
                                        {
                                            Toolbox.Instance.DestroyObject(entranceArrow);
                                        }
                                    }

                                    entrancePair = null;
                                }

                                if (entrancePair != null) Game.Log("Set found paring tile of " + entrancePair.globalTileCoord + " to entrance: " + isEntrance + " " + isMainEntrance);

                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    public void SetAsStairwell(bool val, bool spawnPrefabs, bool isInverted)
    {
        isStairwell = val;
        //debugStairwell.Add(isStairwell.ToString() + " " + spawnPrefabs);

        if (isStairwell)
        {
            isInvertedStairwell = isInverted;

            //Use the default stairwell for now...
            StairwellPreset stairwellPreset = InteriorControls.Instance.defaultStairwell;

            if (isInvertedStairwell)
            {
                stairwellPreset = InteriorControls.Instance.defaultStairwellInverted;
                if (building != null && building.preset != null && building.preset.stairwellLarge != null) stairwellPreset = building.preset.stairwellLarge;
            }
            else
            {
                if (building != null && building.preset != null && building.preset.stairwellRegular != null) stairwellPreset = building.preset.stairwellRegular;
            }

            foreach (NewNode node in nodes)
            {
                if(!node.room.isNullRoom && !node.room.isOutsideWindow)
                {
                    node.room.featuresStairwell = true;
                }

                if(!isTop && !isBottom)
                {
                    node.SetFloorType(NewNode.FloorTileType.noneButIndoors);
                }
                else if(isTop)
                {
                    node.SetFloorType(NewNode.FloorTileType.CeilingOnly);
                }
                else if(isBottom)
                {
                    node.SetFloorType(NewNode.FloorTileType.floorOnly);
                }
            }

            SetStairwellRotation(stairwellRotation);

            //Add stairwell to building
            stairwellAssign = building.AddStairwellSystem(this, stairwellPreset);

            if (spawnPrefabs && stairwell == null)
            {
                if(!isTop)
                {
                    stairwell = Toolbox.Instance.SpawnObject(stairwellPreset.spawnObject, parent);
                }
                else
                {
                    //Game.Log("Spawn top stairwell");
                    stairwell = Toolbox.Instance.SpawnObject(stairwellPreset.objectTop, parent);
                }

                stairwell.transform.position = position;
                Toolbox.Instance.SetLightLayer(stairwell, building);

                //Parent to room
                if(nodes.Count > 0)
                {
                    if(nodes[0].room != null)
                    {
                        stairwell.transform.SetParent(nodes[0].room.transform, true);
                    }
                }

                stairwell.transform.localEulerAngles = new Vector3(0, stairwellRotation, 0);

                //Configure elevator buttons
                stairwellAssign.OnSpawnStairwell(this);
            }
        }
        else
        {
            foreach (NewNode node in nodes)
            {
                node.room.featuresStairwell = false;
            }


            if (stairwell != null) Toolbox.Instance.DestroyObject(stairwell);
        }
    }

    public void SetStairwellRotation(int newRot)
    {
        while (newRot >= 360)
        {
            newRot -= 360;
        }

        while (newRot <= -360)
        {
            newRot += 360;
        }

        stairwellRotation = newRot;

        if (stairwell != null)
        {
            stairwell.transform.localEulerAngles = new Vector3(0, stairwellRotation, 0);
        }
    }

    public void SetAsTop(bool newIsTop)
    {
        isTop = newIsTop;

        foreach (NewNode node in nodes)
        {
            node.SetFloorType(NewNode.FloorTileType.CeilingOnly);
        }

        //Remove model
        if (elevator != null) Toolbox.Instance.DestroyObject(elevator);
        if (stairwell != null) Toolbox.Instance.DestroyObject(stairwell);
    }

    public void SetAsBottom(bool newIsBottom)
    {
        isBottom = newIsBottom;

        foreach (NewNode node in nodes)
        {
            node.SetFloorType(NewNode.FloorTileType.floorOnly);
        }
    }

    //Can this tile use an optimized larger tile instead of 9 smaller ones?
    public bool CanBeOptimized()
    {
        //For this tile's floor to be optimized, all nodes must belong to the same room
        bool opt = true;
        NewRoom thisRoom = null;
        NewNode.FloorTileType floorType = NewNode.FloorTileType.floorAndCeiling;

        foreach (NewNode nod in nodes)
        {
            //Set initial values from the first node we find here
            if (thisRoom == null)
            {
                thisRoom = nod.room;
                floorType = nod.floorType; //What is the floor type for this tile?
            }
            else if (thisRoom != nod.room)
            {
                opt = false;
                break;
            }
            //If different floor/ceiling type, cancel
            else if (floorType != nod.floorType)
            {
                opt = false;
                break;
            }
            //If ait vent, cancel
            else if (nod.ceilingAirVent || nod.floorAirVent)
            {
                opt = false;
                break;
            }
        }

        return opt;
    }

    public void SetFloorCeilingOptimization(bool val, bool spawnPrefabs)
    {
        //If there's an air vent on the ceiling, force no optimization
        bool forceNoCeling = false;
        bool forceNoFloor = false;

        foreach(NewNode node in nodes)
        {
            if(node.ceilingAirVent)
            {
                forceNoCeling = true;
            }

            if(node.floorAirVent)
            {
                forceNoFloor = true;
            }
        }

        useOptimizedFloor = val;
        useOptimizedCeiling = val;

        if (forceNoCeling) useOptimizedCeiling = false;
        if (forceNoFloor) useOptimizedFloor = false;

        //Refresh tiles
        if(spawnPrefabs)
        {
            foreach (NewNode nod in nodes)
            {
                nod.SpawnCeiling(false);
                nod.SpawnFloor(false);
            }
        }
    }

    //Set all nodes in this tile as an obstacle
    public void SetAsObstacle(bool val)
    {
        isObstacle = val;

        foreach(NewNode node in nodes)
        {
            node.SetAsObstacle(val);

            ////If also alley, stop new furniture
            //if(isAlley)
            //{
            //    //node.SetAllowNewFurniture(false);
            //}
        }
    }

    //Set all nodes in this tile as outside
    public void SetAsOutside(bool val)
    {
        isOutside = val;

        foreach (NewNode node in nodes)
        {
            node.SetAsOutside(val);
        }
    }

    //Check offmap: Useful for finding if this is a map corner
    public void CheckOffMap()
    {
        bool onMap = true;

        //Find out if this is on the map
        if (globalTileCoord.x < 0) onMap = false;
        else if (globalTileCoord.x > PathFinder.Instance.tileCitySize.x) onMap = false;
        if (globalTileCoord.y < 0) onMap = false;
        else if (globalTileCoord.y > PathFinder.Instance.tileCitySize.y) onMap = false;

        if (!onMap) SetAsObstacle(true);

        //Check if this is a corner of the map
        isMapCorner = false;

        if (globalTileCoord.x == 0 || globalTileCoord.x == PathFinder.Instance.tileCitySize.x - 1)
        {
            if (globalTileCoord.y == 0 || globalTileCoord.y == PathFinder.Instance.tileCitySize.y - 1)
            {
                isMapCorner = true;
            }
        }
    }

    //Connect stairwell nodes
    public void ConnectStairwell()
    {
        //Use this dictionary for referencing the actual nodes, but in this code I can use fixed coordinates
        Dictionary<Vector2, NewNode> rotatedRef = new Dictionary<Vector2, NewNode>();

        foreach(NewNode node in nodes)
        {
            //The toolbox rotate vector will use 0,0 as a pivot, but we want to rotate these coordinates by using the centre as a pivot...
            //Minus from the coordinates so 0,0 is the centre
            Vector2 rotateVector = new Vector2(node.localTileCoord.x - ((CityControls.Instance.nodeMultiplier - 1) * 0.5f), node.localTileCoord.y - ((CityControls.Instance.nodeMultiplier - 1) * 0.5f));

            rotateVector = Toolbox.Instance.RotateVector2ACW(rotateVector, stairwellRotation); //Positive values will rotate anti-clockwise assuming 0,0 is bottom left

            //Add again to find the final result
            Vector2 rotated = new Vector2(Mathf.RoundToInt(rotateVector.x + ((CityControls.Instance.nodeMultiplier - 1) * 0.5f)), Mathf.RoundToInt(rotateVector.y + ((CityControls.Instance.nodeMultiplier - 1) * 0.5f)));

            try
            {
                rotatedRef.Add(rotated, node);
            }
            catch
            {
                if(Game.Instance.devMode)
                {
                    Game.Log(name);

                    foreach(NewNode node2 in nodes)
                    {
                        Game.Log(node2.localTileCoord);
                    }

                    Game.LogError("Existing rotation error: " + rotateVector + " rotated is " + rotated + " " + node.name);
                }

                return;
            }
            
        }

        foreach(KeyValuePair<Vector2, NewNode> pair in rotatedRef)
        {
            //if (pair.Value.isConnected) continue; //Skip already connected (works on creation, not on load!)

            //On the left and right side, connect vertically
            if(pair.Key.x == 0 || pair.Key.x == CityControls.Instance.nodeMultiplier - 1)
            {
                NewNode foundNode = null;

                if(!isTop && rotatedRef.TryGetValue(new Vector2(pair.Key.x + 0, pair.Key.y + 1), out foundNode))
                {
                    if(!pair.Value.accessToOtherNodes.ContainsKey(foundNode) || !pair.Value.accessToOtherNodes[foundNode].walkingAccess)
                    {
                        pair.Value.AddAccessToOtherNode(foundNode, forceWalkable: true/*, newDebug: "Atairwell connection 1"*/);
                    }
                }

                if(!isTop && rotatedRef.TryGetValue(new Vector2(pair.Key.x + 0, pair.Key.y - 1), out foundNode))
                {
                    if (!pair.Value.accessToOtherNodes.ContainsKey(foundNode) || !pair.Value.accessToOtherNodes[foundNode].walkingAccess)
                    {
                        pair.Value.AddAccessToOtherNode(foundNode, forceWalkable: true/*, newDebug: "Atairwell connection 2"*/);
                    }
                }
            }

            //On the bottom, connect horizontally
            if (pair.Key.y == 0)
            {
                NewNode foundNode = null;

                if (!isTop && rotatedRef.TryGetValue(new Vector2(pair.Key.x + 1, pair.Key.y), out foundNode))
                {
                    if (!pair.Value.accessToOtherNodes.ContainsKey(foundNode) || !pair.Value.accessToOtherNodes[foundNode].walkingAccess)
                    {
                        pair.Value.AddAccessToOtherNode(foundNode, forceWalkable: true);
                    }
                }

                if (!isTop && rotatedRef.TryGetValue(new Vector2(pair.Key.x - 1, pair.Key.y), out foundNode))
                {
                    if (!pair.Value.accessToOtherNodes.ContainsKey(foundNode) || !pair.Value.accessToOtherNodes[foundNode].walkingAccess)
                    {
                        pair.Value.AddAccessToOtherNode(foundNode, forceWalkable: true);
                    }
                }
            }

            //In the centre (elevator, connect vertical space)
            if(pair.Key.x == 1 && pair.Key.y < 2)
            {
                NewNode foundNode = null;

                if (PathFinder.Instance.nodeMap.TryGetValue(pair.Value.nodeCoord + new Vector3Int(0, 0, 1), out foundNode))
                {
                    if (!pair.Value.accessToOtherNodes.ContainsKey(foundNode))
                    {
                        pair.Value.AddAccessToOtherNode(foundNode);
                    }
                }

                if (PathFinder.Instance.nodeMap.TryGetValue(pair.Value.nodeCoord + new Vector3Int(0, 0, -1), out foundNode))
                {
                    if (!pair.Value.accessToOtherNodes.ContainsKey(foundNode))
                    {
                        pair.Value.AddAccessToOtherNode(foundNode);
                    }
                }
            }

            //Mark as the lower/upper connection point
            if(isInvertedStairwell)
            {
                //Set node offsets
                if (pair.Key.x == 0 && pair.Key.y == 2)
                {
                    pair.Value.stairwellLowerLink = true;
                    if (!isTop) pair.Value.SetFloorHeight(7);
                }
                else if (pair.Key.x == 0 && pair.Key.y == 1)
                {
                    if (!isTop) pair.Value.SetFloorHeight(20);
                }
                else if (pair.Key.x == 0 && pair.Key.y == 0)
                {
                    if (!isTop) pair.Value.SetFloorHeight(24);
                }
                else if (pair.Key.x == 1 && pair.Key.y == 0)
                {
                    if (!isTop) pair.Value.SetFloorHeight(28);
                }
                else if (pair.Key.x == 2 && pair.Key.y == 0)
                {
                    if (!isTop) pair.Value.SetFloorHeight(33);
                }
                else if (pair.Key.x == 2 && pair.Key.y == 1)
                {
                    if (!isTop) pair.Value.SetFloorHeight(39);
                }
                else if (pair.Key.x == 2 && pair.Key.y == 2)
                {
                    pair.Value.stairwellUpperLink = true;
                    if (!isTop) pair.Value.SetFloorHeight(51);
                }
            }
            else
            {
                //Set node offsets
                if (pair.Key.x == 2 && pair.Key.y == 2)
                {
                    pair.Value.stairwellLowerLink = true;
                    if (!isTop) pair.Value.SetFloorHeight(7);
                }
                else if (pair.Key.x == 2 && pair.Key.y == 1)
                {
                    if (!isTop) pair.Value.SetFloorHeight(20);
                }
                else if (pair.Key.x == 2 && pair.Key.y == 0)
                {
                    if (!isTop) pair.Value.SetFloorHeight(24);
                }
                else if (pair.Key.x == 1 && pair.Key.y == 0)
                {
                    if (!isTop) pair.Value.SetFloorHeight(28);
                }
                else if (pair.Key.x == 0 && pair.Key.y == 0)
                {
                    if (!isTop) pair.Value.SetFloorHeight(33);
                }
                else if (pair.Key.x == 0 && pair.Key.y == 1)
                {
                    if (!isTop) pair.Value.SetFloorHeight(39);
                }
                else if (pair.Key.x == 0 && pair.Key.y == 2)
                {
                    pair.Value.stairwellUpperLink = true;
                    if (!isTop) pair.Value.SetFloorHeight(51);
                }
            }

            pair.Value.isConnected = true;
        }
    }

    //Generate save data
    public CitySaveData.TileCitySave GenerateSaveData()
    {
        CitySaveData.TileCitySave output = new CitySaveData.TileCitySave();

        output.tileID = tileID;
        output.floorCoord = floorCoord; //Coordinate within floor
        output.globalTileCoord = globalTileCoord; //Coordinate within all tiles
        output.isOutside = isOutside; //True if this tile is outside, false if it's indoors
        output.isObstacle = isObstacle; //True if the whole tile is considered an obstacle
        //output.isAlley = isAlley; //True if the all nodes are considered an alley
        //output.isBackstreet = isBackstreet; //True if this is street space behind a building
        output.isEdge = isEdge; //True if this reperesents the edge of a groundmap tile (eg road space)
        output.rotation = rotation; //Rotation of this tile relative to the address
        output.isEntrance = isEntrance; //True if this is an entrance
        output.isMainEntrance = isMainEntrance; //True if this is a main entrance
        output.isStairwell = isStairwell; //True if this is a stairwell
        output.stairwellRotation = stairwellRotation; //Rotation of the stairwell
        output.isElevator = isInvertedStairwell; //True if this is an elevator
        output.elevatorRotation = elevatorRotation; //Rotation of the elevator
        output.isTop = isTop; //If true this is the top of the elevator/stairs
        output.isBottom = isBottom; //If true this is the bottom of the elevator/stairs
        //output.useOptimizedFloor = useOptimizedFloor;
        //output.useOptimizedCeiling = useOptimizedCeiling;

        return output;
    }
}
