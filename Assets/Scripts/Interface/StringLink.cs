﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StringLink : MonoBehaviour
{
    public Image img;

    RectTransform rect;
    RectTransform rectFrom;
    RectTransform rectTo;

    RectTransform scaleReference;

    //public void Setup(EntryInstance newFrom, EntryInstance newTo, RectTransform newScaleReference)
    //{
    //    fromInstance = newFrom;
    //    toInstance = newTo;
    //    scaleReference = newScaleReference;
    //    img = this.gameObject.GetComponent<Image>();

    //    rect = this.gameObject.GetComponent<RectTransform>();
    //    //rectFrom = fromInstance.buttonController.GetComponent<RectTransform>();
    //    //rectTo = toInstance.buttonController.GetComponent<RectTransform>();

    //    VisualUpdate();
    //}

    public void VisualUpdate()
    {
        //Set position to objects - offset
        Vector2 toOffset = new Vector2(rectTo.sizeDelta.x * 0.5f, 0f);
        Vector2 fromOffset = new Vector2(rectFrom.sizeDelta.x * 0.5f, 0f);

        Vector2 differenceVector = new Vector2(rectTo.localPosition.x + toOffset.x, rectTo.localPosition.y + toOffset.y) - new Vector2(rectFrom.localPosition.x + fromOffset.x, rectFrom.localPosition.y + fromOffset.y);

        //Calculate with based on whether this is a file link
        float width = 5f;

        rect.sizeDelta = new Vector2(differenceVector.magnitude * scaleReference.localScale.x, width);

        //Set angle
        float angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;
        rect.rotation = Quaternion.Euler(0, 0, angle);

        rect.position = new Vector3(rectFrom.position.x + fromOffset.x, rectFrom.position.y + fromOffset.y, 0f);
    }
}
