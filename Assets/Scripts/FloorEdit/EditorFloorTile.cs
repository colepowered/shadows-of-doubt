﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorFloorTile : MonoBehaviour
{
    public Vector2Int floorCoord;
    public bool edgeTile = false;
}
