﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//A version of ScrollRect that uses right mouse for scrolling
//Script pass 1
public class CustomScrollRect : ScrollRect
{
    public bool rightMouseScroll = true;
    public bool leftMouseScroll = false;
    public bool isScrolling = false;
    public void ScrollZoom(Vector2 deltaPos)
	{
		m_ContentStartPosition -= deltaPos;
	}

    //This works by overriding functions and passing the other mouse button
	public override void OnBeginDrag(PointerEventData eventData)
     {
        if (InputController.Instance.mouseInputMode)
        {
            if (rightMouseScroll && eventData.button == PointerEventData.InputButton.Right)
            {
                eventData.button = PointerEventData.InputButton.Left;

                base.OnBeginDrag(eventData);

                isScrolling = true;

                if (InterfaceController.Instance != null)
                {
                    InterfaceController.Instance.AddMouseOverElement(this);
                }
            }
            else if (leftMouseScroll)
            {
                base.OnBeginDrag(eventData);

                isScrolling = true;

                if (InterfaceController.Instance != null)
                {
                    InterfaceController.Instance.AddMouseOverElement(this);
                }
            }
        }
     }

     public override void OnEndDrag(PointerEventData eventData)
     {
        if (InputController.Instance.mouseInputMode)
        {
            if (rightMouseScroll && eventData.button == PointerEventData.InputButton.Right)
            {
                eventData.button = PointerEventData.InputButton.Left;

                base.OnEndDrag(eventData);

                isScrolling = false;

                InterfaceController.Instance.RemoveMouseOverElement(this);
            }
            else if (leftMouseScroll)
            {
                base.OnEndDrag(eventData);

                isScrolling = false;

                InterfaceController.Instance.RemoveMouseOverElement(this);
            }
        }
    }
 
     public override void OnDrag(PointerEventData eventData)
     {
        if (InputController.Instance.mouseInputMode)
        {
            if (rightMouseScroll && eventData.button == PointerEventData.InputButton.Right)
            {
                eventData.button = PointerEventData.InputButton.Left;

                base.OnDrag(eventData);

                isScrolling = true;
            }
            else if (leftMouseScroll)
            {
                base.OnDrag(eventData);

                isScrolling = true;
            }
        }
    }

    public void SetAnchorPos(Vector2 position)
     {
		base.SetContentAnchoredPosition(position);
	 }

}