﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSceneEnableCeilings : MonoBehaviour
{
    public Transform ceilingParent;

    // Start is called before the first frame update
    void Start()
    {
        ceilingParent.gameObject.SetActive(true);
    }
}
