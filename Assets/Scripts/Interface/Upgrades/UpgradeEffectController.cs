﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Reflection;
using UnityEngine.EventSystems;

public class UpgradeEffectController : MonoBehaviour
{
    [Header("New Upgrades System")]
    public List<AppliedEffect> appliedEffects = new List<AppliedEffect>();

    [System.Serializable]
    public class AppliedEffect
    {
        public UpgradesController.Upgrades disk;
        public SyncDiskPreset.Effect effect;
        public float value;
    }

    //One-shot effects
    public void OnInstall(UpgradesController.Upgrades disk, SyncDiskPreset.Effect effect, float value)
    {
        if(effect == SyncDiskPreset.Effect.starchLoan)
        {
            GameplayController.Instance.AddMoney(Mathf.RoundToInt(value), true, "sync disk");
        }
    }

    public void OnUninstall(UpgradesController.Upgrades disk, SyncDiskPreset.Effect effect, float value)
    {

    }

    public void OnUpgrade(UpgradesController.Upgrades disk, SyncDiskPreset.UpgradeEffect effect, float value, int level)
    {
        if (effect == SyncDiskPreset.UpgradeEffect.reduceUninstallCost)
        {
            disk.uninstallCost -= Mathf.RoundToInt(value);
            disk.uninstallCost = Mathf.Max(disk.uninstallCost, 0);
        }
    }

    //Called when sync disks are changed in any way
    public void OnSyncDiskChange(bool forceUpdate = false)
    {
        if (SessionData.Instance.isFloorEdit) return;
        if (!forceUpdate && !SessionData.Instance.startedGame) return;

        //Set player health
        Player.Instance.SetMaxHealth(GameplayControls.Instance.baseMaxPlayerHealth + (GetUpgradeEffect(SyncDiskPreset.Effect.increaseHealth) * GameplayControls.Instance.baseMaxPlayerHealth), false);

        //Set recovery rate
        Player.Instance.SetRecoveryRate(GameplayControls.Instance.playerRecoveryRate + (GetUpgradeEffect(SyncDiskPreset.Effect.increaseRegeneration) * GameplayControls.Instance.playerRecoveryRate));

        //Set inventory
        FirstPersonItemController.Instance.SetSlotSize(GameplayControls.Instance.defaultInventorySlots + Mathf.RoundToInt(GetUpgradeEffect(SyncDiskPreset.Effect.increaseInventory)));

        //Set punch power
        Player.Instance.SetCombatHeft(GameplayControls.Instance.playerCombatHeft * (1f + GetUpgradeEffect(SyncDiskPreset.Effect.punchPowerModifier)));

        //Set player height
        if(!Player.Instance.transitionActive)
        {
            if(!Player.Instance.inAirVent)
            {
                if(Player.Instance.isCrouched)
                {
                    Player.Instance.SetPlayerHeight(Player.Instance.GetPlayerHeightCrouched());
                }
                else
                {
                    Player.Instance.SetPlayerHeight(Player.Instance.GetPlayerHeightNormal());
                }
            }

            Player.Instance.RestorePlayerMovementSpeed();
        }
    }

    //Singleton pattern
    private static UpgradeEffectController _instance;
    public static UpgradeEffectController Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    //New System
    //Return the desired effect value
    public float GetUpgradeEffect(SyncDiskPreset.Effect effect)
    {
        //Special case: Enable awaken at home for story mode
        if(effect == SyncDiskPreset.Effect.awakenAtHome || effect == SyncDiskPreset.Effect.reduceMedicalCosts)
        {
            int chapt = 0;

            if(Toolbox.Instance.IsStoryMissionActive(out _, out chapt))
            {
                if(chapt < 31)
                {
                    return 1f;
                }
            }
        }

        float ret = 0f;

        foreach(AppliedEffect ap in appliedEffects)
        {
            if(ap.effect == effect)
            {
                ret += ap.value;
            }
        }

        return ret;
    }
}
