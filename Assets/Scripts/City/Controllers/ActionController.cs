﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Reflection;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using Rewired.Data.Mapping;

public class ActionController : MonoBehaviour
{
    public List<AIActionPreset> allActions = new List<AIActionPreset>();
    Dictionary<AIActionPreset, MethodInfo> actionRef = new Dictionary<AIActionPreset, MethodInfo>(); //Store pre-configured method info in here for speed

    //Special cases
    [System.NonSerialized]
    private Interactable bargeDoor;

    //Events
    public delegate void PlayerAction(AIActionPreset action, Interactable what, NewNode where, Actor who);
    public event PlayerAction OnPlayerAction;

    //Singleton pattern
    private static ActionController _instance;
    public static ActionController Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        //Load actions
        allActions = AssetLoader.Instance.GetAllActions();

        foreach (AIActionPreset act in allActions)
        {
            //Create method info
            MethodInfo mi = this.GetType().GetMethod(act.name);

            if (mi != null)
            {
                actionRef.TryAdd(act, mi);
            }
        }
    }

    //Execute an interactable action: This must be fast as all object interactions are sent to this!
    public void ExecuteAction(AIActionPreset action, Interactable what, NewNode where, Actor who)
    {
        MethodInfo method = null;

        if (actionRef.TryGetValue(action, out method))
        {
            object[] passed = { what, where, who };
            method.Invoke(this, passed);
        }

        //Fire event
        if(who != null && who.isPlayer)
        {
            if (OnPlayerAction != null)
            {
                OnPlayerAction(action, what, where, who);
            }
        }
    }

    //Write methods below (they must correspond to the action preset names)
    public void TurnOnMainLight(Interactable what, NewNode where, Actor who)
    {
        where.room.SetMainLights(true, what.name + " " + where.name + " " + who.name, who);
    }

    public void TurnOffMainLight(Interactable what, NewNode where, Actor who)
    {
        where.room.SetMainLights(false, what.name + " " + where.name + " " + who.name, who);
    }

    public void TurnOnSecondaryLight(Interactable what, NewNode where, Actor who)
    {
        where.room.SetSecondaryLight(true);
    }

    public void TurnOffSecondaryLight(Interactable what, NewNode where, Actor who)
    {
        what.SetSwitchState(false, who); //This is automatic, but also do it here so we can detect the room's light status properly
        //what.lightController.SetOn(false); //This is automatic, but also do it here so we can detect the room's light status properly

        //Keep status as true, even if just one is still on
        bool l = false;

        foreach (Interactable inter in where.room.secondaryLights)
        {
            if (inter.sw0)
            {
                l = true;
                break;
            }
        }

        where.room.SetSecondaryLight(l);
    }

    //TV acts as secondary light
    public void TurnOnTV(Interactable what, NewNode where, Actor who)
    {
        where.room.SetSecondaryLight(true);

        //Force suspicious
        //bool forceSuspicious = false;

        //if (who != null && who.isTrespassing)
        //{
        //    forceSuspicious = true;
        //}

        //Play sound
        //AudioController.Instance.StopSound(what.loopingAudioEvent, false);
        //what.loopingAudioEvent = AudioController.Instance.PlayWorldLooping(AudioControls.Instance.tvShow, who as Human, what.node, new Vector3(0, 1, 0), forceSuspicious: forceSuspicious);
    }

    public void TurnOffTV(Interactable what, NewNode where, Actor who)
    {
        what.SetSwitchState(false, who); //This is automatic, but also do it here so we can detect the room's light status properly
        //what.lightController.SetOn(false); //This is automatic, but also do it here so we can detect the room's light status properly

        //AudioController.Instance.StopSound(what.loopingAudioEvent, false);
        //what.loopingAudioEvent = null;

        //Keep status as true, even if just one is still on
        //Keep status as true, even if just one is still on
        bool l = false;

        foreach (Interactable inter in where.room.secondaryLights)
        {
            if (inter.sw0)
            {
                l = true;
                break;
            }
        }

        where.room.SetSecondaryLight(l);
    }

    public void PickUp(Interactable what, NewNode where, Actor who)
    {
        InteractionController.Instance.PickUp(what);

        //Execute confront bark
        foreach(Actor a in where.room.currentOccupants)
        {
            if (a == who) continue;
            if (a.ai == null) continue;
            if (a.speechController == null) continue;
            if (a.isDead || a.isStunned || a.isAsleep) continue;

            if(a.locationsOfAuthority.Contains(where.gameLocation))
            {
                if(a.ai.trackedTargets.Exists(item => item.actor == who))
                {
                    if(a.speechController.speechQueue.Count <= 0)
                    {
                        a.speechController.TriggerBark(SpeechController.Bark.confrontMessingAround);
                        break;
                    }
                }
            }
        }
    }

    public void PutDown(Interactable what, NewNode where, Actor who)
    {
        if (InteractionController.Instance.carryingObject != null)
        {
            //Play the furniture placement sound
            if(InteractionController.Instance.carryingObject.interactable != null)
            {
                if(InteractionController.Instance.carryingObject.interactable.preset.apartmentPlacementMode != InteractablePreset.ApartmentPlacementMode.physics)
                {
                    AudioController.Instance.Play2DSound(AudioControls.Instance.furniturePlacement);
                }
            }

            Game.Log("Put Down " + InteractionController.Instance.carryingObject);
            InteractionController.Instance.carryingObject.DropThis(false);
        }
    }

    public void Throw(Interactable what, NewNode where, Actor who)
    {
        if (InteractionController.Instance.carryingObject != null)
        {
            Game.Log("Throw " + InteractionController.Instance.carryingObject);
            InteractionController.Instance.carryingObject.DropThis(true);
        }
    }

    public void OpenDoor(Interactable what, NewNode where, Actor who)
    {
        //The polymorphic reference should be a door controller
        NewDoor door = what.objectRef as NewDoor;

        //Game.Log(who.name + " open door " + what.name);

        if (door != null)
        {
            //Make sure of fingerprint on handle...
            if(who != null && !who.isPlayer)
            {
                if(what != door.handleInteractable)
                {
                    door.handleInteractable.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);
                }
                else if(door.lockInteractableFront != null && what != door.lockInteractableFront)
                {
                    door.lockInteractableFront.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);
                }
                else if (door.lockInteractableRear != null && what != door.lockInteractableRear)
                {
                    door.lockInteractableRear.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);
                }
            }

            if (!door.isLocked && !door.isJammed)
            {
                door.OpenByActor(who);
            }
            else
            {
                //Force update of door icons
                if(who != null)
                {
                    if(who.isPlayer)
                    {
                        InterfaceControls.Instance.lockedIcon.gameObject.SetActive(true);
                        InteractionController.Instance.AlignInteractionIcons();

                        //Trigger notification
                        if(!Game.Instance.sandboxMode && ChapterController.Instance != null && ChapterController.Instance.currentPart < 21)
                        {
                            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "Locked_help"), InterfaceControls.Icon.door);
                        }
                        else
                        {
                            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "Locked"), InterfaceControls.Icon.door);
                        }
                    }
                    else
                    {
                        Game.Log("AI locked door open attempt: " + who.name);
                    }
                }

                //Play sound
                AudioController.Instance.PlayWorldOneShot(door.preset.audioLockedEntryAttempt, who, door.parentedWall.node, door.transform.position);
            }

            //Set know locked status
            if (who != null && who.isPlayer)
            {
                door.SetKnowLockedStatus(true);
            }
        }
    }

    public void CloseDoor(Interactable what, NewNode where, Actor who)
    {
        //The polymorphic reference should be a door controller
        NewDoor door = what.objectRef as NewDoor;

        if (door != null)
        {
            //Make sure of fingerprint on handle...
            if (who != null && what != door.handleInteractable && !who.isPlayer)
            {
                door.handleInteractable.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);
            }

            door.SetOpen(0f, who);

            //Set know locked status
            if (who == Player.Instance)
            {
                door.SetKnowLockedStatus(true);
            }
            else door.SetKnowLockedStatus(false);
        }
    }

    public void Open(Interactable what, NewNode where, Actor who)
    {
        if(what != null && who != null && !who.isPlayer)
        {
            what.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);
            what.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);

            if (what.lockInteractable != null && what.lockInteractable != what)
            {
                what.lockInteractable.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);
                what.lockInteractable.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);
            }
        }
    }

    public void KnockOnDoor(Interactable what, NewNode where, Actor who)
    {
        //The polymorphic reference should be a door controller
        NewDoor door = what.objectRef as NewDoor;

        if (door != null)
        {
            float forceAdditional = 0f;
            int knocks = 2;

            if(who.isEnforcer && who.isOnDuty)
            {
                forceAdditional = 7f;
                knocks = 3;
                who.speechController.TriggerBark(SpeechController.Bark.enforcersKnock);
            }

            door.OnKnock(who, knockCount: knocks, forceAdditionalUrgency: forceAdditional);
        }
    }

    public void LockDoor(Interactable what, NewNode where, Actor who)
    {
        //The polymorphic reference should be a door controller
        NewDoor door = what.objectRef as NewDoor;

        if (door != null)
        {
            //Make sure of fingerprint on handle and lock...
            if(who != null && !who.isPlayer)
            {
                //Print on handle
                if (what != door.handleInteractable && door.handleInteractable != null)
                {
                    door.handleInteractable.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);
                }

                //Print on lock
                if(what != door.lockInteractableFront && door.lockInteractableFront != null)
                {
                    door.lockInteractableFront.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);
                }

                if (what != door.lockInteractableRear && door.lockInteractableRear != null)
                {
                    door.lockInteractableRear.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);
                }
            }

            door.SetLocked(true, who);

            //Set know locked status
            if (who == Player.Instance)
            {
                door.SetKnowLockedStatus(true);
            }
            else door.SetKnowLockedStatus(false);
        }
        else if(what != null)
        {
            what.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);

            what.SetLockedState(true, who);
        }
    }

    public void UnlockDoor(Interactable what, NewNode where, Actor who)
    {
        //Game.Log(what.name + " unlockdoor");

        //The polymorphic reference should be a door controller
        NewDoor door = what.objectRef as NewDoor;

        if (door != null)
        {
            //Make sure of fingerprint on handle and lock...
            if (who != null && !who.isPlayer)
            {
                //Print on handle
                if (what != door.handleInteractable && door.handleInteractable != null)
                {
                    door.handleInteractable.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);
                }

                //Print on lock
                if (what != door.lockInteractableFront && door.lockInteractableFront != null)
                {
                    door.lockInteractableFront.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);
                }

                if (what != door.lockInteractableRear && door.lockInteractableRear != null)
                {
                    door.lockInteractableRear.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);
                }
            }

            //Game.Log(who + " unlockdoor");

            door.SetLocked(false, who);

            //Set know locked status
            if (who == Player.Instance)
            {
                door.SetKnowLockedStatus(true);
            }
            else door.SetKnowLockedStatus(false);
        }
        else if (what != null)
        {
            what.AddNewDynamicFingerprint(who as Human, Interactable.PrintLife.timed);

            what.SetLockedState(false, who);
        }
    }

    public void Lockpick(Interactable what, NewNode where, Actor who)
    {
        //The polymorphic reference should be a door controller
        NewDoor door = what.objectRef as NewDoor;

        if (door != null)
        {
            SessionData.Instance.TutorialTrigger("lockpicking");

            if(GameplayController.Instance.lockPicks > 0)
            {
                door.OnLockpick();
            }
            else
            {
                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "not_enough_lockpicks"), InterfaceControls.Icon.lockpick, null, colourOverride: true, col: InterfaceControls.Instance.messageRed, ping: GameMessageController.PingOnComplete.lockpicks);
            }

            //Set know locked status
            if (who == Player.Instance)
            {
                door.SetKnowLockedStatus(true);
            }
            else door.SetKnowLockedStatus(false);
        }
        //Otherwise this must be an object's lock...
        else
        {
            SessionData.Instance.TutorialTrigger("lockpicking");

            if (GameplayController.Instance.lockPicks > 0)
            {
                what.OnLockpick();
            }
            else
            {
                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "not_enough_lockpicks"), InterfaceControls.Icon.lockpick, null, colourOverride: true, col: InterfaceControls.Instance.messageRed, ping: GameMessageController.PingOnComplete.lockpicks);
            }
        }
    }

    public void PeekUnderDoor(Interactable what, NewNode where, Actor who)
    {
        //The polymorphic reference should be a door controller
        NewDoor door = what.objectRef as NewDoor;

        if (door != null)
        {
            door.OnDoorPeek();
        }
    }

    public void Hide(Interactable what, NewNode where, Actor who)
    {
        Game.Log("Player: Hide");
        Player.Instance.OnHide(what);
    }

    public void AnswerTelephone(Interactable what, NewNode where, Actor who)
    {
        if (who.isPlayer)
        {
            Player.Instance.OnAnswerPhone(what);
        }

        //Answer receiver
        if (what.t != null)
        {
            //if (what.t.activeCall.Count <= 0)
            //{
            //    what.SetCustomState3(false, null, false);
            //}

            if (what.t.activeReceiver == null && (who as Human) != null)
            {
                what.t.SetTelephoneAnswered(who as Human);
            }
        }
    }

    public void AIHangUp(Interactable what, NewNode where, Actor who)
    {
        //End call
        if (what.t != null)
        {
            what.t.SetTelephoneAnswered(null);
        }
    }

    //Return from locked-in action
    public void Return(Interactable what, NewNode where, Actor who)
    {
        if(who.isPlayer)
        {
            InteractionController.Instance.SetLockedInInteractionMode(null);
        }
    }

    //AI pulls player from hiding place
    public void PullPlayerFromHidingPlace(Interactable what, NewNode where, Actor who)
    {
        Game.Log("Player: AI has pulled player from hiding place");
        InteractionController.Instance.SetLockedInInteractionMode(null);
    }

    public void TakeKey(Interactable what, NewNode where, Actor who)
    {
        //Add to keyring: Adds keys to all doors of the found location
        int roomID = (int)what.pv.Find(item => item.varType == Interactable.PassedVarType.roomID).value;
        NewRoom room = CityData.Instance.roomDictionary[roomID];

        //Give keys for key lock doors
        foreach (NewNode.NodeAccess acc in room.entrances)
        {
            if (acc.wall.door != null)
            {
                if(acc.wall.door.preset.lockType == DoorPreset.LockType.key)
                {
                    who.AddToKeyring(acc.wall.door, true);
                }
            }
        }

        //Add steal fine
        if (who != null && who.isPlayer && InteractionController.Instance.GetValidPlayerActionIllegal(what, Player.Instance.currentNode))
        {
            //Add peril if worth more than 1
            if (what.val > 1)
            {
                StatusController.Instance.AddFineRecord(Player.Instance.currentNode.gameLocation.thisAsAddress, what, StatusController.CrimeType.theft, true);
            }
        }

        //Destroy the object
        what.Delete();
    }

    public void TakeBlueprints(Interactable what, NewNode where, Actor who)
    {
        //Add to map information
        foreach(KeyValuePair<int, NewFloor> pair in where.building.floors)
        {
            foreach(NewAddress ad in pair.Value.addresses)
            {
                foreach (NewRoom room in ad.rooms)
                {
                    room.SetExplorationLevel(Mathf.Max(1, room.explorationLevel));

                    //Discover ducts
                    foreach (AirDuctGroup ductGrp in room.ductGroups)
                    {
                        foreach (AirDuctGroup.AirDuctSection duct in ductGrp.airDucts)
                        {
                            duct.SetDiscovered(true);
                        }
                    }
                }
            }
        }

        //Add steal fine
        if (who != null && who.isPlayer && InteractionController.Instance.GetValidPlayerActionIllegal(what, Player.Instance.currentNode))
        {
            //Add peril if worth more than 1
            if (what.val > 1)
            {
                StatusController.Instance.AddFineRecord(Player.Instance.currentNode.gameLocation.thisAsAddress, what, StatusController.CrimeType.theft, true);
            }
        }

        //Destroy the object
        what.Delete();
    }

    public void TakeMoney(Interactable what, NewNode where, Actor who)
    {
        if(who != null && who.isPlayer)
        {
            AudioController.Instance.Play2DSound(AudioControls.Instance.pickUpMoney);
            GameplayController.Instance.AddMoney(Mathf.RoundToInt(what.val), true, "");

            //Add steal fine
            if (InteractionController.Instance.GetValidPlayerActionIllegal(what, Player.Instance.currentNode))
            {
                //Add peril if worth more than 1
                if (what.val > 1)
                {
                    StatusController.Instance.AddFineRecord(Player.Instance.currentNode.gameLocation.thisAsAddress, what, StatusController.CrimeType.theft, true);
                }
            }
        }

        //Destroy the object
        what.Delete();
    }

    public void AIPickUpItemFromFloor(Interactable what, NewNode where, Actor who)
    {
        if (what == null) return;
        Game.Log(who + " AI PICKUP FROM FLOOR: " + what.name);

        if (who != null && !who.isPlayer)
        {
            Human human = who as Human;
            if (human == null) return;

            //Pick up money
            if (what.preset.isMoney)
            {
                foreach (Human.WalletItem i in human.walletItems)
                {
                    if (i.itemType == Human.WalletItemType.money)
                    {
                        i.money++;
                        break;
                    }
                }

                //Destroy the object
                what.Delete();
            }
            //Consumable; consume if found at home
            else if (what.preset.isInventoryItem && what.preset.consumableAmount > 0f && what.cs > 0f && what.belongsTo != who && who.locationsOfAuthority.Contains(where.gameLocation))
            {
                human.AddCurrentConsumable(what.preset);

                //Destroy the object
                what.SafeDelete(true);
            }
            //Pick up trash
            else if (what.IsLitter())
            {
                human.AddTrash(what.preset, what.belongsTo, null);

                //Destroy the object
                what.SafeDelete(true);
            }
            //Take back to spawn
            else
            {
                if (human.ai.currentGoal != null)
                {
                    human.ai.currentGoal.TryInsertInteractableAction(what, RoutineControls.Instance.putBack, 6, what.spawnNode, true);
                    what.SetInInventory(human);
                }
                //Just reset...
                else if(what.inInventory == null)
                {
                    what.originalPosition = false;
                    what.SetOriginalPosition(true);
                }
            }
        }
    }

    public void AIPutBack(Interactable what, NewNode where, Actor who)
    {
        if (what == null) return;
        Game.Log("AI PUT BACK " + what.name);
        what.SetAsNotInventory(what.spawnNode);
        what.originalPosition = false;
        what.SetOriginalPosition(true);
    }

    public void TakeSyncDisk(Interactable what, NewNode where, Actor who)
    {
        if (what == null) return;

        if(what.syncDisk == null)
        {
            Game.LogError("No sync disk associated with this object!");
            return;
        }

        //UpgradesController.Instance.AddUpgradeToPool(what.syncDisk);

        if (who != null && who.isPlayer)
        {
            //GameplayController.Instance.AddMoney(Mathf.RoundToInt(what.val), true, "");

            //Add steal fine
            if (InteractionController.Instance.GetValidPlayerActionIllegal(what, Player.Instance.currentNode))
            {
                //Add peril if worth more than 1
                if (what.val > 1)
                {
                    StatusController.Instance.AddFineRecord(Player.Instance.currentNode.gameLocation.thisAsAddress, what, StatusController.CrimeType.theft, true);
                }
            }
        }

        SessionData.Instance.TutorialTrigger("upgrades");

        //Destroy the object
        what.SafeDelete(true);
    }

    public void TakeLockpick(Interactable what, NewNode where, Actor who)
    {
        AudioController.Instance.Play2DSound(AudioControls.Instance.pickUpLockpicks);
        GameplayController.Instance.AddLockpicks(1, true);

        SessionData.Instance.TutorialTrigger("lockpicking");

        //Destroy the object
        what.Delete();
    }

    public void TakeLockpickKit(Interactable what, NewNode where, Actor who)
    {
        AudioController.Instance.Play2DSound(AudioControls.Instance.pickUpLockpicks);
        GameplayController.Instance.AddLockpicks(30, true);

        SessionData.Instance.TutorialTrigger("lockpicking");

        //Destroy the object
        what.Delete();
    }

    //public void TakeItem(Interactable what, NewNode where, Actor who)
    //{
    //    Inspect(what, where, who);

    //    //Destroy the object
    //    what.Remove(false);
    //}

    public void Rob(Interactable what, NewNode where, Actor who)
    {
        //Destroy the object
        what.SafeDelete(true);
    }

    public void Inspect(Interactable what, NewNode where, Actor who)
    {
        //Open evidence
        if (what.evidence != null)
        {
            SessionData.Instance.TutorialTrigger("evidence");

            SessionData.Instance.PauseGame(true);

            InterfaceController.Instance.SpawnWindow(what.evidence, worldInteraction: true, autoPosition: false, forcePosition: new Vector2(0.5f, 0.5f), passedEvidenceKey: Evidence.DataKey.photo, passedInteractable: what);
        }
        else
        {
            Game.LogError("No evidence to inspect...");
        }

        what.ins = true; //Inspected flag
    }

    public void InspectRemove(Interactable what, NewNode where, Actor who)
    {
        //Open evidence
        if (what.evidence != null)
        {
            SessionData.Instance.TutorialTrigger("evidence");

            SessionData.Instance.PauseGame(true);

            InterfaceController.Instance.SpawnWindow(what.evidence, worldInteraction: true, autoPosition: false, forcePosition: new Vector2(0.5f, 0.5f), passedEvidenceKey: Evidence.DataKey.photo, passedInteractable: what);
        }
        else
        {
            Game.Log("Player: No evidence to inspect...");
        }

        what.ins = true; //Inspected flag

        what.MarkAsTrash(true);
        what.RemoveFromPlacement();
    }

    public void InspectComputer(Interactable what, NewNode where, Actor who)
    {
        //Open evidence
        if(Player.Instance.computerInteractable != null)
        {
            if(Player.Instance.computerInteractable.controller != null)
            {
                if(Player.Instance.computerInteractable.controller.computer != null)
                {
                    foreach(GameObject app in Player.Instance.computerInteractable.controller.computer.spawnedContent)
                    {
                        SurveillanceApp getSurv = app.GetComponent<SurveillanceApp>();

                        if(getSurv != null)
                        {
                            if(getSurv.hoveredActor != null)
                            {
                                SessionData.Instance.PauseGame(true);

                                InterfaceController.Instance.SpawnWindow(getSurv.hoveredActor.human.evidenceEntry, worldInteraction: false, autoPosition: false, forcePosition: new Vector2(0.5f, 0.5f), passedEvidenceKey: Evidence.DataKey.photo);

                                break;
                            }
                            else if(getSurv.selectedActor != null)
                            {
                                SessionData.Instance.PauseGame(true);

                                InterfaceController.Instance.SpawnWindow(getSurv.selectedActor.human.evidenceEntry, worldInteraction: false, autoPosition: false, forcePosition: new Vector2(0.5f, 0.5f), passedEvidenceKey: Evidence.DataKey.photo);

                                break;
                            }
                        }

                        ProfileApp getProf = app.GetComponent<ProfileApp>();

                        if(getProf != null && getProf.controller.loggedInAs != null)
                        {
                            SessionData.Instance.PauseGame(true);

                            InterfaceController.Instance.SpawnWindow(getProf.controller.loggedInAs.evidenceEntry, worldInteraction: false, autoPosition: false, forcePosition: new Vector2(0.5f, 0.5f), passedEvidenceKey: Evidence.DataKey.photo);

                            break;
                        }
                    }
                }
            }
        }
    }

    public void InspectMultiPage(Interactable what, NewNode where, Actor who)
    {
        //Open evidence
        if (what.evidence != null)
        {
            EvidenceMultiPage mp = what.evidence as EvidenceMultiPage;

            if(mp != null)
            {
                List<EvidenceMultiPage.MultiPageContent> pageContent = mp.GetContentForPage(mp.page);

                foreach(EvidenceMultiPage.MultiPageContent content in pageContent)
                {
                    if (content.evID != null && content.evID.Length > 0)
                    {
                        Evidence getEv = null;

                        if (GameplayController.Instance.evidenceDictionary.TryGetValue(content.evID, out getEv))
                        {
                            SessionData.Instance.PauseGame(true);

                            InterfaceController.Instance.SpawnWindow(getEv, worldInteraction: true, autoPosition: false, forcePosition: new Vector2(0.5f, 0.5f), passedEvidenceKey: Evidence.DataKey.photo);
                        }
                        else
                        {
                            Game.LogError("Unable to find evidence " + content.evID);
                        }
                    }
                    else if(content.meta > 0)
                    {
                        MetaObject meta = CityData.Instance.FindMetaObject(content.meta);

                        if(meta != null)
                        {
                            Evidence ev = meta.GetEvidence(true, where.nodeCoord);

                            if (ev != null)
                            {
                                SessionData.Instance.PauseGame(true);

                                //Enable window focus
                                InterfaceController.Instance.SpawnWindow(ev, worldInteraction: true, autoPosition: false, forcePosition: new Vector2(0.5f, 0.5f), passedEvidenceKey: Evidence.DataKey.photo);
                            }
                            else
                            {
                                Game.LogError("Unable to get evidence from meta object " + meta.preset);
                            }
                        }
                        else
                        {
                            Game.LogError("Unable to get meta object " + content.meta);
                        }
                    }

                    if (content.discEvID != null && content.discEvID.Length > 0)
                    {
                        Evidence getEv = null;

                        if (GameplayController.Instance.evidenceDictionary.TryGetValue(content.discEvID, out getEv))
                        {
                            getEv.AddDiscovery(content.disc);
                        }
                        else
                        {
                            Game.LogError("Unable to find evidence " + content.evID);
                        }
                    }
                }
            }
            else
            {
                Game.Log("Player: No multi-page evidence to inspect...");
            }
        }
        else
        {
            Game.Log("Player: No evidence to inspect...");
        }

        what.ins = true; //Inspected flag
    }

    public void TalkTo(Interactable what, NewNode where, Actor who)
    {
        Game.Log("TalkTo");

        //Get the interactable human/actor
        Actor interacting = what.controller.isActor;

        //Player interacts with citizen
        if (who.isPlayer)
        {
            if (interacting.interactingWith == null)
            {
                interacting.ai.TalkTo();
            }
        }
        else if (interacting.isPlayer)
        {
            if (who.interactingWith == null)
            {
                who.ai.TalkTo();
            }
        }
    }

    public void Call(Interactable what, NewNode where, Actor who)
    {
        //Load dialog options
        if(who.isPlayer)
        {
            //Stop dial tone
            if(what.t != null)
            {
                if (what.t.dialTone != null)
                {
                    AudioController.Instance.StopSound(what.t.dialTone, AudioController.StopType.immediate, "phone hang up"); //Stop dial tone
                    what.t.dialTone = null;
                }
            }

            InteractionController.Instance.SetDialog(true, what);
        }
    }

    public void Dial(Interactable what, NewNode where, Actor who)
    {
        //Load dialog options
        if (who.isPlayer)
        {
            SessionData.Instance.PauseGame(true);
            InfoWindow selectWindow = InterfaceController.Instance.SpawnWindow(CityData.Instance.telephone, autoPosition: false, forcePosition: InterfaceControls.Instance.handbookWindowPosition, worldInteraction: true, passedInteractable: what);
        }
    }

    public void CallSomeone(Interactable what, NewNode where, Actor who)
    {
        List<Acquaintance> callpool = new List<Acquaintance>();
        Human thisPerson = who as Human;

        //Specific person...
        Human call = null;

        if (who != null && who.ai != null && who.ai.currentGoal != null)
        {
            if(who.ai.currentGoal.passedVar > -2)
            {
                CityData.Instance.GetHuman(who.ai.currentGoal.passedVar, out call);
            }
        }

        if(call == null)
        {
            foreach (Acquaintance aq in thisPerson.acquaintances)
            {
                if (aq.with.home == null) continue; //Must have a home
                if (aq.with.home == thisPerson.home) continue; //Must have a different home
                if (aq.with.home.telephones.Count <= 0) continue;

                //Is this person likely to be home?
                if (aq.with.isAtWork)
                {
                    continue;
                }

                //If in same location
                if (aq.with.currentGameLocation == where.gameLocation.thisAsAddress) continue;

                if (aq.known < SocialControls.Instance.telephoneBookInclusionThreshold) continue;

                int frequency = 1;

                if (aq.secretConnection == Acquaintance.ConnectionType.paramour || aq.connections.Contains(Acquaintance.ConnectionType.paramour))
                {
                    frequency = 7;
                }
                else if (aq.connections.Contains(Acquaintance.ConnectionType.lover) && thisPerson.paramour == null)
                {
                    frequency = 4;
                }
                else
                {
                    frequency = Mathf.CeilToInt(aq.like * 3f);
                }

                for (int i = 0; i < frequency; i++)
                {
                    callpool.Add(aq);
                }
            }
        }

        if (callpool.Count > 0 || call != null)
        {
            //Payphone
            if(what != null && what.preset.isPayphone)
            {
                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.payphoneMoneyIn, who, where, what.GetWorldPosition());
            }

            if (callpool.Count > 0 && call == null)
            {
                Acquaintance callThisPerson = callpool[Toolbox.Instance.Rand(0, callpool.Count)];
                call = callThisPerson.with;
            }

            Game.Log("Phone: Citizen " + thisPerson.citizenName + " calling " + call.citizenName);

            //Answer receiver
            if (what.t != null)
            {
                if (what.t.activeReceiver == null)
                {
                    what.t.SetTelephoneAnswered(thisPerson);
                }

                //Pick a DDS conversation...
                string ddsRef = CitizenControls.Instance.fallbackTelephoneConversation;

                //Speech trigger point
                if (thisPerson.dds.ContainsKey(DDSSaveClasses.TriggerPoint.vmail))
                {
                    List<string> treePool = new List<string>();

                    foreach (DDSSaveClasses.DDSTreeSave sp in thisPerson.dds[DDSSaveClasses.TriggerPoint.vmail])
                    {
                        bool pass = true;

                        //Attempt to match conditions for participants
                        List<Human> otherParticipants = new List<Human>();

                        for (int i = 0; i < 4; i++)
                        {
                            //Participant A is always the initiator, aka this human
                            if (i == 0)
                            {
                                bool self = thisPerson.DDSParticipantConditionCheck(thisPerson, sp.participantA, DDSSaveClasses.TreeType.conversation);

                                if (!self)
                                {
                                    pass = false;
                                    break;
                                }
                            }
                            else
                            {
                                DDSSaveClasses.DDSParticipant participant = sp.participantB;
                                if (i == 2) participant = sp.participantC;
                                if (i == 3) participant = sp.participantD;

                                if (participant.required)
                                {
                                    bool pFound = false;

                                    //Scan contacts in a random order
                                    List<Acquaintance> scanAq = new List<Acquaintance>(thisPerson.acquaintances);
                                    int s = thisPerson.acquaintances.Count;

                                    while (scanAq.Count > 0 && s > 0)
                                    {
                                        int index = Toolbox.Instance.Rand(0, scanAq.Count);

                                        Acquaintance aq = scanAq[index];

                                        if (aq.with.DDSParticipantConditionCheck(thisPerson, participant, DDSSaveClasses.TreeType.conversation))
                                        {
                                            otherParticipants.Add(aq.with);
                                            pFound = true;
                                            break;
                                        }
                                        else
                                        {
                                            scanAq.RemoveAt(index);
                                        }

                                        s--;
                                    }

                                    if (!pFound)
                                    {
                                        //If this point is reached, we can't find actors to complete the required for the tree...
                                        pass = false;
                                        break;
                                    }
                                }
                                else continue; //Continued if we don't need this participant
                            }
                        }

                        if (!pass) continue; //If invalid participants, continue scanning for usable trees...

                        treePool.Add(sp.id);
                    }

                    if(treePool.Count > 0)
                    {
                        ddsRef = treePool[Toolbox.Instance.Rand(0, treePool.Count)];
                    }
                }

                if(call.home.telephones.Count > 0) TelephoneController.Instance.CreateNewCall(what.t, call.home.telephones[0], thisPerson, call, new TelephoneController.CallSource(TelephoneController.CallType.dds, ddsRef));
            }
        }
        //Complete current action
        else
        {
            if(thisPerson !=null && thisPerson.ai != null && thisPerson.ai.currentAction != null)
            {
                thisPerson.ai.currentAction.Complete();
            }
        }
    }

    public void Say(Interactable what, NewNode where, Actor who)
    {
        if (InteractionController.Instance.dialogMode && InteractionController.Instance.dialogOptions.Count > 0)
        {
            DialogButtonController button = InteractionController.Instance.dialogOptions[InteractionController.Instance.dialogSelection];
            DialogPreset preset = button.option.preset;

            if (!button.selectable) return; //Unselectable

            //Handle dialog input
            if (button.option.preset.inputBox != DialogPreset.InputSetting.none)
            {
                PopupMessageController.Instance.PopupMessage(button.option.preset.inputBox.ToString(), true, LButton: "Confirm", enableInputField: true);
                StartCoroutine(_DialogInputBox(what, where, who, button, preset)); //As we can't pass the needed params via the dialog box, run a coroutine to wait for input instead...
            }
            else
            {
                try
                {
                    _InvokeDialog(what, where, who, button, preset, DialogController.ForceSuccess.none);
                }
                catch
                {

                }
            }
        }
    }

    public IEnumerator _DialogInputBox(Interactable what, NewNode where, Actor who, DialogButtonController button, DialogPreset preset)
    {
        //Wait while the dialog box is present
        while(PopupMessageController.Instance.active)
        {
            yield return null;
        }

        DialogController.ForceSuccess forceSuccess = DialogController.ForceSuccess.fail;

        if(preset.inputBox == DialogPreset.InputSetting.addressPassword)
        {
            if(InteractionController.Instance.talkingTo != null)
            {
                Actor a = InteractionController.Instance.talkingTo.isActor;

                if(a != null)
                {
                    if (a.currentGameLocation != null && a.currentGameLocation.thisAsAddress != null)
                    {
                        if (PopupMessageController.Instance.inputField.text.ToLower() == a.currentGameLocation.thisAsAddress.GetPassword().ToLower())
                        {
                            Game.Log("Gameplay: Password is correct!");
                            forceSuccess = DialogController.ForceSuccess.success;
                        }
                        else Game.Log("Gameplay: Password is incorrect: " + PopupMessageController.Instance.inputField.text.ToLower() + " != " + a.currentGameLocation.thisAsAddress.GetPassword());
                    }
                }
            }
        }

        _InvokeDialog(what, where, who, button, preset, forceSuccess);
    }

    private void _InvokeDialog(Interactable what, NewNode where, Actor who, DialogButtonController button, DialogPreset preset, DialogController.ForceSuccess forceSuccess)
    {
        _InvokeDialog(what, where, who, button.option, preset, forceSuccess);
    }

    private void _InvokeDialog(Interactable what, NewNode where, Actor who, EvidenceWitness.DialogOption option, DialogPreset preset, DialogController.ForceSuccess forceSuccess)
    {
        Interactable to = what;
        bool success = DialogController.Instance.ExecuteDialog(option, InteractionController.Instance.talkingTo, where, who, forceSuccess);

        Game.Log("Gameplay: Executing dialog " + preset.name + " with forced success " + forceSuccess + " (" + success + ")");

        if (success)
        {
            int baseCost = preset.GetCost(to.isActor, who);

            //Apply cost
            if (preset.specialCase == DialogPreset.SpecialCase.medicalCosts)
            {
                int cost = Mathf.RoundToInt(baseCost * (1f - UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.reduceMedicalCosts)));

                if (cost > 0)
                {
                    GameplayController.Instance.AddMoney(-cost, true, "dialog_cost");
                }
            }
            else if (baseCost > 0)
            {
                GameplayController.Instance.AddMoney(-baseCost, true, "dialog_cost");
            }

            if(preset.specialCase == DialogPreset.SpecialCase.hotelBill)
            {
                GameplayController.HotelGuest g = Toolbox.Instance.GetHotelRoom(who as Human);

                if(g != null)
                {
                    g.PayBill(baseCost);
                }
            }
            //Pay & check out of a hotel room
            else if (preset.specialCase == DialogPreset.SpecialCase.hotelCheckOut)
            {
                GameplayController.HotelGuest g = Toolbox.Instance.GetHotelRoom(who as Human);

                if (g != null)
                {
                    g.PayBill(baseCost);
                }

                GameplayController.Instance.RemoveHotelGuest(g.GetAddress(), who as Human);
            }
            //Trigger hotel room renting
            else if(preset.specialCase == DialogPreset.SpecialCase.rentHotelRoomCheap)
            {
                GameplayController.Instance.AddHotelGuest(who as Human, false);
            }
            else if(preset.specialCase == DialogPreset.SpecialCase.rentHotelRoomExpensive)
            {
                GameplayController.Instance.AddHotelGuest(who as Human, true);
            }

            foreach (DialogPreset dp in preset.removeDialogOnSuccess)
            {
                (what.evidence as EvidenceWitness).RemoveDialogOption(dp.tiedToKey, dp);
            }

            //Add any follow ups
            foreach (DialogPreset dp in preset.followUpDialogSuccess)
            {
                Game.Log("Adding follow up dialog: " + dp.name);
                (what.evidence as EvidenceWitness).AddDialogOption(dp.tiedToKey, dp);
            }
        }
        else
        {
            foreach (DialogPreset dp in preset.removeDialogOnFail)
            {
                (what.evidence as EvidenceWitness).RemoveDialogOption(dp.tiedToKey, dp);
            }

            //Add any follow ups
            foreach (DialogPreset dp in preset.followUpDialogFail)
            {
                (what.evidence as EvidenceWitness).AddDialogOption(dp.tiedToKey, dp);
            }
        }

        //This ends the conversation immedaitely
        foreach (DialogPreset dp in preset.removeDialog)
        {
            (what.evidence as EvidenceWitness).RemoveDialogOption(dp.tiedToKey, dp);
        }
    }

    public void CrawlIntoVent(Interactable what, NewNode where, Actor who)
    {
        //The polymorphic reference should be a door controller
        if (Player.Instance.inAirVent)
        {
            Player.Instance.OnCrawlOutOfVent(what);
        }
        else
        {
            Player.Instance.OnCrawlIntoVent(what);

            SessionData.Instance.TutorialTrigger("airducts");
        }
    }

    public void UseKeypad(Interactable what, NewNode where, Actor who)
    {
        //Open evidence
        if (what.evidence != null)
        {
            if (who.isPlayer)
            {
                SessionData.Instance.PauseGame(true);

                InfoWindow win = InterfaceController.Instance.SpawnWindow(what.evidence, worldInteraction: true, autoPosition: false, forcePosition: new Vector2(0.5f, 0.5f), passedEvidenceKey: Evidence.DataKey.photo, passedInteractable: what);

                //Check for door entry in keyring
                Interactable door = what.thisDoor;

                if(door != null)
                {
                    //Check bypassed list
                    //if(Player.Instance.bypassedLocks.Contains(door.identifier))
                    //{
                    //    Game.Log("Player bypassed list includes id " + door.identifier);
                    //    InterfaceController.Instance.InputCodeButton(what.GetPasswordFromSource(out _));
                    //    return;
                    //}

                    //Check for actual door
                    NewDoor doorContainer = door.objectRef as NewDoor;

                    if(doorContainer != null)
                    {
                        if(Player.Instance.keyring.Contains(doorContainer))
                        {
                            Game.Log("Player bypassed list includes door " + doorContainer);
                            InterfaceController.Instance.InputCodeButton(what.GetPasswordFromSource(out _));
                        }
                    }
                }
            }
        }

        //AI use keypad
        if(who != null && !who.isPlayer)
        {
            Game.Log("USE KEYPAD BY AI: " + what.preset.name);

            //Unlock keypad
            what.SetLockedState(false, who);
            if(what.thisDoor != null) what.thisDoor.SetLockedState(false, who);
        }
    }

    public void NextPage(Interactable what, NewNode where, Actor who)
    {
        EvidenceMultiPage ev = what.evidence as EvidenceMultiPage;

        if(ev != null)
        {
            if (ev.page < what.controller.pages.Count) ev.SetPage(ev.page + 1, false);
        }

        //Reading delay
        if(what.preset.pageTurnReadingDelay > 0f)
        {
            what.readingDelay = what.preset.pageTurnReadingDelay;
        }
    }

    public void PreviousPage(Interactable what, NewNode where, Actor who)
    {
        EvidenceMultiPage ev = what.evidence as EvidenceMultiPage;

        if(ev != null)
        {
            if (ev.page > 0) ev.SetPage(ev.page - 1, false);
        }

        //Reading delay
        if (what.preset.pageTurnReadingDelay > 0f)
        {
            what.readingDelay = what.preset.pageTurnReadingDelay;
        }
    }

    public void SetCurrentMonth(Interactable what, NewNode where, Actor who)
    {
        //Set page in the evidence
        (what.evidence as EvidenceMultiPage).SetPage(SessionData.Instance.monthInt + 1, false);
    }

    public void Sleep(Interactable what, NewNode where, Actor who)
    {
        Hide(what, where, who);
    }

    public void GetUp(Interactable what, NewNode where, Actor who)
    {
        Return(what, where, who);
    }

    public void CallElevator(Interactable what, NewNode where, Actor who)
    {
        Elevator elevator = what.objectRef as Elevator;

        if(elevator != null)
        {
            Game.Log("Call elevator search for buttons...");

            //Find this button
            foreach(KeyValuePair<int, Elevator.ElevatorFloor> pair in elevator.elevatorFloors)
            {
                if(pair.Value.downButton == what)
                {
                    elevator.CallElevator(pair.Value.floor, false);
                    return;
                }
                else if(pair.Value.upButton == what)
                {
                    elevator.CallElevator(pair.Value.floor, true);
                    return;
                }
            }
        }
        else
        {
            Game.Log("No elevator reference!");
        }
    }

    public void PassTime(Interactable what, NewNode where, Actor who)
    {
        //Set as locked in interaction #2
        Player.Instance.OnHide(what, 1, true);

        //Bring up watch
        BioScreenController.Instance.SelectSlot(FirstPersonItemController.Instance.slots.Find(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.watch));

        Player.Instance.setAlarmModeAfterDelay = 1f;
    }

    public void CancelPassTime(Interactable what, NewNode where, Actor who)
    {
        Player.Instance.SetSpendingTimeMode(false);

        //Don't allow return position write as the player will already be sitting
        Player.Instance.OnHide(what, 0, true, allowReturnPositionWrite: false);
    }

    //Toggle between setting hours/minutes on the watch alarm
    public void HoursMinutesToggle(Interactable what, NewNode where, Actor who)
    {
        //Play sfx
        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.watchToggleHoursMinutes, Player.Instance, Player.Instance.currentNode, Player.Instance.lookAtThisTransform.position);

        Player.Instance.editingHours = !Player.Instance.editingHours;

        SessionData.Instance.UpdateUIClock();
        SessionData.Instance.UpdateUIDay();
    }

    //Activate time passing
    public void ActivateTimePass(Interactable what, NewNode where, Actor who)
    {
        //Play sfx
        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.setAlarm, Player.Instance, Player.Instance.currentNode, Player.Instance.lookAtThisTransform.position);

        //Cancel alarm mode
        Player.Instance.SetSettingAlarmMode(false);

        //Cancel this
        Player.Instance.OnHide(what, 0, true, allowReturnPositionWrite: false); //Don't allow writing of return position here as we want to use the transition from the chair instead

        //Put away watch
        BioScreenController.Instance.SelectSlot(FirstPersonItemController.Instance.slots.Find(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.holster));

        //Set speed up time (short delay needed to avoid transition reset)
        Player.Instance.spendingTimeDelay = 0.5f;

        Game.Log("Player: Activate time pass...");
    }

    public void WatchForward(Interactable what, NewNode where, Actor who)
    {
        if (Player.Instance.editingHours)
        {
            Player.Instance.AddToAlarmTime(1f);
        }
        else
        {
            Player.Instance.AddToAlarmTime(1f / 60f);
        }
    }

    public void WatchBack(Interactable what, NewNode where, Actor who)
    {
        if (Player.Instance.editingHours)
        {
            Player.Instance.AddToAlarmTime(-1f);
        }
        else
        {
            Player.Instance.AddToAlarmTime(-1f / 60f);
        }
    }

    //Same as hide, but skip transition animation
    public void HideInstant(Interactable what, NewNode where, Actor who)
    {
        Player.Instance.OnHide(what, 0, true);
    }

    public void BargeDoor(Interactable what, NewNode where, Actor who)
    {
        NewDoor door = what.objectRef as NewDoor;

        if(door != null)
        {
            if (!door.isClosed) return; //Only do this if closed
        }

        if(who.isPlayer)
        {
            bargeDoor = what;

            //Barge animation
            Player.Instance.TransformPlayerController(GameplayControls.Instance.bargeDoorEnter, null, what, null);

            //Auto return
            Player.Instance.OnTransitionCompleted += BargeReturn;
        }
        else
        {
            who.ai.bargeTimer = 0f;
        }
    }

    public void BargeReturn(bool restoreTransform = false)
    {
        Game.Log("Player: Barge return...");
        Player.Instance.OnTransitionCompleted -= BargeReturn;

        if (bargeDoor == null) return;
        NewDoor bargeD = bargeDoor.objectRef as NewDoor;

        bargeD.Barge(Player.Instance);
    }

    public void UseComputer(Interactable what, NewNode where, Actor who)
    {
        Player.Instance.OnUseComputer(what);
    }

    public void ReturnComputer(Interactable what, NewNode where, Actor who)
    {
        Player.Instance.OnReturnFromUseComputer();
    }

    public void TriggerAlarm(Interactable what, NewNode where, Actor who)
    {
        if(where != null && where.gameLocation != null && where.gameLocation.thisAsAddress != null)
        {
            if (!what.sw0) return; //Disabled

            Interactable breaker = where.gameLocation.thisAsAddress.GetBreakerSecurity();
            if (breaker != null && !breaker.sw0) return; //Don't do this if breaker is tripped

            Human target = null;

            if(who.ai != null && who.ai.persuitTarget != null)
            {
                target = who.ai.persuitTarget as Human;
            }
            else if(who != null)
            {
                Human h = who as Human;

                if(h != null && h.lastScaredBy != null)
                {
                    target = h.lastScaredBy as Human;
                }
            }

            where.gameLocation.thisAsAddress.SetAlarm(true, target);

            if (who != null)
            {
                who.AddNerve(CitizenControls.Instance.nerveAlarmSwitched); //Gives actor nerve restore
            }
        }
    }

    public void Search(Interactable what, NewNode where, Actor who)
    {
        if(who.isPlayer)
        {
            Player.Instance.OnSearch(what);

            //Trigger case
            if(what.isActor != null)
            {
                if(what.isActor.isDead && MurderController.Instance.activeMurders.Exists(item => item.victim == what.isActor))
                {
                    MurderController.Instance.OnVictimDiscovery();
                }
            }
        }
    }

    public void Vomit(Interactable what, NewNode where, Actor who)
    {
        if (who.isPlayer)
        {
            Player.Instance.OnGenericTimedAction("Vomiting", 1f, 0.5f, what);
        }
    }

    public void TakePrint(Interactable what, NewNode where, Actor who)
    {
        Player.Instance.OnTakePrint(what);
    }

    public void NextChoice(Interactable what, NewNode where, Actor who)
    {
        if(InteractionController.Instance.dialogMode)
        {
            InteractionController.Instance.SetDialogSelection(InteractionController.Instance.dialogSelection - 1);
        }
    }

    public void PreviousChoice(Interactable what, NewNode where, Actor who)
    {
        if (InteractionController.Instance.dialogMode)
        {
            InteractionController.Instance.SetDialogSelection(InteractionController.Instance.dialogSelection + 1);
        }
    }

    public void TakeFirstPersonItem(Interactable what, NewNode where, Actor who)
    {
        SessionData.Instance.TutorialTrigger("inventory");

        FirstPersonItemController.Instance.PickUpItem(what, false, true);
    }

    IEnumerator AddFirstPersonItemDelay(Interactable newInteractable)
    {
        float delay = 0.9f;

        while (delay > 0f)
        {
            delay -= Time.deltaTime;
            yield return null;
        }

        FirstPersonItemController.Instance.PickUpItem(newInteractable, false);
    }

    public void TakeFirstPersonItemUsed(Interactable what, NewNode where, Actor who)
    {
        if(what.sw0)
        {
            FirstPersonItemController.Instance.PickUpItem(what, false, true);
        }
    }

    public void Buy(Interactable what, NewNode where, Actor who)
    {
        if(who.isPlayer)
        {
            Game.Log("Interface: Spawn buy select...");

            SessionData.Instance.PauseGame(true);
            InterfaceController.Instance.SpawnWindow(null, presetName: "Buy", autoPosition: false, forcePosition: InterfaceControls.Instance.handbookWindowPosition, worldInteraction: true, passedInteractable: what);
        }
    }

    public void TakeConsumable(Interactable what, NewNode where, Actor who)
    {
        //Choose an item compatible with needs...
        Human human = who as Human;

        if(human != null)
        {
            Dictionary<InteractablePreset, int> prices = new Dictionary<InteractablePreset, int>();
            Company company = null;

            bool createReceipt = true;

            //Search for menu override
            if(what.preset.specialCaseFlag == InteractablePreset.SpecialCase.fridge)
            {
                foreach (Interactable ip in what.furnitureParent.spawnedInteractables)
                {
                    if(ip.preset.retailItem != null && !prices.ContainsKey(ip.preset))
                    {
                        prices.Add(ip.preset, 0);
                    }
                }

                createReceipt = false;
            }
            else if (what.preset.menuOverride != null)
            {
                //For vending machines etc, use a relatively expensive price
                foreach (InteractablePreset ip in what.preset.menuOverride.itemsSold)
                {
                    int newPrice = Mathf.RoundToInt(Mathf.Lerp(ip.value.x, ip.value.y, 1f));
                    prices.Add(ip, newPrice);
                }

                createReceipt = what.preset.menuOverride.createReceipt;

                if(what.preset.menuOverride.purchaseAudio != null)
                {
                    AudioController.Instance.PlayWorldOneShot(what.preset.menuOverride.purchaseAudio, who, what.node, what.wPos, what);
                }
            }
            //Search for company at address
            else if(where.gameLocation.thisAsAddress != null && where.gameLocation.thisAsAddress.company != null)
            {
                prices = where.gameLocation.thisAsAddress.company.prices;
                company = where.gameLocation.thisAsAddress.company;
            }
            //Search for self employed
            else if(what.furnitureParent != null && what.furnitureParent.ownerMap.Count > 0)
            {
                foreach(KeyValuePair<FurnitureLocation.OwnerKey, int> pair in what.furnitureParent.ownerMap)
                {
                    if(pair.Key.human != null)
                    {
                        if(pair.Key.human.job != null && pair.Key.human.job.employer != null && pair.Key.human.job.preset.selfEmployed)
                        {
                            prices = pair.Key.human.job.employer.prices;
                            company = pair.Key.human.job.employer;
                            break;
                        }
                    }
                }
            }

            //Take up to 3 items...
            List<InteractablePreset> pickedConsumables = new List<InteractablePreset>();

            float saveNourishment = human.nourishment;
            float saveHydration = human.hydration;
            float saveAlertness = human.alertness;
            float saveEnergy = human.energy;
            float saveExcitement = human.excitement;
            float saveHygeine = human.hygiene;
            float saveChores = human.chores;
            float saveHeat = human.heat;

            for (int i = 0; i < 3; i++)
            {
                InteractablePreset consumable = human.PickConsumable(ref prices, out _, pickedConsumables);

                if (consumable != null)
                {
                    pickedConsumables.Add(consumable);

                    human.AddCurrentConsumable(consumable);

                    //Do I need something else? Temporarily apply stats, then revert...
                    if(consumable.retailItem != null)
                    {
                        human.nourishment += consumable.retailItem.nourishment;
                        human.currentHealth += consumable.retailItem.health;
                        human.hydration += consumable.retailItem.hydration;
                        human.alertness += consumable.retailItem.alertness;
                        human.energy += consumable.retailItem.energy;
                        human.excitement += consumable.retailItem.excitement;
                        human.hygiene += consumable.retailItem.hygiene;
                        human.chores += consumable.retailItem.chores;
                        human.heat += consumable.retailItem.heat;
                    }

                    if (Toolbox.Instance.Rand(0f, 1f) > 2.5f + (human.societalClass * 0.75f)) break; //Use soc class to determin stoppage
                }
                else break;
            }

            human.nourishment = saveNourishment;
            human.hydration = saveHydration;
            human.alertness = saveAlertness;
            human.energy = saveEnergy;
            human.excitement = saveExcitement;
            human.hygiene = saveHygeine;
            human.chores = saveChores;
            human.heat = saveHeat;

            //Create receipt...
            if (pickedConsumables.Count > 0)
            {
                if(createReceipt)
                {
                    List<Interactable.Passed> passed = new List<Interactable.Passed>();
                    if (company != null) passed.Add(new Interactable.Passed(Interactable.PassedVarType.companyID, company.companyID));
                    passed.Add(new Interactable.Passed(Interactable.PassedVarType.time, SessionData.Instance.gameTime));

                    for (int i = 0; i < pickedConsumables.Count; i++)
                    {
                        passed.Add(new Interactable.Passed(Interactable.PassedVarType.stringInteractablePreset, -1, pickedConsumables[i].name));
                    }

                    human.AddTrash(InteriorControls.Instance.receipt, human, passed);
                }

                if (company != null && company.preset.recordSalesData)
                {
                    company.AddSalesRecord(human, pickedConsumables, SessionData.Instance.gameTime);
                }
            }
        }
    }

    public void MakeCoffeeStart(Interactable what, NewNode where, Actor who)
    {
        if (what.furnitureParent == null) return;

        //Position kettle...
        Interactable kettle = null;

        FurniturePreset.SubObject hob = what.furnitureParent.furniture.subObjects.Find(item => item.preset.name == "HobBoilPoint");

        foreach(NewNode n in where.room.nodes)
        {
            foreach (Interactable i in n.interactables)
            {
                if (i.preset == InteriorControls.Instance.stovetopKettle)
                {
                    if (i.inInventory == null && (i.controller == null || !i.controller.isCarriedByPlayer))
                    {
                        kettle = i;
                        kettle.ConvertToFurnitureSpawnedObject(what.furnitureParent, hob, true, true);
                        //Game.Log("Found existing kettle in room!");
                        break;
                    }
                }
            }

            if (kettle != null) break;
        }

        //Create a new kettle
        if (kettle == null)
        {
            kettle = InteractableCreator.Instance.CreateFurnitureSpawnedInteractable(InteriorControls.Instance.stovetopKettle, what.furnitureParent, hob, null, null, null, null, null, null);
            //Game.Log("Created new kettle!");
        }
    }

    public void MakeCoffeeEnd(Interactable what, NewNode where, Actor who)
    {
        Human human = who as Human;

        if(human != null)
        {
            if(human.characterTraits.Exists(item => item.trait == CitizenControls.Instance.coffeeLiker))
            {
                human.AddCurrentConsumable(InteriorControls.Instance.coffeeHomemade);
            }
            else if(human.characterTraits.Exists(item => item.trait == CitizenControls.Instance.teaLiker))
            {
                human.AddCurrentConsumable(InteriorControls.Instance.teaHomemade);
            }
            else
            {
                if (Toolbox.Instance.Rand(0f, 1f) > 0.3f)
                {
                    human.AddCurrentConsumable(InteriorControls.Instance.coffeeHomemade);
                }
                else
                {
                    human.AddCurrentConsumable(InteriorControls.Instance.teaHomemade);
                }
            }
        }
    }

    public void TurnOnHob(Interactable what, NewNode where, Actor who)
    {
        //Activate kettles in close proximity
        if(what.furnitureParent != null)
        {
            FurniturePreset.SubObject hob = what.furnitureParent.furniture.subObjects.Find(item => item.preset.name == "HobBoilPoint");

            if (hob != null)
            {
                Matrix4x4 m = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(new Vector3(0, what.furnitureParent.angle + what.furnitureParent.diagonalAngle, 0)), Vector3.one);
                Vector3 wPos = m.MultiplyPoint3x4(hob.localPos) + what.furnitureParent.anchorNode.position; //World position of hob

                foreach (Interactable i in where.interactables)
                {
                    if(i.preset == InteriorControls.Instance.stovetopKettle)
                    {
                        float dist = Vector3.Distance(wPos, i.GetWorldPosition());

                        //Game.Log("Kettle is " + dist + " away from active hob...");

                        if(dist < 0.3f)
                        {
                            i.SetSwitchState(true, null);
                        }
                    }
                }
            }
        }
    }

    public void TurnOffHob(Interactable what, NewNode where, Actor who)
    {
        foreach (Interactable i in where.interactables)
        {
            if (i.preset == InteriorControls.Instance.stovetopKettle)
            {
                 i.SetSwitchState(false, null);
            }
        }
    }

    public void TurnOnMusic(Interactable what, NewNode where, Actor who)
    {
        if(where != null)
        {
            Game.Log("TURN ON MUSIC: " + where.room.name);
            where.room.musicPlaying = true;
            where.room.musicStartedAt = SessionData.Instance.gameTime;

            foreach (Actor a in where.room.currentOccupants)
            {
                if (a.ai != null && a != who)
                {
                    a.ai.AITick();
                }
            }
        }
    }

    public void TurnOffMusic(Interactable what, NewNode where, Actor who)
    {
        if (where != null)
        {
            where.room.musicPlaying = false;

            foreach (NewNode n in where.room.nodes)
            {
                foreach(Interactable i in n.interactables)
                {
                    if (i == what) continue;

                    if(i.aiActionReference.ContainsKey(RoutineControls.Instance.turnOnMusic))
                    {
                        if(i.sw0)
                        {
                            where.room.musicPlaying = true;
                            break;
                        }
                    }
                }
            }

            Game.Log("SET MUSIC: " + where.room.name + " " + where.room.musicPlaying);
        }
    }

    public void PurchaseItem(Interactable what, NewNode where, Actor who)
    {
        //Choose an item compatible with needs...
        Human human = who as Human;

        if (human != null)
        {
            Dictionary<InteractablePreset, int> prices = new Dictionary<InteractablePreset, int>();
            Company company = null;

            bool createReceipt = true;

            //Search for menu override
            if (what.preset.menuOverride != null)
            {
                //For vending machines etc, use a relatively expensive price
                foreach (InteractablePreset ip in what.preset.menuOverride.itemsSold)
                {
                    int newPrice = Mathf.RoundToInt(Mathf.Lerp(ip.value.x, ip.value.y, 1f));
                    prices.Add(ip, newPrice);
                }

                createReceipt = what.preset.menuOverride.createReceipt;

                if (what.preset.menuOverride.purchaseAudio != null)
                {
                    AudioController.Instance.PlayWorldOneShot(what.preset.menuOverride.purchaseAudio, who, what.node, what.wPos, what);
                }
            }
            //Search for company at address
            else if (where.gameLocation.thisAsAddress != null && where.gameLocation.thisAsAddress.company != null)
            {
                prices = where.gameLocation.thisAsAddress.company.prices;
                company = where.gameLocation.thisAsAddress.company;
            }
            //Search for self employed
            else if (what.furnitureParent != null && what.furnitureParent.ownerMap.Count > 0)
            {
                foreach (KeyValuePair<FurnitureLocation.OwnerKey, int> pair in what.furnitureParent.ownerMap)
                {
                    if(pair.Key.human != null)
                    {
                        if (pair.Key.human.job != null && pair.Key.human.job.employer != null && pair.Key.human.job.preset.selfEmployed)
                        {
                            prices = pair.Key.human.job.employer.prices;
                            company = pair.Key.human.job.employer;
                            break;
                        }
                    }
                }
            }

            List<InteractablePreset> pickedConsumables = new List<InteractablePreset>();

            if (human.ai != null && human.ai.currentAction != null && human.ai.currentAction.passedAcquireItems != null)
            {
                foreach(InteractablePreset ip in human.ai.currentAction.passedAcquireItems)
                {
                    pickedConsumables.Add(ip);

                    Interactable newObj = InteractableCreator.Instance.CreateWorldInteractable(ip, human, human, null, Vector3.zero, Vector3.zero, null, null);
                    if (newObj != null) newObj.SetInInventory(human);
                }

                if (human.ai.currentGoal.murderRef != null)
                {
                    human.ai.currentGoal.murderRef.EuipmentCheck();
                }
            }

            //Create receipt...
            if (pickedConsumables.Count > 0)
            {
                if(createReceipt)
                {
                    List<Interactable.Passed> passed = new List<Interactable.Passed>();
                    if (company != null) passed.Add(new Interactable.Passed(Interactable.PassedVarType.companyID, company.companyID));
                    passed.Add(new Interactable.Passed(Interactable.PassedVarType.time, SessionData.Instance.gameTime));

                    for (int i = 0; i < pickedConsumables.Count; i++)
                    {
                        passed.Add(new Interactable.Passed(Interactable.PassedVarType.stringInteractablePreset, -1, pickedConsumables[i].name));
                    }

                    human.AddTrash(InteriorControls.Instance.receipt, human, passed);
                }

                if(company != null && company.preset.recordSalesData)
                {
                    company.AddSalesRecord(human, pickedConsumables, SessionData.Instance.gameTime);
                }
            }
        }
    }

    //Done on end of consume...
    public void Consume(Interactable what, NewNode where, Actor who)
    {
        Human human = who as Human;

        int safety = 99;

        while(human != null && human.currentConsumables.Count > 0 && safety > 0)
        {
            human.RemoveCurrentConsumable(human.currentConsumables[0]);
            safety--;
        }
    }

    //Dispose of an item into a bin
    public void Dispose(Interactable what, NewNode where, Actor who)
    {
        if(who != null && who.isPlayer && what != null && BioScreenController.Instance.selectedSlot != null)
        {
            Human human = who as Human;

            //Street cleaner
            int streetCleaningMoney = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.streetCleaningMoney));

            //Trash item
            Interactable trashItem = BioScreenController.Instance.selectedSlot.GetInteractable();

            if(trashItem != null)
            {
                if (streetCleaningMoney > 0)
                {
                    if (what.preset.specialCaseFlag == InteractablePreset.SpecialCase.garbageDisposal || (what.preset.prefab != null && what.preset.prefab.CompareTag("Garbage")) || (what.spawnedObject != null && what.spawnedObject.CompareTag("Garbage")))
                    {
                        Game.Log("Player: Disposal in trash");

                        if (trashItem.belongsTo != Player.Instance && (CleanupController.Instance.trash.Contains(trashItem) 
                            || (trashItem.preset.retailItem != null && trashItem.cs <= 0.1f && trashItem.preset.retailItem.isConsumable))
                            )
                        {
                            trashItem.SafeDelete();
                            GameplayController.Instance.AddMoney(streetCleaningMoney, true, "readingforstreetcleaning");
                        }
                        else
                        {
                            Game.Log("Player: This item is not trash");
                        }
                    }
                }

                //Dispose of trash...
                if (trashItem != null)
                {
                    EvidenceMultiPage mp = what.evidence as EvidenceMultiPage;

                    if (mp != null)
                    {
                        //Convert to meta object
                        MetaObject t = new MetaObject(trashItem.preset, trashItem.belongsTo, trashItem.writer, trashItem.reciever, trashItem.pv);

                        List<EvidenceMultiPage.MultiPageContent> contained = mp.pageContent.FindAll(item => item.meta > 0);

                        if (contained.Count >= GameplayControls.Instance.binTrashLimit)
                        {
                            //Remove lowest page number
                            int lowestPageNumber = 9999999;
                            EvidenceMultiPage.MultiPageContent toRemove = null;

                            foreach (EvidenceMultiPage.MultiPageContent emp in contained)
                            {
                                if (emp.page < lowestPageNumber)
                                {
                                    lowestPageNumber = emp.page;
                                    toRemove = emp;
                                }
                            }

                            if (toRemove != null)
                            {
                                if (toRemove.meta > 0)
                                {
                                    MetaObject mo = CityData.Instance.FindMetaObject(toRemove.meta);
                                    if (mo != null) mo.Remove();
                                }

                                mp.pageContent.Remove(toRemove);
                            }

                            mp.AddContainedMetaObjectToPage(lowestPageNumber, t);
                        }
                        else
                        {
                            mp.AddContainedMetaObjectToNewPage(t);
                            CleanupController.Instance.binTrash++;
                        }
                    }

                    //Empty inventory
                    FirstPersonItemController.Instance.EmptySlot(BioScreenController.Instance.selectedSlot, false, true, false, true);
                }
            }
        }
        else if(who != null && !who.isPlayer)
        {
            Human human = who as Human;

            if(human != null)
            {
                //Dispose of all trash...
                if (what != null)
                {
                    EvidenceMultiPage mp = what.evidence as EvidenceMultiPage;

                    if (mp != null)
                    {
                        for (int i = 0; i < human.trash.Count; i++)
                        {
                            MetaObject t = CityData.Instance.FindMetaObject(human.trash[i]);

                            if (t != null)
                            {
                                InteractablePreset p = null;
                                Toolbox.Instance.objectPresetDictionary.TryGetValue(t.preset, out p);

                                if (p.disposal == Human.DisposalType.homeOnly && !human.isHome) continue;
                                else if (p.disposal == Human.DisposalType.workOnly && human.job.employer != null && !human.isAtWork) continue;
                                else if (p.disposal == Human.DisposalType.homeOrWork && !human.isHome && !human.isAtWork) continue;

                                if (ChapterController.Instance != null && ChapterController.Instance.chapterScript != null)
                                {
                                    ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                                    if (intro != null)
                                    {
                                        if (human == intro.kidnapper)
                                        {
                                            Game.Log("Chapter: Kidnapper " + human.GetCitizenName() + ": Bin trash " + p.name + " " + what.name + " " + where.room.name + " " + where.gameLocation.name);
                                        }
                                    }
                                }

                                List<EvidenceMultiPage.MultiPageContent> contained = mp.pageContent.FindAll(item => item.meta > 0);

                                if (contained.Count >= GameplayControls.Instance.binTrashLimit)
                                {
                                    //Remove lowest page number
                                    int lowestPageNumber = 9999999;
                                    EvidenceMultiPage.MultiPageContent toRemove = null;

                                    foreach (EvidenceMultiPage.MultiPageContent emp in contained)
                                    {
                                        if (emp.page < lowestPageNumber)
                                        {
                                            lowestPageNumber = emp.page;
                                            toRemove = emp;
                                        }
                                    }

                                    if (toRemove != null)
                                    {
                                        MetaObject mo = CityData.Instance.FindMetaObject(toRemove.meta);
                                        if (mo != null) mo.Remove();

                                        mp.pageContent.Remove(toRemove);
                                    }

                                    mp.AddContainedMetaObjectToPage(lowestPageNumber, t);
                                }
                                else
                                {
                                    mp.AddContainedMetaObjectToNewPage(t);
                                    CleanupController.Instance.binTrash++;
                                }

                                if (p.disposal == Human.DisposalType.anywhere) human.anywhereTrash--; //Dispose of anywhere trash...
                                human.trash.RemoveAt(i);
                                i--;
                            }
                            else
                            {
                                human.trash.RemoveAt(i);
                                i--;
                            }
                        }
                    }
                }
            }
        }
    }

    //Post a job
    public void PostJob(Interactable what, NewNode where, Actor who)
    {
        //Get the job ID from current goal
        if(who.ai != null && who.ai.currentGoal != null)
        {
            SideJob job = null;

            if(SideJobController.Instance.allJobsDictionary.TryGetValue(who.ai.currentGoal.jobID, out job))
            {
                if(job.post != null)
                {
                    Game.LogError("Jobs: Post for job " + job.jobID + " already exists! Not posting...");
                    return;
                }

                List<FurniturePreset.SubObject> placePool = new List<FurniturePreset.SubObject>();
                FurnitureLocation jobBoard = what.furnitureParent;

                if(jobBoard != null)
                {
                    for (int i = 0; i < jobBoard.furniture.subObjects.Count; i++)
                    {
                        FurniturePreset.SubObject so = jobBoard.furniture.subObjects[i];
                        if (!job.preset.jobPosting.subObjectClasses.Contains(so.preset)) continue; //class must match...
                        if (jobBoard.spawnedInteractables.Exists(item => item.subObject == so)) continue; //item is already here...
                        placePool.Add(so);
                    }

                    //chosen position
                    if (placePool.Count > 0)
                    {
                        FurniturePreset.SubObject chosenLocation = placePool[Toolbox.Instance.Rand(0, placePool.Count)];

                        List<Interactable.Passed> spawnPass = new List<Interactable.Passed>();
                        spawnPass.Add(new Interactable.Passed(Interactable.PassedVarType.jobID, job.jobID));

                        //Immediately create post...
                        job.post = InteractableCreator.Instance.CreateFurnitureSpawnedInteractable(job.preset.jobPosting, jobBoard, chosenLocation, job.poster, job.poster, null, spawnPass, null, null, ddsOverride: job.preset.startingScenarios[job.startingScenario].dds);
                        job.postID = job.post.id;
                        Game.Log("Jobs: Job posted manually by citizen at " + jobBoard.anchorNode.gameLocation.name);

                        job.SetJobState(SideJob.JobState.posted);
                    }
                }
                else
                {
                    Game.LogError("Jobs: Post job board location is null");
                }
            }
            else
            {
                Game.LogError("Jobs: Unable to find job with ID " + who.ai.currentGoal.jobID);
            }
        }
        else
        {
            Game.LogError("Jobs: Unable to post as current goal or AI for citizen is inactive!");
        }
    }

    //AI log on computer
    public void LogOnComputer(Interactable what, NewNode where, Actor who)
    {
        Human hu = who as Human;

        if(hu != null && !hu.isPlayer)
        {
            if (what.controller != null && what.controller.computer != null)
            {
                //Log in...
                what.controller.computer.SetLoggedIn(hu);
            }
            else
            {
                what.SetValue(hu.humanID);
                what.SetLockedState(false, hu);
            }
        }
    }

    //Sabotage security camera
    //public void Sabotage(Interactable what, NewNode where, Actor who)
    //{
    //    if(what != null)
    //    {
    //        what.SetSwitchState(false, who);
    //    }
    //}

    //Sabotage security camera
    public void Sabotage(Interactable what, NewNode where, Actor who)
    {
        if (what != null)
        {
            if (GameplayController.Instance.lockPicks > 0)
            {
                InteractionController.Instance.OnSabotage(what);
            }
            else
            {
                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "not_enough_lockpicks"), InterfaceControls.Icon.lockpick, null, colourOverride: true, col: InterfaceControls.Instance.messageRed, ping: GameMessageController.PingOnComplete.lockpicks);
            }
        }
    }

    public void DryOff(Interactable what, NewNode where, Actor who)
    {
        if (who != null)
        {
            Human h = who as Human;

            if(h != null)
            {
                h.AddWet(-1);
                h.AddHeat(0.2f);
            }
        }
    }

    public void OpenSyncDisks(Interactable what, NewNode where, Actor who)
    {
        SessionData.Instance.PauseGame(true);

        if (!UpgradesController.Instance.isOpen)
        {
            UpgradesController.Instance.OpenUpgrades();
        }
    }

    //AI calls enforcers
    public void CallEnforcers(Interactable what, NewNode where, Actor who)
    {
        NewGameLocation loc = who.currentGameLocation;
        Human.Death.ReportType reportType = Human.Death.ReportType.visual;

        //Payphone
        if (what != null && what.preset.isPayphone)
        {
            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.payphoneMoneyIn, who, where, what.GetWorldPosition());
        }

        if (who != null)
        {
            //Enforcer call
            who.speechController.Speak("2cfd78b6-9980-4f52-b692-d0c2d0dfb4e9", interupt: true);
        }

        bool successfulReport = false;

        //Look for location of passed interactable
        if (who.ai != null)
        {
            NewAIGoal smellBody = who.ai.goals.Find(item => item.preset == RoutineControls.Instance.smellDeadBody);

            if (smellBody != null && smellBody.passedInteractable != null)
            {
                Human h = smellBody.passedInteractable.isActor as Human;

                if (h != null && h.death != null && h.death.isDead && !h.death.reported && !h.unreportable)
                {
                    loc = h.death.GetDeathLocation();
                    reportType = (Human.Death.ReportType)(int)smellBody.passedVar;

                    GameplayController.Instance.CallEnforcers(loc);
                    h.death.SetReported(who as Human, reportType);
                    successfulReport = true;
                }
            }
        }

        if(!successfulReport)
        {
            for (int i = 0; i < CityData.Instance.deadCitizensDirectory.Count; i++)
            {
                Human dead = CityData.Instance.deadCitizensDirectory[i];
                if (dead.death == null) continue;

                if (!dead.unreportable && dead.death.isDead && !dead.death.reported)
                {
                    GameplayController.Instance.CallEnforcers(dead.death.GetDeathLocation());
                    dead.death.SetReported(who as Human, Human.Death.ReportType.visual);
                }
            }
        }
    }

    //Put up police tape
    public void PutUpPoliceTape(Interactable what, NewNode where, Actor who)
    {
        bargeDoor = what;
        NewDoor door = bargeDoor.objectRef as NewDoor;

        if (door != null)
        {
            door.SetForbidden(true);
        }
    }

    public void RemovePoliceTape(Interactable what, NewNode where, Actor who)
    {
        bargeDoor = what;
        NewDoor door = bargeDoor.objectRef as NewDoor;

        if (door != null)
        {
            door.SetForbidden(false);
        }
    }

    public void PutUpStreetCrimeScene(Interactable what, NewNode where, Actor who)
    {
        Game.Log("Attempting to create street crime scene at " + what.node.position);
        Interactable streetScene = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.streetCrimeScene, null, null, null, what.node.position, Vector3.zero, null, null);
    }

    //Get case forms
    public void GetCaseForm(Interactable what, NewNode where, Actor who)
    {
        foreach(Case c in CasePanelController.Instance.activeCases)
        {
            if((c.caseType == Case.CaseType.murder || c.caseType == Case.CaseType.mainStory || c.caseType == Case.CaseType.retirement) && c.caseStatus == Case.CaseStatus.handInNotCollected)
            {
                c.SetStatus(Case.CaseStatus.handInCollected);

                //Display message
                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "Collected Handin") + ": " + c.name, InterfaceControls.Icon.resolve);
            }
        }

        CasePanelController.Instance.UpdateCloseCaseButton();
    }

    //Get case forms
    public void HandInCase(Interactable what, NewNode where, Actor who)
    {
        if(CasePanelController.Instance.activeCase != null && CasePanelController.Instance.activeCase.caseStatus == Case.CaseStatus.handInCollected && CasePanelController.Instance.activeCase.handInValid)
        {
            Game.Log("Jobs: Hand in case...");

            //Side jobs do this internally
            if(CasePanelController.Instance.activeCase.caseType == Case.CaseType.mainStory || CasePanelController.Instance.activeCase.caseType == Case.CaseType.murder)
            {
                //Set to submitted
                if(!GameplayController.Instance.caseProcessing.ContainsKey(CasePanelController.Instance.activeCase))
                {
                    GameplayController.Instance.caseProcessing.Add(CasePanelController.Instance.activeCase, SessionData.Instance.gameTime);
                }
            
                CasePanelController.Instance.activeCase.SetStatus(Case.CaseStatus.submitted);

                //Display message
                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "Case Submitted"), InterfaceControls.Icon.resolve);
            }
            //Search for side missions to trigger objectives with
            else if(CasePanelController.Instance.activeCase.caseType == Case.CaseType.sideJob)
            {
                if(CasePanelController.Instance.activeCase.job != null)
                {
                    if(CasePanelController.Instance.activeCase.handIn.Contains(what.id))
                    {
                        foreach (Objective obj in CasePanelController.Instance.activeCase.currentActiveObjectives)
                        {
                            foreach (Objective.ObjectiveTrigger trg in obj.queueElement.triggers)
                            {
                                if (trg.triggered) continue;

                                if (trg.triggerType == Objective.ObjectiveTriggerType.submitCase)
                                {
                                    trg.Trigger();
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach(int i in CasePanelController.Instance.activeCase.handIn)
                        {
                            Game.Log("Jobs: Checking for peek interactable " + i);

                            NewDoor d = null;

                            if (CityData.Instance.doorDictionary.TryGetValue(-i, out d))
                            {
                                if(what == d.peekInteractable)
                                {
                                    Game.Log("Jobs: Found peek interactable " + i + ", checking " + CasePanelController.Instance.activeCase.currentActiveObjectives.Count + " objectives for submit case...");

                                    foreach (Objective obj in CasePanelController.Instance.activeCase.currentActiveObjectives)
                                    {
                                        foreach (Objective.ObjectiveTrigger trg in obj.queueElement.triggers)
                                        {
                                            if (trg.triggered) continue;

                                            if (trg.triggerType == Objective.ObjectiveTriggerType.submitCase)
                                            {
                                                trg.Trigger();
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Game.Log("Job: Peek interactable " + (-i) + " was not found in the door dictionary...");
                            }
                        }
                    }
                }
                else
                {
                    Game.LogError("Active case does not contain job reference!");
                }
            }
            else if(CasePanelController.Instance.activeCase.caseType == Case.CaseType.retirement)
            {
                PopupMessageController.Instance.PopupMessage("Retire", true, true, RButton: "Confirm");
                PopupMessageController.Instance.OnRightButton += RetirementConfirm;
                PopupMessageController.Instance.OnLeftButton += RetirementCancel;
            }

            //Update actions to remove option
            what.UpdateCurrentActions();
        }
    }

    public void RetirementConfirm()
    {
        PopupMessageController.Instance.OnRightButton -= RetirementConfirm;
        PopupMessageController.Instance.OnLeftButton -= RetirementCancel;

        //Tirgger end game scene...
        Game.Log("Gameplay: Retirement triggered");

        CutSceneController.Instance.PlayCutScene(GameplayControls.Instance.outro);
    }

    public void RetirementCancel()
    {
        PopupMessageController.Instance.OnRightButton -= RetirementConfirm;
        PopupMessageController.Instance.OnLeftButton -= RetirementCancel;
    }

    public void CheckPulse(Interactable what, NewNode where, Actor who)
    {
        Game.Log("Check pulse execution...");

        if(what != null)
        {
            if(what.isActor != null)
            {
                if(what.isActor.ai != null && !what.isActor.isDead)
                {
                    what.isActor.ai.koTime = SessionData.Instance.gameTime; //Set to awaken
                    Game.Log(what.isActor.name + " KO time SET: " + what.isActor.ai.koTime + " (" + SessionData.Instance.gameTime+")");
                }

                if(what.isActor.isDead)
                {
                    if(who != null)
                    {
                        who.speechController.TriggerBark(SpeechController.Bark.examineBody);
                    }
                }
            }
        }
    }

    public void TakeActiveCodebreaker(Interactable what, NewNode where, Actor who)
    {
        if (what != null)
        {
            Interactable newObj = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.codebreaker, Player.Instance, Player.Instance, null, Player.Instance.transform.position + new Vector3(0, 3.5f, 0), Vector3.zero, null, null);
            FirstPersonItemController.Instance.PickUpItem(newObj, false);

            what.SafeDelete(true); //Delete active
        }
    }

    public void TakeActiveDoorWedge(Interactable what, NewNode where, Actor who)
    {
        if (what != null)
        {
            Interactable newObj = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.doorWedge, Player.Instance, Player.Instance, null, Player.Instance.transform.position + new Vector3(0, 3.5f, 0), Vector3.zero, null, null);
            FirstPersonItemController.Instance.PickUpItem(newObj, false);

            //Set door to unjammed here...
            NewDoor d = null;

            if (CityData.Instance.doorDictionary.TryGetValue((int)what.val, out d))
            {
                d.SetJammed(false, what);
            }

            what.SafeDelete(true); //Delete active
        }
    }

    public void TakeActiveTracker(Interactable what, NewNode where, Actor who)
    {
        if (what != null)
        {
            Interactable newObj = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.tracker, Player.Instance, Player.Instance, null, Player.Instance.transform.position + new Vector3(0, 3.5f, 0), Vector3.zero, null, null);
            FirstPersonItemController.Instance.PickUpItem(newObj, false);

            what.SafeDelete(true); //Delete active
        }
    }

    public void TakeActiveFlashBomb(Interactable what, NewNode where, Actor who)
    {
        if (what != null)
        {
            Game.Log("Take active flash bomb");
            Interactable newObj = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.flashbomb, Player.Instance, Player.Instance, null, Player.Instance.transform.position + new Vector3(0, 3.5f, 0), Vector3.zero, null, null);
            FirstPersonItemController.Instance.PickUpItem(newObj, false);

            what.SafeDelete(true); //Delete active
        }
    }

    public void TakeActiveIncapacitator(Interactable what, NewNode where, Actor who)
    {
        if (what != null)
        {
            Interactable newObj = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.incapacitator, Player.Instance, Player.Instance, null, Player.Instance.transform.position + new Vector3(0, 3.5f, 0), Vector3.zero, null, null);
            FirstPersonItemController.Instance.PickUpItem(newObj, false);

            what.SafeDelete(true); //Delete active
        }
    }

    public void OpenBreaker(Interactable what, NewNode where, Actor who)
    {
        if (what != null)
        {
            Game.Log("Debug: Breaker open");
            what.SetValue(1f);
        }
    }

    public void CloseBreaker(Interactable what, NewNode where, Actor who)
    {
        if (what != null)
        {
            Game.Log("Debug: Breaker closed");
            what.SetValue(0f);
        }
    }

    public void ShootPoolBall(Interactable what, NewNode where, Actor who)
    {
        //Active physics and apply a forward force
        if(what != null && what.controller != null)
        {
            what.controller.DropThis(true);
        }
    }

    public void ResetPoolGame(Interactable what, NewNode where, Actor who)
    {
        if(what != null)
        {
            if (what.furnitureParent != null)
            {
                //Reset positions of all pool balls
                foreach(Interactable i in what.furnitureParent.spawnedInteractables)
                {
                    if(i.preset.physicsProfile != null)
                    {
                        if(i.preset.physicsProfile.name == "PoolBall")
                        {
                            Game.Log("Reset pool ball: " + i.preset.name);
                            i.originalPosition = false;
                            i.SetOriginalPosition(true, true);
                        }
                    }
                }
            }
        }
    }

    public void PutBack(Interactable what, NewNode where, Actor who)
    {

    }

    public void Release(Interactable what, NewNode where, Actor who)
    {
        //Release from handcuffs
        if(what != null && what.isActor != null && what.isActor.ai != null)
        {
            if(what.isActor.ai.restrained)
            {
                what.isActor.ai.SetRestrained(false, 0);

                for (int i = 0; i < what.isActor.inventory.Count; i++)
                {
                    Interactable inter = what.isActor.inventory[i];

                    if(inter.preset.name == "Handcuffs")
                    {
                        inter.SetAsNotInventory(Player.Instance.currentNode);

                        if(!FirstPersonItemController.Instance.PickUpItem(inter, false, false))
                        {
                            inter.MoveInteractable(Player.Instance.currentNode.position, new Vector3(0, -Player.Instance.transform.eulerAngles.y, 0), true); //Face objects towards player
                            inter.LoadInteractableToWorld(false, true);
                        }

                        break;
                    }
                }
            }
        }
    }

    public void TakeDetectiveStuff(Interactable what, NewNode where, Actor who)
    {
        Interactable newObj = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.printReader, Player.Instance, Player.Instance, null, what.wPos + new Vector3(0, 3.5f, 0), what.wEuler, null, null);

        //Execute purchase
        if (newObj != null)
        {
            Game.Log("Successfully created newObject " + newObj.name + " id " + newObj.id);

            if (FirstPersonItemController.Instance.PickUpItem(newObj, false, false))
            {
                newObj.MarkAsTrash(true); //Remove this once we can
            }
            else
            {
                Game.LogError("Unable to pickup item " + newObj.name);
                newObj.Delete();
            }
        }

        Interactable newObj2 = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.handcuffs, Player.Instance, Player.Instance, null, what.wPos + new Vector3(0, 3.5f, 0), what.wEuler, null, null);

        //Execute purchase
        if (newObj2 != null)
        {
            Game.Log("Successfully created newObject " + newObj2.name + " id " + newObj2.id);

            //Drop an item forcefully if needed...
            FirstPersonItemController.InventorySlot emptySlot = FirstPersonItemController.Instance.slots.Find(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic && item.GetInteractable() == null);

            if(emptySlot == null)
            {
                FirstPersonItemController.InventorySlot dropSlot = null;

                foreach(FirstPersonItemController.InventorySlot s in FirstPersonItemController.Instance.slots)
                {
                    if(s.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic)
                    {
                        if(dropSlot == null || (s.GetInteractable() != null && s.GetInteractable().val < dropSlot.GetInteractable().val))
                        {
                            dropSlot = s;
                        }
                    }
                }

                if(dropSlot != null)
                {
                    FirstPersonItemController.Instance.EmptySlot(dropSlot, false, false);
                }
            }

            if (FirstPersonItemController.Instance.PickUpItem(newObj2, false, false))
            {
                newObj2.MarkAsTrash(true); //Remove this once we can
            }
            else
            {
                Game.LogError("Unable to pickup item " + newObj2.name);
                newObj2.Delete();
            }
        }

        GameplayController.Instance.AddLockpicks(30, true);

        SessionData.Instance.TutorialTrigger("inventory");
        SessionData.Instance.TutorialTrigger("lockpicking");

        //Destroy the object
        what.Delete();
    }

    public void Mugging(Interactable what, NewNode where, Actor who)
    {
        if(what.isActor != null && who != null)
        {
            Human mugger = who as Human;
            Human victim = what.isActor as Human;

            if(mugger != null && victim != null)
            {
                Game.Log("MUGGING By " + who.name + " to " + what.name);

                //If player
                if(what.isActor.isPlayer)
                {
                    mugger.ai.TalkTo(InteractionController.ConversationType.mugging);

                    EvidenceWitness.DialogOption muggingDialog = mugger.evidenceEntry.dialogOptions[Evidence.DataKey.photo].Find(item => item.preset.name == "Mugging");
                    _InvokeDialog(mugger.interactable, where, victim, muggingDialog, muggingDialog.preset, DialogController.ForceSuccess.none);
                }
                else
                {
                    //Init convo...
                    DDSSaveClasses.DDSTreeSave convo = null;

                    if (Toolbox.Instance.allDDSTrees.TryGetValue("e07fe5d5-5195-437e-8776-2798cc53f6a5", out convo))
                    {
                        mugger.ExecuteConversationTree(convo, (new Human[] { victim }).ToList());
                    }
                }
            }
        }
    }

    public void DebtCollection(Interactable what, NewNode where, Actor who)
    {
        if (what.isActor != null && who != null)
        {
            Human mugger = who as Human;
            Human victim = what.isActor as Human;

            if (mugger != null && victim != null)
            {
                Game.Log("DEBT COLLECTION By " + who.name + " to " + what.name);

                //If player
                if (what.isActor.isPlayer)
                {
                    mugger.ai.TalkTo(InteractionController.ConversationType.loanSharkVisit);

                    EvidenceWitness.DialogOption muggingDialog = mugger.evidenceEntry.dialogOptions[Evidence.DataKey.photo].Find(item => item.preset.name == "LoanShark_AssociateVisit");
                    _InvokeDialog(mugger.interactable, where, victim, muggingDialog, muggingDialog.preset, DialogController.ForceSuccess.none);
                }
            }
        }
    }

    public void NextTrack(Interactable what, NewNode where, Actor who)
    {
        if (what != null)
        {
            what.MusicPlayerNextTrack(1);
        }
    }

    public void PreviousTrack(Interactable what, NewNode where, Actor who)
    {
        if (what != null)
        {
            what.MusicPlayerNextTrack(-1);
        }
    }

    public void CancelPutDownHomeInventoryItem(Interactable what, NewNode where, Actor who)
    {
        Game.Log("Decor: Cancel item placement");

        if (what != null)
        {
            what.SetPhysicsPickupState(false, null);
            PlayerApartmentController.Instance.MoveItemToStorage(what);
        }
    }

    public void RotatePhysicsLeft(Interactable what, NewNode where, Actor who)
    {
        if(InteractionController.Instance.carryingObject != null)
        {
            InteractionController.Instance.carryingObject.RotateHeldObject(-45);
        }
    }

    public void RotatePhysicsRight(Interactable what, NewNode where, Actor who)
    {
        if (InteractionController.Instance.carryingObject != null)
        {
            InteractionController.Instance.carryingObject.RotateHeldObject(45);
        }
    }
}
