using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class AnimationFrameReference : MonoBehaviour
{
    [System.Serializable]
    public class AnimationReference
    {
        public string name;
        public bool isArms = false;
        public CitizenAnimationController.IdleAnimationState idle;
        public CitizenAnimationController.ArmsBoolSate arms;
        public List<AnimationAnchorRef> anim;
    }

    [System.Serializable]
    public class AnimationAnchorRef
    {
        public CitizenOutfitController.CharacterAnchor anchor;
        public Vector3 localPos;
        public Quaternion localRot;
    }

    [Header("Database")]
    public List<AnimationReference> reference = new List<AnimationReference>();
    public List<AnimationReference> walkingReference = new List<AnimationReference>();
    public List<AnimationReference> runningReference = new List<AnimationReference>();

    [Header("Capture")]
    public CitizenOutfitController captureFrom;
    public CitizenAnimationController.IdleAnimationState captureIdle;
    public CitizenAnimationController.ArmsBoolSate captureArms;

    //Singleton pattern
    private static AnimationFrameReference _instance;
    public static AnimationFrameReference Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public AnimationReference GetAnimationReference(CitizenAnimationController.ArmsBoolSate arms, string seed)
    {
        List<AnimationReference> armsRet = reference.FindAll(item => item.isArms && item.arms == arms);
        if (armsRet.Count <= 0) return reference.Find(item => item.isArms && item.arms == CitizenAnimationController.ArmsBoolSate.none);
        return armsRet[Toolbox.Instance.GetPsuedoRandomNumber(0, armsRet.Count, seed)];
    }

    public AnimationReference GetAnimationReference(CitizenAnimationController.IdleAnimationState idle, string seed)
    {
        List<AnimationReference> idleRet = reference.FindAll(item => !item.isArms && item.idle == idle);
        if (idleRet.Count <= 0) return null;
        return idleRet[Toolbox.Instance.GetPsuedoRandomNumber(0, idleRet.Count, seed)];
    }

    [Button]
    public void CaptureState()
    {
        if (captureFrom == null) return;
#if UNITY_EDITOR

        AnimationReference newAnimRef = new AnimationReference();
        newAnimRef.anim = new List<AnimationAnchorRef>();

        newAnimRef.arms = captureArms;
        newAnimRef.idle = captureIdle;

        newAnimRef.isArms = false;
        newAnimRef.name = "Idle: " + captureIdle.ToString();

        if (captureIdle == CitizenAnimationController.IdleAnimationState.none && captureArms != CitizenAnimationController.ArmsBoolSate.none)
        {
            newAnimRef.isArms = true;
            newAnimRef.name = "Arms: " + captureArms.ToString();
        }

        foreach (CitizenOutfitController.AnchorConfig a in captureFrom.anchorConfig)
        {
            //Don't capture legs on arms movement
            if(newAnimRef.isArms)
            {
                if ((int)a.anchor >= 10 || (int)a.anchor <= 16) continue;
            }

            AnimationAnchorRef newRef = new AnimationAnchorRef();
            newRef.anchor = a.anchor;
            newRef.localPos = a.trans.localPosition;
            newRef.localRot = a.trans.localRotation;
            newAnimRef.anim.Add(newRef);
        }

        reference.Add(newAnimRef);
        UnityEditor.EditorUtility.SetDirty(this);
#endif
    }

    [Button]
    public void CaptureWalkingState()
    {
        if (captureFrom == null) return;
#if UNITY_EDITOR

        AnimationReference newAnimRef = new AnimationReference();
        newAnimRef.anim = new List<AnimationAnchorRef>();

        newAnimRef.arms = captureArms;
        newAnimRef.idle = captureIdle;

        newAnimRef.isArms = false;
        newAnimRef.name = "Walking: " + captureIdle.ToString();

        foreach (CitizenOutfitController.AnchorConfig a in captureFrom.anchorConfig)
        {
            AnimationAnchorRef newRef = new AnimationAnchorRef();
            newRef.anchor = a.anchor;
            newRef.localPos = a.trans.localPosition;
            newRef.localRot = a.trans.localRotation;
            newAnimRef.anim.Add(newRef);
        }

        walkingReference.Add(newAnimRef);
        UnityEditor.EditorUtility.SetDirty(this);
#endif
    }

    [Button]
    public void CaptureRunningState()
    {
        if (captureFrom == null) return;
#if UNITY_EDITOR

        AnimationReference newAnimRef = new AnimationReference();
        newAnimRef.anim = new List<AnimationAnchorRef>();

        newAnimRef.arms = captureArms;
        newAnimRef.idle = captureIdle;

        newAnimRef.isArms = false;
        newAnimRef.name = "Running: " + captureIdle.ToString();

        foreach (CitizenOutfitController.AnchorConfig a in captureFrom.anchorConfig)
        {
            AnimationAnchorRef newRef = new AnimationAnchorRef();
            newRef.anchor = a.anchor;
            newRef.localPos = a.trans.localPosition;
            newRef.localRot = a.trans.localRotation;
            newAnimRef.anim.Add(newRef);
        }

        runningReference.Add(newAnimRef);
        UnityEditor.EditorUtility.SetDirty(this);
#endif
    }
}
