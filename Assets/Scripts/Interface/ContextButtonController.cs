﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ContextButtonController : ButtonController
{
    public ContextMenuController cmc;
    public ContextMenuPanelController panelController;
    public ContextMenuController.ContextMenuButtonSetup setup;

    public void Setup(ContextMenuController newCmc, ContextMenuPanelController newPanel, ContextMenuController.ContextMenuButtonSetup newSetup)
    {
        cmc = newCmc;
        panelController = newPanel;
        setup = newSetup;

        SetupReferences();

        UpdateButtonText();

        if(setup.useColour)
        {
            icon.gameObject.SetActive(true);
            icon.color = setup.colour;
        }
    }

    public override void UpdateButtonText()
    {
        //Set text
        if (setup.useText && text != null)
        {
            if (setup.overrideText != null && setup.overrideText.Length > 0)
            {
                text.text = setup.overrideText;
            }
            else
            {
                text.text = Strings.Get("ui.context", this.setup.commandString);
            }
        }
        else if(text != null)
        {
            Destroy(text);
        }
    }

    public override void OnLeftClick()
    {
        cmc.OnCommand(this);
    }
}
