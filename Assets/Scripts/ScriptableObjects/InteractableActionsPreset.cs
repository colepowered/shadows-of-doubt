﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "actions_data", menuName = "Database/Interactable Actions Preset")]

public class InteractableActionsPreset : SoCustomComparison
{
    [Tooltip("Additional actions able to be performed")]
    [BoxGroup("Primary Actions")]
    public List<InteractablePreset.InteractionAction> actions = new List<InteractablePreset.InteractionAction>();

    [Space(7)]
    [BoxGroup("Locked-in Interaction 1")]
    [Tooltip("Disable the collider when locked-in")]
    public bool disableCollider = false;
    [BoxGroup("Locked-in Interaction 1")]
    public List<InteractablePreset.InteractionAction> lockedInActions1 = new List<InteractablePreset.InteractionAction>();

    [Space(7)]
    [BoxGroup("Locked-in Interaction 2")]
    public List<InteractablePreset.InteractionAction> lockedInActions2 = new List<InteractablePreset.InteractionAction>();

    [Space(7)]
    [BoxGroup("Physics Pick Up Actions")]
    public List<InteractablePreset.InteractionAction> physicsActions = new List<InteractablePreset.InteractionAction>();
}
