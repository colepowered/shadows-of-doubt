using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class ControllerViewRectScroll : MonoBehaviour
{
    public bool controlEnabled = false;
    public CustomScrollRect scrollRect;
    public float sensitivity = 10f;

    private void OnEnable()
    {
        InputController.Instance.OnInputModeChange += OnInputModeChange;
        OnInputModeChange();
    }

    private void OnDisable()
    {
        InputController.Instance.OnInputModeChange -= OnInputModeChange;
    }

    public void OnInputModeChange()
    {
        try
        {
            if (InputController.Instance.mouseInputMode)
            {
                if(this != null) this.enabled = false;
            }
            else
            {
                if (this != null) this.enabled = true;
            }
        }
        catch
        {

        }
    }

    private void Update()
    {
        if (InputController.Instance.mouseInputMode)
        {
            this.enabled = false;
        }
        else
        {
            if(controlEnabled)
            {
                Vector2 scroll = new Vector2(InputController.Instance.GetAxisRelative("ContentMoveAxisX") * sensitivity, InputController.Instance.GetAxisRelative("ContentMoveAxisY") * sensitivity);

                if (!scrollRect.horizontal) scroll.x = 0;
                if (!scrollRect.vertical) scroll.y = 0;

                scrollRect.content.anchoredPosition -= scroll;
            }
        }
    }
}
