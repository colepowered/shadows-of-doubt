﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "broadcast_data", menuName = "Database/Broadcast Preset")]

public class BroadcastPreset : SoCustomComparison
{
	[Header("Contents")]
	public AudioEvent audioEvent;
	[Tooltip("Change image every x seconds (real time)")]
	public float changeImageEvery = 2;
	[Tooltip("What order these images display in")]
	public ImageOrder order = ImageOrder.random;
	public enum ImageOrder { random, ordered};

	[Header("Atlas")]
	[ShowAssetPreview]
	public Texture2D spriteSheet;
	public Vector2 spriteResolution = new Vector2(32, 32);
	public int indexWidth = 5;
	public int indexHeight = 1;
	public int totalSpriteCount = 5;
}
