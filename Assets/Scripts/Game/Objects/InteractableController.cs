﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering.HighDefinition;
using NaughtyAttributes;
using System.Linq;

public class InteractableController : Controller
{
    public enum InteractableID { A, B, C, D, E, F, G, H, I, J, hidingPlace, none, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, AA, BB, CC, DD};
    [System.NonSerialized]
    public Interactable interactable; //Base non-monobehaviour class that can be interacted with without a physical object

    [Header("Editor Setup")]
    [Tooltip("In-editor, set the ID here. This will be used by the preset to identify pairing for interactables.")]
    public InteractableID id = InteractableID.A;

    [Header("Components")]
    public List<MeshRenderer> meshes = new List<MeshRenderer>();
    public LODGroup lod;
    public Rigidbody rb;
    public Collider coll;
    public Collider altColl;
    public List<Collider> additionalPhysicsOnlyColliders = new List<Collider>(); //To be disabled/reenabled on pick up and put down
    public Transform alternativePhysicsParent; //Parent rb and more using a different parent
    public WorldFlashController flash;
    public DoorMovementController doorMovement;
    public DoorMovementController secondaryDoorMovement;
    public DoorMovementController thirdDoorMovement;
    public LightController lightController;
    public SteamController steam;
    public ComputerController computer;
    public SecuritySystem securitySystem;
    public FileSystemController fileSystem;
    public DecalProjector decalProjector;
    public List<Transform> pages;
    public ParticleSystem partSystem;
    public Transform lockParentOverride;
    public Vector3 lockedInTransformOffset = Vector3.zero;

    [Space(5)]
    [Tooltip("Automatically sync these on/off depending on switch state.")]
    public bool enableSwitchSync = false;
    public List<SwitchSyncBehaviour> switchSyncObjects = new List<SwitchSyncBehaviour>();

    [Header("State")]
    [Tooltip("True if currently being carried by the player")]
    public bool isVisible = true;
    public bool isCarriedByPlayer = false;
    private float carryProgress = 0f; //Smooth lerp to player's hands
    private float rotProgress = 0f; //Smooth rotation lerp on rotation
    private Vector3 pickupPos; //Position recorded on pickup
    private Quaternion pickupRot; //Rotation recorded on pickup
    private Vector3 heldEuler; //Rotation that this is held by the player by 
    private bool setHeldEuler = false; //Used to set the above from the physics profile on the first time this is picked up by the player. We don't want to set it other times so it remembers the player's set position (for the game session at least!)
    [Tooltip("True if the physics are currently active")]
    public bool physicsOn = false;
    [Tooltip("For measuring time after physics movement")]
    public float minimumPhysicsTime = 0f; //Minimum physics idle time of 2 seconds
    //[Tooltip("The spawn point as positioned on creation")]
    //public Vector3 spawnPoint;
    //public Vector3 spawnRotation;
    //public Transform spawnParent;
    public bool damageEligable = false; //Only cause damage once per throw
    public bool wasTrigger = false; //Was the non-physics version of this a trigger collider?
    public Actor thrownBy = null;
    private float objectParticleCreationDelay = 0f;
    Vector3 colliderExtents;
    public bool apartmentPlacementIsValid = false;

    //[Tooltip("True if this object has been tampered with (over tamper threshold)")]
    //public bool isTampered = false;
    //private Quaternion pickUpRotation;

    [Header("Interactions")]
    [Tooltip("Look at when interacting (if null then use centre)")]
    public Transform lookAtTarget;
    [Tooltip("The interaction window for this object")]
    public InfoWindow interactionWindow;

    [Header("Special Cases")]
    [Tooltip("Use this flag for quickly checking if they player is looking at a door")]
    public NewDoor isDoor;
    public Actor isActor;
    public Human belongsTo;
    public bool isPhone = false;
    public GameObject phoneReciever;
    public float particleSystemDistance = 20;

    public bool willBeSavedWithCity = false;
    public bool willBeSavedWithState = false;
    public bool isMainLight = false;
    private bool broken = false;

    [Header("Debug")]
    public List<Interactable> debugInteractable = new List<Interactable>();

    [Tooltip("Angle of furniture parent")]
    public int debugAngle;
    public Vector3 debugFurnitureAnchorNodePos;
    [Tooltip("Local position of this, should match the transform")]
    public Vector3 debugLocalPos;
    [Tooltip("Local euler of this, should match the transform")]
    public Vector3 debugLocalEuler;
    public Vector3 debugWorldPos;
    public Vector3 debugInteractablePredictedWorldPos;
    [Tooltip("Interactable node")]
    public Vector3 debugNodeCoord;
    [Tooltip("The usage point")]
    public Interactable.UsagePoint debugUsagePoint;
    public Human debugOwnedBy;
    public Human debugWrittenBy;
    public Human debugReceivedBy;
    public object debugPasswordSource;
    public List<MonoBehaviour> debugFurnitureOwnedBy;
    public bool debugSwitchState = false;
    public bool debugState1 = false;
    public NewRoom debugRoom;
    public AirDuctGroup.AirVent debugVent = null;


    public void Setup(Interactable newInteractable)
    {
        //Pair to interactable
        interactable = newInteractable;
        interactable.controller = this;
        //name = interactable.name; //Copy name: Name is needed for subobject reference
        if(this.gameObject.layer == 0 && !interactable.preset.showWorldObjectInSceneCapture) this.gameObject.layer = 7; //Set to interactables layer

        if(Game.Instance.collectDebugData)
        {
            debugInteractable.Clear();
            debugInteractable.Add(interactable);
        }

        //Set tag
        this.gameObject.tag = "Interactable";
        if (altColl != null) altColl.gameObject.tag = "Interactable";

        //Get components
        //if (meshes == null || meshes.Count <= 0) SetupInteractable();
        if (rb == null) rb = this.gameObject.GetComponent<Rigidbody>();
        if (coll == null) coll = this.gameObject.GetComponent<Collider>();
        if (doorMovement != null) doorMovement.Setup(interactable); //Initialise door movement
        if (secondaryDoorMovement != null) secondaryDoorMovement.Setup(interactable); //Initialise door movement
        if (thirdDoorMovement != null) thirdDoorMovement.Setup(interactable); //Initialise door movement
        if (securitySystem != null) securitySystem.Setup(interactable); //Initialise door movement
        if (steam != null) steam.Setup(interactable.node.room); //Init steam
        if (computer != null) computer.Setup(this);
        if (fileSystem != null) fileSystem.Setup(this);

        //Particle systems
        if(partSystem != null)
        {
            SessionData.Instance.particleSystems.Add(this);
            UpdateParticleSystemDistance();
        }

        //Update switch sync
        UpdateSwitchSync();

        if (isPhone)
        {
            interactable.OnState1Change += State1Change;
        }

        SetPhysics(interactable.preset.forcePhysicsAlwaysOn); //Set physics off
        this.enabled = false;

        belongsTo = newInteractable.belongsTo;

        //Setup decal projector
        if(decalProjector != null && interactable.preset.isDecal)
        {
            ArtPreset foundArt = interactable.objectRef as ArtPreset;

            if(foundArt != null)
            {
                Material mat = foundArt.material;

                //Try to load dynamic text image
                if (foundArt.useDynamicText)
                {
                    if (!GameplayController.Instance.dynamicTextImages.TryGetValue(foundArt, out mat))
                    {
                        mat = foundArt.material;
                    }
                }

                decalProjector.material = mat;

                //Set sizes
                Texture tex = decalProjector.material.GetTexture("_BaseColorMap");
                decalProjector.size = new Vector3(tex.width * foundArt.pixelScaleMultiplier, tex.height * foundArt.pixelScaleMultiplier, 0.14f);
            }
            //Otherwise look for text to generate
            else
            {
                Interactable.Passed dynamic = interactable.pv.Find(item => item.varType == Interactable.PassedVarType.decalDynamicText);

                if(dynamic != null)
                {
                    Material cached = null;

                    if (GameplayController.Instance.graffitiCache.TryGetValue(dynamic.str, out cached))
                    {
                        decalProjector.material = cached;
                    }
                    else
                    {
                        //Parse data
                        //TEXT | FONT | SIZE | COLOUR
                        string[] parsed = dynamic.str.Split('|');

                        if (parsed.Length >= 4)
                        {
                            Game.Log("Parsing custom graffiti: " + dynamic.str);

                            Material matInstance = Instantiate(decalProjector.material);

                            //Capture text to img
                            TextToImageController.TextToImageSettings newSettings = new TextToImageController.TextToImageSettings();
                            newSettings.textString = parsed[0];

                            newSettings.font = DDSControls.Instance.fonts.Find(item => item.name == parsed[1]);
                            if (newSettings.font == null) Game.Log("Unable to find font " + parsed[1]);

                            if (!float.TryParse(parsed[2], out newSettings.textSize)) Game.Log("Unable to parse float " + parsed[2]);
                            if (!ColorUtility.TryParseHtmlString("#" + parsed[3], out newSettings.color)) Game.Log("Unable to parse colour " + parsed[3]);

                            newSettings.useAlpha = true;
                            newSettings.enableProcessing = true;

                            Texture2D writing = TextToImageController.Instance.CaptureTextToImage(newSettings);
                            matInstance.SetTexture("_BaseColorMap", writing);

                            decalProjector.material = matInstance;
                            decalProjector.size = new Vector3(writing.width * 0.02f, writing.height * 0.02f, 0.14f);

                            GameplayController.Instance.AddToGraffitiCache(dynamic.str, matInstance);
                        }
                    }
                }
            }
        }

        //Pages listen for changes
        if (pages != null && pages.Count > 0)
        {
            EvidenceMultiPage mp = interactable.evidence as EvidenceMultiPage;

            if(mp != null)
            {
                mp.OnPageChanged += OnPageChange;

                //Set pages according to evidence
                for (int i = 0; i < pages.Count; i++)
                {
                    Transform thisPage = pages[i];
                    DoorMovementController movement = thisPage.gameObject.GetComponent<DoorMovementController>();

                    if (movement != null)
                    {
                        if (!movement.isSetup) movement.Setup(interactable, false);

                        if (i < mp.page && !movement.isOpen) movement.SetOpen(1f, null, true);
                        else if (i >= mp.page && movement.isOpen) movement.SetOpen(0f, null, true);
                    }
                }
            }
        }

        //Force colliders as triggers
        if(coll != null)
        {
            colliderExtents = coll.bounds.extents;

            if (interactable.ft)
            {
                coll.isTrigger = true;
            }
        }

        if(altColl != null)
        {
            if (coll == null) colliderExtents = altColl.bounds.extents;

            if (interactable.ft)
            {
                altColl.isTrigger = true;
            }
        }

        //Set furniture angle debug
        if (Game.Instance.collectDebugData)
        {
            if (interactable.furnitureParent != null)
            {
                debugAngle = interactable.furnitureParent.angle;
                debugFurnitureAnchorNodePos = interactable.furnitureParent.anchorNode.position;
            }

            debugLocalEuler = interactable.lEuler;
            debugLocalPos = interactable.lPos;
            debugWorldPos = this.transform.position;
            debugInteractablePredictedWorldPos = interactable.wPos;

            if (interactable.node != null)
            {
                debugNodeCoord = interactable.node.nodeCoord;
                debugRoom = interactable.node.room;
            }
            else debugNodeCoord = Vector3.zero;

            debugUsagePoint = interactable.usagePoint;
            debugOwnedBy = interactable.belongsTo;
            debugWrittenBy = interactable.writer;
            debugReceivedBy = interactable.reciever;
            debugSwitchState = interactable.sw0;
            debugState1 = interactable.sw1;
            debugPasswordSource = interactable.passwordSource;

            if (interactable.furnitureParent != null)
            {
                debugFurnitureOwnedBy = interactable.furnitureParent.debugOwners;
            }
        }
    }

    public void UpdateSwitchSync()
    {
        if(enableSwitchSync)
        {
            foreach(SwitchSyncBehaviour g in switchSyncObjects)
            {
                if (g == null) continue;

                if (g.onlySyncWhenParentIsOn && !interactable.sw0) continue;

                if(g.syncWithState == InteractablePreset.Switch.switchState)
                {
                    g.SetOn(interactable.sw0);
                }
                else if(g.syncWithState == InteractablePreset.Switch.custom1)
                {
                    g.SetOn(interactable.sw1);
                }
                else if(g.syncWithState == InteractablePreset.Switch.custom2)
                {
                    g.SetOn(interactable.sw2);
                }
                else if(g.syncWithState == InteractablePreset.Switch.custom3)
                {
                    g.SetOn(interactable.sw3);
                }
                else if(g.syncWithState == InteractablePreset.Switch.lockState)
                {
                    g.SetOn(interactable.locked);
                }
                else if(g.syncWithState == InteractablePreset.Switch.securityGrid)
                {
                    g.SetOn(interactable.sw0 && (interactable.sw1 || interactable.sw2)); //Set on if power enabled & alarm active or company is closed
                }
                else if (g.syncWithState == InteractablePreset.Switch.carryPhysicsObject)
                {
                    g.SetOn(interactable.phy);
                }
            }
        }
    }

    public void OnPageChange(int newPage)
    {
        if (fileSystem != null) fileSystem.SetPage(newPage);

        for (int i = 0; i < pages.Count; i++)
        {
            Transform thisPage = pages[i];

            if (thisPage != null)
            {
                DoorMovementController movement = thisPage.gameObject.GetComponent<DoorMovementController>();

                if (movement != null)
                {
                    if (!movement.isSetup) movement.Setup(interactable, false);

                    if (i < newPage && !movement.isOpen) movement.SetOpen(1f, null);
                    else if (i >= newPage && movement.isOpen) movement.SetOpen(0f, null);
                }
            }
            else Game.Log("Calendar page transform missing at index " + i);
        }
    }

    private void OnDestroy()
    {
        if (pages != null && interactable != null && pages.Count > 0)
        {
            (interactable.evidence as EvidenceMultiPage).OnPageChanged -= OnPageChange;
        }

        if (isPhone && interactable != null)
        {
            interactable.OnState1Change -= State1Change;
        }
    }

    //Return a screen space box for this object
    public void GetScreenBox(out Vector2 uiMin, out Vector2 uiMax)
    {
        uiMax = Vector2.zero;
        uiMin = Vector2.zero;

        if (coll == null) return;

        //Are we looking at the primary or secondary collider?
        Collider col = coll;

        if(altColl != null && InteractionController.Instance.playerCurrentRaycastHit.collider == altColl)
        {
            col = altColl;
        }

        //Get the 8 points of the bounds
        Vector3 center = col.bounds.center;

        List<Vector3> extents = new List<Vector3>();

        Vector3 extentMin = center - col.bounds.extents;
        Vector3 extentMax = center + col.bounds.extents;

        extents.Add(extentMin);
        extents.Add(extentMax);

        extents.Add(new Vector3(extentMin.x, extentMin.y, extentMax.z));
        extents.Add(new Vector3(extentMin.x, extentMax.y, extentMin.z));
        extents.Add(new Vector3(extentMax.x, extentMin.y, extentMin.z));
        extents.Add(new Vector3(extentMin.x, extentMax.y, extentMax.z));
        extents.Add(new Vector3(extentMax.x, extentMin.y, extentMax.z));
        extents.Add(new Vector3(extentMax.x, extentMax.y, extentMin.z));

        //Now get the min/max screen points. These are from bottom left (min) to top right (max)
        Vector2 min = new Vector2(9999999, 9999999);
        Vector2 max = new Vector2(-9999999, -9999999);

        for (int i = 0; i < extents.Count; i++)
        {
            Vector3 screenPoint = CameraController.Instance.cam.WorldToScreenPoint(extents[i]);

            min = Vector2.Min(min, screenPoint);
            max = Vector2.Max(max, screenPoint);
        }

        //Game.Log("Screen space: " + min + max);

        //You should now have the min and max coordinates in screen space, convert them locally to the first person ui object
        RectTransformUtility.ScreenPointToLocalPointInRectangle(InterfaceController.Instance.firstPersonUI, min, null, out uiMin);
        RectTransformUtility.ScreenPointToLocalPointInRectangle(InterfaceController.Instance.firstPersonUI, max, null, out uiMax);

        //Game.Log("Rect pos " + uiMin + uiMax);
    }

    //Triggered when this has left interaction mode
    public void OnExitInteractionMode()
    {

    }

    public void MovablePickUpThis()
    {
        //If already carrying an object, drop it
        if (InteractionController.Instance.carryingObject != null) InteractionController.Instance.carryingObject.DropThis(false);

        Game.Log("Player: Pick up " + this.name);

        if (meshes.Count <= 0) return;

        damageEligable = false;

        //Set to carried
        InteractionController.Instance.carryingObject = this;
        isCarriedByPlayer = true;

        //Enable physics
        SetPhysics(false);

        //Disable interaction cursor
        InteractionController.Instance.DisplayInteractionCursor(false, true);

        //Cancel kettle heating
        if(interactable.preset.specialCaseFlag == InteractablePreset.SpecialCase.stovetopKettle)
        {
            interactable.SetSwitchState(false, null);
        }

        //Reset idle time
        minimumPhysicsTime = 0f;

        pickupPos = this.transform.position;
        pickupRot = this.transform.rotation;
        carryProgress = 0f;
        rotProgress = 0f;

        ////Set constraints
        //if (interactable.preset.constrainOnPickup)
        //{
        //    rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        //}

        //Ignore collisions with player when picked up
        //Physics.IgnoreCollision(coll, Player.Instance.charController);

        foreach (Collider c in additionalPhysicsOnlyColliders)
        {
            Physics.IgnoreCollision(c, Player.Instance.charController, true);
        }

        //Set pick-up rotation
        //pickUpRotation = Toolbox.Instance.TransformRotation(this.transform.rotation, Player.Instance.transform);

        //Change player skin width
        Player.Instance.UpdateSkinWidth();

        coll.enabled = false;
        if (altColl != null) altColl.enabled = false;
        this.enabled = true;

        //Force update shadows in the room
        //Player.Instance.currentRoom.UpdateShadowMaps();

        //Set this to be moved by player
        interactable.SetOriginalPosition(false);
    }

    public void RotateHeldObject(float val)
    {
        heldEuler.y += val;
        heldEuler = new Vector3(Mathf.RoundToInt(heldEuler.x), Mathf.RoundToInt(heldEuler.y), Mathf.RoundToInt(heldEuler.z)); //Attempt to keep things nice and round!
        pickupRot = this.transform.rotation;
        rotProgress = 0f;
        Game.Log("Player: Rotate held object: " + heldEuler);
    }

    private void Update()
    {
        if (objectParticleCreationDelay > 0f)
        {
            objectParticleCreationDelay -= Time.deltaTime;
        }

        if (isCarriedByPlayer)
        {
            //Disable gravity
            if(rb != null)
            {
                rb.useGravity = false;
            }

            if(carryProgress < 1f)
            {
                carryProgress += Time.deltaTime * 9f;
                carryProgress = Mathf.Clamp01(carryProgress);
            }

            if (rotProgress < 1f)
            {
                rotProgress += Time.deltaTime * 8f;
                rotProgress = Mathf.Clamp01(rotProgress);
            }

            //Kick AI off usage point
            if (interactable.usagePoint != null)
            {
                Human existing = null;
                interactable.usagePoint.TryGetUserAtSlot(Interactable.UsePointSlot.defaultSlot, out existing);

                if(existing != null)
                {
                    if (existing.ai != null)
                    {
                        if (existing.ai.currentAction != null)
                        {
                            //Bark: Fall off chair
                            existing.speechController.TriggerBark(SpeechController.Bark.fallOffChair);

                            //Lower torso position
                            Vector3 lowerTorso = existing.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.lowerTorso).position;

                            //Raycast to AI in order to get damage position
                            Vector3 camDir = lowerTorso - CameraController.Instance.cam.transform.position;

                            //Force ragdoll
                            existing.RecieveDamage(0.01f, Player.Instance, lowerTorso, camDir, CriminalControls.Instance.punchSpatter, null, SpatterSimulation.EraseMode.useDespawnTime, false, true, Toolbox.Instance.Rand(0f, 0.05f));

                            //End current action
                            existing.ai.currentAction.Remove(existing.ai.currentAction.preset.repeatDelayOnActionFail);
                        }
                    }
                }
            }

            PhysicsProfile phy = interactable.preset.GetPhysicsProfile();

            if (phy != null)
            {
                //Used to set the above from the physics profile on the first time this is picked up by the player. We don't want to set it other times so it remembers the player's set position (for the game session at least!)
                if (!setHeldEuler)
                {
                    heldEuler = phy.heldEuler;
                    setHeldEuler = true;
                }
            }

            //Find the point on the bounds facing the player
            float extraDistance = 0.25f; //Extra distance which is equal to the distance between the bounds centre and the closest point facing the player
            Vector3 closest = this.transform.position;
            if (alternativePhysicsParent != null) closest = alternativePhysicsParent.transform.position;

            //This bit needs a mesh reference to work; cannot use collider as it is disabled when picked up...
            if (meshes.Count > 0)
            {
                closest = meshes[0].bounds.ClosestPoint(Player.Instance.cam.transform.position);
                extraDistance = Vector3.Distance(meshes[0].bounds.center, closest);
            }

            //Position infront of camera
            Vector3 forwardPos = Player.Instance.cam.transform.TransformPoint(Vector3.forward * (GameplayControls.Instance.carryDistance + extraDistance));

            //Calculate carry height by converting the bounding centre to transform position
            Vector3 carryPoint = ConvertBoundsPositionToTransformPosition(forwardPos);

            //Game.Log("Forward: " + forwardPos + ", closest: " + closest + " bounds center: " + meshes[0].bounds.center + " extra: " + extraDistance + " carry point: " + carryPoint);

            //Now cast a ray out infront to stop the held object going through walls and other obstacles
            //Forward direction
            Vector3 forwardDirection = forwardPos - Player.Instance.cam.transform.position;

            //Cast a ray infront of the player, equal to the distance infront + 0.5 of bounds
            Ray holdingRay = new Ray(Player.Instance.cam.transform.position, forwardDirection);

            //Get raycast distance- ensure this is slightly longer than from the camera to the other end of the object...
            float rayDist = GameplayControls.Instance.carryDistance + (extraDistance * 2f);

            RaycastHit hit;
            Quaternion quat = Quaternion.identity;
            bool detectedSnapSurface = false;
            apartmentPlacementIsValid = true;

            if(interactable.preset.apartmentPlacementMode != InteractablePreset.ApartmentPlacementMode.physics)
            {
                apartmentPlacementIsValid = false;
                rayDist += 2.5f;
            }

            if(Physics.Raycast(holdingRay, out hit, rayDist, Toolbox.Instance.heldObjectsObjectsLayerMask))
            {
                //Use the following for apartment placement mode
                if(interactable.preset.apartmentPlacementMode == InteractablePreset.ApartmentPlacementMode.vertical)
                {
                    //Detect a face-sideways surface
                    if (hit.normal.y == 0)
                    {
                        forwardPos = hit.point + (hit.normal * colliderExtents.z);
                        carryPoint = ConvertBoundsPositionToTransformPosition(forwardPos);
                        quat = Quaternion.LookRotation(hit.normal);
                        detectedSnapSurface = true;
                        apartmentPlacementIsValid = true;
                    }
                }
                else if(interactable.preset.apartmentPlacementMode == InteractablePreset.ApartmentPlacementMode.ceiling)
                {
                    //Game.Log(forwardPos + ": " + hit.point + " + (" + hit.normal + " * " + colliderExtents.z + ")");
                    //Detect a down facing surface
                    if (hit.normal.y == -1)
                    {
                        forwardPos = hit.point + (hit.normal * colliderExtents.z);
                        carryPoint = ConvertBoundsPositionToTransformPosition(forwardPos);
                        quat = Quaternion.identity;
                        detectedSnapSurface = true;
                        apartmentPlacementIsValid = true;
                    }
                }

                //Check certain objects are touching for apartment placement
                if(apartmentPlacementIsValid && interactable.preset.apartmentPlacementMode != InteractablePreset.ApartmentPlacementMode.physics && interactable.preset.mustTouchFurniture.Count > 0)
                {
                    MeshFilter mf = hit.transform.gameObject.GetComponent<MeshFilter>();

                    if (mf != null)
                    {
                        FurniturePreset f = Toolbox.Instance.GetFurnitureFromMesh(mf.sharedMesh);

                        if (f != null)
                        {
                            if (interactable.preset.mustTouchFurniture.Contains(f))
                            {
                                apartmentPlacementIsValid = true;
                            }
                            else
                            {
                                apartmentPlacementIsValid = false;
                            }
                        }
                        else
                        {
                            apartmentPlacementIsValid = false;
                        }
                    }
                    else
                    {
                        apartmentPlacementIsValid = false;
                    }
                }

                if(!detectedSnapSurface)
                {
                    float minusDistance = rayDist - hit.distance;
                    //Game.Log("Hit minus distance: " + minusDistance);

                    //Clamp this with a max to stop it getting too close to the camera
                    forwardPos = Player.Instance.cam.transform.TransformPoint(Vector3.forward * Mathf.Max((GameplayControls.Instance.carryDistance + extraDistance - minusDistance), Mathf.Max(GameplayControls.Instance.carryDistance, extraDistance + 0.1f)));
                    carryPoint = ConvertBoundsPositionToTransformPosition(forwardPos);
                }
            }

            if(interactable.preset.apartmentPlacementMode != InteractablePreset.ApartmentPlacementMode.physics)
            {
                //FirstPersonItemController.Instance.UpdateCurrentActions();
                interactable.UpdateCurrentActions();
            }

            if(!detectedSnapSurface)
            {
                //Set to rotation
                quat = Player.Instance.transform.rotation * Quaternion.Euler(heldEuler);
            }

            //Make sure Y pos is a at minimum the player pos Y
            carryPoint = new Vector3(carryPoint.x, Mathf.Max(carryPoint.y, Player.Instance.transform.position.y - (Player.Instance.charController.height * 0.5f) - 0.2f), carryPoint.z);

            if (alternativePhysicsParent != null)
            {
                alternativePhysicsParent.transform.SetPositionAndRotation(Vector3.Lerp(pickupPos, carryPoint, carryProgress), Quaternion.Slerp(pickupRot, quat, rotProgress)); //Set pos & rot
            }
            else this.transform.SetPositionAndRotation(Vector3.Lerp(pickupPos, carryPoint, carryProgress), Quaternion.Slerp(pickupRot, quat, rotProgress)); //Set pos & rot
        }
        else if(physicsOn)
        {
            //Idle detectted
            if (SessionData.Instance.play && rb != null && (Mathf.Abs(rb.velocity.x) + Mathf.Abs(rb.velocity.y) + Mathf.Abs(rb.velocity.z) < 0.1f) && !interactable.preset.forcePhysicsAlwaysOn)
            {
                minimumPhysicsTime += Time.deltaTime;

                //If 2+ seconds idle then remove physics
                if (minimumPhysicsTime >= GameplayControls.Instance.physicsOffTime)
                {
                    //Game.Log("Removing physics from object " + name);

                    //Remove physics
                    SetPhysics(false);
                    this.enabled = false;
                    return;
                }
            }
            else
            {
                minimumPhysicsTime = 0f; //Else then reset
            }

            if (interactable.preset.particleProfile != null && rb != null && objectParticleCreationDelay <= 0f && rb.velocity.magnitude > 0.1f)
            {
                if (interactable.preset.particleProfile.spatterTrigger == ParticleEffect.SpatterTrigger.whileInAirOrAnyImpact)
                {
                    Spatter(this.transform.position);
                }

                if (interactable.preset.particleProfile.creationTrigger == ParticleEffect.SpatterTrigger.whileInAirOrAnyImpact)
                {
                    ParticleObjectCreation();
                }
            }
        }

        UpdateLastMovement();
    }

    //Input the position of where the bounds center should be location and return the transform position value (world)
    private Vector3 ConvertBoundsPositionToTransformPosition(Vector3 boundsPosition)
    {
        Vector3 ret = boundsPosition;

        if (meshes.Count > 0)
        {
            //Calculate carry distance based on physical bounds...
            Vector3 localBoundsCentre = this.transform.InverseTransformPoint(meshes[0].bounds.center); //The bounds centre in local space

            ret = boundsPosition - localBoundsCentre;
        }

        return ret;
    }

    public void DropThis(bool throwThis)
    {
        carryProgress = 0f;
        rotProgress = 0f;

        //Set this to be moved by player
        interactable.SetOriginalPosition(false);

        if(isCarriedByPlayer)
        {
            InteractionController.Instance.SetLockedInInteractionMode(null, dropCarriedCheck: false);
            //interactable.SetSwitchState(false, Player.Instance); //Not neededed???
            if(interactable.phy) interactable.SetPhysicsPickupState(false, Player.Instance); //Not doing this will result in not being able to pick stuff up again!

            InteractionController.Instance.carryingObject = null;
            isCarriedByPlayer = false;
        }

        //Enable physics
        if(!physicsOn)
        {
            if(interactable.preset.apartmentPlacementMode == InteractablePreset.ApartmentPlacementMode.physics)
            {
                SetPhysics(true, Player.Instance);
            }
        }

        //Don't ignore collisions with player when picked up
        //Physics.IgnoreCollision(coll, Player.Instance.charController, false);
        //if(altColl != null) Physics.IgnoreCollision(altColl, Player.Instance.charController, false);

        foreach (Collider c in additionalPhysicsOnlyColliders)
        {
            Physics.IgnoreCollision(c, Player.Instance.charController, false);
        }

        //Enable gravity
        if(rb != null) rb.useGravity = true;
        coll.enabled = true;
        if (altColl != null) altColl.enabled = true;

        ////Reset constraints
        //if (interactable.preset.constrainOnPickup)
        //{
        //    rb.constraints = RigidbodyConstraints.None;
        //}

        //Apply forward force to object
        if (throwThis && rb != null)
        {
            damageEligable = true; //Make this able to hurt someone
            float throwForceMP = 1f;

            PhysicsProfile phy = interactable.preset.GetPhysicsProfile();
            throwForceMP = phy.throwForceMultiplier;

            Vector3 force = Player.Instance.cam.transform.forward * (GameplayControls.Instance.throwForce * throwForceMP) * (1f + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.throwPowerModifier));
            rb.AddForce(force, ForceMode.VelocityChange);
            Game.Log(name + " add force of " + force);
        }

        //Reset idle time
        minimumPhysicsTime = 0f;

        //Change player skin width
        Player.Instance.UpdateSkinWidth();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (rb == null) return;
        float damageMP = 1f;

        PhysicsProfile phy = interactable.preset.GetPhysicsProfile();

        damageMP = phy.throwDamageMultiplier;
        float damage = ((collision.relativeVelocity.magnitude * rb.mass) * 0.015f) * damageMP;

        //So small an impact; don't bother registering...
        if (damage < 0.002f)
        {
            return;
        }

        bool playSound = true;
        float soundVolMP = Mathf.Lerp(0.1f, 1f, damage * 10f);

        //Damage force is velocity * mass
        Game.Log("Object: " + name + " thrown by " + thrownBy + " collides with " + collision.collider.name + " with damage: " + damage);

        //Who threw this?
        Actor soundMaker = thrownBy;
        if (phy.treatAsCausedByPlayer && soundMaker == null) soundMaker = Player.Instance;

        //Trigger messing around bark
        if(!damageEligable && soundMaker == Player.Instance)
        {
            //Execute confront bark
            foreach (Actor a in interactable.node.room.currentOccupants)
            {
                if (a == soundMaker) continue;
                if (a.ai == null) continue;
                if (a.speechController == null) continue;
                if (a.isDead || a.isStunned || a.isAsleep) continue;

                if (a.locationsOfAuthority.Contains(interactable.node.gameLocation))
                {
                    if (a.ai.trackedTargets.Exists(item => item.actor == soundMaker))
                    {
                        if(a.speechController.speechQueue.Count <= 0)
                        {
                            a.speechController.TriggerBark(SpeechController.Bark.confrontMessingAround);
                            break;
                        }
                    }
                }
            }
        }

        //Smash window?
        BreakableWindowController bwc = collision.collider.gameObject.GetComponent<BreakableWindowController>();

        if(bwc != null && !bwc.isBroken)
        {
            bwc.InteractableCollision(collision, damage, soundMaker);

            //Apply continuing velocity to cancel out this impact...
            if(bwc.isBroken)
            {
                rb.AddForce(collision.relativeVelocity * -0.66f, ForceMode.VelocityChange);
                playSound = false;
            }
        }

        if (damageEligable)
        {
            //Hit citizen?
            Human hitCit = collision.collider.gameObject.GetComponentInParent<Human>();

            if (hitCit != null && hitCit != thrownBy)
            {
                //Does this cause a knock down? If damage is more than 10% of their max health then yes
                if (damage >= hitCit.maximumHealth * 0.1f && !hitCit.isPlayer)
                {
                    hitCit.RecieveDamage(damage, thrownBy, collision.GetContact(0).point, rb.velocity, CriminalControls.Instance.punchSpatter, null, SpatterSimulation.EraseMode.useDespawnTime, true, true, Toolbox.Instance.Rand(0.06f, 0.11f));
                }
                else
                {
                    if(hitCit.isPlayer)
                    {
                        damage *= GameplayControls.Instance.incomingPlayerPhysicsDamageMultiplier;
                    }

                    hitCit.RecieveDamage(damage, thrownBy, collision.GetContact(0).point, rb.velocity, CriminalControls.Instance.punchSpatter, null, SpatterSimulation.EraseMode.useDespawnTime, false);
                }

                //Manual trigger for throwing food in someone's face...
                foreach(Case c in CasePanelController.Instance.activeCases)
                {
                    List<Case.ResolveQuestion> throwingFoodQ = c.resolveQuestions.FindAll(item => item.revengeObjTarget == hitCit.humanID && item.revengeObjective == "PurpThrowFood");

                    foreach(Case.ResolveQuestion q in throwingFoodQ)
                    {
                        Game.Log("Jobs: Manually complete throw food objective");
                        q.completedRevenge = true; //Manually trigger completion here
                        q.UpdateValid(c);

                        //Trigger complete if we know what they look like...
                        List<Evidence.DataKey> keys = hitCit.evidenceEntry.GetTiedKeys(Evidence.DataKey.name);

                        if(keys.Contains(Evidence.DataKey.photo))
                        {
                            q.SetProgress(1f);
                        }

                        c.ValidationCheck();
                    }
                }
            }

            damageEligable = false;
        }

        //Get surface data
        AudioController.SoundMaterialOverride detailedMaterialData = null;

        //Play impact sound
        if (interactable != null && phy.physicsCollisionAudio != null)
        {
            //Look for override controllers first...
            MaterialOverrideController moc = collision.transform.GetComponent<MaterialOverrideController>();

            if (moc != null)
            {
                detailedMaterialData = new AudioController.SoundMaterialOverride(moc.concrete, moc.wood, moc.carpet, moc.tile, moc.plaster, moc.fabric, moc.metal, moc.glass);
            }
            else
            {
                MeshFilter mf = collision.transform.GetComponent<MeshFilter>();

                if (mf != null)
                {
                    FurniturePreset f = Toolbox.Instance.GetFurnitureFromMesh(mf.sharedMesh);

                    if (f != null)
                    {
                        detailedMaterialData = new AudioController.SoundMaterialOverride(f.concrete, f.wood, f.carpet, f.tile, f.plaster, f.fabric, f.metal, f.glass);
                    }
                }
            }

            if (detailedMaterialData == null && interactable.node != null)
            {
                detailedMaterialData = new AudioController.SoundMaterialOverride(interactable.node.room.floorMaterial.concrete, interactable.node.room.floorMaterial.wood, interactable.node.room.floorMaterial.carpet, interactable.node.room.floorMaterial.tile, interactable.node.room.floorMaterial.plaster, interactable.node.room.floorMaterial.fabric, interactable.node.room.floorMaterial.metal, interactable.node.room.floorMaterial.glass);
            }

            if(playSound)
            {
                AudioController.Instance.PlayWorldOneShot(phy.physicsCollisionAudio, soundMaker, interactable.node, this.transform.position, interactable, surfaceData: detailedMaterialData, volumeOverride: soundVolMP);
            }
        }

        //Street cleaner
        int streetCleaningMoney = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.streetCleaningMoney));

        if(streetCleaningMoney > 0)
        {
            if(collision.collider.gameObject.CompareTag("Garbage"))
            {
                Game.Log("Garbage collision");

                if(CleanupController.Instance.trash.Contains(interactable))
                {
                    interactable.SafeDelete();
                    GameplayController.Instance.AddMoney(streetCleaningMoney, true, "readingforstreetcleaning");
                }
                else
                {
                    Game.Log("This is not trash");
                }
            }
        }

        if (interactable.preset.particleProfile != null)
        {
            //Play sounds
            foreach (AudioEvent a in interactable.preset.particleProfile.impactEvents)
            {
                AudioController.Instance.PlayWorldOneShot(a, soundMaker, interactable.node, this.transform.position, interactable, surfaceData: detailedMaterialData);
            }

            if (interactable.preset.particleProfile.spatterTrigger == ParticleEffect.SpatterTrigger.onAnyImpact || interactable.preset.particleProfile.spatterTrigger == ParticleEffect.SpatterTrigger.whileInAirOrAnyImpact)
            {
                Spatter(collision.GetContact(0).point);
            }

            if (interactable.preset.particleProfile.creationTrigger == ParticleEffect.SpatterTrigger.onAnyImpact || interactable.preset.particleProfile.creationTrigger == ParticleEffect.SpatterTrigger.whileInAirOrAnyImpact)
            {
                ParticleObjectCreation();
            }
        }

        //Breakage
        if (interactable.preset.breakable && interactable.preset.particleProfile != null && interactable.preset.particleProfile.deleteObject && !broken)
        {
            //Game.Log("Damage from impact: " + damage);

            if(damage >= interactable.preset.particleProfile.damageBreakPoint)
            {
                //Play sounds
                foreach (AudioEvent a in interactable.preset.particleProfile.breakEvents)
                {
                    AudioController.Instance.PlayWorldOneShot(a, soundMaker, interactable.node, this.transform.position, interactable, surfaceData: detailedMaterialData);
                }

                ContactPoint cp = collision.GetContact(0);
                BreakObject(cp.point, cp.normal, collision.relativeVelocity.magnitude, soundMaker);
            }
        }
    }

    public void BreakObject(Vector3 contactPoint, Vector3 normal, float magnitude, Actor breaker)
    {
        if(interactable.preset.particleProfile.effectPrefab != null)
        {
            GameObject newParticle = Instantiate(interactable.preset.particleProfile.effectPrefab, PrefabControls.Instance.mapContainer);

            //Pull position data from contact point
            newParticle.transform.position = contactPoint;
            newParticle.transform.rotation = Quaternion.FromToRotation(Vector3.up, normal);
        }

        broken = true;

        if(interactable.preset.particleProfile.shatter) Shatter(contactPoint, magnitude);

        if(interactable.preset.particleProfile != null)
        {
            if(interactable.preset.particleProfile.spatterTrigger == ParticleEffect.SpatterTrigger.onBreak)
            {
                Spatter(contactPoint);
            }

            if (interactable.preset.particleProfile.creationTrigger == ParticleEffect.SpatterTrigger.onBreak)
            {
                ParticleObjectCreation();
            }
        }

        //Add vandal fine
        if (breaker != null && breaker.isPlayer && interactable.spawnNode != null && interactable.spawnNode.gameLocation != null && interactable.spawnNode.gameLocation.thisAsAddress != null && InteractionController.Instance.GetValidPlayerActionIllegal(interactable, interactable.spawnNode, false))
        {
            //Add peril if worth more than 1
            if (interactable.val > 1)
            {
                interactable.spawnNode.gameLocation.thisAsAddress.AddVandalism(interactable);
                StatusController.Instance.AddFineRecord(interactable.spawnNode.gameLocation.thisAsAddress, interactable, StatusController.CrimeType.vandalism, true, forcedPenalty: Mathf.RoundToInt(interactable.val * GameplayControls.Instance.vandalismFineMultiplier));
                InteractionController.Instance.SetIllegalActionActive(true);
            }
        }

        interactable.MarkAsTrash(true);
        interactable.RemoveFromPlacement();
    }

    public void Spatter(Vector3 target)
    {
        if (interactable.preset.particleProfile.spatter != null)
        {
            SpatterPatternPreset spat = interactable.preset.particleProfile.spatter;
            float mp = interactable.preset.particleProfile.countMultiplier;
            bool stickToActors = interactable.preset.particleProfile.stickToActors;

            if (interactable.preset.overrideSpatterSettings)
            {
                if (interactable.preset.spatterSimulation != null) spat = interactable.preset.spatterSimulation;
                mp = interactable.preset.spatterCountMultiplier;
            }

            SpatterSimulation newSim = new SpatterSimulation(meshes[0].bounds.center, target, spat, SpatterSimulation.EraseMode.onceExecutedAndOutOfAddressPlusDespawnTime, mp, stickToActors);

            //Cause vandalism
            if(interactable.preset.particleProfile.spatterIsVandalism)
            {
                Vector3Int nodePos = CityData.Instance.RealPosToNodeInt(meshes[0].bounds.center);
                NewNode n = null;

                if(PathFinder.Instance.nodeMap.TryGetValue(nodePos, out n))
                {
                    if(n.gameLocation != null && n.gameLocation.thisAsAddress != null)
                    {
                        if(InteractionController.Instance.GetValidPlayerActionIllegal(interactable, n, false))
                        {
                            StatusController.Instance.AddFineRecord(n.gameLocation.thisAsAddress, null, StatusController.CrimeType.vandalism, true, interactable.preset.particleProfile.vandalismFine, ignoreDuplicates: true);
                            n.gameLocation.thisAsAddress.AddVandalism(interactable.preset.particleProfile.vandalismFine);
                            InteractionController.Instance.SetIllegalActionActive(true);
                        }
                    }
                }
            }
        }
    }

    public void ParticleObjectCreation()
    {
        Game.Log("Particle obj creation...");

        if (objectParticleCreationDelay > 0f)
        {
            Game.Log("Delay " + objectParticleCreationDelay);
            return;
        }

        for (int i = 0; i < interactable.preset.particleProfile.instances; i++)
        {
            GameObject newParticle = Instantiate(interactable.preset.particleProfile.objectPool[Toolbox.Instance.Rand(0, interactable.preset.particleProfile.objectPool.Count)], PrefabControls.Instance.mapContainer);
            if(coll != null) Physics.IgnoreCollision(newParticle.GetComponent<Collider>(), coll);

            //Pull position data from contact point
            newParticle.transform.position = this.transform.position;

            if(interactable.preset.particleProfile.useRandomRotation)
            {
                newParticle.transform.rotation = Quaternion.Euler(new Vector3(Toolbox.Instance.Rand(0, 360), Toolbox.Instance.Rand(0, 360), Toolbox.Instance.Rand(0, 360)));
            }
            else
            {
                newParticle.transform.eulerAngles = this.transform.eulerAngles + interactable.preset.particleProfile.localEuler;
            }

            Game.Log("Create " + newParticle.name);
        }

        objectParticleCreationDelay = 0.25f;
    }

    //Get local mesh coordinates of this UV coordinate
    private Vector3 UvTo3D(Vector2 uv, Mesh mesh)
    {
        if (mesh == null || !mesh.isReadable)
        {
            Game.LogError("Mesh is not readable! Fingerprints cannot be gathered as the verts aren't readable...");
            return Vector3.zero;
        }

        int[] tris = mesh.triangles;
        Vector2[] uvs = mesh.uv;
        Vector3[] verts = mesh.vertices;

        for (int i = 0; i < tris.Length; i += 3)
        {
            Vector2 u1 = uvs[tris[i]]; // get the triangle UVs
            Vector2 u2 = uvs[tris[i + 1]];
            Vector2 u3 = uvs[tris[i + 2]];

            // calculate triangle area - if zero, skip it
            float a = Area(u1, u2, u3);
            if (a == 0) continue;

            // calculate barycentric coordinates of u1, u2 and u3
            // if anyone is negative, point is outside the triangle: skip it
            float a1 = Area(u2, u3, uv) / a;
            if (a1 < 0) continue;

            float a2 = Area(u3, u1, uv) / a;
            if (a2 < 0) continue;

            float a3 = Area(u1, u2, uv) / a;
            if (a3 < 0) continue;

            // point inside the triangle - find mesh position by interpolation...
            Vector3 p3D = a1 * verts[tris[i]] + a2 * verts[tris[i + 1]] + a3 * verts[tris[i + 2]];

            //Return as local coordinates
            return p3D;
        }

        //point outside any uv triangle: return Vector3.zero
        return Vector3.zero;
    }

    //calculate signed triangle area using a kind of "2D cross product":
    private float Area(Vector2 p1, Vector2 p2, Vector2 p3)
    {
        Vector2 v1 = p1 - p3;
        Vector2 v2 = p2 - p3;
        return (v1.x * v2.y - v1.y * v2.x) / 2;
    }

    //Use mesh UV pixels to generate shatting voxels...
    public void Shatter(Vector3 contact, float force)
    {
        if (!this.gameObject.activeInHierarchy) return; //Only do if active
        Game.Log("Shattering " + name + " with " + meshes.Count + " meshes...");

        if(meshes.Count > 0)
        {
            foreach(MeshRenderer r in meshes)
            {
                //Get the mesh
                MeshFilter m = r.gameObject.GetComponent<MeshFilter>();

                if (m == null)
                {
                    Game.Log("Misc Error: Cannot find mesh filter for " + r.name);
                    continue; //Can't proceed without a mesh
                }

                Texture2D diffuse = r.material.GetTexture("_BaseColorMap") as Texture2D;

                if (diffuse == null)
                {
                    Game.Log("Misc Error: Cannot find base Texture for " + r.material.name);
                    continue;
                }
                else if(!diffuse.isReadable)
                {
                    Game.LogError("Texture for " + name + " is not readable! Set this if you want to shatter this object...");
                }

                Vector3 voxelScale = interactable.preset.particleProfile.shardSize;

                int shardsSpawned = 0; //The number of shards spawned
                int spawnEveryXpixels = interactable.preset.particleProfile.shardEveryXPixels; //Spawn a shard every this number of pixels
                int pixelCursor = 0; //Used to calc above
                int usedUVPixels = 0; //The amount of used UV pixels in the model

                if (interactable.preset.overrideShatterSettings)
                {
                    voxelScale = interactable.preset.shardSize;
                    spawnEveryXpixels = interactable.preset.shardEveryXPixels;
                }

                GameObject shardPrefab = PrefabControls.Instance.shatterShard;
                if(interactable.preset.particleProfile.isGlass) shardPrefab = PrefabControls.Instance.glassShard;

                //Scan for non black pixels...
                for (int i = 0; i < diffuse.width; i++)
                {
                    for (int l = 0; l < diffuse.height; l++)
                    {
                        Color pxCol = diffuse.GetPixel(i, l);

                        if(pxCol != Color.black && pxCol != Color.clear)
                        {
                            if (pixelCursor <= 0)
                            {
                                Vector2 coord = new Vector2(i / (float)diffuse.width, l / (float)diffuse.height);

                                GameObject shard = Instantiate(shardPrefab, interactable.node.room.transform);
                                shard.transform.position = m.gameObject.transform.TransformPoint(UvTo3D(coord, m.mesh));

                                shard.transform.localScale = voxelScale;

                                if (interactable.preset.particleProfile.isGlass) pxCol.a = 0.5f;

                                MeshRenderer mr = shard.GetComponent<MeshRenderer>();
                                mr.material.SetColor("_BaseColor", pxCol);
                                shardsSpawned++;
                                pixelCursor = spawnEveryXpixels;

                                Rigidbody rb = shard.GetComponent<Rigidbody>();
                                rb.AddExplosionForce(force * interactable.preset.particleProfile.shatterForceMultiplier * Toolbox.Instance.Rand(0.8f, 1.5f), contact, m.mesh.bounds.size.magnitude);
                            }
                            else pixelCursor--;

                            usedUVPixels++;
                        }
                    }
                }

                Game.Log("...Created " + shardsSpawned + " shards.");
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Street cleaner
        int streetCleaningMoney = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.streetCleaningMoney));

        if (streetCleaningMoney > 0)
        {
            if (other.gameObject.CompareTag("Garbage"))
            {
                Game.Log("Garbage collision");

                if (CleanupController.Instance.trash.Contains(interactable))
                {
                    interactable.SafeDelete();
                    GameplayController.Instance.AddMoney(streetCleaningMoney, true, "readingforstreetcleaning");
                }
                else
                {
                    Game.Log("This is not trash");
                }
            }
        }
    }

    public void SetPhysics(bool val, Actor setThrownBy = null)
    {
        physicsOn = val;
        //Game.Log("Set physics for " + name + ": " + physicsOn);

        if(physicsOn)
        {
            //Enable this to allow physics step updates
            this.enabled = true;
            thrownBy = setThrownBy;

            if(coll!= null) wasTrigger = coll.isTrigger;
            coll.isTrigger = false;

            //Cancel kettle heating
            if (interactable.preset.specialCaseFlag == InteractablePreset.SpecialCase.stovetopKettle)
            {
                interactable.SetSwitchState(false, null);
            }

            //Create a rigidbody
            if (rb == null)
            {
                if(alternativePhysicsParent != null) rb = alternativePhysicsParent.gameObject.AddComponent<Rigidbody>();
                else rb = this.gameObject.AddComponent<Rigidbody>();

                PhysicsProfile phy = interactable.preset.GetPhysicsProfile();

                //Load settings from preset
                rb.mass = phy.mass;
                rb.drag = phy.drag;
                rb.angularDrag = phy.angularDrag;
                rb.collisionDetectionMode = phy.collisionMode;

                if(interactable.preset.overrideMass)
                {
                    rb.mass = interactable.preset.mass;
                }

                //Set interpolation
                rb.interpolation = GameplayControls.Instance.interpolation;
            }

            if (!GameplayController.Instance.activePhysics.Contains(interactable))
            {
                GameplayController.Instance.activePhysics.Add(interactable);
                Player.Instance.UpdateCulling();
            }
        }
        else
        {
            if (GameplayController.Instance.activePhysics.Contains(interactable))
            {
                GameplayController.Instance.activePhysics.Remove(interactable);
                Player.Instance.UpdateCullingOnEndOfFrame();
            }

            objectParticleCreationDelay = 0f;
            minimumPhysicsTime = 0f;
            damageEligable = false;

            if (wasTrigger && coll != null) coll.isTrigger = true;

            if(rb != null)
            {
                Destroy(rb);
            }

            //Update kettle
            if(interactable.preset.specialCaseFlag == InteractablePreset.SpecialCase.stovetopKettle)
            {
                //Activate kettles in close proximity
                if (interactable.node != null)
                {
                    FurniturePreset.SubObject hob = null;
                    FurnitureLocation oven = null;

                    foreach(FurnitureLocation f in interactable.node.individualFurniture)
                    {
                        hob = f.furniture.subObjects.Find(item => item.preset.name == "HobBoilPoint");

                        if (hob != null)
                        {
                            Interactable hobInteractable = f.integratedInteractables.Find(item => item.preset.actionsPreset.Exists(item => item.name == "Cooker"));

                            if(hobInteractable != null)
                            {
                                if(hobInteractable.sw0)
                                {
                                    oven = f;
                                    break;
                                }
                            }
                        }
                    }

                    if (hob != null && oven != null)
                    {
                        Matrix4x4 m = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(new Vector3(0, oven.angle + oven.diagonalAngle, 0)), Vector3.one);
                        Vector3 wPos = m.MultiplyPoint3x4(hob.localPos) + oven.anchorNode.position; //World position of hob

                        float dist = Vector3.Distance(wPos, interactable.GetWorldPosition());

                        //Game.Log("Kettle is " + dist + " away from active hob...");

                        if (dist < 0.3f)
                        {
                            interactable.SetSwitchState(true, null);
                        }
                    }
                }
            }

            //UpdateLastMovement();

            thrownBy = null;
            this.enabled = false;
        }
    }

    public void SetVisible(bool val, bool forceUpdate = false)
    {
        if(val != isVisible || forceUpdate)
        {
            isVisible = val;
            if (interactable.printDebug) Game.Log("Interactable " + interactable.name + " set visible: " + isVisible);

            for (int i = 0; i < meshes.Count; i++)
            {
                if (meshes[i] != null)
                {
                    meshes[i].enabled = isVisible;
                }
                else
                {
                    meshes.RemoveAt(i);
                    i--;
                }
            }
        }
    }

    private void UpdateLastMovement()
    {
        if (interactable != null)
        {
            //Update last moved at
            interactable.lma = SessionData.Instance.gameTime;

            if (coll == null)
            {
                coll = this.transform.gameObject.GetComponent<Collider>();
            }

            //Game.Log("Move interactable");

            interactable.MoveInteractable(this.transform.position, this.transform.eulerAngles, false);
        }
    }

    //Update distance for particles
    public void UpdateParticleSystemDistance()
    {
        float dist = Vector3.Distance(this.transform.position, Player.Instance.transform.position);

        if (dist <= particleSystemDistance)
        {
            if (!partSystem.gameObject.activeSelf)
            {
                partSystem.gameObject.SetActive(true);
                partSystem.Play();
            }
        }
        //TODO: Load in a fake representation at distance
        else
        {
            if (partSystem.gameObject.activeSelf)
            {
                partSystem.Pause();
                partSystem.gameObject.SetActive(false);
            }
        }
    }

    public void State1Change()
    {
        if(interactable.sw1)
        {
            if(phoneReciever != null) phoneReciever.SetActive(false);
        }
        else
        {
            if (phoneReciever != null) phoneReciever.SetActive(true);
        }
    }

    [Button]
    public void DisplayCCTVVectors()
    {
        Vector3 cameraForward = Quaternion.Euler(interactable.cve) * Vector3.forward;
        Debug.DrawRay(interactable.cvp, cameraForward, Color.red, 6f);
    }

    [Button]
    public void DisplayCCTVViewNodes()
    {
        if(interactable.sceneRecorder != null)
        {
            Game.Log("Camera covers " + interactable.sceneRecorder.coveredNodes.Count + " nodes");

            foreach(KeyValuePair<NewNode, List<NewRoom.CullTreeEntry>> pair in interactable.sceneRecorder.coveredNodes)
            {
                Debug.DrawRay(interactable.cvp, pair.Key.position - interactable.cvp, Color.blue, 6f);
            }
        }
    }

    [Button]
    public void UpdateSaveFlags()
    {
        willBeSavedWithCity = interactable.save;
        willBeSavedWithState = interactable.IsSaveStateEligable();
        isMainLight = interactable.ml;
    }

    [Button]
    public void GetSaveStateEligable()
    {
        Game.Log(interactable.IsSaveStateEligable() + " (moved: " + interactable.mov + ")");
    }

    //Was this loaded from save game data?
    [Button]
    public void WasThisLoadedFromSaveGameData()
    {
        Game.Log(interactable.wasLoadedFromSave);
    }

    [Button]
    public void SetupInteractable()
    {
        if (meshes == null || meshes.Count <= 0) meshes = this.gameObject.GetComponentsInChildren<MeshRenderer>().ToList();
        if (rb == null) rb = this.gameObject.GetComponent<Rigidbody>();
        if (coll == null) coll = this.gameObject.GetComponent<Collider>();

        if(coll != null)
        {
            MeshCollider meshColl = coll as MeshCollider;

            if(meshColl != null)
            {
                meshColl.convex = true;
            }
        }

        if(coll != null && lightController != null)
        {
            coll.gameObject.layer = 23;
        }

        if (lod == null) lod = this.gameObject.GetComponentInChildren<LODGroup>();

        if(lod == null && (meshes != null && meshes.Count > 0))
        {
            lod = this.gameObject.AddComponent<LODGroup>();

            LOD newLOD = new LOD();
            newLOD.renderers = meshes.ToArray();
            newLOD.screenRelativeTransitionHeight = 0.01f;

            lod.SetLODs(new LOD[] { newLOD });
            lod.RecalculateBounds();
        }

        #if UNITY_EDITOR
        //Force save data
        UnityEditor.EditorUtility.SetDirty(this);
        #endif
    }

    [Button]
    public void IsOnPoolList()
    {
        Debug.Log("Interactable controller reference: " + interactable.controller);
        Debug.Log("Is on range checking (load) list: " + ObjectPoolingController.Instance.interactableRangeToLoadList.Contains(interactable));
        Debug.Log("Is on range checking (enable/disable) list: " + ObjectPoolingController.Instance.interactableRangeToEnableDisableList.Contains(interactable));

        float dist = 0f;
        bool vis = ObjectPoolingController.Instance.SpawnRangeCheck(interactable, out dist);
        Debug.Log("Current visibility range status: " + vis + " distance: " + dist);

        Debug.Log("Geometry loaded: " + interactable.loadedGeometry);
    }

    [Button("Load Furniture's Nodespace Area")]
    public void LoadWalkable()
    {
        if(interactable.furnitureParent != null)
        {
            foreach(NewNode n in interactable.furnitureParent.coversNodes)
            {
                foreach(KeyValuePair<Vector3, NewNode.NodeSpace> pair in n.walkableNodeSpace)
                {
                    GameObject origin = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    origin.transform.SetParent(this.gameObject.transform);
                    origin.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                    origin.transform.position = pair.Value.position;
                }
            }
        }
    }

    [Button("List Users")]
    public void ListUsers()
    {
        foreach(KeyValuePair<Interactable.UsePointSlot, Human> pair in interactable.usagePoint.users)
        {
            Game.Log("Usage slot " + pair.Key.ToString() + ": " + pair.Value);
        }

        Game.Log("Reserved: " + interactable.usagePoint.reserved);
    }

    [Button]
    public void CalculateLocalFurniturePostion()
    {
        if(interactable.furnitureParent != null)
        {
            Game.Log(interactable.furnitureParent.GetSubObjectLocalPosition(interactable.subObject));
        }
    }

    [Button]
    public void TogglePrintDebug()
    {
        if(interactable != null)
        {
            interactable.printDebug = !interactable.printDebug;
            Game.Log("Print debug for " + interactable.name + ": " + interactable.printDebug);
        }
    }

    [Button]
    public void Explode()
    {
        BreakObject(meshes[0].bounds.center, Vector3.zero, 8f, null);
    }

    [Button]
    public void GetLocalizedSnapshot()
    {
        Interactable newPhoto = Toolbox.Instance.GetLocalizedSnapshot(interactable);

        if (newPhoto != null)
        {
            ActionController.Instance.Inspect(newPhoto, Player.Instance.currentNode, Player.Instance);
        }
    }

    [Button]
    public void UpdateName()
    {
        interactable.UpdateName();
    }
}
