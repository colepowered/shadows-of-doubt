﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EvidenceSalesNote : Evidence
{
    public NewAddress forSale;

    //Constructor
    public EvidenceSalesNote(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        foreach(object obj in newPassedObjects)
        {
            Interactable.Passed p = obj as Interactable.Passed;

            if(p != null)
            {
                if(p.varType == Interactable.PassedVarType.addressID)
                {
                    if(CityData.Instance.addressDictionary.TryGetValue(Mathf.RoundToInt(p.value), out forSale))
                    {
                        if(interactable != null)
                        {
                            string noteText = Strings.GetTextForComponent("600d4a18-7306-4871-a68e-e7764ae62f81", interactable, null, null); //data
                            SetNote((new Evidence.DataKey[] { Evidence.DataKey.name }).ToList(), noteText);
                        }
                    }
                }
            }
        }
    }
}
