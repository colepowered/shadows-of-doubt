﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DoorMovementController : MonoBehaviour
{
    [Header("State Positions")]
    public Transform door;
    [System.NonSerialized]
    public Interactable interactable;
    public Dictionary<Collider, int> spawnedDoorColliders = new Dictionary<Collider, int>(); //Remember what original layers these belong to
    [Space(7)]
    public Vector3 closedLocalPos = Vector3.zero;
    public Vector3 openLocalPos = Vector3.zero;
    public Vector3 closedLocalEuler = Vector3.zero;
    public Vector3 openLocalEuler = Vector3.zero;
    public Vector3 closedLocalScale = Vector3.zero;
    public Vector3 openLocalScale = Vector3.zero;
    [Space(7)]
    public Vector3 desiredPos = Vector3.zero;
    public Vector3 desiredEuler = Vector3.zero;
    public Vector3 desiredScale = Vector3.zero;

    [Header("Door State")]
    public DoorMovementPreset preset;
    [Tooltip("0 = Closed, 1 = Open")]
    public float desiredTransition = 0f; // 0 = closed, 1 = open
    public float currentTransition = 0f;
    [Tooltip("True if open")]
    public bool isOpen = false;
    public Actor interacting;
    public bool isAnimating = false;
    public bool isSetup = false;
    public bool isOpening = false;
    public bool isClosing = false;
    [Tooltip("If true this will update looping audio params while animating. Useful for fridge doors etc where the animation is tied to a sfx param.")]
    public bool updateLoopingParams = false;
    [Tooltip("If true this will remove collisions with player while animating")]
    public bool removePlayerCollisionsWhileAnimating = false;

    private void Start()
    {
        if(!isSetup)
        {
            InteractableController cont = this.gameObject.GetComponentInParent<InteractableController>();
            if (cont != null && cont.interactable != null) Setup(cont.interactable, false);
        }
    }

    public void Setup(Interactable newInteractable, bool inheritOpenStatusFromInteractable = true)
    {
        interactable = newInteractable;
        //interactable.doorMovement = this;

        if (door == null) door = this.transform;

        //Get colliders
        if(door != null && removePlayerCollisionsWhileAnimating)
        {
            Collider[] allColliders = door.GetComponentsInChildren<Collider>();
            spawnedDoorColliders.Clear();

            foreach (Collider c in allColliders)
            {
                spawnedDoorColliders.Add(c, c.gameObject.layer);
            }
        }

        //Setup relation positons
        closedLocalPos = door.transform.localPosition + preset.closedRelativePos;
        openLocalPos = door.transform.localPosition + preset.openRelativePos;
        closedLocalEuler = door.transform.localEulerAngles + preset.closedRelativeEuler;
        openLocalEuler = door.transform.localEulerAngles + preset.openRelativeEuler;
        closedLocalScale = door.transform.localScale + preset.closedRelativeScale;
        openLocalScale = door.transform.localScale + preset.openRelativeScale;

        //Set open/closed
        if (inheritOpenStatusFromInteractable)
        {
            if (interactable.sw0)
            {
                SetOpen(1f, null, true);
            }
            else if (!interactable.sw0)
            {
                SetOpen(0f, null, true);
            }
        }

        SetDoorPosition();

        isSetup = true;
    }

    public virtual void SetOpen(float newAjar, Actor interactor, bool skipAnimation = false)
    {
        //Game.Log("Object: " + gameObject.name + " " + interactable.id + " SetOpen " + newAjar);

        //Clamp ajar to 2dp
        newAjar = Mathf.RoundToInt(newAjar * 100f) / 100f;
        desiredTransition = Mathf.Clamp(newAjar, 0f, 1f);

        desiredEuler = Vector3.Lerp(closedLocalEuler, openLocalEuler, desiredTransition);
        desiredPos = Vector3.Lerp(closedLocalPos, openLocalPos, desiredTransition);
        desiredScale = Vector3.Lerp(closedLocalScale, openLocalScale, desiredTransition);

        //Trigger open immediately
        if (desiredTransition != Mathf.Abs(0))
        {
            OnOpen(interactor, !skipAnimation);
        }

        //Skip animation
        if (skipAnimation || !this.gameObject.activeInHierarchy)
        {
            currentTransition = desiredTransition;
            door.localEulerAngles = desiredEuler;
            //Game.Log(gameObject.name + " SetEuler " + desiredEuler);
            door.localPosition = desiredPos;
            door.localScale = desiredScale;

            //Trigger closed
            if (desiredTransition == Mathf.Abs(0))
            {
                OnClose(interactor, false);
            }
        }
        else
        {
            //Play sound
            if(interactable != null)
            {
                if (desiredTransition < currentTransition && preset.closeAction != null)
                {
                    AudioController.Instance.PlayWorldOneShot(preset.closeAction, interactor, interactable.node, this.transform.position, forceIgnoreOcclusion: preset.ignoreOcclusion);
                }
                else if (desiredTransition > currentTransition && preset.openAction != null)
                {
                    AudioController.Instance.PlayWorldOneShot(preset.openAction, interactor, interactable.node, this.transform.position, forceIgnoreOcclusion: preset.ignoreOcclusion);
                }
            }

            StopAllCoroutines();
            StartCoroutine(OpenDoor(interactor));
        }
    }

    //Remove collisions with player when door opens
    public void SetCollisionsWithPlayerActive(bool val)
    {
        if (val)
        {
            foreach (KeyValuePair<Collider, int> pair in spawnedDoorColliders)
            {
                pair.Key.gameObject.layer = pair.Value;
            }
        }
        else
        {
            foreach (KeyValuePair<Collider, int> pair in spawnedDoorColliders)
            {
                pair.Key.gameObject.layer = 6;
            }
        }
    }

    IEnumerator OpenDoor(Actor interactor)
    {
        isAnimating = true;
        if (removePlayerCollisionsWhileAnimating) SetCollisionsWithPlayerActive(false);
        if (preset.switchState1AnimationSync && interactable != null) interactable.SetCustomState1(true, null);

        while(interactable != null && desiredTransition != currentTransition)
        {
            if(SessionData.Instance.play && isAnimating)
            {
                if (preset.collisionBehaviour != DoorMovementPreset.PhysicsBehaviour.ignore)
                {
                    //Enable all physics objects
                    if (interactable != null)
                    {
                        if (interactable.furnitureParent != null)
                        {
                            foreach (NewNode n in interactable.furnitureParent.coversNodes)
                            {
                                foreach (Interactable i in n.interactables)
                                {
                                    if (i.preset.physicsProfile != null && i.preset.reactWithExternalStimuli)
                                    {
                                        if (i.controller != null && !i.controller.physicsOn)
                                        {
                                            Game.Log("Object: Set physics on for " + i.name);
                                            i.controller.SetPhysics(true, null);
                                        }
                                    }
                                }

                                foreach (KeyValuePair<NewNode, NewNode.NodeAccess> pair in n.accessToOtherNodes)
                                {
                                    foreach (Interactable i in pair.Key.interactables)
                                    {
                                        if (i.preset.physicsProfile != null && i.preset.reactWithExternalStimuli)
                                        {
                                            if (i.controller != null && !i.controller.physicsOn)
                                            {
                                                Game.Log("Object: Set physics on for " + i.name);
                                                i.controller.SetPhysics(true, null);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (currentTransition < desiredTransition)
                {
                    currentTransition += Time.deltaTime * preset.doorOpenSpeed;
                    if (currentTransition > desiredTransition) currentTransition = desiredTransition;
                    isOpening = true;
                    isClosing = false;
                }
                else if(currentTransition > desiredTransition)
                {
                    currentTransition -= Time.deltaTime * preset.doorCloseSpeed;
                    if (currentTransition < desiredTransition) currentTransition = desiredTransition;
                    isClosing = true;
                    isOpening = false;
                }

                //float lerpValue = preset.animationCurve.Evaluate(currentTransition);

                //door.localEulerAngles = Vector3.Lerp(closedLocalEuler, openLocalEuler, lerpValue);
                //door.localPosition = Vector3.Lerp(closedLocalPos, openLocalPos, lerpValue);
                //door.localScale = Vector3.Lerp(closedLocalScale, openLocalScale, lerpValue);

                SetDoorPosition();

                if (updateLoopingParams) interactable.UpdateLoopingAudioParams();
            }

            yield return null;
        }

        isAnimating = false;
        if (preset.switchState1AnimationSync && interactable != null) interactable.SetCustomState1(false, null);

        //Play closed sound
        if (desiredTransition == Mathf.Abs(0))
        {
            OnClose(interactor);
        }
    }

    //Set ajar to cs value in interactable
    public void SetDoorPosition()
    {
        float lerpValue = preset.animationCurve.Evaluate(Mathf.Clamp01(currentTransition));

        door.localEulerAngles = Vector3.Lerp(closedLocalEuler, openLocalEuler, lerpValue);
        door.localPosition = Vector3.Lerp(closedLocalPos, openLocalPos, lerpValue);
        door.localScale = Vector3.Lerp(closedLocalScale, openLocalScale, lerpValue);
    }

    public void OnClose(Actor interactor, bool playSound = true)
    {
        isOpen = false;
        isAnimating = false;
        isOpening = false;
        isClosing = false;

        if (removePlayerCollisionsWhileAnimating) SetCollisionsWithPlayerActive(true);

        if (interactable != null)
        {
            interactable.OnDoorMovementClosed();
        }

        if (preset.switchState1AnimationSync && interactable != null) interactable.SetCustomState1(false, null);

        //Play sound
        if (preset.closeFinished != null && playSound)
        {
            AudioController.Instance.PlayWorldOneShot(preset.closeFinished, interactor, interactable.node, this.transform.position, forceIgnoreOcclusion: preset.ignoreOcclusion);
        }
    }

    public void OnOpen(Actor interactor, bool playSound = true)
    {
        isOpen = true;
        isOpening = false;
        isClosing = false;

        if (interactable != null)
        {
            interactable.OnDoorMovementOpened();
        }

        if (preset.switchState1AnimationSync && interactable != null) interactable.SetCustomState1(false, null);

        //Play sound
        if (preset.openFinished != null && playSound)
        {
            AudioController.Instance.PlayWorldOneShot(preset.openFinished, interactor, interactable.node, this.transform.position, forceIgnoreOcclusion: preset.ignoreOcclusion);
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (preset.collisionBehaviour == DoorMovementPreset.PhysicsBehaviour.ignore) return;

        if(Game.Instance.collectDebugData) Game.Log("Door movement " + this.name + " collision: " + collision.gameObject.name + ": " + preset.collisionBehaviour.ToString() + " @ " + collision.GetContact(0).point);

        //Play SFX
        if (preset.objectImpact != null)
        {
            NewNode nPos = null;
            if (interactable != null) nPos = interactable.node;
            AudioController.Instance.PlayWorldOneShot(preset.objectImpact, null, nPos, this.transform.position, forceIgnoreOcclusion: preset.ignoreOcclusion);
        }

        if ((isOpening && preset.behaviourAppliesWhenOpening) || (isClosing && preset.behaviourAppliesWhenClosing))
        {
            //Stop closing
            if(preset.collisionBehaviour == DoorMovementPreset.PhysicsBehaviour.stopDoorMovement)
            {
                //Other object must not be moving...
                if(collision.rigidbody == null || collision.rigidbody.velocity.magnitude < 0.1f)
                {
                    isAnimating = false;
                    if (preset.switchState1AnimationSync && interactable != null) interactable.SetCustomState1(false, null);
                }
                else
                {
                    Game.Log("Door movement collision: " + collision.gameObject.name + " is moving too fast to stop the door (" + collision.rigidbody.velocity.magnitude + ")");
                }
            }
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        if (preset.collisionBehaviour == DoorMovementPreset.PhysicsBehaviour.ignore) return;

        if (desiredTransition != currentTransition)
        {
            isAnimating = true;
            if (preset.switchState1AnimationSync && interactable != null) interactable.SetCustomState1(true, null);
        }
    }
}
