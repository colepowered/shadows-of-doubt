using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using TMPro;
using UnityEngine.Video;
using UnityEditor;
using UnityEngine.UI;

public class ResultsController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public RectTransform pageRect;
    public WindowContentController wcc;
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI descriptionText;
    public TextMeshProUGUI successText;
    public GameObject inputFieldPrefab;
    public ButtonController closeCaseButton;
    public LayoutGroup layout;
    public ProgressBarController questionsBar;
    public ProgressBarController victimsBar;
    public Image rankImage;
    public TextMeshProUGUI rankText;

    [Header("State")]
    public bool isSetup = false;
    public List<InputFieldController> spawnedInputFields = new List<InputFieldController>();

    private static ResultsController _instance;
    public static ResultsController Instance { get { return _instance; } }

    public void Setup(WindowContentController newContentController)
    {
        rect = this.gameObject.GetComponent<RectTransform>();

        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        wcc = newContentController;
        SetPageSize(new Vector2(740, 768));

        titleText.text = Strings.Get("ui.interface", "Investigation Results") + ": " + wcc.window.passedCase.name;
        descriptionText.text = Strings.Get("ui.interface", "results_description");

        UpdateResolveFields();

        if(wcc.window.passedCase.isSolved)
        {
            questionsBar.SetValue(wcc.window.passedCase.questionsRank * 5);
            victimsBar.SetValue(wcc.window.passedCase.victimsRank * 5);

            successText.text = Strings.Get("ui.interface", "Case solved");
        }
        else
        {
            questionsBar.SetValue(0);
            victimsBar.SetValue(0);

            successText.text = Strings.Get("ui.interface", "Case unsolved");
        }

        rankText.text = Strings.Get("ui.interface", wcc.window.passedCase.rank.ToString());
        closeCaseButton.SetInteractable(wcc.window.passedCase.isSolved); //Allow closing of case if solved

        isSetup = true;
    }

    public void UpdateResolveFields()
    {
        CasePanelController.Instance.SetPickModeActive(false, null);

        while(spawnedInputFields.Count > 0)
        {
            Destroy(spawnedInputFields[0].gameObject);
            spawnedInputFields.RemoveAt(0);
        }

        foreach (Case.ResolveQuestion rq in wcc.window.passedCase.resolveQuestions)
        {
            if(rq.displayOnlyAtPhase && wcc.window.passedCase != null && wcc.window.passedCase.job != null)
            {
                if(wcc.window.passedCase.job.phase < rq.displayAtPhase)
                {
                    continue;
                }
            }

            GameObject newQ = Instantiate(inputFieldPrefab, pageRect);
            InputFieldController ifc = newQ.GetComponent<InputFieldController>();
            ifc.Setup(rq, wcc.window.passedCase, true);
            spawnedInputFields.Add(ifc);
        }

        //Make sure button is last
        questionsBar.transform.SetAsLastSibling();
        victimsBar.transform.SetAsLastSibling();
        rankImage.transform.SetAsLastSibling();
        successText.transform.SetAsLastSibling();
        closeCaseButton.rect.SetAsLastSibling();

        //Make sure content is correct size
        SetPageSize(new Vector2(740, Mathf.Max(768, spawnedInputFields.Count * 152f + 640f)));
    }

    public void SetPageSize(Vector2 newSize)
    {
        Game.Log("Set page size: " + newSize);
        rect.sizeDelta = newSize;
        wcc.normalSize = rect.sizeDelta;
    }

    public void CloseCaseButton()
    {
        
    }
}
