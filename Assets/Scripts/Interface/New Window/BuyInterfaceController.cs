﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyInterfaceController : MonoBehaviour
{
    [Header("Settings")]
    public bool sellMode = false;

    [Header("References")]
    public RectTransform pageRect;
    public WindowContentController wcc;
    public Company company;

    [Header("Prefabs")]
    public GameObject elementPrefab;

    private List<ShopSelectButtonController> spawned = new List<ShopSelectButtonController>();

    public void Setup(WindowContentController newWcc)
    {
        wcc = newWcc;

        UpdateElements();
    }

    private void OnEnable()
    {
        if(sellMode)
        {
            UpdateElements();
        }

        UpdatePurchaseAbility();
    }

    public void UpdateElements()
    {
        //Spawn elements
        if(wcc != null && wcc.window != null && wcc.window.passedInteractable != null)
        {
            while (spawned.Count > 0)
            {
                Destroy(spawned[0].gameObject);
                spawned.RemoveAt(0);
            }

            if (!sellMode)
            {
                Dictionary<InteractablePreset, int> prices = new Dictionary<InteractablePreset, int>();

                if (wcc.window.passedInteractable.preset.menuOverride != null)
                {
                    //For vending machines etc, use a relatively expensive price
                    foreach(InteractablePreset ip in wcc.window.passedInteractable.preset.menuOverride.itemsSold)
                    {
                        int newPrice = Mathf.RoundToInt(ip.value.y * (1 + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.priceModifier)));
                        prices.Add(ip, newPrice);
                    }
                }
                else
                {
                    Human staff = wcc.window.passedInteractable.isActor as Human;

                    if(staff != null)
                    {
                        company = staff.job.employer;

                        foreach(KeyValuePair<InteractablePreset, int> pair in company.prices)
                        {
                            prices.Add(pair.Key, Mathf.RoundToInt(pair.Value * (1 + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.priceModifier))));
                        }
                    }
                    else
                    {
                        if(wcc.window.passedInteractable.node.gameLocation.thisAsAddress != null && wcc.window.passedInteractable.node.gameLocation.thisAsAddress.company != null)
                        {
                            company = wcc.window.passedInteractable.node.gameLocation.thisAsAddress.company;

                            foreach (KeyValuePair<InteractablePreset, int> pair in company.prices)
                            {
                                prices.Add(pair.Key, Mathf.RoundToInt(pair.Value * (1 + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.priceModifier))));
                            }
                        }
                    }
                }

                float yPos = 0;

                //Spawn sync disks
                List<MenuPreset> sdMenus = new List<MenuPreset>();

                if (wcc.window.passedInteractable.preset.menuOverride != null)
                {
                    sdMenus.Add(wcc.window.passedInteractable.preset.menuOverride);
                }
                else if(company != null && company.preset != null)
                {
                    sdMenus.AddRange(company.preset.menus);
                }

                foreach (MenuPreset mp in sdMenus)
                {
                    if(mp.syncDiskSlots > 0)
                    {
                        List<SyncDiskPreset> dailyDisks = new List<SyncDiskPreset>();
                        List<SyncDiskPreset> diskPool = new List<SyncDiskPreset>();

                        foreach (SyncDiskPreset diskP in Toolbox.Instance.allSyncDisks)
                        {
                            if (mp.syncDisks.Contains(diskP)) continue;
                            if (!mp.fromManufacturers.Contains(diskP.manufacturer)) continue;

                            int rarity = 4 - (int)diskP.rarity;
                            rarity *= rarity;

                            for (int i = 0; i < rarity; i++)
                            {
                                diskPool.Add(diskP);
                            }
                        }

                        while (dailyDisks.Count < mp.syncDiskSlots && diskPool.Count > 0)
                        {
                            //Choose a new daily deal disk
                            SyncDiskPreset newDisk = diskPool[Toolbox.Instance.GetPsuedoRandomNumber(0, diskPool.Count, CityData.Instance.seed + (SessionData.Instance.dateInt * SessionData.Instance.monthInt * SessionData.Instance.yearInt))];

                            diskPool.RemoveAll(item => item == newDisk);

                            GameObject newObject = Instantiate(elementPrefab, pageRect);
                            ShopSelectButtonController psbc = newObject.GetComponent<ShopSelectButtonController>();
                            spawned.Add(psbc);

                            psbc.rect.anchoredPosition = new Vector2(0, yPos);

                            psbc.Setup(newDisk.interactable, newDisk.price, this, wcc.window, newDisk, true);
                            yPos -= psbc.rect.sizeDelta.y + 10f;

                            pageRect.sizeDelta = new Vector2(pageRect.sizeDelta.x, -yPos + psbc.rect.sizeDelta.y + 60f);

                            dailyDisks.Add(newDisk);
                        }
                    }

                    foreach (SyncDiskPreset newDisk in mp.syncDisks)
                    {
                        GameObject newObject = Instantiate(elementPrefab, pageRect);
                        ShopSelectButtonController psbc = newObject.GetComponent<ShopSelectButtonController>();
                        spawned.Add(psbc);

                        psbc.rect.anchoredPosition = new Vector2(0, yPos);

                        psbc.Setup(newDisk.interactable, newDisk.price, this, wcc.window, newDisk);
                        yPos -= psbc.rect.sizeDelta.y + 10f;

                        pageRect.sizeDelta = new Vector2(pageRect.sizeDelta.x, -yPos + psbc.rect.sizeDelta.y + 60f);
                    }
                }

                foreach (KeyValuePair<InteractablePreset, int> p in prices)
                {
                    GameObject newObject = Instantiate(elementPrefab, pageRect);
                    ShopSelectButtonController psbc = newObject.GetComponent<ShopSelectButtonController>();
                    spawned.Add(psbc);

                    psbc.rect.anchoredPosition = new Vector2(0, yPos);

                    psbc.Setup(p.Key, p.Value, this, wcc.window);
                    yPos -= psbc.rect.sizeDelta.y + 10f;

                    pageRect.sizeDelta = new Vector2(pageRect.sizeDelta.x, -yPos + psbc.rect.sizeDelta.y + 60f);
                }
            }
            else
            {
                float yPos = 0;

                Human staff = wcc.window.passedInteractable.isActor as Human;

                if (staff != null)
                {
                    company = staff.job.employer;
                }
                else
                {
                    if (wcc.window.passedInteractable.node.gameLocation.thisAsAddress != null && wcc.window.passedInteractable.node.gameLocation.thisAsAddress.company != null)
                    {
                        company = wcc.window.passedInteractable.node.gameLocation.thisAsAddress.company;
                    }
                }

                float sellMP = 0.5f;
                if (company != null) sellMP = company.preset.sellValueMultiplier;

                //In sell mode display inventory slots
                foreach (FirstPersonItemController.InventorySlot slot in FirstPersonItemController.Instance.slots)
                {
                    if (slot.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.nonStatic)
                    {
                        Interactable interactable = slot.GetInteractable();

                        if(interactable != null)
                        {
                            GameObject newObject = Instantiate(elementPrefab, pageRect);
                            ShopSelectButtonController psbc = newObject.GetComponent<ShopSelectButtonController>();
                            spawned.Add(psbc);

                            psbc.rect.anchoredPosition = new Vector2(0, yPos);

                            //Selling things = half value
                            psbc.Setup(interactable.preset, Mathf.CeilToInt(interactable.val * sellMP), this, wcc.window, newSellInteractable: interactable, newSellMode: true);
                            yPos -= psbc.rect.sizeDelta.y + 10f;

                            pageRect.sizeDelta = new Vector2(pageRect.sizeDelta.x, -yPos + psbc.rect.sizeDelta.y + 60f);
                        }
                    }
                }
            }
        }

        UpdatePurchaseAbility();
    }

    public void UpdatePurchaseAbility()
    {
        foreach(ShopSelectButtonController s in spawned)
        {
            s.UpdatePurchaseAbility();
        }
    }
}
