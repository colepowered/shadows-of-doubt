﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AssetConfig : ScriptableObject
{
    [MenuItem("Assets/Set Animators Always On")]
    static void SetAnimatorsAlwaysOn()
    {
        if (UnityEditor.EditorApplication.isPlaying)
            return;

        UnityEngine.Object[] selection = Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.DeepAssets);

        string desc = "Set Animators Always On";
        EditorUtility.DisplayProgressBar("Working", "Making changes...", 0);
        int i = 0;
        foreach (UnityEngine.Object item in selection)
        {
            if (item is GameObject)
            {
                try
                {
                    GameObject go = (GameObject)item;
                    Animator[] animators1 = go.GetComponents<Animator>();
                    Animator[] animators2 = go.GetComponentsInChildren<Animator>();
                    if (animators1.Length > 0 || animators2.Length > 0)
                    {
                        UnityEditor.Undo.RecordObject(go, desc);
                        for (int j = 0; j < animators1.Length; j++)
                        {
                            animators1[j].cullingMode = AnimatorCullingMode.AlwaysAnimate;
                        }
                        for (int j = 0; j < animators2.Length; j++)
                        {
                            animators2[j].cullingMode = AnimatorCullingMode.AlwaysAnimate;
                        }

                        UnityEditor.EditorUtility.SetDirty(go);
                        UnityEditor.PrefabUtility.RecordPrefabInstancePropertyModifications(go);
                    }
                }
                catch (Exception ex)
                {

                }
            }
            i++;

            EditorUtility.DisplayProgressBar("Working", "Making changes...", (float)i / (float)selection.Length);
        }

        AssetDatabase.SaveAssets();
        EditorUtility.ClearProgressBar();
    }
}
#endif