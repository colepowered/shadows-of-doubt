using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakerController : DoorMovementController
{
    public override void SetOpen(float newAjar, Actor interactor, bool skipAnimation = false)
    {
        newAjar = Mathf.Clamp01(interactable.val);
        base.SetOpen(newAjar, interactor, skipAnimation);
    }
}
