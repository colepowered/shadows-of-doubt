﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EvidenceFingerprintController : MonoBehaviour
{
    public InfoWindow parentWindow;
    public RawImage photoRaw;
    public TextMeshProUGUI identifierText;

    private void OnEnable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange += CheckEnabled;
        parentWindow.OnWindowRefresh += CheckEnabled;

        CheckEnabled();
    }

    private void OnDisable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        parentWindow.passedEvidence.OnDataKeyChange -= CheckEnabled;
        parentWindow.OnWindowRefresh -= CheckEnabled;
    }

    public void CheckEnabled()
    {
        Game.Log("Interface: Check for fingerprint key in parent window...");
        Game.Log("Player: Fingerprints belong to " + parentWindow.passedEvidence.writer.GetCitizenName());

        if (parentWindow.evidenceKeys.Contains(Evidence.DataKey.fingerprints))
        {
            //Get prints letter loop
            if (parentWindow.passedEvidence.writer.fingerprintLoop <= -1)
            {
                parentWindow.passedEvidence.writer.fingerprintLoop = GameplayController.Instance.printsLetterLoop;
                GameplayController.Instance.printsLetterLoop++;
            }

            photoRaw.texture = CitizenControls.Instance.prints[Toolbox.Instance.GetPsuedoRandomNumber(0, CitizenControls.Instance.prints.Count, parentWindow.passedEvidence.writer.citizenName + parentWindow.passedEvidence.writer.humanID)];
            identifierText.text = Strings.Get("evidence.generic", "Type", Strings.Casing.firstLetterCaptial) + " " + Toolbox.Instance.ToBase26(parentWindow.passedEvidence.writer.fingerprintLoop);
            identifierText.gameObject.SetActive(true);
        }
        else
        {
            photoRaw.texture = CitizenControls.Instance.unknownPrint;
            identifierText.gameObject.SetActive(false);
        }

        photoRaw.color = Color.white;
    }
}
