﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;
using System;
using System.Text;
using FMOD.Studio;

//Base class for any 'item' of evidence
//[System.Serializable]
public class Evidence : CaseComponent
{
    //Evidence ID
    //Identifier comprises of Type, ID
    public string evID;
    public bool forceSave = false;

    //Interactable for this, if there is one
    [System.NonSerialized]
    public Interactable interactable;
    public InteractablePreset interactablePreset;

    //Preset containing data for this
    public EvidencePreset preset;

    //Override picture
    public Sprite imageOverride;

    //Parent of this evidence
    [System.NonSerialized]
    public Evidence parent;

    //Sender/reciver
    public Human writer;
    public Human reciever;
    public Human belongsTo;
    public string overrideDDS; //Overrides the preset's DDS id. Used for setting notes etc through interactables

    //Controller
    public Controller controller;

    //Meta object
    public MetaObject meta;

    //Children of this evidence
    [System.NonSerialized]
    public List<Evidence> children = new List<Evidence>();

    public enum Discovery { livesAt, partnerDiscovery, jobDiscovery, purchasedAt, phoneLocation, paramourDiscovery, phonePersonal, foundAt, foundOn, addressBookDiscovery, relationshipDiscovery, jobHours, diaryDiscovery, jobDiscoveryPhoto, dateOfBirth, timeOfDeath, referenceDiscovery, postedByDiscovery };

    //Facts based on mystery key data
    public enum DataKey { name, photo, fingerprints, code, voice, height, build, age, sex, hair, eyes, bloodType, shoeSize, facialHair, address, work, workHours, jobTitle, shoeSizeEstimate, glasses, dateOfBirth, salary, randomInterest, randomSocialClub, ageGroup, firstNameInitial, partnerFirstName, partnerJobTitle, partnerSocialClub, randomAffliction, heightEstimate, handwriting, livesOnFloor, telephoneNumber, livesInBuilding, worksInBuilding, location, blueprints, firstName, surname, initialedName, initials, purpose }; //Unique identifiers that can be used to identify a subject (and linked to each other)

    //Data keys can be tied to other data keys, use this dictionary as a reference to get tied data keys
    public Dictionary<DataKey, List<DataKey>> keyTies = new Dictionary<DataKey, List<DataKey>>();

    //Data struct that holds info about the links to facts. This acts as an instance of a fact transfer to the fact (but not to the destination).
    [System.Serializable]
    public class FactLink
    {
        public Fact fact; //Reference to the fact (may be multiple to/from evidence)

        public Evidence thisEvidence; //This or parent evidence == class that holds this link
        public List<Evidence.DataKey> thisKeys; //List of this or parent evidence keys

        public List<Evidence> destinationEvidence; //List of destination evidence
        public List<Evidence.DataKey> destinationKeys; //List of destination evidence keys

        public bool thisIsTheFromEvidence; //Useful for keeping track whether this evidence is the from or to evidence in the fact class

        //Constructor: Try to inherit string colour from pin colour
        public FactLink()
        {

        }
    }

    public List<CustomName> customNames = new List<CustomName>();

    [System.Serializable]
    public class CustomName
    {
        public Evidence.DataKey key;
        public string name;
    }

    //Facts linked to data keys
    public Dictionary<DataKey, List<FactLink>> factDictionary = new Dictionary<DataKey, List<FactLink>>();
    [System.NonSerialized]
    public List<FactLink> allFacts = new List<FactLink>(); //Quick reference list containing all fact connections, regardless of data key

    //Discovery progress for find other evidence
    public List<Discovery> discoveryProgress = new List<Discovery>();

    //Match types
    public List<MatchPreset> matches = new List<MatchPreset>();

    //Notes
    public Dictionary<DataKey, string> notes = new Dictionary<DataKey, string>();

    //Events
    //Triggered when this is discovered
    public delegate void OnDiscover(Evidence disc);
    public event OnDiscover OnDiscovered;

    //Triggered when a new parent for this is set
    public delegate void NewParent();
    public event NewParent OnNewParent;

    //Triggered when a new child evidence is added
    public delegate void NewChild();
    public event NewChild OnNewChild;

    //Triggered when a child evidence is removed
    public delegate void RemChild();
    public event RemChild OnRemoveChild;

    //Triggered when a child evidence is discovered (also triggered when an added child is already discovered)
    public delegate void DiscoverChild();
    public event DiscoverChild OnDiscoverChild;

    //Triggered when a new fact is connected
    public delegate void ConnectFact();
    public event ConnectFact OnConnectFact;

    //Triggered when a connected fact is discovered (also triggered when an added fact is already discovered)
    public delegate void DiscoverConnectedFact();
    public event DiscoverConnectedFact OnDiscoverConnectedFact;

    //Triggered when data keys are merged/added
    public delegate void DataKeyChange();
    public event DataKeyChange OnDataKeyChange;

    //Triggered when discovery is added
    public delegate void DiscoveryChanged(Discovery newDisc);
    public event DiscoveryChanged OnDiscoveryChanged;

    //Triggered when a match type is added
    public delegate void MatchTypeAdded();
    public event MatchTypeAdded OnMatchTypeAdded;

    //Triggered when the evidence interaction mode is changed
    //public delegate void InteractionChange();
    //public event InteractionChange OnInteractionChange;

    //Triggered when any key of this evidence is pinned or unpinned
    public delegate void AnyPinnedChange();
    public event AnyPinnedChange OnAnyPinnedChange;

    //Triggered when a note is added
    public delegate void NoteAdded();
    public event NoteAdded OnNoteAdded;

    //Actions - used as callback for end of frame
    Action OrderCheck;

    //Constructor
    public Evidence(EvidencePreset newPreset, string newID, Controller newController, List<object> newPassedObjects)
    {
        //Set preset
        preset = newPreset;
        evID = newID;

        //Attempt to parse interactable from passed objects...
        if(newPassedObjects != null)
        {
            foreach (object obj in newPassedObjects)
            {
                Interactable inter = obj as Interactable;

                if (inter != null)
                {
                    interactable = inter;
                    interactablePreset = inter.preset;
                }

                InteractablePreset pr = obj as InteractablePreset;

                if(pr != null)
                {
                    interactablePreset = pr;
                }

                Interactable.Passed pVar = obj as Interactable.Passed;

                //Pass a DDS override
                if(pVar != null)
                {
                    if(pVar.varType == Interactable.PassedVarType.ddsOverride)
                    {
                        SetOverrideDDS(pVar.str);
                    }
                    else if(pVar.varType == Interactable.PassedVarType.metaObjectID)
                    {
                        meta = CityData.Instance.FindMetaObject((int)pVar.value);

                        if (meta == null)
                        {
                            Game.Log("Unable to find meta object " + (int)pVar.value + " for evidence " + preset.name + " (" + evID + ")");
                        }
                    }
                }
            }
        }

        //Add to evidence list
        if(!GameplayController.Instance.evidenceDictionary.ContainsKey(evID))
        {
            GameplayController.Instance.evidenceDictionary.Add(evID, this);
        }
        else
        {
            Game.LogError("Trying to create evidence with existing ID " + evID);
        }

        //Singleton
        if (preset.isSingleton)
        {
            GameplayController.Instance.singletonEvidence.Add(this);
        }

        //Set controller
        controller = newController;

        //Set the sprite reference for icons
        SetNewIcon(preset.iconSpriteLarge);

        //Setup key ties (to begin with each key is only tied to itself, with some exceptions).
        SetupKeyTies();

        //Setup matches
        foreach (MatchPreset mt in preset.matchTypes)
        {
            AddMatch(mt);
        }
    }

    public override string GenerateName()
    {
        //Add belongs to if found
        if (writer != null && preset.useBelongsToInName && isFound)
        {
            return writer.GetCitizenName() + "'s " + Strings.Get("evidence.names", preset.name);
        }

        else return base.GenerateName();
    }

    //Compile: This happens after creation if game has started, if before it is called at a late stage of loading the city
    public virtual void Compile()
    {
        AutoCreateFacts(false); //Setup facts

        //Build text data sources
        if(!SessionData.Instance.isFloorEdit && !SessionData.Instance.isTestScene)
        {
            BuildDataSources();
        }

        //Setup fact links
        foreach(EvidencePreset.FactLinkSetup l in preset.addFactLinks)
        {
            Human sub = writer;
            if (l.subject == EvidencePreset.FactLinkSubject.receiver) sub = reciever;

            if(sub != null)
            {
                if (sub.factDictionary.ContainsKey(l.factDictionary))
                {
                    AddFactLink(sub.factDictionary[l.factDictionary], l.key, false);
                }
            }
        }

        //Set name
        UpdateName();

        //Discover on create
        if (preset.discoverOnCreate || Game.Instance.discoverAllEvidence) SetFound(true);
    }

    public override string GetIdentifier()
    {
        return evID;
    }

    //Get the current pinned item in the current case
    public PinnedItemController GetPinned(List<DataKey> inputKeys)
    {
        if (CasePanelController.Instance.activeCase == null) return null; //Needs an active case...

        inputKeys = GetTiedKeys(inputKeys); //Make sure we're using tied keys here

        //List<DataKey> keys = GetTiedKeys(inputKeys);
        List<Case.CaseElement> currentCaseElements = CasePanelController.Instance.activeCase.caseElements.FindAll(item => item.id == evID);

        //If a unique key is present, then the returned must feature that key...
        HashSet<DataKey> uniqueKeys = new HashSet<DataKey>();

        if (preset.useDataKeys)
        {
            foreach(DataKey key in inputKeys)
            {
                if(preset.IsKeyUnique(key))
                {
                    if(!uniqueKeys.Contains(key)) uniqueKeys.Add(key);
                }
            }
        }

        foreach(DataKey key in inputKeys)
        {
            foreach(Case.CaseElement element in currentCaseElements)
            {
                if (element.pinnedController == null) continue;

                if(element.dk.Contains(key))
                {
                    //If a unique key is present, then the returned must feature that key...
                    if (preset.useDataKeys && uniqueKeys.Count > 0)
                    {
                        bool uniqueKeyPass = false;

                        foreach(DataKey u in uniqueKeys)
                        {
                            if (element.dk.Contains(u))
                            {
                                uniqueKeyPass = true;
                                break;
                            }
                        }

                        if (!uniqueKeyPass) continue;
                    }

                    //Game.Log("Returned pinned " + element.pinnedController.name + " for evidence " + evID + " using key " + key.ToString());
                    return element.pinnedController;
                }
            }
        }

        return null;
    }

    //Update name
    public override string FoundAtName()
    {
        string ret = string.Empty;

        //if (preset.useDiscoveryFactInName && isFound && autoDiscoveryFact != null)
        //{
        //    ret = " " + StringTables.Get("evidence.generic", preset.name) + " ";

        //    //Get to names
        //    for (int i = 0; i < autoDiscoveryFact.toEvidence.Count; i++)
        //    {
        //        Evidence toEv = autoDiscoveryFact.toEvidence[i];

        //        if (i > 0) ret += " & ";
        //        ret += toEv.GetNameForDataKey(Evidence.DataKey.name);
        //    }
        //}

        return ret;
    }

    //Setup key ties (to begin with each key is only tied to itself, with some exceptions).
    private void SetupKeyTies()
    {
        List<Evidence.DataKey> validKeys = preset.GetValidProfileKeys();

        //Setup data keys
        if(preset.useDataKeys)
        {
            foreach (DataKey newKey in validKeys)
            {
                keyTies.Add(newKey, new List<DataKey>());
                keyTies[newKey].Add(newKey); //Add itself
            }
        }
        else
        {
            keyTies.Add(DataKey.name, new List<DataKey>());
            keyTies[DataKey.name].Add(DataKey.name);
        }
    }

    //Building text data sources
    public virtual void BuildDataSources()
    {
        if (writer != null)
        {
            //Add the citizen as data source so they are shared.
            //dataSource = writer.dataSource;
        }
    }

    //Add parent evidence
    public void SetParent(Evidence newParent)
    {
        if(SessionData.Instance.startedGame) Game.Log("Evidence: " + name + " set parent to " + newParent.name);
        if (parent == newParent) return;

        //Remove from previous parent
        if (parent != null)
        {
            parent.RemoveChild(this);
        }

        //Add to new parent's children
        newParent.AddChild(this);

        parent = newParent;

        //Check parent's discoveries to see if this should be found
        if (!isFound)
        {
            if (parent != null)
            {
                if (parent.isFound)
                {
                    parent.UpdateDiscoveries();
                }
            }
        }

        //Fire event
        if (OnNewParent != null) OnNewParent();
    }

    public void SetBelongsTo(Human newOwner)
    {
        belongsTo = newOwner;

        if (preset.itemOwner == EvidencePreset.BelongsToSetting.partner && belongsTo.partner != null)
        {
            belongsTo = belongsTo.partner;
        }
        else if (preset.itemOwner == EvidencePreset.BelongsToSetting.boss && belongsTo.job != null && belongsTo.job.boss != null && belongsTo.job.boss.employee != null)
        {
            belongsTo = belongsTo.job.boss.employee;
        }
        else if (preset.itemOwner == EvidencePreset.BelongsToSetting.paramour && belongsTo.paramour != null)
        {
            belongsTo = belongsTo.paramour;
        }
        else if (preset.itemOwner == EvidencePreset.BelongsToSetting.doctor)
        {
            belongsTo = belongsTo.GetDoctor();
        }
        else if (preset.itemOwner == EvidencePreset.BelongsToSetting.landlord)
        {
            belongsTo = belongsTo.GetLandlord();
        }
    }

    //Set writer
    public void SetWriter(Human newWriter)
    {
        writer = newWriter;

        if (preset.itemWriter == EvidencePreset.BelongsToSetting.partner && writer.partner != null)
        {
            writer = writer.partner;
        }
        else if(preset.itemWriter == EvidencePreset.BelongsToSetting.boss && writer.job != null && writer.job.boss != null && writer.job.boss.employee != null)
        {
            writer = writer.job.boss.employee;
        }
        else if(preset.itemWriter == EvidencePreset.BelongsToSetting.paramour && writer.paramour != null)
        {
            writer = writer.paramour;
        }
        else if (preset.itemWriter == EvidencePreset.BelongsToSetting.doctor)
        {
            writer = writer.GetDoctor();
        }
        else if (preset.itemWriter == EvidencePreset.BelongsToSetting.landlord)
        {
            writer = writer.GetLandlord();
        }
    }

    //Set reciever
    public void SetReciever(Human newReciever)
    {
        reciever = newReciever;

        if (preset.itemReceiver == EvidencePreset.BelongsToSetting.partner && reciever.partner != null)
        {
            reciever = reciever.partner;
        }
        else if (preset.itemReceiver == EvidencePreset.BelongsToSetting.boss && reciever.job != null && reciever.job.boss != null && reciever.job.boss.employee != null)
        {
            reciever = reciever.job.boss.employee;
        }
        else if (preset.itemReceiver == EvidencePreset.BelongsToSetting.paramour && reciever.paramour != null)
        {
            reciever = reciever.paramour;
        }
        else if (preset.itemReceiver == EvidencePreset.BelongsToSetting.doctor)
        {
            reciever = reciever.GetDoctor();
        }
        else if (preset.itemReceiver == EvidencePreset.BelongsToSetting.landlord)
        {
            reciever = reciever.GetLandlord();
        }
    }

    //Set override DDS
    public void SetOverrideDDS(string newTreeID)
    {
        if(newTreeID.Length > 0) overrideDDS = newTreeID;
    }

    //Add a child (done through set parent)
    private void AddChild(Evidence newEv)
    {
        if (!children.Contains(newEv))
        {
            children.Add(newEv);

            //Fire event
            if (OnNewChild != null) OnNewChild();

            //If already found, trigger the discovery
            if (newEv.isFound)
            {
                OnChildEvidenceDiscovery();
            }
        }
    }

    //Remove a child (done through set parent)
    private void RemoveChild(Evidence newEv)
    {
        if (children.Contains(newEv))
        {
            children.Remove(newEv);

            //Fire event
            if (OnRemoveChild != null) OnRemoveChild();
        }
    }

    //Triggered when a child evidence of this is discovered
    public void OnChildEvidenceDiscovery()
    {
        if (OnDiscoverChild != null) OnDiscoverChild();
    }

    //Add a fact to the dictionary (done through fact constructor hierarchy)
    //Set up to handle multiple keys but call events once: It's important to use the correct fuction here as calling the single key one multiple times will slow things down!
    public void AddFactLink(Fact newFact, List<DataKey> newKey, bool thisIsTheFromEvidence)
    {
        if(SessionData.Instance.startedGame) Game.Log("Evidence: " + name + " Add fact link for " + newFact.name + " with " + newKey.Count + " keys");

        foreach (DataKey key in newKey)
        {
            AddFactLinkExe(newFact, key, thisIsTheFromEvidence);
        }

        //Fire event
        if (OnConnectFact != null) OnConnectFact();

        //If this fact is alread discovered, trigger OnConnectedFactDiscovery, if not, listen for discovery
        if (newFact.isFound)
        {
            if (SessionData.Instance.startedGame) Game.Log("Evidence: " + newFact.preset.name + " is found: Trigger OnConnectedFactDiscovery");
            OnConnectedFactDiscovery(newFact);
        }
        else
        {
            //Listen for fact discovery
            if (SessionData.Instance.startedGame) Game.Log("Evidence: " + newFact.preset.name + " is NOT found: Listen for fact's OnDiscoveredThis");
            newFact.OnDiscoveredThis += OnConnectedFactDiscovery;
        }
    }

    public void AddFactLink(Fact newFact, DataKey newKey, bool thisIsTheFromEvidence)
    {
        if (SessionData.Instance.startedGame) Game.Log("Match: AddFactLink " + newKey.ToString());
        AddFactLinkExe(newFact, newKey, thisIsTheFromEvidence);

        //Fire event
        if (OnConnectFact != null) OnConnectFact();

        //If this fact is alread discovered, trigger OnConnectedFactDiscovery, if not, listen for discovery
        if (newFact.isFound)
        {
            if (SessionData.Instance.startedGame) Game.Log("Match: NewFact is found: Trigger OnConnectedFactDiscovery");
            OnConnectedFactDiscovery(newFact);
        }
        else
        {
            //Listen for fact discovery
            if (SessionData.Instance.startedGame) Game.Log("Match: NewFact is NOT found: Listen for fact's OnDiscoveredThis");
            newFact.OnDiscoveredThis += OnConnectedFactDiscovery;
        }
    }

    private void AddFactLinkExe(Fact newFact, DataKey newKey, bool thisIsTheFromEvidence)
    {
        //Create data key instance
        if (!factDictionary.ContainsKey(newKey))
        {
            factDictionary.Add(newKey, new List<FactLink>());
        }

        //Create new link
        FactLink newLink = new FactLink();
        newLink.fact = newFact;
        newLink.thisEvidence = this;

        //Lists are passed by reference, so these should be kept up-to-date
        if (thisIsTheFromEvidence)
        {
            newLink.thisKeys = newFact.fromDataKeys;
            newLink.destinationEvidence = newFact.toEvidence;
            newLink.destinationKeys = newFact.toDataKeys;
        }
        else
        {
            newLink.thisKeys = newFact.toDataKeys;
            newLink.destinationEvidence = newFact.fromEvidence;
            newLink.destinationKeys = newFact.fromDataKeys;
        }

        newLink.thisIsTheFromEvidence = thisIsTheFromEvidence;

        //Add the fact
        factDictionary[newKey].Add(newLink);
        allFacts.Add(newLink);
    }

    public void RemoveFactLink(Fact removeThis)
    {
        List<FactLink> links = allFacts.FindAll(item => item.fact == removeThis);

        while(links.Count > 0)
        {
            allFacts.Remove(links[0]);
            links.RemoveAt(0);
        }

        foreach (KeyValuePair<DataKey, List<FactLink>> pair in factDictionary)
        {
            for (int i = 0; i < pair.Value.Count; i++)
            {
                if (pair.Value[i].fact == removeThis)
                {
                    pair.Value.RemoveAt(i);
                    i--;
                    continue;
                }
            }
        }

        //Fire event
        if (OnConnectFact != null) OnConnectFact();
    }

    ////Add a timeline event to the dictionary
    //public void AddTimelineEvent(TimelineBlock newEntry, DataKey newKey)
    //{
    //    //Create data key instance
    //    if (!eventDictionary.ContainsKey(newKey))
    //    {
    //        eventDictionary.Add(newKey, new List<TimelineBlock>());

    //        //Add the fact
    //        eventDictionary[newKey].Add(newEntry);
    //        allEvents.Add(newEntry);

    //        //Fire event
    //        if (OnTimelineEventsChanged != null) OnTimelineEventsChanged();
    //    }
    //}

    //Triggered when a connected fact is discovered (regardless of data key) or a new discovered fact is linked
    public virtual void OnConnectedFactDiscovery(CaseComponent discovered)
    {
        //Fire event
        if (OnDiscoverConnectedFact != null) OnDiscoverConnectedFact();
    }

    //Add a match preset
    public void AddMatch(MatchPreset newMatch)
    {
        matches.Add(newMatch);

        //Add to police db
        GameplayController.Instance.AddNewMatch(newMatch, this);

        //Fire event
        if (OnMatchTypeAdded != null) OnMatchTypeAdded();
    }

    //If this contains incrimination sources, add to the police dictionary
    public override void OnDiscovery()
    {
        //if(SessionData.Instance.startedGame) Game.Log("Discover Evidence: " + name);

        //Merge keys on discovery
        foreach (EvidencePreset.MergeKeysSetup mk in preset.keyMergeOnDiscovery)
        {
            Evidence sub = GetLinkForFact(mk.link);

            if(sub != null)
            {
                foreach (DataKey key in mk.mergeKeys)
                {
                    foreach (DataKey key2 in mk.mergeKeys)
                    {
                        //Game.Log(name + " Found: Merge keys " + key.ToString() + " and " + key2.ToString() + " in " + sub.name);
                        sub.MergeDataKeys(key, key2);
                    }
                }
            }
        }

        AutoCreateFacts(true);

        //Trigger child discovery in parent
        if (parent != null)
        {
            parent.OnChildEvidenceDiscovery();
        }
        else
        {
            //if (SessionData.Instance.startedGame && !SessionData.Instance.isFloorEdit) Game.Log("Object: " + name + " parent not found");
        }

        //Apply on discovery
        foreach(EvidencePreset.DiscoveryApplication dis in preset.applicationOnDiscover)
        {
            Evidence sub = GetLinkForFact(dis.link);

            if(sub != null)
            {
                sub.AddDiscovery(dis.applyDiscoveryTrigger);
            }
        }

        //Update discoveries of this to see if any children should be found
        UpdateDiscoveries();

        //Setup fact links
        foreach (EvidencePreset.FactLinkSetup l in preset.addFactLinks)
        {
            if (!l.discovery) continue;
            Human sub = writer;
            if (l.subject == EvidencePreset.FactLinkSubject.receiver) sub = reciever;

            if (sub != null)
            {
                if (sub.factDictionary.ContainsKey(l.factDictionary))
                {
                    if(!sub.factDictionary[l.factDictionary].isFound)
                    {
                        sub.factDictionary[l.factDictionary].SetFound(true);
                    }
                }
            }
        }

        //Update interactable name
        if (interactable != null)
        {
            interactable.UpdateName(true, DataKey.photo);
        }

        //Fire event
        if (OnDiscovered != null) OnDiscovered(this);
    }

    public virtual void AutoCreateFacts(bool discovery)
    {
        foreach(EvidencePreset.EvidenceFactSetup f in preset.factSetup)
        {
            if (f.createOnDiscovery && !discovery) continue;
            else if (!f.createOnDiscovery && discovery) continue;

            Evidence from = this;
            Evidence to = GetLinkForFact(f.link);

            if(f.switchFindingFactToFrom)
            {
                from = to;
                to = this;
            }

            if(from != null && to != null)
            {
                //If not on furniture this won't be created.
                if (!f.onlyIfInOwnedPosition || (interactable.subObject != null && interactable.subObject.belongsTo != FurniturePreset.SubObjectOwnership.nobody))
                {
                    EvidenceCreator.Instance.CreateFact(f.preset.name, from, to, forceDiscoveryOnCreate: f.forceDiscoveryOnCreation);
                }
            }
        }
    }

    public Evidence GetLinkForFact(EvidencePreset.Subject subject)
    {
        Evidence ret = null;

        if (subject == EvidencePreset.Subject.self)
        {
            ret = this;
        }
        else if (subject == EvidencePreset.Subject.writer && writer != null)
        {
            ret = writer.evidenceEntry;
        }
        else if(subject == EvidencePreset.Subject.receiver && reciever != null)
        {
            ret = reciever.evidenceEntry;
        }
        else if (subject == EvidencePreset.Subject.parent && parent != null)
        {
            ret = parent;
        }
        else if (subject == EvidencePreset.Subject.interactable && interactable != null && interactable.evidence != null)
        {
            ret = interactable.evidence;
        }
        else if (subject == EvidencePreset.Subject.interactableLocation)
        {
            if (interactable != null)
            {
                if (interactable.node != null)
                {
                    ret = interactable.node.gameLocation.evidenceEntry;
                }
                else Game.LogError("Unable to get current node for evidence " + evID + " interactable: " + interactable.id + " " + interactable.GetName());
            }
            else Game.LogError("Unable to get current location via interactable for evidence " + evID);
        }

        return ret;
    }

    //Merge data keys with each other...
    //Unique keys can feature non-unique keys, but the opposite is not true
    public virtual void MergeDataKeys(DataKey keyOne, DataKey keyTwo)
    {
        //Cannot merge the 'none' key
        if (keyOne == keyTwo) return; // No point in merging the same keys...
        if (!preset.useDataKeys) return;

        //New set of data keys
        List<DataKey> newSet = new List<DataKey>();

        //Add key one elements
        bool photoKey1 = false;
        bool nameKey1 = false;
        int previousAmount = 0; //Needed for UI graphic
        bool uniqueKeyMerge = false; //Trigger if this does something
        bool displayMessage = false; //Display a notification

        if(keyTies.ContainsKey(keyOne))
        {
            previousAmount = preset.GetProfileKeyCount(keyTies[keyOne]);

            foreach (DataKey key in keyTies[keyOne])
            {
                if (!newSet.Contains(key)) newSet.Add(key);

                //Photo or name is in first
                if(key == DataKey.photo)
                {
                    photoKey1 = true;
                }
                else if(key == DataKey.name)
                {
                    nameKey1 = true;
                }
            }
        }

        bool photoKey2 = false;
        bool nameKey2 = false;

        //Add key two elements
        if(keyTies.ContainsKey(keyTwo))
        {
            previousAmount = Mathf.Max(previousAmount, preset.GetProfileKeyCount(keyTies[keyTwo]));

            foreach (DataKey key in keyTies[keyTwo])
            {
                if (!newSet.Contains(key))
                {
                    newSet.Add(key);

                    if(!displayMessage)
                    {
                        if(preset.IsKeyUnique(key) || newSet.Exists(item => preset.IsKeyUnique(item)))
                        {
                            displayMessage = true;
                        }
                    }
                }

                //Photo or name is in first
                if (key == DataKey.photo)
                {
                    photoKey2 = true;
                }
                else if (key == DataKey.name)
                {
                    nameKey2 = true;
                }
            }
        }

        //This is the new set for all unique keys involved
        foreach(DataKey key in newSet)
        {
            if(preset.IsKeyUnique(key))
            {
                if (SessionData.Instance.startedGame) Game.Log("Evidence: Key merge " + name + "; " + key.ToString() + " and " + newSet.Count + " keys (unique)");
                keyTies[key] = newSet;
                uniqueKeyMerge = true;
            }
        }

        //Trigger in self when any data keys are merged
        OnDataKeyMerge(keyOne, keyTwo);

        //Detect photo to name tie
        //Both sets must have 1 but not both
        if ((photoKey1 && !nameKey1) || (!photoKey1 && nameKey1))
        {
            if ((photoKey2 && !nameKey2) || (!photoKey2 && nameKey2))
            {
                //One must have 1 and the other must have the other
                if ((photoKey1 && nameKey2) || (photoKey2 && nameKey1))
                {
                    NamePhotoMerge();
                }
            }
        }

        //Fire event
        if (OnDataKeyChange != null) OnDataKeyChange();

        InstancingCheck();

        //Notificaion
        if (SessionData.Instance.startedGame && uniqueKeyMerge && preset.notifyOfTies && displayMessage)
        {
            if (!InterfaceController.Instance.notificationQueue.Exists(item => item.keyMerge && item.keyMergeEvidence == this))
            {
                List<Evidence.DataKey> mergedKeys = GetTiedKeys(keyOne);
                int keysOutOfTotal = preset.GetProfileKeyCount(mergedKeys);

                mergedKeys = GetTiedKeys(keyTwo);
                keysOutOfTotal = Mathf.Max(keysOutOfTotal, preset.GetProfileKeyCount(mergedKeys));

                if (keysOutOfTotal > previousAmount)
                {
                    InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.keyMerge, previousAmount, Strings.Get("ui.gamemessage", "Linked Unique Information") + ": " + GetNameForDataKey(mergedKeys) + " (" + keysOutOfTotal + "/"+preset.GetValidProfileKeys().Count+")", InterfaceControls.Icon.lookingGlass, keyMergeEvidence: this, keyMergeKeys: mergedKeys);
                    SessionData.Instance.TutorialTrigger("citizens");
                }
            }
        }
    }

    //Detected a merge between name and photo keys
    public virtual void NamePhotoMerge()
    {

    }

    public virtual void OnDataKeyMerge(DataKey keyOne, DataKey keyTwo)
    {
        
    }

    public List<DataKey> GetTiedKeys(DataKey inputKey)
    {
        List<DataKey> keys = new DataKey[] { inputKey }.ToList();
        return GetTiedKeys(keys);
    }

    //Get a list of keys tied to these keys
    public virtual List<DataKey> GetTiedKeys(List<DataKey> inputKeys)
    {
        if (!preset.useDataKeys)
        {
            return Toolbox.Instance.allDataKeys;
        }

        List<DataKey> keys = new List<DataKey>();

        if (keyTies == null)
        {
            Game.LogError(name + " key ties is null for " + name);
            return keys;
        }

        //Check unique keys; if a unique key features any of the input keys, add this uique key to the input pool
        //List<Evidence.DataKey> uniqueKeys = preset.GetUniqueProfileKeys();

        //foreach (DataKey key in uniqueKeys)
        //{
        //    if (keyTies.ContainsKey(key) && !inputKeys.Contains(key))
        //    {
        //        List<DataKey> originalInput = new List<DataKey>(inputKeys);

        //        foreach(DataKey k in originalInput)
        //        {
        //            if(keyTies[key].Contains(k))
        //            {
        //                inputKeys.Add(key);
        //            }
        //        }
        //    }
        //}

        //Get passive key ties
        foreach (EvidencePreset.DataKeyAutomaticTies setting in preset.passiveTies)
        {
            if (inputKeys.Contains(setting.mainKey))
            {
                foreach (DataKey key in setting.mergeAtStart)
                {
                    if (!keys.Contains(key)) keys.Add(key);
                }
            }
        }

        for (int i = 0; i < inputKeys.Count; i++)
        {
            DataKey key = inputKeys[i];

            if (!keys.Contains(key)) keys.Add(key); //Make sure this features itself...

            if (keyTies.ContainsKey(key))
            {
                foreach (DataKey key2 in keyTies[key])
                {
                    if (!keys.Contains(key2))
                    {
                        keys.Add(key2);
                    }
                }
            }
        }

        return keys;
    }

    //Get a list of facts tied to one or more keys (any may be tied to others) : Do this for all data sources
    public List<FactLink> GetFactsForDataKey(DataKey inputKey)
    {
        List<DataKey> requestKeys = new List<DataKey>();
        requestKeys.Add(inputKey);

        return GetFactsForDataKey(requestKeys);
    }

    public List<FactLink> GetFactsForDataKey(List<DataKey> inputKeys)
    {
        List<DataKey> keys = GetTiedKeys(inputKeys);

        List<FactLink> ret = new List<FactLink>();

        if (inputKeys == null) return ret;

        //Loop tied keys (this will include itself)
        foreach (DataKey k in keys)
        {
            //Must contain a list
            if (factDictionary.ContainsKey(k))
            {
                foreach (FactLink link in factDictionary[k])
                {
                    if (!ret.Contains(link))
                    {
                        ret.Add(link);
                    }
                }
            }
        }

        return ret;
    }

    //Get name using one or more keys (any may be tied to others)
    public string GetNameForDataKey(DataKey inputKey)
    {
        List<DataKey> requestKeys = new List<DataKey>();
        requestKeys.Add(inputKey);

        return GetNameForDataKey(requestKeys);
    }

    public virtual string GetNameForDataKey(List<DataKey> inputKeys)
    {
        string ret = string.Empty;

        //This can fail for some reason... Workaround by wrapping in a try
        try
        {
            if (inputKeys == null) return Strings.Get("evidence.names", preset.name);

            List<DataKey> keys = GetTiedKeys(inputKeys);

            //Return custom names first
            if (customNames.Count > 0)
            {
                foreach (CustomName n in customNames)
                {
                    if(keys.Contains(n.key))
                    {
                        return n.name;
                    }
                }
            }

            //New code: This may need changing: Check against name key for input keys...
            List<DataKey> nameKeys = GetTiedKeys(DataKey.name);

            //Does the name key set contain one of the input keys?
            if (name != null && name.Length > 0 && (keys.Contains(DataKey.name) || nameKeys.Exists(item => inputKeys.Contains(item))))
            {
                return name;
            }
            else
            {
                //Evidence type name
                return Strings.Get("evidence.names", preset.name);
            }
        }
        catch
        {
            return ret;
        }
    }

    //Add a custom name
    public void AddOrSetCustomName(Evidence.DataKey dk, string newCustomName)
    {
        CustomName n = customNames.Find(item => item.key == dk);

        if (n == null)
        {
            n = new CustomName { key = dk };
            customNames.Add(n);
        }

        n.name = newCustomName;

        //Firing this will cause name related events to update
        if(OnDataKeyChange != null)
        {
            OnDataKeyChange();
        }
    }

    public void AddOrSetCustomName(List<Evidence.DataKey> dk, string newCustomName)
    {
        foreach (Evidence.DataKey d in dk)
        {
            CustomName n = customNames.Find(item => item.key == d);

            if (n == null)
            {
                n = new CustomName { key = d };
                customNames.Add(n);
            }

            n.name = newCustomName;
        }

        //Firing this will cause name related events to update
        if (OnDataKeyChange != null)
        {
            OnDataKeyChange();
        }
    }

    //Add discovery
    public void AddDiscovery(Discovery disc)
    {
        //Create key if needed
        if (!discoveryProgress.Contains(disc))
        {
            discoveryProgress.Add(disc);
        }

        if(SessionData.Instance.startedGame) Game.Log("Evidence: Add discovery " + disc.ToString() + " to " + name);

        //Check evidence for new discoveries
        UpdateDiscoveries();

        //Fire Event
        if (OnDiscoveryChanged != null)
        {
            OnDiscoveryChanged(disc);
        }
    }

    //Update Discoveries; find child evidence and facts connected to this evidence
    public virtual void UpdateDiscoveries()
    {
        //if(SessionData.Instance.startedGame) Game.Log("Evidence: Update discoveries for " + name + "...");
        //Child evidence
        for (int i = 0; i < children.Count; i++)
        {
            Evidence child = children[i];

            //Continue if found
            if (child.isFound) continue;

            //Loop discovery to test passes
            foreach (Discovery eds in child.discoveryProgress)
            {
                //This contains type
                if (preset.discoveryTriggers.Contains(eds))
                {
                    //Discover this!
                    child.SetFound(true);
                }
            }
        }

        //Facts
        for (int i = 0; i < allFacts.Count; i++)
        {
            FactLink fact = allFacts[i];

            //if (SessionData.Instance.startedGame) Game.Log("Evidence: Checking fact " + fact.fact.GetName() + "("+fact.fact.isFound+")");

            //Continue if found
            if (fact.fact.isFound) continue;

            //Loop discovery to test passes
            foreach (Discovery eds in discoveryProgress)
            {
                //This contains type
                if (fact.fact.preset.discoveryTriggers.Contains(eds))
                {
                    //Discover this!
                    fact.fact.SetFound(true);
                }
            }
        }
    }

    //Get icon for this evidence
    public virtual Sprite GetIcon()
    {
        if(interactablePreset != null)
        {
            return interactablePreset.iconOverride;
        }

        return preset.iconSpriteLarge;
    }

    //Get photo for this evidence
    public Texture2D GetPhoto(List<DataKey> keys)
    {
        if(preset.useInGamePhoto)
        {
            if(SessionData.Instance.startedGame)
            {
                try
                {
                    if(preset.useWriter && writer != null)
                    {
                        return SceneCapture.Instance.CapturePhotoOfEvidence(writer.evidenceEntry);
                    }
                    else if(keys.Contains(DataKey.photo))
                    {
                        return SceneCapture.Instance.CapturePhotoOfEvidence(this);
                    }
                    else if (interactablePreset != null && interactablePreset.staticImage != null)
                    {
                        return interactablePreset.staticImage.texture;
                    }
                }
                catch
                {
                    Game.LogError("Unable to capture photo for evidence " + name);
                }
            }
            //else return InterfaceControls.Instance.nullPhotoReference;
        }
        else if(preset.useSurveillanceCapture)
        {
            if (SessionData.Instance.startedGame)
            {
                EvidenceSurveillance cctv = this as EvidenceSurveillance;

                if (cctv != null)
                {
                    return SceneCapture.Instance.GetSurveillanceScene(cctv.savedCapture);
                }
            }
            //else return InterfaceControls.Instance.nullPhotoReference;
        }
        else
        {
            EvidenceFingerprint print = this as EvidenceFingerprint;

            if(print != null)
            {
                if(print.writer != null)
                {
                    return CitizenControls.Instance.prints[Toolbox.Instance.GetPsuedoRandomNumber(0, CitizenControls.Instance.prints.Count, print.writer.citizenName + print.writer.humanID)];
                }
                else
                {
                    Game.LogError("Missing writer for fingerprint " + evID);

                    if (interactablePreset != null && interactablePreset.staticImage != null)
                    {
                        return interactablePreset.staticImage.texture;
                    }
                }
            }
            else
            {
                if(interactablePreset != null && interactablePreset.staticImage != null)
                {
                    return interactablePreset.staticImage.texture;
                }
            }
        }

        return preset.defaultNullImage;
    }

    public virtual string GetSummary(List<DataKey> keys)
    {
        if(interactablePreset != null && interactablePreset.summaryMessageSource != null && interactablePreset.summaryMessageSource.Length > 0)
        {
            return Strings.GetTextForComponent(interactablePreset.summaryMessageSource, null, dataKeys: keys);
        }

        return "<Summary Missing>";
    }

    //Set note (data key)
    public void SetNote(List<DataKey> keys, string str)
    {
        //If this doesn't use keys, just store the note under the 'name' key
        if(!preset.useDataKeys)
        {
            if (!notes.ContainsKey(DataKey.name))
            {
                notes.Add(DataKey.name, string.Empty);
            }

            notes[DataKey.name] = str;
        }
        else
        {
            List<DataKey> tiedKeys = GetTiedKeys(keys);

            foreach (DataKey key in tiedKeys)
            {
                if (!notes.ContainsKey(key))
                {
                    notes.Add(key, string.Empty);
                }

                notes[key] = str;
            }
        }

        //Fire event
        if(OnNoteAdded != null)
        {
            OnNoteAdded();
        }
    }

    //Get note raw text
    public virtual string GetNote(List<Evidence.DataKey> keys)
    {
        string ret = string.Empty;

        List<string> getNotes = new List<string>();
        bool foundAny = false;

        //If this evidence doesn't use datakeys, then save time searching by just using the 'name' key.
        if (!preset.useDataKeys)
        {
            if (notes.ContainsKey(DataKey.name))
            {
                string foundNote = notes[DataKey.name];

                if (!getNotes.Contains(foundNote))
                {
                    getNotes.Add(foundNote);

                    //Leave a space when combining multiple notes
                    if (foundAny)
                    {
                        ret += " ";
                    }

                    ret += foundNote;
                    foundAny = true;
                }
            }
        }
        else
        {
            List<Evidence.DataKey> tiedKeys = GetTiedKeys(keys);

            //Search data keys
            foreach (DataKey key in tiedKeys)
            {
                if (notes.ContainsKey(key))
                {
                    string foundNote = notes[key];

                    if (!getNotes.Contains(foundNote))
                    {
                        getNotes.Add(foundNote);

                        //Leave a space when combining multiple notes
                        if (foundAny)
                        {
                            ret += " ";
                        }

                        ret += foundNote;
                        foundAny = true;
                    }
                }
            }
        }

        return ret;
    }

    //Get note with text composed with the data reference from this evidence
    public virtual string GetNoteComposed(List<Evidence.DataKey> keys, bool useLinks = true)
    {
        Strings.LinkSetting links = Strings.LinkSetting.forceNoLinks;
        if (useLinks) links = Strings.LinkSetting.forceLinks;
        return Strings.ComposeText(GetNote(keys), interactable, links, evidenceKeys: keys);
    }

    //Triggered when pinned/unpinned
    public void OnPinnedChange()
    {
        if (OnAnyPinnedChange != null) OnAnyPinnedChange();
    }

    //Triggered when within reading range & looked at by player (may be non applicable)
    public virtual void OnPlayerLookedAtWithinReadingRange()
    {
        return;
    }

    //Set the image (overrides that of the preset)
    public void SetImageOverride(Sprite newSprite)
    {
        imageOverride = newSprite;
    }

    //Check for instancing duplicates in case elements and windows
    public void InstancingCheck()
    {
        //First search for open windows for conflicts...
        //Shortlist of windows representing this evidence
        List<InfoWindow> evWindows = InterfaceController.Instance.activeWindows.FindAll(item => item.passedEvidence == this);

        //Update evidence keys for all windows
        if(evWindows.Count > 1)
        {
            //Keys need to be updated first...
            for (int i = 0; i < evWindows.Count; i++)
            {
                //Update evidence keys
                evWindows[i].UpdateEvidenceKeys();
            }

            Dictionary<Evidence.DataKey, List<InfoWindow>> duplicateWindows = new Dictionary<Evidence.DataKey, List<InfoWindow>>();

            //Do any of the active windows contain the same keys?
            for (int i = 0; i < evWindows.Count; i++)
            {
                InfoWindow window = evWindows[i];

                for (int u = 0; u < evWindows.Count; u++)
                {
                    InfoWindow other = evWindows[u];

                    if (window == other) continue; //Skip same window

                    for (int l = 0; l < window.evidenceKeys.Count; l++)
                    {
                        Evidence.DataKey key = window.evidenceKeys[l];
                        if (!preset.IsKeyUnique(key)) continue;

                        if (other.evidenceKeys.Contains(key))
                        {
                            //This key is present in both windows!
                            if (!duplicateWindows.ContainsKey(key)) duplicateWindows.Add(key, new List<InfoWindow>());

                            if (!duplicateWindows[key].Contains(window)) duplicateWindows[key].Add(window);
                            if (!duplicateWindows[key].Contains(other)) duplicateWindows[key].Add(other);
                        }
                    }
                }
            }

            if (duplicateWindows.Count > 0) Game.Log("Duplicate case windows found: " + duplicateWindows.Count);

            //Figure out which windows to close and pin the remaining one if needed
            foreach (KeyValuePair<Evidence.DataKey, List<InfoWindow>> pair in duplicateWindows)
            {
                Evidence.DataKey key = pair.Key;

                InfoWindow keepWindow = null;
                List<InfoWindow> removeWindows = new List<InfoWindow>();

                for (int i = 0; i < pair.Value.Count; i++)
                {
                    InfoWindow window = pair.Value[i];
                    if (window == null) continue;

                    if (keepWindow == null)
                    {
                        keepWindow = window;
                        continue;
                    }
                    else
                    {
                        removeWindows.Add(window);
                    }
                }

                //Remove windows
                for (int i = 0; i < removeWindows.Count; i++)
                {
                    InfoWindow window = removeWindows[i];
                    window.CloseWindow(false);
                }

                //Pin if needed
                if(keepWindow != null) keepWindow.InstanceUpdateComplete(); //Run refresh complete to update window
            }
        }

        //Update case elements
        if(!SessionData.Instance.isFloorEdit)
        {
            int allCases = CasePanelController.Instance.activeCases.Count + CasePanelController.Instance.archivedCases.Count;

            for (int k = 0; k < allCases; k++)
            {
                Case a = null;

                if(k < CasePanelController.Instance.activeCases.Count)
                {
                    a = CasePanelController.Instance.activeCases[k];
                }
                else
                {
                    a = CasePanelController.Instance.archivedCases[k - CasePanelController.Instance.activeCases.Count];
                }

                List<Case.CaseElement> caseElements = a.caseElements.FindAll(item => item.id == evID);

                if(caseElements.Count > 1)
                {
                    //Update keys for each first...
                    foreach(Case.CaseElement element in caseElements)
                    {
                        element.dk = GetTiedKeys(element.dk);
                    }

                    Dictionary<Evidence.DataKey, List<Case.CaseElement>> duplicateWindows = new Dictionary<Evidence.DataKey, List<Case.CaseElement>>();

                    //Do any of the active windows contain the same keys?
                    for (int i = 0; i < caseElements.Count; i++)
                    {
                        Case.CaseElement element = caseElements[i];

                        for (int u = 0; u < caseElements.Count; u++)
                        {
                            Case.CaseElement other = caseElements[u];

                            if (element == other) continue; //Skip same element

                            for (int l = 0; l < element.dk.Count; l++)
                            {
                                Evidence.DataKey key = element.dk[l];
                                if (!preset.IsKeyUnique(key)) continue;

                                if (other.dk.Contains(key))
                                {
                                    //This key is present in both windows!
                                    if (!duplicateWindows.ContainsKey(key)) duplicateWindows.Add(key, new List<Case.CaseElement>());

                                    if (!duplicateWindows[key].Contains(element)) duplicateWindows[key].Add(element);
                                    if (!duplicateWindows[key].Contains(other)) duplicateWindows[key].Add(other);
                                }
                            }
                        }
                    }

                    if(duplicateWindows.Count > 0) Game.Log("Duplicate case elements found: " + duplicateWindows.Count);

                    //Figure out which windows to close and pin the remaining one if needed
                    foreach (KeyValuePair<Evidence.DataKey, List<Case.CaseElement>> pair in duplicateWindows)
                    {
                        Evidence.DataKey key = pair.Key;

                        Case.CaseElement keepElement = null;
                        List<Case.CaseElement> removeElements = new List<Case.CaseElement>();

                        for (int i = 0; i < pair.Value.Count; i++)
                        {
                            Case.CaseElement element = pair.Value[i];
                            if (element == null) continue;

                            if (keepElement == null)
                            {
                                keepElement = element;
                                continue;
                            }
                            else
                            {
                                removeElements.Add(element);
                            }
                        }

                        //Remove windows
                        for (int i = 0; i < removeElements.Count; i++)
                        {
                            a.caseElements.Remove(removeElements[i]);
                        }
                    }

                    //If this is the active case then update
                    if (a == CasePanelController.Instance.activeCase)
                    {
                        CasePanelController.Instance.UpdatePinned();

                        //Also update current window pinned status
                        foreach(InfoWindow w in evWindows)
                        {
                            w.PinnedUpdateCheck();
                        }
                    }
                }
            }
        }
    }

    //Set force save state
    public void SetForceSave(bool val)
    {
        forceSave = val;
    }

    //Return merged discovery keys for this link
    public List<DataKey> GetMergedDiscoveryLinkKeysFor(Evidence linkEvidence, DataKey mustFeature)
    {
        List<DataKey> ret = new List<DataKey>();
        if (linkEvidence == null) return ret;

        foreach(EvidencePreset.MergeKeysSetup keyMerge in preset.keyMergeOnDiscovery)
        {
            Evidence sub = GetLinkForFact(keyMerge.link);

            if(linkEvidence == sub)
            {
                Game.Log("DDS: Link evidence " + linkEvidence.name + " matches (" + keyMerge.link.ToString() + ")");

                if(keyMerge.mergeKeys.Contains(mustFeature))
                {
                    ret.AddRange(keyMerge.mergeKeys);
                }
            }
            //else Game.Log("DDS: Link evidence " + linkEvidence.name + " does not match " + sub.name +" (" + keyMerge.link.ToString() + ")");
        }

        if(!ret.Contains(mustFeature))
        {
            ret.Add(mustFeature);
        }

        return ret;
    }
}


