using System;
using System.Collections.Generic;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;


namespace UnityStandardAssets.Utility
{
    [Serializable]
    public class CurveControlledBob
    {
        public float HorizontalBobRange = 0.33f;
        public float VerticalBobRange = 0.33f;
        public AnimationCurve Bobcurve = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(0.5f, 1f),
                                                            new Keyframe(1f, 0f), new Keyframe(1.5f, -1f),
                                                            new Keyframe(2f, 0f)); // sin curve for head bob
        public float VerticaltoHorizontalRatio = 1f;

        [System.NonSerialized]
        public float m_CyclePositionX;
        [System.NonSerialized]
        public float m_CyclePositionY;
        public float m_BobBaseInterval;
        [System.NonSerialized]
        private Vector3 m_OriginalCameraPosition;
        [System.NonSerialized]
        public float m_Time;


        public void Setup(Camera camera, float bobBaseInterval, Vector3 camOriginalPos)
        {
            m_BobBaseInterval = bobBaseInterval;
            m_OriginalCameraPosition = camOriginalPos;

            // get the length of the curve in time
            m_Time = Bobcurve[Bobcurve.length - 1].time;
        }
    }
}
