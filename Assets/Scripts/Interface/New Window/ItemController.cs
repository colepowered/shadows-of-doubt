﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

//Controls what's available to view for this window...
//Script pass 1
public class ItemController : MonoBehaviour
{
    //Item ID used to access the police evidence dictionary
    //public Evidence evidence;
    //public List<Evidence.DataKey> evidenceKeys = new List<Evidence.DataKey>();

    //References
    [System.NonSerialized]
    public InfoWindow parentWindow;

    //File/Detail/Location content
    public WindowContentController childEvContent;
    public List<ButtonController> spawnedChildEvButtons = new List<ButtonController>();

    //Facts content
    public WindowContentController factContent;
    public List<FactButtonController> spawnedFactButtons = new List<FactButtonController>();
    public ButtonController newCustomFactButton;

    public int unSeenFacts = 0;
    private int prevUnSeenFacts = -1;

    //Debug facts
    public List<string> debugFacts = new List<string>();

    //Events
    //Triggered when the number of unseen facts is updated
    public delegate void UpdateUnseenFacts(int val);
    public event UpdateUnseenFacts OnUpdateUnseenFacts;

    //Setup- run on creation
    public void Setup(InfoWindow newParent)
    {
        parentWindow = newParent;

        //Listen for required changes
        //Name changes are executed when data keys are changed
        parentWindow.passedEvidence.OnDataKeyChange += UpdateNameDisplay;

        //Children add/remove (only discovery is needed as this is triggered when added && discovered)
        //parentWindow.passedEvidence.OnDiscoverChild += UpdateChildEvidenceDisplay;
        //parentWindow.passedEvidence.OnRemoveChild += UpdateChildEvidenceDisplay;

        //Facts are updated when a new one is discovered or data keys are changed
        parentWindow.passedEvidence.OnDiscoverConnectedFact += UpdateFactsDisplay;
        parentWindow.passedEvidence.OnDataKeyChange += UpdateFactsDisplay;

        UpdateNameDisplay();
    }

    private void OnDestroy()
    {
        if (newCustomFactButton != null)
        {
            newCustomFactButton.OnPress -= NewCustomFactButton;
        }

        parentWindow.passedEvidence.OnDataKeyChange -= UpdateNameDisplay;
        //parentWindow.passedEvidence.OnDiscoverChild -= UpdateChildEvidenceDisplay;
        //parentWindow.passedEvidence.OnRemoveChild -= UpdateChildEvidenceDisplay;
        parentWindow.passedEvidence.OnDiscoverConnectedFact -= UpdateFactsDisplay;
        parentWindow.passedEvidence.OnDataKeyChange -= UpdateFactsDisplay;
    }

    //Update name
    public void UpdateNameDisplay()
    {
        //Set window name
        parentWindow.SetName(parentWindow.passedEvidence.GetNameForDataKey(parentWindow.evidenceKeys));
    }

    //Update facts list
    public void UpdateFactsDisplay()
    {
        if (factContent == null)
        {
            Game.Log("Interface: Fact content is null");
            return;
        }

        debugFacts.Add("Update fact display");
        Game.Log("Interface: Update fact display");

        if(newCustomFactButton == null)
        {
            GameObject newButton = Instantiate(PrefabControls.Instance.newCustomFactButton, factContent.gameObject.transform);
            newCustomFactButton = newButton.GetComponent<ButtonController>();
            newCustomFactButton.SetupReferences();
            newCustomFactButton.OnPress += NewCustomFactButton;
        }

        //Compile a list of file icons that should exist
        List<Evidence.FactLink> required = new List<Evidence.FactLink>();

        List<Evidence.FactLink> getKeys = parentWindow.passedEvidence.GetFactsForDataKey(parentWindow.evidenceKeys);

        foreach (Evidence.FactLink link in getKeys)
        {
            debugFacts.Add("Fact: " + link.fact.name + "...");

            if (link.fact.isFound)
            {
                debugFacts.Add("Fact: ... Is found");

                if (!required.Contains(link))
                {
                    bool duplicate = false;

                    foreach (Evidence.FactLink comp in required)
                    {
                        //Same fact
                        if (comp.fact == link.fact)
                        {
                            //Same 'this' evidence
                            if (comp.thisEvidence == link.thisEvidence)
                            {
                                //Same to evidence
                                bool containsAll = true;

                                foreach (Evidence dest in comp.destinationEvidence)
                                {
                                    if (!link.destinationEvidence.Contains(dest))
                                    {
                                        debugFacts.Add("Fact: ... Same to evidence!");
                                        containsAll = false;
                                        break;
                                    }
                                }

                                if (containsAll)
                                {
                                    debugFacts.Add("Fact: ... Duplicate!");
                                    duplicate = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (!duplicate)
                    {
                        debugFacts.Add("Fact: ... Added to required.");
                        required.Add(link);
                    }
                }
            }
        }

        //Go through existing list
        for (int i = 0; i < spawnedFactButtons.Count; i++)
        {
            FactButtonController fbc = spawnedFactButtons[i] as FactButtonController;

            //If a null entry, this has obviously been destroyed- remove entry
            if (fbc == null)
            {
                spawnedFactButtons.RemoveAt(i);
                i--;
                continue;
            }

            //If this is supposed to exist, remove from found files list
            int index = required.FindIndex(item => item.fact == fbc.fact);

            if (index > -1)
            {
                required.RemoveAt(index);
            }
            //If it's not found in the list, it's safe to remove
            else
            {
                if (!fbc.fact.isSeen) fbc.fact.OnSeen -= UpdateUnSeenFacts;
                Destroy(fbc.gameObject);
                spawnedFactButtons.RemoveAt(i);
                i--;
                continue;
            }
        }

        //The ones left in the found files list are buttons to spawn
        foreach (Evidence.FactLink fct in required)
        {
            GameObject newButton = Instantiate(PrefabControls.Instance.factButton, factContent.gameObject.transform);
            FactButtonController fbc = newButton.GetComponent<FactButtonController>();

            fbc.Setup(fct, parentWindow);
            spawnedFactButtons.Add(fbc);
            if(!fct.fact.isSeen) fct.fact.OnSeen += UpdateUnSeenFacts;
        }

        //Update positions
        PositionSpawnedFacts();

        //Update non-sceen
        UpdateUnSeenFacts();
    }

    //Position facts; Required special case as they can be paired
    public void PositionSpawnedFacts(float edgeMargin = 10f, float iconMargin = 6f)
    {
        if (factContent == null) return;

        float yPos = -edgeMargin;

        //ContentSize
        float height = 0f;

        spawnedFactButtons.Sort((p1, p2) => p2.fact.preset.factRank.CompareTo(p1.fact.preset.factRank)); //Using P2 first gives highest first

        //Position all buttons
        for (int i = 0; i < spawnedFactButtons.Count; ++i)
        {
            //If this is paired to another through a fact link, skip as the parent will sort out it's postion...
            //if (spawnedFactButtons[i].link.pairedTo != null) continue;

            //Get button's rect transform
            FactButtonController spawned = spawnedFactButtons[i];
            RectTransform b = spawned.rect;
            spawned.inSlot = false;

            b.anchoredPosition = new Vector2(b.anchoredPosition.x, yPos);

            if(spawned.toggleHiddenButton != null)
            {
                spawned.toggleHiddenButton.rect.anchoredPosition = new Vector2(-24, yPos);
            }

            //Update content size
            height = Mathf.Abs(yPos - b.sizeDelta.y - edgeMargin);
            yPos -= b.sizeDelta.y + iconMargin;
        }

        if(newCustomFactButton != null)
        {
            //Get button's rect transform
            RectTransform b = newCustomFactButton.rect;

            b.anchoredPosition = new Vector2(b.anchoredPosition.x, yPos);

            //Update content size
            height = Mathf.Abs(yPos - b.sizeDelta.y - edgeMargin);
            yPos -= b.sizeDelta.y + iconMargin;
        }

        //Set the content size to the area of the buttons
        RectTransform page = factContent.gameObject.transform.GetComponent<RectTransform>();
        page.sizeDelta = new Vector2(page.sizeDelta.x, Mathf.Max(page.sizeDelta.y, height));
        factContent.pageImg.gameObject.GetComponent<RectTransform>().sizeDelta = page.sizeDelta;

        factContent.normalSize = page.sizeDelta; //Update 'normal' size
        factContent.UpdateFitScale(); //Force update of fit scale

        //Cheat to scroll to top: Set the rect to -9999
        factContent.rect.anchoredPosition = new Vector2(factContent.rect.anchoredPosition.x, -99999);
    }

    //Count the number of 'non-seen' facts and update the tab's count
    public void UpdateUnSeenFacts()
    {
        unSeenFacts = 0;

        //Stop listening if seen
        foreach(FactButtonController fbc in spawnedFactButtons)
        {
            if (fbc.fact.isSeen) fbc.fact.OnSeen -= UpdateUnSeenFacts;
            else unSeenFacts++;
        }

        if(prevUnSeenFacts != unSeenFacts)
        {
            prevUnSeenFacts = unSeenFacts;

            //Fire event
            if (OnUpdateUnseenFacts != null) OnUpdateUnseenFacts(unSeenFacts);
        }
    }

    public void NewCustomFactButton(ButtonController thisButton)
    {
        if(CasePanelController.Instance.activeCase != null)
        {
            if(CasePanelController.Instance.activeCase.caseElements.Contains(parentWindow.currentPinnedCaseElement))
            {
                if(parentWindow.currentPinnedCaseElement.pinnedController != null)
                {
                    CasePanelController.Instance.CustomStringLinkSelection(parentWindow.currentPinnedCaseElement.pinnedController, false);
                }
            }
        }
    }
}
