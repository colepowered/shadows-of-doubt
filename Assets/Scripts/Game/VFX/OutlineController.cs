﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UI;

public class OutlineController : MonoBehaviour
{
    public int normalLayer = 24;
    public Actor actor;
    public List<MeshRenderer> meshesToOutline = new List<MeshRenderer>();
    public bool outlineActive = false;
    public bool isSetup = false;

    public void Setup()
    {
        outlineActive = false;
        isSetup = true;
    }

    public void SetOutlineActive(bool val)
    {
        outlineActive = val;

        foreach(MeshRenderer m in meshesToOutline)
        {
            if (m == null)
            {
                continue;
            }

            if(outlineActive)
            {
                m.gameObject.layer = 30;
            }
            else
            {
                m.gameObject.layer = normalLayer;
            }
        }

        //When outline becomes active, set visible
        if(outlineActive && !actor.visible)
        {
            actor.SetVisible(true);
        }
    }

    public void SetColor(Color newCol)
    {
        foreach(MeshRenderer rndr in meshesToOutline)
        {
            if (rndr == null) continue;

            var propertyBlock = new MaterialPropertyBlock();
            rndr.GetPropertyBlock(propertyBlock);

            propertyBlock.SetColor("_SelectionColor", newCol);

            rndr.SetPropertyBlock(propertyBlock);
        }
    }

    public void SetAlpha(float val)
    {
        foreach (MeshRenderer rndr in meshesToOutline)
        {
            if (rndr == null) continue;

            var propertyBlock = new MaterialPropertyBlock();
            rndr.GetPropertyBlock(propertyBlock);

            propertyBlock.SetFloat("_AlphaVal", val);

            rndr.SetPropertyBlock(propertyBlock);
        }
    }
}
