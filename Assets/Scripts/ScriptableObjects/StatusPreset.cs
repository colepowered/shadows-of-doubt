﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "status_data", menuName = "Database/Status Preset")]

public class StatusPreset : SoCustomComparison
{
	[Header("Interface")]
	public Color color = Color.white;
	public Color alternateColour = Color.white;
	public Sprite icon;
	public Sprite alternateIcon;
	[Tooltip("After creation, minimize this to an icon for the in-game UI")]
	public bool minimizeToIcon = true;
	[Tooltip("Glow when active")]
	public bool pulseBackground = false;
	public bool pulseIcon = true;
	public Color pulseIconAdditiveColour = Color.clear;
	[Tooltip("Include a description on the detail text")]
	public bool includeDescription = true;
	[Tooltip("Instancing rules for this")]
	public Grouping instancingType = Grouping.perPresetType;

	public enum Grouping { perPresetType, perCrimeLocation, perGuestPass };

	[Space(7)]
	[Tooltip("Automatically display a message when this is activated")]
	public bool autoNotificationMessage = false;

	[Range(0, 10)]
	[Tooltip("Where this appears on the right side menu hierarchy")]
	public int priority = 4;

	[Header("Progress")]
	[Tooltip("Fade to white the closer the amount is to 0")]
	public bool fadeToWhite = false;
	[Tooltip("Use progress bar")]
	public bool enableProgressBar = true;
	[EnableIf("enableProgressBar")]
	public ProgressBarTrack barTracking = ProgressBarTrack.none;

	public enum ProgressBarTrack { none, witnesses, wantedInBuilding, alarmTime, guestPassTime };

	[Header("Checking")]
	[Tooltip("Use the custom named method to check the status of this")]
	public bool useCustomMethod = false;

	[Header("Audio")]
	public AudioEvent onAcquire;
	public AudioEvent onRemove;

	[Header("Counts")]
	public StatusCountType countType = StatusCountType.none;
	[Tooltip("Override the base colour with the highest count's colour")]
	public bool overrrideColorWithCount = false;
	[Tooltip("Display the count number in the main text")]
	public bool displayCountCountsInMainText = true;
	[Tooltip("Replace description based on counts")]
	public bool replaceDescriptionBasedOnCounts = false;
	[Tooltip("Display the address at the end of the detail text")]
	public bool displayAddressInDetailText = false;
	[Tooltip("Display the building at the end of the detail text")]
	public bool displayBuildingInDetailText = false;
	[Tooltip("List counts in detail text")]
	public bool listCountsInDetailText = false;
	[Tooltip("Display the fine total in the main text")]
	public bool displayFineTotalInMainText = false;
	[Tooltip("Alert when new count is added")]
	public bool alertWhenNewCountIsAdded = false;
	[Tooltip("Display total fine when minimized")]
	public bool displayTotalFineWhenMinimized = false;
	public enum StatusCountType { none, crime};
	[ReorderableList]
	public List<StatusCountConfig> countConfig = new List<StatusCountConfig>();

	[System.Serializable]
	public class StatusCountConfig
	{
		public string name;
		public Sprite icon;
		public Color colour;
		public PenaltyRule penaltyRule = PenaltyRule.fixedValue;
		public float penalty = 0f;
		public AudioEvent onAcquire;
	}

	public enum PenaltyRule { fixedValue, percentageValue, objectValueMultiplied};

	[Header("Attribute Effects (Binary)")]
	public bool stopsRecovery = false;
	public bool stopsSprint = false;
	public bool stopsJump = false;

	[Header("Attribute Effects (Gradual)")]
	[Space(7)]
	public float recoveryRatePlusMP = 0f;
	public float maxHealthPlusMP = 0f;
	public float movementSpeedPlusMP = 0f;
	public float temperatureGainPlusMP = 0f;
	public float damageIncomingPlusMP = 0f;
	public float damageOutgoingPlusMP = 0f;
	[Space(7)]
	public float drunkControls = 0f;
	public float tripChanceWet = 0f;
	public float tripChanceDrunk = 0f;
	public float affectHeadBob = 0f;
	public AnimationCurve headBob;
	[Space(7)]
	public float drunkVision = 0f;
	public float shiverVision = 0f;
	public float drunkLensDistort = 0f;
	public float headacheVision = 0f;
	[Space(7)]
	public float bloomIntensityPlusMP = 0f;
	public float motionBlurPlusMP = 0f;
	public float chromaticAbberationAmount = 0f;
	public float vignetteAmount = 0f;
	public float expsosure = 0f;
	[Space(7)]
	public bool useChannelMixer = false;

	[Space(7)]
	[ShowIf("useChannelMixer")]
	[Range(-200, 200)]
	public int redR = 0;
	[ShowIf("useChannelMixer")]
	[Range(-200, 200)]
	public int redG = 0;
	[ShowIf("useChannelMixer")]
	[Range(-200, 200)]
	public int redB = 0;

	[Space(7)]
	[ShowIf("useChannelMixer")]
	[Range(-200, 200)]
	public int greenR = 0;
	[ShowIf("useChannelMixer")]
	[Range(-200, 200)]
	public int greenG = 0;
	[ShowIf("useChannelMixer")]
	[Range(-200, 200)]
	public int greenB = 0;

	[Space(7)]
	[ShowIf("useChannelMixer")]
	[Range(-200, 200)]
	public int blueR = 0;
	[ShowIf("useChannelMixer")]
	[Range(-200, 200)]
	public int blueG = 0;
	[ShowIf("useChannelMixer")]
	[Range(-200, 200)]
	public int blueB = 0;
}
