using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CruncherTimelineEntry : ComputerOSUIComponent
{
    public SurveillanceApp app;
    public Image img;
    public JuiceController juice;
    [System.NonSerialized]
    public SceneRecorder.SceneCapture sceneReference;

    public bool mousedOver = false;
    public bool flagged = false;

    public void Setup(SurveillanceApp newApp, SceneRecorder.SceneCapture newCap)
    {
        app = newApp;
        sceneReference = newCap;
    }

    public void SetMouseOver(bool val)
    {
        if(mousedOver != val)
        {
            mousedOver = val;
            VisualUpdate();
        }
    }

    public void SetFlagged(bool val)
    {
        if (flagged != val)
        {
            flagged = val;
            VisualUpdate();
        }
    }

    public void VisualUpdate()
    {
        if (mousedOver)
        {
            if (flagged)
            {
                img.color = app.timelineFlagColor;
                juice.elements[0].originalColour = app.timelineFlagColor;
                juice.pulsateColour = app.timelineMOColor;
                juice.Pulsate(true, false);
            }
            else
            {
                img.color = app.timelineColor;
                juice.elements[0].originalColour = app.timelineColor;
                juice.pulsateColour = app.timelineMOColor;
                juice.Pulsate(true, false);
            }
        }
        else
        {
            if (flagged)
            {
                juice.Pulsate(false, false);
                img.color = app.timelineFlagColor;
            }
            else
            {
                juice.Pulsate(false, false);
                img.color = app.timelineColor;
            }
        }
    }

    public override void OnLeftClick()
    {
        app.SetScene(sceneReference);
    }
}
