﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class FactsWindowTabController : MonoBehaviour
{
    public InfoWindow parentWindow;
    public Evidence evidence;
    public RectTransform rect;
    public RectTransform scrollRectRect;
    public RectTransform parentRect;
    public WindowContentController contentController;

    //Use this as the native size, and scale around it
    public Vector2 nativeSize = new Vector2(342, 152);
    public float fitScale = 1f;

    public void Setup(InfoWindow newWindow)
    {
        rect = this.gameObject.GetComponent<RectTransform>();
        parentRect = this.gameObject.transform.parent as RectTransform;
        nativeSize = rect.sizeDelta;

        parentWindow = newWindow;
        evidence = parentWindow.passedEvidence;

        //Listen for window resize
        parentWindow.OnResizedWindow += OnWindowResize;
       
        rect.sizeDelta = new Vector2(rect.sizeDelta.x, 42f);
    }

    //Update slot content
    public void UpdateSlotContent()
    {
        //Run update of position buttons
        parentWindow.item.PositionSpawnedFacts();
    }

    public void OnWindowResize()
    {
        if (!this.gameObject.activeInHierarchy) return;

        //Get the viewport size
        Vector2 viewportSize = new Vector2(parentRect.rect.width, parentRect.rect.height);

        //Get the content size
        float idealX = viewportSize.x / (nativeSize.x + (parentWindow.centringTollerance * 0.5f));
        float idealY = viewportSize.y / (nativeSize.y + (parentWindow.centringTollerance * 0.5f));

        //Choose the lowest so it fits onscreen
        fitScale = Mathf.RoundToInt( Mathf.Min(Mathf.Min(idealX, idealY), 1f) * 100f ) / 100f; //Fit scale to 2dp

        rect.localScale = new Vector3(fitScale, fitScale, 1f);

        //Set position
        rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, -4);

        //Relocate top of scrollrect
        if (scrollRectRect == null)
        {
            scrollRectRect = parentWindow.scrollRect.GetComponent<RectTransform>();
        }

        //Set the scroll rect to the Ysize plus double the margin (y position)
        scrollRectRect.offsetMax = new Vector2(scrollRectRect.offsetMax.x, (-rect.sizeDelta.y * fitScale) + (rect.anchoredPosition.y * 2));

        //Scale content
        if (contentController == null)
        {
            contentController = parentWindow.item.factContent;
        }

        contentController.rect.localScale = new Vector3(fitScale, fitScale, 1f);
    }
}
