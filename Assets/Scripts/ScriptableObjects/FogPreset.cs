﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "fog_data", menuName = "Database/Fog Settings")]

public class FogPreset : SoCustomComparison
{
    [Header("Lighting")]
    [Tooltip("Sun rises at this hour (90 degrees to terrain)")]
    public float sunRiseHour = 6.5f;
    [Tooltip("Sun sets at this hour (90 degrees to terrain)")]
    public float sunSetHour = 19.5f;
    [Tooltip("Sun intensity curve")]
    public AnimationCurve daytimeSunIntensityCurve = AnimationCurve.EaseInOut(1.0f, 1.0f, 0.0f, 0.0f);
    [Tooltip("Multiply the above curve by this")]
    public float sunIntensityBooster = 2.2f;
    [Tooltip("Morning sun Colour")]
    public Color morningSunColour = Color.red;
    [Tooltip("Midday sun Colour")]
    public Color middaySunColour = Color.white;
    [Tooltip("Evening sun Colour")]
    public Color eveningSunColour = Color.red;
    [Tooltip("Sun shadow strength curve")]
    public AnimationCurve sunShadowStrengthCurve = AnimationCurve.EaseInOut(1.0f, 1.0f, 0.0f, 0.0f);
    [Tooltip("Sun dimmer")]
    public AnimationCurve sunVolumetricDimmer;
    [Tooltip("Sun shadows dimmer")]
    public AnimationCurve sunVolumetricShadowDimmer;
    [Tooltip("Exterior Ambient curve")]
    public AnimationCurve exteriorAmbientIntensityCurve = AnimationCurve.EaseInOut(1.0f, 1.0f, 0.0f, 0.0f);
    [Tooltip("Multiply the above curve by this")]
    public float ambientExteriorBooster = 2.2f;
    [Tooltip("Exterior Ambient curve")]
    public AnimationCurve interiorAmbientIntensityCurve = AnimationCurve.EaseInOut(1.0f, 1.0f, 0.0f, 0.0f);
    [Tooltip("Multiply the above curve by this")]
    public float ambientInteriorBooster = 2.2f;

    [Header("Colouring")]
    [Tooltip("Skybox colour grades w/ fog colour settings")]
    [ReorderableList]
    public List<SessionData.SkyboxGradient> skyboxGradientGrading = new List<SessionData.SkyboxGradient>();
    [Range(0f, 1f)]
    public float skyColourMultiplier = 1f;
    [Range(0f, 1f)]
    public float fogColourMultiplier = 1f;
    [Range(0f, 1f)]
    public float ambientLightMultiplier = 1f;
    [Range(0f, 1f)]
    public float globalLightIntensityMultiplier = 1f;

    [Header("Fog")]
    [Tooltip("Fog distance ranges")]
    public Vector2 fogDistanceRange = new Vector2(10f, 85f);
    [Tooltip("Fog distance throughout the day")]
    public AnimationCurve fogDistanceCurve = AnimationCurve.EaseInOut(1.0f, 1.0f, 0.0f, 0.0f);
    public Vector2 maxFogDistanceRange = new Vector2(10f, 85f);
    [Tooltip("Max Fog distance throughout the day")]
    public AnimationCurve maxFogDistanceCurve = AnimationCurve.EaseInOut(1.0f, 1.0f, 0.0f, 0.0f);

    [Header("Skyline")]
    public AnimationCurve skylineEmissionCurve;
    [ColorUsageAttribute(true, true)]
    public Color skylineEmissionColor;

    [Header("Weather")]
    public AnimationCurve monthSnowChanceCurve;
    public AnimationCurve weatherExtremityCurve;
    public float thunderDelay = 8f;

    [Header("Temperature")]
    public AnimationCurve monthTempCurve;
    public AnimationCurve dayTempCurve;
    public float NoRainModifier = 0.8f;
    public float NoWindModifier = 0.7f;
    public float NoSnowModifier = 0.3f;
}
