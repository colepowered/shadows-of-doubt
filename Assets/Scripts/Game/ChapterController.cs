﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using NaughtyAttributes;
using UnityEngine.EventSystems;
//using System.Security.Policy;

public class ChapterController : MonoBehaviour
{
    public List<ChapterPreset> allChapters = new List<ChapterPreset>();

    [Header("Loaded")]
    public ChapterPreset loadedChapter;
    public Chapter chapterScript;
    public GameObject chapterObject;
    [ReadOnly]
    public int currentPart = -1;
    [ReadOnly]
    public string currentPartName = string.Empty;
    public bool loadFirstPartOnStart = true;

    //Events
    public delegate void NewPart(bool delay, bool teleport);
    public event NewPart OnNewPart;

    //Singleton pattern
    private static ChapterController _instance;
    public static ChapterController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        allChapters = AssetLoader.Instance.GetAllChapters();

        //Sort chapters by number
        allChapters.Sort((p1, p2) => p1.chapterNumber.CompareTo(p2.chapterNumber)); //Lowest first
    }

    public void LoadChapter(ChapterPreset newChapter, bool newLoadFirstPartOnStart)
    {
        Game.Log("CityGen: Load chapter #" + newChapter.chapterNumber);
        loadedChapter = newChapter;
        loadFirstPartOnStart = newLoadFirstPartOnStart;

        //Reset
        currentPart = -1;
        currentPartName = string.Empty;

        //Unload the previous script
        if(chapterObject != null)
        {
            Destroy(chapterObject);
        }

        //Add script as component
        if(loadedChapter.scriptObject != null)
        {
            chapterObject = Instantiate(loadedChapter.scriptObject, this.transform);
        }
    }

    //Load a new part
    public void LoadPart(int partNumber, bool teleportPlayer = false, bool delay = true)
    {
        if(currentPart != partNumber)
        {
            currentPart = partNumber;
            currentPartName = loadedChapter.partNames[currentPart];

            //Fire event
            if (OnNewPart != null)
            {
                OnNewPart(delay, teleportPlayer);
            }
        }
    }

    //Load a new part
    public void LoadPart(string chapterString)
    {
        if(loadedChapter == null)
        {
            Game.Log("Chapter: No chapter is loaded while trying to load part " + chapterString + "...");
        }

        int partNumber = loadedChapter.partNames.FindIndex(item => item == chapterString);

        if (partNumber > -1)
        {
            LoadPart(partNumber);
        }
    }

    //Immediate skip and teleport to chapter location.
    public void SkipToChapterPart(int newPart, bool teleport, bool delay)
    {
        if (loadedChapter != null)
        {
            Game.Log("Chapter: Skipping to chapter part " + newPart);

            //Enable all actions
            Player.Instance.ClearAllDisabledActions();

            //Load the new part
            LoadPart(newPart, teleport, delay);
        }
    }

    [Button]
    public void SkipToNextPart()
    {
        SkipToChapterPart(currentPart + 1, true, false);
    }

    public void ResetThis()
    {
        //Unload the previous script
        if (chapterObject != null)
        {
            Destroy(chapterObject);
        }

        loadedChapter = null;
        currentPart = -1;
        currentPartName = string.Empty;
    }
}
