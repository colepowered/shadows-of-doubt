﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveTooltip : MonoBehaviour
{
	public bool setupComplete = false;
	public TooltipController ttc;

    //Failsafe removal
    private void Update()
    {
        if(ttc != null && !ttc.isActiveAndEnabled)
        {
            ttc.ForceClose();
        }
        else if(ttc == null)
        {
            Destroy(this.gameObject);
        }
    }
}
