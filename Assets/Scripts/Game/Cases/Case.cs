﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Reflection;
using NaughtyAttributes;

[System.Serializable]
public class Case
{
    [Header("Serializable")]
    public string name = "New Case";
    public int id = 0;
    public static int assignCaseID = 1;
    public CaseType caseType = CaseType.custom;
    public CaseStatus caseStatus = CaseStatus.closable;
    public int jobReference = -1; //References a side job
    public string mainStoryChapter;
    public List<CaseElement> caseElements = new List<CaseElement>(); //Current case elements
    public List<StringColours> stringColours = new List<StringColours>(); //Altered string colours
    public List<string> hiddenConnections = new List<string>();
    public bool isActive = false;
    public bool handInValid = false;
    public bool isSolved = false;
    public float questionsRank = 0f;
    public float victimsRank = 0f;
    public CaseRank rank = CaseRank.unSolved;
    public List<Objective> currentActiveObjectives = new List<Objective>();
    public List<Objective> inactiveCurrentObjectives = new List<Objective>();
    public List<Objective> endedObjectives = new List<Objective>(); //Complete or cancelled objectives are here
    public List<ResolveQuestion> resolveQuestions = new List<ResolveQuestion>();
    public List<int> suspectsDetained = new List<int>();
    public List<int> handIn = new List<int>();

    [NonSerialized]
    public List<Objective> waitForObjectives = new List<Objective>(); //Don't save this as we want to populate it with loaded objective references
    [NonSerialized]
    public SideJob job;

    //handInNotCollected = player has not got a hand-in form
    //handInCollected = player has got the hand-in form
    //closable = the case is active but complete/can be closed
    //archived = the case is inactive
    //forced = null state, things must be set manually

    public enum CaseStatus { handInNotCollected, handInCollected, submitted, closable, archived, forced };

    public enum CaseType { mainStory, murder, sideJob, custom, retirement };

    public enum CaseRank { super, A, B, C, D, unSolved };

    [System.Serializable]
    public class CaseElement
    {
        public int caseID; //Case ID
        public string n; //Evidence name (mostly for debug)
        public string id; //Evidence ID reference
        public List<Evidence.DataKey> dk; //Data keys for this evidence window
        public Vector2 v; //Case board position
        public List<Evidence.DataKey> sdk; //The seen data keys for this evidence

        public bool ap; //Use autopin to find position when this is next spawned.

        public bool w; //Is window open?
        public Vector3 resPos; //Window restore local position
        public Vector2 resPiv; //Window restore pivot

        public bool co; //Crossed out
        public bool m; //Minimized
        public InterfaceControls.EvidenceColours color = InterfaceControls.EvidenceColours.red; //Colour

        //Non serialized
        [NonSerialized]
        public PinnedItemController pinnedController;

        public void SetColour(InterfaceControls.EvidenceColours newColour)
        {
            color = newColour;

            //Update pin colour of active windows
            foreach (InfoWindow w in InterfaceController.Instance.activeWindows)
            {
                if (w.currentPinnedCaseElement == this)
                {
                    w.UpdatePinColour();
                }
            }

            //Update pinned items
            foreach (PinnedItemController p in CasePanelController.Instance.spawnedPins)
            {
                if (p.caseElement == this)
                {
                    p.pinButtonController.UpdatePinColour();
                }
            }
        }
    }

    [System.Serializable]
    public class StringColours
    {
        public string fromEv;
        public List<string> toEv;
        public List<Evidence.DataKey> fromDK;
        public List<Evidence.DataKey> toDK;
        public int colIndex = 0;
    }

    [System.Serializable]
    public class ResolveQuestion
    {
        [HorizontalLine(color: EColor.Gray)]
        [Header("Setup")]
        public string name; //Reference to the question string
        public bool displayObjective = true;
        [EnableIf("displayObjective")]
        public bool displayOnlyAtPhase = false;
        [EnableIf("displayOnlyAtPhase")]
        public int displayAtPhase = 0;
        public float objectiveDelay = 0f;
        public List<SideMissionIntroPreset> onlyCompatibleWithIntros = new List<SideMissionIntroPreset>();
        public List<SideMissionHandInPreset> onlyCompatibleWithHandIns = new List<SideMissionHandInPreset>();
        public InputType inputType; //Input type
        [Tooltip("A list of automatically set answers")]
        public List<AutoCorrectAnswer> automaticAnswers = new List<AutoCorrectAnswer>();
        public JobPreset.JobTag tag;
        public InterfaceControls.Icon icon = InterfaceControls.Icon.resolve;
        public Vector2 rewardRange;
        public Vector2 penaltyRange;
        public bool isOptional = false; //Is this optonal?
        [HorizontalLine(color: EColor.Gray)]

        [Header("Revenge Objective")]
        public bool useAlternateName = false;
        public RevengeObjectiveName useName = RevengeObjectiveName.IDTarget;
        public JobPreset.LeadCitizen target;
        public JobPreset.JobSpawnWhere location;
        [ReadOnly]
        public string revengeObjective; //Revenge objective reference
        [ReadOnly]
        public int revengeObjTarget = -1; //Revenge objective target
        [ReadOnly]
        public int revengeObjLoc = -1; //Revenge objective location
        [ReadOnly]
        public float revengeObjPassed = 0;
        [ReadOnly]
        public bool completedRevenge = false; //Once a revenge objective is completed it's never uncompleted

        [Header("Inputted")]
        [ReadOnly]
        public string input = "..."; //Manual user input (saved)
        [ReadOnly]
        public string inputtedEvidence = string.Empty; //Holds evidence ID string of the input

        [Header("State")]
        [ReadOnly]
        public List<string> correctAnswers = new List<string>(); //IDs of the correct answers
        //Runtime values
        [ReadOnly]
        public float progress = 0f; //Progress on objective based things
        [ReadOnly]
        public int reward; //Reward for getting this right
        [ReadOnly]
        public int penalty; //Cost of getting this wrong
        [ReadOnly]
        public bool isValid = false; //Is the current input valid?
        [ReadOnly]
        public bool isCorrect = false; //Is this a correct answer?

        [NonSerialized]
        public InputFieldController inputField;

        //Events
        public delegate void ProgressChange(ResolveQuestion resolve);
        public event ProgressChange OnProgressChange;

        public bool UpdateCorrect(Case forCase, bool isMainStory = true)
        {
            string debugClass = "Jobs: ";
            if (isMainStory) debugClass = "Chapter: ";

            if (!isValid)
            {
                Game.Log(debugClass + "Question " + name + " is incorrect: Invalid input (" + input + ")");
                isCorrect = false;
                return isCorrect;
            }

            if (inputType == Case.InputType.citizen)
            {
                Citizen cit = CityData.Instance.citizenDirectory.Find(item => item.GetCitizenName().ToLower() == input.ToLower());

                if (cit != null && correctAnswers.Contains(cit.humanID.ToString()))
                {
                    Game.Log(debugClass + "Question " + name + " is correct: " + input);
                    isCorrect = true;
                    return isCorrect;
                }
                else
                {
                    string correct = string.Empty;

                    foreach (string str in correctAnswers)
                    {
                        Human correctH = null;
                        int i = -1;
                        int.TryParse(str, out i);

                        if (CityData.Instance.GetHuman(i, out correctH))
                        {
                            correct += correctH.GetCitizenName() + " (" + i.ToString() + "), ";
                        }
                    }

                    Game.Log(debugClass + "Question " + name + " is incorrect. Input: " + input + ", correct answer(s): " + correct);
                    isCorrect = false;
                    return isCorrect;
                }
            }
            else if (inputType == Case.InputType.location)
            {
                List<NewGameLocation> inputLocations = CityData.Instance.gameLocationDirectory.FindAll(item => item.name.ToLower() == input.ToLower());

                if (inputLocations.Count > 0)
                {
                    foreach (NewGameLocation gl in inputLocations)
                    {
                        if (gl.thisAsAddress != null && correctAnswers.Contains(gl.thisAsAddress.id.ToString()))
                        {
                            Game.Log(debugClass + "Question " + name + " is correct: " + input);
                            isCorrect = true;
                            return isCorrect;
                        }
                        else if (gl.thisAsStreet != null && correctAnswers.Contains(gl.thisAsStreet.streetID.ToString()))
                        {
                            Game.Log(debugClass + "Question " + name + " is correct: " + input);
                            isCorrect = true;
                            return isCorrect;
                        }
                    }
                }
                else
                {
                    Game.Log(debugClass + "Question " + name + " is incorrect. Input: " + input + ", no matching locations...");
                    isCorrect = false;
                    return isCorrect;
                }
            }
            else if (inputType == Case.InputType.item)
            {
                //Parse from inputted object field
                if (correctAnswers.Contains(inputtedEvidence))
                {
                    Game.Log(debugClass + "Question " + name + " is correct: " + input + "(" + inputtedEvidence + ")");
                    isCorrect = true;
                    return isCorrect;
                }
                //Interactable inter = CityData.Instance.interactableDirectory.Find(item => item.id == inputtedEvidence);

                //if (inter != null && correctAnswers.Contains(inter.id))
                //{
                //    Game.Log(debugClass + "Question " + name + " is correct: " + input);
                //    return true;
                //}
                else
                {
                    string correct = string.Empty;

                    foreach (string str in correctAnswers)
                    {
                        correct += str + ", ";
                    }

                    Game.Log(debugClass + "Question " + name + " is incorrect. Input: " + input + " (" + inputtedEvidence + "), correct answer(s): " + correct);
                    isCorrect = false;
                    return isCorrect;
                }
            }
            else if (inputType == InputType.revengeObjective)
            {
                if (revengeObjective != null && revengeObjective.Length > 0)
                {
                    if (completedRevenge)
                    {
                        Game.Log(debugClass + " Previously completed revenge objective");
                        isCorrect = true;
                        return isCorrect;
                    }

                    RevengeObjective rev = null;
                    Toolbox.Instance.LoadDataFromResources<RevengeObjective>(revengeObjective, out rev);

                    if (rev != null)
                    {
                        //Create method info
                        MethodInfo method = rev.GetType().GetMethod(rev.answerMethod);

                        if (method != null)
                        {
                            object[] passed = { revengeObjTarget, revengeObjLoc, revengeObjPassed };
                            object ret = method.Invoke(rev, passed);

                            //The returned should be a bool
                            float amount = 0f;

                            if (float.TryParse(ret.ToString(), out amount))
                            {
                                Game.Log(debugClass + " Managed to parse float from " + ret.ToString());

                                if (amount >= revengeObjPassed)
                                {
                                    Game.Log(debugClass + "Question " + name + " is correct");
                                    completedRevenge = true; //Set this so it won't need to be checked again...
                                    isCorrect = true;
                                    return isCorrect;
                                }
                                else
                                {
                                    Game.Log(debugClass + "Question " + name + " is incorrect.");
                                    isCorrect = false;
                                    return isCorrect;
                                }
                            }
                            else
                            {
                                bool pass = false;

                                if (bool.TryParse(ret.ToString(), out pass))
                                {
                                    Game.Log(debugClass + " Managed to parse bool from " + ret.ToString());

                                    if (pass)
                                    {
                                        Game.Log(debugClass + "Question " + name + " is correct");
                                        completedRevenge = true; //Set this so it won't need to be checked again...
                                        isCorrect = true;
                                        return isCorrect;
                                    }
                                    else
                                    {
                                        Game.Log(debugClass + "Question " + name + " is incorrect.");
                                        isCorrect = false;
                                        return isCorrect;
                                    }
                                }
                                else Game.Log("Misc Error: Unable to parse bool from invokation of method " + rev.answerMethod + " for resolve question " + name);
                            }
                        }
                        else Game.Log("Misc Error: Unable to parse answer method " + rev.answerMethod + " for resolve question " + name);
                    }
                    else Game.Log("Misc Error: No revenge objective for resolve question " + name);
                }
            }
            else if (inputType == InputType.objective)
            {
                if (revengeObjective != null && revengeObjective.Length > 0)
                {
                    if (forCase != null && forCase.job != null)
                    {
                        List<Objective> obj = null;

                        if(forCase.job.objectiveReference.TryGetValue(revengeObjective, out obj))
                        {
                            foreach(Objective o in obj)
                            {
                                if(o.isComplete)
                                {
                                    isCorrect = true;
                                    return isCorrect;
                                }
                            }

                            return isCorrect;
                        }
                        else
                        {
                            Game.Log(debugClass + " Could not find objective " + revengeObjective + " in job objective's reference...");
                            isCorrect = false;
                            return isCorrect;
                        }
                    }
                    else Game.Log(debugClass + " Unable to find case");

                    isCorrect = false;
                    return isCorrect;
                }
                else
                {
                    Game.Log(debugClass + " Objective reference is null!");
                    isCorrect = false;
                    return isCorrect;
                }
            }
            else if (inputType == InputType.arrestPurp)
            {
                //Use revenge objective field to find the ID question which needs to be answered to find this...
                if (revengeObjective != null && revengeObjective.Length > 0)
                {
                    if (forCase != null)
                    {
                        ResolveQuestion getQ = forCase.resolveQuestions.Find(item => item.name.ToLower() == revengeObjective.ToLower());

                        if (getQ != null)
                        {
                            foreach(string correct in getQ.correctAnswers)
                            {
                                Human correctH = null;
                                int i = -1;
                                int.TryParse(correct, out i);

                                if (CityData.Instance.GetHuman(i, out correctH))
                                {
                                    if(correctH != null && correctH.ai != null && correctH.ai.restrained)
                                    {
                                        Game.Log(debugClass + " Found correct citizen is restrained (" + correctH.citizenName + ")");
                                        isCorrect = true;
                                        return isCorrect;
                                    }
                                }
                            }

                            //Citizen cit = CityData.Instance.citizenDirectory.Find(item => item.GetCitizenName().ToLower() == getQ.input.ToLower());
                            
                            //if(cit != null && cit.ai != null && cit.ai.restrained)
                            //{
                            //    isCorrect = true;
                            //    return isCorrect;
                            //}
                        }
                        else
                        {
                            Game.Log(debugClass + " Could not find resolve question " + revengeObjective + " to know if suspect is arrested...");
                            isCorrect = false;
                            return isCorrect;
                        }
                    }
                    else Game.Log(debugClass + " Unable to find case");

                    isCorrect = false;
                    return isCorrect;
                }
                //Otherwise assume the purp has been IDd already...
                else if(forCase.job != null)
                {
                    if (forCase.job.purp != null && forCase.job.purp.ai != null && forCase.job.purp.ai.restrained)
                    {
                        isCorrect = true;
                        return isCorrect;
                    }
                }
            }

            Game.Log(debugClass + "Question " + name + " is incorrect.");
            isCorrect = false;
            return isCorrect;
        }

        public bool UpdateValid(Case forCase)
        {
            //Game.Log("Checking validity for " + name + "...");

            if (displayOnlyAtPhase)
            {
                if (forCase != null && forCase.job != null)
                {
                    if (forCase.job.phase < displayAtPhase)
                    {
                        isValid = false; //Not yet valid as we don't display this Q at this phase...
                        SetProgress(0f);
                        return isValid;
                    }
                }
            }

            if (inputType == Case.InputType.revengeObjective)
            {
                if (revengeObjective != null && revengeObjective.Length > 0)
                {
                    if (completedRevenge)
                    {
                        isValid = true;
                    }
                    else
                    {
                        RevengeObjective rev = null;
                        Toolbox.Instance.LoadDataFromResources<RevengeObjective>(revengeObjective, out rev);

                        if (rev != null)
                        {
                            //Create method info
                            MethodInfo method = rev.GetType().GetMethod(rev.answerMethod);

                            if (method != null)
                            {
                                //When updating validity, we need to have the inputted data instead of the actual correct data...
                                NewGameLocation thisLocation = forCase.GetGameLocationFromQuestionInput(this);
                                int locID = -1;

                                Human thisTarget = forCase.GetCitizenFromQuestionInput(this);
                                int tarID = -1;

                                if(thisLocation != null && thisLocation.thisAsAddress != null)
                                {
                                    locID = thisLocation.thisAsAddress.id;
                                }

                                if (thisTarget != null) tarID = thisTarget.humanID;

                                object[] passed = { tarID, locID, revengeObjPassed };
                                object ret = method.Invoke(rev, passed);

                                //The returned should be a bool
                                float amount = 0f;

                                if (float.TryParse(ret.ToString(), out amount))
                                {
                                    Game.Log("Jobs: Managed to parse float from " + ret.ToString());

                                    if (amount >= revengeObjPassed)
                                    {
                                        Game.Log("Question " + name + " is valid (" + amount + "/" + revengeObjPassed + ")");
                                        //completedRevenge = true; //Set this so it won't need to be checked again...
                                        isValid = true;
                                    }
                                    else
                                    {
                                        Game.Log("Question " + name + " is invalid (" + amount + "/" + revengeObjPassed + ")");
                                        isValid = false;
                                    }
                                }
                                else
                                {
                                    bool pass = false;

                                    if (bool.TryParse(ret.ToString(), out pass))
                                    {
                                        Game.Log("Managed to parse bool from " + ret.ToString());

                                        if (pass)
                                        {
                                            Game.Log("Question " + name + " is valid");
                                            //completedRevenge = true; //Set this so it won't need to be checked again...
                                            isValid = true;
                                        }
                                        else
                                        {
                                            Game.Log("Question " + name + " is invalid.");
                                            isValid = false;
                                        }
                                    }
                                    else Game.Log("Misc Error: Unable to parse bool from invokation of method " + rev.answerMethod + " for resolve question " + name);
                                }
                            }
                            else Game.Log("Misc Error: Unable to parse answer method " + rev.answerMethod + " for resolve question " + name);
                        }
                        else Game.Log("Misc Error: No revenge objective for resolve question " + name);
                    }
                }
            }
            else if (inputType == Case.InputType.objective)
            {
                isValid = false;

                if (revengeObjective != null && revengeObjective.Length > 0 && forCase != null)
                {
                    List<Objective> obj = null;

                    if (forCase.job.objectiveReference.TryGetValue(revengeObjective, out obj))
                    {
                        foreach(Objective o in obj)
                        {
                            Game.Log("Jobs: objective " + o.queueElement.entryRef + " is complete: " + o.isComplete + " is cancelled: " + o.isCancelled);

                            foreach (Objective.ObjectiveTrigger tri in o.queueElement.triggers)
                            {
                                Game.Log("Jobs: ... Trigger: " + tri.triggerType.ToString() + ": " + tri.interactableID + " = " + tri.progressAdd);
                            }

                            if (o.isComplete)
                            {
                                isValid = true;
                            }
                        }

                        //question.isCorrect = obj.isComplete;
                    }
                    else
                    {
                        Game.Log("Jobs: Could not find objective " + revengeObjective + " in job objective's reference (job " + forCase.job.jobID + ", ref count: " + forCase.job.objectiveReference.Count + ")");
                    }
                }
            }
            else if (inputType == Case.InputType.arrestPurp)
            {
                isValid = false;

                if (forCase != null)
                {
                    //Use revenge objective field to find the ID question which needs to be answered to find this...
                    if (revengeObjective != null && revengeObjective.Length > 0)
                    {
                        isValid = false;

                        Case.ResolveQuestion getQ = forCase.resolveQuestions.Find(item => item.name.ToLower() == revengeObjective.ToLower());

                        if (getQ != null)
                        {
                            Citizen cit = CityData.Instance.citizenDirectory.Find(item => item.GetCitizenName().ToLower() == getQ.input.ToLower());

                            if (cit != null && cit.ai != null && cit.ai.restrained)
                            {
                                Game.Log("Purp arrest is valid: " + cit.GetCitizenName());
                                isValid = true;
                            }
                            else
                            {
                                Game.Log("Purp arrest is invalid: " + getQ.input);
                            }
                        }
                        else
                        {
                            Game.Log("Could not find resolve question " + revengeObjective + " to know if suspect is arrested...");
                        }
                    }
                    //Otherwise assume the purp has been IDd already...
                    else if(forCase.job != null)
                    {
                        if (forCase.job.purp != null && forCase.job.purp.ai != null && forCase.job.purp.ai.restrained)
                        {
                            isValid = true;
                            //question.isCorrect = true;
                        }
                    }
                    else
                    {
                        Game.Log("Could not find resolve question " + revengeObjective + " to know if suspect is arrested...");
                    }
                }
            }
            else
            {
                isValid = false;

                //Validate input
                if (input != null && input.Length > 0)
                {
                    if (inputType == Case.InputType.citizen)
                    {
                        //Must contain first and surname
                        string[] split = input.Split(new char[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries);
                        if (split.Length >= 2) isValid = true;
                        else isValid = false;
                    }
                    else if (inputType == Case.InputType.location)
                    {
                        if (CityData.Instance.gameLocationDirectory.Exists(item => item.name.ToLower() == input.ToLower()))
                        {
                            isValid = true;
                        }
                    }
                    else if (inputType == Case.InputType.item)
                    {
                        if (inputtedEvidence != null && inputtedEvidence.Length > 0)
                        {
                            if (GameplayController.Instance.evidenceDictionary.ContainsKey(inputtedEvidence))
                            {
                                Game.Log("Jobs: Found valid item input for evidence; " + inputtedEvidence);
                                isValid = true;
                            }
                            else
                            {
                                int iID = 0;

                                if(int.TryParse(inputtedEvidence, out iID))
                                {
                                    if (CityData.Instance.savableInteractableDictionary.TryGetValue(iID, out _))
                                    {
                                        Game.Log("Jobs: Found valid item input for interactable; " + inputtedEvidence);
                                        isValid = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(inputType != InputType.revengeObjective)
            {
                if (isValid)
                {
                    SetProgress(1f);
                }
                else SetProgress(0f);
            }
            else
            {
                if(!isValid)
                {
                    SetProgress(0f);
                }
            }

            //Update the checkbox at this point
            if(inputField != null)
            {
                inputField.UpdateCheckbox();
            }

            return isValid;
        }

        public string GetText(Case belongsToCase, bool includeReward = true, bool includePenalty = true)
        {
            string ret = string.Empty;

            if (belongsToCase != null && belongsToCase.job != null)
            {
                ret = Strings.ComposeText(Strings.Get("missions.postings", name), belongsToCase.job, Strings.LinkSetting.forceLinks); //Set question text
            }
            else
            {
                ret = Strings.Get("missions.postings", name); //Set question text
            }

            if (isOptional)
            {
                ret += " (" + Strings.Get("missions.postings", "Optional") + ")"; //Set question text
            }

            if (includeReward) ret += "\n<size=70%>" + Strings.Get("missions.postings", "Reward") + ": " + CityControls.Instance.cityCurrency + reward;

            if (penalty != 0 && includePenalty)
            {
                ret += " <color=#d70a0a>" + Strings.Get("missions.postings", "Penalty") + ": " + CityControls.Instance.cityCurrency + penalty;
            }

            return ret;
        }

        public RevengeObjective GetRevengeObjective()
        {
            RevengeObjective ret = null;
            Toolbox.Instance.LoadDataFromResources<RevengeObjective>(revengeObjective, out ret);
            return ret;
        }

        public void SetProgress(float val, bool forceTrigger = false)
        {
            if(progress != val || forceTrigger)
            {
                progress = Mathf.Clamp01(val);
                Game.Log("Setting question " + name + " progress to " + val);

                //Fire event
                if (OnProgressChange != null)
                {
                    OnProgressChange(this);
                }
            }
        }
    }

    public enum RevengeObjectiveName {D0, D1, IDTarget };

    public enum AutoCorrectAnswer { none, poster, purp, purpsParamour, posterHome, purpHome, purpsParamourHome, posterWork, purpWork, purpsParamourWork, posterPhoto, purpPhoto, purpsParamourPhoto, posterHomePhoto, purpHomePhoto, purpsParamourHomePhoto, posterWorkPhoto, purpWorkPhoto, purpsParamourWorkPhoto, spawnedItemA, spawnedItemB, spawnedItemC, spawnedItemD, spawnedItemE, spawnedItemF, spawnedItemTag };

    public enum InputType { citizen, location, item, revengeObjective, objective, arrestPurp };

    public void AddNewStringColour(Evidence.FactLink link, InterfaceControls.EvidenceColours col)
    {
        bool foundDuplicate = false;

        //Search for an existing entry that matches this...
        for (int i = 0; i < stringColours.Count; i++)
        {
            StringColours sc = stringColours[i];

            if(sc.fromEv == link.thisEvidence.evID)
            {
                bool toEvMatch = true;

                foreach(Evidence ev in link.destinationEvidence)
                {
                    if(!sc.toEv.Contains(ev.evID))
                    {
                        toEvMatch = false;
                        break;
                    }
                }

                if(toEvMatch)
                {
                    bool keysPass = false;

                    foreach(Evidence.DataKey dk in link.thisKeys)
                    {
                        if(sc.fromDK.Contains(dk))
                        {
                            keysPass = true;
                            break;
                        }
                    }

                    if(keysPass)
                    {
                        keysPass = false;

                        foreach (Evidence.DataKey dk in link.destinationKeys)
                        {
                            if (sc.toDK.Contains(dk))
                            {
                                keysPass = true;
                                break;
                            }
                        }

                        //We've found a duplicate!
                        if(keysPass)
                        {
                            Game.Log("Interface: Existing string colour found, changing to " + col);
                            foundDuplicate = true;

                            //Change the colour
                            sc.colIndex = (int)col;

                            //If it's red, then remove
                            if(col == InterfaceControls.EvidenceColours.red)
                            {
                                stringColours.RemoveAt(i);
                                i--;
                                continue;
                            }
                        }
                    }
                }
            }
        }

        //No existing found...
        if(!foundDuplicate)
        {
            StringColours newColours = new StringColours();
            newColours.fromEv = link.thisEvidence.evID;
            newColours.toEv = new List<string>();

            foreach (Evidence ev in link.destinationEvidence)
            {
                newColours.toEv.Add(ev.evID);
            }

            newColours.fromDK = link.thisKeys;
            newColours.toDK = link.destinationKeys;
            newColours.colIndex = (int)col;

            stringColours.Add(newColours);

            Game.Log("Interface: Created a new string colour: " + col);
        }
    }

    public void SetHidden(Fact fact, bool val)
    {
        string identifier = fact.GetIdentifier();

        if (hiddenConnections.Contains(identifier) && !val)
        {
            Game.Log("Interface: Removing hidden connection: " + identifier);
            hiddenConnections.Remove(identifier);
        }
        else if (!hiddenConnections.Contains(identifier) && val)
        {
            Game.Log("Interface: Adding hidden connection: " + identifier);
            hiddenConnections.Add(identifier);
        }

        //Update strings
        List<StringController> str = CasePanelController.Instance.spawnedStrings.FindAll(item => item.connection.facts.Contains(fact));

        foreach(StringController s in str)
        {
            s.UpdateHidden();
        }

        foreach(InfoWindow w in InterfaceController.Instance.activeWindows)
        {
            if(w.item != null)
            {
                List<FactButtonController> fb = w.item.spawnedFactButtons.FindAll(item => item.fact == fact);

                foreach(FactButtonController f in fb)
                {
                    f.VisualUpdate();
                }
            }
        }
    }

    public void ToggleHidden(Fact fact)
    {
        string identifier = fact.GetIdentifier();
        Game.Log("Interface: Toggle hidden connection: " + identifier);

        if (hiddenConnections.Contains(identifier))
        {
            Game.Log("Interface: Remove hidden connection: " + identifier);
            hiddenConnections.Remove(identifier);
        }
        else
        {
            Game.Log("Interface: Add hidden connection: " + identifier);
            hiddenConnections.Add(identifier);
        }

        //Update strings
        List<StringController> str = CasePanelController.Instance.spawnedStrings.FindAll(item => item.connection.facts.Contains(fact));

        foreach (StringController s in str)
        {
            s.UpdateHidden();
        }

        foreach (InfoWindow w in InterfaceController.Instance.activeWindows)
        {
            if (w.item != null)
            {
                List<FactButtonController> fb = w.item.spawnedFactButtons.FindAll(item => item.fact == fact);

                foreach (FactButtonController f in fb)
                {
                    f.VisualUpdate();
                }
            }
        }
    }

    public void SetStatus(CaseStatus newStatus, bool cancelObjectives = true)
    {
        caseStatus = newStatus;

        //Create hand in collection objective...
        if(caseStatus == CaseStatus.handInNotCollected)
        {
            if(caseType == CaseType.mainStory || caseType == CaseType.murder || caseType == CaseType.retirement)
            {
                Objective.ObjectiveTrigger collectHandIn = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.collectHandIn, "Collect Hand In", newProgressAdd: 1f);

                NewBuilding cityHall = CityData.Instance.buildingDirectory.Find(item => item.preset == RoutineControls.Instance.cityHall);
                Interactable handInTray = Toolbox.Instance.FindNearestWithAction(RoutineControls.Instance.getHandIn, cityHall.mainEntrance.node.room, Player.Instance, AIActionPreset.FindSetting.allAreas, overrideWithHome: false, restrictToBuilding: cityHall);
                Vector3 pointerPos = Vector3.zero;

                if (handInTray != null)
                {
                    pointerPos = handInTray.GetWorldPosition();
                }
                else
                {
                    Game.LogError("Could not find case hand in tray object!");
                }

                if(caseType == CaseType.retirement)
                {
                    AddObjective("Collect Retirement Form", collectHandIn, true, pointerPos, InterfaceControls.Icon.hand, onCompleteAction: Objective.OnCompleteAction.nothing, forceBottomOfList: true);
                }
                else AddObjective("Collect Hand In", collectHandIn, true, pointerPos, InterfaceControls.Icon.hand, onCompleteAction: Objective.OnCompleteAction.nothing, forceBottomOfList: true);
            }
        }
        //Trigger tutorial
        else if(caseStatus == CaseStatus.handInCollected)
        {
            //Complete form objective: The main story has it's own version of this with a 'view questions' objective.
            if(caseType != CaseType.mainStory || (ChapterController.Instance != null && ChapterController.Instance.currentPart > 30))
            {
                if(job != null)
                {
                    job.DisplayResolveObjectivesCheck();
                }
                else
                {
                    //Create objectives for case
                    foreach (ResolveQuestion q in resolveQuestions)
                    {
                        if (!q.displayObjective) continue;

                        if (q.inputType == Case.InputType.objective) continue; //Skip if this already depends on an objective to trigger...

                        Game.Log("Jobs: Adding auto objective (resolve question): " + q.name + "...");

                        Objective.ObjectiveTrigger completedJob = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.onCompleteJob, "complete");
                        AddObjective(q.name, completedJob, false, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: q.icon, jobRef: job);
                        q.OnProgressChange += OnQuestionProgressChange;
                    }
                }

                //Rename murder case
                if(caseType == CaseType.murder && name == Strings.Get("ui.interface", "New Murder Case"))
                {
                    foreach(MurderController.Murder m in MurderController.Instance.activeMurders)
                    {
                        if(m.murderer != null)
                        {
                            string monkier = m.GetMonkier();

                            if (monkier.Length > 0)
                            {
                                string txt = Strings.Get("ui.gamemessage", "The case of the ") + monkier;

                                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.gameHeader, 0, txt);
                                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.gameHeader, 0, SessionData.Instance.CurrentTimeString(false, true) + ", " + SessionData.Instance.LongDateString(SessionData.Instance.gameTime, true, true, true, true, true, false, false, true) + ", " + Player.Instance.currentGameLocation.name);

                                name = monkier;
                                CasePanelController.Instance.UpdateCaseControls();
                                break;
                            }
                        }
                    }
                }
            }
        }
        else if(caseStatus == CaseStatus.submitted)
        {
            //Close resolve case window
            if(ResolveController.Instance != null)
            {
                ResolveController.Instance.wcc.window.CloseWindow(false);
            }

            //Update the correct answers to this case...
            if (caseType == CaseType.murder || caseType == CaseType.mainStory)
            {
                MurderController.Instance.UpdateCorrectResolveAnswers();
            }
            else if(caseType == CaseType.sideJob)
            {
                job.UpdateResolveAnswers();
            }

            //Decide correct answers now
            foreach (ResolveQuestion question in resolveQuestions)
            {
                question.UpdateValid(this);
                question.UpdateCorrect(this);
            }

            Objective.ObjectiveTrigger caseProcessing = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.waitForCaseProcessing, "");
            AddObjective("case processing", caseProcessing, false, onCompleteAction: Objective.OnCompleteAction.nothing, useIcon: InterfaceControls.Icon.time, ignoreDuplicates: true);
        }
        else if(caseStatus == CaseStatus.closable)
        {
            if(cancelObjectives) ClearAllObjectives();
        }

        if(CasePanelController.Instance.activeCase == this)
        {
            CasePanelController.Instance.UpdateCloseCaseButton();

            //Update resolve window if there is one...
            if(ResolveController.Instance != null)
            {
                ResolveController.Instance.UpdateResolveFields();
            }
        }

        CasePanelController.Instance.UpdateResolveNotifications();
    }

    public NewGameLocation GetGameLocationFromQuestionInput(Case.ResolveQuestion question)
    {
        //Parse locations
        NewGameLocation thisLocation = null;

        if(job != null)
        {
            thisLocation = job.GetGameLocationFromQuestionInput(question);
        }
        else
        {
            Case.ResolveQuestion addressQuestion = resolveQuestions.Find(item => item.inputType == Case.InputType.location);

            if (addressQuestion != null && addressQuestion.input != null && addressQuestion.input.Length > 0)
            {
                //Parse locations
                thisLocation = CityData.Instance.gameLocationDirectory.Find(item => item.name.ToLower() == addressQuestion.input.ToLower());
            }
            else Game.Log("Jobs: Unable to get a valid address for address question input");
        }

        return thisLocation;
    }

    public Human GetCitizenFromQuestionInput(Case.ResolveQuestion question)
    {
        //Parse locations
        Human thisPerson = null;

        //With D0 difficulty, we already know the address...
        if (job != null)
        {
            thisPerson = job.GetCitizenFromQuestionInput(question);
        }
        else
        {
            Case.ResolveQuestion citizenQuestion = resolveQuestions.Find(item => item.inputType == Case.InputType.citizen);

            if (citizenQuestion != null && citizenQuestion.input != null && citizenQuestion.input.Length > 0)
            {
                //Parse locations
                thisPerson = CityData.Instance.citizenDirectory.Find(item => item.GetCitizenName().ToLower() == citizenQuestion.input.ToLower());
            }
            else Game.Log("Jobs: Unable to get a valid citizen for name question input");
        }

        return thisPerson;
    }

    //Sync resolve question progress to objective
    public void OnQuestionProgressChange(ResolveQuestion question)
    {
        //Find the objective to sync
        Objective toSync = currentActiveObjectives.Find(item => item.queueElement.entryRef == question.name);

        if(toSync != null)
        {
            toSync.SetProgress(question.progress);
        }
    }

    public void Resolve()
    {
        Game.Log("Resolving case " + name + "...");

        //Update the correct answers to this case...
        //if(caseType == CaseType.murder || caseType == CaseType.mainStory)
        //{
        //    MurderController.Instance.UpdateCorrectResolveAnswers();
        //}

        int totalReward = 0;
        int totalPenalty = 0;
        isSolved = true;

        questionsRank = 0f; //Normalized questions rank
        rank = CaseRank.C;

        //Tally up right/wrong answers...
        foreach(ResolveQuestion question in resolveQuestions)
        {
            //question.isCorrect = question.ReturnCorrect();

            if(question.isCorrect)
            {
                totalReward += question.reward;
                questionsRank += 1f;
            }
            else if(!question.isOptional)
            {
                totalPenalty += question.penalty;
                isSolved = false;
                rank = CaseRank.unSolved;
            }
        }

        questionsRank /= resolveQuestions.Count;
        victimsRank = Mathf.InverseLerp(GameplayControls.Instance.worstCaseVictimCount, GameplayControls.Instance.bestCaseVictimCount, MurderController.Instance.activeMurders.Count);
        float overallRank = (questionsRank + victimsRank) / 2f; //Average of both equals final rank

        if(overallRank >= 2f)
        {
            rank = CaseRank.super;
        }
        else
        {
            rank = (CaseRank)(1 + 4 - Mathf.RoundToInt(overallRank * 4));
        }

        Game.Log("Case resolve score: Questions rank: " + questionsRank + ", victims Rank: " + victimsRank + ", overall rank: " + overallRank + " = " + rank.ToString());

        //Execute Popup
        if(ResultsController.Instance != null)
        {
            ResultsController.Instance.wcc.window.CloseWindow(false);
        }

        //Show correct/incorrect questions display
        InterfaceController.Instance.ExecuteResolveDisplay(this);

        if(isSolved)
        {
            InterfaceController.Instance.ExecuteMissionCompleteDisplay(this);
        }
        else
        {
            InterfaceController.Instance.ExecuteMissionUnsolvedDisplay(this);
        }

        //Pause & open window
        //SessionData.Instance.PauseGame(true);
        //InterfaceController.Instance.SpawnWindow(null, presetName: "CaseResults", autoPosition: false, forcePosition: InterfaceControls.Instance.handbookWindowPosition, worldInteraction: false, passedCase: this);

        //Release any innocent detained suspects...
        foreach (int i in suspectsDetained)
        {
            Human h = null;

            if(CityData.Instance.GetHuman(i, out h))
            {
                if(isSolved && (caseType == CaseType.murder || caseType == CaseType.mainStory))
                {
                    //Guilty!
                    if (MurderController.Instance.activeMurders.Exists(item => item.murdererID == i))
                    {
                        h.ai.isConvicted = true;
                        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "suspect convicted"), InterfaceControls.Icon.star);
                        continue;
                    }
                }

                //Innocent
                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "suspect released"), InterfaceControls.Icon.empty);
                h.RemoveFromWorld(false);
            }
        }

        if (!isSolved) totalReward = 0; //You don't get any reward if the main answer is wrong...
        GameplayController.Instance.AddMoney(totalReward, true, "case reward");
        GameplayController.Instance.AddMoney(-totalPenalty, true, "case penalty");

        if (isSolved)
        {
            //Display message
            SetStatus(CaseStatus.closable);

            if(caseType == CaseType.sideJob)
            {
                if(job != null)
                {
                    job.Complete();
                }
            }
            else if(caseType == CaseType.murder)
            {
                for (int i = 0; i < MurderController.Instance.activeMurders.Count; i++)
                {
                    MurderController.Instance.activeMurders[i].murderer.ai.isConvicted = true;
                    MurderController.Instance.activeMurders[i].murderer.RemoveFromWorld(true); //Convict murderers...
                    MurderController.Instance.activeMurders[i].SetMurderState(MurderController.MurderState.solved, true);
                    i--;
                }

                //Remove current murderer references
                MurderController.Instance.currentMurderer = null;
                MurderController.Instance.currentVictim = null;
                MurderController.Instance.currentActiveCase = null;

                MurderController.Instance.maxDifficultyLevel++; //Add to max difficulty
            }
            else if(caseType == CaseType.mainStory)
            {
                for (int i = 0; i < MurderController.Instance.activeMurders.Count; i++)
                {
                    MurderController.Instance.activeMurders[i].murderer.ai.isConvicted = true;
                    MurderController.Instance.activeMurders[i].murderer.RemoveFromWorld(true); //Convict murderers...
                    MurderController.Instance.activeMurders[i].SetMurderState(MurderController.MurderState.solved, true);
                    i--;
                }

                //Remove current murderer references
                MurderController.Instance.currentMurderer = null;
                MurderController.Instance.currentVictim = null;
                MurderController.Instance.currentActiveCase = null;
            }

            //Create objectives for case
            foreach (ResolveQuestion q in resolveQuestions)
            {
                q.OnProgressChange -= OnQuestionProgressChange; //Remove listen
            }
        }
        else
        {
            //Display message
            SetStatus(CaseStatus.handInCollected);

            //Reset questions
            foreach(ResolveQuestion q in resolveQuestions)
            {
                q.input = "...";
            }
        }
    }

    public bool ValidationCheck()
    {
        handInValid = false;

        //Not all questions have been revealed yet...
        if (job != null)
        {
            if (resolveQuestions != null && resolveQuestions.Exists(item => item.displayOnlyAtPhase && job.phase < item.displayAtPhase))
            {
                handInValid = false;
                return handInValid;
            }
        }

        if (caseStatus == Case.CaseStatus.handInCollected)
        {
            handInValid = true;

            foreach (ResolveQuestion r in resolveQuestions)
            {
                if(!r.isOptional && !r.UpdateValid(this))
                {
                    handInValid = false;
                    break;
                }
            }

            if(CasePanelController.Instance.activeCase != null && CasePanelController.Instance.activeCase.currentActiveObjectives != null)
            {
                Objective existingObj = CasePanelController.Instance.activeCase.currentActiveObjectives.Find(item => item.queueElement.entryRef == "Submit case");

                if (handInValid)
                {
                    Game.Log("Jobs: Case input is valid");

                    //Only do the following if this isn't a side job OR we know the side job hand in location
                    if (existingObj == null && (CasePanelController.Instance.activeCase.caseType != Case.CaseType.sideJob || (CasePanelController.Instance.activeCase.job != null && CasePanelController.Instance.activeCase.job.knowHandInLocation)))
                    {
                        //Get the nearest hand-in
                        Interactable closestHandIn = GetClosestHandIn();

                        if (closestHandIn != null)
                        {
                            if (CasePanelController.Instance.activeCase.caseType == Case.CaseType.sideJob)
                            {
                                //Objective.ObjectiveTrigger submit = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.submitCase, "Submit case sidejob", newProgressAdd: 1f);
                                //CasePanelController.Instance.activeCase.AddObjective("Submit case sidejob", submit, true, pointerPosition: closestHandIn.GetWorldPosition(), useIcon: InterfaceControls.Icon.hand, onCompleteAction: Objective.OnCompleteAction.nothing);
                            }
                            else if (CasePanelController.Instance.activeCase.caseType == Case.CaseType.retirement)
                            {

                            }
                            else
                            {
                                Objective.ObjectiveTrigger submit = new Objective.ObjectiveTrigger(Objective.ObjectiveTriggerType.submitCase, "Submit case", newProgressAdd: 1f);
                                CasePanelController.Instance.activeCase.AddObjective("Submit case", submit, true, pointerPosition: closestHandIn.GetWorldPosition(), useIcon: InterfaceControls.Icon.hand, onCompleteAction: Objective.OnCompleteAction.nothing, ignoreDuplicates: true);
                            }
                        }
                        else
                        {
                            Game.Log("Jobs: No hand in locations for case " + name);
                        }
                    }
                }
                else
                {
                    Game.Log("Jobs: Case input is invalid");

                    if (existingObj != null)
                    {
                        existingObj.Cancel();
                    }
                }
            }
        }

        if(ResolveController.Instance != null)
        {
            ResolveController.Instance.ValidationUpdate();
        }

        CasePanelController.Instance.UpdateResolveNotifications();

        return handInValid;
    }

    //Same as above but using just one trigger
    public virtual void AddObjective(string entryRef, Objective.ObjectiveTrigger trigger, bool usePointer = false, Vector3 pointerPosition = new Vector3(), InterfaceControls.Icon useIcon = InterfaceControls.Icon.lookingGlass, Objective.OnCompleteAction onCompleteAction = Objective.OnCompleteAction.nextChapterPart, float delay = 0f, bool removePrevious = false, string chapterString = "", bool isSilent = false, bool allowCrouchPromt = false, SideJob jobRef = null, bool forceBottomOfList = false, bool ignoreDuplicates = false, bool useParsing = true)
    {
        //Only add objective if it's not already created (helps with save games)
        if (ignoreDuplicates || !currentActiveObjectives.Exists(item => item.queueElement.entryRef == entryRef))
        {
            if (ignoreDuplicates || !inactiveCurrentObjectives.Exists(item => item.queueElement.entryRef == entryRef))
            {
                if (ignoreDuplicates || !endedObjectives.Exists(item => item.queueElement.entryRef == entryRef && item.isCancelled))
                {
                    Game.Log("Objective: Successfully added objective " + entryRef + " to case " + name);

                    List<Objective.ObjectiveTrigger> triggerList = new List<Objective.ObjectiveTrigger>();
                    triggerList.Add(trigger);
                    Player.Instance.speechController.speechQueue.Add(new SpeechController.QueueElement(id, entryRef, usePointer, pointerPosition, useIcon, triggerList, onCompleteAction, delay, removePrevious, chapterString, isSilent, allowCrouchPromt, jobRef, forceBottomOfList, newUseParsing: useParsing));

                    //Start coroutine
                    Player.Instance.speechController.enabled = true;

                    //Force elements to bottom
                    List<SpeechController.QueueElement> toBottom = Player.Instance.speechController.speechQueue.FindAll(item => item.forceBottom);

                    foreach (SpeechController.QueueElement sq in toBottom)
                    {
                        Player.Instance.speechController.speechQueue.Remove(sq);
                        Player.Instance.speechController.speechQueue.Add(sq);
                    }
                }
                else
                {
                    Game.Log("Objective: " + entryRef + " already exists in case " + name + " ended objectives");
                }
            }
            else
            {
                Game.Log("Objective: " + entryRef + " already exists in case " + name + " inactive objectives");
            }
        }
        else
        {
            Game.Log("Objective: " + entryRef + " already exists in case " + name + " active objectives");
        }
    }

    public Case(string newName, CaseType newCaseType, CaseStatus newCaseStatus)
    {
        name = newName;
        caseType = newCaseType;
        id = assignCaseID;
        assignCaseID++;

        SetStatus(newCaseStatus);
    }

    public Interactable GetClosestHandIn()
    {
        //Get the nearest hand-in
        Interactable closestHandIn = null;
        float closest = Mathf.Infinity;

        foreach (int i in handIn)
        {
            Interactable h = null;

            if (CityData.Instance.savableInteractableDictionary.TryGetValue(i, out h))
            {
                float dist = Vector3.Distance(h.wPos, Player.Instance.transform.position);

                if (dist < closest || closestHandIn == null)
                {
                    closestHandIn = h;
                    closest = dist;
                }
            }
            //Else this is a door interactable...
            else
            {
                NewDoor d = null;

                if (CityData.Instance.doorDictionary.TryGetValue(-i, out d))
                {
                    float dist = Vector3.Distance(d.peekInteractable.wPos, Player.Instance.transform.position);

                    if (dist < closest || closestHandIn == null)
                    {
                        closestHandIn = d.peekInteractable;
                        closest = dist;
                    }
                }
            }
        }

        return closestHandIn;
    }

    //Clears all previously spawned and active objectives
    public void ClearAllObjectives()
    {
        Game.Log("Jobs: Clearing all objectives for " + name + "...");

        //Clear speech
        Player.Instance.speechController.speechQueue.Clear();

        if (Player.Instance.speechController.activeSpeechBubble != null)
        {
            Toolbox.Instance.DestroyObject(Player.Instance.speechController.activeSpeechBubble.gameObject);
            Player.Instance.speechController.activeSpeechBubble = null;
        }

        Player.Instance.speechController.speechDelay = 0f; //Reset delays

        //Remove previous
        for (int i = 0; i < currentActiveObjectives.Count; i++)
        {
            if (!currentActiveObjectives[i].isCancelled)
            {
                currentActiveObjectives[i].Cancel();
            }
        }
    }
}
