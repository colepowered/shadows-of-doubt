﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "colourscheme_data", menuName = "Database/Colour Scheme")]

public class ColourSchemePreset : SoCustomComparison
{
    [Header("Colours")]
    public Color primary1;
    public Color secondary1;
    public Color neutral;
    public Color secondary2;
    public Color primary2;

    [Header("Settings")]
    [Tooltip("0 = old fashioned/conservative, 1 = modern/liberal: Driven by the design style")]
    [Range(0, 10)]
    public int modernity = 5;
    [Tooltip("0 = informal/cosy, 1 = clean/souless: Driven by the room type.")]
    [Range(0, 10)]
    public int cleanness = 5;
    [Tooltip("0 = understated/quiet, 1 = loud/bold: Driven by the owner's personality")]
    [Range(0, 10)]
    public int loudness = 5;
    [Tooltip("0 = cold/hard, 1 = warm/sensitive: Driven by the owner's personality")]
    [Range(0, 10)]
    public int emotive = 5;
}
