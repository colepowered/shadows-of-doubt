﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Rendering.HighDefinition;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "lighting_data", menuName = "Database/Lighting Preset")]

public class LightingPreset : SoCustomComparison
{
    [Header("Light")]
    [Tooltip("Cool Colours")]
    public List<CityControls.WindowColour> coolColours = new List<CityControls.WindowColour>();
    [Tooltip("Warm Colours")]
    public List<CityControls.WindowColour> warmColours = new List<CityControls.WindowColour>();
    [Tooltip("If no room is assigned, use this intensity")]
    public float defaultIntensity = 550f;
    public float defaultRange = 9f;
    [Tooltip("how much the intensity changes per room size, also range")]
    public float intensityRoomSizeMultiplier = 1f;
    [Tooltip("Clamped intensity range")]
    public Vector2 intensityRange = new Vector2(100f, 1600f);
    [Tooltip("Fade in or out when turned on or off")]
    public bool fadeOnOff = true;
    [Tooltip("Fade in/out by this speed")]
    public float fadeSpeed = 10f;
    [Tooltip("When setup, set the light")]
    public bool onByDefault = true;
    [Tooltip("Distance at which the emission is culled completely")]
    public float fadeDistance = 40f;

    public enum CullMode { roomParent, occlusion}
    public CullMode cullMode = CullMode.roomParent;

    [Header("Materials")]
    [Tooltip("If true, uses the tv broadcast material")]
    public bool useBroadcastMaterial = false;
    [DisableIf("useBroadcaseMaterial")]
    [Tooltip("Use an alternate material when on (shared)")]
    public Material useOnMaterial = null;
    [Tooltip("Dynamically alter emissive (create instanced material)")]
    public bool useInstancedEmissive = false;
    [EnableIf("useInstancedEmissive")]
    [Tooltip("Emmission multiplier")]
    public float emissionMultiplier = 1f;

    //[Header("Special Options")]
    //[Tooltip("If true, a special copy of this light will be created, set to illuminate the street (if this room has an entrance to the street). Watch out for this as it could be expensive if too many are created.")]
    //public bool enableExteriorLight = false;
    [Header("Atrium Lights")]
    [Tooltip("Special option to make this hang down")]
    public bool isAtriumLight = false;
    [EnableIf("isAtriumLight")]
    [Tooltip("What is the minimum number of floors this atrium covers before it is allowed to feature this light?")]
    public int minimumFloors = 2;
    [EnableIf("isAtriumLight")]
    public GameObject cablePrefab;
    [EnableIf("isAtriumLight")]
    public GameObject bulbPrefab;
    [EnableIf("isAtriumLight")]
    public GameObject endBulbPrefab;
    [EnableIf("isAtriumLight")]
    [Tooltip("Spawn a bulb every x metres")]
    public float heightInterval = 8f;

    [Header("Volumetrics")]
    public bool enableVolumetrics = true;
    [Tooltip("The atmosphere setting in the room preset is multiplied by this")]
    public float atmosphereMultiplier = 1f;

    [Header("Shadows")]
    public bool enableShadows = true;

    public enum ShadowMode { everyFrame, onEnable, onDemand, dynamicSystemStatic, dynamicSystemSlowerUpdate };

    public ShadowMode shadowMode = ShadowMode.everyFrame;
    public ShadowResolution resolution = ShadowResolution.medium;
    public enum ShadowResolution { low, medium, high, ultra};

    [Tooltip("Distance at which shadows are culled completely.")]
    public float shadowFadeDistance = 40f;

    [Header("Flickering")]
    [Range(0f, 1f)]
    public float chanceOfFlicker = 0f;
    [Tooltip("When flickering, use this multiplier on the flicker colour to determin the actual colour (basically a darker version of flicker colour)")]
    public Vector2 flickerMultiplierRange = new Vector2(0f, 0.2f);
    [Tooltip("When flickering, how fast it pulses")]
    public Vector2 flickerPulseRange = new Vector2(1f, 1.1f);
    [Tooltip("Flickering lasts this long")]
    public Vector2 flickerIntervalRange = new Vector2(0.25f, 2f);
    [Tooltip("Intervals between flickering are this long")]
    public Vector2 flickerNormalityIntervalRange = new Vector2(0.1f, 10f);
}
