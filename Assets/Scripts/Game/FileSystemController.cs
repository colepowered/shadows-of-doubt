﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FileSystemController : MonoBehaviour
{
    [Header("Setup")]
    public StackType stackMode = StackType.filingSystem;
    public InteractableController controller;
    public GameObject filePrefab;
    public Vector3 pagesOffset = new Vector3(0f, 0f, 0.02f);
    public EvidenceMultiPage ev;
    [Space(5)]
    [Tooltip("Apply a postion & rotation to the top pages group")]
    public Vector3 frontPagesPos = Vector3.zero;
    public Vector3 frontPagesEuler = Vector3.zero;

    public Dictionary<int, List<EvidenceMultiPage.MultiPageContent>> content = new Dictionary<int, List<EvidenceMultiPage.MultiPageContent>>();
    public int pageCount = 0;

    public enum StackType { filingSystem, pile};

    [Header("File System")]
    public Transform frontBunch;
    public Transform rearBunch;
    public int currentPage = 0;
    public List<Transform> fontPages = new List<Transform>();
    public List<Transform> rearPages = new List<Transform>();
    private float moveProgress = 0f;

    public void Setup(InteractableController newController)
    {
        controller = newController;
        ev = controller.interactable.evidence as EvidenceMultiPage;

        if(ev != null)
        {
            //Get unique page content
            foreach(EvidenceMultiPage.MultiPageContent c in ev.pageContent)
            {
                if(!content.ContainsKey(c.page))
                {
                    content.Add(c.page, new List<EvidenceMultiPage.MultiPageContent>());
                }

                content[c.page].Add(c);

                pageCount = Mathf.Max(pageCount, c.page);
            }

            //Game.Log("Filing system page count: " + pageCount);

            //Create pages on the rear bunch: Maximum of 14 pages
            for (int i = 0; i < Mathf.Min(pageCount, 14); i++)
            {
                GameObject newPage = Instantiate(filePrefab, rearBunch);
                Toolbox.Instance.SetLightLayer(filePrefab, controller.interactable.node.building);
                controller.pages.Add(newPage.transform);

                //Set Z Position
                newPage.transform.localPosition = pagesOffset * i;
            }

            //Add to rear pages in reverse
            for (int i = controller.pages.Count - 1; i >= 0; i--)
            {
                rearPages.Add(controller.pages[i]);
            }

            //Set page
            SetPage(ev.page, true);
        }
        else
        {
            Game.LogError("No multi page evidence found for " + controller.interactable.name + " (check evidence config!)");
            return;
        }

        this.enabled = false; //Not needed at start
    }

    public void SetPage(int newPage, bool instant = false)
    {
        currentPage = newPage;
        //Game.Log("File system: Set page = " + currentPage);

        //All pages lower than this index are moved to the front bunch...
        for (int i = 0; i < controller.pages.Count; i++)
        {
            Transform thisPage = controller.pages[i];

            if (i < currentPage)
            {
                thisPage.SetParent(frontBunch);
                rearPages.Remove(thisPage);
                if(!fontPages.Contains(thisPage)) fontPages.Add(thisPage);
            }
            else
            {
                thisPage.SetParent(rearBunch);
                fontPages.Remove(thisPage);
                if (!rearPages.Contains(thisPage)) rearPages.Add(thisPage);
            }
        }

        moveProgress = 0f;

        if (instant)
        {
            for (int i = 0; i < fontPages.Count; i++)
            {
                fontPages[i].localPosition = -pagesOffset * i;
            }

            //Arrange rear bunch
            for (int i = 0; i < rearPages.Count; i++)
            {
                rearPages[i].localPosition = pagesOffset * i;
            }
        }
        else
        {
            //Run update to move pages into correct position
            this.enabled = true;
        }
    }

    private void Update()
    {
        moveProgress += Time.deltaTime;

        int pagesCount = rearPages.Count + fontPages.Count;

        //Arrange front bunch
        for (int i = 0; i < fontPages.Count; i++)
        {
            Transform t = fontPages[i];
            int totalPos = rearPages.Count + i;
            float posRatio = (float)totalPos / (float)pagesCount;

            //Calculate these as a ratio of it's position in the whole stack...
            Vector3 additionalPos = frontPagesPos * posRatio;
            Vector3 additionalEuler = frontPagesEuler * posRatio;

            Vector3 desiredPos = additionalPos;

            if(stackMode == StackType.filingSystem)
            {
                desiredPos += -pagesOffset * i;
            }
            else
            {
                desiredPos += pagesOffset * totalPos;
            }

            t.localPosition = Vector3.Lerp(t.localPosition, desiredPos, moveProgress);
            t.localEulerAngles = Vector3.Lerp(t.localEulerAngles, additionalEuler, moveProgress);
        }

        //Arrange rear bunch
        for (int i = 0; i < rearPages.Count; i++)
        {
            Transform t = rearPages[i];
            int totalPos = i;
            float posRatio = (float)totalPos / (float)pagesCount;

            //Calculate these as a ratio of it's position in the whole stack...
            //Vector3 additionalPos = frontPagesPos * posRatio;
            Vector3 additionalEuler = Vector3.zero;

            Vector3 desiredPos = Vector3.zero;

            if (stackMode == StackType.filingSystem)
            {
                desiredPos += pagesOffset * i;
            }
            else
            {
                desiredPos += pagesOffset * totalPos;
            }

            t.localPosition = Vector3.Lerp(t.localPosition, desiredPos, moveProgress);
            t.localEulerAngles = Vector3.Lerp(t.localEulerAngles, additionalEuler, moveProgress);
        }

        if(moveProgress >= 1f)
        {
            moveProgress = 1f;
            this.enabled = false;
        }
    }
}
