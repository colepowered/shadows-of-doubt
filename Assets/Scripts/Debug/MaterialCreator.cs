﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using UnityEditor;

public class MaterialCreator : MonoBehaviour
{
    [Tooltip("Removes the mesh collider")]
    public bool removeCollider = false;
    [Tooltip("If true then add an interactable controller to this object")]
    public bool addInteractableController = false;
    [Tooltip("Duplicate the diffuse map as a normal map. This will not happen if a separate normal map is found.")]
    public bool duplicateDiffuseAndUseAsNormal = false;
    [Tooltip("Force the 'Colour' shader (alternate colour options and grub texture features). If false this may use the default unity 'Lit' shader if there isn't a colour or grub map...")]
    public bool forceColourShader = true;
    //[Tooltip("Save as Prefab")]
    //public bool saveAsPrefab = false;

    [Button]
    public void CreateMaterial()
    {
#if UNITY_EDITOR
        MeshFilter mesh = this.gameObject.GetComponent<MeshFilter>();
        MeshRenderer rend = this.gameObject.GetComponent<MeshRenderer>();

        if(mesh == null)
        {
            Debug.Log("Cannot find mesh filter on this object, make sure there is one attached...");
            return;
        }

        if (rend == null)
        {
            Debug.Log("Cannot find mesh renderer on this object, make sure there is one attached...");
            return;
        }

        if(removeCollider)
        {
            Collider coll = this.gameObject.GetComponent<MeshCollider>();

            if(coll != null)
            {
                SafeDestroy(coll);
            }
        }

        //Get/rename object
        string[] underscoreSplit = this.name.Split('_');
        this.name = underscoreSplit[0];

        //Have a look to find the matching material texture files...
        string meshPath;
        meshPath = AssetDatabase.GetAssetPath(mesh.sharedMesh);

        string[] reconstructPath = meshPath.Split('/');
        string importPath = string.Empty;

        for (int i = 0; i < reconstructPath.Length - 1; i++)
        {
            importPath += reconstructPath[i] + "/";
        }

        Debug.Log("Found mesh path at: " + importPath + ", looking for corresponding texures...");

        bool colourShaderNeeded = false;
        bool maskMapNeeded = false;

        Texture2D diffuse = (Texture2D)AssetDatabase.LoadAssetAtPath(importPath + "/materials/" + name + "_diffuse.png", typeof(Texture2D));

        if(diffuse != null)
        {
            Debug.Log("... Found " + name + " diffuse map!");
        }

        Texture2D height = (Texture2D)AssetDatabase.LoadAssetAtPath(importPath + "/materials/" + name + "_height.png", typeof(Texture2D));

        if(height == null && diffuse != null && duplicateDiffuseAndUseAsNormal)
        {
            Debug.Log("Duplicating diffuse map and using as normal map...");

            //Make readable
            List<Texture2D> makeUnreadableAgain = new List<Texture2D>();

            if (!diffuse.isReadable)
            {
                SetTextureImporterFormat(diffuse, true);
                makeUnreadableAgain.Add(diffuse);
            }

            //Create a new mask map...
            Texture2D newNormal = new Texture2D(diffuse.width, diffuse.height);

            //Cylce through data from other maps
            for (int x = 0; x < diffuse.width; x++)
            {
                for (int y = 0; y < diffuse.height; y++)
                {
                    Color pixel = Color.black;

                    //If this is unavailable, use the smoothness...
                    float greyscale = GetPixel(diffuse, x, y);
                    pixel.r = greyscale;
                    pixel.b = greyscale;
                    pixel.g = greyscale;

                    newNormal.SetPixel(x, y, pixel);
                }
            }

            newNormal.Apply(); //Apply changes

            //Save to file...
            var bytes = newNormal.EncodeToPNG();
            string path = importPath + "/materials/" + name + "_height.png";
            Debug.Log("Creating height map at " + path);
            System.IO.File.WriteAllBytes(path, bytes);
            AssetDatabase.Refresh();
            AssetDatabase.ImportAsset(path);
            height = (Texture2D)AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D));

            //Make unreadable again...
            while (makeUnreadableAgain.Count > 0)
            {
                SetTextureImporterFormat(makeUnreadableAgain[0], false);
                makeUnreadableAgain.RemoveAt(0);
            }
        }

        if (height != null)
        {
            Debug.Log("... Found " + name + " height map!");
            Debug.Log("Converting height map to normal map...");

            TextureImporter importer = AssetImporter.GetAtPath(importPath + "/materials/" + name + "_height.png") as TextureImporter;
            importer.textureType = TextureImporterType.NormalMap;
            importer.heightmapScale = 0.3f;
            importer.convertToNormalmap = true;
            AssetDatabase.WriteImportSettingsIfDirty(importPath + "/materials/" + name + "_height.png");
            importer.SaveAndReimport();
        }

        Texture2D colour = (Texture2D)AssetDatabase.LoadAssetAtPath(importPath + "/materials/" + name + "_colour.png", typeof(Texture2D));

        if (colour != null)
        {
            Debug.Log("... Found " + name + " colour map!");
            colourShaderNeeded = true;
        }

        Texture2D smoothness = (Texture2D)AssetDatabase.LoadAssetAtPath(importPath + "/materials/" + name + "_smoothness.png", typeof(Texture2D));

        if (smoothness != null)
        {
            Debug.Log("... Found " + name + " smoothness map!");
            maskMapNeeded = true;
        }

        Texture2D metallic = (Texture2D)AssetDatabase.LoadAssetAtPath(importPath + "/materials/" + name + "_metallic.png", typeof(Texture2D));

        if (metallic != null)
        {
            Debug.Log("... Found " + name + " metallic map!");
            maskMapNeeded = true;
        }

        Texture2D grub = (Texture2D)AssetDatabase.LoadAssetAtPath(importPath + "/materials/" + name + "_grub.png", typeof(Texture2D));

        if (grub != null)
        {
            Debug.Log("... Found " + name + " grub map!");
            colourShaderNeeded = true;
        }

        Texture2D ao = (Texture2D)AssetDatabase.LoadAssetAtPath(importPath + "/materials/" + name + "_ao.png", typeof(Texture2D));

        if (ao != null)
        {
            Debug.Log("... Found " + name + " ambient occlusion map!");
            maskMapNeeded = true;
        }

        Texture2D emission = (Texture2D)AssetDatabase.LoadAssetAtPath(importPath + "/materials/" + name + "_emission.png", typeof(Texture2D));

        if (emission != null)
        {
            Debug.Log("... Found " + name + " emission map!");
        }

        //Create mask map...
        Texture2D maskMap = null;

        if(maskMapNeeded)
        {
            //Make readable
            List<Texture2D> makeUnreadableAgain = new List<Texture2D>();

            if(metallic != null && !metallic.isReadable)
            {
                SetTextureImporterFormat(metallic, true);
                makeUnreadableAgain.Add(metallic);
            }

            if (smoothness != null && !smoothness.isReadable)
            {
                SetTextureImporterFormat(smoothness, true);
                makeUnreadableAgain.Add(smoothness);
            }

            if (ao != null && !ao.isReadable)
            {
                SetTextureImporterFormat(ao, true);
                makeUnreadableAgain.Add(ao);
            }

            int w = 1;
            int h = 1;

            if (smoothness != null)
            {
                w = smoothness.width;
                h = smoothness.height;
            }
            else if(metallic != null)
            {
                w = metallic.width;
                h = metallic.height;
            }
            else if (ao != null)
            {
                w = ao.width;
                h = ao.height;
            }

            //Create a new mask map...
            maskMap = new Texture2D(w, h);

            //Cylce through data from other maps
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    Color pixel = Color.clear;

                    //Red = metallic
                    //If this is unavailable, use the smoothness...
                    if(metallic != null)
                    {
                        pixel.r = GetPixel(metallic, x, y);
                    }
                    else if(smoothness != null)
                    {
                        pixel.r = GetPixel(smoothness, x, y);
                    }

                    //Green = ambient occlusion
                    if(ao != null)
                    {
                        pixel.g = GetPixel(ao, x, y);
                    }

                    //blue is unused
                    pixel.b = 0;

                    //Alpha = smoothness
                    //If this is unavailable, use the metallic...
                    if(smoothness != null)
                    {
                        pixel.a = GetPixel(smoothness, x, y);
                    }
                    else if(metallic != null)
                    {
                        pixel.a = GetPixel(metallic, x, y);
                    }

                    maskMap.SetPixel(x, y, pixel);
                }
            }

            maskMap.Apply(); //Apply changes

            //Save to file...
            var bytes = maskMap.EncodeToPNG();
            string path = importPath + "/materials/" + name + "_mask.png";
            Debug.Log("Creating new mask map at " + path);
            System.IO.File.WriteAllBytes(path, bytes);
            AssetDatabase.Refresh();
            AssetDatabase.ImportAsset(path);
            maskMap = (Texture2D)AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D));

            //Make unreadable again...
            while(makeUnreadableAgain.Count > 0)
            {
                SetTextureImporterFormat(makeUnreadableAgain[0], false);
                makeUnreadableAgain.RemoveAt(0);
            }
        }

        Material newMat = null;

        //Create material
        if(!colourShaderNeeded && !forceColourShader)
        {
            newMat = new Material(Shader.Find("HDRP/Lit"));
            if (diffuse != null) newMat.SetTexture("_BaseColorMap", diffuse);
            if (height != null) newMat.SetTexture("_NormalMap", height);
            if (emission != null)
            {
                newMat.SetTexture("_EmissiveColorMap", emission);
                newMat.SetColor("_EmissiveColor", Color.white);
            }
            if(maskMap != null) newMat.SetTexture("_MaskMap", maskMap);
        }
        else
        {
            newMat = new Material(Shader.Find("Shader Graphs/ColourShader"));
            if(diffuse != null) newMat.SetTexture("_BaseColorMap", diffuse);
            if (height != null) newMat.SetTexture("_NormalMap", height);
            if (colour != null) newMat.SetTexture("_ColourMap", colour);
            if (grub != null) newMat.SetTexture("_GrubMap", grub);
            if (emission != null)
            {
                newMat.SetTexture("_EmissionMap", emission);
                newMat.SetColor("_EmissionColour", Color.white);
            }
            if (maskMap != null) newMat.SetTexture("_MaskMap", maskMap);
        }

        if(newMat != null)
        {
            //Save in the correct corresponding directory...
            string matPath = string.Empty;

            for (int i = 4; i < reconstructPath.Length - 1; i++)
            {
                matPath += reconstructPath[i] + "/";
            }

            string materialPath = "Assets/Materials/" + matPath + name + ".mat";

            Debug.Log("Saving new material at " + materialPath);

            AssetDatabase.CreateAsset(newMat, materialPath);
            AssetDatabase.Refresh();

            Material loadedMat = (Material)AssetDatabase.LoadAssetAtPath(materialPath, typeof(Material));
            rend.sharedMaterial = loadedMat;
            UnityEditor.Selection.activeObject = loadedMat; //Select this

            if(addInteractableController)
            {
                this.gameObject.AddComponent<InteractableController>();
                this.gameObject.GetComponent<InteractableController>().coll = this.gameObject.GetComponent<Collider>();
            }

            //Save as a prefab
            //if (saveAsPrefab)
            //{
            //    string presetName = name;

            //    string presetPath = "Assets/Prefabs/" + matPath + name + ".asset";

            //    PrefabUtility.SaveAsPrefabAssetAndConnect(this.gameObject, presetPath, InteractionMode.AutomatedAction);
            //}

            ////Create clothing preset...
            //if(createClothingPreset)
            //{
            //    string presetName = name;
            //    presetName = presetName.Replace("UpperTorso", string.Empty);
            //    presetName = presetName.Replace("LowerTorso", string.Empty);
            //    presetName = presetName.Replace("Midriff", string.Empty);
            //    presetName = presetName.Replace("UpperArm", string.Empty);
            //    presetName = presetName.Replace("LowerArm", string.Empty);
            //    presetName = presetName.Replace("UpperLeg", string.Empty);
            //    presetName = presetName.Replace("LowerLeg", string.Empty);
            //    string presetPath = "Assets/Resources/Data/AI/Clothes/" + name + ".asset";

            //    ClothesPreset newPreset = new ClothesPreset();

            //    AssetDatabase.CreateAsset(newPreset, presetPath);
            //    AssetDatabase.Refresh();
            //}

            SafeDestroy(this);
        }

#endif
    }

    public static void SetTextureImporterFormat(Texture2D texture, bool isReadable)
    {
#if UNITY_EDITOR
        if (null == texture) return;

        string assetPath = AssetDatabase.GetAssetPath(texture);
        var tImporter = AssetImporter.GetAtPath(assetPath) as TextureImporter;
        if (tImporter != null)
        {
            tImporter.isReadable = isReadable;

            Debug.Log("...Making " + texture.name + " readable: " + isReadable);

            AssetDatabase.ImportAsset(assetPath);
            AssetDatabase.Refresh();
        }

#endif
    }

    private float GetPixel(Texture2D tex, int x, int y)
    {
        return tex.GetPixel(x, y).grayscale;
    }

    public T SafeDestroyGameObject<T>(T component) where T : Component
    {
        if (component != null)
            SafeDestroy(component.gameObject);
        return null;
    }

    public T SafeDestroy<T>(T obj) where T : Object
    {
        if (Application.isEditor)
            Object.DestroyImmediate(obj);
        else
            Object.Destroy(obj);

        return null;
    }
}
