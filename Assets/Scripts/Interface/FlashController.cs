﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashController : MonoBehaviour
{
    public List<Image> colourCodeElements = new List<Image>();
    public bool getNormalColourAtStart = true;
    public Color normalColour = Color.white;
    public Color flashColour = Color.red;
    public float speed = 10f;

    private bool flashActive = false;
    private int repeat = 0;

    // Use this for initialization
    void Start()
    {
        if (colourCodeElements.Count <= 0) colourCodeElements.Add(this.gameObject.GetComponent<Image>());

        if (getNormalColourAtStart)
        {
            normalColour = colourCodeElements[0].color;
        }
    }

    //Flash
    public void Flash(int newRepeat)
    {
        if (!this.isActiveAndEnabled) return;

        //If already active, add repeats to current coroutine
        if(flashActive)
        {
            repeat += newRepeat;
        }
        else
        {
            StartCoroutine("FlashColour", newRepeat);
        }
    }

    public IEnumerator FlashColour(int newRepeat)
    {
        flashActive = true;

        repeat = newRepeat;
        int cycle = 0;
        float progress = 0f;
        float flashF = 0f;

        while (cycle < repeat && progress < 2f)
        {
            progress += speed * Time.deltaTime;

            if (progress <= 1f) flashF = progress;
            else flashF = 2f - progress;

            for (int i = 0; i < colourCodeElements.Count; ++i)
            {
                Image element = colourCodeElements[i];

                element.color = Color.Lerp(normalColour, flashColour, flashF);
            }

            if (progress >= 2f)
            {
                cycle++;
                progress = 0f;
            }

            yield return null;
        }

        //Return to original colors
        for (int i = 0; i < colourCodeElements.Count; ++i)
        {
            Image element = colourCodeElements[i];

            element.color = normalColour;
        }

        flashActive = false;
    }

    private void OnDisable()
    {
        StopCoroutine("FlashColour");
        flashActive = false;
        repeat = 0;

        //Return to original colors
        for (int i = 0; i < colourCodeElements.Count; ++i)
        {
            Image element = colourCodeElements[i];

            element.color = normalColour;
        }
    }
}
