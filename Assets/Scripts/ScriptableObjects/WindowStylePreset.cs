﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "windowstyle_data", menuName = "Database/Window Style")]

public class WindowStylePreset : SoCustomComparison
{
    [Header("Interaction")]
    public bool closable = true;
    public bool pinnable = true;
    [Tooltip("If true this will always and only be able to be a world interaction")]
    public bool forceWorldInteraction = false;
    [Tooltip("Use window focus mode (black screen behind the window)")]
    public bool useWindowFocusMode = false;

    [Header("Resizing")]
    public bool resizable = true;
	public Vector2 defaultSize = new Vector2(514, 658);
	public Vector2 minSize = new Vector2(514, 514);
	public Vector2 maxSize = new Vector2(1000, 1000);

    [Space(7)]
    [InfoBox("Used to make the window size relative to DDS document sizes: Adds this on to the document size to make the window size.", EInfoBoxType.Normal)]
    public Vector2 DDSadditionalSize = new Vector2(172, 176);

    [Header("Icons")]
    public Sprite overrideIcon;

    [Header("Tabs")]
    public List<WindowTabPreset> tabs = new List<WindowTabPreset>();
}
