﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEditor;

public class ProfileApp : CruncherAppContent
{
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI infoText;
    public RawImage actorImage;

    //Called on setup
    public override void OnSetup()
    {
        if(controller.loggedInAs != null)
        {
            List<Evidence.DataKey> mergeKeys = new List<Evidence.DataKey>();

            try
            {
                actorImage.texture = controller.loggedInAs.evidenceEntry.GetPhoto(Toolbox.Instance.GetList<Evidence.DataKey>(Evidence.DataKey.photo));
            }
            catch
            {
                Game.LogError("Unable to get cruncher login photo");
            }
            
            mergeKeys.Add(Evidence.DataKey.photo);
            mergeKeys.Add(Evidence.DataKey.name);
            mergeKeys.Add(Evidence.DataKey.code);

            string txt = string.Empty;

            txt += Strings.Get("descriptors", "First Name", Strings.Casing.firstLetterCaptial) + ": " + controller.loggedInAs.GetFirstName();
            txt += "\n" + Strings.Get("descriptors", "Surname", Strings.Casing.firstLetterCaptial) + ": " + controller.loggedInAs.GetSurName();
            txt += "\n" + Strings.Get("descriptors", "Passcode", Strings.Casing.firstLetterCaptial) + ": " + controller.loggedInAs.passcode.GetDigit(0) + controller.loggedInAs.passcode.GetDigit(1) + controller.loggedInAs.passcode.GetDigit(2) + controller.loggedInAs.passcode.GetDigit(3);

            if (controller.loggedInAs.home != null)
            {
                mergeKeys.Add(Evidence.DataKey.address);

                txt += "\n\n" + Strings.Get("descriptors", "Address", Strings.Casing.firstLetterCaptial) + ": " + controller.loggedInAs.home.name;

                if (controller.loggedInAs.home.telephones != null && controller.loggedInAs.home.telephones.Count > 0)
                {
                    mergeKeys.Add(Evidence.DataKey.telephoneNumber);
                    txt += "\n" + Strings.Get("descriptors", "Telephone", Strings.Casing.firstLetterCaptial) + ": " + controller.loggedInAs.home.telephones[0].numberString;
                }
            }

            foreach(Evidence.DataKey merge1 in mergeKeys)
            {
                if(controller.loggedInAs.evidenceEntry != null) controller.loggedInAs.evidenceEntry.MergeDataKeys(Evidence.DataKey.name, merge1);
            }

            infoText.text = txt;
            controller.ic.interactable.SetCustomState3(true, Player.Instance); //This allows the inspection action
        }
    }

    private void OnDestroy()
    {
        controller.ic.interactable.SetCustomState3(false, Player.Instance);
    }

    public void ExitButton()
    {
        controller.OnAppExit();
    }
}
