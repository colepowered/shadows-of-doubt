﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "murderweapon_data", menuName = "Database/Murder Weapon")]

public class MurderWeaponPreset : SoCustomComparison
{
	public enum WeaponType { handgun, rifle, shotgun, blade, bluntObject, poison, strangulation, fists};

	[Header("Configuration")]
	public WeaponType type = WeaponType.handgun;
	public List<InteractablePreset> ammunition = new List<InteractablePreset>();
	[Range(0, 3)]
	public int murderDifficultyModifier = 0;

	[Header("World Items")]
	[Tooltip("Local muzzle position relative to the pivot")]
	public Vector3 muzzleOffset = Vector3.zero;
	[Tooltip("Local brass eject relative to the pivot")]
	public Vector3 brassEjectOffset = Vector3.zero;
	[Space(7)]
	public GameObject itemRightOverride;
	public Vector3 itemRightLocalPos;
	public Vector3 itemRightLocalEuler;
	[Space(7)]
	public GameObject itemLeftOverride;
	public Vector3 itemLeftLocalPos;
	public Vector3 itemLeftLocalEuler;
	[Space(7)]
	public bool overideUsesCarryAnimation = false;
	[EnableIf("overideUsesCarryAnimation")]
	public int overrideCarryAnimation = 1;

	[Header("Personal Defence")]
	[Tooltip("If true, citizens may carry this about to defend themselves")]
	public bool usedInPersonalDefence = false;
	public bool disabled = false;
	[EnableIf("usedInPersonalDefence")]
	[Range(0, 10)]
	public int basePriority = 3;

	[Space(7)]
	[MinMaxSlider(0f, 1f)]
	public Vector2 socialClassRange = new Vector2(0, 1);
	[Range(0, 10)]
	public int citizenSpawningWithScore = 0;
	[EnableIf("usedInPersonalDefence")]
	public List<MurderPreset.MurdererModifierRule> personalDefenceTraitModifiers = new List<MurderPreset.MurdererModifierRule>();

	[Space(7)]
	public List<OccupationPreset> jobModifierList = new List<OccupationPreset>();
	public int jobScoreModifier = 0;

	[Space(7)]
	[Tooltip("How this impacts nerve levels of a citizen if drawn")]
	public float drawnNerveModifier = -0.1f;
	[Tooltip("Chance of bark trigger")]
	public float barkTriggerChance = 0f;
	public SpeechController.Bark bark = SpeechController.Bark.threatenByItem;
	[Tooltip("With this weapon, multiply incoming nerve damage by this")]
	public float incomingNerveDamageMultiplier = 1f;

	[Header("Weapon Handling")]
	[Tooltip("At what point during the attack is the trigger executed? Normalized value")]
	[Range(0f, 1f)]
	public float attackTriggerPoint = 0.5f;
	[Tooltip("At what point during the attack is the trigger removed? Normalized value")]
	[Range(0f, 1f)]
	public float attackRemovePoint = 0.7f;
	[Tooltip("How many shots are fired?")]
	public int shots = 1;

	[Space(7)]
	[Tooltip("Weapon range")]
	public Vector2 weaponMaxRange = Vector2.one;
	public float minimumRange = 1f;
	public float maximumBulletRange = 25f;
	public StatMultiplier weaponRangeLerpSource = StatMultiplier.one;

	[Space(7)]
	[Tooltip("Time in seconds between attacks")]
	public Vector2 fireDelay = Vector2.one;
	public StatMultiplier fireDelayLerpSource = StatMultiplier.one;

	[Space(7)]
	[Tooltip("Attack accuracy")]
	public Vector2 attackAccuracy = Vector2.one;
	public StatMultiplier attackAccuracyLerpSource = StatMultiplier.one;

	[Space(7)]
	[Tooltip("Attack damage")]
	public Vector2 attackDamage = Vector2.one;
	public StatMultiplier attackDamageLerpSource = StatMultiplier.one;
	public float applyPoison = 0f;

	public enum StatMultiplier { zero, one, random, combatSkill, combatHeft};
	public enum EjectBrass { none, onFire, onPumpAction, revolver};
	public enum AttackValue { range, fireDelay, accuracy, damage};

	[Header("FX Prefabs")]
	public InteractablePreset shellCasing;
	public EjectBrass ejectBrassSetting = EjectBrass.onFire;
	public InteractablePreset bulletHole;
	public InteractablePreset entryWound;

	[Header("Hits")]
	public SpatterPatternPreset forwardSpatter;
	public SpatterPatternPreset backSpatter;

	[Header("Audio")]
	public AudioEvent fireEvent;
	public AudioEvent impactEvent;
	public AudioEvent impactEventBody;
	public AudioEvent impactEventPlayer;

	public float GetAttackValue(AttackValue valueType, Human human)
	{
		StatMultiplier statMP = StatMultiplier.zero;
		Vector2 input = Vector2.zero;

		if(valueType == AttackValue.accuracy)
        {
			input = attackAccuracy;
			statMP = attackAccuracyLerpSource;
		}
		else if(valueType == AttackValue.damage)
        {
			input = attackDamage;
			statMP = attackDamageLerpSource;
		}
		else if(valueType == AttackValue.fireDelay)
        {
			input = fireDelay;
			statMP = fireDelayLerpSource;
		}
		else if(valueType == AttackValue.range)
        {
			input = weaponMaxRange;
			statMP = weaponRangeLerpSource;
		}

		float output = input.x;

		if (statMP == StatMultiplier.one)
		{
			output = input.y;
		}
		else if (statMP == StatMultiplier.random)
		{
			output = Mathf.Lerp(input.x, input.y, Toolbox.Instance.Rand(0f, 1f, definitelyNotPartOfCityGeneration: true));
		}
		else if (statMP == StatMultiplier.combatHeft)
		{
			output = Mathf.Lerp(input.x, input.y, human.combatHeft);
		}
		else if (statMP == StatMultiplier.combatSkill)
		{
			output = Mathf.Lerp(input.x, input.y, human.combatSkill);
		}

		return output;
	}
}
