﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using System.Linq;
using TMPro;
using NaughtyAttributes;

public class FirstPersonItemController : MonoBehaviour
{
    [Header("Radial Menu")]
    //public bool radialMenuActive = false;
    //[ReadOnly]
    //public float radialFade = 1f;
    //private List<RadialSegmentController> radialSegements = new List<RadialSegmentController>();
    //public RectTransform summaryRect;
    //public TextMeshProUGUI summaryText;
    //private string summaryTextToDisplay;
    //private float summaryTextProgress = 0f;
    //private float summaryDesiredHeight = 0f;

    [Header("Slots")]
    public List<InventorySlot> slots = new List<InventorySlot>();
    public int inventorySlots = 0; //Number of slots not counting static slots

    [System.Serializable]
    public class InventorySlot
    {
        public int index;
        public int interactableID = -1;
        public string debugName;
        public string hotkey;
        public StaticSlot isStatic = StaticSlot.nonStatic;
        public enum StaticSlot { nonStatic, holster, watch, fists, coin, printReader };

        [System.NonSerialized]
        public InventorySquareController spawnedSegment;

        public void SetSegmentContent(Interactable newI)
        {
            //Drop things first...
            if(interactableID > -1)
            {
                FirstPersonItemController.Instance.EmptySlot(this);
            }

            if(newI != null)
            {
                interactableID = newI.id;
                newI.SetInInventory(Player.Instance);
                if(Game.Instance.devMode && Game.Instance.collectDebugData) debugName = newI.name + " (" + newI.preset.name + ")";
            }
            else
            {
                interactableID = -1;
                if (Game.Instance.devMode && Game.Instance.collectDebugData) debugName = "empty";
            }

            if(spawnedSegment != null)
            {
                spawnedSegment.OnUpdateContent();
            }
        }

        public Interactable GetInteractable()
        {
            Interactable ret = null;

            if(interactableID > -1)
            {
                if(!CityData.Instance.savableInteractableDictionary.TryGetValue(interactableID, out ret))
                {
                    Game.LogError("Unable to get inventory interactable " + interactableID + ": " + debugName);

                    ret = CityData.Instance.interactableDirectory.Find(item => item.id == interactableID);

                    if(ret != null)
                    {
                        Game.LogError("Found inventory interactable in directory but not savable dictionary!");
                    }
                }
            }

            return ret;
        }

        public FirstPersonItem GetFirstPersonItem()
        {
            FirstPersonItem ret = GameplayControls.Instance.nothingItem;

            Interactable inter = GetInteractable();
            if (inter != null && inter.preset.fpsItem != null) ret = inter.preset.fpsItem;

            if(isStatic == StaticSlot.holster)
            {
                ret = GameplayControls.Instance.nothingItem;
            }
            else if(isStatic == StaticSlot.fists)
            {
                ret = GameplayControls.Instance.fistsItem;
            }
            else if(isStatic == StaticSlot.watch)
            {
                ret = GameplayControls.Instance.watchItem;
            }
            else if (isStatic == StaticSlot.printReader)
            {
                ret = GameplayControls.Instance.printReader;
            }
            else if(isStatic == StaticSlot.coin)
            {
                ret = GameplayControls.Instance.coinItem;
            }

            return ret;
        }

        public void SetHotKey(string newHotkey)
        {
            Game.Log("Interface: Assign new hotkey to slot: " + newHotkey);

            if(hotkey == newHotkey)
            {
                newHotkey = string.Empty; //Reset hotkey to none
            }

            hotkey = newHotkey;
            if (spawnedSegment != null) spawnedSegment.UpdateHotkeyDisplay();

            if(newHotkey.Length > 0)
            {
                foreach(InventorySlot sl in FirstPersonItemController.Instance.slots)
                {
                    if (sl == this) continue;

                    if(sl.hotkey == newHotkey)
                    {
                        sl.hotkey = string.Empty;
                        if (sl.spawnedSegment != null) sl.spawnedSegment.UpdateHotkeyDisplay();
                    }
                }
            }
        }
    }

    [Header("First Person Items")]
    public bool enableItemSelection = true;
    public Transform lagPivotTransform;
    //public List<FirstPersonItem> firstPersonItems = new List<FirstPersonItem>();
    public FirstPersonItem previousItem;
    public FirstPersonItem currentItem;
    public FirstPersonItem drawnItem;
    public bool finishedDrawingItem = false;
    //public float itemSwitchCounter = 0f;
    //private List<WeaponSelectorController> spawnedWeaponIcons = new List<WeaponSelectorController>();
    public float attackMainDelay = 0f;
    public float attackSecondaryDelay = 0f;
    public Transform leftHandObjectParent;
    public Transform rightHandObjectParent;
    public AnimationClip nothingClip;

    private float equipSoundDelay = 0f;
    private float holsterSoundDelay = 0f;

    //First person hand model materials that correspond to the player's skin colour
    private Material fistMaterial;
    private Material fingerUpperMaterial;
    private Material fingerLowerMaterial;
    private Material fingerTipMaterial;
    private Material thumbJointMaterial;

    //Force holster
    public bool forceHolstered = false;
    public InventorySlot selectedWhenForceHolstered; //The item selected when force-holstered

    public bool listenForHolster = true;
    public bool listenForDrawFinish = false;

    [Header("Interactions")]
    public Dictionary<InteractablePreset.InteractionKey, Interactable.InteractableCurrentAction> currentActions = new Dictionary<InteractablePreset.InteractionKey, Interactable.InteractableCurrentAction>();
    public bool isConsuming = false;
    public bool isRaised = false;
    private bool takeOneActive = false;

    [Header("Flashlight")]
    public bool flashlight = false; //True if flashlight is on
    public GameObject flashLightObject;
    public GameObject captureLightObject; //Light used for capturing evidence images
    public GameObject fingerprintLights;
    public Light printScannerPulseLight;

    [Header("Print Scanner")]
    [Tooltip("Point of raycast impact")]
    public Vector3 scannerRayPoint;
    [Tooltip("Radius of detection")]
    public float printDetectionRadius = 0.3f;

    [Header("Items")]
    public InteractablePreset worldCoin;

    [Header("Audio")]
    public AudioController.LoopingSoundInfo activeLoop;
    public AudioController.LoopingSoundInfo consumeLoop;

    //Used in combat
    private Actor counterAttackActor;
    private Vector3 counterAttackPoint;

    private int updateInteractionCounter = 0;

    //Singleton pattern
    private static FirstPersonItemController _instance;
    public static FirstPersonItemController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        if(!SessionData.Instance.isFloorEdit)
        {
            InterfaceController.Instance.firstPersonModel.SetActive(false);
            //InterfaceController.Instance.radialParent.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        if (SessionData.Instance.play)
        {
            //Update interaction text every x frames
            if (currentItem != null)
            {
                foreach (FirstPersonItem.FPSInteractionAction act in currentItem.actions)
                {
                    if (act.availability != FirstPersonItem.AttackAvailability.always || act.availability != FirstPersonItem.AttackAvailability.never)
                    {
                        if (updateInteractionCounter <= 0)
                        {
                            updateInteractionCounter = 3;
                            InteractionController.Instance.UpdateInteractionText();
                        }

                        updateInteractionCounter--;

                        break;
                    }
                }

                //Consume
                if (isConsuming && !takeOneActive && BioScreenController.Instance.selectedSlot != null && BioScreenController.Instance.selectedSlot.interactableID > -1)
                {
                    Interactable consumable = BioScreenController.Instance.selectedSlot.GetInteractable();

                    if (consumable != null && consumable.cs > 0f)
                    {
                        //Add nourishment
                        if (consumable.preset.retailItem != null)
                        {
                            Player.Instance.AddNourishment(consumable.preset.retailItem.nourishment / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddHydration(consumable.preset.retailItem.hydration / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddAlertness(consumable.preset.retailItem.alertness / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddEnergy(consumable.preset.retailItem.energy / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddExcitement(consumable.preset.retailItem.excitement / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddChores(consumable.preset.retailItem.chores / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddHygiene(consumable.preset.retailItem.hygiene / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddBladder(consumable.preset.retailItem.bladder / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddHeat(consumable.preset.retailItem.heat / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddDrunk(consumable.preset.retailItem.drunk / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddSick(consumable.preset.retailItem.sick / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddHeadache(consumable.preset.retailItem.headache / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddWet(consumable.preset.retailItem.wet / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddBrokenLeg(consumable.preset.retailItem.brokenLeg / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddBruised(consumable.preset.retailItem.bruised / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddBlackEye(consumable.preset.retailItem.blackEye / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddBlackedOut(consumable.preset.retailItem.blackedOut / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddNumb(consumable.preset.retailItem.numb / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddBleeding(consumable.preset.retailItem.bleeding / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddBreath(consumable.preset.retailItem.breath / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddStarchAddiction(consumable.preset.retailItem.starchAddiction / consumable.preset.consumableAmount * Time.deltaTime);
                            Player.Instance.AddPoisoned(consumable.preset.retailItem.poisoned / consumable.preset.consumableAmount * Time.deltaTime, null);
                            Player.Instance.AddHealth(consumable.preset.retailItem.health / consumable.preset.consumableAmount * Time.deltaTime);
                        }

                        consumable.cs = Mathf.Max(consumable.cs - Time.deltaTime, 0);

                        if (consumable.cs <= 0f)
                        {
                            SetConsuming(false);

                            //Effect starch loan...
                            float starchLoan = UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.starchLoan);

                            if (starchLoan != 0)
                            {
                                if (consumable.preset.retailItem != null)
                                {
                                    if(consumable.preset.retailItem.tags.Contains(RetailItemPreset.Tags.starchProduct))
                                    {
                                        foreach(UpgradesController.Upgrades u in UpgradesController.Instance.upgrades)
                                        {
                                            u.uninstallCost -= 5;
                                            u.uninstallCost = Mathf.Max(0, u.uninstallCost);
                                        }
                                    }
                                    else
                                    {
                                        foreach (UpgradesController.Upgrades u in UpgradesController.Instance.upgrades)
                                        {
                                            u.uninstallCost += 1;
                                        }
                                    }

                                    UpgradesController.Instance.UpdateUpgrades();
                                }
                            }

                            //Remove when consumed
                            if (consumable.preset.destroyWhenAllConsumed)
                            {
                                EmptySlot(BioScreenController.Instance.selectedSlot, false, true, playSound: false);

                                //Remove all current hand items
                                foreach (Transform child in rightHandObjectParent)
                                {
                                    Destroy(child.gameObject);
                                }
                            }
                        }
                    }
                    else
                    {
                        SetConsuming(false);
                    }
                }
            }

            //Attack delay
            if (attackMainDelay > 0f)
            {
                attackMainDelay -= Time.deltaTime;
            }

            if (attackSecondaryDelay > 0f)
            {
                attackSecondaryDelay -= Time.deltaTime;
            }

            if (listenForHolster || listenForDrawFinish)
            {
                //Game.Log("Listen for holster: " + listenForHolster);
                //Game.Log("Listen for draw finish: " + listenForDrawFinish);

                if(InterfaceController.Instance.firstPersonAnimator != null)
                {
                    //Game.Log("First person animator");
                    AnimatorClipInfo[] currentClips = InterfaceController.Instance.firstPersonAnimator.GetCurrentAnimatorClipInfo(0);

                    if (currentClips != null && currentClips.Length > 0)
                    {
                        AnimatorClipInfo clip = currentClips[0];

                        //Game.Log(clip.clip);

                        if (listenForHolster && ((clip.clip != null && nothingClip != null && clip.clip.name == nothingClip.name) || (clip.clip != null && currentItem.idleClip != null && clip.clip.name == currentItem.idleClip.name)))
                        {
                            //Game.Log("Holster");
                            OnHolster();
                        }
                        else if(listenForDrawFinish)
                        {
                            if ((clip.clip != null && currentItem.idleClip != null && clip.clip.name == currentItem.idleClip.name) || currentItem == GameplayControls.Instance.nothingItem || currentItem == PlayerApartmentController.Instance.furnitureFPSItem)
                            {
                                //Game.Log("Finished");

                                FinishedDrawingNewItem();
                            }
                        }
                    }
                    else if (listenForHolster)
                    {
                        Game.Log("Holster");
                        OnHolster();
                    }
                    else if (listenForDrawFinish && (currentItem == GameplayControls.Instance.nothingItem || currentItem == PlayerApartmentController.Instance.furnitureFPSItem))
                    {
                        Game.Log("Finished");

                        FinishedDrawingNewItem();
                    }
                }
            }

            if (equipSoundDelay > 0f)
            {
                equipSoundDelay -= Time.deltaTime;

                if (equipSoundDelay <= 0f)
                {
                    AudioController.Instance.Play2DSound(currentItem.equipEvent);

                    if (activeLoop != null)
                    {
                        AudioController.Instance.StopSound(activeLoop, AudioController.StopType.fade, "New weapon");
                    }

                    if (currentItem.activeLoop != null)
                    {
                        activeLoop = AudioController.Instance.Play2DLooping(currentItem.activeLoop);
                    }

                    equipSoundDelay = 0;
                }
            }

            if (holsterSoundDelay > 0f)
            {
                holsterSoundDelay -= Time.deltaTime;

                if (holsterSoundDelay <= 0f)
                {
                    AudioController.Instance.Play2DSound(currentItem.holsterEvent);

                    holsterSoundDelay = 0;
                }
            }
        }

        if (lagPivotTransform != null) lagPivotTransform.rotation = Quaternion.Slerp(lagPivotTransform.rotation, CameraController.Instance.cam.transform.rotation, GameplayControls.Instance.fpsModelLag * Time.deltaTime);
    }

    private void LateUpdate()
    {
        if (lagPivotTransform != null) lagPivotTransform.rotation = Quaternion.Slerp(lagPivotTransform.rotation, CameraController.Instance.cam.transform.rotation, GameplayControls.Instance.fpsModelLag * Time.deltaTime);
    }

    public void SetSlotSize(int newSize)
    {
        //Hard clamped slot sizes
        newSize = Mathf.Clamp(newSize, 1, 12);
        Game.Log("Player: Set new inventory slot size:" + newSize);

        //Make sure we have static items...
        AddSpecificStaticSlot(InventorySlot.StaticSlot.holster);
        AddSpecificStaticSlot(InventorySlot.StaticSlot.fists);
        AddSpecificStaticSlot(InventorySlot.StaticSlot.watch);
        AddSpecificStaticSlot(InventorySlot.StaticSlot.printReader);

        //Should the player have a coin item?
        PlayerMoneyCheck();

        List<InventorySlot> slotCount = slots.FindAll(item => item.isStatic == InventorySlot.StaticSlot.nonStatic);
        int nonStatic = slotCount.Count;

        while(nonStatic < newSize)
        {
            nonStatic++;
            slots.Add(new InventorySlot());
        }

        while(nonStatic > newSize)
        {
            //Remove empty slots first...
            InventorySlot s = slots.Find(item => item.isStatic == InventorySlot.StaticSlot.nonStatic && item.GetInteractable() == null);

            if(s == null)
            {
                //Then move onto items
                s = slots.Find(item => item.isStatic == InventorySlot.StaticSlot.nonStatic && item.GetInteractable() == null);
            }

            if (s != null)
            {
                Game.Log("Player: Emptying slot because it's more than the slot count of " + newSize);
                EmptySlot(s);
            }
            else break;

            if(s.spawnedSegment != null)
            {
                Destroy(s.spawnedSegment.gameObject);
            }

            slots.Remove(s);
            nonStatic--;
        }

        //Update index references
        for (int i = 0; i < slots.Count; i++)
        {
            InventorySlot s = slots[i];
            s.index = i;

            //Setup new
            if(s.spawnedSegment == null)
            {
                //GameObject newSegment = Instantiate(PrefabControls.Instance.radialSelectionSegment, InterfaceController.Instance.radialParent);
                //s.spawnedSegment = newSegment.GetComponent<RadialSegmentController>();

                s.spawnedSegment = BioScreenController.Instance.SpawnSlotObject(s);
            }

            //Update even if existing
            //s.spawnedSegment.UpdateSegment(s);
            s.spawnedSegment.OnUpdateContent();
        }

        if(BioScreenController.Instance.selectedSlot == null)
        {
            BioScreenController.Instance.SelectSlot(slots.Find(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.holster));
        }
    }

    public InventorySlot AddSpecificStaticSlot(InventorySlot.StaticSlot staticItem)
    {
        if (SessionData.Instance.isFloorEdit) return null;

        InventorySlot existingSlot = slots.Find(item => item.isStatic == staticItem);
        if (existingSlot != null) return existingSlot; //Don't allow duplicates of these

        InventorySlot newSlot = new InventorySlot();
        newSlot.isStatic = staticItem;

        //Assign default hotkeys
        if(staticItem == InventorySlot.StaticSlot.holster)
        {
            if(!slots.Exists(item => item.hotkey == "1"))
            {
                newSlot.SetHotKey("1");
            }
        }
        else if (staticItem == InventorySlot.StaticSlot.fists)
        {
            if (!slots.Exists(item => item.hotkey == "2"))
            {
                newSlot.SetHotKey("2");
            }
        }
        else if (staticItem == InventorySlot.StaticSlot.watch)
        {
            if (!slots.Exists(item => item.hotkey == "3"))
            {
                newSlot.SetHotKey("3");
            }
        }
        else if (staticItem == InventorySlot.StaticSlot.printReader)
        {
            if (!slots.Exists(item => item.hotkey == "4"))
            {
                newSlot.SetHotKey("4");
            }
        }
        else if (staticItem == InventorySlot.StaticSlot.coin)
        {
            if (!slots.Exists(item => item.hotkey == "5"))
            {
                newSlot.SetHotKey("5");
            }
        }

        slots.Add(newSlot);

        //Update index references
        for (int i = 0; i < slots.Count; i++)
        {
            InventorySlot s = slots[i];
            s.index = i;

            //Setup new
            if (s.spawnedSegment == null)
            {
                if(staticItem == InventorySlot.StaticSlot.holster)
                {
                    s.spawnedSegment = BioScreenController.Instance.nothingSquare;
                    BioScreenController.Instance.nothingSquare.Setup(s);
                }
                else
                {
                    s.spawnedSegment = BioScreenController.Instance.SpawnSlotObject(s);
                }
            }

            //Update even if existing
            //s.spawnedSegment.UpdateSegment(s);
            s.spawnedSegment.OnUpdateContent();
        }

        if (BioScreenController.Instance.selectedSlot == null)
        {
            BioScreenController.Instance.SelectSlot(FirstPersonItemController.Instance.slots.Find(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.holster));
        }

        return newSlot;
    }

    public void RemoveSpecificStaticSlot(InventorySlot.StaticSlot staticItem)
    {
        InventorySlot newSlot = slots.Find(item => item.isStatic == staticItem);
        if (newSlot == null) return;

        EmptySlot(newSlot);

        if (newSlot.spawnedSegment != null)
        {
            Destroy(newSlot.spawnedSegment.gameObject);
        }

        slots.Remove(newSlot);

        //Update index references
        for (int i = 0; i < slots.Count; i++)
        {
            InventorySlot s = slots[i];
            s.index = i;

            //Setup new
            if (s.spawnedSegment == null)
            {
                //GameObject newSegment = Instantiate(PrefabControls.Instance.radialSelectionSegment, InterfaceController.Instance.radialParent);
                //s.spawnedSegment = newSegment.GetComponent<RadialSegmentController>();

                s.spawnedSegment = BioScreenController.Instance.SpawnSlotObject(s);
            }

            //Update even if existing
            //s.spawnedSegment.UpdateSegment(s);
            s.spawnedSegment.OnUpdateContent();
        }

        if (BioScreenController.Instance.selectedSlot == null)
        {
            BioScreenController.Instance.SelectSlot(slots.Find(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.holster));
        }
    }

    //Check whether the player has money
    public void PlayerMoneyCheck()
    {
        if(GameplayController.Instance.money > 0)
        {
            if(!slots.Exists(item => item.isStatic == InventorySlot.StaticSlot.coin))
            {
                AddSpecificStaticSlot(InventorySlot.StaticSlot.coin);
            }
        }
        else if(GameplayController.Instance.money <= 0)
        {
            if (slots.Exists(item => item.isStatic == InventorySlot.StaticSlot.coin))
            {
                RemoveSpecificStaticSlot(InventorySlot.StaticSlot.coin);
            }
        }
    }

    //Pick up item to slot (returns true if successful)
    public bool PickUpItem(Interactable pickUpThis, bool switchToNew = false, bool allowSwap = false, bool enableFullMessage = true, bool enablePickupMessage = true, bool playSound = true)
    {
        if (pickUpThis == null) return false;

        bool ret = false;
        Game.Log("Player: Attempting to pick up item: " + pickUpThis.GetName());

        if(pickUpThis.preset.name == "PrintScanner")
        {
            AddSpecificStaticSlot(InventorySlot.StaticSlot.printReader);
            pickUpThis.Delete();
            return true;
        }
        else if (pickUpThis.preset.name == "LockpickKit")
        {
            ActionController.Instance.TakeLockpickKit(pickUpThis, null, Player.Instance);
            return true;
        }

        //Find an empty slot...
        InventorySlot emptySlot = slots.Find(item => item.isStatic == InventorySlot.StaticSlot.nonStatic && item.GetInteractable() == null);

        //Otherwise swap with item in current slot...
        if(emptySlot == null && allowSwap && BioScreenController.Instance.selectedSlot != null && BioScreenController.Instance.selectedSlot.isStatic == InventorySlot.StaticSlot.nonStatic)
        {
            emptySlot = BioScreenController.Instance.selectedSlot;

            if(emptySlot.interactableID > -1)
            {
                EmptySlot(emptySlot, false, false, playSound: false);
            }

            switchToNew = true; //Swapping forces switch to new
        }
        else if(emptySlot == null && enableFullMessage)
        {
            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "inventory full"), InterfaceControls.Icon.agent);

            if (Game.Instance.displayExtraControlHints && !CutSceneController.Instance.cutSceneActive)
            {
                ControlsDisplayController.Instance.DisplayControlIconAfterDelay(1.75f, InteractablePreset.InteractionKey.WeaponSelect, "Inventory", InterfaceControls.Instance.controlIconDisplayTime);
            }
        }

        if(emptySlot != null)
        {
            //Add steal fine
            if(pickUpThis.spawnNode != null && pickUpThis.spawnNode.gameLocation != null && pickUpThis.spawnNode.gameLocation.thisAsAddress != null)
            {
                if(InteractionController.Instance.GetValidPlayerActionIllegal(pickUpThis, pickUpThis.spawnNode))
                {
                    //Add peril if worth more than 1
                    if (pickUpThis.val > 1)
                    {
                        StatusController.Instance.AddFineRecord(pickUpThis.spawnNode.gameLocation.thisAsAddress, pickUpThis, StatusController.CrimeType.theft, true);
                    }
                }

                //Remove vandalism
                pickUpThis.spawnNode.gameLocation.thisAsAddress.RemoveVandalism(pickUpThis);
                StatusController.Instance.RemoveFineRecord(null, pickUpThis, StatusController.CrimeType.vandalism, matchAddress: false);
            }

            //Trigger found on evidence if applicable
            if (pickUpThis != null && pickUpThis.evidence != null)
            {
                pickUpThis.evidence.SetFound(true);
            }

            emptySlot.SetSegmentContent(pickUpThis);

            if (enablePickupMessage)
            {
                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "Added to inventory") + ": " + pickUpThis.name, InterfaceControls.Icon.hand);

                if (Game.Instance.displayExtraControlHints && !CutSceneController.Instance.cutSceneActive)
                {
                    ControlsDisplayController.Instance.DisplayControlIconAfterDelay(1.75f, InteractablePreset.InteractionKey.WeaponSelect, "Inventory", InterfaceControls.Instance.controlIconDisplayTime);
                }
            }

            ret = true;

            if(playSound) AudioController.Instance.Play2DSound(AudioControls.Instance.pickUpItem);

            //An update of available upgrades is needed
            if (pickUpThis.syncDisk != null || pickUpThis.preset.specialCaseFlag == InteractablePreset.SpecialCase.syncDiskUpgrade) UpgradesController.Instance.UpdateUpgrades();

            if (switchToNew && !InteractionController.Instance.dialogMode)
            {
                BioScreenController.Instance.SelectSlot(emptySlot, false);
            }
        }

        Game.Log("Player: ...Successful pick up: " + ret);

        return ret;
    }

    //Is there an available empty slot in the inventory?
    public bool IsSlotAvailable()
    {
        return slots.Exists(item => item.isStatic == InventorySlot.StaticSlot.nonStatic && item.GetInteractable() == null);
    }

    //Drop item/empty a slot
    public void EmptySlot(InventorySlot emptySlot, bool throwObject = false, bool destroyObject = false, bool removeStolenFine = true, bool playSound = true)
    {
        if (emptySlot == null) return;
        Interactable slotInteractable = emptySlot.GetInteractable();
        emptySlot.interactableID = -1;

        //Remove all current hand items
        if (emptySlot == BioScreenController.Instance.selectedSlot)
        {
            foreach (Transform child in rightHandObjectParent)
            {
                Destroy(child.gameObject);
            }

            foreach (Transform child in leftHandObjectParent)
            {
                Destroy(child.gameObject);
            }

            BioScreenController.Instance.SelectSlot(slots.Find(item => item.isStatic == InventorySlot.StaticSlot.holster), true);
        }

        if(slotInteractable != null)
        {
            slotInteractable.SetAsNotInventory(Player.Instance.currentNode);
            if(!slotInteractable.wo) slotInteractable.ConvertToWorldObject(false);

            bool relocationAuthority = Toolbox.Instance.GetRelocateAuthority(Player.Instance, slotInteractable);

            if (throwObject)
            {
                slotInteractable.MoveInteractable(rightHandObjectParent.transform.position, rightHandObjectParent.transform.eulerAngles, false); //Face objects towards player
                slotInteractable.LoadInteractableToWorld(false, true);

                //Set physics to simulate throw...
                slotInteractable.controller.DropThis(true);
            }
            else if (destroyObject)
            {
                slotInteractable.MarkAsTrash(true);
                slotInteractable.RemoveFromPlacement();
            }
            else
            {
                Game.Log("Player: Put down object...");

                //Close enough to spawn position to put back?
                float spawnDist = Vector3.Distance(CameraController.Instance.cam.transform.position, slotInteractable.spWPos);
                float interactionDistance = slotInteractable.GetReachDistance();

                if (spawnDist <= interactionDistance && slotInteractable.spR)
                {
                    Game.Log("Player: Put back to original spawn position: " + slotInteractable.wPos);
                    slotInteractable.originalPosition = false;
                    slotInteractable.SetOriginalPosition(true, true);

                    slotInteractable.MoveInteractable(slotInteractable.spWPos, new Vector3(0, -Player.Instance.transform.eulerAngles.y, 0), relocationAuthority); //Face objects towards player
                    slotInteractable.LoadInteractableToWorld(false, true);

                    slotInteractable.ForcePhysicsActive(true, false);
                    slotInteractable.SetSpawnPositionRelevent(true); //Enable putting back
                }
                else
                {
                    Vector3 pos = InteractionController.Instance.playerCurrentRaycastHit.point;

                    if(Vector3.Distance(pos, CameraController.Instance.cam.transform.position) < 1.5f)
                    {
                        Vector3Int nodeCoord = CityData.Instance.RealPosToNodeInt(pos);
                        NewNode foundNode = null;

                        if (PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out foundNode))
                        {
                            Game.Log("Player: Put down found node");

                            slotInteractable.MoveInteractable(pos, new Vector3(0, -Player.Instance.transform.eulerAngles.y, 0), relocationAuthority); //Face objects towards player
                            slotInteractable.LoadInteractableToWorld(false, true);

                            slotInteractable.ForcePhysicsActive(true, false);
                            slotInteractable.SetSpawnPositionRelevent(true); //Enable putting back
                        }
                    }
                    else
                    {
                        Game.Log("Player: Put down found node");

                        slotInteractable.MoveInteractable(Player.Instance.currentNode.position, new Vector3(0, -Player.Instance.transform.eulerAngles.y, 0), relocationAuthority); //Face objects towards player
                        slotInteractable.LoadInteractableToWorld(false, true);

                        slotInteractable.ForcePhysicsActive(true, false);
                        slotInteractable.SetSpawnPositionRelevent(true); //Enable putting back
                    }
                }
            }

            emptySlot.SetSegmentContent(null);
            if(playSound) AudioController.Instance.Play2DSound(AudioControls.Instance.dropItem);

            //Remove theft fine if object is back where it belongs...
            if(removeStolenFine && slotInteractable != null && slotInteractable.node != null && slotInteractable.spawnNode != null && slotInteractable.spawnNode.gameLocation != null && !slotInteractable.rPl)
            {
                //Remove stolen
                if(slotInteractable.spawnNode.gameLocation == slotInteractable.node.gameLocation)
                {
                    StatusController.Instance.RemoveFineRecord(null, slotInteractable, StatusController.CrimeType.theft, matchAddress: false);
                }

                //Remove vandalism
                if(slotInteractable.originalPosition)
                {
                    StatusController.Instance.RemoveFineRecord(null, slotInteractable, StatusController.CrimeType.vandalism, matchAddress: false);
                    if (slotInteractable.spawnNode.gameLocation.thisAsAddress != null) slotInteractable.spawnNode.gameLocation.thisAsAddress.RemoveVandalism(slotInteractable);
                }
                else
                {
                    //Add vandal fine if not in position
                    if (slotInteractable.inInventory == null && slotInteractable.spawnNode.gameLocation.thisAsAddress != null && InteractionController.Instance.GetValidPlayerActionIllegal(slotInteractable, Player.Instance.currentNode, true, illegalIfNotPlayersHome: false))
                    {
                        //Add peril if worth more than 1
                        if (slotInteractable.val > 1)
                        {
                            slotInteractable.spawnNode.gameLocation.thisAsAddress.AddVandalism(slotInteractable);
                            StatusController.Instance.AddFineRecord(slotInteractable.spawnNode.gameLocation.thisAsAddress, slotInteractable, StatusController.CrimeType.vandalism, true, forcedPenalty: Mathf.RoundToInt(slotInteractable.val * GameplayControls.Instance.vandalismFineMultiplier));
                            InteractionController.Instance.SetIllegalActionActive(true);
                        }
                    }
                }
            }

            //An update of available upgrades is needed
            if(slotInteractable.syncDisk != null || slotInteractable.preset.specialCaseFlag == InteractablePreset.SpecialCase.syncDiskUpgrade) UpgradesController.Instance.UpdateUpgrades();
        }

        //Reselect slot
        //SelectSlot(slots.Find(item => item.isStatic == InventorySlot.StaticSlot.holster), false);
    }

    //Update which actions are currently active
    public void UpdateCurrentActions()
    {
        //Create a blank list for each key
        foreach (InteractablePreset.InteractionKey key in InteractionController.Instance.allInteractionKeys)
        {
            if (!currentActions.ContainsKey(key))
            {
                currentActions.Add(key, new Interactable.InteractableCurrentAction());
            }

            currentActions[key].enabled = false;
            currentActions[key].display = false;
        }

        if (drawnItem == null)
        {
            //Game.Log("Cannot update items: Drawn item is null");
            return;
        }
        else if (!finishedDrawingItem)
        {
            //Game.Log("Cannot update items: Has not finished drawing item");
            return;
        }

        List<FirstPersonItem.FPSInteractionAction> actionList = drawnItem.actions;

        //Sort list by priority
        actionList.Sort((p1, p2) => p2.action.inputPriority.CompareTo(p1.action.inputPriority)); //Using P2 first gives highest first

        //Game.Log("Update current actions: " + actionList.Count);

        for (int i = 0; i < actionList.Count; i++)
        {
            FirstPersonItem.FPSInteractionAction pre = actionList[i];

            if (pre.GetInteractionKey() == InteractablePreset.InteractionKey.none)
            {
                Game.Log("Interaction key equals none");
                continue; //Hidden action, skip...
            }

            //Find the correct entry in current actions
            Interactable.InteractableCurrentAction currentAction = currentActions[pre.GetInteractionKey()];

            if (currentAction == null)
            {
                continue;
            }

            //If this is already enabled with prior action, then don't overwrite...
            if (currentAction.enabled)
            {
                if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Skipping as this is key is already enabled");
                continue;
            }

            //If this uses strikethrough, it will be shown regardless
            if (pre.useStrikethrough)
            {
                currentAction.display = true; //Display as strikethrough if disabled this way
            }

            //...Or banned completely by the player class
            if (Player.Instance.disabledActions.Contains(pre.interactionName.ToLower()))
            {
                if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Disabled actions");
                currentAction.enabled = false; //Disable this
                currentAction.currentAction = pre; //Set as current if displayed
                continue;
            }

            //Disable if player is KO'd
            if (Player.Instance.playerKOInProgress)
            {
                if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Player KO");
                currentAction.enabled = false; //Disable this
                currentAction.currentAction = pre; //Set as current if displayed
                continue;
            }

            //Display while locked in interaction. Do this before the strikethrough check.
            if (!pre.availableWhileLockedIn && InteractionController.Instance.lockedInInteraction != null)
            {
                if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Locked in");
                currentAction.enabled = false; //Disable this
                currentAction.currentAction = pre; //Set as current if displayed
                continue;
            }

            if (!pre.availableWhileJumping && !Player.Instance.fps.m_CharacterController.isGrounded)
            {
                if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Jumping");
                currentAction.enabled = false; //Disable this
                currentAction.currentAction = pre; //Set as current if displayed
                continue;
            }

            if(pre.actionCost > 0)
            {
                if(GameplayController.Instance.money < pre.actionCost)
                {
                    if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Cost too much");
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }

            if (!pre.availableWhileIllegal)
            {
                if (Player.Instance.illegalStatus || Player.Instance.witnessesToIllegalActivity.Count > 0)
                {
                    if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Time isn't in fast forward");
                    //Game.Log("... Not available while illegal");
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }

            if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.onlyAvailableInFastForward)
            {
                if (!Player.Instance.spendingTimeMode)
                {
                    if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Time isn't in fast forward");
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.specialCase != InteractablePreset.InteractionAction.SpecialCase.availableInFastForward)
            {
                if (Player.Instance.spendingTimeMode)
                {
                    if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Time is in fast forward");
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }

            if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.furniturePlacement)
            {
                if (!PlayerApartmentController.Instance.furniturePlacementMode || PlayerApartmentController.Instance.furnPlacement == null)
                {
                    if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Furniture placement");
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.specialCase == InteractablePreset.InteractionAction.SpecialCase.decorItemPlacement)
            {
                if (InteractionController.Instance.carryingObject != null)
                {
                    if(InteractionController.Instance.carryingObject.apartmentPlacementIsValid)
                    {
                        //Success
                    }
                    else
                    {
                        if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Decor placement");
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
                else
                {
                    if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Decor placement");
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }

            if (pre.action.unavailableWhenItemSelected && BioScreenController.Instance.selectedSlot != null)
            {
                if (BioScreenController.Instance.selectedSlot.interactableID > -1 || BioScreenController.Instance.selectedSlot.isStatic != FirstPersonItemController.InventorySlot.StaticSlot.holster)
                {
                    if (pre.action.unavailableWhenItemsSelected == null || pre.action.unavailableWhenItemsSelected.Count <= 0)
                    {
                        if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Items drawn");
                        //Game.Log("... Unavailable while items drawn");
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                    else if (pre.action.unavailableWhenItemsSelected.Contains(BioScreenController.Instance.selectedSlot.GetFirstPersonItem()))
                    {
                        if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Items drawn");
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
            }

            if (pre.action.onlyAvailableWhenItemSelected)
            {
                if (BioScreenController.Instance.selectedSlot != null && !pre.action.availableWhenItemsSelected.Contains(BioScreenController.Instance.selectedSlot.GetFirstPersonItem()))
                {
                    if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Items drawn");
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }

            if (pre.availability == FirstPersonItem.AttackAvailability.never)
            {
                currentAction.enabled = false; //Disable this
                currentAction.currentAction = pre; //Set as current if displayed
                continue;
            }
            else if (pre.availability == FirstPersonItem.AttackAvailability.handcuffs)
            {
                //Must be looking at citizen, must be behind them or they must be KO'd.
                if (InteractionController.Instance.lookingAtInteractable && InteractionController.Instance.currentLookingAtInteractable.interactable.isActor != null && InteractionController.Instance.playerCurrentRaycastHit.distance <= InteractionController.Instance.currentLookingAtInteractable.interactable.GetReachDistance())
                {
                    if (InteractionController.Instance.currentLookingAtInteractable.interactable.isActor.ai != null && (InteractionController.Instance.currentLookingAtInteractable.interactable.isActor.ai.restrained || InteractionController.Instance.currentLookingAtInteractable.interactable.isActor.isDead))
                    {
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                    else if (InteractionController.Instance.currentLookingAtInteractable.interactable.isActor.isAsleep || (InteractionController.Instance.currentLookingAtInteractable.interactable.isActor.ai != null && InteractionController.Instance.currentLookingAtInteractable.interactable.isActor.ai.ko))
                    {
                        //pass
                    }
                    else
                    {
                        float halfFOV = 70;

                        //Must be within FoV of this and within range...
                        //Is this within my FOV?
                        Vector3 targetDir = Player.Instance.transform.position - InteractionController.Instance.currentLookingAtInteractable.interactable.isActor.transform.position;
                        Vector3 cameraForward = -InteractionController.Instance.currentLookingAtInteractable.interactable.isActor.transform.forward;

                        float angleToOther = Vector3.Angle(targetDir, cameraForward); //Local angle

                        if (angleToOther >= -halfFOV && angleToOther <= halfFOV) //Within fov
                        {
                            //pass
                        }
                        else
                        {
                            currentAction.enabled = false; //Disable this
                            currentAction.currentAction = pre; //Set as current if displayed
                            continue;
                        }
                    }
                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.availability == FirstPersonItem.AttackAvailability.behindCitizen)
            {
                //Must be looking at citizen, must be behind them and not KO'd
                if (InteractionController.Instance.lookingAtInteractable && InteractionController.Instance.currentLookingAtInteractable.interactable.isActor != null && InteractionController.Instance.playerCurrentRaycastHit.distance <= InteractionController.Instance.currentLookingAtInteractable.interactable.GetReachDistance())
                {
                    if (InteractionController.Instance.currentLookingAtInteractable.interactable.isActor.ai != null && InteractionController.Instance.currentLookingAtInteractable.interactable.isActor.ai.ko)
                    {
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                    else
                    {
                        float halfFOV = 70;

                        //Must be within FoV of this and within range...
                        //Is this within my FOV?
                        Vector3 targetDir = Player.Instance.transform.position - InteractionController.Instance.currentLookingAtInteractable.interactable.isActor.transform.position;
                        Vector3 cameraForward = -InteractionController.Instance.currentLookingAtInteractable.interactable.isActor.transform.forward;

                        float angleToOther = Vector3.Angle(targetDir, cameraForward); //Local angle

                        if (angleToOther >= -halfFOV && angleToOther <= halfFOV) //Within fov
                        {
                            //pass
                        }
                        else
                        {
                            currentAction.enabled = false; //Disable this
                            currentAction.currentAction = pre; //Set as current if displayed
                            continue;
                        }
                    }
                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.availability == FirstPersonItem.AttackAvailability.onConsuming)
            {
                if (!isConsuming)
                {
                    if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Not consuming");
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.availability == FirstPersonItem.AttackAvailability.onNotConsuming)
            {
                if (isConsuming)
                {
                    if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Consuming");
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.availability == FirstPersonItem.AttackAvailability.onNotConsumingButLeftovers)
            {
                if (!isConsuming && BioScreenController.Instance.selectedSlot != null && BioScreenController.Instance.selectedSlot.interactableID > -1 && BioScreenController.Instance.selectedSlot.GetInteractable().cs > 0f)
                {
                    //pass
                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.availability == FirstPersonItem.AttackAvailability.onRaisedButLeftovers)
            {
                if (isRaised && BioScreenController.Instance.selectedSlot != null && BioScreenController.Instance.selectedSlot.interactableID > -1 && BioScreenController.Instance.selectedSlot.GetInteractable().cs > 0f)
                {
                    //pass
                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.availability == FirstPersonItem.AttackAvailability.nearPutDown)
            {
                if (!isConsuming)
                {
                    //Am I near the spawn pos?
                    Interactable heldInteractable = BioScreenController.Instance.selectedSlot.GetInteractable();
                    currentAction.overrideInteractionName = string.Empty;

                    if (heldInteractable != null && heldInteractable.spR)
                    {
                        float spawnDist = Vector3.Distance(CameraController.Instance.cam.transform.position, heldInteractable.spWPos);
                        float interactionDistance = heldInteractable.GetReachDistance();

                        if (spawnDist <= interactionDistance && heldInteractable.spR)
                        {
                            currentAction.overrideInteractionName = "Put Back";
                        }
                        else
                        {
                            if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Out of interaction distance for put back (" + spawnDist + "/" + interactionDistance + ")");
                            currentAction.enabled = false; //Disable this
                            currentAction.currentAction = pre; //Set as current if displayed
                            //continue;
                        }
                    }

                    //Am I near a flat surface? If so I can put it here...
                    if (InteractionController.Instance.playerCurrentRaycastHit.transform != null && InteractionController.Instance.playerCurrentRaycastHit.distance <= GameplayControls.Instance.interactionRange)
                    {
                        bool placable = false;

                        if(InteractionController.Instance.lookingAtInteractable)
                        {
                            if(InteractionController.Instance.currentLookingAtInteractable != null && InteractionController.Instance.currentLookingAtInteractable.interactable != null)
                            {
                                if(!InteractionController.Instance.currentLookingAtInteractable.interactable.preset.spawnable && InteractionController.Instance.currentLookingAtInteractable.interactable.preset.prefab == null)
                                {
                                    //This must be an integrated interactable
                                    placable = true;
                                }
                            }
                        }
                        else if(InteractionController.Instance.playerCurrentRaycastHit.transform.gameObject.CompareTag("Untagged") || InteractionController.Instance.playerCurrentRaycastHit.transform.gameObject.CompareTag("FloorMesh"))
                        {
                            placable = true;
                        }

                        //Must be a static object (isStatic does not work in builds, instead check for no tag applied)
                        if (placable)
                        {
                            Vector3 hitNormal = InteractionController.Instance.playerCurrentRaycastHit.normal;

                            //Detect a face-up surface
                            if (hitNormal == new Vector3(0, 1, 0))
                            {
                                //pass
                                if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Found valid face up surface");
                                currentAction.enabled = true; //Disable this
                                currentAction.currentAction = pre; //Set as current if displayed
                            }
                            else
                            {
                                if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Put down normal is not UP");
                                currentAction.enabled = false; //Disable this
                                currentAction.currentAction = pre; //Set as current if displayed
                                continue;
                            }
                        }
                        else
                        {
                            if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: Non-static raycast target (" + InteractionController.Instance.playerCurrentRaycastHit.transform.name + ")");
                            currentAction.enabled = false; //Disable this
                            currentAction.currentAction = pre; //Set as current if displayed
                            continue;
                        }
                    }
                    else
                    {
                        if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " Not available: No raycast hit");
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.availability == FirstPersonItem.AttackAvailability.onRaised)
            {
                if (!isRaised)
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.availability == FirstPersonItem.AttackAvailability.onNotRaised)
            {
                if (isRaised)
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.availability == FirstPersonItem.AttackAvailability.codebreaker)
            {
                if (Player.Instance.computerInteractable != null || (InteractionController.Instance.lookingAtInteractable && InteractionController.Instance.playerCurrentRaycastHit.distance <= InteractionController.Instance.currentLookingAtInteractable.interactable.GetReachDistance()))
                {
                    //Is keypad
                    if(InteractionController.Instance.currentLookingAtInteractable != null && InteractionController.Instance.currentLookingAtInteractable.interactable.preset == InteriorControls.Instance.keypad)
                    {
                        if(!InteractionController.Instance.currentLookingAtInteractable.interactable.locked)
                        {
                            currentAction.enabled = false; //Disable this
                            currentAction.currentAction = pre; //Set as current if displayed
                            continue;
                        }
                    }
                    //Is computer
                    else if(Player.Instance.computerInteractable != null && Player.Instance.computerInteractable.controller != null)
                    {
                        bool computerValid = false;

                        if(Player.Instance.computerInteractable.controller.computer.currentApp == InteriorControls.Instance.loginApp)
                        {
                            ComputerLogin login = Player.Instance.computerInteractable.controller.computer.GetComponentInChildren<ComputerLogin>();

                            if(login != null)
                            {
                                if (login.loginSelection.selected != null)
                                {
                                    computerValid = true;
                                }
                            }
                        }

                        if(!computerValid)
                        {
                            currentAction.enabled = false; //Disable this
                            currentAction.currentAction = pre; //Set as current if displayed
                            continue;
                        }
                    }
                    else
                    {
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.availability == FirstPersonItem.AttackAvailability.doorWedge)
            {
                Game.Log("Wedge availability...");

                if (InteractionController.Instance.lookingAtInteractable && (InteractionController.Instance.currentLookingAtInteractable.interactable.preset == InteriorControls.Instance.door || InteractionController.Instance.currentLookingAtInteractable.interactable.preset == InteriorControls.Instance.peekUnderDoor) && InteractionController.Instance.playerCurrentRaycastHit.distance <= InteractionController.Instance.currentLookingAtInteractable.interactable.GetReachDistance())
                {
                    //Door is open
                    if (InteractionController.Instance.currentLookingAtInteractable.interactable.sw0)
                    {
                        Game.Log("... Door is open");
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }

                    //Lower half of door...
                    if((InteractionController.Instance.currentLookingAtInteractable.interactable.preset == InteriorControls.Instance.door && InteractionController.Instance.playerCurrentRaycastHit.point.y <= InteractionController.Instance.currentLookingAtInteractable.transform.position.y + 1f) || InteractionController.Instance.currentLookingAtInteractable.interactable.preset == InteriorControls.Instance.peekUnderDoor)
                    {
                        Game.Log("... Lower half");
                    }
                    else
                    {
                        Game.Log("... Upper half");
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
                else
                {
                    Game.Log("... Not looking at correct interactable");
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if(pre.availability == FirstPersonItem.AttackAvailability.giveItem)
            {
                if (!isConsuming && InteractionController.Instance.lookingAtInteractable && InteractionController.Instance.currentLookingAtInteractable.interactable.isActor != null && InteractionController.Instance.playerCurrentRaycastHit.distance <= InteractionController.Instance.currentLookingAtInteractable.interactable.GetReachDistance())
                {
                    
                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }
            else if (pre.availability == FirstPersonItem.AttackAvailability.tracker)
            {
                //Game.Log("Tracker availability...");

                //If able to reach actor
                if (!isConsuming && InteractionController.Instance.lookingAtInteractable && InteractionController.Instance.currentLookingAtInteractable.interactable.isActor != null && InteractionController.Instance.playerCurrentRaycastHit.distance <= GameplayControls.Instance.interactionRange)
                {
                    //Game.Log("Actor...");
                }
                //If looking at wall
                else if(!isConsuming && InteractionController.Instance.playerCurrentRaycastHit.transform != null && InteractionController.Instance.playerCurrentRaycastHit.distance <= GameplayControls.Instance.interactionRange)
                {
                    bool placable = false;

                    if (InteractionController.Instance.lookingAtInteractable)
                    {
                        if (InteractionController.Instance.currentLookingAtInteractable != null && InteractionController.Instance.currentLookingAtInteractable.interactable != null)
                        {
                            if (!InteractionController.Instance.currentLookingAtInteractable.interactable.preset.spawnable && InteractionController.Instance.currentLookingAtInteractable.interactable.preset.prefab == null)
                            {
                                //This must be an integrated interactable
                                placable = true;
                            }
                        }
                    }
                    else if (InteractionController.Instance.playerCurrentRaycastHit.transform.gameObject.CompareTag("Untagged") || InteractionController.Instance.playerCurrentRaycastHit.transform.gameObject.CompareTag("WallsMesh"))
                    {
                        placable = true;
                    }

                    //Must be a static object (isStatic does not work in builds, instead check for no tag applied)
                    if (placable)
                    {
                        Vector3 hitNormal = InteractionController.Instance.playerCurrentRaycastHit.normal;

                        //Detect a face-sideways surface
                        if (hitNormal.y == 0)
                        {
                            //Game.Log("Wall...");
                            //pass
                        }
                        else
                        {
                            currentAction.enabled = false; //Disable this
                            currentAction.currentAction = pre; //Set as current if displayed
                            continue;
                        }
                    }
                    else
                    {
                        currentAction.enabled = false; //Disable this
                        currentAction.currentAction = pre; //Set as current if displayed
                        continue;
                    }
                }
                else
                {
                    currentAction.enabled = false; //Disable this
                    currentAction.currentAction = pre; //Set as current if displayed
                    continue;
                }
            }

            //if(currentAction.currentAction != null)
            //{
            //    Game.Log("Set current action for " + currentAction.currentAction.key.ToString());
            //}

            //Below means actions are valid
            //If we've reached here, then this is a valid action for this key
            currentAction.display = true;
            currentAction.enabled = true;
            currentAction.currentAction = pre;

            if (pre.action.debug) Game.Log("Debug: Action " + pre.action.name + " is valid: " + currentAction.enabled);
        }

        //Game.Log(enablePrimary.ToString() + enableSecondary.ToString() + enableAlternate.ToString());
        //Game.Log(displayPrimary.ToString() + displaySecondary.ToString() + displayAlternate.ToString());

        //InteractionController.Instance.UpdateInteractionText();
        //InteractionController.Instance.OnPlayerLookAtChange(); //Force look @ change
    }

    //Triggered when switching between items or on no item
    public void OnHolster()
    {
        Game.Log("Player: Holster...");

        listenForHolster = false;

        RefreshHeldObjects();
    }

    public void RefreshHeldObjects()
    {
        //Remove all current hand items
        foreach (Transform child in rightHandObjectParent)
        {
            if(child != null) Destroy(child.gameObject);
        }

        foreach (Transform child in leftHandObjectParent)
        {
            if (child != null) Destroy(child.gameObject);
        }

        //Stop existing loop
        if (activeLoop != null)
        {
            AudioController.Instance.StopSound(activeLoop, AudioController.StopType.fade, "New weapon");
        }

        if (currentItem != null)
        {
            //Now create new items
            if (currentItem.leftHandObject != null)
            {
                GameObject lHandPrefab = currentItem.leftHandObject;

                if (currentItem.useAlternateTrashObjects && currentItem.leftHandObjectTrash != null)
                {
                    if (BioScreenController.Instance.selectedSlot != null)
                    {
                        Interactable inter = BioScreenController.Instance.selectedSlot.GetInteractable();

                        if (inter != null && inter.cs <= 0f)
                        {
                            lHandPrefab = currentItem.leftHandObjectTrash;
                        }
                    }
                }

                if(lHandPrefab != null)
                {
                    GameObject newLeft = Instantiate(lHandPrefab, leftHandObjectParent);
                    newLeft.layer = 5;
                    newLeft.transform.localScale = currentItem.spawnScale;

                    MeshRenderer[] meshes = newLeft.GetComponentsInChildren<MeshRenderer>();
                    int mask = ~0;

                    foreach (MeshRenderer mesh in meshes)
                    {
                        mesh.renderingLayerMask = (uint)mask;
                        mesh.gameObject.layer = 5;
                        mesh.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    }
                }
            }

            if (currentItem.rightHandObject != null || currentItem.useFoodSlotItem)
            {
                GameObject newRight = null;
                GameObject rHandPrefab = currentItem.rightHandObject;

                if (currentItem.useAlternateTrashObjects && currentItem.rightHandObjectTrash != null)
                {
                    if (BioScreenController.Instance.selectedSlot != null)
                    {
                        Interactable inter = BioScreenController.Instance.selectedSlot.GetInteractable();

                        if (inter != null && inter.cs <= 0f)
                        {
                            rHandPrefab = currentItem.rightHandObjectTrash;
                        }
                    }
                }

                if (currentItem.useFoodSlotItem && BioScreenController.Instance.selectedSlot != null)
                {
                    Interactable inter = BioScreenController.Instance.selectedSlot.GetInteractable();

                    if(inter != null)
                    {
                        newRight = Instantiate(inter.preset.prefab, rightHandObjectParent);
                        newRight.transform.localPosition = inter.preset.fpsItemOffset;
                        newRight.transform.localEulerAngles = inter.preset.fpsItemRotation;

                        //Remove colliders
                        foreach (Collider c in newRight.GetComponentsInChildren<Collider>())
                        {
                            c.enabled = false;
                        }

                        InteractableController ic = newRight.GetComponent<InteractableController>();

                        if (ic != null)
                        {
                            ic.enabled = false;
                        }
                    }
                    else
                    {
                        if(currentItem.rightHandObject != null) Game.Log("Spawning new right hand object: " + currentItem.rightHandObject.name);
                        if (rHandPrefab != null) newRight = Instantiate(rHandPrefab, rightHandObjectParent);
                    }
                }
                else
                {
                    if (currentItem.rightHandObject != null) Game.Log("Spawning new right hand object: " + currentItem.rightHandObject.name);
                    if(rHandPrefab != null) newRight = Instantiate(rHandPrefab, rightHandObjectParent);
                }

                newRight.layer = 5;
                newRight.transform.localScale = currentItem.spawnScale;

                MeshRenderer[] meshes = newRight.GetComponentsInChildren<MeshRenderer>();
                int mask = ~0;

                foreach (MeshRenderer mesh in meshes)
                {
                    mesh.renderingLayerMask = (uint)mask;
                    mesh.gameObject.layer = 5;
                    mesh.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                }
            }

            //Set arms models active or not
            InterfaceController.Instance.firstPersonModel.SetActive(currentItem.modelActive);

            if (currentItem.equipEvent != null)
            {
                if (currentItem.equipSoundDelay <= 0f)
                {
                    AudioController.Instance.Play2DSound(currentItem.equipEvent);

                    if (currentItem.activeLoop != null)
                    {
                        activeLoop = AudioController.Instance.Play2DLooping(currentItem.activeLoop);
                    }
                }
                else
                {
                    equipSoundDelay = currentItem.equipSoundDelay;
                }
            }
        }
        else
        {
            Game.Log("Current item is null");
        }
    }

    //Generate skin colour materials
    public void GenerateSkinColourMaterials()
    {
        fistMaterial = Instantiate(GameplayControls.Instance.fistMaterial) as Material;
        fingerLowerMaterial = Instantiate(GameplayControls.Instance.fingerLowerMaterial) as Material;
        fingerUpperMaterial = Instantiate(GameplayControls.Instance.fingerUpperMaterial) as Material;
        fingerTipMaterial = Instantiate(GameplayControls.Instance.fingerTipMaterial) as Material;
        thumbJointMaterial = Instantiate(GameplayControls.Instance.thumbJointMaterial) as Material;

        //Set colours: Color 1 is skin colour
        fistMaterial.SetColor("_Color1", Game.Instance.playerSkinColour);
        fingerLowerMaterial.SetColor("_Color1", Game.Instance.playerSkinColour);
        fingerUpperMaterial.SetColor("_Color1", Game.Instance.playerSkinColour);
        fingerTipMaterial.SetColor("_Color1", Game.Instance.playerSkinColour);
        thumbJointMaterial.SetColor("_Color1", Game.Instance.playerSkinColour);

        //Finger tips are closer to white and use colour 2
        fingerTipMaterial.SetColor("_Color2", Color.Lerp(Game.Instance.playerSkinColour, Color.white, 0.7f));
    }

    //Set the first person item
    public void SetFirstPersonItem(FirstPersonItem newItem, bool forceSwitch = false)
    {
        if (currentItem != newItem || forceSwitch)
        {
            if (currentItem != null) Game.Log("Player: Set first person item: " + newItem.name);

            //Stop active loop
            if (activeLoop != null)
            {
                AudioController.Instance.StopSound(activeLoop, AudioController.StopType.fade, "weapon switch");
            }

            if(SessionData.Instance.enableTutorialText)
            {
                if (newItem.name == "fingerptintScanner")
                {
                    SessionData.Instance.TutorialTrigger("fingerprints");
                }
                else if (newItem.name == "handcuffs")
                {
                    if (!SessionData.Instance.tutorialTextTriggered.Contains("arresting"))
                    {
                        PopupMessageController.Instance.TutorialMessage("arresting");
                        SessionData.Instance.tutorialTextTriggered.Add("arresting");
                    }
                }
            }

            //Holster audio
            if (currentItem != null && currentItem.holsterEvent != null)
            {
                if (currentItem.holsterSoundDelay <= 0f)
                {
                    AudioController.Instance.Play2DSound(currentItem.holsterEvent);
                }
                else
                {
                    holsterSoundDelay = currentItem.holsterSoundDelay;
                }
            }

            //Remove any delayed equip sounds
            equipSoundDelay = 0;

            listenForHolster = true;
            Game.Log("Player: Listen for holster...");

            //Compatibility check
            if (!newItem.compatibleWithHidden && Player.Instance.isHiding) return;
            if (!newItem.compatibleWithLockedIn && InteractionController.Instance.lockedInInteraction != null) return;

            previousItem = currentItem;
            currentItem = newItem;
            InteractionController.Instance.UpdateInteractionText();

            if (currentItem.modelActive)
            {
                InterfaceController.Instance.firstPersonModel.SetActive(true);
            }

            //Update interactions
            if (InteractionController.Instance.currentLookingAtInteractable != null)
            {
                InteractionController.Instance.currentLookingAtInteractable.interactable.UpdateCurrentActions();
            }

            InteractionController.Instance.UpdateInteractionText();

            if (InterfaceController.Instance.firstPersonAnimator != null) InterfaceController.Instance.firstPersonAnimator.SetBool("MainAttackActive", false); //Force no attack

            ReadyNewItemDraw();

            if(currentItem != null && currentItem.triggerTutorial != null && currentItem.triggerTutorial.Length > 0)
            {
                SessionData.Instance.TutorialTrigger(currentItem.triggerTutorial);
            }

            Game.Log("Player: Set new first person item: " + currentItem.name);
        }
    }

    public void SetFirstPersonSkinColour()
    {
        //Set material to skin colour...
        MeshRenderer[] rends = InterfaceController.Instance.firstPersonModel.GetComponentsInChildren<MeshRenderer>();

        foreach (MeshRenderer ren in rends)
        {
            //Replace materials
            if (ren.sharedMaterial.name == "Fist")
            {
                ren.sharedMaterial = fistMaterial;
            }
            else if (ren.sharedMaterial.name == "FingerLower")
            {
                ren.sharedMaterial = fingerLowerMaterial;
            }
            else if (ren.sharedMaterial.name == "FingerUpper")
            {
                ren.sharedMaterial = fingerUpperMaterial;
            }
            else if (ren.sharedMaterial.name == "FingerTip")
            {
                ren.sharedMaterial = fingerTipMaterial;
            }
            else if (ren.sharedMaterial.name == "ThumbUpper")
            {
                ren.sharedMaterial = thumbJointMaterial;
            }
        }
    }

    public void ReadyNewItemDraw()
    {
        Game.Log("Player: Ready new item:" + currentItem.name + " " + currentItem.slotPriority);

        drawnItem = currentItem;
        finishedDrawingItem = false;
        listenForDrawFinish = true;

        //Update interactions
        InteractionController.Instance.UpdateInteractionText();

        //Set animation speeds
        if (InterfaceController.Instance.firstPersonAnimator != null)
        {
            InterfaceController.Instance.firstPersonAnimator.SetFloat("DrawSpeed", currentItem.drawSpeed);
            InterfaceController.Instance.firstPersonAnimator.SetFloat("HolsterSpeed", currentItem.holsterSpeed);

            //Load clock text
            TextMeshPro[] watchText = InterfaceController.Instance.firstPersonModel.GetComponentsInChildren<TextMeshPro>();

            foreach (TextMeshPro tmp in watchText)
            {
                if (tmp.CompareTag("WatchTimeText"))
                {
                    SessionData.Instance.newWatchTimeText = tmp;
                    SessionData.Instance.UpdateUIClock();
                }
                else if (tmp.CompareTag("WatchDateText"))
                {
                    SessionData.Instance.newWatchDateText = tmp;
                    SessionData.Instance.UpdateUIDay();
                }
            }

            //Play animation
            InterfaceController.Instance.firstPersonAnimator.SetInteger("CurrentItem", currentItem.slotPriority);
        }
    }

    public void FinishedDrawingNewItem()
    {
        Game.Log("Player: Finished drawing item");

        finishedDrawingItem = true;
        listenForDrawFinish = false;

        //Update interactions
        InteractionController.Instance.UpdateInteractionText();
    }

    //Handles attack animations
    public void OnInteraction(InteractablePreset.InteractionKey input)
    {
        if (attackMainDelay <= 0f)
        {
            Game.Log("Player: FPS Input " + input.ToString());

            Interactable.InteractableCurrentAction action = null;

            if (currentActions.TryGetValue(input, out action))
            {
                FirstPersonItem.FPSInteractionAction fpsAction = action.currentAction as FirstPersonItem.FPSInteractionAction;

                //Play sound
                if (action.currentAction.soundEvent != null && action.currentAction.playOnTrigger)
                {
                    AudioController.Instance.PlayWorldOneShot(action.currentAction.soundEvent, Player.Instance, Player.Instance.currentNode, Player.Instance.lookAtThisTransform.position);
                }

                if (fpsAction.useCameraJolt)
                {
                    Vector3 joltDir = new Vector3(Toolbox.Instance.Rand(fpsAction.joltXRange.x, fpsAction.joltXRange.y), Toolbox.Instance.Rand(fpsAction.joltYRange.x, fpsAction.joltYRange.y), Toolbox.Instance.Rand(fpsAction.joltZRange.x, fpsAction.joltZRange.y));
                    Player.Instance.fps.JoltCamera(joltDir, fpsAction.joltAmplitude, fpsAction.joltSpeed);
                }

                //Special cases (can be used as to not rely on animation events)
                if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.attack)
                {
                    InterfaceController.Instance.firstPersonAnimator.SetFloat("AttackSpeed", fpsAction.attackMainSpeed);
                    InterfaceController.Instance.firstPersonAnimator.SetTrigger("Attack"); //Trigger attack

                    //Do transition
                    if (fpsAction.attackTrasition != null)
                    {
                        Player.Instance.TransformPlayerController(fpsAction.attackTrasition, null, null, null);
                    }

                    //Play sound
                    if(fpsAction.attackEvent != null)
                    {
                        AudioController.Instance.PlayWorldOneShot(fpsAction.attackEvent, Player.Instance, Player.Instance.currentNode, Player.Instance.lookAtThisTransform.position);
                    }

                    attackMainDelay = fpsAction.attackDelay;
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.block)
                {
                    Block();
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.handcuff)
                {
                    Handcuff();
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.takedown)
                {
                    Takedown();
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.punch)
                {
                    InterfaceController.Instance.firstPersonAnimator.SetTrigger("Punch");

                    //Play sound
                    if (fpsAction.attackEvent != null)
                    {
                        AudioController.Instance.PlayWorldOneShot(fpsAction.attackEvent, Player.Instance, Player.Instance.currentNode, Player.Instance.lookAtThisTransform.position);
                    }

                    attackMainDelay = fpsAction.attackDelay;
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.consumeTrue)
                {
                    SetConsuming(true);
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.consumeFalse)
                {
                    SetConsuming(false);
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.takeOne)
                {
                    TakeOne();
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.takeBriefcaseCash)
                {
                    TakeOne();
                    GameplayController.Instance.AddMoney(1000, true, "Stolen from briefcase");

                    foreach(KeyValuePair<int, SideJob> pair in SideJobController.Instance.allJobsDictionary)
                    {
                        SideJob j = pair.Value as SideJobStealBriefcase;

                        if(j != null)
                        {
                            if(j.activeJobItems.ContainsValue(BioScreenController.Instance.selectedSlot.GetInteractable()))
                            {
                                j.TriggerFail("Briefcase has been opened");
                                break;
                            }
                        }
                    }
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.putDown)
                {
                    SetConsuming(false);
                    PutDown();
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.raiseTrue)
                {
                    SetRaised(true);
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.openBriefcaseBomb)
                {
                    SetRaised(true);

                    if (BioScreenController.Instance.selectedSlot != null && BioScreenController.Instance.selectedSlot.interactableID > -1)
                    {
                        Toolbox.Instance.TriggerBriefcaseBomb(BioScreenController.Instance.selectedSlot.GetInteractable());

                        foreach (KeyValuePair<int, SideJob> pair in SideJobController.Instance.allJobsDictionary)
                        {
                            SideJob j = pair.Value as SideJobStealBriefcase;

                            if (j != null)
                            {
                                if (j.activeJobItems.ContainsValue(BioScreenController.Instance.selectedSlot.GetInteractable()))
                                {
                                    j.TriggerFail("Briefcase has been opened");
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.raiseFalse)
                {
                    SetRaised(false);
                }
                else if(fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.takePicture)
                {
                    TakePicture();
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.placeCodebreaker)
                {
                    PlaceCodebreaker();
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.placeDoorWedge)
                {
                    PlaceDoorWedge();
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.placeTracker)
                {
                    PlaceTracker();
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.placeFlashbomb)
                {
                    PlaceGrenade(InteriorControls.Instance.activeFlashbomb);
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.placeIncapacitator)
                {
                    PlaceGrenade(InteriorControls.Instance.activeIncapacitator);
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.placeFurniture)
                {
                    PlaceFurniture();
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.cancelFurniture)
                {
                    CancelFurniture();
                }
                else if (fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.give)
                {
                    Give();
                }
                else if(fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.rotateFurnLeft)
                {
                    PlayerApartmentController.Instance.RotateFurn(false);
                }
                else if(fpsAction.mainSpecialAction == FirstPersonItem.SpecialAction.rotateFurnRight)
                {
                    PlayerApartmentController.Instance.RotateFurn(true);
                }

                if(fpsAction.isHidingPlace && !Player.Instance.isHiding && (!fpsAction.onlyHidingPlaceIfPublic || !Player.Instance.isTrespassing))
                {
                     Player.Instance.SetHiding(true, null);
                }
                else if(!fpsAction.isHidingPlace && Player.Instance.isHiding && Player.Instance.hidingInteractable == null)
                {
                    Player.Instance.SetHiding(false, null);
                }
            }
        }
    }

    public void ForceHolster()
    {
        if (!forceHolstered)
        {
            forceHolstered = true;
            selectedWhenForceHolstered = BioScreenController.Instance.selectedSlot;
        }

        BioScreenController.Instance.SelectSlot(slots.Find(item => item.isStatic == InventorySlot.StaticSlot.holster));
    }

    public void RestoreItemSelection()
    {
        if (forceHolstered)
        {
            forceHolstered = false;
            BioScreenController.Instance.SelectSlot(selectedWhenForceHolstered);
            selectedWhenForceHolstered = null;
        }
    }

    public void SetEnableFirstPersonItemSelection(bool val)
    {
        enableItemSelection = val;

        if(!enableItemSelection)
        {
            BioScreenController.Instance.SetInventoryOpen(false, true);
        }
    }

    //Flashlight
    public void SetFlashlight(bool val)
    {
        if (val != flashlight)
        {
            if (flashlight)
            {
                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.flashlightOn, Player.Instance, Player.Instance.currentNode, Player.Instance.lookAtThisTransform.position);
            }
            else
            {
                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.flashlightOff, Player.Instance, Player.Instance.currentNode, Player.Instance.lookAtThisTransform.position);
            }

            Player.Instance.UpdateOverallVisibility();
        }

        flashlight = val;
        flashLightObject.SetActive(flashlight);
    }

    public void ToggleFlashlight()
    {
        SetFlashlight(!flashlight);
    }

    //Triggered when a player punched
    public void MeleeAttack()
    {
        //Calculate potential damage
        float dmg = Player.Instance.combatHeft;

        if(BioScreenController.Instance.selectedSlot != null)
        {
            if(BioScreenController.Instance.selectedSlot.isStatic == InventorySlot.StaticSlot.fists)
            {
                dmg = InteriorControls.Instance.fistsWeapon.GetAttackValue(MurderWeaponPreset.AttackValue.damage, Player.Instance);
            }
            else if(BioScreenController.Instance.selectedSlot.isStatic == InventorySlot.StaticSlot.nonStatic)
            {
                Interactable i = BioScreenController.Instance.selectedSlot.GetInteractable();

                if(i != null)
                {
                    if(i.preset.weapon != null)
                    {
                        dmg = i.preset.weapon.GetAttackValue(MurderWeaponPreset.AttackValue.damage, Player.Instance);
                    }
                }
            }
        }

        Game.Log("Player: Melee attack! Damage: " + dmg + " (combat heft: " + Player.Instance.combatHeft + ")");

        SessionData.Instance.TutorialTrigger("combat");

        //Ray ray = CameraController.Instance.cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        Ray ray = new Ray(CameraController.Instance.cam.transform.position, CameraController.Instance.cam.transform.forward);

        // Bit shift the index of the layer (20) to get a bit mask
        int layerMask = 1 << 20;

        // This would cast rays only against colliders in layer 20.
        // But instead we want to collide against everything except layer 20. The ~ operator does this, it inverts a bitmask.
        layerMask = ~layerMask;

        RaycastHit hit;

        if (Physics.SphereCast(ray, 0.2f, out hit, 2f, layerMask))
        {
            Actor hitActor = hit.transform.gameObject.GetComponentInParent<Actor>();

            if (hitActor != null)
            {
                if (hitActor.ai != null)
                {
                    //Is this actor attacking?
                    if (hitActor.ai.attackActive)
                    {
                        //Won't block
                    }

                    //Apply transition recoil state
                    Player.Instance.transitionRecoilState = true;

                    if (hitActor.currentGameLocation != null && hitActor.currentGameLocation.thisAsAddress != null)
                    {
                        StatusController.Instance.AddFineRecord(hitActor.currentGameLocation.thisAsAddress, hitActor.interactable, StatusController.CrimeType.assault, true);
                    }
                    else
                    {
                        StatusController.Instance.AddFineRecord(null, hitActor.interactable, StatusController.CrimeType.assault, true);
                    }

                    hitActor.RecieveDamage(dmg, Player.Instance, hit.point, hit.point - CameraController.Instance.cam.transform.position, CriminalControls.Instance.punchSpatter, null, SpatterSimulation.EraseMode.useDespawnTime);

                    //SFX: Did I hit his skin or clothes?
                    //Skin objects: Head, hands
                    Human hu = hitActor as Human;

                    try
                    {
                        if (hu != null && hu.outfitController != null)
                        {
                            if (hit.transform.parent.gameObject == hu.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.Head).gameObject || hit.transform.parent.gameObject == hu.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.HandLeft).gameObject || hit.transform.parent.gameObject == hu.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.HandRight).gameObject)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitFlesh, Player.Instance, hitActor.currentNode, hit.point);
                            }
                            else
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitFabric, Player.Instance, hitActor.currentNode, hit.point);
                            }
                        }
                    }
                    catch
                    {

                    }

                }
            }
            else
            {
                //Hit window
                BreakableWindowController wbc = hit.transform.gameObject.GetComponent<BreakableWindowController>();

                if (wbc != null)
                {
                    //A rough but hopefully good enough approximation of the hit world position
                    Vector3 approxHitLocation = this.transform.position;

                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitGlass, null, null, hit.point);

                    wbc.BreakWindow(approxHitLocation, -this.transform.up, Player.Instance);

                    if(BioScreenController.Instance.selectedSlot.isStatic == InventorySlot.StaticSlot.fists)
                    {
                        Player.Instance.RecieveDamage(0.01f, null, Vector3.zero, Vector3.zero, null, null);
                        Player.Instance.AddBleeding(0.5f); //Add bleeding
                    }
                }
                else
                {
                    //Hit interactable
                    InteractableController[] ic = hit.transform.gameObject.GetComponentsInChildren<InteractableController>();

                    for (int i = 0; i < ic.Length; i++)
                    {
                        InteractableController obj = ic[i];

                        if (obj != null && obj.interactable != null && obj.interactable.preset != null && obj.interactable.preset.physicsProfile != null && obj.interactable.preset.reactWithExternalStimuli)
                        {
                            obj.SetPhysics(true, Player.Instance);

                            if (obj.rb != null)
                            {
                                Vector3 force = -this.transform.up * GameplayControls.Instance.throwForce;
                                obj.rb.AddForce(force, ForceMode.VelocityChange);
                            }
                        }
                    }
                }

                //Look for override controllers first...
                MaterialOverrideController moc = hit.transform.GetComponentInChildren<MaterialOverrideController>();

                if (moc != null)
                {
                    if (moc.wood > 0.5f)
                    {
                        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitWood, Player.Instance, Player.Instance.currentNode, hit.point);
                    }
                    else if (moc.plaster > 0.5f)
                    {
                        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitPlaster, Player.Instance, Player.Instance.currentNode, hit.point);
                        if (BioScreenController.Instance.selectedSlot.isStatic == InventorySlot.StaticSlot.fists) Player.Instance.RecieveDamage(0.01f, null, Vector3.zero, Vector3.zero, null, null);
                    }
                    else if (moc.concrete > 0.5f)
                    {
                        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitConcrete, Player.Instance, Player.Instance.currentNode, hit.point);
                        if (BioScreenController.Instance.selectedSlot.isStatic == InventorySlot.StaticSlot.fists) Player.Instance.RecieveDamage(0.01f, null, Vector3.zero, Vector3.zero, null, null);
                    }
                    else if (moc.metal > 0.5f)
                    {
                        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitMetal, Player.Instance, Player.Instance.currentNode, hit.point);
                    }
                    else if (moc.glass > 0.5f)
                    {
                        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitGlass, Player.Instance, Player.Instance.currentNode, hit.point);
                    }
                    else if (moc.tile > 0.5f)
                    {
                        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitTile, Player.Instance, Player.Instance.currentNode, hit.point);
                        if (BioScreenController.Instance.selectedSlot.isStatic == InventorySlot.StaticSlot.fists) Player.Instance.RecieveDamage(0.01f, null, Vector3.zero, Vector3.zero, null, null);
                    }
                    else if (moc.fabric > 0.5f)
                    {
                        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitFabric, Player.Instance, Player.Instance.currentNode, hit.point);
                    }
                    else if (moc.carpet > 0.5f)
                    {
                        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitCarpet, Player.Instance, Player.Instance.currentNode, hit.point);
                    }
                }
                else
                {
                    MeshRenderer r = hit.transform.GetComponentInChildren<MeshRenderer>();
                    bool foundMat = false;

                    if (r != null)
                    {
                        MaterialGroupPreset mg = Toolbox.Instance.GetMaterialProperties(r.sharedMaterial);

                        if (mg != null)
                        {
                            foundMat = true;

                            if (mg.wood > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitWood, Player.Instance, Player.Instance.currentNode, hit.point);
                            }
                            else if (mg.plaster > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitPlaster, Player.Instance, Player.Instance.currentNode, hit.point);
                                if (BioScreenController.Instance.selectedSlot.isStatic == InventorySlot.StaticSlot.fists) Player.Instance.RecieveDamage(0.01f, null, Vector3.zero, Vector3.zero, null, null);
                            }
                            else if (mg.concrete > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitConcrete, Player.Instance, Player.Instance.currentNode, hit.point);
                                if (BioScreenController.Instance.selectedSlot.isStatic == InventorySlot.StaticSlot.fists) Player.Instance.RecieveDamage(0.01f, null, Vector3.zero, Vector3.zero, null, null);
                            }
                            else if (mg.metal > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitMetal, Player.Instance, Player.Instance.currentNode, hit.point);
                            }
                            else if (mg.glass > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitGlass, Player.Instance, Player.Instance.currentNode, hit.point);
                            }
                            else if (mg.tile > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitTile, Player.Instance, Player.Instance.currentNode, hit.point);
                                if (BioScreenController.Instance.selectedSlot.isStatic == InventorySlot.StaticSlot.fists) Player.Instance.RecieveDamage(0.01f, null, Vector3.zero, Vector3.zero, null, null);
                            }
                            else if (mg.fabric > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitFabric, Player.Instance, Player.Instance.currentNode, hit.point);
                            }
                            else if (mg.carpet > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitCarpet, Player.Instance, Player.Instance.currentNode, hit.point);
                            }
                        }
                    }
                    //This could be furniture, if it is, get the composition from there...

                    if(!foundMat)
                    {
                        //Look for interactable
                        InteractableController i = hit.transform.GetComponent<InteractableController>();

                        if (i != null && i.interactable != null && i.interactable.preset.useMaterialOverride)
                        {
                            foundMat = true;

                            if (i.interactable.preset.materialOverride.wood > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitWood, Player.Instance, Player.Instance.currentNode, hit.point);
                            }
                            else if (i.interactable.preset.materialOverride.plaster > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitPlaster, Player.Instance, Player.Instance.currentNode, hit.point);
                                //Player.Instance.RecieveDamage(0.01f, null, Vector3.zero, Vector3.zero, null, null);
                            }
                            else if (i.interactable.preset.materialOverride.concrete > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitConcrete, Player.Instance, Player.Instance.currentNode, hit.point);
                                //Player.Instance.RecieveDamage(0.01f, null, Vector3.zero, Vector3.zero, null, null);
                            }
                            else if (i.interactable.preset.materialOverride.metal > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitMetal, Player.Instance, Player.Instance.currentNode, hit.point);
                            }
                            else if (i.interactable.preset.materialOverride.glass > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitGlass, Player.Instance, Player.Instance.currentNode, hit.point);
                            }
                            else if (i.interactable.preset.materialOverride.tile > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitTile, Player.Instance, Player.Instance.currentNode, hit.point);
                                //Player.Instance.RecieveDamage(0.01f, null, Vector3.zero, Vector3.zero, null, null);
                            }
                            else if (i.interactable.preset.materialOverride.fabric > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitFabric, Player.Instance, Player.Instance.currentNode, hit.point);
                            }
                            else if (i.interactable.preset.materialOverride.carpet > 0.5f)
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitCarpet, Player.Instance, Player.Instance.currentNode, hit.point);
                            }
                        }
                    }
                    
                    if(!foundMat)
                    {
                        MeshFilter mf = hit.transform.GetComponent<MeshFilter>();

                        if(mf != null)
                        {
                            FurniturePreset furn = Toolbox.Instance.GetFurnitureFromMesh(mf.sharedMesh);

                            if (furn != null)
                            {
                                if (furn.wood > 0.5f)
                                {
                                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitWood, Player.Instance, Player.Instance.currentNode, hit.point);
                                }
                                else if (furn.plaster > 0.5f)
                                {
                                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitPlaster, Player.Instance, Player.Instance.currentNode, hit.point);
                                    if (BioScreenController.Instance.selectedSlot.isStatic == InventorySlot.StaticSlot.fists) Player.Instance.RecieveDamage(0.01f, null, Vector3.zero, Vector3.zero, null, null);
                                }
                                else if (furn.concrete > 0.5f)
                                {
                                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitConcrete, Player.Instance, Player.Instance.currentNode, hit.point);
                                    if (BioScreenController.Instance.selectedSlot.isStatic == InventorySlot.StaticSlot.fists) Player.Instance.RecieveDamage(0.01f, null, Vector3.zero, Vector3.zero, null, null);
                                }
                                else if (furn.metal > 0.5f)
                                {
                                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitMetal, Player.Instance, Player.Instance.currentNode, hit.point);
                                }
                                else if (furn.glass > 0.5f)
                                {
                                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitGlass, Player.Instance, Player.Instance.currentNode, hit.point);
                                }
                                else if (furn.tile > 0.5f)
                                {
                                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitTile, Player.Instance, Player.Instance.currentNode, hit.point);
                                    if (BioScreenController.Instance.selectedSlot.isStatic == InventorySlot.StaticSlot.fists) Player.Instance.RecieveDamage(0.01f, null, Vector3.zero, Vector3.zero, null, null);
                                }
                                else if (furn.fabric > 0.5f)
                                {
                                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitFabric, Player.Instance, Player.Instance.currentNode, hit.point);
                                }
                                else if (furn.carpet > 0.5f)
                                {
                                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitCarpet, Player.Instance, Player.Instance.currentNode, hit.point);
                                }
                            }
                            else
                            {
                                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitWood, Player.Instance, Player.Instance.currentNode, hit.point);
                            }
                        }
                    }
                }
            }

            //Jolt camera
            Vector3 joltDir = new Vector3(-1f, -0.2f, 0f);
            Player.Instance.fps.JoltCamera(joltDir, 6f, 1.1f);
        }
    }

    public void Block()
    {
        Game.Log("Player: Block!");

        SessionData.Instance.TutorialTrigger("combat");

        //Ray ray = CameraController.Instance.cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        Ray ray = new Ray(CameraController.Instance.cam.transform.position, CameraController.Instance.cam.transform.forward);

        // Bit shift the index of the layer (20) to get a bit mask
        int layerMask = 1 << 20;

        // This would cast rays only against colliders in layer 20.
        // But instead we want to collide against everything except layer 20. The ~ operator does this, it inverts a bitmask.
        layerMask = ~layerMask;

        RaycastHit hit;

        if (Physics.SphereCast(ray, 0.2f, out hit, 2f, layerMask))
        {
            Actor hitActor = hit.transform.gameObject.GetComponentInParent<Actor>();

            if (hitActor != null)
            {
                if (hitActor.ai != null)
                {
                    //Is this actor attacking?
                    if (hitActor.ai.attackActive)
                    {
                        //Calculate bar progress towards damage valid
                        float blockProgress = hitActor.ai.attackProgress / hitActor.ai.currentWeaponPreset.attackTriggerPoint;

                        //Is this within block threshold?
                        if (blockProgress >= 1f - GameplayControls.Instance.successfulBlockThreshold && blockProgress <= 1f)
                        {
                            //Is this a perfect block?
                            bool perfect = false;

                            if (blockProgress >= 1f - GameplayControls.Instance.perfectBlockThreshold)
                            {
                                perfect = true;

                                counterAttackActor = hitActor;
                                counterAttackPoint = hit.point;
                                InterfaceController.Instance.firstPersonAnimator.SetTrigger("Counter"); //Counter animation
                                Player.Instance.TransformPlayerController(GameplayControls.Instance.counterTransition, null, null, null);
                                hitActor.ai.OnAttackBlock(perfect);
                                return;
                            }
                            else
                            {
                                InterfaceController.Instance.firstPersonAnimator.SetTrigger("SuccessfulBlock"); //Block animation
                                Player.Instance.TransformPlayerController(GameplayControls.Instance.successfulBlockTransition, null, null, null);
                            }

                            hitActor.ai.OnAttackBlock(perfect);
                            return;
                        }
                    }
                }
            }
        }

        InterfaceController.Instance.firstPersonAnimator.SetTrigger("UnsuccessfulBlock"); //Block animation
        Player.Instance.TransformPlayerController(GameplayControls.Instance.unsuccessfulBlockTransition, null, null, null);
    }

    //Triggered by animation on counter attack punch
    public void CounterAttack()
    {
        Game.Log("Player: Counter attack!");

        if (counterAttackActor != null)
        {
            //Apply transition recoil state
            Player.Instance.transitionRecoilState = true;

            AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.punchHitFlesh, counterAttackActor, counterAttackActor.currentNode, counterAttackPoint);
            counterAttackActor.RecieveDamage(InteriorControls.Instance.fistsWeapon.GetAttackValue(MurderWeaponPreset.AttackValue.damage, Player.Instance), Player.Instance, counterAttackPoint, counterAttackPoint - CameraController.Instance.cam.transform.position, CriminalControls.Instance.punchSpatter, null, SpatterSimulation.EraseMode.useDespawnTime);
        }
    }

    //Throw coin
    public void ThrowCoin()
    {
        Game.Log("Player: Throw coin");

        //Remove right object
        //foreach (Transform child in rightHandObjectParent)
        //{
        //    Destroy(child.gameObject);
        //}

        //Minus money
        GameplayController.Instance.AddMoney(-1, false, "Thrown coin");

        //Create money object
        //Vector3 locPos = Player.Instance.currentRoom.transform.InverseTransformPoint(rightHandObjectParent.transform.position);
        Interactable newCoin = InteractableCreator.Instance.CreateWorldInteractable(worldCoin, Player.Instance, Player.Instance, null, rightHandObjectParent.transform.position, rightHandObjectParent.transform.eulerAngles, null, null);

        newCoin.MarkAsTrash(true);

        newCoin.MoveInteractable(rightHandObjectParent.transform.position, rightHandObjectParent.transform.eulerAngles, false); //Face objects towards player
        newCoin.LoadInteractableToWorld(false, true);

        //The coin should be spawned upon creation...
        //Set physics to simulate throw...
        newCoin.controller.DropThis(true);
    }

    //Handcuff
    public void Handcuff()
    {
        if (InteractionController.Instance.currentLookingAtInteractable != null)
        {
            Player.Instance.OnHandcuff(InteractionController.Instance.currentLookingAtInteractable.interactable);
        }
    }

    //Triggered on takedown
    public void Takedown()
    {
        Game.Log("Player: Takedown!");

        InterfaceController.Instance.firstPersonAnimator.SetTrigger("Takedown"); //Counter animation

        if (InteractionController.Instance.currentLookingAtInteractable != null)
        {
            if (InteractionController.Instance.currentLookingAtInteractable.interactable.isActor != null)
            {
                InteractionController.Instance.currentLookingAtInteractable.interactable.isActor.RecieveDamage(
                    0f,
                    Player.Instance,
                    Player.Instance.lookAtThisTransform.position,
                    InteractionController.Instance.currentLookingAtInteractable.interactable.isActor.lookAtThisTransform.position - Player.Instance.lookAtThisTransform.position,
                    null,
                    null,
                    SpatterSimulation.EraseMode.useDespawnTime,
                    false,
                    true,
                    GameplayControls.Instance.takedownTimer);
            }
        }
    }

    public void SetConsuming(bool val)
    {
        isConsuming = val;
        InterfaceController.Instance.firstPersonAnimator.SetBool("Consume", isConsuming);

        if(isConsuming)
        {
            Interactable consumable = null;

            if (BioScreenController.Instance.selectedSlot != null && BioScreenController.Instance.selectedSlot.interactableID > -1)
            {
                consumable = BioScreenController.Instance.selectedSlot.GetInteractable();
            }

            if (consumeLoop != null)
            {
                AudioController.Instance.StopSound(consumeLoop, AudioController.StopType.fade, "New consume");
                consumeLoop = null;
            }

            if(consumable != null)
            {
                if(consumable.preset.playerConsumeLoop != null)
                {
                    consumeLoop = AudioController.Instance.Play2DLooping(consumable.preset.playerConsumeLoop);
                }
            }
        }
        else
        {
            if (consumeLoop != null)
            {
                AudioController.Instance.StopSound(consumeLoop, AudioController.StopType.fade, "New consume");
                consumeLoop = null;
            }
        }

        //Update First person interaction
        FirstPersonItemController.Instance.UpdateCurrentActions();
    }

    public void TakeOne()
    {
        Interactable consumable = null;

        if (BioScreenController.Instance.selectedSlot != null && BioScreenController.Instance.selectedSlot.interactableID > -1)
        {
            consumable = BioScreenController.Instance.selectedSlot.GetInteractable();

            if(consumable.preset.takeOneEvent != null)
            {
                AudioController.Instance.Play2DSound(consumable.preset.takeOneEvent);
            }
        }

        if (!takeOneActive) StartCoroutine(TakeOneExecute());
    }

    IEnumerator TakeOneExecute()
    {
        float progress = 0f;
        takeOneActive = true;
        SetConsuming(true);

        Interactable consumable = null;

        if (BioScreenController.Instance.selectedSlot != null && BioScreenController.Instance.selectedSlot.interactableID > -1)
        {
            consumable = BioScreenController.Instance.selectedSlot.GetInteractable();
        }

        while (progress < 1f && consumable != null && consumable.cs > 0f)
        {
            float amountThisFrame = Time.deltaTime / 1.8f;

            //Add nourishment
            if (consumable.preset.retailItem != null)
            {
                Player.Instance.AddNourishment(consumable.preset.retailItem.nourishment * amountThisFrame);
                Player.Instance.AddHydration(consumable.preset.retailItem.hydration * amountThisFrame);
                Player.Instance.AddAlertness(consumable.preset.retailItem.alertness * amountThisFrame);
                Player.Instance.AddEnergy(consumable.preset.retailItem.energy * amountThisFrame);
                Player.Instance.AddExcitement(consumable.preset.retailItem.excitement * amountThisFrame);
                Player.Instance.AddChores(consumable.preset.retailItem.chores * amountThisFrame);
                Player.Instance.AddHygiene(consumable.preset.retailItem.hygiene * amountThisFrame);
                Player.Instance.AddBladder(consumable.preset.retailItem.bladder * amountThisFrame);
                Player.Instance.AddHeat(consumable.preset.retailItem.heat * amountThisFrame);
                Player.Instance.AddDrunk(consumable.preset.retailItem.drunk * amountThisFrame);
                Player.Instance.AddSick(consumable.preset.retailItem.sick * amountThisFrame);
                Player.Instance.AddHeadache(consumable.preset.retailItem.headache * amountThisFrame);
                Player.Instance.AddWet(consumable.preset.retailItem.wet * amountThisFrame);
                Player.Instance.AddBrokenLeg(consumable.preset.retailItem.brokenLeg * amountThisFrame);
                Player.Instance.AddBruised(consumable.preset.retailItem.bruised * amountThisFrame);
                Player.Instance.AddBlackEye(consumable.preset.retailItem.blackEye * amountThisFrame);
                Player.Instance.AddBlackedOut(consumable.preset.retailItem.blackedOut * amountThisFrame);
                Player.Instance.AddNumb(consumable.preset.retailItem.numb * amountThisFrame);
                Player.Instance.AddBleeding(consumable.preset.retailItem.bleeding * amountThisFrame);
                Player.Instance.AddBreath(consumable.preset.retailItem.breath * amountThisFrame);
                Player.Instance.AddStarchAddiction(consumable.preset.retailItem.starchAddiction * amountThisFrame);
                Player.Instance.AddPoisoned(consumable.preset.retailItem.poisoned * amountThisFrame, null);
                Player.Instance.AddHealth(consumable.preset.retailItem.health * amountThisFrame);
            }

            progress += amountThisFrame;
            yield return null;
        }

        if(consumable != null)
        {
            consumable.cs--;

            if (consumable.cs <= 0f)
            {
                //Remove when consumed
                if (consumable.preset.destroyWhenAllConsumed)
                {
                    EmptySlot(BioScreenController.Instance.selectedSlot, false, true, playSound: false);

                    //Remove all current hand items
                    foreach (Transform child in rightHandObjectParent)
                    {
                        Destroy(child.gameObject);
                    }
                }
            }
        }

        takeOneActive = false;
        SetConsuming(false);

        if(consumable.cs <= 0f)
        {
            RefreshHeldObjects(); //Refresh held items
        }
    }

    public void SetRaised(bool val)
    {
        isRaised = val;
        InterfaceController.Instance.firstPersonAnimator.SetBool("Raised", isRaised);

        //Update First person interaction
        FirstPersonItemController.Instance.UpdateCurrentActions();
    }

    //Put down
    public void PutDown()
    {
        Game.Log("Player: Put down");

        EmptySlot(BioScreenController.Instance.selectedSlot, false, false);
    }

    //Throw coin
    public void ThrowFood()
    {
        Game.Log("Player: Throw food");

        EmptySlot(BioScreenController.Instance.selectedSlot, true, false, playSound: false);
    }

    //Throw grenade
    public void ThrowGrenade()
    {
        Game.Log("Player: Throw grenade");
        InteractablePreset gren = InteriorControls.Instance.activeFlashbomb;
        if (currentItem == InteriorControls.Instance.incapacitator.fpsItem) gren = InteriorControls.Instance.activeIncapacitator;

        Interactable newGrenade = InteractableCreator.Instance.CreateWorldInteractable(gren, Player.Instance, null, null, rightHandObjectParent.transform.position, rightHandObjectParent.transform.eulerAngles, null, null);

        if (newGrenade != null)
        {
            newGrenade.ForcePhysicsActive(true, true, useThrowingForce: true);
            newGrenade.SetCustomState2(true, Player.Instance, forceUpdate: true); //Use custom switch 2 to arm the grenade
            newGrenade.SetValue(GameplayControls.Instance.thrownGrenadeFuse);
            EmptySlot(BioScreenController.Instance.selectedSlot, false, true, playSound: false);

            InteractionController.Instance.SetIllegalActionActive(true);
        }
    }

    public void TakePicture()
    {
        Game.Log("Player: Take photo...");

        //Create new interactable to hold capture...
        //Capture current...
        Player.Instance.interactable.node = Player.Instance.currentNode;
        Player.Instance.interactable.cvp = CameraController.Instance.cam.transform.TransformPoint(new Vector3(0, -0.1f, 0.1f));
        Player.Instance.interactable.cve = CameraController.Instance.cam.transform.eulerAngles;

        Player.Instance.sceneRecorder.RefreshCoveredArea(); //Update the covered area first...
        SceneRecorder.SceneCapture camScene = Player.Instance.sceneRecorder.ExecuteCapture(false, true);
        Game.Log("Player: New capture: " + camScene);

        //Save the surveillance capture
        Player.Instance.sceneRecorder.interactable.sCap.Add(camScene);

        //Get passed vars
        Interactable.Passed passedID = new Interactable.Passed(Interactable.PassedVarType.savedSceneCapID, camScene.id);
        List<Interactable.Passed> newPassed = new List<Interactable.Passed>();
        newPassed.Add(passedID);

        Interactable newPhoto = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.surveillancePrintout, Player.Instance, Player.Instance, null, Player.Instance.transform.position, Vector3.zero, newPassed, null);
        
        if (newPhoto != null)
        {
            newPhoto.RemoveFromPlacement();
            newPhoto.MarkAsTrash(true);
        }

        ActionController.Instance.Inspect(newPhoto, Player.Instance.currentNode, Player.Instance);

        //Who can see the player right now that's in the photo? Do they mind their picture taken?
        foreach(SceneRecorder.ActorCapture sceneActor in camScene.aCap)
        {
            Human h = sceneActor.GetHuman();

            if(!h.isDead && !h.isStunned && !h.isAsleep)
            {
                if(h.ai != null)
                {
                    if(h.ai.trackedTargets.Exists(item => item.actor.isPlayer && item.active))
                    {
                        Game.Log(h.GetCitizenName() + " sees player taking photo and isn't happy about it!");
                        h.ai.SetPersue(Player.Instance, false, 1, true, 0);
                    }
                }
            }
        }

        //Trigger upgrade photos
        if(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.kitchenPhotos) > 0f)
        {
            if(Player.Instance.currentRoom != null && Player.Instance.currentGameLocation.thisAsAddress != null && Player.Instance.currentGameLocation.thisAsAddress.company != null)
            {
                if(!Player.Instance.foodHygeinePhotos.Contains(Player.Instance.currentRoom.roomID))
                {
                    if (Player.Instance.currentRoom.preset.roomClass.name == "Kitchen" || Player.Instance.currentRoom.preset.roomClass.name == "Diner" || Player.Instance.currentRoom.preset.roomClass.name == "FastFood")
                    {
                        //Success; how dirty is this?
                        int rating = Mathf.RoundToInt((1f - Player.Instance.currentRoom.defaultWallKey.grubiness) * 10f);
                        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.interface", "Hygeine Rating") + ": " + rating + "/10", InterfaceControls.Icon.trash);

                        int maxPayment = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.kitchenPhotos));
                        GameplayController.Instance.AddMoney(Mathf.CeilToInt(maxPayment * ((10 - rating) / 10f)), true, "Hygeine photo");

                        Player.Instance.foodHygeinePhotos.Add(Player.Instance.currentRoom.roomID);
                    }
                }
            }
        }

        if (UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.bathroomPhotos) > 0f)
        {
            if (Player.Instance.currentRoom != null && Player.Instance.currentGameLocation.thisAsAddress != null && Player.Instance.currentGameLocation.thisAsAddress.company != null)
            {
                if (!Player.Instance.sanitaryHygeinePhotos.Contains(Player.Instance.currentRoom.roomID))
                {
                    if (Player.Instance.currentRoom.preset.roomClass.name == "Bathroom" || Player.Instance.currentRoom.preset.roomClass.name == "PublicBathroomMale" || Player.Instance.currentRoom.preset.roomClass.name == "PublicBathroomFemale")
                    {
                        //Success; how dirty is this?
                        int rating = Mathf.RoundToInt((1f - Player.Instance.currentRoom.defaultWallKey.grubiness) * 10f);
                        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.interface", "Hygeine Rating") + ": " + rating + "/10", InterfaceControls.Icon.trash);

                        int maxPayment = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.bathroomPhotos));
                        GameplayController.Instance.AddMoney(Mathf.CeilToInt(maxPayment * ((10 - rating) / 10f)), true, "Hygeine photo");

                        Player.Instance.sanitaryHygeinePhotos.Add(Player.Instance.currentRoom.roomID);
                    }
                }
            }
        }
    }

    //TODO: Figure out a way to save the game when a codebreaker is active
    public void PlaceCodebreaker()
    {
        List<Interactable.Passed> passed = new List<Interactable.Passed>();
        passed.Add(new Interactable.Passed(Interactable.PassedVarType.time, SessionData.Instance.gameTime));

        Interactable activeCodebreaker = null;

        if (Player.Instance.computerInteractable != null)
        {
            activeCodebreaker = InteractableCreator.Instance.CreateTransformInteractable(InteriorControls.Instance.activeCodebreaker, Player.Instance.computerInteractable.controller.transform, Player.Instance, null, new Vector3(-0.078f, 0.075f, 0.145f), new Vector3(0, 90, 0), passed);
            activeCodebreaker.objectRef = Player.Instance.computerInteractable;
        }
        else
        {
            activeCodebreaker = InteractableCreator.Instance.CreateTransformInteractable(InteriorControls.Instance.activeCodebreaker, InteractionController.Instance.currentLookingAtInteractable.transform, Player.Instance, null, Vector3.zero, new Vector3(90, 0, 0), passed);
            activeCodebreaker.objectRef = InteractionController.Instance.currentLookingAtInteractable.interactable;
        }

        //TODO: Figure out a way to save the game when a codebreaker is active

        //activeCodebreaker.SetValue(InteractionController.Instance.currentLookingAtInteractable.interactable.id);
        activeCodebreaker.cs = -0.5f;

        EmptySlot(BioScreenController.Instance.selectedSlot, false, true, playSound: false);
    }

    public void PlaceDoorWedge()
    {
        NewDoor doorRef = InteractionController.Instance.currentLookingAtInteractable.interactable.objectRef as NewDoor;

        if(doorRef != null)
        {
            List<Interactable.Passed> passed = new List<Interactable.Passed>();
            passed.Add(new Interactable.Passed(Interactable.PassedVarType.time, SessionData.Instance.gameTime));

            //Parent to door transform so it's always visible on both sides
            Interactable activeDoorWedge = InteractableCreator.Instance.CreateDoorParentedInteractable(InteriorControls.Instance.activeDoorWedge, doorRef, Player.Instance, new Vector3(0f, 0f, -0.05f), Vector3.zero, passed);

            //Use value as wall ID
            activeDoorWedge.SetValue(doorRef.wall.id);

            EmptySlot(BioScreenController.Instance.selectedSlot, false, true, playSound: false);
        }
    }

    public void PlaceTracker()
    {
        Interactable activeTracker= null;

        //Place on actor
        if (InteractionController.Instance.lookingAtInteractable && InteractionController.Instance.currentLookingAtInteractable.interactable.isActor != null)
        {
            Human placeOnHuman = InteractionController.Instance.currentLookingAtInteractable.interactable.isActor as Human;

            if(placeOnHuman != null)
            {
                if (placeOnHuman.ai != null && placeOnHuman.ai.trackedTargets.Exists(item => item.actor.isPlayer && item.active))
                {
                    Game.Log(placeOnHuman.GetCitizenName() + " sees player planting tracker and isn't happy about it!");
                    placeOnHuman.ai.SetPersue(Player.Instance, false, 1, true, 0);
                }

                activeTracker = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.activeTracker, Player.Instance, null, null, Player.Instance.transform.position + new Vector3(0, 3.5f, 0) , Vector3.zero, null, null );

                if(activeTracker != null)
                {
                    activeTracker.SetInInventory(placeOnHuman);
                    EmptySlot(BioScreenController.Instance.selectedSlot, false, true, playSound: false);
                }
            }
        }
        //Place on a wall
        else if (!InteractionController.Instance.lookingAtInteractable && InteractionController.Instance.playerCurrentRaycastHit.transform != null)
        {
            Vector3Int nodeCoord = CityData.Instance.RealPosToNodeInt(InteractionController.Instance.playerCurrentRaycastHit.point);
            NewNode foundNode = null;

            if (PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out foundNode))
            {
                Vector3 normals = Quaternion.FromToRotation(Vector3.up, InteractionController.Instance.playerCurrentRaycastHit.normal).eulerAngles;
                if (normals.x == 270) normals.z = 180; //I don't understand why this is needed, but it should work
                Vector3 eul = new Vector3(90, 0, normals.z);
                Game.Log("Placing motion tracker with euler " + eul + " from wall normals " + normals);

                activeTracker = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.activeTracker, Player.Instance, null, null, InteractionController.Instance.playerCurrentRaycastHit.point, eul, null, null);

                if(activeTracker != null)
                {
                    activeTracker.LoadInteractableToWorld(false, true);
                    activeTracker.SetCustomState1(true, null, forceUpdate: true); //Use custom state 1 to trigger wall tracking mode
                    EmptySlot(BioScreenController.Instance.selectedSlot, false, true, playSound: false);
                }
            }
        }
    }

    public void PlaceGrenade(InteractablePreset activeGrenade)
    {
        Interactable activeTracker = null;

        //Place on actor
        if (InteractionController.Instance.lookingAtInteractable && InteractionController.Instance.currentLookingAtInteractable.interactable.isActor != null)
        {
            Human placeOnHuman = InteractionController.Instance.currentLookingAtInteractable.interactable.isActor as Human;

            if (placeOnHuman != null)
            {
                if (placeOnHuman.ai != null && placeOnHuman.ai.trackedTargets.Exists(item => item.actor.isPlayer && item.active))
                {
                    Game.Log(placeOnHuman.GetCitizenName() + " sees player planting grenade and isn't happy about it!");
                    placeOnHuman.ai.SetPersue(Player.Instance, false, 1, true, 0);
                }

                activeTracker = InteractableCreator.Instance.CreateWorldInteractable(activeGrenade, Player.Instance, null, null, Player.Instance.transform.position + new Vector3(0, 3.5f, 0), Vector3.zero, null, null);

                if (activeTracker != null)
                {
                    activeTracker.SetInInventory(placeOnHuman);
                    activeTracker.SetCustomState2(true, Player.Instance, forceUpdate: true); //Use custom switch 2 to arm the grenade
                    activeTracker.SetValue(GameplayControls.Instance.thrownGrenadeFuse);
                    EmptySlot(BioScreenController.Instance.selectedSlot, false, true, playSound: false);
                }
            }
        }
        //Place on a wall
        else if (!InteractionController.Instance.lookingAtInteractable && InteractionController.Instance.playerCurrentRaycastHit.transform != null)
        {
            Vector3Int nodeCoord = CityData.Instance.RealPosToNodeInt(InteractionController.Instance.playerCurrentRaycastHit.point);
            NewNode foundNode = null;

            if (PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out foundNode))
            {
                Vector3 normals = Quaternion.FromToRotation(Vector3.up, InteractionController.Instance.playerCurrentRaycastHit.normal).eulerAngles;
                if (normals.x == 270) normals.z = 180; //I don't understand why this is needed, but it should work
                Vector3 eul = new Vector3(90, 0, normals.z);
                Game.Log("Placing grenade with euler " + eul + " from wall normals " + normals);

                activeTracker = InteractableCreator.Instance.CreateWorldInteractable(activeGrenade, Player.Instance, null, null, InteractionController.Instance.playerCurrentRaycastHit.point, eul, null, null);

                if (activeTracker != null)
                {
                    activeTracker.LoadInteractableToWorld(false, true);
                    activeTracker.SetCustomState1(true, null, forceUpdate: true); //Use custom state 1 to trigger wall tracking mode
                    activeTracker.SetValue(GameplayControls.Instance.proxyGrenadeFuse);
                    EmptySlot(BioScreenController.Instance.selectedSlot, false, true, playSound: false);
                }
            }
        }
    }

    public void PlaceFurniture()
    {
        if (PlayerApartmentController.Instance.furniturePlacementMode && PlayerApartmentController.Instance.furnPlacement != null && PlayerApartmentController.Instance.isPlacementValid)
        {
            Game.Log("Decor: Placement, furniture mode...");

            //Purchase (check money, display message)
            if (PlayerApartmentController.Instance.GetExistingFurniture() != null)
            {
                PlayerApartmentController.Instance.ExecutePlacement();
            }
            else if(PlayerApartmentController.Instance.furnPlacement.preset.cost <= GameplayController.Instance.money)
            {
                PopupMessageController.Instance.PopupMessage("FurniturePlacement", true, true, RButton: "Confirm");
                PopupMessageController.Instance.OnLeftButton += PlaceFurnitureCancel;
                PopupMessageController.Instance.OnRightButton += PlaceFurnitureConfirm;
            }
            else if(PlayerApartmentController.Instance.furnPlacement.preset.cost > GameplayController.Instance.money)
            {
                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "not_enough_money"), InterfaceControls.Icon.money, null, colourOverride: true, col: InterfaceControls.Instance.messageRed, ping: GameMessageController.PingOnComplete.money);
            }
        }
        else if(PlayerApartmentController.Instance.decoratingMode)
        {
            Game.Log("Decor: Placement, decor mode...");

            if (PlayerApartmentController.Instance.GetCurrentCost() <= GameplayController.Instance.money)
            {
                PopupMessageController.Instance.PopupMessage("FurniturePlacement", true, true, RButton: "Confirm", newPauseState: PopupMessageController.AffectPauseState.no);
                PopupMessageController.Instance.OnLeftButton += PlaceFurnitureCancel;
                PopupMessageController.Instance.OnRightButton += PlaceFurnitureConfirm;
            }
            else
            {
                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "not_enough_money"), InterfaceControls.Icon.money, null, colourOverride: true, col: InterfaceControls.Instance.messageRed, ping: GameMessageController.PingOnComplete.money);
            }
        }
        else
        {
            Game.Log("Decor: Placement is invalid!");
        }
    }

    public void PlaceFurnitureConfirm()
    {
        PopupMessageController.Instance.OnLeftButton -= PlaceFurnitureCancel;
        PopupMessageController.Instance.OnRightButton -= PlaceFurnitureConfirm;

        PlayerApartmentController.Instance.ExecutePlacement();
    }

    public void PlaceFurnitureCancel()
    {
        PopupMessageController.Instance.OnLeftButton -= PlaceFurnitureCancel;
        PopupMessageController.Instance.OnRightButton -= PlaceFurnitureConfirm;
    }

    public void CancelFurniture()
    {
        PlayerApartmentController.Instance.CancelPlacement(PlayerApartmentController.Instance.placeExistingRoomObject);
    }

    public void Give()
    {
        if (InteractionController.Instance.lookingAtInteractable && InteractionController.Instance.currentLookingAtInteractable.interactable.isActor != null)
        {
            Human hu = InteractionController.Instance.currentLookingAtInteractable.interactable.isActor as Human;

            if(hu != null)
            {
                Interactable giveThis = BioScreenController.Instance.selectedSlot.GetInteractable();

                if(giveThis != null)
                {
                    hu.TryGiveItem(giveThis, Player.Instance, true);
                }
            }
        }
    }
}
