﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using TMPro;
using UnityEngine.Video;
using UnityEditor;
using UnityEngine.UI;

public class HelpController : MonoBehaviour
{
    public RectTransform rect;
    public WindowContentController wcc;
    public bool isSetup = false;

    //[System.Serializable]
    //public class HelpContent
    //{
    //    public string name;
    //    public string messageID;
    //    public VideoClip videoClip;
    //    public Texture2D image;
    //    public ButtonController spawnedButton;
    //}

    [Header("Contents")]
    public RectTransform helpContents;
    //public VideoPlayer videoPlayer;
    public TMP_InputField searchInputField;
    public RectTransform helpContentButtonParent;
    public TextMeshProUGUI contentsText;
    public List<InterfaceVideoController> videos = new List<InterfaceVideoController>();

    [Header("Page")]
    public GameObject page;
    public TextMeshProUGUI helpTitle;
    public TextMeshProUGUI helpContent;
    //public RectTransform videoParent;
    //public GameObject videoPrefab;
    public ButtonController backButtonTop;
    public ButtonController backButtonBottom;
    public VerticalLayoutGroup layoutGroup;

    [Header("Content")]
    //[ReorderableList]
    //public List<HelpContent> content = new List<HelpContent>();

    public List<ButtonController> helpContentButtons = new List<ButtonController>();
    public GameObject helpContentButtonPrefab;

    //private Dictionary<string, EvidenceLinkData> dataSource = new Dictionary<string, EvidenceLinkData>();

    //Singleton pattern
    private static HelpController _instance;
    public static HelpController Instance { get { return _instance; } }

    public void Setup(WindowContentController newContentController)
    {
        rect = this.gameObject.GetComponent<RectTransform>();

        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        wcc = newContentController;
        SetPageSize(new Vector2(740, 738));

        contentsText.text = Strings.Get("ui.handbook", "contentsheader");
        
        if(InterfaceController.Instance.openHelpToPage.Length > 0)
        {
            DisplayHelpPage(InterfaceController.Instance.openHelpToPage);
            InterfaceController.Instance.openHelpToPage = "";
        }
        else
        {
            DisplayHelpContents();
        }

        isSetup = true;

        //backButtonTop.button.navigation = new Navigation { mode = Navigation.Mode.Explicit, selectOnDown = backButtonBottom.button, selectOnRight = backButtonTop.button.FindSelectableOnRight() };
        //backButtonBottom.button.navigation = new Navigation { mode = Navigation.Mode.Explicit, selectOnUp = backButtonTop.button, selectOnRight = backButtonBottom.button.FindSelectableOnRight() };

        //Subscribe to notification updates
        SessionData.Instance.OnTutorialNotificationChange += UpdateHelpButtonList;
    }

    public void SetPageSize(Vector2 newSize)
    {
        Game.Log("Interface: Set help size: " + newSize);
        rect.sizeDelta = newSize;
        wcc.normalSize = rect.sizeDelta;
    }

    public void DisplayHelpContents()
    {
        //Force deselect
        if(InterfaceController.Instance.selectedElement != null) InterfaceController.Instance.selectedElement.OnDeselect();

        helpContents.gameObject.SetActive(true);
        rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 0);

        //Deactivate all other sections
        page.SetActive(false);

        UpdateHelpButtonList();

        backButtonTop.RefreshAutomaticNavigation();
        backButtonBottom.RefreshAutomaticNavigation();
    }

    public void UpdateHelpButtonList()
    {
        try
        {
            if(helpContents != null && helpContents.gameObject != null && helpContents.gameObject.activeSelf)
            {
                Game.Log("Interface: Updating help list...");

                List<HelpContentPage> required = new List<HelpContentPage>();

                foreach (KeyValuePair<string, HelpContentPage> pair in Toolbox.Instance.allHelpContent)
                {
                    if (pair.Value.disabled) continue;

                    //Only available if triggered...
                    //if (SessionData.Instance.enableTutorialText)
                    //{
                    //    if (!SessionData.Instance.tutorialTextTriggered.Contains(h.name.ToLower()))
                    //    {
                    //        continue;
                    //    }
                    //}

                    if (searchInputField.text.Length <= 0)
                    {
                        required.Add(pair.Value);
                    }
                    else
                    {
                        string realText = Strings.Get("ui.handbook", pair.Value.name);

                        if (realText.ToLower().Contains(searchInputField.text.ToLower()))
                        {
                            required.Add(pair.Value);
                        }
                    }
                }

                for (int i = 0; i < helpContentButtons.Count; i++)
                {
                    ButtonController bc = helpContentButtons[i];

                    bool found = false;

                    for (int u = 0; u < required.Count; u++)
                    {
                        HelpContentPage h = required[u];

                        string realText = Strings.Get("ui.handbook", h.name).ToLower();

                        if (realText == bc.text.text.ToLower())
                        {
                            /*if (SessionData.Instance.enableTutorialText && !SessionData.Instance.tutorialTextRead.Contains(h.name.ToLower()))
                            {
                                bc.icon.enabled = true;
                            }
                            else*/ bc.icon.enabled = false;

                            required.RemoveAt(u);
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        bc.OnPress -= DisplayHelpPage;
                        Destroy(bc.gameObject);
                        helpContentButtons.RemoveAt(i);
                        i--;
                        continue;
                    }
                }

                foreach (HelpContentPage req in required)
                {
                    GameObject newObj = Instantiate(helpContentButtonPrefab, helpContentButtonParent);
                    ButtonController bc = newObj.GetComponent<ButtonController>();
                    helpContentButtons.Add(bc);
                    //req.spawnedButton = bc;
                    bc.text.text = Strings.Get("ui.handbook", req.name);
                    bc.name = req.name;

                    /*if (SessionData.Instance.enableTutorialText && !SessionData.Instance.tutorialTextRead.Contains(req.name.ToLower()))
                    {
                        bc.icon.enabled = true;
                    }
                    else*/ bc.icon.enabled = false;

                    bc.OnPress += DisplayHelpPage;
                }

                //Hierarchy based on list
                //for (int i = 0; i < content.Count; i++)
                //{
                //    if (content[i].spawnedButton != null)
                //    {
                //        if (!content[i].spawnedButton.icon.enabled)
                //        {
                //            content[i].spawnedButton.rect.SetAsLastSibling();
                //        }
                //    }
                //}

                //Update rect sizes...
                helpContentButtonParent.sizeDelta = new Vector2(helpContentButtonParent.sizeDelta.x, Mathf.Max(helpContentButtons.Count * 72f + 24f, 466));
                SetPageSize(new Vector2(rect.sizeDelta.x, helpContentButtonParent.sizeDelta.y + 400f));

                for (int i = 0; i < helpContentButtons.Count; i++)
                {
                    ButtonController bc = helpContentButtons[i];
                    bc.RefreshAutomaticNavigation();
                }
            }
        }
        catch
        {
            //Rare unknown error happening here
        }
    }

    private void OnDestroy()
    {
        SessionData.Instance.OnTutorialNotificationChange -= UpdateHelpButtonList;

        //Unsubscribe all
        foreach (ButtonController bc in helpContentButtons)
        {
            bc.OnPress -= DisplayHelpPage;
        }
    }

    public void ClearSearchButton()
    {
        searchInputField.text = string.Empty;
    }

    public void DisplayHelpPage(ButtonController button)
    {
        //HelpContent cont = content.Find(item => item.spawnedButton == button);

        //if(cont != null)
        //{
        //    LoadHelpPage(button.name);
        //}

        LoadHelpPage(button.name);
    }

    public void DisplayHelpPage(string pageName)
    {
        LoadHelpPage(pageName.ToLower());
    }

    public void LoadHelpPage(string h)
    {
        HelpContentPage helpPage = null;

        if(!Toolbox.Instance.allHelpContent.TryGetValue(h, out helpPage))
        {
            return;
        }
        
        //Force deselect
        if (InterfaceController.Instance.selectedElement != null) InterfaceController.Instance.selectedElement.OnDeselect();

        page.SetActive(true);

        helpContents.gameObject.SetActive(false);

        Game.Log("Interface: Displaying help content " + helpPage.name);
        page.SetActive(true);

        //Set text
        helpTitle.text = Strings.Get("ui.handbook", helpPage.name);

        //Get text
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        //The first message should skip the first block (subject)
        List<string> say = Player.Instance.ParseDDSMessage(helpPage.messageID, null, out _);

        for (int i = 0; i < say.Count; i++)
        {
            string composed = Strings.ComposeText(Strings.Get("dds.blocks", say[i]), null, Strings.LinkSetting.forceLinks);

            if (i < say.Count - 1)
            {
                composed += "\n\n"; //Add line break
            }

            sb.Append(composed);
        }

        helpContent.text = sb.ToString();

        //helpContent.text = Strings.ComposeText(Strings.Get("ui.handbook", "content" + h.name), null, Strings.LinkSetting.forceLinks); //Compile using links
        float pageHeight = 0f;

        helpContent.rectTransform.sizeDelta = new Vector2(helpContent.rectTransform.sizeDelta.x, helpContent.preferredHeight);
        rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 0);
        pageHeight += helpContent.preferredHeight;

        while (videos.Count > 0)
        {
            Destroy(videos[0].gameObject);
            videos.RemoveAt(0);
        }

        //Load clip
        for (int i = 0; i < helpPage.contentDisplay.Count; i++)
        {
            GameObject newVid = Instantiate(PrefabControls.Instance.interfaceVideo, page.transform);
            InterfaceVideoController ivc = newVid.GetComponent<InterfaceVideoController>();
            ivc.Setup(helpPage.contentDisplay[i].clip, helpPage.contentDisplay[i].image);
            videos.Add(ivc);

            pageHeight += 300;
        }

        //Update page size
        SetPageSize(new Vector2(rect.sizeDelta.x, Mathf.Max(pageHeight + 400f, 648)));

        backButtonTop.juice.GetOriginalRectSize();
        backButtonBottom.juice.GetOriginalRectSize();

        //Update handbook notifications
        //if (!SessionData.Instance.tutorialTextRead.Contains(h.name.ToLower()))
        //{
        //    SessionData.Instance.tutorialTextRead.Add(h.name.ToLower());
        //}

        SessionData.Instance.UpdateTutorialNotifications();
    }
}
