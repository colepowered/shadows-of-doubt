using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using System.Linq;

public class MenuNavigationOrderConfig : MonoBehaviour
{
    public List<Transform> contentParentHierarchy = new List<Transform>();
    public bool leftMovesUpHierarchy = true;

    [Button]
    public void Configure()
    {
        Dictionary<Transform, List<Button>> buttonDict = new Dictionary<Transform, List<Button>>();

        foreach(Transform t in contentParentHierarchy)
        {
            Button[] allButtons = t.GetComponentsInChildren<Button>(true);

            buttonDict.Add(t, allButtons.ToList());
        }

        foreach(KeyValuePair<Transform, List<Button>> pair in buttonDict)
        {
            foreach(Button b in pair.Value)
            {
                //Search for up...
                Button upButton = null;
                float closest = Mathf.Infinity;
                float closestAltAxis = Mathf.Infinity;

                foreach (Button bU in pair.Value)
                {
                    if (b == bU) continue;

                    if(bU.transform.position.y > b.transform.position.y)
                    {
                        float diff = Mathf.RoundToInt(Mathf.Abs(b.transform.position.y - bU.transform.position.y) * 100) / 100f;
                        float diffX = Mathf.RoundToInt(Mathf.Abs(b.transform.position.x - bU.transform.position.x) * 100) / 100f;

                        if (diff < closest)
                        {
                            upButton = bU;
                            closest = diff;
                            closestAltAxis = diffX;
                        }
                        else if(diff == closest)
                        {
                            if (diffX <= closestAltAxis)
                            {
                                upButton = bU;
                                closest = diff;
                                closestAltAxis = diffX;
                            }
                        }
                    }
                }

                //Search for down...
                Button downButton = null;
                closest = Mathf.Infinity;
                closestAltAxis = Mathf.Infinity;

                foreach (Button bU in pair.Value)
                {
                    if (b == bU) continue;

                    if (bU.transform.position.y < b.transform.position.y)
                    {
                        float diff = Mathf.RoundToInt(Mathf.Abs(b.transform.position.y - bU.transform.position.y) * 100) / 100f;
                        float diffX = Mathf.RoundToInt(Mathf.Abs(b.transform.position.x - bU.transform.position.x) * 100) / 100f;

                        if (diff < closest)
                        {
                            downButton = bU;
                            closest = diff;
                            closestAltAxis = diffX;
                        }
                        else if (diff == closest)
                        {
                            if (diffX <= closestAltAxis)
                            {
                                downButton = bU;
                                closest = diff;
                                closestAltAxis = diffX;
                            }
                        }
                    }
                }

                //Search for left...
                Button leftButton = b.navigation.selectOnLeft as Button;

                if (leftButton == null)
                {
                    int tIndex = contentParentHierarchy.IndexOf(pair.Key);

                    if(tIndex > 0 && leftMovesUpHierarchy)
                    {
                        Transform prev = contentParentHierarchy[tIndex - 1];

                        if(buttonDict.ContainsKey(prev))
                        {
                            leftButton = buttonDict[prev][0];
                        }
                    }
                }

                Navigation newNav = new Navigation();
                newNav.mode = Navigation.Mode.Explicit;
                newNav.selectOnUp = upButton;
                newNav.selectOnDown = downButton;
                newNav.selectOnLeft = leftButton;
                newNav.selectOnRight = b.navigation.selectOnRight;

                b.navigation = newNav;
            }
        }
    }
}
