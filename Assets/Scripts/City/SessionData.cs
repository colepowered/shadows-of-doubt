﻿using NaughtyAttributes;
using Rewired;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UI;
using System.IO;
using System.Linq;

//Holdsdata relevent to the current city session
//Script pass 1
public class SessionData : MonoBehaviour
{
    [Header("Flags")]
    public bool isFloorEdit = false; //Check this for the floor edit scene
    public bool isDialogEdit = false;
    public bool isTestScene = false;
    public bool dirtyScene = false; //True if a city has been started to setup
    public bool isDecorEdit = false;

    //Pause Game
    public bool enableUserPause = true; //Can be modified to disable pausing- mainly done by the summary notes edit
    public bool enableFirstPersonMap = true;
    public bool play = false;

    //Tutorial text
    public bool enableTutorialText = false;
    //public HashSet<string> tutorialTextRead = new HashSet<string>(); //Display all tutorial text at least once...
    public HashSet<string> tutorialTextTriggered = new HashSet<string>();

    //Misc
    public bool startedGame = false;
    private int pauseUnpauseDelay = 0; //Short 1 frame delay enforced between pausing/unpausing

    //Oscillators
    private float drunkOscillatorX = 0f;
    private float drunkOscillatorY = 0f;
    public Vector2 drunkOscillation = Vector2.zero;

    private float shiverOscillatorX = 0f;
    private float shiverOscillatorY = 0f;
    private float shiverProgress = 0f;
    public Vector2 shiverOscillation = Vector2.zero;
    private float drunkLensProgress = 0f;
    private float headacheProgress = 0f;

    //Shadow frame counter
    //private int dynamicShadowFrameCounter = 0; //Used to calculate x frames for shadow updates
    private int sunShadowFrameCounter = 0;

    [Header("Time")]
    //GameTime holds complete time data inside a float
    public float gameTime = 0f;
    public double gameTimeDouble = 0f;
    public double gameTimePassedThisFrame = 0f;

    private int prevHour = 0; //Used for hour change trigger

    //24H clock in decimal time
    public float decimalClock = 0f;
    public double decimalClockDouble = 0f;

    //24H clock in formatted time
    public float formattedClock = 0f;

    //Time
    public enum TimeSpeed { slow, normal, fast, veryFast, simulation };
    public TimeSpeed currentTimeSpeed;
    public float currentTimeMultiplier = 1f;

    //The delay in the citizen behaviour checking loop (seconds)
    public float behaviourDelay = 0.5f;

    public enum TimeOfDay { morning, afternoon, evening };
    public TimeOfDay timeOfDay = TimeOfDay.morning;

    //Day of the week 0=Monday, 6=Sunday
    public int dayInt = 0;
    public enum WeekDay { monday, tuesday, wednesday, thursday, friday, saturday, sunday };
    public WeekDay day;
    public int dateInt = 0;

    //Month of the year
    public enum Month { jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec };
    public Month month;
    public int monthInt = 0;
    public List<int> daysInMonths = new List<int>();

    //Year: Important to keep at minimal and use an offset for display
    public int yearInt = 0;
    public int publicYear = 0;
    //Leap year; if cycle == 3 then add an extra day in feb
    public int leapYearCycle = 0;

    //Game timer in seconds
    public float gameTimeLimit = 0f;

    [Header("Weather")]
    [Range(0f, 1f)]
    public float currentRain = 0f;
    [Range(0f, 1f)]
    public float desiredRain = 0f;
    [Range(0f, 1f)]
    public float currentWind = 0f;
    [Range(0f, 1f)]
    public float desiredWind = 0f;
    [Range(0f, 1f)]
    public float currentSnow = 0f;
    [Range(0f, 1f)]
    public float desiredSnow = 0f;
    [Range(0f, 1f)]
    public float currentLightning = 0f;
    [Range(0f, 1f)]
    public float desiredLightning = 0f;

    public float transitionSpeed = 0.1f;

    public float weatherChangeTimer = 0f;

    private float monthTempMultiplier = 0f;
    public float temperature = 0f;

    [Space(5)]
    public float lightningTimer = 0f;
    [Space(5)]
    public Vector3 windDirection = Vector3.zero;
    public float windForce = 20f;

    [Header("Scene")]
    public float dayProgress;

    public RainSheetController nearRainSheet;
    public RainSheetController farRainSheet;
    public Vector2 nearRainAlpha1Threshold = new Vector2(0.9f, 0.9f);
    public Vector2 nearRainAlpha2Threshold = new Vector2(0.5f, 0.5f);
    public Vector2 nearRainSpeedThreshold = new Vector2(5f, 5f);
    public Vector2 nearRainXTile1Threshold = new Vector2(10f, 10f);
    public Vector2 nearRainXTile2Threshold = new Vector2(20f, 20f);
    [Space(5)]
    public Vector2 farRainAlpha1Threshold = new Vector2(1f, 1f);
    public Vector2 farRainAlpha2Threshold = new Vector2(1f, 1f);
    public Vector2 farRainSpeedThreshold = new Vector2(5f, 5f);
    public Vector2 farRainXTile1Threshold = new Vector2(40f, 40f);
    public Vector2 farRainXTile2Threshold = new Vector2(40f, 40f);
    [Space(5)]
    public Vector2 particalRainCountThreshold = new Vector2(200, 800);
    public Vector2 particalSnowCountThreshold = new Vector2(200, 800);
    [Space(5)]
    public float cityWetness = 0f;
    public float citySnow = 0f;
    [Tooltip("A list of materials where the _CityWetness param will change based on wetness.")]
    public List<WetMaterial> wetMaterials = new List<WetMaterial>();
    [Tooltip("A list of materials where the _Rain param will change based on rain amount.")]
    public List<Material> raindropMaterials = new List<Material>();

    [Tooltip("A list of materials to modify with wind")]
    public List<WetMaterial> swingShaderMaterials = new List<WetMaterial>();

    public List<CustomPassVolume> customPasses = new List<CustomPassVolume>();

    //Rainy windows
    public Dictionary<GameObject, WallFrontagePreset> rainyWindowFrontageObjects = new Dictionary<GameObject, WallFrontagePreset>();
    public List<Material> rainyWindowMaterials = new List<Material>();

    [System.Serializable]
    public class WetMaterial
    {
        public Material mat;
        public float multiplier = 1f;
    }

    public float autoPauseTimer = 0f;
    public float autoResetTimer = 0f;

    private float lightswitchPulse = 0f;
    private bool lightswitchPulseMode = false;

    [Header("PP Profiles")]
    public SceneProfile currentProfile = SceneProfile.outdoors;
    [System.NonSerialized]
    public CityControls.PPProfile currentSceneProfile = null;
    [System.NonSerialized]
    public CityControls.PPProfile desiredSceneProfile = null;

    public enum SceneProfile { outdoors, indoors, grimey, clean, corporate, cbd, chinatown, industrial, residential, warm };

    [Header("HDRP")]
    [Tooltip("The global (outdoors) profile")]
    public Volume globalVolume;
    public GradientSky gradientSky;
    public Fog volFog;

    public DepthOfField dof;
    public Vignette vignette;
    public MotionBlur motionBlur;
    public FilmGrain grain;
    public Tonemapping toneMapping;
    public Bloom bloom;
    public ChromaticAberration chromaticAberration;
    public LiftGammaGain lgg;
    public ColorAdjustments colour;
    public LensDistortion lensDistort;
    public Exposure exposure;
    public ChannelMixer channelMixer;

    [System.Serializable]
    public class SkyboxGradient : IComparable<SkyboxGradient>
    {
        public float time;
        public Color skyColour = Color.cyan; //Camera background colour
        public Color fogAlbedo = Color.cyan; //Haze colour

        [Space(3)]
        public Color ambientLightTop = Color.cyan;
        public Color ambientLightMiddle = Color.cyan;
        public Color ambientLightBottom = Color.cyan;

        public Color ambientLightingColour = Color.cyan;

        public Color fogColour = Color.cyan;
        public Color seaEmission = Color.black;
        public Color smokeEmission = Color.black;

        public int CompareTo(SkyboxGradient otherObject)
        {
            return this.time.CompareTo(otherObject.time);
        }
    }

    public int skyboxGradientIndex = 0;
    public SkyboxGradient fromSkyboxColours;
    public SkyboxGradient toSkyboxColours;

    [Header("Elevators")]
    public List<Elevator> activeElevators = new List<Elevator>();

    [Header("Particle Systems")]
    public List<InteractableController> particleSystems = new List<InteractableController>();

    [Header("Television")]
    public Material broadcastMaterial;
    public Material broadcastMaterialInstanced;
    public BroadcastSchedule defaultBroadcastSchedule;
    public BroadcastSchedule currentBroadcastSchedule;
    //public float lastBroadcastUpdate = 0;
    public BroadcastPreset currentShow;
    public int currentScheduleIndex = -1;
    public float currentShowProgressSeconds = 0f;
    public float currentShowImageProgress = 0f;
    FMOD.Studio.EventDescription currentShowEventDescription;
    int currentShowAudioLength = 0;
    int currentShowImageLength = 0;
    int currentImageIndex = 0;
    //Texture2D currentShowImage;

    [Header("References")]
    public TextMeshProUGUI pauseText;
    public GameObject pauseLensFlare;
    //public Image watchNightMode;
    //public float watchNightModeProgress = 0f;

    //Time control buttons
    public Image pauseButtonImg;
    public Image normalSpeedButtonImg;
    public Image fastSpeedButtonImg;
    public Image veryFastSpeedButtonImg;
    public TextMeshPro newWatchTimeText;
    public TextMeshPro newWatchDateText;
    public TextMeshProUGUI clockText;
    public TextMeshProUGUI dayText;

    public Image pauseButtonIcon;
    public Sprite pauseIcon;
    public Sprite playIcon;

    //Starting node
    public NewNode startingNode;

    //UI snapshot
    [System.NonSerialized]
    AudioController.LoopingSoundInfo interfaceActiveAudio = null;

    [Header("Debug")]
    public Vector2 debugDecimalRange = new Vector2(18, 6);
    public List<WeekDay> debugDayList = new List<WeekDay>();

    //Actions - used as callback for end of frame
    public Action UnloadPipes;
    public List<PipeConstructor.PipeGroup> pipesToUnload = new List<PipeConstructor.PipeGroup>();

    public enum PhysicsSyncType { now, onPlayerMovement, both };

    //Singleton pattern
    private static SessionData _instance;
    public static SessionData Instance { get { return _instance; } }

    //Events
    //Case status change; open a new active case or close one
    public delegate void OnPauseUnPause(bool openDesktopMode);
    public event OnPauseUnPause OnPauseChange;

    public delegate void WeatherChange();
    public event WeatherChange OnWeatherChange;

    public delegate void HourChange();
    public event HourChange OnHourChange;

    public delegate void TutorialNotificationChange();
    public event TutorialNotificationChange OnTutorialNotificationChange;

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        //Setup calendar reference
        daysInMonths.Add(31);
        daysInMonths.Add(28);
        daysInMonths.Add(31);
        daysInMonths.Add(30);
        daysInMonths.Add(31);
        daysInMonths.Add(30);
        daysInMonths.Add(31);
        daysInMonths.Add(31);
        daysInMonths.Add(30);
        daysInMonths.Add(31);
        daysInMonths.Add(30);
        daysInMonths.Add(31);

        //Grab all global volume effects
        if (globalVolume != null)
        {
            for (int i = 0; i < globalVolume.profile.components.Count; i++)
            {
                if (globalVolume.profile.components[i] as DepthOfField != null)
                {
                    dof = globalVolume.profile.components[i] as DepthOfField;
                }
                else if (globalVolume.profile.components[i] as Vignette != null)
                {
                    vignette = globalVolume.profile.components[i] as Vignette;
                }
                else if (globalVolume.profile.components[i] as FilmGrain != null)
                {
                    grain = globalVolume.profile.components[i] as FilmGrain;
                }
                else if (globalVolume.profile.components[i] as MotionBlur != null)
                {
                    motionBlur = globalVolume.profile.components[i] as MotionBlur;
                }
                else if (globalVolume.profile.components[i] as Bloom != null)
                {
                    bloom = globalVolume.profile.components[i] as Bloom;
                }
                else if (globalVolume.profile.components[i] as Tonemapping != null)
                {
                    toneMapping = globalVolume.profile.components[i] as Tonemapping;
                }
                else if (globalVolume.profile.components[i] as GradientSky != null)
                {
                    gradientSky = globalVolume.profile.components[i] as GradientSky;
                }
                else if (globalVolume.profile.components[i] as Fog != null)
                {
                    volFog = globalVolume.profile.components[i] as Fog;
                }
                else if (globalVolume.profile.components[i] as ChromaticAberration != null)
                {
                    chromaticAberration = globalVolume.profile.components[i] as ChromaticAberration;
                }
                else if (globalVolume.profile.components[i] as LiftGammaGain != null)
                {
                    lgg = globalVolume.profile.components[i] as LiftGammaGain;
                }
                else if (globalVolume.profile.components[i] as ColorAdjustments != null)
                {
                    colour = globalVolume.profile.components[i] as ColorAdjustments;
                }
                else if (globalVolume.profile.components[i] as LensDistortion != null)
                {
                    lensDistort = globalVolume.profile.components[i] as LensDistortion;
                }
                else if (globalVolume.profile.components[i] as Exposure != null)
                {
                    exposure = globalVolume.profile.components[i] as Exposure;
                }
                else if (globalVolume.profile.components[i] as ChannelMixer != null)
                {
                    channelMixer = globalVolume.profile.components[i] as ChannelMixer;
                }
            }
        }
    }

    void Start()
    {
        //foreach (UnityEngine.Experimental.Rendering.VolumeComponent comp in hdrpScene.components)
        //{
        //    Game.Log(comp);
        //}

        SetGameTime(GameplayControls.Instance.startingYear, GameplayControls.Instance.startingMonth, GameplayControls.Instance.startingDate, GameplayControls.Instance.dayZero, Game.Instance.sandboxStartTime, GameplayControls.Instance.yearZeroLeapYearCycle);

        //Reset we textures
        foreach (WetMaterial wet in wetMaterials)
        {
            wet.mat.SetFloat("_CoatMask", 0f);
        }

        //Start the test scene
        if (isTestScene)
        {
            StartTestScene();
        }

        UnloadPipes += ExecuteUnloadPipes;

        //Instance brodcast material
        broadcastMaterialInstanced = Instantiate(broadcastMaterial);
        broadcastMaterialInstanced.EnableKeyword("_TEX_ON");
        broadcastMaterialInstanced.EnableKeyword("_EMISSION");
        broadcastMaterialInstanced.EnableKeyword("_EMISSIVE_COLOR_MAP");
    }

    public void StartTestScene()
    {
        isTestScene = true;

        int citySize = 5;
        CityData.Instance.citySize = new Vector2(citySize, citySize);
        PathFinder.Instance.tileSize = new Vector3(CityControls.Instance.cityTileSize.x / CityControls.Instance.tileMultiplier, CityControls.Instance.cityTileSize.y / CityControls.Instance.tileMultiplier, CityControls.Instance.cityTileSize.z / CityControls.Instance.tileMultiplier);
        PathFinder.Instance.nodeSize = new Vector3(PathFinder.Instance.tileSize.x / CityControls.Instance.nodeMultiplier, PathFinder.Instance.tileSize.y / CityControls.Instance.nodeMultiplier, PathFinder.Instance.tileSize.z);

        PathFinder.Instance.citySizeReal = new Vector2(CityData.Instance.citySize.x * CityControls.Instance.cityTileSize.x, CityData.Instance.citySize.y * CityControls.Instance.cityTileSize.y);
        PathFinder.Instance.halfCitySizeReal = PathFinder.Instance.citySizeReal * 0.5f;
        PathFinder.Instance.tileCitySize = CityData.Instance.citySize * CityControls.Instance.tileMultiplier;
        PathFinder.Instance.nodeCitySize = PathFinder.Instance.tileCitySize * CityControls.Instance.nodeMultiplier;

        //Create a dummy area in data...
        //Create city tiles
        for (int i = -1; i < citySize + 1; i++)
        {
            for (int u = -1; u < citySize + 1; u++)
            {
                //Load actual city
                if (i >= 0 && u >= 0 && i < citySize && u < citySize)
                {
                    //Create city tile
                    GameObject newTile = Instantiate(PrefabControls.Instance.cityTile, PrefabControls.Instance.cityContainer.transform);
                    CityTile cityTile = newTile.GetComponent<CityTile>();

                    Vector2Int cityCoord = new Vector2Int(i, u);
                    cityTile.Setup(cityCoord);
                }
            }
        }

        //Fill pathfinding map with street locations
        for (int i = 0; i < citySize; i++)
        {
            for (int n = 0; n < citySize; n++)
            {
                CityTile currentCityTile = CityData.Instance.cityTiles[new Vector2Int(i, n)];

                //Zero point of the groundMap tile.
                Vector2Int localZero = new Vector2Int(Mathf.RoundToInt(i * CityControls.Instance.tileMultiplier), Mathf.RoundToInt(n * CityControls.Instance.tileMultiplier));

                //Fill streets pathfinding map...
                for (int r = 0; r < CityControls.Instance.tileMultiplier; r++)
                {
                    for (int p = 0; p < CityControls.Instance.tileMultiplier; p++)
                    {
                        //Is this the edge within range of 1, if so this is a street tile...
                        //if ((r == 0 || r == CityControls.Instance.tileMultiplier - 1) || (p == 0 || p == CityControls.Instance.tileMultiplier - 1))
                        //{
                        Vector3Int currentTileCoord = new Vector3Int(localZero.x + r, localZero.y + p, 0); //Current tile coord

                        //Do not create street if this is 4 or closer to the edge
                        //if (currentTileCoord.x <= citySize || currentTileCoord.x >= citySize * CityControls.Instance.tileMultiplier - 6)
                        //{
                        //    continue;
                        //}
                        //else if (currentTileCoord.y <= citySize || currentTileCoord.y >= citySize * CityControls.Instance.tileMultiplier - 6)
                        //{
                        //    continue;
                        //}

                        NewTile newTile = new NewTile();
                        newTile.name = "Pathfinder Created Tile " + currentTileCoord;

                        //Instead of running a setup here, leave that to the loading of buildings: Instead add to the pathfinding map so you can complete the rest of this setup...
                        newTile.globalTileCoord = currentTileCoord;

                        newTile.SetAsOutside(true);
                        newTile.cityTile = currentCityTile;
                        newTile.isEdge = true; //This is an edge tile
                        PathFinder.Instance.tileMap.Add(currentTileCoord, newTile);
                        //}
                    }
                }
            }
        }

        GameObject newDistrict = Instantiate(PrefabControls.Instance.district, PrefabControls.Instance.cityContainer.transform);
        DistrictController dist = newDistrict.GetComponent<DistrictController>();
        dist.Setup(null);

        GameObject newBlockObj = Instantiate(PrefabControls.Instance.block, PrefabControls.Instance.cityContainer.transform);
        BlockController newBlock = newBlockObj.GetComponent<BlockController>();
        newBlock.Setup(dist);

        foreach (KeyValuePair<Vector2Int, CityTile> pair in CityData.Instance.cityTiles)
        {
            dist.AddCityTile(pair.Value);
        }

        dist.AddBlock(newBlock);

        GameObject newStreet = Instantiate(PrefabControls.Instance.street, PrefabControls.Instance.cityContainer.transform);
        StreetController street = newStreet.GetComponent<StreetController>();
        street.Setup(dist);

        for (int i = 0; i < citySize; i++)
        {
            for (int n = 0; n < citySize; n++)
            {
                CityTile currentCityTile = CityData.Instance.cityTiles[new Vector2Int(i, n)];

                //Zero point of the groundMap tile.
                Vector2Int localZero = new Vector2Int(Mathf.RoundToInt(i * CityControls.Instance.tileMultiplier), Mathf.RoundToInt(n * CityControls.Instance.tileMultiplier));

                //Fill streets pathfinding map...
                for (int r = 0; r < CityControls.Instance.tileMultiplier; r++)
                {
                    for (int p = 0; p < CityControls.Instance.tileMultiplier; p++)
                    {
                        Vector3Int currentTileCoord = new Vector3Int(localZero.x + r, localZero.y + p, 0); //Current tile coord

                        if (PathFinder.Instance.tileMap.ContainsKey(currentTileCoord))
                        {
                            NewTile t = PathFinder.Instance.tileMap[currentTileCoord];
                            street.AddTile(t);
                            t.SetupExterior(currentCityTile, currentTileCoord);
                        }
                    }
                }
            }
        }

        //GameObject newObj = Instantiate(PrefabControls.Instance.room, PrefabControls.Instance.cityContainer.transform);
        //NewRoom newRoom = newObj.GetComponent<NewRoom>();
        //newRoom.Setup(street, CityControls.Instance.streetRoom);

        //for (int i = 0; i < street.nodes.Count; i++)
        //{
        //    GameObject newObj = Instantiate(PrefabControls.Instance.debugAccess, PrefabControls.Instance.cityContainer.transform);
        //    newObj.transform.position = street.nodes[i].position;
        //}

        street.rooms[0].SetVisible(true, false, "Test Scene");
        Player.Instance.currentRoom = street.rooms[0];

        //Set fps mode
        CameraController.Instance.SetupFPS();

        //Force update of groundmap to update groundmap-based culling systems
        Player.Instance.OnCityTileChange();

        //Force update of position within building etc. Need to do this for proper lighting/culling update
        //Player.Instance.currentBuilding.SetPlayerPresentOnGroundmap(true);
        //Player.Instance.UpdatePlayerPosition();

        //Activate HUD canvas
        InterfaceController.Instance.SetInterfaceActive(true);

        //Started game flag
        SessionData.Instance.startedGame = true;

        //Execute citizen behaviour
        CitizenBehaviour.Instance.StartGame();

        ResumeGame();

        //Start ambience SFX
        AudioController.Instance.StartAmbienceTracks();

        //Load broadcast schedule
        SessionData.Instance.currentBroadcastSchedule = SessionData.Instance.defaultBroadcastSchedule;

        if (ChapterController.Instance.loadedChapter != null && ChapterController.Instance.loadedChapter.broadcastSchedule != null)
        {
            SessionData.Instance.currentBroadcastSchedule = ChapterController.Instance.loadedChapter.broadcastSchedule;
        }

        //Fade out menu
        MainMenuController.Instance.EnableMainMenu(false, true, true);
    }

    public void SetGameTime(int newYear, int newMonth, int newDate, int newDay, float newStartingTime, int newLeapYearCycle)
    {
        yearInt = newYear;
        monthInt = newMonth;
        month = MonthFromInt(monthInt);
        monthTempMultiplier = CityControls.Instance.weatherSettings.monthTempCurve.Evaluate((float)month / 11f);
        dateInt = newDate;
        dayInt = newDay;
        decimalClock = newStartingTime;
        leapYearCycle = newLeapYearCycle;
        formattedClock = FloatMinutes24H(newStartingTime);

        decimalClockDouble = decimalClock;
        SetTimeSpeed(GameplayControls.Instance.startingTimeSpeed);

        while (leapYearCycle >= 4)
        {
            leapYearCycle -= 4;
        }

        //Get the gameTime from the above
        gameTime = ParseGameTime(decimalClock, dateInt, monthInt, yearInt, out dayInt, out leapYearCycle);
        gameTimeDouble = gameTime;
        day = WeekdayFromInt(dayInt);
        publicYear = yearInt + GameplayControls.Instance.publicYearZero;

        Game.Log("CityGen: Set game time to: " + gameTime + " (" + decimalClock + ")");

        UpdateSkyboxGraidentTargets();
        UpdateUIClock();
        UpdateUIDay();
    }

    public void SetGameTime(float newGameTime, int newLeapYearCycle)
    {
        gameTime = newGameTime;
        gameTimeDouble = newGameTime;
        leapYearCycle = newLeapYearCycle;

        int yearProper = 0;
        ParseTimeData(newGameTime, out decimalClock, out dayInt, out dateInt, out monthInt, out yearProper);

        yearInt = yearProper - GameplayControls.Instance.publicYearZero;

        decimalClockDouble = decimalClock;
        month = MonthFromInt(monthInt);
        monthTempMultiplier = CityControls.Instance.weatherSettings.monthTempCurve.Evaluate((float)month / 11f);
        formattedClock = FloatMinutes24H(decimalClock);

        SetTimeSpeed(GameplayControls.Instance.startingTimeSpeed);

        while (leapYearCycle >= 4)
        {
            leapYearCycle -= 4;
        }

        day = WeekdayFromInt(dayInt);
        publicYear = yearInt + GameplayControls.Instance.publicYearZero;

        Game.Log("CityGen: Set game time to: " + gameTime);

        UpdateSkyboxGraidentTargets();
        UpdateUIClock();
        UpdateUIDay();
    }

    public void UpdateSkyboxGraidentTargets()
    {
        skyboxGradientIndex = 0;

        if (SessionData.Instance.isDialogEdit) return;

        for (int i = 0; i < CityControls.Instance.weatherSettings.skyboxGradientGrading.Count; i++)
        {
            if (decimalClock > CityControls.Instance.weatherSettings.skyboxGradientGrading[i].time)
            {
                skyboxGradientIndex++;

                if (skyboxGradientIndex >= CityControls.Instance.weatherSettings.skyboxGradientGrading.Count) skyboxGradientIndex = 0;

                continue;
            }
        }

        //Set the 'from' skybox colours
        fromSkyboxColours = CityControls.Instance.weatherSettings.skyboxGradientGrading[skyboxGradientIndex];
        if (skyboxGradientIndex + 1 >= CityControls.Instance.weatherSettings.skyboxGradientGrading.Count) toSkyboxColours = CityControls.Instance.weatherSettings.skyboxGradientGrading[0];
        else toSkyboxColours = CityControls.Instance.weatherSettings.skyboxGradientGrading[skyboxGradientIndex + 1];
    }

    public void SetTimeSpeed(TimeSpeed newTimeSpeed)
    {
        if (SessionData.Instance.isDialogEdit) return;

        currentTimeSpeed = newTimeSpeed;
        currentTimeMultiplier = GameplayControls.Instance.timeMultipliers[(int)currentTimeSpeed];

        Game.Log("Player: Set new time speed: " + currentTimeSpeed.ToString());

        SessionData.Instance.motionBlur.intensity.value = Game.Instance.motionBlurIntensity + GetGameSpeedMotionBlurModifier();

        //Behaviour routines need to change speed as they use InvokeRepeating
        if(CitizenBehaviour.Instance != null) CitizenBehaviour.Instance.GameSpeedChange();
    }

    public float GetGameSpeedMotionBlurModifier()
    {
        if(currentTimeSpeed == TimeSpeed.veryFast)
        {
            return Game.Instance.motionBlurGameSpeedModifier;
        }

        return 0f;
    }

    public void SetSceneProfile(SceneProfile newProfile, bool immediate = false)
    {
        if(newProfile != currentProfile)
        {
            Game.Log("Player: Setting to volume profile: " + currentProfile);
            currentProfile = newProfile;
            desiredSceneProfile = CityControls.Instance.sceneProfileSetup.Find(item => item.profile == newProfile);

            if(immediate)
            {
                foreach (CityControls.PPProfile pr in CityControls.Instance.sceneProfileSetup)
                {
                    if (pr == desiredSceneProfile)
                    {
                        currentSceneProfile = desiredSceneProfile;
                        pr.objectRef.SetActive(true);
                        pr.volume.priority = 1;
                        pr.volume.weight = 1;
                    }
                    else
                    {
                        pr.volume.priority = -1;
                        pr.volume.weight = 0;
                        pr.objectRef.SetActive(false);
                    }
                }
            }
        }
    }

    private void Update()
    {
        //Auto reset
        if (Game.Instance.demoAutoReset && Game.Instance.demoMode)
        {
            if (ChapterController.Instance != null && ChapterController.Instance.currentPart >= Game.Instance.resetChapterPart)
            {
                if (autoResetTimer >= Game.Instance.resetSeconds)
                {
                    if (Game.Instance.resetSaveGameName.Length >= 0)
                    {
                        var embedInternal = new DirectoryInfo(Application.streamingAssetsPath + "/Save");
                        List<FileInfo> embeddedFilesInternal = embedInternal.GetFiles("*.sod", SearchOption.AllDirectories).ToList();

                        FileInfo foundSave = embeddedFilesInternal.Find(item => item.Name == Game.Instance.resetSaveGameName);

                        if (foundSave != null)
                        {
                            RestartSafeController.Instance.saveStateFileInfo = foundSave;
                            RestartSafeController.Instance.newGameLoadCity = false;
                            RestartSafeController.Instance.generateNew = false;
                            RestartSafeController.Instance.loadSaveGame = true;

                            RestartSafeController.Instance.loadFromDirty = true;

                            Game.Log("CityGen: Scene is dirty, restarting to continue load process...");

                            //Reset the scene to safely remove everything
                            AudioController.Instance.StopAllSounds();
                            UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
                        }
                        else
                        {
                            foreach (FileInfo info in embeddedFilesInternal)
                            {
                                Game.LogError("Found save: " + info.Name);
                            }
                        }
                    }
                }
                else
                {
                    autoResetTimer += Time.deltaTime;
                }
            }
        }

        if (play)
        {
            //Auto pause
            if(Game.Instance.autoPause)
            {
                if(autoPauseTimer >= Game.Instance.autoPauseSeconds)
                {
                    PauseGame(true);
                    InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.ComposeText(Strings.Get("ui.gamemessage", "You can open your case board at any time by pressing F"), null, Strings.LinkSetting.forceNoLinks), newIcon: InterfaceControls.Icon.pin);
                }
                else if(!CutSceneController.Instance.cutSceneActive && !BioScreenController.Instance.isOpen)
                {
                    autoPauseTimer += Time.deltaTime;
                }
            }

            //Time steps in seconds
            gameTimePassedThisFrame = Time.deltaTime * currentTimeMultiplier * 0.0025f;

            //Use doubles to keep track of exact time to avoid floating point errors
            decimalClockDouble += gameTimePassedThisFrame;
            gameTimeDouble += gameTimePassedThisFrame;

            //Round times into their float values for referencing
            decimalClock = Toolbox.Instance.RoundToPlaces(decimalClockDouble, 4);
            gameTime = Toolbox.Instance.RoundToPlaces(gameTimeDouble, 4);

            //Check for hour change
            int hour = Mathf.FloorToInt(gameTime);

            if (hour != prevHour)
            {
                prevHour = hour;

                CitizenBehaviour.Instance.OnHourChange();

                //Fire event
                if (OnHourChange != null)
                {
                    OnHourChange();
                }
            }

            //24 hour clock
            if (decimalClockDouble >= 24)
            {
                //Use an accuracy of 2dp to start the day after a change
                float floatingAccuracy = Toolbox.Instance.RoundToPlaces(decimalClock - 24, 2);

                //To ensure sync, start day from 0 hours
                decimalClockDouble = floatingAccuracy;
                decimalClock = floatingAccuracy;
                gameTime = Mathf.Round(gameTime) + floatingAccuracy;
                gameTimeDouble = Mathf.Round((float)gameTimeDouble) + floatingAccuracy;

                dateInt++;
                dayInt++;

                if (dayInt >= 7)
                {
                    dayInt -= 7;
                }

                day = WeekdayFromInt(dayInt);

                //Calc days in month including leap year
                int daysInMonth = daysInMonths[monthInt];
                if (leapYearCycle == 3 && monthInt == 1) daysInMonth += 1;

                if (dateInt >= daysInMonth)
                {
                    dateInt -= daysInMonth;
                    monthInt++;

                    if (monthInt >= 12)
                    {
                        monthInt -= 12;
                        yearInt++;
                        leapYearCycle++;
                        publicYear = yearInt + GameplayControls.Instance.publicYearZero;

                        while (leapYearCycle >= 4)
                        {
                            leapYearCycle -= 4;
                        }
                    }

                    month = MonthFromInt(monthInt);

                    monthTempMultiplier = CityControls.Instance.weatherSettings.monthTempCurve.Evaluate((float)month / 11f);
                }

                //Birthday check for citizens
                foreach (Citizen cit in CityData.Instance.citizenDirectory)
                {
                    cit.BirthdayCheck();
                }

                //Execute day change
                //DayChange.Instance.ExecuteDayChange();

                //Trigger day check behaviours
                CitizenBehaviour.Instance.OnDayChange();

                //Update day display
                UpdateUIDay();

                //New newspaper
                NewspaperController.Instance.GenerateNewNewspaper();
            }

            formattedClock = FloatMinutes24H(decimalClock);
            UpdateUIClock();

            //Set time of day
            //'Morning' starts at 3am
            if (decimalClock > 3 && decimalClock <= 12 && timeOfDay != TimeOfDay.morning)
            {
                timeOfDay = TimeOfDay.morning;
            }
            else if (decimalClock > 12 && decimalClock <= 5 && timeOfDay != TimeOfDay.afternoon)
            {
                timeOfDay = TimeOfDay.afternoon;
            }
            else if (decimalClock > 5 && decimalClock <= 3 && timeOfDay != TimeOfDay.evening)
            {
                timeOfDay = TimeOfDay.evening;
            }

            //Set day progress
            dayProgress = decimalClock / 24f;

            //Pass time of day to city ambient
            if (CityConstructor.Instance == null || !CityConstructor.Instance.preSimActive)
            {
                if(weatherChangeTimer >= 0.33f) //At its fastest rate the weather changes every ~20 mins
                {
                    float windVal = CityControls.Instance.weatherSettings.weatherExtremityCurve.Evaluate(Toolbox.Instance.Rand(0f, 1f));
                    float rainVal = 0;
                    float lightningVal = 0;
                    float snowVal = 0f;
                    float snowChance = CityControls.Instance.weatherSettings.monthSnowChanceCurve.Evaluate((float)month / 11f);

                    if(Toolbox.Instance.Rand(0f, 1f) <= snowChance)
                    {
                        snowVal = CityControls.Instance.weatherSettings.weatherExtremityCurve.Evaluate(Toolbox.Instance.Rand(0f, 1f));
                        snowVal = Toolbox.Instance.RandomRangeWeighted(0f, 1f, snowVal);
                    }

                    if (snowVal <= 0.1f)
                    {
                        rainVal = CityControls.Instance.weatherSettings.weatherExtremityCurve.Evaluate(Toolbox.Instance.Rand(0f, 1f));
                        lightningVal = CityControls.Instance.weatherSettings.weatherExtremityCurve.Evaluate(Toolbox.Instance.Rand(0f, 1f));
                    }

                    SetWeather(rainVal, windVal, snowVal, lightningVal, Toolbox.Instance.Rand(0.1f, 0.4f));

                    weatherChangeTimer = 0f;
                }
                else
                {
                    weatherChangeTimer += (float)gameTimePassedThisFrame * (Game.Instance.weatherChangeFrequency * 0.7f); //Apply a general multiplier here...
                }

                bool weatherChange = false;

                //Rain cannot exist while city snow is active past 25%
                if (citySnow >= 0.25f)
                {
                    desiredRain = 0;
                }

                //Snow cannot exist while city wetness is active past 25%
                if(cityWetness >= 0.25f)
                {
                    desiredSnow = 0;
                }

                if (currentSnow < desiredSnow && currentRain <= 0.01f)
                {
                    currentSnow += Time.deltaTime * currentTimeMultiplier * transitionSpeed;
                    if (currentSnow > desiredSnow) currentSnow = desiredSnow;
                    weatherChange = true;
                }
                else if (currentSnow > desiredSnow)
                {
                    currentSnow -= Time.deltaTime * currentTimeMultiplier * transitionSpeed;
                    if (currentSnow < desiredSnow) currentSnow = desiredSnow;
                    weatherChange = true;
                }

                if (currentRain < desiredRain && currentSnow <= 0.01f)
                {
                    currentRain += Time.deltaTime * currentTimeMultiplier * transitionSpeed;
                    if (currentRain > desiredRain) currentRain = desiredRain;
                    weatherChange = true;
                }
                else if (currentRain > desiredRain)
                {
                    currentRain -= Time.deltaTime * currentTimeMultiplier * transitionSpeed;
                    if (currentRain < desiredRain) currentRain = desiredRain;
                    weatherChange = true;
                }

                if (currentWind < desiredWind)
                {
                    currentWind += Time.deltaTime * currentTimeMultiplier * transitionSpeed;
                    if (currentWind > desiredWind) currentWind = desiredWind;
                    weatherChange = true;
                    ExecuteWindChange();
                }
                else if (currentWind > desiredWind)
                {
                    currentWind -= Time.deltaTime * currentTimeMultiplier * transitionSpeed;
                    if (currentWind < desiredWind) currentWind = desiredWind;
                    weatherChange = true;
                    ExecuteWindChange();
                }

                if (currentLightning < desiredLightning)
                {
                    currentLightning += Time.deltaTime * currentTimeMultiplier * transitionSpeed;
                    if (currentLightning > desiredLightning) currentLightning = desiredLightning;
                    weatherChange = true;
                }
                else if (currentLightning > desiredLightning)
                {
                    currentLightning -= Time.deltaTime * currentTimeMultiplier * transitionSpeed;
                    if (currentLightning < desiredLightning) currentLightning = desiredLightning;
                    weatherChange = true;
                }

                if (weatherChange)
                {
                    ExecuteWeatherChange();
                }

                //Set city wetness
                bool wetnessChange = false;

                if (currentRain > 0f && cityWetness < 1f)
                {
                    cityWetness += currentRain * ((float)gameTimePassedThisFrame / CityControls.Instance.timeForCityToGetWet);
                    cityWetness = Mathf.Clamp01(cityWetness);
                    wetnessChange = true;
                }
                else if (currentRain <= 0f && cityWetness > 0f)
                {
                    cityWetness -= ((float)gameTimePassedThisFrame / CityControls.Instance.timeForCityToGetDry);
                    cityWetness = Mathf.Clamp01(cityWetness);
                    wetnessChange = true;
                }

                if (currentSnow > 0f && citySnow < 1f)
                {
                    citySnow += currentSnow * ((float)gameTimePassedThisFrame / CityControls.Instance.timeForCityToGetSnow);
                    citySnow = Mathf.Clamp01(citySnow);
                    wetnessChange = true;
                }
                else if (currentSnow <= 0f && citySnow > 0f)
                {
                    citySnow -= ((float)gameTimePassedThisFrame / CityControls.Instance.timeForCityToGetNotSnow);
                    citySnow = Mathf.Clamp01(citySnow);
                    wetnessChange = true;
                }

                //Set materials to wet
                if (wetnessChange)
                {
                    ExecuteWetnessChange();
                }

                //Simulate lightning strike
                if (currentLightning > 0f)
                {
                    lightningTimer += Toolbox.Instance.Rand(0f, currentLightning) * Time.deltaTime * currentTimeMultiplier;

                    if (AudioDebugging.Instance.overrideThunderDelay && lightningTimer >= AudioDebugging.Instance.thunderDelay)
                    {
                        ExecuteLightningStrike();
                        lightningTimer = 0f;
                    }
                    else if (!AudioDebugging.Instance.overrideThunderDelay && lightningTimer >= CityControls.Instance.weatherSettings.thunderDelay)
                    {
                        ExecuteLightningStrike();
                        lightningTimer = 0f;
                    }
                }

                AudioController.Instance.PassTimeOfDay();

                SetSceneVisuals(decimalClock);

                //Set flickering neon
                for (int i = 0; i < CityControls.Instance.neonColours.Count; i++)
                {
                    CityControls.NeonMaterial neonMat = CityControls.Instance.neonColours[i];

                    if (neonMat.flickingMat != null)
                    {
                        neonMat.interval += Time.deltaTime * SessionData.Instance.currentTimeMultiplier;

                        //Turn on or off flicker state after x interval
                        if (!neonMat.flickerInterval && neonMat.interval >= neonMat.intervalTime)
                        {
                            neonMat.interval = 0f;
                            neonMat.flickerInterval = true;
                            neonMat.intervalTime = Toolbox.Instance.Rand(0.25f, 2f); //If off turn on for this time
                        }
                        else if (neonMat.flickerInterval && neonMat.interval >= neonMat.intervalTime)
                        {
                            neonMat.interval = 0f;
                            neonMat.flickerInterval = false;
                            neonMat.intervalTime = Toolbox.Instance.Rand(0.1f, 10f); //If on turn off for this time
                        }

                        if (neonMat.flickerState < 1f && neonMat.flickerSwitch)
                        {
                            neonMat.flickerState += Time.deltaTime * neonMat.pulseSpeed;
                        }
                        else if (neonMat.flickerState >= 1f && neonMat.flickerSwitch)
                        {
                            neonMat.flickerSwitch = false;

                            //Flicker variables
                            neonMat.flickerColourMultiplier = Toolbox.Instance.Rand(0.1f, 0.95f);
                            neonMat.pulseSpeed = Toolbox.Instance.Rand(20f, 22f);
                        }

                        if (neonMat.flickerInterval && neonMat.flickerState > 0f && !neonMat.flickerSwitch)
                        {
                            neonMat.flickerState -= Time.deltaTime * neonMat.pulseSpeed;
                        }
                        else if (neonMat.flickerState <= 0f && !neonMat.flickerSwitch)
                        {
                            neonMat.flickerSwitch = true;

                            //Flicker variables
                            neonMat.flickerColourMultiplier = Toolbox.Instance.Rand(0.1f, 0.95f);
                        }

                        neonMat.flickerState = Mathf.Clamp01(neonMat.flickerState);
                        neonMat.brightness = neonMat.flickerColourMultiplier;
                        neonMat.flickingMat.SetColor("_BaseColor", neonMat.neonColour * neonMat.flickerColourMultiplier);
                    }
                }

                //Elevators
                if (activeElevators.Count > 0)
                {
                    for (int i = 0; i < activeElevators.Count; i++)
                    {
                        activeElevators[i].ElevatorUpdate();
                    }
                }

                //Broadcast
                if (currentBroadcastSchedule != null)
                {
                    //New show...
                    if (currentShow == null)
                    {
                        currentScheduleIndex++;
                        if (currentScheduleIndex >= currentBroadcastSchedule.broadcasts.Count) currentScheduleIndex = 0;

                        currentShow = currentBroadcastSchedule.broadcasts[currentScheduleIndex];
                        currentShowProgressSeconds = 0f;
                        currentShowImageProgress = 9999999f; //Will force an image change right away
                        currentImageIndex = -1;

                        if (currentShow.audioEvent != null)
                        {
                            FMODUnity.RuntimeManager.StudioSystem.getEventByID(FMOD.GUID.Parse(currentShow.audioEvent.guid), out currentShowEventDescription);
                            currentShowEventDescription.getLength(out currentShowAudioLength); //returns length in milliseconds
                        }

                        currentShowImageLength = currentShow.totalSpriteCount;

                        broadcastMaterialInstanced.SetTexture("_EmissiveColorMap", currentShow.spriteSheet); //Set the correct sprite sheet
                        broadcastMaterialInstanced.SetTextureScale("_EmissiveColorMap", new Vector2(1f/ (float)currentShow.indexWidth, 1f / (float)currentShow.indexHeight));

                        //broadcastMaterialInstanced.SetTexture("_BaseColorMap", currentShow.spriteSheet); //Set the correct sprite sheet
                        //broadcastMaterialInstanced.SetTextureScale("_BaseColorMap", new Vector2(1f / (float)currentShow.indexWidth, 1f / (float)currentShow.indexHeight));

                        //Change SFX on all television loops...
                        foreach (AudioController.LoopingSoundInfo loop in AudioController.Instance.loopingSounds)
                        {
                            if (loop.isBroadcast)
                            {
                                //Stop existing
                                loop.audioEvent.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
                                loop.audioEvent.release();
                                loop.isValid = loop.audioEvent.isValid();

                                //New audio
                                loop.eventPreset = currentShow.audioEvent;

                                loop.UpdateOcclusion();
                            }
                        }
                    }

                    //New picture if existing is null or it's time to change...
                    if (currentShow != null && currentShowImageProgress >= currentShow.changeImageEvery)
                    {
                        if(currentShow.order == BroadcastPreset.ImageOrder.ordered)
                        {
                            //Scan for next
                            currentImageIndex++;
                        }
                        else if(currentShow.order == BroadcastPreset.ImageOrder.random)
                        {
                            currentImageIndex = Toolbox.Instance.Rand(0, currentShowImageLength);
                        }

                        //Loop index at end of images
                        if (currentImageIndex >= currentShowImageLength) currentImageIndex = 0;

                        //Set offset to display a new image from the atlas
                        int h = 0;
                        int v = 0;

                        int cursor = currentImageIndex;

                        while(cursor >= currentShow.indexWidth)
                        {
                            cursor -= currentShow.indexWidth;
                            v++;
                        }

                        h = cursor;

                        Vector2 offset = new Vector2((float)h / (float)currentShow.indexWidth, (float)(currentShow.indexHeight - 1 - v) / (float)currentShow.indexHeight);

                        //Game.Log("Displaying img index " + currentImageIndex + " (" + h + ", " + v + ") = " + offset);

                        //Set texture offset
                        broadcastMaterialInstanced.SetTextureOffset("_EmissiveColorMap", offset);
                        //broadcastMaterialInstanced.SetTextureOffset("_BaseColorMap", offset);
                        currentShowImageProgress = 0f;
                    }

                    currentShowImageProgress += Time.deltaTime * currentTimeMultiplier;

                    //Progress show in actual seconds * time multiplier
                    currentShowProgressSeconds += Time.deltaTime * currentTimeMultiplier;

                    //End show
                    if (currentShowProgressSeconds >= currentShowAudioLength / 1000f)
                    {
                        currentShow = null;
                    }
                }

                //Lightswitch material
                if(InteriorControls.Instance.pulsingLightswitch != null && InteriorControls.Instance.pulsingLightswitchSwitch != null)
                {
                    if(!lightswitchPulseMode && lightswitchPulse < 1f)
                    {
                        lightswitchPulse += Time.deltaTime;

                        if (lightswitchPulse >= 1f)
                        {
                            lightswitchPulseMode = true;
                        }
                    }
                    else if(lightswitchPulseMode && lightswitchPulse > 0f)
                    {
                        lightswitchPulse -= Time.deltaTime;

                        if (lightswitchPulse <= 0f)
                        {
                            lightswitchPulseMode = false;
                        }
                    }

                    Color col = Color.Lerp(Color.black, Color.white, lightswitchPulse);

                    InteriorControls.Instance.pulsingLightswitch.SetColor("_EmissiveColor", col);
                    InteriorControls.Instance.pulsingLightswitchSwitch.SetColor("_EmissiveColor", col);
                }
            }

            //Set temperature
            temperature = (monthTempMultiplier * CityControls.Instance.weatherSettings.dayTempCurve.Evaluate(dayProgress) + (CityControls.Instance.weatherSettings.NoRainModifier * (1f - currentRain)) + (CityControls.Instance.weatherSettings.NoWindModifier * (1f - currentWind)) + (CityControls.Instance.weatherSettings.NoSnowModifier * (1f - currentSnow))) - 2.5f;

            //Volume fading sounds
            if(AudioController.Instance.volumeChangingSounds.Count > 0)
            {
                AudioController.Instance.UpdateVolumeChanging();
            }

            //Delayed sounds
            if (AudioController.Instance.delayedSound.Count > 0)
            {
                for (int i = 0; i < AudioController.Instance.delayedSound.Count; i++)
                {
                    AudioController.DelayedSoundInfo delayed = AudioController.Instance.delayedSound[i];
                    delayed.delay -= Time.deltaTime * currentTimeMultiplier;

                    if (delayed.delay <= 0)
                    {
                        if(delayed.eventPreset.debug) Game.Log("Audio: Play delayed sound " + i + "/" + AudioController.Instance.delayedSound.Count + ": " + delayed.eventPreset.name);

                        if (delayed.is2D)
                        {
                            AudioController.Instance.Play2DSound(delayed.eventPreset, delayed.parameters, delayed.volumeOverride);
                        }
                        else
                        {
                            AudioController.Instance.PlayWorldOneShot(delayed.eventPreset, delayed.who, delayed.location, delayed.worldPosition, null, delayed.parameters, delayed.volumeOverride, delayed.additionalSources, delayed.forceIgnoreOcclusion);
                        }

                        AudioController.Instance.delayedSound.Remove(delayed);
                        i--;
                    }
                }
            }

            //Drunk Oscillator
            if(StatusController.Instance.drunkVision > 0f || StatusController.Instance.drunkControls > 0f)
            {
                drunkOscillatorX += Time.deltaTime * Toolbox.Instance.Rand(GameplayControls.Instance.drunkOscillationSpeed.x, GameplayControls.Instance.drunkOscillationSpeed.y);
                if (drunkOscillatorX >= 1f) drunkOscillatorX -= 1f;
                drunkOscillatorY += Time.deltaTime * Toolbox.Instance.Rand(GameplayControls.Instance.drunkOscillationSpeed.x, GameplayControls.Instance.drunkOscillationSpeed.y);
                if (drunkOscillatorY >= 1f) drunkOscillatorY -= 1f;

                drunkOscillation = new Vector2(GameplayControls.Instance.oscillatorX.Evaluate(drunkOscillatorX) * 12f, GameplayControls.Instance.oscillatorX.Evaluate(drunkOscillatorY) * 12f);
            }

            //Shiver Oscillator
            if(StatusController.Instance.shiverVision > 0f)
            {
                shiverOscillatorX += Time.deltaTime * Toolbox.Instance.Rand(GameplayControls.Instance.shiverOscillationSpeed.x, GameplayControls.Instance.shiverOscillationSpeed.y);
                if (shiverOscillatorX >= 1f) shiverOscillatorX -= 1f;
                shiverOscillatorY += Time.deltaTime * Toolbox.Instance.Rand(GameplayControls.Instance.shiverOscillationSpeed.x, GameplayControls.Instance.shiverOscillationSpeed.y);
                if (shiverOscillatorY >= 1f) shiverOscillatorY -= 1f;

                shiverProgress += Time.deltaTime * Toolbox.Instance.Rand(0.1f, 0.6f);
                if (shiverProgress >= 5f) shiverProgress -= 5f;
                float shiverFluctuation = GameplayControls.Instance.shiverFluctuation.Evaluate(shiverProgress);

                shiverOscillation = new Vector2(GameplayControls.Instance.oscillatorX.Evaluate(shiverOscillatorX) * shiverFluctuation * 0.5f, GameplayControls.Instance.oscillatorX.Evaluate(shiverOscillatorY) * shiverFluctuation * 0.5f);
            }

            //Drunk lens distort/headache vision
            if(StatusController.Instance.drunkLensDistort != 0f || StatusController.Instance.headacheVision != 0f)
            {
                lensDistort.active = true;

                if (StatusController.Instance.drunkLensDistort != 0f)
                {
                    drunkLensProgress += Time.deltaTime * Toolbox.Instance.Rand(GameplayControls.Instance.drunkLensDistortSpeed.x, GameplayControls.Instance.drunkLensDistortSpeed.x);
                    if (drunkLensProgress >= 5) drunkLensProgress -= 5f;
                }

                float headache = 0f;

                if(StatusController.Instance.headacheVision != 0f)
                {
                    headacheProgress += Time.deltaTime;
                    if (headacheProgress >= 1.7f) headacheProgress -= 1.7f;

                    headache = GameplayControls.Instance.headacheFluctuation.Evaluate(headacheProgress) * StatusController.Instance.headacheVision;
                }

                lensDistort.intensity.value = (GameplayControls.Instance.drunkLensDistortOscillator.Evaluate(drunkLensProgress) * StatusController.Instance.drunkLensDistort) + headache;

            }
            else if(lensDistort.IsActive())
            {
                lensDistort.active = false;
            }

            //Close breakers
            for (int i = 0; i < GameplayController.Instance.closedBreakers.Count; i++)
            {
                Interactable b = GameplayController.Instance.closedBreakers[i];
                b.SetValue(Mathf.Clamp01(b.val + (float)gameTimePassedThisFrame / (GameplayControls.Instance.breakerResetTime * (1f + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.securityBreakerModifier)))));
            }

            //Dynamic shadow system
            //dynamicShadowFrameCounter++;

            //if (dynamicShadowFrameCounter >= Game.Instance.updateShadowsEveryXFrames)
            //{
            //    dynamicShadowFrameCounter = 0;

            //    Game.Instance.lastShadowsUpdatedCount = 0;

            //    //Update shadows
            //    foreach (NewRoom room in CityData.Instance.visibleRooms)
            //    {
            //        //Room needs update if:
            //        //1: Physics objects are active within the room
            //        //2: Actors are moving within the room
            //        //3: Doors are animating within the room
            //        if(room.TestForDynamicShadowsUpdate())
            //        {
            //            room.UpdateShadowMaps();
            //        }
            //    }
            //}
        }

        //Game timer
        if(Game.Instance.timeLimited && !CutSceneController.Instance.cutSceneActive && !PopupMessageController.Instance.active)
        {
            if(!Game.Instance.startTimerAfterApartmentExit || (Game.Instance.sandboxMode || ChapterController.Instance.currentPart >= 21 || Player.Instance.currentGameLocation != Player.Instance.home))
            {
                if(!MainMenuController.Instance.mainMenuActive)
                {
                    if(gameTimeLimit > 0f)
                    {
                        if(Game.Instance.pauseTimerOnGamePause)
                        {
                            if(play)
                            {
                                gameTimeLimit -= Time.deltaTime;
                            }
                        }
                        else
                        {
                            gameTimeLimit -= Time.deltaTime;
                        }
                    }

                    gameTimeLimit = Mathf.Max(0f, gameTimeLimit);
                    UpdateGameTimerText();

                    //Time limit reached!
                    if (gameTimeLimit <= 0f && !PopupMessageController.Instance.active)
                    {
                        PopupMessageController.Instance.OnLeftButton += EndDemo;
                        PopupMessageController.Instance.PopupMessage("Time Limit Reached", true, LButton: "End Demo");
                    }
                }
            }
        }

        if (pauseUnpauseDelay > 0)
        {
            pauseUnpauseDelay--;
        }

        //Transition scene
        if(desiredSceneProfile != currentSceneProfile)
        {
            foreach(CityControls.PPProfile pr in CityControls.Instance.sceneProfileSetup)
            {
                if(pr == desiredSceneProfile)
                {
                    pr.objectRef.SetActive(true);
                    if (pr.volume.priority != 1) pr.volume.priority = 1;

                    if (pr.volume.weight < 1f)
                    {
                        pr.volume.weight += Time.deltaTime * 0.4f;
                    }
                    else
                    {
                        //Game.Log("Transitioned to volume profile: " + currentSceneProfile);
                        currentSceneProfile = desiredSceneProfile;

                        foreach (CityControls.PPProfile pr2 in CityControls.Instance.sceneProfileSetup)
                        {
                            if(pr2 != desiredSceneProfile)
                            {
                                pr2.volume.priority = -1;
                                pr2.volume.weight = 0;
                                pr2.objectRef.SetActive(false);
                            }
                        }

                        break;
                    }
                }
                else if(pr == currentSceneProfile)
                {
                    pr.objectRef.SetActive(true);
                    if (pr.volume.weight > 0f)
                    {
                        pr.volume.weight -= Time.deltaTime;
                    }
                }
                else
                {
                    pr.volume.priority = -1;
                }
            }
        }
    }

    public void UpdateGameTimerText()
    {
        float minutes = Mathf.Floor((gameTimeLimit / 60f));

        //Return minutes only
        float seconds = Mathf.Floor(((gameTimeLimit / 60f) - minutes) * 60f) / 100f;

        InterfaceController.Instance.timerText.text = MinutesToClockString(minutes + seconds, true);
    }

    public void EndDemo()
    {
        PopupMessageController.Instance.OnLeftButton -= EndDemo;

        RestartSafeController.Instance.newGameLoadCity = false;
        RestartSafeController.Instance.generateNew = false;
        RestartSafeController.Instance.loadSaveGame = false;

        Game.Log("CityGen: Restarting game due to time limit...");

        AudioController.Instance.StopAllSounds();
        UnityEngine.SceneManagement.SceneManager.LoadScene("Main");
    }

    public void ExecuteSyncPhysics(PhysicsSyncType syncType)
    {
        if(syncType == PhysicsSyncType.now)
        {
            Physics.SyncTransforms(); //Immediately syncs transforms with the physics engine
        }
        else if(syncType == PhysicsSyncType.onPlayerMovement)
        {
            Player.Instance.fps.syncTransforms = true; //This flag will sync just before the player moves
        }
        else if(syncType == PhysicsSyncType.both)
        {
            Player.Instance.fps.syncTransforms = true;
            Physics.SyncTransforms();
        }
    }

    public void ExecuteWeatherChange()
    {
        //Pass weather params
        AudioController.Instance.PassWeatherParams();

        //Alter rain materials/systems
        float rainVisuals = currentRain;

        if(currentSnow >= 0.01f || currentRain >= 0.01f)
        {
            nearRainSheet.SetEnabled(true);
            farRainSheet.SetEnabled(true);
            PrecipitationParticleSystemController.Instance.SetEnabled(true);
        }
        else
        {
            nearRainSheet.SetEnabled(false);
            farRainSheet.SetEnabled(false);
            PrecipitationParticleSystemController.Instance.SetEnabled(false);
        }

        //Override with snow
        if (currentSnow >= 0.01f && currentRain <= 0.01f)
        {
            rainVisuals = currentSnow;

            nearRainSheet.SetSnowMode(true);
            farRainSheet.SetSnowMode(true);
            PrecipitationParticleSystemController.Instance.SetSnowMode(true);
            //RainParticleController.Instance.SetSnowMode(true);
            //RainParticleController.Instance.desiredParticleCount = Mathf.RoundToInt(Mathf.Lerp(particalSnowCountThreshold.x, particalSnowCountThreshold.y, rainVisuals));
        }
        else
        {
            nearRainSheet.SetSnowMode(false);
            farRainSheet.SetSnowMode(false);
            PrecipitationParticleSystemController.Instance.SetSnowMode(false);
            //RainParticleController.Instance.SetSnowMode(false);
            //RainParticleController.Instance.desiredParticleCount = Mathf.RoundToInt(Mathf.Lerp(particalRainCountThreshold.x, particalRainCountThreshold.y, rainVisuals));
        }

        nearRainSheet.material.SetFloat("Vector1_9C48CC0E", Mathf.Lerp(nearRainAlpha1Threshold.x, nearRainAlpha1Threshold.y, rainVisuals));
        nearRainSheet.material.SetFloat("Vector1_CAAA1509", Mathf.Lerp(nearRainAlpha2Threshold.x, nearRainAlpha2Threshold.y, rainVisuals));
        nearRainSheet.material.SetFloat("Vector1_85EA96A5", Mathf.Lerp(nearRainSpeedThreshold.x, nearRainSpeedThreshold.y, rainVisuals));
        nearRainSheet.material.SetVector("Vector2_E2361C8B", new Vector2(Mathf.Lerp(nearRainXTile1Threshold.x, nearRainXTile1Threshold.y, rainVisuals), 27.2f));
        nearRainSheet.material.SetVector("Vector2_10A9AE04", new Vector2(Mathf.Lerp(nearRainXTile2Threshold.x, nearRainXTile2Threshold.y, rainVisuals), 26.8f));

        farRainSheet.material.SetFloat("Vector1_9C48CC0E", Mathf.Lerp(farRainAlpha1Threshold.x, farRainAlpha1Threshold.y, rainVisuals));
        farRainSheet.material.SetFloat("Vector1_CAAA1509", Mathf.Lerp(farRainAlpha2Threshold.x, farRainAlpha2Threshold.y, rainVisuals));
        farRainSheet.material.SetFloat("Vector1_85EA96A5", Mathf.Lerp(farRainSpeedThreshold.x, nearRainSpeedThreshold.y, rainVisuals));
        farRainSheet.material.SetVector("Vector2_E2361C8B", new Vector2(Mathf.Lerp(farRainXTile1Threshold.x, farRainXTile1Threshold.y, rainVisuals), 27.2f));
        farRainSheet.material.SetVector("Vector2_10A9AE04", new Vector2(Mathf.Lerp(farRainXTile2Threshold.x, farRainXTile2Threshold.y, rainVisuals), 26.8f));

        //Set rain texture rain amount
        if (Game.Instance.enableRaindrops)
        {
            foreach (Material mat in raindropMaterials)
            {
                mat.SetFloat("_Rain", currentRain);
            }
        }

        //Set rain window texture rain amount
        //if (Game.Instance.enableRainyWindows)
        //{
            foreach (Material mat in rainyWindowMaterials)
            {
                mat.SetFloat("_Rain", currentRain);
            }
        //}

        //Fire event
        if (OnWeatherChange != null)
        {
            OnWeatherChange();
        }
    }

    public void ExecuteWetnessChange()
    {
        foreach (WetMaterial wet in wetMaterials)
        {
            wet.mat.SetFloat("_CityWetness", Mathf.Clamp01(cityWetness * wet.multiplier));
            wet.mat.SetFloat("_Snow", Mathf.Clamp01(citySnow * wet.multiplier));
        }
    }

    public void ExecuteWindChange()
    {
        foreach (WetMaterial wind in swingShaderMaterials)
        {
            wind.mat.SetFloat("_Wind", Mathf.Clamp01(currentWind * wind.multiplier));
        }
    }

    public void ExecuteLightningStrike()
    {
        if (!SessionData.Instance.play) return;
        if (!SessionData.Instance.startedGame) return;
        if (CityConstructor.Instance != null && CityConstructor.Instance.preSimActive) return;

        //Pick a random position anywhere in the city...
        //Strike can be -1 / +1 to each side...
        Vector2Int randomCityTilePos = new Vector2Int(Toolbox.Instance.Rand(-2, Mathf.RoundToInt(CityData.Instance.citySize.x + 2)), Toolbox.Instance.Rand(-2, Mathf.RoundToInt(CityData.Instance.citySize.y + 2)));

        //Game.Log("Stike at " + randomCityTilePos);

        CityTile chosenTile = null;
        NewBuilding tallestBuilding = null;
        float buildingHeight = 0;

        if (CityData.Instance.cityTiles.TryGetValue(randomCityTilePos, out chosenTile))
        {
            //Game.Log("Found valid building for lightning strike...");
        }

        if (chosenTile != null && chosenTile.building != null)
        {
            //Game.Log("Tile chosen contains building...");
            tallestBuilding = chosenTile.building;
            buildingHeight = chosenTile.building.preset.buildingHeight;
        }

        //Search for the tallest building within 9 tiles
        if (chosenTile != null)
        {
            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX8)
            {
                Vector2Int cityCoord = chosenTile.cityCoord + v2;
                CityTile foundTile = null;

                if (CityData.Instance.cityTiles.TryGetValue(cityCoord, out foundTile))
                {
                    if (foundTile.building != null)
                    {
                        //Game.Log("Found adjacent contains building...");

                        if (foundTile.building.preset.buildingHeight > buildingHeight)
                        {
                            tallestBuilding = foundTile.building;
                            buildingHeight = foundTile.building.preset.buildingHeight;
                        }
                    }
                }
            }
        }

        Vector3 lightningWorldPos = Vector3.zero;

        //We now have the closest tallest building, if any
        if (tallestBuilding != null)
        {
            //Spawn @ the building's lightning rod
            lightningWorldPos = tallestBuilding.buildingModelBase.transform.TransformPoint(tallestBuilding.preset.lightningRodLocalPos);
        }
        //Else strike a random location within this tile
        else
        {
            //Spawn @ a random location on the tile
            lightningWorldPos = CityData.Instance.CityTileToRealpos(randomCityTilePos + new Vector2(Toolbox.Instance.Rand(-0.5f, 0.5f), Toolbox.Instance.Rand(-0.5f, 0.5f))) + new Vector3(0f, -12.5f, 0);
        }

        //Game.Log("Lightning strike at " + lightningWorldPos + " building: " + tallestBuilding);

        //Strike here!
        GameObject newStrike = Instantiate(PrefabControls.Instance.lightningStrike, PrefabControls.Instance.cityContainer.transform);
        newStrike.transform.position = lightningWorldPos;

        //Always set end point in the sky
        LightningStrikeController strike = newStrike.GetComponent<LightningStrikeController>();
        strike.startPoint.transform.position = new Vector3(strike.startPoint.transform.position.x + Toolbox.Instance.Rand(-20f, 20f), 250f, strike.startPoint.transform.position.z + Toolbox.Instance.Rand(-20f, 20f));

        //Activate light
        LightController light = newStrike.GetComponentInChildren<LightController>();
        light.Setup(null, null, null, light.preset);

        //Calculate distance
        float dist = Vector3.Distance(CameraController.Instance.transform.position, lightningWorldPos);
        float dist2D = Vector2.Distance(new Vector2(CameraController.Instance.transform.position.x, CameraController.Instance.transform.position.z), new Vector2(lightningWorldPos.x, lightningWorldPos.z));

        List<AudioController.FMODParam> thunderParams = new List<AudioController.FMODParam>();

        AudioController.FMODParam thunderDistance = new AudioController.FMODParam { name = "ThunderDistance" };
        if (dist2D >= AudioDebugging.Instance.thunderDistanceThreshold) thunderDistance.value = 1;
        else thunderDistance.value = 0;

        thunderParams.Add(thunderDistance);

        AudioController.FMODParam intExt = new AudioController.FMODParam { name = "Interior" };
        if (Player.Instance.currentRoom != null && Player.Instance.currentRoom.IsOutside()) intExt.value = 0;
        else intExt.value = 1;

        thunderParams.Add(intExt);

        Game.Log("Audio: Thunder: Distance = " + dist2D + "/" + AudioDebugging.Instance.thunderDistanceThreshold + ": " + thunderDistance.value + " Interior: " + intExt.value);

        //Calculate speed of sound delay
        float soundDelay = dist / AudioController.Instance.speedOfSound;

        AudioController.Instance.PlayOneShotDelayed(soundDelay, AudioControls.Instance.thunder, null, null, lightningWorldPos, thunderParams);
    }

    //Set world scene visuals: Seperate so we can easily set them for scene captures
    public void SetSceneVisuals(float newDecimalClock)
    {
        float visualsDayProgress = newDecimalClock / 24f;

        //Set skybox colours
        if (skyboxGradientIndex + 1 < CityControls.Instance.weatherSettings.skyboxGradientGrading.Count) //Before midnight
        {
            if (newDecimalClock >= CityControls.Instance.weatherSettings.skyboxGradientGrading[skyboxGradientIndex + 1].time)
            {
                skyboxGradientIndex++; //Move to next gradient
                if (skyboxGradientIndex >= CityControls.Instance.weatherSettings.skyboxGradientGrading.Count) skyboxGradientIndex = 0;

                fromSkyboxColours = CityControls.Instance.weatherSettings.skyboxGradientGrading[skyboxGradientIndex];

                if (skyboxGradientIndex + 1 >= CityControls.Instance.weatherSettings.skyboxGradientGrading.Count) toSkyboxColours = CityControls.Instance.weatherSettings.skyboxGradientGrading[0];
                else toSkyboxColours = CityControls.Instance.weatherSettings.skyboxGradientGrading[skyboxGradientIndex + 1];
            }
        }
        else
        {
            if (newDecimalClock < fromSkyboxColours.time && newDecimalClock >= CityControls.Instance.weatherSettings.skyboxGradientGrading[0].time)
            {
                skyboxGradientIndex = 0;
                fromSkyboxColours = CityControls.Instance.weatherSettings.skyboxGradientGrading[skyboxGradientIndex];
                toSkyboxColours = CityControls.Instance.weatherSettings.skyboxGradientGrading[skyboxGradientIndex + 1];
            }
        }

        //Get lerp value
        float timeRange = toSkyboxColours.time - fromSkyboxColours.time;
        if (toSkyboxColours.time < fromSkyboxColours.time) timeRange = (toSkyboxColours.time + 24f) - fromSkyboxColours.time;

        float lerpVal = (newDecimalClock - fromSkyboxColours.time) / timeRange;

        //Set the fog distance

        //Set ambient light levels using the HDRP gradient sky
        gradientSky.top.Override(Color.Lerp(fromSkyboxColours.ambientLightTop * CityControls.Instance.weatherSettings.ambientLightMultiplier, toSkyboxColours.ambientLightTop * CityControls.Instance.weatherSettings.ambientLightMultiplier, lerpVal));
        gradientSky.middle.Override(Color.Lerp(fromSkyboxColours.ambientLightMiddle * CityControls.Instance.weatherSettings.ambientLightMultiplier, toSkyboxColours.ambientLightMiddle * CityControls.Instance.weatherSettings.ambientLightMultiplier, lerpVal));
        gradientSky.bottom.Override(Color.Lerp(fromSkyboxColours.ambientLightBottom * CityControls.Instance.weatherSettings.ambientLightMultiplier, toSkyboxColours.ambientLightBottom * CityControls.Instance.weatherSettings.ambientLightMultiplier, lerpVal));

        //Set fog (HDRP Volumetric)
        float val = Mathf.Lerp(CityControls.Instance.weatherSettings.fogDistanceRange.x, CityControls.Instance.weatherSettings.fogDistanceRange.y, CityControls.Instance.weatherSettings.fogDistanceCurve.Evaluate(visualsDayProgress));
        volFog.meanFreePath.Override(val);

        val = Mathf.Lerp(CityControls.Instance.weatherSettings.maxFogDistanceRange.x, CityControls.Instance.weatherSettings.maxFogDistanceRange.y, CityControls.Instance.weatherSettings.maxFogDistanceCurve.Evaluate(visualsDayProgress));
        volFog.maxFogDistance.Override(val);

        //Albedo (global lighting tint)
        Color col = Color.Lerp(fromSkyboxColours.fogAlbedo * CityControls.Instance.weatherSettings.fogColourMultiplier, toSkyboxColours.fogAlbedo * CityControls.Instance.weatherSettings.fogColourMultiplier, lerpVal);
        volFog.albedo.Override(col);

        //Fog colour (sky colour setting doesn't seem to work? Manually set the sky colour
        volFog.color.Override(Color.Lerp(fromSkyboxColours.fogColour, toSkyboxColours.fogColour, lerpVal));

        //Sea emission colour
        CityControls.Instance.seaMaterial.SetColor("Color_1A9C35BA", Color.Lerp(fromSkyboxColours.seaEmission, toSkyboxColours.seaEmission, lerpVal));

        //Smoke emission colour
        CityControls.Instance.smokeMaterial.SetColor("_EmissiveColor", Color.Lerp(fromSkyboxColours.smokeEmission, toSkyboxColours.smokeEmission, lerpVal));

        //Camera background colour = equator colour
        CameraController.Instance.hdrpCam.backgroundColorHDR = Color.Lerp(fromSkyboxColours.skyColour * CityControls.Instance.weatherSettings.skyColourMultiplier, toSkyboxColours.skyColour * CityControls.Instance.weatherSettings.skyColourMultiplier, lerpVal);

        //Ambient light colours
        CityControls.Instance.exteriorAmbientHDRP.color = Color.Lerp(fromSkyboxColours.ambientLightingColour * CityControls.Instance.weatherSettings.ambientLightMultiplier, toSkyboxColours.ambientLightingColour * CityControls.Instance.weatherSettings.ambientLightMultiplier, lerpVal);
        CityControls.Instance.interiorAmbientHDRP.color = Color.Lerp(fromSkyboxColours.ambientLightingColour * CityControls.Instance.weatherSettings.ambientLightMultiplier, toSkyboxColours.ambientLightingColour * CityControls.Instance.weatherSettings.ambientLightMultiplier, lerpVal);
        CityControls.Instance.exteriorAmbientHDRP.intensity = CityControls.Instance.weatherSettings.exteriorAmbientIntensityCurve.Evaluate(visualsDayProgress) * CityControls.Instance.weatherSettings.ambientExteriorBooster;
        CityControls.Instance.interiorAmbientHDRP.intensity = CityControls.Instance.weatherSettings.interiorAmbientIntensityCurve.Evaluate(visualsDayProgress) * CityControls.Instance.weatherSettings.ambientInteriorBooster;

        CityControls.Instance.hdrpLightSunData.intensity = CityControls.Instance.weatherSettings.daytimeSunIntensityCurve.Evaluate(visualsDayProgress) * CityControls.Instance.weatherSettings.sunIntensityBooster;
        CityControls.Instance.hdrpLightSunData.volumetricDimmer = CityControls.Instance.weatherSettings.sunVolumetricDimmer.Evaluate(visualsDayProgress);
        CityControls.Instance.hdrpLightSunData.volumetricShadowDimmer = CityControls.Instance.weatherSettings.sunVolumetricShadowDimmer.Evaluate(visualsDayProgress);
        CityControls.Instance.sunLight.shadowStrength = Mathf.Clamp01(CityControls.Instance.weatherSettings.sunShadowStrengthCurve.Evaluate(visualsDayProgress));

        //Move ships
        if (CityControls.Instance.ships1 != null)
        {
            CityControls.Instance.ships1.eulerAngles = new Vector3(0, CityControls.Instance.ships1.eulerAngles.y + ((Time.deltaTime * currentTimeMultiplier) * 0.1f), 0);
        }

        //Set skyline emission strength
        if (CityControls.Instance.skylineMaterial != null)
        {
            CityControls.Instance.skylineMaterial.SetColor("_EmissiveColor", CityControls.Instance.weatherSettings.skylineEmissionColor * CityControls.Instance.weatherSettings.skylineEmissionCurve.Evaluate(visualsDayProgress));
        }

        //Move the sun
        float sunAngle = 0f;

        //Sun rise is 90 degrees or 0.25 ratio
        if (newDecimalClock >= CityControls.Instance.weatherSettings.sunRiseHour && newDecimalClock < CityControls.Instance.weatherSettings.sunSetHour)
        {
            float sunriseProgress = (newDecimalClock - CityControls.Instance.weatherSettings.sunRiseHour) / (CityControls.Instance.weatherSettings.sunSetHour - CityControls.Instance.weatherSettings.sunRiseHour);
            sunAngle = Mathf.Lerp(90f, 270f, sunriseProgress);

            if (sunriseProgress <= 0.5f)
            {
                CityControls.Instance.sunLight.color = Color.Lerp(CityControls.Instance.weatherSettings.morningSunColour, CityControls.Instance.weatherSettings.middaySunColour, sunriseProgress * 2f);
            }
            else
            {
                CityControls.Instance.sunLight.color = Color.Lerp(CityControls.Instance.weatherSettings.middaySunColour, CityControls.Instance.weatherSettings.eveningSunColour, sunriseProgress * 0.5f);
            }

            //Update sun shadows
            sunShadowFrameCounter++;

            if (sunShadowFrameCounter >= Game.Instance.sunShadowUpdateFrequency)
            {
                CityControls.Instance.hdrpLightSunData.RequestShadowMapRendering();
                sunShadowFrameCounter = 0;
            }
        }
        //Sun set is 270 degrees or 0.75 ratio
        else if (newDecimalClock < CityControls.Instance.weatherSettings.sunRiseHour)
        {
            float sunsetProgress = newDecimalClock / CityControls.Instance.weatherSettings.sunRiseHour;
            sunAngle = Mathf.Lerp(0f, 90f, sunsetProgress);
        }
        else if (newDecimalClock >= CityControls.Instance.weatherSettings.sunSetHour)
        {
            sunAngle = Mathf.Lerp(270f, 360f, visualsDayProgress);
        }

        sunAngle -= 90f; //The angle the sun should be -90 (because mignight)
        CityControls.Instance.sunLight.transform.eulerAngles = new Vector3(sunAngle, CityControls.Instance.angleOfSun, 0);
    }

    public void SetEnablePause(bool val)
    {
        enableUserPause = val;

        if(!enableUserPause && !play)
        {
            ResumeGame();
        }
    }

    //Parse time data from gameTime float (complete)
    public void ParseTimeData(float newTime, out float decimalHourOut, out int dayIntOut, out int dateIntOut, out int monthIntOut, out int yearIntOut, out WeekDay dayEnumOut, out Month monthEnumOut, out int leapCycleOut)
    {
        decimalHourOut = newTime;
        dayIntOut = GameplayControls.Instance.dayZero;
        dateIntOut = 0;
        monthIntOut = 0;
        yearIntOut = GameplayControls.Instance.publicYearZero + GameplayControls.Instance.startingYear;
        dayEnumOut = WeekDay.monday;
        monthEnumOut = Month.jan;
        leapCycleOut = GameplayControls.Instance.yearZeroLeapYearCycle;

        //Hours counting down- everything else adds up
        while (decimalHourOut >= 24f)
        {
            decimalHourOut -= 24f;
            dayIntOut++;
            dateIntOut++;

            if (dayIntOut >= 7)
            {
                dayIntOut -= 7;
            }

            dayEnumOut = WeekdayFromInt(dayIntOut);

            //Calc days in month including leap year
            int daysInMonth = daysInMonths[monthIntOut];
            if (leapCycleOut == 3 && monthIntOut == 1) daysInMonth += 1;

            if (dateIntOut >= daysInMonth)
            {
                dateIntOut -= daysInMonth;
                monthIntOut++;

                if (monthIntOut >= 12)
                {
                    monthIntOut -= 12;
                    yearIntOut++;
                    leapCycleOut++;

                    while (leapCycleOut >= 4)
                    {
                        leapCycleOut -= 4;
                    }
                }

                monthEnumOut = MonthFromInt(monthInt);
            }
        }
    }

    //Parse time data from gameTime float (numbers only)
    public void ParseTimeData(float newTime, out float decimalHourOut, out int dayIntOut, out int dateIntOut, out int monthIntOut, out int yearIntOut)
    {
        decimalHourOut = newTime;
        dayIntOut = 0;
        dateIntOut = 0;
        monthIntOut = 0;
        yearIntOut = GameplayControls.Instance.publicYearZero + GameplayControls.Instance.startingYear;
        WeekDay dayEnumOut = WeekDay.monday;
        Month monthEnumOut = Month.jan;
        int leapCycleOut = GameplayControls.Instance.yearZeroLeapYearCycle;

        //Call main function
        ParseTimeData(newTime, out decimalHourOut, out dayIntOut, out dateIntOut, out monthIntOut, out yearIntOut, out dayEnumOut, out monthEnumOut, out leapCycleOut);
    }

    //Parse time data from gameTime float (enums)
    public void ParseTimeData(float newTime, out float decimalHourOut, out WeekDay dayEnumOut, out int dateIntOut, out Month monthEnumOut, out int yearIntOut)
    {
        decimalHourOut = newTime;
        int dayIntOut = 0;
        dateIntOut = 0;
        int monthIntOut = 0;
        yearIntOut = GameplayControls.Instance.publicYearZero + GameplayControls.Instance.startingYear;
        dayEnumOut = WeekDay.monday;
        monthEnumOut = Month.jan;
        int leapCycleOut = GameplayControls.Instance.yearZeroLeapYearCycle;

        //Call main function
        ParseTimeData(newTime, out decimalHourOut, out dayIntOut, out dateIntOut, out monthIntOut, out yearIntOut, out dayEnumOut, out monthEnumOut, out leapCycleOut);
    }

    //Parse gameTime from time data
    public float ParseGameTime(float decimalHourIn, int dateIntIn, int monthIntIn, int yearIntIn, out int dayCount, out int leapYear)
    {
        //+ Hours
        float ret = decimalHourIn;
        int dateCount = 0;
        int monthCount = 0;
        int yearCount = GameplayControls.Instance.publicYearZero + GameplayControls.Instance.startingYear;
        dayCount = GameplayControls.Instance.dayZero;
        leapYear = GameplayControls.Instance.yearZeroLeapYearCycle;

        //Add years
        while (yearCount < yearIntIn)
        {
            int monthCycle = 0;

            while (monthCycle < 12)
            {
                //Calc days in month including leap year
                int daysInMonth = daysInMonths[monthCycle];
                if (leapYear == 3 && monthCycle == 1) daysInMonth += 1;

                while (dateCount < daysInMonth)
                {
                    dateCount++;
                    ret += 24f;

                    dayCount++;
                    if (dayCount >= 7)
                    {
                        dayCount -= 7;
                    }
                }

                monthCycle++;
                dateCount = 0;
            }

            yearCount++;
            leapYear++;
            if (leapYear >= 4) leapYear -= 4;
        }

        //Add months
        while (monthCount < monthIntIn)
        {
            int monthCycle = 0;

            //Calc days in month including leap year
            int daysInMonth = daysInMonths[monthCycle];
            if (leapYear == 3 && monthCycle == 1) daysInMonth += 1;

            while (dateCount < daysInMonth)
            {
                dateCount++;
                ret += 24f;

                dayCount++;
                if (dayCount >= 7)
                {
                    dayCount -= 7;
                }
            }

            monthCycle++;
            dateCount = 0;
        }

        //Add days
        while (dateCount < dateIntIn)
        {
            ret += 24f;
            dateCount++;

            dayCount++;
            if (dayCount >= 7)
            {
                dayCount -= 7;
            }
        }

        return ret;
    }

    //Format to 24 decimal clock
    public float FloatDecimal24H(float time)
    {
        while (time < 0f)
        {
            time += 24f;
        }
        time = time % 24f;
        return Mathf.Clamp(time, 0f, 24f);
    }

    //Format to 24H and proper minutes
    public float FloatMinutes24H(float newTime)
    {
        newTime = FloatDecimal24H(newTime);

        float hour = Mathf.Floor(newTime);

        //Return minutes only
        float minutes = Mathf.Floor((newTime - hour) * 60f) / 100f;

        return hour + minutes;
    }

    //Format to 12H and proper minutes
    public float FloatMinutes12H(float newTime)
    {
        newTime = FloatDecimal24H(newTime);

        float hour = Mathf.Floor(newTime);
        if (hour > 12) hour = hour - 12;

        //Return minutes only
        float minutes = Mathf.Floor((newTime - Mathf.Floor(newTime)) * 60f) / 100f;

        return hour + minutes;
    }

    //Format decimal clock to time string eg. 14:01
    public string DecimalToClockString(float newTime, bool useZeroHoursMethod)
    {
        float formatted = FloatMinutes24H(newTime);

        return MinutesToClockString(formatted, useZeroHoursMethod);
    }

    //Format decimal to a time length string eg. 5 Hours, 15 Minutes
    public string DecimalToTimeLengthString(float newTime)
    {
        float formatted = FloatMinutes24H(newTime);

        //Get hours only
        int hours = Mathf.FloorToInt(formatted);
        //string output = hours.ToString() + " " + Strings.Get("ui.interface", "hours");

        //Add the minutes
        int minutesInt = Mathf.CeilToInt((formatted - (float)hours) * 100f);
        string minutes = minutesInt.ToString();
        //minutes += " " + Strings.Get("ui.interface", "minutes");

        //if (minutesInt == 0) return output;
        //else if (hours <= 0) return minutes;
        return hours + ":" + minutes;
    }

    //Format gametime to time string eg. 14:01
    public string GameTimeToClock24String(float newGameTime, bool useZeroHoursMethod)
    {
        float decimalHourOut = 0f;
        int dayIntOut = 0;
        int dateIntOut = 0;
        int monthIntOut = 0;
        int yearIntOut = 0;

        ParseTimeData(newGameTime, out decimalHourOut, out dayIntOut, out dateIntOut, out monthIntOut, out yearIntOut);

        float formatted = FloatMinutes24H(decimalHourOut);

        return MinutesToClockString(formatted, useZeroHoursMethod);
    }

    //Format gametime to time string eg. 2:01pm
    public string GameTimeToClock12String(float newGameTime, bool useZeroHoursMethod)
    {
        float decimalHourOut = 0f;
        int dayIntOut = 0;
        int dateIntOut = 0;
        int monthIntOut = 0;
        int yearIntOut = 0;

        ParseTimeData(newGameTime, out decimalHourOut, out dayIntOut, out dateIntOut, out monthIntOut, out yearIntOut);

        float formatted = FloatMinutes12H(decimalHourOut);

        string am = Strings.Get("ui.interface", "AM");
        if (decimalHourOut > 12) am = Strings.Get("ui.interface", "PM");

        return MinutesToClockString(formatted, useZeroHoursMethod) + am;
    }

    //Format minutes clock to time string eg. 14:01
    public string MinutesToClockString(float formatted, bool useZeroHoursMethod)
    {
        //Get hours only
        int hours = Mathf.FloorToInt(formatted);
        string output = hours.ToString();

        //If single digit hours, shove another 0 infront!
        if (useZeroHoursMethod && output.Length == 1) output = "0" + output;

        //Add the minutes
        string minutes = Mathf.RoundToInt((formatted - (float)hours) * 100f).ToString();

        if (minutes.Length == 1) minutes = "0" + minutes;

        return output + ":" + minutes;
    }

    //Get current time as string
    public string CurrentTimeString(bool useZeroHoursMethod, bool use12HourClock = false)
    {
        if (!use12HourClock)
        {
            return MinutesToClockString(formattedClock, useZeroHoursMethod);
        }
        else
        {
            return GameTimeToClock12String(gameTime, useZeroHoursMethod);
        }
    }

    //Get short date (American) month/date/year
    public string ShortDateString(float newGameTime, bool shortenYear)
    {
        float decimalHourOut = 0f;
        int dayIntOut = 0;
        int dateIntOut = 0;
        int monthIntOut = 0;
        int yearIntOut = 0;

        ParseTimeData(newGameTime, out decimalHourOut, out dayIntOut, out dateIntOut, out monthIntOut, out yearIntOut);

        string monthStr = (monthIntOut + 1).ToString();
        if (monthStr.Length == 1) monthStr = "0" + monthStr;

        string dateStr = (dateIntOut + 1).ToString();
        if (dateStr.Length == 1) dateStr = "0" + dateStr;

        //Display public year, not the internal
        //yearIntOut += GameplayControls.Instance.publicYearZero;
        string yearStr = yearIntOut.ToString();

        if (shortenYear)
        {
            if (yearStr.Length > 2) yearStr = yearStr.Substring(yearStr.Length - 2);
        }

        return monthStr + "/" + dateStr + "/" + yearStr;
    }

    //Get current short date as string
    public string CurrentShortDateString(bool shortenYear)
    {
        return ShortDateString(gameTime, shortenYear);
    }

    //Get long date eg. Tuesday November 10th 1997 - fully flexible
    public string LongDateString(float newGameTime, bool includeDay, bool shortenDay, bool includeMonth, bool shortenMonth, bool includeDate, bool includeYear, bool shortenYear, bool useCommas)
    {
        float decimalHourOut = 0f;
        int dayIntOut = 0;
        int dateIntOut = 0;
        int monthIntOut = 0;
        int yearIntOut = 0;

        ParseTimeData(newGameTime, out decimalHourOut, out dayIntOut, out dateIntOut, out monthIntOut, out yearIntOut);

        string dayStr = string.Empty;
        string monthStr = string.Empty;
        string dateStr = string.Empty;
        string yearStr = string.Empty;

        if (includeDay)
        {
            dayStr = Strings.Get("ui.interface", WeekdayFromInt(dayIntOut).ToString());
            if (shortenDay && Game.Instance.language == "English") dayStr = dayStr.Substring(0, Mathf.Min(3, dayStr.Length));

            if (includeMonth || includeDate || includeYear)
            {
                if (useCommas) dayStr += ",";
                dayStr += " ";
            }
        }

        if (includeMonth)
        {
            monthStr = Strings.Get("ui.interface", MonthFromInt(monthIntOut).ToString());
            if (shortenMonth && Game.Instance.language == "English") monthStr = monthStr.Substring(0, Mathf.Min(3, monthStr.Length));
            monthStr += " ";
        }

        if (includeDate)
        {
            dateStr = Toolbox.Instance.GetNumbericalStringReference(dateIntOut + 1);

            if (includeYear)
            {
                if (useCommas) dateStr += ",";
                dateStr += " ";
            }
        }

        if (includeYear)
        {
            //Display public year, not the internal
            yearStr = yearIntOut.ToString();

            if (shortenYear)
            {
                if (yearStr.Length > 2) yearStr = yearStr.Substring(yearStr.Length - 2);
            }
        }

        string ret = dayStr + monthStr + dateStr + yearStr;

        //Remove space/comma at end if there is one
        if (ret.Length > 1 && ret.Substring(ret.Length - 1, 1) == " ") ret = ret.Substring(0, ret.Length - 1);
        if (useCommas && ret.Length > 1 && ret.Substring(ret.Length - 1, 1) == ",") ret = ret.Substring(0, ret.Length - 1);

        return ret;
    }

    //Get current long date as string
    public string CurrentLongDateString(bool includeDay, bool shortenDay, bool includeMonth, bool shortenMonth, bool includeDate, bool includeYear, bool shortenYear, bool useCommas)
    {
        return LongDateString(gameTime, includeDay, shortenDay, includeMonth, shortenMonth, includeDate, includeYear, shortenYear, useCommas);
    }

    //Get time as 'Hours:Minutes' 
    public string TimeString(float newGameTime, bool useZeroHoursMethod)
    {
        float decimalHourOut = 0f;
        int dayIntOut = 0;
        int dateIntOut = 0;
        int monthIntOut = 0;
        int yearIntOut = 0;

        ParseTimeData(newGameTime, out decimalHourOut, out dayIntOut, out dateIntOut, out monthIntOut, out yearIntOut);

        string formattedHour = DecimalToClockString(decimalHourOut, useZeroHoursMethod);

        return formattedHour;
    }

    //Get time as 'Hours:Minutes day' 
    public string TimeStringOnDay(float newGameTime, bool useZeroHoursMethod, bool shortenDay)
    {
        float decimalHourOut = 0f;
        int dayIntOut = 0;
        int dateIntOut = 0;
        int monthIntOut = 0;
        int yearIntOut = 0;

        ParseTimeData(newGameTime, out decimalHourOut, out dayIntOut, out dateIntOut, out monthIntOut, out yearIntOut);

        string formattedHour = DecimalToClockString(decimalHourOut, useZeroHoursMethod);
        string dayStr = Strings.Get("ui.interface", WeekdayFromInt(dayIntOut).ToString());
        if (shortenDay && Game.Instance.language == "English" && dayStr.Length > 3) dayStr = dayStr.Substring(0, 3);

        return formattedHour + " " + dayStr;
    }

    //Get time as 'Hours:Minutes day month/date/year
    public string TimeAndDate(float newGameTime, bool useZeroHoursMethod, bool shortenDay, bool shortenYear)
    {
        return TimeStringOnDay(newGameTime, useZeroHoursMethod, shortenDay) + " " + ShortDateString(newGameTime, shortenYear);
    }

    //Simply get "on Day" to append to string manually 
    public string OnDay(int newDay, bool shortenDay)
    {
        WeekDay dayEnumOut = WeekdayFromInt(newDay);
        string dayStr = Strings.Get("ui.interface", dayEnumOut.ToString());
        if (shortenDay && Game.Instance.language == "English" && dayStr.Length > 3) dayStr = dayStr.Substring(0, 3);

        return Strings.Get("ui.interface", "on") + " " + dayStr;
    }

    //Return the gameTime of when this is triggered
    public float GetNextOrPreviousGameTimeForThisHour(List<SessionData.WeekDay> days, float startHour, float endHour)
    {
        return GetNextOrPreviousGameTimeForThisHour(gameTime, decimalClock, day, days, startHour, endHour);
    }

    public float GetNextOrPreviousGameTimeForThisHour(float newTime, float newDecimalClock, WeekDay newDay, List<SessionData.WeekDay> days, float startHour, float endHour)
    {
        //Find midnight for today: Take away decimal hour from gametime
        float midnight = newTime - newDecimalClock;

        //Game.Log("Midnight = " + newTime + " - " + newDecimalClock + " = " + midnight);

        //The end hour is past the start hour, ie this range isn't split over multiple days
        if (endHour >= startHour)
        {
            //Game.Log("The end hour " + endHour + " is past the start hour " + startHour);

            if (days.Contains(newDay))
            {
                //We're in the time frame now, return when it last started...
                if (newDecimalClock >= startHour && newDecimalClock <= endHour)
                {
                    //Game.Log("We're in the timeframe now, return when it last started...");

                    float startedXHoursAgo = newDecimalClock - startHour;
                    return newTime - startedXHoursAgo;
                }
                //The start time is ahead, so return the next start time
                else if(newDecimalClock < startHour)
                {
                    //Game.Log("The start time " + startHour + " is ahead of " + newDecimalClock + ", so return the next start time");

                    //Add the decimal hour to midnight to get next open time
                    return midnight + startHour;
                }
            }
        }
        //The last start was yesterday
        else if (endHour < startHour)
        {
            //Game.Log("The last start was yesterday");

            //It's past the end, so use the next opening time today
            if (newDecimalClock >= endHour)
            {
                //Game.Log("It's past the end, so use the next opening time today");

                return midnight + startHour;
            }
            //This started yesterday
            else
            {
                //Game.Log("This started yesterday");

                WeekDay yesterday = newDay;

                if (day == WeekDay.monday)
                {
                    yesterday = WeekDay.sunday;
                }
                else
                {
                    yesterday = (WeekDay)(int)day - 1;
                }

                if (days.Contains(yesterday))
                {
                    return midnight - (24f - startHour);
                }
            }
        }

        //The next is in the future...
        //Game.Log("The next start is in the future...");

        int nextDay = (int)newDay + 1;
        if (nextDay >= 7) nextDay = 0;

        WeekDay nextStart = (WeekDay)nextDay;

        float midnightNextDay = midnight + 24f;
        int safety = 8;

        while (!days.Contains(nextStart) && safety > 0)
        {
            if (nextStart == WeekDay.sunday)
            {
                nextStart = WeekDay.monday;
            }
            else
            {
                nextStart = (WeekDay)(int)day + 1;
            }

            midnightNextDay += 24f;
            safety--;
        }

        //Game.Log("Found next start as " + nextStart.ToString() + " start hour " + startHour + " (" + (midnightNextDay + startHour) + "/" + SessionData.Instance.gameTime + ")");

        return midnightNextDay + startHour;
    }

    //Get amount of time difference between two gameTimes
    public float GetTimeDifference(float time1, float time2)
    {
        return time2 - time1;
    }

    //Compare times (minute accuracy)
    public bool CompareTimes(float time1, float time2)
    {
        float rounded1 = Toolbox.Instance.RoundToPlaces(time1, 2);
        float rounded2 = Toolbox.Instance.RoundToPlaces(time2, 2);

        if (rounded1 == rounded2) return true;
        return false;
    }

    //Get weekday enum from int
    public WeekDay WeekdayFromInt(int weekInt)
    {
        return (WeekDay)weekInt;
    }

    //Get month enum from int
    public Month MonthFromInt(int monthInt)
    {
        return (Month)monthInt;
    }

    //Set the weather
    public void SetWeather(float newRain, float newWind, float newSnow, float newLightning, float newTransitionSpeed = 0.1f)
    {
        if(Game.Instance.disableSnow)
        {
            desiredSnow = 0;
        }
        else desiredSnow = Mathf.Clamp01(newSnow);

        if (desiredSnow <= 0.01f)
        {
            desiredRain = Mathf.Clamp01(newRain);
        }
        else desiredRain = 0f;

        desiredWind = Mathf.Clamp01(newWind);
        desiredLightning = Mathf.Clamp01(newLightning);
        transitionSpeed = newTransitionSpeed;

        Game.Log("Gameplay: Set weather: Rain: " + desiredRain + " Wind: " + desiredWind + " Lightning: " + desiredLightning + " Snow: " + desiredSnow);
    }

    //Update UI clock
    public void UpdateUIClock()
    {
        //Update 3D watch text if it exists
        if (newWatchTimeText != null)
        {
            //If setting: Only update the time if the actual time is ahead of the alarm time
            if (Player.Instance.setAlarmMode)
            {
                //Alarm can't be in the past
                if (gameTime > Player.Instance.alarm)
                {
                    Player.Instance.alarm = gameTime;
                }

                //Set text to alarm time
                newWatchTimeText.text = DecimalToClockString(FloatMinutes24H(Player.Instance.alarm), true);
            }
            else
            {
                newWatchTimeText.text = DecimalToClockString(decimalClock, true);
            }
        }
    }

    //Update UI Day
    public void UpdateUIDay()
    {
        string ret = string.Empty;

        //Set text
        if (newWatchDateText != null)
        {
            float decimalHourOut = 0f;
            int dayIntOut = 0;
            int dateIntOut = 0;
            int monthIntOut = 0;
            int yearIntOut = 0;

            if (Player.Instance.setAlarmMode)
            {
                //Alarm can't be in the past
                if (gameTime > Player.Instance.alarm)
                {
                    Player.Instance.alarm = gameTime;
                }

                //Set text to alarm time
                ParseTimeData(Player.Instance.alarm, out decimalHourOut, out dayIntOut, out dateIntOut, out monthIntOut, out yearIntOut);
            }
            else
            {
                ParseTimeData(gameTime, out decimalHourOut, out dayIntOut, out dateIntOut, out monthIntOut, out yearIntOut);
            }

            string dayStr = string.Empty;
            string monthStr = string.Empty;
            string dateStr = string.Empty;
            string yearStr = string.Empty;

            dayStr = Strings.Get("ui.interface", WeekdayFromInt(dayIntOut).ToString());
            //if (shortenDay) dayStr.Substring(Mathf.Max(0, dayStr.Length - 3));
            dayStr += "\n";

            monthStr = Strings.Get("ui.interface", MonthFromInt(monthIntOut).ToString() + "_short");
            monthStr.Substring(Mathf.Max(0, monthStr.Length - 3));
            monthStr += " ";

            dateStr = (dateIntOut + 1).ToString();

            if (dateStr.Length > 0 && dateStr.Substring(dateStr.Length - 1, 1) == "1") dateStr += Strings.Get("ui.interface", "st");
            else if (dateStr.Length > 0 && dateStr.Substring(dateStr.Length - 1, 1) == "2") dateStr += Strings.Get("ui.interface", "nd");
            else if (dateStr.Length > 0 && dateStr.Substring(dateStr.Length - 1, 1) == "3") dateStr += Strings.Get("ui.interface", "rd");
            else dateStr += Strings.Get("ui.interface", "th");

            dateStr += " ";

            //Display public year, not the internal
            //yearIntOut += GameSettings.Instance.publicYearZero;
            //yearStr = yearIntOut.ToString();

            ret = dayStr + monthStr + dateStr;// + yearStr;

            //Remove space at end if there is one
            if (dateStr.Length > 1 && ret.Substring(dateStr.Length - 1, 1) == " ") ret = ret.Substring(0, dateStr.Length - 1);

            newWatchDateText.text = ret;
        }

        //dayText.text = ret;
    }

    public void TogglePause(bool openDesktopMode = true)
    {
        if (play)
        {
            PauseGame(true, openDesktopMode: openDesktopMode);
        }
        else
        {
            ResumeGame();
        }
    }

    //These pause commands override everything and set the play bool directly.
    public void PauseGame(bool showPauseText, bool delayOverride = false, bool openDesktopMode = true)
    {
        if (play)
        {
            autoPauseTimer = 0f; //Reset auto pause
            if (!delayOverride && pauseUnpauseDelay > 0) return;
            pauseUnpauseDelay = 1;

            if (InterfaceController.Instance.timeText != null)
            {
                InterfaceController.Instance.timeText.text = SessionData.Instance.TimeAndDate(SessionData.Instance.gameTime, true, true, true);
            }

            Game.Log("Player: Pause Game");

            if(pauseText != null) pauseText.text = Strings.Get("ui.interface", "Paused");

            if (InterfaceControls.Instance.fastForwardArrow != null) InterfaceControls.Instance.fastForwardArrow.gameObject.SetActive(false);

            //Pause game clock from advancing
            play = false;

            //Update depth of field
            InterfaceController.Instance.UpdateDOF();

            //Update behaviour routines(stop)
            if(CitizenBehaviour.Instance != null) CitizenBehaviour.Instance.GameSpeedChange();

            //Display pause text
            if(pauseText != null)
            {
                if (showPauseText)
                {
                    pauseText.enabled = true;
                    pauseLensFlare.gameObject.SetActive(true);
                }
                else
                {
                    pauseText.enabled = false;
                    pauseLensFlare.gameObject.SetActive(false);
                }
            }

            //Fire event
            if (OnPauseChange != null) OnPauseChange(openDesktopMode);

            InterfaceController.Instance.SetCrosshairVisible(false);

            //Display location text
            InterfaceController.Instance.DisplayLocationText(4f, true);
        }

        if(interfaceActiveAudio == null)
        {
            interfaceActiveAudio = AudioController.Instance.Play2DLooping(AudioControls.Instance.interfaceEvent);
        }
    }

    public void ResumeGame()
    {
        if (!play)
        {
            InteractionController.Instance.inputCooldown = 0.1f; //Cooldown stops other actions from being triggered from caseboard button

            if (Game.Instance.disableCaseBoardClose) return;

            if (pauseUnpauseDelay > 0) return;
            pauseUnpauseDelay = 1;

            if(isDecorEdit)
            {
                isDecorEdit = false; //Disables decor editing
                InterfaceController.Instance.UpdateDOF();
            }

            //Close inventory if triggered during pause
            if(BioScreenController.Instance.isOpen && BioScreenController.Instance.openedFromPause)
            {
                BioScreenController.Instance.SetInventoryOpen(false, false);
            }

            Game.Log("Player: Resume Game");

            play = true;

            if(UpgradesController.Instance.playSyncDiskInstallAudio)
            {
                AudioController.Instance.Play2DSound(AudioControls.Instance.syncDiskInstallStatus);
                UpgradesController.Instance.playSyncDiskInstallAudio = false;
            }

            InterfaceController.Instance.SetCrosshairVisible(true);

            //Update behaviour routines(start)
            if(CitizenBehaviour.Instance != null) CitizenBehaviour.Instance.GameSpeedChange();

            //Update depth of field
            InterfaceController.Instance.UpdateDOF();

            if(!SessionData.Instance.isFloorEdit)
            {
                pauseText.enabled = false;
                pauseLensFlare.SetActive(false);
            }

            //Cancel all world interactions
            for (int i = 0; i < InterfaceController.Instance.activeWindows.Count; i++)
            {
                InfoWindow win = InterfaceController.Instance.activeWindows[i];

                if(win.isWorldInteraction)
                {
                    win.SetWorldInteraction(false);

                    if(Game.Instance.closeInteractionsOnResume)
                    {
                        win.CloseWindow(false);
                        i--;
                    }
                }
            }

            //Fire event
            if (OnPauseChange != null) OnPauseChange(true);

            //Resume animation
            //for (int i = 0; i < CityData.Instance.citizenDirectory.Count; i++)
            //{
            //    CityData.Instance.citizenDirectory[i].animationController.SetPauseAnimation(false);
            //    CityData.Instance.citizenDirectory[i].animationController.ForceUpdateAnimationSate();
            //}

            //Resume looping sounds
            //foreach (AudioController.LoopingSoundInfo loop in AudioController.Instance.loopingSounds)
            //{
            //    if (loop.pauseWhenGamePaused)
            //    {
            //        loop.audioEvent.setPaused(false);
            //    }
            //}

            if (InterfaceControls.Instance.fastForwardArrow != null)
            {
                if (Player.Instance.spendingTimeMode)
                {
                    InterfaceControls.Instance.fastForwardArrow.gameObject.SetActive(true);
                }
                else InterfaceControls.Instance.fastForwardArrow.gameObject.SetActive(false);
            }
        }

        if(interfaceActiveAudio != null)
        {
            AudioController.Instance.StopSound(interfaceActiveAudio, AudioController.StopType.triggerCue, "Game resumed");
            interfaceActiveAudio = null;
        }
    }

    public void SetDisplayTutorialText(bool val)
    {
        if (enableTutorialText != val)
        {
            enableTutorialText = val;
            tutorialTextTriggered.Clear();
            //tutorialTextRead.Clear();
        }

        if (!enableTutorialText)
        {
            InterfaceController.Instance.notebookButton.notifications.SetNotifications(0);
        }

        //Write to options
        PlayerPrefsController.GameSetting findSetting = PlayerPrefsController.Instance.gameSettingControls.Find(item => item.identifier.ToLower() == "popuptips"); //Case insensitive find

        //Convert ui control values...
        if (findSetting.toggle != null)
        {
            findSetting.intValue = Convert.ToInt32(enableTutorialText);
            PlayerPrefs.SetInt(findSetting.identifier, findSetting.intValue); //Save to player prefs
        }
    }

    public void TutorialTrigger(string str, bool isSilent = false)
    {
        if (!enableTutorialText) return;
        if (!SessionData.Instance.startedGame) return;
        if (CityConstructor.Instance != null && CityConstructor.Instance.preSimActive) return;
        if (CityConstructor.Instance != null && CityConstructor.Instance.loadingOperationActive) return;

        str = str.ToLower();

        //if (tutorialTextRead.Contains(str)) return;

        if (!tutorialTextTriggered.Contains(str))
        {
            tutorialTextTriggered.Add(str);

            //Notification
            if (!isSilent)
            {
                InterfaceController.Instance.NewHelpPointer(str);
            }
        }

        UpdateTutorialNotifications();
    }

    public void UpdateTutorialNotifications()
    {
        int notificationCount = 0;

        if(enableTutorialText)
        {
            //foreach (string str in tutorialTextTriggered)
            //{
            //    if (!tutorialTextRead.Contains(str))
            //    {
            //        notificationCount++;
            //    }
            //}
        }

        InterfaceController.Instance.notebookButton.notifications.SetNotifications(notificationCount);

        //Fire event
        if(OnTutorialNotificationChange != null)
        {
            OnTutorialNotificationChange();
        }
    }

    public void ExecuteUnloadPipes()
    {
        foreach (PipeConstructor.PipeGroup pg in pipesToUnload)
        {
            bool hide = true;

            foreach (int i in pg.rooms)
            {
                NewRoom r = null;

                if (CityData.Instance.roomDictionary.TryGetValue(i, out r))
                {
                    if(r.isVisible)
                    {
                        hide = false;
                        break;
                    }
                }
            }

            pg.SetVisible(!hide);
        }

        pipesToUnload.Clear();
    }

    [Button]
    public void DebugPreviousOrLastTime()
    {
        float returnTime = GetNextOrPreviousGameTimeForThisHour(debugDayList, debugDecimalRange.x, debugDecimalRange.y);

        Debug.Log(TimeStringOnDay(returnTime, true, true));
    }
}
