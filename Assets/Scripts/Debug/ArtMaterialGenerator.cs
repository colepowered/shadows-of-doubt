using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using UnityEditor;
using System.IO;

public class ArtMaterialGenerator : MonoBehaviour
{
    [Header("Pointers")]
    public string textureSourceDirectory;
    public string materialOutputDirectory;
    public string presetOutputDirectory;
    public ArtPreset presetTemplate;
    public Material materialTemplate;

    [Button]
    public void GenerateMaterialsAndPresets()
    {
#if UNITY_EDITOR
        string root_path = Application.dataPath + "/" + textureSourceDirectory;
        Debug.Log(root_path);

        if (!Directory.Exists(root_path))
        {
            Debug.LogWarning("Path doesn't exist");
        }

        string[] fileEntries = Directory.GetFiles(root_path);
        List<Texture2D> loadedTextures = new List<Texture2D>();

        foreach (string FileName in fileEntries)
        {
            string[] filepath = FileName.Split(new string[] { Application.dataPath }, System.StringSplitOptions.None);
            //Debug.Log("Assets" + filepath[1]);

            Texture2D loadTex = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets" + filepath[1]);

            if (loadTex != null)
            {
                if (!loadTex.isReadable)
                {
                    SetTextureImporterFormat(loadTex, true);
                }

                loadedTextures.Add(loadTex);
            }
        }

        Debug.Log("Art material gen: Loaded " + loadedTextures.Count + " textures from " + fileEntries.Length);

        foreach(Texture2D tex in loadedTextures)
        {
            Material newMat = Instantiate(materialTemplate);
            newMat.SetTexture("_BaseColorMap", tex);

            //Save the material
            if (newMat != null)
            {
                string materialPath = materialOutputDirectory + tex.name + ".mat";

                Debug.Log("Saving new material at " + materialPath);

                AssetDatabase.CreateAsset(newMat, materialPath);
                AssetDatabase.Refresh();

                Material loadedMat = (Material)AssetDatabase.LoadAssetAtPath(materialPath, typeof(Material));

                if (loadedMat != null)
                {
                    ArtPreset newPreset = Instantiate(presetTemplate);
                    newPreset.material = loadedMat;
                    newPreset.texturePreview = tex;
                    newPreset.GenerateColourMatching();
                    newPreset.disable = false;

                    string presetPath = presetOutputDirectory + tex.name + ".asset";

                    Debug.Log("Saving new preset at " + presetPath);
                    AssetDatabase.CreateAsset(newPreset, presetPath);
                }
            }
        }

        AssetDatabase.Refresh();
#endif
    }

    public static void SetTextureImporterFormat(Texture2D texture, bool isReadable)
    {
#if UNITY_EDITOR
        if (null == texture) return;

        string assetPath = AssetDatabase.GetAssetPath(texture);
        var tImporter = AssetImporter.GetAtPath(assetPath) as TextureImporter;
        if (tImporter != null)
        {
            tImporter.isReadable = isReadable;

            Debug.Log("...Making " + texture.name + " readable: " + isReadable);

            AssetDatabase.ImportAsset(assetPath);
            AssetDatabase.Refresh();
        }

#endif
    }
}
