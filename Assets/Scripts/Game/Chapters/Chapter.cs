﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;
using System.Reflection;
using NaughtyAttributes;
using System.Linq.Expressions;

public class Chapter : MonoBehaviour
{
    public ChapterPreset preset;
    //private bool newChapterDelayActive = false; //Stops multiple chapters being skipped to once the delay is active.
    [System.NonSerialized]
    public Case thisCase = null;
    public bool loadedFromSave = false; //True if game is loaded from save state
    public bool gameStart = false;

    public float blackTimer = 0f;
    public float blurTimer = 0f;
    public float blackFade = 1f;
    public float blurFade = 1f;

    //Location of current chapter (Used for teleport/skipping).
    public NewNode currentPartLocation = null;

    private bool teleportPlayerToChapter = false;
    private bool chapterFrameDelay = false;

    //Invoke routines on delay (if unpaused)
    public Dictionary<string, float> invokeOnDelay = new Dictionary<string, float>();

    public void Awake()
    {
        if (CityConstructor.Instance.saveState == null)
        {
            loadedFromSave = false;
        }
        else
        {
            Game.Log("Chapter: Loading chapter save data...");
            LoadStateSaveData(CityConstructor.Instance.saveState.chapterSaveState);
            loadedFromSave = true;
        }

        OnLoaded();

        //If the game is already started, trigger now, otherwise listen
        if (SessionData.Instance.startedGame && (CityConstructor.Instance == null || !CityConstructor.Instance.preSimActive))
        {
            OnGameStart();
        }
        else
        {
            CityConstructor.Instance.OnLoadFinalize += OnLoadFinalize;
            CityConstructor.Instance.OnGameStarted += OnGameStart;
            ChapterController.Instance.OnNewPart += OnNewChapterPart; //Listen for part change
        }
    }

    //Triggered on load chapter
    public virtual void OnLoaded()
    {
        Game.Log("Chapter: OnLoad: " + ChapterController.Instance.loadedChapter);

        //Set the preset
        preset = ChapterController.Instance.loadedChapter;
        ChapterController.Instance.chapterScript = this;

        //Attempt to load in existing case
        thisCase = CasePanelController.Instance.activeCases.Find(item => item.caseType == Case.CaseType.mainStory && item.mainStoryChapter == preset.name);

        //Otherwise create new case...
        if(thisCase == null)
        {
            Game.Log("Chapter: Creating new case for chapter " + preset.name);
            thisCase = CasePanelController.Instance.CreateNewCase(Case.CaseType.mainStory, Case.CaseStatus.forced, true, Strings.Get("ui.chapters", preset.name));
            thisCase.mainStoryChapter = preset.name; //Set main story chapter so we know to load this above...
            MurderController.Instance.currentActiveCase = thisCase;
        }
        
        //Set the starting time
        if(!loadedFromSave)
        {
            //SessionData.Instance.SetGameTime(ChapterController.Instance.loadedChapter.startingYear, ChapterController.Instance.loadedChapter.startingMonth, ChapterController.Instance.loadedChapter.startingDate, ChapterController.Instance.loadedChapter.dayZero, ChapterController.Instance.loadedChapter.startingHour, ChapterController.Instance.loadedChapter.yearZeroLeapYearCycle);

            //Set weather
            SessionData.Instance.SetWeather(ChapterController.Instance.loadedChapter.rainAmount, ChapterController.Instance.loadedChapter.windAmount, ChapterController.Instance.loadedChapter.snowAmount, ChapterController.Instance.loadedChapter.lightningAmount, ChapterController.Instance.loadedChapter.transitionSpeed);
        }

        Game.Log("CityGen: Chapter " + preset.name + " (" + preset.chapterNumber + ") loaded...");
    }

    //Triggered on finalize load
    public virtual void OnLoadFinalize()
    {
        Game.Log("Chapter: OnLoadFinalize");
    }

    //Triggered when objects are created
    public virtual void OnObjectsCreated()
    {
        Game.Log("Chapter: OnObjectsCreated");
    }

    //Triggered when the player starts the game
    public virtual void OnGameStart()
    {
        Game.Log("Chapter: OnGameStart");

        CityConstructor.Instance.OnGameStarted -= OnGameStart;
        gameStart = true;

        if(ChapterController.Instance.loadFirstPartOnStart)
        {
            if(Game.Instance.demoMode && Game.Instance.demoChapterSkip)
            {
                ChapterController.Instance.SkipToChapterPart(21, true, false);
            }
            else
            {
                ChapterController.Instance.SkipToChapterPart(preset.startingPart, true, false); //Load the first part
            }
        }

        Player.Instance.nourishment = 0.7f;
        Player.Instance.hydration = 0.7f;
        Player.Instance.energy = 0.7f;

        //Listen for game world loop
        CitizenBehaviour.Instance.OnGameWorldLoop += OnGameWorldLoop;
    }

    //Clears all previously spawned and active objectives
    public void ClearAllObjectives()
    {
        if(gameStart)
        {
            Game.Log("Chapter: Clearing all objectives for " + preset.name + "...");

            //Clear speech
            Player.Instance.speechController.speechQueue.Clear();

            if (Player.Instance.speechController.activeSpeechBubble != null)
            {
                Destroy(Player.Instance.speechController.activeSpeechBubble.gameObject);
                Player.Instance.speechController.activeSpeechBubble = null;
            }

            Player.Instance.speechController.speechDelay = 0f; //Reset delays

            //Remove previous
            for (int i = 0; i < thisCase.currentActiveObjectives.Count; i++)
            {
                if (!thisCase.currentActiveObjectives[i].isCancelled)
                {
                    thisCase.currentActiveObjectives[i].Cancel();
                }
            }
        }
    }

    //Clear a specific objective
    public void ClearObjective(string clearThis)
    {
        Game.Log("Chapter: Clearing objectives named " + clearThis + "...");

        //Clear speech
        List<SpeechController.QueueElement> allNamed = Player.Instance.speechController.speechQueue.FindAll(item => item.isObjective && item.entryRef.ToLower() == clearThis.ToLower());

        foreach(SpeechController.QueueElement q in allNamed)
        {
            Player.Instance.speechController.speechQueue.Remove(q);
        }

        if (Player.Instance.speechController.activeSpeechBubble != null && allNamed.Contains(Player.Instance.speechController.activeSpeechBubble.speech))
        {
            Destroy(Player.Instance.speechController.activeSpeechBubble.gameObject);
            Player.Instance.speechController.activeSpeechBubble = null;
        }

        //Remove previous
        for (int i = 0; i < thisCase.currentActiveObjectives.Count; i++)
        {
            if(thisCase.currentActiveObjectives[i].queueElement.entryRef.ToLower() == clearThis.ToLower())
            {
                if (!thisCase.currentActiveObjectives[i].isCancelled)
                {
                    thisCase.currentActiveObjectives[i].Cancel();
                }
            }
        }
    }

    //Triggered when a new chapter part is started (after the game is started)
    public virtual void OnNewChapterPart(bool delay = false, bool teleportPlayer = false)
    {
        Game.Log("Chapter: OnNewChapterPart: " + ChapterController.Instance.currentPartName);
        Game.Log("CityGen: Chapter part " + ChapterController.Instance.currentPartName + " (" + ChapterController.Instance.currentPart + ") loaded...");

        teleportPlayerToChapter = teleportPlayer;
        chapterFrameDelay = delay;

        StopCoroutine("ChapterActivationDelay");
        StartCoroutine("ChapterActivationDelay");
    }

    //Cause a delay before activating this chapter. Not everything is loading in properly, could it just need more time?
    IEnumerator ChapterActivationDelay()
    {
        int frameDelay = 3;
        if (!chapterFrameDelay) frameDelay = 0;

        //Automatically invoke corresponding method
        MethodInfo method = this.GetType().GetMethod(ChapterController.Instance.currentPartName);
        object[] passed = { 0 };

        while (frameDelay > 0)
        {
            frameDelay--;
            yield return null;
        }

        method.Invoke(this, passed);

        //Teleport player
        if(teleportPlayerToChapter)
        {
            //Teleport player (if not using something)
            if (InteractionController.Instance.lockedInInteraction == null)
            {
                Player.Instance.Teleport(currentPartLocation, null);
            }
            else
            {
                Game.Log("Chapter: Cancelled teleport as player is using " + InteractionController.Instance.lockedInInteraction);
            }
        }
    }

    private void OnDestroy()
    {
        CityConstructor.Instance.OnLoadFinalize -= OnLoadFinalize;
        CityConstructor.Instance.OnGameStarted -= OnGameStart;
        ChapterController.Instance.OnNewPart -= OnNewChapterPart;
        CitizenBehaviour.Instance.OnGameWorldLoop -= OnGameWorldLoop;
        invokeOnDelay.Clear();
    }

    //Triggered upon game world loop
    public virtual void OnGameWorldLoop()
    {
        //Trigger onvoking on delay
        if(SessionData.Instance.play && invokeOnDelay.Count > 0)
        {
            float timePassed = SessionData.Instance.gameTime - CitizenBehaviour.Instance.timeOnLastGameWorldUpdate;

            List<string> toRemove = new List<string>();
            List<string> keys = invokeOnDelay.Keys.ToList();

            foreach(string str in keys)
            {
                invokeOnDelay[str] -= timePassed; //Convert time passed to real seconds

                //Game.Log(invokeOnDelay[str]);

                if (invokeOnDelay[str] <= 0)
                {
                    Invoke(str, 0f);
                    toRemove.Add(str);
                }
            }

            foreach(string str in toRemove)
            {
                invokeOnDelay.Remove(str);
            }
        }
    }

    public void InvokeAfterDelay(string command, float delayRealSeconds)
    {
        if(!invokeOnDelay.ContainsKey(command))
        {
            //Convert real seconds to game time
            float gtseconds = delayRealSeconds / 0.016667f / 60f / 60f * 0.6f;

            invokeOnDelay.Add(command, gtseconds); //Record this in real
        }
    }

    //Set for each part; the teleport location for this chapter
    public virtual void SetCurrentPartLocation(NewNode newNode)
    {
        if(newNode != null && newNode.gameLocation != null) Game.Log("Chapter: Set current part location to " + newNode.gameLocation.name);
        currentPartLocation = newNode;
    }

    //Player VO speech from dictionary
    public virtual void PlayerVO(string entryRef, float delay = 0f, bool useParsing = true, bool shouting = false, bool interupt = false, bool forceColour = false, Color color = new Color())
    {
        if(!Player.Instance.speechController.speechQueue.Exists(item => item.dictRef == ChapterController.Instance.loadedChapter.dictionary && item.entryRef == entryRef))
        {
            if (Strings.stringTable.ContainsKey("dds.blocks"))
            {
                if (Strings.stringTable["dds.blocks"].ContainsKey(entryRef))
                {

                    Player.Instance.speechController.Speak("dds.blocks", entryRef, useParsing, shouting, interupt, delay, forceColour, color);
                }
                else
                {
                    Game.Log("Chapter: Entry " + entryRef + " not found! This may have been deleted...");
                }
            }
        }
    }

    //Add an objective: Uses the player speech system so player's focus isn't divided between the two
    public virtual void AddObjective(string entryRef, List<Objective.ObjectiveTrigger> triggers, bool usePointer = false, Vector3 pointerPosition = new Vector3(), InterfaceControls.Icon useIcon = InterfaceControls.Icon.lookingGlass, Objective.OnCompleteAction onCompleteAction = Objective.OnCompleteAction.nextChapterPart, float delay = 0f, bool removePrevious = false, string chapterString = "", bool isSilent = false, bool allowCrouchPromt = false, bool useParsing = true)
    {
        //Only add objective if it's not already created (helps with save games)
        if (thisCase != null)
        {
            if (!thisCase.currentActiveObjectives.Exists(item => item.queueElement.entryRef == entryRef))
            {
                if (!thisCase.inactiveCurrentObjectives.Exists(item => item.queueElement.entryRef == entryRef))
                {
                    if (!thisCase.endedObjectives.Exists(item => item.queueElement.entryRef == entryRef))
                    {
                        Player.Instance.speechController.speechQueue.Add(new SpeechController.QueueElement(thisCase.id, entryRef, usePointer, pointerPosition, useIcon, triggers, onCompleteAction, delay, removePrevious, chapterString, isSilent, allowCrouchPromt, newUseParsing: useParsing));

                        //Start coroutine
                        Player.Instance.speechController.enabled = true;

                        //Force elements to bottom
                        List<SpeechController.QueueElement> toBottom = Player.Instance.speechController.speechQueue.FindAll(item => item.forceBottom);
                        
                        foreach(SpeechController.QueueElement sq in toBottom)
                        {
                            Player.Instance.speechController.speechQueue.Remove(sq);
                            Player.Instance.speechController.speechQueue.Add(sq);
                        }
                    }
                    else
                    {
                        Game.Log("Chapter: Objective " + entryRef + " already exists in case ended objectives");
                    }
                }
                else
                {
                    Game.Log("Chapter: Objective " + entryRef + " already exists in case inactive objectives");
                }
            }
            else
            {
                Game.Log("Chapter: Objective " + entryRef + " already exists in case active objectives");
            }
        }
        else
        {
            Game.LogError("Chapter: Trying to create objective " + entryRef + " but no case is assigned to the job!");
        }
    }

    //Same as above but using just one trigger
    public virtual void AddObjective(string entryRef, Objective.ObjectiveTrigger trigger, bool usePointer = false, Vector3 pointerPosition = new Vector3(), InterfaceControls.Icon useIcon = InterfaceControls.Icon.lookingGlass, Objective.OnCompleteAction onCompleteAction = Objective.OnCompleteAction.nextChapterPart, float delay = 0f, bool removePrevious = false, string chapterString = "", bool isSilent = false, bool allowCrouchPromt = false)
    {
        //Only add objective if it's not already created (helps with save games)
        if (thisCase != null)
        {
            thisCase.AddObjective(entryRef, trigger, usePointer, pointerPosition, useIcon, onCompleteAction, delay, removePrevious, chapterString, isSilent, allowCrouchPromt);
        }
        else
        {
            Game.LogError("Chapter: Trying to create objective " + entryRef + " but no case is assigned to the job!");
        }
    }

    public virtual StateSaveData.ChaperStateSave GetChapterSaveData()
    {
        return null;
    }

    public virtual void LoadStateSaveData(StateSaveData.ChaperStateSave newData)
    {
        
    }

    public Interactable LoadInteractableFromData(string reference, ref StateSaveData.ChaperStateSave saveData)
    {
        Interactable ret = null;

        int id = saveData.GetDataInt(reference);

        if(id > -1)
        {
            if(!CityData.Instance.savableInteractableDictionary.TryGetValue(id, out ret))
            {
                ret = CityData.Instance.interactableDirectory.Find(item => item.id == id);

                if(ret == null) Game.LogError("Chapter: Unable to load object " + reference + " for chapter, ID " + id + " out of " + CityData.Instance.savableInteractableDictionary.Count + " interactables");
            }
        }

        return ret;
    }
}
