﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirDuctGroup : Controller
{
    public int ductID = 0;
    public static int assignID = 0;
    public NewBuilding building;
    public bool isExterior = false; //If true this group features an exterior duct that must be drawn from outside
    public bool isVisible = true;

    [System.Serializable]
    public class AirVent
    {
        public int ventID = 0;
        public static int assignVentID = 0;

        public NewAddress.AirVent ventType;
        public NewWall wall; //If wall vent, this is the wall
        //public int wallID = -1; //Needed because these are loaded before walls...
        //public Vector3 nodeCoord; //Needed because these are loaded before nodes...
        public NewNode node; //If ceiling vent, this is the node
        public NewNode roomNode; //If wall went, this is the node on the side of the room
        public NewRoom room;
        public AirDuctGroup group;

        public MapDuctsButtonController mapButton;
        public bool discovered = false;
        public bool removed = false;
        public InteractableController spawned;
        //public int debugVentSpawnedRoom = -1;
        //public bool debugSpawnedVent = false;
        //public Vector3 debugSpawnedVentLocalPosition;
        //public Vector3 debugSpawnedVentLocalEuler;

        public Vector3 debugNode;
        public Vector3 debugRoomNode;

        //Create new
        public AirVent(NewAddress.AirVent newType, NewRoom newRoom)
        {
            ventID = assignVentID;
            assignVentID++;

            ventType = newType;
            room = newRoom;

            CityData.Instance.airVentDirectory.Add(this); //Add to directory
        }

        //Set as discovered
        public void SetDiscovered(bool val)
        {
            if (val != discovered)
            {
                discovered = val;

                //Set it's room to be at least level 1 discovery
                if(room.explorationLevel < 1)
                {
                    room.SetExplorationLevel(1);
                }

                //Add update call to map
                if (mapButton != null)
                {
                    MapController.Instance.AddDuctUpdateCall(mapButton);
                }
            }
        }

        //Load
        public AirVent(CitySaveData.AirVentSave load)
        {
            ventID = load.id;
            ventType = load.ventType;
            CityData.Instance.airVentDirectory.Add(this); //Add to directory
        }

        //Remove
        public void Remove()
        {
            removed = true;

            Game.Log("CityGen: Removed air vent " + ventID + " at " + room.name +" ("+room.gameLocation.name+")");

            //Set walls back to normal
            if(ventType == NewAddress.AirVent.ceiling)
            {
                node.SetCeilingVent(false);

            }
            else if(ventType == NewAddress.AirVent.wallLower || ventType == NewAddress.AirVent.wallUpper)
            {
                wall.SetDoorPairPreset(InteriorControls.Instance.wallNormal);
            }

            if(group != null) group.airVents.Remove(this);
            room.airVents.Remove(this);
            
            CityData.Instance.airVentDirectory.Remove(this); //Remove from directory
        }
    }

    [System.Serializable]
    public class AirDuctSection
    {
        public int level = 0;
        public int index = 0;
        public Vector3Int duct;
        public Vector3Int previous;
        public Vector3Int next;
        public bool ext = false; //True if exterior

        //Peek section? Is only true if a straight line...
        public bool peekSection = false;
        public Vector3Int additionalRot = Vector3Int.zero;

        public NewNode node;
        public AirDuctGroup group;

        public MapDuctsButtonController mapButton;
        public bool discovered = false;

        public AirDuctSection(int newLevel, int newIndex, Vector3Int newDuct, Vector3Int newPrevious, Vector3Int newNext, NewNode newNode, AirDuctGroup newGroup, bool newPeek, Vector3Int newAdditionalRot)
        {
            level = newLevel;
            index = newIndex;
            duct = newDuct;
            previous = newPrevious;
            next = newNext;
            node = newNode;
            group = newGroup;
            ext = node.room.IsOutside();
            peekSection = newPeek;
            additionalRot = newAdditionalRot;

            //Add to both group and node references
            group.airDucts.Add(this);
            node.airDucts.Add(this);

            //Add room ref to duct group
            if(newGroup != null)
            {
                if(!node.room.isNullRoom)
                {
                    node.room.AddDuctGroup(newGroup);
                }
            }

            //Add to map
            node.building.ductMap.Add(duct, this);
        }

        //Set as discovered
        public void SetDiscovered(bool val)
        {
            if (val != discovered)
            {
                discovered = val;

                //Add update call to map
                if(mapButton != null)
                {
                    MapController.Instance.AddDuctUpdateCall(mapButton);
                }

                int moneyForDucts = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.moneyForDucts));

                if(discovered && moneyForDucts > 0)
                {
                    GameplayController.Instance.AddMoney(moneyForDucts, false, "moneyforducts");
                }
            }
        }

        //Get neighboring connected sections along with their corresponding relative offsets
        public List<AirDuctSection> GetNeighborSections(out List<Vector3Int> relativeOffsets, out List<AirVent> vents, out List<Vector3Int> ventRelativeOffsets)
        {
            relativeOffsets = new List<Vector3Int>();
            vents = new List<AirVent>();
            ventRelativeOffsets = new List<Vector3Int>();

            List<AirDuctSection> ret = new List<AirDuctSection>();

            foreach(Vector3Int v3 in CityData.Instance.offsetArrayX6)
            {
                Vector3Int searchVector = duct + v3;
                AirDuctSection foundSection;

                if(node.building.ductMap.TryGetValue(searchVector, out foundSection))
                {
                    //we've found an adjacent section, but is it actually connected? To see check for them within routes...
                    if(v3 == next || v3 == previous || foundSection.next == -v3 || foundSection.previous == -v3)
                    {
                        if(!ret.Contains(foundSection))
                        {
                            ret.Add(foundSection);
                            relativeOffsets.Add(v3);
                        }
                    }
                }
            }

            //Search for air vents
            //Search for ceiling vent below...
            if (level == 2)
            {
                //We're looking for the same node, a ceiling vent with this as a level of 2...
                AirDuctGroup.AirVent foundVent = node.room.airVents.Find(item => item.node == node && item.ventType == NewAddress.AirVent.ceiling);

                if (foundVent != null)
                {
                    if (!vents.Contains(foundVent))
                    {
                        vents.Add(foundVent);

                        Vector3Int off = new Vector3Int(0, 0, -1);
                        ventRelativeOffsets.Add(off);
                    }
                }
            }
            //Search for wall vents
            else
            {
                //Get adjacent node ducts
                foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                {
                    Vector3Int offset = new Vector3Int(v2.x, v2.y, 0);
                    Vector3Int nodeSearch = node.nodeCoord + offset;
                    NewNode foundNode;

                    if (PathFinder.Instance.nodeMap.TryGetValue(nodeSearch, out foundNode))
                    {
                        AirDuctGroup.AirVent foundVent = foundNode.room.airVents.Find(item => item.node == node && item.roomNode == foundNode);

                        if (foundVent != null)
                        {
                            if (foundVent.ventType == NewAddress.AirVent.wallUpper && level == 1)
                            {
                                if(!vents.Contains(foundVent))
                                {
                                    vents.Add(foundVent);
                                    ventRelativeOffsets.Add(offset);
                                }
                            }
                            else if (foundVent.ventType == NewAddress.AirVent.wallLower && level == 0)
                            {
                                if (!vents.Contains(foundVent))
                                {
                                    vents.Add(foundVent);
                                    ventRelativeOffsets.Add(offset);
                                }
                            }
                        }
                    }
                }
            }

            return ret;
        }
    }

    [Header("Air Vents")]
    public List<AirVent> airVents = new List<AirVent>();

    [Header("Air Ducts")]
    public List<AirDuctSection> airDucts = new List<AirDuctSection>(); //0 = lower vent, 1 = upper vent, 2 = ceiling vent

    [Header("Combined Mesh")]
    public MeshFilter meshFilter;
    public MeshRenderer combinedMesh;

    [Header("Culling")]
    public List<AirDuctGroup> adjoiningGroups = new List<AirDuctGroup>();
    public List<NewRoom> ventRooms = new List<NewRoom>();

    //Create new
    public void SetupNew(NewBuilding newBuilding)
    {
        ductID = assignID;
        assignID++;
        this.transform.name = "Air Duct Group " + ductID.ToString();

        building = newBuilding;
        building.airDucts.Add(this);

        CityData.Instance.airDuctGroupDirectory.Add(this); //Add to directory
    }

    //Load existing
    public void Load(CitySaveData.AirDuctGroupCitySave load, NewBuilding newBuilding)
    {
        ductID = load.id;
        this.transform.name = "Air Duct Group " + ductID.ToString();

        building = newBuilding;
        isExterior = load.ext;
        building.airDucts.Add(this);

        CityData.Instance.airDuctGroupDirectory.Add(this); //Add to directory

        //Load air vents
        foreach (int ventID in load.airVents)
        {
            AirDuctGroup.AirVent foundVent = CityData.Instance.airVentDirectory.Find(item => item.ventID == ventID);

            if (foundVent == null)
            {
                Game.LogError("Cannot find vent with ID of " + ventID + " from " + CityData.Instance.airVentDirectory.Count + " loaded vents");
            }
            else
            {
                foundVent.group = this;
                AddAirVent(foundVent);
            }
        }

        //Load air ducts
        foreach (CitySaveData.AirDuctSegmentCitySave ductSegement in load.airDucts)
        {
            NewNode foundNode = null;

            if(PathFinder.Instance.nodeMap.TryGetValue(ductSegement.node, out foundNode))
            {
                AirDuctSection loadSeg = new AirDuctSection(ductSegement.level, ductSegement.index, ductSegement.duct, ductSegement.previous, ductSegement.next, foundNode, this, ductSegement.peek, ductSegement.addRot);
            }
        }
    }

    //Add a Air Duct section by passing the level (0 = lower vent, 1 = upper vent, 2 = ceiling)
    public void AddAirDuctSection(int level, Vector3Int duct, Vector3Int previous, Vector3Int next, NewNode newNode, int index = 0)
    {
        int existingLevel = airDucts.FindIndex(item => item.level == level && item.node == newNode);

        if (existingLevel > -1)
        {
            airDucts[existingLevel].index = index;
            airDucts[existingLevel].previous = previous;
            airDucts[existingLevel].next = next;
            return;
        }
        else
        {
            new AirDuctSection(Mathf.RoundToInt(level), index, duct, previous, next, newNode, this, false, Vector3Int.zero);
        }
    }

    //Add an air vent
    public void AddAirVent(AirVent newVent)
    {
        if(!airVents.Contains(newVent))
        {
            newVent.group = this; //Set group reference
            airVents.Add(newVent);

            if(!ventRooms.Contains(newVent.room))
            {
                ventRooms.Add(newVent.room);
            }
        }
    }

    //Add an adjoining air duct group
    public void AddAdjoiningDuctGroup(AirDuctGroup ductGroup)
    {
        if(!adjoiningGroups.Contains(ductGroup))
        {
            adjoiningGroups.Add(ductGroup);
        }

        ////Add vent rooms of the adjoining...
        //foreach (NewRoom room in ductGroup.ventRooms)
        //{
        //    if (!ventRooms.Contains(room))
        //    {
        //        ventRooms.Add(room);
        //    }
        //}
    }

    //Calculate the air duct models to spawn
    public void LoadDucts()
    {
        List<MeshFilter> childMeshes = new List<MeshFilter>(); //Populate list of children meshes as we spawn them

        foreach (AirDuctSection duct in airDucts)
        {
            List<Vector3Int> ductOffsets;
            List<Vector3Int> ventOffsets;
            duct.GetNeighborSections(out ductOffsets, out _, out ventOffsets);

            ductOffsets.AddRange(ventOffsets); //Combine vent + duct offsets for this purpose

            //List<Vector3> ductOffsets = GetDuctOffsets(duct.node, duct); //Get offsets, search for vents

            //Game.Log("Attempting to load air duct...");

            //We have a complete list of offsets now, we can scan for the correct component
            bool placed = false;

            string offsetDebug = string.Empty;

            if(Game.Instance.collectDebugData)
            {
                foreach (Vector3 offset in ductOffsets)
                {
                    offsetDebug += " " + offset.ToString();
                }
            }

            foreach (InteriorControls.AirDuctOffset air in InteriorControls.Instance.airDuctModels)
            {
                if (air.offsets.Count != ductOffsets.Count) continue; //Must be the same number of offsets...

                bool valid = true;

                foreach (Vector3 offset in ductOffsets)
                {
                    if (!air.offsets.Contains(offset.normalized))
                    {
                        valid = false;
                        break;
                    }
                }

                if (!valid) continue;

                GameObject prefabModel = air.prefabs[(int)duct.index]; //Get prefab model from configured list

                //Make this a peek section
                if (CityConstructor.Instance.generateNew)
                {
                    //Rules for changing to peek model: Must be a straight section
                    //Must be not in a null room or outside
                    if(prefabModel == InteriorControls.Instance.ductStraightModel)
                    {
                        if((!duct.node.room.isNullRoom && !duct.node.room.isBaseNullRoom && duct.level < 2) || (duct.node.room.IsOutside() && !duct.node.room.isOutsideWindow))
                        {
                            string seed = duct.node.nodeCoord.ToString() + duct.node.room.roomID.ToString();

                            //50% chance of peek vent
                            if(Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) <= 0.5f)
                            {
                                duct.peekSection = true;

                                //Give a random Z rotation of -90, 0 or 90
                                duct.additionalRot = new Vector3Int(0, 0, Toolbox.Instance.GetPsuedoRandomNumberContained(-1, 2, seed, out seed) * 90);
                            }
                        }
                    }
                }

                //Replace straight with peek section
                if (duct.peekSection)
                {
                    prefabModel = InteriorControls.Instance.ductStraightWithPeekVent;

                    //Add this room to visible rooms
                    if (!ventRooms.Contains(duct.node.room))
                    {
                        ventRooms.Add(duct.node.room);
                    }
                }

                //If this is reached, we have a valid model!
                //Game.Log("... Air duct loaded!");
                GameObject newDuctModel = Instantiate(prefabModel, this.transform);
                newDuctModel.transform.position = CityData.Instance.NodeToRealpos(duct.node.nodeCoord + new Vector3(0, 0, duct.level * 2f + 1f) / 6f) + new Vector3(0, InteriorControls.Instance.airDuctYOffset, 0);
                newDuctModel.transform.localEulerAngles = air.rotation + duct.additionalRot;
                newDuctModel.name = "L" + duct.level.ToString() + " Duct:" + offsetDebug + " outside: " + duct.node.room.IsOutside();
                childMeshes.Add(newDuctModel.GetComponent<MeshFilter>());

                //Do we also need an air vent?
                //Open gaps in wall and ceilings...
                foreach (Vector3Int offset in ductOffsets)
                {
                    if (duct.level == 2 && offset.z == -1)
                    {
                        duct.node.SetCeilingVent(true);
                    }
                    else if(duct.level == 2 && offset.z == 1)
                    {
                        //Make a hole in above's floor
                        Vector3Int aboveNode = duct.node.nodeCoord + new Vector3Int(0, 0, 1);
                        NewNode foundNode = null;

                        if (PathFinder.Instance.nodeMap.TryGetValue(aboveNode, out foundNode))
                        {
                             foundNode.SetFloorVent(true);
                        }
                    }
                    else if(duct.level == 0 && offset.z == 1)
                    {
                        duct.node.SetFloorVent(true);
                    }
                    //Make holes in walls & spawn vents
                    else if (offset.z == 0)
                    {
                        Vector3Int nodeSearch = duct.node.nodeCoord + offset;
                        NewNode foundNode = null;

                        if (PathFinder.Instance.nodeMap.TryGetValue(nodeSearch, out foundNode))
                        {
                            foreach (NewWall wall in duct.node.walls)
                            {
                                if (wall.otherWall.node == foundNode)
                                {
                                    if (duct.level == 2)
                                    {
                                        //Game.Log("Load level 2 duct at " + duct.node.position + ", found offseted towards: " + foundNode.position + ": " + wall.preset.sectionClass.ToString());

                                        //Search below for other ducts...
                                        bool foundUpperVent = false;
                                        bool foundLowerVent = false;

                                        for (int i = -1; i >= -2; i--)
                                        {
                                            Vector3Int searchThisNode = duct.duct + new Vector3Int(0, 0, i);
                                            AirDuctSection fSection = null;

                                            if (duct.node.building.ductMap.TryGetValue(searchThisNode, out fSection))
                                            {
                                                //Game.Log("...level 2 duct at " + duct.node.position + ": Found below duct section");

                                                //Look for offsets in the same direction as this...
                                                List<Vector3Int> fDuctOffsets;
                                                List<Vector3Int> fVentOffsets;
                                                fSection.GetNeighborSections(out fDuctOffsets, out _, out fVentOffsets);

                                                if(fDuctOffsets.Contains(offset) || fVentOffsets.Contains(offset))
                                                {
                                                    if (i == -1)
                                                    {
                                                        foundUpperVent = true;
                                                        //Game.Log("...level 2 duct at " + duct.node.position + ": Found upper vent");
                                                    }
                                                    else if (i == -2)
                                                    {
                                                        foundLowerVent = true;
                                                        //Game.Log("...level 2 duct at " + duct.node.position + ": Found lower vent");
                                                    }
                                                }
                                            }
                                        }

                                        if(foundUpperVent)
                                        {
                                            wall.SetDoorPairPreset(InteriorControls.Instance.wallVentUpperWithTopSpace, true);
                                        }
                                        else if(foundLowerVent)
                                        {
                                            wall.SetDoorPairPreset(InteriorControls.Instance.wallVentLowerWithTopSpace, true);
                                        }
                                        else if (wall.preset.sectionClass == DoorPairPreset.WallSectionClass.wall)
                                        {
                                            //Game.Log("...level 2 duct at " + duct.node.position + ": Found wall class, setting to vent top");
                                            wall.SetDoorPairPreset(InteriorControls.Instance.wallVentTop, true);
                                        }

                                        break;
                                    }
                                    else if (duct.level == 1)
                                    {
                                        if (wall.preset.sectionClass == DoorPairPreset.WallSectionClass.wall)
                                        {
                                            wall.SetDoorPairPreset(InteriorControls.Instance.wallVentUpper, true);
                                        }

                                        break;
                                    }
                                    else if (duct.level == 0)
                                    {
                                        if (wall.preset.sectionClass == DoorPairPreset.WallSectionClass.wall)
                                        {
                                            wall.SetDoorPairPreset(InteriorControls.Instance.wallVentLower, true);
                                        }

                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                //Search for vents...
                //Search for ceiling vent below...
                if (duct.level == 2)
                {
                    //We're looking for the same node, a ceiling vent with this as a level of 2...
                    AirDuctGroup.AirVent foundVent = duct.node.room.airVents.Find(item => item.node == duct.node && item.ventType == NewAddress.AirVent.ceiling);

                    if (foundVent != null)
                    {
                        Vector3 off = new Vector3(0, -1, 0);
                        Vector3 worldOffset = newDuctModel.transform.position + (off * (PathFinder.Instance.nodeSize.x * 0.5f)) + new Vector3(0, 0.1f, 0);

                        Vector3 localDownward = this.transform.InverseTransformDirection(off);

                        //We want this to face away from the parent's position
                        Quaternion facingQuat = Quaternion.LookRotation(localDownward);
                        Vector3 localEuler = Toolbox.Instance.GetLocalEulerAtRotation(this.transform, facingQuat);

                        //Spawn an air vent
                        Interactable newVent = InteractableCreator.Instance.CreateTransformInteractable(PrefabControls.Instance.airVent, this.transform, null, null, this.transform.InverseTransformPoint(worldOffset), localEuler, null);
                        
                        if(newVent != null)
                        {
                            newVent.SetPolymorphicReference(this);
                            newVent.LoadInteractableToWorld(forceSpawnImmediate: true); //Force load this as it's part of the duct, not the room.
                            Toolbox.Instance.SetLightLayer(newVent.spawnedObject, building, isExterior);
                            if(newVent.controller != null) newVent.controller.debugVent = foundVent;
                            foundVent.spawned = newVent.controller;
                        }

                        //Add this group to the room's reference
                        duct.node.room.AddDuctGroup(this);
                    }
                }
                //Search for wall vents
                else
                {
                    //Get adjacent node ducts
                    foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                    {
                        Vector3Int offset = new Vector3Int(v2.x, v2.y, 0);
                        Vector3 unityOffset = new Vector3(v2.x, 0, v2.y);
                        Vector3 worldOffset = newDuctModel.transform.position + (unityOffset * ((PathFinder.Instance.nodeSize.x * 0.5f) + 0.0011f));
                        Vector3Int nodeSearch = duct.node.nodeCoord + offset;
                        NewNode foundNode = null;

                        Vector3 localDownward = this.transform.InverseTransformDirection(unityOffset);

                        //We want this to face away from the parent's position
                        Quaternion facingQuat = Quaternion.LookRotation(localDownward);
                        Vector3 localEuler = Toolbox.Instance.GetLocalEulerAtRotation(this.transform, facingQuat);

                        if (PathFinder.Instance.nodeMap.TryGetValue(nodeSearch, out foundNode))
                        {
                            if(duct.level == 1)
                            {
                                AirDuctGroup.AirVent foundVent = foundNode.room.airVents.Find(item => item.node == duct.node && item.roomNode == foundNode && item.ventType == NewAddress.AirVent.wallUpper);

                                if (foundVent != null)
                                {
                                    //Spawn an air vent
                                    Interactable newVent = InteractableCreator.Instance.CreateTransformInteractable(PrefabControls.Instance.airVent, this.transform, null, null, this.transform.InverseTransformPoint(worldOffset), localEuler, null);
                                    
                                    if(newVent != null)
                                    {
                                        newVent.SetPolymorphicReference(this);
                                        newVent.LoadInteractableToWorld(forceSpawnImmediate: true); //Force load this as it's part of the duct, not the room.
                                        Toolbox.Instance.SetLightLayer(newVent.spawnedObject, building, isExterior);
                                        if(newVent.controller != null) newVent.controller.debugVent = foundVent;
                                        foundVent.spawned = newVent.controller;
                                        //foundVent.debugVentSpawnedRoom = newVent.node.room.roomID;
                                        //foundVent.debugSpawnedVent = true;
                                        //foundVent.debugSpawnedVentLocalPosition = this.transform.InverseTransformPoint(worldOffset);
                                        //foundVent.debugSpawnedVentLocalEuler = localEuler;
                                    }

                                    //Add this group to the room's reference
                                    foundNode.room.AddDuctGroup(this);
                                }
                            }
                            else if(duct.level == 0)
                            {
                                AirDuctGroup.AirVent foundVent = foundNode.room.airVents.Find(item => item.node == duct.node && item.roomNode == foundNode && item.ventType == NewAddress.AirVent.wallLower);

                                if (foundVent != null)
                                {
                                    //Spawn an air vent
                                    Interactable newVent = InteractableCreator.Instance.CreateTransformInteractable(PrefabControls.Instance.airVent, this.transform, null, null, this.transform.InverseTransformPoint(worldOffset), localEuler, null);

                                    if (newVent != null)
                                    {
                                        newVent.SetPolymorphicReference(this);
                                        newVent.LoadInteractableToWorld(forceSpawnImmediate: true); //Force load this as it's part of the duct, not the room.
                                        Toolbox.Instance.SetLightLayer(newVent.spawnedObject, building, isExterior);
                                        if(newVent.controller != null) newVent.controller.debugVent = foundVent;
                                        foundVent.spawned = newVent.controller;
                                        //foundVent.debugVentSpawnedRoom = newVent.node.room.roomID;
                                        //foundVent.debugSpawnedVent = true;
                                        //foundVent.debugSpawnedVentLocalPosition = this.transform.InverseTransformPoint(worldOffset);
                                        //foundVent.debugSpawnedVentLocalEuler = localEuler;
                                    }

                                    //Add this group to the room's reference
                                    foundNode.room.AddDuctGroup(this);
                                }
                            }
                        }
                    }
                }

                //Set light layer: This should include the vents
                if(!Game.Instance.combineAirDuctMeshes)
                {
                    Toolbox.Instance.SetLightLayer(newDuctModel, building);
                }

                placed = true;
                break;
            }

            if (!placed)
            {
                Game.LogError("Unable to find air duct model for offsets:" + offsetDebug);
            }
        }

        //Combine all meshes
        if (Game.Instance.combineAirDuctMeshes)
        {
            //Because the only way to do this is to use world coordinates, make this container position @ 0,0,0
            this.transform.position = Vector3.zero;
            this.transform.eulerAngles = Vector3.zero;

            //Create a mesh instance for combined meshes
            CombineInstance[] combine = new CombineInstance[childMeshes.Count];

            //Cycle through meshes to combine them
            int i = 0;

            while (i < childMeshes.Count)
            {
                if (childMeshes[i].transform == transform) continue;

                combine[i].mesh = childMeshes[i].sharedMesh;
                combine[i].transform = childMeshes[i].transform.localToWorldMatrix;
                Destroy(childMeshes[i].gameObject); //Remove gameobjects
                //childMeshes[i].gameObject.SetActive(false);
                i++;
            }

            //Create mesh renderer
            meshFilter = this.gameObject.AddComponent<MeshFilter>();
            combinedMesh = this.gameObject.AddComponent<MeshRenderer>();
            MeshCollider meshColl = this.gameObject.AddComponent<MeshCollider>();

            //New mesh
            meshFilter.mesh = new Mesh();
            meshFilter.mesh.CombineMeshes(combine, true, true);
            transform.gameObject.SetActive(true);

            //Set to block rain
            this.gameObject.layer = 28;

            combinedMesh.sharedMaterial = InteriorControls.Instance.ductMaterial;
            combinedMesh.shadowCastingMode = Game.Instance.airDuctShadowMode;

            meshColl.sharedMesh = meshFilter.mesh;

            //Reset position to building pos
            this.transform.localPosition = Vector3.zero;
            this.transform.localEulerAngles = Vector3.zero;

            //Set light layer: This should include the vents
            Toolbox.Instance.SetLightLayer(combinedMesh, building, isExterior);

            //Give LOD
            LODGroup newLOD = this.gameObject.AddComponent<LODGroup>();
            LOD l = new LOD(CullingControls.Instance.airDuctLODThreshold, new MeshRenderer[] { combinedMesh });
            newLOD.SetLODs(new LOD[] { l });
        }

        //Start culled
        SetVisible(false);
    }

    //Set culling
    public void SetVisible(bool newVis)
    {
        if(isVisible != newVis)
        {
            isVisible = newVis;
            this.gameObject.SetActive(isVisible); //Self

        #if UNITY_EDITOR
            if (UnityEditor.Selection.Contains(this.gameObject))
            {
                Game.Log("Set air duct visibility: " + newVis + " building vis: " + building.displayBuildingModel);
            }
        #endif
        }
    }

    //Get offsets for neighboring air ducts (deprecated)
    public List<Vector3Int> GetDuctOffsets(NewNode thisNode, AirDuctSection duct)
    {
        //Get list of offsets
        List<Vector3Int> ductOffsets = new List<Vector3Int>();
        List<AirDuctSection> offsetDucts = new List<AirDuctSection>();

        //Get adjacent node ducts
        foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
        {
            Vector3Int offset = new Vector3Int(v2.x, v2.y, 0);
            Vector3Int nodeSearch = thisNode.nodeCoord + offset;
            NewNode foundNode = null;

            if (PathFinder.Instance.nodeMap.TryGetValue(nodeSearch, out foundNode))
            {
                //Does a vent exist on this level?
                AirDuctSection foundOffset = foundNode.airDucts.Find(item => item.level == duct.level);

                if (foundOffset != null)
                {
                    ductOffsets.Add(offset);
                    offsetDucts.Add(foundOffset);
                }
            }
        }

        //Now search for above...
        if (duct.level < 2)
        {
            //Can search on this node...
            AirDuctSection foundOffset = thisNode.airDucts.Find(item => (int)item.level == (int)duct.level + 1);

            if (foundOffset != null)
            {
                ductOffsets.Add(new Vector3Int(0, 0, 1));
                offsetDucts.Add(foundOffset);
            }
        }
        else
        {
            Vector3Int offset = new Vector3Int(0, 0, 1);
            Vector3Int nodeSearch = thisNode.nodeCoord + offset;
            NewNode foundNode = null;

            if (PathFinder.Instance.nodeMap.TryGetValue(nodeSearch, out foundNode))
            {
                //Does a vent exist on lower level?
                AirDuctSection foundOffset = foundNode.airDucts.Find(item => (int)item.level == 0);

                if (foundOffset != null)
                {
                    ductOffsets.Add(offset);
                    offsetDucts.Add(foundOffset);
                }
            }
        }

        //Now search for below...
        if (duct.level > 0)
        {
            //Can search on this node...
            AirDuctSection foundOffset = thisNode.airDucts.Find(item => (int)item.level == (int)duct.level - 1);

            if (foundOffset != null)
            {
                ductOffsets.Add(new Vector3Int(0, 0, -1));
                offsetDucts.Add(foundOffset);
            }
        }
        else
        {
            Vector3Int offset = new Vector3Int(0, 0, -1);
            Vector3Int nodeSearch = thisNode.nodeCoord + offset;
            NewNode foundNode = null;

            if (PathFinder.Instance.nodeMap.TryGetValue(nodeSearch, out foundNode))
            {
                //Does a vent exist on ceiling level?
                AirDuctSection foundOffset = foundNode.airDucts.Find(item => (int)item.level == 2);

                if (foundOffset != null)
                {
                    ductOffsets.Add(offset);
                    offsetDucts.Add(foundOffset);
                }
            }
        }

        //Remove connections that aren't part of the route...
        for (int i = 0; i < ductOffsets.Count; i++)
        {
            Vector3Int offset = ductOffsets[i];
            AirDuctSection ductData = offsetDucts[i];

            //This offset isn't in my route (but this might be in theirs)
            if (offset != duct.previous && offset != duct.next)
            {
                if (ductData.previous != -offset && ductData.next != -offset)
                {
                    ductOffsets.RemoveAt(i);
                    offsetDucts.RemoveAt(i);
                    i--;
                }
            }
        }

        //Search for vents...
        //Search for ceiling vent below...
        if (duct.level == 2)
        {
            //We're looking for the same node, a ceiling vent with this as a level of 2...
            AirDuctGroup.AirVent foundVent = thisNode.room.airVents.Find(item => item.node == thisNode && item.ventType == NewAddress.AirVent.ceiling);

            if (foundVent != null)
            {
                Vector3Int off = new Vector3Int(0, 0, -1);

                if (!ductOffsets.Contains(off))
                {
                    ductOffsets.Add(off);
                }
            }
        }
        //Search for wall vents
        else
        {
            //Get adjacent node ducts
            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
            {
                Vector3Int offset = new Vector3Int(v2.x, v2.y, 0);
                Vector3Int nodeSearch = thisNode.nodeCoord + offset;
                NewNode foundNode = null;

                if (PathFinder.Instance.nodeMap.TryGetValue(nodeSearch, out foundNode))
                {
                    AirDuctGroup.AirVent foundVent = foundNode.room.airVents.Find(item => item.node == thisNode);

                    if (foundVent != null)
                    {
                        if (foundVent.ventType == NewAddress.AirVent.wallUpper && duct.level == 1)
                        {
                            if (!ductOffsets.Contains(offset))
                            {
                                ductOffsets.Add(offset);
                            }
                        }

                        if (foundVent.ventType == NewAddress.AirVent.wallLower && duct.level == 0)
                        {
                            if (!ductOffsets.Contains(offset))
                            {
                                ductOffsets.Add(offset);
                            }
                        }
                    }
                }
            }
        }

        return ductOffsets;
    }

    public CitySaveData.AirDuctGroupCitySave GenerateSaveData()
    {
        CitySaveData.AirDuctGroupCitySave output = new CitySaveData.AirDuctGroupCitySave();
        output.id = ductID;
        output.ext = isExterior;

        //Save air vents by ID. They should be created when it is time to load them.
        foreach (AirDuctGroup.AirVent vent in airVents)
        {
            if (vent.removed) continue;
            output.airVents.Add(vent.ventID);
        }

        //Save air ducts
        foreach (AirDuctSection ductSection in airDucts)
        {
            CitySaveData.AirDuctSegmentCitySave newSeg = new CitySaveData.AirDuctSegmentCitySave();
            newSeg.index = ductSection.index;
            newSeg.level = ductSection.level;
            newSeg.next = ductSection.next;
            newSeg.node = ductSection.node.nodeCoord;
            newSeg.previous = ductSection.previous;
            newSeg.duct = ductSection.duct;
            newSeg.peek = ductSection.peekSection;
            newSeg.addRot = ductSection.additionalRot;
            output.airDucts.Add(newSeg);
        }

        //Save adjoining duct groups
        foreach(AirDuctGroup grp in adjoiningGroups)
        {
            output.adjoining.Add(grp.ductID);
        }

        return output;
    }
}
