﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;

public class SceneRecorder
{
    public Interactable interactable; //Interactable of the camera
    public List<NewRoom> coversRooms = new List<NewRoom>(); //Room of the camera (captures this)
    public Dictionary<NewNode, List<NewRoom.CullTreeEntry>> coveredNodes = new Dictionary<NewNode, List<NewRoom.CullTreeEntry>>(); //A list of nodes to track that the camera can see, and any doors required to be open to see this...

    public HashSet<NewNode> primaryNodes = new HashSet<NewNode>(); //Nodes the camera can see without depending on doors. These will end up triggering captures...
    public HashSet<NewRoom> primaryRooms = new HashSet<NewRoom>(); //All rooms featuring nodes above

    public static List<ScenePoserController> scenePoserPool = new List<ScenePoserController>();
    public static Dictionary<string, List<GameObject>> objectPoserPool = new Dictionary<string, List<GameObject>>();

    public float lastCaptureAt = 0f;
    public static int assignCapID = 1;

    //Events
    //Triggered when this is discovered
    public delegate void OnCapture();
    public event OnCapture OnNewCapture;

    [System.Serializable]
    public class SceneCapture
    {
        //Non saved data
        [System.NonSerialized]
        public SceneRecorder recorder;

        //Saved data
        public int id = 0;
        public List<DynamicRecordPosition> drp = null; //Dynamic saved position (for captures from a recorder that moves)
        public float t; //Timestamp
        public bool k = false; //Keep: If true, don't auto delete

        public List<RoomCapture> rCap = new List<RoomCapture>(); //Room capture
        public List<DoorCapture> dCap = new List<DoorCapture>(); //Door capture
        public List<ActorCapture> aCap = new List<ActorCapture>(); //Actor capture
        public List<InteractableCapture> oCap = new List<InteractableCapture>(); //Object capture
        public List<InteractableStateCapture> oSCap = new List<InteractableStateCapture>(); //Object state capture

        public SceneCapture(SceneRecorder newRecorder, bool detailedCapture)
        {
            id = SceneRecorder.assignCapID;
            SceneRecorder.assignCapID++;
            recorder = newRecorder;

            t = SessionData.Instance.gameTime;
            //t = Toolbox.Instance.RoundToPlaces(SessionData.Instance.gameTime, 5); //Low precision time to save serialization space

            //If this is the player, record positon & rotation
            if (recorder.interactable.isActor != null && recorder.interactable.isActor.isPlayer)
            {
                recorder.RefreshCoveredArea(); //Refresh covered area as the player moves around a lot
                drp = new List<DynamicRecordPosition>();
                drp.Add(new DynamicRecordPosition{ pos = recorder.interactable.cvp, rot = recorder.interactable.cve });

                //Game.Log("Record player positon with scene capture " + id + " using dynamic position:" + recorder.interactable.cvp + ", rot: " + recorder.interactable.cve);
            }

            //Record main light state from (all) rooms, whether they are behind a door or not...
            for (int i = 0; i < recorder.coversRooms.Count; i++)
            {
                RoomCapture newRoomCap = new RoomCapture();
                newRoomCap.id = recorder.coversRooms[i].roomID;
                newRoomCap.light = recorder.coversRooms[i].mainLightStatus;
                rCap.Add(newRoomCap);
            }

            HashSet<NewRoom> visibleRooms = new HashSet<NewRoom>();
            HashSet<NewDoor> capturedDoors = new HashSet<NewDoor>();
            Vector3 capPos = GetCaptureWorldPosition();

            //Loop through all possible nodes...
            foreach (KeyValuePair<NewNode, List<NewRoom.CullTreeEntry>> pair in recorder.coveredNodes)
            {
                //Is this node visible?
                if (pair.Value != null && pair.Value.Count > 0)
                {
                    bool pass = false;

                    foreach (NewRoom.CullTreeEntry c in pair.Value)
                    {
                        bool doorPass = true;

                        foreach (int d in c.requiredOpenDoors)
                        {
                            NewDoor getDoor = null;

                            if (CityData.Instance.doorDictionary.TryGetValue(d, out getDoor))
                            {
                                //Capture door state
                                if(!capturedDoors.Contains(getDoor))
                                {
                                    dCap.Add(new DoorCapture(getDoor));
                                    capturedDoors.Add(getDoor);
                                }

                                if (getDoor.isClosed)
                                {
                                    doorPass = false;
                                    break;
                                }
                            }
                            else
                            {
                                Game.LogError("Cannot find door at wall " + d + " (" + CityData.Instance.doorDictionary.Count + ")");
                            }
                        }

                        if (doorPass)
                        {
                            pass = true;
                            break;
                        }
                    }

                    if (!pass)
                    {
                        if (recorder.interactable.isActor != null && recorder.interactable.isActor.isPlayer)
                        {
                            Game.Log("A door is blocking the way to: " + pair.Key.room.name);
                        }

                        continue;
                    }
                }

                //Capture interactables...
                foreach (Interactable i in pair.Key.interactables)
                {
                    if(i.preset.showWorldObjectInSceneCapture)
                    {
                        if(i.preset.captureStateInSceneCapture)
                        {
                            oSCap.Add(new InteractableStateCapture(i));
                        }
                    }
                    else if(i.preset.createProxy && i.preset.prefab != null)
                    {
                        if (i.preset.onlyCreateProxyInDetailedCapture && !detailedCapture) continue;

                        Vector3 wp = i.GetWorldPosition();

                        float dist = Vector3.Distance(wp, capPos);
                        float range = 999;

                        if (ObjectPoolingController.Instance.loadRanges.ContainsKey((int)i.preset.createProxyAtRange))
                        {
                            range = ObjectPoolingController.Instance.loadRanges[(int)i.preset.createProxyAtRange];
                        }

                        if(dist < range)
                        {
                            oCap.Add(new InteractableCapture(i));
                        }
                    }
                }

                if (!visibleRooms.Contains(pair.Key.room))
                {
                    if (recorder.interactable.isActor != null && recorder.interactable.isActor.isPlayer)
                    {
                        Game.Log("Adding visible room for player scene capture: " + pair.Key.room.name);
                    }

                    visibleRooms.Add(pair.Key.room);
                }
            }

            //Scan for visible humans
            HashSet<Human> visibleHumans = new HashSet<Human>();

            foreach (NewRoom r in visibleRooms)
            {
                foreach (Human ac in r.currentOccupants)
                {
                    if (ac.isPlayer) continue;

                    if (!visibleHumans.Contains(ac))
                    {
                        //if (recorder.interactable.isActor != null && recorder.interactable.isActor.isPlayer)
                        //{
                        //    Game.Log("Adding visible human pending distance check: " + ac.name);
                        //}

                        if (Vector3.Distance(ac.transform.position, capPos) <= GameplayControls.Instance.humanCaptureRange)
                        {
                            //Human must be on a node covered by the camera
                            if (recorder.coveredNodes.ContainsKey(ac.currentNode))
                            {
                                //if (recorder.interactable.isActor != null && recorder.interactable.isActor.isPlayer)
                                //{
                                //    Game.Log("Adding visible human: " + ac.name);
                                //}

                                ActorCapture newCap = new ActorCapture(ac, false);
                                aCap.Add(newCap);
                                visibleHumans.Add(ac);

                                //Add to primary actors...
                                //if (recorder.primaryNodes.Contains(ac.currentNode))
                                //{
                                //    primaryActors.Add(ac.humanID);
                                //}

                                //newCap.inScene = true;
                            }
                        }
                    }
                }
            }

            //Game.Log("Gameplay: " + SessionData.Instance.gameTime + " Scene capture of " + recorder.coveredNodes.Count + " nodes with " + capturedObjects.Count + " interactables and " + actorCapture.Count + " actors...");
        }

        public SceneCapture(SceneCapture copyFrom)
        {
            id = SceneRecorder.assignCapID;
            SceneRecorder.assignCapID++;

            recorder = copyFrom.recorder;
            t = copyFrom.t;
            drp = copyFrom.drp;
            k = copyFrom.k;
            rCap = new List<RoomCapture>(copyFrom.rCap);
            dCap = new List<DoorCapture>(copyFrom.dCap);
            aCap = new List<ActorCapture>(copyFrom.aCap);
            oCap = new List<InteractableCapture>(copyFrom.oCap);
            oSCap = new List<InteractableStateCapture>(copyFrom.oSCap);
        }

        public NewGameLocation GetCaptureGamelocation()
        {
            NewNode n = Toolbox.Instance.FindClosestValidNodeToWorldPosition(GetCaptureWorldPosition());

            if (n != null)
            {
                return n.gameLocation;
            }
            else return null;
        }

        public NewRoom GetCaptureRoom()
        {
            NewNode n = Toolbox.Instance.FindClosestValidNodeToWorldPosition(GetCaptureWorldPosition());

            if (n != null)
            {
                return n.room;
            }
            else return null;
        }

        public float GetDecimalClock()
        {
            float d = 0;
            SessionData.Instance.ParseTimeData(t, out d, out _, out _, out _, out _, out _, out _, out _);
            return d;
        }

        public Vector3 GetCaptureWorldPosition()
        {
            if (drp != null && drp.Count > 0)
            {
                //Game.Log("Gameplay: Return DRP");
                return drp[0].pos;
            }

            if (recorder != null)
            {
                //Game.Log("Gameplay: Return cvp " + recorder.interactable.cvp + " from " + recorder.interactable.name + " (" + recorder.interactable.id + ")");
                return recorder.interactable.cvp;
            }
            else
            {
                Game.LogError("Unable to get capture interactable");
                return Vector3.zero;
            }
        }

        public Vector3 GetCaptureWorldRotation()
        {
            if (drp != null && drp.Count > 0)
            {
                return drp[0].rot;
            }

            if (recorder != null) return recorder.interactable.cve;
            else return Vector3.zero;
        }
    }

    [System.Serializable]
    public class DynamicRecordPosition
    {
        public Vector3 pos;
        public Vector3 rot;
    }

    [System.Serializable]
    public class RoomCapture
    {
        public int id;
        public bool light;

        public NewRoom GetRoom()
        {
            NewRoom r = null;
            CityData.Instance.roomDictionary.TryGetValue(id, out r);
            return r;
        }
    }


    [System.Serializable]
    public class TransformCapture
    {
        public Vector3 wP; //Captured world pos
        public Quaternion wR; //Captured world rotation

        public TransformCapture(Vector3 pos, Quaternion rot)
        {
            //Use low precision here to use up less characters in serialized text
            wP = new Vector3(Mathf.RoundToInt(pos.x * 100)/100f, Mathf.RoundToInt(pos.y * 100) / 100f, Mathf.RoundToInt(pos.z * 100) / 100f);
            wR = new Quaternion(Mathf.RoundToInt(rot.x * 100) / 100f, Mathf.RoundToInt(rot.y * 100) / 100f, Mathf.RoundToInt(rot.z * 100) / 100f, Mathf.RoundToInt(rot.w * 100) / 100f);
        }
    }

    [System.Serializable]
    public class DoorCapture
    {
        public int id; //Wall ID
        public int a; //Ajar

        public DoorCapture(NewDoor door)
        {
            id = door.wall.id;
            a = Mathf.RoundToInt(door.ajar);
        }

        public NewDoor GetDoor()
        {
            NewDoor ret = null;

            if (CityData.Instance.doorDictionary.TryGetValue(id, out ret))
            {

            }

            return ret;
        }

        public bool IsOpen()
        {
            if (a == 0) return false;
            else return true;
        }
    }

    [System.Serializable]
    public class InteractableCapture : TransformCapture
    {
        public string p; //Reference to preset

        [System.NonSerialized]
        public GameObject poser;

        public InteractableCapture(Interactable newInter) : base(newInter.wPos, Quaternion.Euler(newInter.wEuler))
        {
            p = newInter.preset.name;
        }

        public InteractablePreset GetPreset()
        {
            return Toolbox.Instance.GetInteractablePreset(p);
        }

        public void Load()
        {
            InteractablePreset pr = GetPreset();

            //Get a scene poser
            if (SceneRecorder.objectPoserPool.ContainsKey(p) && SceneRecorder.objectPoserPool[p].Count > 0)
            {
                poser = SceneRecorder.objectPoserPool[p][0];
                SceneRecorder.objectPoserPool[p].RemoveAt(0);
            }
            else
            {
                //Check for pool limit
                if (SceneRecorder.objectPoserPool.Keys.Count > 30)
                {
                    string getFirst = SceneRecorder.objectPoserPool.Keys.FirstOrDefault();

                    for (int i = 0; i < SceneRecorder.objectPoserPool[getFirst].Count; i++)
                    {
                        Toolbox.Instance.DestroyObject(SceneRecorder.objectPoserPool[getFirst][i].gameObject);
                        //i--;
                    }

                    SceneRecorder.objectPoserPool.Remove(getFirst);
                }

                if(pr != null && pr.spawnable && pr.prefab != null)
                {
                    poser = Toolbox.Instance.SpawnObject(pr.prefab, PrefabControls.Instance.poserContainer);
                    poser.name = pr.prefab.name;
                    LODGroup lodG = poser.GetComponentInChildren<LODGroup>(true);

                    if (lodG != null)
                    {
                        Object.DestroyImmediate(lodG);
                    }
                }
            }

            if(poser != null)
            {
                poser.transform.position = wP;
                poser.transform.rotation = wR;
                poser.SetActive(true);

                NewNode n = null;

                if(PathFinder.Instance.nodeMap.TryGetValue(CityData.Instance.RealPosToNodeInt(wP), out n))
                {
                    Toolbox.Instance.SetLightLayer(poser, n.building, true);
                }

                //Set material
                //Change material
                if (pr.inheritColouringFromDecor && n != null)
                {
                    if (pr.shareColoursWithFurniture != FurniturePreset.ShareColours.none)
                    {
                        //Find other with this color sharing designation...
                        FurnitureLocation other = n.room.individualFurniture.Find(item => item.furniture.shareColours == pr.shareColoursWithFurniture);

                        if (other != null)
                        {
                            MaterialsController.Instance.ApplyMaterialKey(poser, other.matKey);
                        }
                    }
                    else
                    {
                        MaterialsController.Instance.ApplyMaterialKey(poser, n.room.miscKey);
                    }
                }
                //Apply own colour key
                //else if (pr.useOwnColourSettings)
                //{
                //    Toolbox.Instance.ApplyMaterialKey(poser, mk);
                //}

                //Use book/record setup
                //if (book != null)
                //{
                //    MeshFilter currentFilter = poser.GetComponent<MeshFilter>();

                //    if (currentFilter.sharedMesh != book.bookMesh)
                //    {
                //        currentFilter.sharedMesh = book.bookMesh;

                //        //Make sure collider is the correct size...
                //        poser.GetComponent<BoxCollider>().center = currentFilter.sharedMesh.bounds.center;
                //        poser.GetComponent<BoxCollider>().size = currentFilter.sharedMesh.bounds.size;
                //    }

                //    poser.GetComponent<MeshRenderer>().sharedMaterial = book.bookMaterial;
                //}
            }
        }

        public void Unload()
        {
            if(poser != null)
            {
                poser.SetActive(false);

                if (!SceneRecorder.objectPoserPool.ContainsKey(p))
                {
                    SceneRecorder.objectPoserPool.Add(p, new List<GameObject>());
                }

                SceneRecorder.objectPoserPool[p].Add(poser);
                poser = null;
            }
        }
    }

    [System.Serializable]
    public class InteractableStateCapture
    {
        public int id;
        public bool sw;

        public InteractableStateCapture(Interactable i)
        {
            id = i.id;
            sw = i.sw0;
        }

        public void Load()
        {
            Interactable i = GetInteractable();

            if(i != null)
            {
                if (Player.Instance.computerInteractable != null && Player.Instance.computerInteractable == i) return; //Ignore computer interactable as this could mess things up!

                if (i.sw0 != sw)
                {
                    i.SetSwitchState(sw, null, false, false, true);
                }
            }
        }

        public Interactable GetInteractable()
        {
            Interactable ret = null;

            if(!CityData.Instance.savableInteractableDictionary.TryGetValue(id, out ret))
            {
                
            }

            return ret;
        }
    }

    [System.Serializable]
    public class ActorCapture
    {
        public int id; //Human ID
        public int o; //Outfit
        public Vector3 pos;
        public Vector3 rot;

        //Animation
        public int main;
        public int arms;
        public int sp; //Speed; 0 = stopped, 1 = walking, 2 = running

        //Limbs capture
        public List<LimbCapture> limb = null;

        //Extra
        public HandItemCapture lH; //Left hand object interactable preset
        public HandItemCapture rH; //Right hand object interactable preset

        [System.NonSerialized]
        public ScenePoserController poser;

        public ActorCapture(Human newHuman, bool limbCapture)
        {
            id = newHuman.humanID;
            o = (int)newHuman.outfitController.currentOutfit;

            //pos = newHuman.transform.position;
            //rot = newHuman.transform.eulerAngles;

            //Low precision version
            pos = new Vector3(Toolbox.Instance.RoundToPlaces(newHuman.transform.position.x, 2), Toolbox.Instance.RoundToPlaces(newHuman.transform.position.y, 2), Toolbox.Instance.RoundToPlaces(newHuman.transform.position.z, 2));
            rot = new Vector3(Toolbox.Instance.RoundToPlaces(newHuman.transform.eulerAngles.x, 2), Toolbox.Instance.RoundToPlaces(newHuman.transform.eulerAngles.y, 2), Toolbox.Instance.RoundToPlaces(newHuman.transform.eulerAngles.z, 2));

            if (newHuman.isMoving)
            {
                if (newHuman.isRunning)
                {
                    sp = 2;
                }
                else sp = 1;
            }
            else sp = 0;

            //Get spawned items
            if (newHuman.ai != null)
            {
                if (newHuman.ai.spawnedLeftItem != null)
                {
                    lH = new HandItemCapture(newHuman.ai.spawnedLeftItem, newHuman.ai.spawnedLeftItem.transform.localPosition, newHuman.ai.spawnedLeftItem.transform.localRotation);
                }

                if (newHuman.ai.spawnedRightItem != null)
                {
                    rH = new HandItemCapture(newHuman.ai.spawnedRightItem, newHuman.ai.spawnedRightItem.transform.localPosition, newHuman.ai.spawnedRightItem.transform.localRotation);
                }
            }

            //Always capture limb positions if ragdoll or dead...
            if (newHuman.isDead || newHuman.isStunned)
            {
                limbCapture = true;
            }

            //Capture limb positions
            if (limbCapture)
            {
                limb = new List<LimbCapture>();

                foreach (CitizenOutfitController.AnchorConfig anchor in newHuman.outfitController.anchorConfig)
                {
                    if (!anchor.captureInSurveillance) continue;
                    if (anchor.trans == null) continue;
                    LimbCapture limbCap = new LimbCapture(anchor.anchor, anchor.trans.position, anchor.trans.rotation);
                    limb.Add(limbCap);
                }
            }
            else if (newHuman.animationController != null)
            {
                main = (int)newHuman.animationController.idleAnimationState;
                arms = (int)newHuman.animationController.armsBoolAnimationState;
            }
        }

        public Human GetHuman()
        {
            Human ret = null;
            CityData.Instance.GetHuman(id, out ret);
            return ret;
        }

        public void Load()
        {
            for (int i = 0; i < SceneRecorder.scenePoserPool.Count; i++)
            {
                if(SceneRecorder.scenePoserPool[i] == null)
                {
                    SceneRecorder.scenePoserPool.RemoveAt(i);
                    i--;
                }
            }

            //Get a scene poser
            if (SceneRecorder.scenePoserPool.Count > 0)
            {
                poser = SceneRecorder.scenePoserPool.Find(item => item.human != null && item.human.humanID == id);
                if (poser == null) poser = SceneRecorder.scenePoserPool[0];
                if(poser != null) SceneRecorder.scenePoserPool.Remove(poser);
            }
            else
            {
                //Check for pool limit
                if(SceneRecorder.scenePoserPool.Count > 30)
                {
                    Toolbox.Instance.DestroyObject(SceneRecorder.scenePoserPool[0].gameObject);
                    SceneRecorder.scenePoserPool.RemoveAt(0);
                }

                GameObject newPoser = Toolbox.Instance.SpawnObject(PrefabControls.Instance.scenePoserFigure, PrefabControls.Instance.poserContainer);
                poser = newPoser.GetComponent<ScenePoserController>();
            }

            poser.SetupCitizen(this);
        }

        public void Unload()
        {
            if (poser != null)
            {
                if (!SceneRecorder.scenePoserPool.Contains(poser))
                {
                    SceneRecorder.scenePoserPool.Add(poser);
                }

                poser.gameObject.SetActive(false);
                poser = null;
            }
        }
    }

    [System.Serializable]
    public class LimbCapture : TransformCapture
    {
        public int a; //Anchor

        public LimbCapture(CitizenOutfitController.CharacterAnchor anchor, Vector3 pos, Quaternion rot) : base(pos, rot)
        {
            a = (int)anchor;
        }
    }

    [System.Serializable]
    public class HandItemCapture : TransformCapture
    {
        public string i; //object name

        public HandItemCapture(GameObject obj, Vector3 pos, Quaternion rot) : base(pos, rot)
        {
            i = obj.name;
        }
    }

    public SceneRecorder(Interactable newInteractable)
    {
        interactable = newInteractable; //The camera interactable

        coversRooms.Clear();

        if(interactable.node != null) coversRooms.Add(interactable.node.room);

        if(interactable != Player.Instance.interactable)
        {
            lastCaptureAt = SessionData.Instance.gameTime + Toolbox.Instance.GetPsuedoRandomNumber(0f, GameplayControls.Instance.captureInterval, newInteractable.seed); //Add a random varation of the capture interval so not all camera in the world are capturing at once.

            //Add to behaviour script for live updates
            CitizenBehaviour.Instance.sceneRecorders.Add(this);
        }
    }

    //Needs to be done after culling trees are loaded...
    public void RefreshCoveredArea()
    {
        if(interactable == Player.Instance.interactable) Game.Log("Refreshing covered area of capture for player at " + interactable.cvp);

        //Work out the cameras line of sight/node covered...
        coveredNodes.Clear();
        coveredNodes.Add(interactable.node, null);

        primaryNodes.Clear();
        primaryNodes.Add(interactable.node);

        primaryRooms.Clear();
        primaryRooms.Add(interactable.node.room);

        //Shortlist of other citizens within range
        float halfFOV = GameplayControls.Instance.captureFoV * 0.5f;

        //Game.Log("Scene recorder using culling tree of " + interactable.node.room.cullingTree.Count);

        //Scan culling tree for covered nodes...
        foreach (KeyValuePair<NewRoom, List<NewRoom.CullTreeEntry>> pair in interactable.node.room.cullingTree)
        {
            foreach (NewNode n in pair.Key.nodes)
            {
                if (coveredNodes.ContainsKey(n)) continue;

                //Must be within FoV of this and within range...
                //Is this within my FOV?
                Vector3 targetDir = n.position - interactable.cvp;
                Vector3 cameraForward = Quaternion.Euler(interactable.cve) * Vector3.forward;

                float angleToOther = Vector3.Angle(targetDir, cameraForward); //Local angle

                if (interactable.node == n || angleToOther >= -halfFOV && angleToOther <= halfFOV) //Within fov or on this node
                {
                    //Is this within range?
                    float dist = Vector3.Distance(n.position, interactable.cvp);

                    if (dist <= GameplayControls.Instance.captureRange)
                    {
                        bool addToPrimary = false;

                        if (pair.Value == null || pair.Value.Count <= 0)
                        {
                            coveredNodes.Add(n, null);
                            addToPrimary = true;
                        }
                        else if (pair.Value.Count == 1 && (pair.Value[0].requiredOpenDoors == null || pair.Value[0].requiredOpenDoors.Count <= 0))
                        {
                            coveredNodes.Add(n, null);
                            addToPrimary = true;
                        }
                        else
                        {
                            coveredNodes.Add(n, pair.Value); //Add reference to culling tree entry of required doors...
                        }

                        //Add to covered room list...
                        if (!coversRooms.Contains(n.room))
                        {
                            coversRooms.Add(n.room);
                        }

                        //Check to see if this node is actually visible to the camera...
                        if(addToPrimary)
                        {
                            if(!primaryNodes.Contains(n))
                            {
                                //Do a data raycast with nodes, add the resulting path...
                                List<DataRaycastController.NodeRaycastHit> path = new List<DataRaycastController.NodeRaycastHit>();

                                DataRaycastController.Instance.NodeRaycast(interactable.node, n, out path);

                                //Add all elements on the resulting path...
                                foreach(DataRaycastController.NodeRaycastHit r in path)
                                {
                                    if(r.conditionalDoors == null || r.conditionalDoors.Count <= 0)
                                    {
                                        NewNode foundNode = null;

                                        if(PathFinder.Instance.nodeMap.TryGetValue(r.coord, out foundNode))
                                        {
                                            if(!primaryNodes.Contains(foundNode))
                                            {
                                                primaryNodes.Add(foundNode);
                                            }

                                            if(!primaryRooms.Contains(foundNode.room))
                                            {
                                                primaryRooms.Add(foundNode.room);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //Do a capture of the visible area
    public SceneCapture ExecuteCapture(bool onlyIfMovement, bool detailedCapture = false, bool prepToSaveCapture = true)
    {
        //If the camera is off, don't do anything
        if (!interactable.sw0 && interactable != Player.Instance.interactable)
        {
            return null;
        }

        //Write the time a check was performed...
        lastCaptureAt = SessionData.Instance.gameTime;

        //Only capture if there is movement within the room: IE if someone is in there
        //Make sure there is at least 1 capture though!
        if(onlyIfMovement && interactable.cap.Count > 0)
        {
            //A full capture is needed if this is flagged by an object or there are citizens to capture...
            bool skipCapture = true;

            //Also capture if there were occupants in the previous capture...
            if (interactable.cap[interactable.cap.Count - 1].aCap.Count > 0)
            {
                skipCapture = false;
            }
            else
            {
                foreach (NewRoom r in primaryRooms)
                {
                    //Capture if there are occupants in the primary room...
                    if (r.currentOccupants.Count > 0)
                    {
                        foreach (Human h in r.currentOccupants)
                        {
                            if (primaryNodes.Contains(h.currentNode))
                            {
                                skipCapture = false;
                                break;
                            }
                        }

                        if (!skipCapture) break;
                    }
                }
            }

            if(skipCapture)
            {
                //Game.Log("Skipping capture for " + interactable.id);
                return null;
            }
        }

        //If we reach here, a capture needs to happen...
        SceneCapture newCapture = new SceneCapture(this, detailedCapture);

        //Is there enough room for a new capture? If not then overwrite an old capture...
        if (prepToSaveCapture)
        {
            //Check for captures that need to be removed from the camera's memory (time limit)
            for (int i = 0; i < interactable.cap.Count; i++)
            {
                if (interactable.cap[i].k)
                {
                    continue; //Don't delete this one
                }
                else
                {
                    //Game.Log(interactable.id + ": " + i + " = " + interactable.cap[i].t + " / " + SessionData.Instance.gameTime);

                    if (interactable.cap.Count > 1 && interactable.cap[i].t < (SessionData.Instance.gameTime - GameplayControls.Instance.cameraCaptureMaxTime))
                    {
                        //Game.Log("Removing capture: " + interactable.cap.Count + " from " + interactable.id + "  as it is " + (SessionData.Instance.gameTime - interactable.cap[i].t) + " hours old");
                        interactable.cap.RemoveAt(i);
                        i--;
                    }
                    else if (interactable.cap.Count >= GameplayControls.Instance.cameraCaptureMemory)
                    {
                        //Game.Log("Removing capture: " + interactable.cap.Count + "/" + GameplayControls.Instance.cameraCaptureMemory);
                        interactable.cap.RemoveAt(i);
                        i--;
                    }
                    else break;
                }
            }

            //Now add a capture if there's room...
            if(interactable.cap.Count < GameplayControls.Instance.cameraCaptureMemory)
            {
                interactable.cap.Add(newCapture);
            }
        }

        //Fire event
        if(OnNewCapture != null)
        {
            OnNewCapture();
        }

        return newCapture;
    }
}
