﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;

public class JuiceController : MonoBehaviour
{
    [System.Serializable]
    public class JuiceElement
    {
        public RectTransform transformElement;
        public Image imageElement;
        public RawImage rawImageElement;
        public CanvasRenderer renderer;
        public Color originalColour = Color.white;
        [Tooltip("Get the original colours of images and raw images at the start")]
        public bool getNormalColourAtStart = false;
        public Vector3 originalLocalPos;
        public Vector3 originalLocalRot;
        public Vector3 originalLocalScale;
        public bool getNormalTransformAtStart = false;
    }

    [Header("Elements")]
    [ReorderableList]
    public List<JuiceElement> elements = new List<JuiceElement>();

    [Header("On Start")]
    public bool pulsateActive = false;
    public bool pulsateScale = false;
    public float pulsateProgress = 0f;
    public bool pulsateOnStart = false;
    public Color pulsateColour = Color.white;
    public float pulsateSpeed = 5f;

    //Flash
    private bool flashActive = false;
    private float flashSpeed = 10f;
    public Color flashColour;
    private int cycle = 0;
    private float flashProgress = 0f;
    private float flashF = 0f;
    private int flashRepeat = 3;

    //Pulsate
    private bool onOff = false;
    public bool smoothPulsateOff = false;

    //Nudge
    private bool nudgeActive = false;
    private bool nudgeState = false;
    private float nudgeProgress = 0f;
    private float amountToScale;
    private Vector3 desiredScale;
    //This can be + or -
    private float amountToRotate;
    private bool nudgeEffectScale = true;
    private bool nudgeEffectRotation = true;

    //Appear
    public bool fancyAppearActive = false;
    public float appearSpeed = 2f;
    private float fancyAppearProgress = 0f;

    //Disappear
    public bool fancyDisappearActive = false;
    public float disappearSpeed = 2f;
    private float fancyDisappearProgress = 0f;

    //Use this for initialization
    void Start()
    {
        //Get colours, pos @ start
        foreach(JuiceElement j in elements)
        {
            if(j.getNormalColourAtStart)
            {
                if(j.imageElement != null)
                {
                    j.originalColour = j.imageElement.color;
                }
                else if(j.rawImageElement != null)
                {
                    j.originalColour = j.rawImageElement.color;
                }
            }

            if(j.getNormalTransformAtStart)
            {
                if (j.transformElement != null)
                {
                    j.originalLocalPos = j.transformElement.localPosition;
                    j.originalLocalRot = j.transformElement.localEulerAngles;
                    j.originalLocalScale = j.transformElement.localScale;
                }
            }
        }

        if(pulsateOnStart)
        {
            Pulsate(true);
        }
    }

    //Get rect
    public void GetOriginalRectSize()
    {
        foreach (JuiceElement j in elements)
        {
            if (j.transformElement != null)
            {
                j.originalLocalPos = j.transformElement.localPosition;
                j.originalLocalRot = j.transformElement.localEulerAngles;
                j.originalLocalScale = j.transformElement.localScale;
            }
        }
    }

    //Use update for pulsate so it can be toggled when inactive
    private void Update()
    {
        if (pulsateActive)
        {
            if (!onOff && pulsateProgress < 1f)
            {
                pulsateProgress += pulsateSpeed * Time.deltaTime;
            }
            else if (onOff && pulsateProgress > 0f)
            {
                pulsateProgress -= pulsateSpeed * Time.deltaTime;
            }

            //Switch
            if (pulsateProgress >= 1f && !onOff) onOff = true;
            else if (pulsateProgress <= 0f && onOff) onOff = false;

            //Set colour
            for (int i = 0; i < elements.Count; ++i)
            {
                JuiceElement j = elements[i];

                if (j.imageElement != null)
                {
                    j.imageElement.color = Color.Lerp(j.originalColour, pulsateColour, pulsateProgress);
                }

                if (j.rawImageElement != null)
                {
                    j.rawImageElement.color = Color.Lerp(j.originalColour, pulsateColour, pulsateProgress);
                }

                if(pulsateScale && j.transformElement != null)
                {
                    float sc = Mathf.Lerp(1f, 1.5f, pulsateProgress);
                    j.transformElement.localScale = new Vector3(sc, sc, sc);
                }
            }

            if (smoothPulsateOff && pulsateProgress <= 0f)
            {
                smoothPulsateOff = false;
                pulsateActive = false;
            }
        }
        else if (flashActive)
        {
            if (cycle < flashRepeat && flashProgress < 2f)
            {
                flashProgress += flashSpeed * Time.deltaTime;

                if (flashProgress <= 1f) flashF = flashProgress;
                else flashF = 2f - flashProgress;

                for (int i = 0; i < elements.Count; ++i)
                {
                    JuiceElement j = elements[i];

                    if (j.imageElement != null)
                    {
                        j.imageElement.color = Color.Lerp(j.originalColour, flashColour, flashF);
                    }

                    if (j.rawImageElement != null)
                    {
                        j.rawImageElement.color = Color.Lerp(j.originalColour, flashColour, flashF);
                    }
                }

                if (flashProgress >= 2f)
                {
                    cycle++;
                    flashProgress = 0f;
                }
            }
            else
            {
                flashActive = false;
            }
        }

        if (nudgeActive)
        {
            if (!nudgeState)
            {
                nudgeProgress += 10f * Time.deltaTime;
                nudgeProgress = Mathf.Clamp01(nudgeProgress);

                if (nudgeProgress >= 1f)
                {
                    nudgeState = true;
                }
            }
            else
            {
                nudgeProgress -= 5f * Time.deltaTime;
                nudgeProgress = Mathf.Clamp01(nudgeProgress);

                if (nudgeProgress <= 0f)
                {
                    nudgeActive = false;
                }
            }

            for (int i = 0; i < elements.Count; ++i)
            {
                JuiceElement j = elements[i];

                if (j.transformElement != null)
                {
                    if (nudgeEffectScale)
                    {
                        j.transformElement.localScale = Vector3.Lerp(j.originalLocalScale, desiredScale, nudgeProgress);
                    }

                    if (nudgeEffectRotation)
                    {
                        float currentRot = Mathf.SmoothStep(j.originalLocalRot.z, j.originalLocalRot.z + amountToRotate, nudgeProgress);
                        j.transformElement.localEulerAngles = new Vector3(j.transformElement.localEulerAngles.x, j.transformElement.localEulerAngles.y, currentRot);
                    }
                }
            }
        }

        //Fancy appear
        if(fancyAppearActive)
        {
            fancyAppearProgress += Time.deltaTime * appearSpeed;
            fancyAppearProgress = Mathf.Clamp01(fancyAppearProgress);

            //Fade in
            foreach (JuiceElement el in elements)
            {
                if (el.imageElement != null)
                {
                    el.imageElement.canvasRenderer.SetAlpha(fancyAppearProgress);
                }

                if (el.rawImageElement != null)
                {
                    el.rawImageElement.canvasRenderer.SetAlpha(fancyAppearProgress);
                }

                if (el.renderer != null)
                {
                    el.renderer.SetAlpha(fancyAppearProgress);
                }
            }

            if(fancyAppearProgress >= 1f)
            {
                fancyAppearActive = false;
            }
        }

        //Fancy disappear
        if (fancyDisappearActive)
        {
            fancyDisappearProgress += Time.deltaTime * disappearSpeed;
            fancyDisappearProgress = Mathf.Clamp01(fancyDisappearProgress);

            //Fade in
            foreach (JuiceElement el in elements)
            {
                if (el.imageElement != null)
                {
                    el.imageElement.canvasRenderer.SetAlpha(fancyDisappearProgress);
                }

                if (el.rawImageElement != null)
                {
                    el.rawImageElement.canvasRenderer.SetAlpha(fancyDisappearProgress);
                }

                if (el.renderer != null)
                {
                    el.renderer.SetAlpha(fancyDisappearProgress);
                }
            }

            if (fancyDisappearProgress >= 1f)
            {
                fancyDisappearActive = false;
                this.gameObject.SetActive(false);
            }
        }

        //Else turn off
        if (!flashActive && !pulsateActive && !nudgeActive && !fancyAppearActive && !fancyDisappearActive)
        {
            this.enabled = false;
        }
    }

    //Flash the image with this colour (multiply) and repeat at this speed
    public void Flash(int newRepeat, bool colourOverride, Color colour= new Color(), float speed = 10f)
    {
        if(colourOverride)
        {
            flashColour = colour;
        }

        //Game.Log("Flash");
        flashSpeed = speed;
        flashRepeat = newRepeat;

        //If already active, add repeats to current coroutine
        if (flashActive)
        {
            flashRepeat += flashRepeat;
        }
        else
        {
            flashActive = true;
            cycle = 0;
            flashProgress = 0f;
            flashF = 0f;
            this.enabled = true;
        }
    }

    //Pulsate a colour and back to original
    public void Pulsate(bool toggle, bool smoothOff = false)
    {
        if(toggle)
        {
            smoothOff = false;
            smoothPulsateOff = false;
        }

        if (toggle && !pulsateActive)
        {
            this.enabled = true;
            pulsateProgress = 0f;
        }

        //Game.Log("Pulsate " + toggle);
        pulsateActive = toggle;

        //Activate smooth off
        if (smoothOff && !toggle)
        {
            this.enabled = true;
            smoothPulsateOff = true;
            pulsateActive = true;
        }

        if(!smoothOff && !toggle)
        {
            //Set colour
            for (int i = 0; i < elements.Count; ++i)
            {
                JuiceElement j = elements[i];

                if (j.imageElement != null)
                {
                    j.imageElement.color = j.originalColour;
                }

                if (j.rawImageElement != null)
                {
                    j.rawImageElement.color = j.originalColour;
                }
            }
        }
    }

    //Scale and resize a transform briefly before returning to original
    public void Nudge(Vector2 scaleRange, Vector2 rotationRange, bool updateOriginalPositionFirst = true, bool affectScale = true, bool affectRotation = true)
    {
        if(!nudgeActive)
        {
            //Game.Log("Nudge");

            if(updateOriginalPositionFirst)
            {
                foreach (JuiceElement j in elements)
                {
                    if (j.transformElement != null)
                    {
                        j.originalLocalPos = j.transformElement.localPosition;
                        j.originalLocalRot = j.transformElement.localEulerAngles;
                        j.originalLocalScale = j.transformElement.localScale;
                    }
                }
            }


            amountToScale = Toolbox.Instance.Rand(scaleRange.x, scaleRange.y);
            desiredScale = new Vector3(amountToScale, amountToScale, amountToScale);
            //This can be + or -
            amountToRotate = Toolbox.Instance.Rand(rotationRange.x, rotationRange.y);

            nudgeProgress = 0f;
            nudgeActive = true;
            nudgeState = false;
            nudgeEffectScale = affectScale;
            nudgeEffectRotation = affectRotation;

            this.enabled = true;
        }
    }

    //Fancy activate
    public void FancyAppear(float newAppearSpeed = 2f)
    {
        fancyAppearActive = true;
        fancyDisappearActive = false;
        fancyDisappearProgress = 0f;
        fancyAppearProgress = 0f;
        appearSpeed = newAppearSpeed;

        foreach(JuiceElement el in elements)
        {
            if (el.imageElement != null)
            {
                el.imageElement.canvasRenderer.SetAlpha(0f);
            }

            if(el.rawImageElement != null)
            {
                el.rawImageElement.canvasRenderer.SetAlpha(0f);
            }
        }

        Nudge(new Vector2(1.5f, 1.5f), Vector2.zero, true, true, false);

        this.gameObject.SetActive(true);
        this.enabled = true;
    }

    //Fancy disappear
    public void FancyDisappear(float newDisappearSpeed = 2f)
    {
        fancyDisappearActive = true;
        fancyAppearActive = false;
        fancyAppearProgress = 0f;
        fancyDisappearProgress = 0f;
        disappearSpeed = newDisappearSpeed;

        this.gameObject.SetActive(true);
        this.enabled = true;
    }

    private void OnDisable()
    {
        //Cancel flash & nudge
        flashActive = false;
        nudgeActive = false;

        //Return to original colors
        for (int i = 0; i < elements.Count; ++i)
        {
            JuiceElement j = elements[i];

            if(j.originalLocalScale.x > 0f)
            {
                if (j.imageElement != null)
                {
                    j.imageElement.color = j.originalColour;
                }

                if (j.rawImageElement != null)
                {
                    j.rawImageElement.color = j.originalColour;
                }

                if (j.transformElement != null)
                {
                    j.transformElement.localScale = j.originalLocalScale;
                    j.transformElement.localEulerAngles = j.originalLocalRot;
                    j.transformElement.localPosition = j.originalLocalPos;
                }
            }
        }
    }

    //Buttons
    [Button]
    public void Flash()
    {
        Flash(3, false, speed: 10f);
    }

    [Button]
    public void PulsateToggle()
    {
        if(pulsateActive)
        {
            Pulsate(false);
        }
        else
        {
            Pulsate(true);
        }
    }

    [Button]
    public void Nudge()
    {
        Nudge(new Vector2(1.2f, 1.3f), new Vector2(0, 0));
    }

    [Button]
    public void Appear()
    {
        FancyAppear();
    }

    [Button]
    public void Disappear()
    {
        FancyDisappear();
    }
}
