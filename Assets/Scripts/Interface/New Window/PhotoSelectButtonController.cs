﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PhotoSelectButtonController : ButtonController
{
    public Human citizen;
    public Case.CaseElement element;
    public RawImage photo;
    public InfoWindow thisWindow;

    public void Setup(Human newCitizen, Case.CaseElement newCaseElement, InfoWindow newThisWindow)
    {
        base.SetupReferences();

        citizen = newCitizen;
        element = newCaseElement;
        thisWindow = newThisWindow;

        UpdateButtonText();
    }

    public override void UpdateButtonText()
    {
        //Set name
        if (citizen != null)
        {
            photo.texture = citizen.evidenceEntry.GetPhoto(element.dk);
            text.text = citizen.evidenceEntry.GetNameForDataKey(element.dk);
        }
    }

    public override void OnLeftClick()
    {
        if(InteractionController.Instance.talkingTo != null && InteractionController.Instance.talkingTo.isActor != null)
        {
            DialogController.Instance.askTarget = citizen;
            DialogController.Instance.askTargetKeys = citizen.evidenceEntry.GetTiedKeys(element.dk);

            Game.Log("Ask target: " + citizen.name);
            Game.Log("Ask target keys: " + DialogController.Instance.askTargetKeys.Count);

            //Trigger response...
            Human human = InteractionController.Instance.talkingTo.isActor as Human;
            Acquaintance aq = null;

            //We have a solid enough starting point to get the full info...
            if((DialogController.Instance.askTargetKeys.Contains(Evidence.DataKey.name) || DialogController.Instance.askTargetKeys.Contains(Evidence.DataKey.photo)) && (human.FindAcquaintanceExists(citizen, out aq) || citizen == human))
            {
                //That's me!
                if(citizen == human)
                {
                    human.speechController.Speak("4e8db6e9-04fe-4c5b-9bf6-05d8c3f8a230", speakAbout: citizen);
                    MergeTargetKeys(Evidence.DataKey.name);
                    MergeTargetKeys(Evidence.DataKey.photo);
                    MergeTargetKeys(Evidence.DataKey.voice);
                }
                else if (aq != null)
                {
                    if(aq.connections.Contains(Acquaintance.ConnectionType.lover))
                    {
                        human.speechController.Speak("694fc0ab-2b16-43ba-9ce0-8954a27bb652", speakAbout: citizen);
                        MergeTargetKeys(Evidence.DataKey.name);
                    }
                    else if(aq.connections.Contains(Acquaintance.ConnectionType.friend))
                    {
                        human.speechController.Speak("8b710c17-57f2-4157-b02f-2be3bbfde699", speakAbout: citizen);
                        MergeTargetKeys(Evidence.DataKey.name);
                    }
                    else if (aq.connections.Contains(Acquaintance.ConnectionType.neighbor))
                    {
                        human.speechController.Speak("bbc084dc-d268-4bc4-bd9f-9a8ee9824a06", speakAbout: citizen);
                        MergeTargetKeys(Evidence.DataKey.name);
                    }
                    else if((aq.connections.Contains(Acquaintance.ConnectionType.workOther) || aq.connections.Contains(Acquaintance.ConnectionType.workTeam) || aq.connections.Contains(Acquaintance.ConnectionType.familiarWork) || aq.connections.Contains(Acquaintance.ConnectionType.boss)) && citizen.job != null && citizen.job.employer != null)
                    {
                        human.speechController.Speak("84ef41ab-03fe-4888-a8fe-b4c71486dca4", speakAbout: citizen);
                        MergeTargetKeys(Evidence.DataKey.name);
                    }
                    else
                    {
                        human.speechController.Speak("0e443d05-2bd7-4a52-adcb-5957a5d82860", speakAbout: citizen);
                        MergeTargetKeys(Evidence.DataKey.name);
                    }

                    //Reveal a piece of information that is always the same every time
                    float reveal = Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, human.citizenName + citizen.citizenName);

                    if(reveal <= 0.33f && citizen.characterTraits.Exists(item => item.trait.featureInAfflictionPool))
                    {
                        human.speechController.Speak("f98fdfc4-88ff-46bb-80eb-115995da7b73", speakAbout: citizen);
                        MergeTargetKeys(Evidence.DataKey.randomAffliction);
                    }
                    else if (reveal > 0.33f && reveal <= 0.66f && citizen.characterTraits.Exists(item => item.trait.featureInInterestPool))
                    {
                        human.speechController.Speak("7448c520-bf46-4da7-95cd-bbd9c20eb9ac", speakAbout: citizen);
                        MergeTargetKeys(Evidence.DataKey.randomInterest);
                    }
                    else if(citizen.job != null && citizen.job.employer != null)
                    {
                        human.speechController.Speak("314ca1e3-cd3f-4773-b32e-ec39c2ec3194", speakAbout: citizen);
                        MergeTargetKeys(Evidence.DataKey.work);
                    }

                    //"They live with me"
                    if(citizen.home == human.home)
                    {
                        human.speechController.Speak("3a98db60-c57f-4d6b-b2b3-bfb19d2da5c0", speakAbout: citizen);

                        //Merge address keys if I know them
                        List<Evidence.DataKey> addKeys = human.evidenceEntry.GetTiedKeys(Evidence.DataKey.address);

                        foreach(Evidence.DataKey k in human.evidenceEntry.GetTiedKeys(Evidence.DataKey.voice))
                        {
                            if(human.evidenceEntry.GetTiedKeys(k).Contains(Evidence.DataKey.address))
                            {
                                MergeTargetKeys(Evidence.DataKey.address);
                                break;
                            }
                        }
                    }

                    //Reveal immediate location
                    if(aq.known >= SocialControls.Instance.knowImmediateLocationThreshold && !citizen.isDead && citizen.currentGameLocation.thisAsAddress != null && citizen.currentGameLocation.thisAsAddress.company != null)
                    {
                        human.speechController.Speak("d91b9493-a847-4074-833c-900e445095de", speakAbout: citizen);
                    }
                    else human.RevealSighting(citizen, true, false);
                }
            }
            else
            {
                bool speech = false;

                //Given photo
                if (DialogController.Instance.askTargetKeys.Contains(Evidence.DataKey.photo))
                {
                    human.RevealSighting(citizen, false, false);
                    speech = true;
                }
                //Given name "This could be..."
                else if (DialogController.Instance.askTargetKeys.Contains(Evidence.DataKey.name))
                {
                    foreach (Acquaintance aqu in human.acquaintances)
                    {
                        Human other = aqu.GetOther(human);

                        if (other.GetCitizenName() == citizen.GetCitizenName())
                        {
                            //human.speechController.Speak("ed7d288d-9044-4979-a705-bf6cb583c652", speakAbout: other);
                            human.RevealSighting(other, true, false);
                            speech = true;
                            break;
                        }
                    }
                }
                //Given first name only "This could be..."
                else if (DialogController.Instance.askTargetKeys.Contains(Evidence.DataKey.firstName))
                {
                    foreach (Acquaintance aqu in human.acquaintances)
                    {
                        Human other = aqu.GetOther(human);

                        if (other.GetFirstName() == citizen.GetFirstName())
                        {
                            human.speechController.Speak("ed7d288d-9044-4979-a705-bf6cb583c652", speakAbout: other);
                            human.RevealSighting(other, true, false);
                            speech = true;
                            break;
                        }
                    }
                }
                //Given surname only "This could be..."
                else if (DialogController.Instance.askTargetKeys.Contains(Evidence.DataKey.surname))
                {
                    foreach (Acquaintance aqu in human.acquaintances)
                    {
                        Human other = aqu.GetOther(human);

                        if (other.GetSurName() == citizen.GetSurName())
                        {
                            human.speechController.Speak("6bb725e2-f98d-42b9-ae77-b2f9ecfe70fd", speakAbout: other);
                            human.RevealSighting(other, true, false);
                            speech = true;
                            break;
                        }
                    }
                }
                //Given initials only "This could be..."
                else if (DialogController.Instance.askTargetKeys.Contains(Evidence.DataKey.initials))
                {
                    foreach (Acquaintance aqu in human.acquaintances)
                    {
                        Human other = aqu.GetOther(human);

                        if(other.GetInitials() == citizen.GetInitials())
                        {
                            human.speechController.Speak("6fedb3fb-31ba-4d15-a3c5-2f60f53eb652", speakAbout: other);
                            human.RevealSighting(other, true, false);
                            speech = true;
                            break;
                        }
                    }
                }

                //Not enough to go on
                if (!speech)
                {
                    human.speechController.Speak("a6815309-f9d4-40b0-8a1e-3ec3550c64a2", speakAbout: citizen);
                    speech = true;
                }
            }
        }

        thisWindow.CloseWindow();

        base.OnLeftClick();
    }

    private void MergeTargetKeys(Evidence.DataKey key)
    {
        foreach(Evidence.DataKey k in DialogController.Instance.askTargetKeys)
        {
            citizen.evidenceEntry.MergeDataKeys(k, key);
        }
    }

    //Reveal a piece of information about the citizen
    //private void GenerateBreadcrumb()
    //{
    //    Human human = InteractionController.Instance.talkingTo.isActor as Human;
    //    Acquaintance aq = null;

    //    Human prospectCitizen = null;

    //    //First name
    //    if (DialogController.Instance.askTargetKeys.Contains(Evidence.DataKey.firstName))
    //    {
    //        List<Acquaintance> prospects = human.acquaintances.FindAll(item => item.with.firstName.ToLower() == citizen.firstName.ToLower());

    //        if (prospects.Count > 0)
    //        {
    //            aq = prospects[Toolbox.Instance.GetPsuedoRandomNumber(0, prospects.Count, human.name)];
    //            prospectCitizen = aq.with;

    //            //Gather list of things to say from this message
    //            List<string> say2 = human.ParseDDSMessage("43140b5e-69d3-4435-92b3-3f8e365dcb46", aq);

    //            //We can add all these to the citizen's speak queue at once
    //            foreach (string str in say2)
    //            {
    //                human.speechController.Speak("dds.blocks", str, true, speakingAbout: prospectCitizen);
    //            }

    //            RevealWorkOrHome(prospectCitizen, Evidence.DataKey.firstName);
    //            return;
    //        }
    //    }

    //    //Surname
    //    if (DialogController.Instance.askTargetKeys.Contains(Evidence.DataKey.surname))
    //    {
    //        List<Acquaintance> prospects = human.acquaintances.FindAll(item => item.with.surName.ToLower() == citizen.surName.ToLower());

    //        if (prospects.Count > 0)
    //        {
    //            aq = prospects[Toolbox.Instance.GetPsuedoRandomNumber(0, prospects.Count, human.name)];
    //            prospectCitizen = aq.with;

    //            //Gather list of things to say from this message
    //            List<string> say2 = human.ParseDDSMessage("3c264677-5d5a-4edf-8179-218ce342b345", aq);

    //            //We can add all these to the citizen's speak queue at once
    //            foreach (string str in say2)
    //            {
    //                human.speechController.Speak("dds.blocks", str, true, speakingAbout: prospectCitizen);
    //            }

    //            RevealWorkOrHome(prospectCitizen, Evidence.DataKey.surname);
    //            return;
    //        }
    //    }

    //    //Initialed name
    //    if (DialogController.Instance.askTargetKeys.Contains(Evidence.DataKey.initialedName))
    //    {
    //        List<Acquaintance> prospects = human.acquaintances.FindAll(item => item.with.initialledName.ToLower() == citizen.initialledName.ToLower());

    //        if (prospects.Count > 0)
    //        {
    //            aq = prospects[Toolbox.Instance.GetPsuedoRandomNumber(0, prospects.Count, human.name)];
    //            prospectCitizen = aq.with;

    //            //Gather list of things to say from this message
    //            List<string> say2 = human.ParseDDSMessage("708689f9-01fa-45f6-a6e5-4663da548053", aq);

    //            //We can add all these to the citizen's speak queue at once
    //            foreach (string str in say2)
    //            {
    //                human.speechController.Speak("dds.blocks", str, true, speakingAbout: prospectCitizen);
    //            }

    //            RevealWorkOrHome(prospectCitizen, Evidence.DataKey.initialedName);
    //            return;
    //        }
    //    }

    //    //Initials
    //    if (DialogController.Instance.askTargetKeys.Contains(Evidence.DataKey.initials))
    //    {
    //        List<Acquaintance> prospects = human.acquaintances.FindAll(item => item.with.initialledName.Substring(0, 1).ToLower() == citizen.initialledName.Substring(0, 1).ToLower());

    //        if (prospects.Count > 0)
    //        {
    //            aq = prospects[Toolbox.Instance.GetPsuedoRandomNumber(0, prospects.Count, human.name)];
    //            prospectCitizen = aq.with;

    //            //Gather list of things to say from this message
    //            List<string> say2 = human.ParseDDSMessage("708689f9-01fa-45f6-a6e5-4663da548053", aq);

    //            //We can add all these to the citizen's speak queue at once
    //            foreach (string str in say2)
    //            {
    //                human.speechController.Speak("dds.blocks", str, true, speakingAbout: prospectCitizen);
    //            }

    //            RevealWorkOrHome(prospectCitizen, Evidence.DataKey.initials);
    //            return;
    //        }
    //    }

    //    //Photo
    //    if(DialogController.Instance.askTargetKeys.Contains(Evidence.DataKey.photo))
    //    {
    //        RevealSighting(citizen);
    //        return;
    //    }

    //    //Offer a sighting with ever-decreasing likihood
    //    List<Evidence.DataKey> visualKeys = new List<Evidence.DataKey>();
    //    visualKeys.Add(Evidence.DataKey.age);
    //    visualKeys.Add(Evidence.DataKey.hair);
    //    visualKeys.Add(Evidence.DataKey.build);
    //    visualKeys.Add(Evidence.DataKey.eyes);
    //    visualKeys.Add(Evidence.DataKey.height);
    //    visualKeys.Add(Evidence.DataKey.shoeSize);
    //    visualKeys.Add(Evidence.DataKey.facialHair);
    //    visualKeys.Add(Evidence.DataKey.glasses);

    //    List<Evidence.DataKey> includedKeys = DialogController.Instance.askTargetKeys.FindAll(item => visualKeys.Contains(item));

    //    float chance = (float)includedKeys.Count / (float)visualKeys.Count;

    //    if(Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, human.name) <= chance)
    //    {
    //        RevealSighting(citizen);
    //        return;
    //    }
    //}

    //private void RevealWorkOrHome(Human prospectCitizen, Evidence.DataKey givenKey)
    //{
    //    Human human = InteractionController.Instance.talkingTo.isActor as Human;

    //    if (Toolbox.Instance.GetPsuedoRandomNumber(0, 1f, human.name) > 0.5f && prospectCitizen.job != null && prospectCitizen.job.employer != null)
    //    {
    //        //Gather list of things to say from this message
    //        List<string> say2 = human.ParseDDSMessage("d411568b-85c5-41d1-a888-02d6a2cf8176", null);

    //        //We can add all these to the citizen's speak queue at once
    //        foreach (string str in say2)
    //        {
    //            human.speechController.Speak("dds.blocks", str, true, speakingAbout: prospectCitizen);
    //        }

    //        prospectCitizen.evidenceEntry.MergeDataKeys(givenKey, Evidence.DataKey.work);
    //    }
    //    else if(prospectCitizen.home != null)
    //    {
    //        //Gather list of things to say from this message
    //        List<string> say2 = human.ParseDDSMessage("ccbd5664-e6d4-46c3-8750-d8cd17dc260a", null);

    //        //We can add all these to the citizen's speak queue at once
    //        foreach (string str in say2)
    //        {
    //            human.speechController.Speak("dds.blocks", str, true, speakingAbout: prospectCitizen);
    //        }

    //        prospectCitizen.evidenceEntry.MergeDataKeys(givenKey, Evidence.DataKey.livesInBuilding);
    //        prospectCitizen.evidenceEntry.MergeDataKeys(givenKey, Evidence.DataKey.livesOnFloor);
    //    }

    //    InterfaceController.Instance.SpawnWindow(prospectCitizen.evidenceEntry, givenKey);
    //}
}
