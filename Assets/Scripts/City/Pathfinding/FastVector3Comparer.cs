﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class FastVector3Comparer : IEqualityComparer<Vector3>
{
    bool IEqualityComparer<Vector3>.Equals(Vector3 x, Vector3 y)
    {
        // Normally this would be a terrible way to compare vectors but in this case we're actually looking for a fast exact match within a collection
        return x.GetHashCode() == y.GetHashCode();
    }

    int IEqualityComparer<Vector3>.GetHashCode(Vector3 obj)
    {
        HashCode hashCode = new HashCode();
        hashCode.Add(obj.x);
        hashCode.Add(obj.y);
        hashCode.Add(obj.z);
        return hashCode.ToHashCode();
    }

    private static FastVector3Comparer sharedFastVector3Comparer;

    public static FastVector3Comparer SharedFastVector3Comparer
    {
        get
        {
            if (sharedFastVector3Comparer == null)
            {
                sharedFastVector3Comparer = new FastVector3Comparer();
            }
            return sharedFastVector3Comparer;
        }
    }
}
