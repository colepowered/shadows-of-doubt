﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AttentionMarkerController : MonoBehaviour
{
	public TextMeshProUGUI tmp;

	void Start()
	{
		tmp = this.gameObject.GetComponentInChildren<TextMeshProUGUI>();
	}

	public void SetText(string newText)
	{
		if(tmp == null) tmp = this.gameObject.GetComponentInChildren<TextMeshProUGUI>();
		tmp.text = newText;
	}
}
