﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CallLogsContentController : MonoBehaviour
{
    public WindowContentController windowContent;
    public InfoWindow parentWindow;
    public TextMeshProUGUI descriptionText;
    public GameObject entryPrefab;
    public bool incoming = false; //Incoming/outgoing toggle
    public TextMeshProUGUI titleText;
    public List<CallLogsEntryController> spawnedEntries = new List<CallLogsEntryController>();
    public VerticalLayoutGroup layout;

    private void OnEnable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
        if (windowContent == null) windowContent = this.gameObject.GetComponentInParent<WindowContentController>();

        CheckEnabled();
    }

    private void OnDisable()
    {
        if (parentWindow == null) parentWindow = this.gameObject.GetComponentInParent<InfoWindow>();
    }

    public void CheckEnabled()
    {
        NewBuilding building = parentWindow.passedEvidence.interactable.node.building;

        if (incoming)
        {
            titleText.text = Strings.Get("ui.interface", "Incoming calls from") + " " + building.name;
        }
        else
        {
            titleText.text = Strings.Get("ui.interface", "Outgoing calls from") + " " + building.name;
        }

        List<TelephoneController.PhoneCall> logPool = null;

        if(incoming)
        {
            logPool = building.callLog.FindAll(item => item.toNS != null && item.toNS.interactable != null && item.toNS.interactable.node != null & item.toNS.interactable.node.gameLocation.thisAsAddress != null && item.toNS.interactable.node.building == building);
        }
        else
        {
            logPool = building.callLog.FindAll(item => item.fromNS != null && item.fromNS.interactable != null && item.fromNS.interactable.node != null & item.fromNS.interactable.node.gameLocation.thisAsAddress != null && item.fromNS.interactable.node.building == building);
        }

        //Spawn entries
        while (spawnedEntries.Count > 0)
        {
            Destroy(spawnedEntries[0].gameObject);
            spawnedEntries.RemoveAt(0);
        }

        float yPos = 0;

        for (int i = 0; i < logPool.Count; i++)
        {
            TelephoneController.PhoneCall temp = logPool[i];

            GameObject newT = Instantiate(entryPrefab, this.transform);
            CallLogsEntryController bs = newT.GetComponent<CallLogsEntryController>();

            bs.Setup(temp, building);

            //bs.rect.anchoredPosition = new Vector2(bs.rect.anchoredPosition.x, yPos);
            yPos += bs.rect.sizeDelta.y + layout.spacing;

            spawnedEntries.Add(bs);
        }

        windowContent.pageRect.sizeDelta = new Vector2(windowContent.pageRect.sizeDelta.x, Mathf.Max(windowContent.pageRect.sizeDelta.y, yPos + 38));
        windowContent.rect.sizeDelta = new Vector2(windowContent.pageRect.sizeDelta.x, Mathf.Max(windowContent.pageRect.sizeDelta.y, yPos + 100));

        //layout.CalculateLayoutInputVertical();
        //windowContent.pageRect.sizeDelta = new Vector2(windowContent.pageRect.sizeDelta.x, Mathf.Max(windowContent.pageRect.sizeDelta.y, layout.preferredHeight));
        //windowContent.rect.sizeDelta = new Vector2(windowContent.pageRect.sizeDelta.x, Mathf.Max(windowContent.pageRect.sizeDelta.y, layout.preferredHeight + 64));
    }
}
