﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;
using UnityEngine.Rendering;
using System;

public class ObjectPoolingController : MonoBehaviour
{
    [Header("Settings")]
    [Tooltip("Spawn object gradually instead of all at once")]
    public bool useGradualSpawning = true;
    [EnableIf("useGradualSpawning")]
    public int loadNewObjectPerFrame = 1;
    [EnableIf("useGradualSpawning")]
    public int loadPooledObjectPerFrame = 5;
    [Tooltip("Store uneeded objects in a list ready to be re-used")]
    public bool usePooling = true;
    [Tooltip("The maximum number of unrendered but loaded interactables to keep in the object pool.")]
    [EnableIf("usePooling")]
    public int maximumInteractablePoolCache = 1000;
    [Tooltip("Use range for loading in objects")]
    public bool useRange = true;
    [ReorderableList]
    public List<ObjectLoadRangeConfig> loadRangeConfig = new List<ObjectLoadRangeConfig>();
    [EnableIf("useRange")]
    public int maximumRangeCheckingPerFrame = 50;
    [Tooltip("Maximum number of rooms that are loaded at once before oldest is unloaded")]
    public int maxRoomCache = 100;

    public enum ObjectLoadRange { veryClose, close, medium, far, veryFar, maximum};

    [System.Serializable]
    public class ObjectLoadRangeConfig
    {
        public ObjectLoadRange range;
        public float loadDistance;
    }

    [Header("Stats")]
    [ReadOnly]
    public int interactablesLoaded = 0;

    [ReadOnly]
    public int interactablesToLoadCount = 0;
    [ReadOnly]
    public int interactablesRangeCount = 0;
    [NonSerialized]
    public float updateObjectRangesTimer = 0;

    [Space(7)]
    [ReadOnly]
    public int furntiureCheckCount = 0;
    [ReadOnly]
    public int furnitureToLoadCount = 0;
    [ReadOnly]
    public int furnitureRangeCount = 0;

    [Space(7)]

    [ReadOnly]
    public int interactablesCurrentlyPooled = 0;
    [ReadOnly]
    public int interactableInstancesSaved = 0;
    [ReadOnly]
    public float interactableFullPercentage = 0;

    [Space(7)]
    [ReadOnly]
    public int roomsLoaded = 0;


    //Quick reference for object load ranges
    public Dictionary<int, float> loadRanges = new Dictionary<int, float>();

    //Object range list: These are active objects but waiting to be loaded based on player range
    public List<Interactable> interactableRangeToLoadList = new List<Interactable>();

    //These are spawned objects but we need to keep them to know when to disable the mesh depending on range
    [NonSerialized]
    public List<Interactable> interactableRangeToEnableDisableList = new List<Interactable>();

    //Object loading list: These will be loaded over time if gradual spawning is enabled
    public HashSet<Interactable> interactableLoadList = new HashSet<Interactable>();

    //Object range list: These are active objects but waiting to be loaded based on player range
    private List<FurnitureLocation> furnitureRangeToLoadList = new List<FurnitureLocation>();

    //These are spawned objects but we need to keep them to know when to disable the mesh depending on range
    [NonSerialized]
    public HashSet<FurnitureLocation> furnitureRangeToEnableDisableList = new HashSet<FurnitureLocation>();

    //Use prefabs as pool keys
    public Dictionary<InteractablePreset, HashSet<Interactable>> interactablePool = new Dictionary<InteractablePreset, HashSet<Interactable>>(); //All objects that are not being rendered. Last on the list is the most recently discarded.

    //Use this as a loaded objects list/reference for the entire pool, delete the oldest entries once full
    private HashSet<Interactable> entirePoolReference = new HashSet<Interactable>();

    //Pool furniture to check
    private HashSet<FurnitureLocation> furnitureCheckingPool = new HashSet<FurnitureLocation>();

    //Actions - used as callback for end of frame
    Action UpdateObjectRange;

    //Singleton pattern
    private static ObjectPoolingController _instance;
    public static ObjectPoolingController Instance { get { return _instance; } }

    private void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        //Add configured load ranges to quick reference
        foreach(ObjectLoadRangeConfig olrc in loadRangeConfig)
        {
            loadRanges.Add((int)olrc.range, olrc.loadDistance);
        }

        UpdateObjectRange += ExecuteUpdateObjectRanges;
    }

    private void LateUpdate()
    {
        //Check interactables
        ExecuteInteractableCheckingPool(loadPooledObjectPerFrame, loadNewObjectPerFrame);

        //Check furniture
        ExecuteFurnitureCheckingPool(maximumRangeCheckingPerFrame);

        //Disable this loop when complete
        if (interactableLoadList.Count <= 0)
        {
            interactablesToLoadCount = interactableLoadList.Count;

            if(furnitureCheckingPool.Count <= 0)
            {
                this.enabled = false;
            }
        }
    }

    private void ExecuteInteractableCheckingPool(int maxPooledLoops, int maxNewObjectLoops)
    {
        int loadedNewThisFrame = 0;
        int loadedPooledThisFrame = 0;

        //Load interactables
        interactablesToLoadCount = interactableLoadList.Count;

        while (interactableLoadList.Count > 0)
        {
            bool wasPooled = false;
            Interactable first = interactableLoadList.First();

            if(first == null || first.preset == null)
            {
                interactableLoadList.Remove(first);
                continue;
            }

            first.SpawnObject(out wasPooled);
            interactableLoadList.Remove(first);

            if (wasPooled) loadedPooledThisFrame++;
            else loadedNewThisFrame++;

            if (loadedPooledThisFrame >= maxPooledLoops) return;
            if (loadedNewThisFrame >= maxNewObjectLoops) return;
        }
    }

    private void ExecuteFurnitureCheckingPool(int maxLoops)
    {
        int furnitureCheckingThisFrame = 0;

        while (furnitureCheckingPool.Count > 0)
        {
            FurnitureLocation first = furnitureCheckingPool.First();

            bool meshesEnabled = SpawnRangeCheck(first, out _);

            for (int l = 0; l < first.meshes.Count; l++)
            {
                MeshRenderer rend = first.meshes[l];

                if (rend == null)
                {
                    first.meshes.RemoveAt(l);
                }
                else
                {
                    rend.enabled = meshesEnabled;
                }
            }

            furnitureCheckingPool.Remove(first);
            furnitureCheckingThisFrame++;
            if (furnitureCheckingThisFrame >= maxLoops) return;
        }

        furntiureCheckCount = furnitureCheckingPool.Count;
    }

    //Add a request for this to be loaded
    public void MarkAsToLoad(Interactable interactable)
    {
        if (interactable.controller != null && interactable.controller.isVisible) return;
        if (interactable.printDebug) Game.Log("Interactable " + interactable.name + " Mark to load");

        //Do range check first
        if (useRange && !Game.Instance.freeCam && !interactable.preset.excludeFromVisibilityRangeChecks)
        {
            //Is this spawned?
            if(interactable.loadedGeometry && interactable.controller != null)
            {
                //Should this be spawned immediately?
                if (SpawnRangeCheck(interactable, out _))
                {
                    interactable.controller.SetVisible(true);
                }
                else
                {
                    //Make sure this is on the range enable list
                    if (!interactableRangeToEnableDisableList.Contains(interactable))
                    {
                        interactableRangeToEnableDisableList.Add(interactable);
                    }
                }
            }
            else
            {
                //Should this be spawned immediately?
                if (SpawnRangeCheck(interactable, out _))
                {
                    if (useGradualSpawning && !Game.Instance.freeCam)
                    {
                        if (!interactableLoadList.Contains(interactable))
                        {
                            interactableLoadList.Add(interactable);
                            this.enabled = true; //Make sure checking loop is enabled
                        }
                    }
                    else
                    {
                        //If gradual spawning is off, spawn this immediately.
                        interactable.SpawnObject(out _);
                    }
                }
                else
                {
                    if (!interactableRangeToLoadList.Contains(interactable))
                    {
                        interactableRangeToLoadList.Add(interactable);
                    }
                }
            }
        }
        else if(interactable.loadedGeometry && interactable.controller != null)
        {
            interactable.controller.SetVisible(true);
        }
        else
        {
            if (useGradualSpawning && !Game.Instance.freeCam)
            {
                if (!interactableLoadList.Contains(interactable))
                {
                    interactableLoadList.Add(interactable);
                    this.enabled = true; //Make sure checking loop is enabled
                }
            }
            else
            {
                //If gradual spawning is off, spawn this immediately.
                interactable.SpawnObject(out _);
            }
        }
    }

    //Remove a request for this to be loaded
    public void MarkAsNotNeeded(Interactable interactable)
    {
        if (interactable.printDebug) Game.Log("Interactable " + interactable.name + " Mark not needed");

        if (interactable.controller != null && !interactable.preset.excludeFromVisibilityRangeChecks)
        {
            interactable.controller.SetVisible(false);
        }

        interactableRangeToLoadList.Remove(interactable);
        interactableRangeToEnableDisableList.Remove(interactable);
        interactableLoadList.Remove(interactable);
    }

    //Add a request for this to be loaded
    public void MarkAsToLoad(FurnitureLocation furniture, bool forceSpawnImmediate = false)
    {
        //This has been changed due to colliders needing to be loaded in to simulate murder ragdoll effects. Furniture now must always spawn immediately...
        furniture.SpawnObject(forceSpawnImmediate);

        ////Do range check first
        //if (useRange)
        //{
        //    //Is this spawned?
        //    if (furniture.spawnedObject != null)
        //    {
        //        bool meshesEnabled = SpawnRangeCheck(furniture, out _);

        //        foreach (MeshRenderer rend in furniture.meshes)
        //        {
        //            rend.enabled = meshesEnabled;
        //        }
        //    }
        //    else
        //    {
        //        //Should this be spawned immediately?
        //        if (SpawnRangeCheck(furniture, out _))
        //        {
        //            //If gradual spawning is off, spawn this immediately.
        //            furniture.SpawnObject();
        //        }
        //        else
        //        {
        //            if (!furnitureRangeToLoadList.Contains(furniture))
        //            {
        //                furnitureRangeToLoadList.Add(furniture);
        //            }
        //        }
        //    }
        //}
        //else
        //{
        //    //If gradual spawning is off, spawn this immediately.
        //    furniture.SpawnObject();
        //}
    }

    //Remove a request for this to be loaded
    public void MarkAsNotNeeded(FurnitureLocation furniture)
    {
        furnitureRangeToLoadList.Remove(furniture);
        furnitureRangeToEnableDisableList.Remove(furniture);
    }

    //Called when player changes node or every 5 seconds (do on end of frame)
    public void UpdateObjectRanges()
    {
        Toolbox.Instance.InvokeEndOfFrame(UpdateObjectRange, "Update object ranges");
    }

    public void ExecuteUpdateObjectRanges()
    {
        ExecuteUpdateObjectRanges(false);
    }

    public void ExecuteUpdateObjectRanges(bool forceImmediateSpawning = false)
    {
        //Game.Log("Update object ranges for " + furnitureRangeToEnableDisableList.Count + " furniture items");

        //Spawn/despawn existing interactables...
        foreach(FurnitureLocation f in furnitureRangeToEnableDisableList)
        {
            if (!furnitureCheckingPool.Contains(f))
            {
                furnitureCheckingPool.Add(f);
            }
        }

        if(forceImmediateSpawning)
        {
            //Run the furniture checking pool now...
            Game.Log("Gameplay: Executing immediate update of object/furniture ranges: " + furnitureCheckingPool.Count);
            ExecuteFurnitureCheckingPool(99999);
        }

        if (furnitureCheckingPool.Count > 0 && !this.enabled) this.enabled = true;

        //Load in new interactables...
        for (int i = 0; i < furnitureRangeToLoadList.Count; i++)
        {
            FurnitureLocation furniture = furnitureRangeToLoadList[i];

            if (SpawnRangeCheck(furniture, out _))
            {
                furniture.SpawnObject(true); //Spawn immediately as the range chaekc is done

                furnitureRangeToLoadList.RemoveAt(i);
                i--;
            }
        }

        //Spawn/despawn existing interactables...
        for (int i = 0; i < interactableRangeToEnableDisableList.Count; i++)
        {
            Interactable interactable = interactableRangeToEnableDisableList[i];

            if (interactable.controller != null)
            {
                if (interactable.preset.excludeFromVisibilityRangeChecks)
                {
                    interactable.controller.SetVisible(true);
                }
                else interactable.controller.SetVisible(SpawnRangeCheck(interactable, out _));
            }
        }

        //Load in new interactables...
        for (int i = 0; i < interactableRangeToLoadList.Count; i++)
        {
            Interactable interactable = interactableRangeToLoadList[i];

            if(SpawnRangeCheck(interactable, out _))
            {
                if(useGradualSpawning && !Game.Instance.freeCam && !forceImmediateSpawning)
                {
                    if (!interactableLoadList.Contains(interactable))
                    {
                        interactableLoadList.Add(interactable);
                        this.enabled = true; //Make sure checking loop is enabled
                    }
                }
                else
                {
                    //If gradual spawning is off, spawn this immediately.
                    interactable.SpawnObject(out _);
                }

                interactableRangeToLoadList.RemoveAt(i);
                i--;
            }
        }

        if(forceImmediateSpawning)
        {
            ExecuteInteractableCheckingPool(99999, 99999);
        }

        interactablesRangeCount = interactableRangeToLoadList.Count;
        updateObjectRangesTimer = 0;
    }

    //Check distance between player and object: Should this be spawned?
    public bool SpawnRangeCheck(Interactable interactable, out float distance)
    {
        distance = Vector3.Distance(CameraController.Instance.cam.transform.position, interactable.wPos);

        if (interactable == null || interactable.preset == null) return false;

        float range = 999;

        if(loadRanges.ContainsKey((int)interactable.preset.spawnRange))
        {
            range = loadRanges[(int)interactable.preset.spawnRange];
        }
        else
        {
            Game.LogError("There was a problem reading range from " + interactable.preset.name + ", check the spawn range setting...");
        }

        if (distance <= range)
        {
            return true;
        }
        else return false;
    }

    //Check distance between player and object: Should this be spawned?
    public bool SpawnRangeCheck(FurnitureLocation furniture, out float distance)
    {
        distance = Vector3.Distance(CameraController.Instance.cam.transform.position, furniture.anchorNode.position);
        if (furniture == null || furniture.furniture == null) return false;

        float range = 999;

        if(!SessionData.Instance.isFloorEdit)
        {
            if(loadRanges.ContainsKey((int)furniture.furniture.spawnRange))
            {
                range = loadRanges[(int)furniture.furniture.spawnRange];
            }
            else
            {
                Game.LogError("There was a problem reading range from " + furniture.furniture.name + ", check the spawn range setting...");
            }
        }

        //This is approx. distance as we only have the node location
        if (distance <= range)
        {
            return true;
        }
        else return false;
    }

    //Get an object: Pass a prefab and returns an available object or new instance
    public GameObject GetInteractableObject(Interactable interactable, out bool wasPooled, out bool isSelf)
    {
        if (interactable.printDebug) Game.Log("Interactable " + interactable.name + " Get interactable (spawn/draw from pool)");

        isSelf = false; //Returns true if this is the exact same object...
        wasPooled = false;
        GameObject ret = null;
        HashSet<Interactable> availableObjects = null;

        //Is there an object in the pool?
        if(usePooling)
        {
            if(!interactable.preset.excludeFromObjectPooling && interactablePool.TryGetValue(interactable.preset, out availableObjects))
            {
                //Check to see if itself is already in this list?
                if (availableObjects.Contains(interactable))
                {
                    ret = interactable.spawnedObject;

                    interactableInstancesSaved++;
                    RemoveFromPool(interactable);

                    isSelf = true;
                    wasPooled = true;
                    //Game.Log("Returned self object from pool: " + interactable.name + " parented to " + interactable.spawnParent);
                }
                else if(availableObjects.Count > 0)
                {
                    Interactable chosen = availableObjects.First();
                    ret = chosen.spawnedObject;

                    interactableInstancesSaved++;
                    RemoveFromPool(chosen);

                    //Set parent
                    //Toolbox.Instance.SetLightLayer(ret, interactable.node.building);
                    ret.transform.SetParent(interactable.spawnParent);

                    wasPooled = true;
                    //Game.Log("Returned identical object from pool: " + ret.name + " parented to " + interactable.spawnParent);
                }
            }
        }

        //Instantiate a new object if we can't find one...
        if(ret == null)
        {
            try
            {
                ret = Toolbox.Instance.SpawnObject(interactable.preset.prefab, interactable.spawnParent);
            }
            catch
            {
                Game.LogError("Unable to spawn object " + interactable.name);
            }

            //Toolbox.Instance.SetLightLayer(ret, interactable.node.building);
            interactablesLoaded++;
            //Game.Log("No object in pool, spawned new " + interactable.preset.name + " parented to " + interactable.spawnParent);
        }

        //Add to range check list for disabling meshes
        if (useRange && !Game.Instance.freeCam && !interactableRangeToEnableDisableList.Contains(interactable))
        {
            interactableRangeToEnableDisableList.Add(interactable);
        }

        return ret;
    }

    public void RemoveFromPool(Interactable interactable)
    {
        interactablePool[interactable.preset].Remove(interactable);
        entirePoolReference.Remove(interactable);

        interactablesCurrentlyPooled = entirePoolReference.Count;
        interactableFullPercentage = (float)entirePoolReference.Count / maximumInteractablePoolCache;
    }

    //Send an object to the pool
    public void PoolInteractable(Interactable interactable)
    {
        //Is the pool too big? Remove the oldest object...
        if(interactablesCurrentlyPooled >= maximumInteractablePoolCache)
        {
            Interactable oldestPool = entirePoolReference.First();
            oldestPool.DespawnObject();

            RemoveFromPool(oldestPool);
        }

        if(!interactablePool.ContainsKey(interactable.preset))
        {
            interactablePool.Add(interactable.preset, new HashSet<Interactable>());
        }

        interactablePool[interactable.preset].Add(interactable);
        entirePoolReference.Add(interactable);

        interactablesCurrentlyPooled = entirePoolReference.Count;
        interactableFullPercentage = (float)entirePoolReference.Count / maximumInteractablePoolCache;
    }
}
