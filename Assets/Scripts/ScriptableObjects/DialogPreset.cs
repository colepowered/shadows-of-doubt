﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "dialog_data", menuName = "Database/Dialog Option")]

public class DialogPreset : SoCustomComparison
{
    [Header("Setup")]
    public string msgID;
    [Tooltip("Is this option added to citizens at the start?")]
    public bool defaultOption = false;
    [EnableIf("defaultOption")]
    [Tooltip("Is this used for the telephone calling dialog?")]
    public bool telephoneCallOption = false;
    [Tooltip("Is this used for the hospital decision tree dialog?")]
    public bool hospitalDecisionOption = false;
    [Tooltip("Must have access to this key for this option to appear")]
    public Evidence.DataKey tiedToKey = Evidence.DataKey.photo;
    [Tooltip("Ranking within options")]
    public int ranking = 0;
    [Tooltip("Remove this after it's been said")]
    public bool removeAfterSaying = true;
    [Tooltip("Replenish after each day: Every 24 hours this will be added to every citizen if not already added")]
    [EnableIf("defaultOption")]
    public bool dailyReplenish = false;
    [Tooltip("This dialog will give the player the mission details")]
    public bool isJobDetails = false;
    [Tooltip("If false, this will only be active when the associated job is active. If true, this requirement will be ignored.")]
    public bool ignoreActiveJobRequirement = false;
    [Tooltip("Special cases")]
    public SpecialCase specialCase = SpecialCase.none;
    [Tooltip("This option is selectable for a cost")]
    public int cost = 0;
    [Tooltip("If ture, the above is a percentage cost of the player's total wealth")]
    public bool usePercentageCost = false;
    [Tooltip("If true, and the player doesn't have enough to cover the cost then use the total amount of player's wealth")]
    public bool useAllWealthIfNotEnough = false;
    [Tooltip("Only displayed if the current address requires a password")]
    public bool displayIfPasswordUnknown = false;
    [Tooltip("Player must input correct string before forcing a success or fail")]
    public InputSetting inputBox = InputSetting.none;
    [Tooltip("Display this dialog in red (illegal)")]
    public bool displayAsIllegal = false;
    [Tooltip("Preceeding syntax")]
    public string preceedingSyntax;
    [Tooltip("Following syntax")]
    public string followingSyntax;

    public enum InputSetting { none, addressPassword };

    public enum SpecialCase { none, backroomBribe, publicFacingWorkplace, working, workingGuestPass, callInSuspect, talkingToJobPoster, inputName, lastCaller, knowName, lookAroundHome, returnJobItemA, medicalCosts, starchPitch, mugging, neverDisplay, loanSharkAccept, loanSharkPayment, loanSharkPaymentRefuse, loanSharkAsk, revealHiddenitemPhoto, hotelBill, rentHotelRoomCheap, rentHotelRoomExpensive, hotelCheckOut, hotelRentRoom, mustHaveRoomAtHotel, mustBeMurdererForSuccess};

    [Header("Success Test")]
    [Tooltip("Use a success test to determin the outcome response")]
    public bool useSuccessTest = false;
    [EnableIf("useSuccessTest")]
    [Tooltip("Requires the correct password to be successful, if there is one")]
    public bool requiresPassword = false;
    [EnableIf("useSuccessTest")]
    [Range(0f, 1f)]
    public float baseChance = 0f;
    [EnableIf("useSuccessTest")]
    [Tooltip("If restrained, the success change is affected this much...")]
    public float affectChanceIfRestrained = 0f;
    [Tooltip("Modify success based on below traits...")]
    [ReorderableList]
    public List<CharacterTrait.TraitPickRule> modifySuccessChanceTraits = new List<CharacterTrait.TraitPickRule>();

    [Header("Responses")]
    [ReorderableList]
    public List<AIActionPreset.AISpeechPreset> responses = new List<AIActionPreset.AISpeechPreset>();

    [Header("Follow up")]
    [Tooltip("Add these as player responses following...")]
    public List<DialogPreset> followUpDialogSuccess = new List<DialogPreset>();
    public List<DialogPreset> followUpDialogFail = new List<DialogPreset>();
    [Tooltip("Remove these other options")]
    public List<DialogPreset> removeDialog = new List<DialogPreset>();
    public List<DialogPreset> removeDialogOnSuccess = new List<DialogPreset>();
    public List<DialogPreset> removeDialogOnFail = new List<DialogPreset>();

    public int GetCost(Actor talkingTo, Actor talking = null)
    {
        int ret = cost;
        if (usePercentageCost) ret = Mathf.RoundToInt(GameplayController.Instance.money * (cost / 100f));
        if (useAllWealthIfNotEnough && GameplayController.Instance.money < ret) ret = GameplayController.Instance.money;

        //Use loan replayment
        if(specialCase == SpecialCase.loanSharkPayment && talkingTo != null)
        {
            Human hu = talkingTo as Human;

            if(hu.job != null && hu.job.employer != null)
            {
                GameplayController.LoanDebt debt = GameplayController.Instance.debt.Find(item => item.companyID == hu.job.employer.companyID);

                if(debt != null)
                {
                    ret = debt.GetRepaymentAmount();
                }
            }
        }
        //Use hotel bill
        else if(specialCase == SpecialCase.hotelBill || specialCase == SpecialCase.hotelCheckOut)
        {
            GameplayController.HotelGuest g = Toolbox.Instance.GetHotelRoom(talking as Human);

            if(g != null)
            {
                ret = g.bill;
            }
        }

        return ret;
    }
}
