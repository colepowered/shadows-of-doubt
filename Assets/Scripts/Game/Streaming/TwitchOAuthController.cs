using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;

public class TwitchOAuthController : MonoBehaviour
{
    // Singleton pattern
    private static TwitchOAuthController _instance;
    public static TwitchOAuthController Instance { get { return _instance; } }

    // Necessary constants
    private const string TwitchAuthUrl = "https://id.twitch.tv/oauth2/authorize";
    // TODO: This could be held at a simple API for a little obfuscation. Users would still have access to it. 
    // This is generated from the Twitch Dev applications console
    private const string ClientID = "bq0wyxhwa7xjlyomjjdv2o6wun6l2t";
    private const string TwitchRedirectURL = "http://localhost:8085/";
    
    // Private members
    private string _twitchAuthStateVerify;
    private string _authToken = null;

    // Queue for tokens as this operation can happen off the main thread. We can further this with an asynclock, but for the time being is consistently working.
    private Queue<string> _tokenQueue = new Queue<string>();

    public bool _hasAuth;
    private bool _tryingValidation;

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    
    private void QueueAuthorizationToken()
    {
        _tokenQueue.Enqueue(_authToken);
        if (String.IsNullOrEmpty(_authToken))
        {
            Debug.LogWarning("Twitch authorization token returned null or empty");
        }
    }

    public string GetAuthToken()
    {
        if (string.IsNullOrEmpty(_authToken))
            Debug.LogWarning("Trying to retrieve empty authToken");
        
        return _authToken;
    }
    
    public string GetClientID()
    {
        if (string.IsNullOrEmpty(ClientID))
            Debug.LogWarning("Trying to retrieve empty authToken");
        
        return ClientID;
    }

    public void TryTwitchAuthorization()
    {
        InitiateTwitchAuth();
    }
    
    /// <summary>
    /// Starts the Twitch OAuth flow by constructing the Twitch auth URL based on the scopes you want/need.
    /// </summary>
    public void InitiateTwitchAuth()
    {
        string[] scopes;
        string s;

        // list of scopes we want
        scopes = new[]
        {
            "chat:read",
            "moderator:read:chatters",
            "moderation:read",
            "channel:read:vips",
            "moderator:read:followers",
        };

        // generate something for the "state" parameter.
        // this can be whatever you want it to be, it's gonna be "echoed back" to us as is and should be used to
        // verify the redirect back from Twitch is valid.
        _twitchAuthStateVerify = ((Int64) (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds).ToString();

        // query parameters for the Twitch auth URL
        s = "client_id=" + ClientID + "&" +
            "redirect_uri=" + UnityWebRequest.EscapeURL(TwitchRedirectURL) + "&" +
            "state=" + _twitchAuthStateVerify + "&" +
            "response_type=token&" +
            "scope=" + String.Join("+", scopes);

        // start our local webserver to receive the redirect back after Twitch authenticated
        StartLocalWebserver();

        // open the users browser and send them to the Twitch auth URL
        Application.OpenURL(TwitchAuthUrl + "?" + s);
    }
    
    private void StartLocalWebserver()
    {
        HttpListener httpListener = new HttpListener();

        httpListener.Prefixes.Add(TwitchRedirectURL);
        httpListener.Start();
        httpListener.BeginGetContext(new AsyncCallback(IncomingHttpRequest), httpListener);
    }
    
    // <summary>
    /// Handles the incoming HTTP request
    /// </summary>
    /// <param name="result"></param>
    private void IncomingHttpRequest(IAsyncResult result)
    {
        Debug.Log("Incoming Http");

        HttpListener httpListener;
        HttpListenerContext httpContext;
        HttpListenerRequest httpRequest;
        HttpListenerResponse httpResponse;
        string responseString;

        // get back the reference to our http listener
        httpListener = (HttpListener) result.AsyncState;

        // fetch the context object
        httpContext = httpListener.EndGetContext(result);

        // if we'd like the HTTP listener to accept more incoming requests, we'd just restart the "get context" here:
        httpListener.BeginGetContext(new AsyncCallback(IncomingAuth), httpListener);

        // the context object has the request object for us, that holds details about the incoming request
        httpRequest = httpContext.Request;

        // build a response to send JS back to the browser for OAUTH Relay
        httpResponse = httpContext.Response;
        string url = (httpRequest.RawUrl);

        Regex rx = new Regex(@"\berror=access_denied\b");
        var errorMatch = rx.Match(url);

        string landingPageString = "";
        string closeoutScript = "";

        if (!errorMatch.Success)
        {
            landingPageString = TwitchAuthLandingPages.SuccessLandingPage;
            closeoutScript =
                "document.addEventListener(\"DOMContentLoaded\", () =>{setTimeout(function() {window.close()}, 5000);});";
        }
        else
        {
            landingPageString = TwitchAuthLandingPages.RejectedLandingPage;
        }

        responseString = $"{landingPageString}" +
                         "<script type=\"text/javascript\">" +
                         "var xhr = new XMLHttpRequest(); " +
                         $"xhr.open(\"POST\", \"{UnityWebRequest.EscapeURL(TwitchRedirectURL)}\");" +
                         "xhr.send(window.location);" + //Sending the window location (the url bar) from the browser to our listener
                         $"{closeoutScript}" +
                         "</script></body></html>";

        byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);

        // send the output to the client browser
        httpResponse.ContentLength64 = buffer.Length;
        System.IO.Stream output = httpResponse.OutputStream;
        output.Write(buffer, 0, buffer.Length);
        output.Close();
    }
    
    private void IncomingAuth(IAsyncResult ar)
    {
        Debug.Log("Incoming auth");
        //mostly the same as IncomingHttpRequest
        HttpListener httpListener;
        HttpListenerContext httpContext;
        HttpListenerRequest httpRequest;

        httpListener = (HttpListener) ar.AsyncState;
        httpContext = httpListener.EndGetContext(ar);
        httpRequest = httpContext.Request;

        //this time we take an input stream from the request to receive the url
        string url;
        using (var reader = new StreamReader(httpRequest.InputStream,
                   httpRequest.ContentEncoding))
        {
            url = reader.ReadToEnd();
            Debug.Log(url);
        }

        //regex to extract the OAUTH and auth state
        Regex rx = new Regex(@".+#access_token=(.+)&scope.*state=(\d+)");
        var match = rx.Match(url);

        if (match.Success)
        {
            _hasAuth = true;
        }

        //if state doesnt match reject data
        if (match.Groups[2].Value != _twitchAuthStateVerify)
        {
            httpListener.Stop();
            return;
        }

        _authToken = match.Groups[1].Value;
        if (!string.IsNullOrEmpty(_authToken))
        {
            QueueAuthorizationToken();
        }
        
        httpListener.Stop();
    }
}
