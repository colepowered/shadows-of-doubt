﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using NaughtyAttributes;
using System.Linq;

//Controller assigned to each citizen in the city
//This script must be super efficient as it's being run by each citizen (so ~4k times!)
//Script pass 1
public class Police : Human
{
    //Remove this from game world
    public void Remove()
    {
        Destroy(this.gameObject);
    }
}
	
