﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class WindowMapper : MonoBehaviour
{
    public GameObject buildingObject;
    public GameObject debugWindow;
    public BuildingPreset preset;
    public Transform buildingModel;
    public Transform cableLinkingContainer;
    public Transform neonSideSignContainer;

    [Button]
    public void SpawnObjectsOnWindows()
    {
        for (int i = 0; i < preset.sortedWindows.Count; i++)
        {
            BuildingPreset.WindowUVFloor floor = preset.sortedWindows[i];
            GameObject newFloorObj = new GameObject();
            newFloorObj.transform.SetParent(buildingObject.transform, false);
            newFloorObj.name = "Floor " + (i + 1);

            foreach(BuildingPreset.WindowUVBlock block in floor.front)
            {
                GameObject newObj = Instantiate(debugWindow, newFloorObj.transform);
                newObj.transform.localPosition = block.localMeshPositionLeft;
                newObj.transform.name = block.floor + " | " + block.side + " | " + block.horizonal + " ("+block.localMeshPositionLeft + " - " + block.localMeshPositionRight +")";
            }

            foreach (BuildingPreset.WindowUVBlock block in floor.back)
            {
                GameObject newObj = Instantiate(debugWindow, newFloorObj.transform);
                newObj.transform.localPosition = block.localMeshPositionLeft;
                newObj.transform.name = block.floor + " | " + block.side + " | " + block.horizonal + " (" + block.localMeshPositionLeft + " - " + block.localMeshPositionRight + ")";
            }

            foreach (BuildingPreset.WindowUVBlock block in floor.left)
            {
                GameObject newObj = Instantiate(debugWindow, newFloorObj.transform);
                newObj.transform.localPosition = block.localMeshPositionLeft;
                newObj.transform.name = block.floor + " | " + block.side + " | " + block.horizonal + " (" + block.localMeshPositionLeft + " - " + block.localMeshPositionRight + ")";
            }

            foreach (BuildingPreset.WindowUVBlock block in floor.right)
            {
                GameObject newObj = Instantiate(debugWindow, newFloorObj.transform);
                newObj.transform.localPosition = block.localMeshPositionLeft;
                newObj.transform.name = block.floor + " | " + block.side + " | " + block.horizonal + " (" + block.localMeshPositionLeft + " - " + block.localMeshPositionRight + ")";
            }
        }
    }

    [Button]
    public void GenerateCableLinkingPoints()
    {
        preset.cableLinkPoints.Clear();

        Transform[] allChildren = cableLinkingContainer.GetComponentsInChildren<Transform>();

        foreach (Transform child in allChildren)
        {
            //Get local: Make sure you have the correct building model plugged in
            Vector3 relativeToModel = buildingModel.InverseTransformPoint(child.position);
            Quaternion LocalRotation = Quaternion.Inverse(buildingModel.rotation) * child.rotation;

            //Does this have a collider?
            Collider coll = child.gameObject.GetComponent<Collider>();

            if (coll != null)
            {
                preset.cableLinkPoints.Add(new BuildingPreset.CableLinkPoint { localPos = relativeToModel, localRot = LocalRotation.eulerAngles });
            }
        }

        //Force save data
    #if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(this);
        #endif
    }

    [Button]
    public void GenerateNeonSignSidePoints()
    {
        preset.sideSignPoints.Clear();

        Transform[] allChildren = neonSideSignContainer.GetComponentsInChildren<Transform>();

        foreach (Transform child in allChildren)
        {
            Vector3 relativeToModel = buildingModel.InverseTransformPoint(child.position);
            Quaternion LocalRotation = Quaternion.Inverse(buildingModel.rotation) * child.rotation;

            //Does this have a collider?
            Collider coll = child.gameObject.GetComponent<Collider>();

            if (coll != null)
            {
                preset.sideSignPoints.Add(new BuildingPreset.CableLinkPoint { localPos = relativeToModel, localRot = LocalRotation.eulerAngles });
            }
        }

        //Force save data
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(this);
#endif
    }
}
