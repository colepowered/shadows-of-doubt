using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SyncDiskElementController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    [Space(7)]
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI numberText;
    public TextMeshProUGUI descriptionText;
    [Space(7)]
    public ButtonController option1Button;
    public ButtonController option2Button;
    public ButtonController option3Button;
    public ButtonController upgradeButton;
    public ButtonController sideEffectButton;
    public ButtonController uninstallButton;
    public Image option1Icon;
    public Image option2Icon;
    public Image option3Icon;
    [Space(7)]
    public ButtonController upgradePip1;
    public ButtonController upgradePip2;
    public ButtonController upgradePip3;
    [Space(7)]
    public Image manufacturerLogo;

    [Header("Settings")]
    public Sprite upgradeEmptySprite;
    public Sprite upgradeEnabledSprite;

    [Header("State")]
    public UpgradesController.Upgrades upgrade;
    public SyncDiskPreset preset;
    public int selectedOption = 0;
    public bool installAllowed = false;
    
    public void Setup(UpgradesController.Upgrades newUpgrade)
    {
        upgrade = newUpgrade;
        preset = upgrade.GetPreset();

        //Setup preset
        if (preset.mainEffect1 == SyncDiskPreset.Effect.none) Destroy(option1Button.gameObject);
        else
        {
            option1Button.tooltip.mainDictionaryKey = preset.mainEffect1Name;
            option1Button.tooltip.detailText = Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.mainEffect1Description), preset, additionalObject: new int[] { 0, 0 });
            option1Icon.sprite = preset.mainEffect1Icon;
        }

        if (preset.mainEffect2 == SyncDiskPreset.Effect.none) Destroy(option2Button.gameObject);
        else
        {
            option2Button.tooltip.mainDictionaryKey = preset.mainEffect2Name;
            option2Button.tooltip.detailText = Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.mainEffect2Description), preset, additionalObject: new int[] { 1, 0 });
            option2Icon.sprite = preset.mainEffect2Icon;
        }

        if (preset.mainEffect3 == SyncDiskPreset.Effect.none) Destroy(option3Button.gameObject);
        else
        {
            option3Button.tooltip.mainDictionaryKey = preset.mainEffect3Name;
            option3Button.tooltip.detailText = Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.mainEffect3Description), preset, additionalObject: new int[] { 2, 0 });
            option3Icon.sprite = preset.mainEffect3Icon;
        }

        if(preset.sideEffect == SyncDiskPreset.Effect.none) Destroy(sideEffectButton.gameObject);
        else
        {
            sideEffectButton.tooltip.detailText = Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.sideEffectDescription), preset);
        }

        int maxUpgrades = Mathf.Max(new int[] { preset.option1UpgradeEffects.Count, preset.option2UpgradeEffects.Count, preset.option3UpgradeEffects.Count });

        if (maxUpgrades <= 0)
        {
            Destroy(upgradePip1.gameObject);
            Destroy(upgradePip2.gameObject);
            Destroy(upgradePip3.gameObject);
            Destroy(upgradeButton.gameObject);
        }

        if (maxUpgrades <= 1)
        {
            if(upgradePip2 != null) Destroy(upgradePip2.gameObject);
            if (upgradePip3 != null) Destroy(upgradePip3.gameObject);
        }

        if (maxUpgrades <= 2)
        {
            if (upgradePip3 != null) Destroy(upgradePip3.gameObject);
        }

        numberText.text = "#" + preset.syncDiskNumber + "/" + Toolbox.Instance.allSyncDisks.Count;
        titleText.text = Strings.Get("evidence.syncdisks", preset.name);

        if(preset.manufacturer == SyncDiskPreset.Manufacturer.StarchKola)
        {
            manufacturerLogo.sprite = InterfaceControls.Instance.starchLogo;
        }
        else if(preset.manufacturer == SyncDiskPreset.Manufacturer.CandorNews)
        {
            manufacturerLogo.sprite = InterfaceControls.Instance.candorLogo;
        }
        else if(preset.manufacturer == SyncDiskPreset.Manufacturer.ElGen)
        {
            manufacturerLogo.sprite = InterfaceControls.Instance.elGenLogo;
        }
        else if(preset.manufacturer == SyncDiskPreset.Manufacturer.Kaizen)
        {
            manufacturerLogo.sprite = InterfaceControls.Instance.KaizenLogo;
        }
        else if(preset.manufacturer == SyncDiskPreset.Manufacturer.KensingtonIndigo)
        {
            manufacturerLogo.sprite = InterfaceControls.Instance.kensingtonLogo;
        }
        else
        {
            manufacturerLogo.sprite = InterfaceControls.Instance.blackMarketLogo;
        }

        installAllowed = UpgradesController.Instance.installedAllowed;
        VisualUpdate();
    }

    public void SetInstallAllowed(bool val)
    {
        installAllowed = val;
        VisualUpdate();
    }

    public void VisualUpdate()
    {
        //Option buttons
        if(upgrade.state == UpgradesController.SyncDiskState.notInstalled)
        {
            string options = string.Empty;
            titleText.text = Strings.Get("evidence.syncdisks", preset.name) + "*";

            if (preset.mainEffect1 != SyncDiskPreset.Effect.none)
            {
                options += Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.mainEffect1Description), preset, additionalObject: new int[] { 0, 0 });
            }

            if (preset.mainEffect2 != SyncDiskPreset.Effect.none)
            {
                options += "\n" + Strings.Get("evidence.syncdisks", "OR") + "\n" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.mainEffect2Description), preset, additionalObject: new int[] { 1, 0 });
            }

            if (preset.mainEffect3 != SyncDiskPreset.Effect.none)
            {
                options += "\n" + Strings.Get("evidence.syncdisks", "OR") + "\n" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.mainEffect3Description), preset, additionalObject: new int[] { 2, 0 });
            }

            descriptionText.text = options;

            if (selectedOption <= 0)
            {
                if (option1Button != null)
                {
                    option1Button.icon.gameObject.SetActive(false);
                    option1Button.juice.Pulsate(installAllowed);
                }
                if (option2Button != null)
                {
                    option2Button.icon.gameObject.SetActive(false);
                    option2Button.juice.Pulsate(installAllowed);
                }
                if (option3Button != null)
                {
                    option3Button.icon.gameObject.SetActive(false);
                    option3Button.juice.Pulsate(installAllowed);
                }
            }
            else if(selectedOption == 1)
            {
                if (option1Button != null) option1Button.icon.gameObject.SetActive(true);
                if (option2Button != null) option2Button.icon.gameObject.SetActive(false);
                if (option3Button != null) option3Button.icon.gameObject.SetActive(false);
            }
            else if(selectedOption == 2)
            {
                if (option1Button != null) option1Button.icon.gameObject.SetActive(false);
                if (option2Button != null) option2Button.icon.gameObject.SetActive(true);
                if (option3Button != null) option3Button.icon.gameObject.SetActive(false);
            }
            else
            {
                if (option1Button != null) option1Button.icon.gameObject.SetActive(false);
                if (option2Button != null) option2Button.icon.gameObject.SetActive(false);
                if (option3Button != null) option3Button.icon.gameObject.SetActive(true);
            }

            if (option1Button != null)
            {
                option1Button.SetInteractable(installAllowed);
            }
            if (option2Button != null)
            {
                option2Button.SetInteractable(installAllowed);
            }
            if (option3Button != null)
            {
                option3Button.SetInteractable(installAllowed);
            }

            //Enable/disable install button
            uninstallButton.text.text = Strings.Get("ui.interface", "Install");

            if(installAllowed && selectedOption >= 1)
            {
                uninstallButton.SetInteractable(true);
            }
            else
            {
                uninstallButton.SetInteractable(false);
            }

            //Inable/disable upgrades button
            if (upgradeButton != null)
            {
                upgradeButton.SetInteractable(false);
                upgradeButton.juice.Pulsate(false);
                upgradeButton.tooltip.detailText = string.Empty;
            }

            if (upgradePip1 != null)
            {
                upgradePip1.icon.sprite = upgradeEmptySprite;

                //Collect all upgrade descriptions
                upgradePip1.tooltip.detailText = string.Empty;

                if (preset.option1UpgradeNameReferences.Count > 0)
                {
                    upgradePip1.tooltip.detailText += "<u>" + Strings.Get("evidence.syncdisks", preset.mainEffect1Name) + ":</u>\n<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option1UpgradeNameReferences[0]), preset, additionalObject: new int[] { 0, 0 }) + "</i>";
                }

                if (preset.option2UpgradeNameReferences.Count > 0)
                {
                    upgradePip1.tooltip.detailText += "\n\n<u>" + Strings.Get("evidence.syncdisks", preset.mainEffect2Name) + ":</u>\n<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option2UpgradeNameReferences[0]), preset, additionalObject: new int[] { 1, 0 }) + "</i>";
                }

                if (preset.option3UpgradeNameReferences.Count > 0)
                {
                    upgradePip1.tooltip.detailText += "\n\n<u>" + Strings.Get("evidence.syncdisks", preset.mainEffect3Name) + ":</u>\n<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option3UpgradeNameReferences[0]), preset, additionalObject: new int[] { 2, 0 }) + "</i>";
                }
            }

            if (upgradePip2 != null)
            {
                upgradePip2.icon.sprite = upgradeEmptySprite;

                //Collect all upgrade descriptions
                upgradePip2.tooltip.detailText = string.Empty;

                if (preset.option1UpgradeNameReferences.Count > 1)
                {
                    upgradePip2.tooltip.detailText += "<u>" + Strings.Get("evidence.syncdisks", preset.mainEffect1Name) + ":</u>\n<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option1UpgradeNameReferences[1]), preset, additionalObject: new int[] { 0, 1 }) + "</i>";
                }

                if (preset.option2UpgradeNameReferences.Count > 1)
                {
                    upgradePip2.tooltip.detailText += "\n\n<u>" + Strings.Get("evidence.syncdisks", preset.mainEffect2Name) + ":</u>\n<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option2UpgradeNameReferences[1]), preset, additionalObject: new int[] { 1, 1 }) + "</i>";
                }

                if (preset.option3UpgradeNameReferences.Count > 1)
                {
                    upgradePip2.tooltip.detailText += "\n\n<u>" + Strings.Get("evidence.syncdisks", preset.mainEffect3Name) + ":</u>\n<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option3UpgradeNameReferences[1]), preset, additionalObject: new int[] { 2, 1 }) + "</i>";
                }
            }

            if (upgradePip3 != null)
            {
                upgradePip3.icon.sprite = upgradeEmptySprite;

                //Collect all upgrade descriptions
                upgradePip3.tooltip.detailText = string.Empty;

                if (preset.option1UpgradeNameReferences.Count > 2)
                {
                    upgradePip3.tooltip.detailText += "<u>" + Strings.Get("evidence.syncdisks", preset.mainEffect1Name) + ":</u>\n<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option1UpgradeNameReferences[2]), preset, additionalObject: new int[] { 0, 2 }) + "</i>";
                }

                if (preset.option2UpgradeNameReferences.Count > 2)
                {
                    upgradePip3.tooltip.detailText += "\n\n<u>" + Strings.Get("evidence.syncdisks", preset.mainEffect2Name) + ":</u>\n<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option2UpgradeNameReferences[2]), preset, additionalObject: new int[] { 1, 2 }) + "</i>";
                }

                if (preset.option3UpgradeNameReferences.Count > 2)
                {
                    upgradePip3.tooltip.detailText += "\n\n<u>" + Strings.Get("evidence.syncdisks", preset.mainEffect3Name) + ":</u>\n<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option3UpgradeNameReferences[2]), preset, additionalObject: new int[] { 2, 2 }) + "</i>";
                }
            }
        }
        else
        {
            titleText.text = Strings.Get("evidence.syncdisks", preset.name);

            if (option1Button != null) option1Button.juice.Pulsate(false);
            if (option2Button != null) option2Button.juice.Pulsate(false);
            if (option3Button != null) option3Button.juice.Pulsate(false);

            if (option1Button != null) option1Button.icon.gameObject.SetActive(false);
            if (option2Button != null) option2Button.icon.gameObject.SetActive(false);
            if (option3Button != null) option3Button.icon.gameObject.SetActive(false);

            int maxLevel = 0;

            if (upgrade.state == UpgradesController.SyncDiskState.option1)
            {
                descriptionText.text = Strings.Get("evidence.syncdisks", preset.mainEffect1Name) + ": " + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.mainEffect1Description), preset, additionalObject: new int[] { 0, 0 });

                if (option1Button != null)
                {
                    option1Button.SetInteractable(true);
                    option1Button.icon.gameObject.SetActive(true);
                }

                if (option2Button != null)
                {
                    option2Button.SetInteractable(false);
                    option2Icon.color = Color.gray;
                }

                if (option3Button != null)
                {
                    option3Button.SetInteractable(false);
                    option3Icon.color = Color.gray;
                }

                maxLevel = preset.option1UpgradeEffects.Count;

                try
                {
                    upgradeButton.tooltip.detailText = "<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option1UpgradeNameReferences[upgrade.level]), preset, additionalObject: new int[] { 0, upgrade.level });

                    if (maxLevel >= 1 & upgradePip1 != null)
                    {
                        upgradePip1.tooltip.detailText = "<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option1UpgradeNameReferences[0]), preset, additionalObject: new int[] { 0, 0 });
                    }

                    if (maxLevel >= 2 & upgradePip2 != null)
                    {
                        upgradePip2.tooltip.detailText = "<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option1UpgradeNameReferences[1]), preset, additionalObject: new int[] { 0, 1 });
                    }

                    if (maxLevel >= 3 & upgradePip3 != null) upgradePip3.tooltip.detailText = "<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option1UpgradeNameReferences[2]), preset, additionalObject: new int[] { 0, 2 });
                }
                catch
                {
                    Game.LogError("Error setting upgrade levels for " + preset.name);
                }
            }
            else if (upgrade.state == UpgradesController.SyncDiskState.option2)
            {
                descriptionText.text = Strings.Get("evidence.syncdisks", preset.mainEffect2Name) + ": " + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.mainEffect2Description), preset, additionalObject: new int[] { 1, 0 });

                if (option1Button != null)
                {
                    option1Button.SetInteractable(false);
                    option1Icon.color = Color.gray;
                }

                if (option2Button != null)
                {
                    option2Button.SetInteractable(true);
                    option2Button.icon.gameObject.SetActive(true);
                }

                if (option3Button != null)
                {
                    option3Button.SetInteractable(false);
                    option3Icon.color = Color.gray;
                }

                maxLevel = preset.option2UpgradeEffects.Count;

                try
                {
                    upgradeButton.tooltip.detailText = "<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option2UpgradeNameReferences[upgrade.level]), preset, additionalObject: new int[] { 1, upgrade.level });
                    if (maxLevel >= 1 & upgradePip1 != null) upgradePip1.tooltip.detailText = "<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option2UpgradeNameReferences[0]), preset, additionalObject: new int[] { 1, 0 });
                    if (maxLevel >= 2 & upgradePip2 != null) upgradePip2.tooltip.detailText = "<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option2UpgradeNameReferences[1]), preset, additionalObject: new int[] { 1, 1 });
                    if (maxLevel >= 3 & upgradePip3 != null) upgradePip3.tooltip.detailText = "<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option2UpgradeNameReferences[2]), preset, additionalObject: new int[] { 1, 2 });
                }
                catch
                {
                    Game.LogError("Error setting upgrade levels for " + preset.name);
                }
            }
            else if (upgrade.state == UpgradesController.SyncDiskState.option3)
            {
                descriptionText.text = Strings.Get("evidence.syncdisks", preset.mainEffect3Name) + ": " + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.mainEffect3Description), preset, additionalObject: new int[] { 2, 0 });

                if (option1Button != null)
                {
                    option1Button.SetInteractable(false);
                    option1Icon.color = Color.gray;
                }

                if (option2Button != null)
                {
                    option2Button.SetInteractable(false);
                    option2Icon.color = Color.gray;
                }

                if (option3Button != null)
                {
                    option3Button.SetInteractable(true);
                    option3Button.icon.gameObject.SetActive(true);
                }

                maxLevel = preset.option3UpgradeEffects.Count;

                try
                {
                    upgradeButton.tooltip.detailText = "<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option3UpgradeNameReferences[upgrade.level]), preset, additionalObject: new int[] { 2, upgrade.level });
                    if (maxLevel >= 1 & upgradePip1 != null) upgradePip1.tooltip.detailText = "<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option3UpgradeNameReferences[0]), preset, additionalObject: new int[] { 2, 0 });
                    if (maxLevel >= 2 & upgradePip2 != null) upgradePip2.tooltip.detailText = "<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option3UpgradeNameReferences[0]), preset, additionalObject: new int[] { 2, 1 });
                    if (maxLevel >= 3 & upgradePip3 != null) upgradePip3.tooltip.detailText = "<i>" + Strings.ComposeText(Strings.Get("evidence.syncdisks", preset.option3UpgradeNameReferences[0]), preset, additionalObject: new int[] { 2, 2 });
                }
                catch
                {
                    Game.LogError("Error setting upgrade levels for " + preset.name);
                }
            }

            //Enable/disable install button
            uninstallButton.text.text = Strings.Get("ui.interface", "Uninstall");

            if(upgrade.uninstallCost > 0)
            {
                uninstallButton.text.text += " [" + CityControls.Instance.cityCurrency + upgrade.uninstallCost + "]";
            }

            if (installAllowed && (upgrade.uninstallCost <= 0 || GameplayController.Instance.money >= upgrade.uninstallCost))
            {
                uninstallButton.SetInteractable(true);
            }
            else uninstallButton.SetInteractable(false);

            //Enable/disable upgrades
            if (upgradeButton != null)
            {
                if (installAllowed && UpgradesController.Instance.upgradeVials.Count > 0 && upgrade.level < maxLevel)
                {
                    upgradeButton.SetInteractable(true);
                    upgradeButton.juice.Pulsate(true, false);
                }
                else
                {
                    upgradeButton.SetInteractable(false);
                    upgradeButton.juice.Pulsate(false, false);
                }
            }

            if(upgrade.level <= 0)
            {
                if (upgradePip1 != null) upgradePip1.icon.sprite = upgradeEmptySprite;
                if (upgradePip2 != null) upgradePip2.icon.sprite = upgradeEmptySprite;
                if (upgradePip3 != null) upgradePip3.icon.sprite = upgradeEmptySprite;
            }
            else if(upgrade.level == 1)
            {
                if (upgradePip1 != null) upgradePip1.icon.sprite = upgradeEnabledSprite;
                if (upgradePip2 != null) upgradePip2.icon.sprite = upgradeEmptySprite;
                if (upgradePip3 != null) upgradePip3.icon.sprite = upgradeEmptySprite;
            }
            else if(upgrade.level == 2)
            {
                if (upgradePip1 != null) upgradePip1.icon.sprite = upgradeEnabledSprite;
                if (upgradePip2 != null) upgradePip2.icon.sprite = upgradeEnabledSprite;
                if (upgradePip3 != null) upgradePip3.icon.sprite = upgradeEmptySprite;
            }
            else
            {
                if (upgradePip1 != null) upgradePip1.icon.sprite = upgradeEnabledSprite;
                if (upgradePip2 != null) upgradePip2.icon.sprite = upgradeEnabledSprite;
                if (upgradePip3 != null) upgradePip3.icon.sprite = upgradeEnabledSprite;
            }

            //Remove side effect
            if(sideEffectButton != null)
            {
                if(upgrade.GetUpgradeEffects().Exists(item => item.effect == SyncDiskPreset.Effect.removeSideEffect))
                {
                    Destroy(sideEffectButton.gameObject);
                }
            }
        }
    }

    public void SelectOptionButton(int val)
    {
        selectedOption = val + 1;
        VisualUpdate();
    }

    public void InstallButton()
    {
        if(upgrade.state == UpgradesController.SyncDiskState.notInstalled)
        {
            if(selectedOption > 0)
            {
                PopupMessageController.Instance.PopupMessage("InstallSyncDisk", true, true, LButton: "Cancel", RButton: "Install");
                PopupMessageController.Instance.OnRightButton += InstallPromptSuccess;
                PopupMessageController.Instance.OnLeftButton += PopupCancel;
            }
        }
        else
        {
            if (upgrade.uninstallCost == 0 || (upgrade.uninstallCost > 0 && GameplayController.Instance.money >= upgrade.uninstallCost))
            {
                PopupMessageController.Instance.PopupMessage("UninstallSyncDisk", true, true, LButton: "Cancel", RButton: "Uninstall");
                PopupMessageController.Instance.OnRightButton += UninstallPromptSuccess;
                PopupMessageController.Instance.OnLeftButton += PopupCancel;
            }
        }
    }

    public void PopupCancel()
    {
        PopupMessageController.Instance.OnRightButton -= InstallPromptSuccess;
        PopupMessageController.Instance.OnRightButton -= UninstallPromptSuccess;
        PopupMessageController.Instance.OnRightButton -= UpgradePromptSuccess;
        PopupMessageController.Instance.OnLeftButton -= PopupCancel;
    }

    public void InstallPromptSuccess()
    {
        PopupMessageController.Instance.OnRightButton -= InstallPromptSuccess;
        PopupMessageController.Instance.OnLeftButton -= PopupCancel;

        UpgradesController.Instance.InstallSyncDisk(upgrade, selectedOption);
        VisualUpdate();
    }

    public void UninstallPromptSuccess()
    {
        PopupMessageController.Instance.OnRightButton -= UninstallPromptSuccess;
        PopupMessageController.Instance.OnLeftButton -= PopupCancel;

        if (upgrade.uninstallCost > 0 && GameplayController.Instance.money >= upgrade.uninstallCost)
        {
            GameplayController.Instance.AddMoney(-upgrade.uninstallCost, true, "Uninstall sync disk");
            UpgradesController.Instance.UninstallSyncDisk(upgrade);
            VisualUpdate();
        }
        else
        {
            UpgradesController.Instance.UninstallSyncDisk(upgrade);
            VisualUpdate();
        }
    }

    public void UpgradeButton()
    {
        PopupMessageController.Instance.PopupMessage("UpgradeSyncDisk", true, true, LButton: "Cancel", RButton: "Upgrade");
        PopupMessageController.Instance.OnRightButton += UpgradePromptSuccess;
        PopupMessageController.Instance.OnLeftButton += PopupCancel;
    }

    public void UpgradePromptSuccess()
    {
        PopupMessageController.Instance.OnRightButton -= UpgradePromptSuccess;
        PopupMessageController.Instance.OnLeftButton -= PopupCancel;

        UpgradesController.Instance.UpgradeSyncDisk(upgrade);
        VisualUpdate();
    }
}
