﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEditor;

//A scriptable object that can be used to make company presets.
//Script pass 1
[CreateAssetMenu(fileName = "address_data", menuName = "Database/Address Preset")]

public class AddressPreset : SoCustomComparison
{
    [Header("Zoning")]
    public bool debug = false;
    [Tooltip("Fits in units of this size (in tiles)")]
    [Range(0, 225)]
    public int fitsUnitSizeMin = 0;
    [Range(0, 225)]
    public int fitsUnitSizeMax = 225;
    [Tooltip("If true, units incompatible with size will be completely discounted instead of just ranked lower...")]
    public bool hardSizeLimits = false;

    [Space(7)]
    public Vector2 minMaxFloors = new Vector2(-1, 999);

    [Space(7)]
    [Tooltip("If true, the game will place at least one of these if at all possible")]
    public bool important = false;
    [Tooltip("Maximum number of instances")]
    public int maxInstances = 9999;

    [Space(7)]
    public int baseScore = 3;
    [Tooltip("Minus to base score with every instance")]
    public int baseScoreFrequencyPenalty = 1;
    [Space(7)]
    [Range(0f, 1f)]
    public float idealFootfall = 0;
    [Tooltip("How important is the correct footfall?")]
    public float footfallMultiplier = 3f;

    public List<AddressRule> addressRules = new List<AddressRule>();
    public List<BuildingPreset> limitToBuildings = new List<BuildingPreset>();

    [Tooltip("Always pick this if it is compatible")]
    public bool forcePick = false;

    [System.Serializable]
    public class AddressRule
    {
        public DistrictPreset districtPreset;
        public int scoreModifier;
    }

    [Header("Ownership")]
    [Tooltip("Does the ethnicity of this address factor in the ownership?")]
    public bool ethnicityMatters = false;
    [EnableIf("ethnicityMatters")]
    [Tooltip("Ethnicity of this address")]
    public Descriptors.EthnicGroup ethnicity = Descriptors.EthnicGroup.northAmerican;

    [Header("Compatible Layouts")]
    public List<LayoutConfiguration> compatible = new List<LayoutConfiguration>();

    [Header("Room Config")]
    public List<RoomConfiguration> roomConfig = new List<RoomConfiguration>();

    public enum AccessType { allPublic, residents, buildingInhabitants, employees, none };
    [Header("Access")]
    public AccessType access = AccessType.allPublic;
    [Tooltip("If true an AI can pass through this on the way to another place (origins, destinations unaffected)")]
    public bool canPassThrough = false;
    [Tooltip("Are open hours dictated by a company that ajoins this?")]
    public bool openHoursDicatedByAdjoiningCompany = false;
    [Tooltip("The player needs a password to enter this location")]
    public bool needsPassword = false;
    [Tooltip("Sources for a password")]
    public List<string> dictionaryPasswordSources = new List<string>();
    [Tooltip("If possible pick a password at least this long...")]
    public int minimumPasswordLength = 5;

    [Header("Purpose")]
    [Tooltip("If a company operates this address, this is the preset")]
    public CompanyPreset company;
    [Tooltip("If a residence is at this address, this is the preset")]
    public ResidencePreset residence;
    [Tooltip("Purpose/icon is known to the player at the start")]
    public bool playerKnowsPurpose = true;

    [Header("Interface")]
    public Sprite evidenceIconLarge = null;

    [Header("Signage")]
    [Range(0f, 1f)]
    public float chanceOfNameSignHorizontal = 1f;
    [Tooltip("Make a sign using this character set")]
    public Vector3 horizontalSignOffset = Vector3.zero;
    public List<NeonSignCharacters> signCharacterSet = new List<NeonSignCharacters>();
    [Range(0f, 1f)]
    public float chanceOfNameSignVertical = 1f;
    [Tooltip("Make a sign using one of these")]
    public List<GameObject> possibleSigns = new List<GameObject>();

    [Header("Special Items")]
    public List<InteractablePreset> specialItems = new List<InteractablePreset>();
    [Tooltip("Chance of a spare key being left in an adjoining lobby (will be hidden under mat, or in a plant or radiator)")]
    public float chanceOfExternalSpareKey = 0f;

    [Header("Air Vents")]
    public Vector2 airVentRange = Vector2.one;

    [Header("Security")]
    [Tooltip("If false, this uses the building's security system")]
    public bool useOwnSecuritySystem = false;
    [Tooltip("If false, this uses the breaker box contained on the floor")]
    public bool useOwnBreakerBox = false;
    [Tooltip("If triggered, does the alarm lock down the building floor?")]
    [EnableIf("useOwnSecuritySystem")]
    public bool alarmLocksDownFloor = false;

    [Header("Environment")]
    public bool overrideBuildingEnvironment = false;
    [EnableIf("overrideBuildingEnvironment")]
    public SessionData.SceneProfile sceneProfile = SessionData.SceneProfile.indoors;

    [Header("Misc")]
    [Tooltip("Are entrance doors locked by default?")]
    public bool entrancesLockedByDefault = true;
    [Tooltip("AI leaves lights on, even when empty")]
    public bool leaveLightsOn = false;
    [Tooltip("If enabled, AI leaves doesn't lock doors out of hours or when empty")]
    public bool disableLockingUp = false;
    [Tooltip("Stop this from appearing in the bottom left when the player enters")]
    public bool disableLocationInformationDisplay = false;
    [Tooltip("This will be included in the city directory")]
    public bool forceCityDirectoryInclusion = false;
    [Tooltip("The name of this will become the name of the building")]
    public bool overrideBuildingName = false;
    [Tooltip("Employees in the same building will have this as a location of authority")]
    public bool sameBuildingEmployeesAuthority = false;
    [Tooltip("Residents in the same building will have this as a location of authority")]
    public bool sameBuildingResidentsAuthority = false;
    [Tooltip("This address can feature lost & found notes")]
    public bool canFeatureLostAndFound = false;
    [Tooltip("The minimum land value for this address type")]
    [Range(0f, 1f)]
    public float minimumLandValue = 0f;
    [Tooltip("The maximum land value for this address type")]
    [Range(0f, 1f)]
    public float maximumLandValue = 1f;

    [Header("Debug")]
    [Tooltip("If true this won't be chosen in-game")]
    public bool disableThis = false;
}
