﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class PasswordButtonController : ButtonController
{
    //private bool isSetup = false;

    public void Setup(InfoWindow newParentWindow)
    {
        parentWindow = newParentWindow;
        SetupReferences();

        //password = newPassword;

        UpdateTooltipText();
        VisualUpdate();

        //isSetup = true;
    }

    private void OnEnable()
    {

    }

    private void OnDisable()
    {

    }

    private void OnDestroy()
    {

    }

    public override void VisualUpdate()
    {
        //Set sprite- use the reference in the fact class, if null use the first 'other' evidence

        UpdateTooltipText();
    }

    //On double click, open evidence
    public override void OnLeftClick()
    {
        //InterfaceController.Instance.InputCodeButton((parentWindow.passedEvidence.controller as Human).possiblePasswords[password]); //Input code
    }

    //Right click will unpair
    public override void OnRightClick()
    {
        
    }

    public override void UpdateTooltipText()
    {
        if (tooltip == null) return;

        string code = string.Empty;

        for (int i = 0; i < 4; i++)
        {
            //code += (parentWindow.passedEvidence.controller as Human).possiblePasswords[password][i];
        }

        //tooltip.mainText = Strings.Get("evidence.generic", "password " + password.ToString()) + ": " + code;
        text.text = tooltip.mainText;
        tooltip.detailText = Strings.Get("evidence.generic", "passwordguess");
    }

    public override void OnHoverStart()
    {
        text.fontStyle = FontStyles.Underline;

        //Start evidence mouse over
        //InterfaceController.Instance.NewFactButtonMouseOver(this);
    }

    public override void OnHoverEnd()
    {
        text.fontStyle = FontStyles.Normal;

        //End mouse over
        //InterfaceController.Instance.CancelCurrentFactButtonMouseOver();
    }
}
