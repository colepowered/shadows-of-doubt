﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "materialgroup_data", menuName = "Database/Decor/Material Group Preset")]

public class MaterialGroupPreset : SoCustomComparison
{
    [System.Serializable]
    public class MaterialSettings
    {
        public DesignStylePreset designStyle;
        [Range(1, 5)]
        public int weighting = 3;
    }

    public enum MaterialType { walls, floor, ceiling, other };

    public enum MaterialColour { anyPrimary, anySecondary, anyPrimaryOrNeutral, anySecondaryOrNeutral, any1, any2, any1OrNeutral, any2OrNeutral, any, primary1, primary2, secondary1, secondary2, neutral, wood, none, anyPrimaryOrSecondary };

    [System.Serializable]
    public class MaterialVariation
    {
        public string name;
        public MaterialColour main = MaterialColour.none;
        public MaterialColour colour1 = MaterialColour.any;
        public MaterialColour colour2 = MaterialColour.any;
        public MaterialColour colour3 = MaterialColour.any;
    }

    [Header("Material")]
    public Material material;

    [Header("Material Variations")]
    [ReorderableList]
    public List<MaterialVariation> variations = new List<MaterialVariation>();

    [Header("Material Properties")]
    [Range(0f, 1f)]
    public float concrete = 1f;
    [Range(0f, 1f)]
    public float plaster = 0f;
    [Range(0f, 1f)]
    public float wood = 0f;
    [Range(0f, 1f)]
    public float carpet = 0f;
    [Range(0f, 1f)]
    public float tile = 0f;
    [Range(0f, 1f)]
    public float metal = 0f;
    [Range(0f, 1f)]
    public float glass = 0f;
    [Range(0f, 1f)]
    public float fabric = 0f;

    [Tooltip("If this is assigned and the node has no floor, instead use this material.")]
    public MaterialGroupPreset noFloorReplacement;
    public bool allowFootprints = false;
    [Range(-1f, 1f)]
    public float affectFootprintDirt = 0f;
    [Tooltip("If this material is grubby, use this multiplier to add to footprint dirt")]
    public float grubFootprintDirtMultiplier = 0.05f;

    [Header("Suitability")]
    public MaterialType materialType = MaterialType.floor;
    [Range(0f, 1f)]
    public float minimumWealth = 0f;
    [ReorderableList]
    public List<MaterialSettings> designStyles = new List<MaterialSettings>();

    [ReorderableList]
    [Tooltip("The furniture is only allowed in these room types")]
    public List<RoomTypeFilter> allowedRoomFilters = new List<RoomTypeFilter>();

    [Header("In-Game")]
    public bool purchasable = true;
    public int price = 10;
    public Sprite decorSprite;
}
