using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshToggleBehaviour : SwitchSyncBehaviour
{
    public List<MeshRenderer> objectsToToggle = new List<MeshRenderer>();

    public override void SetOn(bool val)
    {
        base.SetOn(val);

        if (basicBehaviour == BasicBehaviour.hideWhenOn)
        {
            foreach (MeshRenderer g in objectsToToggle)
            {
                g.enabled = !isOn;
            }
        }
        else if (basicBehaviour == BasicBehaviour.hideWhenOff)
        {
            foreach (MeshRenderer g in objectsToToggle)
            {
                g.enabled = isOn;
            }
        }
    }
}
