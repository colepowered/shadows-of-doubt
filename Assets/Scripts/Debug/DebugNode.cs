using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class DebugNode : MonoBehaviour
{
    public NewNode node;

    public Vector3 coordinate;
    public Vector3 tileCoordinate;
    public Vector2Int localTileCoordinate;
    public bool isConnected;
    public List<NewNode.NodeAccess> accessToOtherNodes = new List<NewNode.NodeAccess>();
    public bool upperStairwellLink = false;
    public bool lowerStairwellLink = false;
    public bool isTileStairwell = false;
    public bool isTileInvertedStairwell = false;
    public NewNode.FloorTileType floorType;

    private bool displaySpawnedConnections = false;
    public List<GameObject> spawnedConnections = new List<GameObject>();

    public void Setup(NewNode newNode)
    {
        node = newNode;
        RefreshData();
    }

    [Button]
    public void RefreshData()
    {
        coordinate = node.nodeCoord;
        tileCoordinate = node.tile.globalTileCoord;
        localTileCoordinate = node.localTileCoord;
        isConnected = node.isConnected;
        accessToOtherNodes = new List<NewNode.NodeAccess>(node.accessToOtherNodes.Values);
        upperStairwellLink = node.stairwellUpperLink;
        lowerStairwellLink = node.stairwellLowerLink;
        isTileStairwell = node.tile.isStairwell;
        isTileInvertedStairwell = node.tile.isInvertedStairwell;
        floorType = node.floorType;
    }

    [Button]
    public void ToggleDisplayConnections()
    {
        while(spawnedConnections.Count > 0)
        {
            Destroy(spawnedConnections[0]);
            spawnedConnections.RemoveAt(0);
        }

        if (!displaySpawnedConnections)
        {
            foreach(NewNode.NodeAccess acc in accessToOtherNodes)
            {
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                cube.transform.SetParent(this.transform, true);
                cube.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

                cube.transform.position = acc.toNode.position + new Vector3(0, 0.5f, 0);
                cube.name = acc.name;

                spawnedConnections.Add(cube);
            }

            displaySpawnedConnections = true;
        }
        else displaySpawnedConnections = false;
    }
}
