﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapPinButtonController : ButtonController
{
    public RawImage pin;
    public InfoWindow evWindow;
    public CanvasRenderer canvasRend;

    public void Setup(InfoWindow newWindow)
    {
        evWindow = newWindow;
        base.SetupReferences();
        pin = this.gameObject.GetComponent<RawImage>();
        canvasRend = this.gameObject.GetComponent<CanvasRenderer>();
    }

    //Open evidence on click
    public override void OnLeftDoubleClick()
    {
        InterfaceController.Instance.SpawnWindow(evWindow.passedEvidence, passedEvidenceKeys: evWindow.passedKeys);
    }

    public override void OnHoverStart()
    {
        tooltip.mainText = evWindow.passedEvidence.GetNameForDataKey(evWindow.passedKeys);
    }
}
