﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using System.Linq;
using NaughtyAttributes;
using UnityEditor;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "layoutconfig_data", menuName = "Database/Decor/Layout Configuration")]

public class LayoutConfiguration : SoCustomComparison
{
    [Header("Zoning")]
    public bool assignPurpose = true;
    [DisableIf("assignPurpose")]
    public AddressPreset addressPreset;

    [Space(7)]
    public bool publicFacing = true;
    public bool isOutside = false;
    public bool isLobby = false;

    [Header("Room Configuration")]
    public List<RoomTypePreset> roomLayout = new List<RoomTypePreset>();
    [Tooltip("This room may require an internal hallway to connect far rooms")]
    public bool requiresHallway = true;
    public RoomConfiguration hallway;
    [Tooltip("How far away a room is from the entrance to place a hallway (nodes).")]
    public int hallwayDistanceThreshold = 6;
    [Tooltip("Use the building's default design style")]
    public bool useBuildingDesignStyle = false;

    [Header("Interface")]
    public bool overrideEvidencePhotoSettings = false;
    [EnableIf("overrideEvidencePhotoSettings")]
    public Vector3 relativeCamPhotoPos = new Vector3(0, 2.25f, 23f);
    [EnableIf("overrideEvidencePhotoSettings")]
    public Vector3 relativeCamPhotoEuler = new Vector3(-45f, 180f, 0);

    [Header("Doorways")]
    public List<DoorPairPreset> doorwaysNormal = new List<DoorPairPreset>();
    public List<DoorPairPreset> doorwaysFlat = new List<DoorPairPreset>();
    public List<DoorPairPreset> roomDividersLeft = new List<DoorPairPreset>();
    public List<DoorPairPreset> roomDividersCentre = new List<DoorPairPreset>();
    public List<DoorPairPreset> roomDividersRight = new List<DoorPairPreset>();
}
