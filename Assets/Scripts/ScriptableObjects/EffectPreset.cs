﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "effect_data", menuName = "Database/Effect Preset")]

public class EffectPreset : SoCustomComparison
{
    //First value is % increase/decrease
    public bool firstValueIsPercentageIncrease = false;

    //Run activation multiple times; whenever updating
    public bool runActivationOnUpdate = false;
}
