﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;
using UnityEditor;
using System.Reflection;
using System.IO;

public class CitizenOutfitController : MonoBehaviour
{
    public enum CharacterAnchor { lowerTorso, upperTorso, Head, Hat, UpperArmRight, UpperArmLeft, LowerArmRight, LowerArmLeft, HandRight, HandLeft, UpperLegRight, UpperLegLeft, LowerLegRight, LowerLegLeft, Midriff, RightFoot, LeftFoot, Hair, Glasses, ArmsParent, beard };

    [System.Serializable]
    public class AnchorConfig
    {
        public CharacterAnchor anchor;
        public Transform trans;
        public bool outline = false;
        public bool captureInSurveillance = true;
    }

    [System.Serializable]
    public class Outfit
    {
        public ClothesPreset.OutfitCategory category;
        public List<OutfitClothes> clothes = new List<OutfitClothes>();
    }

    [System.Serializable]
    public class OutfitClothes
    {
        public string clothes;
        public List<ClothesPreset.ClothesTags> tags = new List<ClothesPreset.ClothesTags>();
        public Color baseColor = Color.clear;
        public Color color1 = Color.clear;
        public Color color2 = Color.clear;
        public Color color3 = Color.clear;
        public bool borrowed = false;

        //NonSerialized
        [System.NonSerialized]
        public Dictionary<CharacterAnchor, List<MeshRenderer>> spawned = new Dictionary<CharacterAnchor, List<MeshRenderer>>();
        [System.NonSerialized]
        public List<Material> materials;
        [System.NonSerialized]
        public int rank = 2;
        [System.NonSerialized]
        public bool incomplete = false;
        [System.NonSerialized]
        public bool loadedThisCycle = false;
    }

    public struct BackupCovering
    {
        public Outfit outfit;
        public ClothesPreset preset;
    }

    public class NewClothingCreation
    {
        public GameObject newPrefab;
        public Vector3 offset;
        public Vector3 euler;
    }

    public enum ClothingCreatorDirectory { Tops, Bottoms, Hats, Heads, Shoes, Underwear, Undressed};

    public enum Expression { neutral, angry, sad, surprised, happy, asleep};

    [System.Serializable]
    public class ExpressionSetup
    {
        public Expression expression;
        public Vector3 eyebrowsEuler;
        public float eyebrowsRaise;
        public float eyeHeightMultiplier;
        public bool allowBlinking = false;
    }

    [Header("Components/Anchors")]
    public Human human;
    public LODGroup lod;
    public MeshRenderer distantLOD;
    public bool isPoser = false;
    public ScenePoserController poser;
    [ReorderableList]
    public List<AnchorConfig> anchorConfig = new List<AnchorConfig>();
    public Dictionary<CharacterAnchor, Transform> anchorReference = new Dictionary<CharacterAnchor, Transform>(); //Quick access reference for getting anchor transforms
    public Transform pupilParent;
    public Transform leftPupil;
    public Transform rightPupil;
    public Transform eyebrowParent;
    public Transform rightEyebrow;
    public Transform leftEyebrow;
    public Transform mouth;
    [ReorderableList]
    public List<MeshRenderer> eyeRenderers = new List<MeshRenderer>();
    [ReorderableList]
    public List<MeshRenderer> eyebrowRenderers = new List<MeshRenderer>();
    public MeshRenderer mouthRenderer;
    [ReadOnly]
    public Vector3 pupilParentOffset;
    [ReorderableList]
    public List<ExpressionSetup> expressions = new List<ExpressionSetup>();
    public Dictionary<Expression, ExpressionSetup> expressionReference = new Dictionary<Expression, ExpressionSetup>(); //Quick access reference for getting expressions
    [Space(5)]
    public Material bluePupil;
    public Material greenPupil;
    public Material brownPupil;
    public Material greyPupil;

    [Header("Outfits")]
    public ClothesPreset.OutfitCategory loadedOutfit = ClothesPreset.OutfitCategory.casual;
    public ClothesPreset.OutfitCategory currentOutfit = ClothesPreset.OutfitCategory.casual;
    public ClothesPreset.OutfitCategory previousOutfit = ClothesPreset.OutfitCategory.casual;
    [System.NonSerialized]
    public List<OutfitClothes> currentlyLoadedClothes = new List<OutfitClothes>();
    public List<MeshRenderer> allCurrentMeshes = new List<MeshRenderer>();
    public List<MeshFilter> allCurrentMeshFilters = new List<MeshFilter>();

    //Needed for quick checking of hat/hair rendering
    private ClothesPreset currentHair;
    private MeshRenderer currentHairRend;
    private ClothesPreset currentHat;
    private MeshRenderer currentHatRend;
    [ReorderableList]
    public List<Outfit> outfits = new List<Outfit>();

    [Header("Debug")]
    public List<MeshRenderer> debugRenderers = new List<MeshRenderer>();
    public bool debugOverride = false;
    [EnableIf("debugOverride")]
    public OccupationPreset debugOverrideJob;
    [EnableIf("debugOverride")]
    public Human.Gender debugOverrideGender = Human.Gender.female;
    [EnableIf("debugOverride")]
    public Descriptors.BuildType debugOverrideBuild = Descriptors.BuildType.average;
    [EnableIf("debugOverride")]
    public Descriptors.HairStyle debugOverrideHair = Descriptors.HairStyle.shortHair;
    [EnableIf("debugOverride")]
    public Descriptors.EyeColour debugOverrideEyeColour = Descriptors.EyeColour.blueEyes;
    [EnableIf("debugOverride")]
    public Human.ShoeType debugOverrideShoeType = Human.ShoeType.normal;
    [EnableIf("debugOverride")]
    [Range(0f, 1f)]
    public float debugOverrideLipstick = 0f;
    [EnableIf("debugOverride")]
    public Color debugOverrideSkinColour = Color.white;
    [EnableIf("debugOverride")]
    public Color debugOverrideHairColour = Color.black;
    [EnableIf("debugOverride")]
    public Expression debugOverrideExpression = Expression.neutral;
    [EnableIf("debugOverride")]
    [Range(0f, 1f)]
    public float debugOverrideGrub = 0.1f;
    [Space(5)]
    public bool enableDebugLog = false;
    public List<string> outfitDebug = new List<string>();

    [Header("Load Specific")]
    [ReorderableList]
    public List<ClothesPreset> outfitToLoad = new List<ClothesPreset>();

    [Header("Create New")]
    public string newClothingName;
    public ClothingCreatorDirectory directory = ClothingCreatorDirectory.Tops;
    [ReorderableList]
    public List<GameObject> newClothingComponents = new List<GameObject>();
    [Tooltip("If this is true, you only need the right side arms and/or legs present as models. The opposite side will be created with flipping.")]
    public bool CreateFlippedArmsAndLegsFromRightSide = true;

    //Covered anchors and rank
    Dictionary<CharacterAnchor, int> coveredAnchors = new Dictionary<CharacterAnchor, int>();

    private void Awake()
    {
        //Setup a dictionary reference
        foreach(AnchorConfig a in anchorConfig)
        {
            anchorReference.Add(a.anchor, a.trans);
        }

        foreach(ExpressionSetup e in expressions)
        {
            expressionReference.Add(e.expression, e);
        }
    }

    //Generate outfits
    public void GenerateOutfits(bool forceSpecificDebugOutfit = false)
    {
        outfits.Clear();
        outfitDebug.Clear();

        List<ClothesPreset.OutfitCategory> categories = null;
        List<ClothesPreset> allClothes = null;
        Dictionary<string, ClothesPreset> clothesDictionary = null;

        OccupationPreset job = null;
        Human.Gender gender = human.birthGender;
        Descriptors.BuildType build = human.descriptors.build;
        Descriptors.HairStyle hair = human.descriptors.hairType;
        Descriptors.EyeColour eyes = human.descriptors.eyeColour;
        Human.ShoeType shoes = human.descriptors.footwear;
        Color skinColour = human.descriptors.skinColour;
        Color hairColour = human.descriptors.hairColour;
        Color mouthColour = new Color(110f / 255f, 80f / 255f, 80f / 255f, 1);
        bool editor = false;

        //Editor implementation
        if (Toolbox.Instance == null)
        {
            categories = ClothesPreset.OutfitCategory.GetValues(typeof(ClothesPreset.OutfitCategory)).Cast<ClothesPreset.OutfitCategory>().ToList();
            allClothes = AssetLoader.Instance.GetAllClothes();

            clothesDictionary = new Dictionary<string, ClothesPreset>();

            foreach(ClothesPreset cl in allClothes)
            {
                clothesDictionary.Add(cl.name, cl);
            }

            editor = true;

            if (debugOverride)
            {
                job = debugOverrideJob;
                gender = debugOverrideGender;
                hair = debugOverrideHair;
                build = debugOverrideBuild;
                skinColour = debugOverrideSkinColour;
                eyes = debugOverrideEyeColour;
                hairColour = debugOverrideHairColour;
                shoes = debugOverrideShoeType;

                mouthColour = Color.Lerp(new Color(110f / 255f, 80f / 255f, 80f / 255f, 1), new Color(220f / 255f, 0, 0, 1), debugOverrideLipstick);

                human.humility = UnityEngine.Random.Range(0f, 1f);
                human.extraversion = UnityEngine.Random.Range(0f, 1f);
                human.emotionality = UnityEngine.Random.Range(0f, 1f);
                human.agreeableness = UnityEngine.Random.Range(0f, 1f);
                human.conscientiousness = UnityEngine.Random.Range(0f, 1f);
                human.creativity = UnityEngine.Random.Range(0f, 1f);

                if(gender == Human.Gender.male)
                {
                    human.genderScale = UnityEngine.Random.Range(0.5f, 1f);
                }
                else
                {
                    human.genderScale = UnityEngine.Random.Range(0f, 0.5f);
                }
            }
        }
        else
        {
            categories = Toolbox.Instance.allOutfitCategories;
            allClothes = Toolbox.Instance.allClothes;
            clothesDictionary = Toolbox.Instance.clothesDictionary;

            if(human.job != null) job = human.job.preset;
            gender = human.gender;
            build = human.descriptors.build;
            hair = human.descriptors.hairType;
            eyes = human.descriptors.eyeColour;
            skinColour = human.descriptors.skinColour;
            hairColour = human.descriptors.hairColour;
            shoes = human.descriptors.footwear;

            //For easiness, side with a binary gender for this (we can only make so many clothes!)
            if (gender == Human.Gender.nonBinary)
            {
                if (human.genderScale < 0.5f)
                {
                    gender = Human.Gender.female;
                }
                else gender = Human.Gender.male;
            }

            //Calculate lipstick
            //if (human.genderScale <= 0.4f)
            //{
            //    float lipstickScale = human.extraversion * 0.5f;

            //    if(human.characterTraits.Exists(item => item.trait.name == "Flirty"))
            //    {
            //        lipstickScale += Toolbox.Instance.GetPsuedoRandomNumberContained(0.35f, 0.5f, human.seed, out human.seed);
            //    }

            //    Color lipstick = SocialStatistics.Instance.lipstickColours[Toolbox.Instance.GetPsuedoRandomNumber(0, SocialStatistics.Instance.lipstickColours.Count, human.humanID.ToString())];
            //    mouthColour = Color.Lerp(skinColour * 0.9f, lipstick * 0.5f, lipstickScale);
            //}
            //else
            //{
            //    Color lipstick = SocialStatistics.Instance.lipstickColours[Toolbox.Instance.GetPsuedoRandomNumber(0, SocialStatistics.Instance.lipstickColours.Count, human.humanID.ToString())];
            //    mouthColour = Color.Lerp(skinColour * 0.9f, lipstick * 0.5f, Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 0.1f, human.seed, out human.seed));
            //}
        }

        //Load facial feature materials
        foreach (MeshRenderer r in eyeRenderers)
        {
            if(eyes == Descriptors.EyeColour.blueEyes)
            {
                r.sharedMaterial = bluePupil;
            }
            else if (eyes == Descriptors.EyeColour.greenEyes)
            {
                r.sharedMaterial = greenPupil;
            }
            else if (eyes == Descriptors.EyeColour.brownEyes)
            {
                r.sharedMaterial = brownPupil;
            }
            else if (eyes == Descriptors.EyeColour.greyEyes)
            {
                r.sharedMaterial = greyPupil;
            }
        }

        //Blend eyebrown colour with skin colour
        if(eyebrowRenderers.Count > 0)
        {
            Color eyebrowColour = Color.Lerp(hairColour, skinColour, 0.4f); //Lerp with skin colour for averaging and make darker
            Material eyeBrowMat = Instantiate(eyebrowRenderers[0].sharedMaterial);
            eyeBrowMat.SetColor("_BaseColor", eyebrowColour);

            foreach (MeshRenderer r in eyebrowRenderers)
            {
                r.sharedMaterial = eyeBrowMat;
            }
        }

        //Set mouth colour
        //Material mouthMat = Instantiate(mouthRenderer.sharedMaterial);
        //mouthMat.SetColor("_BaseColor", mouthColour);
        //mouthRenderer.sharedMaterial = mouthMat;

        //Keep track of missing components so we can search for others later...
        Dictionary <ClothesPreset.OutfitCategory, List<CharacterAnchor>> missingComponents = new Dictionary<ClothesPreset.OutfitCategory, List<CharacterAnchor>>();

        foreach (ClothesPreset.OutfitCategory cat in categories)
        {
            if (forceSpecificDebugOutfit && cat != ClothesPreset.OutfitCategory.undressed) continue; //We only want the naked outfit for loading specific outfit...

            if((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("> Category " + cat);

            //Gather list of required components; all of these enums
            List<CharacterAnchor> requiredComponents = CharacterAnchor.GetValues(typeof(CharacterAnchor)).Cast<CharacterAnchor>().ToList();

            //Remove hat
            //if(cat != ClothesPreset.OutfitCategory.outdoorsCasual && cat != ClothesPreset.OutfitCategory.outdoorsSmart && cat != ClothesPreset.OutfitCategory.outdoorsWork && cat != ClothesPreset.OutfitCategory.work)
            //{
            //    requiredComponents.Remove(CharacterAnchor.Hat);
            //}

            //Gather possible outfits...
            List<ClothesPreset> possiblePresets = new List<ClothesPreset>();

            List<ClothesPreset> searchInClothes = allClothes;
            bool includePool = true;

            if(cat == ClothesPreset.OutfitCategory.work && job != null && job.workOutfit.Count > 0)
            {
                searchInClothes = job.workOutfit;
                includePool = false;
            }

            //Add option using this citizen's traits...
            foreach (ClothesPreset preset in searchInClothes)
            {
                if(preset == null)
                {
                    Debug.Log("Null clothes preset, probably from job " + job.name);
                }

                if (includePool && !preset.includeInPersonalityMatching) continue;
                if (!preset.suitableForGenders.Contains(gender)) continue; //If not suitable for gender
                if (!preset.outfitCategories.Contains(cat)) continue; //If not in this category, skip...
                if (!preset.suitableForBuilds.Contains(build)) continue; //If not suitable for build

                if(preset.useWealthValues)
                {
                    if (human.societalClass < preset.minimumWealth) continue;
                    else if (human.societalClass > preset.maximumWealth) continue;
                }

                if (preset.enableFacialFeatureSetup)
                {
                    if (!preset.suitableForHairstyle.Contains(hair)) continue; //If not suitable for hair type
                }

                //if(preset.setFootwear && preset.footwear != Human.ShoeType.barefoot)
                //{
                //    if (preset.footwear != shoes) continue;
                //}

                possiblePresets.Add(preset);
            }

            //Choose outfits...
            Outfit newOutfit = new Outfit();
            newOutfit.category = cat;

            List<CharacterAnchor> foundThisCycle = new List<CharacterAnchor>();

            for (int i = 0; i < requiredComponents.Count; i++)
            {
                CharacterAnchor req = requiredComponents[i];
                if (foundThisCycle.Contains(req)) continue;

                if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("Searching for " + req + "...");

                //Find clothes with a matching requirement
                List<ClothesPreset> matchingClothes = possiblePresets.FindAll(item => item.covers.Contains(req));

                List<ClothesPreset> matchingWithFrequency = new List<ClothesPreset>();

                foreach(ClothesPreset cp in matchingClothes)
                {
                    bool compPass = false;

                    foreach(CharacterAnchor a in cp.covers)
                    {
                        if(requiredComponents.Contains(a) && !foundThisCycle.Contains(a))
                        {
                            compPass = true;
                        }
                        else
                        {
                            compPass = false;
                            break;
                        }
                    }

                    if (!compPass) continue; //Skip this if not all components are required...

                    if(cp.incompatibility.Count > 0)
                    {
                        bool comp = true;

                        foreach(ClothesPreset.IncompatibilitySetting inc in cp.incompatibility)
                        {
                            if(inc.incompatibleIf == ClothesPreset.Incompatibility.inThisCategory)
                            {
                                if(inc.tags.Count > 0)
                                {
                                    foreach(ClothesPreset.ClothesTags t in inc.tags)
                                    {
                                        if (newOutfit.clothes.Exists(item => item.tags.Contains(t)))
                                        {
                                            comp = false;
                                            break;
                                        }
                                    }

                                    if (!comp) break;
                                }
                                else if(inc.featured != null)
                                {
                                    if (newOutfit.clothes.Exists(item => item.clothes == inc.featured.name))
                                    {
                                        comp = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                foreach (Outfit o in outfits)
                                {
                                    if(inc.tags.Count > 0)
                                    {
                                        foreach (ClothesPreset.ClothesTags t in inc.tags)
                                        {
                                            if (newOutfit.clothes.Exists(item => item.tags.Contains(t)))
                                            {
                                                comp = false;
                                                break;
                                            }
                                        }

                                        if (!comp) break;
                                    }
                                    else if(inc.featured != null)
                                    {
                                        if (o.clothes.Exists(item => item.clothes == inc.featured.name))
                                        {
                                            comp = false;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (!comp) break;
                        }

                        if (!comp) continue;
                    }

                    int traitScore = 0;
                    int hexacoScore = 0;

                    if (cp.useTraits && cp.characterTraits.Count > 0)
                    {
                        GetChance(human, ref cp.characterTraits, out traitScore);
                    }

                    //Get matching stats...
                    if(cp.useHEXACO)
                    {
                        hexacoScore = human.GetHexacoScore(ref cp.hexaco);
                    }

                    int totalScore = cp.baseChance + traitScore + hexacoScore;

                    ////Use a score of 1 if the above are both disabled
                    //if(!cp.useHEXACO && !cp.useTraits)
                    //{
                    //    totalScore = 1;
                    //}

                    for (int u = 0; u < totalScore; u++)
                    {
                        matchingWithFrequency.Add(cp);
                    }
                }

                if(matchingWithFrequency.Count > 0)
                {
                    int index = 0;

                    if(!editor)
                    {
                        index = Toolbox.Instance.GetPsuedoRandomNumberContained(0, matchingWithFrequency.Count, human.seed, out human.seed);
                    }
                    else
                    {
                        index = UnityEngine.Random.Range(0, matchingWithFrequency.Count);
                    }

                    ClothesPreset p = matchingWithFrequency[index];

                    OutfitClothes newClothes = new OutfitClothes();
                    newClothes.clothes = p.name;
                    newClothes.tags = new List<ClothesPreset.ClothesTags>(p.tags);
                    newClothes.rank = p.priority;
                    newClothes.borrowed = false;

                    //Compile colours...
                    if(p.baseColourSource != ClothesPreset.ClothingColourSource.none)
                    {
                        //Get underneath colours
                        if(p.baseColourSource == ClothesPreset.ClothingColourSource.underneathColour1 || p.baseColourSource == ClothesPreset.ClothingColourSource.underneathColour2 || p.baseColourSource == ClothesPreset.ClothingColourSource.underneathColour3)
                        {
                            newClothes.baseColor = GetColourFromUnderneath(p, cat, p.baseColourSource, ref clothesDictionary);
                        }
                        else if(p.baseColourSource == ClothesPreset.ClothingColourSource.white)
                        {
                            newClothes.baseColor = Color.white;
                        }
                        else if(p.baseColourSource == ClothesPreset.ClothingColourSource.garment)
                        {
                            if (p.colourBase.Count <= 0) Game.LogError("No palettes for garment " + p.name + " colourBase");
                            newClothes.baseColor = PickColourFromPalette(ref p.colourBase, p.name);
                        }
                        else if(p.baseColourSource == ClothesPreset.ClothingColourSource.skin)
                        {
                            newClothes.baseColor = skinColour;
                        }
                        else if(p.baseColourSource == ClothesPreset.ClothingColourSource.hair)
                        {
                            newClothes.baseColor = hairColour;
                        }
                        else if(p.baseColourSource == ClothesPreset.ClothingColourSource.workUniformColour && human.job != null && human.job.employer != null)
                        {
                            newClothes.baseColor = human.job.employer.uniformColour;
                        }
                    }

                    if (p.colour1Source != ClothesPreset.ClothingColourSource.none)
                    {
                        //Get underneath colours
                        if (p.colour1Source == ClothesPreset.ClothingColourSource.underneathColour1 || p.colour1Source == ClothesPreset.ClothingColourSource.underneathColour2 || p.colour1Source == ClothesPreset.ClothingColourSource.underneathColour3)
                        {
                            newClothes.color1 = GetColourFromUnderneath(p, cat, p.colour1Source, ref clothesDictionary);
                        }
                        else if (p.colour1Source == ClothesPreset.ClothingColourSource.white)
                        {
                            newClothes.color1 = Color.white;
                        }
                        else if (p.colour1Source == ClothesPreset.ClothingColourSource.garment)
                        {
                            if (p.colour1.Count <= 0) Game.LogError("No palettes for garment " + p.name + " colour1");
                            newClothes.color1 = PickColourFromPalette(ref p.colour1, p.name);
                        }
                        else if (p.colour1Source == ClothesPreset.ClothingColourSource.skin)
                        {
                            newClothes.color1 = skinColour;
                        }
                        else if (p.colour1Source == ClothesPreset.ClothingColourSource.hair)
                        {
                            newClothes.color1 = hairColour;
                        }
                        else if (p.baseColourSource == ClothesPreset.ClothingColourSource.workUniformColour && human.job != null && human.job.employer != null)
                        {
                            newClothes.color1 = human.job.employer.uniformColour;
                        }
                    }

                    if (p.colour2Source != ClothesPreset.ClothingColourSource.none)
                    {
                        //Get underneath colours
                        if (p.colour2Source == ClothesPreset.ClothingColourSource.underneathColour1 || p.colour2Source == ClothesPreset.ClothingColourSource.underneathColour2 || p.colour2Source == ClothesPreset.ClothingColourSource.underneathColour3)
                        {
                            newClothes.color2 = GetColourFromUnderneath(p, cat, p.colour2Source, ref clothesDictionary);
                        }
                        else if (p.colour2Source == ClothesPreset.ClothingColourSource.white)
                        {
                            newClothes.color2 = Color.white;
                        }
                        else if (p.colour2Source == ClothesPreset.ClothingColourSource.garment)
                        {
                            if (p.colour2.Count <= 0) Game.LogError("No palettes for garment " + p.name + " colour2");
                            newClothes.color2 = PickColourFromPalette(ref p.colour2, p.name);
                        }
                        else if (p.colour2Source == ClothesPreset.ClothingColourSource.skin)
                        {
                            newClothes.color2 = skinColour;
                        }
                        else if (p.colour2Source == ClothesPreset.ClothingColourSource.hair)
                        {
                            newClothes.color2 = hairColour;
                        }
                        else if (p.baseColourSource == ClothesPreset.ClothingColourSource.workUniformColour && human.job != null && human.job.employer != null)
                        {
                            newClothes.color2 = human.job.employer.uniformColour;
                        }
                    }

                    if (p.colour3Source != ClothesPreset.ClothingColourSource.none)
                    {
                        //Get underneath colours
                        if (p.colour3Source == ClothesPreset.ClothingColourSource.underneathColour1 || p.colour3Source == ClothesPreset.ClothingColourSource.underneathColour2 || p.colour3Source == ClothesPreset.ClothingColourSource.underneathColour3)
                        {
                            newClothes.color3 = GetColourFromUnderneath(p, cat, p.colour3Source, ref clothesDictionary);
                        }
                        if (p.colour3Source == ClothesPreset.ClothingColourSource.white)
                        {
                            newClothes.color3 = Color.white;
                        }
                        else if (p.colour3Source == ClothesPreset.ClothingColourSource.garment)
                        {
                            if (p.colour3.Count <= 0) Game.LogError("No palettes for garment " + p.name + " colour3");
                            newClothes.color3 = PickColourFromPalette(ref p.colour3, p.name);
                        }
                        else if (p.colour3Source == ClothesPreset.ClothingColourSource.skin)
                        {
                            newClothes.color3 = skinColour;
                        }
                        else if (p.colour3Source == ClothesPreset.ClothingColourSource.hair)
                        {
                            newClothes.color3 = hairColour;
                        }
                        else if (p.baseColourSource == ClothesPreset.ClothingColourSource.workUniformColour && human.job != null && human.job.employer != null)
                        {
                            newClothes.color3 = human.job.employer.uniformColour;
                        }
                    }

                    newOutfit.clothes.Add(newClothes);
                    if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("... Adding " + p.name);
                    foundThisCycle.AddRange(p.covers);
                }
            }

            foreach(CharacterAnchor ca in foundThisCycle)
            {
                requiredComponents.Remove(ca);
            }

            //Rank clothes by priority
            newOutfit.clothes.Sort((p2, p1) => p1.rank.CompareTo(p2.rank)); //Highest first

            outfits.Add(newOutfit);

            //Save missing components
            foreach(CharacterAnchor comp in requiredComponents)
            {
                if(!missingComponents.ContainsKey(cat))
                {
                    missingComponents.Add(cat, new List<CharacterAnchor>());
                }

                missingComponents[cat].Add(comp);
            }
        }

        if (forceSpecificDebugOutfit)
        {
            foreach (ClothesPreset.OutfitCategory cat in categories)
            {
                if (cat == ClothesPreset.OutfitCategory.undressed) continue; //We only want the naked outfit for loading specific outfits...

                //Gather list of required components; all of these enums
                List<CharacterAnchor> requiredComponents = CharacterAnchor.GetValues(typeof(CharacterAnchor)).Cast<CharacterAnchor>().ToList();

                //Remove hat requirement indoors
                if (cat != ClothesPreset.OutfitCategory.outdoorsCasual && cat != ClothesPreset.OutfitCategory.outdoorsSmart && cat != ClothesPreset.OutfitCategory.outdoorsWork)
                {
                    requiredComponents.Remove(CharacterAnchor.Hat);
                }

                //Choose outfits...
                Outfit newOutfit = new Outfit();
                newOutfit.category = cat;

                List<CharacterAnchor> foundThisCycle = new List<CharacterAnchor>();

                foreach (ClothesPreset p in outfitToLoad)
                {
                    OutfitClothes newClothes = new OutfitClothes();
                    newClothes.clothes = p.name;
                    newClothes.tags = new List<ClothesPreset.ClothesTags>(p.tags);
                    newClothes.rank = p.priority;
                    newClothes.borrowed = false;

                    //Compile colours...
                    if (p.baseColourSource != ClothesPreset.ClothingColourSource.none)
                    {
                        if (p.baseColourSource == ClothesPreset.ClothingColourSource.white)
                        {
                            newClothes.baseColor = Color.white;
                        }
                        else if (p.baseColourSource == ClothesPreset.ClothingColourSource.garment)
                        {
                            if (p.colourBase.Count <= 0) Game.LogError("No palettes for garment " + p.name + " colourBase");
                            newClothes.baseColor = PickColourFromPalette(ref p.colourBase, p.name);
                        }
                        else if (p.baseColourSource == ClothesPreset.ClothingColourSource.skin)
                        {
                            newClothes.baseColor = skinColour;
                        }
                        else if (p.baseColourSource == ClothesPreset.ClothingColourSource.hair)
                        {
                            newClothes.baseColor = hairColour;
                        }
                        else if (p.baseColourSource == ClothesPreset.ClothingColourSource.workUniformColour && human.job != null && human.job.employer != null)
                        {
                            newClothes.baseColor = human.job.employer.uniformColour;
                        }
                    }

                    if (p.colour1Source != ClothesPreset.ClothingColourSource.none)
                    {
                        if (p.colour1Source == ClothesPreset.ClothingColourSource.white)
                        {
                            newClothes.color1 = Color.white;
                        }
                        else if (p.colour1Source == ClothesPreset.ClothingColourSource.garment)
                        {
                            if (p.colour1.Count <= 0) Game.LogError("No palettes for garment " + p.name + " colour1");
                            newClothes.color1 = PickColourFromPalette(ref p.colour1, p.name);
                        }
                        else if (p.colour1Source == ClothesPreset.ClothingColourSource.skin)
                        {
                            newClothes.color1 = skinColour;
                        }
                        else if (p.colour1Source == ClothesPreset.ClothingColourSource.hair)
                        {
                            newClothes.color1 = hairColour;
                        }
                        else if (p.baseColourSource == ClothesPreset.ClothingColourSource.workUniformColour && human.job != null && human.job.employer != null)
                        {
                            newClothes.color1 = human.job.employer.uniformColour;
                        }
                    }

                    if (p.colour2Source != ClothesPreset.ClothingColourSource.none)
                    {
                        if (p.colour2Source == ClothesPreset.ClothingColourSource.white)
                        {
                            newClothes.color2 = Color.white;
                        }
                        else if (p.colour2Source == ClothesPreset.ClothingColourSource.garment)
                        {
                            if (p.colour2.Count <= 0) Game.LogError("No palettes for garment " + p.name + " colour2");
                            newClothes.color2 = PickColourFromPalette(ref p.colour2, p.name);
                        }
                        else if (p.colour2Source == ClothesPreset.ClothingColourSource.skin)
                        {
                            newClothes.color2 = skinColour;
                        }
                        else if (p.colour2Source == ClothesPreset.ClothingColourSource.hair)
                        {
                            newClothes.color2 = hairColour;
                        }
                        else if (p.baseColourSource == ClothesPreset.ClothingColourSource.workUniformColour && human.job != null && human.job.employer != null)
                        {
                            newClothes.color2 = human.job.employer.uniformColour;
                        }
                    }

                    if (p.colour3Source != ClothesPreset.ClothingColourSource.none)
                    {
                        if (p.colour3Source == ClothesPreset.ClothingColourSource.white)
                        {
                            newClothes.color3 = Color.white;
                        }
                        else if (p.colour3Source == ClothesPreset.ClothingColourSource.garment)
                        {
                            if (p.colour3.Count <= 0) Game.LogError("No palettes for garment " + p.name + " colour3");
                            newClothes.color3 = PickColourFromPalette(ref p.colour3, p.name);
                        }
                        else if (p.colour3Source == ClothesPreset.ClothingColourSource.skin)
                        {
                            newClothes.color3 = skinColour;
                        }
                        else if (p.colour3Source == ClothesPreset.ClothingColourSource.hair)
                        {
                            newClothes.color3 = hairColour;
                        }
                        else if (p.baseColourSource == ClothesPreset.ClothingColourSource.workUniformColour && human.job != null && human.job.employer != null)
                        {
                            newClothes.color3 = human.job.employer.uniformColour;
                        }
                    }

                    newOutfit.clothes.Add(newClothes);
                    //outfitDebug.Add("... Adding " + p.name);
                    foundThisCycle.AddRange(p.covers);
                }

                foreach (CharacterAnchor ca in foundThisCycle)
                {
                    requiredComponents.Remove(ca);
                }

                outfits.Add(newOutfit);

                //Save missing components
                foreach (CharacterAnchor comp in requiredComponents)
                {
                    if (!missingComponents.ContainsKey(cat))
                    {
                        missingComponents.Add(cat, new List<CharacterAnchor>());
                    }

                    missingComponents[cat].Add(comp);
                }
            }
        }

        //Search existing outfits to replace with components that aren't found...
        foreach (KeyValuePair<ClothesPreset.OutfitCategory, List<CharacterAnchor>> pair in missingComponents)
        {
            if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add(" -> Outfit " + pair.Key + " is missing " + pair.Value.Count + " components <-");

            //The list to check against for replacements; can be different for each category...
            List<ClothesPreset.OutfitCategory> backupCheckList = new List<ClothesPreset.OutfitCategory>();

            Outfit outfitWithMissing = outfits.Find(item => item.category == pair.Key);

            //Backup outfits: Steal clothing elements from these...
            if (pair.Key == ClothesPreset.OutfitCategory.bed || pair.Key == ClothesPreset.OutfitCategory.underwear)
            {
                backupCheckList.Add(ClothesPreset.OutfitCategory.undressed);
            }
            else if (pair.Key == ClothesPreset.OutfitCategory.casual)
            {
                backupCheckList.Add(ClothesPreset.OutfitCategory.underwear);
                backupCheckList.Add(ClothesPreset.OutfitCategory.undressed);
            }
            else if(pair.Key == ClothesPreset.OutfitCategory.work || pair.Key == ClothesPreset.OutfitCategory.smart || pair.Key == ClothesPreset.OutfitCategory.outdoorsCasual)
            {
                backupCheckList.Add(ClothesPreset.OutfitCategory.casual);
                backupCheckList.Add(ClothesPreset.OutfitCategory.underwear);
                backupCheckList.Add(ClothesPreset.OutfitCategory.undressed);
            }
            else if (pair.Key == ClothesPreset.OutfitCategory.outdoorsSmart)
            {
                backupCheckList.Add(ClothesPreset.OutfitCategory.outdoorsCasual); //Steal trenchcoat from this...
                backupCheckList.Add(ClothesPreset.OutfitCategory.smart);
                backupCheckList.Add(ClothesPreset.OutfitCategory.casual);
                backupCheckList.Add(ClothesPreset.OutfitCategory.underwear);
                backupCheckList.Add(ClothesPreset.OutfitCategory.undressed);
            }
            else if (pair.Key == ClothesPreset.OutfitCategory.outdoorsWork)
            {
                backupCheckList.Add(ClothesPreset.OutfitCategory.outdoorsCasual); //Steal trenchcoat from this...
                backupCheckList.Add(ClothesPreset.OutfitCategory.work);
                backupCheckList.Add(ClothesPreset.OutfitCategory.casual);
                backupCheckList.Add(ClothesPreset.OutfitCategory.underwear);
                backupCheckList.Add(ClothesPreset.OutfitCategory.undressed);
            }

            for (int i = 0; i < backupCheckList.Count; i++)
            {
                ClothesPreset.OutfitCategory backupCat = backupCheckList[i];
                Outfit matchingOutfit = outfits.Find(item => item.category == backupCat);

                if (matchingOutfit != null)
                {
                    List<CharacterAnchor> foundThisCycle = new List<CharacterAnchor>();

                    for (int l = 0; l < pair.Value.Count; l++)
                    {
                        CharacterAnchor missing = pair.Value[l];
                        if (foundThisCycle.Contains(missing)) continue;

                        if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("Searching for " + missing + " in " + backupCat + "...");

                        bool found = false;

                        foreach (OutfitClothes cl in matchingOutfit.clothes)
                        {
                            if (cl.borrowed)
                            {
                                continue; //Ignore borrowed...
                            }

                            ClothesPreset p = clothesDictionary[cl.clothes];

                            //All of these must match...
                            bool compPass = false;

                            //Scan to see if all the model parts are available/unassigned...
                            //Must contain what we're looking for here...
                            if(p.covers.Contains(missing))
                            {
                                foreach (CharacterAnchor ca in p.covers)
                                {
                                    if (pair.Value.Contains(ca) && !foundThisCycle.Contains(ca))
                                    {
                                        compPass = true;
                                    }
                                    else if(p.onlyChooseIfAllModelPartsAreAvailable)
                                    {
                                        compPass = false;
                                        break;
                                    }
                                }
                            }

                            if (compPass)
                            {
                                if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog)
                                {
                                    List<string> covering = new List<string>();
                                    foreach (CharacterAnchor a in p.covers) covering.Add(" " + a.ToString());
                                    outfitDebug.Add("> " + cl.clothes + " borrowed from outfit " + backupCat + " covering: " + covering);
                                }

                                OutfitClothes newClothes = new OutfitClothes();
                                newClothes.clothes = p.name;
                                newClothes.tags = new List<ClothesPreset.ClothesTags>(p.tags);
                                newClothes.rank = p.priority;

                                //Copy colours from existing...
                                newClothes.baseColor = cl.baseColor;
                                newClothes.color1 = cl.color1;
                                newClothes.color2 = cl.color2;
                                newClothes.color3 = cl.color3;

                                //Not part of the original outfit (borrowed)
                                newClothes.borrowed = true;

                                outfitWithMissing.clothes.Add(newClothes);

                                //Rank clothes by priority
                                outfitWithMissing.clothes.Sort((p2, p1) => p1.rank.CompareTo(p2.rank)); //Highest first

                                foundThisCycle.AddRange(p.covers);
                                found = true;

                                break;
                            }
                        }

                        if(!found)
                        {
                            if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("Unable to find " + missing + " in " + backupCat);
                        }
                    }

                    foreach (CharacterAnchor a in foundThisCycle)
                    {
                        pair.Value.Remove(a);
                    }
                }

                if (pair.Value.Count <= 0)
                {
                    break;
                }
            }
        }

        if (editor)
        {
            human.genderScale = 0.5f;
            human.humility = 0f;
            human.extraversion = 0f;
            human.emotionality = 0f;
            human.agreeableness = 0f;
            human.conscientiousness = 0f;
            human.creativity = 0f;
        }
    }

    //Get body part transform
    public Transform GetBodyAnchor(CharacterAnchor anchor)
    {
        Transform ret = null;

        if(!anchorReference.TryGetValue(anchor, out ret))
        {
            try
            {
                Debug.LogError("Unable to get citizen body anchor " + anchor + ": " + human.GetCitizenName());
            }
            catch
            {

            }
        }

        return ret;
    }

    //Make sure this citizen is clothed, based on current/last outfit
    public void MakeClothed()
    {
        //Use outdoors outfit...
        bool outdoorsNeeded = false;

        if(human.ai != null && human.ai.currentAction != null && human.ai.currentAction.preset.outdoorClothingCheck && human.ai.currentAction.node != null)
        {
            //Am I going outside for this?
            if(human.ai.currentAction.node.building != human.currentBuilding || human.ai.currentAction.node.building == null || human.ai.currentAction.node.isOutside || human.ai.currentAction.node.room.gameLocation.IsOutside() || human.ai.currentAction.node.room.IsOutside())
            {
                outdoorsNeeded = true;
            }
        }

        if(currentOutfit == ClothesPreset.OutfitCategory.bed || currentOutfit == ClothesPreset.OutfitCategory.underwear || currentOutfit == ClothesPreset.OutfitCategory.undressed)
        {
            if (human.isAtWork)
            {
                if (outdoorsNeeded)
                {
                    SetCurrentOutfit(ClothesPreset.OutfitCategory.outdoorsWork);
                }
                else
                {
                    SetCurrentOutfit(ClothesPreset.OutfitCategory.work);
                }
            }
            else
            {
                if (outdoorsNeeded)
                {
                    SetCurrentOutfit(ClothesPreset.OutfitCategory.outdoorsCasual);
                }
                else
                {
                    SetCurrentOutfit(ClothesPreset.OutfitCategory.casual);
                }
            }
        }
        //Take coat on/off
        else
        {
            if(outdoorsNeeded)
            {
                if(currentOutfit == ClothesPreset.OutfitCategory.work)
                {
                    SetCurrentOutfit(ClothesPreset.OutfitCategory.outdoorsWork);
                }
                else if(currentOutfit == ClothesPreset.OutfitCategory.casual)
                {
                    SetCurrentOutfit(ClothesPreset.OutfitCategory.outdoorsCasual);
                }
                else if(currentOutfit == ClothesPreset.OutfitCategory.smart)
                {
                    SetCurrentOutfit(ClothesPreset.OutfitCategory.outdoorsSmart);
                }
            }
            else
            {
                if (currentOutfit == ClothesPreset.OutfitCategory.outdoorsWork)
                {
                    SetCurrentOutfit(ClothesPreset.OutfitCategory.work);
                }
                else if (currentOutfit == ClothesPreset.OutfitCategory.outdoorsCasual)
                {
                    SetCurrentOutfit(ClothesPreset.OutfitCategory.casual);
                }
                else if (currentOutfit == ClothesPreset.OutfitCategory.outdoorsSmart)
                {
                    SetCurrentOutfit(ClothesPreset.OutfitCategory.smart);
                }
            }
        }
    }

    //Load an outfit
    public void SetCurrentOutfit(ClothesPreset.OutfitCategory category, bool forceLoad = false, bool forceReload = false)
    {
        //If this is in editor then force a reload
        if (Toolbox.Instance == null) forceReload = true;

        //Cannot do if restrained, KO, in combat or dead
        if(human.ai != null)
        {
            if (human.isDead || human.isStunned || human.ai.inCombat || human.ai.ko || human.ai.restrained) return;
        }

        //Disallow nudity mode
        if(!Game.Instance.allowNudity && category == ClothesPreset.OutfitCategory.undressed)
        {
            category = ClothesPreset.OutfitCategory.underwear;
        }

        //Don't do this if ragdolled
        if (human != null && human.ai != null && !isPoser)
        {
            if (human.ai.isRagdoll) return;

            if (Game.Instance.collectDebugData) human.SelectedDebug("AI: Set current outfit: " + category, Actor.HumanDebug.misc);
        }

        //Only allow work outfit if this has a job
        if(Toolbox.Instance != null)
        {
            if(category == ClothesPreset.OutfitCategory.work)
            {
                if (human.job == null || human.job.preset == null || human.job.preset.workOutfit.Count <= 0)
                {
                    category = ClothesPreset.OutfitCategory.casual;
                }
            }
            else if (category == ClothesPreset.OutfitCategory.outdoorsWork)
            {
                if (human.job == null || human.job.preset == null || human.job.preset.workOutfit.Count <= 0)
                {
                    category = ClothesPreset.OutfitCategory.outdoorsCasual;
                }
            }
        }

        if (currentOutfit != category || forceReload)
        {
            currentOutfit = category;

            //If visible, then load now...
            if (human.visible || forceLoad || isPoser)
            {
                LoadCurrentOutfit(forceLoad, forceReload);
            }
        }
    }

    public void LoadCurrentOutfit(bool forceLoad = false, bool forceReload = false)
    {
        //If this is in editor then force a reload
        if (Toolbox.Instance == null) forceReload = true;

        if (loadedOutfit != currentOutfit || forceLoad)
        {
            //Force reload: Completely remove everything
            if (forceReload)
            {
                RemoveCurrentOutfit();
            }

            if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("> Loading outfit: " + currentOutfit.ToString());

            RemoveDebugRenderers();

            foreach(OutfitClothes existing in currentlyLoadedClothes)
            {
                existing.loadedThisCycle = false; //This is for keeping track of what's been loaded this change cycle.
            }

            //Get outfit
            Outfit newOutfit = outfits.Find(item => item.category == currentOutfit);

            if(newOutfit != null)
            {
                Dictionary<string, ClothesPreset> clothesDictionary = null;

                if(Toolbox.Instance != null)
                {
                    clothesDictionary = Toolbox.Instance.clothesDictionary;
                }
                //Editor implementation
                else
                {
                    clothesDictionary = new Dictionary<string, ClothesPreset>();

                    List<ClothesPreset> allClothes = AssetLoader.Instance.GetAllClothes();

                    foreach(ClothesPreset cp in allClothes)
                    {
                        clothesDictionary.Add(cp.name, cp);
                    }
                }

                List<OutfitClothes> required = new List<OutfitClothes>(newOutfit.clothes);

                //Make sure all achors are covered
                coveredAnchors = new Dictionary<CharacterAnchor, int>();
                HashSet<OutfitClothes> existingItems = new HashSet<OutfitClothes>();

                //Look for changes needed in clothes...
                for (int i = 0; i < required.Count; i++)
                {
                    OutfitClothes req = required[i];
                    if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("Loading clothing: " + req.clothes + "...");

                    OutfitClothes match = currentlyLoadedClothes.Find(item => !item.incomplete &&  item.clothes == req.clothes && item.baseColor == req.baseColor && item.color1 == req.color1 && item.color2 == req.color2 && item.color3 == req.color3);

                    //A match is found
                    if (match != null)
                    {
                        //Add to covered anchors
                        ClothesPreset cp = null;

                        if (!clothesDictionary.TryGetValue(req.clothes, out cp))
                        {
                            Game.LogError("Unable to find " + req.clothes + " in clothes dictionary...");
                            continue;
                        }

                        foreach (ClothesPreset.ModelSettings model in cp.models)
                        {
                            if (model.exclusiveAnchorModel)
                            {
                                int rank = -1;

                                //If this is covered already, remove the one with the lowest priority...
                                if (coveredAnchors.TryGetValue(model.anchor, out rank))
                                {
                                    //If other is higher priority then skip this...
                                    if(rank > cp.priority)
                                    {
                                        if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("Removing model " + model.prefab.name + " because anchor " + model.anchor + " is already covered and is of higher priority (" + rank + ">" + cp.priority + ")");
                                        RemoveSpecificModel(match, model.anchor);
                                        match.incomplete = true;
                                    }
                                }
                                else
                                {
                                    coveredAnchors.Add(model.anchor, cp.priority);
                                }
                            }
                        }

                        if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("Found existing item " + req.clothes);

                        //Add to this list: Remove all that aren't required...
                        existingItems.Add(match);

                        match.loadedThisCycle = true; //Now mark this as loaded this cycle
                    }
                    //Otherwise spawn model...
                    else
                    {
                        //Add to covered anchors
                        ClothesPreset cp = null;

                        if (!clothesDictionary.TryGetValue(req.clothes, out cp))
                        {
                            Game.LogError("Unable to find " + req.clothes + " in clothes dictionary...");
                            continue;
                        }

                        SpawnClothingElement(req, cp);

                        //Add to this list: Remove all that aren't required...
                        existingItems.Add(req);
                    }
                }

                //Remove unneeded
                for (int i = 0; i < currentlyLoadedClothes.Count; i++)
                {
                    OutfitClothes spawned = currentlyLoadedClothes[i];

                    if(!existingItems.Contains(spawned))
                    {
                        if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("Unrequired item " + spawned.clothes + " (removing)");
                        RemoveClothingComponent(spawned);
                        i--;
                        continue;
                    }
                }
            }
            else
            {
                if(!isPoser) UnityEngine.Debug.Log("Cannot find " + currentOutfit + " outfit for " + human.GetCitizenName());
            }

            loadedOutfit = currentOutfit;

            //Clean up meshes
            for (int i = 0; i < allCurrentMeshes.Count; i++)
            {
                MeshRenderer rend = allCurrentMeshes[i];

                //Remove deleted meshes
                if(rend == null)
                {
                    allCurrentMeshes.RemoveAt(i);
                    allCurrentMeshFilters.RemoveAt(i);
                    i--;
                    continue;
                }
                //Remove deleted material objects
                else if(rend.sharedMaterial == null)
                {
                    if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("Cleaning object " + rend.gameObject.name);
                    SafeDestroy(rend.gameObject);
                    allCurrentMeshes.RemoveAt(i);
                    allCurrentMeshFilters.RemoveAt(i);
                    i--;
                    continue;
                }
            }

            if(human != null && !isPoser) human.updateMeshList = true; //Make sure mesh list is updated

            HairHatCompatibilityCheck(); //Check for hat compatibility
        }
    }

    private void SpawnClothingElement(OutfitClothes cl, ClothesPreset cp)
    {
        if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("Spawning required item " + cl.clothes);

        cl.spawned = new Dictionary<CharacterAnchor, List<MeshRenderer>>();
        cl.materials = new List<Material>();

        cl.loadedThisCycle = true; //Mark this as loaded this change cycle

        if (anchorReference.Count <= 0)
        {
            foreach (AnchorConfig a in anchorConfig)
            {
                anchorReference.Add(a.anchor, a.trans);
            }
        }

        //Set footwear
        if (cp.setFootwear && Application.isPlaying && !isPoser)
        {
            human.SetFootwear(cp.footwear);
        }

        //If head then set offsets
        if (cp.isHead)
        {
            pupilParent.localPosition = cp.pupilsOffset;
            pupilParentOffset = cp.pupilsOffset;
            eyebrowParent.localPosition = cp.eyebrowsOffset;
            mouth.localPosition = cp.mouthOffset;
        }

        //Spawn new objects
        foreach (ClothesPreset.ModelSettings model in cp.models)
        {
            if (model == null)
            {
                Game.LogError("Missing model setting on " + cl.clothes);
                continue;
            }
            else if (model.prefab == null)
            {
                Game.LogError("Missing prefab on model " + cl.clothes);
                continue;
            }
            else if (!anchorReference.ContainsKey(model.anchor))
            {
                Game.LogError("Missing anchor reference " + model.anchor);
                continue;
            }

            if (model.exclusiveAnchorModel)
            {
                //Check if we are overwriting a previously loaded model and if we are, mark it as incomplete to force a re-load
                foreach(OutfitClothes existing in currentlyLoadedClothes)
                {
                    if(!existing.loadedThisCycle)
                    {
                        if(existing.spawned.ContainsKey(model.anchor))
                        {
                            RemoveSpecificModel(existing, model.anchor);
                            existing.incomplete = true;
                        }
                    }
                }

                int rank = -1;

                //Skip because the loading order will mean this is a lower priority than what's there already.
                if (coveredAnchors.TryGetValue(model.anchor, out rank))
                {
                    if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("Skipping model " + model.prefab.name + " because anchor " + model.anchor + " is already covered...");
                    cl.incomplete = true; //Mark this as incomplete
                    continue; //Do this to avoid loading models over each other...
                }
                else
                {
                    coveredAnchors.Add(model.anchor, cp.priority);
                }
            }

            //Create object
            GameObject newClothesItem = Instantiate(model.prefab, anchorReference[model.anchor]);
            newClothesItem.transform.localPosition = model.offsetPosition;
            newClothesItem.transform.localEulerAngles = model.offsetEuler;
            newClothesItem.transform.tag = "CitizenModelDebug";

            //Create material
            MeshRenderer rend = newClothesItem.GetComponent<MeshRenderer>();

            Material newMat = Instantiate(rend.sharedMaterial) as Material;
            if (cp.baseColourSource != ClothesPreset.ClothingColourSource.none) newMat.SetColor("_BaseColor", cl.baseColor);
            if (cp.colour1Source != ClothesPreset.ClothingColourSource.none) newMat.SetColor("_Color1", cl.color1);
            if (cp.colour2Source != ClothesPreset.ClothingColourSource.none) newMat.SetColor("_Color2", cl.color2);
            if (cp.colour3Source != ClothesPreset.ClothingColourSource.none) newMat.SetColor("_Color3", cl.color3);

            //Set grub
            float grub = 0.1f;

            if (Application.isPlaying)
            {
                grub = Mathf.Clamp01(1f - human.conscientiousness) * 0.25f; //25% consceientiousness...
                if (human.isHomeless) grub += 0.75f;
                grub = Mathf.Clamp(0.1f, grub, 1f);
            }
            else
            {
                grub = debugOverrideGrub;
            }

            newMat.SetFloat("_GrubAmount", grub); //Apply grub

            cl.materials.Add(newMat);

            //Parse LOD here...
            LODGroup lod = newClothesItem.GetComponent<LODGroup>();

            if(lod != null)
            {
                LOD[] lods = lod.GetLODs();

                if(lods.Length > 0)
                {
                    foreach(MeshRenderer r in lods[0].renderers)
                    {
                        AddMeshRenderer(r, ref newMat, false, ref cl, model);
                    }
                }

                if(lods.Length > 1)
                {
                    for (int i = 0; i < lods[1].renderers.Length; i++)
                    {
                        if(isPoser)
                        {
                            SafeDestroy(lods[1].renderers[i].gameObject); //Remove LODs for poser
                        }
                        else
                        {
                            AddMeshRenderer(lods[1].renderers[i] as MeshRenderer, ref newMat, true, ref cl, model);
                        }
                    }
                }

                //Get rid of LOD
                lod.SetLODs(new LOD[] { });
                SafeDestroy(lod);
            }
            else
            {
                //If no LOD group attached: Add to both level 0 and 1 LODs
                AddMeshRenderer(rend, ref newMat, false, ref cl, model);
                if(!isPoser) human.AddMesh(rend, true, false, true, false);
            }

            if(!isPoser)
            {
                if (eyebrowRenderers != null) human.AddMeshes(eyebrowRenderers, true);
                if (eyeRenderers != null) human.AddMeshes(eyeRenderers, true);
                if (mouthRenderer != null) human.AddMesh(mouthRenderer, true);
            }

            //Add to reference
            if (!cl.spawned.ContainsKey(model.anchor))
            {
                cl.spawned.Add(model.anchor, new List<MeshRenderer>());
            }

            cl.spawned[model.anchor].Add(rend);

            //Set light layers
            //if (Toolbox.Instance != null)
            //{
            //    for (int i = 0; i < human.meshes.Count; i++)
            //    {
            //        Toolbox.Instance.SetLightLayer(human.meshes[i], human.currentBuilding);
            //    }

            //    for (int i = 0; i < human.meshesLOD1.Count; i++)
            //    {
            //        Toolbox.Instance.SetLightLayer(human.meshesLOD1[i], human.currentBuilding);
            //    }
            //}

            //Set this so we can quickly check for hair/hat compatibility...
            if(model.anchor == CharacterAnchor.Hair)
            {
                currentHair = cp;
                currentHairRend = rend;
            }
            else if (model.anchor == CharacterAnchor.Hat)
            {
                currentHat = cp;
                currentHatRend = rend;
            }
        }

        currentlyLoadedClothes.Add(cl);
    }

    private void AddMeshRenderer(MeshRenderer rend, ref Material applyMat, bool isLOD, ref OutfitClothes clothesOutfit, ClothesPreset.ModelSettings model)
    {
        allCurrentMeshes.Add(rend);

        MeshFilter filt = rend.gameObject.GetComponent<MeshFilter>();
        allCurrentMeshFilters.Add(filt);

        if (Toolbox.Instance != null)
        {
            if(isPoser && poser.node != null)
            {
                Toolbox.Instance.SetLightLayer(rend, poser.node.building); //Get light layer
            }
            else
            {
                Toolbox.Instance.SetLightLayer(rend, human.currentBuilding); //Get light layer
            }
        }

        rend.gameObject.transform.tag = "CitizenModelDebug";
        if(!isPoser) rend.gameObject.layer = 24;
        //newClothesItem.layer = 15; //Set to ragdoll layer
        rend.sharedMaterial = applyMat;

        //Add renderers reference
        if(!isPoser)
        {
            human.AddMesh(rend, true, false, isLOD, false);
        }

        //Turn off shadows
        if(isLOD)
        {
            if (!Game.Instance.shadowsOnCitizenLOD) rend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        }

        //Add to reference
        if (!clothesOutfit.spawned.ContainsKey(model.anchor))
        {
            clothesOutfit.spawned.Add(model.anchor, new List<MeshRenderer>());
        }

        clothesOutfit.spawned[model.anchor].Add(rend);
    }

    private void RemoveSpecificModel(OutfitClothes cl, CharacterAnchor a)
    {
        List<MeshRenderer> found = null;

        if(cl.spawned.TryGetValue(a, out found))
        {
            while (found.Count > 0)
            {
                //if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("Removing model " + found[0].gameObject);

                try
                {
                    if(found[0] != null)
                    {
                        //Remove mesh references
                        if (human != null && !isPoser)
                        {
                            human.RemoveMesh(found[0], true);
                        }

                        SafeDestroy(found[0].gameObject);
                    }
                }
                catch
                {
                    Debug.LogError("Unable to remove " + found[0]);
                }

                found.RemoveAt(0);
            }
        }
    }

    //Check hair/hat compatibility and hide hair if needed
    public void HairHatCompatibilityCheck()
    {
        //Have I currently got a hat on?
        if(currentHatRend != null)
        {
            //When in ragdoll...
            if(human.ai.isRagdoll)
            {
                if (currentHairRend != null) currentHairRend.enabled = true; //Render hair
            }
            //When not in ragdoll...
            else
            {
                if (currentHat.hairRenderMode == ClothesPreset.HairRenderSetting.renderAllHair)
                {
                    if (currentHairRend != null) currentHairRend.enabled = true;
                }
                else if (currentHat.hairRenderMode == ClothesPreset.HairRenderSetting.renderHatCompatibleHair && currentHair != null && currentHair.hatRenderCompatible && !currentHair.excludeHats.Contains(currentHat))
                {
                    if (currentHairRend != null) currentHairRend.enabled = true;
                }
                else
                {
                    if (currentHairRend != null) currentHairRend.enabled = false;
                }
            }
        }
    }

    //Remove a single clothing component (does not affect loaded models dictionary: This is rebuilt when outfit is changed)
    private void RemoveClothingComponent(OutfitClothes cl)
    {
        foreach (KeyValuePair<CharacterAnchor, List<MeshRenderer>> pair in cl.spawned)
        {
            while (pair.Value.Count > 0)
            {
                if(pair.Value[0] != null)
                {
                    if ((Game.Instance == null || (Game.Instance.devMode && Game.Instance.collectDebugData)) && enableDebugLog) outfitDebug.Add("Removing model " + pair.Value[0].gameObject);

                    try
                    {
                        //Remove mesh references
                        if (human != null && !isPoser)
                        {
                            human.RemoveMesh(pair.Value[0], true);
                        }

                        SafeDestroy(pair.Value[0].gameObject);
                    }
                    catch
                    {
                        Debug.LogError("Unable to remove " + pair.Value[0]);
                    }
                }

                pair.Value.RemoveAt(0);
            }
        }

        cl.spawned.Clear();

        while (cl.materials.Count > 0)
        {
            try
            {
                SafeDestroy(cl.materials[0]);
            }
            catch
            {
                Game.LogError("Unable to remove " + cl.materials[0]);
            }

            cl.materials.RemoveAt(0);
        }

        currentlyLoadedClothes.Remove(cl);
    }

    private void RemoveDebugRenderers()
    {
        while (debugRenderers.Count > 0)
        {
            try
            {
                SafeDestroy(debugRenderers[0]);
            }
            catch
            {
                Debug.LogError("Unable to remove " + debugRenderers[0]);
            }

            debugRenderers.RemoveAt(0);
        }
    }

    [Button]
    public void RemoveCurrentOutfit()
    {
        RemoveDebugRenderers();

        //Remove all currently loaded clothes (in-game only)
        while (currentlyLoadedClothes.Count > 0)
        {
            OutfitClothes cl = currentlyLoadedClothes[0];

            foreach(KeyValuePair<CharacterAnchor, List<MeshRenderer>> pair in cl.spawned)
            {
                while (pair.Value.Count > 0)
                {
                    if(pair.Value[0] != null)
                    {
                        try
                        {
                            SafeDestroy(pair.Value[0].gameObject);
                        }
                        catch
                        {
                            Debug.LogError("Unable to remove " + pair.Value[0]);
                        }
                    }

                    pair.Value.RemoveAt(0);
                }
            }

            cl.spawned.Clear();

            while (cl.materials.Count > 0)
            {
                try
                {
                    SafeDestroy(cl.materials[0]);
                }
                catch
                {
                    Debug.LogError("Unable to remove " + cl.materials[0]);
                }

                cl.materials.RemoveAt(0);
            }

            currentlyLoadedClothes.RemoveAt(0);
        }

        currentlyLoadedClothes.Clear();

        //Remove debug models: Useful for editor
        List<MeshRenderer> allMeshes = this.transform.GetComponentsInChildren<MeshRenderer>(true).ToList();
        List<MeshRenderer> debugModel = allMeshes.FindAll(item => item.transform.CompareTag("CitizenModelDebug"));

        while (debugModel.Count > 0)
        {
            try
            {
                SafeDestroy(debugModel[0].gameObject);
            }
            catch
            {
                Debug.LogError("Unable to remove " + debugModel[0]);
                SafeDestroy(debugModel[0]);
            }
            
            debugModel.RemoveAt(0);
        }

        if (human != null && !isPoser)
        {
            //Forcing an update of meshes will remove any null/removed entries
            human.UpdateMeshList();
        }
    }

    [Button]
    public void LoadSpecificOutfit()
    {
        RemoveCurrentOutfit();
        GenerateOutfits(true);
        CycleOutfits();
    }

    [Button]
    public void SelectRandomOutfits()
    {
        RemoveCurrentOutfit();
        GenerateOutfits();
        CycleOutfits();
    }

    [Button]
    public void CycleOutfits()
    {
        if(outfits.Count <= 0)
        {
            Debug.LogError("No outfits assigned to citizen! If in editor, make sure you SelectRandomOutfits first...");
            return;
        }

        int currentOutfitIndex = (int)currentOutfit;
        currentOutfitIndex++;
        List<ClothesPreset.OutfitCategory> cat = ClothesPreset.OutfitCategory.GetValues(typeof(ClothesPreset.OutfitCategory)).Cast<ClothesPreset.OutfitCategory>().ToList();
        if (currentOutfitIndex >= cat.Count) currentOutfitIndex = 0;
        SetCurrentOutfit((ClothesPreset.OutfitCategory)currentOutfitIndex, true);

        Debug.Log("Loaded " + currentOutfit + " outfit");
    }

    [Button]
    public void ResetAllOutfits()
    {
        RemoveCurrentOutfit();

        outfits.Clear();
        outfitDebug.Clear();
        currentOutfit = ClothesPreset.OutfitCategory.underwear;
        previousOutfit = ClothesPreset.OutfitCategory.underwear;
        loadedOutfit = ClothesPreset.OutfitCategory.underwear;
    }

    [Button]
    public void CreateNewClothingPreset()
    {
#if UNITY_EDITOR
        if (newClothingComponents.Count <= 0) return;

        string prefabPath = "Assets/Prefabs/Citizens/";

        Dictionary<CharacterAnchor, List<NewClothingCreation>> prefabsSaved = new Dictionary<CharacterAnchor, List<NewClothingCreation>>(); //List of new prefabs

        //Create prefabs
        foreach(GameObject go in newClothingComponents)
        {
            //Find directory...
            Transform p = go.transform.parent;
            AnchorConfig anchor = anchorConfig.Find(item => item.trans == p);

            if(anchor != null)
            {
                string dir = anchor.anchor.ToString() + "s";

                if(anchor.anchor == CharacterAnchor.HandLeft || anchor.anchor == CharacterAnchor.HandRight)
                {
                    dir = "Hands";
                }
                else if(anchor.anchor == CharacterAnchor.UpperArmLeft || anchor.anchor == CharacterAnchor.UpperArmRight)
                {
                    dir = "UpperArms";
                }
                else if (anchor.anchor == CharacterAnchor.LowerArmLeft || anchor.anchor == CharacterAnchor.LowerArmRight)
                {
                    dir = "LowerArms";
                }
                else if (anchor.anchor == CharacterAnchor.UpperLegLeft || anchor.anchor == CharacterAnchor.UpperLegRight)
                {
                    dir = "UpperLegs";
                }
                else if (anchor.anchor == CharacterAnchor.LowerLegLeft || anchor.anchor == CharacterAnchor.LowerLegRight)
                {
                    dir = "LowerLegs";
                }

                if(!prefabsSaved.ContainsKey(anchor.anchor))
                {
                    prefabsSaved.Add(anchor.anchor, new List<NewClothingCreation>());
                }

                NewClothingCreation newClothes = new NewClothingCreation();
                newClothes.newPrefab = PrefabUtility.SaveAsPrefabAssetAndConnect(go, prefabPath + dir + "/" + go.name + ".prefab", InteractionMode.AutomatedAction);
                newClothes.offset = go.transform.localPosition;
                newClothes.euler = go.transform.localEulerAngles;
                prefabsSaved[anchor.anchor].Add(newClothes);

                if(CreateFlippedArmsAndLegsFromRightSide)
                {
                    if(anchor.anchor == CharacterAnchor.HandRight && !prefabsSaved.ContainsKey(CharacterAnchor.HandLeft))
                    {
                        NewClothingCreation newFlipped = new NewClothingCreation();
                        newFlipped.newPrefab = newClothes.newPrefab;
                        newFlipped.offset = go.transform.localPosition;
                        newFlipped.euler = go.transform.localEulerAngles;
                        prefabsSaved.Add(CharacterAnchor.HandLeft, new List<NewClothingCreation>());
                        prefabsSaved[CharacterAnchor.HandLeft].Add(newFlipped);
                    }
                    else if (anchor.anchor == CharacterAnchor.LowerArmRight && !prefabsSaved.ContainsKey(CharacterAnchor.LowerArmLeft))
                    {
                        NewClothingCreation newFlipped = new NewClothingCreation();
                        newFlipped.newPrefab = newClothes.newPrefab;
                        newFlipped.offset = go.transform.localPosition;
                        newFlipped.euler = go.transform.localEulerAngles;
                        prefabsSaved.Add(CharacterAnchor.LowerArmLeft, new List<NewClothingCreation>());
                        prefabsSaved[CharacterAnchor.LowerArmLeft].Add(newFlipped);
                    }
                    else if (anchor.anchor == CharacterAnchor.LowerLegRight && !prefabsSaved.ContainsKey(CharacterAnchor.LowerLegLeft))
                    {
                        NewClothingCreation newFlipped = new NewClothingCreation();
                        newFlipped.newPrefab = newClothes.newPrefab;
                        newFlipped.offset = go.transform.localPosition;
                        newFlipped.euler = go.transform.localEulerAngles;
                        prefabsSaved.Add(CharacterAnchor.LowerLegLeft, new List<NewClothingCreation>());
                        prefabsSaved[CharacterAnchor.LowerLegLeft].Add(newFlipped);
                    }
                    else if (anchor.anchor == CharacterAnchor.UpperArmRight && !prefabsSaved.ContainsKey(CharacterAnchor.UpperArmLeft))
                    {
                        NewClothingCreation newFlipped = new NewClothingCreation();
                        newFlipped.newPrefab = newClothes.newPrefab;
                        newFlipped.offset = go.transform.localPosition;
                        newFlipped.euler = go.transform.localEulerAngles;
                        prefabsSaved.Add(CharacterAnchor.UpperArmLeft, new List<NewClothingCreation>());
                        prefabsSaved[CharacterAnchor.UpperArmLeft].Add(newFlipped);
                    }
                    else if (anchor.anchor == CharacterAnchor.UpperLegRight && !prefabsSaved.ContainsKey(CharacterAnchor.UpperLegLeft))
                    {
                        NewClothingCreation newFlipped = new NewClothingCreation();
                        newFlipped.newPrefab = newClothes.newPrefab;
                        newFlipped.offset = go.transform.localPosition;
                        newFlipped.euler = go.transform.localEulerAngles;
                        prefabsSaved.Add(CharacterAnchor.UpperLegLeft, new List<NewClothingCreation>());
                        prefabsSaved[CharacterAnchor.UpperLegLeft].Add(newFlipped);
                    }
                }
            }
            else
            {
                Debug.Log("Unable to find anchor for " + go.name + ", skipping...");
            }
        }

        //Create new preset file...
        ClothesPreset newPreset = new ClothesPreset();

        foreach(KeyValuePair<CharacterAnchor, List <NewClothingCreation>> pair in prefabsSaved)
        {
            foreach(NewClothingCreation cl in pair.Value)
            {
                ClothesPreset.ModelSettings newSettings = new ClothesPreset.ModelSettings();
                newSettings.anchor = pair.Key;
                newSettings.prefab = cl.newPrefab;
                newSettings.offsetPosition = cl.offset;
                newSettings.offsetEuler = cl.euler;

                newPreset.models.Add(newSettings);

                if(!newPreset.covers.Contains(pair.Key))
                {
                    newPreset.covers.Add(pair.Key);
                }
            }
        }

        string presetPath = "Assets/Resources/Data/AI/Clothes/" + directory.ToString() + "/" + newClothingName + ".asset";
        AssetDatabase.CreateAsset(newPreset, presetPath);
        AssetDatabase.Refresh();

        ClothesPreset loadedP = (ClothesPreset)AssetDatabase.LoadAssetAtPath(presetPath, typeof(ClothesPreset));
        UnityEditor.Selection.activeObject = loadedP; //Select this
#endif
    }

    [Button]
    public void LoadExpression()
    {
        ExpressionSetup exp = expressions.Find(item => item.expression == debugOverrideExpression);

        if(exp != null)
        {
            rightEyebrow.localEulerAngles = exp.eyebrowsEuler;
            leftEyebrow.localEulerAngles = -exp.eyebrowsEuler;
            rightEyebrow.localPosition = new Vector3(0.045f, exp.eyebrowsRaise, 0);
            leftEyebrow.localPosition = new Vector3(-0.045f, exp.eyebrowsRaise, 0);
            rightPupil.localScale = new Vector3(0.02f, 0.02f * exp.eyeHeightMultiplier, 0.02f);
            leftPupil.localScale = new Vector3(0.02f, 0.02f * exp.eyeHeightMultiplier, 0.02f);
        }
    }

    public T SafeDestroyGameObject<T>(T component) where T : Component
    {
        if (component != null)
            SafeDestroy(component.gameObject);
        return null;
    }

    public T SafeDestroy<T>(T obj) where T : Object
    {
        if ((Application.isEditor && !Application.isPlaying) || isPoser)
        {
             Object.DestroyImmediate(obj);
        }
        else
        {
            Object.Destroy(obj);
        }

        return null;
    }

    private Color PickColourFromPalette(ref List<ColourPalettePreset> palettes, string debug = "")
    {
        Color ret = Color.black;

        if(palettes.Count <= 0)
        {
            Debug.LogError("No palettes selected...");
            return ret;
        }

        List<Color> possibleColours = new List<Color>();

        foreach (ColourPalettePreset c in palettes)
        {
            float av = human.GetHexacoScore(ref c.hexaco);

            foreach (ColourPalettePreset.MaterialSettings set in c.colours)
            {
                int freq = Mathf.RoundToInt(av * set.weighting * 0.5f);

                for (int u = 0; u < freq; u++)
                {
                    possibleColours.Add(set.colour);
                }
            }
        }

        if(possibleColours.Count <= 0)
        {
            Debug.Log("No possible colours: " + debug);
            return ret;
        }

        int colIndex = 0;

        if (!Application.isEditor)
        {
            colIndex = Toolbox.Instance.GetPsuedoRandomNumberContained(0, possibleColours.Count, human.seed, out human.seed);
        }
        else
        {
            colIndex = UnityEngine.Random.Range(0, possibleColours.Count);
        }

        ret = possibleColours[colIndex];

        return ret;
    }

    private Color GetColourFromUnderneath(ClothesPreset thisPreset, ClothesPreset.OutfitCategory category, ClothesPreset.ClothingColourSource source, ref Dictionary<string, ClothesPreset> clothesDictionary)
    {
        Color ret = Color.black;

        ClothesPreset.OutfitCategory lookInCategory = ClothesPreset.OutfitCategory.underwear;

        if (category == ClothesPreset.OutfitCategory.outdoorsCasual)
        {
            lookInCategory = ClothesPreset.OutfitCategory.casual;
        }
        else if(category == ClothesPreset.OutfitCategory.outdoorsSmart)
        {
            lookInCategory = ClothesPreset.OutfitCategory.smart;
        }
        else if(category == ClothesPreset.OutfitCategory.outdoorsWork)
        {
            lookInCategory = ClothesPreset.OutfitCategory.work;
        }

        Outfit findOutfit = outfits.Find(item => item.category == lookInCategory);

        if(findOutfit != null)
        {
            OutfitClothes similarCovered = null;
            float bestScore = -99999;

            //Find the garment that covers the most similar areas...
            foreach(OutfitClothes cl in findOutfit.clothes)
            {
                ClothesPreset p = clothesDictionary[cl.clothes];

                int score = 0;

                foreach(CitizenOutfitController.CharacterAnchor covers in p.covers)
                {
                    if(thisPreset.covers.Contains(covers))
                    {
                        score++;
                    }
                }

                if(score > bestScore)
                {
                    similarCovered = cl;
                    bestScore = score;
                }
            }

            if(similarCovered != null)
            {
                if(source == ClothesPreset.ClothingColourSource.underneathColour1)
                {
                    return similarCovered.color1;
                }
                else if(source == ClothesPreset.ClothingColourSource.underneathColour2)
                {
                    return similarCovered.color2;
                }
                else if(source == ClothesPreset.ClothingColourSource.underneathColour3)
                {
                    return similarCovered.color3;
                }
            }
        }

        return ret;
    }

    //Get the chance of picking this
    public bool GetChance(Human human, ref List<ClothesPreset.TraitPickRule> pickRules, out int addChance)
    {
        //Check picking rules
        bool passRules = true;
        addChance = 0;

        foreach (ClothesPreset.TraitPickRule rule in pickRules)
        {
            bool pass = false;

            if (rule.rule == CharacterTrait.RuleType.ifAnyOfThese)
            {
                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (human.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = true;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifAllOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (!human.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifNoneOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (human.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifPartnerAnyOfThese)
            {
                if (human.partner != null)
                {
                    foreach (CharacterTrait searchTrait in rule.traitList)
                    {
                        if (human.partner.characterTraits.Exists(item => item.trait == searchTrait))
                        {
                            pass = true;
                            break;
                        }
                    }
                }
            }

            if (pass)
            {
                addChance += rule.addChance;
            }
            else if (rule.mustPassForApplication)
            {
                passRules = false;
            }
        }

        if (!passRules)
        {
            addChance = 0;
            return false;
        }

        return true;
    }
}
