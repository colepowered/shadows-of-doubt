﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvidenceStickyNote : Evidence
{
    //Constructor
    public EvidenceStickyNote(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        
    }

    //Name
    public override string GenerateName()
    {
        return Strings.Get("evidence.names", preset.name);
    }
}
