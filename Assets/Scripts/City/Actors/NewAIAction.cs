﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using System.Linq;
using UnityEditor;

[System.Serializable]
public class NewAIAction
{
    public string name = "Action";

    [Header("Action Variables")]
    [NonSerialized]
    public NewAIGoal goal;
    public AIActionPreset preset;
    [Tooltip("Is this action currently active?")]
    public bool isActive = false;
    public bool completed = false;
    public bool repeat = false; //Repeat on complete
    [NonSerialized]
    public bool checkedForInsertions = false; //True if lighting preferences have been checked at least once
    public bool insertedAction = false; //True if this is an action that has been inserted, and not part of the goal usually
    [ReadOnly]
    public int insertedActionPriority = 3; //How important is this inserted action? Governs where it will be interted on the actions list

    [Header("Location")]
    public NewNode node;
    [System.NonSerialized]
    public Interactable interactable;
    [System.NonSerialized]
    public Interactable.UsagePoint usagePoint;
    [Tooltip("Is the citizen at the correct location?")]
    public bool isAtLocation = false;
    public PathFinder.PathData path = null;
    [System.NonSerialized]
    public Interactable passedInteractable; //Optional passed to the constructor to force and interactables
    public NewRoom passedRoom = null; //Optionally passed to the constructor to force a room
    public NewNode forcedNode = null; //Pass this to force a node upon which to perform the action
    [System.NonSerialized]
    public GroupsController.SocialGroup passedGroup = null; //Pass this usually to use with social groups meeting and reserving seats etc.
    [NonSerialized]
    public bool forceRun = false; //This citizen will run to complete this action
    public float estimatedArrival = -1; //Estimated arrival time
    public float arrivedAtDestination = 0f; //Recorded gametime when arrived at destination
    private bool actionCheckRecursion = false;
    private NewGameLocation.ObjectPlacement bestPlacement = null;
    public List<InteractablePreset> passedAcquireItems = null;

    [Header("Audio")]
    [NonSerialized]
    public AudioController.LoopingSoundInfo audioLoop = null;

    [Header("Progress")]
    [NonSerialized]
    public float lastRecordedTickWhileAtDesitnation = 0f;
    public float timeThisWillTake = 0f;
    public float progress = 0f;
    [NonSerialized]
    public float dontUpdateGoalPriorityForExtraTime = 0f;
    public float createdAt;

    [Header("Debug")]
    public string debug;
    [Space(7)]
    public InteractableController debugPassedInteractable;
    public NewRoom debugPassedRoom;
    public bool debugForcedNode = false;
    public Vector3 debugForcedNodeWorldPos = Vector3.zero;
    public List<Interactable> debugPickupInteractable = new List<Interactable>();
    [Space(7)]
    public InteractableController debugInteractableController;
    public Vector3 debugInteractionUsagePosition;

    public NewAIAction(
        NewAIGoal newGoal,
        AIActionPreset newPreset,
        bool newInsertedAction = false,
        NewRoom newPassedRoom = null,
        Interactable newPassedInteractable = null,
        NewNode newForcedNode = null,
        GroupsController.SocialGroup newPassedGroup = null,
        List<InteractablePreset> newPassedAcquireItems = null,
        bool newForceRun = false,
        int newInsertedActionPriority = 3,
        string newDebug = "")
    {
        goal = newGoal;
        preset = newPreset;
        name = preset.name;
        passedRoom = newPassedRoom;

        if (newPassedInteractable != null)
        {
            passedInteractable = newPassedInteractable;
        }
        else passedInteractable = null;

        forcedNode = newForcedNode;
        passedGroup = newPassedGroup;
        passedAcquireItems = newPassedAcquireItems;
        debug = newDebug;
        repeat = preset.repeatOnComplete;

        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Create new action: " + preset.name + " for goal " + goal.name +" actor position: " + goal.aiController.transform.position, Actor.HumanDebug.actions);

        createdAt = SessionData.Instance.gameTime;

        //forceRun = preset.forceRun;
        forceRun = newForceRun;

        insertedAction = newInsertedAction;
        insertedActionPriority = newInsertedActionPriority;

        //Add to actions
        if(!insertedAction)
        {
            goal.actions.Add(this);
        }
        else
        {
            //Insert based on passed action priority: But take into account what room we're currently in...
            bool inserted = false;

            for (int i = 0; i < goal.actions.Count; i++)
            {
                NewAIAction nextOnList = goal.actions[i];

                //If the next event isn't an inserted action, definitely insert before it (no matter what location)
                if (!nextOnList.insertedAction)
                {
                    //Insert before next
                    goal.actions.Insert(i, this);
                    inserted = true;
                    break;
                }

                //If this action is in the same room, then go ahead and insert it
                if (forcedNode != null)
                {
                    if (forcedNode.room == goal.aiController.human.currentRoom)
                    {
                        //Insert before next
                        goal.actions.Insert(i, this);
                        inserted = true;
                        break;
                    }
                }

                //If this action is in the same room, then go ahead and insert it
                if (passedRoom != null)
                {
                    if (passedRoom == goal.aiController.human.currentRoom)
                    {
                        //Insert before next
                        goal.actions.Insert(i, this);
                        inserted = true;
                        break;
                    }
                }

                //If the next action is in the same room, then don't insert now...
                //if (nextOnList.forcedNode != null)
                //{
                //    if (nextOnList.forcedNode.room == goal.aiController.human.currentRoom)
                //    {
                //        continue;
                //    }
                //}

                ////If the next action is in the same room, then don't insert now...
                //if (nextOnList.passedRoom != null)
                //{
                //    if (nextOnList.passedRoom == goal.aiController.human.currentRoom)
                //    {
                //        continue;
                //    }
                //}

                //If this is reached, the new action isn't in the same room, but neither is the next...

                //If this action is in the same gamelocation, then go ahead and insert it
                //if (forcedNode != null)
                //{
                //    if (forcedNode.gameLocation == goal.aiController.human.currentGameLocation)
                //    {
                //        //Insert before next
                //        goal.actions.Insert(i, this);
                //        inserted = true;
                //        break;
                //    }
                //}

                ////If this action is in the same gamelocation, then go ahead and insert it
                //if (passedRoom != null)
                //{
                //    if (passedRoom.gameLocation == goal.aiController.human.currentGameLocation)
                //    {
                //        //Insert before next
                //        goal.actions.Insert(i, this);
                //        inserted = true;
                //        break;
                //    }
                //}

                //If the next action is in the same gamelocation, then don't insert now...
                //if (nextOnList.forcedNode != null)
                //{
                //    if (nextOnList.forcedNode.gameLocation == goal.aiController.human.currentGameLocation)
                //    {
                //        continue;
                //    }
                //}

                ////If the next action is in the same gamelocation, then don't insert now...
                //if (nextOnList.passedRoom != null)
                //{
                //    if (nextOnList.passedRoom.gameLocation == goal.aiController.human.currentGameLocation)
                //    {
                //        continue;
                //    }
                //}

                //If none of the above is true, insert if a higher priority
                if (insertedActionPriority >= nextOnList.insertedActionPriority)
                {
                    //Insert before next
                    goal.actions.Insert(i, this);
                    inserted = true;
                    break;
                }
            }

            if(!inserted) goal.actions.Add(this);
        }

        //Calculate time this will take (gametime)
        float chosenMin = (float)Toolbox.Instance.Rand(preset.minutesTakenRange.x, preset.minutesTakenRange.y);

        //If we divide 0 by a number, it will return NaN, so avoid this
        if (chosenMin > 0)
        {
            timeThisWillTake = chosenMin / 60f;
        }
        else timeThisWillTake = 0;

        //Fill out debug info
        if(passedInteractable != null)
        {
            debugPassedInteractable = passedInteractable.controller;
        }

        debugPassedRoom = passedRoom;

        debugForcedNode = false;
        debugForcedNodeWorldPos = Vector3.zero;

        //Override: Forced node
        if (forcedNode != null)
        {
            debugForcedNode = true;
            debugForcedNodeWorldPos = forcedNode.position;
        }
    }

    //Triggered when this action becomes active
    public void OnActivate()
    {
        if (!isActive)
        {
            if(preset.exitConversationOnActivate)
            {
                if (goal.aiController.human.inConversation && goal.aiController.human.currentConversation != null)
                {
                    goal.aiController.human.currentConversation.EndConversation();
                }
            }

            if (Game.Instance.collectDebugData) goal.aiController.DebugDestinationPosition("Activate action: " + preset.name);

            completed = false;

            if (Game.Instance.collectDebugData) goal.aiController.DebugDestinationPosition("New action is activated: " + preset.name);

            //Reset hand state
            if(goal.aiController.human.animationController.armsBoolAnimationState != CitizenAnimationController.ArmsBoolSate.armsOneShotUse && goal.aiController.human.animationController.armsBoolAnimationState != CitizenAnimationController.ArmsBoolSate.none && !goal.aiController.restrained)
            {
                goal.aiController.human.animationController.SetArmsBoolState(CitizenAnimationController.ArmsBoolSate.none);
            }

            //Stop previous animation state
            if(goal.aiController.human.animationController.idleAnimationState != CitizenAnimationController.IdleAnimationState.none && !goal.aiController.restrained)
            {
                goal.aiController.human.animationController.SetIdleAnimationState(CitizenAnimationController.IdleAnimationState.none);
            }

            //Wake up if needed
            if (!preset.sleepOnArrival)
            {
                if(goal.aiController.human.isAsleep)
                {
                    //Game.Log("Wake up: " + preset.name);
                    goal.aiController.human.WakeUp();
                    return; //The above should create a new top priority goal, so we need to exit this.
                }
            }

            if (!preset.lying && goal.aiController.human.isInBed)
            {
                goal.aiController.human.SetInBed(false);
            }

            if (preset.activationRequiresConsumable)
            {
                if(goal.aiController.human.currentConsumables == null || goal.aiController.human.currentConsumables.Count <= 0)
                {
                    if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Removing action as human has no consumables...", Actor.HumanDebug.actions);
                    Remove(preset.repeatDelayOnActionFail);
                    return;
                }
            }

            goal.aiController.currentAction = this;
            goal.aiController.pathCursor = -1; //Reset path cursor
            isActive = true;

            //Spawn items (after current action is set)
            goal.aiController.UpdateHeldItems(AIActionPreset.ActionStateFlag.onActivation);

            if(passedInteractable != null)
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Activated action: " + preset.name + " actor position: " + goal.aiController.transform.position + " passed interactable: " + passedInteractable.GetName(), Actor.HumanDebug.actions);
            }
            else
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Activated action: " + preset.name + " actor position: " + goal.aiController.transform.position + " passed interactable: null", Actor.HumanDebug.actions);
            }

            //Pick location
            if (preset.actionLocation == AIActionPreset.ActionLocation.pause)
            {
                node = goal.aiController.human.currentNode;
                goal.aiController.AddDebugAction("New Action is pause at (" + node.name + ")");
            }
            else if (preset.actionLocation == AIActionPreset.ActionLocation.investigate)
            {
                node = goal.aiController.investigateLocation;

                if(node != null)
                {
                    goal.aiController.AddDebugAction("New Action is investigate location (" + node.name + ")");
                }

                //Reset goal's search progress
                goal.searchProgress = 0;
                goal.searchedNodes.Clear();
            }
            //Pick a location where we can put down the passed interactable
            else if (preset.actionLocation == AIActionPreset.ActionLocation.putDownInteractable)
            {
                //This is only compatible with the current location
                //Search for empty pick up sublocations
                FurnitureLocation pickedFurn = null;

                //Use same method as spawning to decide best position
                if (goal.aiController.human.currentGameLocation != null && goal.aiController.human.currentNode != null && passedInteractable != null)
                {
                    bestPlacement = goal.aiController.human.currentGameLocation.GetBestSpawnLocation(passedInteractable.preset, false, goal.aiController.human, null, null, out pickedFurn, forceSecuritySettings: true, forcedSecurity: 0, forcedOwnership: InteractablePreset.OwnedPlacementRule.both, placeClosestTo: goal.aiController.human.currentNode, usePutDownPosition: true);
                }
                else if(passedInteractable == null)
                {
                    Game.Log("AI Error: " + goal.aiController.human.name + " Cannot execute 'put down interactable' action without a passed interactable...");
                }
               
                if(bestPlacement != null)
                {
                    node = bestPlacement.furnParent.anchorNode;
                }
                else if(Game.Instance.devMode && Game.Instance.collectDebugData)
                {
                    string listPlacements = string.Empty;

                    foreach(SubObjectClassPreset cl in passedInteractable.preset.putDownPositions)
                    {
                        listPlacements += cl.name + ", ";
                    }

                    listPlacements += " - backup: ";

                    foreach (SubObjectClassPreset cl in passedInteractable.preset.backupPutDownPositions)
                    {
                        listPlacements += cl.name + ", ";
                    }

                    if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Unable to find put down interactable (" + passedInteractable.preset.name + ") furniture location in " + goal.aiController.human.currentGameLocation.name + ": " + listPlacements, Actor.HumanDebug.actions);
                    Game.Log("AI Error: Unable to find put down interactable (" + passedInteractable.preset.name + ") furniture location in " + goal.aiController.human.currentGameLocation.name + ": " + listPlacements);
                }

                //We need to remove the reference to the interactable, otherwise it will become the use point
                interactable = null;
            }
            //Pick up a put down interactable
            else if (preset.actionLocation == AIActionPreset.ActionLocation.pickUpInteractable)
            {
                node = passedInteractable.node;
                if((Game.Instance.devMode && Game.Instance.collectDebugData)) debugPickupInteractable.Add(passedInteractable);

                //We need to remove the reference to the interactable, otherwise it will become the use point
                interactable = null;
            }
            //Pick a random node at the given location
            else if (preset.actionLocation == AIActionPreset.ActionLocation.randomNodeWithinLocation)
            {
                List<NewNode> validNodes = new List<NewNode>();

                NewGameLocation loc = goal.aiController.CheckConfinedLocation(goal.gameLocation);

                if(loc != null)
                {
                    foreach (NewRoom validRoom in loc.rooms)
                    {
                        if (validRoom.roomType == InteriorControls.Instance.nullRoomType) continue;
                        if (validRoom.entrances.Count <= 0) continue;
                        if (validRoom.nodes.Count <= 1) continue;

                        //Ignore banned room types
                        if(preset.bannedRooms == AIActionPreset.SourceOfBannedRooms.jobPreset)
                        {
                            if(goal.aiController.human.job != null && goal.aiController.human.job.preset != null)
                            {
                                if (goal.aiController.human.job.preset.bannedRooms.Contains(validRoom.preset)) continue;
                            }
                        }

                        //Must not trespass
                        if(!goal.preset.allowTrespass)
                        {
                            bool trespass = goal.aiController.human.IsTrespassing(validRoom, out _, out _, goal.preset.allowEnforcersEverywhere);
                            if (trespass) continue;
                        }

                        foreach (NewNode n in validRoom.nodes)
                        {
                            //No entrances
                            if (n.isIndoorsEntrance || n.noAccess || n.noPassThrough || (n.gameLocation.thisAsStreet == null && n.floorType != NewNode.FloorTileType.floorOnly && n.floorType != NewNode.FloorTileType.floorAndCeiling)) continue;

                            validNodes.Add(n);
                        }
                    }
                }

                if(validNodes.Count <= 0)
                {
                    foreach(NewNode.NodeAccess ent in loc.entrances)
                    {
                        if(!ent.fromNode.room.isNullRoom)
                        {
                            node = ent.fromNode;
                            break;
                        }
                    }
                }
                else
                {
                    if(loc != null)
                    {
                        node = goal.aiController.human.FindSafeTeleport(loc);
                    }

                    goal.aiController.AddDebugAction("New Action is random node within location (" + node.name + ")");
                }
            }
            //Pick a random node weighted towards music
            else if(preset.actionLocation == AIActionPreset.ActionLocation.proximityToMusic)
            {
                List<NewNode> validNodes = new List<NewNode>();

                foreach (NewRoom validRoom in goal.gameLocation.rooms)
                {
                    if (validRoom.roomType == InteriorControls.Instance.nullRoomType) continue;
                    if (validRoom.entrances.Count <= 0) continue;
                    if (validRoom.nodes.Count <= 1) continue;
                    if (!validRoom.musicPlaying) continue;

                    //Ignore banned room types
                    if (preset.bannedRooms == AIActionPreset.SourceOfBannedRooms.jobPreset)
                    {
                        if (goal.aiController.human.job != null && goal.aiController.human.job.preset != null)
                        {
                            if (goal.aiController.human.job.preset.bannedRooms.Contains(validRoom.preset)) continue;
                        }
                    }

                    //Must not trespass
                    if (!goal.preset.allowTrespass)
                    {
                        bool trespass = goal.aiController.human.IsTrespassing(validRoom, out _, out _, goal.preset.allowEnforcersEverywhere);
                        if (trespass) continue;
                    }

                    foreach (NewNode n in validRoom.nodes)
                    {
                        if(n.room.gameLocation.thisAsStreet == null)
                        {
                            if (n.floorType != NewNode.FloorTileType.floorAndCeiling && n.floorType != NewNode.FloorTileType.floorOnly) continue;
                        }

                        //No entrances
                        if (n.isIndoorsEntrance || n.noAccess || n.noPassThrough) continue;

                        validNodes.Add(n);
                    }
                }

                if (validNodes.Count <= 0)
                {
                    foreach (NewNode.NodeAccess ent in goal.gameLocation.entrances)
                    {
                        if (!ent.fromNode.room.isNullRoom)
                        {
                            node = ent.fromNode;
                            break;
                        }
                    }
                }
                else
                {
                    node = validNodes[Toolbox.Instance.Rand(0, validNodes.Count)];

                    goal.aiController.AddDebugAction("New Action is random node within location (" + node.name + ")");
                }
            }
            //Pick a random node at home
            else if (preset.actionLocation == AIActionPreset.ActionLocation.randomNodeWithinHome && goal.aiController.human.home != null)
            {
                node = goal.aiController.human.FindSafeTeleport(goal.aiController.human.home);
            }
            else if (preset.actionLocation == AIActionPreset.ActionLocation.tailAndConfrontPlayer)
            {
                if(goal.aiController.trackedTargets.Exists(item => item.actor != null && item.actor.isPlayer))
                {
                    interactable = Player.Instance.interactable;
                    node = Player.Instance.currentNode;
                    if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Action location update with player node", Actor.HumanDebug.actions);
                }
                else
                {
                    interactable = null;
                    node = goal.aiController.human.FindSafeTeleport(Player.Instance.currentGameLocation);
                }
            }
            //Pick a nearby street with random node
            else if(preset.actionLocation == AIActionPreset.ActionLocation.NearbyStreetRandomNode)
            {
                //Pick the nearest street that the person isn't on
                StreetController chosenStreet = CityData.Instance.streetDirectory[Toolbox.Instance.Rand(0, CityData.Instance.streetDirectory.Count)]; //Fall back on random street
                float dist = Mathf.Infinity;

                foreach(StreetController sc in CityData.Instance.streetDirectory)
                {
                    if(sc != goal.aiController.human.currentGameLocation)
                    {
                        if(sc.rooms.Count > 0)
                        {
                            float d = Vector3.Distance(goal.aiController.human.currentNodeCoord, sc.rooms[0].nodes.FirstOrDefault().nodeCoord);

                            if(d < dist)
                            {
                                chosenStreet = sc;
                                dist = d;
                            }
                        }
                    }
                }

                List<NewNode> validNodes = new List<NewNode>();

                foreach (NewRoom validRoom in chosenStreet.rooms)
                {
                    if (validRoom.roomType == InteriorControls.Instance.nullRoomType) continue;
                    if (validRoom.entrances.Count <= 0) continue;
                    if (validRoom.nodes.Count <= 1) continue;

                    //Ignore banned room types
                    if (preset.bannedRooms == AIActionPreset.SourceOfBannedRooms.jobPreset)
                    {
                        if (goal.aiController.human.job != null && goal.aiController.human.job.preset != null)
                        {
                            if (goal.aiController.human.job.preset.bannedRooms.Contains(validRoom.preset)) continue;
                        }
                    }

                    //Must not trespass
                    if (!goal.preset.allowTrespass)
                    {
                        bool trespass = goal.aiController.human.IsTrespassing(validRoom, out _, out _, goal.preset.allowEnforcersEverywhere);
                        if (trespass) continue;
                    }

                    foreach (NewNode n in validRoom.nodes)
                    {
                        //No entrances
                        if (n.isIndoorsEntrance || n.noAccess || n.noPassThrough || (n.gameLocation.thisAsStreet == null && n.floorType != NewNode.FloorTileType.floorOnly && n.floorType != NewNode.FloorTileType.floorAndCeiling)) continue;

                        validNodes.Add(n);
                    }
                }

                if (validNodes.Count <= 0)
                {
                    //foreach (NewNode.NodeAccess ent in goal.gameLocation.entrances)
                    //{
                    //    if (!ent.fromNode.room.isNullRoom)
                    //    {
                    //        node = ent.fromNode;
                    //        break;
                    //    }
                    //}
                }
                else
                {
                    node = validNodes[Toolbox.Instance.Rand(0, validNodes.Count)];

                    goal.aiController.AddDebugAction("New Action is random node within location (" + node.name + ")");
                }
            }
            //Run somewhere safe that the player isn't
            else if (preset.actionLocation == AIActionPreset.ActionLocation.flee)
            {
                List<NewNode> validNodes = new List<NewNode>();

                Actor persuer = Player.Instance;
                if (goal.aiController.human.lastScaredBy != null) persuer = goal.aiController.human.lastScaredBy;

                //Confine AI to location...
                bool confined = false;
                NewGameLocation confinedLocation = null;

                if(goal.aiController.confineLocation != null)
                {
                    confined = true;
                    confinedLocation = goal.aiController.confineLocation;
                }

                if (goal.aiController.victimsForMurders.Count > 0)
                {
                    foreach (MurderController.Murder m in goal.aiController.victimsForMurders)
                    {
                        if (m.location == null) continue;
                        if ((int)m.state < 4) continue;

                        if (m.preset.blockVictimFromLeavingLocation)
                        {
                            confined = true;
                            confinedLocation = m.location;
                            break;
                        }
                    }
                }

                //Use confined location
                if(confined && confinedLocation != null)
                {
                    foreach (NewRoom validRoom in confinedLocation.rooms)
                    {
                        if (validRoom.roomType == InteriorControls.Instance.nullRoomType) continue;
                        if (persuer.currentRoom == validRoom) continue; //Different room

                        foreach (NewNode n in validRoom.nodes)
                        {
                            if (n.gameLocation.thisAsStreet == null && n.floorType != NewNode.FloorTileType.floorAndCeiling && n.floorType != NewNode.FloorTileType.floorOnly) continue;

                            //No entrances
                            if (n.isIndoorsEntrance || n.noAccess || n.noPassThrough) continue;

                            validNodes.Add(n);
                        }
                    }
                }
                //The default place is home
                else if (goal.aiController.human.home != persuer.currentGameLocation || !persuer.isPlayer)
                {
                    if (goal.aiController.human.home != null && !goal.aiController.avoidLocations.Contains(goal.aiController.human.home))
                    {
                        foreach (NewRoom validRoom in goal.aiController.human.home.rooms)
                        {
                            if (validRoom.roomType == InteriorControls.Instance.nullRoomType) continue;
                            if (persuer.currentRoom == validRoom) continue; //Different room

                            foreach (NewNode n in validRoom.nodes)
                            {
                                if (n.gameLocation.thisAsStreet == null && n.floorType != NewNode.FloorTileType.floorAndCeiling && n.floorType != NewNode.FloorTileType.floorOnly) continue;

                                //No entrances
                                if (n.isIndoorsEntrance || n.noAccess || n.noPassThrough) continue;

                                validNodes.Add(n);
                            }
                        }
                    }
                    //Otherwise use street
                    else
                    {
                        foreach (NewRoom validRoom in CityData.Instance.streetDirectory[Toolbox.Instance.Rand(0, CityData.Instance.streetDirectory.Count)].rooms)
                        {
                            //if (validRoom.preset.roomType == InteriorControls.Instance.nullRoomType) continue;
                            if (persuer.currentRoom == validRoom) continue; //Different room

                            foreach (NewNode n in validRoom.nodes)
                            {
                                if (n.gameLocation.thisAsStreet == null && n.floorType != NewNode.FloorTileType.floorAndCeiling && n.floorType != NewNode.FloorTileType.floorOnly) continue;

                                //No entrances
                                if (n.isIndoorsEntrance || n.noAccess || n.noPassThrough) continue;

                                validNodes.Add(n);
                            }
                        }
                    }
                }
                //If the player is in their home, run to somewhere with a public telephone
                else
                {
                    Interactable nearestPublicPhone = Toolbox.Instance.FindNearestWithAction(RoutineControls.Instance.answerTelephone, goal.aiController.human.currentRoom, goal.aiController.human, AIActionPreset.FindSetting.onlyPublic, true, enforcersAllowedEverywhere: false);

                    foreach (NewNode n in nearestPublicPhone.node.room.nodes)
                    {
                        if (n.gameLocation.thisAsStreet == null && n.floorType != NewNode.FloorTileType.floorAndCeiling && n.floorType != NewNode.FloorTileType.floorOnly) continue;

                        //No entrances
                        if (n.isIndoorsEntrance || n.noAccess) continue;

                        validNodes.Add(n);
                    }
                }

                if(validNodes.Count > 0) node = validNodes[Toolbox.Instance.Rand(0, validNodes.Count)];
            }
            //Pick a random location close to the investigate location
            else if (preset.actionLocation == AIActionPreset.ActionLocation.nearbyInvestigate)
            {
                List<NewNode> shortlist = new List<NewNode>();

                //Add nodes up to 3 away...
                List<NewNode> openSet = new List<NewNode>();
                openSet.Add(goal.aiController.investigateLocation);
                int safety = 50;

                //Add to search progress: Every time this is activated, increase the distance from the area by 1
                goal.searchProgress++;

                while (openSet.Count > 0 && safety > 0)
                {
                    NewNode searchNode = openSet[0];

                    shortlist.Add(searchNode);

                    //Encourage searching other rooms by adding extra entries
                    try
                    {
                        if (searchNode.room != goal.aiController.investigateLocation.room)
                        {
                            shortlist.Add(searchNode);
                        }

                        foreach (KeyValuePair<NewNode, NewNode.NodeAccess> pair in searchNode.accessToOtherNodes)
                        {
                            if (pair.Key.room.roomType == InteriorControls.Instance.nullRoomType) continue;
                            if (!pair.Value.walkingAccess) continue; //Skip no walking access
                            //if (pair.Value.toNode.noPassThrough) continue; //Skip no pass through
                            //if (pair.Value.toNode.noAccess) continue; //Skip no access
                            if (pair.Key.isIndoorsEntrance) continue; //Skip entrace way
                            if (pair.Key.room.nodes.Count <= 1) continue; //Skip closets
                            if (pair.Key.gameLocation.thisAsStreet == null && pair.Key.floorType != NewNode.FloorTileType.floorAndCeiling && pair.Key.floorType != NewNode.FloorTileType.floorOnly) continue;

                            //Discount other rooms if no escalation
                            if(goal.aiController.human.escalationLevel <= 0)
                            {
                                if (pair.Key.room != searchNode.room) continue;
                            }
                            //Discount other gamelocation if no escalation
                            else if (goal.aiController.human.escalationLevel <= 1)
                            {
                                if (pair.Key.gameLocation != searchNode.gameLocation) continue;
                            }

                            //Must not trespass
                            if (!goal.preset.allowTrespass)
                            {
                                bool trespass = goal.aiController.human.IsTrespassing(pair.Key.room, out _, out _, goal.preset.allowEnforcersEverywhere);
                                if (trespass) continue;
                            }

                            if (!shortlist.Contains(pair.Key) && !openSet.Contains(pair.Key) && !goal.searchedNodes.Contains(pair.Key))
                            {
                                //Closer than 4 tiles
                                float dist = Vector3.Distance(goal.aiController.investigateLocation.nodeCoord, pair.Key.nodeCoord);

                                if (dist <= (3f + goal.searchProgress))
                                {
                                    openSet.Add(pair.Key);
                                }
                            }
                        }
                    }
                    catch
                    {

                    }

                    openSet.RemoveAt(0);
                    safety--;
                }

                //Pick one...
                node = shortlist[Toolbox.Instance.Rand(0, shortlist.Count)];

                if(node != null)
                {
                    goal.searchedNodes.Add(node);
                    if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("New Action is random nearby investigate location " + node.position, Actor.HumanDebug.actions);
                    if (Game.Instance.collectDebugData) goal.aiController.AddDebugAction("New Action is random nearby investigate location " + node.position);
                }
            }
            else
            {
                if (preset.actionLocation == AIActionPreset.ActionLocation.interactable)
                {
                    if (Game.Instance.collectDebugData)
                    {
                        if (passedInteractable != null) goal.aiController.human.SelectedDebug("Received passed interactable: " + passedInteractable.name + " (" + passedInteractable.id + ")", Actor.HumanDebug.actions);
                        else goal.aiController.human.SelectedDebug("Haven't received a passed interactable", Actor.HumanDebug.actions);
                    }

                    //First check for a passed interactable
                    if (passedInteractable != null && passedInteractable.isActor != null)
                    {
                        interactable = passedInteractable;
                        node = passedInteractable.isActor.currentNode;
                        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Received passed interactable: " + passedInteractable.name, Actor.HumanDebug.actions);
                    }
                    //If passed an interactable directly, check it for this action available
                    else if (passedInteractable != null && (!preset.confirmActionLocation || (passedInteractable.originalPosition && passedInteractable.node != null && passedInteractable.node.room.actionReference.ContainsKey(preset) && passedInteractable.node.room.actionReference[preset].Contains(passedInteractable))))
                    {
                        interactable = passedInteractable;
                        node = passedInteractable.node;
                        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("New Action in passed interactable (" + passedInteractable.name + ")", Actor.HumanDebug.actions);

                        if (node != null && node.accessToOtherNodes.Count <= 0)
                        {
                            Game.Log("AI Error: Action picked node with no access points.");
                        }

                        //goal.aiController.AddDebugAction("New Action in passed interactable (" + passedInteractable.name + ")");
                    }
                    //First check for a passed room
                    else if (passedRoom != null)
                    {
                        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Received passed room (" + passedRoom.GetName() + "), searching for interactables...", Actor.HumanDebug.actions);

                        if (!preset.confirmActionLocation || passedRoom.actionReference.ContainsKey(preset))
                        {
                            List<Interactable> foundInteractables = new List<Interactable>();

                            if(passedRoom.actionReference.ContainsKey(preset))
                            {
                                foundInteractables = passedRoom.actionReference[preset];
                            }

                            //Pick an interactable, enable picking of used ones if there are no unused ones...
                            Interactable.UsagePoint up = null;
                            interactable = InteractablePicker(ref foundInteractables, goal.aiController.human.transform.position, preset.socialRules, out node, out up, meetingGroup: passedGroup);
                            
                            if(up != null) SetUsagePoint(up, preset.usageSlot);
                            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("New Action in passed room (" + passedRoom.GetName() + ") : " + interactable, Actor.HumanDebug.actions);
                        }
                        else if(preset.confirmActionLocation)
                        {
                            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Room " + passedRoom.GetName() + " " + passedRoom.roomID + " does not contain action reference " + preset.name + " ("+ passedRoom.actionReference.Count + ") This will default to the nearest...", Actor.HumanDebug.actions);
                        }

                        if (node != null && node.accessToOtherNodes.Count <= 0)
                        {
                            Game.Log("AI Error: Action picked node with no access points.");
                        }
                    }
                    //Is there a room location tied to the goal?
                    else if (goal.roomLocation != null && goal.roomLocation.actionReference.ContainsKey(preset))
                    {
                        List<Interactable> foundInteractables = goal.roomLocation.actionReference[preset];

                        //Pick an interactable, enable picking of used ones if there are no unused ones...
                        Interactable.UsagePoint up = null;
                        interactable = InteractablePicker(ref foundInteractables, goal.aiController.human.transform.position, preset.socialRules, out node, out up, meetingGroup: passedGroup);
                        if(up != null) SetUsagePoint(up, preset.usageSlot);

                        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("New Action in selected room (" + goal.roomLocation.name + ") : " + interactable, Actor.HumanDebug.actions);

                        if (node != null && node.accessToOtherNodes.Count <= 0)
                        {
                            Game.Log("AI Error: Action picked node with no access points.");
                        }
                    }
                    //Is there a valid item at the goal's game location?
                    else if (goal.gameLocation.actionReference.ContainsKey(preset))
                    {
                        List<Interactable> foundInteractables = goal.gameLocation.actionReference[preset];

                        //Pick an interactable, enable picking of used ones if there are no unused ones...
                        Interactable.UsagePoint up = null;
                        interactable = InteractablePicker(ref foundInteractables, goal.aiController.human.transform.position, preset.socialRules, out node, out up, meetingGroup: passedGroup);
                        if(up != null) SetUsagePoint(up, preset.usageSlot);

                        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("New Action in selected gamelocation (" + goal.gameLocation.name + ") : " + interactable, Actor.HumanDebug.actions);

                        if (node != null && node.accessToOtherNodes.Count <= 0)
                        {
                            Game.Log("AI Error: Action picked node with no access points.");
                        }
                    }
                }
                else if(preset.actionLocation == AIActionPreset.ActionLocation.interactableSpawn)
                {
                    if (passedInteractable != null)
                    {
                        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Received passed interactable: " + passedInteractable.name + " (" + passedInteractable.id + ")", Actor.HumanDebug.actions);
                        interactable = passedInteractable;
                        node = interactable.spawnNode;
                    }
                }
                else if(preset.actionLocation == AIActionPreset.ActionLocation.player)
                {
                    interactable = Player.Instance.interactable;
                    node = Player.Instance.currentNode;
                    if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Action location update with player node", Actor.HumanDebug.actions);
                }

                if(preset.actionLocation == AIActionPreset.ActionLocation.interactableLOS)
                {
                    if (passedInteractable != null)
                    {
                        //Search for nodes with LOS, based
                        List<NewNode> validNodes = new List<NewNode>();

                        NewNode baseNode = passedInteractable.node;

                        if (passedInteractable.isActor != null)
                        {
                            baseNode = passedInteractable.isActor.currentNode;
                        }

                        validNodes.Add(baseNode);

                        List<NewNode> openSet = new List<NewNode>();
                        HashSet<NewNode> closedSet = new HashSet<NewNode>();
                        int safety = 50;

                        while (openSet.Count > 0 && safety > 0)
                        {
                            NewNode currentNode = openSet[0];
                            validNodes.Add(currentNode);

                            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                            {
                                Vector3 sNode = currentNode.nodeCoord + new Vector3Int(v2.x, v2.y, 0);
                                NewNode foundNode = null;

                                if (PathFinder.Instance.nodeMap.TryGetValue(sNode, out foundNode))
                                {
                                    if (!foundNode.noAccess && !foundNode.noPassThrough)
                                    {
                                        if (!openSet.Contains(foundNode))
                                        {
                                            if (!closedSet.Contains(foundNode))
                                            {
                                                Vector3 charHeightIfOnThisNode = foundNode.position;
                                                charHeightIfOnThisNode.y = goal.aiController.human.lookAtThisTransform.position.y;

                                                bool losCheck = Toolbox.Instance.RaycastCheck(charHeightIfOnThisNode, passedInteractable.controller.transform, 10f, out _);

                                                if (losCheck)
                                                {
                                                    openSet.Add(foundNode);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            closedSet.Add(currentNode);
                            openSet.RemoveAt(0);
                            safety--;
                        }

                        //Remove actor's node as a valid option
                        if(validNodes.Count > 1)
                        {
                            validNodes.Remove(baseNode);
                        }

                        //Pick the closest valid node to current position
                        node = validNodes[0];
                        float closest = Mathf.Infinity;

                        foreach (NewNode n in validNodes)
                        {
                            float dist = Vector3.Distance(n.nodeCoord, goal.aiController.human.currentNodeCoord);

                            if(dist < closest)
                            {
                                node = n;
                                closest = dist;
                            }
                        }
                    }
                    else
                    {
                        Game.Log("AI Error: No passed interactable for interactable LOS");
                    }
                }

                if (preset.actionLocation == AIActionPreset.ActionLocation.findNearest || (node == null && forcedNode == null && preset.onUnableToFindLocation == AIActionPreset.ActionFinding.findNearest))
                {
                    if(preset.actionLocation != AIActionPreset.ActionLocation.findNearest)
                    {
                        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(goal.aiController.human.name + " unable to locate node for " + preset.name + "(" + goal.gameLocation.name + "), finding nearest (expensive)...", Actor.HumanDebug.actions);
                    }

                    //if(goal.aiController.human.job != null && goal.aiController.human.job.employer != null)
                    //{
                    //    Game.Log("AI: Job is: " + goal.aiController.human.job.preset.name);
                    //}

                    //Compile ignore list
                    HashSet<NewRoom> ignoreRooms = new HashSet<NewRoom>();

                    if(goal.aiController.human.job != null && goal.aiController.human.job.preset != null && goal.gameLocation != null)
                    {
                        foreach(NewRoom r in goal.gameLocation.rooms)
                        {
                            if(goal.aiController.human.job.preset.bannedRooms.Contains(r.preset))
                            {
                                ignoreRooms.Add(r);
                            }
                        }
                    }

                    NewGameLocation limitTo = null;
                    if (preset.limitSearchToGoalLocation) limitTo = goal.gameLocation;

                    List<Interactable> avoidInteractables = null;
                    if (preset.avoidRepeatingInteractables) avoidInteractables = goal.chosenInteractablesThisGoal;
                    bool useDesireCategory = false;
                    if (goal.preset.locationOption == AIGoalPreset.LocationOption.commercialDecision) useDesireCategory = true;
                    interactable = Toolbox.Instance.FindNearestWithAction(preset, goal.aiController.human.currentRoom, goal.aiController.human, preset.searchSetting, preset.findOverrideWithHome,  ignore: ignoreRooms, filterWithRoomType: preset.filterSearchUsingRoomType, roomTypeFilter: preset.searchRoomType, restrictTo: limitTo, enforcersAllowedEverywhere: goal.preset.allowEnforcersEverywhere, robberyPriority: preset.robberyPriorityMultiplier, avoidInteractables: avoidInteractables, shopItems: passedAcquireItems, mustContainDesireCategory: useDesireCategory, containDesireCategory: goal.preset.desireCategory);

                    if (interactable != null)
                    {
                        if(!goal.chosenInteractablesThisGoal.Contains(interactable)) goal.chosenInteractablesThisGoal.Add(interactable);

                        node = interactable.node;
                        //Game.Log("AI: ...Found interactable at " + node.room.gameLocation.name);
                    }
                    else
                    {
                        //Game.Log("AI: ...Unable to find interactable!");
                    }

                    if (node != null && node.accessToOtherNodes.Count <= 0)
                    {
                        //Game.LogError("Action picked node with no access points.");
                    }

                    goal.aiController.AddDebugAction("New Action, find nearest " + preset.name);
                }

                if (node == null && forcedNode == null && preset.onUnableToFindLocation == AIActionPreset.ActionFinding.removeAction)
                {
                    Game.Log("AI Error: " + goal.aiController.human.name + " unable to locate node for " + preset.name + ", removing as per action setting...");
                    goal.aiController.AddDebugAction(goal.aiController.human.name + " unable to locate node for " + preset.name + ", removing as per action setting...");
                    Remove(preset.repeatDelayOnActionFail);
                    return;
                }
                else if(node == null && forcedNode == null && preset.onUnableToFindLocation == AIActionPreset.ActionFinding.removeGoal)
                {
                    try
                    {
                        Game.Log("AI Error: " + goal.aiController.human.name + " unable to locate node for " + preset.name + ", removing goal " + goal.preset.name + " as per action setting...");
                        goal.aiController.AddDebugAction(goal.aiController.human.name + " unable to locate node for " + preset.name + ", removing goal " + goal.preset.name + " as per action setting...");
                    }
                    catch
                    {

                    }

                    Remove(preset.repeatDelayOnActionFail);
                    goal.Remove();
                    return;
                }
            }

            //if(goal.aiController.bargeComplete && preset == RoutineControls.Instance.bargeDoor)
            //{
            //    goal.aiController.bargeComplete = false;
            //}

            //Get usage point if we haven't already (if there is one)
            if (interactable != null)
            {
                SetUsagePoint(interactable.usagePoint, preset.usageSlot);

                if (usagePoint != null)
                {
                    //If this already busy, look elsewhere. If not successful then proceed anyway as it may be free by the time we get there...
                    Human existing = null;

                    if(usagePoint.TryGetUserAtSlot(preset.usageSlot, out existing))
                    {
                        if(existing != null && existing != goal.aiController.human)
                        {
                            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(existing.name + " is already using furniture slot " + preset.usageSlot, Actor.HumanDebug.actions);
                            UsingFurnitureCheck();
                        }
                    }

                    if(interactable != null && usagePoint != null)
                    {
                        node = usagePoint.node;

                        //Update travel node to that of the use point...
                        Vector3 wPos = usagePoint.GetUsageWorldPosition(goal.aiController.human.transform.position, goal.aiController.human);
                        Vector3Int nodeP = CityData.Instance.RealPosToNodeInt(wPos);
                        NewNode f = null;

                        if (PathFinder.Instance.nodeMap.TryGetValue(nodeP, out f))
                        {
                            node = f;
                        }

                        if(interactable.isActor != null)
                        {
                            node = interactable.isActor.currentNode;
                        }

                        if(node != null && Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Automatic selection of interactable's use point uses node: " + node.nodeCoord + " world pos: " + node.position, Actor.HumanDebug.actions);
                    }
                }

                debugInteractableController = interactable.controller;
            }

            debugForcedNode = false;
            debugForcedNodeWorldPos = Vector3.zero;

            //Override: Forced node
            if (forcedNode != null)
            {
                node = forcedNode;
                debugForcedNode = true;
                debugForcedNodeWorldPos = node.position;
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Node overridden with forced node! This may distrupt any selected use points: " + preset.name, Actor.HumanDebug.actions);
                if (Game.Instance.collectDebugData) goal.aiController.DebugDestinationPosition("Node overridden with forced node! This may distrupt any selected use points: " + preset.name);
            }
            else if (node == null)
            {
                Game.Log("AI Error: " + goal.aiController.human.name + "Cannot execute " + preset.name + " at this location: " + goal.gameLocation + " Error in goal choosing location?");
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Cannot execute " + preset.name + " at this location: " + goal.gameLocation + " Error in goal choosing location?", Actor.HumanDebug.actions);

                //No valid location: Remove action
                Remove(preset.repeatDelayOnActionFail);
                return;
            }

            //Confine AI to location...
            if(goal.aiController.confineLocation != null && goal.aiController.confineLocation != node.gameLocation)
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("AI is confined to location " + goal.aiController.confineLocation.name + ", removing action for " + node.gameLocation.name + " " + preset.name + "...", Actor.HumanDebug.actions);
                if (Game.Instance.collectDebugData) goal.aiController.DebugDestinationPosition("AI is confined to location " + goal.aiController.confineLocation.name + ", removing action for " + node.gameLocation.name + " " + preset.name + "...");
                Remove(preset.repeatDelayOnActionFail);
                return;
            }

            if(goal.aiController.avoidLocations != null && goal.aiController.avoidLocations.Contains(node.gameLocation))
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("AI is avoiding location " + node.gameLocation.name + ", removing action for " + node.gameLocation.name + " " + preset.name + "...", Actor.HumanDebug.actions);
                if (Game.Instance.collectDebugData) goal.aiController.DebugDestinationPosition("AI is avoiding location " + node.gameLocation.name + ", removing action for " + node.gameLocation.name + " " + preset.name + "...");
                Remove(preset.repeatDelayOnActionFail);
                return;
            }

            if(goal.aiController.victimsForMurders.Count > 0)
            {
                foreach(MurderController.Murder m in goal.aiController.victimsForMurders)
                {
                    if (m.location == null) continue;
                    if ((int)m.state < 4) continue;

                    if(m.preset.blockVictimFromLeavingLocation)
                    {
                        if(m.location != node.gameLocation)
                        {
                            if (Game.Instance.collectDebugData)
                            {
                                Game.Log("Murder: AI " + goal.aiController.human.name + " is confined to location " + m.location.name + ", removing action for " + node.gameLocation.name + " " + preset.name + "...");
                                goal.aiController.human.SelectedDebug("AI " + goal.aiController.human.name + " is confined to location " + m.location.name + ", removing action for " + node.gameLocation.name + " " + preset.name + "...", Actor.HumanDebug.actions);
                                goal.aiController.DebugDestinationPosition("AI " + goal.aiController.human.name + " is confined to location " + m.location.name + ", removing action for " + node.gameLocation.name + " " + preset.name + "...");
                            }

                            Remove(preset.repeatDelayOnActionFail);
                            //goal.AITick();
                            return;
                        }
                    }
                }
            }

            if (usagePoint != null)
            {
                debugInteractionUsagePosition = usagePoint.GetUsageWorldPosition(node.position, goal.aiController.human);
            }
            else debugInteractionUsagePosition = Vector3.zero;

            //Force reaction state
            if(preset.forceReactionState)
            {
                goal.aiController.SetReactionState(preset.setReactionState);
            }

            //Trigger speech
            if(preset.onTriggerBark.Count > 0 && Toolbox.Instance.Rand(0f, 1f) <= preset.chanceOfOnTrigger)
            {
                if (InterfaceController.Instance.activeSpeechBubbles.Count <= CitizenControls.Instance.maxSpeechBubbles)
                {
                    goal.aiController.human.speechController.TriggerBark(preset.onTriggerBark[Toolbox.Instance.Rand(0, preset.onTriggerBark.Count)]);
                }
            }

            //Change outfits
            if (preset.specificOutfitOnActivate)
            {
                goal.aiController.human.outfitController.SetCurrentOutfit(preset.allowedOutfitOnActivate);
            }
            else if (preset.makeClothedOnActivate)
            {
                goal.aiController.human.outfitController.MakeClothed();
            }

            //Set expression
            if (preset.setExpressionOnActivate)
            {
                goal.aiController.SetExpression(preset.activateExpression);
            }

            //Are we already at the destination?
            bool atDest = DestinationCheck("OnActionStart: ");

            if (!atDest)
            {
                //Set activation anmation if we're not at the destination
                if (preset.changeIdleOnActivate && !goal.aiController.restrained)
                {
                    if (interactable != null && interactable.preset.specialCaseFlag == InteractablePreset.SpecialCase.forceStanding)
                    {
                        goal.aiController.human.animationController.SetIdleAnimationState(CitizenAnimationController.IdleAnimationState.none);
                    }
                    else
                    {
                        goal.aiController.human.animationController.SetIdleAnimationState(preset.idleAnimationOnActivate);
                    }
                }

                if (preset.changeArmsOnActivate && !goal.aiController.restrained)
                {
                    goal.aiController.human.animationController.SetArmsBoolState(preset.armsAnimationOnActivate);
                }

                SetupPath();
            }

            //Speech trigger point
            goal.aiController.human.SpeechTriggerPoint(DDSSaveClasses.TriggerPoint.onNewAction, null, preset);

            UpdateCombatPose();

            //Update passed room for goal
            if(goal.preset.actionFoundRoomBecomesPassedRoom && !insertedAction && node != null)
            {
                goal.roomLocation = node.room;

                foreach(NewAIAction act in goal.actions)
                {
                    act.passedRoom = node.room;
                }
            }
        }
    }

    //Check and set if this actor has reached the final destination for this action
    public bool DestinationCheck(string debug = "")
    {
        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Destination check...", Actor.HumanDebug.movement);

        //Check mugging
        if(preset.cancelIfNonValidMugging)
        {
            if(passedInteractable != null && passedInteractable.isActor != null)
            {
                Human mug = passedInteractable.isActor as Human;

                if (mug != null)
                {
                    string debugMugging = string.Empty;

                    if (!goal.aiController.IsMuggingValid(mug, out debugMugging))
                    {
                        Game.Log("Failed mugging of " + mug.name + " by " + goal.aiController.human.name + ": " + debugMugging);
                        Remove(preset.repeatDelayOnActionFail);
                        return false;
                    }
                }
            }
        }

        //Check guest pass
        if(preset.skipIfGuestPass)
        {
            NewAddress add = null;

            if (forcedNode != null) add = forcedNode.gameLocation.thisAsAddress;
            if (add == null && passedInteractable != null && passedInteractable.node != null) add = passedInteractable.node.gameLocation.thisAsAddress;

            if(add != null)
            {
                if(GameplayController.Instance.guestPasses.ContainsKey(add))
                {
                    Remove(preset.repeatDelayOnActionFail);
                    return false;
                }
            }
        }

        //Is this now closed?
        if(!goal.preset.allowTrespass && !insertedAction)
        {
            if(goal.aiController.killerForMurders.Count <= 0 || !goal.aiController.killerForMurders.Exists(item => item.state == MurderController.MurderState.executing || item.state == MurderController.MurderState.post))
            {
                if(goal.aiController.human.IsTrespassing(node.room, out _, out _, goal.preset.allowEnforcersEverywhere))
                {
                    if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Would be trespassing at this location...", Actor.HumanDebug.actions);
                    OnUsePointBusy();
                    return false;
                }
            }
        }

        //Are we already at the destination?
        bool atDest = false;

        //Reached the correct node. This reference will be overridden with the forced node if there is one, which may be different to the use point's node.
        if (goal.aiController.human.currentNode == node)
        {
            if (Game.Instance.collectDebugData) goal.aiController.DebugDestinationPosition(debug + "Reached the correct node...");

            //Check if the desired object is still usable if there is one...
            if (!InteractableUsePointCheck())
            {
                OnUsePointBusy();
            }
            else
            {
                //Force an update of the AI node position
                goal.aiController.SetDestinationNode(node, false);

                //If the usage point is null, the correct node will suffice
                if (usagePoint == null)
                {
                    if (Game.Instance.collectDebugData) goal.aiController.DebugDestinationPosition(debug+ "Reached the correct node, and use point is null so setting at desitnation");
                    SetAtDestination(true);
                    atDest = true;
                }
                else
                {
                    Vector3 usePointDest = usagePoint.GetUsageWorldPosition(node.position, goal.aiController.human);

                    //We've reached the destination of the use point
                    if (Vector3.Distance(goal.aiController.human.transform.position, usePointDest) <= 0.05f)
                    {
                        Human existing = null;
                        usagePoint.TryGetUserAtSlot(preset.usageSlot, out existing);

                        if(existing == null || existing == goal.aiController.human)
                        {
                            if (Game.Instance.collectDebugData)
                            {
                                goal.aiController.human.SelectedDebug(debug + "Reached the correct node and the correct use position: " + goal.aiController.human.transform.position + ", existing: " + existing, Actor.HumanDebug.actions);
                                goal.aiController.DebugDestinationPosition(debug + "Reached the correct node and the correct use position: " + goal.aiController.human.transform.position + ", existing: " + existing);
                            }
                            SetAtDestination(true);
                            atDest = true;
                        }
                        else
                        {
                            if (Game.Instance.collectDebugData)
                            {
                                goal.aiController.human.SelectedDebug(debug + "Reached the correct node but the use point is busy", Actor.HumanDebug.actions);
                                goal.aiController.DebugDestinationPosition(debug + "Reached the correct node but the use point is busy");
                            }
                        }
                    }
                    else
                    {
                        //Are we not there because of a forced node?
                        if (node != usagePoint.node && node == forcedNode)
                        {
                            if (Game.Instance.collectDebugData) goal.aiController.DebugDestinationPosition(debug + "Reached the correct node, but it is not the same node as evident on the usage point");
                            SetAtDestination(true);
                            atDest = true;
                        }
                        else
                        {
                            if (Game.Instance.collectDebugData) goal.aiController.DebugDestinationPosition(debug + "Reached the correct node, but not at desitnation position");
                        }
                    }
                }
            }
        }
        else
        {
            //goal.aiController.human.SelectedDebug("Not at correct node...");

            //Update travel node to that of the use point...
            if (usagePoint != null && !usagePoint.useSetting.useDoorBehaviour)
            {
                Vector3 wPos = usagePoint.GetUsageWorldPosition(goal.aiController.human.transform.position, goal.aiController.human);
                Vector3Int nodePos = CityData.Instance.RealPosToNodeInt(wPos);
                NewNode f = null;

                if (PathFinder.Instance.nodeMap.TryGetValue(nodePos, out f))
                {
                    if(f != node)
                    {
                        node = f;
                        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Updated node position", Actor.HumanDebug.actions);
                        return DestinationCheck("Recheck destination after use point update...");
                    }
                    else
                    {
                        MovementDestinationCheck(f);
                    }
                }
                else
                {
                    if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Unable to find node pos: " + wPos, Actor.HumanDebug.actions);
                }
            }
            //else if(usagePoint != null && usagePoint.useSetting.useDoorBehaviour && forcedNode != null && forcedNode != usagePoint.node)
            //{
            //    //Remove forced node
            //    forcedNode = null;
            //    node = usagePoint.node;
            //    return DestinationCheck("Recheck destination after use point update...");
            //}
            //Attack within range equals destination
            else if(preset.attackPersuitTargetOnProximity && goal.aiController.persuitTarget != null)
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(debug + " attempt attack...", Actor.HumanDebug.attacks);

                //Is close enough
                if (goal.aiController.seesOnPersuit && !goal.aiController.persuitTarget.isDead && !goal.aiController.persuitTarget.isStunned && goal.aiController.human.escalationLevel >= 2)
                {
                    float dist = Vector3.Distance(goal.aiController.persuitTarget.transform.position, goal.aiController.transform.position);

                    if(dist <= goal.aiController.weaponRangeMax && dist >= goal.aiController.currentWeaponPreset.minimumRange)
                    {
                        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(debug + " within weapon range, setting at destination to trigger attack...", Actor.HumanDebug.attacks);

                        atDest = true;

                        //Force an update of the AI node position
                        goal.aiController.SetDestinationNode(node, false);
                        SetAtDestination(true, true);
                    }
                    else
                    {
                        if (Game.Instance.collectDebugData)
                        {
                            if (dist > goal.aiController.weaponRangeMax) goal.aiController.human.SelectedDebug(debug + " too far to use weapon (" + dist + "/" + goal.aiController.weaponRangeMax + ")", Actor.HumanDebug.attacks);
                            else if (dist < goal.aiController.currentWeaponPreset.minimumRange) goal.aiController.human.SelectedDebug(debug + " too close to use weapon (" + dist + "/" + goal.aiController.currentWeaponPreset.minimumRange + ")", Actor.HumanDebug.attacks);
                        }

                        if (preset.throwObjectsAtTarget && !goal.preset.disableThrowing && goal.aiController.killerForMurders.Count <= 0)
                        {
                            if (dist <= CitizenControls.Instance.throwMaxRange && dist >= CitizenControls.Instance.throwMinRange)
                            {
                                if (goal.aiController.human.currentConsumables.Count > 0 || goal.aiController.human.trash.Count > 0)
                                {
                                    if(goal.aiController.attackDelay <= 0f && !goal.aiController.attackActive && goal.aiController.throwDelay <= 0f)
                                    {
                                        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(debug + " within throwing range, setting at destination to trigger attack...", Actor.HumanDebug.attacks);

                                        atDest = true;

                                        //Force an update of the AI node position
                                        goal.aiController.SetDestinationNode(node, false);
                                        SetAtDestination(true, true);
                                    }
                                }
                            }
                        }
                    }
                }
                else if(Game.Instance.collectDebugData)
                {
                    if (goal.aiController.persuitTarget.isDead) goal.aiController.human.SelectedDebug(debug + " Target is dead", Actor.HumanDebug.attacks);
                    if (goal.aiController.persuitTarget.isStunned) goal.aiController.human.SelectedDebug(debug + " Target is stunned", Actor.HumanDebug.attacks);
                    if (!goal.aiController.seesOnPersuit) goal.aiController.human.SelectedDebug(debug + " SeesOnPersuit is not true", Actor.HumanDebug.attacks);
                    if (goal.aiController.human.escalationLevel < 2) goal.aiController.human.SelectedDebug(debug + " Escalation level is too low: " + goal.aiController.human.escalationLevel, Actor.HumanDebug.attacks);
                }
            }
            else if(node == null)
            {
                goal.aiController.DebugDestinationPosition(debug + ": Null node detected!");
                SetAtDestination(true);
                atDest = true;
            }
            else
            {
                //goal.aiController.DebugDestinationPosition(debug + ": Not at the correct node (" + node.position  + ")");
                SetAtDestination(false);

                MovementDestinationCheck(node);

                //Check LOS
                if (preset.useLOSCheck)
                {
                    LOSCheck();
                }
            }
        }

        return atDest;
    }

    public void MovementDestinationCheck(NewNode resetNode)
    {
        //goal.aiController.human.SelectedDebug("Checking movement vs destination " + goal.aiController.transform.position + "/" + goal.aiController.currentDestinationPositon);

        if (goal.aiController.currentDestinationPositon == goal.aiController.transform.position)
        {
            //goal.aiController.human.SelectedDebug("Destination node matches, and arrived at current destination position. Attempting to reset destination node...");
            goal.aiController.SetDestinationNode(resetNode);
        }
        else
        {
            //goal.aiController.human.SelectedDebug("Destination node matches: Should be travelling to node " + resetNode.position + ", destination pos = " + goal.aiController.currentDestinationPositon);
        }
    }

    //Set the usage point for this action
    public void SetUsagePoint(Interactable.UsagePoint newUsagePoint, Interactable.UsePointSlot newSlot)
    {
        if (preset.actionLocation == AIActionPreset.ActionLocation.interactableSpawn)
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Action location is interactable spawn, so no usage point should be set...", Actor.HumanDebug.actions);
            usagePoint = null;
            return;
        }

        if (Game.Instance.collectDebugData)
        {
            if (newUsagePoint != null) goal.aiController.human.SelectedDebug("Setting usage point... " + newUsagePoint.interactable.name, Actor.HumanDebug.actions);
            else goal.aiController.human.SelectedDebug("Setting usage point to null", Actor.HumanDebug.actions);
        }

        if (newUsagePoint == usagePoint) return; //Is the same
        else
        {
            //There is an existing usage point...
            if(usagePoint != null)
            {
                usagePoint.RemoveUserFromAllSlots(goal.aiController.human); //Remove references to this but do not set as null.
            }

            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Set new usage point slot " + newSlot, Actor.HumanDebug.actions);
            usagePoint = newUsagePoint;
        }
    }

    //Check to see if the currently-set use point is valid, if not then pick a new one
    public bool InteractableUsePointCheck()
    {
        //I don't have a usage point, or the current one is full.
        Human existing = null;

        if (usagePoint != null)
        {
            try
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Interactable use point check for: " + usagePoint.interactable.name + " " + usagePoint.interactable.id + " at " + usagePoint.interactable.node.position, Actor.HumanDebug.actions);
            }
            catch
            {

            }
            
            usagePoint.TryGetUserAtSlot(preset.usageSlot, out existing);
        }

        if (usagePoint == null)
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("The currently set usage point is null...", Actor.HumanDebug.actions);
            return true;
        }
        else if (existing == null)
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("The currently set usage point is empty...", Actor.HumanDebug.actions);
            return true;
        }
        else if(existing != null)
        {
            if(existing == goal.aiController.human)
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("The existing usage point user is me: " + existing.name, Actor.HumanDebug.actions);
                return true;
            }
            else if(existing.ai != null && existing.ai.currentAction != null && existing.ai.currentAction.usagePoint != usagePoint)
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Invalid usage point detected for " + existing.name + ", their usage point is elsewhere and this did not unassign correctly", Actor.HumanDebug.actions);
            }

            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("The currently set usage point is full ("+ existing.name + "), searching for other use points at the same furniture...", Actor.HumanDebug.actions);
            return false;
        }
        else if(interactable != null)
        {
            return false;
        }

        return true;
    }

    //Executed when use point is busy
    public void OnUsePointBusy()
    {
        if(preset.armsAnimationOnArrival != CitizenAnimationController.ArmsBoolSate.none)
        {
            goal.aiController.human.animationController.SetArmsBoolState(CitizenAnimationController.ArmsBoolSate.none);
        }

        if (preset.idleAnimationOnArrival != CitizenAnimationController.IdleAnimationState.none)
        {
            goal.aiController.human.animationController.SetIdleAnimationState(CitizenAnimationController.IdleAnimationState.none);
        }

        if (!actionCheckRecursion && preset.onUsePointBusy == AIActionPreset.ActionBusy.findAlternate)
        {
            //Do a current action user check anyway as it may be preferable to sit somewhere else. This has to be fairly fast as it happens often...
            if (UsingFurnitureCheck())
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("...Successfully found an alternate interactable solution for current action.", Actor.HumanDebug.actions);
                actionCheckRecursion = true;
                SetupPath(true);
                return;
            }
            //If this is a success then we need a new path...
            else
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("...Failed to find alternate interactable solution for current action, postponing goal...", Actor.HumanDebug.actions);

                //Cancel current action: Effectively wait...
                //Deactivate goal
                if (goal != null)
                {
                    actionCheckRecursion = true;
                    goal.OnDeactivate(goal.preset.repeatDelayOnBusy);
                    return;
                }
            }
        }
        else if (actionCheckRecursion || preset.onUsePointBusy == AIActionPreset.ActionBusy.skipAction)
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("...Skipping action only...", Actor.HumanDebug.actions);
            actionCheckRecursion = true;
            Remove(preset.repeatDelayOnActionFail);
            return;
        }
        else if (preset.onUsePointBusy == AIActionPreset.ActionBusy.skipGoal || (preset.onUsePointBusy == AIActionPreset.ActionBusy.standGuardIfEnforcerSkipGoalNot && (!goal.aiController.human.isEnforcer || !goal.aiController.human.isOnDuty)))
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("...Skipping goal...", Actor.HumanDebug.actions);

            if (goal != null)
            {
                actionCheckRecursion = true;
                goal.OnDeactivate(goal.preset.repeatDelayOnBusy);
                return;
            }
        }
        else if (preset.onUsePointBusy == AIActionPreset.ActionBusy.standGuard || (preset.onUsePointBusy == AIActionPreset.ActionBusy.standGuardIfEnforcerSkipGoalNot && goal.aiController.human.isEnforcer && goal.aiController.human.isOnDuty))
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("...Skipping action, standing guard...", Actor.HumanDebug.actions);

            Interactable findStandAgainstWall = Toolbox.Instance.FindNearestWithAction(RoutineControls.Instance.standAgainstWall, goal.aiController.human.currentRoom, goal.aiController.human, AIActionPreset.FindSetting.onlyPublic, restrictTo: goal.aiController.human.currentGameLocation, excludeAIUsingThis: true);

            if (findStandAgainstWall != null)
            {
                if(!goal.TryInsertInteractableAction(findStandAgainstWall, RoutineControls.Instance.standAgainstWall, 99, duplicateActionCheck: true))
                {
                    actionCheckRecursion = true;
                    Remove(preset.repeatDelayOnActionFail);
                    return;
                }
            }
            else
            {
                findStandAgainstWall = Toolbox.Instance.FindNearestWithAction(RoutineControls.Instance.standGuard, goal.aiController.human.currentRoom, goal.aiController.human, AIActionPreset.FindSetting.onlyPublic, restrictTo: goal.aiController.human.currentGameLocation, excludeAIUsingThis: true);

                if (!goal.TryInsertInteractableAction(findStandAgainstWall, RoutineControls.Instance.standGuard, 99, duplicateActionCheck: true))
                {
                    actionCheckRecursion = true;
                    Remove(preset.repeatDelayOnActionFail);
                    return;
                }
            }

            actionCheckRecursion = true;
        }
    }

    //Update combat pose based on preset
    public void UpdateCombatPose()
    {
        //Put in combat mode
        if (preset.useCombatPose == AIActionPreset.CombatPose.always)
        {
            if (!preset.onlyUseCombatPoseWithEscalationOne || (preset.onlyUseCombatPoseWithEscalationOne && goal.aiController.human.escalationLevel >= 2))
            {
                goal.aiController.SetInCombat(true);
            }
            else
            {
                goal.aiController.SetInCombat(false);
            }
        }
        else if (preset.useCombatPose == AIActionPreset.CombatPose.never)
        {
            goal.aiController.SetInCombat(false);
        }
        else if (preset.useCombatPose == AIActionPreset.CombatPose.onlyWhenPreviouslyPersuing)
        {
            if (goal.aiController.persuitTarget != null && (!preset.onlyUseCombatPoseWithEscalationOne || (preset.onlyUseCombatPoseWithEscalationOne && goal.aiController.human.escalationLevel >= 2)))
            {
                goal.aiController.SetInCombat(true);
            }
            else
            {
                goal.aiController.SetInCombat(false);
            }
        }
    }

    //Pick a path to the destination in preparation for movement. Also can be used to reset the movement.
    public void SetupPath(bool scanForNextNodeFurniture = true)
    {
        try
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Setting up path to node " + node.position + " from " + goal.aiController.human.currentNode.position + " , scan for next node furniture: " + scanForNextNodeFurniture, Actor.HumanDebug.movement);

            //Time estimate
            if (Game.Instance.collectRoutineTimingInfo)
            {
                estimatedArrival = SessionData.Instance.gameTime + Toolbox.Instance.TravelTimeEstimate(goal.aiController.human, goal.aiController.human.currentNode, node);
            }

            //Get the path
            path = PathFinder.Instance.GetPath(goal.aiController.human.currentNode, node, goal.aiController.human);
        }
        catch
        {

        }

        if (path == null)
        {
            //No valid location: Remove action
            Remove(preset.repeatDelayOnActionFail);
            return;
        }

        //Do some wizzardry to stop doubling back: This actor could be on its way to a new node that is infact already on this path, but the above might cause it to double back first
        //Does my current destination node match anywhere at the beginning of this path
        //if (goal.aiController.currentDestinationNode != null)
        //{
        //    for (int i = 0; i < Mathf.Min(2, path.accessList.Count); i++)
        //    {
        //        if (path.accessList[i].toNode == goal.aiController.currentDestinationNode)
        //        {
        //            //Remove start of path up to this...
        //            for (int u = 0; u < i; u++)
        //            {
        //                goal.aiController.human.SelectedDebug("Removing " + path.accessList[i].toNode.position + " from path (doubling back)");
        //                path.accessList.RemoveAt(0);
        //            }

        //            break;
        //        }
        //    }
        //}

        //Set first destination node
        goal.aiController.pathCursor = -1; //Reset path cursor

        goal.aiController.ReachNewPathNode("Start action", scanForNextNodeFurniture);
        //goal.aiController.SetDestinationNode(path.accessList[0].fromNode);

        if (path != null)
        {
            if (Game.Instance.collectDebugData) goal.aiController.DebugDestinationPosition("Set up new path with " + path.accessList.Count + " nodes for action " + preset.name + ", set destination to false...");
        }

        SetAtDestination(false);
        goal.aiController.SetUpdateEnabled(true); //Make sure controller update is enabled as we're about to move.

        if (path != null && path.accessList.Count <= 0)
        {
            DestinationCheck("Checking for destination as path list is 0");
            //goal.aiController.DebugDestinationPosition("Setting at destination as path list is 0");
            //SetAtDestination(true);
        }
    }

    //This is triggered when the AI is one space away and somebody is using the furniture...
    public bool UsingFurnitureCheck()
    {
        if (interactable == null) return false;

        //Scan room for nearest alternate...
        List<Interactable> ignoreList = new List<Interactable>();
        ignoreList.Add(interactable);

        //Next attempt other furniture in this room...
        if(interactable.node == null)
        {
            Game.Log("Attempting update update node for interactable " + interactable.name + " as node is missing...");
            interactable.UpdateWorldPositionAndNode(false);
        }
        List<Interactable> foundInteractables = new List<Interactable>();

        if (interactable != null && interactable.node != null && interactable.node.room.actionReference.ContainsKey(preset))
        {
            foundInteractables = interactable.node.room.actionReference[preset];
        }

        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("...Scanning " + foundInteractables.Count + " entries in same room...", Actor.HumanDebug.actions);

        Interactable newChoice = InteractablePicker(ref foundInteractables, goal.aiController.human.transform.position, preset.socialRules, out node, out usagePoint, meetingGroup: passedGroup, ignore: ignoreList);

        if(newChoice != null)
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Found another interactable within the same room: " + newChoice.name, Actor.HumanDebug.actions);

            interactable = newChoice;
            SetUsagePoint(interactable.usagePoint, preset.usageSlot);
            node = usagePoint.node;
            return true;
        }

        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Couldn't find another node on the same furniture, checking gamelocation for other interactables...", Actor.HumanDebug.actions);

        //Next attempt other furniture in this address...
        if(interactable.node.gameLocation.actionReference.ContainsKey(preset))
        {
            foundInteractables = interactable.node.gameLocation.actionReference[preset];
        }

        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("...Scanning " + foundInteractables.Count + " entries...", Actor.HumanDebug.actions);

        newChoice = InteractablePicker(ref foundInteractables, goal.aiController.human.transform.position, preset.socialRules, out node, out usagePoint, meetingGroup: passedGroup, ignore: ignoreList);

        if (newChoice != null)
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Found another interactable within the same gamelocation: " + newChoice.name, Actor.HumanDebug.actions);

            interactable = newChoice;
            SetUsagePoint(interactable.usagePoint, preset.usageSlot);
            node = usagePoint.node;
            return true;
        }

        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Couldn't find other furniture within the same address, checking for nearest public interactables...", Actor.HumanDebug.actions);

        //Find nearest public
        if(!preset.limitSearchToGoalLocation)
        {
            HashSet<NewRoom> igList = new HashSet<NewRoom>(interactable.node.gameLocation.rooms);

            bool useDesireCategory = false;
            if (goal.preset.locationOption == AIGoalPreset.LocationOption.commercialDecision) useDesireCategory = true;
            Interactable nearestPublic = Toolbox.Instance.FindNearestWithAction(preset, goal.aiController.human.currentRoom, goal.aiController.human, AIActionPreset.FindSetting.nonTrespassing, true, ignore: igList, filterWithRoomType: preset.filterSearchUsingRoomType, roomTypeFilter: preset.searchRoomType, enforcersAllowedEverywhere: goal.preset.allowEnforcersEverywhere, shopItems: passedAcquireItems, mustContainDesireCategory: useDesireCategory, containDesireCategory: goal.preset.desireCategory);

            if (nearestPublic != null)
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Found another interactable - nearest public", Actor.HumanDebug.actions);
                SetUsagePoint(nearestPublic.usagePoint, preset.usageSlot);
                node = usagePoint.node;
                interactable = nearestPublic;
                return true;
            }
        }

        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("...Failed to find any valid interactable interaction spaces on this or any other furniture :(", Actor.HumanDebug.actions);

        return false;
    }

    //Triggered when this action becomes inactive
    public void OnDeactivate(bool executeDeactivateAnimation = true)
    {
        actionCheckRecursion = false;

        EndSoundLoop();

        //Execute additional actions
        if (preset.executeCompleteActionsOnEnd && !completed && (!preset.executeCompleteActionsOnEndIfArrived || isAtLocation))
        {
            ExecuteAdditionalActions(ref preset.forcedActionsOnComplete);
        }

        if(!completed && isAtLocation)
        {
            ExecuteEndSwitchChanges();
        }

        if (Game.Instance.collectDebugData)
        {
            goal.aiController.DebugDestinationPosition("Deactivate action: " + preset.name);
            goal.aiController.human.SelectedDebug("Deactivated action: " + preset.name + " actor position: " + goal.aiController.transform.position, Actor.HumanDebug.actions);
        }

        if (usagePoint != null)
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Deactivate: Setting usage point to null...", Actor.HumanDebug.actions);
            Human existing = null;
            usagePoint.TryGetUserAtSlot(preset.usageSlot, out existing);

            if (existing == goal.aiController.human) usagePoint.TrySetUser(preset.usageSlot, null, "OnDeactivate action " + preset.name);
        }
        else if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Deactivate: Usage point is already null...", Actor.HumanDebug.actions);

        SetUsagePoint(null, preset.usageSlot);

        //Remove as next AI action
        CancelNextAIInteraction();

        //Change outfits
        if (preset.specificOutfitOnDeactivate && !goal.aiController.restrained)
        {
            goal.aiController.human.outfitController.SetCurrentOutfit(preset.allowedOutfitOnDeactivate);
        }
        else if (preset.makeClothedOnDeactivate && !goal.aiController.restrained)
        {
            goal.aiController.human.outfitController.MakeClothed();
        }

        //Set expression
        if (preset.setExpressionOnDeactivate && !goal.aiController.restrained)
        {
            goal.aiController.SetExpression(preset.deactivateExpression);
        }

        //Set anmation
        if(executeDeactivateAnimation)
        {
            if (preset.changeIdleOnDeactivate && !goal.aiController.restrained)
            {
                goal.aiController.human.animationController.SetIdleAnimationState(preset.idleAnimationOnDeactivate);
            }

            if (preset.changeArmsOnDeactivate && !goal.aiController.restrained)
            {
                goal.aiController.human.animationController.SetArmsBoolState(preset.armsAnimationOnDeactivate);
            }
        }

        if (isActive)
        {
            goal.aiController.currentAction = null;
            isActive = false;
            node = null;
            path = null;

            if (Game.Instance.collectDebugData)
            {
                goal.aiController.human.SelectedDebug("Deactivated previously-active action: " + preset.name, Actor.HumanDebug.actions);
                goal.aiController.DebugDestinationPosition("Set at destination to false on deactivate of new action");
            }

            if (preset.limitTickRate) goal.aiController.UpdateTickRate(); //If this could have affected tick rate, update it
            SetAtDestination(false);

            //Get out of combat
            //if(goal.aiController.inCombat)
            //{
            //    goal.aiController.SetInCombat(false);
            //}
        }

        //Remove items
        goal.aiController.UpdateHeldItems(AIActionPreset.ActionStateFlag.onDeactivation);
    }

    public void CancelNextAIInteraction()
    {
        //Remove as next AI action
        if (passedInteractable != null)
        {
            if (passedInteractable.nextAIInteraction == this)
            {
                passedInteractable.SetNextAIInteraction(null, goal.aiController);
            }
        }

        if (interactable != null)
        {
            if (interactable.nextAIInteraction == this)
            {
                interactable.SetNextAIInteraction(null, goal.aiController);
            }
        }
    }

    //Complete this action
    public void Complete()
    {
        completed = true;

        EndSoundLoop();

        //Change outfits
        if (preset.specificOutfitOnComplete)
        {
            goal.aiController.human.outfitController.SetCurrentOutfit(preset.allowedOutfitOnComplete);
        }
        else if (preset.makeClothedOnActivate)
        {
            goal.aiController.human.outfitController.MakeClothed();
        }

        //Set expression
        if (preset.setExpressionOnComplete)
        {
            goal.aiController.SetExpression(preset.completeExpression);
        }

        //Set anmation
        if (preset.changeIdleOnComplete && !goal.aiController.restrained)
        {
            goal.aiController.human.animationController.SetIdleAnimationState(preset.idleAnimationOnComplete);
        }

        if (preset.changeArmsOnComplete && !goal.aiController.restrained)
        {
            goal.aiController.human.animationController.SetArmsBoolState(preset.armsAnimationOnComplete);
        }

        //Remove as next AI action
        CancelNextAIInteraction();

        //Do action
        if (preset.executeThisOnComplete)
        {
            if(interactable != null)
            {
                InteractablePreset.InteractionAction action = null;

                if (interactable.aiActionReference.TryGetValue(preset, out action))
                {
                    if(Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Executing action " + preset.name + " in interactable " + interactable.name + " " + interactable.id, Actor.HumanDebug.actions);
                    interactable.OnInteraction(action, goal.aiController.human);
                }
                else
                {
                    if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Could not find action " + preset.name + " in interactable " + interactable.name + " " + interactable.id, Actor.HumanDebug.actions);
                }
            }
            else
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Could not find action " + preset.name + " because interactable isn't part of this action", Actor.HumanDebug.actions);
            }
        }

        //Forced actions
        ExecuteAdditionalActions(ref preset.forcedActionsOnComplete);
        ExecuteEndSwitchChanges();

        //Trigger speech
        if (preset.onCompleteBark.Count > 0 && Toolbox.Instance.Rand(0f, 1f) <= preset.chanceOfOnComplete)
        {
            if (InterfaceController.Instance.activeSpeechBubbles.Count <= CitizenControls.Instance.maxSpeechBubbles)
            {
                goal.aiController.human.speechController.TriggerBark(preset.onCompleteBark[Toolbox.Instance.Rand(0, preset.onCompleteBark.Count)]);
            }
        }

        ////Wake up if needed
        //if (preset.sleepOnArrival)
        //{
        //    goal.aiController.human.WakeUp();
        //}

        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Completed action: " + preset.name, Actor.HumanDebug.actions);

        goal.OnCompletedAction(this);
        Remove(preset.repeatDelayOnActionSuccess);
    }

    //Execute additional actions
    private void ExecuteAdditionalActions(ref List<AIActionPreset.AutomaticAction> actionPresets)
    {
        if (interactable == null) return;

        //Forced arrival actions
        foreach (AIActionPreset.AutomaticAction forcedEnd in actionPresets)
        {
            InteractablePreset.InteractionAction forcedAction = null;

            if(forcedEnd.proximityCheck)
            {
                if (interactable == null) continue;

                float dist = 0;

                if(interactable.isActor != null)
                {
                    if (!goal.aiController.trackedTargets.Exists(item => item.actor != null && item.actor == interactable.isActor)) continue;
                    dist = Vector3.Distance(interactable.isActor.transform.position, goal.aiController.human.transform.position);
                }
                else dist = Vector3.Distance(interactable.wPos, goal.aiController.human.transform.position);

                if (dist > 3) continue; //Pass on this
            }

            if (interactable.aiActionReference.TryGetValue(forcedEnd.forcedAction, out forcedAction))
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Force action: " + forcedAction.interactionName, Actor.HumanDebug.actions);
                interactable.OnInteraction(forcedAction, goal.aiController.human, true, additionalDelay: forcedEnd.additionalDelay);
            }
            //Search lock interactable...
            else if (interactable.lockInteractable != null && interactable.lockInteractable.aiActionReference.TryGetValue(forcedEnd.forcedAction, out forcedAction))
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Force action found on lock: " + forcedAction.interactionName, Actor.HumanDebug.actions);
                interactable.OnInteraction(forcedAction, goal.aiController.human, true, additionalDelay: forcedEnd.additionalDelay);
            }
            //If we cannot find the action in the main object, try other interactables on this furniture...
            else
            {
                bool found = false;

                if(interactable.furnitureParent != null)
                {
                    //First try integrated interactables...
                    if((int)preset.forcedActionsSearchLevel >= 1)
                    {
                        foreach (Interactable i in interactable.furnitureParent.integratedInteractables)
                        {
                            if (i == interactable) continue;

                            if (i.aiActionReference.TryGetValue(forcedEnd.forcedAction, out forcedAction))
                            {
                                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Force action: " + forcedAction.interactionName, Actor.HumanDebug.actions);
                                i.OnInteraction(forcedAction, goal.aiController.human, true, additionalDelay: forcedEnd.additionalDelay);
                                found = true;
                                break;
                            }
                        }
                    }

                    if(!found && (int)preset.forcedActionsSearchLevel == 2)
                    {
                        //Find the name of the inegrated interactable...
                        //Look at the prefab and find the local positions of all the controllers. We can use this for localpositions.
                        List<InteractableController> controllerLocs = interactable.furnitureParent.furniture.prefab.GetComponentsInChildren<InteractableController>(true).ToList();

                        //Find their proper spawn positions...
                        InteractableController correspondingController = controllerLocs.Find(item => item.id == (InteractableController.InteractableID)interactable.pt);

                        if (correspondingController == null)
                        {
                            string interactableParentName = correspondingController.gameObject.name;

                            //Next try interactables spawned on the same furniture that are children of the interacting...
                            foreach (Interactable i in interactable.furnitureParent.spawnedInteractables)
                            {
                                if (i == interactable) continue;

                                if (i.aiActionReference.TryGetValue(forcedEnd.forcedAction, out forcedAction))
                                {
                                    if(i.subObject.parent == interactableParentName)
                                    {
                                        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Force action: " + forcedAction.interactionName, Actor.HumanDebug.actions);
                                        i.OnInteraction(forcedAction, goal.aiController.human, true, additionalDelay: forcedEnd.additionalDelay);
                                        found = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else if (!found && (int)preset.forcedActionsSearchLevel >= 3)
                    {
                        //Next try interactables spawned on the same furniture that are children of the interacting...
                        foreach (Interactable i in interactable.furnitureParent.spawnedInteractables)
                        {
                            if (i == interactable) continue;

                            if (i.aiActionReference.TryGetValue(forcedEnd.forcedAction, out forcedAction))
                            {
                                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Force action: " + forcedAction.interactionName, Actor.HumanDebug.actions);
                                i.OnInteraction(forcedAction, goal.aiController.human, true, additionalDelay: forcedEnd.additionalDelay);
                                found = true;
                                break;
                            }
                        }
                    }

                }

                if(!found && preset.forcedActionsSearchLevel == AIActionPreset.ForcedActionsSearchLevel.InteractablesOnNode)
                {
                    //Lastly try interactables on this node
                    foreach (Interactable i in interactable.node.interactables)
                    {
                        if (i == interactable) continue;

                        if (i.aiActionReference.TryGetValue(forcedEnd.forcedAction, out forcedAction))
                        {
                            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Force action: " + forcedAction.interactionName, Actor.HumanDebug.actions);
                            i.OnInteraction(forcedAction, goal.aiController.human, true, additionalDelay: forcedEnd.additionalDelay);
                            break;
                        }
                    }
                }

            }
        }
    }

    //Executed when the action is ended
    public void ExecuteEndSwitchChanges()
    {
        if (preset.switchStatesOnEnd.Count > 0 && interactable != null)
        {
            foreach (InteractablePreset.SwitchState st in preset.switchStatesOnEnd)
            {
                if (st.switchState == InteractablePreset.Switch.switchState)
                {
                    interactable.SetSwitchState(st.boolIs, goal.aiController.human);
                }
                else if (st.switchState == InteractablePreset.Switch.lockState)
                {
                    interactable.SetLockedState(st.boolIs, goal.aiController.human);
                }
                else if (st.switchState == InteractablePreset.Switch.custom1)
                {
                    interactable.SetCustomState1(st.boolIs, goal.aiController.human);
                }
                else if (st.switchState == InteractablePreset.Switch.custom2)
                {
                    interactable.SetCustomState2(st.boolIs, goal.aiController.human);
                }
                else if (st.switchState == InteractablePreset.Switch.custom3)
                {
                    interactable.SetCustomState3(st.boolIs, goal.aiController.human);
                }
                else if (st.switchState == InteractablePreset.Switch.carryPhysicsObject)
                {
                    interactable.SetPhysicsPickupState(st.boolIs, goal.aiController.human);
                }
            }
        }
    }

    //Remove this action
    public void Remove(float delayReactivationTime = 0)
    {
        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Removing action " + preset.name + " (repeat delay of " + delayReactivationTime + ")", Actor.HumanDebug.actions);

        EndSoundLoop();

        if (delayReactivationTime > 0f)
        {
            if (!goal.aiController.delayedActionsForTime.ContainsKey(preset))
            {
                goal.aiController.delayedActionsForTime.Add(preset, 0f);
            }

            goal.aiController.delayedActionsForTime[preset] = SessionData.Instance.gameTime + delayReactivationTime;
        }

        //Remove user
        if (usagePoint != null)
        {
            Human existing = null;
            usagePoint.TryGetUserAtSlot(preset.usageSlot, out existing);

            if (existing == goal.aiController.human) usagePoint.TrySetUser(preset.usageSlot, null, "OnRemove action: " + preset.name);
            SetUsagePoint(null, preset.usageSlot);
        }
        //else goal.aiController.human.SelectedDebug("Usage point is already null...");

        //Remove as next AI action
        CancelNextAIInteraction();

        //If this is currently active, set current to null
        if (isActive)
        {
            goal.aiController.currentAction = null;
            isActive = false;


            ////Wake up if needed
            //if(preset.sleepOnArrival)
            //{
            //    goal.aiController.human.WakeUp();
            //}

            if (preset.limitTickRate) goal.aiController.UpdateTickRate(); //If this could have affected tick rate, update it
        }

        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Deactivate action: " + preset.name, Actor.HumanDebug.actions);

        goal.actions.Remove(this);
    }

    public void TriggerArrivalSound()
    {
        if(preset.onArrivalSound != null)
        {
            Vector3 wp = goal.aiController.human.transform.position;
            if (interactable != null) wp = interactable.GetWorldPosition();

            if (preset.isLoop)
            {
                if(audioLoop != null)
                {
                    AudioController.Instance.StopSound(audioLoop, AudioController.StopType.immediate, "Start of new action requires new loop");
                    audioLoop = null;
                }

                audioLoop = AudioController.Instance.PlayWorldLooping(preset.onArrivalSound, goal.aiController.human, interactable);
            }
            else
            {
                if (preset.soundDelay > 0f)
                {
                    AudioController.Instance.PlayOneShotDelayed(preset.soundDelay, preset.onArrivalSound, goal.aiController.human, goal.aiController.human.currentNode, wp);
                }
                else
                {
                    AudioController.Instance.PlayWorldOneShot(preset.onArrivalSound, goal.aiController.human, goal.aiController.human.currentNode, wp, interactable: interactable);
                }
            }
        }
    }

    public void EndSoundLoop()
    {
        if (audioLoop != null)
        {
            AudioController.Instance.StopSound(audioLoop, AudioController.StopType.fade, "End of action");
            audioLoop = null;
        }
    }

    //Triggered when active on tick
    public void AITick()
    {
        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Action AITick: " + preset.name, Actor.HumanDebug.updates);

        Human existing = null;

        if(usagePoint != null)
        {
            //Update usage point position if this is an actor...
            if(interactable != null && interactable.isActor != null)
            {
                usagePoint.node = interactable.isActor.currentNode;
                usagePoint.PositionUpdate();
                node = usagePoint.node;
            }

            usagePoint.TryGetUserAtSlot(preset.usageSlot, out existing);
        }

        //Do switch state checks to see if I still need to continue with this action
        if(preset.completableAction && interactable != null && usagePoint != null && existing != goal.aiController.human && isActive && !completed)
        {
            foreach(AIActionPreset.CheckActionAgainstState stateCheck in preset.checkActionAgainstState)
            {
                if(InteractableStateCheck(stateCheck))
                {
                    if (stateCheck.outcome == AIActionPreset.CheckActionOutcome.cancelAction)
                    {
                        Remove(preset.repeatDelayOnActionFail);
                        return;
                    }
                    else if (stateCheck.outcome == AIActionPreset.CheckActionOutcome.cancelGoal)
                    {
                        goal.Remove();
                        return;
                    }
                }
            }
        }

        //Check LOS
        if(preset.useLOSCheck)
        {
            LOSCheck();
        }

        if(isAtLocation)
        {
            //Measure time since last tick at destination...
            float timeSinceLast = SessionData.Instance.gameTime - lastRecordedTickWhileAtDesitnation;
            lastRecordedTickWhileAtDesitnation = SessionData.Instance.gameTime;

            if(dontUpdateGoalPriorityForExtraTime > 0f)
            {
                dontUpdateGoalPriorityForExtraTime -= timeSinceLast;
                dontUpdateGoalPriorityForExtraTime = Mathf.Max(dontUpdateGoalPriorityForExtraTime, 0);
            }

            //Keep interactables active...
            if (preset.forcedActive.Count > 0 && interactable != null && interactable.furnitureParent != null)
            {
                foreach (Interactable i in interactable.furnitureParent.integratedInteractables)
                {
                    //This must be active...
                    if (preset.forcedActive.Contains(i.preset))
                    {
                        if (!i.sw0)
                        {
                            i.SetSwitchState(true, goal.aiController.human, true);
                        }
                    }
                }
            }

            //Chance of falling asleep
            if (preset.canFallAsleep)
            {
                //Remove this for now as it was causing difficulties

                //if(goal.aiController.human.energy <= 0.2f)
                //{
                //    if (SessionData.Instance.gameTime > arrivedAtDestination + (preset.fallAsleepAfterMinimum / 60f))
                //    {
                //        //Insert action
                //        if (!goal.actions.Exists(item => item.preset == RoutineControls.Instance.sleep && item.interactable == interactable))
                //        {
                //            NewAIAction newSleep = goal.aiController.CreateNewAction(goal, RoutineControls.Instance.sleep, true, newPassedInteractable: interactable);
                //        }

                //        goal.AITick(); //Force a tick of the goal to trigger sleep
                //        return;
                //    }
                //}
            }

            //Attack on proximity
            if (preset.attackPersuitTargetOnProximity && goal.aiController.persuitTarget != null && !goal.aiController.attackActive)
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(debug + " attempt attack...", Actor.HumanDebug.attacks);

                //Is close enough
                if (goal.aiController.seesOnPersuit && !goal.aiController.persuitTarget.isDead && !goal.aiController.persuitTarget.isStunned && goal.aiController.human.escalationLevel >= 2)
                {
                    float dist = Vector3.Distance(goal.aiController.persuitTarget.transform.position, goal.aiController.transform.position);

                    if (dist <= goal.aiController.weaponRangeMax && dist >= goal.aiController.currentWeaponPreset.minimumRange)
                    {
                        goal.aiController.StartAttack(goal.aiController.persuitTarget);
                    }
                    else
                    {
                        if (Game.Instance.collectDebugData)
                        {
                            if (dist < goal.aiController.weaponRangeMax) goal.aiController.human.SelectedDebug(debug + " too far to use weapon (" + dist + "/" + goal.aiController.weaponRangeMax + ")", Actor.HumanDebug.attacks);
                            else if (dist > goal.aiController.currentWeaponPreset.minimumRange) goal.aiController.human.SelectedDebug(debug + " to close too use weapon (" + dist + "/" + goal.aiController.currentWeaponPreset.minimumRange + ")", Actor.HumanDebug.attacks);
                        }

                        if (preset.throwObjectsAtTarget && !goal.preset.disableThrowing && goal.aiController.killerForMurders.Count <= 0)
                        {
                            if (dist <= CitizenControls.Instance.throwMaxRange && dist >= CitizenControls.Instance.throwMinRange)
                            {
                                if (goal.aiController.human.currentConsumables.Count > 0 || goal.aiController.human.trash.Count > 0)
                                {
                                    goal.aiController.ThrowObject(goal.aiController.persuitTarget);
                                }
                            }
                        }
                    }
                }
                else if (Game.Instance.collectDebugData)
                {
                    if (goal.aiController.persuitTarget.isDead) goal.aiController.human.SelectedDebug(debug + " Target is dead", Actor.HumanDebug.attacks);
                    if (goal.aiController.persuitTarget.isStunned) goal.aiController.human.SelectedDebug(debug + " Target is stunned", Actor.HumanDebug.attacks);
                    if (!goal.aiController.seesOnPersuit) goal.aiController.human.SelectedDebug(debug + " SeesOnPersuit is not true", Actor.HumanDebug.attacks);
                    if (goal.aiController.human.escalationLevel < 2) goal.aiController.human.SelectedDebug(debug + " Escalation level is too low: " + goal.aiController.human.escalationLevel, Actor.HumanDebug.attacks);
                }
            }

            //Cancel if player isn't here
            if (preset.cancelIfPersuitTargetNotInRange)
            {
                if(!goal.aiController.throwActive)
                {
                    if (!IsPersuitTargetCatchable())
                    {
                        Remove(preset.repeatDelayOnActionFail);
                        return;
                    }
                }
            }

            //Add modifiers over time
            goal.aiController.human.AddNourishment(timeSinceLast * preset.overtimeNourishment);
            goal.aiController.human.AddHydration(timeSinceLast * preset.overtimeHydration);
            goal.aiController.human.AddAlertness(timeSinceLast * preset.overtimeAlertness);
            goal.aiController.human.AddEnergy(timeSinceLast * preset.overtimeEnergy);
            goal.aiController.human.AddExcitement(timeSinceLast * preset.overtimeExcitement);
            goal.aiController.human.AddChores(timeSinceLast * preset.overtimeChores);
            goal.aiController.human.AddHygiene(timeSinceLast * preset.overtimeHygiene);
            goal.aiController.human.AddBladder(timeSinceLast * preset.overtimeBladder);
            goal.aiController.human.AddHeat(timeSinceLast * preset.overtimeHeat);
            goal.aiController.human.AddDrunk(timeSinceLast * preset.overtimeDrunk);
            goal.aiController.human.AddBreath(timeSinceLast * preset.overtimeBreath);
            goal.aiController.human.AddPoisoned(timeSinceLast * preset.overtimePoison, null);

            //Advance progress
            if (preset.completableAction)
            {
                //Immediately complete actions that take 0 time
                float addProgress = 1f;

                if (timeThisWillTake > 0)
                {
                    addProgress = timeSinceLast / timeThisWillTake;
                }

                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Add progress " + addProgress + " to action " + preset.name + "(" + progress + ") time since last update: " + timeSinceLast + " and time this will take: " + timeThisWillTake, Actor.HumanDebug.actions);

                progress += addProgress;

                if(preset.completeOnSeeIllegal && goal.aiController.persuitTarget != null)
                {
                    if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Persuit target " + goal.aiController.persuit + " active...", Actor.HumanDebug.attacks);

                    if (goal.aiController.human.seesIllegal.ContainsKey(goal.aiController.persuitTarget))
                    {
                        if(goal.aiController.human.seesIllegal[goal.aiController.persuitTarget] >= 1f)
                        {
                            progress = 1f;
                        }
                        else
                        {
                            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Persuit target sight progress: " + goal.aiController.human.seesIllegal[goal.aiController.persuitTarget], Actor.HumanDebug.sight);
                        }
                    }
                }

                progress = Mathf.Clamp01(progress);

                //Apply modifiers via progress
                if(preset.useCurrentConsumable && goal.aiController.human.currentConsumables.Count > 0 && goal.aiController.human.currentConsumables[0].retailItem != null)
                {
                    foreach(InteractablePreset c in goal.aiController.human.currentConsumables)
                    {
                        if(c.retailItem != null)
                        {
                            goal.aiController.human.AddNourishment(addProgress * c.retailItem.nourishment);
                            goal.aiController.human.AddHydration(addProgress * c.retailItem.hydration);
                            goal.aiController.human.AddAlertness(addProgress * c.retailItem.alertness);
                            goal.aiController.human.AddEnergy(addProgress * c.retailItem.energy);
                            goal.aiController.human.AddExcitement(addProgress * c.retailItem.excitement);
                            goal.aiController.human.AddChores(addProgress * c.retailItem.chores);
                            goal.aiController.human.AddHygiene(addProgress * c.retailItem.hygiene);
                            goal.aiController.human.AddBladder(addProgress * c.retailItem.bladder);
                            goal.aiController.human.AddHeat(addProgress * c.retailItem.heat);
                            goal.aiController.human.AddDrunk(addProgress * c.retailItem.drunk);
                            goal.aiController.human.AddBreath(addProgress * c.retailItem.breath);
                            goal.aiController.human.AddPoisoned(addProgress * c.retailItem.poisoned, null);
                            goal.aiController.human.AddHealth(addProgress * c.retailItem.health);
                        }
                    }
                }
                else
                {
                    goal.aiController.human.AddNourishment(addProgress * preset.progressNourishment);
                    goal.aiController.human.AddHydration(addProgress * preset.progressHydration);
                    goal.aiController.human.AddAlertness(addProgress * preset.progressAlertness);
                    goal.aiController.human.AddEnergy(addProgress * preset.progressEnergy);
                    goal.aiController.human.AddExcitement(addProgress * preset.progressExcitement);
                    goal.aiController.human.AddChores(addProgress * preset.progressChores);
                    goal.aiController.human.AddHygiene(addProgress * preset.progressHygeiene);
                    goal.aiController.human.AddBladder(addProgress * preset.progressBladder);
                    goal.aiController.human.AddHeat(addProgress * preset.progressHeat);
                    goal.aiController.human.AddDrunk(addProgress * preset.progressDrunk);
                    goal.aiController.human.AddBreath(addProgress * preset.progressBreath);
                    goal.aiController.human.AddPoisoned(addProgress * preset.progressPoisoned, null);
                }
            }

            //While arrived speech
            if (preset.whileArrivedBark.Count > 0 && SessionData.Instance.gameTime - goal.aiController.human.speechController.lastSpeech > 0.03f && Toolbox.Instance.Rand(0f, 1f) <= preset.chanceOfWhileArrived)
            {
                if(!preset.mustSeeOtherCitizen || (goal.aiController.currentTrackTarget != null && goal.aiController.currentTrackTarget.actor != null && goal.aiController.currentTrackTarget.active))
                {
                    if (InterfaceController.Instance.activeSpeechBubbles.Count <= CitizenControls.Instance.maxSpeechBubbles)
                    {
                        goal.aiController.human.speechController.TriggerBark(preset.whileArrivedBark[Toolbox.Instance.Rand(0, preset.whileArrivedBark.Count)]);
                    }
                }
            }

            //Progress Vmail Threads
            if(preset.progressVmailThreads)
            {
                if(timeSinceLast > 0.1f)
                {
                    bool found = false;

                    //Search active threads
                    foreach(StateSaveData.MessageThreadSave thread in goal.aiController.human.messageThreadFeatures)
                    {
                        //Can I reply next?
                        DDSSaveClasses.DDSTreeSave tree = null;

                        if (!Toolbox.Instance.allDDSTrees.TryGetValue(thread.treeID, out tree))
                        {
                            Game.LogError("Cannot find vmail tree " + thread.treeID);
                            continue;
                        }

                        //Run through timestamps and get the index of the last message sent
                        //Run through tree and record messages
                        string instanceID = tree.startingMessage;
                        float timeStamp = -999999f;

                        for (int i = 0; i < thread.timestamps.Count; i++)
                        {
                            if (thread.timestamps[i] > timeStamp)
                            {
                                timeStamp = thread.timestamps[i];
                                instanceID = thread.messages[i];
                            }
                        }

                        DDSSaveClasses.DDSMessageSettings msg = null;

                        if (!tree.messageRef.TryGetValue(instanceID, out msg))
                        {
                            Game.LogError("Cannot find message instance ID " + instanceID);
                            continue;
                        }

                        Human from = null;
                        CityData.Instance.GetHuman(thread.participantA, out from);

                        //Find the next message...
                        List<Human.DDSRank> immediateLinks = Toolbox.Instance.GetMessageTreeLinkRankings(thread, msg);

                        //Are there any messages sent by me?
                        foreach (Human.DDSRank opt in immediateLinks)
                        {
                            if (tree.messageRef.TryGetValue(opt.linkRef.to, out msg))
                            {
                                //Set currently talking
                                bool match = false;

                                if (msg.saidBy <= 0 && goal.aiController.human.humanID == thread.participantA) match = true;
                                else if (msg.saidBy == 1 && goal.aiController.human.humanID == thread.participantB) match = true;
                                else if (msg.saidBy == 2 && goal.aiController.human.humanID == thread.participantC) match = true;
                                else if (msg.saidBy == 3 && goal.aiController.human.humanID == thread.participantD) match = true;

                                if(match)
                                {
                                    //Pick a time interval: this cannot be in the future! The most recent it can be is about 4 hours ago...
                                    //Pick a normalized point along this remaining range...
                                    timeStamp = Mathf.Lerp(timeStamp, Mathf.Max(SessionData.Instance.gameTime - 4, timeStamp), Toolbox.Instance.Rand(0f, 0.5f));

                                    thread.messages.Add(opt.linkRef.to);
                                    thread.timestamps.Add(timeStamp);

                                    //Set currently talking
                                    if (msg.saidBy <= 0) thread.senders.Add(from.humanID);
                                    else if (msg.saidBy == 1) thread.senders.Add(thread.participantB);
                                    else if (msg.saidBy == 2) thread.senders.Add(thread.participantC);
                                    else if (msg.saidBy == 3) thread.senders.Add(thread.participantD);

                                    if (msg.saidTo <= 0) thread.recievers.Add(from.humanID);
                                    else if (msg.saidTo == 1) thread.recievers.Add(thread.participantB);
                                    else if (msg.saidTo == 2) thread.recievers.Add(thread.participantC);
                                    else if (msg.saidTo == 3) thread.recievers.Add(thread.participantD);

                                    Game.Log("Progressed vmail thread!");

                                    found = true;
                                    break;
                                }
                            }
                        }

                        //Reply to one email at a time
                        if(found)
                        {
                            break;
                        }
                    }
                }
            }

            //Trigger completion
            if(progress >= 1)
            {
                Complete();
            }
        }
        else
        {
            //Do a check
            DestinationCheck("Tick check");

            //Set no idle animation state
            //goal.aiController.human.animationController.SetIdleAnimationState(CitizenAnimationController.IdleAnimationState.none);

            if (goal.aiController.human.isInBed)
            {
                goal.aiController.human.SetInBed(false);
            }

            //While journey speech
            if (preset.whileJourneyBark.Count > 0 && SessionData.Instance.gameTime - goal.aiController.human.speechController.lastSpeech > 0.03f && Toolbox.Instance.Rand(0f, 1f) <= preset.chanceOfWhileJourney)
            {
                if (InterfaceController.Instance.activeSpeechBubbles.Count <= CitizenControls.Instance.maxSpeechBubbles)
                {
                    goal.aiController.human.speechController.TriggerBark(preset.whileJourneyBark[Toolbox.Instance.Rand(0, preset.whileJourneyBark.Count)]);
                }
            }
        }
    }

    public bool InteractableStateCheck(AIActionPreset.CheckActionAgainstState stateCheck)
    {
        if (interactable == null) return false;

        bool ret = false;

        if (stateCheck.switchState == InteractablePreset.Switch.switchState && interactable.sw0 == stateCheck.switchIs)
        {
            ret = true;
        }
        else if (stateCheck.switchState == InteractablePreset.Switch.custom1 && interactable.sw1 == stateCheck.switchIs)
        {
            ret = true;
        }
        else if (stateCheck.switchState == InteractablePreset.Switch.custom2 && interactable.sw2 == stateCheck.switchIs)
        {
            ret = true;
        }
        else if (stateCheck.switchState == InteractablePreset.Switch.custom3 && interactable.sw3 == stateCheck.switchIs)
        {
            ret = true;
        }
        else if (stateCheck.switchState == InteractablePreset.Switch.lockState && interactable.locked == stateCheck.switchIs)
        {
            ret = true;
        }
        else if (stateCheck.switchState == InteractablePreset.Switch.carryPhysicsObject && interactable.phy == stateCheck.switchIs)
        {
            ret = true;
        }
        else if (stateCheck.switchState == InteractablePreset.Switch.enforcersInside && goal.aiController.human.currentGameLocation != null && goal.aiController.human.isHome && goal.aiController.human.currentGameLocation.currentOccupants.Exists(item => item != goal.aiController.human && item.isEnforcer && item.isOnDuty))
        {
            ret = true;
        }

        if (ret)
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(interactable.name + "'s state " + stateCheck.switchState.ToString() +" matches the actions: " + stateCheck.switchIs + " outcome: " + stateCheck.outcome, Actor.HumanDebug.attacks);
        }

        return ret;
    }

    //Stop movement if this actor can see this other person...
    public void LOSCheck()
    {
        if (passedInteractable != null && passedInteractable.controller != null)
        {
            bool los = false;

            if(passedInteractable.isActor != null && goal.aiController.human.currentNode == passedInteractable.isActor.currentNode)
            {
                los = true;
            }
            else if (goal.aiController.human.currentNode == passedInteractable.node)
            {
                los = true;
            }
            else
            {
                los = Toolbox.Instance.RaycastCheck(goal.aiController.human.lookAtThisTransform, passedInteractable.controller.transform, 10f, out _);
            }

            //Game.Log("Action LOS check: " + los);

            if(los)
            {
                node = goal.aiController.human.currentNode;
                goal.aiController.DebugDestinationPosition("Set at destination due to LOS");
                SetAtDestination(true);
            }
        }
    }

    //Check if player is catchable
    private bool IsPersuitTargetCatchable()
    {
        //The player must currently be seen
        if (goal.aiController.persuitTarget == null)
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("...Cancelling as target isn't here (no persuit target)", Actor.HumanDebug.attacks);
            return false;
        }

        //If KO'd or dead...
        if (goal.aiController.persuitTarget.isDead || goal.aiController.persuitTarget.isStunned)
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("...Cancelling as target is dead or stunned", Actor.HumanDebug.attacks);
            return false;
        }

        if (!goal.aiController.human.seenIllegalThisCheck.Contains(goal.aiController.persuitTarget) && goal.aiController.persuitChaseLogicUses <= 0)
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("...Cancelling as target isn't here (unseen this check)", Actor.HumanDebug.attacks);
            return false;
        }

        //Player must be within this distance
        float dist = Vector3.Distance(goal.aiController.persuitTarget.transform.position, goal.aiController.transform.position);

        if (dist > goal.aiController.weaponRangeMax || dist < goal.aiController.currentWeaponPreset.minimumRange)
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("...Cancelling as target isn't here (too near/far away: " + dist +")", Actor.HumanDebug.attacks);
            return false;
        }

        //Is the player hiding but seen? If so then insert a pull out action
        if (goal.aiController.persuitTarget.isHiding && goal.aiController.persuitTarget.isPlayer)
        {
            if(Player.Instance.spottedWhileHiding.Contains(goal.aiController.human as Actor))
            {
                goal.InsertPlayerHidingPlaceRemoval();
                return false;
            }
        }

        return true;
    }

    public void SetAtDestination(bool val, bool forceUpdate = false)
    {
        if(isAtLocation != val || forceUpdate)
        {
            isAtLocation = val;

            if (Game.Instance.collectDebugData)
            {
                goal.aiController.human.SelectedDebug("Set at destination: " + isAtLocation, Actor.HumanDebug.actions);
                goal.aiController.DebugDestinationPosition("Set at destination: " + isAtLocation);
            }

            if (preset.limitTickRate) goal.aiController.UpdateTickRate(); //If this affects tick rate, update it

            //Enable or disable ai controller for movement
            if (!isAtLocation)
            {
                goal.aiController.SetUpdateEnabled(true); //Allow movement
            }
            //Is at location
            else
            {
                TriggerArrivalSound();

                //Change outfits
                if(preset.specificOutfitOnArrive)
                {
                    goal.aiController.human.outfitController.SetCurrentOutfit(preset.allowedOutfitOnArrive);
                }
                else if (preset.makeClothedOnActivate)
                {
                    goal.aiController.human.outfitController.MakeClothed();
                }

                //Set expression
                if (preset.setExpressionOnArrive)
                {
                    goal.aiController.SetExpression(preset.arriveExpression);
                }

                //Set anmation
                if (preset.changeIdleOnArrival && !goal.aiController.restrained)
                {
                    if (interactable != null && interactable.preset.specialCaseFlag == InteractablePreset.SpecialCase.forceStanding)
                    {
                        goal.aiController.human.animationController.SetIdleAnimationState(CitizenAnimationController.IdleAnimationState.none);
                    }
                    else goal.aiController.human.animationController.SetIdleAnimationState(preset.idleAnimationOnArrival);
                }

                if (preset.changeArmsOnArrival && !goal.aiController.restrained)
                {
                    goal.aiController.human.animationController.SetArmsBoolState(preset.armsAnimationOnArrival);
                }

                //Spawn items
                goal.aiController.UpdateHeldItems(AIActionPreset.ActionStateFlag.onArrival);

                //Update node space pos
                goal.aiController.human.UpdateCurrentNodeSpace();

                //Set don't update goal priority for this time
                dontUpdateGoalPriorityForExtraTime = preset.dontUpdateGoalPriorityFor / 60f;

                //Record time of arrival
                lastRecordedTickWhileAtDesitnation = SessionData.Instance.gameTime;
                arrivedAtDestination = SessionData.Instance.gameTime;

                //Lying
                if (preset.lying)
                {
                    goal.aiController.human.SetInBed(true);
                }

                //Add time record
                if (estimatedArrival > -1)
                {
                    //+ is early, - is late
                    Toolbox.Instance.AddToTravelTimeRecords(goal.aiController.human, estimatedArrival - arrivedAtDestination);
                }

                //Attack on proximity
                if (preset.attackPersuitTargetOnProximity && goal.aiController.persuitTarget != null && !goal.aiController.attackActive)
                {
                    if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(debug + " attempt attack at destination...", Actor.HumanDebug.attacks);

                    //Is close enough
                    if (goal.aiController.seesOnPersuit && !goal.aiController.persuitTarget.isDead && !goal.aiController.persuitTarget.isStunned && goal.aiController.human.escalationLevel >= 2)
                    {
                        float dist = Vector3.Distance(goal.aiController.persuitTarget.transform.position, goal.aiController.transform.position);

                        if (dist <= goal.aiController.weaponRangeMax && dist >= goal.aiController.currentWeaponPreset.minimumRange)
                        {
                            goal.aiController.StartAttack(goal.aiController.persuitTarget);
                        }
                        else
                        {
                            if (Game.Instance.collectDebugData)
                            {
                                if (dist > goal.aiController.weaponRangeMax) goal.aiController.human.SelectedDebug(debug + " too far to use weapon (" + dist + "/" + goal.aiController.weaponRangeMax + ")", Actor.HumanDebug.attacks);
                                else if (dist < goal.aiController.currentWeaponPreset.minimumRange) goal.aiController.human.SelectedDebug(debug + " too close to use weapon (" + dist + "/" + goal.aiController.currentWeaponPreset.minimumRange + ")", Actor.HumanDebug.attacks);
                            }

                            if (preset.throwObjectsAtTarget && !goal.preset.disableThrowing && goal.aiController.killerForMurders.Count <= 0)
                            {
                                if (dist <= CitizenControls.Instance.throwMaxRange && dist >= CitizenControls.Instance.throwMinRange)
                                {
                                    if (goal.aiController.human.currentConsumables.Count > 0 || goal.aiController.human.trash.Count > 0)
                                    {
                                        goal.aiController.ThrowObject(goal.aiController.persuitTarget);
                                    }
                                }
                            }
                        }
                    }
                    else if (Game.Instance.collectDebugData)
                    {
                        if (goal.aiController.persuitTarget.isDead) goal.aiController.human.SelectedDebug(debug + " Target is dead", Actor.HumanDebug.attacks);
                        if (goal.aiController.persuitTarget.isStunned) goal.aiController.human.SelectedDebug(debug + " Target is stunned", Actor.HumanDebug.attacks);
                        if (!goal.aiController.seesOnPersuit) goal.aiController.human.SelectedDebug(debug + " SeesOnPersuit is not true", Actor.HumanDebug.attacks);
                        if (goal.aiController.human.escalationLevel < 2) goal.aiController.human.SelectedDebug(debug + " Escalation level is too low: " + goal.aiController.human.escalationLevel, Actor.HumanDebug.attacks);
                    }
                }

                //Cancel if player isn't here
                if (preset.cancelIfPersuitTargetNotInRange)
                {
                    if(!goal.aiController.throwActive)
                    {
                        if (!IsPersuitTargetCatchable())
                        {
                            Remove(preset.repeatDelayOnActionFail);
                            return;
                        }
                    }
                }

                //Sleep if needed
                if (preset.sleepOnArrival)
                {
                    goal.aiController.human.GoToSleep();
                }

                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Attempting to set arrival direction: " + preset.facing, Actor.HumanDebug.movement);

                //Set facing
                if (preset.facing == AIActionPreset.ActionFacingDirection.towardsDestination)
                {
                    if(goal.aiController.currentDestinationNode != null)
                    {
                        goal.aiController.SetFaceTravelDirection();
                    }
                }
                else if(preset.facing == AIActionPreset.ActionFacingDirection.awayFromDestination)
                {
                    if (goal.aiController.currentDestinationNode != null)
                    {
                        goal.aiController.SetFacingPosition(-goal.aiController.currentDestinationPositon);
                    }
                }
                else if(preset.facing == AIActionPreset.ActionFacingDirection.interactable)
                {
                    if(interactable != null)
                    {
                        goal.aiController.SetFacingPosition(interactable.usagePoint.GetUsageWorldPosition(node.position, goal.aiController.human));
                    }
                }
                else if(preset.facing == AIActionPreset.ActionFacingDirection.InverseInteractable)
                {
                    if (interactable != null)
                    {
                        goal.aiController.SetFacingPosition(-interactable.usagePoint.GetUsageWorldPosition(node.position, goal.aiController.human));
                    }
                }
                else if (preset.facing == AIActionPreset.ActionFacingDirection.player)
                {
                    goal.aiController.SetFacingPosition(Player.Instance.transform.position);
                }
                else if (preset.facing == AIActionPreset.ActionFacingDirection.door)
                {
                    foreach (KeyValuePair<NewNode, NewNode.NodeAccess> pair in node.accessToOtherNodes)
                    {
                        if (pair.Value.accessType == NewNode.NodeAccess.AccessType.door)
                        {
                            if (pair.Value.door != null)
                            {
                                //Is this door the same as the passed interactable (door or handle?)
                                if(pair.Value.door.doorInteractable == passedInteractable || pair.Value.door.handleInteractable == passedInteractable)
                                {
                                    //Look @ the other node
                                    NewNode otherDoorNode = pair.Key;
                                    goal.aiController.SetFacingPosition(otherDoorNode.position);
                                    //Game.Log("AI: Found look@ door");
                                    break;
                                }
                            }
                        }
                    }
                }
                else if(preset.facing == AIActionPreset.ActionFacingDirection.accessableDirection)
                {
                    List<NewNode> accessable = new List<NewNode>();

                    if(node != null)
                    {
                        foreach(KeyValuePair<NewNode, NewNode.NodeAccess> pair in node.accessToOtherNodes)
                        {
                            if (!pair.Value.walkingAccess) continue; //Skip non-walkable routes
                            if (pair.Value.toNode.noPassThrough) continue;

                            if (pair.Value.accessType == NewNode.NodeAccess.AccessType.door)
                            {
                                if (pair.Value.door != null)
                                {
                                    if (pair.Value.door.isClosed) continue; //Skip closed doors
                                }
                                else continue;
                            }

                            accessable.Add(pair.Key);
                        }
                    }

                    //Pick at random
                    if(accessable.Count > 0) goal.aiController.SetFacingPosition(accessable[Toolbox.Instance.Rand(0, accessable.Count)].position);
                }
                else if(preset.facing == AIActionPreset.ActionFacingDirection.investigate)
                {
                    //Can I see player? If so face them
                    if (goal.aiController.seesOnPersuit && goal.aiController.persuitTarget != null)
                    {
                        goal.aiController.SetFacingPosition(goal.aiController.persuitTarget.transform.position);
                        
                        //Game.Log("Set facing to player position");
                    }
                    else
                    {
                        goal.aiController.SetFacingPosition(goal.aiController.investigatePositionProjection);
                        //Game.Log("Set facing to investigate postion");
                    }
                }
                else if(preset.facing == AIActionPreset.ActionFacingDirection.interactableSetting)
                {
                    if(usagePoint != null)
                    {
                        //Vector3 offsetCoord = node.nodeCoord + new Vector3(Mathf.Round(usagePoint.rotatedFacing.x), Mathf.Round(usagePoint.rotatedFacing.y), 0);
                        //Vector3 worldPos = CityData.Instance.NodeToRealpos(offsetCoord);
                        goal.aiController.SetFacingPosition(usagePoint.worldLookAtPoint);
                    }
                }
                else if (preset.facing == AIActionPreset.ActionFacingDirection.inverseInteractableSetting)
                {
                    if (usagePoint != null)
                    {
                        //Vector3 offsetCoord = node.nodeCoord + new Vector3(Mathf.Round(usagePoint.rotatedFacing.x), Mathf.Round(usagePoint.rotatedFacing.y), 0);
                        //Vector3 worldPos = CityData.Instance.NodeToRealpos(offsetCoord);
                        goal.aiController.SetFacingPosition(-usagePoint.worldLookAtPoint);
                    }
                }

                //Trigger speech
                if (preset.onArrivalBark.Count > 0 && Toolbox.Instance.Rand(0f, 1f) <= preset.chanceOfOnArrival)
                {
                    if (InterfaceController.Instance.activeSpeechBubbles.Count <= CitizenControls.Instance.maxSpeechBubbles)
                    {
                        goal.aiController.human.speechController.TriggerBark(preset.onArrivalBark[Toolbox.Instance.Rand(0, preset.onArrivalBark.Count)]);
                    }
                }

                //Set using interactable
                if (usagePoint != null)
                {
                    if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Setting usage point to me " + goal.aiController.human.name + "...", Actor.HumanDebug.actions);
                    usagePoint.TrySetUser(preset.usageSlot, goal.aiController.human, "Set on destination: " + preset.name);
                }

                if (interactable != null)
                {
                    ExecuteAdditionalActions(ref preset.forcedActionsOnArrival);
                }

                //Special case: Put down object on subspawn
                if(passedInteractable != null && preset.actionLocation == AIActionPreset.ActionLocation.putDownInteractable && bestPlacement != null)
                {
                    if(bestPlacement != null)
                    {
                        //Game.Log("Putting " + passedInteractable.name + " down at " + bestPlacement.furnParent.furniture.name + " at " + node.position + " sub obj " + bestPlacement.furnParent.furniture.subObjects.IndexOf(bestPlacement.location));

                        passedInteractable.SetAsNotInventory(node);

                        bool relocationAuthority = Toolbox.Instance.GetRelocateAuthority(goal.aiController.human, passedInteractable);

                        passedInteractable.ConvertToFurnitureSpawnedObject(bestPlacement.furnParent, bestPlacement.location, true, relocationAuthority);

                        //Vector3 localPos = bestPlacement.furnParent.GetSubObjectLocalPosition(bestPlacement.location);
                        //Vector3 localEuler = bestPlacement.furnParent.GetSubObjectLocalEuler(bestPlacement.location);

                        ////Use a matrix to get the world position
                        //Matrix4x4 m = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(new Vector3(0, bestPlacement.furnParent.angle + bestPlacement.furnParent.diagonalAngle, 0)), Vector3.one);

                        //Vector3 wPos = m.MultiplyPoint3x4(localPos) + bestPlacement.furnParent.anchorNode.position;
                        //Vector3 wEuler = new Vector3(0, bestPlacement.furnParent.angle + bestPlacement.furnParent.diagonalAngle, 0) + localEuler;

                        ////Game.Log(goal.aiController.human.name + " Putting down " + passedInteractable.name + " on " + bestPlacement.furnParent.furniture.name + " at " + wPos + " (localpos: " + localPos + ")");

                        //passedInteractable.MoveInteractable(wPos, wEuler, true);

                        //Add to put down list
                        if(!goal.aiController.putDownItems.Contains(passedInteractable))
                        {
                            goal.aiController.putDownItems.Add(passedInteractable);
                        }

                        //Game.Log("Set " + passedInteractable.name + " as not in inventory, at node in " + node.room.name + " room parent: " + passedInteractable.worldObjectRoomParent + " found on node: " + node.interactables.Exists(item => item == passedInteractable) + " rem: " + passedInteractable.rem);
                    }
                    else
                    {
                        Game.Log("AI Error: No best placement for putting down " + passedInteractable.name);
                    }
                }
                //Special case: pick up an object
                if (passedInteractable != null && preset.actionLocation == AIActionPreset.ActionLocation.pickUpInteractable)
                {
                    //Add fingerprints to furniture
                    if(passedInteractable.furnitureParent != null)
                    {
                        foreach(Interactable i in passedInteractable.furnitureParent.integratedInteractables)
                        {
                            i.AddNewDynamicFingerprint(goal.aiController.human, Interactable.PrintLife.timed);
                            i.AddNewDynamicFingerprint(goal.aiController.human, Interactable.PrintLife.timed);
                            i.AddNewDynamicFingerprint(goal.aiController.human, Interactable.PrintLife.timed);
                        }
                    }

                    passedInteractable.SetInInventory(goal.aiController.human);
                    goal.aiController.putDownItems.Remove(passedInteractable);

                    //Game.Log("Set " + passedInteractable.name + " as in inventory, at node in " + node.room.name + " room parent: " + passedInteractable.worldObjectRoomParent + " found on node: " + node.interactables.Exists(item => item == passedInteractable));
                }
            }
        }
    }

    //Triggered if the node is unable to fit this character on (attempt number 0 - 2)
    public void OnInvalidMovement(int attemptNumber)
    {
        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Action: OnInvalidMovement", Actor.HumanDebug.movement);
    }

    //Immediately complete this action
    public void ImmediateComplete()
    {
        if(preset.completableAction)
        {
            Complete();
        }
    }

    public Interactable InteractablePicker(ref List<Interactable> opt, Vector3 currentWorldPosition, bool useSocialRules, out NewNode useNode, out Interactable.UsagePoint usePoint, GroupsController.SocialGroup meetingGroup = null, bool useDistance = false, bool useDistanceIfInSameAddress = true, List<Interactable> ignore = null)
    {
        useNode = null;
        usePoint = null;

        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Picking interactable from pool of " + opt.Count +"...", Actor.HumanDebug.actions);

        Interactable.UsagePoint bestValid = null;
        float bestRank = -99999f;

        foreach (Interactable inter in opt)
        {
            //Ignore if moved by player
            if (!inter.originalPosition && inter.wo)
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(inter.name + " not at original postion...", Actor.HumanDebug.actions);
                continue;
            }

            //Ignore list
            if (ignore != null)
            {
                if (ignore.Contains(inter))
                {
                    if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(inter.name + " on ignore list...", Actor.HumanDebug.actions);
                    continue;
                }
            }

            //Interactable must have a valid location
            if (inter.node == null)
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(inter.name + " no valid node location...", Actor.HumanDebug.actions);
                continue;
            }

            if(inter.node.accessToOtherNodes.Count <= 0)
            {
                Game.LogError("Interactable " + inter.name + " at " + inter.wPos + " has no access...");
                continue;
            }

            //Must not be being used
            Human existing = null;
            inter.usagePoint.TryGetUserAtSlot(preset.usageSlot, out existing);

            if (existing != null && existing != goal.aiController.human)
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(inter.name + " being used already by " + existing.name, Actor.HumanDebug.actions);
                continue;
            }

            //Must not be reserved...
            if(inter.usagePoint.reserved != null)
            {
                //Should the meeting be over yet?
                if(SessionData.Instance.decimalClock >= inter.usagePoint.reserved.decimalStartTime + Toolbox.Instance.groupsDictionary[inter.usagePoint.reserved.preset].meetUpLength)
                {
                    inter.usagePoint.SetReserved(null);
                }
                else if(!goal.aiController.human.groups.Contains(inter.usagePoint.reserved) && meetingGroup != inter.usagePoint.reserved)
                {
                    if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(inter.name + " reserved by " + inter.usagePoint.reserved, Actor.HumanDebug.actions);
                    continue;
                }
            }

            //Is in illegal room
            string trespassDebug = string.Empty;

            if (!goal.preset.allowTrespass && goal.aiController.human.IsTrespassing(inter.node.room, out _, out trespassDebug, goal.preset.allowEnforcersEverywhere))
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(inter.name + " no valid authority: " + trespassDebug, Actor.HumanDebug.actions);
                continue;
            }

            //Gather info from everybody using an interactable on this node...
            List<Human> localUsers = new List<Human>();

            int localSeats = 0;
            int localFreeSeats = 0;

            foreach (Interactable i in inter.node.interactables)
            {
                //This is in the list...
                if (!i.preset.disableForSocialGroups && opt.Contains(i))
                {
                    Human existing2 = null;

                    if (i.usagePoint.TryGetUserAtSlot(preset.usageSlot, out existing2))
                    {
                        localUsers.Add(existing2);
                    }
                    else
                    {
                        localFreeSeats++; //This is a seat on the same node...
                    }

                    localSeats++;
                }
            }

            //Asign interactable priority ranking
            float aiPriority = inter.preset.AIPriority;
            float distMP = inter.preset.pickDistanceMultiplier;

            if (inter.preset.perActionPrioritySettings.Count > 0)
            {
                InteractablePreset.AIUsePriority spec = inter.preset.perActionPrioritySettings.Find(item => item.actions.Contains(preset));

                if (spec != null)
                {
                    aiPriority = spec.AIPriority;
                    distMP = spec.pickDistanceMultiplier;
                }
            }

            float cRank = aiPriority + Toolbox.Instance.Rand(0f, 1f); //Add a little random...

            //Use social rules...
            if (useSocialRules)
            {
                //Is the person I'm meeting here?
                if(meetingGroup != null)
                {
                    GroupPreset grp = Toolbox.Instance.groupsDictionary[meetingGroup.preset];
                    distMP += grp.useDistanceMultiplierModifier;

                    if (inter.usagePoint.reserved == meetingGroup)
                    {
                        cRank += 200f;
                        if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Found reservation for group at " + inter.name + " id " + inter.id, Actor.HumanDebug.actions);
                    }

                    int matchingFriends = 0;

                    foreach(int h in meetingGroup.members)
                    {
                        Human c = null;

                        if(CityData.Instance.GetHuman(h, out c))
                        {
                            //A person I'm meeting is sitting here but not at this exact position...
                            if(localUsers.Contains(c) && c.ai != null && c.ai.currentAction != null && c.ai.currentAction.interactable != inter)
                            {
                                cRank += 200f;
                                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Found matching group memeber " + c.name + " using " + c.ai.currentAction.interactable.name + " id " + c.ai.currentAction.interactable.id +" local to " + inter.name + " id " + inter.id, Actor.HumanDebug.actions);
                                matchingFriends++;
                            }
                        }
                    }

                    //Nobody I know is sitting by here...
                    if(matchingFriends <= 0 && !inter.preset.disableForSocialGroups)
                    {
                        //There's nobody else sitting here either, so is a good place to sit and wait...
                        if(localUsers.Count <= 0)
                        {
                            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(inter.name + " " + inter.id + " has no other users...", Actor.HumanDebug.actions);
                            cRank += Mathf.Min(localSeats, meetingGroup.members.Count) * 5f;
                        }

                        //There's someone here but there are enough seats...
                        if(localFreeSeats >= meetingGroup.members.Count)
                        {
                            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug(inter.name + " " + inter.id + " has enough free seats for my group of " + meetingGroup.members.Count, Actor.HumanDebug.actions);
                            cRank += +Mathf.Min(localFreeSeats, meetingGroup.members.Count) * 5f;
                        }
                    }
                }
                //I haven't got a group, sit by friends though...
                else
                {
                    foreach(Human h in localUsers)
                    {
                        Acquaintance aq = null;

                        if(goal.aiController.human.FindAcquaintanceExists(h, out aq))
                        {
                            cRank += ((aq.known - 0.2f) + (aq.like - 0.2f)) * 10f;
                        }

                        cRank += (goal.aiController.human.extraversion - 0.75f) * 5f;
                    }
                }
            }

            //Add distance. -1 per 5m
            //Use distance if this is executed within the same room as the interactable
            if (useDistance || (useDistanceIfInSameAddress && goal.aiController.human.currentGameLocation == inter.node.gameLocation))
            {
                cRank -= (Vector3.Distance(inter.wPos, currentWorldPosition) * 0.2f) * distMP;
            }

            if(bestValid == null || cRank > bestRank)
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("BEST YET: Evaluate interactable " + inter.name + " " + inter.id + " on " + inter.node.nodeCoord + " in room " + inter.node.room.name + ": " + cRank + "...", Actor.HumanDebug.actions);
                bestValid = inter.usagePoint;
                bestRank = cRank;
            }
            else
            {
                if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Evaluate interactable " + inter.name + " " + inter.id + " on " + inter.node.nodeCoord + " in room " + inter.node.room.name + ": " + cRank + "...", Actor.HumanDebug.actions);
            }
        }

        if(bestValid != null)
        {
            if (bestValid.interactable.node.accessToOtherNodes.Count <= 0)
            {
                Game.LogError("Picked a node with no access. Something is wrong here...");
            }

            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("Chosen interactable " + bestValid.interactable.name + " id " + bestValid.interactable.id, Actor.HumanDebug.actions);

            usePoint = bestValid;
            useNode = usePoint.node;

            //Reserve seats...
            if (useSocialRules && meetingGroup != null)
            {
                GroupPreset grp = Toolbox.Instance.groupsDictionary[meetingGroup.preset];

                if(grp.reserveSeats)
                {
                    foreach(Interactable prev in meetingGroup.reserved)
                    {
                        prev.usagePoint.reserved = null;
                    }

                    foreach (Interactable i in usePoint.node.interactables)
                    {
                        //This is in the list...
                        if (opt.Contains(i))
                        {
                            i.usagePoint.SetReserved(meetingGroup); //Set reserved
                            if (meetingGroup.reserved == null) meetingGroup.reserved = new List<Interactable>();
                            meetingGroup.reserved.Add(i);
                        }
                    }
                }
            }

            return usePoint.interactable;
        }
        else
        {
            if (Game.Instance.collectDebugData) goal.aiController.human.SelectedDebug("No valid options for interactable", Actor.HumanDebug.actions);
            return null;
        }
    }
}
