﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class WindowTabController : MonoBehaviour, IPointerClickHandler
{
    public RectTransform rect;
    public Button tabButton;
    public WindowContentController content;
    public WindowTabPreset preset;
    public TextMeshProUGUI text;

    //Count controller
    public int newItems = 0;
    public PulsateController pulsateController;

    // Use this for initialization
    void Awake()
    {
        rect = this.gameObject.GetComponent<RectTransform>();
        text = this.gameObject.GetComponentInChildren<TextMeshProUGUI>();
    }

    public void SetupButton()
    {
        this.gameObject.GetComponent<Image>().color = preset.colour;

        text.text = Strings.Get("ui.interface", preset.tabName);
        name = text.text;

        content.CentrePage();
    }

    //On double click, centre page
    public void OnPointerClick(PointerEventData eventData)
    {
        int tap = eventData.clickCount;

        //Play sound
        AudioController.Instance.Play2DSound(AudioControls.Instance.tab);

        if (tap == 2)
        {
            content.CentrePage();
        }
    }

    public void VisualUpdate()
    {

    }

    public void SetNewItems(int newItemCount)
    {
        newItems = newItemCount;

        if(newItems > 0)
        {
            //Create pulsate colour
            //if (pulsateController == null)
            //{
            //    pulsateController = this.gameObject.AddComponent<PulsateController>();
            //    pulsateController.img = this.gameObject.GetComponent<Image>();
            //    pulsateController.speed = 1;
            //    pulsateController.normalColour = preset.colour;
            //    pulsateController.pulsateColour = Color.white;
            //}
            //else pulsateController.enabled = true;
        }
        else
        {
            //Remove pulsate colour
            if (pulsateController != null) pulsateController.enabled = false;
        }
    }
}
