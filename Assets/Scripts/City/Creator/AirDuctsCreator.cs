﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This script contains a function that populates the map with buildings.
//Script pass 1
public class AirDuctsCreator : Creator
{
    //Chunks to load per frame
    public int loadChunk = 1;

	//Singleton pattern
	private static AirDuctsCreator _instance;
    public static AirDuctsCreator Instance { get { return _instance; } }

	void Awake ()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
	}

    public override void StartLoading()
    {
        Game.Log("CityGen: Generating building air ducts...");
        StartCoroutine("Load");
	}

	IEnumerator Load()
	{
        int cursor = 0;

        while (cursor < CityData.Instance.buildingDirectory.Count)
        {
            for (int i = 0; i < loadChunk; i++)
            {
                //Break loop if complete
                if (cursor >= CityData.Instance.buildingDirectory.Count) break;

                if (CityConstructor.Instance.generateNew)
                {
                    CityData.Instance.buildingDirectory[cursor].GenerateAirDucts();
                }
                else
                {
                    foreach(AirDuctGroup ductGroup in CityData.Instance.buildingDirectory[cursor].airDucts)
                    {
                        ductGroup.LoadDucts();
                    }
                }

                cursor++;
            }

            CityConstructor.Instance.loadingProgress = ((float)cursor / (float)CityData.Instance.buildingDirectory.Count);
            yield return null;
        }

        //Complete!
        SetComplete();
	}
}
