using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinkButtonController : ButtonController
{
    TMP_SelectionController selectionController;
    string linkID;

    public void Setup(string newLinkID, TMP_SelectionController newSelectionController)
    {
        linkID = newLinkID;
        selectionController = newSelectionController;
        SetupReferences();

        if(selectionController != null)
        {
            if(selectionController.m_TextMeshPro != null)
            {
                if(selectionController.m_TextMeshPro.alignment == TMPro.TextAlignmentOptions.Left 
                    || selectionController.m_TextMeshPro.alignment == TMPro.TextAlignmentOptions.TopLeft 
                    || selectionController.m_TextMeshPro.alignment == TMPro.TextAlignmentOptions.BaselineLeft
                    || selectionController.m_TextMeshPro.alignment == TMPro.TextAlignmentOptions.CaplineLeft
                    || selectionController.m_TextMeshPro.alignment == TMPro.TextAlignmentOptions.MidlineLeft
                    || selectionController.m_TextMeshPro.alignment == TMPro.TextAlignmentOptions.BottomLeft)
                {
                    thisNavRectPoint = NavRectPoint.min;
                    otherNavRectPoint = NavRectPoint.min;
                }
                else if (selectionController.m_TextMeshPro.alignment == TMPro.TextAlignmentOptions.Right
                    || selectionController.m_TextMeshPro.alignment == TMPro.TextAlignmentOptions.TopRight
                    || selectionController.m_TextMeshPro.alignment == TMPro.TextAlignmentOptions.BaselineRight
                    || selectionController.m_TextMeshPro.alignment == TMPro.TextAlignmentOptions.CaplineRight
                    || selectionController.m_TextMeshPro.alignment == TMPro.TextAlignmentOptions.MidlineRight
                    || selectionController.m_TextMeshPro.alignment == TMPro.TextAlignmentOptions.BottomRight)
                {
                    thisNavRectPoint = NavRectPoint.max;
                    otherNavRectPoint = NavRectPoint.max;
                }
            }
        }
    }

    public override void OnLeftClick()
    {
        //Parse link ID to int
        int iD = 0;
        int.TryParse(linkID, out iD);

        Strings.LinkData foundLink = null;

        if (Strings.Instance.linkIDReference.TryGetValue(iD, out foundLink))
        {
            foundLink.OnLink();
        }

        base.OnLeftClick();
    }
}
