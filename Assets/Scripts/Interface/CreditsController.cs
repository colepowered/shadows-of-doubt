using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Text;

public class CreditsController : MonoBehaviour
{
    [System.Serializable]
    public class CreditCategory
    {
        public string name;
        public bool localize = false;
        public string extra;
        public bool localizeExtra = false;
        public List<CreditEntry> credits = new List<CreditEntry>();
    }

    [System.Serializable]
    public class CreditEntry
    {
        public string title;
        public List<CreditName> names = new List<CreditName>();
    }

    [System.Serializable]
    public class CreditName
    {
        public string name;
        public string additional;
    }

    [Header("Credits")]
    public List<CreditCategory> credits = new List<CreditCategory>();

    //Singleton pattern
    private static CreditsController _instance;
    public static CreditsController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        this.enabled = false;
    }

    public string GetFormattedCreditsText()
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < credits.Count; i++)
        {
            CreditCategory cat = credits[i];

            string catTitle = cat.name;
            if (cat.localize) catTitle = Strings.Get("ui.interface", catTitle);

            catTitle = "<uppercase><size=120%><b><u>" + catTitle + "</u></b><size=100%></uppercase>";

            sb.Append(catTitle);

            if(cat.extra != null && cat.extra.Length > 0)
            {
                string extra = cat.extra;
                if(cat.localizeExtra) extra = Strings.Get("ui.interface", extra);

                extra = "<line-height=150%><smallcaps>" + extra + "</smallcaps><line-height=100%>";
                sb.AppendLine();
                sb.Append(extra);
            }

            sb.AppendLine();

            for (int u = 0; u < cat.credits.Count; u++)
            {
                CreditEntry entry = cat.credits[u];

                sb.AppendLine();
                if(entry.title != null && entry.title.Length > 0) sb.Append("<smallcaps><size=100%><b>" + Strings.Get("ui.interface", entry.title) + "</b><size=100%></smallcaps>");
                sb.AppendLine();

                for (int l = 0; l < entry.names.Count; l++)
                {
                    CreditName cr = entry.names[l];

                    string nameEntry = string.Empty;

                    if(cr.additional != null && cr.additional.Length  > 0)
                    {
                        nameEntry = Strings.Get("ui.interface", cr.additional) + ": ";
                    }

                    nameEntry += cr.name;

                    sb.Append("<size=90%>" + nameEntry + "<size=100%>");
                    sb.AppendLine();
                }
            }

            sb.AppendLine();
        }

        return sb.ToString();
    }
}
