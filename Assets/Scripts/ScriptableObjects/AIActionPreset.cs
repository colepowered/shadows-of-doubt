﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "aiaction_data", menuName = "Database/AI/AI Action")]

public class AIActionPreset : SoCustomComparison
{
    public enum ActionLocation { interactable, findNearest, investigate, nearbyInvestigate, pause, randomNodeWithinLocation, flee, interactableLOS, meetOther, NearbyStreetRandomNode, putDownInteractable, pickUpInteractable, randomNodeWithinHome, interactableSpawn, proximityToMusic, player, tailAndConfrontPlayer };
    public enum ActionFacingDirection { towardsDestination, awayFromDestination, interactable, InverseInteractable, accessableDirection, investigate, door, interactableSetting, none, inverseInteractableSetting, player };

    public enum ActionFinding { doNothing, findNearest, removeAction, removeGoal };
    public enum ActionBusy { findAlternate, skipAction, skipGoal, standGuard, standGuardIfEnforcerSkipGoalNot};

    public enum FindSetting { nonTrespassing, onlyPublic, allAreas, homeOnly, workOnly};

    [System.Serializable]
    public class AISpeechPreset
    {
        public string dictionaryString;
        public string ddsMessageID;
        public bool isSuccessful = false;
        [Range(0, 10)]
        public int chance = 1;
        [Tooltip("Use parsing for special items in this string")]
        public bool useParsing = false;
        public bool shout = false;
        public bool interupt = false;
        public bool onlyIfEnfocerOnDuty = false;
        public bool onlyIfNotEnforcerOnDuty = false;
        [Tooltip("Must feature ANY of these character traits")]
        public List<CharacterTrait> mustFeatureTrait = new List<CharacterTrait>();
        [Tooltip("Can't feature ANY of these character traits")]
        public List<CharacterTrait> cantFeatureTrait = new List<CharacterTrait>();
        [Tooltip("Must be a killer and feature this motive")]
        public List<MurderMO> mustBeKillerWithMotive = new List<MurderMO>();

        //Dialog
        public List<Evidence.DataKey> tieKeys = new List<Evidence.DataKey>();
        public List<Evidence.Discovery> applyDiscovery = new List<Evidence.Discovery>();
        public bool endsDialog = false;
        public bool jobHandIn = false;
        public bool startCombat = false;
        public bool flee = false;
        public bool giveUpSelf = false;
    }

    [System.Serializable]
    public class AutomaticAction
    {
        public AIActionPreset forcedAction;
        public bool proximityCheck = false;
        public float additionalDelay = 0f;
    }

    [Header("Input")]
    public InteractablePreset.InteractionKey defaultKey = InteractablePreset.InteractionKey.none;
    [Tooltip("Debug this input")]
    public bool debug = false;
    [Tooltip("Useful when there are multiple active actions, the highest takes priority.")]
    [Range(-10, 11)]
    public int inputPriority = 0;
    [Tooltip("Only available when no first person item selected")]
    public bool unavailableWhenItemSelected = false;
    [Tooltip("If left blank, this is available when any first person item is selected...")]
    [EnableIf("unavailableWhenItemSelected")]
    public List<FirstPersonItem> unavailableWhenItemsSelected = new List<FirstPersonItem>();
    [Tooltip("Only available when this first person item selected")]
    public bool onlyAvailableWhenItemSelected = false;
    [EnableIf("onlyAvailableWhenItemSelected")]
    public List<FirstPersonItem> availableWhenItemsSelected = new List<FirstPersonItem>();
    public bool holsterCurrentItemOnAction = false;
    [Tooltip("Disable display on UI")]
    public bool disableUIDisplay = false;

    [Header("Location")]
    [InfoBox("Use this carefully in conjuction with the goal's location.\nInteractable: Checks for passed interactable, room, then location to find a destination.\nFind Nearest: Finds the nearest.\nInvestigate: Uses investigate position\nNearby Investigate: Uses a destination close to the investigation position\nPause: Uses the AI existing position\nRandom Node Within Location: A random node within the passed gamelocation\nFlee: Destination is somewhere safe\nInteractable LOS: Location within line of sight of the passed interactable\nNearby Random Street Node: Pick somewhere on a close by street.", EInfoBoxType.Normal)]
    public ActionLocation actionLocation = ActionLocation.interactable;
    [Tooltip("Check this against the room's action reference to continue using the passed reference.")]
    public bool confirmActionLocation = true;
    [Tooltip("If unable to find a node location for this action, attempt to find one using 'nearest' fuction (can be expensive). If this isn't checked then the goal will be removed.")]
    public ActionFinding onUnableToFindLocation = ActionFinding.findNearest;
    [Tooltip("Where to search when finding a location...")]
    public FindSetting searchSetting = FindSetting.nonTrespassing;
    [Tooltip("If the found use point is busy, do this...")]
    public ActionBusy onUsePointBusy = ActionBusy.findAlternate;
    public Interactable.UsePointSlot usageSlot = Interactable.UsePointSlot.defaultSlot; 

    [Tooltip("How much to factor in robbery priority when searching for location...")]
    public float robberyPriorityMultiplier = 0f;
    [Tooltip("Avoid choosing repeating interactables as long as this goal exists...")]
    public bool avoidRepeatingInteractables = false;
    [Tooltip("Aids searching by filtering rooms types that this must be in...")]
    public bool filterSearchUsingRoomType = false;
    [Tooltip("Aids searching by filtering rooms types that this must be in...")]
    [EnableIf("filterSearchUsingRoomType")]
    public List<RoomTypePreset> searchRoomType = new List<RoomTypePreset>();
    [Tooltip("Limit search to the goal's game location")]
    public bool limitSearchToGoalLocation = false;
    [Tooltip("When finding an action; Always use home as an option (even if above is true)")]
    public bool findOverrideWithHome = true;
    [Tooltip("Use special availability settings: Address telephone with nobody answering")]
    public bool requiresTelephone = false;
    [Tooltip("Use special availability settings: Address telephone with no calls active")]
    [ShowIf("requiresTelephone")]
    public bool requiresTelephoneNoCall = false;
    [Tooltip("Skip activation if there is no consumable to hand")]
    public bool activationRequiresConsumable = false;
    [Tooltip("Pull banned rooms from here...")]
    public SourceOfBannedRooms bannedRooms = SourceOfBannedRooms.none;
    public enum SourceOfBannedRooms { none, jobPreset};

    [Header("Completion")]
    [Tooltip("If true this action will execute until it is interupted by something else")]
    public bool completableAction = true;
    [Tooltip("Time taken in minutes")]
    [ShowIf("completableAction")]
    [MinMaxSlider(0, 120)]
    public Vector2 minutesTakenRange = new Vector2(1, 10);
    [Tooltip("Complete when AI has seen player do something illegal")]
    public bool completeOnSeeIllegal = false;
    [Tooltip("If true, once complete, this action will create another instance of itself, effectively repeating")]
    public bool repeatOnComplete = false;
    [EnableIf("repeatOnComplete")]
    [Tooltip("Repeat while the citizen has consumable items...")]
    public bool repeatWhileHavingConsumables = false;
    [Tooltip("AI controller will not be diabled on idle while this action is active if true")]
    public bool requiresForcedUpdate = false;

    [Header("Update")]
    [Tooltip("Don't update the priority of other goals (apart from investigate) while this is active")]
    public bool dontUpdateGoalPriorityWhileActive = false;
    [DisableIf("dontUpdateGoalPriorityWhileActive")]
    [Tooltip("Don't update the priority of other goals (apart from investigate) for this long after the action has been started (minutes)")]
    public int dontUpdateGoalPriorityFor = 0;
    [Space(5)]
    [Tooltip("If true the tick rate can be no higher than below while performing this action")]
    public bool limitTickRate = false;
    [EnableIf("limitTickRate")]
    public NewAIController.AITickRate minimumTickRate = NewAIController.AITickRate.veryLow;
    [EnableIf("limitTickRate")]
    public NewAIController.AITickRate maximumTickRate = NewAIController.AITickRate.medium;
    [Tooltip("If true, this action won't be removed upon goal's RefreshActions()")]
    public bool dontRemoveOnRefresh = false;
    [Tooltip("If true, this action won't be replaced upon goal's RefreshActions()")]
    public bool nonRefreshable = false;
    [Tooltip("Once victim is in LOS, then stop")]
    public bool useLOSCheck = false;
    [Tooltip("Cancel if target is not a valid mugging")]
    public bool cancelIfNonValidMugging = false;
    [Tooltip("Skip creation of this action if the AI is in the following state...")]
    public bool skipIfAIIsInState = false;
    [EnableIf("skipIfAIIsInState")]
    [Tooltip("Skip creation of this action if the AI is in the following state...")]
    public NewAIController.ReactionState skipIfReaction = NewAIController.ReactionState.none;
    [Tooltip("Skip if the player has a guest pass to here")]
    public bool skipIfGuestPass = false;

    [Header("Facing")]
    [Tooltip("Which way will this AI face when arrived at this action")]
    public ActionFacingDirection facing = ActionFacingDirection.interactableSetting;
    [Tooltip("If true, the AI will look around randomly if they don't have a specific target")]
    public bool lookAround = false;
    [Tooltip("If the persuit target isn't in range, cancel this action")]
    public bool cancelIfPersuitTargetNotInRange = false;
    [Tooltip("Face player when interacting")]
    public bool facePlayerWhileTalkingTo = true;

    [BoxGroup("Idle Animations")]
    public bool changeIdleOnActivate = true;
    [BoxGroup("Idle Animations")]
    [EnableIf("changeIdleOnActivate")]
    public CitizenAnimationController.IdleAnimationState idleAnimationOnActivate = CitizenAnimationController.IdleAnimationState.none;

    [Space(5)]
    [BoxGroup("Idle Animations")]
    public bool changeIdleOnArrival = false;
    [BoxGroup("Idle Animations")]
    [EnableIf("changeIdleOnArrival")]
    public CitizenAnimationController.IdleAnimationState idleAnimationOnArrival = CitizenAnimationController.IdleAnimationState.none;

    [Space(5)]
    [BoxGroup("Idle Animations")]
    public bool changeIdleOnDeactivate = false;
    [BoxGroup("Idle Animations")]
    [EnableIf("changeIdleOnDeactivate")]
    public CitizenAnimationController.IdleAnimationState idleAnimationOnDeactivate = CitizenAnimationController.IdleAnimationState.none;

    [Space(5)]
    [BoxGroup("Idle Animations")]
    public bool changeIdleOnComplete = false;
    [BoxGroup("Idle Animations")]
    [EnableIf("changeIdleOnComplete")]
    public CitizenAnimationController.IdleAnimationState idleAnimationOnComplete = CitizenAnimationController.IdleAnimationState.none;

    [BoxGroup("Arm Animations")]
    public bool changeArmsOnActivate = true;
    [BoxGroup("Arm Animations")]
    [EnableIf("changeArmsOnActivate")]
    public CitizenAnimationController.ArmsBoolSate armsAnimationOnActivate = CitizenAnimationController.ArmsBoolSate.none;

    [Space(5)]
    [BoxGroup("Arm Animations")]
    public bool changeArmsOnArrival = false;
    [BoxGroup("Arm Animations")]
    [EnableIf("changeArmsOnArrival")]
    public CitizenAnimationController.ArmsBoolSate armsAnimationOnArrival = CitizenAnimationController.ArmsBoolSate.none;

    [Space(5)]
    [BoxGroup("Arm Animations")]
    public bool changeArmsOnDeactivate = false;
    [BoxGroup("Arm Animations")]
    [EnableIf("changeArmsOnDeactivate")]
    public CitizenAnimationController.ArmsBoolSate armsAnimationOnDeactivate = CitizenAnimationController.ArmsBoolSate.none;

    [Space(5)]
    [BoxGroup("Arm Animations")]
    public bool changeArmsOnComplete = false;
    [BoxGroup("Arm Animations")]
    [EnableIf("changeArmsOnComplete")]
    public CitizenAnimationController.ArmsBoolSate armsAnimationOnComplete = CitizenAnimationController.ArmsBoolSate.none;

    [Space(5)]
    [Tooltip("Once destination is reached, tell the AI to lie down")]
    public bool lying = false;

    [Header("On Progress Stat modifiers")]
    [Tooltip("Pull the below from the currently-held consumable item...")]
    public bool useCurrentConsumable = false;
    [Tooltip("This is applied as progress increases")]
    [EnableIf("completableAction")]
    [Range(-1f, 1f)]
    public float progressNourishment = 0f;
    [Tooltip("This is applied as progress increases")]
    [EnableIf("completableAction")]
    [Range(-1f, 1f)]
    public float progressHydration = 0f;
    [Tooltip("This is applied as progress increases")]
    [EnableIf("completableAction")]
    [Range(-1f, 1f)]
    public float progressAlertness = 0f;
    [Tooltip("This is applied as progress increases")]
    [EnableIf("completableAction")]
    [Range(-1f, 1f)]
    public float progressEnergy = 0f;
    [Tooltip("This is applied as progress increases")]
    [EnableIf("completableAction")]
    [Range(-1f, 1f)]
    public float progressExcitement = 0f;
    [Tooltip("This is applied as progress increases")]
    [EnableIf("completableAction")]
    [Range(-1f, 1f)]
    public float progressChores = 0f;
    [Tooltip("This is applied as progress increases")]
    [EnableIf("completableAction")]
    [Range(-1f, 1f)]
    public float progressHygeiene = 0f;
    [Tooltip("This is applied as progress increases")]
    [EnableIf("completableAction")]
    [Range(-1f, 1f)]
    public float progressBladder = 0f;
    [Tooltip("This is applied as progress increases")]
    [EnableIf("completableAction")]
    [Range(-1f, 1f)]
    public float progressHeat = 0f;
    [Tooltip("This is applied as progress increases")]
    [EnableIf("completableAction")]
    [Range(-1f, 1f)]
    public float progressDrunk = 0f;
    [Tooltip("This is applied as progress increases")]
    [EnableIf("completableAction")]
    [Range(-1f, 1f)]
    public float progressBreath = 0f;
    [EnableIf("completableAction")]
    [Range(-1f, 1f)]
    public float progressPoisoned = 0f;

    [Header("Per Hour Stat modifiers")]
    [Tooltip("This is applied over time")]
    [Range(-12f, 12f)]
    public float overtimeNourishment = 0f;
    [Tooltip("This is applied over time")]
    [Range(-12f, 12f)]
    public float overtimeHydration = 0f;
    [Tooltip("This is applied over time")]
    [Range(-12f, 12f)]
    public float overtimeAlertness = 0f;
    [Tooltip("This is applied over time")]
    [Range(-12f, 12f)]
    public float overtimeEnergy = 0f;
    [Tooltip("This is applied over time")]
    [Range(-12f, 12f)]
    public float overtimeExcitement = 0f;
    [Tooltip("This is applied over time")]
    [Range(-12f, 12f)]
    public float overtimeChores = 0f;
    [Tooltip("This is applied over time")]
    [Range(-12f, 12f)]
    public float overtimeHygiene = 0f;
    [Tooltip("This is applied over time")]
    [Range(-12f, 12f)]
    public float overtimeBladder = 0f;
    [Tooltip("This is applied over time")]
    [Range(-12f, 12f)]
    public float overtimeHeat = 0f;
    [Tooltip("This is applied over time")]
    [Range(-12f, 12f)]
    public float overtimeDrunk = 0f;
    [Tooltip("This is applied over time")]
    [Range(-12f, 12f)]
    public float overtimeBreath = 0f;
    [Tooltip("This is applied over time")]
    [Range(-12f, 12f)]
    public float overtimePoison = 0f;

    [Header("Movement")]
    [Tooltip("If true this will use rules consistent with AI's investigate urgency state. If false and below is false, it will walk...")]
    public bool useInvestigationUrgency = false;
    [DisableIf("useInvestigationUrgency")]
    [Tooltip("If true this will use running only")]
    public bool forceRun = false;
    [Tooltip("Will run if this citizen can see the player")]
    public bool runIfSeesPlayer = false;

    [Header("AI")]
    [Tooltip("If true this will encourage using interactable with people I know and discourgage using them with people I don't. If this has a passed human interactable, I will save a space for them.")]
    public bool socialRules = false;
    [Tooltip("If true, the AI will detect the player as suspicious while this is active")]
    public bool spookAction = false;
    [Tooltip("Disable sighting updates while this action is active")]
    public bool disableSightingUpdates = false;
    [Tooltip("Attack the persuit target if they are close enough")]
    public bool attackPersuitTargetOnProximity = false;
    [Tooltip("Throw current items if at suitable range")]
    [EnableIf("attackPersuitTargetOnProximity")]
    public bool throwObjectsAtTarget = true;
    [Tooltip("Put the AI in combat pose")]
    public CombatPose useCombatPose = CombatPose.never;
    [Tooltip("In addition to the above condition, only use combat pose when escalation of investiation is 1")]
    public bool onlyUseCombatPoseWithEscalationOne = true;
    public enum CombatPose { noChange, always, never, onlyWhenPreviouslyPersuing};
    [Tooltip("Go to sleep OnComplete, wake up on end")]
    public bool sleepOnArrival = false;
    [Tooltip("This action is uninteruptable after destination has been reached (overrides the goal's interuption preset settings)")]
    public bool uninteruptableWhileAtLocation = false;
    [Tooltip("While active, Vmail threads can be progressed")]
    public bool progressVmailThreads = false;
    [Tooltip("If true, will disable casual conversation triggers while this is active")]
    public bool disableConversationTriggers = false;
    [Tooltip("If true, will cancel any conversations when activated")]
    public bool exitConversationOnActivate = false;
    [Space(5)]
    [Tooltip("Interactable presets related to this furniture parent must stay swtiched on while this is active...")]
    public List<InteractablePreset> forcedActive = new List<InteractablePreset>();
    [Tooltip("If AI, force perform these actions on the same object if they exist, if not integrated interactables on the same furniture will also be checked: On Arrival")]
    public List<AutomaticAction> forcedActionsOnArrival = new List<AutomaticAction>();
    [Tooltip("If AI, force perform these actions on the same object if they exist, if not integrated interactables on the same furniture will also be checked: On End")]
    public List<AutomaticAction> forcedActionsOnComplete = new List<AutomaticAction>();
    [Tooltip("To complete the above actions, if AI cannot find the appropriate action on the immediate interactable, search this much...")]
    public ForcedActionsSearchLevel forcedActionsSearchLevel = ForcedActionsSearchLevel.spawnedInteractablesAll;
    public enum ForcedActionsSearchLevel { thisObjectOnly, otherIntegratedInteractables, spawnInteractablesChildren, spawnedInteractablesAll, InteractablesOnNode};
    [Tooltip("Also execute the above actions if action is ended for any reason")]
    public bool executeCompleteActionsOnEnd = false;
    [Tooltip("Only do the above if at location")]
    [EnableIf("executeCompleteActionsOnEnd")]
    public bool executeCompleteActionsOnEndIfArrived = true;
    [Tooltip("Automatically execute this action on complete (action controller script)")]
    public bool executeThisOnComplete = true;
    [Tooltip("Execute these switch state changes on end along with the above actions...")]
    public List<InteractablePreset.SwitchState> switchStatesOnEnd = new List<InteractablePreset.SwitchState>();
    [Tooltip("This action will trigger an interactable being illegally activated if a character tresspassing triggers it")]
    public bool tamperAction = false;
    [Tooltip("This action will close/deactivate an illegally activated object (eg. turn off tv)")]
    public bool tamperResetAction = false;
    [Tooltip("If true then this citizen can fall asleep while doing this if they are tired enough")]
    public bool canFallAsleep = false;
    [EnableIf("canFallAsleep")]
    [Tooltip("If above is true then citizens can fall asleep after this time")]
    public int fallAsleepAfterMinimum = 20;

    [Space(7)]
    [Tooltip("On tick, check state of chosen interactable. If any of these match then cancel this action.")]
    public List<CheckActionAgainstState> checkActionAgainstState = new List<CheckActionAgainstState>();

    [System.Serializable]
    public class CheckActionAgainstState
    {
        public InteractablePreset.Switch switchState;
        public bool switchIs;
        public CheckActionOutcome outcome;
    }

    public enum CheckActionOutcome { cancelAction, cancelGoal};

    [Space(5)]
    [Tooltip("Enable to force a reaction state once this action is activated. Will not switch it back once ended.")]
    public bool forceReactionState = false;
    [EnableIf("forceReactionState")]
    public NewAIController.ReactionState setReactionState = NewAIController.ReactionState.none;
    [Tooltip("Ignore door keys settings")]
    public bool ignoreLockedDoors = false;
    [Tooltip("Break down doors in my way!")]
    public bool breakDownDoors = false;

    //[Header("Room Preferences")]
    //public bool overrideGoalLightingPreference = false;
    //[EnableIf("overrideGoalLightingPreference")]
    //public AIGoalPreset.LightingPreference lightPreferenceOverride = AIGoalPreset.LightingPreference.doesntMatter;

    [Header("Allowable Action Insertions")]
    public bool doorsAllowed = true;
    public bool deactivateAllowed = true;

    [Header("Delay")]
    [Tooltip("If use point is busy, delay goal from repeating for this time...")]
    public float repeatDelayOnActionFail = 0f;
    [Tooltip("If interupted by a more important goal, delay goal from repeating for this time...")]
    public float repeatDelayOnActionSuccess = 0f;

    [Header("Basic Actions")]
    [Tooltip("When at the gamelocation, turn all lights off, excluding the destination room.")]
    public bool turnAllGamelocationLightsOff = false;
    public bool overrideGoalLightRule = false;
    [EnableIf("overrideGoalLightRule")]
    public bool onlyOverrideIfAtGamelocation = true;
    [EnableIf("overrideGoalLightRule")]
    public List<RoomConfiguration.AILightingBehaviour> lightingBehaviour = new List<RoomConfiguration.AILightingBehaviour>();
    public bool overrideGoalDoorRule = false;
    [EnableIf("overrideGoalDoorRule")]
    [Tooltip("Execute the closing of doors as below:")]
    public DoorRule doorRule = DoorRule.normal;
    public enum DoorRule { normal, dontLock, dontClose, onlyCloseToLocation, onlyLockToLocation };
    public enum LightRule { normal, dontSwitch, onlyWhenArrived };

    [Header("Sounds")]
    [InfoBox("Note: This is only triggered by AI")]
    public AudioEvent onArrivalSound;
    public bool isLoop = false;
    [DisableIf("isLoop")]
    public float soundDelay = 0f;

    [BoxGroup("Outfits")]
    [Tooltip("Check to see if we need outdoor clothes when 'make clothed' is enabled below...")]
    public bool outdoorClothingCheck = true;
    [Space(5)]
    [BoxGroup("Outfits")]
    public bool specificOutfitOnActivate = false;
    [BoxGroup("Outfits")]
    [EnableIf("specificOutfitOnActivate")]
    public ClothesPreset.OutfitCategory allowedOutfitOnActivate = ClothesPreset.OutfitCategory.casual;
    [BoxGroup("Outfits")]
    [DisableIf("specificOutfitOnActivate")]
    [Tooltip("If no specific outfit is required, make sure the citizen is at least clothed!")]
    public bool makeClothedOnActivate = true;
    [Space(5)]

    [BoxGroup("Outfits")]
    public bool specificOutfitOnArrive = false;
    [BoxGroup("Outfits")]
    [EnableIf("specificOutfitOnArrive")]
    public ClothesPreset.OutfitCategory allowedOutfitOnArrive = ClothesPreset.OutfitCategory.casual;
    [BoxGroup("Outfits")]
    [DisableIf("specificOutfitOnArrive")]
    [Tooltip("If no specific outfit is required, make sure the citizen is at least clothed!")]
    public bool makeClothedOnArrive = true;
    [Space(5)]

    [BoxGroup("Outfits")]
    public bool specificOutfitOnDeactivate= false;
    [EnableIf("specificOutfitOnDeactivate")]
    [BoxGroup("Outfits")]
    public ClothesPreset.OutfitCategory allowedOutfitOnDeactivate= ClothesPreset.OutfitCategory.casual;
    [BoxGroup("Outfits")]
    [DisableIf("specificOutfitOnDeactivate")]
    [Tooltip("If no specific outfit is required, make sure the citizen is at least clothed!")]
    public bool makeClothedOnDeactivate = true;
    [Space(5)]

    [BoxGroup("Outfits")]
    public bool specificOutfitOnComplete = false;
    [EnableIf("specificOutfitOnComplete")]
    [BoxGroup("Outfits")]
    public ClothesPreset.OutfitCategory allowedOutfitOnComplete = ClothesPreset.OutfitCategory.casual;
    [BoxGroup("Outfits")]
    [DisableIf("specificOutfitOnComplete")]
    [Tooltip("If no specific outfit is required, make sure the citizen is at least clothed!")]
    public bool makeClothedOnComplete = true;

    [BoxGroup("Expressions")]
    public bool setExpressionOnActivate = false;
    [EnableIf("setExpressionOnActivate")]
    [BoxGroup("Expressions")]
    public CitizenOutfitController.Expression activateExpression = CitizenOutfitController.Expression.neutral;
    [Space(5)]
    [BoxGroup("Expressions")]
    public bool setExpressionOnArrive = false;
    [EnableIf("setExpressionOnArrive")]
    [BoxGroup("Expressions")]
    public CitizenOutfitController.Expression arriveExpression = CitizenOutfitController.Expression.neutral;
    [Space(5)]
    [BoxGroup("Expressions")]
    public bool setExpressionOnDeactivate = false;
    [EnableIf("setExpressionOnDeactivate")]
    [BoxGroup("Expressions")]
    public CitizenOutfitController.Expression deactivateExpression = CitizenOutfitController.Expression.neutral;
    [Space(5)]
    [BoxGroup("Expressions")]
    public bool setExpressionOnComplete = false;
    [EnableIf("setExpressionOnComplete")]
    [BoxGroup("Expressions")]
    public CitizenOutfitController.Expression completeExpression = CitizenOutfitController.Expression.neutral;

    [Header("Items")]
    [Tooltip("Allow (any) items to be held during this action")]
    public bool allowItems = true;
    [Tooltip("Allow a action-specific custom item to be held")]
    public bool enableCustomItem = false;
    [Tooltip("Spawn this item in right hand")]
    [EnableIf("enableCustomItem")]
    public GameObject itemRight;
    [EnableIf("enableCustomItem")]
    public Vector3 itemRightLocalPos;
    [EnableIf("enableCustomItem")]
    public Vector3 itemRightLocalEuler;
    [Space(7)]
    [Tooltip("Spawn this item in left hand")]
    [EnableIf("enableCustomItem")]
    public GameObject itemLeft;
    [EnableIf("enableCustomItem")]
    public Vector3 itemLeftLocalPos;
    [EnableIf("enableCustomItem")]
    public Vector3 itemLeftLocalEuler;
    [EnableIf("enableCustomItem")]
    public ActionStateFlag spawnCustomItemOn = ActionStateFlag.onActivation;
    [EnableIf("enableCustomItem")]
    public ActionStateFlag destroyCustomItemOn = ActionStateFlag.onGoalDeactivation;
    [EnableIf("enableCustomItem")]
    [Tooltip("Does this require a custom carrying animation?")]
    public bool requiresCarryAnimation = false;
    [EnableIf("enableCustomItem")]
    public int overrideCarryAnimation = 1;

    public enum ActionItemToggle { onActivation, onArrival};
    public enum ActionDestroyItem { onFinishGoal, onFinishAction}

    public enum ActionStateFlag { onActivation, onArrival, onDeactivation, onGoalDeactivation, none};

    [Header("Speech")]
    [Range(0f, 1f)]
    public float chanceOfOnTrigger = 0.5f;
    public List<SpeechController.Bark> onTriggerBark = new List<SpeechController.Bark>();
    [Range(0f, 1f)]
    public float chanceOfWhileJourney = 0.5f;
    public List<SpeechController.Bark> whileJourneyBark = new List<SpeechController.Bark>();
    [Range(0f, 1f)]
    public float chanceOfOnArrival = 0.5f;
    public List<SpeechController.Bark> onArrivalBark = new List<SpeechController.Bark>();
    [Range(0f, 1f)]
    public float chanceOfWhileArrived= 0.5f;
    public bool mustSeeOtherCitizen = false;
    public List<SpeechController.Bark> whileArrivedBark = new List<SpeechController.Bark>();
    [Range(0f, 1f)]
    public float chanceOfOnComplete = 0.5f;
    public List<SpeechController.Bark> onCompleteBark = new List<SpeechController.Bark>();
}
