﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MetaObject
{
    public int id = -1;
    public string preset;
    public int owner = -1;
    public int writer = -1;
    public int reciever = -1;
    public string dds;
    public List<Interactable.Passed> passed;
    public Vector3Int n;
    public bool cd = false; //Saved with city data

    public MetaObject(InteractablePreset newPreset, Human newOwner, Human newWriter, Human newReciever, List<Interactable.Passed> newPassed)
    {
        //Asssign same ID as interactable...
        id = Interactable.worldAssignID;

        //If this exists already, add to ID to avoid conflicts
        while (CityData.Instance.savableInteractableDictionary.ContainsKey(id) || CityData.Instance.metaObjectDictionary.ContainsKey(id))
        {
            id++;
        }

        Interactable.worldAssignID = id + 1;

        preset = newPreset.name;
        if(newOwner != null) owner = newOwner.humanID;
        if (newWriter != null) writer = newWriter.humanID;
        if (newReciever != null) reciever = newReciever.humanID;
        passed = newPassed;

        //Add self reference to passed variables
        if(passed == null)
        {
            passed = new List<Interactable.Passed>();
        }

        passed.Add(new Interactable.Passed(Interactable.PassedVarType.metaObjectID, id));

        CityData.Instance.metaObjectDictionary.Add(id, this);
    }

    public void Remove()
    {
        if(CityData.Instance.metaObjectDictionary.ContainsKey(id))
        {
            CityData.Instance.metaObjectDictionary.Remove(id);
        }
    }

    public Evidence GetEvidence(bool setPosition = false, Vector3Int nodeCoord = new Vector3Int())
    {
        Game.Log("Evidence: Getting evidence for meta object " + preset);

        Human o = null;
        Human w = null;
        Human r = null;

        if (owner > -1)
        {
            CityData.Instance.GetHuman(owner, out o);
        }

        if (writer > -1)
        {
            CityData.Instance.GetHuman(writer, out w);
        }

        if (reciever > -1)
        {
            CityData.Instance.GetHuman(reciever, out r);
        }

        NewGameLocation foundAt = null;

        if(setPosition)
        {
            n = nodeCoord;
            //Game.Log("Get evidence of meta object and setting positon: " + n);
        }

        NewNode foundNode = null;

        if(PathFinder.Instance.nodeMap.TryGetValue(n, out foundNode))
        {
            //Game.Log("Found node gamelocation of meta object " + foundNode.gameLocation.name);
            foundAt = foundNode.gameLocation;
        }

        InteractablePreset ip = null;

        if (Toolbox.Instance.objectPresetDictionary.TryGetValue(preset, out ip))
        {
            return Toolbox.Instance.GetOrCreateEvidenceForInteractable(ip, "I" + id, null, o, w, r, null, foundAt, ip.retailItem, passed);
        }
        else return null;
    }

    public InteractablePreset GetPreset()
    {
        InteractablePreset ip = null;
        Toolbox.Instance.objectPresetDictionary.TryGetValue(preset, out ip);
        return ip;
    }
}
