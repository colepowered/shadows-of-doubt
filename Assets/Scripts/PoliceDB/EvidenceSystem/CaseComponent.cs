﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//Comprised of evidence and facts, this is a base class for both.
public class CaseComponent
{
    //Name given to this component
    public string name;

    //Controls whether this component has been found yet
    public bool isFound = false;

    //The sprite for the icon of this component
    public Sprite iconSprite; //32x32

    //Events
    //Triggered on found
    public delegate void DiscoveredThis(CaseComponent discovered);
    public event DiscoveredThis OnDiscoveredThis;

    //Triggered when name is updated (only if different)
    public delegate void NewName();
    public event NewName OnNewName;

    //Triggered when the sprite is updated
    public delegate void NewSprite();
    public event NewSprite OnNewSprite;

    //Constructor
    public CaseComponent()
    {
        
    }

    //Set found this component or not
    public virtual void SetFound(bool newVal)
    {
        if(isFound != newVal)
        {
            bool prev = isFound;
            isFound = newVal;

            if (!prev && newVal)
            {
                OnDiscovery();

                //Fire event
                if (OnDiscoveredThis != null) OnDiscoveredThis(this);
            }
        }
    }

    public virtual string GetIdentifier()
    {
        return string.Empty;
    }

    //Triggered once when discovered
    public virtual void OnDiscovery()
    {
        return;
    }

    //Set the name for this component
    public virtual void UpdateName()
    {
        string newName = GenerateName() + FoundAtName() + GenerateNameSuffix();

        //If the new name is different, trigger this
        if(newName != name)
        {
            //Trim extra whitespace
            name = newName.Trim();

            //Fire event
            if (OnNewName != null) OnNewName();
        }
    }

    //Methods that make up name
    public virtual string GenerateName()
    {
        return string.Empty;
    }

    //...Found at
    public virtual string FoundAtName()
    {
        return string.Empty;
    }

    //Suffix to the name (goes on end)
    public virtual string GenerateNameSuffix()
    {
        return string.Empty;
    }

    //Set the icon image of this component
    public void SetNewIcon(Sprite newLarge)
    {
        if(newLarge != null) iconSprite = newLarge;

        if (OnNewSprite != null) OnNewSprite();
    }
}
