﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using Unity.Collections;
using Unity.Mathematics;
using System.Linq;
using NaughtyAttributes;
using System;

public class NewAddress : NewGameLocation
{
    [Header("Address Contents")]
    public AddressSaveData saveData; //Reference to the loaded data (editor only)
    public int loadedVarIndex = -1;
    //public AddressLayoutVariation loadedVariation; //Reference to the loaded room layout variation (editor only)
    public List<NewWall> generatedInteriorEntrances = new List<NewWall>(); //List of interior entrances generated: Reset these upon another generation cycle
    public List<NewNode> protectedNodes = new List<NewNode>(); //List of nodes that can't be overwritten: Reset these upon another generation cycle
    public bool featuresNeonSignageHorizontal = false;
    public bool featuresNeonSignageVertical = false;
    public NeonSignCharacters neonFont;
    public int neonColour = 0;
    public GameObject neonSignHorizontal;
    public GameObject neonSignVertical;
    public int neonVerticalIndex = -1;
    public bool featuresBrokenSign = false;
    public bool generatedRoomConfigs = false;

    public List<Vandalism> vandalism = new List<Vandalism>();

    [System.Serializable]
    public class Vandalism
    {
        public float time = 0; //The time this happened
        public int fine; //Fine value
        public int obj = -1; //Object id
        public Vector3 win = new Vector3(0, -100, 0); //Window loc (id)
    }

    [Header("Details")]
    public int id = 0;
    public static int assignID = 1;
    //public string baseSeed;
    public int editorID = 0; //Editor IDs used for naming inside the floor editor
    public static int assignEditorID = 1; //Editor IDs used for naming inside the floor editor
    public LayoutConfiguration preset; //Preset used to determin interior generation
    public Color editorColour = Color.cyan; //Colour assigned to the room in the floor editor (editor only)
    public Color wood; //Colour of the wood for this address
    public bool isOutsideAddress = false; //If true, this is the floor's outside address
    public bool isLobbyAddress = false; //If true, this is the floor's lobby address
    public float normalizedLandValue = 0f;
    public bool hiddenSpareKey = false; //True if a spare key has been added hidden near the entrance

    [Header("Inhabitants")]
    public List<Human> owners = new List<Human>(); //The owners of this address. Can be inhabitants of an apartment, or owner of a business
    public List<Human> inhabitants = new List<Human>(); //Similar to owners but in the case of a business includes everyone who works here. Anyone who has stuff here basically.
    public List<Human> favouredCustomers = new List<Human>(); //Citizens where this is one of their fav places
    public AddressPreset addressPreset; //Address details preset
    public ResidenceController residence; //Residence controller
    public Company company; //Company controller
    public bool interiorLightsEnabled = true; //Controlled by CitizenBehaviour gameworld check function

    //Belongs to references
    public Dictionary<RoomTypePreset, Dictionary<NewRoom, List<Human>>> roomsBelongTo = new Dictionary<RoomTypePreset, Dictionary<NewRoom, List<Human>>>();

    [Space(7)]
    //Updated stats for the owners: This is useful for generating the decor...
    public float averageHumility = 0f;
    public float averageEmotionality = 0f;
    public float averageExtraversion = 0f;
    public float averageAgreeableness = 0f;
    public float averageConscientiousness = 0f;
    public float maxConscientiousness = 0f;
    public float averageCreativity = 0f;

    [Header("For Sale")]
    [System.NonSerialized]
    public Interactable saleNote;

    [Header("Alarms")]
    public List<Interactable> alarms = new List<Interactable>(); //List of all interactables to turn on/off with alarm
    public List<Interactable> sentryGuns = new List<Interactable>();
    public List<Interactable> otherSecurity = new List<Interactable>();
    public bool alarmActive = false;
    public NewBuilding.AlarmTargetMode targetMode = NewBuilding.AlarmTargetMode.illegalActivities;
    public float targetModeSetAt; //Keeps track of when this was altered so we can change it back
    public List<Human> alarmTargets = new List<Human>();
    public float alarmTimer = 0f;
    public int breakerSecurityID = -1;
    public int breakerDoorsID = -1;
    public int breakerLightsID = -1;
    [System.NonSerialized]
    public Interactable breakerSecurity;
    [System.NonSerialized]
    public Interactable breakerDoors;
    [System.NonSerialized]
    public Interactable breakerLights;
    public float breakerSecurityState = 1; //Breaker for security state
    public float breakerLightsState = 1; //Breaker for security state
    public float breakerDoorsState = 1; //Breaker for security state

    [Header("AI Navigation")]
    public Dictionary<PathKey, List<NewNode.NodeAccess>> internalRoutes = new Dictionary<PathKey, List<NewNode.NodeAccess>>();
    public bool generatedEntranceWeights = false; //True once entrance weights have been generated

    //Used for job pathfinding
    public NativeMultiHashMap<float3, int> accessRef; //Use a node coord to access multiple references to access
    public NativeHashMap<int, float3> accessPositions; //Use a access reference to access node positions of the access (for heuristics)
    public NativeHashMap<int, float3> toNodeReference; //Use as a reference to the 'to' node
    public NativeList<float3> noPassRef; //Use a node coord as reference to nodes that are not allowed a pass through

    //Pathfinding key- use value operator for comparisons
    public struct PathKey : IEquatable<PathKey>
    {
        public NewNode origin;
        public NewNode destination;
        private bool hasHash;
        private int hash;

        public PathKey(NewNode locOne, NewNode locTwo)
        {
            origin = locOne;
            destination = locTwo;
            hasHash = false;
            hash = 0;
        }

        public override int GetHashCode()
        {
            if (!hasHash)
            {
                hash = HashCode.Combine(origin, destination);
                hasHash = true;
            }
            return hash;
        }

        bool IEquatable<PathKey>.Equals(PathKey other)
        {
            return other.GetHashCode() == GetHashCode();
        }
    }

    //For use when choosing air vent locations
    public enum AirVent { ceiling, wallUpper, wallLower};

    public struct AirVentLocation
    {
        public NewRoom room;
        public AirVent location;
    }

    public class AddressCalc
    {
        public AddressPreset preset;
        public float score;
    }

    [Header("Passwords")]
    public GameplayController.Passcode passcode;

    //evidence
    public EvidenceMultiPage calendar;

    [Header("Debug")]
    public GameObject floorEditDebugParent;
    public List<CompanyOpenHoursPreset.CompanyShift> debugCompanyShifts = new List<CompanyOpenHoursPreset.CompanyShift>();

    //Setup
    public void Setup(NewFloor newFloor, LayoutConfiguration newType, DesignStylePreset newDefaultStyle)
    {
        newFloor.AddNewAddress(this);
        this.transform.SetParent(floor.gameObject.transform);
        this.transform.localPosition = Vector3.zero;

        id = assignID;
        assignID++;
        //baseSeed = (Toolbox.Instance.SeedRand(0, 99999999) * id).ToString();

        editorID = assignEditorID;
        assignEditorID++;
        preset = newType;
        if (preset.isOutside) isOutside = true;

        if(SessionData.Instance.isFloorEdit)
        {
            editorColour = FloorEditController.Instance.editorAddressColours[editorID % FloorEditController.Instance.editorAddressColours.Count];
        }

        //Game.Log("New address created with " + preset.name);
        SetName();

        passcode = new GameplayController.Passcode(GameplayController.PasscodeType.address);
        passcode.id = id;

        //Add to directory
        if (!CityData.Instance.addressDictionary.ContainsKey(id))
        {
            CityData.Instance.addressDirectory.Add(this);
            CityData.Instance.addressDictionary.Add(id, this);
        }

        //Create evidence file
        CreateEvidence();

        //Set lobby: Not used in editor, game only
        if (preset.roomLayout != null)
        {
            if (preset.isLobby)
            {
                isLobby = true;
                building.AddLobby(this);

                //if (floor.floor == 0)
                //{
                //    isMainLobby = true;
                //}
            }
        }

        //Run base setup
        if(SessionData.Instance.isFloorEdit)
        {
            base.CommonSetup(false, null, newDefaultStyle);
        }
        else
        {
            base.CommonSetup(false, building.cityTile.district, newDefaultStyle);
        }
    }

    //Generate room configurations
    public void GenerateRoomConfigs()
    {
        //Must have address preset for this
        if(addressPreset != null)
        {
            //Clear open plan elements
            foreach(NewRoom r in rooms)
            {
                if(r.preset != null)
                {
                    if (!r.isNullRoom && (!preset.requiresHallway || preset.hallway != r.preset)) r.preset = null;
                }

                r.openPlanElements.Clear();
            }

            foreach(RoomConfiguration cf in addressPreset.roomConfig)
            {
                NewRoom findAppropriate = rooms.Find(item => item.preset == null && item.roomType == cf.roomType);

                if(findAppropriate != null)
                {
                    findAppropriate.SetConfiguration(cf);
                }
                else
                {
                    if(SessionData.Instance.isFloorEdit) Game.Log("Unable to find an appropriate room for " + cf.name + " in " + name);

                    //Are there no rooms of this type that exist?
                    if (cf.canBeOpenPlan && cf.openPlanRoom != null)
                    {
                        NewRoom roomToAddTo = rooms.Find(item => item.roomType == cf.openPlanRoom);

                        if (roomToAddTo != null)
                        {
                            //Game.Log("Added " + room.room.ToString() + " to " + roomToAddTo.name + " in open plan format");
                            roomToAddTo.AddOpenPlanElement(cf);
                            continue;
                        }
                        else continue;
                    }
                }
            }

            //Repeat list for existing rooms. This ensures lobbies etc are filled up.
            foreach(NewRoom room in rooms)
            {
                if(room.preset == null)
                {
                    RoomConfiguration findAppropriate = addressPreset.roomConfig.Find(item => item.roomType == room.roomType);

                    if (findAppropriate != null)
                    {
                        room.SetConfiguration(findAppropriate);
                    }
                    else if(room.roomType.forceConfiguration != null)
                    {
                        room.SetConfiguration(room.roomType.forceConfiguration);
                    }
                    else
                    {
                        Game.Log("Unable to find room for " + room.roomType + " at address " + id + " setting as null...");
                        room.SetConfiguration(InteriorControls.Instance.nullConfig);
                    }
                }
            }

            generatedRoomConfigs = true;
        }
        else
        {
            foreach (NewRoom room in rooms)
            {
                if (room.preset == null)
                {
                    if (room.roomType.forceConfiguration != null)
                    {
                        room.SetConfiguration(room.roomType.forceConfiguration);
                    }
                    else
                    {
                        Game.Log("Unable to find room for " + room.roomType + " at address " + id + " setting as null...");
                        room.SetConfiguration(InteriorControls.Instance.nullConfig);
                    }
                }
            }
        }
    }

    //Calculate land value
    public void CalculateLandValue()
    {
        if(SessionData.Instance.isFloorEdit)
        {
            normalizedLandValue = Toolbox.Instance.Rand(0, 5);
        }
        else
        {
            //City tile land value is the floor
            normalizedLandValue = (int)building.cityTile.landValue / 4f; //City tile's land value 0 - 4

            //Increase with every floor (maximum of +5 (floor #20))
            normalizedLandValue += Mathf.Min(floor.floor / 4f, 5f);

            //Increase with size (+1 for every 20 nodes)
            foreach(NewRoom room in rooms)
            {
                normalizedLandValue += room.nodes.Count * 0.05f;
            }

            //Act as though the maximum of the above is 12. This would be max land value building, 20th floor with 60 node floor space.
            normalizedLandValue = Mathf.Clamp01(normalizedLandValue / 12f);
        }

        if (addressPreset != null) normalizedLandValue = Mathf.Clamp(normalizedLandValue, addressPreset.minimumLandValue, addressPreset.maximumLandValue);
    }

    //Set the target mode for alarm systems
    public void SetTargetMode(NewBuilding.AlarmTargetMode newMode, bool setResetTimer = true)
    {
        targetMode = newMode;

        Game.Log("Set target mode for " + name + ": " + targetMode);

        if (setResetTimer)
        {
            targetModeSetAt = SessionData.Instance.gameTime;

            if (targetMode != NewBuilding.AlarmTargetMode.illegalActivities)
            {
                if (!GameplayController.Instance.alteredSecurityTargetsLocations.Contains(this))
                {
                    GameplayController.Instance.alteredSecurityTargetsLocations.Add(this);
                }
            }
        }
    }

    //Load from data
    public void Load(CitySaveData.AddressCitySave data, NewFloor newFloor)
    {
        id = data.id;
        assignID = Mathf.Max(assignID, id + 1); //Make sure others won't overwrite this ID

        generatedRoomConfigs = true;

        name = data.name;
        this.transform.name = name;
        residenceNumber = data.residenceNumber; //Residence number relative to this floor (set in addresses only)
        isLobby = data.isLobby; //Game Only: True if this is a building lobby (set in addresses only)
        //isMainLobby = data.isMainLobby; //Game Only: True if this lobby features the main entrance
        isOutside = data.isOutside; //Is this location outside?
        access = data.access;
        wood = data.wood;
        isLobbyAddress = data.isLobbyAddress;
        isOutsideAddress = data.isOutsideAddress;
        featuresNeonSignageHorizontal = data.neonHor;
        featuresNeonSignageVertical = data.neonVer;
        neonVerticalIndex = data.neonVerticalIndex;
        neonColour = data.neonColour;
        normalizedLandValue = data.landValue;
        passcode = data.passcode;
        hiddenSpareKey = data.hkey;
        breakerDoorsID = data.breakerDoors;
        breakerLightsID = data.breakerLights;
        breakerSecurityID = data.breakerSec;

        newFloor.AddNewAddress(this);
        this.transform.SetParent(floor.gameObject.transform);
        this.transform.localPosition = Vector3.zero;

        if (isLobbyAddress)
        {
            newFloor.lobbyAddress = this;
        }

        if (isOutsideAddress)
        {
            newFloor.outsideAddress = this;
        }

        //Load scriptable object presets
        if (data.neonFont != null && data.neonFont.Length > 0) Toolbox.Instance.LoadDataFromResources<NeonSignCharacters>(data.neonFont, out neonFont);
        Toolbox.Instance.LoadDataFromResources<LayoutConfiguration>(data.preset, out preset);
        Toolbox.Instance.LoadDataFromResources<DesignStylePreset>(data.designStyle, out designStyle); //Design style chosen for the address

        if (data.address != null && data.address.Length > 0)
        {
            Toolbox.Instance.LoadDataFromResources<AddressPreset>(data.address, out addressPreset); //Design style chosen for the address

            //Add ref
            if (!CityData.Instance.addressTypeReference.ContainsKey(addressPreset))
            {
                CityData.Instance.addressTypeReference.Add(addressPreset, new List<NewAddress>());
            }

            CityData.Instance.addressTypeReference[addressPreset].Add(this);
        }

        //Add to directory
        if (!CityData.Instance.addressDictionary.ContainsKey(id))
        {
            CityData.Instance.addressDirectory.Add(this);
            CityData.Instance.addressDictionary.Add(id, this);
        }

        //For now, create evidence
        CreateEvidence();

        //Set lobby: Not used in editor, game only
        if (preset.roomLayout != null)
        {
            if (preset.isLobby)
            {
                isLobby = true;
                building.AddLobby(this);

                //if (floor.floor == 0)
                //{
                //    isMainLobby = true;
                //}
            }
        }

        //Load protected nodes
        foreach(Vector3 v3 in data.protectedNodes)
        {
            protectedNodes.Add(PathFinder.Instance.nodeMap[v3]);
        }

        //Run base setup
        base.CommonSetup(false, building.cityTile.district, designStyle);

        //Load residence & company
        if(data.residence != null && data.residence.preset != null && data.residence.preset.Length > 0)
        {
            residence = this.gameObject.AddComponent<ResidenceController>();
            residence.Load(data.residence, this);
        }

        if(data.company != null && data.company.preset != null && data.company.preset.Length > 0)
        {
            company = new Company();
            company.Load(data.company, this);
        }

        //Load rooms
        int countFurniture = 0;

        foreach (CitySaveData.RoomCitySave room in data.rooms)
        {
            NewRoom newRoom = null;

            if (!room.isBaseNullRoom)
            {
                //Create new room
                GameObject newObj = Instantiate(PrefabControls.Instance.room, this.transform);
                newRoom = newObj.GetComponent<NewRoom>();
            }
            else
            {
                //If this is the null room, it is loaded 'on top' of the existing created room...
                newRoom = nullRoom;
            }

            CitySaveData.RoomCitySave loadData = room;

            //Do we have decor override data for this?
            if (CityConstructor.Instance != null && CityConstructor.Instance.saveState != null)
            {
                StateSaveData.RoomStateSave rOvr = CityConstructor.Instance.saveState.rooms.Find(item => item.id == room.id && item.decorOverride != null && item.decorOverride.Count > 0);

                if (rOvr != null)
                {
                    Game.Log("CityGen: Found save state room decor override for: " + room.name);
                    loadData = rOvr.decorOverride[0];
                    newRoom.decorEdit = true;
                }
            }

            newRoom.Load(loadData, this);

            countFurniture += newRoom.furniture.Count;
        }

        //Furniture bug check
        if (countFurniture <= 0)
        {
            if(residence != null)
            {
                Game.Log(name + " residential address has no furniture loaded...");

                int clusters = 0;
                int objs = 0;

                foreach (CitySaveData.RoomCitySave room in data.rooms)
                {
                    foreach (CitySaveData.FurnitureClusterCitySave cluster in room.f)
                    {
                        clusters++;
                        objs += cluster.objs.Count;
                    }
                }

                Game.Log("... The save files contains " + clusters + " and " + objs + " objects");
            }
        }

        if(residence != null)
        {
            if(data.residence.mail > 0)
            {
                if(!CityConstructor.Instance.loadingFurnitureReference.TryGetValue(data.residence.mail, out residence.mailbox))
                {
                    Game.LogError("Cannot find mailbox furniture " + data.residence.mail);
                }
            }
        }
    }

    //Add a residence/company
    public void AssignPurpose()
    {
        if (preset.assignPurpose)
        {
            List<AddressCalc> shortlist = new List<AddressCalc>();
            List<AddressPreset> compatibleImportant = new List<AddressPreset>();
            AddressPreset forcePick = null;

            string pKey = seed;

            foreach (AddressPreset pre in Toolbox.Instance.allAddressPresets)
            {
                if (pre.disableThis) continue; //Disable this

                if(pre.compatible.Contains(preset) && (floor == null || (floor.floor >= pre.minMaxFloors.x && floor.floor <= pre.minMaxFloors.y)))
                {
                    if (pre.debug) Game.Log("Testing " + pre.name + " for " + id + "...");

                    //All possible types are present in the floor editor...
                    if (SessionData.Instance.isFloorEdit)
                    {
                        compatibleImportant.Add(pre);
                        continue;
                    }

                    if(pre.limitToBuildings.Count > 0 && !pre.limitToBuildings.Contains(building.preset))
                    {
                        continue;
                    }

                    int existingInstances = 0;

                    //Calculate a base score
                    float score = pre.baseScore + Toolbox.Instance.GetPsuedoRandomNumberContained(0, 0.1f, pKey, out pKey);

                    //Apply sizing bonus
                    if (nodes.Count >= pre.fitsUnitSizeMin && nodes.Count <= pre.fitsUnitSizeMax)
                    {
                        if (pre.debug) Game.Log(pre.name + " fits within unit size: " + nodes.Count + " at " + id);
                        score += 5f;
                    }
                    else if (pre.hardSizeLimits)
                    {
                        if (pre.debug) Game.Log(pre.name + " does not within unit size: " + nodes.Count + " at " + id);
                        continue;
                    }

                    //Try to place at least one of these addresses...
                    if (CityData.Instance.addressTypeReference.ContainsKey(pre))
                    {
                        existingInstances = CityData.Instance.addressTypeReference[pre].Count;

                        if (pre.important && existingInstances <= 0)
                        {
                            compatibleImportant.Add(pre);
                            continue;
                        }
                        //Max instances reached...
                        else if(existingInstances >= pre.maxInstances)
                        {
                            continue;
                        }
                    }
                    else
                    {
                        if (pre.important)
                        {
                            compatibleImportant.Add(pre);
                        }
                    }

                    //Minus for existing...
                    score -= existingInstances * pre.baseScoreFrequencyPenalty;

                    //Apply footfall
                    if (streetAccess != null)
                    {
                        StreetController getStr = streetAccess.fromNode.gameLocation.thisAsStreet;
                        if(getStr == null) getStr = streetAccess.toNode.gameLocation.thisAsStreet;

                        float diff = 1f - Mathf.Abs(getStr.normalizedFootfall - pre.idealFootfall);

                        score += diff * pre.footfallMultiplier;
                    }

                    //Apply district rules
                    foreach(AddressPreset.AddressRule rule in pre.addressRules)
                    {
                        if(district.preset == rule.districtPreset)
                        {
                            score += rule.scoreModifier;
                        }
                    }

                    //Add to shortlist
                    if(pre.forcePick)
                    {
                        forcePick = pre;
                        break;
                    }
                    else
                    {
                        shortlist.Add(new AddressCalc { preset = pre, score = score });
                    }
                }
            }

            if(shortlist.Count <= 0 && compatibleImportant.Count <= 0 && forcePick == null)
            {
                if (isLobby)
                {
                    if (building.preset.lobbyPreset != null)
                    {
                        addressPreset = building.preset.lobbyPreset;
                    }
                    else addressPreset = InteriorControls.Instance.lobbyAddressPreset;

                    return;
                }

                Game.LogError("No address preset shortlist for " + preset.name + " nodes count = " + nodes.Count + " @ " + building.name + " floor " + floor.floor);
                return;
            }

            //Pick from lists...
            if(forcePick != null)
            {
                if (forcePick.debug) Game.Log("Force picked " + forcePick.name + " for " + id);
                addressPreset = forcePick;
            }
            else if (compatibleImportant.Count > 0)
            {
                addressPreset = compatibleImportant[Toolbox.Instance.GetPsuedoRandomNumberContained(0, compatibleImportant.Count, pKey, out pKey)];
            }
            else if(shortlist.Count > 0)
            {
                //Pick address with the best score...
                shortlist.Sort((p2, p1) => p1.score.CompareTo(p2.score)); //Highest first
                addressPreset = shortlist[0].preset;
            }
        }
        else if(preset.addressPreset != null)
        {
            addressPreset = preset.addressPreset;
        }

        if(addressPreset == null)
        {
            if(SessionData.Instance.isFloorEdit) Game.Log("No address preset for " + name);
            return;
        }

        access = addressPreset.access;

        //Add ref
        if(!CityData.Instance.addressTypeReference.ContainsKey(addressPreset))
        {
            CityData.Instance.addressTypeReference.Add(addressPreset, new List<NewAddress>());
        }

        CityData.Instance.addressTypeReference[addressPreset].Add(this);

        //Add company
        if (addressPreset.company != null)
        {
            //companyContainer = new GameObject();
            //companyContainer.transform.SetParent(this.gameObject.transform);
            //company = companyContainer.AddComponent<CompanyController>();
            company = new Company();
            company.Setup(addressPreset.company, this);
        }

        //Add residence
        if (addressPreset.residence != null)
        {
            residenceNumber = floor.assignResidence;
            floor.assignResidence++;
            residence = this.gameObject.AddComponent<ResidenceController>();
            residence.Setup(addressPreset.residence, this);
        }

        if (!generatedRoomConfigs)
        {
            GenerateRoomConfigs(); //New room configs
        }

        //Re-assign walls presets to allow overrides
        if (!SessionData.Instance.isFloorEdit)
        {
            foreach (NewRoom room in rooms)
            {
                foreach (NewNode node in room.nodes)
                {
                    foreach (NewWall wall in node.walls)
                    {
                        wall.SetDoorPairPreset(wall.preset);
                    }
                }
            }
        }
    }

    //Must do this after naming
    public void SetupNeonSigns()
    {
        //Add neon sign
        if(addressPreset != null)
        {
            string pKey = seed;

            if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, pKey, out pKey) <= addressPreset.chanceOfNameSignHorizontal && addressPreset.signCharacterSet.Count > 0)
            {
                featuresNeonSignageHorizontal = true;
                neonColour = GetNeon();
                neonFont = addressPreset.signCharacterSet[Toolbox.Instance.GetPsuedoRandomNumberContained(0, addressPreset.signCharacterSet.Count, pKey, out pKey)];
            }

            if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, pKey, out pKey) <= addressPreset.chanceOfNameSignVertical && addressPreset.possibleSigns.Count > 0)
            {
                featuresNeonSignageVertical = true;
                if (!featuresNeonSignageHorizontal) neonColour = GetNeon();
                neonVerticalIndex = Toolbox.Instance.GetPsuedoRandomNumberContained(0, addressPreset.possibleSigns.Count, pKey, out pKey);
            }
        }
    }

    private int GetNeon()
    {
        List<int> valid = new List<int>();

        if (company != null)
        {
            foreach(string tag in company.nameAltTags)
            {
                for (int i = 0; i < CityControls.Instance.neonColours.Count; i++)
                {
                    CityControls.NeonMaterial n = CityControls.Instance.neonColours[i];

                    if (n.colourTag == tag)
                    {
                        valid.Add(i);
                    }
                }
            }
        }

        if(valid.Count <= 0)
        {
            for (int i = 0; i < CityControls.Instance.neonColours.Count; i++)
            {
                valid.Add(i);
            }
        }

        return valid[Toolbox.Instance.GetPsuedoRandomNumber(0, valid.Count, seed)];
    }

    //Add an owner
    public void AddOwner(Human newOwner)
    {
        if (!owners.Contains(newOwner))
        {
            owners.Add(newOwner);

            averageHumility = 0f;
            averageEmotionality = 0f;
            averageExtraversion = 0f;
            averageAgreeableness = 0f;
            averageConscientiousness = 0f;
            averageCreativity = 0f;
            maxConscientiousness = 0f;

            foreach (Human cit in owners)
            {
                averageHumility += cit.humility;
                averageEmotionality += cit.emotionality;
                averageExtraversion += cit.extraversion;
                averageAgreeableness += cit.agreeableness;
                averageConscientiousness += cit.conscientiousness;
                averageCreativity += cit.creativity;
                maxConscientiousness = Mathf.Max(maxConscientiousness, cit.conscientiousness);
            }

            averageHumility /= (float)owners.Count;
            averageEmotionality /= (float)owners.Count;
            averageExtraversion /= (float)owners.Count;
            averageAgreeableness /= (float)owners.Count;
            averageConscientiousness /= (float)owners.Count;
            averageCreativity /= (float)owners.Count;
        }
    }

    //Add an inhabitant
    public void AddInhabitant(Human newInhabitant)
    {
        if (!inhabitants.Contains(newInhabitant))
        {
            inhabitants.Add(newInhabitant);
        }
    }

    //Remove an inhabitant
    public void RemoveInhabitant(Human newInhabitant)
    {
        if (inhabitants.Contains(newInhabitant))
        {
            inhabitants.Remove(newInhabitant);
        }
    }

    //Add an owner
    public void RemoveOwner(Human newOwner)
    {
        if (owners.Contains(newOwner))
        {
            owners.Remove(newOwner);

            averageHumility = 0f;
            averageEmotionality = 0f;
            averageExtraversion = 0f;
            averageAgreeableness = 0f;
            averageConscientiousness = 0f;
            averageCreativity = 0f;
            maxConscientiousness = 0f;

            foreach (Human cit in owners)
            {
                averageHumility += cit.humility;
                averageEmotionality += cit.emotionality;
                averageExtraversion += cit.extraversion;
                averageAgreeableness += cit.agreeableness;
                averageConscientiousness += cit.conscientiousness;
                averageCreativity += cit.creativity;
                maxConscientiousness = Mathf.Max(maxConscientiousness, cit.conscientiousness);
            }

            averageHumility /= (float)owners.Count;
            averageEmotionality /= (float)owners.Count;
            averageExtraversion /= (float)owners.Count;
            averageAgreeableness /= (float)owners.Count;
            averageConscientiousness /= (float)owners.Count;
            averageCreativity /= (float)owners.Count;
        }
    }

    //Update the design style based on owners...
    public void UpdateDesignStyle()
    {
        List<DesignStylePreset> possiblePresets = new List<DesignStylePreset>();
        bool foundStyle = false;

        //Get the design style for the building, so it's all consistent
        if(!SessionData.Instance.isFloorEdit)
        {
            if (preset.useBuildingDesignStyle)
            {
                //Game.Log(preset.name + " borrowing design style from building...");

                if(building.designStyle.compatibleAddressTypes.Contains(preset))
                {
                    possiblePresets.Add(building.designStyle);
                    foundStyle = true;
                }
            }
        }

        if(!foundStyle)
        {
            float wealthLevel = Toolbox.Instance.GetNormalizedLandValue(this);

            //Add option using this citizen's traits...
            foreach (DesignStylePreset dPreset in Toolbox.Instance.allDesignStyles)
            {
                if (!dPreset.compatibleAddressTypes.Contains(preset))
                {
                    continue;
                }

                if(!dPreset.includeInPersonalityMatching)
                {
                    continue;
                }

                //Basement
                if(SessionData.Instance.isFloorEdit && FloorEditController.Instance.forceBasementToggle.isOn)
                {
                    if (!dPreset.isBasement) continue;
                }
                else
                {
                    if (floor.floor >= 0)
                    {
                        if (dPreset.isBasement) continue;
                    }
                    else
                    {
                        if (!dPreset.isBasement) continue;
                    }
                }

                if (dPreset.minimumWealth > wealthLevel) continue;

                int av = 1;

                //Get matching stats...
                int humility = 10 - Mathf.RoundToInt(Mathf.Abs(dPreset.humility - (averageHumility * 10f)));
                int emotionality = 10 - Mathf.RoundToInt(Mathf.Abs(dPreset.emotionality - (averageEmotionality * 10f)));
                int extraversion = 10 - Mathf.RoundToInt(Mathf.Abs(dPreset.extraversion - (averageExtraversion * 10f)));
                int agreeableness = 10 - Mathf.RoundToInt(Mathf.Abs(dPreset.agreeableness - (averageAgreeableness * 10f)));
                int conscientiousness = 10 - Mathf.RoundToInt(Mathf.Abs(dPreset.conscientiousness - (averageConscientiousness * 10f)));
                int creativity = 10 - Mathf.RoundToInt(Mathf.Abs(dPreset.creativity - (averageCreativity * 10f)));

                //Add and divide by 6 to give a score out of 10
                av = Mathf.FloorToInt((float)(humility + emotionality + extraversion + agreeableness + conscientiousness + creativity) / 6f);

                for (int i = 0; i < av; i++)
                {
                    possiblePresets.Add(dPreset);
                }
            }
        }

        if(possiblePresets.Count <= 0)
        {
            possiblePresets.Add(CityControls.Instance.fallbackStyle);
            Game.Log("CityGen: No compatible design styles found for " + name + ", using backup style...");
        }

        //You should now have a list proportionally weighted towards personality...
        int rng = 0;

        if(SessionData.Instance.isFloorEdit)
        {
            rng = Toolbox.Instance.Rand(0, possiblePresets.Count);
        }
        else
        {
            rng = Toolbox.Instance.GetPsuedoRandomNumber(0, possiblePresets.Count, id.ToString() + district.districtID.ToString());
        }

        SetDesignStyle(possiblePresets[rng]);
    }

    //Set name depending on how many other addresses of this type there are on the floor (intended for editor)
    public void SetName()
    {
        //Set name to type & number of types present
        int numberOfTypes = 0;
        List<NewAddress> addressesOfType = floor.addresses.FindAll(item => item.preset == preset);
        if (addressesOfType != null) numberOfTypes += addressesOfType.Count;

        if(preset != null) name = preset.name + " " + numberOfTypes.ToString();
        this.gameObject.name = name;
    }

    //Set name
    public void SetName(string newName)
    {
        name = newName;
        this.transform.name = name;
    }

    //Set the address type of this
    public void SetAddressType(LayoutConfiguration newType)
    {
        if (newType != preset)
        {
            Game.Log("Set address to " + preset.name);
            preset = newType;
            if (preset.isOutside) isOutside = true;
            SetName();

            //Add as lobby
            if(newType.isLobby)
            {
                isLobby = true;
                building.AddLobby(this);

                //if (floor.floor == 0)
                //{
                //    isMainLobby = true;
                //}
            }
        }
    }

    //On destroy, remove from parent
    private void OnDestroy()
    {
        if(SessionData.Instance.isFloorEdit)
        {
            if (floor != null) floor.RemoveAddress(this);
        }
    }

    //There was a knock on this door
    public void OnDoorKnockByActor(NewDoor dc, float urgency, Actor byWho)
    {
        Game.Log("Debug: Knock on door " + name + " with urgency " + urgency + " with occupants count: " + currentOccupants.Count);

        foreach (Actor cc in currentOccupants)
        {
            if (cc.isDead)
            {
                Game.Log("Debug: Occupant: isDead");
                continue;
            }

            if (cc.isStunned)
            {
                Game.Log("Debug: Occupant: stunned");
                continue;
            }

            if (cc.isPlayer)
            {
                Game.Log("Debug: Occupant: isPlayer");
                continue;
            }

            if (cc.isAsleep)
            {
                if (Toolbox.Instance.Rand(0f, 1f) <= urgency + 0.1f)
                {
                    Game.Log("Debug: " + cc.name + " was woken up by knock");
                    cc.WakeUp();
                    //continue;
                }
                else
                {
                    Game.Log("Debug: " + cc.name + " was not woken by knock");
                    continue;
                }
            }

            Human h = cc as Human;

            if (h != null)
            {
                if(h.locationsOfAuthority.Contains(this as NewGameLocation) || (h.isEnforcer && h.isOnDuty))
                {
                    //Sombody is already answering the door
                    foreach(Actor citHere in currentOccupants)
                    {
                        if (citHere.ai != null && citHere.ai.goals.Exists(item => item.preset == RoutineControls.Instance.answerDoorGoal && item.passedInteractable == dc.handleInteractable))
                        {
                            Game.Log("Debug: Answering door event already exists for " + citHere.name);
                            return;
                        }
                    }

                    //Go and open the door
                    Game.Log("Debug: " +cc.name + " answering door...");
                    cc.ai.AnswerDoor(dc, this as NewGameLocation, byWho);

                    break;
                }
            }
        }
    }

    public override void CreateEvidence()
    {
        //Create entry
        if (evidenceEntry == null && !isOutsideAddress && preset.roomLayout.Count > 0)
        {
            evidenceEntry = EvidenceCreator.Instance.CreateEvidence("location", "Location" + id, this, newParent: building.evidenceEntry) as EvidenceLocation;
        }
    }

    //The evidence entry file should already be made- run this on creation
    public override void SetupEvidence()
    {
        base.SetupEvidence();
    }

    //Create a calendar and add owner's events to it
    public EvidenceMultiPage CreateCalendar()
    {
        if(calendar == null)
        {
            //Game.Log("Create calendar for " + name + " with " + owners.Count + " owners...");

            calendar = EvidenceCreator.Instance.CreateEvidence("Calendar", "Calendar" + id, this) as EvidenceMultiPage;

            //This should be saved with city data; no need to generated it unless this is a new game...
            if(CityConstructor.Instance == null || CityConstructor.Instance.generateNew)
            {
                //Add months
                for (int i = 1; i < 13; i++)
                {
                    calendar.AddStringContentToPage(i, Strings.Get("ui.interface", ((SessionData.Month)(i - 1)).ToString())); //Add month as title
                }

                //Add birthdays
                Dictionary<int, Dictionary<int, List<Human>>> calendarBirthdays = new Dictionary<int, Dictionary<int, List<Human>>>();

                foreach (Human human in owners)
                {
                    if (human.isPlayer) continue;

                    string[] bday = human.birthday.Split('/');
                    int birthdayMonth = 0;
                    int birthdayDay = 0;
                    int.TryParse(bday[0], out birthdayMonth);
                    int.TryParse(bday[1], out birthdayDay);

                    if (!calendarBirthdays.ContainsKey(birthdayMonth))
                    {
                        calendarBirthdays.Add(birthdayMonth, new Dictionary<int, List<Human>>());
                    }

                    if (!calendarBirthdays[birthdayMonth].ContainsKey(birthdayDay))
                    {
                        calendarBirthdays[birthdayMonth].Add(birthdayDay, new List<Human>());
                    }

                    if (!calendarBirthdays[birthdayMonth][birthdayDay].Contains(human))
                    {
                        calendarBirthdays[birthdayMonth][birthdayDay].Add(human);

                        //calendar.AddFactLink(human.factDictionary["Birthday"], Evidence.DataKey.name, false);
                        calendar.AddEvidenceDiscoveryToPage(birthdayMonth, human.evidenceEntry, Evidence.Discovery.dateOfBirth); //Add fact discovery
                    }

                    foreach (Acquaintance aq in human.acquaintances)
                    {
                        if (aq.with.isPlayer) continue;

                        if (aq.known >= SocialControls.Instance.knowBirthdayThreshold)
                        {
                            string[] bday2 = aq.with.birthday.Split('/');
                            int birthdayMonth2 = 0;
                            int birthdayDay2 = 0;
                            int.TryParse(bday2[0], out birthdayMonth2);
                            int.TryParse(bday2[1], out birthdayDay2);

                            //Game.Log("Add birthday for " + aq.with.name + " month: " + birthdayMonth2 + " day: " + birthdayDay2 + " (" + aq.with.birthday + ")");

                            if (!calendarBirthdays.ContainsKey(birthdayMonth2))
                            {
                                calendarBirthdays.Add(birthdayMonth2, new Dictionary<int, List<Human>>());
                            }

                            if (!calendarBirthdays[birthdayMonth2].ContainsKey(birthdayDay2))
                            {
                                calendarBirthdays[birthdayMonth2].Add(birthdayDay2, new List<Human>());
                            }

                            if (!calendarBirthdays[birthdayMonth2][birthdayDay2].Contains(aq.with))
                            {
                                calendarBirthdays[birthdayMonth2][birthdayDay2].Add(aq.with);

                                //calendar.AddFactLink(aq.with.factDictionary["Birthday"], Evidence.DataKey.name, false);
                                calendar.AddEvidenceDiscoveryToPage(birthdayMonth2, aq.with.evidenceEntry, Evidence.Discovery.dateOfBirth); //Add fact discovery
                            }
                        }
                    }
                }

                //Parse into text data
                for (int i = 1; i < 13; i++)
                {
                    //The birthday reference contains the month...
                    if (calendarBirthdays.ContainsKey(i))
                    {
                        for (int u = 1; u < 32; u++)
                        {
                            //The birthday reference contains the day in the month
                            if(calendarBirthdays[i].ContainsKey(u))
                            {
                                string humanList = string.Empty;

                                for (int l = 0; l < calendarBirthdays[i][u].Count; l++)
                                {
                                    Human bdayperson = calendarBirthdays[i][u][l];

                                    if (l > 0) humanList += ", "; //Add comma for list
                                    humanList += bdayperson.GetCitizenName();
                                }

                                //Game.Log("Birthdays " + u + ": " + humanList + " (" + calendarBirthdays[i][u].Count + ")");

                                if (humanList.Length > 0) calendar.AddStringContentToPage(i, Toolbox.Instance.GetNumbericalStringReference(u) + " — " + humanList + " " + Strings.Get("ui.interface", "birthday"), order: u);
                            }
                        }
                    }
                }

                //Add random stuff
            }
            //Add birthday discoveries
            else
            {
                foreach (Human human in owners)
                {
                    if (human.isPlayer) continue;

                    string[] bday = human.birthday.Split('/');
                    int birthdayMonth = 0;
                    int birthdayDay = 0;
                    int.TryParse(bday[0], out birthdayMonth);
                    int.TryParse(bday[1], out birthdayDay);

                    //calendar.AddFactLink(human.factDictionary["Birthday"], Evidence.DataKey.name, false);
                    calendar.AddEvidenceDiscoveryToPage(birthdayMonth, human.evidenceEntry, Evidence.Discovery.dateOfBirth); //Add fact discovery

                    foreach (Acquaintance aq in human.acquaintances)
                    {
                        if (aq.with.isPlayer) continue;

                        if (aq.known >= SocialControls.Instance.knowBirthdayThreshold)
                        {
                            string[] bday2 = aq.with.birthday.Split('/');
                            int birthdayMonth2 = 0;
                            int birthdayDay2 = 0;
                            int.TryParse(bday2[0], out birthdayMonth2);
                            int.TryParse(bday2[1], out birthdayDay2);

                            //calendar.AddFactLink(aq.with.factDictionary["Birthday"], Evidence.DataKey.name, false);
                            calendar.AddEvidenceDiscoveryToPage(birthdayMonth2, aq.with.evidenceEntry, Evidence.Discovery.dateOfBirth); //Add fact discovery
                        }
                    }
                }
            }

            calendar.SetPage(SessionData.Instance.monthInt + 1, false);
        }

        return calendar;
    }

    public CitySaveData.AddressCitySave GenerateSaveData()
    {
        CitySaveData.AddressCitySave output = new CitySaveData.AddressCitySave();

        output.name = name;
        output.residenceNumber = residenceNumber; //Residence number relative to this floor (set in addresses only)
        output.isLobby = isLobby; //Game Only: True if this is a building lobby (set in addresses only)
        //output.isMainLobby = isMainLobby; //Game Only: True if this lobby features the main entrance
        output.isOutside = isOutside; //Is this location outside?
        output.access = access;
        output.neonHor = featuresNeonSignageHorizontal;
        output.neonVer = featuresNeonSignageVertical;
        output.neonVerticalIndex = neonVerticalIndex;
        output.neonColour = neonColour;
        output.landValue = normalizedLandValue;
        output.passcode = passcode;
        output.hkey = hiddenSpareKey;

        output.breakerSec = breakerSecurityID;
        output.breakerLights = breakerLightsID;
        output.breakerDoors = breakerDoorsID;

        if (neonFont != null) output.neonFont = neonFont.name;

        foreach(NewRoom room in rooms)
        {
            output.rooms.Add(room.GenerateSaveData());
        }

        foreach(NewNode node in protectedNodes)
        {
            output.protectedNodes.Add(node.nodeCoord);
        }

        output.designStyle = designStyle.name; //Design style chosen for the address
        output.id = id;
        output.preset = preset.name; //Preset used to determin interior generation
        output.wood = wood; //Colour of the wood for this address
        if(addressPreset != null) output.address = addressPreset.name; //Address details preset
        output.isLobbyAddress = isLobbyAddress;
        output.isOutsideAddress = isOutsideAddress;

        if(residence != null) output.residence = residence.GenerateSaveData();
        if (company != null) output.company = company.GenerateSaveData();

        if (Game.Instance.devMode)
        {
            //output.noteDebug = debugNotePlacement;
        }

        return output;
    }

    //Create a sign for this company
    public void CreateSignageHorizontal()
    {
        //Remove previous
        if (neonSignHorizontal != null) Destroy(neonSignHorizontal);

        //Find the longest placement of this: Anylise outside walls and find the longest row of adjacent ones
        List<NewWall> outsideWalls = new List<NewWall>();

        foreach (NewRoom room in rooms)
        {
            foreach (NewNode node in room.nodes)
            {
                foreach(NewWall wall in node.walls)
                {
                    if(wall.node.gameLocation.thisAsStreet != null)
                    {
                        if(!wall.node.tile.isObstacle)
                        {
                            outsideWalls.Add(wall);
                        }
                        
                    }
                    else if(wall.otherWall.node.gameLocation.thisAsStreet != null)
                    {
                        if(!wall.otherWall.node.tile.isObstacle)
                        {
                            outsideWalls.Add(wall.otherWall);
                        }
                    }
                }
            }
        }

        if (outsideWalls.Count <= 0) return; //Cannot make sign if there are no outside walls...

        //You should now have all the outside walls at the address, find the largest grouping with the same offset...

        int safety = 999;
        List<List<NewWall>> adjacentCollections = new List<List<NewWall>>();

        while (outsideWalls.Count > 0 && safety > 0)
        {
            safety--;

            List<NewWall> openSet = new List<NewWall>();
            openSet.Add(outsideWalls[0]);

            List<NewWall> closedSet = new List<NewWall>();
            int safety2 = 999;

            while (openSet.Count > 0 && safety2 > 0)
            {
                NewWall currentWall = openSet[0];
                outsideWalls.Remove(currentWall); //Remove from outside walls reference

                //Scan adjacent tiles
                foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                {
                    Vector3 adj = currentWall.node.nodeCoord + new Vector3Int(v2.x, v2.y, 0);
                    NewNode foundNode = null;

                    if (PathFinder.Instance.nodeMap.TryGetValue(adj, out foundNode))
                    {
                        //Does this feature a wall in the list?
                        NewWall foundWall = outsideWalls.Find(item => item.node == foundNode);

                        if (foundWall != null)
                        {
                            //The offset must also match
                            if (foundWall.wallOffset == currentWall.wallOffset)
                            {
                                if (!openSet.Contains(foundWall) && !closedSet.Contains(foundWall))
                                {
                                    openSet.Add(foundWall);
                                }
                            }
                        }
                    }
                }

                //Add to closed set
                closedSet.Add(currentWall);
                openSet.Remove(currentWall);
                safety2--;
            }

            //Add the closed set to collections
            adjacentCollections.Add(closedSet);
        }

        //Find the longest wall
        List<NewWall> longestWall = null;
        int longestWallScore = -1;

        foreach(List<NewWall> listWall in adjacentCollections)
        {
            //If this wall collection features an entrance, treat it as if the count was boosted by 2
            NewWall entranceWall = listWall.Find(item => item.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance);
            int extra = 0;
            if (entranceWall != null) extra = 2;

            if(longestWall == null || (listWall.Count + extra) > longestWallScore)
            {
                longestWall = listWall;
                longestWallScore = listWall.Count + extra;
            }
        }

        //Game.Log("Longest outside wall is " + longestWall.Count + " (out of " + adjacentCollections.Count + " sets)");

        //Find the maximum supported width
        float maximumWidth = longestWall.Count * 1.8f;

        //Anylise length
        float signLength = 0f;
        List<GameObject> prefabList = new List<GameObject>();

        if(company != null)
        {
            for (int i = 0; i < company.shortName.Length; i++)
            {
                try
                {
                    string character = company.shortName.Substring(i, 1).ToLower();

                    //Detect space
                    if(character == " ")
                    {
                        signLength += 0.4f;
                        prefabList.Add(null);
                        continue;
                    }

                    //Get letter prefab
                    NeonSignCharacters.NeonCharacter foundChar = neonFont.characterList.Find(item => item.character.ToLower() == character);

                    if (foundChar != null)
                    {
                        GameObject letterPrefab = foundChar.prefab;
                        prefabList.Add(letterPrefab);
                        signLength += letterPrefab.GetComponent<MeshRenderer>().bounds.size.x;
                    }
                    else continue;

                    //Add letter spacing
                    if(i < name.Length - 1)
                    {
                        signLength += 0.1f;
                    }

                    //Is the sign too long?
                    if (signLength > maximumWidth) return;
                }
                catch
                {
                    return;
                }
            }
        }


        //Find the central wall tile to parent to...
        //Average node position of all
        Vector3 avNodePos = Vector3.zero;
        Vector3 centrePosition = Vector3.zero;

        foreach(NewWall wall in longestWall)
        {
            avNodePos += wall.node.nodeCoord;
            centrePosition += wall.position;
        }

        centrePosition /= (float)longestWall.Count;
        avNodePos /= (float)longestWall.Count;
        avNodePos = new Vector3(Mathf.Round(avNodePos.x), Mathf.Round(avNodePos.y), Mathf.Round(avNodePos.z)); //Average pos: This node needs to be ref by int
        NewWall centreWall = longestWall.Find(item => item.node.nodeCoord == avNodePos);

        neonSignHorizontal = Instantiate(PrefabControls.Instance.neonSign, centreWall.node.room.transform);

        Matrix4x4 matrixTrans = Matrix4x4.identity;
        matrixTrans.SetTRS(centreWall.position, Quaternion.Euler(centreWall.localEulerAngles + centreWall.node.room.transform.eulerAngles), Vector3.one);

        //Matrix4x4 version of Transform.InverseTransformPoint
        neonSignHorizontal.transform.position = matrixTrans.MultiplyPoint3x4(building.preset.horizontalSignOffset + addressPreset.horizontalSignOffset);
        neonSignHorizontal.transform.localEulerAngles = centreWall.localEulerAngles;

        //neonSignHorizontal.transform.localPosition = centreWall.node.room.transform.InverseTransformPoint(centrePosition + building.preset.horizontalSignOffset + addressPreset.horizontalSignOffset);
        //neonSignHorizontal.transform.localPosition = new Vector3(neonSignHorizontal.transform.localPosition.x, 4.046f, neonSignHorizontal.transform.localPosition.z);

        //Set the parent node as an audio source with offset
        centreWall.node.SetAsAudioSource(AudioControls.Instance.neonSignLoopSmall, new Vector3(0, 3f, 0f));

        //Create material(s)
        CityControls.NeonMaterial neonMaterial = CityControls.Instance.neonColours[neonColour];

        if(neonMaterial.regularMat == null)
        {
            neonMaterial.regularMat = Instantiate(CityControls.Instance.neonMaterial) as Material;
            neonMaterial.regularMat.SetColor("_BaseColor", neonMaterial.neonColour * CityControls.Instance.neonIntensity);

            //Create variation that can use flicker...
            neonMaterial.flickingMat = Instantiate(CityControls.Instance.neonMaterial) as Material;
            neonMaterial.flickingMat.SetColor("_BaseColor", neonMaterial.neonColour * CityControls.Instance.neonIntensity);
        }

        //Set light colours
        Light[] lights = neonSignHorizontal.GetComponentsInChildren<Light>();

        foreach (Light light in lights)
        {
            HDAdditionalLightData hdrpLightData = light.transform.gameObject.GetComponent<HDAdditionalLightData>();
            light.color = neonMaterial.neonColour;

            HDLightTypeAndShape shape = hdrpLightData.GetLightTypeAndShape();

            if (shape == HDLightTypeAndShape.RectangleArea)
            {
                hdrpLightData.shapeWidth = signLength;
            }
        }

        neonSignHorizontal.transform.name = this.name;
        float placement = 0f;

        List<MeshFilter> childMeshes = new List<MeshFilter>();

        for (int i = 0; i < prefabList.Count; i++)
        {
            if(prefabList[i] == null)
            {
                //Add spacing
                placement += 0.5f;
                continue;
            }

            //Add half of this
            Vector2 letterBounds = prefabList[i].GetComponent<MeshRenderer>().bounds.size;

            placement += letterBounds.x * 0.5f;

            //Spawn letter
            GameObject newLetter = Instantiate(prefabList[i], neonSignHorizontal.transform);
            newLetter.transform.localPosition = new Vector3(-placement + (signLength * 0.5f) + 0.3f, letterBounds.y * -0.5f, 0);

            //Chance of flicker 4%
            if(Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, this.GetReplicableSeed()) <= 0.04f)
            {
                newLetter.GetComponent<MeshRenderer>().material = neonMaterial.flickingMat;

                //Add a flickering sound source here.
                NeonLetterFlickerController flicker = newLetter.AddComponent<NeonLetterFlickerController>();
                flicker.neonMat = neonMaterial;

                featuresBrokenSign = true; //Flag for checking convo triggers
            }
            else
            {
                //Set shader
                //newLetter.GetComponent<MeshRenderer>().material = neonMaterial.regularMat; //Not needed as we now combine non-flickering letters into one mesh below

                childMeshes.Add(newLetter.GetComponent<MeshFilter>());
            }

            //Add half of this
            placement += prefabList[i].GetComponent<MeshRenderer>().bounds.size.x * 0.5f;

            //Add spacing
            placement += 0.1f;
        }

        //Combine letter meshes...
        GameObject ret = new GameObject();

        //Create a mesh instance for combined meshes
        CombineInstance[] combine = new CombineInstance[childMeshes.Count];

        //Cycle through meshes to combine them
        int c = 0;

        while (c < childMeshes.Count)
        {
            combine[c].mesh = childMeshes[c].sharedMesh;
            combine[c].transform = childMeshes[c].transform.localToWorldMatrix;

            //As we still need colliders, only remove mesh filters and renderers from existing components
            Destroy(childMeshes[c].gameObject);

            c++;
        }

        //Create mesh renderer
        MeshFilter meshFilter = ret.AddComponent<MeshFilter>();
        MeshRenderer meshRenderer = ret.AddComponent<MeshRenderer>();
        MeshCollider meshColl = ret.AddComponent<MeshCollider>();

        //New mesh
        meshFilter.mesh = new Mesh();
        meshFilter.mesh.CombineMeshes(combine, true, true);
        transform.gameObject.SetActive(true);

        //Set collision mesh
        meshColl.sharedMesh = meshFilter.mesh;

        //Parent container to building
        ret.transform.SetParent(neonSignHorizontal.transform);
        ret.transform.position = Vector3.zero;
        ret.transform.eulerAngles = Vector3.zero;

        //Set to block rain
        ret.layer = 29;

        //Set shader
        MeshRenderer rend = ret.GetComponent<MeshRenderer>();
        rend.sharedMaterial = neonMaterial.regularMat;
        rend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off; //No need to cast shadows

        //Set light layer
        Toolbox.Instance.SetLightLayer(meshRenderer, building, true);

        //Static object has no benefit as it's not created in the editor
        meshRenderer.gameObject.isStatic = false; //NOTE: This has no effects in builds!
    }

    //Create a sign for this company
    public void CreateSignageVertical()
    {
        //Game.Log("Create signage vertical...");

        //Remove previous
        if (neonSignVertical != null) Destroy(neonSignVertical);

        if (streetAccess == null) return;
        if (addressPreset.possibleSigns.Count <= 0) return;

        //Get nearby cable link points...
        List<BuildingPreset.CableLinkPoint> bestLinkChoices = new List<BuildingPreset.CableLinkPoint>();
        List<float> bestLinkDistances = new List<float>();

        //Search for cable linking points
        foreach (BuildingPreset.CableLinkPoint link in building.preset.cableLinkPoints)
        {
            //Do raycast to check for other buildings...
            //Check Distance threshold
            Vector3 spawnPoint = building.buildingModelBase.transform.TransformPoint(link.localPos);

            if (spawnPoint.y < 8f) continue;

            float distance = Vector3.Distance(spawnPoint, streetAccess.fromNode.position);

            if(bestLinkChoices.Count <= 0)
            {
                bestLinkChoices.Add(link);
                bestLinkDistances.Add(distance);
            }
            else
            {
                for (int i = 0; i < bestLinkChoices.Count; i++)
                {
                    if (distance < bestLinkDistances[i])
                    {
                        bestLinkChoices.Insert(i, link);
                        bestLinkDistances.Insert(i, distance);
                        break;
                    }
                }
            }
        }

        //We should now have a list of cable spawn points, ranked by proximity to the entrance...
        if (bestLinkChoices.Count <= 0)
        {
            Game.Log("No valid link choices...");
            return; //Escape if no spawn points
        }

        BuildingPreset.CableLinkPoint placementPos = bestLinkChoices[0];

        //Create sign
        neonSignVertical = Instantiate(addressPreset.possibleSigns[neonVerticalIndex], building.buildingModelBase.transform);
        neonSignVertical.transform.localEulerAngles = placementPos.localRot + new Vector3(0, 90, 0);
        neonSignVertical.transform.localPosition = placementPos.localPos;

        //Create materials of the correct colour
        NeonSignController neonController = neonSignVertical.GetComponent<NeonSignController>();

        //Use this for grabbing colour...
        if(neonController != null)
        {
            CityControls.NeonMaterial neonMaterial = CityControls.Instance.neonColours[neonColour];

            for (int i = 0; i < neonController.materialAnimations.Count; i++)
            {
                bool lightBool = neonController.lightBools[i];

                //Only change material if light is on on this frame...
                if(lightBool)
                {
                    neonController.materialAnimations[i] = Instantiate(neonController.materialAnimations[i]) as Material;

                    if(neonController.useAddressColours)
                    {
                        if (neonController.changeBaseColour)
                        {
                            neonController.materialAnimations[i].SetColor("_BaseColor", neonMaterial.neonColour * CityControls.Instance.neonIntensity);
                        }

                        if (neonController.changeAltColour1)
                        {
                            neonController.materialAnimations[i].SetColor("_Color1", neonMaterial.neonColour * CityControls.Instance.neonIntensity);
                        }

                        if (neonController.changeAltColour2)
                        {
                            neonController.materialAnimations[i].SetColor("_Color2", neonMaterial.altColour2 * CityControls.Instance.neonIntensity);
                        }

                        if (neonController.changeAltColour3)
                        {
                            neonController.materialAnimations[i].SetColor("_Color3", neonMaterial.altColour3 * CityControls.Instance.neonIntensity);
                        }
                    }
                }
            }

            //Set light colours
            Light[] lights = neonSignVertical.GetComponentsInChildren<Light>();

            foreach (Light light in lights)
            {
                HDAdditionalLightData hdrpLightData = light.transform.gameObject.GetComponent<HDAdditionalLightData>();
                light.color = neonMaterial.neonColour;
            }
        }

        neonSignVertical.transform.name = this.name;
    }

    //Generate containers needed for the pathfinding job system
    public void GenerateJobPathingData()
    {
        accessRef = new NativeMultiHashMap<float3, int>(0, Allocator.Persistent);
        accessPositions = new NativeHashMap<int, float3>(0, Allocator.Persistent); //Use a access reference to access node positions of the access (for heuristics)
        toNodeReference = new NativeHashMap<int, float3>(0, Allocator.Persistent);
        noPassRef = new NativeList<float3>(0, Allocator.Persistent);

        foreach(NewNode node in nodes)
        {
            //Add reference to no pass through
            if(node.noPassThrough)
            {
                noPassRef.Add(Toolbox.Instance.ToFloat3(node.nodeCoord));
            }

            foreach(KeyValuePair<NewNode, NewNode.NodeAccess> pair in node.accessToOtherNodes)
            {
                if (!pair.Value.walkingAccess) continue; //Skip walking access unless this is the destination node
                if (pair.Value.employeeDoor) continue;
                if (pair.Value.toNode.noPassThrough) continue;

                accessRef.Add(Toolbox.Instance.ToFloat3(node.nodeCoord), pair.Value.id);
                accessPositions.TryAdd(pair.Value.id, CityData.Instance.RealPosToNodeFloat(pair.Value.worldAccessPoint));
                toNodeReference.TryAdd(pair.Value.id, Toolbox.Instance.ToFloat3(pair.Value.toNode.nodeCoord));
            }
        }

        //Add entrances also
        foreach(NewNode.NodeAccess entrance in entrances)
        {
            if (!entrance.walkingAccess) continue;
            if (entrance.employeeDoor) continue;
            if (entrance.toNode.noPassThrough) continue;

            accessRef.Add(Toolbox.Instance.ToFloat3(entrance.fromNode.nodeCoord), entrance.id);
            accessPositions.TryAdd(entrance.id, CityData.Instance.RealPosToNodeFloat(entrance.worldAccessPoint));
            toNodeReference.TryAdd(entrance.id, Toolbox.Instance.ToFloat3(entrance.toNode.nodeCoord));
        }
    }

    //Calculate ownership of rooms
    public void CalculateRoomOwnership()
    {
        //Loop rooms
        foreach(NewRoom room in rooms)
        {
            List<RoomConfiguration> elements = new List<RoomConfiguration>(room.openPlanElements);
            if(!elements.Contains(room.preset)) elements.Add(room.preset);

            foreach(RoomConfiguration roomConfig in elements)
            {
                if (roomConfig.useOwnership)
                {
                    //Add new ownership class reference...
                    if (!roomsBelongTo.ContainsKey(roomConfig.roomType))
                    {
                        roomsBelongTo.Add(roomConfig.roomType, new Dictionary<NewRoom, List<Human>>());
                    }

                    //Make a list of valid inhabitants that need to be assigned on of these types...
                    List<Human> validAssignees = new List<Human>(inhabitants);

                    //Remove already owned rooms from valid list...
                    foreach (KeyValuePair<NewRoom, List<Human>> ownedRooms in roomsBelongTo[roomConfig.roomType])
                    {
                        foreach (Human human in ownedRooms.Value)
                        {
                            validAssignees.Remove(human);
                        }
                    }

                    //Remove non-compatible jobs from valid list
                    if (roomConfig.belongsToJob.Count > 0)
                    {
                        for (int i = 0; i < validAssignees.Count; i++)
                        {
                            Human val = validAssignees[i];

                            if (val.job == null || val.job.preset == null || !roomConfig.belongsToJob.Contains(val.job.preset))
                            {
                                validAssignees.RemoveAt(i);
                                i--;
                            }
                        }
                    }

                    //Now assign to new furniture...
                    Human lastAssignee = null;

                    for (int i = 0; i < roomConfig.assignBelongsToOwners; i++)
                    {
                        if (i < validAssignees.Count)
                        {
                            //Look for couples
                            Human thisAssignee = validAssignees[i];

                            if (lastAssignee != null && roomConfig.preferCouples)
                            {
                                if (lastAssignee.partner != null)
                                {
                                    if (validAssignees.Contains(lastAssignee.partner))
                                    {
                                        thisAssignee = lastAssignee.partner;
                                    }
                                    else break; //If partner isn't in asignees, then break.
                                }
                            }

                            if (thisAssignee != null)
                            {
                                room.AddOwner(thisAssignee);
                                lastAssignee = thisAssignee;
                            }
                        }
                    }
                }
            }
        }
    }

    //Get air vent points
    public void SelectAirVentLocations()
    {
        //Game.Log("Selecting air vent locations for " + this.name);

        if (addressPreset == null)
        {
            //Game.Log("Window: No inhabitant preset for " + name + ": Cannot proceed as we need to know room-specific values for vent placement.");
            return; //Needs data from inhabit preset to do this...
        }

        string pKey = seed;

        int ventCount = Mathf.RoundToInt(Toolbox.Instance.GetPsuedoRandomNumberContained(addressPreset.airVentRange.x, addressPreset.airVentRange.y + 1, pKey, out pKey));

        //Proceed if we need air vents...
        if(ventCount > 0)
        {
            List<AirVentLocation> validLocations = new List<AirVentLocation>();

            foreach(NewRoom room in rooms)
            {
                for (int i = 0; i < room.preset.chanceOfRoofVent; i++)
                {
                    validLocations.Add(new AirVentLocation { room = room, location = AirVent.ceiling });
                }

                for (int i = 0; i < room.preset.chanceOfWallVentLower; i++)
                {
                    validLocations.Add(new AirVentLocation { room = room, location = AirVent.wallLower });
                }

                for (int i = 0; i < room.preset.chanceOfWallVentUpper; i++)
                {
                    validLocations.Add(new AirVentLocation { room = room, location = AirVent.wallUpper });
                }
            }

            //We now have a big list of valid vent locations...
            while(ventCount > 0)
            {
                //break if list is empty
                if (validLocations.Count <= 0) break;

                AirVentLocation chosen = validLocations[Toolbox.Instance.GetPsuedoRandomNumberContained(0, validLocations.Count, pKey, out pKey)];
                chosen.room.AddRandomAirVent(chosen.location);

                //If maximum number of vents are reached, then remove them from valid placements
                if(chosen.room.airVents.Count >= chosen.room.preset.maximumVents)
                {
                    for (int i = 0; i < validLocations.Count; i++)
                    {
                        if(validLocations[i].room == chosen.room)
                        {
                            validLocations.RemoveAt(i);
                            i--;
                        }
                    }
                }

                ventCount--;
            }
        }
    }

    //Pick password: Pick a password, after all possible ones completed
    public void PickPassword()
    {
        //For now, password is always random
        passcode.digits.Clear();

        string pKey = seed;

        passcode.digits.Add(Toolbox.Instance.GetPsuedoRandomNumberContained(0, 10, pKey, out pKey));
        passcode.digits.Add(Toolbox.Instance.GetPsuedoRandomNumberContained(0, 10, pKey, out pKey));
        passcode.digits.Add(Toolbox.Instance.GetPsuedoRandomNumberContained(0, 10, pKey, out pKey));
        passcode.digits.Add(Toolbox.Instance.GetPsuedoRandomNumberContained(0, 10, pKey, out pKey));

        //EvidenceLinkData passwordLink = new EvidenceLinkData((passcode.digits[0].ToString() + passcode.digits[1].ToString() + passcode.digits[2].ToString() + passcode.digits[3].ToString()));

        //Employees will have the note written down at home
        if (company != null)
        {
            //foreach(Human hu in owners)
            //{
            //    if (!hu.dataSource.ContainsKey("companypassword"))
            //    {
            //        hu.dataSource.Add("companypassword", passwordLink);
            //    }
            //}

            //There will be a note from the owners to others about the keycode
            if (passcode.used && owners.Count > 0)
            {
                Interactable newNote = owners[0].WriteNote(Human.NoteObject.note, "65ad8237-a50a-4fbe-95c0-1c14de71912c", owners[0], this, 0, InteractablePreset.OwnedPlacementRule.ownedOnly, 3);

                if(newNote != null)
                {
                    passcode.notes.Add(newNote.id);
                }
            }

            foreach (Occupation job in company.companyRoster)
            {
                if(job.employee != null)
                {
                    //if (!job.employee.dataSource.ContainsKey("companypassword"))
                    //{
                    //    job.employee.dataSource.Add("companypassword", passwordLink);
                    //}

                    if (passcode.used && job.employee.home != null)
                    {
                        Interactable newNote = job.employee.WriteNote(Human.NoteObject.note, "1e7df673-89e6-45b0-8e23-1cff0bbca97f", job.employee, job.employee.home, 1, InteractablePreset.OwnedPlacementRule.both, 2);

                        if (newNote != null)
                        {
                            passcode.notes.Add(newNote.id);
                        }
                    }
                }
            }
        }
        else
        {
            //foreach(Human human in owners)
            //{
            //    if (!human.dataSource.ContainsKey("residentspassword"))
            //    {
            //        human.dataSource.Add("residentspassword", passwordLink);
            //    }
            //}

            //There will be a note from the owners to others about the keycode
            if (passcode.used && owners.Count > 0)
            {
                Interactable newNote = owners[0].WriteNote(Human.NoteObject.note, "65ad8237-a50a-4fbe-95c0-1c14de71912c", owners[0], this, 1, InteractablePreset.OwnedPlacementRule.both, 3);

                if (newNote != null)
                {
                    passcode.notes.Add(newNote.id);
                }
            }
        }
    }

    //Activate alarm
    public void SetAlarm(bool newVal, Human target)
    {
        if(addressPreset != null && building != null && !addressPreset.useOwnSecuritySystem)
        {
            building.SetAlarm(newVal, target, floor);
            return;
        }

        if (alarmActive != newVal)
        {
            alarmActive = newVal;

            if (alarmActive)
            {
                if (!GameplayController.Instance.activeAlarmsLocations.Contains(this))
                {
                    GameplayController.Instance.activeAlarmsLocations.Add(this);
                }

                //Respond to alarm
                foreach (Actor a in currentOccupants)
                {
                    if (a.isPlayer) continue;
                    if (a.isAsleep || a.isDead || a.isStunned || a.ai == null) continue;

                    if (a.locationsOfAuthority.Contains(this))
                    {
                        a.OnInvestigate(a, 1);
                    }

                    a.AddNerve(CitizenControls.Instance.nerveAlarm);
                }
            }
            else
            {
                alarmTimer = 0f;

                GameplayController.Instance.activeAlarmsLocations.Remove(this);

                if (building != null && addressPreset.alarmLocksDownFloor)
                {
                    foreach (KeyValuePair<int, NewFloor> pair in building.floors)
                    {
                        pair.Value.SetAlarmLockdown(false);
                    }
                }

                alarmTargets.Clear();
            }

            if (Player.Instance.currentGameLocation == this) StatusController.Instance.ForceStatusCheck();

            foreach (Interactable i in alarms)
            {
                //i.SetSwitchState(alarmActive, null);
                i.SetCustomState1(alarmActive, null);
            }

            foreach (Interactable i in sentryGuns)
            {
                //i.SetSwitchState(alarmActive, null);
                i.SetCustomState1(alarmActive, null);
            }

            foreach (Interactable i in otherSecurity)
            {
                i.SetCustomState1(alarmActive, null);
            }

            //Forcing this update also updates the trespassing rules for the player
            if (Player.Instance.currentBuilding == this)
            {
                Player.Instance.OnRoomChange();
            }
        }

        if (alarmActive)
        {
            if (target != null)
            {
                Game.Log("Adding alarm target " + target.name);

                if (target.isPlayer)
                {
                    SessionData.Instance.TutorialTrigger("alarms");
                    if (building != null) StatusController.Instance.SetWantedInBuilding(building, GameplayControls.Instance.buildingWantedTime);
                }

                if (!alarmTargets.Contains(target))
                {
                    alarmTargets.Add(target);
                }
            }

            //Reset alarm timer
            alarmTimer = GetAlarmTime();

            //Place the floor under lockdown
            if (building != null && addressPreset.alarmLocksDownFloor && floor != null)
            {
                floor.SetAlarmLockdown(true);
            }
        }
    }

    public float GetAlarmTime()
    {
        return Mathf.Lerp(GameplayControls.Instance.buildingAlarmTime.x, GameplayControls.Instance.buildingAlarmTime.y, 0);
    }

    public override bool IsAlarmSystemTarget(Human human)
    {
        if (human == null) return false;
        if (human.isDead || human.isStunned) return false;

        if (!addressPreset.useOwnSecuritySystem && building != null)
        {
            return building.IsAlarmSystemTarget(human);
        }

        if (targetMode == NewBuilding.AlarmTargetMode.notPlayer)
        {
            if (!human.isPlayer)
            {
                return true;
            }
        }
        else if (targetMode == NewBuilding.AlarmTargetMode.everybody)
        {
            return true;
        }
        else if (targetMode == NewBuilding.AlarmTargetMode.illegalActivities)
        {
            if (alarmTargets.Contains(human) || (building != null & human.isPlayer && SessionData.Instance.gameTime < building.wantedInBuilding))
            {
                return true;
            }
        }
        else if (targetMode == NewBuilding.AlarmTargetMode.nobody)
        {
            return false;
        }
        else if (targetMode == NewBuilding.AlarmTargetMode.nonResidents)
        {
            if (!inhabitants.Contains(human))
            {
                return false;
            }
            else return true;
        }

        return false;
    }

    public override bool IsAlarmActive(out float retAlarmTimer, out NewBuilding.AlarmTargetMode retTargetMode, out List<Human> retTargets)
    {
        retAlarmTimer = 0f;
        retTargetMode = NewBuilding.AlarmTargetMode.illegalActivities;
        retTargets = null;

        if(building != null && addressPreset != null && !addressPreset.useOwnSecuritySystem)
        {
            retAlarmTimer = building.alarmTimer;
            retTargetMode = building.targetMode;
            retTargets = building.alarmTargets;
            return building.alarmActive;
        }
        else
        {
            retAlarmTimer = alarmTimer;
            retTargetMode = targetMode;
            retTargets = alarmTargets;
            return alarmActive;
        }
    }

    //Add sentry gun
    public void AddSentryGun(Interactable newInteractable)
    {
        sentryGuns.Add(newInteractable);
    }

    //Add other security
    public void AddOtherSecurity(Interactable newInteractable)
    {
        otherSecurity.Add(newInteractable);

        if(company != null)
        {
            newInteractable.SetCustomState2(!company.openForBusinessDesired, null, forceUpdate: true);
        }
        else
        {
            newInteractable.SetCustomState2(true, null, forceUpdate: true);
        }
    }

    public void SetBreakerSecurity(Interactable newObject)
    {
        breakerSecurity = newObject;
        if (breakerSecurity != null) breakerSecurityID = breakerSecurity.id;
    }

    public void SetBreakerLights(Interactable newObject)
    {
        breakerLights = newObject;
        if (breakerLights != null) breakerLightsID = breakerLights.id;
    }

    public void SetBreakerDoors(Interactable newObject)
    {
        breakerDoors = newObject;
        if (breakerDoors != null) breakerDoorsID = breakerDoors.id;
    }

    public override bool IsOutside()
    {
        if (isOutside) return true;
        if (isOutsideAddress) return true;

        return false;
    }

    public Interactable GetBreakerSecurity()
    {
        if (breakerSecurity != null) return breakerSecurity;
        else if(breakerSecurityID > -1)
        {
            breakerSecurity = CityData.Instance.interactableDirectory.Find(item => item.id == breakerSecurityID);
        }

        if (breakerSecurity != null) return breakerSecurity;

        if(floor != null)
        {
            return floor.GetBreakerSecurity();
        }

        return null;
    }

    public Interactable GetBreakerLights()
    {
        if (breakerLights != null) return breakerLights;
        else if (breakerLightsID > -1)
        {
            breakerLights = CityData.Instance.interactableDirectory.Find(item => item.id == breakerLightsID);
        }

        if (breakerLights != null) return breakerLights;

        if (floor != null)
        {
            return floor.GetBreakerLights();
        }

        return null;
    }

    public Interactable GetBreakerDoors()
    {
        if (breakerDoors != null) return breakerDoors;
        else if (breakerDoorsID > -1)
        {
            breakerDoors = CityData.Instance.interactableDirectory.Find(item => item.id == breakerDoorsID);
        }

        if (breakerDoors != null) return breakerDoors;

        if (floor != null)
        {
            return floor.GetBreakerDoors();
        }

        return null;
    }

    //Dispose of job containers when disabled
    private void OnDisable()
    {
        if(Game.Instance.useJobSystem)
        {
            try
            {
                accessRef.Dispose();
                accessPositions.Dispose();
                toNodeReference.Dispose();
                noPassRef.Dispose();
            }
            catch
            {

            }
        }
    }

    [Button]
    public void IsThisOpen()
    {
        if(company != null)
        {
            Game.Log("Is " + name + " Open? Actual: " + company.openForBusinessActual + " desired: " + company.openForBusinessDesired + " open calc: " + company.IsOpenAtThisTime(SessionData.Instance.gameTime, SessionData.Instance.decimalClock, SessionData.Instance.day));

            float nextOpen = SessionData.Instance.GetNextOrPreviousGameTimeForThisHour(SessionData.Instance.gameTime, SessionData.Instance.decimalClock, SessionData.Instance.day, company.daysOpen, company.retailOpenHours.x, company.retailOpenHours.y);
            Game.Log("Next open " + nextOpen + ": " + SessionData.Instance.GameTimeToClock12String(nextOpen, false) + ", " + SessionData.Instance.ShortDateString(nextOpen, false));
            Game.Log("Open hours: " + company.retailOpenHours);
        }
    }

    [Button]
    public void SetPlayerResidence()
    {
        if(residence != null)
        {
            PlayerApartmentController.Instance.BuyNewResidence(residence);
        }
    }

    public void AddVandalism(Interactable interactable)
    {
        if (interactable == null) return;

        if(!vandalism.Exists(item => item.obj == interactable.id))
        {
            Game.Log("Adding vandalism for object " + interactable.name + " at " + name);
            vandalism.Add(new Vandalism { obj = interactable.id, fine = Mathf.RoundToInt(interactable.val * GameplayControls.Instance.vandalismFineMultiplier), time = SessionData.Instance.gameTime });
            SideJobObjectiveCheck();
        }
    }

    public void AddVandalism(Vector3 window)
    {
        if (!vandalism.Exists(item => item.win == window))
        {
            Game.Log("Adding vandalism for window " + window + " at " + name);
            vandalism.Add(new Vandalism { win = window, fine = GameplayControls.Instance.breakingWindowsFine, time = SessionData.Instance.gameTime });
            SideJobObjectiveCheck();
        }
    }

    //Manually add damage (will only get removed on timer)
    public void AddVandalism(int fine)
    {
        Game.Log("Adding vandalism " + fine + " at " + name);
        vandalism.Add(new Vandalism { fine = GameplayControls.Instance.breakingWindowsFine, time = SessionData.Instance.gameTime });
        SideJobObjectiveCheck();
    }

    private void SideJobObjectiveCheck()
    {
        //Manual trigger for causing vandal damage
        foreach (Case c in CasePanelController.Instance.activeCases)
        {
            List<Case.ResolveQuestion> vandal = c.resolveQuestions.FindAll(item => item.revengeObjLoc == id);

            foreach (Case.ResolveQuestion q in vandal)
            {
                if(!q.completedRevenge)
                {
                    if(q.UpdateCorrect(c, false))
                    {
                        Game.Log("Jobs: Manually complete vandalism objective");
                        q.completedRevenge = true; //Manually trigger completion here
                    }
                }
            }
        }
    }

    public void RemoveVandalism(Interactable interactable)
    {
        if (interactable == null) return;

        int rem = vandalism.FindIndex(item => item.obj == interactable.id);
        if (rem > -1) vandalism.RemoveAt(rem);
    }

    public void RemoveVandalism(Vector3 window)
    {
        int rem = vandalism.FindIndex(item => item.win == window);
        if (rem > -1) vandalism.RemoveAt(rem);
    }

    public int GetVandalismDamage(bool includeObjects = true, bool includeWindows = true, bool includeMisc = true)
    {
        int ret = 0;

        foreach(Vandalism v in vandalism)
        {
            if (!includeObjects && v.obj > -1) continue;
            if (!includeWindows && v.win.y > -100) continue;
            if (!includeMisc && v.obj <= -1 && v.win.y <= -100) continue;

            ret += v.fine;
        }

        return ret;
    }

    public override void AddOccupant(Actor newOcc)
    {
        base.AddOccupant(newOcc);

        //Turn off additional security
        if (otherSecurity.Count > 0)
        {
            if (currentOccupants.Exists(item => item.locationsOfAuthority.Contains(this)))
            {
                foreach (Interactable i in otherSecurity)
                {
                    i.SetCustomState2(false, null, forceUpdate: true);
                }
            }
        }

        //Add a bit of footstep dirt...
        //if(residence != null || company != null)
        //{
        //    Human h = newOcc as Human;

        //    if(h != null)
        //    {
        //        h.footstepDirt += 0.2f;
        //    }
        //}
    }

    public override void RemoveOccupant(Actor remOcc)
    {
        base.RemoveOccupant(remOcc);

        //Turn on additional security
        if (otherSecurity.Count > 0 && (company == null || !company.openForBusinessDesired))
        {
            if(!currentOccupants.Exists(item => item.locationsOfAuthority.Contains(this)))
            {
                foreach (Interactable i in otherSecurity)
                {
                    i.SetCustomState2(true, null, forceUpdate: true);
                }
            }
        }
    }

    //Password is generated with seed so no data needs to be saved, and it should be compatible across languages
    public string GetPassword()
    {
        if (addressPreset == null || !addressPreset.needsPassword) return string.Empty;

        string seed = CityData.Instance.seed + addressPreset.name; //Unique password per address preset

        string pickName = addressPreset.dictionaryPasswordSources[Toolbox.Instance.GetPsuedoRandomNumber(0, addressPreset.dictionaryPasswordSources.Count, seed)];

        string passw = NameGenerator.Instance.GenerateName("", 0f, pickName, 1f, "", 0f, useCustomSeed: seed);
        Game.Log("Gameplay: Address password for " + name + " is: " + passw);

        return passw;
    }
}
