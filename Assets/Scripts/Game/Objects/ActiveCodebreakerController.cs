﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ActiveCodebreakerController : MonoBehaviour
{
    public InteractableController controller;
    public TextMeshPro text;
    public bool cracked = false;
    public MeshRenderer rend;
    public List<Material> activeMaterials = new List<Material>();

    // Update is called once per frame
    void Update()
    {
        if(!cracked)
        {
            string str = Mathf.RoundToInt(controller.interactable.cs).ToString();

            if(str.Length == 0)
            {
                str = "0000";
            }
            else if (str.Length == 1)
            {
                str = "000" + str;
            }
            else if (str.Length == 2)
            {
                str = "00" + str;
            }
            else if (str.Length == 3)
            {
                str = "0" + str;
            }

            text.text = str;

            rend.sharedMaterial = activeMaterials[Toolbox.Instance.Rand(0, activeMaterials.Count)];
        }
    }

    public void OnCrack(string codeStr)
    {
        cracked = true;
        rend.sharedMaterial = activeMaterials[0];
        text.text = codeStr;
        text.color = Color.green;
    }
}
