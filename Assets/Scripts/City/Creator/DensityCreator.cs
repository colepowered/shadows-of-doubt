﻿using UnityEngine;
using System.Collections;

//Generates density within the city
//Script pass 1
public class DensityCreator : Creator
{
	//Singleton pattern
	private static DensityCreator _instance;
    public static DensityCreator Instance { get { return _instance; } }

	void Awake ()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
	}

	public override void StartLoading()
	{
        Game.Log("CityGen: Setup city density...");
        //General pattern- the closer to the city centre, the higher chance of dense population.

        //Create a scale, choose the shortest city side and half it
        float cityRadius = Mathf.Min(CityData.Instance.citySize.x, CityData.Instance.citySize.y) * 0.5f;

        CityTile centreTile = CityData.Instance.cityTiles[new Vector2Int (Mathf.RoundToInt(CityData.Instance.citySize.x * 0.5f), Mathf.RoundToInt(CityData.Instance.citySize.y * 0.5f))];

        string seed = CityData.Instance.seed;

		//assign density to all tiles
		for(int i = 0; i < CityData.Instance.citySize.x; i++)
		{
			for(int n = 0; n < CityData.Instance.citySize.y; n++)
			{
                CityTile thisTile = null;

                if(CityData.Instance.cityTiles.TryGetValue(new Vector2Int(i, n), out thisTile))
                {
                    //Calculate distance from centre
                    float dist = Vector2.Distance(centreTile.cityCoord, thisTile.cityCoord);

                    //Distance multiplier from radius border to centre
                    float distMP = Mathf.Clamp(1.0f - dist / cityRadius, 0f, 1f);

                    //Density value
                    int density = Mathf.CeilToInt(distMP * 3.0f);

                    //Assign base land value (= density)
                    thisTile.SetLandVlaue((BuildingPreset.LandValue)Mathf.Clamp(density, 0, 4));

                    //Assign density noise based on grouping.
                    //On rural tiles, 50% chance of upping to light desnsity
                    if (density == 0)
                    {
                        if (Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, seed, out seed) <= 50)
                        {
                            density++;
                        }
                    }
                    //Deliberate cascade possibility from rural > light > medium!
                    if (density == 1)
                    {
                        if (Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, seed, out seed) <= 20)
                        {
                            density++;
                        }
                        if (Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, seed, out seed) <= 5)
                        {
                            density--;
                        }
                    }
                    else if (density == 2) //Else so no skyscrapers in rural areas!
                    {
                        if (Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, seed, out seed) <= 5)
                        {
                            density++;
                        }
                        if (Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, seed, out seed) <= 5)
                        {
                            density--;
                        }
                    }

                    thisTile.SetDensity((BuildingPreset.Density)Mathf.Clamp(density, 0, 3));
                }
			}
		}

		//Calculate average density for all blocks.
        foreach(BlockController block in CityData.Instance.blocksDirectory)
        {
            block.UpdateAverageDensity();
        }

        //This can be loaded pretty much instantly
        SetComplete();
	}
}
