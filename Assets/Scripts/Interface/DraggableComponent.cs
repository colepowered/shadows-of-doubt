﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

//Controller for a draggable component of the UI.
//Script pass 1
public class DraggableComponent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	//References
	private RectTransform thisRect;
    public bool isDragging = false;
    public bool isOver = false;
    public GameObject dragObject;
    private GameObject spawnedObject;
    private RectTransform rect;
	public Vector2 pointerOffset;

    private Vector2 originalClickPoint;
    private bool recClick = false;
    private float dragThresholdCheck = 0f;

	//Settings
	public GameObject objectOverride = null;
	public string dragTag = string.Empty;

	//Events
	public delegate void DragEnd(GameObject dragObj, string tag);
    public event DragEnd OnDragEnd;

	// Use this for initialization
	void Start()
	{
		thisRect = this.gameObject.GetComponent<RectTransform>();
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        isOver = true;
        StartCoroutine("MouseOver");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;
        isOver = false;
    }

    IEnumerator MouseOver()
    {
        while(isOver)
        {
            if(Input.GetMouseButton(0))
            {
                if(!recClick)
                {
                    //Record original mouse click position
                    originalClickPoint = Input.mousePosition;
                    recClick = true;
                    dragThresholdCheck = 0f;
                }
                else
                {
                    dragThresholdCheck = Mathf.Abs(Input.mousePosition.x - originalClickPoint.x) + Mathf.Abs(Input.mousePosition.y - originalClickPoint.y);
                }

                if (!isDragging && dragThresholdCheck > 10f)
                {
                    if(InterfaceController.Instance.dragged == null)
                    {
                        EndDrag();

                        //New Drag
                        StartCoroutine("Drag");
                    }
                }
            }
            else
            {
                recClick = false;
            }

            yield return null;
        }
    }

	IEnumerator Drag()
	{
		isDragging = true;

		//Set the dragging object
		GameObject draggingObj = this.gameObject;
		if (objectOverride != null) draggingObj = objectOverride;

        //Get offset of mouse position
        RectTransformUtility.ScreenPointToLocalPointInRectangle(thisRect, Input.mousePosition, null, out pointerOffset);

        //Game.Log("set drag object to " + draggingObj);
        InterfaceController.Instance.SetDragged(draggingObj, dragTag, pointerOffset);

		//Create drag object
        //Spawn as child of this, then transfer to hud
		spawnedObject = Instantiate(dragObject, thisRect.parent, false);
		rect = spawnedObject.GetComponent<RectTransform>();

        //Copy dimensions from gameobject's rect
        rect.localScale = thisRect.localScale;
        rect.sizeDelta = thisRect.sizeDelta;
        rect.offsetMax = thisRect.offsetMax;
        rect.offsetMin = thisRect.offsetMin;
        rect.anchorMax = thisRect.anchorMax;
        rect.anchorMin = thisRect.anchorMin;
        rect.pivot = thisRect.pivot;
        rect.position = thisRect.position;

        //Set parent to hud level
        rect.SetParent(InterfaceControls.Instance.hudCanvasRect, true);
        rect.SetAsLastSibling();

        //Screen Capture Method

        ////Get texture
        //RawImage rawImg = spawnedObject.GetComponent<RawImage>();

        ////Convert rect to screen space
        //Vector2 size = Vector2.Scale(rect.rect.size, rect.lossyScale);
        //Rect screenRect  = new Rect((Vector2)rect.position - (size * 0.5f), size);

        ////Size in pixels
        //Vector2 pixelSize = new Vector2(screenRect.size.x * Screen.width, screenRect.size.y * Screen.height);

        ////Create texture of size
        //Texture2D tex = new Texture2D(Mathf.CeilToInt(pixelSize.x), Mathf.CeilToInt(pixelSize.y), TextureFormat.RGB24, false);

        //tex.ReadPixels(screenRect, 0, 0);
        //tex.Apply();

        //rawImg.texture = tex;

        //Copy object method

        while (Input.GetMouseButton(0))
		{
			SetPosition(Input.mousePosition, pointerOffset);

			yield return null;
		}

		EndDrag();
	}

    void OnDestory()
    {
        EndDrag();
    }

    void OnDisable()
    {
        EndDrag();
    }

	public void EndDrag()
	{
		//Fire Event   	
	    if(OnDragEnd != null)
		OnDragEnd(InterfaceController.Instance.dragged, InterfaceController.Instance.draggedTag);

		InterfaceController.Instance.SetDragged(null, "", Vector2.zero);
		Destroy(spawnedObject);
		StopCoroutine("Drag");
		isDragging = false;

        recClick = false;
	}

	public void SetPosition(Vector2 pointerPosition, Vector2 offset)
	{
		Vector2 localPointerPosition;

		if (RectTransformUtility.ScreenPointToLocalPointInRectangle (
			InterfaceControls.Instance.hudCanvasRect, pointerPosition, null, out localPointerPosition
		))
		{
			rect.localPosition = localPointerPosition - offset;
		}
	}
}
