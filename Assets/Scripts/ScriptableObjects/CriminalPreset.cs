﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "criminal_data", menuName = "Database/Criminal")]

public class CriminalPreset : SoCustomComparison
{
	//Type
	public enum CriminalType {serialKiller};
	public CriminalType type;

	public bool canBeAgent = false;

	public bool canHaveJob = true;

	//Boss
	public int suggestedRank;
	public CriminalPreset boss;

	//The number of this kind of job in the company (random range).
	public int positionsMin = 1;
	public int positionsMax = 1;

    //Desired amount of crime hours per day
    public float desiredCrimePerDay = 1.5f;
}
