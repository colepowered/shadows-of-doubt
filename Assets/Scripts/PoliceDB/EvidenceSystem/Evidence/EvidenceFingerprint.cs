﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.PlayerLoop;
using System.Text;

public class EvidenceFingerprint : Evidence
{
    //bool ownerFound = false;

    //Constructor
    public EvidenceFingerprint(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {

    }

    public override void OnDiscovery()
    {
        base.OnDiscovery();

        //Listen for key changes in citizen
        if (writer != null && writer.evidenceEntry != null)
        {
            OnCitizensDataKeyChange(); //Force update to trigger merge
            writer.evidenceEntry.OnDataKeyChange += OnCitizensDataKeyChange;
        }

        //Manually create on/at facts...
        //To be 'on' the fingerprint must be on an interactable and that interactable must have evidence...
        if (interactable != null && interactable.print != null && interactable.print.Count > 0)
        {
            Interactable printInteractable = null;

            if(CityData.Instance.savableInteractableDictionary.TryGetValue(interactable.print[0].interactableID, out printInteractable))
            {
                if(printInteractable.evidence != null)
                {
                    Game.Log("Evidence: Fingerprint creating 'found on' evidence...");
                    EvidenceCreator.Instance.CreateFact("FoundOn", this, printInteractable.evidence, forceDiscoveryOnCreate: true);
                }
                else
                {
                    Game.Log("Evidence: Fingerprint creating 'found at' evidence...");
                    EvidenceCreator.Instance.CreateFact("FoundAt", this, parent, forceDiscoveryOnCreate: true);
                }
            }
            else
            {
                Game.Log("Evidence: Fingerprint creating 'found at' evidence...");
                EvidenceCreator.Instance.CreateFact("FoundAt", this, parent, forceDiscoveryOnCreate: true);
            }
        }
        else
        {
            Game.Log("Evidence: Fingerprint creating 'found at' evidence...");
            EvidenceCreator.Instance.CreateFact("FoundAt", this, parent, forceDiscoveryOnCreate: true);
        }
    }

    //Override data sources: This needs it's own, not the owners like in EvidenceItem
    public override void BuildDataSources()
    {
        //base.BuildDataSources();
        UpdateSummary();
    }

    public void UpdateSummary()
    {
        //Refresh window content to update summary text
        List<InfoWindow> windows = InterfaceController.Instance.activeWindows.FindAll(item => item.passedEvidence == this);

        foreach(InfoWindow w in windows)
        {
            foreach(WindowTabController t in w.tabs)
            {
                if(w.passedEvidence != null && t.preset.contentType == WindowTabPreset.TabContentType.generated)
                {
                    t.content.LoadContent();
                }
            }
        }
    }

    //Override name variants
    public override string GetNameForDataKey(List<DataKey> inputKeys)
    {
        string ret = string.Empty;

        try
        {
            if (inputKeys == null) return Strings.Get("evidence.names", preset.name);

            List<DataKey> keys = GetTiedKeys(inputKeys);

            //Return custom names first
            if (customNames.Count > 0)
            {
                foreach (CustomName n in customNames)
                {
                    if (keys.Contains(n.key))
                    {
                        return n.name;
                    }
                }
            }

            ret = Strings.Get("evidence.names", preset.name);

            List<DataKey> nameKeys = GetTiedKeys(DataKey.name);

            //Return name
            if (keys.Contains(DataKey.name) || nameKeys.Exists(item => inputKeys.Contains(item)) && writer != null)
            {
                ret += " (" + writer.GetCitizenName() + ")";
            }
            else
            {
                if(writer != null)
                {
                    //Evidence type name
                    ret += " (" + Strings.Get("evidence.generic", "Type", Strings.Casing.firstLetterCaptial) + " " + Toolbox.Instance.ToBase26(writer.fingerprintLoop) + ")";
                }
                else
                {
                    Game.LogError("Missing writer for fingerprint " + evID);
                }
            }

            return ret;
        }
        catch
        {
            return ret;
        }
    }

    public void OnCitizensDataKeyChange()
    {
        List<Evidence.DataKey> tiedToName = writer.evidenceEntry.GetTiedKeys(DataKey.name);

        if (tiedToName.Contains(DataKey.fingerprints))
        {
            Game.Log("Evidence: Fingerprint's owner (" + writer.GetCitizenName() + ") has prints and name merged...");
            MergeDataKeys(DataKey.fingerprints, DataKey.name);
            //ownerFound = true;
            UpdateSummary();
            if(interactable != null) interactable.UpdateName();
        }
        else
        {
            Game.Log("Evidence: Fingerprint's owner (" + writer.GetCitizenName() + ") does not contain name...");
            //ownerFound = false;
        }

        if (interactable != null)
        {
            //Force interactable name
            //Game.Log("Force prints interactable name: " + GetNameForDataKey(DataKey.fingerprints));
            interactable.UpdateName(true, DataKey.fingerprints);
        }
        else Game.Log("No print interactable");
    }

    //Get note with text composed with the data reference from this evidence
    public override string GetNoteComposed(List<Evidence.DataKey> keys, bool useLinks = true)
    {
        Strings.LinkSetting links = Strings.LinkSetting.forceNoLinks;
        if (useLinks) links = Strings.LinkSetting.forceLinks;
        return Strings.ComposeText(GetNote(keys), writer, links, evidenceKeys: keys);
    }

    //Return a list of know info 
    public override string GetNote(List<DataKey> keys)
    {
        StringBuilder sb = new StringBuilder();

        List<DataKey> tiedKeys = GetTiedKeys(keys);

        string checkboxEmpty = "<sprite=\"icons\" name=\"Checkbox Empty\">";
        string checkboxChecked = "<sprite=\"icons\" name=\"Checkbox Checked\">";
        string handwritingFont = "<font=\"PapaManAOE SDF\">";
        string fontEnd = "</font>";

        string checkbox = string.Empty;

        //Blueprints/type
        if (tiedKeys.Contains(DataKey.fingerprints)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append(checkbox + Strings.Get("descriptors", "Type", Strings.Casing.firstLetterCaptial) + ": ");

        if (tiedKeys.Contains(DataKey.fingerprints) && writer != null)
        {
            sb.Append(handwritingFont + Toolbox.Instance.ToBase26(writer.fingerprintLoop) + fontEnd);
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);

        //Location
        if (tiedKeys.Contains(DataKey.name)) checkbox = checkboxChecked;
        else checkbox = checkboxEmpty;

        sb.Append("\n" + checkbox + Strings.Get("descriptors", "Belongs To", Strings.Casing.firstLetterCaptial) + ": ");
        if (tiedKeys.Contains(DataKey.name))
        {
            sb.Append(handwritingFont + "|name|" + fontEnd);
        }
        else sb.Append(handwritingFont + Strings.Get("descriptors", "?") + fontEnd);


        return sb.ToString();
    }
}
