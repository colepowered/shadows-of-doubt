using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaCollider : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        Player getPlayer = other.GetComponent<Player>();

        if(getPlayer != null)
        {
            Game.Log("PLAYER IS IN SEA");
            getPlayer.RecieveDamage(999999, null, Vector2.zero, Vector2.zero, null, null);
            InterfaceController.Instance.Fade(1f, 0.0001f);
        }
        else
        {
            InteractableController getInteractable = other.gameObject.GetComponentInParent<InteractableController>();

            if (getInteractable != null)
            {
                Game.Log(getInteractable.interactable.GetName() + " IN SEA");
                getInteractable.interactable.SafeDelete();
            }
            else
            {
                getInteractable = other.gameObject.GetComponentInChildren<InteractableController>();

                if (getInteractable != null)
                {
                    Game.Log(getInteractable.interactable.GetName() + " IN SEA");
                    getInteractable.interactable.SafeDelete();
                }
            }
        }
    }
}
