﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class EvidenceButtonController : ButtonController, IPointerDownHandler
{
    //Buttons refers to this
    public PinnedItemController pinnedController;
    public Evidence evidence;
    public List<Evidence.DataKey> evidenceKeys;
    public RawImage evPhoto;

    public virtual void Setup(Evidence newEvidence, List<Evidence.DataKey> newKeys, PinnedItemController newController)
    {
        SetupReferences();

        pinnedController = newController;
        evidence = newEvidence;
        evidenceKeys = newKeys;

        UpdateTooltipText();
        VisualUpdate();

        CityConstructor.Instance.OnGameStarted += VisualUpdate;

        if (evidence != null)
        {
            //Listen for evidence changing data keys
            evidence.OnDataKeyChange += VisualUpdate;

            //Listen for evidence name change
            evidence.OnNewName += UpdateTooltipText;

            //Change to sticky note background
            if(evidence.preset.pinnedStyle == EvidencePreset.PinnedStyle.stickNote)
            {
                background.sprite = InterfaceControls.Instance.stickyNoteButtonSprite;
            }
        }

        ExtraSetup();
    }

    void OnDestroy()
    {
        CityConstructor.Instance.OnGameStarted -= VisualUpdate;

        if(evidence != null)
        {
            evidence.OnDataKeyChange -= VisualUpdate;

            //Listen for evidence name change
            evidence.OnNewName -= UpdateTooltipText;
        }
    }

    public virtual void ExtraSetup()
    {
        return;
    }

    public override void OnLeftClick()
    {
        //Pick mode
        if (CasePanelController.Instance.pickModeActive && CasePanelController.Instance.pickForField != null)
        {
            EvidenceCitizen cit = pinnedController.evidence as EvidenceCitizen;

            if (cit != null && CasePanelController.Instance.pickForField.question.inputType == Case.InputType.citizen && cit.GetTiedKeys(pinnedController.caseElement.dk).Contains(Evidence.DataKey.name))
            {
                CasePanelController.Instance.pickForField.OnPick(pinnedController.evidence, pinnedController.caseElement.dk);
                pinnedController.ForcePointerUp();
                return;
            }
            else
            {
                EvidenceLocation loc = pinnedController.evidence as EvidenceLocation;

                if (loc != null && CasePanelController.Instance.pickForField.question.inputType == Case.InputType.location)
                {
                    CasePanelController.Instance.pickForField.OnPick(pinnedController.evidence, pinnedController.caseElement.dk);
                    pinnedController.ForcePointerUp();
                    return;
                }
                else if (CasePanelController.Instance.pickForField.question.inputType == Case.InputType.item)
                {
                    CasePanelController.Instance.pickForField.OnPick(pinnedController.evidence, pinnedController.caseElement.dk);
                    pinnedController.ForcePointerUp();
                    return;
                }
            }
        }
        else
        {
            Case.CaseElement forcedElement = null;

            if(pinnedController != null)
            {
                forcedElement = pinnedController.caseElement;
            }

            InterfaceController.Instance.SpawnWindow(evidence, passedEvidenceKeys: evidenceKeys, passedInteractable: evidence.interactable, forcedPinnedElement: forcedElement);
            pinnedController.ForcePointerUp();
        }
    }

    public override void OnHoverStart()
    {
        base.OnHoverStart();

        //Remove active quick menu on hover of evidence button
        if(pinnedController != null)
        {
            if (PinnedItemController.activeQuickMenu != null)
            {
                PinnedItemController.activeQuickMenu.Remove(true);
            }
        }
    }

    public override void OnPointerDown(PointerEventData data)
    {
        //If this is part of a pinned evidence item, pass this directly to the dragcasepanel. For some reason, it doesn't register there...
        pinnedController.dragController.OnPointerDown(data);
        pinnedController.OnPointerDown(data);
    }

    public override void VisualUpdate()
    {
        //Use evidence details
        if (evidence != null)
        {
            evidenceKeys = evidence.GetTiedKeys(evidenceKeys);

            Texture tex = evidence.GetPhoto(evidenceKeys);

            if (icon != null) icon.gameObject.SetActive(false);

            if (evPhoto != null)
            {
                evPhoto.texture = tex;

                if (tex == null)
                {
                    if (evPhoto != null)
                    {
                        evPhoto.color = Color.clear;
                        evPhoto.gameObject.SetActive(false);
                    }
                }
                else
                {
                    evPhoto.color = Color.white;
                    evPhoto.gameObject.SetActive(true);
                }
            }
        }
        else if(parentWindow != null)
        {
            if (icon != null) icon.sprite = parentWindow.iconLarge;

            if (icon != null) icon.gameObject.SetActive(true);
            if (evPhoto != null) evPhoto.gameObject.SetActive(false);
        }

        UpdateTooltipText();
    }

    public override void UpdateTooltipText()
    {
        if (tooltip == null) return;

        if (evidence != null && evidenceKeys != null)
        {
            tooltip.mainText = evidence.GetNameForDataKey(evidenceKeys);
        }
    }
}
