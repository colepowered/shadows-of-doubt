﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DesktopIconController : ComputerOSUIComponent
{
    public DesktopApp desktop;
    public CruncherAppPreset preset;
    public RectTransform rect;
    public Image icon;
    public TextMeshProUGUI iconText;

    public void Setup(DesktopApp newDesktop, CruncherAppPreset newApp)
    {
        desktop = newDesktop;
        preset = newApp;
        icon.sprite = preset.desktopIcon;
        iconText.text = Strings.Get("computer", preset.name);
    }

    public override void OnLeftClick()
    {
        desktop.OnDesktopAppSelect(preset);
    }
}
