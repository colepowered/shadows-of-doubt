﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StringTooltipController : TooltipController
{
 //   public StringController stringController;
 //   public DBDetail detail;

 //   public GameObject fromArrow = null;
 //   public GameObject toArrow = null;

	//public float positionFromStringCentre = 64f;
 //   public bool multipleTooltipsAppearSideBySide = true;
 //   StringTooltipController[] allTooltips;

 //   public override void OnMouseEnterCustom()
 //   {
 //       //Set text
 //       mainText = detail.name;
 //   }

 //   public override void OnMouseOverCustom()
	//{
 //       if (stringController == null) stringController = this.gameObject.GetComponent<StringController>();

 //       //Get arrows- use additive approach
 //       foreach(DBDetail det in stringController.details)
 //       {
 //           //Spawn arrows
 //           if (fromArrow == null && det.fromItem != null && det.fromOutputIncrimination)
 //           {
 //               fromArrow = Instantiate(GameSettings.Instance.stringArrowPrefab, this.gameObject.transform, false);
 //               RectTransform rect = fromArrow.GetComponent<RectTransform>();

 //               //Position relative to parent
 //               RectTransform thisRect = this.gameObject.GetComponent<RectTransform>();

 //               float pos = positionFromStringCentre * -1f;
 //               rect.localPosition = new Vector2(0.5f * thisRect.sizeDelta.x + pos, 0f * thisRect.sizeDelta.y);
 //               rect.localRotation = Quaternion.Euler(0, 180, 0);

 //               Image img = fromArrow.GetComponent<Image>();

 //               if (stringController.fromIncrim == DBDetail.IncriminatingStatus.incriminating)
 //               {
 //                   img.color = GameSettings.Instance.incriminatingColour;
 //               }
 //               else if (stringController.fromIncrim == DBDetail.IncriminatingStatus.innocent)
 //               {

 //                   img.color = GameSettings.Instance.innocentColour;
 //               }
 //           }

 //           if (toArrow == null && det.toItem != null && det.toOutputIncrimination)
 //           {
 //               toArrow = Instantiate(GameSettings.Instance.stringArrowPrefab, this.gameObject.transform, false);
 //               RectTransform rect = toArrow.GetComponent<RectTransform>();

 //               //Position relative to parent
 //               RectTransform thisRect = this.gameObject.GetComponent<RectTransform>();

 //               float pos = positionFromStringCentre * 1f;
 //               rect.localPosition = new Vector2(0.5f * thisRect.sizeDelta.x + pos, 0f * thisRect.sizeDelta.y);

 //               Image img = toArrow.GetComponent<Image>();

 //               if (stringController.toIncrim == DBDetail.IncriminatingStatus.incriminating)
 //               {
 //                   img.color = GameSettings.Instance.incriminatingColour;
 //               }
 //               else if (stringController.toIncrim == DBDetail.IncriminatingStatus.innocent)
 //               {
 //                   img.color = GameSettings.Instance.innocentColour;
 //               }
 //           }
 //       }
 //   }

 //   public override void CustomPositioning()
 //   {
 //       if(multipleTooltipsAppearSideBySide)
 //       {
 //           RectTransform rect = spawnedTooltip.GetComponent<RectTransform>();

 //           //Get all tooltips
 //           allTooltips = this.gameObject.GetComponents<StringTooltipController>();

 //           //Which position am I in this list?
 //           for (int i = 0; i < allTooltips.Length; i++)
 //           {
 //               if(allTooltips[i] == this)
 //               {
 //                   rect.position = new Vector2(rect.position.x + (i * (rect.sizeDelta.x + 2f)), rect.position.y);

 //                   break;
 //               }
 //           }
 //       }
 //   }

 //   public override void OnMouseOffCustom()
	//{
 //       //Remove arrows
 //       if (fromArrow != null) Destroy(fromArrow);
 //       if (toArrow != null) Destroy(toArrow);
	//}
}
