using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResolveOptionsController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public RectTransform pageRect;
    public WindowContentController wcc;
    public TextMeshProUGUI titleText;
    public ButtonController submitButton;
    public ButtonController openJobPostButton;

    public void Setup(WindowContentController newContentController)
    {
        rect = this.gameObject.GetComponent<RectTransform>();

        wcc = newContentController;
        SetPageSize(new Vector2(740, 648));

        titleText.text = Strings.Get("ui.interface", "Case Options");

        if (wcc != null && wcc.window != null)
        {
            ResolveController resController = wcc.window.GetComponentInChildren<ResolveController>(true);

            if (resController != null)
            {
                submitButton.SetInteractable(resController.submitButton.interactable);
            }
        }
    }

    private void OnEnable()
    {
        if (CasePanelController.Instance.activeCase != null && CasePanelController.Instance.activeCase.job != null && CasePanelController.Instance.activeCase.job.post != null)
        {
            openJobPostButton.SetInteractable(true);
        }
        else openJobPostButton.SetInteractable(false);

        if(wcc != null && wcc.window != null)
        {
            ResolveController resController = wcc.window.GetComponentInChildren<ResolveController>(true);

            if (resController != null)
            {
                submitButton.SetInteractable(resController.submitButton.interactable);
            }
        }
    }

    public void HelpButton()
    {
        InterfaceController.Instance.OpenNotebookNoPause(openHelpSection: true);
    }

    public void OpenJobPostButton()
    {
        if (CasePanelController.Instance.activeCase != null && CasePanelController.Instance.activeCase.job != null && CasePanelController.Instance.activeCase.job.post != null)
        {
            InterfaceController.Instance.SpawnWindow(CasePanelController.Instance.activeCase.job.post.evidence, Evidence.DataKey.name, passedInteractable: CasePanelController.Instance.activeCase.job.post, worldInteraction: false);
        }
    }

    public void SubmitCaseButton()
    {
        ResolveController resController = wcc.window.GetComponentInChildren<ResolveController>(true);

        if (resController != null)
        {
            if(resController.submitButton.interactable)
            {
                resController.SubmitButton();
            }
        }
    }

    public void CloseCaseButton()
    {
        ResolveController resController = wcc.window.GetComponentInChildren<ResolveController>(true);

        if (resController != null)
        {
            resController.CloseCaseButton();
        }
    }

    public void SetPageSize(Vector2 newSize)
    {
        Game.Log("Set page size: " + newSize);
        rect.sizeDelta = newSize;
        wcc.normalSize = rect.sizeDelta;
    }
}
