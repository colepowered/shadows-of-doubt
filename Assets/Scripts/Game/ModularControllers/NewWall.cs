﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;

public class NewWall
{
    [Header("Transform")]
    public Vector3 position;
    public Vector3 localEulerAngles;
    public GameObject physicalObject; //In some cases you may need to spawn this as an actual object...

    [Header("Location")]
    public NewNode node; //Reference to the node
    
    [Space(5)]
    public Vector2 wallOffset; //0.5 +/- in either direction
    public bool isExterior = false; //This is an exterior wall
    public bool separateWall = false; //If true this is not part of the room's default walls and can use a different material
    //public MaterialGroupPreset wallMaterial; //Reference to the material group used (used instead of material as wall sections can change)
    //public Toolbox.MaterialKey wallMatKey;

    [Header("Details")]
    public int id = 0; //Editor IDs used for naming inside the floor editor
    public static int assignID = 1; //Editor IDs used for naming inside the floor editor
    public bool preventEntrance = false; //If true an entrance cannot be here

    [Header("Door Config")]
    public bool foundDoorMaterialKey = false;
    public Toolbox.MaterialKey doorMatKey;

    public float baseDoorStrength = 0f;
    public float currentDoorStrength = 0f;

    public float baseLockStrength = 0f;
    public float currentLockStrength = 0f;

    [Header("Wall Pair")]
    public DoorPairPreset preset;
    public NewWall otherWall; //The wall that makes up the back of this pairing
    public NewWall parentWall; //One of the two walls is the parent wall- this also contains the collider
    public NewWall childWall;

    [System.Serializable]
    public class FrontageSetting
    {
        public WallFrontagePreset preset;
        public Toolbox.MaterialKey matKey;
        public bool colors = false;
        public Vector3 offset;

        //Created at runtime
        [System.NonSerialized]
        public List<Interactable> createdInteractables = new List<Interactable>();
        [System.NonSerialized]
        public Transform mainTransform;
    }

    public List<FrontageSetting> frontagePresets = new List<FrontageSetting>(); //Wall frontage presets...

    [System.NonSerialized]
    public int otherWallID; //Used in loading only
    [System.NonSerialized]
    public int parentWallID; //Used in loading only
    [System.NonSerialized]
    public int childWallID; //Used in loading only

    [Header("Spawned Objects")]
    public bool optimizationOverride = false; //When true, no object is spawned as the space is being covered by another wall class.
    public bool optimizationAnchor = false; //When true spawn a bigger wall section to cover 3x the space
    public int nonOptimizedSegment = 0; //Because the 3 sections use different UVs, keep track of which model to use here...
    //public bool placedByPlayer = false; //True if this was placed by the player in-editor
    public GameObject spawnedWall;
    public GameObject wallPrefabRef; //Used in conjunction with the material group to get the correct material for this section's model
    public GameObject spawnedCorner;
    public GameObject spawnedCoving;
    public GameObject spawnedCornerCoving;
    public GameObject cornerPrefabRef;
    public GameObject spawnedSteps;
    public GameObject editorTrigger; //Used in-editor only for detecting mouse over
    public bool isShortWall = false; //True if this has spawned a short wall
    private GameObject blueprint; //In editor only: To better be able to see from above the generated 
    [System.NonSerialized]
    public Interactable lightswitchInteractable;
    public bool isDivider = false; //True if this wall section is part of divider
    public NewDoor door;
    public List<GameObject> spawnedFrontage = new List<GameObject>(); //Spawn wall frontage

    [Header("Lights")]
    public NewRoom containsLightswitch; //If this contains the lightswitch to the room, reference it here

    [Header("Windows")]
    public int windowUVHorizonalPosition = -1; //used in determining the below data
    public BuildingPreset.WindowUVBlock windowUV;

    [Header("Furniture")]
    public bool placedWallFurn = false; //Has wall furniture been placed?
    //public List<FurnitureLocation> individualFurniture = new List<FurnitureLocation>(); //Created from the above, reference to the individual furniture objects

    //[Header("Debug")]
    //public string otherName;
    //public string parentName;
    //public string childName;
    //public bool cornerCheck1 = false;
    //public bool cornerCheck2 = false;
    //public bool cornerCheck3 = false;
    //public List<string> presetAssignDebug = new List<string>();
    //public Vector2 debugRotatedOffset;
    //public List<NewWall> debugUVHorziontal;

    public void Setup(DoorPairPreset newPreset, NewNode newNode, Vector2 newOffset, bool newIsExterior)
    {
        id = assignID;
        assignID++;

        wallOffset = newOffset;

        newNode.AddNewWall(this);
        //this.transform.SetParent(node.room.gameObject.transform);

        preset = newPreset;
        
        isExterior = newIsExterior;

        //Rotate
        if (wallOffset.x < 0)
        {
            localEulerAngles = new Vector3(0, 90, 0);
        }
        else if (wallOffset.x > 0)
        {
            localEulerAngles = new Vector3(0, 270, 0);
        }
        else if (wallOffset.y < 0)
        {
            localEulerAngles = new Vector3(0, 0, 0);
        }
        else if (wallOffset.y > 0)
        {
            localEulerAngles = new Vector3(0, 180, 0);
        }

        position = node.position + new Vector3(
                                    wallOffset.x * (PathFinder.Instance.tileSize.x / CityControls.Instance.nodeMultiplier - 0.2f), //Account for wall thickness
                                    newNode.floorHeight * -0.1f, //Inverse the floor height to counteract the placement of the node's floor
                                    wallOffset.y * (PathFinder.Instance.tileSize.y / CityControls.Instance.nodeMultiplier - 0.2f)
                                                    );

        if (newPreset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
        {
            SetDoorPairPreset(preset);
        }

        //Set the default material group
        SetWallMaterial(node.room.defaultWallMaterial, node.room.defaultWallKey);

        //Spawn a trigger for detecting mouse over
        if (SessionData.Instance.isFloorEdit)
        {
            if (physicalObject == null)
            {
                physicalObject = Toolbox.Instance.SpawnObject(PrefabControls.Instance.wall, node.room.transform);
                physicalObject.transform.localEulerAngles = localEulerAngles;
                physicalObject.transform.position = position;
                FloorEditWallDetector w = physicalObject.AddComponent<FloorEditWallDetector>();
                w.debugNodePosition = node.position;
                w.debugFloorHeight = node.floorHeight;
                w.wall = this;

                if (spawnedWall != null)
                {
                    spawnedWall.transform.SetParent(physicalObject.transform, true);
                }
            }

            physicalObject.transform.position = position;

            editorTrigger = Toolbox.Instance.SpawnObject(PrefabControls.Instance.wallTrigger, physicalObject.transform);
            FloorEditController.Instance.wallTriggers.Add(editorTrigger);
        }
    }

    //Load from data
    public void Load(CitySaveData.WallCitySave data, NewNode newNode)
    {
        wallOffset = data.wo; //0.5 +/- in either direction

        //Toolbox.Instance.LoadDataFromResources<MaterialGroupPreset>("DecorData/MaterialGroups", data.wm, out wallMaterial);
        //wallMatKey = data.wmk;

        id = data.id; //Editor IDs used for naming inside the floor editor
        assignID = Mathf.Max(assignID, id + 1); //Make sure others won't overwrite this ID

        //Add to reference
        if(!CityConstructor.Instance.loadingWallsReference.ContainsKey(id))
        {
            CityConstructor.Instance.loadingWallsReference.Add(id, this);
        }

        Toolbox.Instance.LoadDataFromResources<DoorPairPreset>(data.p, out preset);

        //Load frontage
        foreach(CitySaveData.WallFrontageSave fr in data.fr)
        {
            WallFrontagePreset frontage = null;
            Toolbox.Instance.LoadDataFromResources<WallFrontagePreset>(fr.str, out frontage);

            if(frontage != null)
            {
                frontagePresets.Add(new FrontageSetting { preset = frontage, matKey = fr.matKey, offset = fr.o });
            }
        }

        //Load door material key
        if(data.dm)
        {
            foundDoorMaterialKey = true;
            doorMatKey = data.dmk;
        }

        //Set IDs
        otherWallID = data.ow; //The wall that makes up the back of this pairing
        parentWallID = data.pw; //One of the two walls is the parent wall- this also contains the collider
        childWallID = data.cw;

        separateWall = data.sw;

        optimizationOverride = data.oo; //When true, no object is spawned as the space is being covered by another wall class.
        optimizationAnchor = data.oa; //When true spawn a bigger wall section to cover 3x the space
        nonOptimizedSegment = data.nos; //Because the 3 sections use different UVs, keep track of which model to use here...
        isShortWall = data.isw; //True if this has spawned a short wall
        //isDivider = data.d; //True if this wall section is part of divider

        SetLockStrengthBase(data.ls);
        SetDoorStrengthBase(data.ds);

        newNode.AddNewWall(this);
        //this.transform.SetParent(node.room.gameObject.transform);

        //Rotate
        if (wallOffset.x < 0)
        {
            localEulerAngles = new Vector3(0, 90, 0);
        }
        else if (wallOffset.x > 0)
        {
            localEulerAngles = new Vector3(0, 270, 0);
        }
        else if (wallOffset.y < 0)
        {
            localEulerAngles = new Vector3(0, 0, 0);
        }
        else if (wallOffset.y > 0)
        {
            localEulerAngles = new Vector3(0, 180, 0);
        }


        position = node.position + new Vector3(
                                    wallOffset.x * (PathFinder.Instance.tileSize.x / CityControls.Instance.nodeMultiplier - 0.2f), //Account for wall thickness
                                    newNode.floorHeight * -0.1f, //Inverse the floor height to counteract the placement of the node's floor
                                    wallOffset.y * (PathFinder.Instance.tileSize.y / CityControls.Instance.nodeMultiplier - 0.2f)
                                                    );

        //Load view from window
        //visibleFromWindow = data.vfw;
    }

    public void SetDoorStrength(float newVal)
    {
        currentDoorStrength = newVal;
        currentDoorStrength = Mathf.Clamp01(currentDoorStrength);
        //Game.Log("New door strength: " + currentDoorStrength);
        if (otherWall != null) otherWall.currentDoorStrength = newVal;

        //Update UI
        if (door != null)
        {
            if ((door.doorInteractable != null && InteractionController.Instance.currentLookingAtInteractable == door.doorInteractable.controller) || (door.handleInteractable != null && InteractionController.Instance.currentLookingAtInteractable == door.handleInteractable.controller))
            {
                InteractionController.Instance.DisplayInteractionCursor(InteractionController.Instance.displayingInteraction, true); //Force update but don't change
            }

            //Add to damaged doors
            if(currentDoorStrength < baseDoorStrength)
            {
                if(!GameplayController.Instance.damagedDoors.Contains(door))
                {
                    GameplayController.Instance.damagedDoors.Add(door);
                }
            }

            //If lock strength is 0, it is picked (unlock)
            if (currentDoorStrength <= 0f)
            {
                //Set unlocked
                door.SetJammed(false);
                door.SetLocked(false, Player.Instance, true);
                door.SetKnowLockedStatus(true);
            }
        }
    }

    public void SetLockStrengthBase(float newVal)
    {
        baseLockStrength = newVal;
        if (otherWall != null) otherWall.baseLockStrength = newVal;

        ResetLockStrength();
    }

    //Reset the lock state strength
    public void ResetLockStrength()
    {
        currentLockStrength = baseLockStrength;
        if (otherWall != null) otherWall.currentLockStrength = baseLockStrength;
    }

    public void SetDoorStrengthBase(float newVal)
    {
        baseDoorStrength = newVal;
        if (otherWall != null) otherWall.baseDoorStrength = newVal;

        ResetDoorStrength();
    }

    //Reset the lock state strength
    public void ResetDoorStrength()
    {
        currentDoorStrength = baseDoorStrength;
        if (otherWall != null) otherWall.currentDoorStrength = baseDoorStrength;
    }

    //Minus lock strength
    public void SetCurrentLockStrength(float newVal)
    {
        currentLockStrength = newVal;
        currentLockStrength = Mathf.Clamp01(currentLockStrength);
        if (otherWall != null) otherWall.currentLockStrength = currentLockStrength;

        //Update UI
        if(door != null)
        {
            if ((door.doorInteractable != null && InteractionController.Instance.currentLookingAtInteractable == door.doorInteractable.controller) || (door.handleInteractable != null && InteractionController.Instance.currentLookingAtInteractable == door.handleInteractable.controller))
            {
                InteractionController.Instance.DisplayInteractionCursor(InteractionController.Instance.displayingInteraction, true); //Force update but don't change
            }

            //If lock strength is 0, it is picked (unlock)
            if (currentLockStrength <= 0f)
            {
                //Game.Log("Doors: " + name + " lock strength reached 0, this is now unlocked...");

                //Set unlocked
                door.SetJammed(false);
                door.SetLocked(false, Player.Instance, true);
                door.SetKnowLockedStatus(true);
            }
        }
    }

    public void SpawnWall(bool prepForCombinedMeshes)
    {
        //Remove existing wall
        Toolbox.Instance.DestroyObject(spawnedWall);
        Toolbox.Instance.DestroyObject(blueprint);
        Toolbox.Instance.DestroyObject(spawnedCoving);
        Toolbox.Instance.DestroyObject(spawnedSteps);

        isShortWall = false;

        UpdateSegmentData(); //Update segment data before spawning

        if (preset.optimizeSections && optimizationOverride) return; //Skip spawning if optimization override is active

        //Pick a gameobject to spawn from the preset
        wallPrefabRef = null;

        string seed = position.ToString() + node.nodeCoord.ToString();

        //Create long wall unless there isn't a prefab to spawn...
        if(optimizationAnchor && preset.parentWallsLong.Count > 0)
        {
            //Long wall
            if (parentWall == this)
            {
                if (preset.parentWallsLong.Count > 0) wallPrefabRef = preset.parentWallsLong[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.parentWallsLong.Count, seed, out seed)];
            }
            else
            {
                if (preset.childWallsLong.Count > 0) wallPrefabRef = preset.childWallsLong[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.childWallsLong.Count, seed, out seed)];
            }

        }
        else if(preset.parentWallsShort.Count > 0)
        {
            isShortWall = true;

            //Short wall
            if (parentWall == this)
            {
                if(preset.parentWallsShort.Count > 0) wallPrefabRef = preset.parentWallsShort[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.parentWallsShort.Count, seed, out seed)];
            }
            else
            {
                if (preset.childWallsShort.Count > 0) wallPrefabRef = preset.childWallsShort[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.childWallsShort.Count, seed, out seed)];
            }
        }

        //If no prefab at this point, abort
        if (wallPrefabRef == null) return;

        spawnedWall = Toolbox.Instance.SpawnObject(wallPrefabRef, node.room.transform);
        spawnedWall.transform.position = position;
        spawnedWall.transform.localEulerAngles = localEulerAngles;

        if(physicalObject != null)
        {
            spawnedWall.transform.SetParent(physicalObject.transform, true);
        }

        //Set to correct material
        if (!prepForCombinedMeshes || preset.materialOverride != null)
        {
            SetWallMaterial(node.room.defaultWallMaterial, node.room.defaultWallKey);
        }

        if (!prepForCombinedMeshes || preset.materialOverride != null)
        {
            if(spawnedWall != null)
            {
                MeshRenderer renderer = spawnedWall.GetComponent<MeshRenderer>();
                if(renderer == null) renderer = spawnedWall.AddComponent<MeshRenderer>();

                //Set light layer
                bool forceStreetLight = node.room.IsOutside();
                if (node.room.preset != null && node.room.preset.forceStreetLightLayer) forceStreetLight = node.room.preset.forceStreetLightLayer;
                Toolbox.Instance.SetLightLayer(renderer, node.building, forceStreetLight);
            }
        }

        //Spawn door if parent
        if (parentWall == this && door != null)
        {
            door.SpawnDoor();
        }

        //Spawn steps
        if(preset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
        {
            if (node.room.preset != null && node.room.preset.steps != null && node.floorHeight < otherWall.node.floorHeight)
            {
                spawnedSteps = Toolbox.Instance.SpawnObject(node.room.preset.steps, node.room.transform);
                spawnedSteps.transform.position = node.position;
                spawnedSteps.transform.localEulerAngles = localEulerAngles;
            }
        }

        //Spawn coving
        if(node.room.allowCoving && node.room.gameLocation.designStyle.allowCoving)
        {
            if(optimizationAnchor)
            {
                spawnedCoving = Toolbox.Instance.SpawnObject(PrefabControls.Instance.covingLong, node.room.transform);
            }
            else
            {
                spawnedCoving = Toolbox.Instance.SpawnObject(PrefabControls.Instance.covingShort, node.room.transform);
            }

            spawnedCoving.transform.position = position + new Vector3(0, node.floor.defaultCeilingHeight * 0.1f, 0);
            spawnedCoving.transform.localEulerAngles = localEulerAngles;

            //Set light layer
            if (!prepForCombinedMeshes || preset.materialOverride != null)
            {
                MeshRenderer renderer = spawnedCoving.AddComponent<MeshRenderer>();

                Toolbox.Instance.SetLightLayer(renderer, node.building, node.room.preset.forceStreetLightLayer);

                //Set material to same as walls
                MaterialsController.Instance.SetMaterialGroup(spawnedCoving, node.room.defaultWallMaterial, node.room.defaultWallKey, renderer: renderer);
            }
        }

        //Spawn blueprint
        if(SessionData.Instance.isFloorEdit)
        {
            if(physicalObject != null)
            {
                if (isShortWall)
                {
                    blueprint = Toolbox.Instance.SpawnObject(PrefabControls.Instance.blueprintWallShort, physicalObject.transform);
                }
                else
                {
                    blueprint = Toolbox.Instance.SpawnObject(PrefabControls.Instance.blueprintWallLong, physicalObject.transform);
                }
            }
        }
    }

    //Remove a wall physically
    public void RemoveWall()
    {
        if(node != null) node.RemoveWall(this);

        if (editorTrigger != null) FloorEditController.Instance.wallTriggers.Remove(editorTrigger);

        if (lightswitchInteractable != null)
        {
            lightswitchInteractable.SafeDelete(true);
        }

        //if (node != null) node.RemoveWall(this);

        if (spawnedCorner != null)
        {
            Toolbox.Instance.DestroyObject(spawnedCorner);
        }

        if (spawnedCornerCoving != null)
        {
            Toolbox.Instance.DestroyObject(spawnedCornerCoving);
        }

        if (spawnedCoving != null)
        {
            Toolbox.Instance.DestroyObject(spawnedCoving);
        }

        //Remove physical wall objects
        if (physicalObject != null)
        {
            Toolbox.Instance.DestroyObject(physicalObject);
        }

        if(door != null)
        {
            if (door.spawnedDoor != null)
            {
                Toolbox.Instance.DestroyObject(door.spawnedDoor);
            }

            Toolbox.Instance.DestroyObject(door.gameObject);
        }
    }

    private void UpdateSegmentData()
    {
        //Set non optimized wall segments...
        //Wall is on the left
        if (wallOffset.x < 0)
        {
            nonOptimizedSegment = (int)node.localTileCoord.y; //On the left you can use the tile coordinate (0-2)
        }
        else if(wallOffset.x > 0)
        {
            nonOptimizedSegment = 2 - (int)node.localTileCoord.y; //...Which means you can use the inverted above for on the right
        }
        else if(wallOffset.y < 0)
        {
            nonOptimizedSegment = 2 - (int)node.localTileCoord.x;
        }
        else if(wallOffset.y > 0)
        {
            nonOptimizedSegment = (int)node.localTileCoord.x;
        }
    }

    public void SpawnCorner(bool prepForCombinedMeshes)
    {
        Toolbox.Instance.DestroyObject(spawnedCorner);
        Toolbox.Instance.DestroyObject(spawnedCornerCoving);

        //Check & spawn a corner piece if needed
        //Check tile @ 2x opposite offset: This must not feature a wall in the same wall offset position (but feature an indoors tile)
        Vector3Int chck = new Vector3Int(Mathf.RoundToInt(wallOffset.y * 2), Mathf.RoundToInt(wallOffset.x * 2), 0);
        if (wallOffset.y == 0) chck *= -1; //Inverse

        Vector3Int adjacentCheck = new Vector3Int(node.nodeCoord.x + chck.x, node.nodeCoord.y + chck.y, node.nodeCoord.z);
        NewNode nodeHere = null;

        if (PathFinder.Instance.nodeMap.TryGetValue(adjacentCheck, out nodeHere))
        {
            NewWall w1 = nodeHere.walls.Find(item => item.wallOffset == wallOffset);

            //Wall doesn't exist in required position
            if (w1 == null || w1.preset.isFence)
            {
                //Check tile @ 2x offset
                chck = new Vector3Int(Mathf.RoundToInt(wallOffset.x * 2), Mathf.RoundToInt(wallOffset.y * 2), 0);
                Vector3Int diagonalCheck = adjacentCheck + chck;
                //cornerCheck1 = true;

                NewNode nodeThere = null;

                if (PathFinder.Instance.nodeMap.TryGetValue(diagonalCheck, out nodeThere))
                {
                    //Wall exists in required position
                    Vector2 wallChk = new Vector2(-wallOffset.y, -wallOffset.x);
                    if (wallOffset.y == 0) wallChk *= -1; //Inverse

                    NewWall w2 = nodeThere.walls.Find(item => item.wallOffset == wallChk);

                    //This one must exist
                    if (w2 != null)
                    {
                        Vector2 diagonalCheck2 = new Vector2(diagonalCheck.x + (wallOffset.x * -2), diagonalCheck.y + (wallOffset.y * -2));
                        //cornerCheck2 = true;

                        //The final check uses the same square as check #1
                        //Wall doesnt exist in required position
                        Vector2 wallChk2 = new Vector2(-wallOffset.y, -wallOffset.x);
                        if (wallOffset.y == 0) wallChk2 *= -1; //Inverse

                        NewWall w3 = nodeHere.walls.Find(item => item.wallOffset == wallChk2);

                        if (w3 == null || w3.preset.isFence)
                        {
                            //The preset to grab prefabs from
                            DoorPairPreset dpPreset = preset;

                            //If this is a fence and w2 isn't, use that preset's config...
                            if(preset.isFence && !w2.preset.isFence)
                            {
                                dpPreset = w2.preset;
                            }

                            //Requires corner here
                            //Pick a gameobject to spawn from the preset
                            NewBuilding building = node.building;
                            if (otherWall != null) building = otherWall.node.building;

                            bool quoin = false;

                            if(dpPreset.quoins.Count > 0 && building != null && building.preset != null && building.preset.enableExteriorQuoins)
                            {
                                if ((isExterior || node.tile.isEdge) && (w2.isExterior || w2.node.tile.isEdge)) quoin = true;
                            }

                            if (quoin)
                            {
                                cornerPrefabRef = dpPreset.quoins[Toolbox.Instance.GetPsuedoRandomNumberContained(0, dpPreset.quoins.Count, node.nodeCoord.ToString(), out _)];
                            }
                            else if(dpPreset.corners.Count > 0)
                            {
                                cornerPrefabRef = dpPreset.corners[Toolbox.Instance.GetPsuedoRandomNumberContained(0, dpPreset.corners.Count, node.nodeCoord.ToString(), out _)];
                            }

                            if(cornerPrefabRef != null)
                            {
                                spawnedCorner = Toolbox.Instance.SpawnObject(cornerPrefabRef, node.room.transform);

                                //Set postion
                                spawnedCorner.transform.localEulerAngles = localEulerAngles;

                                //spawnedCorner.transform.position = position + new Vector3(PathFinder.Instance.tileSize.x / CityControls.Instance.nodeMultiplier * -0.5f, 0, 0);

                                //Rotate this vector
                                Vector2 r = Toolbox.Instance.RotateVector2CW(new Vector2(PathFinder.Instance.tileSize.x / CityControls.Instance.nodeMultiplier * -0.5f, 0), localEulerAngles.y);
                                spawnedCorner.transform.position = position + new Vector3(r.x, 0, r.y);

                                //spawnedCorner.transform.position = position - new Vector3(0, 0, PathFinder.Instance.tileSize.x / CityControls.Instance.nodeMultiplier * -0.5f);

                                //Set to correct material
                                bool forceStreetLight = false;

                                if (!prepForCombinedMeshes || preset.materialOverride != null)
                                {
                                    MeshRenderer renderer = null;

                                    if (renderer == null) renderer = spawnedCorner.GetComponent<MeshRenderer>();
                                    if (renderer == null) renderer = spawnedCorner.AddComponent<MeshRenderer>();

                                    //Set light layer
                                    forceStreetLight = node.room.IsOutside();
                                    if (node.room.preset != null && node.room.preset.forceStreetLightLayer) forceStreetLight = node.room.preset.forceStreetLightLayer;
                                    Toolbox.Instance.SetLightLayer(renderer, node.building, forceStreetLight);

                                    SetWallMaterial(node.room.defaultWallMaterial, node.room.defaultWallKey);
                                }

                                //cornerCheck3 = true;

                                //Spawn coving
                                if (node.room.allowCoving && node.room.gameLocation.designStyle.allowCoving)
                                {
                                    spawnedCornerCoving = Toolbox.Instance.SpawnObject(PrefabControls.Instance.covingCorner, spawnedCorner.transform);
                                    spawnedCornerCoving.transform.localPosition = new Vector3(-0.1f, node.floor.defaultCeilingHeight * 0.1f, 0);
                                    //spawnedCornerCoving.transform.localEulerAngles = localEulerAngles;

                                    //Set light layer
                                    if (!prepForCombinedMeshes || preset.materialOverride != null)
                                    {
                                        MeshRenderer renderer = null;
                                        if (!prepForCombinedMeshes) renderer = spawnedCorner.AddComponent<MeshRenderer>();
                                        else renderer = spawnedCorner.GetComponent<MeshRenderer>();

                                        Toolbox.Instance.SetLightLayer(renderer, node.building, forceStreetLight);

                                        //Set material to same as walls
                                        MaterialsController.Instance.SetMaterialGroup(spawnedCornerCoving, node.room.defaultWallMaterial, node.room.defaultWallKey, renderer: renderer);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void SpawnFrontage(bool overrideWithKey = false, Toolbox.MaterialKey keyOverride = new Toolbox.MaterialKey())
    {
        //Remove existing...
        while(spawnedFrontage.Count > 0)
        {
            Toolbox.Instance.DestroyObject(spawnedFrontage[0]);
            spawnedFrontage.RemoveAt(0);
        }

        foreach(FrontageSetting frontage in frontagePresets)
        {
            GameObject newFrontage = Toolbox.Instance.SpawnObject(frontage.preset.gameObject, node.room.transform);
            newFrontage.transform.position = position + frontage.offset;
            newFrontage.transform.localEulerAngles = localEulerAngles;
            frontage.mainTransform = newFrontage.transform;

            Toolbox.MaterialKey key = frontage.matKey;
            if (overrideWithKey) key = keyOverride;

            //Apply material
            MeshRenderer[] r = newFrontage.GetComponentsInChildren<MeshRenderer>(true);

            foreach(MeshRenderer rend in r)
            {
                Material frontageMat =  MaterialsController.Instance.ApplyMaterialKey(rend, key);

                rend.staticShadowCaster = true; //Set as static ahdow caster

                //Add to static batching
                if (frontage.preset != null && frontage.preset.allowStaticBatching && frontageMat != null)
                {
                    MeshFilter mf = rend.gameObject.GetComponent<MeshFilter>();

                    if(mf != null)
                    {
                        node.room.AddForStaticBatching(rend.gameObject, mf.sharedMesh, frontageMat);
                    }
                }
            }

            //Set light layer
            bool streetLightLayer = node.room.IsOutside();
            if (node.room.preset != null && node.room.preset.forceStreetLightLayer) streetLightLayer = node.room.preset.forceStreetLightLayer;
            Toolbox.Instance.SetLightLayer(newFrontage, node.building, streetLightLayer);

            //Add to rainy window glass reference...
            if(frontage.preset.isRainyWindow && frontage.preset.rainyGlass != null)
            {
                if (!SessionData.Instance.rainyWindowMaterials.Contains(frontage.preset.rainyGlass))
                {
                    SessionData.Instance.rainyWindowMaterials.Add(frontage.preset.rainyGlass);
                    frontage.preset.rainyGlass.SetFloat("_Rain", SessionData.Instance.currentRain); //Set the rain amount now
                }

                //Switch the material now...
                if (Game.Instance.enableRainyWindows && (node.room.IsOutside() || otherWall.node.room.IsOutside()))
                {
                    foreach (Transform tr in newFrontage.transform)
                    {
                        if (tr.tag == "RainWindowGlass")
                        {
                            MeshRenderer rend = tr.gameObject.GetComponent<MeshRenderer>();
                            if(rend != null) rend.sharedMaterial = frontage.preset.rainyGlass;
                        }
                    }
                }
                else if(frontage.preset.regularGlass != null)
                {
                    foreach (Transform tr in newFrontage.transform)
                    {
                        if (tr.tag == "RainWindowGlass")
                        {
                            MeshRenderer rend = tr.gameObject.GetComponent<MeshRenderer>();
                            if (rend != null) rend.sharedMaterial = frontage.preset.regularGlass;
                        }
                    }
                }
            }

            spawnedFrontage.Add(newFrontage);

            //Spawn interactables
            foreach (FurniturePreset.IntegratedInteractable integrated in frontage.preset.integratedInteractables)
            {
                if (integrated.pairToController == InteractableController.InteractableID.none) continue;

                //Otherwise create object...
                InteractablePreset intPreset = integrated.preset;

                //Look at the prefab and find the local positions of all the controllers. We can use this for localpositions.
                List<InteractableController> controllerLocs = newFrontage.GetComponentsInChildren<InteractableController>(true).ToList();

                //Find their proper spawn positions...
                InteractableController correspondingController = controllerLocs.Find(item => item.id == integrated.pairToController);

                Vector3 iLocPos = Vector3.zero;
                Vector3 iLocRot = Vector3.zero;

                if (correspondingController == null)
                {
                    Game.Log("Unable for find corresponding controller for integrated interactable on " + preset.name);
                }

                iLocPos = correspondingController.transform.localPosition;
                iLocRot = correspondingController.transform.localEulerAngles;

                //Position above the zero point otherwise you will have problems with calculated postion
                Interactable newInt = InteractableCreator.Instance.CreateTransformInteractable(intPreset, correspondingController.transform, null, null, iLocPos, iLocRot, null);
                correspondingController.Setup(newInt);
                frontage.createdInteractables.Add(newInt); //Add to interactables
            }
        }
    }

    //Update the wall pair preset & respawn walls
    public void SetDoorPairPreset(DoorPairPreset newPreset, bool enableUpdate = true, bool newIsDivider = false, bool setPair = true)
    {
        //presetAssignDebug.Add("Preset changed through function");

        //Override...
        if(preset != null)
        {
            if (preset.overrideWallNormal && newPreset == InteriorControls.Instance.wallNormal)
            {
                newPreset = preset.wallNormalOverrride;
            }
            else if (preset.overrideDuctLower && newPreset == InteriorControls.Instance.wallVentLower)
            {
                newPreset = preset.ductLowerOverrride;
            }
            else if (preset.overrideDuctUpper && newPreset == InteriorControls.Instance.wallVentUpper)
            {
                newPreset = preset.ductUpperOverrride;
            }
        }

        //if(SessionData.Instance.isFloorEdit)
        //{
        //    if(FloorEditController.Instance.addressSelection == node.gameLocation || FloorEditController.Instance.addressSelection == otherWall.node.gameLocation)
        //    {
        //        Game.Log("Set wall at node " + node.nodeCoord + " with offset " + wallOffset + " to: " + newPreset.sectionClass.ToString() + " (" + preset.name + ")");
        //    }
        //}

        //Override using address preset
        if (parentWall != null && parentWall.node != null && parentWall.node.gameLocation.thisAsAddress != null)
        {
            if (parentWall.node.gameLocation.thisAsAddress.addressPreset != null && parentWall.node.room.preset != null)
            {
                //Is this an outside wall?
                if (childWall.node.gameLocation.thisAsStreet != null || parentWall.node.room.preset.replaceInsideAlso)
                {
                    //Only do this on certain rooms...
                    if(!parentWall.node.room.preset.replaceOnlyIfOtherIs || parentWall.node.room.preset.onlyReplaceIf.Contains(childWall.node.room.roomType))
                    {
                        //Override with privacy walls...
                        if (newPreset.sectionClass == DoorPairPreset.WallSectionClass.wall)
                        {
                            if (parentWall.node.room.preset.replaceWalls != null)
                            {
                                newPreset = parentWall.node.room.preset.replaceWalls;
                            }
                        }
                        else if (newPreset.sectionClass == DoorPairPreset.WallSectionClass.window || newPreset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge)
                        {
                            if (parentWall.node.room.preset.replaceWindows != null)
                            {
                                newPreset = parentWall.node.room.preset.replaceWindows;
                            }
                        }
                        else if (newPreset.sectionClass == DoorPairPreset.WallSectionClass.entrance && parentWall.node.room.preset.replaceEntrance != null)
                        {
                            newPreset = parentWall.node.room.preset.replaceEntrance;
                        }
                    }
                }
            }
        }

        //Override using floor height
        if(newPreset.raisedFloorOverride != null)
        {
            if(node.room.roomType.overrideFloorHeight)
            {
                if(node.room.roomType.floorHeight > 0)
                {
                    newPreset = newPreset.raisedFloorOverride;
                }
            }
            else if(node.floorHeight > 0)
            {
                newPreset = newPreset.raisedFloorOverride;
            }
        }

        preset = newPreset;

        if(setPair)
        {
            if (parentWall != null && parentWall.preset != newPreset) parentWall.SetDoorPairPreset(newPreset, setPair: false);
            if (childWall != null && childWall.preset != newPreset) childWall.SetDoorPairPreset(newPreset, setPair: false);
        }

        //if (parentWall != null) parentWall.isDivider = newIsDivider;
        //if (childWall != null) childWall.isDivider = newIsDivider;

        //Add/remove entrances
        if(newPreset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
        {
            if(newPreset.canFeatureDoor)
            {
                //Destroy existing...
                if (door != null)
                {
                    //Game.Log("Destroy existing door: " + node.room.name + " " + node.room.roomID);
                    if(CityData.Instance.doorDictionary.ContainsKey(door.wall.id))
                    {
                        CityData.Instance.doorDictionary.Remove(door.wall.id);
                    }

                    if (CityData.Instance.doorDictionary.ContainsKey(door.wall.otherWall.id))
                    {
                        CityData.Instance.doorDictionary.Remove(door.wall.otherWall.id);
                    }

                    Toolbox.Instance.DestroyObject(door.gameObject);
                }

                //Spawn door object
                //Chance of skipping door: This can only happen between 2 rooms of the same address
                //Choose the room preset with the highest door priority
                RoomTypePreset highestDoorP = node.room.roomType;
                NewRoom highestDoorPRoom = node.room;

                if (otherWall.node.room.roomType.doorPriority > highestDoorP.doorPriority)
                {
                    highestDoorPRoom = otherWall.node.room;
                    highestDoorP = otherWall.node.room.roomType;
                }
                //If equal? We need a reliable outcome so it doesn't matter which wall side tries to spawn a door first...
                else if(otherWall.node.room.roomType.doorPriority == highestDoorP.doorPriority)
                {
                    //Use highest room ID number
                    if(otherWall.node.room.roomID > node.room.roomID)
                    {
                        highestDoorPRoom = otherWall.node.room;
                        highestDoorP = otherWall.node.room.roomType;
                    }
                }

                float determinedDoorChance = 1f;

                if (highestDoorP.chanceOfNoDoor > 0f)
                {
                    //This should be the same no matter which wall spawns a door first...
                    determinedDoorChance = Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, highestDoorPRoom.roomID.ToString());
                }

                if (node.room.gameLocation != otherWall.node.room.gameLocation || determinedDoorChance >= highestDoorP.chanceOfNoDoor)
                {
                    //If the door opens into a narrow hallway and there is a chance that no door can exist here, don't spawn a door...
                    bool spawnDoor = true;

                    if(node.room.gameLocation == otherWall.node.room.gameLocation && highestDoorP.chanceOfNoDoor > 0.01f)
                    {
                        if(node.walls.Exists(item => item.wallOffset == wallOffset * -1f))
                        {
                            spawnDoor = false;
                        }
                        else if(otherWall.node.walls.Exists(item => item.wallOffset == wallOffset))
                        {
                            spawnDoor = false;
                        }
                    }

                    if(spawnDoor && (node.gameLocation.floor == null || node.gameLocation.floor.floor > CityControls.Instance.lowestFloor))
                    {
                        GameObject newDoorContainer = Toolbox.Instance.SpawnObject(PrefabControls.Instance.door, node.room.transform);

                        newDoorContainer.transform.position = position;
                        newDoorContainer.transform.localEulerAngles = localEulerAngles;

                        //Flip the door position if it can back onto a blank wall...
                        //Rank the two difference inversions to find the best...
                        float normalRank = 0f;
                        float invertedRank = 0f;

                        //Apply a rotation to this wall offset to the the one to check...
                        Vector2 rightWallOffset = Toolbox.Instance.RotateVector2CW(wallOffset, 90);
                        Vector2 leftWallOffset = -rightWallOffset;

                        //There is a wall on the side that this would back against if it were inverted...
                        if(node.walls.Exists(item => item.wallOffset == rightWallOffset && item.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance))
                        {
                            invertedRank += 10f;
                        }

                        if (node.walls.Exists(item => item.wallOffset == leftWallOffset && item.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance))
                        {
                            normalRank += 10f;
                        }

                        //Check the opposite node...
                        if (otherWall.node.walls.Exists(item => item.wallOffset == rightWallOffset && item.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance))
                        {
                            invertedRank += 10f;
                        }

                        if (otherWall.node.walls.Exists(item => item.wallOffset == leftWallOffset && item.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance))
                        {
                            normalRank += 10f;
                        }

                        //Invert X...
                        if(invertedRank > normalRank)
                        {
                            newDoorContainer.transform.localScale = new Vector3(-1, 1, 1);
                        }

                        door = newDoorContainer.GetComponent<NewDoor>();
                        door.Setup(parentWall);
                        otherWall.door = door;
                    }
                }

                if (parentWall.node.room != null && childWall.node.room != null)
                {
                    parentWall.node.room.AddEntrance(parentWall.node, childWall.node);
                }

                if (childWall.node.room != null && parentWall.node.room != null)
                {
                    childWall.node.room.AddEntrance(childWall.node, parentWall.node);
                }
            }
            else if (door != null)
            {
                if (CityData.Instance.doorDictionary.ContainsKey(door.wall.id))
                {
                    CityData.Instance.doorDictionary.Remove(door.wall.id);
                }

                if (CityData.Instance.doorDictionary.ContainsKey(door.wall.otherWall.id))
                {
                    CityData.Instance.doorDictionary.Remove(door.wall.otherWall.id);
                }

                Toolbox.Instance.DestroyObject(door.gameObject);
            }
        }
        else
        {
            //Remove existing door...
            if (door != null)
            {
                if (CityData.Instance.doorDictionary.ContainsKey(door.wall.id))
                {
                    CityData.Instance.doorDictionary.Remove(door.wall.id);
                }

                if (CityData.Instance.doorDictionary.ContainsKey(door.wall.otherWall.id))
                {
                    CityData.Instance.doorDictionary.Remove(door.wall.otherWall.id);
                }

                Toolbox.Instance.DestroyObject(door.gameObject);
            }

            //It looks like this can be called while the wall is being destroyed (due to destroyed node) so check it's node reference to see this
            if (parentWall.node != null && childWall.node != null)
            {
                if (parentWall.node.room != null && childWall.node.room != null)
                {
                    parentWall.node.room.RemoveEntrance(parentWall.node, childWall.node);
                }

                if (childWall.node.room != null && parentWall.node.room != null)
                {
                    childWall.node.room.RemoveEntrance(childWall.node, parentWall.node);
                }
            }
        }

        //Update walls
        if (enableUpdate && parentWall != null && childWall != null)
        {
            try
            {
                GenerationController.Instance.UpdateWallsRoom(parentWall.node.room);
                GenerationController.Instance.UpdateWallsRoom(childWall.node.room);
            }
            catch
            {

            }
        }
    }

    //Select compatible wall frontage
    public void SelectFrontage()
    {
        //Select frontage...
        if (node.room.preset.wallFrontage.Count > 0 && (otherWall == null || otherWall.frontagePresets.Count <= 0))
        {
            string randomSeed = node.nodeCoord.ToString();

            //Find valid configurations
            List<RoomConfiguration.WallFrontage> valid = new List<RoomConfiguration.WallFrontage>();

            if(node.room.preset.oneFrontagePerNode)
            {
                if(node.walls.Exists(item => item.frontagePresets.Count > 0 || item.otherWall.frontagePresets.Count > 0))
                {
                    return;
                }
            }

            foreach (RoomConfiguration.WallFrontage frontage in node.room.preset.wallFrontage)
            {
                //Limit by buildings...
                if(frontage.limitToBuildingTypes)
                {
                    if(node.building != null)
                    {
                        if (!frontage.limitedToBuildings.Contains(node.building.preset)) continue;
                    }
                    else if(otherWall.node.building != null)
                    {
                        if (!frontage.limitedToBuildings.Contains(otherWall.node.building.preset)) continue;
                    }
                }

                //Preset must match...
                if(frontage.wallPreset == preset)
                {
                    if(!frontage.onlyIfBorderingOutside)
                    {
                        valid.Add(frontage);
                    }
                    else
                    {
                        if(node.room.IsOutside() || otherWall.node.room.IsOutside())
                        {
                            valid.Add(frontage);
                        }
                    }
                }
            }

            if (valid.Count <= 0) return;

            foreach(RoomConfiguration.WallFrontage chosen in valid)
            {
                //Find valid presets
                foreach (WallFrontageClass frontClass in chosen.insideFrontage)
                {
                    //Pick preset
                    WallFrontagePreset chosenPreset = Toolbox.Instance.SelectWallFrontage(node.room.gameLocation.designStyle, frontClass, randomSeed);
                    Toolbox.MaterialKey chosenMatKey = new Toolbox.MaterialKey();
                    bool foundMaterialKey = false;

                    //Get material
                    if (chosenPreset.inheritColouringFromDecor && chosenPreset.variations.Count > 0)
                    {
                        if (chosenPreset.shareColours != FurniturePreset.ShareColours.none)
                        {
                            //Find other with this color sharing designation...
                            FrontageSetting other = null;

                            foreach (NewNode node in node.room.nodes)
                            {
                                foreach(NewWall wall in node.walls)
                                {
                                    other = wall.frontagePresets.Find(item => item.colors && item.preset.shareColours == chosenPreset.shareColours);
                                    //if(other == null) other = wall.otherWall.frontagePresets.Find(item => item.colors && item.preset.shareColours == chosenPreset.shareColours);

                                    if (other != null)
                                    {
                                        chosenMatKey = other.matKey;
                                        foundMaterialKey = true;
                                        break;
                                    }
                                }

                                if (other != null) break;
                            }
                        }

                        if (!foundMaterialKey)
                        {
                            chosenMatKey = MaterialsController.Instance.GenerateMaterialKey(chosenPreset.variations[Toolbox.Instance.RandContained(0, chosenPreset.variations.Count, randomSeed, out randomSeed)], node.room.colourScheme, node.room, true);
                            foundMaterialKey = true;
                        }
                    }

                    frontagePresets.Add(new FrontageSetting { preset = chosenPreset, matKey = chosenMatKey, offset = chosen.localOffset, colors = foundMaterialKey });
                }

                if (otherWall != null)
                {
                    foreach (WallFrontageClass frontClass in chosen.outsideFrontage)
                    {
                        //Pick preset
                        WallFrontagePreset chosenPreset = Toolbox.Instance.SelectWallFrontage(node.room.gameLocation.designStyle, frontClass, randomSeed);
                        Toolbox.MaterialKey chosenMatKey = new Toolbox.MaterialKey();

                        //Get material
                        if (chosenPreset.inheritColouringFromDecor && chosenPreset.variations.Count > 0)
                        {
                            bool foundMaterialKey = false;

                            if (chosenPreset.shareColours != FurniturePreset.ShareColours.none)
                            {
                                //Find other with this color sharing designation...
                                FrontageSetting other = null;

                                foreach (NewNode node in node.room.nodes)
                                {
                                    foreach (NewWall wall in node.walls)
                                    {
                                        other = wall.frontagePresets.Find(item => item.preset.shareColours == chosenPreset.shareColours);
                                        if (other == null) other = wall.otherWall.frontagePresets.Find(item => item.preset.shareColours == chosenPreset.shareColours);

                                        if (other != null)
                                        {
                                            chosenMatKey = other.matKey;
                                            foundMaterialKey = true;
                                            break;
                                        }
                                    }

                                    if (other != null) break;
                                }
                            }

                            if (!foundMaterialKey)
                            {
                                chosenMatKey = MaterialsController.Instance.GenerateMaterialKey(chosenPreset.variations[Toolbox.Instance.GetPsuedoRandomNumberContained(0, chosenPreset.variations.Count, randomSeed, out randomSeed)], node.room.colourScheme, node.room, true);
                                foundMaterialKey = true;
                            }
                        }

                        otherWall.frontagePresets.Add(new FrontageSetting { preset = chosenPreset, matKey = chosenMatKey, offset = chosen.localOffset });
                    }
                }
            }
        }
    }

    //Set material (used on non mesh-combine only)
    public void SetWallMaterial(MaterialGroupPreset newMat, Toolbox.MaterialKey newKey)
    {
        //Material override
        if (preset.materialOverride != null)
        {
            newMat = preset.materialOverride;
        }

        //No floor replacement
        if (node.room.nodes.Where(item => item.floorType == NewNode.FloorTileType.CeilingOnly || item.floorType == NewNode.FloorTileType.noneButIndoors || item.tile.isStairwell || item.tile.isInvertedStairwell).FirstOrDefault() != null)
        {
            if (newMat != null && newMat.noFloorReplacement != null)
            {
                newMat = newMat.noFloorReplacement;
            }
        }

        //if(!separateWall) room.wallMat = Toolbox.Instance.SetMaterialGroup(spawnedWall, newMat, newKey);

        //The following will only happen if there is a model present (ie won't happen on combined meshes)
        MaterialsController.Instance.SetMaterialGroup(spawnedWall, newMat, newKey);
        MaterialsController.Instance.SetMaterialGroup(spawnedCorner, newMat, newKey);
        MaterialsController.Instance.SetMaterialGroup(spawnedCoving, newMat, newKey);
        MaterialsController.Instance.SetMaterialGroup(spawnedCornerCoving, newMat, newKey);
    }

    //Set this wall to use a lightswitch for a room
    public void SetAsLightswitch(NewRoom newRoom)
    {
        if (newRoom != null)
        {
            containsLightswitch = newRoom;

            if (!newRoom.lightswitches.Contains(this))
            {
                newRoom.lightswitches.Add(this);
                if ((Game.Instance.devMode && Game.Instance.collectDebugData)) newRoom.debugLightswitches.Add(id.ToString());
            }

            //Create furniture
            List<NewNode> nodes = new List<NewNode>();
            nodes.Add(node);

            List<NewWall> againstWalls = new List<NewWall>();
            againstWalls.Add(this);

            if (lightswitchInteractable != null) return;

            //Offset lightswitch based on relative doorways...
            bool foundOffset = false;
            Vector2 offsetNormalized = wallOffset * 2;
            Vector2 offset = new Vector2(offsetNormalized.y , 0);
            if(offsetNormalized.x != 0) offset = new Vector2(0, offsetNormalized.x);

            //Search for immediate doorways...
            foreach (NewWall w in node.walls)
            {
                if (w == this) continue;

                if(w.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
                {
                    Vector2 otherNormalized = w.wallOffset * 2;
                    Vector2 diff = new Vector2(offsetNormalized.x + otherNormalized.x, offsetNormalized.y + otherNormalized.y);

                    if(offsetNormalized.y != 0)
                    {
                        offset.x = diff.x;
                    }
                    else
                    {
                        offset.y = diff.y;
                    }

                    foundOffset = true;
                    break;
                }
            }

            //Search adjacent walls for doors...
            if(!foundOffset)
            {
                foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                {
                    Vector3Int newOffset = node.nodeCoord + new Vector3Int(v2.x, v2.y, 0);
                    NewNode foundNode = null;

                    if (PathFinder.Instance.nodeMap.TryGetValue(newOffset, out foundNode))
                    {
                        if (foundNode.room == node.room)
                        {
                            NewWall adj = foundNode.walls.Find(item => item.wallOffset == wallOffset);

                            if (adj != null)
                            {
                                if(adj.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
                                {
                                    offset = v2;

                                    foundOffset = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            //Search for lightswitch Y offset from furniture
            float yOffset = 1.325f;

            foreach(FurnitureLocation f in node.individualFurniture)
            {
                foreach(FurnitureClass c in f.furnitureClasses)
                {
                    if(c.raiseLightswitch)
                    {
                        yOffset = Mathf.Max(yOffset, c.lightswitchYOffset);
                    }
                }
            }

            //Game.Log("Spawning lightswitch in room " + node.room.roomID + " with offset " + yOffset + " (" + node.room.furniture.Count + "/" + node.individualFurniture.Count + ")");

            Vector3 pos = this.node.room.transform.InverseTransformPoint(new Vector3(position.x + (offset.x * 0.625f), node.position.y + yOffset, position.z + (offset.y * 0.625f)));

            lightswitchInteractable = InteractableCreator.Instance.CreateTransformInteractable(InteriorControls.Instance.lightswitch, this.node.room.transform, null, null, pos, localEulerAngles, null);
            lightswitchInteractable.SetSwitchState(newRoom.mainLightStatus, null);
            node.room.lightswitchInteractables.Add(lightswitchInteractable);
        }
        else
        {
            if(containsLightswitch != null)
            {
                containsLightswitch.lightswitches.Remove(this);
                containsLightswitch.lightswitchInteractables.Remove(lightswitchInteractable);
                if ((Game.Instance.devMode && Game.Instance.collectDebugData)) containsLightswitch.debugLightswitches.Remove(id.ToString());
            }

            //Remove object
            if(lightswitchInteractable != null)
            {
                lightswitchInteractable = null;
            }

            containsLightswitch = null;
        }
    }

    //Generate save data
    public CitySaveData.WallCitySave GenerateSaveData()
    {
        CitySaveData.WallCitySave output = new CitySaveData.WallCitySave();

        output.wo = wallOffset; //0.5 +/- in either direction
        //output.wm = wallMaterial.name; //Reference to the material group used (used instead of material as wall sections can change)
        //output.wmk = wallMatKey;
        output.id = id; //Editor IDs used for naming inside the floor editor
        output.p = preset.id;
        output.ow = otherWall.id; //The wall that makes up the back of this pairing
        output.pw = parentWall.id; //One of the two walls is the parent wall- this also contains the collider
        output.cw = childWall.id;
        output.oo = optimizationOverride; //When true, no object is spawned as the space is being covered by another wall class.
        output.oa = optimizationAnchor; //When true spawn a bigger wall section to cover 3x the space
        output.nos = nonOptimizedSegment; //Because the 3 sections use different UVs, keep track of which model to use here...
        output.isw = isShortWall; //True if this has spawned a short wall
        //output.d = isDivider; //True if this wall section is part of divider

        output.ds = baseDoorStrength;
        output.ls = baseLockStrength;

        output.sw = separateWall;

        if(foundDoorMaterialKey)
        {
            output.dm = true;
            output.dmk = doorMatKey;
        }
        
        //Save frontage
        foreach(FrontageSetting frontage in frontagePresets)
        {
            output.fr.Add(new CitySaveData.WallFrontageSave { str = frontage.preset.name, matKey = frontage.matKey, o = frontage.offset });
        }

        if (containsLightswitch != null)
        {
            output.cl = containsLightswitch.roomID; //If this contains the lightswitch to the room, reference it here
        }

        //output.vfw = visibleFromWindow;

        return output;
    }

    ////Use rays to pre-calculate the view from this window
    //[Button]
    //public void GetWindowViewTiles()
    //{
    //    visibleFromWindow.Clear();

    //    foreach(StreetController streetCon in CityData.Instance.streetDirectory)
    //    {
    //        foreach(NewNode streetNode in streetCon.nodes)
    //        {
    //            //Only scan nodes infront of the window
    //            Vector3 localPosOfNode = InverseTransformPoint(streetNode.position);
    //            if (localPosOfNode.z > 0) continue; //Skip if behind

    //            //Check Distance threshold
    //            Vector3 rayPos = position + new Vector3(0, 1.8f, 0);

    //            float dist = Vector3.Distance(rayPos, streetNode.position);
    //            if (dist > 45f) continue;

    //            Vector3 targetDir = streetNode.position - rayPos;

    //            //Do a raycast to make sure this tile can be seen from the window
    //            Ray ray = new Ray(rayPos, targetDir);
    //            float rayDistance = 45f;

    //            RaycastHit[] hits = Physics.RaycastAll(ray, rayDistance);

    //            //Loop hits
    //            bool success = true;

    //            for (int u = 0; u < hits.Length; u++)
    //            {
    //                RaycastHit hit = hits[u];

    //                if (hit.transform.CompareTag("Street"))
    //                {
    //                    success = true;
    //                    break;
    //                }
    //                else if (hit.transform.CompareTag("BuildingModel"))
    //                {
    //                    success = false;
    //                    break;
    //                }
    //            }

    //            if(success)
    //            {
    //                if (!visibleFromWindow.Contains(streetNode.room.roomID))
    //                {
    //                    //Debug.DrawRay(rayPos, targetDir, Color.green, 60f);
    //                    visibleFromWindow.Add(streetNode.room.roomID);
    //                    break; //Break as we don't need to check any more nodes...
    //                }
    //                else break; //Break as we don't need to check any more nodes...
    //            }
    //            else
    //            {
    //                //Debug.DrawRay(rayPos, targetDir, Color.red, 60f);
    //            }
    //        }
    //    }
    //}
}
