using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using UnityEngine.EventSystems;

public class PinnedQuickMenuController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Header("Components")]
    public List<CanvasRenderer> renderers = new List<CanvasRenderer>();
    public PinnedItemController parentPinned;
    public ButtonController locateOnMapButton;
    public ButtonController plotRouteButton;
    public ButtonController toggleCollapseButton;
    public ButtonController toggleCrossOutButton;
    public ButtonController stickyNoteButton;
    public ButtonController newLinkButton;
    public ButtonController contextMenuButton;
    public List<ButtonController> activeButtons = new List<ButtonController>();

    [Header("State")]
    public bool isOver = false;
    public bool active = true;
    public float appearProgress = 0f;

    public void Setup(PinnedItemController newParent)
    {
        active = true;
        parentPinned = newParent;

        foreach (CanvasRenderer r in renderers)
        {
            r.SetAlpha(0);
        }

        //Figure out which buttons should be active...
        if(parentPinned.evidence != null)
        {
            EvidenceLocation loc = parentPinned.evidence as EvidenceLocation;

            if(loc == null)
            {
                if(locateOnMapButton != null) Destroy(locateOnMapButton.gameObject);
                if (plotRouteButton != null) Destroy(plotRouteButton.gameObject);
            }

            //Incompatible with multiple...
            if (InterfaceController.Instance.selectedPinned.Count > 1)
            {
                if (locateOnMapButton != null) Destroy(locateOnMapButton.gameObject);
                if (plotRouteButton != null) Destroy(plotRouteButton.gameObject);
                if (newLinkButton != null) Destroy(newLinkButton.gameObject);
            }
        }

        this.transform.rotation = Quaternion.identity;
        this.transform.localScale = Vector3.one;

        float yVal = 128f;
        if (parentPinned.caseElement.m) yVal = 16f;
        this.transform.position = parentPinned.transform.position - new Vector3(0, yVal * parentPinned.transform.lossyScale.y, 0);

        if (locateOnMapButton != null) activeButtons.Add(locateOnMapButton);
        if (plotRouteButton != null) activeButtons.Add(plotRouteButton);
        if (toggleCollapseButton != null) activeButtons.Add(toggleCollapseButton);
        if (toggleCrossOutButton != null) activeButtons.Add(toggleCrossOutButton);
        if (stickyNoteButton != null) activeButtons.Add(stickyNoteButton);
        if (newLinkButton != null) activeButtons.Add(newLinkButton);
        if (contextMenuButton != null) activeButtons.Add(contextMenuButton);
    }

    public void Remove(bool instant = false)
    {
        Game.Log("Interface: Remove quick menu");
        active = false;
        PinnedItemController.activeQuickMenu = null;
        if (instant) Destroy(this.gameObject);
    }

    private void Update()
    {
        float yVal = 128f;

        if (parentPinned != null && parentPinned.transform != null && this != null && this.transform != null)
        {
            this.transform.rotation = Quaternion.identity;
            if (parentPinned.caseElement.m) yVal = 16f;
            this.transform.position = parentPinned.transform.position - new Vector3(0, yVal * parentPinned.transform.lossyScale.y, 0);
        }

        if (active && appearProgress < 1f)
        {
            appearProgress += Time.deltaTime / 0.25f;
            appearProgress = Mathf.Clamp01(appearProgress);

            foreach(CanvasRenderer r in renderers)
            {
                if (r == null) continue;
                r.SetAlpha(appearProgress);
            }
        }
        else if(!active && appearProgress > 0f)
        {
            appearProgress -= Time.deltaTime / 0.2f;
            appearProgress = Mathf.Clamp01(appearProgress);

            foreach (CanvasRenderer r in renderers)
            {
                if (r == null) continue;
                r.SetAlpha(appearProgress);
            }
        }

        if (!active && appearProgress <= 0f && this != null && this.gameObject != null)
        {
            Destroy(this.gameObject);
        }
        else if(parentPinned == null && this != null && this.gameObject != null)
        {
            Destroy(this.gameObject);
        }
    }

    public void LocateOnMapButton()
    {
        if(parentPinned != null) parentPinned.LocateOnMap();
    }

    public void PlotRouteButton()
    {
        if (parentPinned != null) parentPinned.PlotRoute();
    }

    public void ToggleCollapseButton()
    {
        if (parentPinned != null) parentPinned.ToggleCollapse();
    }

    public void ToggleCrossOutButton()
    {
        if (parentPinned != null) parentPinned.ToggleCrossedOut();
    }

    public void StickyNoteButton()
    {
        if (parentPinned != null) parentPinned.NewStickyNote();
    }

    public void NewLinkButton()
    {
        if (parentPinned != null)
        {
            CasePanelController.Instance.CustomStringLinkSelection(parentPinned, false);
        }
    }

    public void ContextMenuButton()
    {
        if (parentPinned != null)
        {
            parentPinned.UpdateContextMenuOptions();
            if(parentPinned.contextMenu != null) parentPinned.contextMenu.OpenMenu();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isOver = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;

        isOver = false;

        if (parentPinned != null)
        {
            if (!parentPinned.isOver)
            {
                Remove();
            }
        }
    }
}
