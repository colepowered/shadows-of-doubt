Elevator Button Font - Aileron Heavy

Treatment is a gradient as follows:

Top left and right corners: #ccd4db

Bottom left and right corners: #bccadd

Underlay: #000000 Offset Y: minus something, use your own discretion and the reference image in the 'Examples of finish UI' folder and 1 softness (very soft and blurry).

*

Evidence Folder Title Font - Leaner Bold

Treatment is a flat colour as follows: #292829

Underlay: colour #e6caa0 Offset Y: -0.5 (potentially needs tweaking) and zero softness (keep a hard edge).

*

Typewriter font - I believe the font you are using already is well suited enough, but use colour: ##292829

*

Popup Title Font - Leaner Bold

Treatment is a flat colour as follows: #ff9393 

Glow: #f24658 Unfortunately the glow intensity should must be adjusted after testing in-game in some different environments, to ensure legibility of the text over immersion/theming.

*

Popup regular Font - Calibri

Treatment is a flat colour as follows: #ff9393

Glow: #f24658 Unfortunately the glow intensity should must be adjusted after testing in-game in some different environments, to ensure legibility of the text over immersion/theming.