﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "physicsprofile_data", menuName = "Database/Physics Profile")]

public class PhysicsProfile : SoCustomComparison
{
    [Header("Physics")]
    //[Tooltip("If this object is able to be carried by the player, this adds onto the distance infront")]
    //public float carryDistanceModifier = 0f;
    //[Tooltip("If this object is able to be carried by the player, this adds onto the carry height")]
    //public float carryHeightModifier = 0f;
    //[Tooltip("Add this to the raycast check performed when carrying (for collision with walls). Basically is half the width of the object, but will depend on its carried rotation.")]
    //public float carryDistanceCollisionBuffer = 0f;
    [Tooltip("Mass of the object when physics is enabled")]
    public float mass = 1f;
    [Tooltip("'How much air resistance affects the object when moving from forces. 0 means no air resistance, and infinity makes the object stop moving immediately.'")]
    public float drag = 0f;
    [Tooltip("'How much air resistance affects the object when rotating from torque. 0 means no air resistance. Note that you cannot make the object stop rotating just by setting its Angular Drag to infinity.'")]
    public float angularDrag = 0.05f;
    [Tooltip("If the object is held, it will default to this euler")]
    public Vector3 heldEuler;
    [Tooltip("Add this on to the base tamper distance before it is considered a crime")]
    public float tamperDistanceModifier = 0f;
    [Tooltip("Muliply the throw force in the gameplay settings by this.")]
    public float throwForceMultiplier = 1f;
    [Tooltip("Multiply any damage caused by throw impact with this")]
    public float throwDamageMultiplier = 1f;
    [Tooltip("Treat the audio event as caused by player, therefore making AI react to it")]
    public bool treatAsCausedByPlayer = false;
    [Tooltip("The default collision detection mode")]
    public CollisionDetectionMode collisionMode = CollisionDetectionMode.Discrete;
    [Tooltip("If true, this will be destroyed/removed if it's position needs to be reset. If false it will be reset to spawn position.")]
    public bool removeOnReset = false;

    [Header("Audio")]
    [Tooltip("Physics collisions use this sound")]
    public AudioEvent physicsCollisionAudio;
}
