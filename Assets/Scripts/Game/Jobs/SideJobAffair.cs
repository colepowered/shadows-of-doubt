﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SideJobAffair : SideJob
{
    Objective.ObjectiveTrigger getInfoTrigger = null;

    public SideJobAffair(JobPreset newPreset, SideJobController.JobPickData newData, bool immediatePost) : base(newPreset, newData, immediatePost)
    {

    }
}
