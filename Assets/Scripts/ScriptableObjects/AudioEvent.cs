﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using FMOD.Studio;
using UnityEngine.Rendering;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "audioevent_data", menuName = "Audio/Audio Event")]

public class AudioEvent : SoCustomComparison
{
    [Header("Setup")]
    [Tooltip("Paste the GUID from FMOD in here. Curly braces will be removed.")]
    [OnValueChanged("OnGUIDValueChangedCallback")]
    public string guid;
    [Tooltip("If enabled, this sound's occlusion will never be checked")]
    public bool disableOcclusion = false;
    [Tooltip("If enabled, this will print out debug info in the console")]
    public bool debug = false;
    [Tooltip("If enabled, this run the sound through the occlusion and AI checks without physically playing it")]
    public bool isDummyEvent = false;
    [Tooltip("Is this a licensed music track?")]
    public bool isLicensed = false;

    [Header("Disable")]
    [Tooltip("Disable this in-game completely.")]
    public bool disabled = false;

    [Header("Occlusion")]
    [Tooltip("If true this sound can penetrate walls")]
    public bool canPenetrateWalls = false;
    [Tooltip("If true this sound can penetrate floors")]
    public bool canPenetrateFloors = false;
    [Tooltip("If true this sound can penetrate cielings")]
    public bool canPenetrateCeilings = false;
    [Space(7)]
    public bool overrideMaximumLoops = false;
    [EnableIf("overrideMaximumLoops")]
    [InfoBox("If more than one of wall/floor/ceiling penetration is enabled, it is recommended to increase the maximum possible loops for this sound to reach the listener properly")]
    public int overriddenMaxLoops = 80;
    [Space(7)]
    [Tooltip("Overrides default occlusion value sound in the audio controller")]
    public bool overrideOcclusionModifier = false;
    [EnableIf("overrideOcclusionModifier")]
    [Tooltip("Each occlusion unit will decrease volume by this amount...")]
    [InfoBox("If overridden, make sure this is set to below 0, otherwise it may cause occlusion evalulation problems.")]
    public float occlusionUnitVolumeModifier = -0.1f;

    [Space(7)]
    public bool overrideOpenDoorOcclusion = false;
    [EnableIf("overrideOpenDoorOcclusion")]
    [Range(0, 10)]
    public int openDoorOcclusionUnits = 1;

    public bool overrideClosedDoorOcclusion = false;
    [EnableIf("overrideClosedDoorOcclusion")]
    [Range(0, 10)]
    public int closedDoorOcclusionUnits = 5;

    public bool overrideWindowOcclusion = false;
    [EnableIf("overrideWindowOcclusion")]
    [Range(0, 10)]
    public int windowOcclusionUnits = 4;

    public bool overrideWallOcclusion = false;
    [EnableIf("overrideWallOcclusion")]
    [Range(0, 10)]
    public int wallOcclusionUnits = 7;

    public bool overrideCeilingOcclusion = false;
    [EnableIf("overrideCeilingOcclusion")]
    [Range(0, 10)]
    public int ceilingOcclusionUnits = 8;

    public bool overrideFloorOcclusion = false;
    [EnableIf("overrideFloorOcclusion")]
    [Range(0, 10)]
    public int floorOcclusionUnits = 8;

    [Space(7)]

    [Tooltip("On updated occlusion, force the volume to change level at a specific rate (seconds)")]
    public bool forceVolumeLevelFadeTime = false;
    [EnableIf("forceVolumeLevelFadeTime")]
    public float volumeLevelFadeTime = 0.5f;

    [Header("Suspicion")]
    [Tooltip("Can this audio trigger an AI reaction?")]
    public bool canBeSuspicious = false;
    [Tooltip("This event will by default trigger an AI reaction")]
    [EnableIf("canBeSuspicious")]
    public bool alwaysSuspicious = false;
    [Tooltip("This event will only trigger an AI reaction if 1) The actor is trespassing and 2) The listener is allowed to go there without trespassing.")]
    [EnableIf("canBeSuspicious")]
    public bool suspiciousIfTresspassing = false;
    [Tooltip("This event will only trigger an AI reaction if the address is empty apart from the player and the following number of people")]
    [EnableIf("canBeSuspicious")]
    public bool onlySuspiciousIfEmptyAddress = false;
    [Tooltip("Only suspicious if not caused by an enforcer")]
    [EnableIf("canBeSuspicious")]
    public bool onlySuspiciousIfNotEnforcer = true;
    [EnableIf("canBeSuspicious")]
    public int suspiciousIfCitizenCount = 2;
    [Tooltip("If this event is urgency (eg gunshot), AI will immediately run for investiation")]
    public bool urgentResponse = false;
    [Tooltip("AI hearing this event triggers this amount of audio focus. 1 Will mean they investigate immediately, any less will be added within their focus window...")]
    [Range(0f, 1f)]
    public float audioFocus = 1f;
    [EnableIf("canBeSuspicious")]
    [Space(5)]
    [Tooltip("Force this to display a red outline if this loop is heard by the player")]
    public bool forceOutlineForLoopIfPlayerTrespassing = false;
    public MemoryTag citizenMemoryTag = MemoryTag.none;

    [Space(7)]
    [Tooltip("Will citizens get scared by this?")]
    public float spookValue = 0f;
    [Tooltip("Enforcers do not get scared by this")]
    public bool noSpookIfEnforcer = true;

    public enum MemoryTag { none, gunshot, scream};

    [Header("AI Hearing and Emulation")]
    [Tooltip("If the AI is asleep, how likely is it that they awaken upon hearing this? (Only works in conjunction with suspicious sounds)")]
    [Range(0f, 1f)]
    public float awakenChance = 0.5f;
    [ReadOnly]
    [Tooltip("The actual sound range set in FMOD.")]
    public float actualSoundRange = 0f;
    [Tooltip("The effective sound range that the AI can hear (applied to actual sound range above)")]
    public float hearingRange = 20f;
    [Tooltip("If in stealth mode, also apply the following modifier to the hearing range...")]
    public float stealthModeModifier = 0f;
    [Tooltip("If running apply the following modifier to the hearing range...")]
    public float runModifier = 0f;
    [Tooltip("Can the AI dance to this?")]
    public bool canDanceTo = false;

    [Header("Overrides")]
    [Range(0f, 1f)]
    public float masterVolumeScale = 1f;
    [Tooltip("If true then the AI 'sound level' above is overridden based on surface type. Important: Only works for footsteps!")]
    public bool modifyBasedOnSurface = false;
    [EnableIf("modifyBasedOnSurface")]
    public float concreteHearingRangeModifier = 0f;
    [EnableIf("modifyBasedOnSurface")]
    public float woodHearingRangeModifier = 0f;
    [EnableIf("modifyBasedOnSurface")]
    public float carpetHearingRangeModifier = 0f;
    [EnableIf("modifyBasedOnSurface")]
    public float tileHearingRangeModifier = 0f;
    [EnableIf("modifyBasedOnSurface")]
    public float plasterHearingRangeModifier = 0f;
    [EnableIf("modifyBasedOnSurface")]
    public float fabricHearingRangeModifier = 0f;
    [EnableIf("modifyBasedOnSurface")]
    public float metalHearingRangeModifier = 0f;
    [EnableIf("modifyBasedOnSurface")]
    public float glassHearingRangeModifier = 0f;

    private void OnGUIDValueChangedCallback()
    {
        string s = guid.Replace("{", "");
        s = s.Replace("}", "");
        guid = s;

        FMOD.GUID fmodEvent = FMOD.GUID.Parse(guid);
        FMOD.Studio.EventDescription description;
        FMODUnity.RuntimeManager.StudioSystem.getEventByID(fmodEvent, out description);
        description.getMinMaxDistance(out _, out actualSoundRange);
    }
}
