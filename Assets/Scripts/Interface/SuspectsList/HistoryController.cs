﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using TMPro;

public class HistoryController : MonoBehaviour
{
    public RectTransform rect;
    public WindowContentController wcc;
    public bool isSetup = false;

    public TextMeshProUGUI contentsText;
    public RectTransform entryParent;

    public TMP_InputField searchInputField;
    public List<SuspectListEntryController> spawnedEntries = new List<SuspectListEntryController>();

    //Singleton pattern
    private static HistoryController _instance;
    public static HistoryController Instance { get { return _instance; } }

    public void Setup(WindowContentController newContentController)
    {
        rect = this.gameObject.GetComponent<RectTransform>();

        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        wcc = newContentController;
        SetPageSize(new Vector2(740, 738));

        contentsText.text = Strings.Get("ui.handbook", "historyheader");

        isSetup = true;
    }

    public void SetPageSize(Vector2 newSize)
    {
        //Game.Log("Set history size: " + newSize);
        rect.sizeDelta = newSize;
        wcc.normalSize = rect.sizeDelta;
    }

    private void OnEnable()
    {
        if (isSetup)
        {
            GameplayController.Instance.OnNewEvidenceHistory += UpdateListDisplay;
            UpdateListDisplay(); //Update when enabled only
        }
    }

    private void OnDisable()
    {
         GameplayController.Instance.OnNewEvidenceHistory -= UpdateListDisplay;
    }

    private void OnDestroy()
    {
         GameplayController.Instance.OnNewEvidenceHistory -= UpdateListDisplay;
    }

    //Update facts list
    public void UpdateListDisplay()
    {
        Game.Log("Interface: Updating history list...");

        //Force deselect
        if (InterfaceController.Instance.selectedElement != null) InterfaceController.Instance.selectedElement.OnDeselect();

        rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 0);

        //Compile a list of file icons that should exist
        List<GameplayController.History> required = new List<GameplayController.History>();

        foreach (GameplayController.History h in GameplayController.Instance.history)
        {
            if (required.Count >= 40) break; //Display 40 maximum entries
            if (required.Contains(h)) continue;

            if (searchInputField.text.Length <= 0)
            {
                required.Add(h);
            }
            else
            {
                Evidence ev = null;

                if (GameplayController.Instance.evidenceDictionary.TryGetValue(h.evID, out ev))
                {
                    string realText = ev.GetNameForDataKey(h.keys);

                    if (realText.ToLower().Contains(searchInputField.text.ToLower()))
                    {
                        required.Add(h);
                    }
                }
            }
        }

        //Add places to the list
        if (searchInputField.text.Length > 0)
        {
            foreach(NewGameLocation location in CityData.Instance.gameLocationDirectory)
            {
                if((location.thisAsStreet == null && location.rooms.Count <=0) || location.entrances.Count <= 0 /*|| !location.rooms.Exists(item => !item.isNullRoom && !item.isBaseNullRoom)*/)
                {
                    continue;
                }

                if(location.evidenceEntry != null)
                {
                    List<Evidence.DataKey> keys = location.evidenceEntry.GetTiedKeys(Evidence.DataKey.location);

                    if(keys.Contains(Evidence.DataKey.name))
                    {
                        string realText = location.evidenceEntry.GetNameForDataKey(Evidence.DataKey.name);

                        if (realText.ToLower().Contains(searchInputField.text.ToLower()))
                        {
                            if(!required.Exists(item => item.evID == location.evidenceEntry.evID))
                            {
                                GameplayController.History h = new GameplayController.History();
                                h.evID = location.evidenceEntry.evID;
                                h.keys = keys;
                                h.lastAccess = -1;
                                required.Add(h);
                            }
                        }
                    }
                }
            }

            foreach(NewBuilding building in CityData.Instance.buildingDirectory)
            {
                if(building.evidenceEntry != null)
                {
                    if(building.floors.Count > 0)
                    {
                        List<Evidence.DataKey> keys = building.evidenceEntry.GetTiedKeys(Evidence.DataKey.location);

                        string realText = building.evidenceEntry.GetNameForDataKey(Evidence.DataKey.location);

                        if (realText.ToLower().Contains(searchInputField.text.ToLower()))
                        {
                            if (!required.Exists(item => item.evID == building.evidenceEntry.evID))
                            {
                                GameplayController.History h = new GameplayController.History();
                                h.evID = building.evidenceEntry.evID;
                                h.keys = keys;
                                h.lastAccess = -1;
                                required.Add(h);
                            }
                        }
                    }
                }
            }
        }

        //Go through existing list
        for (int i = 0; i < spawnedEntries.Count; i++)
        {
            SuspectListEntryController sec = spawnedEntries[i] as SuspectListEntryController;

            //If a null entry, this has obviously been destroyed- remove entry
            if (sec == null)
            {
                spawnedEntries.RemoveAt(i);
                i--;
                continue;
            }

            if(required.Contains(sec.key))
            {
                required.Remove(sec.key);
                sec.VisualUpdate();
            }
            else
            {
                Destroy(sec.gameObject);
                spawnedEntries.RemoveAt(i);
                i--;
                continue;
            }
        }

        //The ones left in the found files list are buttons to spawn
        foreach (GameplayController.History sec in required)
        {
            if (sec == null || sec.evID == null || sec.evID.Length <= 0) continue;

            if(GameplayController.Instance.evidenceDictionary.TryGetValue(sec.evID, out _))
            {
                GameObject newButton = Instantiate(PrefabControls.Instance.suspectWindowEntry, entryParent);
                SuspectListEntryController newEntry = newButton.GetComponent<SuspectListEntryController>();

                newEntry.Setup(sec);
                spawnedEntries.Add(newEntry);
            }
        }

        //Lost list by lastest access time
        spawnedEntries.Sort((p2, p1) => p1.key.lastAccess.CompareTo(p2.key.lastAccess));

        for (int i = 0; i < spawnedEntries.Count; i++)
        {
            spawnedEntries[i].transform.SetAsLastSibling();
        }

        //Update rect sizes...
        entryParent.sizeDelta = new Vector2(entryParent.sizeDelta.x, Mathf.Max(spawnedEntries.Count * 130f + 24f, 466));
        SetPageSize(new Vector2(rect.sizeDelta.x, entryParent.sizeDelta.y + 400f));
    }

    public void ClearSearchButton()
    {
        searchInputField.text = string.Empty;
    }
}
