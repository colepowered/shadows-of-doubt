﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MenuAutoTextController : MonoBehaviour
{
    void Start()
    {
        TextMeshProUGUI text = this.gameObject.GetComponent<TextMeshProUGUI>();
        if(text != null && text.text.Length > 0) text.text = Strings.Get("ui.interface", text.text);
    }
}
