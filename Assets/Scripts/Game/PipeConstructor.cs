using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;

public class PipeConstructor : MonoBehaviour
{
    [Header("Components")]
    public List<PipeSetup> pipeConfig = new List<PipeSetup>();

    [Header("Generated")]
    public List<PipeGroup> generated = new List<PipeGroup>();
    
    [System.Serializable]
    public class PipeSetup
    {
        public PipeType type;
        public Material material;
        public List<GameObject> models = new List<GameObject>();
    }

    public enum PipeType { wire, wire2 };

    [System.Serializable]
    public class PipeGroup
    {
        public int type;
        public List<PipeRoute> routes = new List<PipeRoute>(); //The route and pieces needed
        public List<int> rooms = new List<int>(); //List of rooms covered

        [System.NonSerialized]
        public GameObject spawned;
        [System.NonSerialized]
        public bool isVisible = false;

        public PipeGroup(PipeType newType)
        {
            type = (int)newType;
            PipeConstructor.Instance.generated.Add(this);
        }

        public void AddPipeRoute(NewWall from, NewWall to, int sourceIndex, int endIndex)
        {
            AddPipeRoute(from, to, new int[] { sourceIndex }, new int[] { endIndex });
        }

        public void AddPipeRoute(NewWall from, NewWall to, int[] sourceIndex, int[] endIndex)
        {
            if(from == null)
            {
                //Game.Log("From wall is null!");
                return;
            }
            else if(to == null)
            {
                //Game.Log("To wall is null!");
                return;
            }

            List<NewWall> r = PipeConstructor.Instance.WallPathfind(from, to, this);

            if(r != null)
            {
                //Game.Log("Adding pipe route with " + r.Count + " pieces");

                //Run through to figure out pieces needed...
                for (int i = 0; i < r.Count; i++)
                {
                    NewWall current = r[i];

                    //Create list of covered rooms
                    if(!rooms.Contains(current.node.room.roomID))
                    {
                        rooms.Add(current.node.room.roomID);
                    }

                    PipeRoute route = routes.Find(item => item.w == current.id); //Find existing

                    //If no existing, add new
                    if(route == null)
                    {
                        route = new PipeRoute();
                        route.w = current.id;
                        routes.Add(route);
                    }

                    NewWall next = null;
                    if (i < r.Count - 1) next = r[i + 1];

                    NewWall prev = null;
                    if (i > 0) prev = r[i - 1];

                    //The start will always need a source connection.
                    if(i == 0)
                    {
                        //Default source
                        foreach(int source in sourceIndex)
                        {
                            if (source < 0) continue;

                            if (!route.s.Contains(source))
                            {
                                route.s.Add(source);
                            }
                        }
                    }
                    //End with another source
                    else if(i == r.Count -1)
                    {
                        //Default source
                        foreach(int end in endIndex)
                        {
                            if (end < 0) continue;

                            if (!route.s.Contains(end))
                            {
                                route.s.Add(end);
                            }
                        }
                    }

                    //Underneath
                    if(next != null)
                    {
                        if (next.node.nodeCoord.z < current.node.nodeCoord.z)
                        {
                            if (!route.s.Contains(1))
                            {
                                route.s.Add(1);
                            }
                        }
                        //Above
                        else if(next.node.nodeCoord.z > current.node.nodeCoord.z)
                        {
                            if (!route.s.Contains(0))
                            {
                                route.s.Add(0);
                            }
                        }
                    }

                    //Underneath
                    if(prev != null)
                    {
                        if (prev.node.nodeCoord.z < current.node.nodeCoord.z)
                        {
                            if (!route.s.Contains(1))
                            {
                                route.s.Add(1);
                            }
                        }
                        //Above
                        else if (prev.node.nodeCoord.z > current.node.nodeCoord.z)
                        {
                            if (!route.s.Contains(0))
                            {
                                route.s.Add(0);
                            }
                        }
                    }

                    //Left/Right
                    if(!route.s.Contains(2))
                    {
                        if(next != null && prev != null && next.node.nodeCoord.z == current.node.nodeCoord.z && prev.node.nodeCoord.z == current.node.nodeCoord.z && 
                            ((Instance.IsLeftOf(next, current) && Instance.IsRightOf(prev, current)) || (Instance.IsLeftOf(prev, current) && Instance.IsRightOf(next, current))))
                        {
                             route.s.Add(2);
                        }
                        else
                        {
                            if(next != null && next.node.nodeCoord.z == current.node.nodeCoord.z)
                            {
                                if(Instance.IsLeftOf(next, current))
                                {
                                    if (!route.s.Contains(3))
                                    {
                                        route.s.Add(3);
                                    }
                                }
                                else if (Instance.IsRightOf(next, current))
                                {
                                    if (!route.s.Contains(4))
                                    {
                                        route.s.Add(4);
                                    }
                                }
                                else if (Instance.IsFrontOf(next, current))
                                {
                                    if (!route.s.Contains(7))
                                    {
                                        route.s.Add(7);
                                    }
                                }
                            }

                            if(prev != null && prev.node.nodeCoord.z == current.node.nodeCoord.z)
                            {
                                if (Instance.IsLeftOf(prev, current))
                                {
                                    if (!route.s.Contains(3) && !route.s.Contains(2))
                                    {
                                        route.s.Add(3);
                                    }
                                }
                                else if (Instance.IsRightOf(prev, current))
                                {
                                    if (!route.s.Contains(4) && !route.s.Contains(2))
                                    {
                                        route.s.Add(4);
                                    }
                                }
                                else if (Instance.IsFrontOf(prev, current))
                                {
                                    if (!route.s.Contains(7))
                                    {
                                        route.s.Add(7);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public void AddToRoomsAsReferences()
        {
            foreach(int i in rooms)
            {
                NewRoom r = null;

                if(CityData.Instance.roomDictionary.TryGetValue(i, out r))
                {
                    if(!r.pipes.Contains(this))
                    {
                        r.pipes.Add(this);
                    }
                }
            }
        }

        public void SetVisible(bool val)
        {
            isVisible = val;
            if (spawned != null) spawned.SetActive(isVisible);
        }

        public void Spawn()
        {
            if(spawned == null)
            {
                List<MeshFilter> childMeshes = new List<MeshFilter>(); //Populate list of children meshes as we spawn them

                PipeSetup pipeType = PipeConstructor.Instance.pipeConfig.Find(item => (int)item.type == type);
                if (pipeType == null) return;

                foreach (PipeRoute route in routes)
                {
                    NewWall w = null;

                    if(TryGetWall(route.w, out w))
                    {
                        foreach (int pieceIndex in route.s)
                        {
                            if(pieceIndex < pipeType.models.Count)
                            {
                                GameObject newDuctModel = Instantiate(pipeType.models[pieceIndex], PipeConstructor.Instance.transform);
                                newDuctModel.transform.position = w.position;
                                newDuctModel.transform.localEulerAngles = w.localEulerAngles + w.node.room.transform.eulerAngles;
                                childMeshes.Add(newDuctModel.GetComponent<MeshFilter>());

                                //Position this special piece from the ceiling
                                if(pieceIndex == 6 && w.node.floor != null)
                                {
                                    newDuctModel.transform.position = new Vector3(newDuctModel.transform.position.x, newDuctModel.transform.position.y + w.node.floor.defaultCeilingHeight * 0.1f, newDuctModel.transform.position.z);
                                }

                                //newDuctModel.name = "W: " + w.id + ", " + w.position + ", " + (w.localEulerAngles + w.node.room.transform.eulerAngles);
                            }
                            else
                            {
                                Game.Log("Unable to get piece index " + pieceIndex);
                            }
                        }
                    }
                    else
                    {
                        Game.Log("Unable to find wall ID " + route.w);
                    }

                }

                //Combine all meshes
                spawned = new GameObject();
                spawned.transform.SetParent(PipeConstructor.Instance.transform);

                //Because the only way to do this is to use world coordinates, make this container position @ 0,0,0
                spawned.transform.position = Vector3.zero;
                spawned.transform.eulerAngles = Vector3.zero;

                //Create a mesh instance for combined meshes
                CombineInstance[] combine = new CombineInstance[childMeshes.Count];

                //Cycle through meshes to combine them
                int i = 0;

                while (i < childMeshes.Count)
                {
                    if (childMeshes[i].transform == spawned.transform) continue;

                    combine[i].mesh = childMeshes[i].sharedMesh;
                    combine[i].transform = childMeshes[i].transform.localToWorldMatrix;
                    Destroy(childMeshes[i].gameObject); //Remove gameobjects
                                                        //childMeshes[i].gameObject.SetActive(false);
                    i++;
                }

                //Create mesh renderer
                MeshFilter meshFilter = spawned.AddComponent<MeshFilter>();
                MeshRenderer combinedMesh = spawned.AddComponent<MeshRenderer>();
                //MeshCollider meshColl = spawned.AddComponent<MeshCollider>();

                //New mesh
                meshFilter.mesh = new Mesh();
                meshFilter.mesh.CombineMeshes(combine, true, true);
                spawned.SetActive(true);

                combinedMesh.sharedMaterial = pipeType.material;

                //Reset position to building pos
                spawned.transform.localPosition = Vector3.zero;
                spawned.transform.localEulerAngles = Vector3.zero;

                NewRoom r = null;

                if (rooms.Count > 0)
                {
                    CityData.Instance.roomDictionary.TryGetValue(rooms[0], out r);
                }

                //Set light layer
                if (r != null) Toolbox.Instance.SetLightLayer(spawned, r.building, false);
            }

            if(spawned != null)
            {
                spawned.SetActive(false);
                SetVisible(false);
            }
        }

        public bool TryGetWall(int input, out NewWall output)
        {
            output = null;

            foreach(int i in rooms)
            {
                NewRoom r = null;

                if(CityData.Instance.roomDictionary.TryGetValue(i, out r))
                {
                    foreach(NewNode n in r.nodes)
                    {
                        output = n.walls.Find(item => item.id == input);

                        if(output != null)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }

    [System.Serializable]
    public class PipeRoute
    {
        public int w; //WallID
        public List<int> s = new List<int>(); //Pipe sections
    }

    public int debugGetWall1;
    public int debugGetWall2;

    //Singleton pattern
    private static PipeConstructor _instance;
    public static PipeConstructor Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public List<NewWall> WallPathfind(NewWall from, NewWall to, PipeGroup existingGroup)
    {
        // The set of nodes already evaluated.
        HashSet<NewWall> closedSet = new HashSet<NewWall>();

        // The set of currently discovered nodes still to be evaluated.
        // Initially, only the start node is known.
        HashSet<NewWall> openSet = new HashSet<NewWall>();
        openSet.Add(from);

        // For each node, which node it can most efficiently be reached from.
        // If a node can be reached from many nodes, cameFrom will eventually contain the
        // most efficient previous step.
        Dictionary<NewWall, NewWall> cameFrom = new Dictionary<NewWall, NewWall>();

        // For each node, the cost of getting from the start node to that node.
        Dictionary<NewWall, float> fromStartToThis = new Dictionary<NewWall, float>();
        // The cost of going from start to start is zero.
        fromStartToThis.Add(from, 0f);

        // For each node, the total cost of getting from the start node to the goal
        // by passing by that node. That value is partly known, partly heuristic.
        Dictionary<NewWall, float> fromStartViaThis = new Dictionary<NewWall, float>();
        // For the first node, that value is completely heuristic.
        fromStartViaThis.Add(from, Vector3.Distance(from.position, to.position));//For heutristics, use distance for now

        int calculationLoops = 0;

        //while openSet is not empty
        while (openSet.Count > 0)
        {
            //current := the node in openSet having the lowest fScore[] value
            NewWall current = openSet.FirstOrDefault();
            float lowestFScore = Mathf.Infinity;

            foreach (NewWall access in openSet)
            {
                //If doesn't exist, assume infinity (ie not lower than lowest score)
                if (fromStartViaThis.ContainsKey(access))
                {
                    if (fromStartViaThis[access] < lowestFScore)
                    {
                        lowestFScore = fromStartViaThis[access];
                        current = access;
                    }
                }
            }

            //if current == goal then we're done!
            if (current == to)
            {
                //Construct the path
                List<NewWall> totalPath = new List<NewWall>();
                NewWall previous = null;

                //We're constructing the path backwards, so start with this as the first entry...
                totalPath.Add(current);

                while (cameFrom.ContainsKey(current))
                {
                    previous = current;
                    current = cameFrom[current];

                    //negative vector2 is used as a null, so don't add this.
                    if (current != null)
                    {
                        //The next node to add must be different
                        if (current != previous)
                        {
                            totalPath.Add(current);
                        }
                        else continue;
                    }
                    else
                    {
                        break;
                    }
                }

                totalPath.Reverse();

                //Return
                return totalPath;
            }

            //Add other walls on this node...
            if(current.node != null)
            {
                foreach (NewWall w in current.node.walls)
                {
                    if (w == current) continue;
                    if (w.preset.ignoreCullingRaycasts) continue;

                    // Ignore the neighbor which is already evaluated.
                    if (closedSet.Contains(w)) continue;
                    if (openSet.Contains(w)) continue;

                    //The path weight from start to the neighbor
                    //We're adding the pre-calculated weight from the node of the entrance we just walked into, to the possible next entrance to (exterior/to -> interior/from)
                    float tentativeGScore = fromStartToThis[current] + 1;

                    //Directly accross
                    if(Mathf.Abs(w.wallOffset.y) == Mathf.Abs(current.wallOffset.y) && Mathf.Abs(w.wallOffset.x) == Mathf.Abs(current.wallOffset.x))
                    {
                        tentativeGScore++;
                    }

                    if(existingGroup != null)
                    {
                        PipeRoute existingRoute = existingGroup.routes.Find(item => item.w == w.id);

                        if (existingRoute != null)
                        {
                            tentativeGScore = 0.5f;
                        }
                    }

                    //Discover a new node- add to open set
                    //Game.Log("Pathfinder: Add " + neighbor.pathCoord + " to open set, count: " + openSet.Count);
                    openSet.Add(w);

                    //Path hasn't been added (assume infinity) if tentative score is lower then this is automatically the best until now.
                    if (!fromStartToThis.ContainsKey(w) || tentativeGScore < fromStartToThis[w])
                    {
                        if (!cameFrom.ContainsKey(w)) cameFrom.Add(w, current);
                        else cameFrom[w] = current;

                        if (!fromStartToThis.ContainsKey(w)) fromStartToThis.Add(w, tentativeGScore);
                        else fromStartToThis[w] = tentativeGScore;

                        float viaThis = fromStartToThis[w] + Vector3.Distance(w.node.position, to.node.position);
                        if (!fromStartViaThis.ContainsKey(w)) fromStartViaThis.Add(w, viaThis);
                        else fromStartViaThis[w] = viaThis;
                    }
                }
            }

            //Add adjacent walls
            foreach(Vector2Int v2 in CityData.Instance.offsetArrayX8)
            {
                Vector3Int np = current.node.nodeCoord + new Vector3Int(v2.x, v2.y, 0);
                NewNode foundNode = null;

                if(PathFinder.Instance.nodeMap.TryGetValue(np, out foundNode))
                {
                    foreach(NewWall w in foundNode.walls)
                    {
                        if (w == current) continue;
                        if (w.preset.ignoreCullingRaycasts) continue;

                        // Ignore the neighbor which is already evaluated.
                        if (closedSet.Contains(w)) continue;
                        if (openSet.Contains(w)) continue;

                        float dist = Vector3.Distance(current.position, w.position);

                        if(dist <= 1.801f)
                        {
                            float accessScore = 0f;

                            if(foundNode.room != current.node.room)
                            {
                                if (current.node.accessToOtherNodes.ContainsKey(foundNode))
                                {
                                    NewNode.NodeAccess access = current.node.accessToOtherNodes[foundNode];

                                    if (access.accessType == NewNode.NodeAccess.AccessType.door)
                                    {
                                        accessScore = 3;
                                    }
                                    else if (access.accessType == NewNode.NodeAccess.AccessType.openDoorway)
                                    {
                                        accessScore = 1;
                                    }
                                    else accessScore = 8;
                                }
                                else accessScore = 12;
                            }

                            //The path weight from start to the neighbor
                            //We're adding the pre-calculated weight from the node of the entrance we just walked into, to the possible next entrance to (exterior/to -> interior/from)
                            float tentativeGScore = fromStartToThis[current] + dist + accessScore;

                            if (existingGroup != null)
                            {
                                PipeRoute existingRoute = existingGroup.routes.Find(item => item.w == w.id);

                                if (existingRoute != null)
                                {
                                    tentativeGScore = 1;
                                }
                            }

                            //Discover a new node- add to open set
                            //Game.Log("Pathfinder: Add " + neighbor.pathCoord + " to open set, count: " + openSet.Count);
                            openSet.Add(w);

                            //Path hasn't been added (assume infinity) if tentative score is lower then this is automatically the best until now.
                            if (!fromStartToThis.ContainsKey(w) || tentativeGScore < fromStartToThis[w])
                            {
                                if (!cameFrom.ContainsKey(w)) cameFrom.Add(w, current);
                                else cameFrom[w] = current;

                                if (!fromStartToThis.ContainsKey(w)) fromStartToThis.Add(w, tentativeGScore);
                                else fromStartToThis[w] = tentativeGScore;

                                float viaThis = fromStartToThis[w] + Vector3.Distance(w.node.position, to.node.position);
                                if (!fromStartViaThis.ContainsKey(w)) fromStartViaThis.Add(w, viaThis);
                                else fromStartViaThis[w] = viaThis;
                            }
                        }
                    }
                }
            }

            //Travel up or down
            for (int i = 0; i < 2; i++)
            {
                Vector3Int npUp = current.node.nodeCoord + new Vector3Int(0, 0, 1);
                if(i == 1) npUp = current.node.nodeCoord + new Vector3Int(0, 0, -1);
                NewNode foundNodeUp = null;

                if(PathFinder.Instance.nodeMap.TryGetValue(npUp, out foundNodeUp))
                {
                    NewWall w = foundNodeUp.walls.Find(item => item.wallOffset == current.wallOffset);

                    if(w != null && !w.preset.ignoreCullingRaycasts && w.preset.sectionClass != DoorPairPreset.WallSectionClass.entrance && w.preset.sectionClass != DoorPairPreset.WallSectionClass.windowLarge)
                    {
                        if (!closedSet.Contains(w) && !openSet.Contains(w))
                        {
                            float accessScore = 0f;

                            if (current.node.accessToOtherNodes.ContainsKey(foundNodeUp))
                            {
                                NewNode.NodeAccess access = current.node.accessToOtherNodes[foundNodeUp];

                                if (access.accessType == NewNode.NodeAccess.AccessType.verticalSpace)
                                {
                                    accessScore = 1;
                                }
                                else accessScore = 15;
                            }
                            else accessScore = 18;

                            //The path weight from start to the neighbor
                            //We're adding the pre-calculated weight from the node of the entrance we just walked into, to the possible next entrance to (exterior/to -> interior/from)
                            float tentativeGScore = fromStartToThis[current] + Vector3.Distance(current.position, w.position) + accessScore;

                            if (existingGroup != null)
                            {
                                PipeRoute existingRoute = existingGroup.routes.Find(item => item.w == w.id);

                                if (existingRoute != null)
                                {
                                    tentativeGScore = 0.5f;
                                }
                            }

                            //Discover a new node- add to open set
                            //Game.Log("Pathfinder: Add " + neighbor.pathCoord + " to open set, count: " + openSet.Count);
                            openSet.Add(w);

                            //Path hasn't been added (assume infinity) if tentative score is lower then this is automatically the best until now.
                            if (!fromStartToThis.ContainsKey(w) || tentativeGScore < fromStartToThis[w])
                            {
                                if (!cameFrom.ContainsKey(w)) cameFrom.Add(w, current);
                                else cameFrom[w] = current;

                                if (!fromStartToThis.ContainsKey(w)) fromStartToThis.Add(w, tentativeGScore);
                                else fromStartToThis[w] = tentativeGScore;

                                float viaThis = fromStartToThis[w] + Vector3.Distance(w.node.position, to.node.position);
                                if (!fromStartViaThis.ContainsKey(w)) fromStartViaThis.Add(w, viaThis);
                                else fromStartViaThis[w] = viaThis;
                            }
                        }
                    }
                }
            }

            openSet.Remove(current);
            closedSet.Add(current);

            calculationLoops++;

            if (calculationLoops > 9999)
            {
                return null;
            }
        }

        return null;
    }

    public bool IsLeftOf(NewWall one, NewWall two)
    {
        Matrix4x4 matrixTrans = Matrix4x4.identity;
        matrixTrans.SetTRS(two.position, Quaternion.Euler(two.localEulerAngles + two.node.room.transform.eulerAngles), Vector3.one);

        //Matrix4x4 version of Transform.InverseTransformPoint
        Vector3 localPosition = matrixTrans.inverse.MultiplyPoint3x4(one.position);

        if (localPosition.x < -0.1f)
        {
            return true;
        }

        return false;
    }

    public bool IsRightOf(NewWall one, NewWall two)
    {
        Matrix4x4 matrixTrans = Matrix4x4.identity;
        matrixTrans.SetTRS(two.position, Quaternion.Euler(two.localEulerAngles + two.node.room.transform.eulerAngles), Vector3.one);

        //Matrix4x4 version of Transform.InverseTransformPoint
        Vector3 localPosition = matrixTrans.inverse.MultiplyPoint3x4(one.position);

        if (localPosition.x > 0.1f)
        {
            return true;
        }

        return false;
    }

    public bool IsFrontOf(NewWall one, NewWall two)
    {
        Matrix4x4 matrixTrans = Matrix4x4.identity;
        matrixTrans.SetTRS(two.position, Quaternion.Euler(two.localEulerAngles + two.node.room.transform.eulerAngles), Vector3.one);

        //Matrix4x4 version of Transform.InverseTransformPoint
        Vector3 localPosition = matrixTrans.inverse.MultiplyPoint3x4(one.position);

        if (localPosition.z > 0.1f)
        {
            return true;
        }

        return false;
    }

    //Generate pipes for a new city
    public void GeneratePipes()
    {
        Dictionary<Interactable, NewWall> securityWallRefs = new Dictionary<Interactable, NewWall>();
        Dictionary<Interactable, PipeGroup> securityWireGroups = new Dictionary<Interactable, PipeGroup>();
        Dictionary<NewAddress, PipeGroup> surveillanceLinkGroups = new Dictionary<NewAddress, PipeGroup>();

        string seed = CityData.Instance.seed;

        foreach(NewAddress ad in CityData.Instance.addressDirectory)
        {
            if(ad.securityCameras.Count > 0 || ad.sentryGuns.Count > 0 || ad.otherSecurity.Count > 0 || ad.alarms.Count > 0)
            {
                Interactable sec = ad.GetBreakerSecurity();

                if(sec != null)
                {
                    PipeGroup currentGroup = null;
                    NewWall breakerWall = null;

                    //Find walls and get wire group
                    if(securityWireGroups.ContainsKey(sec))
                    {
                        currentGroup = securityWireGroups[sec];
                        breakerWall = securityWallRefs[sec];
                    }
                    else
                    {
                        currentGroup = new PipeGroup(PipeType.wire);
                        securityWireGroups.Add(sec, currentGroup);

                        //Find the wall
                        foreach(FurnitureLocation furn in sec.node.individualFurniture)
                        {
                            if(furn.furniture.classes[0].wallPiece)
                            {
                                //Get the wall matching rotation
                                breakerWall = sec.node.walls.Find(item => item.localEulerAngles.y + item.node.room.transform.eulerAngles.y == furn.angle);

                                if(breakerWall != null)
                                {
                                    securityWallRefs.Add(sec, breakerWall);
                                    //Game.Log("Found wall for security system");
                                    break;
                                }
                            }
                        }
                    }

                    //Connect cameras
                    foreach(Interactable cam in ad.securityCameras)
                    {
                        if(cam.node.walls.Count > 0)
                        {
                            NewWall startingWall = cam.node.walls[Toolbox.Instance.GetPsuedoRandomNumberContained(0, cam.node.walls.Count, seed, out seed)];
                            currentGroup.AddPipeRoute(startingWall, breakerWall, new int[] { 0, 6 }, new int[] { 5 });
                        }
                    }

                    //Connect sentries
                    foreach(Interactable sentry in ad.sentryGuns)
                    {
                        if (sentry.node.walls.Count > 0)
                        {
                            NewWall startingWall = sentry.node.walls[Toolbox.Instance.GetPsuedoRandomNumberContained(0, sentry.node.walls.Count, seed, out seed)];
                            currentGroup.AddPipeRoute(startingWall, breakerWall, new int[] { 0, 6 }, new int[] { 5 });
                        }
                    }

                    //Connect other
                    foreach (Interactable other in ad.otherSecurity)
                    {
                        if (other.node.walls.Count > 0)
                        {
                            NewWall startingWall = other.node.walls[Toolbox.Instance.GetPsuedoRandomNumberContained(0, other.node.walls.Count, seed, out seed)];
                            currentGroup.AddPipeRoute(startingWall, breakerWall, new int[] { 0, 6 }, new int[] { 5 });
                        }
                    }

                    //Connect alarms
                    foreach (Interactable alarm in ad.alarms)
                    {
                        if (alarm.node.walls.Count > 0)
                        {
                            if(alarm.preset.name == "AlarmSwitch")
                            {
                                //Get the wall matching rotation
                                NewWall startingWall = alarm.node.walls.Find(item => item.localEulerAngles.y + item.node.room.transform.eulerAngles.y == alarm.wEuler.y);
                                if(startingWall != null) currentGroup.AddPipeRoute(startingWall, breakerWall, 5, 5);
                            }
                            else
                            {
                                NewWall startingWall = alarm.node.walls.Find(item => item.localEulerAngles.y + item.node.room.transform.eulerAngles.y == alarm.wEuler.y);
                                if (startingWall != null) currentGroup.AddPipeRoute(startingWall, breakerWall, -1, 5);
                            }
                        }
                    }

                    //Connect security doors
                    Interactable doorBreaker = ad.GetBreakerSecurity();

                    if(doorBreaker != null)
                    {
                        foreach (Interactable door in sec.node.floor.securityDoors)
                        {
                            Interactable breaker = door.node.gameLocation.thisAsAddress.GetBreakerDoors();

                            if(breaker == doorBreaker)
                            {
                                if(door.node.walls.Count > 0)
                                {
                                    NewWall startingWall = door.node.walls[Toolbox.Instance.GetPsuedoRandomNumberContained(0, door.node.walls.Count, seed, out seed)];
                                    currentGroup.AddPipeRoute(startingWall, breakerWall, 0, 5);
                                }
                            }
                        }
                    }

                    //Connect the breaker box to the security room...
                    //if(ad.securityCameras.Count > 0 || ad.sentryGuns.Count > 0)
                    //{
                    //    bool foundSecurityRoom = false;

                    //    foreach(KeyValuePair<int, NewFloor> pair in ad.building.floors)
                    //    {
                    //        foreach(NewAddress ad2 in pair.Value.addresses)
                    //        {
                    //            if(ad2.company != null)
                    //            {
                    //                if(ad2.company.preset.controlsBuildingSurveillance)
                    //                {
                    //                    if(ad2.rooms.Count > 0)
                    //                    {
                    //                        NewRoom controlRoom = ad2.rooms.Find(item => !item.isNullRoom && item.entrances.Count > 0);

                    //                        if (controlRoom != null)
                    //                        {
                    //                            PipeGroup grp = null;

                    //                            if (surveillanceLinkGroups.ContainsKey(ad2))
                    //                            {
                    //                                grp = surveillanceLinkGroups[ad2];
                    //                            }
                    //                            else
                    //                            {
                    //                                grp = new PipeGroup(PipeType.wire2);
                    //                                surveillanceLinkGroups.Add(ad2, grp);
                    //                            }

                    //                            grp.AddPipeRoute(breakerWall, controlRoom.entrances[0].wall, 5, -1);
                    //                            foundSecurityRoom = true;
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }

                    //    NewRoom secRoom = null;

                    //    if(!foundSecurityRoom)
                    //    {
                    //        secRoom = ad.rooms.Find(item => item.roomType.name == "PrivateOffice");

                    //        if(secRoom != null)
                    //        {
                    //            if(secRoom.entrances.Count > 0)
                    //            {
                    //                PipeGroup newGrp = new PipeGroup(PipeType.wire2);
                    //                newGrp.AddPipeRoute(breakerWall, secRoom.entrances[0].wall, 5, -1);
                    //            }
                    //        }
                    //    }
                    //}
                }
            }
        }
    }

    [Button]
    public void GetWall()
    {
        foreach(NewAddress ad in CityData.Instance.addressDirectory)
        {
            foreach(NewRoom r in ad.rooms)
            {
                foreach(NewNode n in r.nodes)
                {
                    foreach (NewWall w in n.walls)
                    {
                        if(w.id == debugGetWall1)
                        {
                            Game.Log(w.id + ": " + w.position + ", " + w.localEulerAngles + w.node.room.transform.eulerAngles);
                            return;
                        }
                    }
                }
            }
        }
    }

    [Button]
    public void LeftRightCheck()
    {
        NewWall w1 = null;
        NewWall w2 = null;

        foreach (NewAddress ad in CityData.Instance.addressDirectory)
        {
            foreach (NewRoom r in ad.rooms)
            {
                foreach (NewNode n in r.nodes)
                {
                    foreach (NewWall w in n.walls)
                    {
                        if (w.id == debugGetWall1)
                        {
                            w1 = w;
                        }

                        if(w.id == debugGetWall2)
                        {
                            w2 = w;
                        }
                    }
                }
            }
        }

        if(w1 != null && w2 != null)
        {
            Game.Log("Is " + debugGetWall1 + " left of " + debugGetWall2 + ": " + IsLeftOf(w1, w2));
            Game.Log("Is " + debugGetWall1 + " right of " + debugGetWall2 + ": " + IsRightOf(w1, w2));
        }
    }
}
