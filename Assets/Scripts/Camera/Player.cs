﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using System.Linq;
using TMPro;
using UnityEditor;
using NaughtyAttributes;

public class Player : Human
{
    [Header("Player Attributes")]
    public bool fpsMode = false;
    public FirstPersonController fps;
    public CharacterController charController;
    public CapsuleCollider transitionDamageTrigger;
    public CameraController cam;
    public Transform camHeightParent;
    public Transform playerContainer;
    public Transform currentVehicle;
    public AirDuctGroup.AirDuctSection previousDuctSection;
    public AirDuctGroup.AirDuctSection currentDuctSection;

    public SceneRecorder sceneRecorder;

    [Header("Watch Alarm")]
    public bool setAlarmMode = false;
    public bool editingHours = true; //If false we are editing minutes
    private float setAlarmFlashCounter = 0.5f;
    public float alarm = 0f;
    private bool alarmFlash = true;
    public float setAlarmModeAfterDelay = 0f; //Set alarm mode after this delay

    //Time speed mode
    public float spendingTimeDelay = 0f;
    public bool spendingTimeMode = false;

    [Header("Telephone")]
    [NonSerialized]
    public Telephone answeringPhone;
    //private float hangUpDelay = 0f;
    [NonSerialized]
    public TelephoneController.PhoneCall activeCall;

    [Header("Audio")]
    public List<CanvasRenderer> footstepSoundObjects = new List<CanvasRenderer>();

    [Header("Player State")]
    public float crouchedTransition = 0f;
    public bool crouchTransitionActive = false;

    private int updateNodeSpace = 0; //Use this for updating the nearest node space every x frames

    //Tells ui indicators to display
    private float takeDamageIndicatorTimer = 0f;
    private float takeDamageDisplaySpeed = 2f;

    private float spawnProtection = 2f;

    private bool wasMoving = false;
    private int nearbyInteractableUpdate = 0;

    public float gasLevel = 0f;
    public float hurt = 0f;

    private Interactable bed;

    //Groundmap tiles in the vicinity (8 around + this)
    public List<CityTile> cityTilesInVicinity = new List<CityTile>();

    //Player owned keys
    //public List<NewDoor> playerKeyring = new List<NewDoor>();
    public List<Interactable> playerKeyringInt = new List<Interactable>(); //Version of above for interactables...

    //Force look at
    public bool forceLookAtActive = false;
    public Interactable forceLookAtInteractable = null;
    public float forceLookAtTime = 0f;
    private float lookAtTime = 0f;
    private float lookAtProgress = 0f;
    private Quaternion originalLookAtModRotationGlobal;

    //Controller transform transition
    public bool transitionActive = false;
    private float transitionTime = 0f;
    public float transitionProgress = 0f;

    [System.NonSerialized]
    public Interactable transitionInteractable;
    public PlayerTransitionPreset currentTransition;
    public PlayerTransitionPreset exitTransition;
    public Vector3 originalPlayerPosition;
    public Vector3 originalModPosition;
    public float originalPlayerHeight;
    public float originalCamHeight;
    public Vector3 startingLookPointWorldPosition;
    public bool transitionRecoilState = false; //Used to apply extra look motion
    private List<PlayerTransitionPreset.SFXSetting> soundsPlayed = new List<PlayerTransitionPreset.SFXSetting>();

    public Quaternion originalModRotationGlobal;
    public Quaternion originalModRotationLocal;

    public Vector3 additionalLookMultiplier = Vector3.one; //Additional multiplier applied after everything else (if disabled, this is 1)
    public float rollMultiplier = 1f;

    public bool transitionForceTime = false;
    public float transtionForcedTime = 0f;
    //private bool transitionPosCalulated = false;

    public Transform transitionLookAt = null;
    private bool movementOnTransitionComplete = true;
    private bool restoreHolsterOnTransitionComplete = true;
    public bool citizensArrestActive = false;
    public List<string> disabledActions = new List<string>(); //The player cannot perform actions matching these strings

    //Can use leaning to mimic getting damaged on punching with these variables
    public int forcedLeanState = 0; //Is added to normal lean state
    public float extraLeanSpeed = 0f;

    //Original Play controller stats
    public float normalStepOffset = 0.25f;
    public float airVentStepOffset = 0.25f;
    public Vector3 storedTransitionPosition;

    public float desiredWalkSpeed = 1f;
    public float desiredRunSpeed = 1f;

    private bool playerKOFadeOut = false;
    private bool paidFines = false;
    private float KOTime = 0f;
    private float KOTimePassed = 0f;
    private bool KORecovery = false;
    private bool dirtyDeath = false;
    private GameplayController.LoanDebt debtPayment;

    public bool pausedRememberPlayerMovement = true; //Save whether player movement is allowed on pause game.

    //Used for save state
    [System.NonSerialized]
    public Interactable hideInteractable;
    [System.NonSerialized]
    public int hideReference = 0;
    [System.NonSerialized]
    public Interactable phoneInteractable;
    [System.NonSerialized]
    public Interactable computerInteractable;
    [System.NonSerialized]
    public Interactable restrainedInteractable;
    [System.NonSerialized]
    public FirstPersonItemController.InventorySlot restrainedHandcuffsSlot;
    [System.NonSerialized]
    public Interactable searchInteractable;
    [System.NonSerialized]
    public Interactable genericActionInteractable;

    //Has the player been walking around a lot? If so then display the sprint prompt
    [System.NonSerialized]
    public int nodesTraversedWhileWalking = 0;

    [Header("Damage Block")]
    public float lastDamageAt = 0f;
    public Actor lastDmgFrom;

    [Header("Illegal State")]
    [System.NonSerialized]
    public float illegalActionTimer = 1f;

    public float seenProgress = 0f;
    public float seenProgressLag = 0f;

    public float persuedProgress = 0f;
    public float persuedProgressLag = 0f;

    [System.NonSerialized]
    public AudioController.LoopingSoundInfo trespassingSnapshot = null;
    [System.NonSerialized]
    public AudioController.LoopingSoundInfo combatSnapshot = null;
    [System.NonSerialized]
    public AudioController.LoopingSoundInfo syncMachineSnapshot = null;

    [System.NonSerialized]
    public float visibilityLag = 0f;

    private float stealthLag = 0f;
    public float seenIconLag = 0f;

    //public float avoidProgress = 0f;
    //private float avoidProgressLag = 0f;

    //public int evadeCitizenCount = 0;
    //public int avoidCitizenCount = 0;
    //private int prevEvadeCount = 0;
    //private int prevAvoidCount = 0;

    private int spotCheckTimer = 0;

    public bool playerKOInProgress = false;
    public bool isLockpicking = false;
    public bool isGrounded = true;
    private bool wasGrounded = false;

    public bool claimedAccidentCover = false; //Has the player claimed accident cover today?
    public List<int> foodHygeinePhotos = new List<int>();
    public List<int> sanitaryHygeinePhotos = new List<int>();
    public List<int> illegalOpsPhotos = new List<int>();

    public bool firstFrame = true;
    private bool lateFixedUpdate = false; //Used to check player out of world workaround

    //private bool updateCullingThisFrame = false; //Used so culling is only updated once per frame max

    [Header("Apartments")]
    public List<NewAddress> apartmentsOwned = new List<NewAddress>(); //Player-owned apartments

    Action updateCullingAction;
    Action updateStatusAction;

    public List<Actor> spottedByPlayer = new List<Actor>(); //Reference of actors visible to player and time remaining to display...
    public List<Actor> spottedWhileHiding = new List<Actor>(); //A list of exceptions; player is hidden from everyone but there people while hiding

    [System.NonSerialized]
    public Interactable hidingInteractable;

    //Events
    public delegate void TransitionCompleted(bool restoreTransform);
    public event TransitionCompleted OnTransitionCompleted;

    //Singleton pattern
    private static Player _instance;
    public static Player Instance { get { return _instance; } }

    //Collection caching
    private List<CityTile> requiredVicinity = new List<CityTile>();

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        normalStepOffset = charController.stepOffset;

        //Setup update action
        updateCullingAction += UpdateCulling;
    }

    void Start()
    {
        if(!SessionData.Instance.isFloorEdit) updateStatusAction += StatusController.Instance.ForceStatusCheck;

        //Set player height
        if(!Game.Instance.freeCam)
        {
            SetPlayerHeight(Player.Instance.GetPlayerHeightNormal());
        }

        //Set combat skill & heft
        SetCombatSkill(GameplayControls.Instance.playerCombatSkill);
        SetCombatHeft(GameplayControls.Instance.playerCombatHeft);

        //Force attirbutes updates throught the upgrade effects controller
        UpgradeEffectController.Instance.OnSyncDiskChange();

        SessionData.Instance.OnPauseChange += OnPauseChange;

        isPlayer = true; //Player flag
        humanID = 1; //Set ID as 1

        humility = 1f;
        emotionality = 0.5f;
        extraversion = 0.4f;
        agreeableness = 0.7f;
        conscientiousness = 1f;
        creativity = 0.5f;

        //Setup light measuring
        //Instantiate a render texture for light measuring
        //foreach(InterfaceControls.LightMeasuringPoint lm in InterfaceControls.Instance.playerLightMeasuring)
        //{
        //    lm.instantiatedRenderTex = (RenderTexture)Instantiate(PrefabControls.Instance.lightMeasureRenderTexture);
        //    lm.lightMeasuringCamera.targetTexture = lm.instantiatedRenderTex;
        //}

        //Set speeds to gameplay controls
        RestorePlayerMovementSpeed();
        UpdateSkinWidth();

        //Set visible
        visible = true;
        CityData.Instance.visibleActors.Add(this as Actor); //Add to list of visible

        SetFootwear(ShoeType.normal); //Set footwear
    }

    public void EnablePlayerMovement(bool val, bool updateCulling = true)
    {
        if (CutSceneController.Instance.cutSceneActive && val) val = false; //Don't allow during cut scenes ever

        Game.Log("Player: Movement: " + val);
        fps.enableMovement = val;

        //Force culling update
        if(updateCulling) UpdateCullingOnEndOfFrame();
    }

    public void EnablePlayerMouseLook(bool val, bool forceHideMouseOnDisable = false)
    {
        if (CutSceneController.Instance.cutSceneActive && val) val = false; //Don't allow during cut scenes ever

        bool showMouse = !val; //By default, show the mouse when mouse look is not enabled
        if (!val && forceHideMouseOnDisable) showMouse = false;

        if (!SessionData.Instance.isFloorEdit) Game.Log("Player: MouseLook: " + val + " show mouse: " + showMouse);
        fps.enableLook = val;

        InputController.Instance.SetCursorLock(val);
        InputController.Instance.SetCursorVisible(showMouse);
    }

    private void OnPauseChange(bool openDesktopMode)
    {
        if (InteractionController.Instance.interactionMode) return; //If in interaction mode, do nothing
        if (SessionData.Instance.isFloorEdit) return; //FlorEdit has it's own pause events

        //Trigger fps mode if previously paused
        if (SessionData.Instance.play)
        {
            EnablePlayerMovement(pausedRememberPlayerMovement);
            InterfaceController.Instance.SetDesktopMode(false, true);
        }
        else
        {
            pausedRememberPlayerMovement = fps.enableMovement;
            EnablePlayerMovement(false);

            if (openDesktopMode)
            {
                //Don't display the case board if we are editing an apartment
                bool displayCaseBoard = true;
                if (SessionData.Instance.isDecorEdit) displayCaseBoard = false;
                if (PlayerApartmentController.Instance.decoratingMode) displayCaseBoard = false;
                if (PlayerApartmentController.Instance.furniturePlacementMode) displayCaseBoard = false;

                InterfaceController.Instance.SetDesktopMode(true, displayCaseBoard);
            }
        }
    }

    //If force triggers is true, all On() triggers will fire
    public override void UpdateGameLocation(float feetOffset = 0f)
    {
        base.UpdateGameLocation(feetOffset);

        //Calculate air duct location
        if(inAirVent)
        {
            //Find nearest duct on node
            AirDuctGroup.AirDuctSection closest = null;
            float closestDist = 9999;

            if(currentNode != null)
            {
                foreach(AirDuctGroup.AirDuctSection ads in currentNode.airDucts)
                {
                    //Get the would-be world position of this
                    Vector3 ductPos = CityData.Instance.NodeToRealpos(ads.node.nodeCoord + new Vector3(0, 0, ads.level * 2f + 1f) / 6f) + new Vector3(0, InteriorControls.Instance.airDuctYOffset, 0);

                    float dist = Vector3.Distance(this.transform.position, ductPos);

                    if(dist < closestDist)
                    {
                        closest = ads;
                        closestDist = dist;
                    }
                }
            }

            if(closest != null)
            {
                if (closest.group != currentDuct)
                {
                    previousDuct = currentDuct;
                    currentDuct = closest.group;

                    OnDuctGroupChange();
                }

                if(closest != currentDuctSection)
                {
                    previousDuctSection = currentDuctSection;
                    currentDuctSection = closest;

                    Game.Log("Player: Found new closest duct section at node position " + closest.node.position + " (current node: " + currentNode.position+")");

                    OnDuctSectionChange();
                }
            }
        }

        BioScreenController.Instance.UpdateDecorEditButton();
    }

    //Triggered when the player changed air duct group
    public virtual void OnDuctGroupChange()
    {
        //UpdateCullingThisFrame();
        UpdateCullingOnEndOfFrame();
    }

    public void OnDuctSectionChange()
    {
        //Do vent discovery
        if (inAirVent)
        {
            Game.Log("Player: Duct section change map update...");

            List<AirDuctGroup.AirDuctSection> openSet = new List<AirDuctGroup.AirDuctSection>();
            List<Vector3> openSetPreviousOffset = new List<Vector3>();
            openSet.Add(currentDuctSection);
            openSetPreviousOffset.Add(Vector3.zero);

            List<AirDuctGroup.AirDuctSection> closedSet = new List<AirDuctGroup.AirDuctSection>();

            int safety = 200;

            while (openSet.Count > 0 && safety > 0)
            {
                AirDuctGroup.AirDuctSection current = openSet[0];
                Vector3 previousOffset = openSetPreviousOffset[0];

                //Get neighbors along with offsets
                List<Vector3Int> offsets;
                List<AirDuctGroup.AirVent> vents;
                List<AirDuctGroup.AirDuctSection> neighbors = current.GetNeighborSections(out offsets, out vents, out _);

                //Discover all vents neighboring this
                foreach(AirDuctGroup.AirVent vent in vents)
                {
                    vent.SetDiscovered(true);

                    //If player is on this node, also discover furniture within room
                    if(vent.room.explorationLevel < 2 && currentDuctSection == current)
                    {
                        vent.room.SetExplorationLevel(2);
                    }
                }

                //Expand into adjacent sections...
                for (int i = 0; i < neighbors.Count; i++)
                {
                    AirDuctGroup.AirDuctSection n = neighbors[i];
                    Vector3 o = offsets[i];

                    //Check this isn't behind a corner (if this isn't the start)
                    if(current != currentDuctSection)
                    {
                        //Offset must match the previous
                        if(o != previousOffset)
                        {
                            continue;
                        }
                    }

                    //Pass: add this to the open set
                    if(!openSet.Contains(n))
                    {
                        if(!closedSet.Contains(n))
                        {
                            openSet.Add(n);
                            openSetPreviousOffset.Add(o);
                        }
                    }
                }

                //Mark as discovered
                current.SetDiscovered(true);
                openSet.RemoveAt(0);
                openSetPreviousOffset.RemoveAt(0);
                safety--;
            }
        }
    }

    //Triggered when the player changes groundmap tile
    public override void OnCityTileChange()
    {
        base.OnCityTileChange();

        //Set previous location not present
        if(previousCityTile != null)
        {
            previousCityTile.SetPlayerPresentOnGroundmap(false);
        }

        //Set present
        if(currentCityTile != null)
        {
            currentCityTile.SetPlayerPresentOnGroundmap(true);
        }

        //Set vicinicty
        requiredVicinity.Clear();
        requiredVicinity.Add(currentCityTile);

        if(currentCityTile != null)
        {
            for (int i = 0; i < CityData.Instance.offsetArrayX8.Length; i++)
            {
                Vector2Int v2 = new Vector2Int(currentCityTile.cityCoord.x + CityData.Instance.offsetArrayX8[i].x, currentCityTile.cityCoord.y + CityData.Instance.offsetArrayX8[i].y);

                CityTile surrounding = null;

                if(CityData.Instance.cityTiles.TryGetValue(v2, out surrounding))
                {
                    requiredVicinity.Add(surrounding);
                }
            }
        }

        //Edit current vicinity
        for (int i = 0; i < cityTilesInVicinity.Count; i++)
        {
            CityTile tile = cityTilesInVicinity[i];

            if(requiredVicinity.Contains(tile))
            {
                requiredVicinity.Remove(tile);
            }
            else
            {
                tile.SetPlayerInVicinity(false);

                cityTilesInVicinity.RemoveAt(i);
                i--;
                continue;
            }
        }

        foreach(CityTile tile in requiredVicinity)
        {
            if (tile == null) continue;

            //Game.Log("New required vicinity: " + tile);

            tile.SetPlayerInVicinity(true);

            cityTilesInVicinity.Add(tile);
        }
    }

    public override void OnGameLocationChange(bool enableSocialSightings = true, bool forceDisableLocationMemory = false)
    {
        base.OnGameLocationChange();

        if (currentGameLocation != null && InterfaceController.Instance.locationText != null)
        {
            if(currentGameLocation.isCrimeScene)
            {
                MusicController.Instance.MusicTriggerCheck(MusicCue.MusicTriggerEvent.arriveAtCrimeScene);
            }

            //Trigger arrival
            if(currentGameLocation.evidenceEntry != null)
            {
                currentGameLocation.evidenceEntry.OnPlayerArrival();
            }

            //Set the location text
            if(currentGameLocation.thisAsAddress == null || (currentGameLocation.thisAsAddress.addressPreset != null && !currentGameLocation.thisAsAddress.addressPreset.disableLocationInformationDisplay))
            {
                InterfaceController.Instance.locationText.text = currentGameLocation.name;
                InterfaceController.Instance.DisplayLocationText(2.2f, false);

                //Arrived at route end!(Non node specific)
                if (MapController.Instance.playerRoute != null && !MapController.Instance.playerRoute.nodeSpecific && MapController.Instance.playerRoute.end.gameLocation == currentGameLocation)
                {
                    //Only do this if there are no existing headers
                    if (InterfaceController.Instance.gameHeaderQueue.Count <= 0)
                    {
                        //Display text to the route destination
                        if (MapController.Instance.playerRoute.destinationTextOverride != null)
                        {
                            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.gameHeader, 0, SessionData.Instance.CurrentTimeString(false, true) + ", " + MapController.Instance.playerRoute.destinationTextOverride.name);
                        }
                        else
                        {
                            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.gameHeader, 0, SessionData.Instance.CurrentTimeString(false, true) + ", " + currentGameLocation.name);
                        }
                    }

                    MapController.Instance.playerRoute.Remove();
                }
            }
        }

        if(SessionData.Instance.isDecorEdit)
        {
            InfoWindow decorWin = InterfaceController.Instance.activeWindows.Find(item => item.preset.name == "ApartmentDecor");

            if(decorWin != null)
            {
                decorWin.CloseWindow(false);
            }
        }
    }

    public override void OnBuildingChange()
    {
        base.OnBuildingChange();

        //Display previous building model on exit
        if (previousBuilding != null)
        {
            previousBuilding.SetDisplayBuildingModel(true, true);
        }

        UpdateCurrentBuildingModelVisibility();

        //Check for removal of fines...
        if(seenProgressLag <= 0f)
        {
            StatusController.Instance.FineEscapeCheck();
        }
    }

    [Button]
    public void UpdateCurrentBuildingModelVisibility()
    {
        if (currentRoom != null && currentRoom.building != null)
        {
            //Draw building model from outside
            if (inAirVent)
            {
                currentRoom.building.SetDisplayBuildingModel(false, false);
            }
            else
            {
                BuildingPreset.InteriorFloorSetting floorSetting = currentRoom.building.preset.GetFloorSetting(currentRoom.floor.floor, currentRoom.floor.layoutIndex);
                bool outside = currentRoom.IsOutside();

                if (currentRoom.preset.drawBuildingModel || outside)
                {
                    if(outside && floorSetting.forceHideModelsOutside.Count > 0)
                    {
                        currentRoom.building.SetDisplayBuildingModel(true, true, floorSetting.forceHideModelsOutside);
                    }
                    else
                    {
                        currentRoom.building.SetDisplayBuildingModel(true, true, null);
                    }
                }
                else if (currentRoom.windows.Count > 0 && floorSetting != null)
                {
                    currentRoom.building.SetDisplayBuildingModel(true, false, floorSetting.forceHideModels);
                }
                else
                {
                    currentRoom.building.SetDisplayBuildingModel(false, false);
                }

                //Selectively hide extra depending on room type and floor setting
                if (floorSetting != null && floorSetting.forceHideModelsInRooms.Count > 0)
                {
                    List<BuildingPreset.ForceHideModelsForRoom> hideModels = floorSetting.forceHideModelsInRooms.FindAll(item => item.roomConfig == currentRoom.preset);

                    foreach (BuildingPreset.ForceHideModelsForRoom r in hideModels)
                    {
                        currentRoom.building.SelectivelyHideModels(r.forceHideModels);
                    }
                }
            }
        }
        //Hide current building (unless in outdoor room)
        else if (currentBuilding != null)
        {
            //Draw building model from outside
            if (inAirVent)
            {
                currentBuilding.SetDisplayBuildingModel(false, false);
            }
            else if (currentRoom.preset.drawBuildingModel || currentRoom.IsOutside())
            {
                currentBuilding.SetDisplayBuildingModel(true, true, null);
            }
            else if (currentRoom.windows.Count > 0)
            {
                currentBuilding.SetDisplayBuildingModel(true, false, currentBuilding.preset.GetFloorSetting(currentRoom.floor.floor, currentRoom.floor.layoutIndex).forceHideModels);
            }
            else currentBuilding.SetDisplayBuildingModel(false, false);
        }
    }

    public override void OnNodeChange()
    {
        //Add as human traveller
        //currentNode.AddHumanTraveller(this, null, out _);

        base.OnNodeChange();

        //Update ambient zones
        AudioController.Instance.UpdateAmbientZonesOnEndOfFrame();

        //Count nodes traversed while walking in aid of sprint promt
        if (!isRunning && !isCrouched && !stealthMode && brokenLeg <= 0f && drunk <= 0f && currentGameLocation != home && !CutSceneController.Instance.cutSceneActive && transform.position.y > CityControls.Instance.basementWaterLevel)
        {
            nodesTraversedWhileWalking++;

            if (nodesTraversedWhileWalking > 30)
            {
                if (Game.Instance.displayExtraControlHints)
                {
                    ControlsDisplayController.Instance.DisplayControlIcon(InteractablePreset.InteractionKey.sprint, "Sprint", InterfaceControls.Instance.controlIconDisplayTime);
                }

                nodesTraversedWhileWalking = 0;
            }
        }

        //Arrived at route end!(Node specific)
        if (MapController.Instance.playerRoute != null && MapController.Instance.playerRoute.nodeSpecific && MapController.Instance.playerRoute.end == currentNode)
        {
            Game.Log("Interface: Reached route destination...");

            //Only do this if there are no existing headers
            if (InterfaceController.Instance.gameHeaderQueue.Count <= 0)
            {
                //Display text to the route destination
                if (MapController.Instance.playerRoute.destinationTextOverride != null)
                {
                    InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.gameHeader, 0, SessionData.Instance.CurrentTimeString(false, true) + ", " + MapController.Instance.playerRoute.destinationTextOverride.name);
                }
                else
                {
                    InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.gameHeader, 0, SessionData.Instance.CurrentTimeString(false, true) + ", " + currentGameLocation.name);
                }
            }

            MapController.Instance.playerRoute.Remove();
        }

        if (!SessionData.Instance.isFloorEdit)
        {
            if (MapController.Instance.playerRoute != null)
            {
                MapController.Instance.playerRoute.start = currentNode;
                MapController.Instance.playerRoute.UpdatePathData();

                if (MapController.Instance.isActiveAndEnabled)
                {
                    MapController.Instance.playerRoute.UpdateDrawnRoute();
                }
            }
        }

        //Update rain particle spawn positions
        //if(RainParticleController.Instance != null && RainParticleController.Instance.isActiveAndEnabled)
        //{
        //    RainParticleController.Instance.UpdateValidSpawnNodes();
        //}

        //Update particle systems
        for (int i = 0; i < SessionData.Instance.particleSystems.Count; i++)
        {
            InteractableController ic = SessionData.Instance.particleSystems[i];

            if (ic == null)
            {
                SessionData.Instance.particleSystems.RemoveAt(i);
                i--;
            }
            else ic.UpdateParticleSystemDistance();
        }

        //Update current interaction sounds...
        foreach (KeyValuePair<InteractablePreset.InteractionKey, InteractionController.InteractionSetting> pair in InteractionController.Instance.currentInteractions)
        {
            if (pair.Value.audioEvent != null)
            {
                if (pair.Value.newUIRef != null)
                {
                    if (pair.Value.newUIRef.soundIndicator != null)
                    {
                        pair.Value.newUIRef.soundIndicator.UpdateCurrentEvent();
                    }
                }
            }
        }

        //Update object spawning distances
        ObjectPoolingController.Instance.UpdateObjectRanges();

        //Do AI instant persuit check
        if(isMoving && currentNode != null)
        {
            if(currentRoom != null)
            {
                try
                {
                    foreach(Actor a in currentRoom.currentOccupants)
                    {
                        if(a.ai != null)
                        {
                            try
                            {
                                a.ai.InstantPersuitCheck(Player.Instance);
                            }
                            catch
                            {

                            }
                        }
                    }
                }
                catch
                {

                }
            }
        }
    }

    private void OnDisable()
    {
        //Set last player positions so they will trigger an update when next triggered
        previousNode = null;
    }

    //Update function - every frame
    private void Update()
    {
        //Interaction- must be in play mode, and not in interaction mode
        if (SessionData.Instance.play)
        {
            if(currentRoom != null)
            {
                if(gasLevel < currentRoom.gasLevel)
                {
                    gasLevel += Time.deltaTime / 2f;
                    gasLevel = Mathf.Clamp01(gasLevel);
                    StatusController.Instance.ForceStatusCheck();
                }
                else if(gasLevel > currentRoom.gasLevel)
                {
                    gasLevel -= Time.deltaTime / 2f;
                    gasLevel = Mathf.Clamp01(gasLevel);
                    StatusController.Instance.ForceStatusCheck();
                }
            }

            if (!CutSceneController.Instance.cutSceneActive)
            {
                //Spawn protection
                if (spawnProtection > 0f)
                {
                    spawnProtection -= Time.deltaTime;
                }

                //Remove sync disk install
                if (Player.Instance.syncDiskInstall > 0f)
                {
                    Player.Instance.AddSyncDiskInstall(-Time.deltaTime / 7.6f);
                }

                //Remove player blinded
                if (Player.Instance.blinded > 0f)
                {
                    Player.Instance.AddBlinded(-Time.deltaTime / 7f);
                }

                //Animate indicator
                if (takeDamageIndicatorTimer > 0f)
                {
                    //Fade in
                    if (takeDamageIndicatorTimer > 0.9f)
                    {
                        InterfaceController.Instance.takeDamageIndicatorImg.canvasRenderer.SetAlpha(Mathf.Clamp01(1f - (takeDamageIndicatorTimer * 10f)));
                    }
                    //Fade out
                    else
                    {
                        InterfaceController.Instance.takeDamageIndicatorImg.canvasRenderer.SetAlpha(Mathf.Clamp01(takeDamageIndicatorTimer / 0.9f));
                    }

                    takeDamageIndicatorTimer -= Time.deltaTime * takeDamageDisplaySpeed; //Display for 1/2 a second

                    if (takeDamageIndicatorTimer <= 0f)
                    {
                        takeDamageIndicatorTimer = 0f; //Reset to 0
                        InterfaceController.Instance.takeDamageIndicatorImg.gameObject.SetActive(false);
                    }
                }

                //Heal over time
                if (currentHealth < GetCurrentMaxHealth() && !StatusController.Instance.disabledRecovery)
                {
                    //Calc recovery
                    float addHealth = (recoveryRate * StatusController.Instance.recoveryRateMultiplier) * Time.deltaTime * SessionData.Instance.currentTimeMultiplier * 0.01f;
                    AddHealth(addHealth);
                }

                //Graduate max speeds
                if (fps.m_WalkSpeed < desiredWalkSpeed)
                {
                    fps.m_WalkSpeed += Time.deltaTime * 2f;
                    fps.m_WalkSpeed = Mathf.Min(desiredWalkSpeed, fps.m_WalkSpeed);

                    movementWalkSpeed = fps.m_WalkSpeed; //In the player script this isn't functional and is used for runtime visual reference only here
                }
                else if (fps.m_WalkSpeed > desiredWalkSpeed)
                {
                    fps.m_WalkSpeed -= Time.deltaTime * 2f;
                    fps.m_WalkSpeed = Mathf.Max(desiredWalkSpeed, fps.m_WalkSpeed);

                    movementWalkSpeed = fps.m_WalkSpeed; //In the player script this isn't functional and is used for runtime visual reference only here
                }

                if (fps.m_RunSpeed < desiredRunSpeed)
                {
                    fps.m_RunSpeed += Time.deltaTime * 2f;
                    fps.m_RunSpeed = Mathf.Min(desiredRunSpeed, fps.m_RunSpeed);

                    movementRunSpeed = fps.m_RunSpeed; //In the player script this isn't functional and is used for runtime visual reference only here
                }
                else if (fps.m_RunSpeed > desiredRunSpeed)
                {
                    fps.m_RunSpeed -= Time.deltaTime * 2f;
                    fps.m_RunSpeed = Mathf.Max(desiredRunSpeed, fps.m_RunSpeed);

                    movementRunSpeed = fps.m_RunSpeed; //In the player script this isn't functional and is used for runtime visual reference only here
                }

                //Update closest window
                AudioController.Instance.updateClosestWindowTicker++;

                if (AudioController.Instance.updateClosestWindowTicker > AudioController.Instance.updateClosestWindow)
                {
                    AudioController.Instance.UpdateClosestWindowAndDoor();
                    AudioController.Instance.UpdateClosestExteriorWall();
                    AudioController.Instance.updateClosestWindowTicker = 0;
                }

                AudioController.Instance.updateMixingTicker++;

                if (AudioController.Instance.updateMixingTicker > AudioController.Instance.updateMixing)
                {
                    AudioController.Instance.UpdateMixing();
                    AudioController.Instance.updateMixingTicker = 0;
                }

                AudioController.Instance.updateAmbientZonesTimer += Time.deltaTime;

                if (AudioController.Instance.updateAmbientZonesTimer > 1f)
                {
                    AudioController.Instance.UpdateAmbientZonesOnEndOfFrame();
                    AudioController.Instance.updateAmbientZonesTimer = 0f;
                }

                ObjectPoolingController.Instance.updateObjectRangesTimer += Time.deltaTime;

                if (ObjectPoolingController.Instance.updateObjectRangesTimer > 5f)
                {
                    ObjectPoolingController.Instance.UpdateObjectRanges();
                    ObjectPoolingController.Instance.updateObjectRangesTimer = 0;
                }

                //Update node space
                updateNodeSpace--;

                if (updateNodeSpace <= 0)
                {
                    updateNodeSpace = 10; //Every 10 frames?
                    UpdateCurrentNodeSpace();
                }

                //Transform transition
                if (transitionActive)
                {
                    ExecuteTransition();
                }
                else if (forceLookAtActive)
                {
                    ExecuteForceLookAt();
                }
                else
                {
                    //Look @ interacting
                    if (InteractionController.Instance.currentInteractable != null)
                    {
                        if (InteractionController.Instance.interactionLookProgress < 1f)
                        {
                            InteractionController.Instance.interactionLookProgress += Time.deltaTime;
                        }

                        Vector3 relativePos = Vector3.zero;

                        if (InteractionController.Instance.currentInteractable.lookAtTarget != null)
                        {
                            relativePos = InteractionController.Instance.currentInteractable.lookAtTarget.position - CameraController.Instance.cam.transform.position;
                        }
                        else
                        {
                            relativePos = InteractionController.Instance.currentInteractable.transform.position - CameraController.Instance.cam.transform.position;
                        }

                        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);

                        //Attempt to avoid Look Rotation Viewing Vector Is Zero errors:
                        if (rotation != CameraController.Instance.cam.transform.rotation) CameraController.Instance.cam.transform.rotation = Quaternion.Lerp(CameraController.Instance.cam.transform.rotation, rotation, InteractionController.Instance.interactionLookProgress);
                    }
                }

                if ((InteractionController.Instance.lockedInInteraction == null || InteractionController.Instance.carryingObject != null) && !inAirVent)
                {
                    //Adjust player height suiting stealth mode or normal
                    if (isCrouched && crouchedTransition < 1f)
                    {
                        crouchedTransition += Time.deltaTime * 2.6f;
                        crouchedTransition = Mathf.Clamp01(crouchedTransition);
                        crouchTransitionActive = true;

                        //Change character height
                        SetPlayerHeight(Mathf.Lerp(Player.Instance.GetPlayerHeightNormal(), Player.Instance.GetPlayerHeightCrouched(), GameplayControls.Instance.crouchHeightCurve.Evaluate(crouchedTransition)));

                        //Position camera relative to char height
                        float newRatio = Mathf.SmoothStep(GameplayControls.Instance.cameraHeightNormal, GameplayControls.Instance.cameraHeightCrouched, crouchedTransition);
                        SetCameraHeight(newRatio);
                    }
                    else if (!isCrouched && crouchedTransition > 0f)
                    {
                        crouchedTransition -= Time.deltaTime * 2.2f;
                        crouchedTransition = Mathf.Clamp01(crouchedTransition);
                        crouchTransitionActive = true;

                        //Change character height
                        SetPlayerHeight(Mathf.Lerp(Player.Instance.GetPlayerHeightNormal(), Player.Instance.GetPlayerHeightCrouched(), GameplayControls.Instance.crouchHeightCurve.Evaluate(crouchedTransition)));

                        //Position camera relative to char height
                        float newRatio = Mathf.SmoothStep(GameplayControls.Instance.cameraHeightNormal, GameplayControls.Instance.cameraHeightCrouched, crouchedTransition);
                        SetCameraHeight(newRatio);
                    }
                    else if (crouchTransitionActive)
                    {
                        crouchTransitionActive = false;
                    }
                }

                //Spotted by player check
                spotCheckTimer++;

                if (spotCheckTimer >= GameplayControls.Instance.playerSpotUpdateEveryXFrame)
                {
                    spotCheckTimer = 0;
                    SightingCheck(0, false);

                    //Update sound check for current interactions
                    foreach (KeyValuePair<InteractablePreset.InteractionKey, InteractionController.InteractionSetting> pair in InteractionController.Instance.currentInteractions)
                    {
                        if (pair.Value.audioEvent != null)
                        {
                            if (pair.Value.newUIRef != null)
                            {
                                if (pair.Value.newUIRef.soundIndicator != null)
                                {
                                    pair.Value.newUIRef.soundIndicator.UpdateCurrentEvent();
                                }
                            }
                        }
                    }
                }

                //Reduce spotted by player after x time...
                if (spottedByPlayer.Count > 0)
                {
                    List<Actor> endOfGrace = new List<Actor>();
                    List<Actor> toRemove = new List<Actor>();

                    for (int i = 0; i < spottedByPlayer.Count; i++)
                    {
                        Actor a = spottedByPlayer[i];

                        if (a.spottedGraceTime > 0f)
                        {
                            a.spottedGraceTime -= Time.deltaTime;

                            //Grace period removed faster if on different floor to the player...
                            if (a.currentNodeCoord.z != Player.Instance.currentNodeCoord.z)
                            {
                                a.spottedGraceTime -= Time.deltaTime;
                            }
                        }
                        else
                        {
                            a.spottedState -= Time.deltaTime / GameplayControls.Instance.spottedFadeSpeed;

                            if (a.spottedState <= 0f)
                            {
                                a.spottedGraceTime = 0f;
                                a.spottedState = 0f;

                                if (a.ai != null)
                                {
                                    //Game.Log("Spotted state == 0");
                                    a.ai.TriggerReactionIndicator(); //Remove indicator
                                }

                                spottedByPlayer.RemoveAt(i);
                                i--;

                                InterfaceController.Instance.footstepAudioIndicator.UpdateCurrentEvent();

                                //Update current interaction sounds...
                                foreach (KeyValuePair<InteractablePreset.InteractionKey, InteractionController.InteractionSetting> pair in InteractionController.Instance.currentInteractions)
                                {
                                    if (pair.Value.audioEvent != null)
                                    {
                                        if (pair.Value.newUIRef != null)
                                        {
                                            if (pair.Value.newUIRef.soundIndicator != null)
                                            {
                                                pair.Value.newUIRef.soundIndicator.UpdateCurrentEvent();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //Seen being illegal...
                seenProgress = 0f; //The progress of being seen...

                //If others can see
                if (witnessesToIllegalActivity.Count > 0)
                {
                    if (seenIconLag < 1f)
                    {
                        seenIconLag += Time.deltaTime * 4f;
                        seenIconLag = Mathf.Clamp01(seenIconLag);

                        InterfaceControls.Instance.seenRenderer.SetAlpha(seenIconLag);
                    }

                    foreach (Actor act in witnessesToIllegalActivity)
                    {
                        //if (act.isMachine) continue; //Don't include machines in this...

                        if (act.seesIllegal.ContainsKey(this as Actor))
                        {
                            seenProgress = Mathf.Max(seenProgress, act.seesIllegal[this as Actor]);
                        }

                        //if(act.ai != null)
                        //{
                        //    if (act.ai.investigationGoal != null)
                        //    {
                        //        //avoidProgress = Mathf.Max(avoidProgress, Mathf.Clamp01(act.ai.investigationGoal.priority * 0.1f));
                        //    }
                        //}
                    }
                }
                else
                {
                    if (seenProgress > 0f) seenProgress = 0;

                    if (seenIconLag > 0f || firstFrame)
                    {
                        seenIconLag -= Time.deltaTime * 3f;
                        seenIconLag = Mathf.Clamp01(seenIconLag);
                        if (InterfaceControls.Instance.seenRenderer != null) InterfaceControls.Instance.seenRenderer.SetAlpha(seenIconLag);
                    }
                }

                if (seenProgressLag != seenProgress || firstFrame)
                {
                    if (seenProgressLag < seenProgress)
                    {
                        seenProgressLag += Time.deltaTime * 0.5f;
                        seenProgressLag = Mathf.Min(seenProgressLag, seenProgress);
                    }
                    else if (seenProgressLag > seenProgress)
                    {
                        seenProgressLag -= Time.deltaTime * 0.25f;
                        seenProgressLag = Mathf.Max(seenProgressLag, seenProgress);
                    }

                    seenProgressLag = Mathf.Clamp(seenProgressLag, 0, 1f);

                    //Check for removal of fines...
                    if (!SessionData.Instance.isFloorEdit)
                    {
                        if (seenProgressLag <= 0f)
                        {
                            StatusController.Instance.FineEscapeCheck();
                        }

                        //Set pulsate speed to max seen progress
                        InterfaceController.Instance.movieBarJuice.pulsateSpeed = Mathf.Lerp(0.5f, 2.5f, seenProgressLag);

                        //Change colour of crosshairs
                        InterfaceControls.Instance.lightOrbFillImg.color = Color.Lerp(Color.white, InterfaceControls.Instance.interactionControlTextColourIllegal, seenProgressLag * 1.25f);
                        InterfaceControls.Instance.lightOrbOutline.color = InterfaceControls.Instance.lightOrbFillImg.color;
                        InterfaceControls.Instance.seenImg.color = InterfaceControls.Instance.lightOrbFillImg.color;
                        InterfaceControls.Instance.seenJuice.elements[0].originalColour = InterfaceControls.Instance.seenImg.color;
                    }
                }

                if (persuedBy.Count > 0)
                {
                    persuedProgress = 1f;

                    if (AudioController.Instance.threatLoop != null)
                    {
                        Game.Log("Debug: Stopping threat loop");
                        AudioController.Instance.StopSound(AudioController.Instance.threatLoop, AudioController.StopType.fade, "Persued");
                        AudioController.Instance.threatLoop = null;
                    }
                }
                else
                {
                    persuedProgress = seenProgress;
                }

                if (persuedProgressLag != persuedProgress || firstFrame)
                {
                    if (persuedProgressLag < persuedProgress && persuedBy.Count <= 0)
                    {
                        persuedProgressLag += Time.deltaTime * 1f;
                        persuedProgressLag = Mathf.Clamp01(persuedProgressLag);
                    }
                    else if (persuedProgressLag > persuedProgress)
                    {
                        persuedProgressLag -= Time.deltaTime * 0.5f;
                        persuedProgressLag = Mathf.Clamp01(persuedProgressLag);
                    }

                    if (persuedProgressLag > 0f)
                    {
                        //if (AudioController.Instance.threatLoop == null && persuedBy.Count <= 0 && !playerKOInProgress)
                        //{
                        //    Game.Log("Debug: Starting threat loop");
                        //    List<AudioController.FMODParam> p = new List<AudioController.FMODParam>();
                        //    p.Add(new AudioController.FMODParam { name = "Threat", value = persuedProgressLag });
                        //    AudioController.Instance.threatLoop = AudioController.Instance.Play2DLooping(AudioControls.Instance.threatLoop, p);
                        //}

                        if (AudioController.Instance.threatLoop != null)
                        {
                            //Game.Log("Debug: Passing threat loop value: " + persuedProgressLag);
                            AudioController.Instance.threatLoop.audioEvent.setParameterByName("Threat", persuedProgressLag);
                        }
                    }
                    else if (AudioController.Instance.threatLoop != null)
                    {
                        Game.Log("Debug: Stopping threat loop");
                        AudioController.Instance.StopSound(AudioController.Instance.threatLoop, AudioController.StopType.fade, "Persued");
                        AudioController.Instance.threatLoop = null;
                    }

                    if (InterfaceController.Instance.movieBarTop != null) InterfaceController.Instance.movieBarTop.sizeDelta = new Vector2(InterfaceController.Instance.movieBarTop.sizeDelta.x, persuedProgressLag * InterfaceControls.Instance.movieBarHeight);
                    if (InterfaceController.Instance.movieBarBottom != null) InterfaceController.Instance.movieBarBottom.sizeDelta = new Vector2(InterfaceController.Instance.movieBarTop.sizeDelta.x, persuedProgressLag * InterfaceControls.Instance.movieBarHeight);
                }

                //Set crosshairs
                if (visibilityLag != overallVisibility || firstFrame)
                {
                    if (visibilityLag < overallVisibility)
                    {
                        visibilityLag += Time.deltaTime;
                        visibilityLag = Mathf.Min(visibilityLag, overallVisibility);
                    }
                    else if (visibilityLag > overallVisibility)
                    {
                        visibilityLag -= Time.deltaTime;
                        visibilityLag = Mathf.Max(visibilityLag, overallVisibility);
                    }

                    visibilityLag = Mathf.Clamp(visibilityLag, 0, 1f);

                    //Set orb to represent visibility
                    if (InterfaceControls.Instance.lightOrbFillImg != null) InterfaceControls.Instance.lightOrbFillImg.canvasRenderer.SetAlpha(visibilityLag * 0.9f);
                }

                //Set vis orb scale
                if (stealthLag < 1f && (stealthMode || witnessesToIllegalActivity.Count > 0) || firstFrame)
                {
                    stealthLag += Time.deltaTime * 2f;
                    stealthLag = Mathf.Clamp01(stealthLag);

                    float ssize = Mathf.LerpUnclamped(InterfaceControls.Instance.lightOrbSize.x, InterfaceControls.Instance.lightOrbSize.y, InterfaceControls.Instance.stealthModeOrbSizeTransitionIn.Evaluate(stealthLag));

                    if (InterfaceControls.Instance.lightOrbRect != null)
                    {
                        InterfaceControls.Instance.lightOrbRect.sizeDelta = new Vector2(ssize, ssize);
                        InterfaceControls.Instance.lightOrbOutline.rectTransform.sizeDelta = InterfaceControls.Instance.lightOrbRect.sizeDelta;
                    }
                }
                else if (stealthLag > 0f && (!stealthMode && witnessesToIllegalActivity.Count <= 0) || firstFrame)
                {
                    stealthLag -= Time.deltaTime * 2f;
                    stealthLag = Mathf.Clamp01(stealthLag);

                    float ssize = Mathf.LerpUnclamped(InterfaceControls.Instance.lightOrbSize.x, InterfaceControls.Instance.lightOrbSize.y, InterfaceControls.Instance.stealthModeOrbSizeTransitionOut.Evaluate(stealthLag));

                    if (InterfaceControls.Instance.lightOrbRect != null)
                    {
                        InterfaceControls.Instance.lightOrbRect.sizeDelta = new Vector2(ssize, ssize);
                        InterfaceControls.Instance.lightOrbOutline.rectTransform.sizeDelta = InterfaceControls.Instance.lightOrbRect.sizeDelta;
                    }
                }

                //Delay before being able to set alarm mode
                if (setAlarmModeAfterDelay > 0f)
                {
                    setAlarmModeAfterDelay -= Time.deltaTime;

                    if (setAlarmModeAfterDelay <= 0f)
                    {
                        SetSettingAlarmMode(true);
                    }
                }

                //Spend time delay
                if (spendingTimeDelay > 0f)
                {
                    spendingTimeDelay -= Time.deltaTime;

                    if (spendingTimeDelay <= 0f)
                    {
                        SetSpendingTimeMode(true);
                    }
                }

                //Player KO
                if (playerKOInProgress)
                {
                    //When faded to black
                    if (InterfaceController.Instance.fade >= 1f && !playerKOFadeOut)
                    {
                        //Reset logic uses so AI will not continue persuit
                        List<Actor> illegalSighted = new List<Actor>(witnessesToIllegalActivity);

                        foreach (Actor act in illegalSighted)
                        {
                            if (act.ai != null)
                            {
                                act.ai.CancelCombat();
                            }
                        }

                        //Cancel action
                        citizensArrestActive = false;

                        if (!KORecovery)
                        {
                            ReturnFromTransform(immediate: true);

                            if (!paidFines)
                            {
                                if(!dirtyDeath)
                                {
                                    //Pay fines
                                    StatusController.Instance.PayActiveFines();
                                }
                                else
                                {
                                    if(debtPayment != null)
                                    {
                                        if(GameplayController.Instance.money >= debtPayment.GetRepaymentAmount())
                                        {
                                            GameplayController.Instance.DebtPayment(CityData.Instance.companyDirectory.Find(item => item.companyID == debtPayment.companyID));
                                        }
                                        else
                                        {
                                            GameplayController.Instance.ShortDebtPayment(CityData.Instance.companyDirectory.Find(item => item.companyID == debtPayment.companyID), GameplayController.Instance.money);
                                        }
                                    }
                                    else
                                    {
                                        //Pay a bunch of money
                                        GameplayController.Instance.AddMoney(-Mathf.Max(Mathf.RoundToInt(GameplayController.Instance.money * 0.5f), 200), true, "Mugged");
                                    }
                                }

                                //Remove persuers
                                List<Actor> pur = new List<Actor>(persuedBy);

                                foreach (Actor p in pur)
                                {
                                    p.RemoveFromSeesIllegal(this, 0f);
                                }

                                paidFines = true;
                            }

                            //Attempt awaken at home
                            if (dirtyDeath)
                            {
                                //Send player to a random basement room
                                List<NewRoom> chosenRoom = CityData.Instance.roomDirectory.FindAll(item => item.preset.muggingAwakenRoom);

                                if (chosenRoom.Count > 0)
                                {
                                    bed = null;
                                    Teleport(FindSafeTeleport(chosenRoom[Toolbox.Instance.Rand(0, chosenRoom.Count)]), null);
                                }
                                else
                                {
                                    //Send player to hospital bed
                                    bed = GameplayController.Instance.hospitalBeds[Toolbox.Instance.Rand(0, GameplayController.Instance.hospitalBeds.Count)];
                                    Teleport(bed.node, null);
                                }
                            }
                            else if (UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.awakenAtHome) > 0f && home != null)
                            {
                                FurnitureLocation furnBed = Toolbox.Instance.FindFurnitureWithinGameLocation(home, InteriorControls.Instance.bed, out _); //Bedroom is assigned on out

                                if (furnBed != null)
                                {
                                    bed = furnBed.integratedInteractables[0];
                                    Teleport(bed.node, null);
                                }
                                else
                                {
                                    //Send player to hospital bed
                                    bed = GameplayController.Instance.hospitalBeds[Toolbox.Instance.Rand(0, GameplayController.Instance.hospitalBeds.Count)];
                                    Teleport(bed.node, null);
                                }
                            }
                            else
                            {
                                //Send player to hospital bed
                                bed = GameplayController.Instance.hospitalBeds[Toolbox.Instance.Rand(0, GameplayController.Instance.hospitalBeds.Count)];
                                Teleport(bed.node, null);
                            }

                            if(bed != null)
                            {
                                InteractablePreset.InteractionAction action = bed.preset.GetActions().Find(item => item.action == RoutineControls.Instance.sleep);
                                InteractionController.Instance.currentLookingAtInteractable = bed.controller;
                                bed.OnInteraction(action, Player.Instance); //Activate by simulating input
                            }

                            if(dirtyDeath)
                            {
                                SetHealth(GetCurrentMaxHealth() * 0.25f);
                            }
                            else
                            {
                                //Reset to half health
                                SetHealth(GetCurrentMaxHealth() * 0.5f);
                                AddBlackedOut(-1f);
                                AddBrokenLeg(-1f);
                            }

                            AddBleeding(-1f);
                            AddPoisoned(-1f, null);
                            AddBlinded(-1);
                        }
                        else
                        {
                            ResetHealthToMaximum(); //Give max health
                            AddBlackedOut(-1f);
                            AddBrokenLeg(-1f);
                            AddBleeding(-1f);
                            AddBruised(-1f);
                            AddHeadache(-1f);
                            AddBlackEye(-1f);
                            AddSick(-1f);
                            AddPoisoned(-1f, null);
                            AddBlinded(-1);
                        }

                        //Stop active AI
                        ClearSeesIllegal();

                        //Stop all actions
                        InteractionController.Instance.UpdateInteractionText();

                        playerKOFadeOut = true;
                        Game.Log("Player: Teleport to bed after KO...", 1);
                    }
                    else if (playerKOFadeOut && KOTimePassed < KOTime)
                    {
                        if (SessionData.Instance.currentTimeSpeed != SessionData.TimeSpeed.veryFast)
                        {
                            //Speed up time
                            SessionData.Instance.SetTimeSpeed(SessionData.TimeSpeed.veryFast);

                            //Display fast forward arrow
                            InterfaceControls.Instance.fastForwardArrow.gameObject.SetActive(true);

                            //Display progress
                            Game.Log("Player: Display KO progress bar...");
                            InteractionController.Instance.SetInteractionAction(0f, KOTime, 1f, "ko", false, false, null, false);

                            if (!KORecovery)
                            {
                                //Display message
                                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "ko"), InterfaceControls.Icon.passedOut);
                                SessionData.Instance.TutorialTrigger("ko");
                            }
                        }

                        if (!SessionData.Instance.play)
                        {
                            SessionData.Instance.ResumeGame();
                        }

                        KOTimePassed += (float)SessionData.Instance.gameTimePassedThisFrame;
                        //Game.Log("Player: KO Time passed: " + KOTimePassed + " / " + GameplayControls.Instance.koTimePass);

                        //Set bars
                        for (int i = 0; i < InteractionController.Instance.spawnedProgressControllers.Count; i++)
                        {
                            LockpickProgressController lpc = InteractionController.Instance.spawnedProgressControllers[i];
                            lpc.SetAmount(KOTimePassed);
                        }

                        if (KOTimePassed >= KOTime)
                        {
                            //Normal time
                            SessionData.Instance.SetTimeSpeed(SessionData.TimeSpeed.normal);

                            //Disapl fast forward arrow
                            InterfaceControls.Instance.fastForwardArrow.gameObject.SetActive(false);

                            //Fade out
                            InterfaceController.Instance.Fade(0f, 6f);

                            //Manually remove progress bar
                            while (InteractionController.Instance.spawnedProgressControllers.Count > 0)
                            {
                                Destroy(InteractionController.Instance.spawnedProgressControllers[0].gameObject);
                                InteractionController.Instance.spawnedProgressControllers.RemoveAt(0);
                            }

                            InterfaceControls.Instance.actionInteractionDisplay.gameObject.SetActive(false);

                            //Complete progress bar
                            InteractionController.Instance.CompleteInteractionAction();

                            if (KORecovery || UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.reduceMedicalCosts) >= 1f || (UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.accidentCover) > 0f && !Player.Instance.claimedAccidentCover))
                            {
                                ResetHealthToMaximum(); //Give max health
                                AddNourishment(1f);
                                AddHydration(1f);
                                AddHygiene(1f);
                                AddEnergy(1f);

                                AddBlackedOut(-1f);
                                AddBrokenLeg(-1f);
                                AddBleeding(-1f);
                                AddBruised(-1f);
                                AddHeadache(-1f);
                                AddBlackEye(-1f);
                                AddSick(-1f);
                                AddStarchAddiction(-1f);
                                AddDrunk(-1f);
                                AddPoisoned(-1f, null);
                                AddBlinded(-1);
                            }
                            else
                            {
                                //Set detained in building
                                if(!dirtyDeath) StatusController.Instance.SetDetainedInBuilding(Player.Instance.currentBuilding, true);

                                //Add numbness
                                AddNumb(0.75f);

                                //25% chance of headache
                                if (Toolbox.Instance.Rand(0f, 1f) <= 0.25f)
                                {
                                    AddHeadache(0.5f);
                                }

                                //25% chance of bruising
                                if (Toolbox.Instance.Rand(0f, 1f) <= 0.25f)
                                {
                                    AddBruised(0.5f);
                                }
                            }
                        }
                    }
                    else if (playerKOFadeOut && InterfaceController.Instance.fade <= 0f)
                    {
                        Game.Log("Player: Wake up from KO", 1);
                        playerKOInProgress = false;
                        InteractionController.Instance.UpdateNearbyInteractables();

                        SessionData.Instance.SetEnablePause(true);

                        if(Toolbox.Instance.IsStoryMissionActive(out _, out _) && ChapterController.Instance.currentPart < 31)
                        {
                            KORecovery = true;
                        }
                        else
                        {
                            if(dirtyDeath)
                            {
                                KORecovery = false;
                            }
                            else
                            {
                                if (UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.accidentCover) > 0f && !Player.Instance.claimedAccidentCover)
                                {
                                    KORecovery = true;
                                    Player.Instance.claimedAccidentCover = true;
                                    GameplayController.Instance.AddMoney(Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.accidentCover)), true, "Accident cover");
                                }
                                else if (UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.reduceMedicalCosts) >= 1f)
                                {
                                    KORecovery = true;
                                }
                            }
                        }

                        //Complete recovery
                        if (KORecovery)
                        {
                            ClearAllDisabledActions();
                            KORecovery = false;
                        }
                        else if(bed != null)
                        {
                            bed.SetCustomState2(true, Player.Instance, false);

                            //Choice...
                            InteractionController.Instance.SetDialog(true, bed);
                        }

                        //Trigger location header text
                        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.gameHeader, 0, SessionData.Instance.CurrentTimeString(false, true) + ", " + SessionData.Instance.LongDateString(SessionData.Instance.gameTime, true, true, true, true, true, false, false, true) + ", " + Player.Instance.currentGameLocation.name);

                        //Disable sleep/get up
                        SetActionDisable("Get In", false);
                        SetActionDisable("Get Up", false);
                        SetActionDisable("Enter", false);
                        SetActionDisable("Sleep", false);
                        SetActionDisable("Sleep...", false);

                        InteractionController.Instance.UpdateInteractionText();
                    }
                }

                //Drive conversation
                if (inConversation)
                {
                    UpdateConversation();
                }

                firstFrame = false;
            }

            //Reduce hurt value
            if(hurt > 0f)
            {
                hurt -= Time.deltaTime * 0.35f; //It takes ~3 seconds to recover from a maximum 'hurt' value
                hurt = Mathf.Clamp01(hurt);

                SessionData.Instance.exposure.active = true;
                SessionData.Instance.exposure.fixedExposure.value = Mathf.Min(-hurt * 0.6f, StatusController.Instance.exposureAmount);
            }
            else if (SessionData.Instance.exposure.IsActive() && StatusController.Instance.exposureAmount == 0f)
            {
                SessionData.Instance.exposure.active = false;
            }
        }
        else if(playerKOInProgress)
        {
            if(!MainMenuController.Instance.mainMenuActive)
            {
                SessionData.Instance.ResumeGame();
            }
        }

        //Face direction arrow if player route is active
        if (MapController.Instance != null && MapController.Instance.playerRoute != null)
        {
            //Cancel if invalid route
            if(MapController.Instance.playerRoute.pathData == null || MapController.Instance.playerRoute.pathData.accessList == null || MapController.Instance.playerRoute.pathData.accessList.Count <= 0)
            {
                Game.LogError("Player route is invalid!");
                //MapController.Instance.RemovePlayerRoute();
            }
            else
            {
                //Get x nodes ahead of current postion
                NewNode.NodeAccess ahead = MapController.Instance.playerRoute.pathData.accessList[Mathf.Min(3, MapController.Instance.playerRoute.pathData.accessList.Count - 1)];

                //World position of this
                Vector3 dir = ahead.worldAccessPoint - Player.Instance.transform.position;
                dir.y = 0;
                Quaternion facingQuat = Quaternion.LookRotation(dir, Vector3.up);
                MapController.Instance.directionalArrow.rotation = Quaternion.Slerp(MapController.Instance.directionalArrow.rotation, facingQuat, 6f * SessionData.Instance.currentTimeMultiplier * Time.deltaTime);

                //Get angle difference
                Vector3 angleDiff = this.transform.eulerAngles - MapController.Instance.directionalArrow.eulerAngles;

                //The Y angle is what we're most interested in...
                if (Mathf.Abs(angleDiff.y) > 45f)
                {
                    MapController.Instance.directionalArrowDesiredFade = Mathf.Clamp01((Mathf.Abs(angleDiff.y) - 45f) / 20f);
                }
                else MapController.Instance.directionalArrowDesiredFade = 0f;

                //Set alpha
                if (MapController.Instance.directionalArrowAlpha < MapController.Instance.directionalArrowDesiredFade)
                {
                    MapController.Instance.directionalArrowAlpha += 1.33f * Time.deltaTime;
                    MapController.Instance.directionalArrowAlpha = Mathf.Clamp01(MapController.Instance.directionalArrowAlpha);
                    MapController.Instance.arrowMaterial.SetFloat("_Alpha", MapController.Instance.directionalArrowAlpha);
                }
                else if (MapController.Instance.directionalArrowAlpha > MapController.Instance.directionalArrowDesiredFade)
                {
                    MapController.Instance.directionalArrowAlpha -= 1.33f * Time.deltaTime;
                    MapController.Instance.directionalArrowAlpha = Mathf.Clamp01(MapController.Instance.directionalArrowAlpha);
                    MapController.Instance.arrowMaterial.SetFloat("_Alpha", MapController.Instance.directionalArrowAlpha);
                }
            }
        }

        //Watch set alarm
        if(setAlarmMode)
        {
            if(alarmFlash)
            {
                setAlarmFlashCounter -= Time.deltaTime;

                if(setAlarmFlashCounter <= 0f)
                {
                    if(SessionData.Instance.newWatchTimeText != null) SessionData.Instance.newWatchTimeText.gameObject.SetActive(false);
                    alarmFlash = false;
                }
            }
            else if(!alarmFlash)
            {
                setAlarmFlashCounter += Time.deltaTime;

                if (setAlarmFlashCounter >= 0.5f)
                {
                    if (SessionData.Instance.newWatchTimeText != null) SessionData.Instance.newWatchTimeText.gameObject.SetActive(true);
                    alarmFlash = true;
                }
            }

            //Game.Log(setAlarmFlashCounter);
        }

        //Spending time mode: Listen for alarm
        if(spendingTimeMode && !playerKOInProgress)
        {
            if(SessionData.Instance.gameTime >= alarm)
            {
                SetSpendingTimeMode(false);

                //Play alarm sound
                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.watchAlarm, this, currentNode, lookAtThisTransform.position);

                //Bring up watch
                BioScreenController.Instance.SelectSlot(FirstPersonItemController.Instance.slots.Find(item => item.isStatic == FirstPersonItemController.InventorySlot.StaticSlot.watch));
            }
        }
    }

    private void FixedUpdate()
    {
        lateFixedUpdate = true;
    }

    private void LateUpdate()
    {
        if (lateFixedUpdate)
        {
            lateFixedUpdate = false;
            fps.PlayerOutOfWorldCheck();
        }
    }

    //Check gamelocation etc when the player moves
    public void UpdateMovementPhysics(bool forceUpdateBeforeGameStart = false)
    {
        if (!forceUpdateBeforeGameStart && !SessionData.Instance.startedGame) return; //Don't do this if the game hasn't started...

        //Update location
        UpdateGameLocation();

        //Update isMoving bool
        isMoving = fps.isMoving;

        if (isMoving && !fps.m_IsWalking)
        {
            //Do this once
            if (!isRunning)
            {
                isRunning = true;
                nodesTraversedWhileWalking = 0; //Reset this
                if(InterfaceController.Instance.footstepAudioIndicator != null) InterfaceController.Instance.footstepAudioIndicator.UpdateCurrentEvent();

                //Refresh actions as some may depend on running
                if (InteractionController.Instance.currentLookingAtInteractable != null)
                {
                    InteractionController.Instance.currentLookingAtInteractable.interactable.UpdateCurrentActions();
                }
            }
        }
        //Do this once
        else if (isRunning)
        {
            isRunning = false;
            if (InterfaceController.Instance.footstepAudioIndicator != null) InterfaceController.Instance.footstepAudioIndicator.UpdateCurrentEvent();

            //Refresh actions as some may depend on running
            if (InteractionController.Instance.currentLookingAtInteractable != null)
            {
                InteractionController.Instance.currentLookingAtInteractable.interactable.UpdateCurrentActions();
            }
        }

        isGrounded = fps.m_CharacterController.isGrounded;

        if (isGrounded != wasGrounded)
        {
            wasGrounded = isGrounded;

            //Refresh actions as some may depend on being grounded
            if (InteractionController.Instance.currentLookingAtInteractable != null)
            {
                InteractionController.Instance.currentLookingAtInteractable.interactable.UpdateCurrentActions();
            }
        }

        //Nearby interactables
        if (isMoving != wasMoving)
        {
            wasMoving = isMoving;

            if (!isMoving)
            {
                InteractionController.Instance.UpdateNearbyInteractables();
            }
            else
            {
                InteractionController.Instance.ClearNearbyInteractables();
            }
        }
        else if (!isMoving)
        {
            nearbyInteractableUpdate++;

            if (nearbyInteractableUpdate >= 10)
            {
                nearbyInteractableUpdate = 0;
                InteractionController.Instance.UpdateNearbyInteractables();
            }
        }
    }

    public void ExecuteTransition()
    {
        float transTime = currentTransition.transitionTime;

        if (transitionForceTime)
        {
            transTime = transtionForcedTime;
            Game.Log("Player: Force transition time: " + transTime);
        }

        //Game.Log("Player: Transition active: " + transitionTime + "/" + transTime);

        if (transitionTime <= transTime)
        {
            transitionTime += Time.deltaTime;

            if (transTime <= 0f)
            {
                transitionProgress = 1f; //If instance make sure this loop is run once
            }
            else transitionProgress = Mathf.Clamp01(transitionTime / transTime);

            //Change character height
            float desiredHeight = GetPlayerHeightNormal() * currentTransition.playerHeightMP;
            if (isCrouched && currentTransition.factorInCrouching) desiredHeight = GetPlayerHeightCrouched() * currentTransition.playerHeightMP;
            float newHeight = Mathf.LerpUnclamped(originalPlayerHeight, desiredHeight, currentTransition.heightCurve.Evaluate(transitionProgress));
            SetPlayerHeight(newHeight, false);

            //Position camera relative to char height
            float desiredRatio = GameplayControls.Instance.cameraHeightNormal * currentTransition.CamHeightMP;
            if (isCrouched && currentTransition.factorInCrouching) desiredRatio = GameplayControls.Instance.cameraHeightCrouched * currentTransition.CamHeightMP;
            float newRatio = Mathf.LerpUnclamped(originalCamHeight, desiredRatio, currentTransition.camHeightCurve.Evaluate(transitionProgress));
            SetCameraHeight(newRatio);

            //The desired end position
            Vector3 desiredPosition = transform.position;

            //Use the postion relative to the interactable
            if (currentTransition.useXMovement || currentTransition.useYMovement || currentTransition.useZMovement)
            {
                //Get desired movement
                Vector3 relativeMovement = Vector3.zero;

                if (currentTransition.useXMovement) relativeMovement.x = currentTransition.playerXCurve.Evaluate(transitionProgress);

                if (currentTransition.useYMovement)
                {
                    relativeMovement.y = currentTransition.playerYCurve.Evaluate(transitionProgress);

                    ////Account for chaning player height in Y position
                    //float playerYDiff = charController.height * 0.5f;
                    //relativeMovement.y += playerYDiff;
                    //Game.Log("Add height diff: " + playerYDiff);
                }

                if (currentTransition.useZMovement) relativeMovement.z = currentTransition.playerZCurve.Evaluate(transitionProgress);

                if (currentTransition.invertZPositionBasedOnRelativePlayerZ || currentTransition.invertYPositionBasedOnRelativePlayerY || currentTransition.invertXPositionBasedOnRelativePlayerX)
                {
                    //Which side is the player on?
                    //Convert player world position to local position relative to the door
                    Vector3 localPos = Vector3.zero;

                    if(transitionInteractable != null && transitionInteractable.controller != null) localPos = transitionInteractable.controller.transform.InverseTransformPoint(transform.position);

                    //Game.Log("RelPos: " + localPos);

                    if (currentTransition.invertZPositionBasedOnRelativePlayerZ && localPos.z < 0)
                    {
                        relativeMovement.z *= -1f;
                    }

                    if (currentTransition.invertYPositionBasedOnRelativePlayerY && localPos.y < 0)
                    {
                        relativeMovement.y *= -1f;
                    }

                    if (currentTransition.invertXPositionBasedOnRelativePlayerX && localPos.x < 0)
                    {
                        relativeMovement.x *= -1f;
                    }
                }

                if (currentTransition.transitionRelativity == PlayerTransitionPreset.TransitionPosition.relativeToInteractable && transitionInteractable != null && transitionInteractable.controller != null)
                {
                    desiredPosition = transitionInteractable.controller.transform.TransformPoint(relativeMovement);
                }
                else if (currentTransition.transitionRelativity == PlayerTransitionPreset.TransitionPosition.relativeToPlayer)
                {
                    //desiredPosition = originalPlayerPosition + relativeMovement;

                    //This will transform the point from the original transform location
                    Matrix4x4 m = Matrix4x4.TRS(originalModPosition, originalModRotationGlobal, Vector3.one);

                    //Apply the position modifier: Add the look position transformed into world space
                    desiredPosition = m.MultiplyPoint3x4(relativeMovement) - originalPlayerPosition;

                    desiredPosition += originalPlayerPosition;
                }

                //Transition from player's existing position
                if (currentTransition.transitionFromExistingPosition)
                {
                    desiredPosition = Vector3.Lerp(originalPlayerPosition, desiredPosition, currentTransition.positionTransitionCurve.Evaluate(transitionProgress));
                }

                //Transition to a stored transtion position
                if (currentTransition.transitionToSavedReturnPosition)
                {
                    Game.Log("Player: Transitioning to the stored transition postion: " + storedTransitionPosition);
                    desiredPosition = Vector3.Lerp(desiredPosition, storedTransitionPosition, currentTransition.positionTransitionCurve.Evaluate(transitionProgress));
                }

                //Move the player
                this.transform.position = desiredPosition;
            }

            //Rotate player
            if (currentTransition.useXLook || currentTransition.useYLook || currentTransition.useZLook)
            {
                //Vector3 relCharPos = Vector3.zero;
                //Vector3 relCamPos = Vector3.zero;
                Vector3 relModifierPos = Vector3.zero;

                Vector3 characterLookPosition = Vector3.zero;

                Quaternion modRotation = Quaternion.identity;

                //Use X and Z to choose the look at position for the character
                if (currentTransition.useXLook) characterLookPosition.x = currentTransition.playerXLookCurve.Evaluate(transitionProgress) * currentTransition.lookMovementMultiplier * additionalLookMultiplier.x;
                if (currentTransition.useYLook) characterLookPosition.y = currentTransition.playerYLookCurve.Evaluate(transitionProgress) * currentTransition.lookMovementMultiplier * additionalLookMultiplier.y;
                if (currentTransition.useZLook) characterLookPosition.z = currentTransition.playerZLookCurve.Evaluate(transitionProgress) * currentTransition.lookMovementMultiplier * additionalLookMultiplier.z;

                //Apply recoil state curves
                if (transitionRecoilState)
                {
                    characterLookPosition.x += currentTransition.playerXRecoilLookCurve.Evaluate(transitionProgress) * currentTransition.lookMovementMultiplier * additionalLookMultiplier.x;
                    characterLookPosition.y += currentTransition.playerYRecoilLookCurve.Evaluate(transitionProgress) * currentTransition.lookMovementMultiplier * additionalLookMultiplier.y;
                    characterLookPosition.z += currentTransition.playerZRecoilLookCurve.Evaluate(transitionProgress) * currentTransition.lookMovementMultiplier * additionalLookMultiplier.z;
                }

                if (currentTransition.lookRelativity == PlayerTransitionPreset.TransitionPosition.relativeToInteractable && transitionInteractable != null && transitionInteractable.controller != null)
                {
                    Vector3 plusOffset = Vector3.zero;

                    if (transitionInteractable.controller != null)
                    {
                        if(transitionInteractable.isActor == null || (!transitionInteractable.isActor.isStunned && !transitionInteractable.isActor.isDead))
                        {
                            plusOffset = transitionInteractable.controller.lockedInTransformOffset;
                        }

                        //Add bounds difference
                        if(transitionInteractable.controller.coll != null)
                        {
                            plusOffset += transitionInteractable.controller.coll.bounds.center - transitionInteractable.controller.transform.position;
                        }
                    }

                    //World position of the look at point
                    relModifierPos = transitionInteractable.controller.transform.TransformPoint(characterLookPosition + plusOffset);

                    //Relative to the current camera mod position
                    relModifierPos -= camHeightParent.position;

                    //Use look rotation to find rotation
                    modRotation = Quaternion.LookRotation(relModifierPos, Vector3.up);
                }
                //Else use relative to player
                else
                {
                    //This is the world position of the forward point at the time the transition was started
                    relModifierPos = startingLookPointWorldPosition;

                    //This will transform the point from the original transform location
                    Matrix4x4 m = Matrix4x4.TRS(originalModPosition, originalModRotationGlobal, Vector3.one);

                    //Apply the position modifier: Add the look position transformed into world space
                    relModifierPos += m.MultiplyPoint3x4(characterLookPosition) - camHeightParent.position;

                    //But it needs to be all be relative so minus original position
                    relModifierPos = m.inverse.MultiplyPoint3x4(relModifierPos);

                    //Use look rotation to find local rotation
                    modRotation = Quaternion.LookRotation(relModifierPos, Vector3.up);
                }

                if (currentTransition.applyCameraRoll)
                {
                    //Apply camera roll
                    modRotation *= Quaternion.Euler(new Vector3(0, 0, currentTransition.cameraRoll.Evaluate(transitionProgress) * currentTransition.rollMultiplier * rollMultiplier));
                }

                if (currentTransition.lookRelativity == PlayerTransitionPreset.TransitionPosition.relativeToInteractable)
                {
                    //Use transition from existing
                    if (currentTransition.transitionFromExistingMouse)
                    {
                        modRotation = Quaternion.Slerp(originalModRotationGlobal, modRotation, currentTransition.mouseTransitionCurve.Evaluate(transitionProgress));
                    }

                    camHeightParent.transform.rotation = modRotation;
                }
                else
                {
                    //Use transition from existing
                    if (currentTransition.transitionFromExistingMouse)
                    {
                        modRotation = Quaternion.Slerp(originalModRotationLocal, modRotation, currentTransition.mouseTransitionCurve.Evaluate(transitionProgress));
                    }

                    //TODO: Fix this, it's broken :(
                    //camHeightParent.transform.localRotation = modRotation;
                }
            }

            if (currentTransition.retainMovementControl)
            {
                //Manually perform look roation here so it will be added to the above
                fps.m_MouseLook.LookRotation(fps.transform, fps.m_Camera.transform);
            }

            //Reset the step offset localpos of the camera
            fps.m_Camera.transform.localPosition = Vector3.Lerp(fps.m_Camera.transform.localPosition, Vector3.zero, transitionProgress);

            //VFX
            if (currentTransition.useChromaticAberration)
            {
                //Turn on
                if (!SessionData.Instance.chromaticAberration.IsActive())
                {
                    SessionData.Instance.chromaticAberration.active = true;
                }

                SessionData.Instance.chromaticAberration.intensity.value = currentTransition.chromaticAberrationCurve.Evaluate(transitionProgress) + StatusController.Instance.chromaticAbberationAmount;
            }

            if (currentTransition.useGain)
            {
                //Turn on
                if (!SessionData.Instance.lgg.gain.overrideState)
                {
                    SessionData.Instance.lgg.gain.overrideState = true;
                }

                SessionData.Instance.lgg.gain.value = new Vector4(1, 1, 1, currentTransition.gainCurve.Evaluate(transitionProgress));
            }

            //SFX
            if (currentTransition.sfx.Count > 0)
            {
                foreach (PlayerTransitionPreset.SFXSetting sfx in currentTransition.sfx)
                {
                    if (soundsPlayed.Contains(sfx)) continue;

                    //Play sound
                    if (transitionProgress >= sfx.atProgress)
                    {
                        AudioController.Instance.PlayWorldOneShot(sfx.soundEvent, this, currentNode, lookAtThisTransform.position);
                        soundsPlayed.Add(sfx);
                    }
                }
            }

            //Reset camera roll
            if (currentTransition.resetCameraRoll)
            {
                Quaternion resetRoll = transform.rotation;
                resetRoll.x = 0;
                resetRoll.z = 0;
                transform.rotation = Quaternion.Lerp(transform.rotation, resetRoll, Mathf.Clamp01((transitionProgress - 0.9f) * 10f));
            }
        }

        //Transition complete
        if(transitionTime >= transTime)
        {
            //Make sure camera roll is reset
            if (currentTransition.resetCameraRoll)
            {
                Quaternion resetRoll = transform.rotation;
                resetRoll.x = 0;
                resetRoll.z = 0;
                transform.rotation = resetRoll;
            }

            ConvertModifierMovementToPlayerMovement(currentTransition.resetCameraRoll);

            //Reset VFX
            if(StatusController.Instance.chromaticAbberationAmount <= 0f) SessionData.Instance.chromaticAberration.active = false;
            SessionData.Instance.lgg.gain.overrideState = false;

            transitionActive = false;
            OnTransitionComplete();
        }
    }

    //Convert modifier movement to proper player movement
    public void ConvertModifierMovementToPlayerMovement(bool resetCamRoll = true)
    {
        //Convert camera modifier to real positions...
        //Convert X to camera
        Vector3 modEuler = camHeightParent.transform.localEulerAngles;
        float roll = 0f;
        if (!resetCamRoll) roll = modEuler.z;

        fps.m_Camera.transform.localRotation *= Quaternion.Euler(modEuler.x, 0, roll);

        //Convert Y to character
        transform.localRotation *= Quaternion.Euler(0, modEuler.y, 0);

        //Reset the cam height modifier
        camHeightParent.transform.localRotation = Quaternion.identity;

        //Update the mouse look as the player has been artificially shifted
        fps.m_MouseLook.Init(fps.transform, fps.m_Camera.transform);
    }

    //Convert player mouse look to modifier movement
    public void ConvertPlayerMovementToModifierMovement()
    {
        //Convert camera X to modifier X
        Vector3 modEuler = fps.m_Camera.transform.localEulerAngles;
        camHeightParent.transform.localRotation *= Quaternion.Euler(modEuler.x, 0, 0);

        //Reset the cam pos
        fps.m_Camera.transform.localRotation = Quaternion.identity;

        //Update the mouse look as the player has been artificially shifted
        fps.m_MouseLook.Init(fps.transform, fps.m_Camera.transform);
    }

    public void ForceLookAt(Interactable interactable, float time)
    {
        if (transitionActive) return; //Don't do this during a transition otherwise the camera will be fighting over what to look at!

        forceLookAtInteractable = interactable;
        forceLookAtTime = time;
        forceLookAtActive = true;
        originalLookAtModRotationGlobal = camHeightParent.transform.rotation;
        lookAtTime = 0f;
        lookAtProgress = 0f;

        ConvertPlayerMovementToModifierMovement();
        ExecuteForceLookAt();
    }

    public void ExecuteForceLookAt()
    {
        if (lookAtTime <= forceLookAtTime)
        {
            lookAtTime += Time.deltaTime; //Add progress

            if (forceLookAtTime <= 0f)
            {
                lookAtProgress = 1f; //If instant make sure this loop is run once
            }
            else lookAtProgress = Mathf.Clamp01(lookAtTime / forceLookAtTime); //Normalized progress

            //Rotate player
            Vector3 lookPosition = Vector3.zero;
            Quaternion modRotation = Quaternion.identity;

            Vector3 plusOffset = Vector3.zero;

            //If an actor, look at their look@ position
            if (forceLookAtInteractable.isActor != null && forceLookAtInteractable.isActor.lookAtThisTransform != null)
            {
                lookPosition = forceLookAtInteractable.isActor.lookAtThisTransform.position;
            }
            else if (forceLookAtInteractable.controller != null)
            {
                if (forceLookAtInteractable.controller.coll != null)
                {
                    lookPosition = forceLookAtInteractable.controller.coll.bounds.center;
                }
                else if (forceLookAtInteractable.controller.altColl != null)
                {
                    lookPosition = forceLookAtInteractable.controller.altColl.bounds.center;
                }
                else if (forceLookAtInteractable.controller.meshes.Count > 0)
                {
                    lookPosition = forceLookAtInteractable.controller.meshes[0].bounds.center;
                }
                else lookPosition = forceLookAtInteractable.GetWorldPosition();
            }
            else lookPosition = forceLookAtInteractable.controller.transform.position;

            //Use look rotation to find rotation
            modRotation = Quaternion.LookRotation(lookPosition - camHeightParent.position, Vector3.up);

            //Use transition from existing
            modRotation = Quaternion.Slerp(camHeightParent.transform.rotation, modRotation, InputController.Instance.nearestLookAtCurve.Evaluate(lookAtProgress));

            camHeightParent.transform.rotation = modRotation;

            //Manually perform look roation here so it will be added to the above
            fps.m_MouseLook.LookRotation(fps.transform, fps.m_Camera.transform);

            //Reset the step offset localpos of the camera
            fps.m_Camera.transform.localPosition = Vector3.Lerp(fps.m_Camera.transform.localPosition, Vector3.zero, lookAtProgress);
        }

        //Transition complete
        if(lookAtTime >= forceLookAtTime)
        {
            //Make sure camera roll is reset
            Quaternion resetRoll = transform.rotation;
            resetRoll.x = 0;
            resetRoll.z = 0;
            transform.rotation = resetRoll;

            ConvertModifierMovementToPlayerMovement();

            forceLookAtActive = false;
            fps.InitialiseController(true);
        }
    }

    //New version that uses transition presets
    public void TransformPlayerController(PlayerTransitionPreset newEnterTransition, PlayerTransitionPreset newExitTransition, Interactable newInteractable, Transform newLookAt, bool newForceMovementOnEnd = false, bool forceTime = false, float forcedTime = 0f, bool useAdditionalLookMultiplier = false, Vector3 newAdditionalLookMultiplier = new Vector3(), float newRollMultiplier = 1f, bool writeReturnPosition = true)
    {
        Game.Log("Player: Enter transition: " + newEnterTransition.name + " at pos " + transform.position, 1);

        //If skipping time, then restore normal time
        if(spendingTimeMode) SetSpendingTimeMode(false);

        transitionInteractable = newInteractable;
        currentTransition = newEnterTransition;
        exitTransition = newExitTransition;

        //Raycast position check
        if (currentTransition.transitionRelativity == PlayerTransitionPreset.TransitionPosition.relativeToPlayer)
        {
            Vector3 desiredPosition = transform.TransformPoint(currentTransition.playerXCurve.Evaluate(1f), currentTransition.playerYCurve.Evaluate(1f), currentTransition.playerZCurve.Evaluate(1f));

            //Do a raycast check
            if (currentTransition.raycastCheck)
            {
                Vector3 rayDir = desiredPosition - transform.position;
                float dist = Vector3.Distance(desiredPosition, transform.position);

                Ray check = new Ray(transform.position, rayDir);
                RaycastHit[] hits = Physics.RaycastAll(check, dist);

                bool pass = true;

                foreach (RaycastHit hit in hits)
                {
                    //Game.Log(hit.transform.name);

                    //Check hit isn't part of this object or player
                    InteractableController inter = hit.transform.GetComponentInParent<InteractableController>();

                    if (inter != null)
                    {
                        if(inter.interactable == newInteractable)
                        {
                            Game.Log(hit.transform.name + " OK");
                            continue;
                        }
                    }

                    Game.Log(hit.transform.name + " FAIL");
                    pass = false;
                    break;
                }

                if (pass)
                {
                    //transitionPosCalulated = true;
                }
                else
                {
                    if(currentTransition.onFailUse != null)
                    {
                        TransformPlayerController(currentTransition.onFailUse, newExitTransition, newInteractable, newLookAt, newForceMovementOnEnd, forceTime, forcedTime);
                    }

                    //Cancel Transition
                    return;
                }
            }
        }

        //Convert mouse vertical movement to that of the modifier...
        if(currentTransition.transitionFromExistingMouse)
        {
            ConvertPlayerMovementToModifierMovement();
        }

        originalPlayerPosition = this.transform.position;
        originalPlayerHeight = charController.height;
        originalCamHeight = camHeightParent.localPosition.y;
        //originalCharRotation = this.transform.rotation;
        //originalCamRotation = fps.m_Camera.transform.rotation;

        originalModRotationGlobal = camHeightParent.transform.rotation;
        originalModRotationLocal = camHeightParent.transform.localRotation;
        originalModPosition = camHeightParent.transform.position;
        transitionRecoilState = false;
        soundsPlayed.Clear(); //Clear sfx played during transition

        additionalLookMultiplier = Vector3.one;
        rollMultiplier = 1f;

        if (useAdditionalLookMultiplier)
        {
            additionalLookMultiplier = newAdditionalLookMultiplier;
            rollMultiplier = newRollMultiplier;
        }

        //additionalCharRotation = Quaternion.identity; //Record the additional mouse movement throughout the transition
        //additionalCamRotation = Quaternion.identity; //Record the additional mouse movement throughout the transition

        //Calculate relative look positions at start
        if(transitionInteractable != null && transitionInteractable.controller != null)
        {
            if (currentTransition.lookRelativity == PlayerTransitionPreset.TransitionPosition.relativeToInteractable)
            {
                //startingRelativeCharPosition = transitionInteractable.controller.transform.position - transform.position;
                //startingRelativeCamPosition = transitionInteractable.controller.transform.position - fps.m_Camera.transform.position;

                startingLookPointWorldPosition = transitionInteractable.controller.transform.position;
            }
            else if (currentTransition.lookRelativity == PlayerTransitionPreset.TransitionPosition.relativeToPlayer)
            {
                //startingRelativeCharPosition = transform.TransformPoint(Vector3.forward) - transform.position;
                //startingRelativeCamPosition = fps.m_Camera.transform.TransformPoint(Vector3.forward) - fps.m_Camera.transform.position;

                startingLookPointWorldPosition = camHeightParent.TransformPoint(Vector3.forward * currentTransition.forwardPositionModifier); //Forward position in world space
            }
        }

        //transitionPosCalulated = false; //This flag allows the position to be calculated once only. Reset it here.

        if (forceTime)
        {
            transitionForceTime = true;
            transtionForcedTime = forcedTime;
        }
        else transitionForceTime = false;

        //Get the return position
        if(!currentTransition.transitionToSavedReturnPosition && !currentTransition.disableWriteReturnPosition && writeReturnPosition)
        {
            if (currentTransition.useCustomReturnPosition && transitionInteractable.controller != null)
            {
                storedTransitionPosition = transitionInteractable.controller.transform.TransformPoint(currentTransition.returnPostion);
                Game.Log("Player: Writing stored return position: " + storedTransitionPosition + " (" + currentTransition + ")");
            }
            //Otherwise use the player's postion
            else
            {
                storedTransitionPosition = originalPlayerPosition;
                Game.Log("Player: Writing stored return position: " + storedTransitionPosition + " (" + currentTransition + ")");
            }
        }

        //Disable input during transition
        if(!currentTransition.retainMovementControl)
        {
            EnablePlayerMovement(false, false);

            //Disable physics
            Game.Log("Player: Disable gravity");
            fps.m_StickToGroundForce = 0;
            fps.m_GravityMultiplier = 0;
            EnableCharacterController(false);
        }

        if(newLookAt != null)
        {
            EnablePlayerMouseLook(false, true);
        }

        transitionLookAt = newLookAt; //Up/down look

        transitionActive = true;
        transitionTime = 0f;
        movementOnTransitionComplete = currentTransition.allowMovementOnEnd;
        restoreHolsterOnTransitionComplete = currentTransition.restoreHolsterOnTransitionEnd;
        if (newForceMovementOnEnd) movementOnTransitionComplete = true;

        //Set max speed
        if(movementOnTransitionComplete)
        {
            if (currentTransition.restoreNormalMovementSpeed)
            {
                RestorePlayerMovementSpeed();
            }
            else
            {
                Game.Log("Player: Setting custom movement speed at end of transition: " + currentTransition);
                SetMaxSpeed(currentTransition.customMovementSpeed, currentTransition.customMovementSpeed);
            }
        }

        //Disable head bob
        fps.enableHeadBob = false;
        fps.m_UseJumpBob = false;

        //Force holster
        if(currentTransition.forceHolsterOnTransition)
        {
            FirstPersonItemController.Instance.ForceHolster();
        }

        //fps.enabled = false;
        Game.Log("Player: Disable physics", 1);

        //Manually calling an update loop will trigger and complete any transitions that need to be done instantly
        //Update();
        ExecuteTransition();
    }

    //Restore player's regular movement speed
    public void RestorePlayerMovementSpeed()
    {
        if (isCrouched && !inAirVent)
        {
            Game.Log("Player: Restoring player movement speed: " + GameplayControls.Instance.playerWalkSpeed + " * " + GameplayControls.Instance.playerStealthWalkMuliplier + " (isCrouched: " + isCrouched + ", in air vent: " + inAirVent + ")...");
            SetMaxSpeed(GameplayControls.Instance.playerWalkSpeed * GameplayControls.Instance.playerStealthWalkMuliplier, GameplayControls.Instance.playerRunSpeed * (1f + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.maxSpeedModifier)) * GameplayControls.Instance.playerStealthRunMultiplier);
        }
        else
        {
            Game.Log("Player: Restoring player movement speed: " + GameplayControls.Instance.playerWalkSpeed + " (isCrouched: " + isCrouched + ", in air vent: " + inAirVent + ")...");
            SetMaxSpeed(GameplayControls.Instance.playerWalkSpeed, GameplayControls.Instance.playerRunSpeed * (1f + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.maxSpeedModifier)));
        }
    }

    public void UpdateSkinWidth()
    {
        if(fps.ghostMovement)
        {
            charController.skinWidth = 0.011f;
            Game.Log("Player: Update skin width (Ghost): " + charController.skinWidth);
        }
        else
        {
            if(InteractionController.Instance.carryingObject != null)
            {
                charController.skinWidth = GameplayControls.Instance.carryingSkinWidth;
                Game.Log("Player: Update skin width (Carrying): " + charController.skinWidth);
            }
            else
            {
                charController.skinWidth = GameplayControls.Instance.normalSkinWidth;
                Game.Log("Player: Update skin width (Normal): " + charController.skinWidth);
            }
        }
    }

    //Return player controller to normal form
    public void ReturnFromTransform(bool immediate = false, bool restorePlayerTransform = true)
    {
        //Cancel interaction if it is active
        InteractionController.Instance.CancelInteractionAction();

        //Add Y height so we don't get stuck in floor
        if (!immediate && restorePlayerTransform)
        {
            bool forceTimeR = false;
            float forcedTimeR = 0f;

            //If already active, take the same amount of time as taken so far
            if(transitionActive)
            {
                forceTimeR = true;
                forcedTimeR = transitionTime;
            }

            if (exitTransition != null)
            {
                TransformPlayerController(exitTransition, null, transitionInteractable, null, true, forceTimeR, forcedTimeR);
            }
            else
            {
                TransformPlayerController(GameplayControls.Instance.defaultReturnTransition, null, transitionInteractable, null, true, forceTimeR, forcedTimeR);
            }
        }
        else
        {
            //Set values immediately
            if(restorePlayerTransform)
            {
                if (isCrouched)
                {
                    SetPlayerHeight(GetPlayerHeightCrouched());
                    SetCameraHeight(GameplayControls.Instance.cameraHeightCrouched);
                }
                else
                {
                    SetPlayerHeight(GetPlayerHeightNormal());
                    SetCameraHeight(GameplayControls.Instance.cameraHeightNormal);
                }

                //transform.position = storedWorldPosition;
                //charController.stepOffset = normalStepOffset;
                //fps.m_UseHeadBob = true;
            }

            transitionActive = false;
            movementOnTransitionComplete = true;

            //Set player rotation to the camera's
            this.transform.rotation = Quaternion.Euler(this.transform.rotation.eulerAngles.x, this.transform.rotation.eulerAngles.y + CameraController.Instance.transform.localRotation.eulerAngles.y, this.transform.rotation.eulerAngles.z);

            if(exitTransition != null)
            {
                 CameraController.Instance.transform.localRotation = Quaternion.Euler(0, 0, 0); //Reset camera
            }
            else
            {
                 CameraController.Instance.transform.localRotation = Quaternion.Euler(0, 0, 0); //Reset camera
            }

            OnTransitionComplete();
        }
    }

    //Triggered when transition is complete
    public void OnTransitionComplete()
    {
        if (currentTransition == null) return;

        Game.Log("Player: OnTransitionComplete : " + InteractionController.Instance.interactionMode, 1);
        fps.InitialiseController(true);

        //If skipping time, then restore normal time
        if(spendingTimeMode) SetSpendingTimeMode(false);

        //Re-enable movement
        if (!InteractionController.Instance.interactionMode)
        {
            EnablePlayerMovement(movementOnTransitionComplete, true);
            EnablePlayerMouseLook(true);
        }

        //Restore holster
        if(restoreHolsterOnTransitionComplete)
        {
            FirstPersonItemController.Instance.RestoreItemSelection();
        }

        //Enable item switching
        FirstPersonItemController.Instance.SetEnableFirstPersonItemSelection(currentTransition.allowWeaponSwitchingAfterTransition);

        //Set alarm mode is always false
        SetSettingAlarmMode(false);

        //Return from locked-in interaction
        if (InteractionController.Instance.lockedInInteraction == null || movementOnTransitionComplete)
        {
            //Enable physics
            if(!currentTransition.disableGravity)
            {
                Game.Log("Player: Enable gravity", 1);
                fps.m_StickToGroundForce = 7;
                fps.m_GravityMultiplier = 2;
            }

            //Enable head bob
            if(!currentTransition.disableHeadBob)
            {
                fps.enableHeadBob = true;
                fps.m_UseJumpBob = true;
            }

            Game.Log("Player: Enable physics", 1);
            EnableCharacterController(true);

            //fps.enabled = true;
        }

        //Fire event
        if(OnTransitionCompleted != null)
        {
            OnTransitionCompleted(false);
        }

        //currentTransition = null; //Set to null as some things check against this...
    }

    public void EnableCharacterController(bool val)
    {
        charController.enabled = val;
        transitionDamageTrigger.enabled = !charController.enabled;
    }

    //Called when illegal status could be changed/needs updating
    public override void UpdateIllegalStatus()
    {
        bool newIllegalStatus = false;

        isTrespassing = IsTrespassing(currentRoom, out trespassingEscalation, out _);
        illegalAreaActive = isTrespassing;

        if (illegalAreaActive || Game.Instance.everywhereIllegal)
        {
            newIllegalStatus = true;

            //Hiding place doesn't count if it's not references by an interactable (ie player is hidden by newspaper)
            if(isHiding && hidingInteractable == null)
            {
                SetHiding(false, null);
            }
        }
        //Moved the below into 'trespassing' checks
        //else
        //{
        //    newIllegalStatus = StatusController.Instance.GetCurrentDetainedStatus(); //Ilegal via detained
        //}

        if (illegalActionActive)
        {
            newIllegalStatus = true;
        }

        if (forceTarget) newIllegalStatus = true;

        //Trigger on change of illegal area
        if(newIllegalStatus != illegalStatus)
        {
            illegalStatus = newIllegalStatus;
            Game.Log("Player: Update illegal status: " + illegalStatus);

            if (illegalStatus)
            {
                SetStealthMode(true); //Set stealth mode

                //Start snapshot
                if(trespassingSnapshot == null)
                {
                    trespassingSnapshot = AudioController.Instance.Play2DLooping(AudioControls.Instance.trespassingSnapshot);
                }
            }
            else
            {
                //Cancel stealth mode if not crouched
                if(!isCrouched) SetStealthMode(false); //Set stealth mode

                //Stop snapshot
                if (trespassingSnapshot != null)
                {
                    AudioController.Instance.StopSound(trespassingSnapshot, AudioController.StopType.triggerCue, "Illegal activity ended");
                    trespassingSnapshot = null;
                }
            }

            if(InteractionController.Instance.currentLookingAtInteractable != null)
            {
                InteractionController.Instance.currentLookingAtInteractable.interactable.UpdateCurrentActions();
            }
        }

        StatusCheckEndOfFrame(); //Update status check
        //StatusController.Instance.Force(); //Update status check
    }

    public override bool IsTrespassing(NewRoom room, out int trespassEscalation, out string debugOutput, bool enforcersAllowedEverywhere = true)
    {
        if (Game.Instance.disableTrespass)
        {
            trespassEscalation = 0;
            debugOutput = "Disable trespass";
            return false;
        }

        if(StatusController.Instance.GetCurrentDetainedStatus())
        {
            trespassEscalation = 2;
            debugOutput = "Detained";
            return true;
        }

        if(room != null && room.gameLocation != null && apartmentsOwned.Contains(room.gameLocation))
        {
            trespassEscalation = 0;
            debugOutput = "Player owns apartment";
            return false;
        }

        return base.IsTrespassing(room, out trespassEscalation, out debugOutput, enforcersAllowedEverywhere);
    }

    public override void OnStealthModeChange()
    {
        InterfaceController.Instance.footstepAudioIndicator.UpdateCurrentEvent();
    }

    public override void OnCrouchedChange()
    {
        nodesTraversedWhileWalking = 0; //Reset this

        //Change movement properies
        if(!Player.Instance.transitionActive || currentTransition == null)
        {
            RestorePlayerMovementSpeed();
        }
        else
        {
            Game.Log("Player: Unable to affect movement speed as the player is part of a transition (" + currentTransition + ")");
        }
    }

    public void SetMaxSpeed(float newWalkSpeed, float newRunSpeed)
    {
        desiredWalkSpeed = newWalkSpeed;
        desiredRunSpeed = newRunSpeed;
        Game.Log("Player: Set player max speed: Walk = " + desiredWalkSpeed + ", Run = " + desiredRunSpeed + " (isCrouched: " + isCrouched + ")");
    }

    public void SetCameraHeight(float newHeight)
    {
        camHeightParent.localPosition = new Vector3(0, newHeight, 0);
        //Game.Log("Player: Set camera height: " + newHeight);
    }

    public void SetPlayerHeight(float newHeight, bool stayOnFloorPlane = true)
    {
        //Game.Log("Player: Set player height: " + newHeight + " (stay on floor plane: " + stayOnFloorPlane + ")");

        //The Y centre of the controller needs to be equal to the difference in height * 0.5f. This will allow the physics to behave as if always standing
        if (stayOnFloorPlane)
        {
            charController.center = new Vector3(0, (Player.Instance.GetPlayerHeightNormal() - charController.height) * -0.5f, 0);
        }
        else charController.center = Vector3.zero;

        charController.height = newHeight;

        //The Y centre of the controller needs to be equal to the difference in height * 0.5f. This will allow the physics to behave as if always standing
        if (stayOnFloorPlane)
        {
            charController.center = new Vector3(0, (Player.Instance.GetPlayerHeightNormal() - charController.height) * -0.5f, 0);
        }
        else charController.center = Vector3.zero;

        //Set the trigger to the same position & scale
        //boxTrigger.center = charController.center;
        //boxTrigger.size = new Vector3(0.4f, charController.height, 0.4f);

        //Set positions of light measuring
        //lightMeasureFloor.localPosition = new Vector3(0, charController.height * -0.45f + charController.center.y, 0);
        //lightMeasureHead.localPosition = new Vector3(0, charController.height * 0.45f, 0);
    }

    public override void UpdateLightLevel()
    {
        //Get the light level for player
        currentLightLevel = CameraController.Instance.GetPlayerLightLevel();

        //Trigger flashlight control promt
        if(Game.Instance.displayExtraControlHints && !illegalStatus && currentLightLevel <= 0.16f && !FirstPersonItemController.Instance.flashlight && InteractionController.Instance.lockedInInteraction == null && !Player.Instance.transitionActive && !CutSceneController.Instance.cutSceneActive)
        {
            ControlsDisplayController.Instance.DisplayControlIcon(InteractablePreset.InteractionKey.flashlight, "Flashlight", InterfaceControls.Instance.controlIconDisplayTime);
        }
    }

    //Culling system: Update visible rooms
    public override void OnRoomChange()
    {
        base.OnRoomChange();

        //Set environment
        SessionData.Instance.SetSceneProfile(currentRoom.GetEnvironment());

        //Make sure this room is discovered (max level)
        if(!inAirVent)
        {
            currentRoom.SetExplorationLevel(2);
        }

        //Hide current building (unless in outdoor room)
        UpdateCurrentBuildingModelVisibility();

        //if (currentRoom.building != null)
        //{
        //    //Draw building model from outside
        //    if (inAirVent)
        //    {
        //        currentRoom.building.SetDisplayBuildingModel(false, false);
        //    }
        //    else if (currentRoom.IsOutside())
        //    {
        //        currentRoom.building.SetDisplayBuildingModel(true, true, null);
        //    }
        //    else if (currentRoom.preset.drawBuildingModel || currentRoom.windows.Count > 0)
        //    {
        //        currentRoom.building.SetDisplayBuildingModel(true, false, currentRoom.building.preset.GetFloorSetting(currentRoom.floor.floor, currentRoom.floor.layoutIndex).forceHideModels);
        //    }
        //    else
        //    {
        //        currentRoom.building.SetDisplayBuildingModel(false, false);
        //    }
        //}

        //Changing room will require an update to culling
        UpdateCulling();
        //UpdateCullingOnEndOfFrame();

        //Update AI tick for citizens in previous room & current room
        if(previousRoom != null)
        {
            foreach(Actor a in previousRoom.currentOccupants)
            {
                Citizen c = a as Citizen;

                if(c != null)
                {
                    c.UpdateTickRateOnProx();
                }
            }

            //for (int i = 0; i < previousRoom.currentOccupants.Count; i++)
            //{
            //    foreach()

            //    if(previousRoom.currentOccupants[i] as Citizen != null)
            //    {
            //        (previousRoom.currentOccupants[i] as Citizen).UpdateTickRateOnProx();
            //    }
            //}
        }

        if (currentRoom != null)
        {
            foreach (Actor a in currentRoom.currentOccupants)
            {
                Citizen c = a as Citizen;

                if (c != null)
                {
                    c.UpdateTickRateOnProx();
                }
            }

            //Sync clinic prompt
            if(UpgradesController.Instance.notInstalled > 0)
            {
                if(currentRoom.preset.name == "SyncClinic")
                {
                    InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.interface", "Use a bed or chamber to install or uninstall Sync Disks"), InterfaceControls.Icon.questionMark);
                }
            }

            //for (int i = 0; i < currentRoom.currentOccupants.Count; i++)
            //{
            //    if (currentRoom.currentOccupants[i] as Citizen != null)
            //    {
            //        (currentRoom.currentOccupants[i] as Citizen).UpdateTickRateOnProx();
            //    }
            //}
        }

        //Update light culling for all visible rooms
        foreach(NewRoom r in CityData.Instance.visibleRooms)
        {
            r.UpdateLightCulling();
        }
    }

    public override void OnTileChange()
    {
        base.OnTileChange();

        //Update culling
        //UpdateCullingOnEndOfFrame();
    }

    public void UpdateCullingOnEndOfFrame()
    {
        //Game.Log("Player: Update culling end of frame");
        if(Toolbox.Instance != null) Toolbox.Instance.InvokeEndOfFrame(updateCullingAction, "Update culling");
    }

    //Dynamic update of culling
    //This is called on a room or ground tile change
    public void UpdateCulling()
    {
        //Game.Log("Update culling");

        GameplayController.Instance.UpdateCullingForRoom(currentRoom, true, inAirVent, currentDuct);

        //Update object spawning distances
        ObjectPoolingController.Instance.UpdateObjectRanges();

        //Manually sync physics
        SessionData.Instance.ExecuteSyncPhysics(SessionData.PhysicsSyncType.onPlayerMovement);
    }

    public override void SetResidence(ResidenceController newHome, bool removePreviousResidence = true)
    {
        Game.Log("Player: Set player residence: " + newHome);
        base.SetResidence(newHome, removePreviousResidence);

        if(newHome != null && newHome.address != null && newHome.address.mapButton != null) MapController.Instance.AddUpdateCall(newHome.address.mapButton);
    }

    public override void AddToKeyring(NewAddress ad, bool gameMessage = true)
    {
        base.AddToKeyring(ad, false);

        //Add game message
        if (gameMessage)
        {
            //Only if there is no existing game message...
            string msg = Strings.Get("ui.gamemessage", "key") + " " + ad.name;

            if (!InterfaceController.Instance.notificationQueue.Exists(item => item.message == msg))
            {
                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, msg, InterfaceControls.Icon.key, colourOverride: true, col: InterfaceControls.Instance.messageGrey);
            }
        }
    }

    public override void AddToKeyring(NewDoor ac, bool gameMessage = true)
    {
        base.AddToKeyring(ac, false);

        ac.SetPlayerHasKey(true);

        //Add game message
        if(ac.preset.lockType == DoorPreset.LockType.key && gameMessage)
        {
            //Only if there is no existing game message...
            string msg = Strings.Get("ui.gamemessage", "key") + " " + ac.GetName();

            if (!InterfaceController.Instance.notificationQueue.Exists(item => item.message == msg))
            {
                InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, msg, InterfaceControls.Icon.key, colourOverride: true, col: InterfaceControls.Instance.messageGrey);
            }
        }
    }

    public void AddToKeyring(Interactable inter, bool gameMessage = true)
    {
        if (inter == null) return;

        if(!playerKeyringInt.Contains(inter))
        {
            playerKeyringInt.Add(inter);
        }

        //Game.Log("Successfully set mailbox key for " + ad.name);
        inter.SetCustomState3(true, null);
    }

    public override void RemoveFromKeyring(NewDoor ac)
    {
        base.RemoveFromKeyring(ac);

        ac.SetPlayerHasKey(false);
    }

    public void RemoveFromKeyring(Interactable inter)
    {
        if (inter == null) return;

        playerKeyringInt.Remove(inter);

        //Game.Log("Successfully set mailbox key for " + ad.name);
        inter.SetCustomState3(false, null);
    }

    //Player KO
    public void TriggerPlayerKO(Vector3 KODirection, float RollMP, bool forceDirtyDeath = false)
    {
        if (playerKOInProgress) return;
        if (InteractionController.Instance.dialogMode) return;

        //Close inventory
        if (BioScreenController.Instance.isOpen)
        {
            BioScreenController.Instance.SetInventoryOpen(false, false);
        }

        Game.Log("Player: Player KO!", 1);

        if (spendingTimeMode) SetSpendingTimeMode(false); //Cancel spending time mode...

        //Drop item
        if(InteractionController.Instance.carryingObject != null)
        {
            InteractionController.Instance.carryingObject.DropThis(false);
        }

        //Remove detained status
        if(Player.Instance.currentBuilding != null)
        {
            StatusController.Instance.SetDetainedInBuilding(Player.Instance.currentBuilding, false);
        }

        playerKOInProgress = true;
        KORecovery = false;
        paidFines = false;

        dirtyDeath = false;
        debtPayment = null;

        InteractionController.Instance.UpdateNearbyInteractables();

        if (InteractionController.Instance.mugger != null && InteractionController.Instance.mugger == lastDmgFrom) dirtyDeath = true;

        if (InteractionController.Instance.debtCollector != null && InteractionController.Instance.debtCollector == lastDmgFrom)
        {
            dirtyDeath = true;

            if(InteractionController.Instance.debtCollector.job != null && InteractionController.Instance.debtCollector.job.employer != null)
            {
                debtPayment = GameplayController.Instance.debt.Find(item => item.companyID == InteractionController.Instance.debtCollector.job.employer.companyID);
            }
        }

        if (forceDirtyDeath) dirtyDeath = true;

        lastDmgFrom = null;

        SessionData.Instance.SetEnablePause(false);

        //If I'm interacting with anything, cancel
        SetInteracting(null);
        ReturnFromTransform(immediate: true);

        FirstPersonItemController.Instance.ForceHolster(); //Force holster
        TransformPlayerController(GameplayControls.Instance.playerKO, null, null, null, useAdditionalLookMultiplier: true, newAdditionalLookMultiplier: KODirection, newRollMultiplier: RollMP);

        playerKOFadeOut = false;
        KOTime = GameplayControls.Instance.koTimePass;
        KOTimePassed = 0f;

        //Reset wanted in building status
        foreach (NewBuilding b in CityData.Instance.buildingDirectory)
        {
            b.wantedInBuilding = 0f;
        }

        //Fade to black
        InterfaceController.Instance.Fade(1f, 2f);

        //Disable sleep/get up
        SetActionDisable("Get In", true);
        SetActionDisable("Get Up", true);
        SetActionDisable("Enter", true);
        SetActionDisable("Sleep...", true);

        //Cancel all interaction
        InteractionController.Instance.UpdateInteractionText();
    }

    public void TriggerPlayerRecovery()
    {
        if (playerKOInProgress) return;

        Game.Log("Player: Player Recovery!", 1);

        playerKOInProgress = true;
        InteractionController.Instance.UpdateNearbyInteractables();
        KORecovery = true;

        SessionData.Instance.SetEnablePause(false);

        //If I'm interacting with anything, cancel
        SetInteracting(null);

        FirstPersonItemController.Instance.ForceHolster(); //Force holster

        playerKOFadeOut = false;
        KOTime = 0.5f;
        KOTimePassed = 0f;

        //Fade to black
        InterfaceController.Instance.Fade(1f, 2f);

        //Disable sleep/get up
        SetActionDisable("Get In", true);
        SetActionDisable("Get Up", true);
        SetActionDisable("Enter", true);
        SetActionDisable("Sleep...", true);

        //Cancel all interaction
        InteractionController.Instance.UpdateInteractionText();
    }

    public override void Teleport(NewNode teleportLocation, Interactable.UsagePoint usagePoint, bool cancelVent = true)
    {
        if (teleportLocation == null)
        {
            Game.Log("Player: Teleport cancelled as location is null");
            return;
        }

        //Game.Log("Player: Teleported player to " + teleportLocation.name);

        //Return from any player locked-in status
        SetInteracting(null);
        InteractionController.Instance.CancelInteractionAction(); //Cancel any interaction
        InteractionController.Instance.SetLockedInInteractionMode(null);

        if (cancelVent && inAirVent)
        {
            Game.Log("Player: Cancelling vent...");
            ExitVent(true);
        }

        //ReturnFromTransform();

        if (teleportLocation.defaultSpace != null) transform.position = teleportLocation.defaultSpace.position + new Vector3(0, 2.5f, 0);
        else transform.position = teleportLocation.position + new Vector3(0, 2.5f, 0);

        fps.lastY = transform.position.y; //Update this so player takes no fall damage from this relocation

        Game.Log("Player: Teleported player to position " + transform.position + " node " + teleportLocation.nodeCoord + " room " + teleportLocation.room.GetName(), 1);
        UpdateGameLocation();
    }

    public void SetPosition(Vector3 newWorldPos, Quaternion newRot)
    {
        SetInteracting(null);
        InteractionController.Instance.CancelInteractionAction(); //Cancel any interaction
        InteractionController.Instance.SetLockedInInteractionMode(null);

        transform.position = newWorldPos + new Vector3(0, 0, 0);
        transform.rotation = newRot;
        fps.InitialiseController(false, true); //Re-init controller
        UpdateGameLocation();
        Game.Log("Player: Set player to position " + newWorldPos + ", new game location: " + currentGameLocation, 1);
    }

    //Update the player ambient state
    public void UpdatePlayerAmbientState()
    {
        //Only do this when game has started
        if(SessionData.Instance.startedGame && (CityConstructor.Instance == null || !CityConstructor.Instance.preSimActive) && !SessionData.Instance.isFloorEdit)
        {
            if(persuedBy.Count > 0)
            {
                MusicController.Instance.SetPlayerState(MusicCue.MusicTriggerPlayerState.combat);
            }
            else
            {
                if (isTrespassing && trespassingEscalation >= 2)
                {
                    MusicController.Instance.SetPlayerState(MusicCue.MusicTriggerPlayerState.trespass);
                }
                else
                {
                    MusicController.Instance.SetPlayerState(MusicCue.MusicTriggerPlayerState.safe);
                }
            }

            //General ambient
            if (isOnStreet)
            {
                MusicController.Instance.SetPlayerLocation(MusicCue.MusicTriggerPlayerLocation.outdoors);
            }
            else if (currentGameLocation != null)
            {
                if (currentGameLocation == home || apartmentsOwned.Contains(currentGameLocation))
                {
                    MusicController.Instance.SetPlayerLocation(MusicCue.MusicTriggerPlayerLocation.playersApartment);
                }
                else if(currentGameLocation.IsOutside())
                {
                    MusicController.Instance.SetPlayerLocation(MusicCue.MusicTriggerPlayerLocation.outdoors);
                }
                else
                {
                    MusicController.Instance.SetPlayerLocation(MusicCue.MusicTriggerPlayerLocation.indoors);
                }
            }
        }
    }

    //On hide
    public void OnHide(Interactable newHideInteractable, int reference = 0, bool instant = false, bool allowReturnPositionWrite = true)
    {
        //Make seat available...
        if(InteractionController.Instance.hideInteractable != null)
        {
            InteractionController.Instance.hideInteractable.SetSwitchState(false, this);
        }

        InteractionController.Instance.hideInteractable = newHideInteractable;

        hideInteractable = newHideInteractable;
        hideReference = reference;

        //Set as locked in interaction
        InteractionController.Instance.SetLockedInInteractionMode(InteractionController.Instance.hideInteractable, reference);

        PlayerTransitionPreset enterTransition = null;
        PlayerTransitionPreset exitTransition = null;

        bool writeReturnPosition = true;

        //Use furniture parent setting
        if(reference == 0)
        {
            if (InteractionController.Instance.hideInteractable.furnitureParent != null)
            {
                enterTransition = InteractionController.Instance.hideInteractable.furnitureParent.furniture.hidingEnterTransition;
                exitTransition = InteractionController.Instance.hideInteractable.furnitureParent.furniture.hidingExitTransition;
            }

            //Override with interactable's preset
            if (enterTransition == null || InteractionController.Instance.hideInteractable.preset.overrideFurnitureSetting)
            {
                enterTransition = InteractionController.Instance.hideInteractable.preset.enterTransition;
                exitTransition = InteractionController.Instance.hideInteractable.preset.exitTransition;
            }
        }
        else if(reference == 1)
        {
            if (InteractionController.Instance.hideInteractable.furnitureParent != null)
            {
                enterTransition = InteractionController.Instance.hideInteractable.furnitureParent.furniture.hidingEnterTransition2;
                exitTransition = InteractionController.Instance.hideInteractable.furnitureParent.furniture.hidingExitTransition2;
            }

            //Override with interactable's preset
            if (enterTransition == null || InteractionController.Instance.hideInteractable.preset.overrideFurnitureSetting)
            {
                enterTransition = InteractionController.Instance.hideInteractable.preset.enterTransition2;
                exitTransition = InteractionController.Instance.hideInteractable.preset.exitTransition2;
            }

            writeReturnPosition = false;
        }

        if (!allowReturnPositionWrite) writeReturnPosition = false;

        TransformPlayerController(enterTransition, exitTransition, InteractionController.Instance.hideInteractable, null, false, forceTime: instant, writeReturnPosition: writeReturnPosition);

        //Listen for return from locked in 
        InteractionController.Instance.OnReturnFromLockedIn += OnReturnFromHide;

        if(newHideInteractable.preset.specialCaseFlag == InteractablePreset.SpecialCase.syncBed)
        {
            UpgradesController.Instance.UpdateInstallButton(true);

            if (newHideInteractable.preset.presetName == "SyncChamber")
            {
                //Start snapshot
                if (syncMachineSnapshot == null)
                {
                    syncMachineSnapshot = AudioController.Instance.Play2DLooping(AudioControls.Instance.syncMachineSnapshot);
                }
            }
            else
            {
                //Stop snapshot
                if (syncMachineSnapshot != null)
                {
                    AudioController.Instance.StopSound(syncMachineSnapshot, AudioController.StopType.triggerCue, "Sync machine ended");
                    syncMachineSnapshot = null;
                }
            }
        }

        if(hideInteractable != null)
        {
            hideInteractable.UpdateCurrentActions();
            InteractionController.Instance.UpdateInteractionText();
        }
    }

    public void OnReturnFromHide()
    {
        hideInteractable = null;

        //Set not hidden
        SetHiding(false, null);

        //Open any cabinet doors...
        if (InteractionController.Instance.hideInteractable.furnitureParent != null)
        {
            foreach (Interactable inter in InteractionController.Instance.hideInteractable.furnitureParent.integratedInteractables)
            {
                if (inter.aiActionReference.ContainsKey(RoutineControls.Instance.openLocker))
                {
                    inter.OnInteraction(inter.aiActionReference[RoutineControls.Instance.openLocker], this);
                    inter.controller.transform.localPosition = new Vector3(inter.controller.transform.localPosition.x + (0.01f * inter.controller.transform.localScale.x), inter.controller.transform.localPosition.y, inter.controller.transform.localPosition.z);
                }
            }
        }

        //Stop snapshot
        if (syncMachineSnapshot != null)
        {
            AudioController.Instance.StopSound(syncMachineSnapshot, AudioController.StopType.triggerCue, "Sync machine ended");
            syncMachineSnapshot = null;
        }

        InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromHide;
        ReturnFromTransform();

        UpgradesController.Instance.UpdateInstallButton(false);
    }

    //On answer phone
    public void OnAnswerPhone(Interactable newPhone)
    {
        answeringPhone = newPhone.t;
        phoneInteractable = newPhone;

        //Set as locked in interaction
        InteractionController.Instance.SetLockedInInteractionMode(newPhone);

        //Disable input during transition
        EnablePlayerMovement(false);

        //Listen for return from locked in 
        InteractionController.Instance.OnReturnFromLockedIn += OnReturnFromAnswerPhone;
    }

    public void OnReturnFromAnswerPhone()
    {
        if (answeringPhone != null)
        {
            answeringPhone.SetTelephoneAnswered(null);
        }

        answeringPhone = null;
        phoneInteractable = null;

        InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromAnswerPhone;
        InteractionController.Instance.SetDialog(false, null);

        EnablePlayerMovement(true);
    }

    //Crawl into vents
    public void OnCrawlIntoVent(Interactable vent, bool instant = false)
    {
        SetCrouched(false, true);

        TransformPlayerController(GameplayControls.Instance.enterVentTransition, null, vent, null, true, forceTime: instant);

        inAirVent = true;

        //Once the transition into the vent is complete, return the player to normal control with the new height
        OnTransitionCompleted += EnterVent;

        if (instant) EnterVent();
    }

    //Crawl out of vent
    public void OnCrawlOutOfVent(Interactable vent, bool instant = false)
    {
        TransformPlayerController(GameplayControls.Instance.exitVentTransition, null, vent, null, true, forceTime: instant);

        inAirVent = false;

        //Once the transition into the vent is complete, return the player to normal control with the new height
        OnTransitionCompleted += ExitVent;
    }

    //Called after transition into airvent
    public void EnterVent(bool restoreTransform = false)
    {
        inAirVent = true;

        OnTransitionCompleted -= EnterVent; //Do this to prevent cascade
        Player.Instance.ReturnFromTransform(restorePlayerTransform: false);
        EnableGhostMovement(true, true);

        AudioController.Instance.UpdateMixing(); //Force update of mixing to pass player in vent to ambient sfx
        AudioController.Instance.UpdateVentIndoorOutdoor();
        AudioController.Instance.UpdateDistanceToVent();

        Game.Log("Player: EnterVent");

        RestorePlayerMovementSpeed();
    }

    //Called after transition out of vent
    public void ExitVent(bool restoreTransform = false)
    {
        inAirVent = false;
        OnTransitionCompleted -= ExitVent; //Do this to prevent cascade
        Player.Instance.ReturnFromTransform(immediate: true, restorePlayerTransform: restoreTransform);
        EnableGhostMovement(false);

        AudioController.Instance.UpdateMixing(); //Force update of mixing to pass player in vent to ambient sfx
        AudioController.Instance.UpdateVentIndoorOutdoor();
        AudioController.Instance.UpdateDistanceToVent();

        RestorePlayerMovementSpeed();
    }

    //Use Computer
    public void OnUseComputer(Interactable newComp, bool instant = false)
    {
        computerInteractable = newComp;

        SessionData.Instance.TutorialTrigger("computers");
        SessionData.Instance.TutorialTrigger("surveillance");
        SessionData.Instance.TutorialTrigger("passwords");

        Game.Log("Player: Use computer");

        //Set as locked in interaction
        InteractionController.Instance.SetLockedInInteractionMode(newComp);

        //Transition
        Player.Instance.TransformPlayerController(GameplayControls.Instance.playerUseComputer, GameplayControls.Instance.playerComputerExit, newComp, null, forceTime: instant);

        //Listen for return from locked in 
        InteractionController.Instance.OnReturnFromLockedIn += OnReturnFromUseComputer;
    }

    //Finish using computer
    public void OnReturnFromUseComputer()
    {
        computerInteractable = null;

        Game.Log("Player: Return from computer");

        InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromUseComputer;

        InteractionController.Instance.SetLockedInInteractionMode(null);
        ReturnFromTransform();
    }

    //Take Print
    public void OnTakePrint(Interactable newHand)
    {
        //Set as locked in interaction
        InteractionController.Instance.SetLockedInInteractionMode(newHand);

        //Transition
        Player.Instance.TransformPlayerController(GameplayControls.Instance.playerSearch, GameplayControls.Instance.playerSearchExit, newHand, null);

        //Set ineraction action
        InteractionController.Instance.SetInteractionAction(0f, 1f, 1f, "takeprint", false, false, newHand.controller.transform);

        //Listen for return from locked in 
        InteractionController.Instance.OnReturnFromLockedIn += OnReturnFromTakePrint;

        //Listen for complete lockpick
        InteractionController.Instance.OnInteractionActionCompleted += OnCompleteTakePrint;

        if(newHand.isActor != null && newHand.isActor.ai != null)
        {
            if(!newHand.isActor.isDead && !newHand.isActor.isStunned && !newHand.isActor.isAsleep && !newHand.isActor.ai.restrained)
            {
                Game.Log(newHand.isActor.name + " sees player taking print and isn't happy about it!");
                newHand.isActor.ai.SetPersue(Player.Instance, false, 1, true, 0);
            }
        }
    }

    public void OnCompleteTakePrint()
    {
        List<Evidence.DataKey> photoKey = new List<Evidence.DataKey>();
        photoKey.Add(Evidence.DataKey.photo);

        List<Evidence.DataKey> tiedToPhoto = InteractionController.Instance.lockedInInteraction.isActor.evidenceEntry.GetTiedKeys(photoKey);

        if(!tiedToPhoto.Contains(Evidence.DataKey.fingerprints))
        {
            InteractionController.Instance.lockedInInteraction.isActor.evidenceEntry.MergeDataKeys(Evidence.DataKey.photo, Evidence.DataKey.fingerprints);
        }

        OnReturnFromTakePrint();
    }

    //Finish taking print
    public void OnReturnFromTakePrint()
    {
        InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromTakePrint;
        InteractionController.Instance.OnInteractionActionCompleted -= OnCompleteTakePrint;

        InteractionController.Instance.SetLockedInInteractionMode(null);
        ReturnFromTransform();
    }

    //Take Print
    public void OnSearch(Interactable newSearchItem)
    {
        searchInteractable = newSearchItem;

        //Play sfx looping
        AudioController.Instance.StopSound(searchInteractable.actionLoop, AudioController.StopType.immediate, "Stop search");
        searchInteractable.actionLoop = AudioController.Instance.PlayWorldLooping(searchInteractable.preset.searchLoop, Player.Instance, searchInteractable);

        //Set as locked in interaction
        InteractionController.Instance.SetLockedInInteractionMode(newSearchItem);

        //Transition
        Player.Instance.TransformPlayerController(GameplayControls.Instance.playerSearch, GameplayControls.Instance.playerSearchExit, newSearchItem, null);

        //Set ineraction action
        bool cancelIfTooFar = true;

        if (searchInteractable.isActor != null)
        {
            if (searchInteractable.isActor.isDead)
            {
                cancelIfTooFar = false;
            }
        }

        InteractionController.Instance.SetInteractionAction(0f, 1f, 1f, "search", false, false, newSearchItem.controller.transform, cancelIfTooFar: cancelIfTooFar);

        //Moved to progress over time
        //if(newSearchItem.preset.isStinky)
        //{
        //    AddHygiene(-1f);
        //}

        //Set up discover over time
        if(newSearchItem.objectRef != null)
        {
            Human act = newSearchItem.objectRef as Human;

            if(act != null)
            {
                foreach (Interactable i in act.inventory)
                {
                    Game.Log("Player: Add search discovery over time (interactable): " + i.name + "...");

                    if(!InteractionController.Instance.discoveryOverTime.ContainsKey(i))
                    {
                        InteractionController.Instance.discoveryOverTime.Add(i, Toolbox.Instance.Rand(0.1f, 0.85f));
                    }
                }

                if (act.isDead)
                {
                    //Add wounds to discovery...
                    foreach (Wound w in act.currentWounds)
                    {
                        if (w.interactable != null)
                        {
                            InteractionController.Instance.discoveryOverTime.Add(w.interactable, Toolbox.Instance.Rand(0.1f, 0.7f));
                            break; //Only do this with one would for now, until you can fix the evidence bug properly...
                        }
                    }

                    //Add time of death fact to search discovery
                    InteractionController.Instance.discoveryOverTimeDiscovery.Add(new EvidenceMultiPage.MultiPageContent { discEvID = act.evidenceEntry.evID, disc = Evidence.Discovery.timeOfDeath }, 0.9f);

                    Evidence deathTimeEv = act.death.GetTimeOfDeathEvidence();

                    if (!InteractionController.Instance.discoveryOverTimeEvidence.ContainsKey(deathTimeEv))
                    {
                        InteractionController.Instance.discoveryOverTimeEvidence.Add(deathTimeEv, 0.9f);
                    }
                }
            }
        }

        //Multi-page evidence search...
        if(newSearchItem.evidence != null)
        {
            EvidenceMultiPage mpEv = newSearchItem.evidence as EvidenceMultiPage;

            if(mpEv != null)
            {
                foreach(EvidenceMultiPage.MultiPageContent content in mpEv.pageContent)
                {
                    if(content.evID != null && content.evID.Length > 0)
                    {
                        Evidence foundEv = null;

                        if (GameplayController.Instance.evidenceDictionary.TryGetValue(content.evID, out foundEv))
                        {
                            Game.Log("Player: Add search discovery over time (contained evidence): " + foundEv.preset.name + "...");

                            if(!InteractionController.Instance.discoveryOverTimeEvidence.ContainsKey(foundEv))
                            {
                                InteractionController.Instance.discoveryOverTimeEvidence.Add(foundEv, Toolbox.Instance.Rand(0.1f, 0.99f));
                            }
                        }
                        else Game.LogError("Unable to find evidence " + content.evID);
                    }
                    else if(content.meta > 0)
                    {
                        MetaObject meta = CityData.Instance.FindMetaObject(content.meta);

                        if(meta != null)
                        {
                            Game.Log("Player: Add search discovery over time (meta object): " + meta.preset + "...");

                            if(!InteractionController.Instance.discoveryOverTimeMeta.ContainsKey(meta))
                            {
                                InteractionController.Instance.discoveryOverTimeMeta.Add(meta, Toolbox.Instance.Rand(0.1f, 0.99f));
                            }
                        }
                    }
                    else if(content.discEvID != null && content.discEvID.Length > 0)
                    {
                        if(!InteractionController.Instance.discoveryOverTimeDiscovery.ContainsKey(content))
                        {
                            InteractionController.Instance.discoveryOverTimeDiscovery.Add(content, Toolbox.Instance.Rand(0.1f, 0.99f));
                        }
                    }
                }
            }
        }

        //Listen for return from locked in 
        InteractionController.Instance.OnReturnFromLockedIn += OnReturnFromSearch;

        //Listen for complete lockpick
        InteractionController.Instance.OnInteractionActionCompleted += OnCompleteSearch;
    }

    //Called while an interaction action is happening
    public void OnInteractionActionProgress(float amountThisFrame, float interactionActionAmount)
    {
        if(InteractionController.Instance.activeInteractionAction)
        {
            if(InteractionController.Instance.interactionActionName == "Vomiting")
            {
                AddSick(-1f * amountThisFrame);
                AddHygiene(-0.5f * amountThisFrame);
            }
            //Change this to remove hygeine if food object is found...
            //else if (searchInteractable != null && searchInteractable.preset.isStinky && InteractionController.Instance.interactionActionName == "search")
            //{
            //    AddHygiene(-1f * amountThisFrame);
            //}
        }
    }

    public void OnGenericTimedAction(string actionName, float threshold, float increaseRate, Interactable newItem, bool playObjectsSearchLoop = false)
    {
        genericActionInteractable = newItem;

        if(playObjectsSearchLoop)
        {
            //Play sfx looping
            AudioController.Instance.StopSound(searchInteractable.actionLoop, AudioController.StopType.immediate, "Stop generic action");
            searchInteractable.actionLoop = AudioController.Instance.PlayWorldLooping(searchInteractable.preset.searchLoop, Player.Instance, searchInteractable);
        }

        //Set as locked in interaction
        InteractionController.Instance.SetLockedInInteractionMode(newItem);

        //Transition
        Player.Instance.TransformPlayerController(GameplayControls.Instance.playerSearch, GameplayControls.Instance.playerSearchExit, newItem, null);

        //Set ineraction action
        InteractionController.Instance.SetInteractionAction(0f, threshold, increaseRate, actionName, false, false, newItem.controller.transform, cancelIfTooFar: true);

        InteractionController.Instance.OnReturnFromLockedIn += OnReturnFromGenericAction;
        InteractionController.Instance.OnInteractionActionCompleted += OnReturnFromGenericAction;
    }

    public void OnReturnFromGenericAction()
    {
        //Stop SFX
        AudioController.Instance.StopSound(genericActionInteractable.actionLoop, AudioController.StopType.fade, "Return from action");
        genericActionInteractable.actionLoop = null;

        genericActionInteractable = null;
        InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromGenericAction;
        InteractionController.Instance.OnInteractionActionCompleted -= OnReturnFromGenericAction;

        InteractionController.Instance.SetLockedInInteractionMode(null);
        ReturnFromTransform();
    }

    public void OnCompleteSearch()
    {
        if(InteractionController.Instance.lockedInInteraction != null)
        {
            //if (InteractionController.Instance.lockedInInteraction.isActor != null)
            //{
            //    //Tie fingerprints
            //    List<Evidence.DataKey> photoKey = new List<Evidence.DataKey>();
            //    photoKey.Add(Evidence.DataKey.photo);

            //    List<Evidence.DataKey> tiedToPhoto = InteractionController.Instance.lockedInInteraction.isActor.evidenceEntry.GetTiedKeys(photoKey);

            //    if (!tiedToPhoto.Contains(Evidence.DataKey.fingerprints))
            //    {
            //        InteractionController.Instance.lockedInInteraction.isActor.evidenceEntry.MergeDataKeys(Evidence.DataKey.photo, Evidence.DataKey.fingerprints);
            //    }
            //}

            InteractionController.Instance.lockedInInteraction.ins = true; //Inspected flag is also triggered on search
        }

        OnReturnFromSearch();
    }

    //Finish taking print
    public void OnReturnFromSearch()
    {
        //Stop SFX
        AudioController.Instance.StopSound(searchInteractable.actionLoop, AudioController.StopType.fade, "Return from search");
        searchInteractable.actionLoop = null;

        searchInteractable = null;
        InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromSearch;
        InteractionController.Instance.OnInteractionActionCompleted -= OnCompleteSearch;

        InteractionController.Instance.SetLockedInInteractionMode(null);
        ReturnFromTransform();
    }

    //Handcuff
    public void OnHandcuff(Interactable newBody)
    {
        Game.Log("Player: handcuff " + newBody.name);

        //Get handcuffs reference
        restrainedHandcuffsSlot = BioScreenController.Instance.selectedSlot;

        //Set as locked in interaction
        InteractionController.Instance.SetLockedInInteractionMode(newBody);

        restrainedInteractable = newBody;

        //Play sfx looping
        AudioController.Instance.StopSound(restrainedInteractable.actionLoop, AudioController.StopType.immediate, "Stop search");
        restrainedInteractable.actionLoop = AudioController.Instance.PlayWorldLooping(AudioControls.Instance.handcuff, Player.Instance, restrainedInteractable);

        //Transition
        Player.Instance.TransformPlayerController(GameplayControls.Instance.playerSearch, GameplayControls.Instance.playerSearchExit, newBody, null);

        //Set ineraction action
        InteractionController.Instance.SetInteractionAction(0f, 1f, 1f, "handcuff", false, false, null, cancelIfTooFar: false);

        //Listen for return from locked in 
        InteractionController.Instance.OnReturnFromLockedIn += OnReturnFromHandcuff;

        //Listen for complete lockpick
        InteractionController.Instance.OnInteractionActionCompleted += OnCompleteHandcuff;

        restrainedInteractable.isActor.ai.SetRestrained(true, GameplayControls.Instance.restrainedTimer);

        //Figure out if anyone else responds to this...
        bool publicFauxPas = true;

        if(restrainedInteractable.isActor.persuedBy.Count > 0 || restrainedInteractable.isActor.illegalStatus)
        {
            publicFauxPas = false;
        }

        restrainedInteractable.isActor.ai.SetPersue(Player.Instance, publicFauxPas, 1, true, CitizenControls.Instance.punchedResponseRange);
    }

    //Executed on success
    public void OnCompleteHandcuff()
    {
        Game.Log("Player: Complete handcuff");

        AudioController.Instance.StopSound(restrainedInteractable.actionLoop, AudioController.StopType.immediate, "Stop search");
        AudioController.Instance.Play2DSound(AudioControls.Instance.handcuffArrestEnd);

        InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromHandcuff;
        InteractionController.Instance.OnInteractionActionCompleted -= OnCompleteHandcuff;

        //Remove handcuffs
        Interactable handcuffs = restrainedHandcuffsSlot.GetInteractable();

        if(handcuffs != null)
        {
            FirstPersonItemController.Instance.EmptySlot(restrainedHandcuffsSlot, false, false, playSound: false);
            handcuffs.SetAsNotInventory(Player.Instance.currentNode);
            handcuffs.SetInInventory(restrainedInteractable.isActor as Human);
        }
        else
        {
            Game.LogError("Unable to get handcuff interactable from inventory");
        }

        InteractionController.Instance.SetLockedInInteractionMode(null);
        ReturnFromTransform();
    }

    //Executed on fail only
    public void OnReturnFromHandcuff()
    {
        AudioController.Instance.StopSound(restrainedInteractable.actionLoop, AudioController.StopType.immediate, "Stop search");
        Game.Log("Player: Return from handcuff");

        InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromHandcuff;
        InteractionController.Instance.OnInteractionActionCompleted -= OnCompleteHandcuff;

        InteractionController.Instance.SetLockedInInteractionMode(null);
        ReturnFromTransform();

        restrainedInteractable.isActor.ai.SetRestrained(false, 0f);
    }

    //Enable/disable ghost movement
    public void EnableGhostMovement(bool ghost, bool clipping = false, float stickToGround = 0)
    {
        Game.Log("Player: Ghost movement: " + ghost + " clipping: " + clipping);

        if(!ghost)
        {
            fps.enableHeadBob = true;
            fps.m_GravityMultiplier = 2;
            fps.m_StickToGroundForce = 7;
            fps.ghostMovement = false;
            fps.fallCount = 0f;
            fps.clipping = true;
            fps.lastY = transform.position.y;
        }
        else
        {
            fps.enableHeadBob = false;
            fps.m_GravityMultiplier = 0;
            fps.m_StickToGroundForce = stickToGround;
            fps.fallCount = 0f;
            fps.ghostMovement = true;
            fps.clipping = clipping;
            fps.lastY = transform.position.y;
        }

        UpdateSkinWidth();
    }

    //Force disable an action
    public void SetActionDisable(string newString, bool val)
    {
        newString = newString.ToLower();
        Game.Log("Player: SetActionDisable " + newString + " = " + val);

        if (val)
        {
            if (!disabledActions.Contains(newString))
            {
                disabledActions.Add(newString);
            }
        }
        else disabledActions.Remove(newString);

        //Update if looking at
        if (InteractionController.Instance.currentLookingAtInteractable != null) InteractionController.Instance.currentLookingAtInteractable.interactable.UpdateCurrentActions();

        //Update locked in
        if(InteractionController.Instance.lockedInInteraction != null)
        {
            InteractionController.Instance.lockedInInteraction.UpdateCurrentActions();
        }

        //Update player text
        InteractionController.Instance.UpdateInteractionText();
    }

    //Clear all disabled actions
    public void ClearAllDisabledActions()
    {
        disabledActions.Clear();

        //Update if looking at
        if (InteractionController.Instance.currentLookingAtInteractable != null) InteractionController.Instance.currentLookingAtInteractable.interactable.UpdateCurrentActions();

        //Update locked in
        if (InteractionController.Instance.lockedInInteraction != null)
        {
            InteractionController.Instance.lockedInInteraction.UpdateCurrentActions();
        }

        //Update player text
        InteractionController.Instance.UpdateInteractionText();
    }

    //Set current vehicle
    public void SetVehicle(Transform newVehicle)
    {
        if (newVehicle != null && !SessionData.Instance.startedGame) return;
        Game.Log("Player: Set vehicle " + newVehicle);

        if(newVehicle != currentVehicle)
        {
            //Make sure postion is saved
            Vector3 currentPos = this.transform.position;
            Quaternion currentRot = this.transform.rotation;

            if (newVehicle != null)
            {
                this.gameObject.transform.SetParent(newVehicle, true);
                currentVehicle = newVehicle;

                //Disable player physics interpolation (object will move player, messing up any interpolation).
                //rb.interpolation = RigidbodyInterpolation.None;
            }
            else
            {
                this.gameObject.transform.SetParent(playerContainer, true);
                currentVehicle = null;

                //Reset player physics interpolation
                //rb.interpolation = RigidbodyInterpolation.Interpolate;
            }

            this.transform.position = currentPos;
            this.transform.rotation = currentRot;
            fps.InitialiseController(true, true);
        }
    }

    //Set setting of alarm mode
    public void SetSettingAlarmMode(bool val)
    {
        setAlarmMode = val;

        Game.Log("Player: Set alarm mode: " + setAlarmMode);

        if (setAlarmMode)
        {
            //Bring up watch
            //FirstPersonItemController.Instance.SetFirstPersonItem(GameplayControls.Instance.watchItem);

            //The alarm time is now
            alarm = SessionData.Instance.gameTime;

            SessionData.Instance.UpdateUIClock();
            SessionData.Instance.UpdateUIDay();

            alarmFlash = true;
            setAlarmFlashCounter = 0.5f;

            ControlsDisplayController.Instance.SetControlDisplayArea(450, 0, 300, 300);
        }
        else
        {
            ControlsDisplayController.Instance.RestoreDefaultDisplayArea();
            if (SessionData.Instance.newWatchTimeText != null) SessionData.Instance.newWatchTimeText.gameObject.SetActive(true); //Make sure this is active
        }
    }

    public void AddToAlarmTime(float plusTime)
    {
        if (setAlarmMode)
        {
            Game.Log("Player: Add " + plusTime + " to alarm...");

            alarm += plusTime;

            SessionData.Instance.UpdateUIClock();
            SessionData.Instance.UpdateUIDay();

            if(plusTime > 0)
            {
                //Play sfx
                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.timeForward, this, currentNode, lookAtThisTransform.position);
            }
            else
            {
                //Play sfx
                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.timeBackward, this, currentNode, lookAtThisTransform.position);
            }
        }
    }

    public void SetSpendingTimeMode(bool val)
    {
        if (playerKOInProgress) return;

        spendingTimeMode = val;

        Game.Log("Player: Spending time mode: " + spendingTimeMode);

        //Speed up time
        if (spendingTimeMode)
        {
            if (SessionData.Instance.currentTimeSpeed != SessionData.TimeSpeed.veryFast)
            {
                SessionData.Instance.SetTimeSpeed(SessionData.TimeSpeed.veryFast);
            }

            if (SessionData.Instance.play)
            {
                InterfaceControls.Instance.fastForwardArrow.gameObject.SetActive(true);
            }
            else InterfaceControls.Instance.fastForwardArrow.gameObject.SetActive(false);

            //Black out if asleep...
            if(InteractionController.Instance.lockedInInteraction != null)
            {
                if(InteractionController.Instance.lockedInInteraction.preset.specialCaseFlag  == InteractablePreset.SpecialCase.sleepPosition)
                {
                    InterfaceController.Instance.Fade(1f, newFadeAudio: true);
                }
            }
        }
        else
        {
            //Awaken
            if(InterfaceController.Instance.fade != 0f || InterfaceController.Instance.desiredFade != 0f)
            {
                InterfaceController.Instance.Fade(0f, newFadeAudio: true);
            }

            if (SessionData.Instance.currentTimeSpeed != GameplayControls.Instance.startingTimeSpeed)
            {
                SessionData.Instance.SetTimeSpeed(GameplayControls.Instance.startingTimeSpeed);
            }

            InterfaceControls.Instance.fastForwardArrow.gameObject.SetActive(false);
        }

        if (InteractionController.Instance.lockedInInteraction != null)
        {
            InteractionController.Instance.lockedInInteraction.UpdateCurrentActions();
        }

        InteractionController.Instance.UpdateInteractionText();
    }

    public override void RecieveDamage(float amount, Actor fromWho, Vector3 damagePosition, Vector3 damageDirection, SpatterPatternPreset forwardSpatter, SpatterPatternPreset backSpatter, SpatterSimulation.EraseMode eraseMode = SpatterSimulation.EraseMode.useDespawnTime, bool alertSurrounding = true, bool forceRagdoll = false, float forcedRagdollDuration = 0f, float shockMP = 1f, bool enableKill = false, bool allowRecoil = true, float ragdollForceMP = 1f)
    {
        if (playerKOInProgress)
        {
            Game.Log("Player: Negated damage; player KO in progress");
            return; //Ignore if already KO'd
        }

        //if (InteractionController.Instance.dialogMode)
        //{
        //    Game.Log("Player: Negated damage; dialog mode active");
        //    return; //Disable damage in dialog mode...
        //}

        lastDmgFrom = fromWho;
        //if (DebugControls.Instance.invinciblePlayer) return; //Don't take damage if invincible!

        if(SessionData.Instance.gameTime - lastDamageAt > 0.1f)
        {
            float dmgBlock = UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.blockIncoming);

            lastDamageAt = SessionData.Instance.gameTime;
            if (dmgBlock >= 1) amount = 0;
        }
        else lastDamageAt = SessionData.Instance.gameTime;

        //Apply ballistic protection: 70%
        if (FirstPersonItemController.Instance.slots.Exists(item => item.GetInteractable() != null && item.GetInteractable().preset.specialCaseFlag == InteractablePreset.SpecialCase.ballisticArmour))
        {
            amount *= 0.7f;
        }

        //Calc spatter before applying difficulty
        //Use damage to set the blood spatter count multiplier: Use ratio of maximum health. Half of their health uses the maximum amount
        float max = GetCurrentMaxHealth();
        float spatterMP = Mathf.Clamp01((amount / max) * 2f);

        //Apply incoming multiplier (difficulty)
        amount *= Game.Instance.difficultyIncomingDamageMultipliers[Game.Instance.gameDifficulty];

        //Apply incoming multiplier (skills)
        amount *= StatusController.Instance.damageIncomingMultiplier * (1f + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.incomingDamageModifier));

        //Apply hurt effects
        hurt += amount;
        hurt = Mathf.Clamp01(hurt);

        base.RecieveDamage(amount, fromWho, damagePosition, damageDirection, forwardSpatter, backSpatter, eraseMode, shockMP: shockMP, enableKill: enableKill); //Base function deals damage

        takeDamageIndicatorTimer = 1f;
        float indicatorSize = Mathf.Lerp(80f, 140f, amount / max);
        InterfaceController.Instance.takeDamageIndicatorImg.rectTransform.sizeDelta = new Vector2(indicatorSize, indicatorSize); //Adjust size depending on damage
        takeDamageDisplaySpeed = Mathf.Lerp(0.5f, 2f, amount / max); //Adjust display speed depending on damage
        InterfaceController.Instance.takeDamageIndicatorImg.gameObject.SetActive(true);

        //Spill blood
        Vector3 localDir = this.transform.InverseTransformDirection(damageDirection);
        Vector3 localPos = this.transform.InverseTransformPoint(damagePosition);

        //Apply camera jolt
        if (amount > 0.01f)
        {
            fps.JoltCamera(-localDir, amount * 50, Mathf.Max(1.1f - amount, 0.5f));
        }

        if (forwardSpatter != null) new SpatterSimulation(this, localPos, localDir, forwardSpatter, eraseMode, spatterMP, false);
        if (backSpatter != null) new SpatterSimulation(this, localPos, -localDir, backSpatter, eraseMode, spatterMP, false);

        Vector3 lookMultiplier = Vector3.zero;

        //Chance of status effects
        if(Toolbox.Instance.Rand(0f, 1f) <= GameplayControls.Instance.combatHitChanceOfBleeding)
        {
            AddBleeding(Toolbox.Instance.Rand(0.2f, 0.8f));
        }
        else if (Toolbox.Instance.Rand(0f, 1f) <= GameplayControls.Instance.combatHitChanceOfBruised)
        {
            AddBruised(Toolbox.Instance.Rand(0.2f, 0.8f));
        }
        else if (Toolbox.Instance.Rand(0f, 1f) <= GameplayControls.Instance.combatHitChanceOfBlackEye)
        {
            AddBrokenLeg(Toolbox.Instance.Rand(0.2f, 0.8f));
        }
        else if (Toolbox.Instance.Rand(0f, 1f) <= GameplayControls.Instance.combatHitChanceOfBrokenLeg)
        {
            AddBrokenLeg(Toolbox.Instance.Rand(0.7f, 1f));
        }

        //Focus from dmg
        if(amount > 0f)
        {
            float focusFromDmg = UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.focusFromDamage);

            if(focusFromDmg > 0f)
            {
                AddAlertness(focusFromDmg);
            }
        }

        //Calculate the force and use it to change the transition look multipliers...
        //Use relative position
        Vector3 relativeToMe = aimTransform.InverseTransformPoint(damagePosition).normalized * -1f; //Relative to me
        Game.Log("Player: Incoming damage " + amount + " relative to me: " + relativeToMe);

        //The Y value determins vertical knockback. Use the Z value for this as it changes based on whether the punch is from infront or behind
        lookMultiplier.y = relativeToMe.z + relativeToMe.y;

        //The X multiplier detmins the side-to-side knock
        lookMultiplier.x = relativeToMe.x;

        //Roll is also affected by side position
        float roll = -relativeToMe.x;

        //If there is still health left...
        //if(currentHealth > 0f && InteractionController.Instance.lockedInInteraction == null && !Player.Instance.transitionActive)
        //{
        //    InteractionController.Instance.SetLockedInInteractionMode(null);

        //    //Transition
        //    TransformPlayerController(GameplayControls.Instance.punchedReaction, null, null, null, useAdditionalLookMultiplier: true, newAdditionalLookMultiplier: lookMultiplier, newRollMultiplier: roll);
        //}
        ////Otherwise player is KO'd
        //else if(!Game.Instance.invinciblePlayer)
        //{
        //    //TriggerPlayerKO(lookMultiplier, roll);
        //}

        //Display the low health indicator
        if (currentHealthNormalized <= InterfaceControls.Instance.lowHealthIndicatorThreshold)
        {
            InterfaceController.Instance.lowHealthIndicatorImg.gameObject.SetActive(true);
        }

        //Set saturation amount
        SessionData.Instance.colour.saturation.overrideState = true;
        SessionData.Instance.colour.saturation.Override(Mathf.Lerp(-100f, Game.Instance.defaultSaturationAmount, currentHealthNormalized));
    }

    public override void SetFootwear(ShoeType newType)
    {
        footwear = newType;

        if (footwear == ShoeType.normal)
        {
            footstepEvent = AudioControls.Instance.playerFootstepShoe;
        }
        else if (footwear == ShoeType.boots)
        {
            footstepEvent = AudioControls.Instance.playerFootstepBoot;
        }
        else if (footwear == ShoeType.heel)
        {
            footstepEvent = AudioControls.Instance.playerFootstepHeel;
        }
    }

    //The player's health here can be affected by game difficulty multipliers
    public override void AddHealth(float amount, bool affectedByGameDifficulty = true, bool displayDamageIndicator = false)
    {
        //Adjust amount according to game difficulty
        if(amount < 0f)
        {
            if (playerKOInProgress) return;
            if (InteractionController.Instance.dialogMode) return; //Disable damage in dialog mode...

            if(affectedByGameDifficulty) amount *= Game.Instance.difficultyIncomingDamageMultipliers[Game.Instance.gameDifficulty];
        }

        currentHealth += amount;
        float max = GetCurrentMaxHealth();
        currentHealth = Mathf.Clamp(currentHealth, 0, max); //Clamp health
        currentHealthNormalized = Mathf.Clamp01(currentHealth / maximumHealth);

        if(displayDamageIndicator && amount < 0f)
        {
            takeDamageIndicatorTimer = 1f;
            float indicatorSize = Mathf.Lerp(80f, 140f, -amount / max);
            InterfaceController.Instance.takeDamageIndicatorImg.rectTransform.sizeDelta = new Vector2(indicatorSize, indicatorSize); //Adjust size depending on damage
            takeDamageDisplaySpeed = Mathf.Lerp(0.5f, 2f, -amount / max); //Adjust display speed depending on damage
            InterfaceController.Instance.takeDamageIndicatorImg.gameObject.SetActive(true);
        }

        //Game.Log("Player: Set health: (" + amount + ") = " + currentHealth + "/" + max);

        //Add/Remove the low health indicator
        if (currentHealthNormalized > InterfaceControls.Instance.lowHealthIndicatorThreshold)
        {
            if(InterfaceController.Instance.lowHealthIndicatorImg != null) InterfaceController.Instance.lowHealthIndicatorImg.gameObject.SetActive(false);
        }
        //Display the low health indicator
        else if (currentHealthNormalized <= InterfaceControls.Instance.lowHealthIndicatorThreshold)
        {
            InterfaceController.Instance.lowHealthIndicatorImg.gameObject.SetActive(true);
        }

        //Set saturation amount
        SessionData.Instance.colour.saturation.overrideState = true;
        SessionData.Instance.colour.saturation.Override(Mathf.Lerp(-100f, Game.Instance.defaultSaturationAmount, currentHealthNormalized));

        if (currentHealth <= 0f && !Game.Instance.invinciblePlayer)
        {
            TriggerPlayerKO(transform.forward, 0f);
        }
    }

    public override void SetHealth(float amount)
    {
        base.SetHealth(amount);

        //Remove the low health indicator
        if (currentHealthNormalized > InterfaceControls.Instance.lowHealthIndicatorThreshold)
        {
            if (InterfaceController.Instance.lowHealthIndicatorImg != null) InterfaceController.Instance.lowHealthIndicatorImg.gameObject.SetActive(false);
        }

        //Set saturation amount
        SessionData.Instance.colour.saturation.overrideState = true;
        SessionData.Instance.colour.saturation.Override(Mathf.Lerp(-100f, Game.Instance.defaultSaturationAmount, currentHealthNormalized));

        if (currentHealth <= 0f && !Game.Instance.invinciblePlayer)
        {
            TriggerPlayerKO(transform.forward, 0f);
        }
    }

    public override void SightingCheck(float fov, bool ignoreLightAndStealth = false)
    {
        if (illegalStatus || witnessesToIllegalActivity.Count > 0 /*|| avoidCitizenCount > 0*/)
        {
            //Reset all citizens I see...
            //currentVisibleTargets.Clear();

            List<Actor> shortlist = new List<Actor>();

            //Only do this with illegal status or you have to avoid citizens...
            if (illegalStatus)
            {
                foreach (Actor act in CityData.Instance.visibleActors)
                {
                    float dist = Vector3.Distance(act.transform.position, this.transform.position);

                    if (dist < GameplayControls.Instance.playerMaxSpotDistance)
                    {
                        shortlist.Add(act);
                    }
                }
            }
            //Otherwise do the check with just citizens you need to avoid...
            else
            {
                foreach (Actor act in witnessesToIllegalActivity)
                {
                    if(act.visible)
                    {
                        float dist = Vector3.Distance(act.transform.position, this.transform.position);

                        if (dist < GameplayControls.Instance.playerMaxSpotDistance)
                        {
                            shortlist.Add(act);
                        }
                    }
                }
            }

            //We now have a shortlist of targets to test...
            foreach(Actor act in shortlist)
            {
                //Do 2 raycasts, one to head and one to feet...
                for (int i = 0; i < 2; i++)
                {
                    Ray r;
                    RaycastHit hit;

                    if (i == 0)
                    {
                        r = new Ray(CameraController.Instance.cam.transform.position, act.lookAtThisTransform.position - CameraController.Instance.cam.transform.position);
                    }
                    else
                    {
                        r = new Ray(CameraController.Instance.cam.transform.position, act.transform.position - CameraController.Instance.cam.transform.position);
                    }

                    if (Physics.Raycast(r, out hit, GameplayControls.Instance.playerMaxSpotDistance, Toolbox.Instance.aiSightingLayerMask))
                    {
                        //Found other citizen!
                        if(hit.transform.parent != null)
                        {
                            if (hit.transform.parent.gameObject == act.gameObject)
                            {
                                act.SpottedByPlayer();

                                break; //No need to do the other raycast for this actor...
                            }
                        }
                    }
                }
            }
        }
    }

    public override void PrepForStart()
    {
        nourishment = 1f;
        hydration = 1f;
        alertness = 1f;
        energy = 1f;
        excitement = 1f;
        chores = 1f;
        hygiene = 1f;
        bladder = 1f;
        heat = 1f;
        drunk = 0f;
        sick = 0f;
        starchAddiction = 0f;
        headache = 0f;
        wet = 0f;
        brokenLeg = 0f;
        bruised = 0f;
        blackEye = 0f;
        blackedOut = 0f;
        numb = 0f;
        breath = 1f;
        poisoned = 0f;
        blinded = 0f;

        descriptors.skinColour = Game.Instance.playerSkinColour;

        //Reset to maximum health
        ResetHealthToMaximum();
        ResetNerveToMaximum();

        //Add locations of authority (homes)
        if (home != null)
        {
            AddLocationOfAuthorty(home);
        }
        
        foreach(NewAddress ad in apartmentsOwned)
        {
            AddLocationOfAuthorty(ad);
        }

        //Set name
        citizenName = Game.Instance.playerFirstName + " " + Game.Instance.playerSurname;
        firstName = Game.Instance.playerFirstName;
        surName = Game.Instance.playerSurname;
        //casualName = firstName;
        casualName = string.Empty; //This will now use the first name if this is empty
        //if (firstName.Length > 1) initialledName = firstName.Substring(0, 1) + ". " + surName;
        //else initialledName = firstName + ". " + surName;
        gameObject.name = citizenName;
        name = citizenName;

        //Set gender
        gender = Game.Instance.playerGender;
        birthGender = Game.Instance.playerGender;

        //Set password
        passcode.digits.Clear();
        passcode.digits.Add(1);
        passcode.digits.Add(2);
        passcode.digits.Add(3);
        passcode.digits.Add(4);

        //Setup evidence
        evidenceEntry = EvidenceCreator.Instance.CreateEvidence("citizen", "Player", this, this, this) as EvidenceWitness;

        //Setup interaction controller
        interactable = InteractableCreator.Instance.CreateCitizenInteractable(CitizenControls.Instance.citizenInteractable, this, this.transform, evidenceEntry);

        sceneRecorder = new SceneRecorder(interactable);
    }

    public override void AddNourishment(float addVal)
    {
        base.AddNourishment(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddHydration(float addVal)
    {
        base.AddHydration(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddEnergy(float addVal)
    {
        base.AddEnergy(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddAlertness(float addVal)
    {
        base.AddAlertness(addVal);

        //Alertness brings energy up
        if(alertness > 0.5f)
        {
            energy = Mathf.Max(energy, alertness - 0.5f);
        }

        StatusCheckEndOfFrame();
    }

    public override void AddHygiene(float addVal)
    {
        base.AddHygiene(addVal);

        //Game.Log("Player: Add Hygeine " + addVal);

        //Perform a stinky check!
        if(hygiene <= 0f)
        {
            StatusCheckEndOfFrame();
        }
        else if(hygiene >= 1f)
        {
            StatusCheckEndOfFrame();
        }
    }

    public override void AddHeat(float addVal)
    {
        //Limit temperature gain by temp gain multiplier
        if(addVal < 0f)
        {
            addVal *= StatusController.Instance.temperatureGainMultiplier;
        }

        base.AddHeat(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddDrunk(float addVal)
    {
        base.AddDrunk(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddSick(float addVal)
    {
        base.AddSick(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddHeadache(float addVal)
    {
        base.AddHeadache(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddWet(float addVal)
    {
        base.AddWet(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddBrokenLeg(float addVal)
    {
        if (addVal > 0f && UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.noBrokenBones) > 0f)
        {
            addVal = 0f;
        }

        base.AddBrokenLeg(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddBruised(float addVal)
    {
        if (addVal > 0f && UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.noBrokenBones) > 0f)
        {
            addVal = 0f;
        }

        base.AddBruised(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddBlackEye(float addVal)
    {
        if (addVal > 0f && UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.noBrokenBones) > 0f)
        {
            addVal = 0f;
        }

        base.AddBlackEye(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddBlackedOut(float addVal)
    {
        base.AddBlackedOut(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddNumb(float addVal)
    {
        base.AddNumb(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddBleeding(float addVal)
    {
        if(addVal > 0f && UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.noBleeding) > 0f)
        {
            addVal = 0f;
        }

        base.AddBleeding(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddStarchAddiction(float addVal)
    {
        base.AddStarchAddiction(addVal);

        StatusCheckEndOfFrame();
    }

    public override void AddSyncDiskInstall(float addVal)
    {
        base.AddSyncDiskInstall(addVal);

        StatusCheckEndOfFrame();
    }

    public void StatusCheckEndOfFrame()
    {
        Toolbox.Instance.InvokeEndOfFrame(updateStatusAction, "Update status");
    }

    public override void SetOnStreet(bool val)
    {
        if (inAirVent) val = false; //Force false if inside an air duct

        base.SetOnStreet(val);
    }

    public void Trip(float damage, bool forwards = false, bool playSound = true)
    {
        if (!transitionActive && !isAsleep && !inAirVent && spawnProtection <= 0f)
        {
            //If I'm interacting with anything, cancel
            SetInteracting(null);

            //Minus health
            AddHealth(-damage, false, true);

            //Apply a jolt to the camera down
            Vector3 dir = Vector3.left;
            if (forwards) dir = Vector3.right;

            //Random other movement
            dir.y = Toolbox.Instance.Rand(-0.1f, 0.1f);
            dir.z = Toolbox.Instance.Rand(-0.1f, 0.1f);

            fps.JoltCamera(dir, 60f, 0.25f);

            if (currentHealth > 0f)
            {
                TransformPlayerController(GameplayControls.Instance.tripTransition, null, null, null);

                if (playSound)
                {
                    AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.playerTripSound, Player.Instance, Player.Instance.currentNode, Player.Instance.transform.position, null);
                }
            }
        }
    }

    public override void SetHiding(bool val, Interactable newHidingPlace)
    {
        if (hygiene < 1f) val = false;

        if (isHiding != val && val)
        {
            //Hidden to everybody except ai who were watching while this happens...
            spottedWhileHiding.Clear();

            if(val)
            {
                hidingInteractable = newHidingPlace;

                foreach (Actor act in witnessesToIllegalActivity)
                {
                    if (act.seesIllegal.ContainsKey(this as Actor))
                    {
                        if (act.seesIllegal[this as Actor] >= 1f)
                        {
                            Game.Log("Player: Spotted while hiding: " + act.name);
                            spottedWhileHiding.Add(act);
                        }
                        else
                        {
                            //Move investigate to a nearby node...
                            if(act.ai != null)
                            {
                                if(act.ai.investigateLocation == currentNode)
                                {
                                    act.ai.investigateLocation = Toolbox.Instance.PickNearbyNode(currentNode);
                                    act.ai.investigatePosition = act.ai.investigateLocation.position;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                hidingInteractable = null;
            }
        }

        base.SetHiding(val, newHidingPlace);

        Game.Log("Player: Set hiding: " + val);

        StatusController.Instance.ForceStatusCheck();
    }

    public float GetPlayerHeightNormal()
    {
        return GameplayControls.Instance.playerHeightNormal * (1f + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.playerHeightModifier));
    }

    public float GetPlayerHeightCrouched()
    {
        return GameplayControls.Instance.playerHeightCrouched * (1f + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.playerHeightModifier));
    }

    [Button]
    public void KillPlayer()
    {
        AddHealth(-99999999, false, true);
    }
}
