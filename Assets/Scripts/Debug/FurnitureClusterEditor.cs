using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class FurnitureClusterEditor : MonoBehaviour
{
    [Header("Current")]
    public FurnitureCluster cluster;
    public List<FurnitureCluster.FurnitureClusterRule> clusterElements = new List<FurnitureCluster.FurnitureClusterRule>();

    [Header("Components")]
    public Transform furnitureParent;
    public List<WalkableRecorder.TileSetup> tiles = new List<WalkableRecorder.TileSetup>();
    public List<ClusterEditorFurniture> spawnedFurniture = new List<ClusterEditorFurniture>();

    [Button]
    public void ScanTilesForFurniture()
    {
        ClearClusterList();
        clusterElements.Clear();

        object[] allFurn = AssetLoader.Instance.GetAllFurniture().ToArray();

        List<FurniturePreset> allFurniture = new List<FurniturePreset>();

        foreach(FurniturePreset p in allFurn)
        {
            allFurniture.Add(p);
        }

        Transform[] children = furnitureParent.GetComponentsInChildren<Transform>(true);

        foreach (Transform t in children)
        {
            string furnitureModelName = t.name;

            //Remove '(Clone)' from name
            if(furnitureModelName.Length > 7 && furnitureModelName.Substring(furnitureModelName.Length - 7) == "(Clone)")
            {
                furnitureModelName = furnitureModelName.Substring(0, furnitureModelName.Length - 7);
            }

            //Check name against furniture prefab
            FurniturePreset foundFurn = allFurniture.Find(item => item.prefab.name == furnitureModelName);

            if(foundFurn != null)
            {
                WalkableRecorder.TileSetup closestTile = null;
                float dist = Mathf.Infinity;

                foreach(WalkableRecorder.TileSetup s in tiles)
                {
                    float distance = Vector3.Distance(s.trans.position, t.transform.position);

                    if(distance < dist)
                    {
                        dist = distance;
                        closestTile = s;
                    }
                }

                if(closestTile != null)
                {
                    //Get facing dir
                    FurnitureCluster.FurnitureFacing facing = GetFacingForFurnitureAngle(t.transform.eulerAngles.y);

                    Debug.Log("Found furniture " + foundFurn.name + " (" + foundFurn.classes[0].name + ") at " + closestTile.offset + " facing " + facing);

                    //Position accurately
                    t.transform.position = closestTile.trans.position;
                    t.transform.eulerAngles = new Vector3(0, GetAngleForFurnitureFacing(facing), 0);

                    //Add a ClusterEditorFurniture class if there isn't one...
                    ClusterEditorFurniture editor = t.gameObject.GetComponent<ClusterEditorFurniture>();

                    //Add a ClusterEditorFurniture class if there isn't one...
                    if (editor == null)
                    {
                        editor = t.gameObject.AddComponent<ClusterEditorFurniture>();
                    }

                    editor.Setup(foundFurn);
                    spawnedFurniture.Add(editor);

                    //Create element for this
                    FurnitureCluster.FurnitureClusterRule newElement = new FurnitureCluster.FurnitureClusterRule();
                    newElement.placements.Add(closestTile.offset);
                    newElement.furnitureClass = editor.furnClass;
                    newElement.facing = facing;
                    newElement.chanceOfPlacementAttempt = 1;
                    newElement.importantToCluster = true;
                    newElement.placementScoreBoost = 1;
                    newElement.useFovBlock = false;
                    newElement.localScale = t.transform.localScale;

                    clusterElements.Add(newElement);
                }
            }
        }
    }

    [Button]
    public void SpawnAlternateFurniture()
    {
        ClearAllFurniture();

        for (int i = 0; i < clusterElements.Count; i++)
        {
            FurnitureCluster.FurnitureClusterRule element = clusterElements[i];

            Vector2 v2 = element.placements[0];

            //First get the placement point that's been rotated by just the angle
            Vector2 placementPos = RotateVector2CW(v2, 0);

            //Get the combined angle of the cluster angle and the furniture facing
            int combinedAngle = GetAngleForFurnitureFacing(element.facing);

            FurniturePreset randomPreset = GetRandomFurnitureForElement(element);

            if (randomPreset != null)
            {
                Transform anchorNode = null;

                WalkableRecorder.TileSetup findAnchor = tiles.Find(item => item.offset == placementPos);

                if (findAnchor != null)
                {
                    anchorNode = findAnchor.trans;

                    if (anchorNode != null)
                    {
                        GameObject spawnedObject = Instantiate(randomPreset.prefab, furnitureParent);
                        spawnedObject.transform.position = anchorNode.position;

                        //Spawn from ceiling...
                        if (element.furnitureClass.ceilingPiece)
                        {
                            spawnedObject.transform.localPosition = new Vector3(spawnedObject.transform.localPosition.x, (42 * 0.1f), spawnedObject.transform.localPosition.z);
                        }

                        spawnedObject.transform.localEulerAngles = new Vector3(0, combinedAngle, 0);

                        //Multiply scale
                        Vector3 prefabScale = randomPreset.prefab.transform.localScale;
                        spawnedObject.transform.localScale = new Vector3(prefabScale.x * element.localScale.x, prefabScale.y * element.localScale.y, prefabScale.z * element.localScale.z);

                        ClusterEditorFurniture cl = spawnedObject.AddComponent<ClusterEditorFurniture>();
                        cl.Setup(randomPreset);
                        spawnedFurniture.Add(cl);
                    }
                }
            }
        }
    }

    [Button]
    public void LoadCluster()
    {
        if(cluster != null)
        {
            ClearAllFurniture();
            ClearClusterList();

            clusterElements = new List<FurnitureCluster.FurnitureClusterRule>(cluster.clusterElements);
            SpawnAlternateFurniture();
        }
    }

    private Vector2 RotateVector2CW(Vector2 v, float degrees)
    {
        float sin = Mathf.Sin(-degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(-degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);

        return v;
    }

    private FurniturePreset GetRandomFurnitureForElement(FurnitureCluster.FurnitureClusterRule inputElement)
    {
        object[] allFurn = AssetLoader.Instance.GetAllFurniture().ToArray();
        FurniturePreset ret = null;

        List<FurniturePreset> options = new List<FurniturePreset>();

        foreach (FurniturePreset obj in allFurn)
        {
            if(obj.classes.Contains(inputElement.furnitureClass))
            {
                options.Add(obj);
            }
        }

        if(options.Count > 0)
        {
            ret = options[UnityEngine.Random.Range(0, options.Count)];
        }

        return ret;
    }

    [Button]
    public void SaveToCluster()
    {
        if(cluster != null)
        {
            cluster.clusterElements = new List<FurnitureCluster.FurnitureClusterRule>(clusterElements);

            //Save changes
#if UNITY_EDITOR
            cluster.UpdatePreCalculatedLimits();
            UnityEditor.EditorUtility.SetDirty(this);
            Debug.Log("Saved cluster to " + cluster.name);
#endif
        }
        else
        {
            Debug.Log("No cluster selected!");
        }
    }

    [Button]
    public void ClearAllFurniture()
    {
        while(spawnedFurniture.Count > 0)
        {
            if(spawnedFurniture[0] != null) DestroyImmediate(spawnedFurniture[0].gameObject);
            spawnedFurniture.RemoveAt(0);
        }
    }

    [Button]
    public void ClearClusterList()
    {
        clusterElements.Clear();
    }

    //Get the angle for furniture facing
    private int GetAngleForFurnitureFacing(FurnitureCluster.FurnitureFacing facing)
    {
        if (facing == FurnitureCluster.FurnitureFacing.up) return 0;
        else if (facing == FurnitureCluster.FurnitureFacing.down) return 180;
        else if (facing == FurnitureCluster.FurnitureFacing.left) return 270;
        else if (facing == FurnitureCluster.FurnitureFacing.right) return 90;

        return 0;
    }

    //Get the angle for furniture facing
    private FurnitureCluster.FurnitureFacing GetFacingForFurnitureAngle(float angle)
    {
        //Round to nearest whole 90 degree angle
        while(angle > 360)
        {
            angle -= 360;
        }

        while(angle < 0)
        {
            angle += 360;
        }

        int angleInt = Mathf.RoundToInt(angle / 90f) * 90; //Round to nearest 90

        if (angleInt == 0) return FurnitureCluster.FurnitureFacing.up;
        else if (angleInt == 90) return FurnitureCluster.FurnitureFacing.right;
        else if (angleInt == 180) return FurnitureCluster.FurnitureFacing.down;
        else if (angleInt == 270) return FurnitureCluster.FurnitureFacing.left;

        return FurnitureCluster.FurnitureFacing.up;
    }

}
