﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make company presets.
//Script pass 1
[CreateAssetMenu(fileName = "company_data", menuName = "Database/Company/Company Preset")]

public class CompanyPreset : SoCustomComparison
{
    public enum CompanyCategory { meal, snack, caffeine, groceries, washing, medical, recreational, retail };
    public enum SalaryRange { illegal, minimumWage, low, average, aboveAverage, high, veryHigh, extreme, millionaire };

    [Header("Category")]
    [Tooltip("The category of this company")]
    [ReorderableList]
    public List<CompanyCategory> companyCategories;

    [Header("Legality")]
    public bool isIllegal = false;

    [Header("Naming")]
    [Tooltip("Use a building's overidden name as main if there is one")]
    public bool useBuildingOverrideName = false;
    [Tooltip("If the above is used, add this extra suffix")]
    public List<string> overrideSuffixList = new List<string>();

    [Space(7)]
    [Tooltip("Chances of using the street name as a main company name")]
    [Range(0f, 1f)]
    public float useStreetNameChance = 0.05f;
    [Tooltip("Chances of using the district name as a main company name")]
    [Range(0f, 1f)]
    public float useDistrictNameChance = 0.05f;
    [Tooltip("Chances of using the owner's first name as a main company name")]
    [Range(0f, 1f)]
    public float useOwnerFirstNameChance = 0.025f;
    [Tooltip("Chances of using the owner's sur name as a main company name")]
    [Range(0f, 1f)]
    public float useOwnerSurNameChance = 0.025f;
    //[Tooltip("Use this name list to pick from for a main name")]
    //public string companyMainNamingList;
    [Tooltip("Chances of using the above name list as a main company name")]
    [Range(0f, 1f)]
    public float useCompanyNameListChance = 1f;
    [Tooltip("Chance of alliteration with prefix. This will add words with the same letter to the suffix to increase the chances of picking them by this amount")]
    [Range(0, 15)]
    public int aliterationWeight = 1;
    [Space(5)]
    [Range(0f, 1f)]
    public float prefixChance = 0.5f;
    [Tooltip("Use this name list to pick a prefix")]
    [ReorderableList]
    public List<string> prefixList = new List<string>();
    [Range(0f, 1f)]
    public float mainChance = 1f;
    [Tooltip("Use this name list to pick a main name")]
    [ReorderableList]
    public List<string> mainNamingList = new List<string>();
    [Tooltip("Append a random selection of this suffix list to the name")]
    [ReorderableList]
    public List<string> suffixList = new List<string>();

    public enum NameComponent { prefix, main, suffix};

    [System.Serializable]
    public class TheRule
    {
        public NameComponent component;
        public bool exists;
        public float chanceModifier;
    }

    [Tooltip("How likely is there to be 'the' appended to the start of this name")]
    public List<TheRule> theRules = new List<TheRule>();

    [Header("Wages")]
    [Tooltip("How much the lowest rank jobs earn")]
    public SalaryRange minimumSalary = SalaryRange.minimumWage;
    [Tooltip("How much the top rank jobs earn")]
    public SalaryRange topSalary = SalaryRange.veryHigh;
    [Tooltip("The pay grade curve from lowest rank to top rank")]
    public AnimationCurve payGradeCurve = AnimationCurve.Linear(0, 0, 1, 1);

    [Header("Retail")]
    [Tooltip("Does this company need a storefront?")]
    public bool publicFacing = true;
    [Tooltip("Is this company a self employed person?")]
    public bool isSelfEmployed = false;
    [EnableIf("isSelfEmployed")]
    [Tooltip("Automatically create self employed companies")]
    public bool autoCreate = false;
    [EnableIf("isSelfEmployed")]
    [Range(0, 10)]
    public int priority = 5;
    [EnableIf("isSelfEmployed")]
    public float cityPopRatio = 0.03f;
    [EnableIf("isSelfEmployed")]
    public int minimumNumber = 2;
    [EnableIf("isSelfEmployed")]
    public int maximumNumber = 10;
    [Tooltip("List of items that this shop stocks")]
    public List<MenuPreset> menus = new List<MenuPreset>();
    public bool recordSalesData = false;
    [EnableIf("recordSalesData")]
    public int previousFakeSalesRecords = 5;
    [EnableIf("recordSalesData")]
    [Tooltip("A citizen must have one of the following to log a sales record here...")]
    public List<CharacterTrait> requiredTraits = new List<CharacterTrait>();
    [Tooltip("Purchasing here also has a sell section")]
    public bool enableSelling = false;
    [EnableIf("enableSelling")]
    public bool enableSellingOfIllegalItems = false;
    [EnableIf("enableSelling")]
    public float sellValueMultiplier = 0.5f;

    [Header("Uniforms")]
    public List<Color> possibleUniformColours = new List<Color>();

    [Header("Work Hours")]
    [Tooltip("Preset detailing work hours")]
    public CompanyOpenHoursPreset workHours;

    [Header("Hierarchy")]
    [Tooltip("This structure of this company detailing jobs")]
    public CompanyStructurePreset structure;

    [Header("Special cases")]
    [Tooltip("Controls surveillance of building")]
    public bool controlsBuildingSurveillance = false;
    [Tooltip("For easily identifying a hotel")]
    public bool isHotel = false;
}
