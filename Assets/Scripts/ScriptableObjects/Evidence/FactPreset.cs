﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using NaughtyAttributes;

//A scriptable object that can be used to make evidence presets.
//Script pass 1
[CreateAssetMenu(fileName = "fact_data", menuName = "Database/Evidence/Fact Preset")]

public class FactPreset : SoCustomComparison
{
    [Header("Setup")]
    [Tooltip("Whenever this fact is displayed in an icon, use this sprite.")]
    public Sprite iconSpriteLarge;
    [Tooltip("Spawn this subclass. If left empty it will use the base class.")]
    public string subClass = string.Empty;
    [Tooltip("Allow to -> from duplicates of this evidence")]
    public bool allowDuplicates = true;
    [Tooltip("Allow reverse duplicates of this evidence")]
    public bool allowReverseDuplicates = true;

    [Header("Links")]
    [ReorderableList]
    [Tooltip("Link specifically to these data keys. These keys can be override manually by passing them in the constructor.")]
    public List<Evidence.DataKey> fromDataKeys = new List<Evidence.DataKey>();
    [ReorderableList]
    [Tooltip("Link specifically to these data keys. These keys can be override manually by passing them in the constructor.")]
    public List<Evidence.DataKey> toDataKeys = new List<Evidence.DataKey>();

    [Header("Discovery")]
    [Tooltip("Discover this evidence when it is created.")]
    public bool discoverOnCreate = false;
    [Tooltip("When discovered, this is eligable to be tagged as 'new information'")]
    public bool countsAsNewInformationOnDiscovery = true;
    [ReorderableList]
    [Tooltip("On discovery, apply these data keys to the 'from' evidence.")]
    public List<Evidence.DataKey> applyFromKeysOnDiscovery = new List<Evidence.DataKey>();
    [ReorderableList]
    [Tooltip("On discovery, apply these data keys to the 'to' evidence.")]
    public List<Evidence.DataKey> applyToKeysOnDiscovery = new List<Evidence.DataKey>();
    [InfoBox("When either of the connecting evidence has this trigger applied, the fact will become 'discovered'")]
    public List<Evidence.Discovery> discoveryTriggers = new List<Evidence.Discovery>();

    [Header("Misc.")]
    [Tooltip("Use this to rank facts within the facts list (lowest displayed first).")]
    [Range(0, 10)]
    public int factRank = 5;
}
