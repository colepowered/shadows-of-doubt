﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CountController : MonoBehaviour
{
    public RectTransform rect;
    public TextMeshProUGUI countText;
    public int count = 0;
    public bool invisibleIfZero = true;

    private void Awake()
    {
        countText = this.gameObject.GetComponentInChildren<TextMeshProUGUI>();
        rect = this.gameObject.GetComponent<RectTransform>();
        VisibilityCheck();
    }

    public void SetCount(int newVal)
    {
        if(countText == null) countText = this.gameObject.GetComponentInChildren<TextMeshProUGUI>();

        //Max is 99 to avoid 3 digitis
        count = Mathf.Min(newVal, 99);
        countText.text = count.ToString();
        if (count >= 10) countText.fontSize = 10;
        else countText.fontSize = 15;

        VisibilityCheck();
    }

    public void VisibilityCheck()
    {
        if(invisibleIfZero)
        {
            if (count <= 0)
            {
                this.gameObject.SetActive(false);
            }
            else
            {
                this.gameObject.SetActive(true);
            }
        }
    }
}
