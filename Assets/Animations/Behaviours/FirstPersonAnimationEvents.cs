﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonAnimationEvents : MonoBehaviour
{
    public void Punch()
    {
        FirstPersonItemController.Instance.MeleeAttack();
    }

    public void CounterAttack()
    {
        FirstPersonItemController.Instance.CounterAttack();
    }

    public void ThrowCoin()
    {
        FirstPersonItemController.Instance.ThrowCoin();
    }

    public void ThrowFood()
    {
        FirstPersonItemController.Instance.ThrowFood();
    }

    public void ThrowGrenade()
    {
        FirstPersonItemController.Instance.ThrowGrenade();
    }
}
