﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleLauncher : MonoBehaviour
{
    public int particles = 50;
    public ParticleSystem particleLauncher;
    public ParticleSystem splatterParticles;
    public Gradient particleColorGradient;
    public ParticleDecalPool splatDecalPool;

    List<ParticleCollisionEvent> collisionEvents = new List<ParticleCollisionEvent>();

    private void Awake()
    {
        //if (splatDecalPool == null) splatDecalPool = PrefabControls.Instance.splatterDecalParticles.gameObject.GetComponent<ParticleDecalPool>();
        //if (splatterParticles == null) splatterParticles = PrefabControls.Instance.splatterParticles;
    }

    void OnParticleCollision(GameObject other)
    {
        ParticlePhysicsExtensions.GetCollisionEvents(particleLauncher, other, collisionEvents);

        for (int i = 0; i < collisionEvents.Count; i++)
        {
            splatDecalPool.ParticleHit(collisionEvents[i], particleColorGradient);
            EmitAtLocation(collisionEvents[i]);
        }
    }

    void EmitAtLocation(ParticleCollisionEvent particleCollisionEvent)
    {
        splatterParticles.transform.position = particleCollisionEvent.intersection;
        splatterParticles.transform.rotation = Quaternion.LookRotation(particleCollisionEvent.normal);
        ParticleSystem.MainModule psMain = splatterParticles.main;
        psMain.startColor = particleColorGradient.Evaluate(Toolbox.Instance.Rand(0f, 1f));

        splatterParticles.Emit(1);
    }

    void Update()
    {
        if(particles > 0)
        {
            ParticleSystem.MainModule psMain = particleLauncher.main;
            psMain.startColor = particleColorGradient.Evaluate(Toolbox.Instance.Rand(0f, 1f));
            particleLauncher.Emit(1);
            particles--;
        }
        else
        {
            Destroy(this.transform.parent.gameObject);
        }
    }
}
