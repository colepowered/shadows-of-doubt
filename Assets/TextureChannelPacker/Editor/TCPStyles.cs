﻿using UnityEngine;

namespace Assets.TextureChannelPacker.Editor
{
    internal static class TCPStyles
    {
        public static readonly string TOOL_NAME = "Texture Channel Packer";
        public static readonly string TOOL_ABR = "TCP";

        public static readonly string WINDOW_TITLE = TOOL_ABR;
        public static readonly string WINDOW_ICON = "TCPIcon";
        public static readonly int WINDOW_BORDER_SPACING = 10;

        public static readonly string EXPORT_TITLE = string.Format("{0} image exporter", TOOL_NAME);
        public static readonly string EXPORT_DEFAULT_NAME = "ChannelPackedImage";
        public static readonly string EXPORT_EXTENSION = "png";

        public static readonly int UNITY_ELEMENT_SPACING = 4;
        public static readonly int UNITY_BUTTON_HEIGHT = 20;
        public static readonly int PREVIEW_BUTTON_WIDHT = 120;
        public static readonly int SQUARE_BUTTON_SIZE = 20;

        public static readonly int TEXTURE_PREVIEW_SIZE = 150;
        public static readonly float TEXTURE_ENUM_WIDTH_SCALE = 0.4f;
        public static readonly int INPUT_OUTPUT_VERTICALSPACING = 25;

        public static readonly TextureFormat PNG_FORMAT = TextureFormat.ARGB32;

        public static readonly Color DEFAULT_COLOR = Color.white;
        public static readonly Color RED_CHANNEL_COLOR = new Color(1, 0.25f, 0.25f);
        public static readonly Color GREEN_CHANNEL_COLOR = new Color(0.3f, 1, 0.3f);
        public static readonly Color BLUE_CHANNEL_COLOR = new Color(0.4f, 0.4f, 1);
        public static readonly Color ALPHA_CHANNEL_COLOR = new Color(0.8f, 0.8f, 0.8f);
        public static readonly Color DELETE_BUTTON_COLOR = new Color(1.0f, 0.2f, 0.125f);

        public static Color GetChannelColor(Channel channel)
        {
            switch (channel)
            {
                case Channel.Red:
                    return RED_CHANNEL_COLOR;

                case Channel.Green:
                    return GREEN_CHANNEL_COLOR;

                case Channel.Blue:
                    return BLUE_CHANNEL_COLOR;

                case Channel.Alpha:
                    return ALPHA_CHANNEL_COLOR;

                default:
                    LogSimpleError(string.Format("[{0}] An invalid channel type was passed: {1}. Returning the default color ...", typeof(TCPStyles), channel));
                    return DEFAULT_COLOR;
            }
        }

        public static Color GetChannelColor(int i)
        {
            return GetChannelColor((Channel)i);
        }

        public static void LogSimpleError(string message)
        {
            Debug.LogError(string.Format("[{0}] {1}", typeof(TextureChannelPacker), message));
        }
    }
}