﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

//Controls a progress bar.
//Script pass 1
public class ProgressBarController : MonoBehaviour
{
	//Settings
	public string barName = "New Bar";

    public float value = 0f;
    public float secondaryValue = 0f;
	public float barMin = 0f;
	public float barMax = 1f;

    public float progress = 0f;
    public float secondaryProgress = 0f;
    private int progressInt = 0;

    public bool usePips = false;
    public GameObject pipObject;
    public int pipValue = 0;
    public int pipNumber = 5;
    public bool useSecondaryPipValue = false;
    public int secondaryPipValue = 0;

    public bool displayProgress = true;
    public bool displayPercentageSign = false;
    public bool setNameOnStart = true;
    public bool useFloorValueForPercent = true;

    //Event for progress change
    public delegate void ValueChange(float newValue, int percentage);
    public event ValueChange OnProgressChange;

    //References
    public RectTransform rect;
    public TextMeshProUGUI barTitle;
	public TextMeshProUGUI progressText;
	public RectTransform barRect;
	public RectTransform progressRect;
	private RectTransform progressTextRect;

    //Pips
    private float pipXSize = 1f;
    public List<ProgressBarPipController> pips = new List<ProgressBarPipController>();
    public ProgressBarPipController hoverOverPip;

    void Awake()
	{
        rect = this.gameObject.GetComponent<RectTransform>();

        //Spawn pips
        if (usePips)
        {
            SetupPips();
        }

        if (displayProgress)
		{
			progressTextRect = progressText.gameObject.GetComponent<RectTransform>();
		}
		else if(progressText != null)
		{
			Destroy(progressText);
		}

        VisualUpdate();
	}

    public void SetupPips()
    {
        displayProgress = false;

        //Remove existings pips
        foreach(ProgressBarPipController p in pips)
        {
            Destroy(p.gameObject);
        }

        pips.Clear();

        for (int i = 0; i < pipNumber; i++)
        {
            GameObject newPip = null;

            if (pipObject != null)
            {
                newPip = Instantiate(pipObject, barRect);
            }
            else
            {
                newPip = Instantiate(PrefabControls.Instance.progressBarPip, barRect);
            }

            ProgressBarPipController pipController = newPip.GetComponent<ProgressBarPipController>();
            pipController.bar = this;
            pipController.rect = pipController.gameObject.GetComponent<RectTransform>();
            pipController.img = pipController.gameObject.GetComponent<Image>();
            pips.Add(pipController);
        }

        if(progressRect != null)
        {
            Destroy(progressRect.gameObject);
        }
    }

    void Start()
    {
        //value = Random.Range(0f, 1f);
        if (setNameOnStart)
        {
            SetName(barName);
        }
	}

	//Set name
	public void SetName(string newName)
	{
		if(barTitle == null) return;
		barTitle.text = Strings.Get("ui.interface", newName);
		this.gameObject.name = newName + " bar";
	}

    //Set the bar
    public void SetValue(float setTo)
    {
        //Clamp
        value = Mathf.Clamp(setTo, barMin, barMax);

        //Set progress
        progress = (value - barMin) / (barMax - barMin);

        if (useFloorValueForPercent)
        {
            progressInt = Mathf.Clamp(Mathf.FloorToInt(progress * 100f), 0, 100);
        }
        else
        {
            progressInt = Mathf.Clamp(Mathf.RoundToInt(progress * 100f), 0, 100);
        }

        //Set pip value
        pipValue = Mathf.RoundToInt(progress * pipNumber);

        VisualUpdate();

        //Fire Event
        if (OnProgressChange != null)
        {
            OnProgressChange(value, progressInt);
        }
    }

    //Set secondary value
    public void SetSecondaryValue(float setTo)
    {
        if (!useSecondaryPipValue) return;

        //Clamp
        secondaryValue = Mathf.Clamp(setTo, barMin, barMax);

        //Set progress
        secondaryProgress = (secondaryValue - barMin) / (barMax - barMin);

        //Set pip value
        secondaryPipValue = Mathf.RoundToInt(secondaryProgress * pipNumber);

        VisualUpdate();

        //Fire Event
        if (OnProgressChange != null)
        {
            OnProgressChange(value, progressInt);
        }
    }

    //Visually update the bar
    public void VisualUpdate()
    {
        //Display visually
        if (displayProgress & progressText != null)
        {
            progressText.text = progressInt.ToString();

            if(displayPercentageSign)
            {
                progressText.text += "%";
            }
        }

        //Make the RectTransform 'right' value
        if(progressRect != null)
        {
            float rightVal = barRect.rect.width * progress;
            progressRect.offsetMax = new Vector2(rightVal, progressRect.offsetMax.y);
        }

        //Position pips
        if(usePips)
        {
            if(barRect != null)
            {
                pipXSize = barRect.rect.width / (float)pipNumber;
            }

            for (int i = 0; i < pips.Count; i++)
            {
                ProgressBarPipController pip = pips[i];

                if(pip == null)
                {
                    pips.RemoveAt(i);
                    i--;
                    continue;
                }

                pip.rect.anchoredPosition = new Vector2(i * pipXSize, pip.rect.anchoredPosition.y);
                pip.rect.sizeDelta = new Vector2(pipXSize, pip.rect.sizeDelta.y);

                if(i < pipValue)
                {
                    pip.SetFilled(true, true);
                }
                else
                {
                    if (useSecondaryPipValue && i < secondaryPipValue)
                    {
                        pip.SetFilled(false, true);
                    }
                    else
                    {
                        pip.SetFilled(false, false);
                    }
                }
            }
        }


        //Switch the position of the progress text if required
        if (displayProgress)
        {
            //Attempt to display inside progress bar, if not enough room then use right side
            float textSpaceInside = progressRect.rect.width;
            float textSpaceOutside = barRect.rect.width - textSpaceInside;

            if(textSpaceInside >= progressTextRect.sizeDelta.x)
            {
                //Display inside bar
                progressTextRect.anchorMin = new Vector2(1f, 0.5f);
                progressTextRect.anchorMax = new Vector2(1f, 0.5f);
                progressTextRect.pivot = new Vector2(1f, 0.5f);
                progressText.alignment = TextAlignmentOptions.MidlineRight;
                progressTextRect.sizeDelta = new Vector2(75f, progressTextRect.sizeDelta.y);
                progressTextRect.localPosition = new Vector2(progressRect.rect.width - 6f, progressTextRect.localPosition.y);
            }
            else if (textSpaceOutside >= progressTextRect.sizeDelta.x)
            {
                //Display outside bar
                progressTextRect.anchorMin = new Vector2(0f, 0.5f);
                progressTextRect.anchorMax = new Vector2(0f, 0.5f);
                progressTextRect.pivot = new Vector2(0f, 0.5f);
                progressText.alignment = TextAlignmentOptions.MidlineLeft;
                progressTextRect.sizeDelta = new Vector2(75f, progressTextRect.sizeDelta.y);
                progressTextRect.localPosition = new Vector2(progressRect.rect.width + 6f, progressTextRect.localPosition.y);
            }
            //If not enough space for either, centre text and make width of bar
            else
            {
                progressTextRect.anchorMin = new Vector2(0.5f, 0.5f);
                progressTextRect.anchorMax = new Vector2(0.5f, 0.5f);
                progressTextRect.pivot = new Vector2(0.5f, 0.5f);
                progressText.alignment = TextAlignmentOptions.Midline;
                progressTextRect.sizeDelta = new Vector2(barRect.rect.width, progressTextRect.sizeDelta.y);
                progressTextRect.localPosition = new Vector2(barRect.rect.width * 0.5f, progressTextRect.localPosition.y);
            }
        }
    }
}
