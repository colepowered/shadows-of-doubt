using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class AutomaticDoorOpener : MonoBehaviour
{
    [Header("Setup")]
    public DoorMovementController door;

    [Header("State")]
    public List<Citizen> overlapping = new List<Citizen>();

    private void OnTriggerEnter(Collider other)
    {
        if (door == null) return;

        //Get citizen
        Citizen cit = other.GetComponentInParent<Citizen>();

        if(cit != null)
        {
            if (!overlapping.Contains(cit)) overlapping.Add(cit);
            this.enabled = true;

            if (!door.isOpen && !door.isOpening)
            {
                door.SetOpen(1, cit, false);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (door == null) return;

        Citizen cit = other.GetComponentInParent<Citizen>();

        if(cit != null)
        {
            overlapping.Remove(cit);
        }
    }

    private void Awake()
    {
        this.enabled = false; //Start disabled
    }

    private void Update()
    {
        if (door == null)
        {
            this.enabled = false;
            return;
        }

        if (overlapping.Count <= 0)
        {
            if (door.isOpen || door.isOpening)
            {
                door.SetOpen(0f, null, false);
                this.enabled = false;
            }
            else this.enabled = false;
        }
    }
}
