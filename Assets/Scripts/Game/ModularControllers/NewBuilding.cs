﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using FMOD.Studio;
using UnityEditor;
using NaughtyAttributes;

public class NewBuilding : Controller
{
    [Header("ID")]
    public int buildingID = 0;
    public static int assignID = 1;
    public string seed;

    [Header("Building Contents")]
    public Dictionary<int, NewFloor> floors = new Dictionary<int, NewFloor>();
    public List<NewAddress> lobbies = new List<NewAddress>();
    public List<GameObject> spawnedCables = new List<GameObject>();
    public List<SideSign> sideSigns = new List<SideSign>();
    public List<AirDuctGroup> airDucts = new List<AirDuctGroup>();
    public Dictionary<Vector3Int, AirDuctGroup.AirDuctSection> ductMap = new Dictionary<Vector3Int, AirDuctGroup.AirDuctSection>();

    [Header("Alarms")]
    public List<Interactable> alarms = new List<Interactable>(); //List of all interactables to turn on/off with alarm
    public List<Interactable> sentryGuns = new List<Interactable>();
    public List<Interactable> otherSecurity = new List<Interactable>();
    public bool alarmActive = false;
    public AlarmTargetMode targetMode = AlarmTargetMode.illegalActivities;
    public float targetModeSetAt; //Keeps track of when this was altered so we can change it back
    public List<Human> alarmTargets = new List<Human>();
    public float alarmTimer = 0f;
    public List<Interactable> securityCameras = new List<Interactable>();
    public float wantedInBuilding = 0f;
    public List<AudioController.LoopingSoundInfo> alarmPALoops = new List<AudioController.LoopingSoundInfo>();

    public enum AlarmTargetMode { illegalActivities, notPlayer, nonResidents, everybody, nobody};

    //Directional culling trees
    public Dictionary<Vector2, Dictionary<NewRoom, List<NewRoom.CullTreeEntry>>> directionalCullingTrees = new Dictionary<Vector2, Dictionary<NewRoom, List<NewRoom.CullTreeEntry>>>();

    [Header("Exterior Data")]
    public MaterialGroupPreset extWallMaterial;
    public Material extMat;

    public class DuctPlacementData
    {
        public AirDuctGroup.AirVent originVent;
        public AirDuctGroup.AirVent destinationVent;

        public Vector3Int previous;
        public Vector3Int next;
    }

    //As the regular system uses a Y-based node space of 1 per floor, we have to multiply the Z value by 3 in order to have enough space to calculate routes between floors
    public Dictionary<Vector3Int, NewNode> validVentSpace = new Dictionary<Vector3Int, NewNode>();

    [System.Serializable]
    public class SideSign
    {
        //These values refer to the index in the preset class of the link point and the sign spawned.
        public int anchorPointIndex;
        public int signPrefabIndex;
    }

    [Header("Culling: Mesh")]
    public GameObject buildingModelBase;
    public List<GameObject> buildingModelsActual = new List<GameObject>();
    public List<GameObject> buildingModelsLights = new List<GameObject>();
    public List<Collider> colliders = new List<Collider>();
    public Transform environmentalSettingsObject;
    public bool displayBuildingModel = true; //True if displaying the building model
    public bool activeColliders = true;
    private List<GameObject> selectivelyHidden = new List<GameObject>();
    public List<Collider> snowColliders = new List<Collider>();

    [Header("Culling: Lighting")]
    public int interiorLightCullingLayer = 0; //Set this up so adjacent buildings are on different layers 1,2,3
    public List<LightController> allInteriorMainLights = new List<LightController>(); //All interior lights

    [Header("Location")]
    public BuildingPreset preset;
    public int rotations = 0;
    public enum Direction { North, East, South, West };
    public Direction facing = Direction.North;
    public CityTile cityTile;
    public Vector3Int globalTileCoords; //Coordinates within all tiles. This is the centre of the building.
    public bool isInaccessible = false;
    private float distance; //Used in comparisons
    public Vector3 worldPosition; //Threading cannot access transform, so store position here

    [Header("Entrances")]
    public NewWall mainEntrance = null;
    public StreetController street = null;
    public List<NewWall> additionalEntrances = new List<NewWall>();

    [Header("Stairwells")]
    //Stairwells where keys are the starting tile
    public Dictionary<NewTile, Elevator> stairwells = new Dictionary<NewTile, Elevator>();

    [Header("Emission")]
    //Emission
    public Texture2D emissionTextureInstanced; //Instanced version of the emission texutre
    public Texture2D emissionTextureUnlit; //Reference to the original unlit texture (non-instanced)
    //public float buildingLobbyLightsOn = 9f; //Building lobby lights turn on at
    //public float buildingLobbyLightsOff = 16f; //Building lobby lights turn off at

    [Header("Environmental")]
    public Volume volume;

    [Header("Evidence")]
    [System.NonSerialized]
    public EvidenceBuilding evidenceEntry = null;
    [System.NonSerialized]
    public EvidenceMultiPage residentRoster;

    public List<TelephoneController.PhoneCall> callLog = new List<TelephoneController.PhoneCall>();

    [Header("Decor")]
    public DesignStylePreset designStyle; //Design style chosen for the address
    public Color wood; //Colour of the wood for this address

    public MaterialGroupPreset floorMaterial;
    public Toolbox.MaterialKey floorMatKey;

    public MaterialGroupPreset ceilingMaterial;
    public Toolbox.MaterialKey ceilingMatKey;

    public MaterialGroupPreset defaultWallMaterial;
    public Toolbox.MaterialKey defaultWallKey;

    public ColourSchemePreset colourScheme;

    public bool triedGroundFloorRandom = false;
    public bool groundFloorOverride = false; //Don't need to save this as it's only relevent on creation: If the ground floor uses overrides

    public bool triedBasementRandom = false;
    public bool basementFloorOverride = false; //Don't need to save this as it's only relevent on creation: If the ground floor uses overrides

    //Overrides
    public MaterialGroupPreset floorMaterialOverride;
    public MaterialGroupPreset ceilingMaterialOverride;
    public MaterialGroupPreset defaultWallMaterialOverride;

    //Overrides (Basement)
    public MaterialGroupPreset basementFloorMaterialOverride;
    public MaterialGroupPreset basementCeilingMaterialOverride;
    public MaterialGroupPreset basementDefaultWallMaterialOverride;

    public NewAddress nameOverride = null;

    [Header("Lost & Found")]
    public List<GameplayController.LostAndFound> lostAndFound = new List<GameplayController.LostAndFound>();

    [Header("Debug")]
    public List<string> debugDecor = new List<string>();

    //Add a floor
    public void AddNewFloor(NewFloor newFloor)
    {
        if(!floors.ContainsKey(newFloor.floor))
        {
            floors.Add(newFloor.floor, newFloor);

            //Set location info
            newFloor.building = this;

            //Tree update
            foreach (NewAddress address in newFloor.addresses)
            {
                address.building = this;

                foreach(NewRoom room in address.rooms)
                {
                    room.building = this;

                    foreach (NewNode node in room.nodes)
                    {
                        node.building = this;
                    }
                }
            }
        }
    }

    public void Setup(CityTile newGroundmap, BuildingPreset newpreset)
    {
        //Assign ID
        buildingID = assignID;
        assignID++;
        name = "Building " + buildingID;
        this.transform.name = name;

        seed = (Toolbox.Instance.SeedRand(0, 99999999) * buildingID).ToString();

        cityTile = newGroundmap;
        cityTile.building = this;
        globalTileCoords = CityData.Instance.CityTileToTile(cityTile.cityCoord); //This will use the centre of the groundmap tile
        preset = newpreset;

        if(preset.defaultExteriorWallMaterial.Count > 0)
        {
            SetExteriorWallMaterialDefault(preset.defaultExteriorWallMaterial[Toolbox.Instance.GetPsuedoRandomNumber(0, preset.defaultExteriorWallMaterial.Count, buildingID.ToString())]);
        }

        //Calculate the interior light culling group from the groupmap coordinate (0-3)
        if (cityTile.cityCoord.x % 2 != 0)
        {
            interiorLightCullingLayer += 1;
        }

        if (cityTile.cityCoord.y % 2 != 0)
        {
            interiorLightCullingLayer += 2;
        }

        //Setup lighting times
        //buildingLobbyLightsOff = Toolbox.Instance.Rand(CityControls.Instance.lightsOff.x + 0.05f, CityControls.Instance.lightsOff.y - 0.05f);
        //buildingLobbyLightsOn = Toolbox.Instance.Rand(CityControls.Instance.lightsOn.x + 0.05f, CityControls.Instance.lightsOn.y - 0.05f);

        //Create evidence entry
        CreateEvidence();

        //Add to directory
        CityData.Instance.buildingDirectory.Add(this);

        //Generate building's decor scheme
        UpdateColourSchemeAndMaterials();

        SetupModel();

        SpawnNeonSideSigns();

        //Register as smokestack
        if(preset.featuresSmokestack)
        {
            CitizenBehaviour.Smokestack newSmokestack = new CitizenBehaviour.Smokestack { building = this, timer = Toolbox.Instance.Rand(preset.spawnInterval.x, preset.spawnInterval.y) };

            if (AudioDebugging.Instance.overrideSmokeStackEmissionFrequency && Game.Instance.devMode)
            {
                newSmokestack.timer = AudioDebugging.Instance.chemSmokeStackEmissionFrequency / 60f;
            }

            CitizenBehaviour.Instance.smokestacks.Add(newSmokestack);
        }

        worldPosition = this.transform.position;
    }

    public void Load(CitySaveData.BuildingCitySave data, CityTile newCityTile)
    {
        //Assign ID
        buildingID = data.buildingID;
        assignID = Mathf.Max(assignID, buildingID + 1); //Make sure others won't overwrite this ID

        //For now, create evidence
        CreateEvidence();

        cityTile = newCityTile;
        cityTile.building = this;
        globalTileCoords = CityData.Instance.CityTileToTile(cityTile.cityCoord); //This will use the centre of the groundmap tile

        //Get preset
        Toolbox.Instance.LoadDataFromResources<BuildingPreset>(data.preset, out preset);

        //Calculate the interior light culling group from the groupmap coordinate (0-3)
        if (cityTile.cityCoord.x % 2 != 0)
        {
            interiorLightCullingLayer += 1;
        }

        if (cityTile.cityCoord.y % 2 != 0)
        {
            interiorLightCullingLayer += 2;
        }

        if (preset.defaultExteriorWallMaterial.Count > 0)
        {
            SetExteriorWallMaterialDefault(preset.defaultExteriorWallMaterial[Toolbox.Instance.GetPsuedoRandomNumber(0, preset.defaultExteriorWallMaterial.Count, buildingID.ToString())]);
        }

        //Setup lighting times
        //buildingLobbyLightsOff = Toolbox.Instance.Rand(CityControls.Instance.lightsOff.x + 0.05f, CityControls.Instance.lightsOff.y - 0.05f);
        //buildingLobbyLightsOn = Toolbox.Instance.Rand(CityControls.Instance.lightsOn.x + 0.05f, CityControls.Instance.lightsOn.y - 0.05f);

        ////Create evidence entry
        //evidenceEntry = EvidenceCreator.Instance.CreateEvidence("Building", this) as EvidenceBuilding;
        //residentRoster = EvidenceCreator.Instance.CreateEvidence("ResidentRoster", this) as EvidenceMultiPage;

        //Add to directory
        CityData.Instance.buildingDirectory.Add(this);

        //Set inaccessable
        if(data.isInaccessible)
        {
            SetInaccessible();
        }

        //load building's decor scheme
        Toolbox.Instance.LoadDataFromResources<DesignStylePreset>(data.designStyle, out designStyle); //Design style chosen for the address
        wood = data.wood; //Colour of the wood for this address

        Toolbox.Instance.LoadDataFromResources<MaterialGroupPreset>(data.floorMaterial, out floorMaterial);
        floorMatKey = data.floorMatKey;

        Toolbox.Instance.LoadDataFromResources<MaterialGroupPreset>(data.ceilingMaterial, out ceilingMaterial);
        ceilingMatKey = data.ceilingMatKey;

        Toolbox.Instance.LoadDataFromResources<MaterialGroupPreset>(data.defaultWallMaterial, out defaultWallMaterial);
        defaultWallKey = data.defaultWallKey;

        if(data.extWallMaterial != null && data.extWallMaterial.Length > 0) Toolbox.Instance.LoadDataFromResources<MaterialGroupPreset>(data.extWallMaterial, out extWallMaterial);

        if (data.wallMatOverride != null && data.wallMatOverride.Length > 0) Toolbox.Instance.LoadDataFromResources<MaterialGroupPreset>(data.wallMatOverride, out defaultWallMaterialOverride);
        if (data.ceilingMatOverride != null && data.ceilingMatOverride.Length > 0) Toolbox.Instance.LoadDataFromResources<MaterialGroupPreset>(data.ceilingMatOverride, out ceilingMaterialOverride);
        if (data.floorMatOverride != null && data.floorMatOverride.Length > 0) Toolbox.Instance.LoadDataFromResources<MaterialGroupPreset>(data.floorMatOverride, out floorMaterialOverride);

        if (data.wallMatOverrideB != null && data.wallMatOverrideB.Length > 0) Toolbox.Instance.LoadDataFromResources<MaterialGroupPreset>(data.wallMatOverrideB, out basementDefaultWallMaterialOverride);
        if (data.ceilingMatOverrideB != null && data.ceilingMatOverrideB.Length > 0) Toolbox.Instance.LoadDataFromResources<MaterialGroupPreset>(data.ceilingMatOverrideB, out basementCeilingMaterialOverride);
        if (data.floorMatOverrideB != null && data.floorMatOverrideB.Length > 0) Toolbox.Instance.LoadDataFromResources<MaterialGroupPreset>(data.floorMatOverrideB, out basementFloorMaterialOverride);

        //Spawn building model
        SetupModel();

        sideSigns = new List<SideSign>(data.sideSigns);

        if(data.name != null)
        {
            name = data.name;
        }
        else
        {
            name = preset.name;
        }

        this.transform.name = name;

        SetupEmissionTexture();

        //Load facing
        SetFacing(data.facing);

        SpawnNeonSideSigns();

        //Register as smokestack
        if (preset.featuresSmokestack)
        {
            CitizenBehaviour.Smokestack newSmokestack = new CitizenBehaviour.Smokestack { building = this, timer = Toolbox.Instance.Rand(preset.spawnInterval.x, preset.spawnInterval.y, true) };

            if (AudioDebugging.Instance.overrideSmokeStackEmissionFrequency && Game.Instance.devMode)
            {
                newSmokestack.timer = AudioDebugging.Instance.chemSmokeStackEmissionFrequency / 60f;
            }

            CitizenBehaviour.Instance.smokestacks.Add(newSmokestack);
        }

        worldPosition = this.transform.position;
    }

    private void SetupModel()
    {
        //Game only setup
        if (!SessionData.Instance.isFloorEdit)
        {
            //Spawn building model
            if (preset.prefab != null)
            {
                buildingModelBase = Instantiate(preset.prefab, this.transform);

                Transform[] allT = Toolbox.Instance.GetAllTransforms(buildingModelBase.transform);
                int modelMask = Toolbox.Instance.CreateLayerMask(Toolbox.LayerMaskMode.onlyCast, 0, 1, 8);

                //Get building models
                foreach (Transform child in allT)
                {
                    if (child.CompareTag("BuildingModel") && !buildingModelsActual.Contains(child.gameObject))
                    {
                        buildingModelsActual.Add(child.gameObject);
                        child.gameObject.isStatic = true; //NOTE: This has no effects in builds!

                        //Search and apply to LOD models
                        foreach (Transform t in allT)
                        {
                            if (t.name.Substring(3) == "_static_" + child.name)
                            {
                                t.tag = "BuildingModel";
                                t.gameObject.layer = 27;

                                MeshRenderer rend = t.GetComponent<MeshRenderer>();

                                if(rend != null)
                                {
                                    rend.renderingLayerMask = (uint)modelMask;
                                    rend.staticShadowCaster = true; //Set as static shadowcaster
                                }

                                if(!buildingModelsActual.Contains(t.gameObject)) buildingModelsActual.Add(t.gameObject);
                            }
                        }
                    }
                    else if(child.CompareTag("BuildingModelLights"))
                    {
                        if(!buildingModelsActual.Contains(child.gameObject))
                        {
                            buildingModelsActual.Add(child.gameObject);
                            child.gameObject.isStatic = true; //NOTE: This has no effects in builds!

                            //Search and apply to LOD models
                            foreach (Transform t in allT)
                            {
                                if (t.name.Substring(3) == "_static_" + child.name)
                                {
                                    t.tag = "BuildingModelLights";
                                    t.gameObject.layer = 27;

                                    MeshRenderer rend = t.GetComponent<MeshRenderer>();

                                    if (rend != null)
                                    {
                                        rend.renderingLayerMask = (uint)modelMask;
                                    }

                                    if (!buildingModelsActual.Contains(t.gameObject)) buildingModelsActual.Add(t.gameObject);
                                }
                            }
                        }

                        if(!buildingModelsLights.Contains(child.gameObject))
                        {
                            buildingModelsLights.Add(child.gameObject);
                        }
                    }
                    else if(child.CompareTag("BuildingSnowCollider"))
                    {
                        child.gameObject.layer = 2;
                        child.gameObject.isStatic = true; //NOTE: This has no effects in builds!

                        Collider coll = child.GetComponent<Collider>();

                        if(coll != null)
                        {
                            coll.isTrigger = true;
                            snowColliders.Add(coll);
                            PrecipitationParticleSystemController.Instance.AddAreaTrigger(coll);
                        }
                    }
                }

                buildingModelBase.tag = "BuildingModel";

                //Get colliders
                foreach(GameObject g in buildingModelsActual)
                {
                    Collider[] coll = g.GetComponentsInChildren<Collider>();

                    foreach (Collider c in coll)
                    {
                        if (c.gameObject.CompareTag("BuildingSnowCollider")) continue; //Skip snow colliders...

                        if (!colliders.Contains(c))
                        {
                            colliders.Add(c);
                        }
                    }
                }
            }

            name = preset.name;

            SetupEmissionTexture();

            if (buildingModelBase != null) environmentalSettingsObject = buildingModelBase.transform.Find("EnvironmentVolume");

            //Setup environmental volume
            if (environmentalSettingsObject != null)
            {
                SetupEnvironment();
            }
        }
    }

    private void SetupEmissionTexture()
    {
        //I'm having problems with the below in the actual build (but not in-editor). Trying to get the texture from the preset instead...
        //emissionTextureUnlit = actualBuildingModelRenderer.sharedMaterial.GetTexture("_EmissionMap") as Texture2D;
        emissionTextureUnlit = preset.emissionMapUnlit;

        if (emissionTextureUnlit != null)
        {
            Transform[] allT = Toolbox.Instance.GetAllTransforms(buildingModelBase.transform);

            foreach (GameObject g in buildingModelsLights)
            {
                MeshRenderer lightingBuildingModelRenderer = g.GetComponent<MeshRenderer>();

                if(lightingBuildingModelRenderer != null)
                {
                    if (emissionTextureInstanced == null)
                    {
                        emissionTextureInstanced = Instantiate(emissionTextureUnlit) as Texture2D; //clone the material
                    }

                    lightingBuildingModelRenderer.material.SetTexture("_EmissiveColorMap", emissionTextureInstanced); //set the material equal to the clone
                                                                                                                      //Each building model should now have an instanced version of it's emmission map

                    //Add to get effected by rain...
                    if(!SessionData.Instance.wetMaterials.Exists(item => item.mat == lightingBuildingModelRenderer.material))
                    {
                        SessionData.Instance.wetMaterials.Add(new SessionData.WetMaterial { mat = lightingBuildingModelRenderer.material, multiplier = 1f });
                    }

                    //Search and apply to LOD models
                    foreach(Transform t in allT)
                    {
                        if (t.name.Substring(3) == "_static_" + g.name)
                        {
                            MeshRenderer lodRender = t.gameObject.GetComponent<MeshRenderer>();

                            if(lodRender != null)
                            {
                                lodRender.sharedMaterial = lightingBuildingModelRenderer.sharedMaterial;
                            }
                        }
                    }
                }
            }

            ////Get the child of this prefab (ie above ground floor-- the actual building model)
            //MeshRenderer lightingBuildingModelRenderer = buildingModelLights.GetComponent<MeshRenderer>();
            //if (lightingBuildingModelRenderer == null) Game.LogError("Cannot find a mesh attached to this building! " + preset.prefab.name);
            ////Game.Log(buildingRenderer);
            ////Game.Log(buildingRenderer.material.GetTexture("_EmissionMap"));

            //Texture2D cloneTex = Instantiate(emissionTextureUnlit) as Texture2D; //clone the material

            ////SRP
            ////buildingRenderer.material.SetTexture("_EmissionMap", cloneTex); //set the material equal to the clone

            ////HDRP
            //lightingBuildingModelRenderer.material.SetTexture("_EmissiveColorMap", cloneTex); //set the material equal to the clone
            //                                                                                //Each building model should now have an instanced version of it's emmission map

            //emissionTextureInstanced = cloneTex; //Set emission texture reference

            ////Add to get effected by rain...
            //SessionData.Instance.wetMaterials.Add(new SessionData.WetMaterial { mat = lightingBuildingModelRenderer.material, multiplier = 1f });
        }
    }

    //Set the target mode for alarm systems
    public void SetTargetMode(NewBuilding.AlarmTargetMode newMode, bool setResetTimer = true)
    {
        targetMode = newMode;

        Game.Log("Set target mode for " + name + ": " + targetMode);

        if (setResetTimer)
        {
            targetModeSetAt = SessionData.Instance.gameTime;

            if (targetMode != NewBuilding.AlarmTargetMode.illegalActivities)
            {
                if (!GameplayController.Instance.alteredSecurityTargetsBuildings.Contains(this))
                {
                    GameplayController.Instance.alteredSecurityTargetsBuildings.Add(this);
                }
            }
        }
    }

    //Setup environmental volume
    private void SetupEnvironment()
    {
        ////Setup environmental volume
        //if (preset.environmentProfile != null && environmentalSettingsObject != null)
        //{
        //    //Find the environment area, parent it to this instead of the building model.
        //    environmentalSettingsObject.SetParent(this.gameObject.transform, true);
        //    environmentalSettingsObject.gameObject.SetActive(true); //Make sure this is active

        //    //Set this to the player layer so interaction raycast ignores it
        //    environmentalSettingsObject.gameObject.layer = 18;

        //    volume = environmentalSettingsObject.gameObject.AddComponent<Volume>();
        //    volume.isGlobal = false;
        //    volume.priority = 2; //Higher than outdoors global profile
        //    volume.blendDistance = 0f;
        //    volume.profile = preset.environmentProfile;
        //}
        //else
        //{
            //Otherwise destroy this
            if(environmentalSettingsObject != null)
            {
                Destroy(environmentalSettingsObject.gameObject);
            }
        //}
    }

    //Generate a colour scheme and update all materials
    public void UpdateColourSchemeAndMaterials()
    {
        //Select a wood colour
        wood = InteriorControls.Instance.woods[Toolbox.Instance.GetPsuedoRandomNumberContained(0, InteriorControls.Instance.woods.Count, seed, out seed)];

        //Update design style
        List<DesignStylePreset> possiblePresets = new List<DesignStylePreset>();

        //Add option using this citizen's traits...
        foreach (DesignStylePreset dPreset in Toolbox.Instance.allDesignStyles)
        {
            if(preset.forceBuildingDesignStyles.Count > 0)
            {
                if (!preset.forceBuildingDesignStyles.Contains(dPreset)) continue;
            }
            else if (!dPreset.includeInPersonalityMatching || dPreset.isBasement) continue;

            int av = 1;

            //Get matching stats...
            int humility = 10 - Mathf.RoundToInt(Mathf.Abs(dPreset.humility - (10f - (int)cityTile.landValue * 2.5f))); //Inverse land value
            int emotionality = 10 - Mathf.RoundToInt(Toolbox.Instance.GetPsuedoRandomNumberContained(0, 10, seed, out seed));
            int extraversion = 10 - Mathf.RoundToInt(Toolbox.Instance.GetPsuedoRandomNumberContained(0, 10, seed, out seed));
            int agreeableness = 10 - Mathf.RoundToInt(Toolbox.Instance.GetPsuedoRandomNumberContained(0, 10, seed, out seed));
            int conscientiousness = 10 - Mathf.RoundToInt(Toolbox.Instance.GetPsuedoRandomNumberContained(0, 10, seed, out seed));
            int creativity = Mathf.RoundToInt(Mathf.Abs(dPreset.creativity - (10f - (int)dPreset.modernity))); //Inverse modernity

            //Add and divide by 6 to give a score out of 10
            av = Mathf.FloorToInt((float)(humility + emotionality + extraversion + agreeableness + conscientiousness + creativity) / 6f);

            for (int i = 0; i < av; i++)
            {
                possiblePresets.Add(dPreset);
            }
        }

        if (possiblePresets.Count <= 0)
        {
            possiblePresets.Add(CityControls.Instance.fallbackStyle);
            Game.Log("CityGen: No compatible design styles found for " + name + ", using backup style...");
        }

        //You should now have a list proportionally weighted towards personality...
        designStyle = possiblePresets[Toolbox.Instance.GetPsuedoRandomNumberContained(0, possiblePresets.Count, seed, out seed)];

        List<ColourSchemePreset> possibleColourSchemes = new List<ColourSchemePreset>();

        foreach (ColourSchemePreset colourScheme in Toolbox.Instance.allColourSchemes)
        {
            //Weight by design style with a hint of owner's creativity (10%)
            int modernity = preset.modernity;

            //Use land value for this
            int cleanness = Mathf.RoundToInt(Mathf.Clamp((int)cityTile.landValue * 2.5f, 0f, 10f));

            //Use weighted random value weighted to 5 (middle range)
            string rKey = buildingID.ToString();

            int loudness = Mathf.RoundToInt(Toolbox.Instance.RandomRangeWeightedSeedContained(0, 10, 5, rKey, out rKey, 4));

            //Use weighted random value weighted to 5 (middle range)
            int emotive = Mathf.RoundToInt(Toolbox.Instance.RandomRangeWeightedSeedContained(0, 10, 5, rKey, out rKey, 4));

            //Add and divide by 8 to give a score out of 5
            int av = Mathf.FloorToInt((float)(modernity + cleanness + loudness + emotive) / 8f);

            for (int i = 0; i < av; i++)
            {
                possibleColourSchemes.Add(colourScheme);
            }
        }

        if (possibleColourSchemes.Count <= 0) possibleColourSchemes.Add(CityControls.Instance.fallbackColourScheme);
        colourScheme = possibleColourSchemes[Toolbox.Instance.GetPsuedoRandomNumberContained(0, possibleColourSchemes.Count, seed, out seed)];

        float wealthLevel = Toolbox.Instance.GetNormalizedLandValue(this);

        string randomKey = buildingID.ToString();

        defaultWallMaterial = Toolbox.Instance.SelectMaterial(preset.lobbyPreset.roomConfig[0].roomClass, wealthLevel, designStyle, MaterialGroupPreset.MaterialType.walls, randomKey, out randomKey);
        MaterialGroupPreset.MaterialVariation chosenVar = defaultWallMaterial.variations[Toolbox.Instance.GetPsuedoRandomNumberContained(0, defaultWallMaterial.variations.Count, seed, out seed)];
        defaultWallKey = MaterialsController.Instance.GenerateMaterialKey(chosenVar, colourScheme, null, true, this);

        ceilingMaterial = Toolbox.Instance.SelectMaterial(preset.lobbyPreset.roomConfig[0].roomClass, wealthLevel, designStyle, MaterialGroupPreset.MaterialType.ceiling, randomKey, out randomKey);
        MaterialGroupPreset.MaterialVariation chosenVar2 = ceilingMaterial.variations[Toolbox.Instance.GetPsuedoRandomNumberContained(0, ceilingMaterial.variations.Count, seed, out seed)];
        ceilingMatKey = MaterialsController.Instance.GenerateMaterialKey(chosenVar2, colourScheme, null, true, this);

        floorMaterial = Toolbox.Instance.SelectMaterial(preset.lobbyPreset.roomConfig[0].roomClass, wealthLevel, designStyle, MaterialGroupPreset.MaterialType.floor, randomKey, out randomKey);
        MaterialGroupPreset.MaterialVariation chosenVar3 = floorMaterial.variations[Toolbox.Instance.GetPsuedoRandomNumberContained(0, floorMaterial.variations.Count, seed, out seed)];
        floorMatKey = MaterialsController.Instance.GenerateMaterialKey(chosenVar3, colourScheme, null, true, this);

        if(Game.Instance.devMode)
        {
            debugDecor.Add("New Decor: Chosen building decor style...");
        }
    }

    //Create interior using floor presets. Facing must be done first as some decisions depend on it.
    //This isn't called by the editor at all so don't worry about that stuff.
    public void LoadInterior()
    {
        if (isInaccessible) return;

        if (preset.floorLayouts.Count <= 0 && !preset.nonEnterable)
        {
            Game.Log("CityGen: Building " + name + " has no floor data.");
        }

        //Load from data
        if(!CityConstructor.Instance.generateNew)
        {
            //Get tile data
            CitySaveData.CityTileCitySave tileData = CityConstructor.Instance.currentData.cityTiles.Find(item => item.cityCoord == cityTile.cityCoord);

            foreach(CitySaveData.FloorCitySave floor in tileData.building.floors)
            {
                //Create a new floor
                GameObject newFloor = Instantiate(PrefabControls.Instance.floor, this.transform);
                NewFloor editFloor = newFloor.GetComponent<NewFloor>();
                editFloor.Load(floor, this);
            }

            //Load additional settings that must be done after creation...
            foreach (CitySaveData.FloorCitySave floor in tileData.building.floors)
            {
                foreach (CitySaveData.AddressCitySave addressData in floor.addresses)
                {
                    foreach (CitySaveData.RoomCitySave roomData in addressData.rooms)
                    {
                        //Connect common rooms...
                        if(roomData.commonRooms.Count > 0)
                        {
                            //Find created room
                            if(!CityData.Instance.roomDictionary.ContainsKey(roomData.id))
                            {
                                Game.LogError("CityGen: Cannot find room " + roomData.id + ": This should have been loaded already and put into the dictionary...");
                            }
                            else
                            {
                                NewRoom room = CityData.Instance.roomDictionary[roomData.id];

                                foreach (int roomID in roomData.commonRooms)
                                {
                                    NewRoom commonRoom = CityData.Instance.roomDictionary[roomID];
                                    room.commonRooms.Add(commonRoom);
                                }
                            }
                        }

                        //Set lightswitches
                        foreach(CitySaveData.NodeCitySave nodeData in roomData.nodes)
                        {
                            foreach(CitySaveData.WallCitySave wallData in nodeData.w)
                            {
                                if(wallData.cl > -1)
                                {
                                    NewNode node = PathFinder.Instance.nodeMap[nodeData.nc];
                                    NewWall wall = node.walls.Find(item => item.id == wallData.id);
                                    NewRoom lightswitchToRoom = CityData.Instance.roomDictionary[wallData.cl];
                                    if(lightswitchToRoom != null && wall != null) wall.SetAsLightswitch(lightswitchToRoom);
                                }
                            }
                        }
                    }
                }
            }

            //Create air duct groups
            foreach (CitySaveData.AirDuctGroupCitySave airDuct in tileData.building.airDucts)
            {
                GameObject newDuctGrp = new GameObject();
                newDuctGrp.transform.SetParent(this.transform, false);
                newDuctGrp.transform.localPosition = Vector3.zero;
                AirDuctGroup newGroup = newDuctGrp.AddComponent<AirDuctGroup>();
                newGroup.Load(airDuct, this);
            }

            //Load adjoining air ducts
            foreach (CitySaveData.AirDuctGroupCitySave airDuct in tileData.building.airDucts)
            {
                AirDuctGroup currentGrp = airDucts.Find(item => item.ductID == airDuct.id);

                foreach(int adj in airDuct.adjoining)
                {
                    AirDuctGroup adjoining = airDucts.Find(item => item.ductID == adj);

                    currentGrp.AddAdjoiningDuctGroup(adjoining);
                    adjoining.AddAdjoiningDuctGroup(currentGrp);
                }
            }
        }
        else
        {
            List<NewTile> stairs = new List<NewTile>();

            //Pick control room floors
            int controlRoomCount = Toolbox.Instance.GetPsuedoRandomNumberContained((int)preset.controlRoomRange.x, (int)preset.controlRoomRange.y, seed, out seed);

            //Find indexes with control room variants...
            List<int> floorIndexesWithPossibleControlRooms = new List<int>();

            int floorCursor = 0;

            if(controlRoomCount > 0)
            {
                for (int u = 0; u < preset.floorLayouts.Count; u++)
                {
                    BuildingPreset.InteriorFloorSetting setting = preset.floorLayouts[u];

                    for (int i = 0; i < setting.floorsWithThisSetting; i++)
                    {
                        if (setting.controlRoomVariants.Count > 0)
                        {
                            Game.Log("CityGen: Added control room possibility on floor " + floorCursor + " " + preset.name);
                            floorIndexesWithPossibleControlRooms.Add(floorCursor);
                        }

                        floorCursor++;
                    }
                }

                floorCursor = -1;

                for (int u = 0; u < preset.basementLayouts.Count; u++)
                {
                    BuildingPreset.InteriorFloorSetting setting = preset.basementLayouts[u];

                    for (int i = 0; i < setting.floorsWithThisSetting; i++)
                    {
                        if (setting.controlRoomVariants.Count > 0)
                        {
                            Game.Log("Added control room possibility on floor " + floorCursor + " " + preset.name);
                            floorIndexesWithPossibleControlRooms.Add(floorCursor);
                        }

                        floorCursor--;
                    }
                }
            }

            //Now pick which floors will feature control rooms...
            List<int> controlRoomFloors = new List<int>();

            for (int i = 0; i < controlRoomCount; i++)
            {
                int newIndex = Toolbox.Instance.GetPsuedoRandomNumberContained(0, floorIndexesWithPossibleControlRooms.Count, seed, out seed);
                controlRoomFloors.Add(floorIndexesWithPossibleControlRooms[newIndex]);
                Game.Log("CityGen: Chose control room floor " + floorIndexesWithPossibleControlRooms[newIndex]);
                floorIndexesWithPossibleControlRooms.RemoveAt(newIndex);
                if (floorIndexesWithPossibleControlRooms.Count <= 0) break;
            }

            //Reset floor cursor, as we need to loop again now...
            floorCursor = 0;

            //Loop settings
            for (int u = 0; u < preset.floorLayouts.Count; u++)
            {
                BuildingPreset.InteriorFloorSetting setting = preset.floorLayouts[u];

                for (int i = 0; i < setting.floorsWithThisSetting; i++)
                {
                    TextAsset floorTxt = null;

                    //Is this chosen for a control room?
                    if (controlRoomFloors.Contains(floorCursor))
                    {
                        Game.Log("CityGen: Picking control room for floor " + floorCursor);
                        floorTxt = setting.controlRoomVariants[Toolbox.Instance.GetPsuedoRandomNumberContained(0, setting.controlRoomVariants.Count, seed, out seed)] as TextAsset; //Choose random layout from list
                    }
                    else
                    {
                        if (setting.blueprints.Count <= 0) Game.LogError("No blueprints found on " + preset.name);
                        floorTxt = setting.blueprints[Toolbox.Instance.GetPsuedoRandomNumberContained(0, setting.blueprints.Count, seed, out seed)] as TextAsset; //Choose random layout from list
                    }

                    if (floorTxt == null)
                    {
                        Game.LogError("CityGen: Missing floor preset for " + name + " floor " + i);
                        return;
                    }

                    //Get the floor data for this using the text asset as a key
                    FloorSaveData savedData = null;
                    CityData.Instance.floorData.TryGetValue(floorTxt.name, out savedData);

                    if (savedData == null)
                    {
                        Game.LogError("CityGen: No floor data for " + floorTxt.name);
                        return;
                    }

                    //Create a new floor
                    GameObject newFloor = Instantiate(PrefabControls.Instance.floor, this.transform);
                    NewFloor editFloor = newFloor.GetComponent<NewFloor>();
                    int ceilHeight = savedData.defaultCeilingHeight;
                    if (setting.overrideCeilingHeight) ceilHeight = setting.newCeilingHeight;
                    editFloor.Setup(floorCursor, this, savedData.floorName, savedData.size, savedData.defaultFloorHeight, ceilHeight);

                    floorCursor++;

                    //Load Designed Layout
                    editFloor.maxDuctExtrusion = setting.airVentMaximumExtrusion; //Pass max air vent extrusion
                    editFloor.LoadDataToFloor(savedData);
                    editFloor.layoutIndex = u; //Set layout index for referencing preset

                    //Save stairs and elevators
                    foreach (KeyValuePair<Vector2Int, NewTile> pair in editFloor.tileMap)
                    {
                        if (pair.Value.isStairwell || pair.Value.isInvertedStairwell)
                        {
                            stairs.Add(pair.Value);
                        }
                    }
                }
            }

            floorCursor = -1;

            //Do the same for basements
            for (int u = 0; u < preset.basementLayouts.Count; u++)
            {
                BuildingPreset.InteriorFloorSetting setting = preset.basementLayouts[u];

                for (int i = 0; i < setting.floorsWithThisSetting; i++)
                {
                    TextAsset floorTxt = null;

                    //Is this chosen for a control room?
                    if (controlRoomFloors.Contains(floorCursor))
                    {
                        Game.Log("Picking control room for floor " + floorCursor);
                        floorTxt = setting.controlRoomVariants[Toolbox.Instance.GetPsuedoRandomNumberContained(0, setting.controlRoomVariants.Count, seed, out seed)] as TextAsset; //Choose random layout from list
                    }
                    else
                    {
                        if (setting.blueprints.Count <= 0) Game.LogError("No blueprints found on " + preset.name);
                        floorTxt = setting.blueprints[Toolbox.Instance.GetPsuedoRandomNumberContained(0, setting.blueprints.Count, seed, out seed)] as TextAsset; //Choose random layout from list
                    }

                    if (floorTxt == null)
                    {
                        Game.LogError("CityGen: Missing floor preset for " + name + " floor " + i);
                        return;
                    }

                    //Get the floor data for this using the text asset as a key
                    FloorSaveData savedData = null;
                    CityData.Instance.floorData.TryGetValue(floorTxt.name, out savedData);

                    if (savedData == null)
                    {
                        Game.LogError("CityGen: No floor data for " + floorTxt.name);
                        return;
                    }

                    //Create a new floor
                    GameObject newFloor = Instantiate(PrefabControls.Instance.floor, this.transform);
                    NewFloor editFloor = newFloor.GetComponent<NewFloor>();
                    int ceilHeight = savedData.defaultCeilingHeight;
                    if (setting.overrideCeilingHeight) ceilHeight = setting.newCeilingHeight;
                    editFloor.Setup(floorCursor, this, savedData.floorName, savedData.size, savedData.defaultFloorHeight, ceilHeight);

                    floorCursor--;

                    //Load Designed Layout
                    editFloor.maxDuctExtrusion = setting.airVentMaximumExtrusion; //Pass max air vent extrusion
                    editFloor.LoadDataToFloor(savedData);
                    editFloor.layoutIndex = u; //Set layout index for referencing preset

                    //Save stairs and elevators
                    foreach (KeyValuePair<Vector2Int, NewTile> pair in editFloor.tileMap)
                    {
                        if (pair.Value.isStairwell || pair.Value.isInvertedStairwell)
                        {
                            stairs.Add(pair.Value);
                        }
                    }
                }
            }

            //Set stair/elevator floor settings
            foreach (NewTile stair in stairs)
            {
                //Is there a tile above this?
                NewTile above = stairs.Find(item => item.floorCoord.x == stair.floorCoord.x && item.floorCoord.y == stair.floorCoord.y && item.floor.floor == stair.floor.floor + 1);

                //If no above, set to ceiling only
                if (above == null)
                {
                    //Remove stairs model
                    stair.SetAsTop(true);
                }

                //Is there a tile below this?
                NewTile below = stairs.Find(item => item.floorCoord.x == stair.floorCoord.x && item.floorCoord.y == stair.floorCoord.y && item.floor.floor == stair.floor.floor - 1);

                //If no below, set to floor only
                if (below == null)
                {
                    //Remove stairs model
                    stair.SetAsBottom(true);
                }
            }
        }
    }

    //Add a building entrance
    public void AddBuildingEntrance(NewWall wallTile, bool isMain = false)
    {
        if (wallTile == null) return;

        if(isMain)
        {
            StreetController getStr = wallTile.node.gameLocation.thisAsStreet;
            if (getStr == null) getStr = wallTile.otherWall.node.gameLocation.thisAsStreet;

            mainEntrance = wallTile;

            if(mainEntrance != null)
            {
                street = mainEntrance.otherWall.node.gameLocation.thisAsStreet;

                //if(evidenceEntry != null && evidenceEntry.dataSource != null)
                //{
                //    //Set street reference in datasources...
                //    //if (!evidenceEntry.dataSource.ContainsKey("street"))
                //    //{
                //    //    evidenceEntry.dataSource.Add("street", new EvidenceLinkData(street.name, link: EvidenceLinkData.OnLinkExecute.spawnWindow, ev: street.evidenceEntry));
                //    //}
                //    //else
                //    //{
                //    //    evidenceEntry.dataSource["street"] = new EvidenceLinkData(street.name, link: EvidenceLinkData.OnLinkExecute.spawnWindow, ev: street.evidenceEntry);
                //    //}

                //    //foreach (NewFloor fl in floors)
                //    //{
                //    //    foreach (NewAddress ad in fl.addresses)
                //    //    {
                //    //        if (!ad.evidenceEntry.dataSource.ContainsKey("street"))
                //    //        {
                //    //            ad.evidenceEntry.dataSource.Add("street", new EvidenceLinkData(street.name, link: EvidenceLinkData.OnLinkExecute.spawnWindow, ev: street.evidenceEntry));
                //    //        }
                //    //        else
                //    //        {
                //    //            ad.evidenceEntry.dataSource["street"] = new EvidenceLinkData(street.name, link: EvidenceLinkData.OnLinkExecute.spawnWindow, ev: street.evidenceEntry);
                //    //        }
                //    //    }
                //    //}
                //}
            }
        }
        else
        {
            if(!additionalEntrances.Contains(wallTile))
            {
                additionalEntrances.Add(wallTile);
            }
        }

        if(wallTile.node.floor != null)
        {
            if (!wallTile.node.floor.buildingEntrances.Contains(wallTile))
            {
                wallTile.node.floor.buildingEntrances.Add(wallTile);
            }
        }
        else
        {
            Game.Log("CityGen: " + wallTile.id + " does not feature reference to a floor. This shouldn't be happening: " + wallTile.position + " room: " + wallTile.node.room.name + " other: " + wallTile.otherWall.node.room.name);
        }
    }

    //Rotate vector tile coordinate local to the floor to suit building's direction
    public Vector2Int FaceLocalTileVector(Vector2Int r)
    {
        for (int l = 0; l < rotations; l++)
        {
            //The toolbox rotate vector will use 0,0 as a pivot, but we want to rotate these coordinates by using the centre as a pivot...
            //Minus from the coordinates so 0,0 is the centre
            Vector2 rotateVector = new Vector2(r.x - ((CityControls.Instance.tileMultiplier - 1) * 0.5f), r.y - ((CityControls.Instance.tileMultiplier - 1) * 0.5f));

            rotateVector = Toolbox.Instance.RotateVector2ACW(rotateVector, 90); //Positive values will rotate anti-clockwise assuming 0,0 is bottom left

            //Add again to find the final result
            r = new Vector2Int(Mathf.RoundToInt(rotateVector.x + ((CityControls.Instance.tileMultiplier - 1) * 0.5f)), Mathf.RoundToInt(rotateVector.y + ((CityControls.Instance.tileMultiplier - 1) * 0.5f)));

            //Game.Log(l + " rotation: " + tileLocation);
        }

        return r;
    }

    //Rotate vector node coordinate local to the floor to suit building's direction
    public Vector2Int FaceLocalNodeVector(Vector2Int r)
    {
        for (int l = 0; l < rotations; l++)
        {
            //The toolbox rotate vector will use 0,0 as a pivot, but we want to rotate these coordinates by using the centre as a pivot...
            //Minus from the coordinates so 0,0 is the centre
            Vector2 rotateVector = new Vector2(r.x - ((CityControls.Instance.tileMultiplier * CityControls.Instance.nodeMultiplier - 1) * 0.5f), r.y - ((CityControls.Instance.tileMultiplier * CityControls.Instance.nodeMultiplier - 1) * 0.5f));

            rotateVector = Toolbox.Instance.RotateVector2ACW(rotateVector, 90); //Positive values will rotate anti-clockwise assuming 0,0 is bottom left

            //Add again to find the final result
            r = new Vector2Int(Mathf.RoundToInt(rotateVector.x + ((CityControls.Instance.tileMultiplier * CityControls.Instance.nodeMultiplier - 1) * 0.5f)), Mathf.RoundToInt(rotateVector.y + ((CityControls.Instance.tileMultiplier * CityControls.Instance.nodeMultiplier - 1) * 0.5f)));

            //Game.Log(l + " rotation: " + tileLocation);
        }

        return r;
    }

    //Rotate wall offset local to the node to suit building's direction
    public Vector2 FaceWallOffsetVector(Vector2 r)
    {
        for (int l = 0; l < rotations; l++)
        {
            r = Toolbox.Instance.RotateVector2ACW(r, 90); //Positive values will rotate anti-clockwise assuming 0,0 is bottom left

            //Game.Log(l + " rotation: " + tileLocation);
        }

        return r;
    }

    //Do the opposite of above: Find the original vector offset by apply building's rotation
    public Vector2 GetOriginalWallOffset(Vector2 r)
    {
        for (int l = 0; l < rotations; l++)
        {
            r = Toolbox.Instance.RotateVector2ACW(r, -90); //Positive values will rotate anti-clockwise assuming 0,0 is bottom left

            //Game.Log(l + " rotation: " + tileLocation);
        }

        return r;
    }

    //Convert from a local vector to a global pathmap vector
    public Vector3 LocalToGlobalPathmap(Vector3 r)
    {
        //The building's pathmap coord is in the centre, while the local coordinates range from 0, 0
        Vector2 zeroVector = new Vector2(globalTileCoords.x - ((CityControls.Instance.tileMultiplier - 1) * 0.5f), globalTileCoords.y - ((CityControls.Instance.tileMultiplier - 1) * 0.5f));

        return new Vector3(Mathf.RoundToInt(zeroVector.x + r.x), Mathf.RoundToInt(zeroVector.y + r.y), r.z);
    }

    //Calculate where to face the building
    public void CalculateFacing()
    {
        //Set facing and road
        //Use the road closest to the centre of the map

        //Find centre of city tile
        Vector2 centre = new Vector2(Mathf.FloorToInt(PathFinder.Instance.tileCitySize.x * 0.5f), Mathf.FloorToInt(PathFinder.Instance.tileCitySize.y * 0.5f));

        //List of possible road tiles to face
        float highestFootfall = -1f;
        Vector2Int faceThis = new Vector2Int(globalTileCoords.x, globalTileCoords.y + 1);

        foreach (Vector2Int v in CityData.Instance.offsetArrayX4)
        {
            Vector3Int search = new Vector3Int(Mathf.RoundToInt(globalTileCoords.x + (v.x * Mathf.Floor(CityControls.Instance.tileMultiplier * 0.5f))), Mathf.RoundToInt(globalTileCoords.y + (v.y * Mathf.Floor(CityControls.Instance.tileMultiplier * 0.5f))), 0);

            if (PathFinder.Instance.tileMap.ContainsKey(search))
            {
                NewTile thisTile = PathFinder.Instance.tileMap[search];

                if (thisTile.streetController != null)
                {
                    if(thisTile.streetController.normalizedFootfall > highestFootfall)
                    {
                        faceThis = new Vector2Int(search.x, search.y);
                        highestFootfall = thisTile.streetController.normalizedFootfall;
                    }
                }
            }
        }

        //Default is face south
        Direction face = Direction.South;

        //Map corners
        if (preset.boundaryCorner)
        {
            if(cityTile.cityCoord.x == 0)
            {
                if(cityTile.cityCoord.y == 0)
                {
                    face = Direction.West;
                }
                else
                {
                    face = Direction.North;
                }
            }
            else
            {
                if (cityTile.cityCoord.y == 0)
                {
                    face = Direction.South;
                }
                else
                {
                    face = Direction.East;
                }
            }
        }
        else
        {
            if (faceThis.x < globalTileCoords.x) face = Direction.East;
            if (faceThis.x > globalTileCoords.x) face = Direction.West;

            if (faceThis.y < globalTileCoords.y)
            {
                face = Direction.North;
            }
        }

        SetFacing(face);
    }

    //Set facing (set rotation)
    //The default is facing south
    public void SetFacing(Direction face)
    {
        //Game.Log(face);
        facing = face;
        Vector3 newFrontCentre = new Vector3(globalTileCoords.x + (0 * Mathf.Floor(CityControls.Instance.tileMultiplier * 0.5f)), globalTileCoords.y + (-1 * Mathf.Floor(CityControls.Instance.tileMultiplier * 0.5f)));

        Vector3 euler = new Vector3(0f, 0f, 0f);

        if (face == Direction.North)
        {
            rotations = 0;
            euler.y = 180f;
            newFrontCentre = new Vector3(globalTileCoords.x + (0 * Mathf.Floor(CityControls.Instance.tileMultiplier * 0.5f)), globalTileCoords.y + (1 * Mathf.Floor(CityControls.Instance.tileMultiplier * 0.5f)));
        }
        else if (face == Direction.West)
        {
            rotations = 1;
            euler.y = 90f;
            newFrontCentre = new Vector3(globalTileCoords.x + (-1 * Mathf.Floor(CityControls.Instance.tileMultiplier * 0.5f)), globalTileCoords.y + (0 * Mathf.Floor(CityControls.Instance.tileMultiplier * 0.5f)));
        }
        else if (face == Direction.South)
        {
            rotations = 2;
            euler.y = 0f;
            newFrontCentre = new Vector3(globalTileCoords.x + (0 * Mathf.Floor(CityControls.Instance.tileMultiplier * 0.5f)), globalTileCoords.y + (-1 * Mathf.Floor(CityControls.Instance.tileMultiplier * 0.5f)));
        }
        else if (face == Direction.East)
        {
            rotations = 3;
            euler.y = 270f;
            newFrontCentre = new Vector3(globalTileCoords.x + (1 * Mathf.Floor(CityControls.Instance.tileMultiplier * 0.5f)), globalTileCoords.y + (0 * Mathf.Floor(CityControls.Instance.tileMultiplier * 0.5f)));
        }

        //Rotate building mode
        if(buildingModelBase != null) buildingModelBase.transform.eulerAngles = euler;
    }

    //Set inaccessible
    public void SetInaccessible()
    {
        isInaccessible = true;
    }

    //Create evidence file (run after all buildings are made)
    public override void SetupEvidence()
    {
        base.SetupEvidence();

        //Create evidence: Building directory (done before citizen's 'lives at' detail, so it's added there)
        //buildingDirectory = EvidenceCreator.Instance.CreateEvidence("BuildingDirectory", this, evidenceEntry) as Evidence;
    }

    //Add new lobby
    public void AddLobby(NewAddress newLob)
    {
        if(!lobbies.Contains(newLob))
        {
            lobbies.Add(newLob);
        }
    }

    //Display building mode
    public void SetDisplayBuildingModel(bool vis, bool coll, List<string> hideModelChildOverride = null)
    {
        if (SessionData.Instance.isFloorEdit) return; //Don't do this in floor edit

        //Reset selectively hidden model parts
        ResetSelectivelyHidden();

        if(displayBuildingModel != vis)
        {
            if(buildingModelBase != null) buildingModelBase.SetActive(vis);
            displayBuildingModel = vis;
        }

        foreach(Collider c in colliders)
        {
            if(c != null) c.enabled = coll;
        }

        if(vis)
        {
            SelectivelyHideModels(hideModelChildOverride);
        }
    }

    public void SelectivelyHideModels(List<string> hideModelChildOverride)
    {
        if (hideModelChildOverride != null)
        {
            foreach (string i in hideModelChildOverride)
            {
                GameObject found = buildingModelsActual.Find(item => item.name == i);

                if (found != null)
                {
                    //Game.Log("Set building model " + name + " hide child override: " + found.name);
                    found.SetActive(false);
                    selectivelyHidden.Add(found);
                }
            }
        }
    }

    public void ResetSelectivelyHidden()
    {
        //Reset selectively hidden model parts
        foreach (GameObject g in selectivelyHidden)
        {
            g.SetActive(true);
        }

        selectivelyHidden.Clear();
    }

    //Spawn street cables
    public void SpawnStreetCables()
    {
        int layerMask = Toolbox.Instance.CreateLayerMask(Toolbox.LayerMaskMode.onlyCast, 27);

        //Search for cable linking points
        foreach(BuildingPreset.CableLinkPoint link in preset.cableLinkPoints)
        {
            //Get height of this as ratio
            float heightRatio = Mathf.Clamp01(link.localPos.y / preset.meshHeight);

            //Get chance from preset curve
            float spawnChance = preset.cableSpawnChanceOverHeight.Evaluate(heightRatio);

            //Possibly skip this...
            if (Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, buildingID + link.localPos.ToString()) > spawnChance) continue;

            //Angle
            Vector3 randomAngle = new Vector3(0, 0, Toolbox.Instance.GetPsuedoRandomNumber(-CityControls.Instance.maximumCableAngle, CityControls.Instance.maximumCableAngle, buildingID + link.localPos.ToString()));

            //Do raycast to check for other buildings...
            //Check Distance threshold
            Vector3 rayPos = buildingModelBase.transform.TransformPoint(link.localPos);
            Vector3 targetDir = buildingModelBase.transform.TransformDirection(Quaternion.Euler(link.localRot).eulerAngles + randomAngle).normalized;

            Ray ray = new Ray(rayPos, targetDir);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 35f, layerMask))
            {
                //The normal must have a difference of around 180
                float dot = Vector3.Dot(targetDir, hit.normal);
                if (dot > -0.85f) continue;

                NewBuilding hitBuilding = hit.transform.gameObject.GetComponentInParent<NewBuilding>(true);

                if (hitBuilding != null)
                {
                    if (hitBuilding == this)
                    {
                        continue;
                    }
                }

                //Find list of cables that will work at this distance...
                List<CityControls.StreetCable> cableShortlist = CityControls.Instance.cables.FindAll(item => hit.distance <= item.maximumWidth && link.localPos.y <= item.maximumHeight && link.localPos.y >= item.minimumHeight && (!item.onlyFromZoneType || item.zone == preset.displayedZone));
                if (cableShortlist.Count <= 0) continue;

                //Pick a cable...
                List<CityControls.StreetCable> cablePool = new List<CityControls.StreetCable>();

                foreach (CityControls.StreetCable cab in cableShortlist)
                {
                    int freq = cab.frequency;

                    if(cab.disitrctFrequencyModifier)
                    {
                        if(cab.districts.Contains(cityTile.district.preset))
                        {
                            freq += cab.frequencyModifier;
                        }
                    }

                    for (int l = 0; l < freq; l++)
                    {
                        cablePool.Add(cab);
                    }
                }

                //Spawn cable
                CityControls.StreetCable cableToSpawn = cablePool[Toolbox.Instance.GetPsuedoRandomNumber(0, cablePool.Count, hit.point.ToString())];
                GameObject newCable = Instantiate(cableToSpawn.prefab, buildingModelBase.transform);
                newCable.transform.position = Vector3.Lerp(rayPos, hit.point, 0.5f);
                spawnedCables.Add(newCable);
                MeshFilter mf = newCable.transform.GetChild(0).GetComponent<MeshFilter>();

                newCable.name = "dir: " + targetDir + " hit normal: " + hit.normal + " dot: " + dot;

                //Scale cable to fill gap
                float scale = (hit.distance - 0.55f) / mf.sharedMesh.bounds.size.z;
                newCable.transform.localScale = new Vector3(scale, scale, scale);

                //Set cable to the correct direction
                newCable.transform.LookAt(hit.point);

                //Set this object's parent to the street below...
                Vector3Int nodePos = CityData.Instance.RealPosToNodeInt(newCable.transform.position);
                NewNode foundGroundNode = null;

                if (PathFinder.Instance.nodeMap.TryGetValue(new Vector3Int(nodePos.x, nodePos.y, 0), out foundGroundNode))
                {
                    if (foundGroundNode.gameLocation.thisAsStreet != null)
                    {
                        if (foundGroundNode.gameLocation.rooms[0].streetObjectContainer == null)
                        {
                            foundGroundNode.gameLocation.rooms[0].streetObjectContainer = new GameObject();
                            foundGroundNode.gameLocation.rooms[0].streetObjectContainer.transform.SetParent(foundGroundNode.gameLocation.rooms[0].transform, false);
                            foundGroundNode.gameLocation.rooms[0].streetObjectContainer.transform.localPosition = Vector3.zero;
                            foundGroundNode.gameLocation.rooms[0].streetObjectContainer.transform.name = "StreetObjectContainer";
                        }

                        newCable.transform.SetParent(foundGroundNode.gameLocation.rooms[0].streetObjectContainer.transform, true);

                        //Alter area lighting
                        //Alter street area lights
                        if ((CityConstructor.Instance == null || CityConstructor.Instance.generateNew) && cableToSpawn.alterAreaLighting)
                        {
                            foreach (NewRoom.LightZoneData lz in foundGroundNode.gameLocation.rooms[0].lightZones)
                            {
                                if (cableToSpawn.possibleColours.Count > 0)
                                {
                                    Color newColour = cableToSpawn.possibleColours[Toolbox.Instance.GetPsuedoRandomNumberContained(0, cableToSpawn.possibleColours.Count, seed, out seed)];

                                    if (cableToSpawn.lightOperation == DistrictPreset.AffectStreetAreaLights.lerp)
                                    {
                                        lz.areaLightColour = Color.Lerp(lz.areaLightColour, newColour, cableToSpawn.lightAmount);
                                    }
                                    else if (cableToSpawn.lightOperation == DistrictPreset.AffectStreetAreaLights.multiply)
                                    {
                                        lz.areaLightColour = lz.areaLightColour * (newColour * cableToSpawn.lightAmount);
                                    }
                                    else if (cableToSpawn.lightOperation == DistrictPreset.AffectStreetAreaLights.add)
                                    {
                                        lz.areaLightColour = lz.areaLightColour + (newColour * cableToSpawn.lightAmount);
                                    }

                                    //Set the colour
                                    if (lz.spawnedAreaLight != null) lz.spawnedAreaLight.color = lz.areaLightColour;
                                }

                                lz.areaLightBrightness += cableToSpawn.brightnessModifier;
                                lz.debug.Add("Area light street cable modifier: " + cableToSpawn.brightnessModifier);

                                //Set the brightness
                                if (lz.spawnedAreaLight != null) lz.aAdditional.intensity = lz.areaLightBrightness;
                            }
                        }
                    }
                }
            }
        }
    }

    //Spawn big neon signs
    public void SpawnNeonSideSigns()
    {
        //Pick new signs
        if(SessionData.Instance.isFloorEdit || CityConstructor.Instance.generateNew)
        {
            int numberOfSigns = Toolbox.Instance.GetPsuedoRandomNumberContained((int)preset.signsPerBuildingRange.x, (int)preset.signsPerBuildingRange.y, seed, out seed);
            List<BuildingPreset.CableLinkPoint> signPoints = new List<BuildingPreset.CableLinkPoint>(preset.sideSignPoints);
            List<GameObject> signs = new List<GameObject>(preset.possibleNeonSigns);

            for (int i = 0; i < numberOfSigns; i++)
            {
                if (signPoints.Count > 0 && signs.Count > 0)
                {
                    BuildingPreset.CableLinkPoint chosenPoint = signPoints[Toolbox.Instance.GetPsuedoRandomNumberContained(0, signPoints.Count, seed, out seed)];
                    GameObject sign = signs[Toolbox.Instance.GetPsuedoRandomNumberContained(0, signs.Count, seed, out seed)];

                    sideSigns.Add(new SideSign { anchorPointIndex = preset.sideSignPoints.IndexOf(chosenPoint), signPrefabIndex = preset.possibleNeonSigns.IndexOf(sign)});

                    //Remove from pool
                    signPoints.Remove(chosenPoint);
                    signs.Remove(sign);
                }
            }
        }

        //Spawn signs
        foreach(SideSign sideSign in sideSigns)
        {
            GameObject newSign = Instantiate(preset.possibleNeonSigns[sideSign.signPrefabIndex], buildingModelBase.transform);
            newSign.transform.localPosition = preset.sideSignPoints[sideSign.anchorPointIndex].localPos;
            newSign.transform.localEulerAngles = preset.sideSignPoints[sideSign.anchorPointIndex].localRot;
        }
    }

    //Generate air duct system
    public void GenerateAirDucts()
    {
        //Pool available space in order to generate a map we can use for connections
        //As the regular system uses a Y-based node space of 1 per floor, we have to multiply the Z value by 3 in order to have enough space to calculate routes between floors
        validVentSpace.Clear();

        foreach (KeyValuePair<int, NewFloor> pair in floors)
        {
            foreach (NewAddress add in pair.Value.addresses)
            {
                foreach (NewRoom room in add.rooms)
                {
                    int upperNodesAddedThisRoom = 0;

                    //Rank nodes by wall count- the ones with the most walls first
                    List<NewNode> rankedNodes = new List<NewNode>(room.nodes);
                    rankedNodes.Sort((p1, p2) => p2.walls.Count.CompareTo(p1.walls.Count)); //Using P2 first gives highest first

                    foreach (NewNode node in rankedNodes)
                    {
                        if (node.tile.isStairwell || node.tile.isInvertedStairwell || node.tile.isObstacle) continue; //Skip stairs, elevators, obstacles

                        //Null rooms have access along all Z space
                        if (room.isNullRoom)
                        {
                            //Outside areas are also null rooms, limit these depending on floor setting maximum extrusion
                            if (room.IsOutside())
                            {
                                bool pass = false;

                                //Check if this is valid based on distance from building edge
                                foreach (Vector3Int v3 in CityData.Instance.offsetArrayX6)
                                {
                                    //Check maximum amount in this direction
                                    for (int i = 0; i < pair.Value.maxDuctExtrusion; i++)
                                    {
                                        Vector3Int checkingNodeCoord = node.nodeCoord + (v3 * (i + 1));
                                        NewNode foundNode = null;

                                        if (PathFinder.Instance.nodeMap.TryGetValue(checkingNodeCoord, out foundNode))
                                        {
                                            if (!foundNode.room.IsOutside() && foundNode.gameLocation.building == this)
                                            {
                                                Vector3Int ductCoord = new Vector3Int(node.nodeCoord.x, node.nodeCoord.y, node.nodeCoord.z * 3 + 0);
                                                validVentSpace.Add(ductCoord, node);

                                                ductCoord = new Vector3Int(node.nodeCoord.x, node.nodeCoord.y, node.nodeCoord.z * 3 + 1);
                                                validVentSpace.Add(ductCoord, node);

                                                ductCoord = new Vector3Int(node.nodeCoord.x, node.nodeCoord.y, node.nodeCoord.z * 3 + 2);
                                                validVentSpace.Add(ductCoord, node);

                                                pass = true;
                                                break;
                                            }
                                        }
                                    }

                                    if (pass) break;
                                }
                            }
                            //Interior null spaces get free reign.
                            else
                            {
                                Vector3Int ductCoord = new Vector3Int(node.nodeCoord.x, node.nodeCoord.y, node.nodeCoord.z * 3 + 0);
                                validVentSpace.Add(ductCoord, node);

                                ductCoord = new Vector3Int(node.nodeCoord.x, node.nodeCoord.y, node.nodeCoord.z * 3 + 1);
                                validVentSpace.Add(ductCoord, node);

                                ductCoord = new Vector3Int(node.nodeCoord.x, node.nodeCoord.y, node.nodeCoord.z * 3 + 2);
                                validVentSpace.Add(ductCoord, node);
                            }
                        }
                        else
                        {
                            //Must have ceiling for this
                            if (node.floor.defaultCeilingHeight <= 48 && (node.floorType == NewNode.FloorTileType.CeilingOnly || node.floorType == NewNode.FloorTileType.floorAndCeiling))
                            {
                                //Add space above ceiling
                                Vector3Int ductCoord = new Vector3Int(node.nodeCoord.x, node.nodeCoord.y, node.nodeCoord.z * 3 + 2);
                                validVentSpace.Add(ductCoord, node);
                            }

                            //Must have ceiling for this
                            if (room.preset.allowUpperWallLevelDucts && (!room.preset.onlyAllowUpperIfFloorLevelIsZero || room.floor.defaultFloorHeight  <= 1) && (node.floorType == NewNode.FloorTileType.CeilingOnly || node.floorType == NewNode.FloorTileType.floorAndCeiling))
                            {
                                //There needs to be at least
                                if (upperNodesAddedThisRoom < room.preset.limitUpperLevelDucts)
                                {
                                    Vector3Int ductCoord = new Vector3Int(node.nodeCoord.x, node.nodeCoord.y, node.nodeCoord.z * 3 + 1);
                                    validVentSpace.Add(ductCoord, node);
                                    upperNodesAddedThisRoom++;
                                }
                            }

                            if (room.preset.allowLowerWallLevelDucts)
                            {
                                Vector3Int ductCoord = new Vector3Int(node.nodeCoord.x, node.nodeCoord.y, node.nodeCoord.z * 3 + 0);
                                validVentSpace.Add(ductCoord, node);
                            }
                        }
                    }
                }
            }
        }

        foreach(KeyValuePair<int, NewFloor> pair in floors)
        {
            pair.Value.GenerateAirDucts();
        }

        //Gather all air vents...
        Dictionary<AirDuctGroup.AirVent, Vector3Int> allVents = new Dictionary<AirDuctGroup.AirVent, Vector3Int>();
        List<AirDuctGroup.AirVent> checkAllVents = new List<AirDuctGroup.AirVent>(); //Check against these @ end for validity

        foreach (KeyValuePair<int, NewFloor> pair in floors)
        {
            foreach (NewAddress add in pair.Value.addresses)
            {
                foreach (NewRoom room in add.rooms)
                {
                    foreach(AirDuctGroup.AirVent vent in room.airVents)
                    {
                        Vector3Int ductCoord = Vector3Int.zero;

                        if(vent.ventType == NewAddress.AirVent.ceiling)
                        {
                            ductCoord = new Vector3Int(vent.node.nodeCoord.x, vent.node.nodeCoord.y, vent.node.nodeCoord.z * 3 + 2); //Ceiling
                            if (!validVentSpace.ContainsKey(ductCoord)) validVentSpace.Add(ductCoord, vent.node); //Add as valid space
                        }
                        else if(vent.ventType == NewAddress.AirVent.wallUpper)
                        {
                            ductCoord = new Vector3Int(vent.node.nodeCoord.x, vent.node.nodeCoord.y, vent.node.nodeCoord.z * 3 + 1); //Upper
                            if (!validVentSpace.ContainsKey(ductCoord)) validVentSpace.Add(ductCoord, vent.node); //Add as valid space
                        }
                        else if (vent.ventType == NewAddress.AirVent.wallLower)
                        {
                            ductCoord = new Vector3Int(vent.node.nodeCoord.x, vent.node.nodeCoord.y, vent.node.nodeCoord.z * 3 + 0); //Lower
                            if (!validVentSpace.ContainsKey(ductCoord)) validVentSpace.Add(ductCoord, vent.node); //Add as valid space
                        }

                        allVents.Add(vent, ductCoord);
                        checkAllVents.Add(vent);
                    }
                }
            }
        }

        //Game.Log("Air vents in building: " + allVents.Count);

        //We now have a list of compatible space above the ceiling on which to spawn air vents, and the points we need to connect.
        //Connect all vents to their nearest one...
        Dictionary<Vector3Int, DuctPlacementData> placedDucts = new Dictionary<Vector3Int, DuctPlacementData>(); //Used for checking if ducts have been placed here already- if so then give a weighting discount
        
        while (allVents.Count > 0)
        {
            //Pick a random vent...
            List<AirDuctGroup.AirVent> keys = allVents.Keys.ToList();
            AirDuctGroup.AirVent current = keys[Toolbox.Instance.GetPsuedoRandomNumberContained(0, keys.Count, seed, out seed)];
            Vector3Int currentCoord = allVents[current];

            List<AirDuctGroup.AirVent> attempted = new List<AirDuctGroup.AirVent>(); //List of attempted and failed

            //Attempt to connect to the nearest vent
            bool connected = false;
            int safety = 99;

            while(!connected && allVents.Count > 0 && safety > 0)
            {
                //Pick the nearest vent
                AirDuctGroup.AirVent nearest = null;
                float nearestDistance = Mathf.Infinity;

                foreach (KeyValuePair<AirDuctGroup.AirVent, Vector3Int> pair in allVents)
                {
                    if (pair.Key == current) continue;
                    if (attempted.Contains(pair.Key)) continue; //Ignore attempted

                    float dist = Vector3Int.Distance(pair.Value, currentCoord);

                    if (dist < nearestDistance)
                    {
                        nearest = pair.Key;
                        nearestDistance = dist;
                    }
                }

                if (nearest == null)
                {
                    break; //Break if no more vents
                }

                //Attempt to connect
                List<Vector3Int> connectionRoute = GetVentRoute(currentCoord, allVents[nearest], ref placedDucts);

                //Record fail
                if(connectionRoute == null || connectionRoute.Count <= 0)
                {
                    attempted.Add(nearest);
                }
                //Connection success
                else
                {
                    for (int i = 0; i < connectionRoute.Count; i++)
                    {
                        Vector3Int v3 = connectionRoute[i];

                        //Get previous and next, use them to avoid extra connections
                        Vector3Int previous = v3;
                        Vector3Int next = v3;

                        if (i > 0) previous = connectionRoute[i - 1] - v3;
                        if (i < connectionRoute.Count - 1) next = connectionRoute[i + 1] - v3;

                        if (!placedDucts.ContainsKey(v3))
                        {
                            placedDucts.Add(v3, new DuctPlacementData { previous = previous, next = next, originVent = current, destinationVent = nearest });
                        }
                    }

                    connected = true;

                    break;
                }

                safety--;
            }

            //Remove from pool
            allVents.Remove(current);
        }

        //Cycle through all ducts and assign all joining
        AirDuctGroup currentGroup = null;

        List<Vector3Int> wholeOpenSet = new List<Vector3Int>(placedDucts.Keys.ToList());

        List<Vector3Int> openSet = new List<Vector3Int>();
        List<Vector3Int> closedSet = new List<Vector3Int>();
        int safety2 = 99;

        while (wholeOpenSet.Count > 0 && safety2 > 0)
        {
            //Add a new coord to the open set for scanning...
            if(openSet.Count <= 0)
            {
                openSet.Clear();
                openSet.Add(wholeOpenSet[0]);

                //Create a new group & object container for this duct system...
                GameObject newDuctGrp = new GameObject();
                newDuctGrp.transform.SetParent(this.transform, false);
                newDuctGrp.transform.localPosition = Vector3.zero;
                currentGroup = newDuctGrp.AddComponent<AirDuctGroup>();
                currentGroup.SetupNew(this);

                int searchSafety = 9999;

                while(openSet.Count > 0 && searchSafety > 0)
                {
                    Vector3Int current = openSet[0];
                    DuctPlacementData placement = placedDucts[current];

                    int nodeZ = Mathf.FloorToInt(current.z / 3f); //The node coord (floor of /3)
                    int level = Mathf.RoundToInt(current.z - nodeZ * 3f); //Get the level
                    Vector3Int nodeCoord = new Vector3Int(current.x, current.y, nodeZ);

                    NewNode foundNode = null;
                    PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out foundNode);

                    //Is this an exterior group?
                    if(foundNode.room.IsOutside())
                    {
                        currentGroup.isExterior = true;
                    }

                    //Add to current group
                    currentGroup.AddAirDuctSection(level, current, placement.previous, placement.next, foundNode);
                    currentGroup.AddAirVent(placement.originVent);
                    currentGroup.AddAirVent(placement.destinationVent);

                    //Search for connecting...
                    foreach (Vector3Int v3 in CityData.Instance.offsetArrayX6)
                    {
                        Vector3Int search = current + v3;

                        //Exists as a duct
                        if(placedDucts.ContainsKey(search))
                        {
                            //Not written yet...
                            if (!openSet.Contains(search) && !closedSet.Contains(search))
                            {
                                //Exterior group of this must match
                                int searchNodeZ = Mathf.FloorToInt(search.z / 3f); //The node coord (floor of /3)
                                int searchLevel = Mathf.RoundToInt(search.z - nodeZ * 3f); //Get the level
                                Vector3Int searchNodeCoord = new Vector3Int(search.x, search.y, searchNodeZ);

                                NewNode searchFoundNode = null;

                                if (PathFinder.Instance.nodeMap.TryGetValue(searchNodeCoord, out searchFoundNode))
                                {
                                    //Exterior bool must match...
                                    if (searchFoundNode.room.IsOutside() == currentGroup.isExterior)
                                    {
                                        openSet.Add(search);
                                    }
                                    //The exterior groups extend 1 node into the building to cover any inaccurate building model geometry...
                                    else if (currentGroup.isExterior)
                                    {
                                        foreach (Vector3Int outsideSearch in CityData.Instance.offsetArrayX6)
                                        {
                                            Vector3Int searchCoord = searchNodeCoord + outsideSearch;
                                            NewNode foundOutside = null;

                                            if (PathFinder.Instance.nodeMap.TryGetValue(searchCoord, out foundOutside))
                                            {
                                                if (foundOutside.room.IsOutside())
                                                {
                                                    openSet.Add(search);

                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //Remove from open set
                    openSet.RemoveAt(0);
                    closedSet.Add(current);
                    wholeOpenSet.Remove(current);
                    //placedDucts.Remove(current);
                    searchSafety--;

                    if (searchSafety <= 0)
                    {
                        Game.LogError("Error while assigning air duct groups: Safety level reached!");
                    }
                }
            }

            safety2--;

            if (safety2 <= 0)
            {
                Game.LogError("Error while assigning air duct groups: Safety level reached!");
            }
        }

        //Now calculate adjoining groups
        foreach (AirDuctGroup ductGroup in airDucts)
        {
            foreach(AirDuctGroup.AirDuctSection section in ductGroup.airDucts)
            {
                foreach(Vector3Int v3 in CityData.Instance.offsetArrayX6)
                {
                    Vector3Int searchCoord = section.duct + v3;

                    //Check this adjacent exists...
                    if(placedDucts.ContainsKey(searchCoord))
                    {
                        //Search for this coordinate in other groups
                        foreach (AirDuctGroup otherGroup in airDucts)
                        {
                            if (otherGroup == ductGroup) continue; //We're looking for other groups...
                            
                            if(otherGroup.airDucts.Exists(item => item.duct == searchCoord))
                            {
                                ductGroup.AddAdjoiningDuctGroup(otherGroup);
                                otherGroup.AddAdjoiningDuctGroup(ductGroup);
                            }
                        }
                    }
                }
            }
        }

        //Now we have all the node groups for this building, connect them up...
        foreach (AirDuctGroup ductGroup in airDucts)
        {
            ductGroup.LoadDucts();
        }

        //Clear vent path map at the end, as we shouldn't need this any more
        validVentSpace.Clear();
        validVentSpace = null;

        //Remove attempted vents
        List<AirDuctGroup.AirVent> failedVents = checkAllVents.FindAll(item => item.group == null);

        if(failedVents.Count > 0)
        {
            Game.Log("CityGen: Detected " + failedVents.Count + " failed vents for building " + preset.name +", removing them now...");

            foreach (AirDuctGroup.AirVent failedVent in failedVents)
            {
                failedVent.Remove();
            }
        }
    }

    //Add a stairwell system
    public Elevator AddStairwellSystem(NewTile newTile, StairwellPreset stairPreset)
    {
        //Game.Log("Add stairwell system: " + newTile.name);

        //Search for an existing system at these tile coordinates...
        for (int i = 0; i <= 50; i++)
        {
            //Search below
            Vector3Int search = new Vector3Int(newTile.globalTileCoord.x, newTile.globalTileCoord.y, (int)newTile.globalTileCoord.z - i);
            NewTile foundTile = null;

            if (PathFinder.Instance.tileMap.TryGetValue(search, out foundTile))
            {
                if (stairwells.ContainsKey(foundTile))
                {
                    //We've found the bottom
                    stairwells[foundTile].AddFloor(newTile); //Add to this elevator
                    return stairwells[foundTile];
                }
            }

            //Search above
            search = new Vector3Int(newTile.globalTileCoord.x, newTile.globalTileCoord.y, (int)newTile.globalTileCoord.z + i);
            foundTile = null;

            if (PathFinder.Instance.tileMap.TryGetValue(search, out foundTile))
            {
                if (stairwells.ContainsKey(foundTile))
                {
                    //We've found the bottom
                    stairwells[foundTile].AddFloor(newTile); //Add to this elevator
                    return stairwells[foundTile];
                }
            }
        }

        //Game.Log("CityGen: No existing stairwell system at " + name + " on tile coord " + newTile.globalTileCoord + ", creating new one...");

        //If this is reached, we need to create a new stairwell system
        if(stairPreset == null) stairPreset = InteriorControls.Instance.defaultStairwell;
        return new Elevator(stairPreset, this, newTile);
    }

    //Default comparer (land value)
    public int CompareTo(NewBuilding otherObject)
    {
        return this.cityTile.landValue.CompareTo(otherObject.cityTile.landValue);
    }

    //Compare by distance
    public static Comparison<NewBuilding> DistanceComparison = delegate (NewBuilding object1, NewBuilding object2)
    {
        return object1.distance.CompareTo(object2.distance);
    };

    //Create save data
    public CitySaveData.BuildingCitySave GenerateSaveData()
    {
        CitySaveData.BuildingCitySave output = new CitySaveData.BuildingCitySave();
        output.buildingID = buildingID;
        output.name = name;
        output.preset = preset.name;;
        output.facing = facing;
        output.isInaccessible = isInaccessible;
        output.sideSigns = sideSigns;

        output.designStyle = designStyle.name; //Design style chosen for the address
        output.wood = wood; //Colour of the wood for this address
        output.floorMaterial = floorMaterial.name;
        output.floorMatKey = floorMatKey;
        output.ceilingMaterial = ceilingMaterial.name;
        output.ceilingMatKey = ceilingMatKey;
        output.defaultWallMaterial = defaultWallMaterial.name;
        output.defaultWallKey = defaultWallKey;
        output.colourScheme = colourScheme.name;

        if(extWallMaterial != null) output.extWallMaterial = extWallMaterial.name;

        if (ceilingMaterialOverride != null) output.ceilingMatOverride = ceilingMaterialOverride.name;
        if (floorMaterialOverride != null) output.floorMatOverride = floorMaterialOverride.name;
        if (defaultWallMaterialOverride != null) output.wallMatOverride = defaultWallMaterialOverride.name;

        if (basementCeilingMaterialOverride != null) output.ceilingMatOverrideB = basementCeilingMaterialOverride.name;
        if (basementFloorMaterialOverride != null) output.floorMatOverrideB = basementFloorMaterialOverride.name;
        if (basementDefaultWallMaterialOverride != null) output.wallMatOverrideB = basementDefaultWallMaterialOverride.name;

        //Record air duct groups
        foreach (AirDuctGroup ductGroup in airDucts)
        {
            output.airDucts.Add(ductGroup.GenerateSaveData());
        }

        //Get floor data
        foreach(KeyValuePair<int, NewFloor> pair in floors)
        {
            output.floors.Add(pair.Value.GenerateSaveData());
        }

        return output;
    }

    //Generate name
    public void UpdateName()
    {
        if(preset.overrideNaming && preset.possibleNames.Count > 0)
        {
            name = Strings.Get("names.building.bespoke", preset.possibleNames[Toolbox.Instance.GetPsuedoRandomNumberContained(0, preset.possibleNames.Count, seed, out seed)]).Trim();
        }
        else
        {
            string eth = cityTile.district.EthnictiyBasedOnDominance().ToString();
            name = NameGenerator.Instance.GenerateName(null, 0f, "names." + eth + ".sur", 1f, "names.building.suffix." + cityTile.landValue.ToString(), 1f, seed).Trim();

            //Make sure name is unique
            int safetyAttempts = 99;

            while (CityData.Instance.buildingDirectory.Exists(item => item != this && item.name == name) && safetyAttempts > 0)
            {
                Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed);
                name = NameGenerator.Instance.GenerateName(null, 0f, "names." + eth + ".sur", 1f, "names.building.suffix." + cityTile.landValue.ToString(), 1f, seed).Trim();
                safetyAttempts--;
            }
        }

        this.gameObject.name = name;

        nameOverride = null;

        //Set residence names
        foreach(KeyValuePair<int, NewFloor> pair in floors)
        {
            foreach(NewAddress add in pair.Value.addresses)
            {
                //Set residence names...
                if (add.residence != null)
                {
                    add.SetName(add.residence.GetResidenceString() + " " + name);
                }
                //Set company names...
                else if(add.company != null)
                {
                    add.company.UpdateName();
                }
                //Other names...
                else
                {
                    add.SetName(Strings.Get("names.rooms", add.preset.name, Strings.Casing.firstLetterCaptial));
                }

                if(add.addressPreset != null && add.addressPreset.overrideBuildingName && nameOverride == null)
                {
                    nameOverride = add;
                }

                foreach(NewRoom r in add.rooms)
                {
                    r.SetRoomName();
                }

                //Setup neon signs
                add.SetupNeonSigns();
            }
        }

        //Name override...
        if(nameOverride != null)
        {
            name = nameOverride.name;
            this.gameObject.name = name;

            foreach (KeyValuePair<int, NewFloor> pair in floors)
            {
                foreach (NewAddress add in pair.Value.addresses)
                {
                    if (add == nameOverride) continue;

                    //Set residence names...
                    if (add.residence != null)
                    {
                        add.SetName(add.residence.GetResidenceString() + " " + name);
                    }
                    //Set company names...
                    else if (add.company != null)
                    {
                        add.company.UpdateName();
                    }
                    //Other cases
                    else
                    {
                        add.SetName(Strings.Get("names.rooms", add.preset.name, Strings.Casing.firstLetterCaptial));
                    }

                    foreach (NewRoom r in add.rooms)
                    {
                        r.SetRoomName();
                    }
                }
            }
        }

        //Set lobby names
        foreach (NewAddress lobby in lobbies)
        {
            string floorString = string.Empty;

            if (lobby.floor.floor < 0)
            {
                floorString = Strings.Get("names.rooms", "basement", Strings.Casing.firstLetterCaptial) + " ";
            }

            if (lobby.floor.floor == 0)
            {
                floorString = Strings.Get("evidence.generic", "ground floor", Strings.Casing.firstLetterCaptial) + " " + Strings.Get("evidence.generic", "lobby");
            }
            else
            {
                floorString += Toolbox.Instance.GetNumbericalStringReference(Mathf.Abs(lobby.floor.floor)) + " " + Strings.Get("evidence.generic", "floor landing");
            }

            lobby.SetName(name + " " + floorString);
        }
    }

    public override void CreateEvidence()
    {
        //Create citizen entry
        if (evidenceEntry == null)
        {
            evidenceEntry = EvidenceCreator.Instance.CreateEvidence("Building", "Building" + buildingID, this) as EvidenceBuilding;
            residentRoster = EvidenceCreator.Instance.CreateEvidence("ResidentRoster", "ResidentRoster" + buildingID, this) as EvidenceMultiPage;
        }
    }

    //Activate alarm
    public void SetAlarm(bool newVal, Human target, NewFloor forFloor)
    {
        if(alarmActive != newVal)
        {
            alarmActive = newVal;

            if (alarmActive)
            {
                if (!GameplayController.Instance.activeAlarmsBuildings.Contains(this))
                {
                    GameplayController.Instance.activeAlarmsBuildings.Add(this);
                }

                foreach (NewAddress ad in lobbies)
                {
                    //Respond to alarm
                    for (int i = 0; i < ad.currentOccupants.Count; i++)
                    {
                        Actor a = ad.currentOccupants[i];

                        if (a == null) continue;
                        if (a.isPlayer) continue;

                        if (a.isAsleep || a.isDead || a.isStunned || a.ai == null) continue;

                        a.AddNerve(CitizenControls.Instance.nerveAlarm);
                        a.OnInvestigate(a, 1);
                    }
                }

                //Trigger sounds
                TriggerAlarmPASounds();
            }
            else
            {
                alarmTimer = 0f;

                GameplayController.Instance.activeAlarmsBuildings.Remove(this);

                foreach (KeyValuePair<int, NewFloor> pair in floors)
                {
                    pair.Value.SetAlarmLockdown(false);
                }

                alarmTargets.Clear();

                StopAlarmPASounds();
            }

            if (Player.Instance.currentBuilding == this) StatusController.Instance.ForceStatusCheck();

            for (int u = 0; u < alarms.Count; u++)
            {
                Interactable i = alarms[u];

                if (i.node.gameLocation.thisAsAddress != null && i.node.gameLocation.thisAsAddress.addressPreset != null && i.node.gameLocation.thisAsAddress.addressPreset.useOwnSecuritySystem) continue;
                i.SetCustomState1(alarmActive, null);
            }

            for (int u = 0; u < sentryGuns.Count; u++)
            {
                Interactable i = sentryGuns[u];

                if (i.node.gameLocation.thisAsAddress != null && i.node.gameLocation.thisAsAddress.addressPreset != null && i.node.gameLocation.thisAsAddress.addressPreset.useOwnSecuritySystem) continue;
                //i.SetSwitchState(alarmActive, null);
                i.SetCustomState1(alarmActive, null);
            }

            for (int u = 0; u < otherSecurity.Count; u++)
            {
                Interactable i = otherSecurity[u];

                if (i.node.gameLocation.thisAsAddress != null && i.node.gameLocation.thisAsAddress.addressPreset != null && i.node.gameLocation.thisAsAddress.addressPreset.useOwnSecuritySystem) continue;
                //i.SetSwitchState(alarmActive, null);
                i.SetCustomState1(alarmActive, null);
            }

            //Forcing this update also updates the trespassing rules for the player
            if (Player.Instance.currentBuilding == this)
            {
                Player.Instance.OnRoomChange();
            }
        }

        if(alarmActive)
        {
            if (target != null)
            {
                if (target.isPlayer)
                {
                    SessionData.Instance.TutorialTrigger("alarms");
                    StatusController.Instance.SetWantedInBuilding(this, GameplayControls.Instance.buildingWantedTime);
                }

                if (!alarmTargets.Contains(target))
                {
                    alarmTargets.Add(target);
                }
            }

            //Reset alarm timer
            alarmTimer = GetAlarmTime();

            //Place the floor under lockdown
            if (forFloor != null)
            {
                forFloor.SetAlarmLockdown(true);
            }
        }
    }

    public float GetAlarmTime()
    {
        return Mathf.Lerp(GameplayControls.Instance.buildingAlarmTime.x, GameplayControls.Instance.buildingAlarmTime.y, 0);
    }

    public bool IsAlarmSystemTarget(Human human)
    {
        if (human == null) return false;

        if(targetMode == AlarmTargetMode.notPlayer)
        {
            if (!human.isPlayer)
            {
                return true;
            }
        }
        else if(targetMode == AlarmTargetMode.everybody)
        {
            return true;
        }
        else if(targetMode == AlarmTargetMode.illegalActivities)
        {
            if (alarmTargets.Contains(human) || (human.isPlayer && SessionData.Instance.gameTime < wantedInBuilding))
            {
                return true;
            }
        }
        else if(targetMode == AlarmTargetMode.nobody)
        {
            return false;
        }
        else if(targetMode == AlarmTargetMode.nonResidents)
        {
            if (human.home != null && human.home.building == this)
            {
                return false;
            }
            else return true;
        }

        return false;
    }

    //Add security camera
    public void AddSecurityCamera(Interactable newInteractable)
    {
        securityCameras.Add(newInteractable);
    }

    public void AddSentryGun(Interactable newInteractable)
    {
        sentryGuns.Add(newInteractable);
    }

    public void AddOtherSecurity(Interactable newInteractable)
    {
        otherSecurity.Add(newInteractable);
    }

    //Set exterior wall info
    public void SetExteriorWallMaterialDefault(MaterialGroupPreset newMat)
    {
        extWallMaterial = newMat;
    }

    ////Set captured player image
    //public void SetCapturedPlayerImage(bool val)
    //{
    //    if(val != capturedPlayerImage)
    //    {
    //        capturedPlayerImage = val;
    //        StatusController.Instance.ForceStatusCheck();
    //    }
    //}

    //Simple pathing system for connecting vents
    public List<Vector3Int> GetVentRoute(Vector3Int origin, Vector3Int destination, ref Dictionary<Vector3Int, NewBuilding.DuctPlacementData> placedDucts)
    {
        //If this code is reached, we need to calculate the route...
        //Uses A* pathfinding... I don't *think* efficiency can be improved by another style...

        // The set of nodes already evaluated.
        Dictionary<Vector3Int, bool> closedSet = new Dictionary<Vector3Int, bool>();

        // The set of currently discovered nodes still to be evaluated.
        // Initially, only the start node is known.
        Dictionary<Vector3Int, bool> openSet = new Dictionary<Vector3Int, bool>();

        // For each node, which node it can most efficiently be reached from.
        // If a node can be reached from many nodes, cameFrom will eventually contain the
        // most efficient previous step.
        Dictionary<Vector3Int, Vector3Int> cameFrom = new Dictionary<Vector3Int, Vector3Int>();

        // For each node, the cost of getting from the start node to that node.
        Dictionary<Vector3Int, float> fromStartToThis = new Dictionary<Vector3Int, float>();

        // For each node, the total cost of getting from the start node to the goal
        // by passing by that node. That value is partly known, partly heuristic.
        Dictionary<Vector3Int, float> fromStartViaThis = new Dictionary<Vector3Int, float>();

        //Add access from origin
        openSet.Add(origin, true);
        fromStartToThis.Add(origin, 0f);
        fromStartViaThis.Add(origin, Vector3.Distance(origin, destination));

        int calculationLoops = 0;
        bool pathfindSuccessful = false;

        //while openSet is not empty
        while (openSet.Count > 0)
        {
            //current := the node in openSet having the lowest fScore[] value
            Vector3Int current = Vector3Int.zero;
            float lowestFScore = Mathf.Infinity;

            foreach (KeyValuePair<Vector3Int, bool> pair in openSet)
            {
                //If doesn't exist, assume infinity (ie not lower than lowest score)
                if (fromStartViaThis.ContainsKey(pair.Key))
                {
                    if (fromStartViaThis[pair.Key] < lowestFScore)
                    {
                        lowestFScore = fromStartViaThis[pair.Key];
                        current = pair.Key;
                    }
                }
            }

            //if current == goal then we're done!
            if (current == destination)
            {
                //Construct the path
                List<Vector3Int> totalPath = new List<Vector3Int>();
                Vector3Int previous = Vector3Int.zero;

                //We're constructing the path backwards, so start with this as the first entry...
                totalPath.Add(current);

                while (cameFrom.ContainsKey(current))
                {
                    previous = current;
                    current = cameFrom[current];

                    //negative vector2 is used as a null, so don't add this.
                    if (current != null)
                    {
                        //The next node to add must be different
                        if (current != previous)
                        {
                            totalPath.Add(current);
                        }
                        else continue;
                    }
                    else
                    {
                        Game.Log("Pathfinder: Room pathfinder path construction error: current is null, invalid path");
                        break;
                    }
                }

                totalPath.Reverse();

                //Return
                pathfindSuccessful = true;
                return totalPath;
            }

            openSet.Remove(current);
            closedSet.Add(current, true);

            //Get came from so we can check for corners (and weight them)
            Vector3Int cameFromThis = current;
            cameFrom.TryGetValue(current, out cameFromThis);
            Vector3Int directionVector = current - cameFromThis;

            //Get node
            int nodeZ = Mathf.FloorToInt(current.z / 3f); //The node coord (floor of /3)
            Vector3Int currentNodeCoord = new Vector3Int(current.x, current.y, nodeZ);
            NewNode currentNode = PathFinder.Instance.nodeMap[currentNodeCoord];

            //Scan for entrances in this room...
            foreach (Vector3Int v3 in CityData.Instance.offsetArrayX6)
            {
                Vector3Int accessNeighbor = current + v3;
                if (!validVentSpace.ContainsKey(accessNeighbor)) continue;

                //if neighbor in closedSet
                // Ignore the neighbor which is already evaluated.
                bool b = false;
                if (closedSet.TryGetValue(accessNeighbor, out b)) continue;

                //This must be a wall or existing vent piece...
                int nNodeZ = Mathf.FloorToInt(accessNeighbor.z / 3f); //The node coord (floor of /3)
                Vector3Int neighborNodeCoord = new Vector3Int(accessNeighbor.x, accessNeighbor.y, nNodeZ);

                //Does a wall exist between? If not then ignore this connection
                NewNode neighborNode = PathFinder.Instance.nodeMap[neighborNodeCoord];
                bool skip = false;

                //Relative level 0 - 2
                int level = Mathf.RoundToInt(accessNeighbor.z - nNodeZ);

                //Scan walls for existing ducts...
                foreach (NewWall wall in currentNode.walls)
                {
                    //This node the the wall between the two nodes
                    if (wall.otherWall.node == neighborNode)
                    {
                        if (wall.node.room.airVents.Exists(item => item.wall == wall))
                        {
                            skip = true;
                            break;
                        }
                        else if (wall.otherWall.node.room.airVents.Exists(item => item.wall == wall.otherWall))
                        {
                            skip = true;
                            break;
                        }
                        //Route is blocked by existing vents
                        else if(wall.preset.sectionClass == DoorPairPreset.WallSectionClass.ventTop || wall.otherWall.preset.sectionClass == DoorPairPreset.WallSectionClass.ventTop)
                        {
                            skip = true;
                            break;
                        }
                        //Route is blocked by windows
                        else if(wall.preset.sectionClass == DoorPairPreset.WallSectionClass.window || wall.preset.sectionClass == DoorPairPreset.WallSectionClass.windowLarge)
                        {
                            skip = true;
                            break;
                        }
                        //Route is blocked by entrances (apart from dividers if below level 3)
                        else if(wall.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance)
                        {
                            //If this isn't a divider or the route in on top level with a divider
                            if(!wall.preset.divider || (wall.preset.divider && level >= 2))
                            {
                                skip = true;
                                break;
                            }
                        }
                    }
                }

                if (skip) continue;

                //To continue this direction, the offset must be the inverse of the dir vector...
                float weightModifier = 2f;

                if (directionVector == v3)
                {
                    weightModifier = 0f; //Continuing this direction, so don't add any additional weight
                }

                //Weighting discount if this is already a placed vent...
                if (placedDucts.ContainsKey(accessNeighbor))
                {
                    weightModifier = 0f;
                }

                //The path weight from start to the neighbor
                //We're adding the pre-calculated weight from the node of the entrance we just walked into, to the possible next entrance to (exterior/to -> interior/from)
                float tentativeGScore = fromStartToThis[current] + 1 + weightModifier;

                //Discover a new node- add to open set
                if (!openSet.TryGetValue(accessNeighbor, out b))
                {
                    //Game.Log("Pathfinder: Add " + neighbor.pathCoord + " to open set, count: " + openSet.Count);
                    openSet.Add(accessNeighbor, true);
                }

                //Path hasn't been added (assume infinity) if tentative score is lower then this is automatically the best until now.
                if (!fromStartToThis.ContainsKey(accessNeighbor) || tentativeGScore < fromStartToThis[accessNeighbor])
                {
                    if (!cameFrom.ContainsKey(accessNeighbor)) cameFrom.Add(accessNeighbor, current);
                    else cameFrom[accessNeighbor] = current;

                    if (!fromStartToThis.ContainsKey(accessNeighbor)) fromStartToThis.Add(accessNeighbor, tentativeGScore);
                    else fromStartToThis[accessNeighbor] = tentativeGScore;

                    float viaThis = fromStartToThis[accessNeighbor] + Vector3.Distance(accessNeighbor, destination);
                    if (!fromStartViaThis.ContainsKey(accessNeighbor)) fromStartViaThis.Add(accessNeighbor, viaThis);
                    else fromStartViaThis[accessNeighbor] = viaThis;
                }
            }

            calculationLoops++;

            if (calculationLoops > 9999)
            {
                //Game.LogError("PathFinder: Air Duct pathfinding failsafe reached, check for cascades!");
                return null;
            }
        }

        if (!pathfindSuccessful)
        {
            //Game.LogError("PathFinder: Air Duct Internal pathfinding unsucessful- there is an unreachable coordinate: " + destination);
        }
        return null;
    }

    //Calculate directional culling trees that we can apply to rooms with windows within this building
    public void CalculateDirectionalCullingTrees()
    {
        //Start at top floor and work downwards...
        for (int f = floors.Count - 1; f > 0; f--)
        {
            if (!floors.ContainsKey(f)) continue;

            NewFloor fl = floors[f];

            if (directionalCullingTrees.Count >= 4) break;

            foreach(NewAddress ad in fl.addresses)
            {
                foreach(NewRoom r in ad.rooms)
                {
                    foreach(NewNode.NodeAccess entrance in r.entrances)
                    {
                        //This room must face outside...
                        if (!entrance.toNode.room.IsOutside()) continue;

                        //Which way does this entrance face?
                        Vector3 diff = (entrance.toNode.nodeCoord - entrance.fromNode.nodeCoord);
                        Vector2 facing = diff.normalized;

                        if (directionalCullingTrees.ContainsKey(facing)) continue;
                        else
                        {
                            Dictionary<NewRoom, List<NewRoom.CullTreeEntry>> cullingTree = new Dictionary<NewRoom, List<NewRoom.CullTreeEntry>>();

                            //Execute culling tree generation for this direction (ie, what we can see from this side of the building)
                            foreach (NewRoom otherRoom in CityData.Instance.roomDirectory)
                            {
                                //Don't run for this room; we've already added itself
                                if (otherRoom == this) continue;
                                if (otherRoom.nodes.Count <= 0) continue;

                                //Useful flags for optizations...
                                bool otherIsStreet = false;
                                if (otherRoom.gameLocation.thisAsStreet != null) otherIsStreet = true;

                                //Don't run this for null rooms (that aren't the street)
                                if (otherRoom.preset != null && otherRoom.preset.roomType == CityControls.Instance.nullDefaultRoom && !otherIsStreet) continue;
                                if (otherRoom.IsOutside()) continue;

                                //Street optimizations...
                                if(!otherIsStreet)
                                {
                                    //Don't run if in different buildings, unless force outside
                                    if (this != otherRoom.building && (otherRoom.preset != null && otherRoom.preset.forceOutside != RoomConfiguration.OutsideSetting.forceOutside))
                                    {
                                        //One must be above ground floor in order to skip...
                                        if (r.floor.floor > 0 || otherRoom.floor.floor > 0)
                                        {
                                            continue;
                                        }
                                    }
                                }

                                if (cullingTree.ContainsKey(otherRoom))
                                {
                                    //Ignore already visible, but potentially add to this if there is a different combination of required open doors...
                                    //Skip if the existing room doesn't require any doors to be open...
                                    if (cullingTree[otherRoom][0].requiredOpenDoors == null || cullingTree[otherRoom][0].requiredOpenDoors.Count <= 0)
                                    {
                                        continue;
                                    }
                                }

                                bool breakRoomLoop = false;

                                //If this isn't a street and above the ground floor, we can ignore other streets unless through a window...
                                if (otherIsStreet && r.floor != null && r.floor.floor > 0 && (r.preset != null && r.preset.forceOutside != RoomConfiguration.OutsideSetting.forceOutside))
                                {
                                    if (entrance.accessType != NewNode.NodeAccess.AccessType.window)
                                    {
                                        continue;
                                    }
                                }

                                //Interior to interiors cannot be connected via window unless looking down on ground floor
                                if (!otherIsStreet)
                                {
                                    if (entrance.accessType == NewNode.NodeAccess.AccessType.window && otherRoom.floor.floor > 0)
                                    {
                                        if (otherRoom.preset != null && otherRoom.preset.forceOutside != RoomConfiguration.OutsideSetting.forceOutside)
                                        {
                                            continue;
                                        }
                                    }
                                }

                                //Room is within FoV...
                                float halfFOV = 91;

                                ////Is this within FoV
                                //Vector3 targetDir = otherRoom.worldPos - entrance.worldAccessPoint;
                                //float angleToOther = Vector3.Angle(targetDir, (entrance.toNode.position - entrance.fromNode.position).normalized); //Local angle
                                //if (angleToOther < -halfFOV || angleToOther > halfFOV) continue; //Out of FoV

                                //Loop entrances for other room...
                                foreach (NewNode.NodeAccess otherEntrance in otherRoom.entrances)
                                {
                                    if (otherEntrance == null) continue;

                                    //We can ignore if both sides of the entrance are the same room...
                                    if (otherEntrance.fromNode.room == otherEntrance.toNode.room)
                                    {
                                        continue;
                                    }

                                    ////If both are streets, we can ignore everything apart from street-to-street type entrances
                                    //if (thisIsStreet && otherIsStreet)
                                    //{
                                    //    if (otherEntrance.accessType != NewNode.NodeAccess.AccessType.streetToStreet)
                                    //    {
                                    //        continue;
                                    //    }
                                    //}

                                    //Entrances are within FoV...
                                    //checkEntrancePairCount++;
                                    //Check from the outside/to node to the other room's inside/from node

                                    //Do a FoV check...
                                    //Is this within FoV
                                    Vector3 targetDir = otherEntrance.fromNode.nodeCoord - entrance.toNode.nodeCoord;
                                    Vector3 dif = (entrance.toNode.nodeCoord - entrance.fromNode.nodeCoord);
                                    float angleToOther = Vector3.Angle(targetDir, dif.normalized); //Local angle

                                    if (angleToOther >= -halfFOV && angleToOther <= halfFOV) // 180° FOV
                                    {
                                        //FoV Check is passed...
                                        //Now do a data raycast and add all visible rooms on the way...
                                        List<DataRaycastController.NodeRaycastHit> pointList;

                                        //I don't think we need a seperate system adding the target as it should be included in the path list output..
                                        if (entrance.toNode == null || otherEntrance.fromNode == null) continue;
                                        DataRaycastController.Instance.EntranceRaycast(entrance, otherEntrance, out pointList);

                                        //Add all rooms on resulting path...
                                        List<NewRoom> revList = new List<NewRoom>();

                                        //Start at 1st as 0 will always be self
                                        for (int i = 1; i < pointList.Count; i++)
                                        {
                                            //Normal direction...
                                            DataRaycastController.NodeRaycastHit v3 = pointList[i];
                                            NewNode foundNode = null;

                                            if (PathFinder.Instance.nodeMap.TryGetValue(v3.coord, out foundNode))
                                            {
                                                if (foundNode.room != null)
                                                {
                                                    if (foundNode.room == this) continue;
                                                    bool add = false;

                                                    //CullingDebugController.CullDebugType newCullType = CullingDebugController.CullDebugType.succeededOvr;

                                                    //Add to this tree...
                                                    if (!cullingTree.ContainsKey(foundNode.room))
                                                    {
                                                        cullingTree.Add(foundNode.room, new List<NewRoom.CullTreeEntry>());
                                                        add = true;
                                                        //newCullType = CullingDebugController.CullDebugType.succeededNew;
                                                    }
                                                    else
                                                    {
                                                        //If the current path requires no open doors, replace all lists with *just* this one
                                                        if (v3.conditionalDoors == null || v3.conditionalDoors.Count <= 0)
                                                        {
                                                            if (cullingTree[foundNode.room].Count > 0) cullingTree[foundNode.room].Clear();
                                                            add = true;
                                                        }
                                                        //Otherwise, only add if the current entry in the list DOESN'T feature an entry with no conditional doors already...
                                                        else
                                                        {
                                                            add = true;

                                                            //Check contents of list are different...
                                                            for (int u = 0; u < cullingTree[foundNode.room].Count; u++)
                                                            {
                                                                NewRoom.CullTreeEntry ce = cullingTree[foundNode.room][u];

                                                                //There is an existing entry of no doors, forget about other door combinations...
                                                                if (ce.requiredOpenDoors == null || ce.requiredOpenDoors.Count <= 0)
                                                                {
                                                                    add = false;
                                                                    break;
                                                                }

                                                                //Otherwise check for a duplicate list of doors sing sequence equal
                                                                if (ce.requiredOpenDoors.SequenceEqual(v3.conditionalDoors))
                                                                {
                                                                    add = false;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }

                                                    if (add)
                                                    {
                                                        cullingTree[foundNode.room].Add(new NewRoom.CullTreeEntry(v3.conditionalDoors));

                                                        //Add any atrium top...
                                                        if (foundNode.room.atriumTop != null)
                                                        {
                                                            if (!cullingTree.ContainsKey(foundNode.room.atriumTop))
                                                            {
                                                                cullingTree.Add(foundNode.room.atriumTop, new List<NewRoom.CullTreeEntry>());
                                                            }

                                                            cullingTree[foundNode.room.atriumTop].Clear();
                                                            cullingTree[foundNode.room.atriumTop].Add(new NewRoom.CullTreeEntry(v3.conditionalDoors));
                                                        }

                                                        //If this addition is the original 'other' room...
                                                        //If this addition had no conditional doors, then we can break this loop
                                                        if (foundNode.room == otherRoom)
                                                        {
                                                            if (v3.conditionalDoors == null || v3.conditionalDoors.Count <= 0)
                                                            {
                                                                breakRoomLoop = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        pointList = null;
                                    }

                                    if (breakRoomLoop)
                                    {
                                        break;
                                    }
                                }

                                if (breakRoomLoop)
                                {
                                    break;
                                }
                            }

                            //Save to directional tree reference
                            directionalCullingTrees.Add(facing, cullingTree);

                            Game.Log("CityGen: Generated directional culling tree " + facing + " for building " + name + " using room on floor " + r.floor.floor + " with " + cullingTree.Count + " entries...");
                        }

                        if (directionalCullingTrees.Count >= 4) break;
                    }

                    if (directionalCullingTrees.Count >= 4) break;
                }

                if (directionalCullingTrees.Count >= 4) break;
            }
        }
    }

    [Button]
    public void CountResidences()
    {
        //Count all residences in building...
        int resCount = 0;

        foreach (KeyValuePair<int, NewFloor> pair in floors)
        {
            resCount += pair.Value.addresses.FindAll(item => item.residence != null).Count;
        }

        Game.Log("Found " + resCount + " residences");
    }

    public void TriggerAlarmPASounds()
    {
        StopAlarmPASounds();

        List<AudioController.FMODParam> para = GetAlarmPAParams();

        List<Vector3> playedAtPositions = new List<Vector3>();

        //Play at every main entrance
        if (mainEntrance != null)
        {
            Vector3 playPos = mainEntrance.otherWall.node.position + new Vector3(0, 2.5f, 0);

            Game.Log("Audio: Playing alarm pa at: " + playPos);
            AudioController.LoopingSoundInfo loop = AudioController.Instance.PlayWorldLoopingStatic(AudioControls.Instance.alarmPA, null, mainEntrance.otherWall.node, playPos, para);

            if (loop != null)
            {
                alarmPALoops.Add(loop);
                playedAtPositions.Add(playPos);
            }
        }

        foreach (NewWall w in additionalEntrances)
        {
            if (w == mainEntrance) continue;
            if (w.door == null) continue; //Skip no doors

            Vector3 playPos = w.otherWall.node.position + new Vector3(0, 2.5f, 0);

            //Don't play if too close to another source
            bool skip = false;

            foreach(Vector3 v3 in playedAtPositions)
            {
                if (Vector3.Distance(v3, playPos) < 3f)
                {
                    skip = true;
                    break;
                }
            }

            if (skip) return;

            Game.Log("Audio: Playing alarm pa at: " + playPos);
            AudioController.LoopingSoundInfo loop = AudioController.Instance.PlayWorldLoopingStatic(AudioControls.Instance.alarmPA, null, w.otherWall.node, playPos, para);

            if (loop != null)
            {
                alarmPALoops.Add(loop);
                playedAtPositions.Add(playPos);
            }
        }
    }

    public void UpdateAlarmPAWindowDistance(float val)
    {
        //Game.Log("Audio: Updating alarm PA window distance params for " + name + ": " + val);

        foreach(AudioController.LoopingSoundInfo loop in alarmPALoops)
        {
            if (loop == null) continue;

            if (loop.parameters != null)
            {
                AudioController.FMODParam p = loop.parameters.Find(item => item.name == "WindowDistance");
                p.value = val;
            }

            loop.audioEvent.setParameterByName("WindowDistance", val);
        }
    }

    public void UpdateAlarmPAExternalDoorDistance(float val)
    {
        //Game.Log("Audio: Updating alarm PA external door distance params for " + name + ": " + val);

        foreach (AudioController.LoopingSoundInfo loop in alarmPALoops)
        {
            if (loop == null) continue;

            if (loop.parameters != null)
            {
                AudioController.FMODParam p = loop.parameters.Find(item => item.name == "ExternalDoorDistance");
                p.value = val;
            }

            loop.audioEvent.setParameterByName("ExternalDoorDistance", val);
        }
    }

    public void UpdateAlarmPAIntExt(float val)
    {
        //Game.Log("Audio: Updating alarm PA int/ext params for " + name + ": " + val);

        foreach (AudioController.LoopingSoundInfo loop in alarmPALoops)
        {
            if (loop == null) continue;

            if (loop.parameters != null)
            {
                AudioController.FMODParam p = loop.parameters.Find(item => item.name == "Interior");
                p.value = val;
            }

            loop.audioEvent.setParameterByName("Interior", val);
        }
    }

    private List<AudioController.FMODParam> GetAlarmPAParams()
    {
        List<AudioController.FMODParam> ret = new List<AudioController.FMODParam>();

        AudioController.FMODParam intExt = new AudioController.FMODParam { name = "Interior"};
        if (Player.Instance.currentRoom != null && Player.Instance.currentRoom.IsOutside()) intExt.value = 0;
        else intExt.value = 1;
        ret.Add(intExt);

        ret.Add(new AudioController.FMODParam { name = "ExternalDoorDistance", value = AudioController.Instance.closestDoorDistanceNormalized });
        ret.Add(new AudioController.FMODParam { name = "WindowDistance", value = AudioController.Instance.closestWindowDistanceNormalized });

        return ret;
    }

    public void StopAlarmPASounds()
    {
        Game.Log("Audio: Stopped PA alarm sounds for " + name);

        for (int i = 0; i < alarmPALoops.Count; i++)
        {
            AudioController.Instance.StopSound(alarmPALoops[i], AudioController.StopType.triggerCue, "Stop building alarm");
        }

        alarmPALoops.Clear();
    }

    public void TriggerNewLostAndFound()
    {
        List<InteractablePreset> validItemChoices = CityControls.Instance.lostAndFoundItems.FindAll(item => !lostAndFound.Exists(item2 => item2.preset == item.presetName));
        List<Citizen> validOwners = CityData.Instance.citizenDirectory.FindAll(item => item.home != null && item.home.building == this && !item.isDead && !lostAndFound.Exists(item2 => item2.ownerID == item.humanID));

        if(validOwners.Count > 0)
        {
            Citizen chosenOwner = validOwners[Toolbox.Instance.Rand(0, validOwners.Count)];
            Game.Log("Gameplay: Creating a new lost & found mission with owner " + chosenOwner.GetCitizenName() + " for building " + name);

            //Find a valid spawning place for the job note...
            List<NewGameLocation.ObjectPlacement> validNotePlacements = new List<NewGameLocation.ObjectPlacement>();

            foreach (KeyValuePair<int, NewFloor> pair in floors)
            {
                foreach (NewAddress add in pair.Value.addresses)
                {
                    if (add.access == AddressPreset.AccessType.allPublic || add.access == AddressPreset.AccessType.buildingInhabitants)
                    {
                        if(add.addressPreset != null && add.addressPreset.canFeatureLostAndFound)
                        {
                            if(add.nodes.Count > 9)
                            {
                                NewGameLocation.ObjectPlacement notePlacement = add.GetBestSpawnLocation(CityControls.Instance.lostAndFoundNote, false, chosenOwner, chosenOwner, null, out _, forceSecuritySettings: true, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseNonOwned, ignoreLimits: true, forcedPriority: 1, forcedSecurity: 0);

                                if (notePlacement != null)
                                {
                                    validNotePlacements.Add(notePlacement);
                                }
                            }
                        }
                    }
                }
            }

            if(validNotePlacements.Count > 0)
            {
                //Find a valid spawning place...
                Interactable spawnedObject = null;

                while(spawnedObject == null && validItemChoices.Count > 0)
                {
                    InteractablePreset chosenItem = validItemChoices[Toolbox.Instance.Rand(0, validItemChoices.Count)];

                    if(chosenItem != null)
                    {
                        List<NewGameLocation.ObjectPlacement> validLocations = new List<NewGameLocation.ObjectPlacement>();

                        foreach(KeyValuePair<int, NewFloor> pair in floors)
                        {
                            if (pair.Key < -1) continue; //Don't place in waterlogged basements

                            foreach (NewAddress add in pair.Value.addresses)
                            {
                                if (add.access == AddressPreset.AccessType.allPublic || add.access == AddressPreset.AccessType.buildingInhabitants)
                                {
                                    NewGameLocation.ObjectPlacement placement = add.GetBestSpawnLocation(chosenItem, false, chosenOwner, chosenOwner, null, out _, forceSecuritySettings: true, forcedOwnership: InteractablePreset.OwnedPlacementRule.prioritiseNonOwned, ignoreLimits: true, forcedPriority: 2, forcedSecurity: 0);

                                    if (placement != null && !validNotePlacements.Contains(placement))
                                    {
                                        validLocations.Add(placement);
                                    }
                                }
                            }
                        }

                        Game.Log("Gameplay: ... Found " + validLocations.Count + " valid locations for spawning a " + chosenItem.presetName + " at " + name + "...");

                        //We've found at least one valid location...
                        if (validLocations.Count > 0)
                        {
                            //Spawn the item
                            NewGameLocation.ObjectPlacement pickedPos = validLocations[Toolbox.Instance.Rand(0, validLocations.Count)];

                            if (pickedPos.existing != null)
                            {
                                pickedPos.existing.SafeDelete(true);
                            }

                            //Pick a placement upon furniture...
                            if (pickedPos.subSpawn == null)
                            {
                                spawnedObject = InteractableCreator.Instance.CreateFurnitureSpawnedInteractableThreadSafe(chosenItem, pickedPos.furnParent.anchorNode.room, pickedPos.furnParent, pickedPos.location, chosenOwner, chosenOwner, null, null, null, null);
                            }
                            //Pick a placement on an existing object
                            else
                            {
                                //Furniture interactables must be created first...
                                if (!pickedPos.subSpawn.furnitureParent.createdInteractables)
                                {
                                    pickedPos.subSpawn.furnitureParent.CreateInteractables();
                                }

                                int ssIndex = Toolbox.Instance.Rand(0, pickedPos.subSpawn.ssp.Count);
                                InteractablePreset.SubSpawnSlot ss = pickedPos.subSpawn.ssp[ssIndex];

                                //Spawn in a small radius around this
                                Vector2 c = (UnityEngine.Random.insideUnitCircle * 0.2f);
                                Vector3 r = pickedPos.subSpawn.wPos + ss.localPos;
                                r += new Vector3(c.x, 0, c.y);

                                Vector3 e = pickedPos.subSpawn.wEuler + ss.localEuler;

                                spawnedObject = InteractableCreator.Instance.CreateWorldInteractable(chosenItem, chosenOwner, chosenOwner, null, r, e, null, null);

                                //Remove slot from interactable
                                pickedPos.subSpawn.ssp.RemoveAt(ssIndex);
                            }

                            //Break out of the loop if we've spawned an object now
                            if(spawnedObject != null)
                            {
                                break;
                            }
                            //Otherwise eliminate this as an option
                            else
                            {
                                validItemChoices.Remove(chosenItem);
                            }
                        }
                        //Otherwise eliminate this as an option
                        else
                        {
                            validItemChoices.Remove(chosenItem);
                        }
                    }
                }

                //We have a spawned item so can continue creating the job...
                if(spawnedObject != null)
                {
                    Game.Log("Gameplay: Successfully spawned " + spawnedObject.GetName() + " at " + spawnedObject.GetWorldPosition());

                    //Spawn a lost & found notice...
                    NewGameLocation.ObjectPlacement chosenNoteLocation = validNotePlacements[Toolbox.Instance.Rand(0, validNotePlacements.Count)];

                    int reward = Math.Clamp(Mathf.FloorToInt(((spawnedObject.val * 0.5f) / 10f) * 10f), 30, 300);

                    List<Interactable.Passed> passed = new List<Interactable.Passed>();
                    passed.Add(new Interactable.Passed(Interactable.PassedVarType.lostItemPreset, 0, spawnedObject.preset.presetName));
                    passed.Add(new Interactable.Passed(Interactable.PassedVarType.lostItemBuilding, buildingID));
                    passed.Add(new Interactable.Passed(Interactable.PassedVarType.lostItemReward, reward));

                    int floor = spawnedObject.node.floor.floor;

                    int floorVariance = Toolbox.Instance.Rand(3, 5);

                    int withinRange = Toolbox.Instance.Rand(0, floorVariance);
                    int upper = floor + withinRange;
                    int lower = floor + withinRange - 4;

                    int safety = 100;

                    while(!floors.ContainsKey(lower) && safety > 0)
                    {
                        upper++;
                        lower++;
                        safety--;
                    }

                    safety = 100;

                    while (!floors.ContainsKey(upper) && safety > 0)
                    {
                        upper--;
                        lower--;
                        safety--;
                    }

                    passed.Add(new Interactable.Passed(Interactable.PassedVarType.lostItemFloorX, lower));
                    passed.Add(new Interactable.Passed(Interactable.PassedVarType.lostItemFloorY, upper));

                    Interactable lostNote = InteractableCreator.Instance.CreateFurnitureSpawnedInteractableThreadSafe(CityControls.Instance.lostAndFoundNote, chosenNoteLocation.furnParent.anchorNode.room, chosenNoteLocation.furnParent, chosenNoteLocation.location, chosenOwner, chosenOwner, null, passed, null, null);

                    if(lostNote != null)
                    {
                        Game.Log("Gameplay: Successfully created a new lost & found mission: Post located at " + lostNote.GetWorldPosition() + " (" + lostNote.node.gameLocation.name + ")");

                        GameplayController.LostAndFound newLostAndFound = new GameplayController.LostAndFound();
                        newLostAndFound.preset = spawnedObject.preset.presetName;
                        newLostAndFound.ownerID = chosenOwner.humanID;
                        newLostAndFound.buildingID = buildingID;
                        newLostAndFound.spawnedItem = spawnedObject.id;
                        newLostAndFound.spawnedNote = lostNote.id;
                        newLostAndFound.rewardMoney = reward;
                        newLostAndFound.rewardSC = 20;

                        lostAndFound.Add(newLostAndFound);
                    }
                    else
                    {
                        Game.Log("Gameplay: Unable to spawn lost note within " + name);
                    }
                }
            }
            else
            {
                Game.Log("Gameplay: Cannot find anywhere to post the lost & found note within " + name);
            }
        }
    }

    public void CompleteLostAndFound(Citizen owner, InteractablePreset itemType, bool giveReward = true)
    {
        if (owner == null || itemType == null) return;

        List<GameplayController.LostAndFound> matches = lostAndFound.FindAll(item => item.ownerID == owner.humanID && item.preset == itemType.presetName && item.buildingID == buildingID);

        foreach(GameplayController.LostAndFound f in matches)
        {
            //Remove notes
            Interactable note = null;

            if(CityData.Instance.savableInteractableDictionary.TryGetValue(f.spawnedNote, out note))
            {
                note.MarkAsTrash(true);
                note.SafeDelete(false);
            }

            //Remove thing
            Interactable thing = null;

            if (CityData.Instance.savableInteractableDictionary.TryGetValue(f.spawnedItem, out thing))
            {
                thing.MarkAsTrash(true);
            }

            //Give reward
            if(giveReward)
            {
                StartCoroutine(PayLostAndFoundReward(f));
            }

            //Remove mission
            lostAndFound.Remove(f);
        }
    }

    IEnumerator PayLostAndFoundReward(GameplayController.LostAndFound f)
    {
        float timer = 0f;

        while(timer < 4f)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        GameplayController.Instance.AddMoney(f.rewardMoney, true, "Complete lost and found");
        GameplayController.Instance.AddSocialCredit(f.rewardSC, true, "Complete lost and found");
    }
}
