﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingEvidenceCreator : Creator
{
    //Chunks to load per frame
    public int loadChunk = 10;
    public int loadChunkCompile = 20;

    //Check agsint this for compiling on create
    public bool called = false;

    //Singleton pattern
    private static StartingEvidenceCreator _instance;
    public static StartingEvidenceCreator Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    //Create evidence files
    public override void StartLoading()
    {
        called = true;
        Game.Log("CityGen: Generating starting evidence...");
        StartCoroutine("GenChunk");
    }

    IEnumerator GenChunk()
    {
        List<Controller> evToCreate = new List<Controller>();

        foreach (StreetController rc in CityData.Instance.streetDirectory) evToCreate.Add(rc);
        foreach (NewBuilding rc in CityData.Instance.buildingDirectory) evToCreate.Add(rc);
        foreach (NewAddress rc in CityData.Instance.addressDirectory) evToCreate.Add(rc);
        foreach (Citizen rc in CityData.Instance.citizenDirectory) evToCreate.Add(rc);
        //foreach (CompanyController rc in CityData.Instance.companyDirectory) evToCreate.Add(rc);

        int evProgress = 0;

        while (evProgress < evToCreate.Count)
        {
            for (int i = 0; i < loadChunk; i++)
            {
                //Break loop if complete
                if (evProgress >= evToCreate.Count) break;

                evToCreate[evProgress].SetupEvidence(); //Create file

                evProgress++;
            }

            CityConstructor.Instance.loadingProgress = ((float)evProgress / (float)evToCreate.Count) * 0.5f;
            yield return null;
        }

        foreach (Company rc in CityData.Instance.companyDirectory)
        {
            rc.SetupEvidence();
        }

        //Update acquaintances and make sure they have the revelent knowledge
        foreach (Citizen cc in CityData.Instance.citizenDirectory)
        {
            foreach (Acquaintance aq in cc.acquaintances)
            {
                aq.OthersKnowledgeUpdate();
            }
        }

        CompileEvidence();
    }

    //Compile evidence files
    public void CompileEvidence()
    {
        StartCoroutine("Compile");
    }

    IEnumerator Compile()
    {
        int evProgress = 0;

        while (evProgress < CityConstructor.Instance.evidenceToCompile.Count)
        {
            for (int i = 0; i < loadChunkCompile; i++)
            {
                //Break loop if complete
                if (evProgress >= CityConstructor.Instance.evidenceToCompile.Count) break;

                CityConstructor.Instance.evidenceToCompile[evProgress].Compile(); //Compile evidence

                evProgress++;
            }

            CityConstructor.Instance.loadingProgress = 0.5f + (((float)evProgress / (float)CityConstructor.Instance.evidenceToCompile.Count) * 0.5f);
            yield return null;
        }

        //Complete!
        SetComplete();
    }
}
