﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "menu_data", menuName = "Database/Menu Preset")]

public class MenuPreset : SoCustomComparison
{
    [Header("Items Sold")]
    [ReorderableList]
    public List<InteractablePreset> itemsSold = new List<InteractablePreset>();

    [Tooltip("If true a receipt will always be created with these items...")]
    public bool createReceipt = true;
    public AudioEvent purchaseAudio;

    [Space(7)]
    public int syncDiskSlots = 0;
    public List<SyncDiskPreset.Manufacturer> fromManufacturers = new List<SyncDiskPreset.Manufacturer>();
    public List<SyncDiskPreset> syncDisks = new List<SyncDiskPreset>();
}
