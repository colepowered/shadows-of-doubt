﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using TMPro;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "murdererMO_data", menuName = "Database/Murderer MO")]

public class MurderMO : SoCustomComparison
{
	[Header("Notes")]
	[ResizableTextArea]
	public string notes;

	[Header("Compatibility")]
	public bool disabled = false;
    [Tooltip("Compatible with these killer types")]
    public List<MurderPreset> compatibleWith = new List<MurderPreset>();
	[Range(0, 2)]
	public int baseDifficulty = 0;

	[Header("Murderer Suitability")]
	public Vector2 pickRandomScoreRange = new Vector2(0f, 1f);
	[InfoBox("The max trait score should equal 30 if you want MOs to be balanced")]
	public List<MurderPreset.MurdererModifierRule> murdererTraitModifiers = new List<MurderPreset.MurdererModifierRule>();
	[InfoBox("The max trait score for the below should equal 20 if you want MOs to be balanced")]
	public List<JobModifier> murdererJobModifiers = new List<JobModifier>();
	public List<CompanyModifier> murdererCompanyModifiers = new List<CompanyModifier>();
	public bool useMurdererSocialClassRange = false;
	[EnableIf("useMurdererSocialClassRange")]
	public Vector2 murdererClassRange = new Vector2(0, 1);
	[EnableIf("useMurdererSocialClassRange")]
	public int murdererClassRangeBoost = 0;
	[Space(7)]
	public bool useHexaco = false;
	[ShowIf("useHexaco")]
	public HEXACO hexaco;

	[Header("Weapons Picking")]
	[InfoBox("The killer will pick one of these to kill ALL their victims...", EInfoBoxType.Normal)]
	public List<MurderWeaponsPool> weaponsPool = new List<MurderWeaponsPool>();

	[Space(7)]
	[Tooltip("Block weapons from being dropped at scene")]
	public bool blockDroppingWeapons = false;

	[Header("Crime Scene")]
	[Tooltip("The murder can happen anywhere")]
	public bool allowAnywhere = false;
	[DisableIf("allowAnywhere")]
	[Tooltip("The murder can happen at home")]
	public bool allowHome = true;
	[DisableIf("allowAnywhere")]
	[Tooltip("The murder can happen at work")]
	public bool allowWork = false;
	[DisableIf("allowAnywhere")]
	[Tooltip("The murder can happen in public")]
	public bool allowPublic = false;
	[DisableIf("allowAnywhere")]
	[Tooltip("The murder can happen in public")]
	public bool allowStreets = false;

	[Header("Victim Suitability")]
	[InfoBox("The below rule will give a big boost to the chances of this person being chosen.")]
	[Range(-20, 20)]
	public int acquaintedSuitabilityBoost = 0;
	[Range(-20, 20)]
	public int attractedToSuitabilityBoost = 0;
	[Range(-20, 20)]
	[Tooltip("The following is multiplied by the like value in acquaintance class.")]
	public int likeSuitabilityBoost = 0;
	[Range(-20, 20)]
	public int sameWorkplaceBoost = 0;
	[Range(-20, 20)]
	public int murdererIsTenantBoost = 0;

	[InfoBox("The killer will rank using these settings to their victims...", EInfoBoxType.Normal)]
	public Vector2 victimRandomScoreRange = new Vector2(0f, 1f);
	public List<MurderPreset.MurdererModifierRule> victimTraitModifiers = new List<MurderPreset.MurdererModifierRule>();
	public List<JobModifier> victimJobModifiers = new List<JobModifier>();
	public List<CompanyModifier> victimCompanyModifiers = new List<CompanyModifier>();
	public bool useVictimSocialClassRange = false;
	[EnableIf("useVictimSocialClassRange")]
	public Vector2 victimClassRange = new Vector2(0, 1);
	[EnableIf("useVictimSocialClassRange")]
	public int victimClassRangeBoost = 0;

	[Header("Monkier DDS Message List")]
	public string monkierDDSMessageList;

	[Header("Leads")]
	public List<MurderPreset.MurderLeadItem> MOleads = new List<MurderPreset.MurderLeadItem>();

	[Header("Calling Cards")]
	public List<Graffiti> graffiti = new List<Graffiti>();
    [InfoBox("The killer will pick one of these to leave at ALL crime scenes...", EInfoBoxType.Normal)]
    public List<CallingCardPick> callingCardPool = new List<CallingCardPick>();

    [System.Serializable]
    public class CallingCardPick
    {
        [Tooltip("The item itself")]
        public InteractablePreset item;
		public CallingCardOrigin origin = CallingCardOrigin.createAtScene;

        [Space(7)]
        public Vector2 randomScoreRange = new Vector2(0f, 0f);
        public List<MurderPreset.MurdererModifierRule> traitModifiers = new List<MurderPreset.MurdererModifierRule>();
    }

	public enum CallingCardOrigin { createAtScene, createOnGoToLocation };

	[System.Serializable]
	public class Graffiti
    {
		public InteractablePreset preset;
		public GraffitiPosition pos;

		public enum GraffitiPosition { victim, nearbyWall};

		[Space(7)]
		public ArtPreset artImage;
		[Space(7)]
		public string ddsMessageTextList;
		public Color color = Color.white;
		public float size;
	}

	[System.Serializable]
	public class JobModifier
    {
		public List<OccupationPreset> jobs = new List<OccupationPreset>();
		[Range(-20, 20)]
		public int jobBoost = 0;
	}

	[System.Serializable]
	public class CompanyModifier
	{
		public List<CompanyPreset> companies = new List<CompanyPreset>();
		public int mininumEmployees = 3;
		[Range(-20, 20)]
		public int companyBoost = 0;
		[Tooltip("Add even more for employee count over the minimum")]
		public int boostPerEmployeeOverMinimum = 1;
	}
}
