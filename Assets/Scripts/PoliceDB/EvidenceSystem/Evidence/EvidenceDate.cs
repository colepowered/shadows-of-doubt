﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class EvidenceDate : Evidence
{
    public string date;

    //Constructor
    public EvidenceDate(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        //Parse data from ID key...
        string[] keyParse = evID.Split('|');

        if (keyParse.Length > 1)
        {
            date = keyParse[1];
        }

        if (keyParse.Length > 3)
        {
            int from = -1;
            int.TryParse(keyParse[3], out from);

            CityData.Instance.GetHuman(from, out writer);
        }

        if (keyParse.Length > 4)
        {
            int from = -1;
            int.TryParse(keyParse[4], out from);

            CityData.Instance.GetHuman(from, out reciever);
        }

        //Add to evidence date
        GameplayController.Instance.dateEvidence.Add(this);
    }

    //Override data sources: This needs it's own, not the owners like in EvidenceItem
    public override void BuildDataSources()
    {
        //base.BuildDataSources();

        //if (dataSource == null) dataSource = new Dictionary<string, EvidenceLinkData>();

        //dataSource.Add("date", new EvidenceLinkData(date));

        //if (!dataSource.ContainsKey("summary"))
        //{
        //    dataSource.Add("summary", new EvidenceLinkData(Strings.Casing.asIs, "evidence.body", preset.name, ev: this));
        //}
    }

    //Name
    public override string GenerateName()
    {
        return date;
    }
}
