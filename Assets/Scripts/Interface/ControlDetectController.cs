using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Rewired;
using NaughtyAttributes;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Linq;
using System.IO;
using System;
using UnityEngine.SceneManagement;

//A script that handles the intial scene; controller detection
public class ControlDetectController : MonoBehaviour
{
    [Header("Components")]
    public TextMeshProUGUI pressAnyKeyText;
    public List<CanvasRenderer> fadeOutRenderers = new List<CanvasRenderer>();
    public List<CanvasRenderer> fadeInRenderers = new List<CanvasRenderer>();
    public RectTransform loadingIcon;
    public AnimationCurve loadingIconAnimCurve;

    [System.NonSerialized]
    public Rewired.Player player;

    [Header("Variables")]
    public bool loadSceneTriggered = false;
    private bool loadingScene = false;
    public float fadeOut = 0f;

    [Space(10)]
    [InfoBox("This scene is loaded before dicitionary text; so include translations for the text 'press any button' below...")]
    public List<ControllerDetectTranslation> translations = new List<ControllerDetectTranslation>();

    [System.Serializable]
    public class ControllerDetectTranslation
    {
        public string languageCode;
        public SystemLanguage language;
        public string text;
    }

    //Singleton pattern
    private static ControlDetectController _instance;
    public static ControlDetectController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    void Start()
    {
        QualitySettings.vSyncCount = 1; //By default limit fps here

        //Set the text 'press any key' ot localized variant; this scene is loaded before any other text so do it this slightly annoying way
        bool playedBefore = false;
        playedBefore = Convert.ToBoolean(PlayerPrefs.GetInt("playedBefore", 0)); //Detect if the game has been played before

        ControllerDetectTranslation loadedLanguage = translations.Find(item => item.language == SystemLanguage.English); //Default to ENG

        //Load saved language
        if(playedBefore)
        {
            string langStr = PlayerPrefs.GetString("language");
            loadedLanguage = translations.Find(item => item.languageCode == langStr);
        }
        //Attempt to autodetect language
        else
        {
            loadedLanguage = translations.Find(item => item.language == Application.systemLanguage);
        }

        if(loadedLanguage != null)
        {
            pressAnyKeyText.text = loadedLanguage.text;
        }
    }

    void Update()
    {
        if (!ReInput.isReady) return; // Exit if Rewired isn't ready. This would only happen during a script recompile in the editor.

        if (player == null)
        {
            player = ReInput.players.GetPlayer(0);

            if (player == null)
            {
                return;
            }
        }
        else
        {
            if(!loadSceneTriggered)
            {
                //Detect mouse/keyboard input
                if (!loadSceneTriggered && (ReInput.controllers.GetAnyButtonUp(ControllerType.Mouse) || ReInput.controllers.GetAnyButtonUp(ControllerType.Keyboard)))
                {
                    loadSceneTriggered = true;
                    PlayerPrefs.SetInt("controlMethod", 1);
                    Debug.Log("Set control method to 1: Mouse & Keybaord");
                }

                //Disable custom controller for now; could be causing input problems here...
                //Change to 'button up' for controller
                if (!loadSceneTriggered && (ReInput.controllers.GetAnyButtonUp(ControllerType.Joystick)/* || ReInput.controllers.GetAnyButton(ControllerType.Custom)*/))
                {
                    Debug.Log("...Controller input...");

                    Rewired.Controller c = ReInput.controllers.GetLastActiveController();

                    if(c != null && !loadSceneTriggered)
                    {
                        Debug.Log("Last controller with input: " + c.hardwareName);

                        loadSceneTriggered = true;
                        PlayerPrefs.SetInt("controlMethod", 0);
                        Debug.Log("Set control method to 0: Controller");
                    }
                    else
                    {
                        Debug.Log("...Last active controller is null...");
                    }
                }
            }
            else
            {
                if(!loadingIcon.gameObject.activeSelf) loadingIcon.gameObject.SetActive(true);

                loadingIcon.localEulerAngles = new Vector3(0, 0, loadingIconAnimCurve.Evaluate(fadeOut));

                if(fadeOut < 1f)
                {
                    fadeOut += Time.deltaTime * 2f;
                    fadeOut = Mathf.Clamp01(fadeOut);

                    foreach(CanvasRenderer r in fadeOutRenderers)
                    {
                        r.SetAlpha(1f - fadeOut);
                    }

                    foreach(CanvasRenderer r in fadeInRenderers)
                    {
                        r.SetAlpha(fadeOut);
                    }

                    if(pressAnyKeyText.text.Length > 0)
                    {
                        pressAnyKeyText.text = pressAnyKeyText.text.Substring(0, pressAnyKeyText.text.Length - 1);
                    }
                }
                else
                {
                    if(!loadingScene)
                    {
                        LoadMainScene();
                    }
                }
            }
        }
    }

    private async void LoadMainScene()
    {
        loadingScene = true;
        Cursor.visible = false; //Set cursor invisible
        GameObject gameObject = new GameObject("SteamManager");
        gameObject.AddComponent<SteamManager>();
        await AssetLoader.Instance.PerformInitialLoadAsync();
        SceneManager.LoadSceneAsync("Main", LoadSceneMode.Single);
    }
}
