﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactJob : Fact
{
    //Constructor
    public FactJob(FactPreset newPreset, List<Evidence> newFromEvidence, List<Evidence> newToEvidence, List<object> newPassedObjects, List<Evidence.DataKey> overrideFromKeys, List<Evidence.DataKey> overrideToKeys, bool isCustomFact) : base(newPreset, newFromEvidence, newToEvidence, newPassedObjects, overrideFromKeys, overrideToKeys, isCustomFact)
    {
        
    }

    //On discovery, add identifiers to parent
    public override void OnDiscovery()
    {
        base.OnDiscovery();

        EvidenceCitizen cit = fromEvidence[0] as EvidenceCitizen;
        //fromEvidence[0].AddIdentifier(Evidence.DataKey.name, cit.witnessController.job.name, this, 11);

        string worksAt = Strings.Get("evidence.generic", preset.name) + " " + toEvidence[0].GetNameForDataKey(Evidence.DataKey.name);
        //fromEvidence[0].AddIdentifier(Evidence.DataKey.name, worksAt, this, 12);
    }

    //public override string GenerateNameSuffix()
    //{
    //    EvidenceCitizen cit = fromEvidence[0] as EvidenceCitizen;

    //    //Add job title to name
    //    return " " + Strings.Get("evidence.generic", "as a") + " " + cit.witnessController.job.name;
    //}
}
