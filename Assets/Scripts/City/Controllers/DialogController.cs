﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Reflection;
using System.Text;
using System;

public class DialogController : MonoBehaviour
{
    public DialogPreset payForCode;

    public InfoWindow askWindow;
    public Human askTarget;
    public List<Evidence.DataKey> askTargetKeys = new List<Evidence.DataKey>();

    public Dictionary<DialogPreset, MethodInfo> dialogRef = new Dictionary<DialogPreset, MethodInfo>(); //Store pre-configured method info in here for speed

    [NonSerialized]
    public SideJob sideJobReference; //Used for referencing the job on a input name check
    [NonSerialized]
    public DialogPreset preset;
    [NonSerialized]
    public Citizen cit;

    public enum ForceSuccess { none, success, fail};

    //Singleton pattern
    private static DialogController _instance;
    public static DialogController Instance { get { return _instance; } }

    //Init
    void Start()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        foreach(DialogPreset act in Toolbox.Instance.allDialog)
        {
            //Create method info
            MethodInfo mi = this.GetType().GetMethod(act.name);

            if(mi != null)
            {
                dialogRef.Add(act, mi);
            }
        }
    }

    public bool ExecuteDialog(EvidenceWitness.DialogOption dialog, Interactable saysTo, NewNode where, Actor saidBy, ForceSuccess forceSuccess = ForceSuccess.none)
    {
        MethodInfo method = null;
        bool success = true;
        sideJobReference = dialog.jobRef;
        preset = dialog.preset;
        cit = null;
        
        if(saysTo.isActor != null)
        {
            cit = saysTo.isActor as Citizen;
        }

        //Do success test
        if(forceSuccess == ForceSuccess.success)
        {
            success = true;
        }
        else if(forceSuccess == ForceSuccess.fail)
        {
            success = false;
        }
        else if(SideJobController.Instance.jobTracking.Exists(item => item.preset.changePosterDialogCompliancy != JobPreset.ParticipantCompliancy.noChange && item.activeJobs.Exists(item => item.accepted && item.poster == cit)))
        {
            SideJobController.JobTracking tr = SideJobController.Instance.jobTracking.Find(item => item.preset.changePosterDialogCompliancy != JobPreset.ParticipantCompliancy.noChange && item.activeJobs.Exists(item => item.accepted && item.poster == cit));

            if(tr != null)
            {
                if (tr.preset.changePosterDialogCompliancy == JobPreset.ParticipantCompliancy.alwaysSuccess)
                {
                    success = true;
                }
                else if (tr.preset.changePosterDialogCompliancy == JobPreset.ParticipantCompliancy.alwaysFail)
                {
                    success = false;
                }
            }
        }
        else if (SideJobController.Instance.jobTracking.Exists(item => item.preset.changePerpDialogCompliancy != JobPreset.ParticipantCompliancy.noChange && item.activeJobs.Exists(item => item.accepted && item.purp == cit)))
        {
            SideJobController.JobTracking tr = SideJobController.Instance.jobTracking.Find(item => item.preset.changePerpDialogCompliancy != JobPreset.ParticipantCompliancy.noChange && item.activeJobs.Exists(item => item.accepted && item.purp == cit));

            if (tr != null)
            {
                if (tr.preset.changePerpDialogCompliancy == JobPreset.ParticipantCompliancy.alwaysSuccess)
                {
                    success = true;
                }
                else if (tr.preset.changePerpDialogCompliancy == JobPreset.ParticipantCompliancy.alwaysFail)
                {
                    success = false;
                }
            }
        }
        else if(dialog.preset.useSuccessTest && (cit == null || !cit.alwaysPassDialogSuccess))
        {
            float chance = dialog.preset.baseChance;

            if (dialog.preset.modifySuccessChanceTraits.Count > 0 && cit != null)
            {
                chance = cit.GetChance(ref dialog.preset.modifySuccessChanceTraits, chance);
            }

            //Apply restrained effect
            if(cit != null && cit.ai != null)
            {
                if(cit.ai.restrained)
                {
                    chance += dialog.preset.affectChanceIfRestrained;
                }
            }

            chance = Mathf.Clamp01(chance + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.dialogChanceModifier));

            //If accuse murder; always fail unless they are
            if(preset.specialCase == DialogPreset.SpecialCase.mustBeMurdererForSuccess)
            {
                if(MurderController.Instance.currentMurderer != cit)
                {
                    chance = 0;
                    success = false;
                }
            }

            //Success, same each time
            if (Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, saysTo.name) <= chance)
            {
                success = true;
            }
            else
            {
                success = false;
            }

            //Requires the location password first
            if (dialog.preset.requiresPassword)
            {
                if (cit != null)
                {
                    if (cit.currentGameLocation != null && cit.currentGameLocation.thisAsAddress != null)
                    {
                        if (cit.currentGameLocation.thisAsAddress.addressPreset != null && cit.currentGameLocation.thisAsAddress.addressPreset.needsPassword)
                        {
                            if (!GameplayController.Instance.playerKnowsPasswords.Contains(cit.currentGameLocation.thisAsAddress.id))
                            {
                                success = false;
                                Game.Log("Player does not know password...");
                            }
                        }
                    }
                }
            }
        }

        //Call about side job inquirey: Is this the right person?
        if (dialog.preset.specialCase == DialogPreset.SpecialCase.talkingToJobPoster)
        {
            success = false;

            if (dialog.jobRef != null)
            {
                if(dialog.jobRef.poster == cit)
                {
                    success = true;
                }
            }
        }
        //Last caller
        else if(dialog.preset.specialCase == DialogPreset.SpecialCase.lastCaller)
        {
            success = false;

            foreach (TelephoneController.PhoneCall call in where.building.callLog)
            {
                if (call.toNS != null && call.toNS.location == where.gameLocation)
                {
                    success = true;
                    break;
                }
            }
        }
        //Hand in job item
        else if(dialog.preset.specialCase == DialogPreset.SpecialCase.returnJobItemA)
        {
            success = false;

            if (sideJobReference != null)
            {
                Interactable stolen = sideJobReference.GetItem(JobPreset.JobTag.A);

                if (stolen != null)
                {
                    //Check in inventory
                    if(InteractionController.Instance.talkingTo  != null && InteractionController.Instance.talkingTo == sideJobReference.poster.interactable)
                    {
                        for (int i = 0; i < FirstPersonItemController.Instance.slots.Count; i++)
                        {
                            FirstPersonItemController.InventorySlot slotItem = FirstPersonItemController.Instance.slots[i];
                            Interactable obj = slotItem.GetInteractable();

                            if(obj != null && obj == stolen)
                            {
                                success = true;
                                FirstPersonItemController.Instance.EmptySlot(slotItem, false, false, true);

                                SideJobStolenItem stolenItemM = sideJobReference as SideJobStolenItem;
                                if (stolenItemM != null) stolenItemM.ReturnItem();

                                break;
                            }
                        }
                    }

                    //Check in mailbox
                    if(!success)
                    {
                        //Find poster mailbox
                        Interactable mailboxDoor = Toolbox.Instance.GetMailbox(sideJobReference.poster);

                        if(mailboxDoor != null)
                        {
                            foreach (Interactable i in mailboxDoor.node.interactables)
                            {
                                if (i.preset == stolen.preset)
                                {
                                    if (i.inInventory == null)
                                    {
                                        float dist = Vector3.Distance(i.GetWorldPosition(), mailboxDoor.GetWorldPosition());

                                        if (dist <= 0.5f)
                                        {
                                            success = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //Starch pitch
        else if(dialog.preset.specialCase == DialogPreset.SpecialCase.starchPitch)
        {
            if(success)
            {
                int pay = Mathf.RoundToInt(UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.starchAmbassador));

                GameplayController.Instance.AddMoney(pay, true, "Starch pitch");
            }
        }
        //Loan shark
        else if(dialog.preset.specialCase == DialogPreset.SpecialCase.loanSharkAsk || dialog.preset.specialCase == DialogPreset.SpecialCase.loanSharkAccept)
        {
            //Fail if existing debt exists
            if (cit.job != null && cit.job.employer != null && GameplayController.Instance.debt.Exists(item => item.companyID == cit.job.employer.companyID))
            {
                Game.Log("Debug: Cannot find existing debt, returning dialog unsuccessful: " + cit.GetCitizenName());
                success = false;
            }
            else
            {
                Game.Log("Debug: Cannot find existing debt, returning dialog success: " + cit.GetCitizenName());
                success = true;
            }
        }
        //Loan shark payment
        else if(dialog.preset.specialCase == DialogPreset.SpecialCase.loanSharkPayment)
        {
            if(cit.job != null && cit.job.employer != null)
            {
                //Success if all paid off
                GameplayController.LoanDebt debt = GameplayController.Instance.debt.Find(item => item.companyID == cit.job.employer.companyID);

                if (debt != null)
                {
                    if (debt.debt > 0)
                    {
                        success = false;
                    }
                    else success = true;
                }
                else success = true;
            }
        }

        //Invoke method for further customisability
        if (dialogRef.TryGetValue(dialog.preset, out method))
        {
            object[] passed = { cit, saysTo, where, saidBy, success, dialog.roomRef, dialog.jobRef };
            method.Invoke(this, passed);
        }

        //Trigger response
        SpeechController sc = saysTo.speechController;
        if (InteractionController.Instance.isRemote && InteractionController.Instance.remoteOverride != null) sc = InteractionController.Instance.remoteOverride.speechController;

        if (sc != null)
        {
            //Pass relevent responses
            List<AIActionPreset.AISpeechPreset> resp = dialog.preset.responses.FindAll(item => !preset.useSuccessTest || item.isSuccessful == success);
            sc.Speak(ref resp, sideJob: dialog.jobRef, dialogPreset: dialog.preset, saysTo: saysTo);
        }
        else
        {
            Game.LogError("Speech controller for " + saysTo.id + " is null. IsRemote: " + InteractionController.Instance.isRemote + " = Override: " + InteractionController.Instance.remoteOverride);
        }

        //Remove after saying
        if (dialog.preset.removeAfterSaying)
        {
            (saysTo.evidence as EvidenceWitness).RemoveDialogOption(dialog.preset.tiedToKey, dialog.preset, dialog.jobRef);
        }

        //Trigger on dialog for job
        if (dialog.jobRef != null && success && dialog.jobRef.thisCase != null)
        {
            foreach (Objective obj in dialog.jobRef.thisCase.currentActiveObjectives)
            {
                foreach (Objective.ObjectiveTrigger trig in obj.queueElement.triggers)
                {
                    if (trig.triggerType == Objective.ObjectiveTriggerType.onDialogSuccess)
                    {
                        if (trig.name.ToLower() == dialog.preset.name.ToLower())
                        {
                            trig.Trigger();
                        }
                    }
                }
            }
        }

        //Trigger on dialog for chapter
        else if(ChapterController.Instance != null && ChapterController.Instance.chapterScript != null)
        {
            ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

            if (intro != null)
            {
                foreach (Objective obj in intro.thisCase.currentActiveObjectives)
                {
                    foreach (Objective.ObjectiveTrigger trig in obj.queueElement.triggers)
                    {
                        if (trig.triggerType == Objective.ObjectiveTriggerType.onDialogSuccess)
                        {
                            if (trig.name.ToLower() == dialog.preset.name.ToLower())
                            {
                                trig.Trigger();
                            }
                        }
                    }
                }
            }
        }

        //Trigger job objectives...
        if (dialog.preset.isJobDetails && dialog.jobRef != null && success)
        {
            dialog.jobRef.OnAcquireJobInfo(dialog.preset);
        }

        return success;
    }

    public bool TestSpecialCaseAvailability(DialogPreset preset, Citizen saysTo, SideJob jobRef)
    {
        bool ret = false;
        if (preset.specialCase == DialogPreset.SpecialCase.neverDisplay) return false;

        //Only allow options marked with mugging present in this dialog type
        if (preset.specialCase == DialogPreset.SpecialCase.mugging)
        {
            if (InteractionController.Instance.dialogType != InteractionController.ConversationType.mugging)
            {
                ret =  false;
            }
            else ret = true;
        }
        else if (InteractionController.Instance.dialogType == InteractionController.ConversationType.mugging)
        {
            return false;
        }

        if (preset.specialCase == DialogPreset.SpecialCase.loanSharkPayment || preset.specialCase == DialogPreset.SpecialCase.loanSharkPaymentRefuse || preset.specialCase == DialogPreset.SpecialCase.loanSharkAsk || preset.specialCase == DialogPreset.SpecialCase.loanSharkAccept)
        {
            if (InteractionController.Instance.dialogType != InteractionController.ConversationType.loanSharkVisit)
            {
                ret = false;
            }
            else ret = true;
        }
        else if (InteractionController.Instance.dialogType == InteractionController.ConversationType.loanSharkVisit)
        {
            return false;
        }

        if (preset.specialCase == DialogPreset.SpecialCase.none)
        {
            ret = true;
        }
        else if(preset.specialCase == DialogPreset.SpecialCase.medicalCosts)
        {
            ret = true;
        }
        else if(preset.specialCase == DialogPreset.SpecialCase.backroomBribe)
        {
            //Citizen must be somewhere with a backroom...
            if(saysTo != null && saysTo.currentGameLocation.thisAsAddress != null && saysTo.job != null && saysTo.job.employer != null)
            {
                if (saysTo.currentGameLocation.thisAsAddress.company != null && saysTo.currentGameLocation.thisAsAddress.company.openForBusinessDesired && saysTo.currentGameLocation.thisAsAddress.company.openForBusinessActual)
                {
                    if (saysTo.currentGameLocation == saysTo.job.employer.placeOfBusiness)
                    {
                        int passCodeRooms = 0;

                        foreach (NewRoom r in saysTo.currentGameLocation.rooms)
                        {
                            if (r.passcode.used)
                            {
                                if (!r.belongsTo.Contains(saysTo as Human))
                                {
                                    passCodeRooms++;
                                }
                            }
                        }

                        if (passCodeRooms > 0) ret = true;
                    }
                }
            }
        }
        else if(preset.specialCase == DialogPreset.SpecialCase.publicFacingWorkplace)
        {
            if (saysTo != null && saysTo.currentGameLocation.thisAsAddress != null && saysTo.job != null && saysTo.job.employer != null && saysTo.job.employer.publicFacing)
            {
                if(saysTo.job.employer.openForBusinessDesired)
                {
                    if (saysTo.currentGameLocation == saysTo.job.employer.placeOfBusiness)
                    {
                        ret = true;
                    }
                }
            }
        }
        else if(preset.specialCase == DialogPreset.SpecialCase.working)
        {
            if (saysTo != null && saysTo.isAtWork && saysTo.currentGameLocation.thisAsAddress != null && saysTo.job != null && saysTo.job.employer != null)
            {
                ret = true;
            }
        }
        else if (preset.specialCase == DialogPreset.SpecialCase.workingGuestPass)
        {
            if (saysTo != null && saysTo.isAtWork && saysTo.currentGameLocation.thisAsAddress != null && saysTo.job != null && saysTo.job.employer != null)
            {
                if (saysTo.job.employer.placeOfBusiness.thisAsAddress != null)
                {
                    if(!GameplayController.Instance.guestPasses.ContainsKey(saysTo.job.employer.placeOfBusiness.thisAsAddress))
                    {
                        ret = true;
                    }
                }
            }
        }
        else if (preset.specialCase == DialogPreset.SpecialCase.lookAroundHome)
        {
            if (saysTo != null && saysTo.home != null && saysTo.currentGameLocation.thisAsAddress != null && saysTo.currentGameLocation == saysTo.home)
            {
                if (!GameplayController.Instance.guestPasses.ContainsKey(saysTo.home))
                {
                    ret = true;
                }
            }
        }
        else if(preset.specialCase == DialogPreset.SpecialCase.starchPitch)
        {
            if (UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.starchAmbassador) > 0f)
            {
                ret = true;
            }
            else ret = false;
        }
        else if (preset.specialCase == DialogPreset.SpecialCase.returnJobItemA)
        {
            if (saysTo != null)
            {
                if(jobRef != null)
                {
                    Interactable stolen = jobRef.GetItem(JobPreset.JobTag.A);

                    if (stolen != null)
                    {
                        //Check in inventory
                        if (InteractionController.Instance.talkingTo != null && InteractionController.Instance.talkingTo == sideJobReference.poster.interactable)
                        {
                            for (int i = 0; i < FirstPersonItemController.Instance.slots.Count; i++)
                            {
                                FirstPersonItemController.InventorySlot slotItem = FirstPersonItemController.Instance.slots[i];
                                Interactable obj = slotItem.GetInteractable();

                                if (obj != null && obj == stolen)
                                {
                                    ret = true;
                                    FirstPersonItemController.Instance.EmptySlot(slotItem, false, false, true);

                                    SideJobStolenItem stolenItemM = sideJobReference as SideJobStolenItem;
                                    if (stolenItemM != null) stolenItemM.ReturnItem();

                                    break;
                                }
                            }
                        }

                        //Check in mailbox
                        if (!ret)
                        {
                            //Find poster mailbox
                            Interactable mailboxDoor = Toolbox.Instance.GetMailbox(sideJobReference.poster);

                            if (mailboxDoor != null)
                            {
                                foreach (Interactable i in mailboxDoor.node.interactables)
                                {
                                    if (i.preset == stolen.preset)
                                    {
                                        if (i.inInventory == null)
                                        {
                                            float dist = Vector3.Distance(i.GetWorldPosition(), mailboxDoor.GetWorldPosition());

                                            if (dist <= 0.5f)
                                            {
                                                ret = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else if(preset.specialCase == DialogPreset.SpecialCase.callInSuspect)
        {
            if(CityData.Instance.citizenDirectory.Exists(item => item.ai != null && item.ai.restrained))
            {
                ret = true;
            }
        }
        else if(preset.specialCase == DialogPreset.SpecialCase.talkingToJobPoster)
        {
            //Must live at the poster's phone address
            foreach (KeyValuePair<int, SideJob> pair in SideJobController.Instance.allJobsDictionary)
            {
                //Job must be accepted
                if (pair.Value.accepted && pair.Value.state == SideJob.JobState.posted)
                {
                    if (pair.Value.poster.home == saysTo.home && saysTo.isHome)
                    {
                        ret = true;
                        break;
                    }
                }
            }
        }
        else if (preset.specialCase == DialogPreset.SpecialCase.lastCaller)
        {
             ret = true;
        }
        else if (preset.specialCase == DialogPreset.SpecialCase.knowName)
        {
            if(saysTo != null)
            {
                List<Evidence.DataKey> tied = saysTo.evidenceEntry.GetTiedKeys(Evidence.DataKey.photo);

                if(tied.Contains(Evidence.DataKey.voice))
                {
                    ret = true;
                }
            }
        }
        else if(preset.specialCase == DialogPreset.SpecialCase.loanSharkPayment || preset.specialCase == DialogPreset.SpecialCase.loanSharkPaymentRefuse)
        {
            if (saysTo != null && saysTo.job != null && saysTo.job.employer != null)
            {
                GameplayController.LoanDebt debt = GameplayController.Instance.debt.Find(item => item.companyID == saysTo.job.employer.companyID);

                if (debt != null)
                {
                    //Minimum time is one day before due deadline
                    if(SessionData.Instance.gameTime >= debt.nextPaymentDueBy - 24)
                    {
                        ret = true;
                    }
                    else ret = false;
                }
                else ret = false;
            }
            else ret = false;
        }
        else if(preset.specialCase == DialogPreset.SpecialCase.loanSharkAsk || preset.specialCase == DialogPreset.SpecialCase.loanSharkAccept)
        {
            if (saysTo != null && saysTo.job != null && saysTo.job.employer != null && saysTo.isAtWork)
            {
                ret = true;
            }
        }
        else if(preset.specialCase == DialogPreset.SpecialCase.revealHiddenitemPhoto)
        {
            ret = true;
        }
        else if(preset.specialCase == DialogPreset.SpecialCase.mustBeMurdererForSuccess)
        {
            ret = true;
        }
        else if (preset.specialCase == DialogPreset.SpecialCase.hotelBill || preset.specialCase == DialogPreset.SpecialCase.hotelCheckOut || preset.specialCase == DialogPreset.SpecialCase.mustHaveRoomAtHotel)
        {
            //Only available if the player has existing room in this hotel
            if (saysTo != null && saysTo.isAtWork && saysTo.currentGameLocation.thisAsAddress != null && saysTo.job != null && saysTo.job.employer != null)
            {
                if (saysTo.job.employer.placeOfBusiness.thisAsAddress != null)
                {
                    if(preset.specialCase == DialogPreset.SpecialCase.hotelBill)
                    {
                        if (GameplayController.Instance.hotelGuests.Exists(item => item.humanID == Player.Instance.humanID && item.GetAddress() != null && item.bill > 0 && item.GetAddress().building == saysTo.job.employer.placeOfBusiness.thisAsAddress.building))
                        {
                            ret = true;
                        }
                    }
                    else
                    {
                        if (GameplayController.Instance.hotelGuests.Exists(item => item.humanID == Player.Instance.humanID && item.GetAddress() != null && item.GetAddress().building == saysTo.job.employer.placeOfBusiness.thisAsAddress.building))
                        {
                            ret = true;
                        }
                    }
                }
            }
        }
        else if (preset.specialCase == DialogPreset.SpecialCase.hotelRentRoom || preset.specialCase == DialogPreset.SpecialCase.rentHotelRoomCheap || preset.specialCase == DialogPreset.SpecialCase.rentHotelRoomExpensive)
        {
            //Only available if the player has no existing room in this hotel...
            if (saysTo != null && saysTo.isAtWork && saysTo.currentGameLocation.thisAsAddress != null && saysTo.job != null && saysTo.job.employer != null)
            {
                if (saysTo.job.employer.placeOfBusiness.thisAsAddress != null)
                {
                    if(!GameplayController.Instance.hotelGuests.Exists(item => item.humanID == Player.Instance.humanID && item.GetAddress() != null && item.GetAddress().building == saysTo.job.employer.placeOfBusiness.thisAsAddress.building))
                    {
                        ret = true;
                    }
                }
            }
        }

        //Test if a password is needed for this address?
        if (preset.displayIfPasswordUnknown)
        {
            //Doesn't need password
            if(saysTo != null && saysTo.currentGameLocation != null && saysTo.currentGameLocation.thisAsAddress != null && saysTo.currentGameLocation.thisAsAddress.addressPreset != null && !saysTo.currentGameLocation.thisAsAddress.addressPreset.needsPassword)
            {
                ret = false;
            }
            //Player has already given password
            else if(saysTo != null && saysTo.currentGameLocation != null && saysTo.currentGameLocation.thisAsAddress != null && saysTo.currentGameLocation.thisAsAddress.addressPreset != null && saysTo.currentGameLocation.thisAsAddress.addressPreset.needsPassword && GameplayController.Instance.playerKnowsPasswords.Contains(saysTo.currentGameLocation.thisAsAddress.id))
            {
                ret = false;
            }
        }

        return ret;
    }

    //Triggered after dialog has finished (ONLY triggers with 'end dialog' flag)
    public void OnDialogEnd(AIActionPreset.AISpeechPreset dialog, string dialogPresetStr, Interactable saysToInteractable, Actor saidBy, int jobRef)
    {
        Game.Log("ONENDDIALOG");

        DialogPreset dialogPreset = null;

        //Get Preset
        if (Toolbox.Instance.LoadDataFromResources<DialogPreset>(dialogPresetStr, out dialogPreset))
        {
            Game.Log("ONENDDIALOG: " + dialogPreset.name);

            //This won't work the first time; that's triggered within the sidejob class, but this will enable it to be displayed on repeat attempts
            if (dialogPreset.specialCase == DialogPreset.SpecialCase.revealHiddenitemPhoto)
            {
                SideJob sideJob = null;

                if (SideJobController.Instance.allJobsDictionary.TryGetValue(jobRef, out sideJob))
                {
                    Game.Log("Jobs: Revealing hidden briefcase photograph...");

                    if (sideJob.hiddenItemPhoto != null)
                    {
                        ActionController.Instance.Inspect(sideJob.hiddenItemPhoto, Player.Instance.currentNode, Player.Instance);
                        CasePanelController.Instance.PinToCasePanel(sideJob.thisCase, sideJob.hiddenItemPhoto.evidence, Evidence.DataKey.photo, true);
                    }
                }
                else Game.LogError("Unable to find sidejob " + jobRef);
            }
        }
        else Game.LogError("Unable to find dialog preset " + dialogPresetStr);
    }

    public void BribeForCode(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        //If successful add bribe options for all passcode rooms...
        if(success)
        {
            //Citizen must be somewhere with a backroom...
            if (saysTo.currentGameLocation.thisAsAddress != null && saysTo.job != null && saysTo.job.employer != null)
            {
                if (saysTo.currentGameLocation == saysTo.job.employer.placeOfBusiness && saysTo.job.employer.placeOfBusiness.thisAsAddress != null)
                {
                    foreach (NewRoom r in saysTo.currentGameLocation.rooms)
                    {
                        if (r.passcode.used)
                        {
                            if (!r.belongsTo.Contains(saysTo as Human))
                            {
                                ////Add room to datasource
                                //Dictionary<string, EvidenceLinkData> ds = new Dictionary<string, EvidenceLinkData>();
                                //ds.Add("room", new EvidenceLinkData(r.name, link: EvidenceLinkData.OnLinkExecute.none));

                                //string code = string.Empty;

                                //foreach(int p in r.passcode.digits)
                                //{
                                //    code += p.ToString();
                                //}

                                //ds.Add("code", new EvidenceLinkData(code, link: EvidenceLinkData.OnLinkExecute.none));

                                //saysTo.evidenceEntry.AddDialogOption(Evidence.DataKey.photo, payForCode, roomRef: r, dataSource: ds);
                            }
                        }
                    }
                }
            }
        }
    }

    public void Beg(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        //If successful add bribe options for all passcode rooms...
        if (success)
        {
            GameplayController.Instance.AddMoney(Toolbox.Instance.Rand(1, 3), true, "Beg");
        }
    }

    public void PayForCode(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        //Citizen must be somewhere with a backroom...
        if (saysTo != null && saysTo.currentGameLocation.thisAsAddress != null && saysTo.job != null && saysTo.job.employer != null)
        {
            if (saysTo.currentGameLocation.thisAsAddress.company != null && saysTo.currentGameLocation.thisAsAddress.company.openForBusinessDesired)
            {
                if (saysTo.currentGameLocation == saysTo.job.employer.placeOfBusiness)
                {
                    foreach (NewRoom r in saysTo.currentGameLocation.rooms)
                    {
                        if (r.passcode.used)
                        {
                            foreach(NewNode.NodeAccess acc in r.entrances)
                            {
                                if(acc.door != null)
                                {
                                    if(acc.door.passwordDoorsRoom != null)
                                    {
                                        if(acc.door.passwordDoorsRoom.passcode != null)
                                        {
                                            GameplayController.Instance.AddPasscode(acc.door.passwordDoorsRoom.passcode, true);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void IssueGuestPass(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        //Citizen must be somewhere with a backroom...
        if(success)
        {
            if (saysTo.job != null && saysTo.job.employer != null)
            {
                if (saysTo.currentGameLocation == saysTo.job.employer.placeOfBusiness && saysTo.job.employer.placeOfBusiness.thisAsAddress != null)
                {
                    GameplayController.Instance.AddGuestPass(saysTo.job.employer.placeOfBusiness.thisAsAddress, 2f);
                }
            }
        }
    }

    public void DoYouKnowThisPerson(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        if(success)
        {
            Game.Log("Interface: Spawn photo select...");

            SessionData.Instance.PauseGame(true);

            //Bring up select window
            askTarget = null;
            askTargetKeys.Clear();
            askWindow = InterfaceController.Instance.SpawnWindow(null, presetName: "SelectPhoto", autoPosition: false, forcePosition: InterfaceControls.Instance.handbookWindowPosition, worldInteraction: true, passDialogSuccess: success);
        }
    }

    public void DoYouKnowThisPersonBribe1(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        if (success)
        {
            Game.Log("Interface: Spawn photo select...");

            SessionData.Instance.PauseGame(true);

            //Bring up select window
            askTarget = null;
            askTargetKeys.Clear();
            askWindow = InterfaceController.Instance.SpawnWindow(null, presetName: "SelectPhoto", autoPosition: false, forcePosition: InterfaceControls.Instance.handbookWindowPosition, worldInteraction: true, passDialogSuccess: success);
        }
    }

    public void DoYouKnowThisPersonBribe2(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        if (success)
        {
            Game.Log("Interface: Spawn photo select...");

            SessionData.Instance.PauseGame(true);

            //Bring up select window
            askTarget = null;
            askTargetKeys.Clear();
            askWindow = InterfaceController.Instance.SpawnWindow(null, presetName: "SelectPhoto", autoPosition: false, forcePosition: InterfaceControls.Instance.handbookWindowPosition, worldInteraction: true, passDialogSuccess: success);
        }
    }

    public void DoYouKnowThisPersonBribe3(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        if (success)
        {
            Game.Log("Interface: Spawn photo select...");

            SessionData.Instance.PauseGame(true);

            //Bring up select window
            askTarget = null;
            askTargetKeys.Clear();
            askWindow = InterfaceController.Instance.SpawnWindow(null, presetName: "SelectPhoto", autoPosition: false, forcePosition: InterfaceControls.Instance.handbookWindowPosition, worldInteraction: true, passDialogSuccess: success);
        }
    }

    public void BuySomething(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        if (success)
        {
            Game.Log("Interface: Spawn buy select...");

            SessionData.Instance.PauseGame(true);

            string windowType = "Buy";

            //Enable sell?
            if(saysTo != null)
            {
                if(saysTo.job != null && saysTo.job.employer != null)
                {
                    if(saysTo.job.employer.preset.enableSelling)
                    {
                        windowType = "BuySell";
                    }
                }
            }

            InfoWindow selectWindow = InterfaceController.Instance.SpawnWindow(null, presetName: windowType, autoPosition: false, forcePosition: InterfaceControls.Instance.handbookWindowPosition, worldInteraction: true, passedInteractable: saysTo.interactable);
        }
    }

    public void PhoneKeypad(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        if (success)
        {
            SessionData.Instance.PauseGame(true);
            InfoWindow selectWindow = InterfaceController.Instance.SpawnWindow(CityData.Instance.telephone, autoPosition: false, forcePosition: InterfaceControls.Instance.handbookWindowPosition, worldInteraction: true, passedInteractable: saysToInteractable);
        }
    }

    public void IdentifyNumber(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        if (success)
        {
            //Reveal and find phone number evidence
            if (saysToInteractable.t != null)
            {
                where.gameLocation.evidenceEntry.AddDiscovery(Evidence.Discovery.phoneLocation);
                saysToInteractable.t.telephoneEntry.SetFound(true);

                Toolbox.Instance.SpawnWindowAfterSeconds(saysToInteractable.t.telephoneEntry, 3f);
            }
        }
    }

    public void LastCalled(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        Game.Log("Getting last called for " + saysToInteractable.id);
        TelephoneController.PhoneCall lastCall = null;
        float lastCallTime = -9999f;

        foreach(TelephoneController.PhoneCall call in where.building.callLog)
        {
            if (call.toNS != null &&  call.toNS.interactable == saysToInteractable)
            {
                if(call.time > lastCallTime)
                {
                    lastCall = call;
                    lastCallTime = call.time;
                }
            }
        }

        if (lastCall != null)
        {
            SpeechController sc = saysToInteractable.speechController;
            if (InteractionController.Instance.isRemote && InteractionController.Instance.remoteOverride != null) sc = InteractionController.Instance.remoteOverride.speechController;

            //if (!sc.phoneLine.telephoneEntry.dataSource.ContainsKey("lastcall"))
            //{
            //    sc.phoneLine.telephoneEntry.dataSource.Add("lastcall", new EvidenceLinkData(lastCall.fromNS.numberString));
            //}
            //else sc.phoneLine.telephoneEntry.dataSource["lastcall"] = new EvidenceLinkData(lastCall.fromNS.numberString);

            //if (!sc.phoneLine.telephoneEntry.dataSource.ContainsKey("lastcalltime"))
            //{
            //    sc.phoneLine.telephoneEntry.dataSource.Add("lastcalltime", new EvidenceLinkData(SessionData.Instance.TimeStringOnDay(lastCall.time, true, true)));
            //}
            //else sc.phoneLine.telephoneEntry.dataSource["lastcalltime"] = new EvidenceLinkData(SessionData.Instance.TimeStringOnDay(lastCall.time, true, true));

            saysToInteractable.recentCallCheck = SessionData.Instance.gameTime;

            Evidence call = EvidenceCreator.Instance.GetTimeEvidence(lastCall.time, lastCall.time, "TelephoneCall", parentID: lastCall.fromNS.telephoneEntry.evID + ">" + lastCall.toNS.telephoneEntry.evID);
            Fact callFrom = EvidenceCreator.Instance.CreateFact("CallFrom", lastCall.fromNS.telephoneEntry, call, forceDiscoveryOnCreate: true);
            Fact callTo = EvidenceCreator.Instance.CreateFact("CallTo", call, where.gameLocation.evidenceEntry, forceDiscoveryOnCreate: true);

            Toolbox.Instance.SpawnWindowAfterSeconds(call, 5.5f);
            Toolbox.Instance.SpawnWindowAfterSeconds(lastCall.fromNS.telephoneEntry, 5.5f);
        }
        else saysToInteractable.recentCallCheck = -1;
    }

    public void Police(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        if(success)
        {
            GameplayController.Instance.CallEnforcers(saidBy.currentGameLocation);
        }
    }

    //Escape from the hospital
    public void Escape(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        saysToInteractable.SetCustomState2(false, Player.Instance, false);
        Player.Instance.ClearAllDisabledActions(); //Clear disabled actions
        InteractionController.Instance.SetDialog(false, null);

        //Trigger location header text
        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.gameHeader, 0, SessionData.Instance.CurrentTimeString(false, true) + ", " + SessionData.Instance.LongDateString(SessionData.Instance.gameTime, true, true, true, true, true, false, false, true) + ", " + Player.Instance.currentGameLocation.name);
    }

    //Pay medical fees
    public void PayMedicalFees(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        saysToInteractable.SetCustomState2(false, Player.Instance, false);
        Player.Instance.ClearAllDisabledActions(); //Clear disabled actions
        InteractionController.Instance.SetDialog(false, null);

        //Remove detained status
        StatusController.Instance.SetDetainedInBuilding(Player.Instance.currentBuilding, false);

        Player.Instance.TriggerPlayerRecovery();
    }

    //Warn the notewriter in chapter 1
    public void WarnNotewriter(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        //Trigger lay low for note writer
        if(ChapterController.Instance.chapterScript != null)
        {
            ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

            if(intro != null)
            {
                intro.NotewriterLayLow();
            }
        }
    }

    //Add a sync disk slot
    public void InstallNewSyncDiskSlot(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        //UpgradesController.Instance.SetSlots(UpgradesController.Instance.slotCount + 1);
    }

    public void TakeALookAround(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        if (success)
        {
            if (saysTo.currentGameLocation == saysTo.home)
            {
                GameplayController.Instance.AddGuestPass(saysTo.home, 2f);
            }
        }
    }

    public void TakeALookAroundBribe1(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        if (success)
        {
            if (saysTo.currentGameLocation == saysTo.home)
            {
                GameplayController.Instance.AddGuestPass(saysTo.home, 2f);
            }
        }
    }

    public void TakeALookAroundBribe2(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        if (success)
        {
            if (saysTo.currentGameLocation == saysTo.home)
            {
                GameplayController.Instance.AddGuestPass(saysTo.home, 2f);
            }
        }
    }

    public void TakeALookAroundBribe3(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        if (success)
        {
            if (saysTo.currentGameLocation == saysTo.home)
            {
                GameplayController.Instance.AddGuestPass(saysTo.home, 2f);
            }
        }
    }

    public void TakeALookAroundBribe4(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        if (success)
        {
            if (saysTo.currentGameLocation == saysTo.home)
            {
                GameplayController.Instance.AddGuestPass(saysTo.home, 2f);
            }
        }
    }

    public void Job_HouseMeet_StolenItem(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        GameplayController.Instance.AddGuestPass(saysTo.home, 6f); //Add guest pass
        Player.Instance.AddToKeyring(saysTo.home);

        if(jobRef != null && jobRef.thisCase != null)
        {
            foreach(Human h in saysTo.home.inhabitants)
            {
                h.evidenceEntry.MergeDataKeys(Evidence.DataKey.name, Evidence.DataKey.fingerprints);
                h.evidenceEntry.MergeDataKeys(Evidence.DataKey.name, Evidence.DataKey.shoeSize);
                h.evidenceEntry.MergeDataKeys(Evidence.DataKey.name, Evidence.DataKey.address);

                CasePanelController.Instance.PinToCasePanel(jobRef.thisCase, h.evidenceEntry, Evidence.DataKey.name, true);
            }

            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "You have acquired information about residents of: ") + saysTo.home.name, InterfaceControls.Icon.building);
        }
    }

    public void SeenOrHeardUnusual(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        Game.Log("Seen or heard unusual...");
        float mostRecent = -99999f;
        Human.Sighting sighting = null;
        Human saw = null;

        foreach(KeyValuePair<Human, Human.Sighting> pair in saysTo.lastSightings)
        {
            if(pair.Value.poi && !pair.Value.phone)
            {
                if(pair.Value.time > mostRecent)
                {
                    mostRecent = pair.Value.time;
                    saw = pair.Key;
                    sighting = pair.Value;
                }
            }
        }

        if(sighting != null)
        {
            //Gunshot
            if(sighting.sound == 1)
            {
                saysTo.speechController.Speak("79b84ed9-5c40-4927-9b57-f096cea0930e");
            }
            //Scream
            else if (sighting.sound == 2)
            {
                saysTo.speechController.Speak("d2ac8d3d-1a62-410a-8a73-fd69965679e5");
            }
            //Sighting
            else
            {
                saysTo.speechController.Speak("00b6772a-46d0-48cb-89fe-e212ce3b4cf8");

                //Reveal info tidbit
                List<string> tidBits = new List<string>();
                if(saw.descriptors.hairType != Descriptors.HairStyle.bald) tidBits.Add("3f0faad0-578d-401e-9fa4-5a92140cba6a"); //Hair
                tidBits.Add("39cf63a9-132d-4369-81c7-59aed085f3a0"); //Build
                tidBits.Add("05585825-104b-4ad2-91ab-4429daecbca1"); //Height
                if(saw.characterTraits.Exists(item => item.trait.name == "Affliction-ShortSighted" || item.trait.name == "Affliction-FarSighted")) tidBits.Add("72ddfd37-fa3b-4903-8671-e4c0d1403cee"); //Glasses
                if (saw.characterTraits.Exists(item => item.trait.name == "Quirk-FacialHair")) tidBits.Add("40987a5c-c7cc-4cb0-b37e-aa28ac57d0cf"); //Beard

                saysTo.speechController.Speak(tidBits[Toolbox.Instance.GetPsuedoRandomNumber(0, tidBits.Count, saidBy.name)], speakAbout: saw);
            }

            saysTo.RevealSighting(saw, sighting);
        }
        else
        {
            //"Nothing"
            saysTo.speechController.Speak("c266400b-fe2f-4363-a071-d37e7c58a09d");
        }
    }

    public void GivePassword(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        if(saysTo != null && saysTo.currentGameLocation != null && saysTo.currentGameLocation.thisAsAddress != null)
        {
            if(success)
            {
                GameplayController.Instance.SetPlayerKnowsPassword(saysTo.currentGameLocation.thisAsAddress);
            }
            else
            {
                saysTo.currentGameLocation.thisAsAddress.GetPassword(); //Display for debugging
            }
        }
    }

    public void MuggingAcquiesce(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        //Reset mugger state
        if(InteractionController.Instance.mugger == saysTo)
        {
            Game.Log("Resetting mug state");
            InteractionController.Instance.mugger = null;
        }
    }

    public void LoanShark_AcceptLoan(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        //Reset mugger state
        if (saysTo != null && saysTo.job != null && saysTo.job.employer != null)
        {
            GameplayController.Instance.AddNewDebt(saysTo.job.employer, GameplayControls.Instance.defaultLoanAmount, GameplayControls.Instance.defaultLoanExtra, GameplayControls.Instance.defaultLoanRepayment); ;
        }
    }

    public void LoanShark_Pay(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        //Set debt payment
        if (saysTo != null && saysTo.job != null && saysTo.job.employer != null)
        {
            GameplayController.Instance.DebtPayment(saysTo.job.employer);
        }

        //Reset mugger state
        if (InteractionController.Instance.debtCollector == saysTo)
        {
            Game.Log("Resetting debt collector state");
            InteractionController.Instance.debtCollector = null;
        }
    }

    public void Give(Citizen saysTo, Interactable saysToInteractable, NewNode where, Actor saidBy, bool success, NewRoom roomRef, SideJob jobRef)
    {
        Game.Log("Interface: Spawn item select...");

        SessionData.Instance.PauseGame(true);

        //Bring up select window
        InterfaceController.Instance.SpawnWindow(null, presetName: "SelectItem", autoPosition: false, forcePosition: InterfaceControls.Instance.handbookWindowPosition, worldInteraction: true, passDialogSuccess: success);
    }
}
