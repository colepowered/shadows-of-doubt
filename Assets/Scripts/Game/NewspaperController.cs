using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using TMPro;

public class NewspaperController : MonoBehaviour
{
    [Header("Components")]
    public TextMeshProUGUI newspaperTitleText;
    public TextMeshProUGUI newspaperDateText;
    [Space(7)]
    public TextMeshProUGUI mainArticleHeadline;
    public TextMeshProUGUI mainArticleColumn1;
    public TextMeshProUGUI mainArticleColumn2;
    public TextMeshProUGUI mainArticleColumn3;
    [Space(7)]
    public TextMeshProUGUI article2Headline;
    public TextMeshProUGUI article2Column1;
    public TextMeshProUGUI article2Column2;
    public TextMeshProUGUI article2Column3;
    [Space(7)]
    public TextMeshProUGUI article3Headline;
    public TextMeshProUGUI article3Column1;
    public TextMeshProUGUI article3Column2;
    [Space(7)]
    public TextMeshProUGUI ad1Text;
    public TextMeshProUGUI ad2Text;
    public TextMeshProUGUI ad3Text;
    public TextMeshProUGUI ad4Text;

    [Header("State")]
    public NewspaperState currentState = null;

    [System.Serializable]
    public class NewspaperState
    {
        //Serialized
        public float time;
        public string seed;
        public int murderID = -1;

        public string mainArticle;
        public string article2;
        public string article3;
        public string ad1;
        public string ad2;
        public string ad3;
        public string ad4;

        [System.NonSerialized]
        public NewspaperArticle articleMain;
        [System.NonSerialized]
        public NewspaperArticle secondArticle;
        [System.NonSerialized]
        public NewspaperArticle thirdArticle;
        [System.NonSerialized]
        public NewspaperArticle firstAd;
        [System.NonSerialized]
        public NewspaperArticle secondAd;
        [System.NonSerialized]
        public NewspaperArticle thirdAd;
        [System.NonSerialized]
        public NewspaperArticle fourthAd;

        public void SerializeFields()
        {
            if(mainArticle != null && mainArticle.Length > 0) Toolbox.Instance.LoadDataFromResources<NewspaperArticle>(mainArticle, out articleMain);
            if (article2 != null && article2.Length > 0) Toolbox.Instance.LoadDataFromResources<NewspaperArticle>(article2, out secondArticle);
            if (article3 != null && article3.Length > 0) Toolbox.Instance.LoadDataFromResources<NewspaperArticle>(article3, out thirdArticle);
            if (ad1 != null && ad1.Length > 0) Toolbox.Instance.LoadDataFromResources<NewspaperArticle>(ad1, out firstAd);
            if (ad2 != null && ad2.Length > 0) Toolbox.Instance.LoadDataFromResources<NewspaperArticle>(ad2, out secondAd);
            if (ad3 != null && ad3.Length > 0) Toolbox.Instance.LoadDataFromResources<NewspaperArticle>(ad3, out thirdAd);
            if (ad4 != null && ad4.Length > 0) Toolbox.Instance.LoadDataFromResources<NewspaperArticle>(ad4, out fourthAd);
        }
    }

    //Singleton pattern
    private static NewspaperController _instance;
    public static NewspaperController Instance { get { return _instance; } }


    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    //When a new first person newspaper is spawned...
    public void UpdateNewspaperReferences(NewspaperDisplayController disp)
    {
        Game.Log("Updating newspaper references...");
        if (disp.newspaperTitleText != null) newspaperTitleText = disp.newspaperTitleText;
        if (disp.newspaperDateText != null) newspaperDateText = disp.newspaperDateText;

        if (disp.mainArticleHeadline != null) mainArticleHeadline = disp.mainArticleHeadline;
        if (disp.mainArticleColumn1 != null) mainArticleColumn1 = disp.mainArticleColumn1;
        if (disp.mainArticleColumn2 != null) mainArticleColumn2 = disp.mainArticleColumn2;
        if (disp.mainArticleColumn3 != null) mainArticleColumn3 = disp.mainArticleColumn3;

        if (disp.article2Headline != null) article2Headline = disp.article2Headline;
        if (disp.article2Column1 != null) article2Column1 = disp.article2Column1;
        if (disp.article2Column2 != null) article2Column2 = disp.article2Column2;
        if (disp.article2Column3 != null) article2Column3 = disp.article2Column3;

        if (disp.article3Headline != null) article3Headline = disp.article3Headline;
        if (disp.article3Column1 != null) article3Column1 = disp.article3Column1;
        if (disp.article3Column2 != null) article3Column2 = disp.article3Column2;

        if (disp.ad1Text != null) ad1Text = disp.ad1Text;
        if (disp.ad2Text != null) ad2Text = disp.ad2Text;
        if (disp.ad3Text != null) ad3Text = disp.ad3Text;
        if (disp.ad4Text != null) ad4Text = disp.ad4Text;

        UpdateText(false);
    }

    //Set the current text
    public void UpdateText(bool updateNewsTicker = false)
    {
        if (currentState.articleMain == null) currentState.SerializeFields();

        Game.Log("Gameplay: Updating newspaper text...");

        SetTextForArticle(currentState.articleMain, mainArticleHeadline, new TextMeshProUGUI[] { mainArticleColumn1, mainArticleColumn2, mainArticleColumn3 });
        SetTextForArticle(currentState.secondArticle, article2Headline, new TextMeshProUGUI[] { article2Column1, article2Column2, article2Column3 });
        SetTextForArticle(currentState.thirdArticle, article3Headline, new TextMeshProUGUI[] { article3Column1, article3Column2 });

        SetAdText(currentState.firstAd, ad1Text);
        SetAdText(currentState.secondAd, ad2Text);
        SetAdText(currentState.thirdAd, ad3Text);
        SetAdText(currentState.fourthAd, ad4Text);

        if(newspaperTitleText != null) newspaperTitleText.text = Strings.ComposeText(Strings.Get("misc", "The |city.name| Herald"), null, linkSetting: Strings.LinkSetting.forceNoLinks);
        if(newspaperDateText != null) newspaperDateText.text = SessionData.Instance.LongDateString(currentState.time, true, false, true, false, true, true, false, true);

        //Update news ticker
        if(updateNewsTicker)
        {
            string allText = Strings.GetTextForComponent(currentState.articleMain.ddsReference, GetContextObject(currentState.articleMain, currentState.seed), linkSetting: Strings.LinkSetting.forceNoLinks, lineBreaks: "\n");
            string[] lineBr = allText.Split('\n');

            string newTickerText = string.Empty;
            if (lineBr.Length > 0) newTickerText = lineBr[0];
            if (lineBr.Length > 1) newTickerText += ": " + lineBr[1];

            TextToImageController.Instance.UpdateNewsTickerHeadline(newTickerText);
        }
    }

    public void SetTextForArticle(NewspaperArticle article, TextMeshProUGUI headline, TextMeshProUGUI[] columns, string lineBreaks = "\n\n")
    {
        if (headline == null || columns == null || columns.Length <= 0) return;

        Game.Log("Setting text for article: " + article.name);

        string allText = Strings.GetTextForComponent(article.ddsReference, GetContextObject(article, currentState.seed), linkSetting: Strings.LinkSetting.forceNoLinks, lineBreaks: lineBreaks);
        string[] lineBr = allText.Split(new char[] { '\n' }, System.StringSplitOptions.RemoveEmptyEntries);

        if (lineBr.Length > 0)
        {
            headline.text = lineBr[0];

            //Remove headline from all text
            allText = allText.Substring(lineBr[0].Length, allText.Length - lineBr[0].Length);
            Game.Log(allText);

            string[] words = allText.Split(' ');

            //Divide by 3 for each column...
            int columnLength = Mathf.CeilToInt(words.Length / (float)columns.Length);
            int wordCursor = 0;
            int wordCursorTotal = 0;
            int column = 0;
            string setText = string.Empty;

            while(wordCursor < words.Length)
            {
                if(column < columns.Length && wordCursor < columnLength && wordCursorTotal < words.Length)
                {
                    setText += " " + words[wordCursorTotal];
                    wordCursor++;
                    wordCursorTotal++;
                }
                //else if(column >= columns.Length - 1 && wordCursorTotal < words.Length)
                //{
                //    setText += " " + words[wordCursorTotal];
                //    wordCursor++;
                //    wordCursorTotal++;
                //}
                else
                {
                    if (column < columns.Length)
                    {
                        columns[column].text = setText.Trim();
                    }
                    else break;

                    column++;
                    wordCursor = 0;
                    setText = string.Empty;
                }
            }
        }
    }

    private object GetContextObject(NewspaperArticle article, string seed)
    {
        object ret = null;

        Game.Log("Gameplay: Getting context object for article " + article.name + ", " + article.context.ToString() + "...");

        if(article.context == NewspaperArticle.ContextSource.lastMurder)
        {
            if (currentState.murderID > -1)
            {
                MurderController.Murder m = MurderController.Instance.activeMurders.Find(item => item.murderID == currentState.murderID);
                if (m == null) m = MurderController.Instance.inactiveMurders.Find(item => item.murderID == currentState.murderID);
                ret = m;
            }
            else Game.Log("Gameplay: Unable to find murder context for " + currentState.murderID);
        }
        else if(article.context == NewspaperArticle.ContextSource.player)
        {
            ret = Player.Instance;
        }
        else if(article.context == NewspaperArticle.ContextSource.randomCitizen)
        {
            ret = CityData.Instance.citizenDirectory[Toolbox.Instance.GetPsuedoRandomNumber(0, CityData.Instance.citizenDirectory.Count, seed)];
        }
        else if(article.context == NewspaperArticle.ContextSource.randomCriminal)
        {
            List<Citizen> rCriminal = CityData.Instance.citizenDirectory.FindAll(item => item.job != null && item.job.preset != null && item.job.preset.isCriminal);
            ret = rCriminal[Toolbox.Instance.GetPsuedoRandomNumber(0, rCriminal.Count, seed)];
        }
        else if(article.context == NewspaperArticle.ContextSource.randomGroup)
        {
            ret = GroupsController.Instance.groups[Toolbox.Instance.GetPsuedoRandomNumber(0, GroupsController.Instance.groups.Count, seed)];
        }

        return ret;
    }

    //Ads are just basic blocks of text with no headline...
    public void SetAdText(NewspaperArticle article, TextMeshProUGUI adText)
    {
        if (adText != null)
        {
            Game.Log("Setting text for ad: " + article.name);
            adText.text = Strings.GetTextForComponent(article.ddsReference, GetContextObject(article, currentState.seed), linkSetting: Strings.LinkSetting.forceNoLinks, lineBreaks: string.Empty);
        }
    }

    [Button]
    public void GenerateNewNewspaper()
    {
        currentState = new NewspaperState();
        currentState.time = SessionData.Instance.gameTime;
        currentState.seed = Toolbox.Instance.GenerateSeed();
        MurderController.Murder murderRef = null;

        //Get last reported murder...
        foreach (MurderController.Murder m in MurderController.Instance.activeMurders)
        {
            if(m.death != null && m.death.reported)
            {
                if(currentState.murderID <= -1 || m.murderID > currentState.murderID)
                {
                    currentState.murderID = m.murderID;
                    murderRef = m;
                }
            }
        }

        foreach (MurderController.Murder m in MurderController.Instance.inactiveMurders)
        {
            if (m.death != null && m.death.reported)
            {
                if (currentState.murderID <= -1 || m.murderID > currentState.murderID)
                {
                    currentState.murderID = m.murderID;
                    murderRef = m;
                }
            }
        }

        bool murderSecond = false;

        if(murderRef != null)
        {
            if (MurderController.Instance.activeMurders.Exists(item => item != murderRef && item.murdererID == murderRef.murdererID)) murderSecond = true;
            else if (MurderController.Instance.inactiveMurders.Exists(item => item != murderRef && item.murdererID == murderRef.murdererID)) murderSecond = true;
        }

        //Pick
        List<NewspaperArticle> main = Toolbox.Instance.allArticles.FindAll(item => !item.disabled && (currentState.murderID > -1 && !murderSecond && item.category == NewspaperArticle.Category.murder) || (currentState.murderID > -1 && murderSecond && item.category == NewspaperArticle.Category.murderSecond) || (currentState.murderID <= -1 && item.category == NewspaperArticle.Category.general));

        if (main.Count > 0)
        {
            currentState.articleMain = main[Toolbox.Instance.Rand(0, main.Count)];
            currentState.mainArticle = currentState.articleMain.name;
        }

        List<NewspaperArticle> article2 = Toolbox.Instance.allArticles.FindAll(item => item.category == NewspaperArticle.Category.general && !item.disabled);
        article2.Remove(currentState.articleMain);

        if (article2.Count > 0)
        {
            currentState.secondArticle = article2[Toolbox.Instance.Rand(0, article2.Count)];
            currentState.article2 = currentState.secondArticle.name;
            article2.Remove(currentState.secondArticle);
        }

        List<NewspaperArticle> fa = Toolbox.Instance.allArticles.FindAll(item => item.category == NewspaperArticle.Category.foreignAffairs && !item.disabled);

        if (fa.Count > 0)
        {
            currentState.thirdArticle = fa[Toolbox.Instance.Rand(0, fa.Count)];
            currentState.article3 = currentState.thirdArticle.name;
        }

        List<NewspaperArticle> ads = Toolbox.Instance.allArticles.FindAll(item => item.category == NewspaperArticle.Category.ad && !item.disabled);

        if (ads.Count > 0)
        {
            currentState.firstAd = ads[Toolbox.Instance.Rand(0, ads.Count)];
            currentState.ad1 = currentState.firstAd.name;
            ads.Remove(currentState.firstAd);

            if (ads.Count > 0)
            {
                currentState.secondAd = ads[Toolbox.Instance.Rand(0, ads.Count)];
                currentState.ad2 = currentState.secondAd.name;

                if (ads.Count > 0)
                {
                    currentState.thirdAd = ads[Toolbox.Instance.Rand(0, ads.Count)];
                    currentState.ad3 = currentState.thirdAd.name;

                    if (ads.Count > 0)
                    {
                        currentState.fourthAd = ads[Toolbox.Instance.Rand(0, ads.Count)];
                        currentState.ad4 = currentState.fourthAd.name;
                    }
                }
            }
        }

        UpdateText(true);
    }
}
