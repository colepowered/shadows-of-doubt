using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NewspaperDisplayController : MonoBehaviour
{
    [Header("Components")]
    public TextMeshProUGUI newspaperTitleText;
    public TextMeshProUGUI newspaperDateText;
    [Space(7)]
    public TextMeshProUGUI mainArticleHeadline;
    public TextMeshProUGUI mainArticleColumn1;
    public TextMeshProUGUI mainArticleColumn2;
    public TextMeshProUGUI mainArticleColumn3;
    [Space(7)]
    public TextMeshProUGUI article2Headline;
    public TextMeshProUGUI article2Column1;
    public TextMeshProUGUI article2Column2;
    public TextMeshProUGUI article2Column3;
    [Space(7)]
    public TextMeshProUGUI article3Headline;
    public TextMeshProUGUI article3Column1;
    public TextMeshProUGUI article3Column2;
    public TextMeshProUGUI article3Column3;
    [Space(7)]
    public TextMeshProUGUI ad1Text;
    public TextMeshProUGUI ad2Text;
    public TextMeshProUGUI ad3Text;
    public TextMeshProUGUI ad4Text;

    private void Start()
    {
        NewspaperController.Instance.UpdateNewspaperReferences(this);
    }
}
