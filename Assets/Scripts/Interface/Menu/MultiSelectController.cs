﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NaughtyAttributes;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class MultiSelectController : MonoBehaviour
{
    [Header("Components")]
    [ReorderableList]
    public List<MultiSelectValue> optionButtons = new List<MultiSelectValue>();

    [Header("State")]
    public string playerPrefsID;
    public int chosenIndex = 0;

    //Events
    public delegate void Select();
    public event Select OnSelect;

    [System.Serializable]
    public class MultiSelectValue
    {
        public ButtonController button;
        public Color colourValue;
        public InterfaceControls.EvidenceColours evidenceColour;
    }

    private void Start()
    {
        SetChosen(chosenIndex);
    }

    public void SetChosen(int newIndex)
    {
        chosenIndex = Mathf.Clamp(newIndex, 0, optionButtons.Count - 1);
        Game.Log("Menu: New chosen multi-select: " + chosenIndex);

        //Highlight correct
        for (int i = 0; i < optionButtons.Count; i++)
        {
            if(i == chosenIndex)
            {
                optionButtons[i].button.icon.enabled = true;
            }
            else
            {
                optionButtons[i].button.icon.enabled = false;
            }
        }

        OnValueChanged();
    }

    public Color GetCurrentSelectedColourValue()
    {
        Color ret = Color.clear;

        try
        {
            ret = optionButtons[chosenIndex].colourValue;
        }
        catch
        {

        }

        return ret;
    }

    public InterfaceControls.EvidenceColours GetCurrentSelectedEvidenceColourValue()
    {
        InterfaceControls.EvidenceColours ret = InterfaceControls.EvidenceColours.red;

        try
        {
            ret = optionButtons[chosenIndex].evidenceColour;
        }
        catch
        {

        }

        return ret;
    }

    public void OnValueChanged()
    {
        if(OnSelect != null)
        {
            OnSelect();
        }

        if (playerPrefsID != null && playerPrefsID.Length > 0)
        {
            PlayerPrefsController.Instance.OnToggleChanged(playerPrefsID, true, this);
        }
    }
}
