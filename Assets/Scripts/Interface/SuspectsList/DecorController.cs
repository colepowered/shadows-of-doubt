﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using TMPro;
using UnityEngine.UI;

public class DecorController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public WindowContentController wcc;
    public RectTransform entryParent;
    public ButtonController wallsButton;
    public ButtonController ceilingButton;
    public ButtonController floorButton;

    public GameObject decorElementPrefab;

    [Header("State")]
    public MaterialGroupPreset.MaterialType decorType = MaterialGroupPreset.MaterialType.walls;
    //public MaterialGroupPreset selectedMaterial;
    public bool isSetup = false;
    public NewRoom room;
    //public InfoWindow materialKeyWindow;
    public MaterialKeyController keyController;

    public TMP_InputField searchInputField;
    public List<DecorElementController> spawnedEntries = new List<DecorElementController>();

    public void Setup(WindowContentController newContentController)
    {
        rect = this.gameObject.GetComponent<RectTransform>();
        wcc = newContentController;

        room = Player.Instance.currentRoom;

        Game.Log("Decor: Starting decor controller for : " + decorType.ToString());

        SetPageSize(new Vector2(640, 748));

        //Update list
        SetDecorType((int)PlayerApartmentController.Instance.rememberDecorType);

        SetSelected(null);

        isSetup = true;
    }

    public void SetDecorType(int newType)
    {
        decorType = (MaterialGroupPreset.MaterialType)newType;
        UpdateListDisplay();
    }

    public void SetPageSize(Vector2 newSize)
    {
        rect.sizeDelta = newSize;
        wcc.normalSize = rect.sizeDelta;
    }

    //Update facts list
    public void UpdateListDisplay()
    {
        //Force deselect
        if (InterfaceController.Instance.selectedElement != null) InterfaceController.Instance.selectedElement.OnDeselect();

        if(decorType == MaterialGroupPreset.MaterialType.walls)
        {
            wallsButton.SetInteractable(false);
            floorButton.SetInteractable(true);
            ceilingButton.SetInteractable(true);
        }
        else if(decorType == MaterialGroupPreset.MaterialType.floor)
        {
            wallsButton.SetInteractable(true);
            floorButton.SetInteractable(false);
            ceilingButton.SetInteractable(true);
        }
        else if(decorType == MaterialGroupPreset.MaterialType.ceiling)
        {
            wallsButton.SetInteractable(true);
            floorButton.SetInteractable(true);
            ceilingButton.SetInteractable(false);
        }

        rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, 0);

        //Remove all if switched tabs
        while (spawnedEntries.Count > 0)
        {
            Destroy(spawnedEntries[0].gameObject);
            spawnedEntries.RemoveAt(0);
        }

        //Compile a list of file icons that should exist
        List<MaterialGroupPreset> allRelevant = Toolbox.Instance.allMaterialGroups.FindAll(item => item.purchasable && item.materialType == decorType);
        List<MaterialGroupPreset> required = new List<MaterialGroupPreset>();

        foreach (MaterialGroupPreset h in allRelevant)
        {
            if (required.Contains(h)) continue;

            if (searchInputField.text.Length <= 0)
            {
                required.Add(h);
            }
            else
            {
                string realText = Strings.Get("evidence.names", h.name);

                if (realText.ToLower().Contains(searchInputField.text.ToLower()))
                {
                    required.Add(h);
                }
            }
        }

        //Sort required by price
        required.Sort((p1, p2) => p1.price.CompareTo(p2.price));

        //Go through existing list
        for (int i = 0; i < spawnedEntries.Count; i++)
        {
            DecorElementController sec = spawnedEntries[i];

            //If a null entry, this has obviously been destroyed- remove entry
            if (sec == null)
            {
                spawnedEntries.RemoveAt(i);
                i--;
                continue;
            }

            if(required.Contains(sec.preset))
            {
                required.Remove(sec.preset);
                sec.VisualUpdate();
            }
            else
            {
                Destroy(sec);
                spawnedEntries.RemoveAt(i);
                i--;
                continue;
            }
        }

        //The ones left in the found files list are buttons to spawn
        foreach (MaterialGroupPreset sec in required)
        {
            GameObject newButton = null;

            newButton = Instantiate(decorElementPrefab, entryParent);

            DecorElementController newEntry = newButton.GetComponent<DecorElementController>();

            newEntry.Setup(sec, this, wcc.window);
            spawnedEntries.Add(newEntry);
        }

        for (int i = 0; i < spawnedEntries.Count; i++)
        {
            spawnedEntries[i].transform.SetAsLastSibling();
        }

        //Update rect sizes...
        entryParent.sizeDelta = new Vector2(entryParent.sizeDelta.x, Mathf.Max(spawnedEntries.Count * 130f + 24f, 466));
        //SetPageSize(new Vector2(rect.sizeDelta.x, entryParent.sizeDelta.y + 500f));
    }

    public void ClearSearchButton()
    {
        searchInputField.text = string.Empty;
    }

    public void SetSelected(MaterialGroupPreset newSelection)
    {
        if (newSelection != null)
        {
            Game.Log("Decor: Set selected material: " + newSelection);

            if (PlayerApartmentController.Instance.decoratingMode)
            {
                PlayerApartmentController.Instance.SetDecoratingMode(false, null);
            }

            //Pick a key based on existing decor...
            Toolbox.MaterialKey useKey = room.defaultWallKey;
            if (decorType == MaterialGroupPreset.MaterialType.floor) useKey = room.floorMatKey;
            else if (decorType == MaterialGroupPreset.MaterialType.ceiling) useKey = room.ceilingMatKey;

            PlayerApartmentController.Instance.SetDecoratingMode(true, newSelection, decorType, useKey, room);

            //Open a material key window so we can pick the material
            PlayerApartmentController.Instance.OpenOrUpdateMaterialWindow(null, useKey, newSelection);

            wcc.window.CloseWindow(false); //Close this
        }
    }

    private void OnDestroy()
    {
        PlayerApartmentController.Instance.rememberDecorType = decorType;

        //Resume the game if this is closed without placing anything
        if (!PlayerApartmentController.Instance.furniturePlacementMode && !PlayerApartmentController.Instance.decoratingMode)
        {
            SessionData.Instance.ResumeGame();
        }
    }
}
