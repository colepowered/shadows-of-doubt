﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;
using System;
using Newtonsoft.Json.Bson;

public class NewDoor : MonoBehaviour
{
    [Header("Location")]
    [Tooltip("This will remain static: Always the parent wall")]
    public NewWall wall;
    [Tooltip("The door will always be parented to the room the player is in (or can see).")]
    public NewRoom playerRoom;
    [Tooltip("This will change with the above, the wall this is currently parented to")]
    public NewWall parentedWall;

    [Header("Details")]
    public DoorPairPreset doorPairPreset;
    public DoorPreset preset;

    [Header("Spawned Objects")]
    public GameObject spawnedDoor;
    public List<Collider> spawnedDoorColliders = new List<Collider>();
    [System.NonSerialized]
    public Interactable doorInteractable;
    [System.NonSerialized]
    public Interactable handleInteractable;
    [System.NonSerialized]
    public Interactable peekInteractable;
    public RectTransform mapDoorObject;

    public GameObject doorSignFront;
    public GameObject doorSignRear;

    public InteractableController doorInteractableController;
    public InteractableController doorHandleInteractableController;
    public InteractableController peekInteractableController;

    public GameObject policeTape;

    [Header("Door State")]
    [Tooltip("-1 = Open, 0 = Closed, 1 = Open")]
    public float ajar = 0f; //-1 = open, 0 = closed, 1 = open
    [Tooltip("True if closed")]
    public bool isClosed = true;
    [Tooltip("True if closing (animation)")]
    public bool isClosing = false;
    [Tooltip("Updated actual ajar value that is consistent with animation")]
    public float ajarProgress = 0f;
    [Tooltip("How fast the door opens and closes")]
    public float doorOpenSpeed = 1.46f;
    [Tooltip("True if animating")]
    public bool animating = false;
    public enum DoorSetting { leaveOpen, leaveClosed };
    [Tooltip("What the AI will do when passing through the door")]
    public DoorSetting doorSetting = DoorSetting.leaveClosed;
    public enum LockSetting { keepUnlocked, keepLocked };
    [Tooltip("What the AI will do when passing through the door")]
    public LockSetting lockSetting = LockSetting.keepUnlocked;
    [Tooltip("If there are others on this list when it comes to the AI closing the door behind them, keep it open.")]
    public HashSet<Actor> usingDoorList = new HashSet<Actor>();
    [Tooltip("The door is being peeked under")]
    public bool peekedUnder = false;
    [Tooltip("True if the other side of this door would be trespassing")]
    public bool otherSideIsTrespassing = false;
    public int otherSideTrespassingEscalation = 0;
    //Keep reference to the other side room (updated when player looks @ door)
    private NewRoom playerOtherSideRoom;

    public float desiredAngle = 0f;
    [Tooltip("The maximum angle for an open door. This should be less than 90 if you don't want it to clip close walls")]
    public float openAngle = 89.9f; //Maximum angle for the door to open
    [Tooltip("Is this door locked?")]
    public bool isLocked = false;
    [Tooltip("Is this door jammed with a door wedge?")]
    public bool isJammed = false;
    [NonSerialized]
    public Interactable doorWedge; //Reference to the current door wedge
    [Tooltip("Is this door marked as forbidden for public?")]
    public bool forbiddenForPublic = false; //Forbidden for public eg crime scene
    [Tooltip("Does the player know the status of the lock?")]
    public bool knowLockStatus = false;
    [Tooltip("Knock attempt count for the player")]
    public bool knockingInProgress = false; //True while knocking coroutine in active
    [Tooltip("True if this features a neon sign")]
    public bool featuresNeonSign = false;

    [Tooltip("Lock interactable")]
    [System.NonSerialized]
    public Interactable lockInteractableFront;
    [System.NonSerialized]
    public Interactable lockInteractableRear;
    public NewRoom passwordDoorsRoom;

    public enum CitizenPassResult { success, isLocked, isJammed, isForbidden};

    //Audio
    private AudioController.LoopingSoundInfo lockpickLoop;
    public List<NewNode> bothNodesForAudioSource = new List<NewNode>(); //Doors are special in that we need any sounds to eminate from both sides of the door (otherwise the occlusion system gets confused!)

    [Header("Debug")]
    public List<string> passwordPlacementDebug = new List<string>();
    public List<string> isLockedDebug = new List<string>();

    public void Setup(NewWall newParent)
    {
        wall = newParent;
        doorPairPreset = newParent.preset;

        //Get the preset
        GetPreset();

        //Set door strength & lock strength (only if creating new city)
        if(SessionData.Instance.isFloorEdit || CityConstructor.Instance.generateNew)
        {
            string seed = this.transform.position.ToString();
            wall.SetDoorStrengthBase(Toolbox.Instance.GetPsuedoRandomNumberContained(preset.doorStrengthRange.x, preset.doorStrengthRange.y, seed, out seed));
            wall.SetLockStrengthBase(Toolbox.Instance.GetPsuedoRandomNumberContained(preset.lockStrengthRange.x, preset.lockStrengthRange.y, seed, out seed));
        }

        //Copy parameters from preset
        doorOpenSpeed = preset.doorOpenSpeed;
        openAngle = preset.openAngle;

        //Door setting is the maximum between the 2 rooms
        doorSetting = (DoorSetting)Mathf.Max((int)wall.node.room.roomType.doorSetting, (int)wall.otherWall.node.room.roomType.doorSetting);

        //Start open?
        SetOpen(ajar, null, true);

        //Setup know lock status
        SetKnowLockedStatus(false);

        //Add to door directory (BOTH SIDES!)
        if (!CityData.Instance.doorDictionary.ContainsKey(wall.id))
        {
            CityData.Instance.doorDictionary.Add(wall.id, this);
        }
        else CityData.Instance.doorDictionary[wall.id] = this;

        if (!CityData.Instance.doorDictionary.ContainsKey(wall.otherWall.id))
        {
            CityData.Instance.doorDictionary.Add(wall.otherWall.id, this);
        }
        else CityData.Instance.doorDictionary[wall.otherWall.id] = this;

        ParentToRoom(newParent.node.room);

        //Setup interactables
        doorInteractable = InteractableCreator.Instance.CreateTransformInteractable(preset.objectPreset, this.transform, null, newParent.node.room.gameLocation.evidenceEntry, Vector3.zero, Vector3.zero, null);
        doorInteractable.SetPolymorphicReference(this);
        wall.otherWall.node.AddInteractable(doorInteractable); //Add interactable reference to both sides of the door

        if(preset.handlePreset != null)
        {
            handleInteractable = InteractableCreator.Instance.CreateTransformInteractable(preset.handlePreset, this.transform, null, newParent.node.room.gameLocation.evidenceEntry, Vector3.zero, Vector3.zero, null);
            handleInteractable.SetPolymorphicReference(this);
            wall.otherWall.node.AddInteractable(handleInteractable); //Add interactable reference to both sides of the door
        }

        //Setup peek under door
        if (preset.canPeakUnderneath)
        {
            peekInteractable = InteractableCreator.Instance.CreateTransformInteractable(PrefabControls.Instance.peekInteractable, this.transform, null, newParent.node.room.gameLocation.evidenceEntry, Vector3.zero, Vector3.zero, null);
            peekInteractable.SetPolymorphicReference(this);
            wall.otherWall.node.AddInteractable(peekInteractable); //Add interactable reference to both sides of the door
            peekInteractable.SetSwitchState(!isClosed, null);

            //If this door has no lock then always set know lock status to false
            if(preset.lockType == DoorPreset.LockType.key)
            {
                peekInteractable.SetCustomState1(knowLockStatus, null);
            }
            else peekInteractable.SetCustomState1(false, null);

            if (preset.lockType != DoorPreset.LockType.none)
            {
                peekInteractable.SetLockedState(isLocked, null);
            }
            else peekInteractable.SetLockedState(false, null);
        }

        //Update switch state
        //Switch state = isClosed, custom state1 = knowlockedstate, lockedstate = islocked, customstate3 = haskey
        doorInteractable.SetSwitchState(!isClosed, null, false);
        if(handleInteractable != null) handleInteractable.SetSwitchState(!isClosed, null, false);

        //If this door has no lock then always set know lock status to false
        if (preset.lockType == DoorPreset.LockType.key)
        {
            doorInteractable.SetCustomState1(knowLockStatus, null, false);
            if (handleInteractable != null) handleInteractable.SetCustomState1(knowLockStatus, null, false);
        }
        else
        {
            doorInteractable.SetCustomState1(false, null, false);
            if (handleInteractable != null) handleInteractable.SetCustomState1(false, null, false);
        }

        if (preset.lockType != DoorPreset.LockType.none)
        {
            doorInteractable.SetLockedState(isLocked, null, false);
            if (handleInteractable != null) handleInteractable.SetLockedState(isLocked, null, false);
        }
        else
        {
            doorInteractable.SetLockedState(false, null, false);
            if (handleInteractable != null) handleInteractable.SetLockedState(false, null, false);
        }

        if (Player.Instance.keyring.Contains(this))
        {
            doorInteractable.SetCustomState3(true, null, false);
            if (handleInteractable != null) handleInteractable.SetCustomState3(true, null, false);
            if(peekInteractable != null ) peekInteractable.SetCustomState3(true, null, false);
        }
        else
        {
            doorInteractable.SetCustomState3(false, null, false);
            if (handleInteractable != null) handleInteractable.SetCustomState3(false, null, false);
            if (peekInteractable != null) peekInteractable.SetCustomState3(false, null, false);
        }

        //Set keeped locked by default
        bool lockedByDefault = GetDefaultLockState();

        if(lockedByDefault)
        {
            lockSetting = LockSetting.keepLocked;
            SetLocked(true, null, false);
        }
        else
        {
            lockSetting = LockSetting.keepUnlocked;
            SetLocked(false, null, false);
        }

        //Setup additional audio sources
        bothNodesForAudioSource.Add(wall.node);
        bothNodesForAudioSource.Add(wall.otherWall.node);

        //Setup sound event overrides based on preset
        doorInteractable.actionAudioEventOverrides.Add(RoutineControls.Instance.openDoor, preset.audioOpen);
        doorInteractable.actionAudioEventOverrides.Add(RoutineControls.Instance.closeDoor, preset.audioClose);
        doorInteractable.actionAudioEventOverrides.Add(RoutineControls.Instance.knockOnDoor, preset.audioKnockLight);

        if (handleInteractable != null)
        {
            handleInteractable.actionAudioEventOverrides.Add(RoutineControls.Instance.openDoor, preset.audioOpen);
            handleInteractable.actionAudioEventOverrides.Add(RoutineControls.Instance.closeDoor, preset.audioClose);
        }

        //Setup lock
        if(preset.lockType == DoorPreset.LockType.keypad)
        {
            if(preset.lockInteractable != null)
            {
                lockInteractableFront = InteractableCreator.Instance.CreateTransformInteractable(preset.lockInteractable, this.transform, null, null, Vector3.zero, Vector3.zero, null);
                lockInteractableFront.thisDoor = doorInteractable;
                lockInteractableFront.SetLockedState(isLocked, null);

                lockInteractableRear = InteractableCreator.Instance.CreateTransformInteractable(preset.lockInteractable, this.transform, null, null, Vector3.zero, Vector3.zero, null);
                lockInteractableRear.thisDoor = doorInteractable;
                lockInteractableRear.SetLockedState(isLocked, null);

                if(passwordDoorsRoom.preset.preferredPassword == RoomConfiguration.RoomPasswordPreference.interactableBelongsTo && passwordDoorsRoom.belongsTo != null && passwordDoorsRoom.belongsTo.Count > 0)
                {
                    lockInteractableFront.SetPasswordSource(passwordDoorsRoom.belongsTo[0]);
                    lockInteractableRear.SetPasswordSource(passwordDoorsRoom.belongsTo[0]);
                }
                else if(passwordDoorsRoom.preset.preferredPassword == RoomConfiguration.RoomPasswordPreference.thisRoom)
                {
                    lockInteractableFront.SetPasswordSource(passwordDoorsRoom);
                    lockInteractableRear.SetPasswordSource(passwordDoorsRoom);
                }
                else if (passwordDoorsRoom.preset.preferredPassword == RoomConfiguration.RoomPasswordPreference.thisAddress)
                {
                    lockInteractableFront.SetPasswordSource(passwordDoorsRoom.gameLocation);
                    lockInteractableRear.SetPasswordSource(passwordDoorsRoom.gameLocation);
                }
            }
        }
    }

    //Only run this after furniture is located
    public void PlaceKeys()
    {
        if (preset.lockType == DoorPreset.LockType.key)
        {
            //Game.Log("Placing key for " + passwordDoorsRoom.gameLocation.name + "...");

            //Only place keys on new map
            if (CityConstructor.Instance != null && CityConstructor.Instance.generateNew)
            {
                if(passwordDoorsRoom == null)
                {
                    Game.Log("Misc Error: No room for door " + wall.id + ". This could mean a null room has a door with a lock. Cannot place door key...");
                    return;
                }

                if (passwordDoorsRoom.preset.placeKey.Count > 0)
                {
                    List<NewGameLocation> posLocations = new List<NewGameLocation>();
                    List<Human> correspondingBelongsTo = new List<Human>();

                    foreach (RoomConfiguration.KeyPlacement keyPlace in passwordDoorsRoom.preset.placeKey)
                    {
                        if (keyPlace == RoomConfiguration.KeyPlacement.thisAddress)
                        {
                            //Only place a key if there are owners to this property...
                            if(passwordDoorsRoom.gameLocation.thisAsAddress.owners.Count > 0)
                            {
                                if(!passwordDoorsRoom.gameLocation.placedKey)
                                {
                                    Human owner = passwordDoorsRoom.gameLocation.thisAsAddress.owners[Toolbox.Instance.SeedRand(0, passwordDoorsRoom.gameLocation.thisAsAddress.owners.Count)];

                                    posLocations.Add(passwordDoorsRoom.gameLocation);
                                    correspondingBelongsTo.Add(owner);
                                }
                            }
                        }
                        else if (keyPlace == RoomConfiguration.KeyPlacement.belongsToHome || keyPlace == RoomConfiguration.KeyPlacement.belongsToWork)
                        {
                            foreach (Human human in passwordDoorsRoom.belongsTo)
                            {
                                if (keyPlace == RoomConfiguration.KeyPlacement.belongsToHome && human.home != null)
                                {
                                    posLocations.Add(human.home);
                                    correspondingBelongsTo.Add(human);
                                }
                                else if (keyPlace == RoomConfiguration.KeyPlacement.belongsToWork)
                                {
                                    if (human.job != null && human.job.employer != null)
                                    {
                                        posLocations.Add(human.job.employer.address);
                                        correspondingBelongsTo.Add(human);
                                    }
                                }
                            }
                        }
                    }

                    if (posLocations.Count > 0)
                    {
                        int chosenIndex = Toolbox.Instance.SeedRand(0, posLocations.Count);
                        NewGameLocation chosenLoc = posLocations[chosenIndex];
                        chosenLoc.PlaceObject(InteriorControls.Instance.key, correspondingBelongsTo[chosenIndex], correspondingBelongsTo[chosenIndex], null, out _, true, Interactable.PassedVarType.roomID, passwordDoorsRoom.roomID, true, 1, passwordDoorsRoom.preset.keyOwnershipPlacement, 1);
                        chosenLoc.placedKey = true; //Limit key to x1
                    }
                }
            }
        }
    }

    //Select the door preset based on room configurations
    private void GetPreset()
    {
        if (wall.node.room.preset == null || wall.otherWall.node.room.preset == null)
        {
            preset = InteriorControls.Instance.defaultDoor;
            return;
        }

        //Get password rooms...
        if(wall.node.room.preset.passwordPriority > wall.otherWall.node.room.preset.passwordPriority)
        {
            passwordDoorsRoom = wall.node.room;
        }
        else
        {
            passwordDoorsRoom = wall.otherWall.node.room;
        }

        //This borders outside
        if (wall.node.gameLocation.thisAsAddress == null || wall.otherWall.node.gameLocation.thisAsAddress == null)
        {
            //If both feature a door, pick the highest priority
            if(wall.node.room.preset.exteriorDoor != null && wall.otherWall.node.room.preset.exteriorDoor != null)
            {
                if (wall.node.room.roomType.doorPriority >= wall.otherWall.node.room.roomType.doorPriority)
                {
                    preset = wall.node.room.preset.exteriorDoor;
                    //passwordDoorsRoom = wall.node.room;
                    return;
                }
                else
                {
                    preset = wall.otherWall.node.room.preset.exteriorDoor;
                    //passwordDoorsRoom = wall.otherWall.node.room;
                    return;
                }
            }
            else if(wall.node.room.preset.exteriorDoor != null)
            {
                preset = wall.node.room.preset.exteriorDoor;
                //passwordDoorsRoom = wall.node.room;
                return;
            }
            else if(wall.otherWall.node.room.preset.exteriorDoor != null)
            {
                preset = wall.otherWall.node.room.preset.exteriorDoor;
                //passwordDoorsRoom = wall.otherWall.node.room;
                return;
            }
        }

        //This borders a different gamelocation
        if (wall.node.gameLocation != wall.otherWall.node.gameLocation)
        {
            //If both feature a door, pick the highest priority
            if (wall.node.room.preset.addressDoor != null && wall.otherWall.node.room.preset.addressDoor != null)
            {
                if (wall.node.room.roomType.doorPriority >= wall.otherWall.node.room.roomType.doorPriority)
                {
                    preset = wall.node.room.preset.addressDoor;
                    //passwordDoorsRoom = wall.node.room;
                    return;
                }
                else
                {
                    preset = wall.otherWall.node.room.preset.addressDoor;
                    //passwordDoorsRoom = wall.otherWall.node.room;
                    return;
                }
            }
            else if (wall.node.room.preset.addressDoor != null)
            {
                preset = wall.node.room.preset.addressDoor;
                //passwordDoorsRoom = wall.node.room;
                return;
            }
            else if (wall.otherWall.node.room.preset.addressDoor != null)
            {
                preset = wall.otherWall.node.room.preset.addressDoor;
                //passwordDoorsRoom = wall.otherWall.node.room;
                return;
            }
        }

        //Use internal doors
        //If both feature a door, pick the highest priority
        if (wall.node.room.preset.internalDoor != null && wall.otherWall.node.room.preset.internalDoor != null)
        {
            if (wall.node.room.roomType.doorPriority >= wall.otherWall.node.room.roomType.doorPriority)
            {
                preset = wall.node.room.preset.internalDoor;
                //passwordDoorsRoom = wall.node.room;
                return;
            }
            else
            {
                preset = wall.otherWall.node.room.preset.internalDoor;
                //passwordDoorsRoom = wall.otherWall.node.room;
                return;
            }
        }
        else if (wall.node.room.preset.internalDoor != null)
        {
            preset = wall.node.room.preset.internalDoor;
            //passwordDoorsRoom = wall.node.room;
            return;
        }
        else if (wall.otherWall.node.room.preset.internalDoor != null)
        {
            preset = wall.otherWall.node.room.preset.internalDoor;
            //passwordDoorsRoom = wall.otherWall.node.room;
            return;
        }

        if(preset == null) preset = InteriorControls.Instance.defaultDoor;
    }

    public void SelectColouring(bool overrideWithKey = false, Toolbox.MaterialKey keyOverride = new Toolbox.MaterialKey())
    {
        //Get material
        if (preset.inheritColouringFromDecor && preset.variations.Count > 0 && !wall.foundDoorMaterialKey)
        {
            wall.foundDoorMaterialKey = false;

            //Share with other door keys in this address
            if (preset.shareColours != FurniturePreset.ShareColours.none)
            {
                //Find other with this color sharing designation...
                NewDoor other = null;

                //Scan entire building...
                foreach(KeyValuePair<int, NewFloor> pair in wall.node.room.building.floors)
                {
                    foreach(NewAddress ad in pair.Value.addresses)
                    {
                        foreach (NewRoom r in ad.rooms)
                        {
                            foreach (NewNode n in r.nodes)
                            {
                                foreach (NewWall w in n.walls)
                                {
                                    if (w.door != null && w != w.otherWall && w.foundDoorMaterialKey && w.door.preset.shareColours == preset.shareColours)
                                    {
                                        //Game.Log("Found door colour from other door in same building: " + wall.room.gameLocation.building.name);

                                        other = w.door;

                                        wall.doorMatKey = other.wall.doorMatKey;
                                        wall.otherWall.doorMatKey = other.wall.doorMatKey;

                                        wall.foundDoorMaterialKey = true;
                                        wall.otherWall.foundDoorMaterialKey = true;
                                        break;
                                    }
                                }

                                if (other != null) break;
                            }

                            if (other != null) break;
                        }

                        if (other != null) break;
                    }

                    if (other != null) break;
                }
            }

            //Create a new key
            if (!wall.foundDoorMaterialKey)
            {
                //Game.Log("Unable to find door colour from other door in same building: " + wall.room.gameLocation.building.name + " so generating new one...");

                ColourSchemePreset scheme = wall.node.room.colourScheme;
                if (scheme == null && wall.node.room.building != null) scheme = wall.node.room.building.colourScheme;

                wall.doorMatKey = MaterialsController.Instance.GenerateMaterialKey(preset.variations[Toolbox.Instance.GetPsuedoRandomNumber(0, preset.variations.Count, wall.node.nodeCoord.ToString())], scheme, wall.node.room, true);
                wall.otherWall.doorMatKey = wall.doorMatKey;

                wall.foundDoorMaterialKey = true;
                wall.otherWall.foundDoorMaterialKey = true;
            }
        }

        Toolbox.MaterialKey key = wall.doorMatKey;
        if (overrideWithKey) key = keyOverride;

        //If spawned, set colours
        //Use this method of comparison to be compatible with multithreading
        if (System.Object.ReferenceEquals(spawnedDoor, null) && wall.foundDoorMaterialKey)
        {
            //Apply material
            MaterialsController.Instance.ApplyMaterialKey(spawnedDoor, key);
        }
    }

    public void SpawnDoor()
    {
        if (spawnedDoor != null) return;

        //Spawn door
        spawnedDoor = Instantiate(preset.doorModel, this.transform);
        spawnedDoor.transform.localPosition = parentedWall.preset.doorOffset;
        spawnedDoorColliders = spawnedDoor.GetComponentsInChildren<Collider>().ToList();
        doorInteractableController = spawnedDoor.GetComponent<InteractableController>();
        doorInteractableController.isDoor = this;
        doorInteractableController.Setup(doorInteractable);

        //Toolbox.Instance.SetLightLayer(spawnedDoor, wall.node.building);
        spawnedDoor.transform.localRotation = Quaternion.Euler(0, desiredAngle, 0); //Set desired angle

        //Set map icon
        if (mapDoorObject != null)
        {
            mapDoorObject.localEulerAngles = new Vector3(0, 0, -desiredAngle);
        }

        //If spawned, set colours
        if (wall.foundDoorMaterialKey)
        {
            //Apply material
            MaterialsController.Instance.ApplyMaterialKey(spawnedDoor, wall.doorMatKey);
        }

        //Spawn handle
        if (handleInteractable != null)
        {
            GameObject handleObj = Instantiate(preset.handleModel, spawnedDoor.transform);
            doorHandleInteractableController = handleObj.GetComponent<InteractableController>();
            doorHandleInteractableController.isDoor = this;
            doorHandleInteractableController.Setup(handleInteractable);
            //Toolbox.Instance.SetLightLayer(handleObj, wall.node.building);
        }

        //Spawn signs
        if(doorSignFront == null && preset.doorSigns.Count > 0)
        {
            List<GameObject> signPool = new List<GameObject>();

            NewNode behindNode = GetBehindNode();
            NewNode infontNode = GetInfontNode();

            if (behindNode != null)
            {
                foreach(DoorPreset.DoorSign s in preset.doorSigns)
                {
                    if (s == null) continue;

                    try
                    {
                        if (s.ifEntranceToRoom.Contains(behindNode.room.preset))
                        {
                            if(!s.placeIfFromOutside || infontNode.isOutside)
                            {
                                if(!s.placeIfFromInside || !infontNode.isOutside)
                                {
                                    if (!s.placeIfFromPublicArea ||
                                        (infontNode.room.preset.forbidden == RoomConfiguration.Forbidden.alwaysAllowed
                                        || infontNode.room.preset.forbidden == RoomConfiguration.Forbidden.allowedDuringOpenHours))
                                    {
                                        signPool.AddRange(s.signagePool);
                                    }
                                }
                            }
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }
            }


            if(signPool.Count > 0)
            {
                GameObject pickSign = signPool[Toolbox.Instance.GetPsuedoRandomNumber(0, signPool.Count, CityData.Instance.seed + preset.name)];

                doorSignFront = Instantiate(pickSign, spawnedDoor.transform);
                doorSignFront.transform.localEulerAngles = new Vector3(0, 180, 0);
                doorSignFront.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                doorSignFront.transform.localPosition = preset.doorSignOffset;

                OpenSignController openSign = doorSignFront.GetComponent<OpenSignController>();

                if(openSign != null)
                {
                    if(doorInteractableController.switchSyncObjects != null) doorInteractableController.switchSyncObjects.Add(openSign);
                    doorInteractableController.enableSwitchSync = true;
                    doorInteractableController.UpdateSwitchSync();
                    featuresNeonSign = true;
                }

                ApartmentNumberController apc = doorSignFront.GetComponent<ApartmentNumberController>();

                if (apc != null)
                {
                    apc.Setup(behindNode, this);
                }
            }
        }

        if (doorSignRear == null && preset.doorSigns.Count > 0)
        {
            List<GameObject> signPool = new List<GameObject>();

            NewNode infontNode = GetInfontNode();
            NewNode behindNode = GetBehindNode();

            if (infontNode != null)
            {
                foreach (DoorPreset.DoorSign s in preset.doorSigns)
                {
                    if (s == null) continue;

                    try
                    {
                        if (s.ifEntranceToRoom.Contains(infontNode.room.preset))
                        {
                            if(!s.placeIfFromOutside || behindNode.isOutside)
                            {
                                if(!s.placeIfFromInside || !behindNode.isOutside)
                                {
                                    if (!s.placeIfFromPublicArea ||
                                        (behindNode.room.preset.forbidden == RoomConfiguration.Forbidden.alwaysAllowed
                                        || behindNode.room.preset.forbidden == RoomConfiguration.Forbidden.allowedDuringOpenHours))
                                    {
                                        signPool.AddRange(s.signagePool);
                                    }
                                }
                            }
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }
            }

            if (signPool.Count > 0)
            {
                GameObject pickSign = signPool[Toolbox.Instance.GetPsuedoRandomNumber(0, signPool.Count, CityData.Instance.seed + preset.name)];

                doorSignRear = Instantiate(pickSign, spawnedDoor.transform);
                doorSignRear.transform.localEulerAngles = new Vector3(0, 0, 0);
                doorSignRear.transform.localScale = new Vector3(-0.5f, 0.5f, 0.5f);
                doorSignRear.transform.localPosition = new Vector3(preset.doorSignOffset.x, preset.doorSignOffset.y, -preset.doorSignOffset.z - 0.1f);

                OpenSignController openSign = doorSignRear.GetComponent<OpenSignController>();

                if (openSign != null)
                {
                    if (doorInteractableController.switchSyncObjects != null) doorInteractableController.switchSyncObjects.Add(openSign);
                    doorInteractableController.enableSwitchSync = true;
                    doorInteractableController.UpdateSwitchSync();
                    featuresNeonSign = true;
                }

                ApartmentNumberController apc = doorSignRear.GetComponent<ApartmentNumberController>();

                if(apc != null)
                {
                    apc.Setup(infontNode, this);
                }
            }
        }

        //Setup peek under door
        if (preset.canPeakUnderneath)
        {
            GameObject newPeek = Instantiate(PrefabControls.Instance.peekUnderDoor, spawnedDoor.transform);
            peekInteractableController = newPeek.GetComponent<InteractableController>();
            peekInteractableController.isDoor = this;
            peekInteractableController.Setup(peekInteractable);
        }

        //Set lock
        if (lockInteractableFront != null)
        {
            lockInteractableFront.spawnedObject = Toolbox.Instance.SpawnObject(lockInteractableFront.preset.prefab, spawnedDoor.transform);
            lockInteractableFront.spawnedObject.transform.localPosition = preset.lockOffsetFront;
            InteractableController lockController = lockInteractableFront.spawnedObject.GetComponent<InteractableController>();
            lockController.Setup(lockInteractableFront);

            if (lockInteractableFront.preset.useMaterialChanges)
            {
                if (lockInteractableFront.locked && lockInteractableFront.preset.lockOnMaterial != null)
                {
                    lockInteractableFront.spawnedObject.GetComponent<MeshRenderer>().material = lockInteractableFront.preset.lockOnMaterial;
                    //Game.Log("Material changed to " + lockInteractableFront.preset.lockOnMaterial);
                }
                else if (!lockInteractableFront.locked && lockInteractableFront.preset.lockOffMaterial != null)
                {
                    lockInteractableFront.spawnedObject.GetComponent<MeshRenderer>().material = lockInteractableFront.preset.lockOffMaterial;
                    //Game.Log("Material changed to " + lockInteractableFront.preset.lockOffMaterial);
                }
            }
        }

        if (lockInteractableRear != null)
        {
            lockInteractableRear.spawnedObject = Toolbox.Instance.SpawnObject(lockInteractableRear.preset.prefab, spawnedDoor.transform);
            lockInteractableRear.spawnedObject.transform.localPosition = preset.lockOffsetRear;
            lockInteractableRear.spawnedObject.transform.localEulerAngles = new Vector3(0, 180, 0);
            InteractableController lockController = lockInteractableRear.spawnedObject.GetComponent<InteractableController>();
            lockController.Setup(lockInteractableRear);

            if (lockInteractableRear.preset.useMaterialChanges)
            {
                if (lockInteractableRear.locked && lockInteractableRear.preset.lockOnMaterial != null)
                {
                    lockInteractableRear.spawnedObject.GetComponent<MeshRenderer>().material = lockInteractableRear.preset.lockOnMaterial;
                    //Game.Log("Material changed to " + lockInteractableRear.preset.lockOnMaterial);
                }
                else if (!lockInteractableRear.locked && lockInteractableRear.preset.lockOffMaterial != null)
                {
                    lockInteractableRear.spawnedObject.GetComponent<MeshRenderer>().material = lockInteractableRear.preset.lockOffMaterial;
                    //Game.Log("Material changed to " + lockInteractableRear.preset.lockOffMaterial);
                }
            }
        }

        //Set light layers: If this is an outside door, include street lighting
        uint includeStreetLighting = 0;

        if (wall.node.gameLocation.thisAsStreet != null || wall.otherWall.node.gameLocation.thisAsStreet != null)
        {
            includeStreetLighting = 2;
        }

        //Manually set light layer as this could need to include both building & street lighting
        MeshRenderer[] meshes = spawnedDoor.GetComponentsInChildren<MeshRenderer>();

        foreach (MeshRenderer mesh in meshes)
        {
            //Set layer to interior + sun (default)
            if (wall.node.building != null)
            {
                if (wall.node.building.interiorLightCullingLayer == 0)
                {
                    //References to values in LightLayerEnum;
                    // +1 to add the default light layer (sun)
                    // + 2 to include streetLighting

                    mesh.renderingLayerMask = 4 + 1 + includeStreetLighting;
                }
                else if (wall.node.building.interiorLightCullingLayer == 1)
                {
                    mesh.renderingLayerMask = 8 + 1 + includeStreetLighting;
                }
                else if (wall.node.building.interiorLightCullingLayer == 2)
                {
                    mesh.renderingLayerMask = 16 + 1 + includeStreetLighting;
                }
                else if (wall.node.building.interiorLightCullingLayer == 3)
                {
                    mesh.renderingLayerMask = 32 + 1 + includeStreetLighting;
                }
            }
        }

        //Setup interactions based on what the player currently knows...
        SetKnowLockedStatus(knowLockStatus);

        //Set name of interactable
        doorInteractable.name = name;

        //Spawn police tape
        if(forbiddenForPublic)
        {
            SpawnPoliceTape();
        }
    }

    //Name door based on player's position, and also whether entering would be trespassing
    public void UpdateNameBasedOnPlayerPosition()
    {
        playerOtherSideRoom = wall.node.room;

        if(playerOtherSideRoom == Player.Instance.currentRoom)
        {
            playerOtherSideRoom = wall.otherWall.node.room;
        }

        otherSideIsTrespassing = Player.Instance.IsTrespassing(playerOtherSideRoom, out otherSideTrespassingEscalation, out _, false);

        //Game.Log("Player: Other room is " + playerOtherSideRoom.name + " Trespassing: " + otherSideIsTrespassing);

        //if(playerOtherSideRoom.gameLocation != Player.Instance.currentGameLocation)
        //{
        //    name = playerOtherSideRoom.gameLocation.name;
        //}
        //else 
        //{
        //    name = playerOtherSideRoom.name;
        //}

        //this.transform.name = name;
        //if (doorInteractable != null) doorInteractable.name = name;
    }

    NewNode GetBehindNode()
    {
        Vector3 behind = this.transform.TransformPoint(-0.1f, 0, 1f);

        NewNode ret = null;

        PathFinder.Instance.nodeMap.TryGetValue(CityData.Instance.RealPosToNodeInt(behind), out ret);

        return ret;
    }

    NewNode GetInfontNode()
    {
        Vector3 behind = this.transform.TransformPoint(-0.1f, 0, -1f);

        NewNode ret = null;

        PathFinder.Instance.nodeMap.TryGetValue(CityData.Instance.RealPosToNodeInt(behind), out ret);

        return ret;
    }

    //Get the name of the parent room
    public string GetNameForParent()
    {
        return wall.otherWall.node.gameLocation.name;
    }

    //Get the name based on door priority
    public string GetName()
    {
        string ret = string.Empty;

        //Use the 'password doors room'
        //If this borders the street or a lobby, use the name of the password door's room address...
        if(wall.node.gameLocation.thisAsStreet != null || wall.node.gameLocation.isLobby || wall.otherWall.node.gameLocation.thisAsStreet != null || wall.otherWall.node.gameLocation.isLobby)
        {
            return passwordDoorsRoom.gameLocation.name;
        }
        //Otherwise use the room name
        else
        {
            return passwordDoorsRoom.name;
        }
    }

    //The door should always be parented to the room the player is in (or can see).
    public void ParentToRoom(NewRoom newRoom)
    {
        if(playerRoom != newRoom)
        {
            playerRoom = newRoom;

            //Maintain wall parent hierarchy
            if (wall.parentWall.node.room == newRoom)
            {
                this.transform.SetParent(wall.parentWall.node.room.transform, true);
                parentedWall = wall.parentWall;
            }
            else if (wall.childWall.node.room == newRoom)
            {
                this.transform.SetParent(wall.childWall.node.room.transform, true);
                parentedWall = wall.childWall;
            }
        }
    }

    //Triggered when know lock status is updated
    public void SetKnowLockedStatus(bool val)
    {
        knowLockStatus = val;
        //if (preset.lockType != DoorPreset.LockType.key) knowLockStatus = false; //If no lock status this is always false

        //Custom state 1 is known lock status
        if (doorInteractable != null) doorInteractable.SetCustomState1(knowLockStatus, null);
        if (handleInteractable != null) handleInteractable.SetCustomState1(knowLockStatus, null);
        if (peekInteractable != null) peekInteractable.SetCustomState1(knowLockStatus, null);

        //Update display
        if((doorInteractable != null && InteractionController.Instance.currentLookingAtInteractable == doorInteractable.controller) || (handleInteractable != null && InteractionController.Instance.currentLookingAtInteractable == handleInteractable.controller))
        {
            InteractionController.Instance.DisplayInteractionCursor(InteractionController.Instance.displayingInteraction, true); //Force update but don't change
        }
    }

    //Triggered when player acquires a key to this
    public void SetPlayerHasKey(bool val)
    {
        if (preset.lockType == DoorPreset.LockType.none) val = false; //If no lock status this is always false

        //Custom state 3 is whether player has key
        if (doorInteractable != null) doorInteractable.SetCustomState3(val, null);
        if (handleInteractable != null) handleInteractable.SetCustomState3(val, null);
        if (peekInteractable != null) peekInteractable.SetCustomState3(val, null);
    } 

    //Uses position paralel to wall to determin which way the door should open
    public void OpenByActor(Actor actor, bool forceInverseOpenDirection = false, float speedMultiplier = 1f)
    {
        //If the door is already open, opening and not closing then skip...
        if ((!isClosed && !isClosing) || (animating && !isClosing)) return;

        //If this object is not active, then open one way (doesn't matter)
        if (!this.isActiveAndEnabled)
        {
            SetOpen(1f, actor, speedMultiplier: speedMultiplier);
            return;
        }

        //Which side is the actor on?
        //Convert player world position to local position relative to the door
        Vector3 localPos = this.transform.InverseTransformPoint(actor.transform.position);

        if (localPos.z > 0)
        {
            if (!forceInverseOpenDirection) SetOpen(-1f, actor, speedMultiplier: speedMultiplier);
            else SetOpen(1f, actor, speedMultiplier: speedMultiplier);
        }
        else
        {
            if (!forceInverseOpenDirection) SetOpen(1f, actor, speedMultiplier: speedMultiplier);
            else SetOpen(-1f, actor, speedMultiplier: speedMultiplier);
        }
    }

    public void SetOpen(float newAjar, Actor actor, bool skipAnimation = false, float speedMultiplier = 1f)
    {
        //passwordPlacementDebug.Add("SetupOpen " + newAjar);

        //if(actor != null && !actor.isPlayer && newAjar == 0f)
        //{
        //    Game.Log("Doors: " + name + " close by " + actor.name);
        //}

        //Clamp ajar to 2dp
        newAjar = Mathf.RoundToInt(newAjar * 100f) / 100f;
        newAjar = Mathf.Clamp(newAjar, -1f, 1f);
        if (ajar == newAjar) return; //Only continue if different

        ajar = newAjar;
        desiredAngle = Mathf.Lerp(-openAngle, openAngle, (ajar + 1f) / 2f);

        //Trigger open immediately
        if (ajar != Mathf.Abs(0))
        {
            OnOpen();
        }

        //Skip animation
        if (skipAnimation || !this.gameObject.activeInHierarchy || spawnedDoor == null)
        {
            ajarProgress = ajar; //Set the progress to equal ajar immediately

            if (spawnedDoor != null)
            {
                spawnedDoor.transform.localRotation = Quaternion.Euler(0, desiredAngle, 0);
            }

            //Set map icon
            if (this.gameObject.activeInHierarchy && mapDoorObject != null)
            {
                mapDoorObject.localEulerAngles = new Vector3(0, 0, -desiredAngle);
            }

            //Trigger closed
            if (ajar == Mathf.Abs(0))
            {
                if (parentedWall != null && actor != null) //We will only have a null actor if the door has closed itself on disable, so no need for sfx
                {
                    AudioController.Instance.PlayWorldOneShot(preset.audioClose, actor as Human, parentedWall.node, this.transform.position, additionalSources: bothNodesForAudioSource);
                }

                OnClose(actor);
            }
        }
        else
        {
            if (actor != null && actor.ai != null && actor.animationController != null)
            {
                actor.animationController.SetArmsBoolState(CitizenAnimationController.ArmsBoolSate.armsOneShotUse);
            }

            StopAllCoroutines();
            StartCoroutine(OpenDoor(actor, speedMultiplier));

            //Trigger sound update for distance to outside...
            if (this.gameObject.activeInHierarchy && (wall.node.gameLocation.isOutside || wall.otherWall.node.gameLocation.isOutside))
            {
                AudioController.Instance.UpdateClosestWindowAndDoor();
            }
        }

        //Play open sound
        if (parentedWall != null && actor != null)
        {
            if(ajar == 0f)
            {
                //Play sound
                AudioController.Instance.PlayWorldOneShot(preset.audioCloseAction, actor, parentedWall.node, this.transform.position, additionalSources: bothNodesForAudioSource);
            }
            else
            {
                AudioController.Instance.PlayWorldOneShot(preset.audioOpen, actor, parentedWall.node, this.transform.position, additionalSources: bothNodesForAudioSource);
            }
        }

        if (doorInteractable != null) doorInteractable.SetSwitchState(!isClosed, actor, !skipAnimation);
        if (handleInteractable != null) handleInteractable.SetSwitchState(!isClosed, actor, !skipAnimation);
        if (peekInteractable != null) peekInteractable.SetSwitchState(!isClosed, actor, !skipAnimation);
    }

    private void OnEnable()
    {
        //Open door icon
        if (mapDoorObject != null && spawnedDoor != null)
        {
            mapDoorObject.localEulerAngles = new Vector3(0, 0, -spawnedDoor.transform.localEulerAngles.y);
        }
    }

    //Skip animation on disable
    private void OnDisable()
    {
        StopAllCoroutines(); //Stop corourintes
        animating = false;

        if(spawnedDoor != null)
        {
            if(spawnedDoor.transform.localEulerAngles.y != desiredAngle)
            {
                if (parentedWall != null) //We will only have a null actor if the door has closed itself on disable, so no need for sfx
                {
                    AudioController.Instance.PlayWorldOneShot(preset.audioClose, null, parentedWall.node, this.transform.position, additionalSources: bothNodesForAudioSource);
                }
            }

            spawnedDoor.transform.localRotation = Quaternion.Euler(0, desiredAngle, 0);

            //Close door icon
            if (mapDoorObject != null)
            {
                mapDoorObject.localEulerAngles = new Vector3(0, 0, -desiredAngle);
            }

            if (!SessionData.Instance.isFloorEdit)
            {
                if (ajar == Mathf.Abs(0))
                {
                    OnClose(null);
                }
            }
        }
    }

    IEnumerator OpenDoor(Actor actor, float speedMultiplier)
    {
        float angle = Mathf.Round(spawnedDoor.transform.localEulerAngles.y * 100f) /100f;
        angle = (angle > 180) ? angle - 360 : angle;

        float amountToRotate = 0f;

        float doorSpeedMultiplier = speedMultiplier;
        if (actor != null && actor.stealthMode && ajar == 0) doorSpeedMultiplier = 0.75f;
        //if (forcedEntry) doorSpeedMultiplier += 4f;

        animating = true;

        //Set closing flag
        if (ajar == Mathf.Abs(0))
        {
            isClosing = true;
        }
        else isClosing = false;

        //SFX update
        int audioUpdateTicker = 0;

        SetCollisionsWithPlayerActive(false);

        bool closeSFXPlayed = false;

        while (desiredAngle != angle)
        {
            //Amount to go
            float amountToGo = Mathf.Abs(desiredAngle - angle);

            //Ajar progress, use inverse lerp to get the value
            ajarProgress = Mathf.InverseLerp(-openAngle, openAngle, angle) * 2f - 1f;

            if (angle < desiredAngle) amountToRotate = Mathf.Min((doorOpenSpeed * 20f + (amountToGo * doorOpenSpeed * doorSpeedMultiplier)) * Time.deltaTime * SessionData.Instance.currentTimeMultiplier, amountToGo);
            else if (angle > desiredAngle) amountToRotate = -Mathf.Min((doorOpenSpeed * 20f + (amountToGo * doorOpenSpeed * doorSpeedMultiplier)) * Time.deltaTime * SessionData.Instance.currentTimeMultiplier, amountToGo);

            //Rotate
            spawnedDoor.transform.localEulerAngles = new Vector3(0, Mathf.Clamp(angle + amountToRotate, -openAngle, openAngle), 0);
            angle = Mathf.Round(spawnedDoor.transform.localEulerAngles.y * 100f) / 100f;
            angle = (angle > 180) ? angle - 360 : angle;

            //Set map icon
            if(mapDoorObject != null && mapDoorObject.gameObject.activeInHierarchy)
            {
                mapDoorObject.localEulerAngles = new Vector3(0, 0, -spawnedDoor.transform.localEulerAngles.y);
            }

            audioUpdateTicker++;

            if (audioUpdateTicker >= 2)
            {
                AudioController.Instance.UpdateClosestWindowAndDoor(true);
                audioUpdateTicker = 0;
            }

            //SFX
            if(!closeSFXPlayed)
            {
                if(desiredAngle == 0f && Mathf.Abs(ajarProgress) <= AudioDebugging.Instance.doorCloseTriggerPoint)
                {
                    if (parentedWall != null) //We will only have a null actor if the door has closed itself on disable, so no need for sfx
                    {
                        if(actor != null)
                        {
                            AudioController.Instance.PlayWorldOneShot(preset.audioClose, actor as Human, parentedWall.node, this.transform.position, additionalSources: bothNodesForAudioSource);
                        }
                        else AudioController.Instance.PlayWorldOneShot(preset.audioClose, null, parentedWall.node, this.transform.position, additionalSources: bothNodesForAudioSource);
                    }

                    closeSFXPlayed = true;
                }
            }

            //Saftey
            if (amountToGo <= 0.001f)
            {
                spawnedDoor.transform.localEulerAngles = new Vector3(0, desiredAngle, 0);
                break;
            }

            yield return null;
        }

        animating = false;
        isClosing = false;

        //Play closed sound
        if (ajar == Mathf.Abs(0))
        {
            OnClose(actor);
        }
    }

    public void OnClose(Actor actor)
    {
        //Moved this to pre-trigger as sometimes it is called before the state change (~90% of way through animation)
        //if (parentedWall != null && actor != null) //We will only have a null actor if the door has closed itself on disable, so no need for sfx
        //{
        //     AudioController.Instance.PlayWorldOneShot(preset.audioClose, actor as Human, parentedWall.node, this.transform.position, additionalSources: bothNodesForAudioSource);
        //}

        //passwordPlacementDebug.Add("OnClose");
        isClosed = true;

        SetCollisionsWithPlayerActive(true);

        if (doorInteractable != null) doorInteractable.SetSwitchState(!isClosed, actor);
        if(handleInteractable != null) handleInteractable.SetSwitchState(!isClosed, actor);
        if (peekInteractable != null) peekInteractable.SetSwitchState(!isClosed, actor);

        //Arm lock on close
        if (!isLocked && preset.armLockOnClose)
        {
            if(lockInteractableFront != null)
            {
                InteractablePreset.InteractionAction act = lockInteractableFront.preset.GetActions().Find(item => item.effectSwitchStates.Exists(item2 => item2.switchState == InteractablePreset.Switch.lockState && item2.boolIs));
                lockInteractableFront.OnInteraction(act, null);
            }

            if (lockInteractableRear != null)
            {
                InteractablePreset.InteractionAction act = lockInteractableRear.preset.GetActions().Find(item => item.effectSwitchStates.Exists(item2 => item2.switchState == InteractablePreset.Switch.lockState && item2.boolIs));
                lockInteractableRear.OnInteraction(act, null);
            }
        }

        //Update rooms
        wall.node.room.openDoors.Remove(this);
        wall.otherWall.node.room.openDoors.Remove(this);
        wall.node.room.closedDoors.Add(this);
        wall.otherWall.node.room.closedDoors.Add(this);

        //A cull update is needed if this door features in the current room's conditional door set...
        if(Player.Instance.currentRoom != null && (Player.Instance.currentRoom.doorCheckSet.Contains(wall.id)  || Player.Instance.currentRoom.doorCheckSet.Contains(wall.otherWall.id)))
        {
            //Player.Instance.UpdateCullingThisFrame();
            Player.Instance.UpdateCullingOnEndOfFrame();
        }

        if(Player.Instance.currentCityTile == wall.node.tile.cityTile)
        {
            AudioController.Instance.UpdateAmbientZonesOnEndOfFrame();
        }

        //if(doorInteractableController != null) doorInteractableController.SetSecondaryInteraction(true); //Enable knocking

        ////Update know lock interactable status
        //if(doorHandleInteractableController != null)
        //{
        //    if (knowLockStatus)
        //    {
        //        if (Player.Instance.playerKeyring.Contains(this))
        //        {
        //            doorHandleInteractableController.SetPrimaryInteraction(true); //Enable locking
        //            doorHandleInteractableController.SetSecondaryInteraction(false); //Disable lockpicking
        //        }
        //        else
        //        {
        //            doorHandleInteractableController.SetPrimaryInteraction(false); //Disable locking

        //            if (locked)
        //            {
        //                doorHandleInteractableController.SetSecondaryInteraction(true); //Enable lockpicking
        //            }
        //            else
        //            {
        //                doorHandleInteractableController.SetSecondaryInteraction(false); //Disable lockpicking
        //            }
        //        }
        //    }
        //    else
        //    {
        //        if (Player.Instance.playerKeyring.Contains(this))
        //        {
        //            doorHandleInteractableController.SetPrimaryInteraction(true); //Enable locking
        //            doorHandleInteractableController.SetSecondaryInteraction(false); //Disable lockpicking
        //        }
        //        else
        //        {
        //            doorHandleInteractableController.SetPrimaryInteraction(false); //Disable locking
        //            doorHandleInteractableController.SetSecondaryInteraction(true); //Disable lockpicking
        //        }
        //    }
        //}
    }

    //Remove collisions with player when door opens
    public void SetCollisionsWithPlayerActive(bool val)
    {
        if(val)
        {
            foreach(Collider c in spawnedDoorColliders)
            {
                c.gameObject.layer = 0;
            }
        }
        else
        {
            foreach (Collider c in spawnedDoorColliders)
            {
                c.gameObject.layer = 6;
            }
        }
    }

    public void OnOpen()
    {
        //passwordPlacementDebug.Add("OnOpen");
        isClosed = false;

        doorInteractable.SetSwitchState(!isClosed, null);
        if (handleInteractable != null) handleInteractable.SetSwitchState(!isClosed, null);
        if (peekInteractable != null) peekInteractable.SetSwitchState(!isClosed, null);

        //Update rooms
        wall.node.room.openDoors.Add(this);
        wall.otherWall.node.room.openDoors.Add(this);
        wall.node.room.closedDoors.Remove(this);
        wall.otherWall.node.room.closedDoors.Remove(this);

        SetCollisionsWithPlayerActive(true);

        //A cull update is needed if this door features in the current room's conditional door set...
        if (Player.Instance.currentRoom != null && (Player.Instance.currentRoom.doorCheckSet.Contains(wall.id) || Player.Instance.currentRoom.doorCheckSet.Contains(wall.otherWall.id)))
        {
            //Player.Instance.UpdateCullingThisFrame();
            Player.Instance.UpdateCullingOnEndOfFrame();
        }

        if (Player.Instance.currentCityTile == wall.node.tile.cityTile)
        {
            AudioController.Instance.UpdateAmbientZonesOnEndOfFrame();
        }

        //if(doorInteractableController != null) doorInteractableController.SetSecondaryInteraction(false); //Disable knocking

        //if(doorHandleInteractableController != null)
        //{
        //    doorHandleInteractableController.SetPrimaryInteraction(false); //Disable locking
        //    doorHandleInteractableController.SetSecondaryInteraction(false); //Disable lockpicking
        //}
    }

    public void SetLocked(bool val, Actor actor, bool playSound = true)
    {
        //Can't lock if there isn't one or if the door strength is zero
        if (preset.lockType == DoorPreset.LockType.none || wall.currentDoorStrength <= 0f || forbiddenForPublic)
        {
            if (isLocked) val = false;
            else return;
        }

        if (isLocked != val)
        {
            isLocked = val;

            //Detect breaking & entering
            //Trigger open immediately
            if (!isLocked)
            {
                //Player has opened this illegally
                if (actor != null)
                {
                    if (actor.isPlayer)
                    {
                        Game.Log("Doors: Set door unlocked by player");

                        ////Check both sides for an illegal area...
                        ////If a door has been closed we need to check the locations on both sides, as the door can be closed from either side
                        //for (int i = 0; i < 2; i++)
                        //{
                        //    //Has this door been closed and is it on a list of illegally opened doors?
                        //    NewRoom locCheck = wall.node.room;
                        //    if (i == 1) locCheck = wall.otherWall.node.room;

                        //    //Is this area illegal?
                        //    //bool isIllegal = Player.Instance.IsTrespassing(locCheck, out _);

                        //    //if (isIllegal)
                        //    //{
                        //    //    //Add this as illegally opened
                        //    //    //Add key
                        //    //    if (!Player.Instance.doorsOpenedIllegally.ContainsKey(locCheck.gameLocation))
                        //    //    {
                        //    //        Player.Instance.doorsOpenedIllegally.Add(locCheck.gameLocation, new List<Interactable>());
                        //    //    }

                        //    //    //Add door interactable
                        //    //    if (!Player.Instance.doorsOpenedIllegally[locCheck.gameLocation].Contains(doorInteractable))
                        //    //    {
                        //    //        Player.Instance.doorsOpenedIllegally[locCheck.gameLocation].Add(doorInteractable);
                        //    //    }

                        //    //    //Update b&e
                        //    //    Player.Instance.UpdateBreakingAndEntering();
                        //    //}
                        //}
                    }
                }
            }
            else
            {
                //Reset the lock strength
                wall.ResetLockStrength();

                ////Only bother doing this if there illegally opened doors present
                //if (Player.Instance.doorsOpenedIllegally.Count > 0)
                //{
                //    //If a door has been closed we need to check the locations on both sides, as the door can be closed from either side
                //    for (int i = 0; i < 2; i++)
                //    {
                //        //Has this door been closed and is it on a list of illegally opened doors?
                //        NewGameLocation locCheck = wall.node.gameLocation;
                //        if (i == 1) locCheck = wall.otherWall.node.gameLocation;

                //        if (Player.Instance.doorsOpenedIllegally.ContainsKey(locCheck))
                //        {
                //            if (Player.Instance.doorsOpenedIllegally[locCheck].Contains(doorInteractable))
                //            {
                //                Player.Instance.doorsOpenedIllegally[locCheck].Remove(doorInteractable);

                //                //Remove breaking and entering if no more doors left (this must be done seperately to the update as it's pobbile an AI may close a door and it becomes impossible for player to reset).
                //                if (Player.Instance.doorsOpenedIllegally[locCheck].Count <= 0 && actor != null && actor.isPlayer)
                //                {
                //                    GameplayController.PerilFine beFine = GameplayController.Instance.perilFines.Find(item => item.perilType == GameplayController.PerilType.breakingAndEntering);

                //                    //Find the location's count
                //                    GameplayController.PerilCount beCount = beFine.counts.Find(item => item.gameLocation == locCheck);
                //                    beFine.counts.Remove(beCount);

                //                    GameplayController.Instance.UpdatePeril();
                //                }
                //            }
                //        }
                //    }
                //}
            }

            //Game.Log("Door " + name + " = " + isLocked);

            if (isLocked)
            {
                if (playSound) AudioController.Instance.PlayWorldOneShot(preset.audioLock, actor, parentedWall.node, this.transform.position, additionalSources: bothNodesForAudioSource);
            }
            else
            {
                if (playSound) AudioController.Instance.PlayWorldOneShot(preset.audioUnlock, actor, parentedWall.node, this.transform.position, additionalSources: bothNodesForAudioSource);
            }

            //Make sure door is closed
            if (isLocked && ajar != 0f)
            {
                SetOpen(0f, actor);
            }
        }

        if (doorInteractable != null) doorInteractable.SetLockedState(isLocked, actor);
        if (handleInteractable != null) handleInteractable.SetLockedState(isLocked, actor);
        if (peekInteractable != null) peekInteractable.SetLockedState(isLocked, actor);
        if (lockInteractableFront != null) lockInteractableFront.SetLockedState(isLocked, actor);
        if (lockInteractableRear != null) lockInteractableRear.SetLockedState(isLocked, actor);
    }

    public void SetJammed(bool val, Interactable newDoorWedge = null)
    {
        if(isJammed != val)
        {
            isJammed = val;

            if(isJammed && newDoorWedge != null)
            {
                if (doorWedge != null) doorWedge.Delete(); //Remove existing
                doorWedge = newDoorWedge;
            }
            //Remove door wedge
            else if(!isJammed && doorWedge != null)
            {
                Game.Log("Removing door wedge...");

                //Create physics used codebreaker
                if (doorWedge.controller != null)
                {
                    Interactable usedWedge = InteractableCreator.Instance.CreateWorldInteractable(InteriorControls.Instance.doorWedgeUsed, Player.Instance, null, null, doorWedge.wPos, doorWedge.wEuler, null, null);
                    usedWedge.ForcePhysicsActive(false, true, (doorWedge.controller.transform.up * 1.25f));
                }

                doorWedge.Delete();
            }
        }
    }

    public void SetForbidden(bool val)
    {
        forbiddenForPublic = val;

        //Forbidden == crime scene, force door open, deactivate usage
        if (forbiddenForPublic)
        {
            SetLocked(false, null, false);
            SetOpen(1f, null);
        }

        //Spawn police tape
        if (forbiddenForPublic)
        {
            if(spawnedDoor != null)
            {
                SpawnPoliceTape();
                Game.Log("Spawning police tape at " + spawnedDoor.name);
            }

            if(!GameplayController.Instance.policeTapeDoors.Contains(this))
            {
                GameplayController.Instance.policeTapeDoors.Add(this);
            }
        }
        else
        {
            if(policeTape != null)
            {
                Destroy(policeTape);
                SetOpen(0, null, false); //Close the door
                Game.Log("Removing police tape at " + spawnedDoor.name);
            }

            GameplayController.Instance.policeTapeDoors.Remove(this);
        }
    }

    private void SpawnPoliceTape()
    {
        if (policeTape == null)
        {
            policeTape = Instantiate(PrefabControls.Instance.policeTape, this.transform);
            policeTape.transform.localPosition = new Vector3(0f, 0f, -0.25f);

            //HDRP: Set mesh to light layer
            Toolbox.Instance.SetLightLayer(policeTape, wall.node.building);
        }
    }

    //Called when a citizen tries to pass through this door
    public bool CitizenPassCheck(Human cc, out CitizenPassResult reason)
    {
        reason = CitizenPassResult.success;

        //For now, give enforcers all-access
        //if (cc.isEnforcer && cc.isOnDuty) return true;

        if(cc.ai != null)
        {
            if (cc.ai.currentAction != null && cc.ai.currentAction.preset.ignoreLockedDoors) return true; //Ignore locked doors
        }

        bool ret = false;

        //Forbidden entry: Most commonly used to prevent access to a discovered crime scene. Normal citizens can move out but not IN.
        bool ignoreForbidden = false;

        //Ignore if exiting room is the entranceway's parent location
        if (cc.currentRoom == wall.node.room)
        {
            ignoreForbidden = true;
        }

        //Ignore tape if no other exits...
        if(forbiddenForPublic && !cc.currentGameLocation.entrances.Exists(item => item.door != this && item.walkingAccess))
        {
            ignoreForbidden = true;
        }

        //if (cc.isCriminal) Game.Log("Door: Criminal Door Pass Check: " + wall.node.gameLocation.name);

        if(isJammed)
        {
            reason = CitizenPassResult.isJammed;
        }
        else if (!isLocked && (!forbiddenForPublic || ignoreForbidden))
        {
            ret = true;
        }
        else if (isLocked && (!forbiddenForPublic || ignoreForbidden))
        {
            //Does the citizen have a key?
            if (cc.keyring.Contains(this) || (cc.ai != null && cc.ai.currentAction != null && cc.ai.currentAction.preset.ignoreLockedDoors))
            {
                ret = true;
            }
            else
            {
                reason = CitizenPassResult.isLocked;
            }
        }
        else if (forbiddenForPublic && !ignoreForbidden)
        {
            reason = CitizenPassResult.isForbidden;
        }

        return ret;
    }

    //Barge
    public void Barge(Actor barger)
    {
        Game.Log("Gameplay: Execute barge door attempt...");

        //Play contact sound
        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.bargeDoorContact, barger, barger.currentNode, barger.lookAtThisTransform.position);

        //If unlocked, then open
        if (!isLocked && !isJammed)
        {
            doorInteractable.controller.isDoor.OpenByActor(barger, speedMultiplier: 4f);

            if (barger != null && barger.isPlayer)
            {
                Player.Instance.TransformPlayerController(GameplayControls.Instance.bargeDoorSuccess, null, doorInteractable, null);
                StatusController.Instance.AddFineRecord(barger.currentGameLocation.thisAsAddress, doorInteractable, StatusController.CrimeType.breakingAndEntering, true);
            }

            //KO everybody on the other side
            if(barger != null)
            {
                NewNode otherNode = wall.node;
                if (barger.currentNode == wall.node) otherNode = wall.otherWall.node;

                foreach(Actor a in otherNode.room.currentOccupants)
                {
                    if(a.ai != null)
                    {
                        if(a.currentNode == otherNode)
                        {
                            if(!a.isStunned && !a.isDead)
                            {
                                a.ai.SetKO(true, spawnedDoor.transform.position + new Vector3(0, 1, 0), barger.transform.forward, forceMultiplier: CitizenControls.Instance.doorBargeKOForceMultiplier);
                            }
                        }
                    }
                }
            }
        }
        //Damage door
        else
        {
            if(barger != null && barger.isPlayer)
            {
                //Know lock strength
                SetKnowLockedStatus(true);

                wall.SetDoorStrength(wall.currentDoorStrength - (Toolbox.Instance.Rand(GameplayControls.Instance.bargeDamageRange.x, GameplayControls.Instance.bargeDamageRange.y) + UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.doorBargeModifier)));
            }
            else if(barger != null)
            {
                wall.SetDoorStrength(wall.currentDoorStrength - barger.combatHeft * 0.1f);
                Game.Log("Gameplay: AI set door strength: " + wall.currentDoorStrength);
            }
            else
            {
                wall.SetDoorStrength(wall.currentDoorStrength - 0.1f);
                Game.Log("AI Error: Barger is null!");
            }

            //Is this door maximum damaged? If so then burst open
            if (wall.currentDoorStrength <= 0f && barger != null)
            {
                OpenByActor(barger, speedMultiplier: 4f);

                if(barger.isPlayer)
                {
                    Player.Instance.TransformPlayerController(GameplayControls.Instance.bargeDoorSuccess, null, doorInteractable, null);
                }

                //Play break sound
                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.bargeDoorBreak, barger, barger.currentNode, barger.lookAtThisTransform.position);
            }
            //Otherwise then fail return
            else if(barger != null)
            {
                if(barger.isPlayer)
                {
                    Player.Instance.TransformPlayerController(GameplayControls.Instance.bargeDoorFail, null, doorInteractable, null);
                }
            }
        }
    }

    //Knock
    public void OnKnock(Actor actor, int knockCount = 2, float forceAdditionalUrgency = 0f)
    {
        if (knockingInProgress) return;
        if (!isClosed) return;

        GameplayController.Instance.KnockOnDoor(this, actor, knockCount, forceAdditionalUrgency);
    }

    //On door peek
    public void OnDoorPeek()
    {
        //Set as locked in interaction
        InteractionController.Instance.SetLockedInInteractionMode(peekInteractable);

        //Which side is the player on?
        //Convert player world position to local position relative to the door
        Vector3 localPos = this.transform.InverseTransformPoint(Player.Instance.transform.position);
        //Game.Log(localPos);
        Vector3 peekPos = Vector3.zero;

        if (localPos.z > 0)
        {
            peekPos = this.transform.TransformPoint(new Vector3(0, 0, 0.3f));
        }
        else
        {
            peekPos = this.transform.TransformPoint(new Vector3(0, 0, -0.3f));
        }

        //Keep player Y pos
        peekPos.y = Player.Instance.transform.position.y;

        Player.Instance.TransformPlayerController(GameplayControls.Instance.doorPeekEnter, GameplayControls.Instance.doorPeekExit, peekInteractable, null, false);

        //Listen for return from locked in 
        InteractionController.Instance.OnReturnFromLockedIn += OnReturnFromPeek;

        //Move door up slightly
        spawnedDoor.transform.localPosition = parentedWall.preset.doorOffset + new Vector3(0, 0.03f, 0);

        //Force culling update as this room will need to be drawn...
        peekedUnder = true;

        //A cull update is needed if this door features in the current room's conditional door set...
        if (Player.Instance.currentRoom != null && (Player.Instance.currentRoom.doorCheckSet.Contains(wall.id) || Player.Instance.currentRoom.doorCheckSet.Contains(wall.otherWall.id)))
        {
            Player.Instance.UpdateCullingOnEndOfFrame();
        }
    }

    public void OnReturnFromPeek()
    {
        InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromPeek;
        Player.Instance.ReturnFromTransform();

        //Return door to normal position
        spawnedDoor.transform.localPosition = parentedWall.preset.doorOffset;

        //Force culling update as this room will need to be drawn...
        peekedUnder = false;

        //A cull update is needed if this door features in the current room's conditional door set...
        if (Player.Instance.currentRoom != null && (Player.Instance.currentRoom.doorCheckSet.Contains(wall.id) || Player.Instance.currentRoom.doorCheckSet.Contains(wall.otherWall.id)))
        {
            Player.Instance.UpdateCullingOnEndOfFrame();
        }
    }

    //On lockpick
    public void OnLockpick()
    {
        //Set know door status
        SetKnowLockedStatus(true);
        if (!isLocked) return;
        if (!isClosed) return;

        //Set as locked in interaction
        InteractionController.Instance.SetLockedInInteractionMode(handleInteractable);

        Player.Instance.TransformPlayerController(GameplayControls.Instance.lockpickEnter, GameplayControls.Instance.lockpickExit, handleInteractable, null, false);

        //Set ineraction action
        InteractionController.Instance.SetInteractionAction(0f, wall.currentLockStrength, Mathf.LerpUnclamped(GameplayControls.Instance.lockpickEffectivenessRange.x, GameplayControls.Instance.lockpickEffectivenessRange.y, UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.lockpickingEfficiencyModifier)), "lockpicking", true, true, doorHandleInteractableController.transform);
        Player.Instance.isLockpicking = true;

        //Listen for progress change
        InteractionController.Instance.OnInteractionActionProgressChange += OnLockpickProgressChange;

        //Listen for complete lockpick
        InteractionController.Instance.OnInteractionActionCompleted += OnCompleteLockpick;

        //Listen for return from locked in 
        InteractionController.Instance.OnReturnFromLockedIn += OnReturnFromLockpick;

        //Play sfx looping
        AudioController.Instance.StopSound(lockpickLoop, AudioController.StopType.immediate, "Stop lockpick");
        lockpickLoop = AudioController.Instance.PlayWorldLooping(AudioControls.Instance.lockpick, Player.Instance, handleInteractable);

        //Add fine
        if(doorInteractable != null) StatusController.Instance.AddFineRecord(Player.Instance.currentGameLocation.thisAsAddress, doorInteractable, StatusController.CrimeType.breakingAndEntering);
    }

    public void OnLockpickProgressChange(float amountChangeThisFrame, float amountToal)
    {
        //Game.Log("Player: On locking use: " + amountChangeThisFrame);
        wall.SetCurrentLockStrength(wall.currentLockStrength - amountChangeThisFrame); //Set actual lock strength

        //Use up lockpick
        GameplayController.Instance.UseLockpick(amountChangeThisFrame);

        //Check we still have lockpicks to continue...
        if(GameplayController.Instance.lockPicks <= 0)
        {
            InteractionController.Instance.SetLockedInInteractionMode(null);
            InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, Strings.Get("ui.gamemessage", "not_enough_lockpicks"), InterfaceControls.Icon.lockpick, null, colourOverride: true, col: InterfaceControls.Instance.messageRed, ping: GameMessageController.PingOnComplete.lockpicks);
        }
    }

    public void OnCompleteLockpick()
    {
        //Stop SFX
        AudioController.Instance.StopSound(lockpickLoop, AudioController.StopType.fade, "Complete lockpick");
        lockpickLoop = null;

        InteractionController.Instance.OnInteractionActionProgressChange -= OnLockpickProgressChange;
        //InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromLockpick;
        InteractionController.Instance.OnInteractionActionCompleted -= OnCompleteLockpick;
        Player.Instance.isLockpicking = false;

        //Return from lock in
        InteractionController.Instance.SetLockedInInteractionMode(null);

        //Remove fine record
        StatusController.Instance.RemoveFineRecord(null, doorInteractable, StatusController.CrimeType.breakingAndEntering, true);
    }

    public void OnReturnFromLockpick()
    {
        //Stop SFX
        AudioController.Instance.StopSound(lockpickLoop, AudioController.StopType.fade, "Return from lockpick");
        lockpickLoop = null;

        InteractionController.Instance.OnInteractionActionProgressChange -= OnLockpickProgressChange;
        InteractionController.Instance.OnReturnFromLockedIn -= OnReturnFromLockpick;
        InteractionController.Instance.OnInteractionActionCompleted -= OnCompleteLockpick;
        Player.Instance.isLockpicking = false;

        Player.Instance.ReturnFromTransform();

        InterfaceController.Instance.NewGameMessage(InterfaceController.GameMessageType.notification, 0, GameplayController.Instance.lockPicks + " " + Strings.Get("ui.gamemessage", "lockpick_deplete"), InterfaceControls.Icon.lockpick, null, moveToOnDestroy: InterfaceController.Instance.moneyNotificationIcon, ping: GameMessageController.PingOnComplete.lockpicks);

        //Remove fine record
        StatusController.Instance.RemoveFineRecord(null, doorInteractable, StatusController.CrimeType.breakingAndEntering, true);
    }

    public bool GetDefaultLockState()
    {
        if ((wall.node.gameLocation.thisAsAddress != null && wall.node.gameLocation.thisAsAddress.residence != null) || (wall.otherWall.node.gameLocation.thisAsAddress != null && wall.otherWall.node.gameLocation.thisAsAddress.residence != null))
        {
            //Main entrances...
            if (wall.node.gameLocation != wall.otherWall.node.gameLocation)
            {
                return true;
            }
        }

        //Is company...
        if (wall.node.gameLocation.thisAsAddress != null && wall.node.gameLocation.thisAsAddress.company != null)
        {
            if (wall.node.gameLocation.thisAsAddress.addressPreset == null || wall.node.gameLocation.thisAsAddress.addressPreset.entrancesLockedByDefault)
            {
                return true;
            }
            else return false;
        }
        else if (wall.otherWall.node.gameLocation.thisAsAddress != null && wall.otherWall.node.gameLocation.thisAsAddress.company != null)
        {
            if (wall.otherWall.node.gameLocation.thisAsAddress.addressPreset == null || wall.otherWall.node.gameLocation.thisAsAddress.addressPreset.entrancesLockedByDefault)
            {
                return true;
            }
            else return false;
        }

        return false;
    }

    [Button]
    public void DebugTestPlayersRelativePosition()
    {
        Vector3 relPosition = this.transform.InverseTransformPoint(Player.Instance.transform.position);
        Game.Log(relPosition);
    }
}
