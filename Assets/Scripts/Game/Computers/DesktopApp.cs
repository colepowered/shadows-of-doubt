﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesktopApp : CruncherAppContent
{
    public GameObject desktopIconPrefab;
    public List<DesktopIconController> spawnedIcons = new List<DesktopIconController>();

    public override void OnSetup()
    {
        base.OnSetup();

        //Update icons
        UpdateIcons();
    }

    public void UpdateIcons()
    {
        foreach(DesktopIconController di in spawnedIcons)
        {
            Destroy(di.gameObject);
        }

        spawnedIcons.Clear();

        Vector2 spawnPos = new Vector2(0.034f, -0.034f);

        foreach(CruncherAppPreset app in controller.ic.interactable.preset.additionalApps)
        {
            //Do a check to see if this app is installed...
            if(!app.alwaysInstalled)
            {
                bool pass = false;

                if (controller.ic.interactable.node.gameLocation.thisAsAddress != null)
                {
                    if (app.companyOnly)
                    {
                        if (controller.ic.interactable.node.gameLocation.thisAsAddress.company == null)
                        {
                            pass = false;
                        }
                        else
                        {
                            if(app.salesRecordsOnly && !controller.ic.interactable.node.gameLocation.thisAsAddress.company.preset.recordSalesData)
                            {
                                pass = false;
                            }
                            else pass = true;
                        }

                        if (!pass) continue;
                    }

                    if (app.onlyIfOwner)
                    {
                        if (controller.ic.interactable.node.gameLocation.thisAsAddress != null)
                        {
                            if (controller.ic.interactable.node.gameLocation.thisAsAddress.owners.Contains(controller.loggedInAs))
                            {
                                pass = true;
                            }
                            else pass = false;
                        }

                        if (!pass) continue;
                    }

                    if(app.onlyInAddresses.Count > 0)
                    {
                        if (controller.ic.interactable.node.gameLocation.thisAsAddress != null && app.onlyInAddresses.Contains(controller.ic.interactable.node.gameLocation.thisAsAddress.addressPreset))
                        {
                            pass = true;
                        }
                        else pass = false;

                        if (!pass) continue;
                    }

                    if(app.onlyIfResidential)
                    {
                        pass = false;

                        if(controller.ic.interactable.node.gameLocation.building != null)
                        {
                            foreach(KeyValuePair<int, NewFloor> pair in controller.ic.interactable.node.gameLocation.building.floors)
                            {
                                if(pair.Value.addresses.Exists(item => item.residence != null))
                                {
                                    pass = true;
                                    break;
                                }
                            }
                        }

                        if (!pass) continue;
                    }
                }

                //Pass trait check...
                foreach (CruncherAppPreset.AppAccess rule in app.installationConditions)
                {
                    if (rule.rule == CharacterTrait.RuleType.ifAnyOfThese)
                    {
                        foreach (CharacterTrait searchTrait in rule.traitList)
                        {
                            if (controller.loggedInAs.characterTraits.Exists(item => item.trait == searchTrait))
                            {
                                pass = true;
                                break;
                            }
                        }
                    }
                    else if (rule.rule == CharacterTrait.RuleType.ifAllOfThese)
                    {
                        pass = true;

                        foreach (CharacterTrait searchTrait in rule.traitList)
                        {
                            if (!controller.loggedInAs.characterTraits.Exists(item => item.trait == searchTrait))
                            {
                                pass = false;
                                break;
                            }
                        }
                    }
                    else if (rule.rule == CharacterTrait.RuleType.ifNoneOfThese)
                    {
                        pass = true;

                        foreach (CharacterTrait searchTrait in rule.traitList)
                        {
                            if (controller.loggedInAs.characterTraits.Exists(item => item.trait == searchTrait))
                            {
                                pass = false;
                                break;
                            }
                        }
                    }
                    else if (rule.rule == CharacterTrait.RuleType.ifPartnerAnyOfThese)
                    {
                        if (controller.loggedInAs.partner != null)
                        {
                            foreach (CharacterTrait searchTrait in rule.traitList)
                            {
                                if (controller.loggedInAs.partner.characterTraits.Exists(item => item.trait == searchTrait))
                                {
                                    pass = true;
                                    break;
                                }
                            }
                        }
                        else pass = false;
                    }
                }

                if (app.onlyIfCorporateSabotageSkill && UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.installMalware) <= 0f)
                {
                    pass = false;
                }

                if (!pass) continue;
            }

            GameObject newIcon = Instantiate(desktopIconPrefab, this.transform);
            DesktopIconController ic = newIcon.GetComponent<DesktopIconController>();
            ic.Setup(this, app);

            spawnedIcons.Add(ic);

            ic.rect.anchoredPosition = spawnPos;
            spawnPos.x += ic.rect.sizeDelta.x + 0.034f;

            //Start a new row
            if (spawnPos.x > 1f - 0.034f - ic.rect.sizeDelta.x)
            {
                spawnPos.y -= 0.034f + ic.rect.sizeDelta.y;
                spawnPos.x = 0.034f;
            }
        }
    }

    public void OnDesktopAppSelect(CruncherAppPreset newApp)
    {
        controller.SetComputerApp(newApp);
    }
}
