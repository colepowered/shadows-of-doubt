using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Linq;

public class SoCustomComparison : ScriptableObject
{
    [Tooltip("We need a internal reference to ID the file based on name (can't access name outside main threads)")]
    public string presetName;


    //Custom comparisions: This is needed for comparing addressable versions to non-addressable versions of the same class
    public bool Equals(SoCustomComparison other)
    {
        return Equals(other, this);
    }

    public virtual string GetPresetName()
    {
        if (presetName != null && presetName.Length > 0) return presetName;

        try
        {
            return name;
        }
        catch(System.Exception e)
        {
            //Probably not being run on the main thread as we can't access name...
            //Game.LogError("Unable to compare names of class " + GetType() + ": " + presetName + " (" + e.Message + ")");
        }

        //This shouldn't happen, at this point use an instance ID so we don't get false positive comparisons
        return GetInstanceID().ToString();
    }

    public override bool Equals(object obj)
    {
        //Reject different types
        if (obj == null || this == null || GetType() != obj.GetType())
        {
            return false;
        }

        var objectToCompareWith = (SoCustomComparison)obj;
        if (objectToCompareWith == null) return false;

        //Use preset name to compare
        return objectToCompareWith.GetPresetName() == GetPresetName() && objectToCompareWith.GetType() == GetType();
    }

    public override int GetHashCode()
    {
        System.HashCode hc = new System.HashCode();
        hc.Add(GetType());
        hc.Add(GetPresetName());

        return hc.ToHashCode();
    }

    public static bool operator ==(SoCustomComparison c1, SoCustomComparison c2)
    {
        if (c1 is null)
        {
            return c2 is null;
        }

        return c1.Equals(c2);
    }

    public static bool operator !=(SoCustomComparison c1, SoCustomComparison c2)
    {
        return !(c1 == c2);
    }
}
