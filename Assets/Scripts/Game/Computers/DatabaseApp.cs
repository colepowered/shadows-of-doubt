﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEditor;

public class DatabaseApp : CruncherAppContent
{
    [Header("Components")]
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI searchText;
    public ComputerOSMultiSelect list;
    public RectTransform printButton;

    [Header("State")]
    public string searchString;
    public InteractablePreset ddsPrintout;
    public CitizenPool citizenPool = CitizenPool.allCitizens;

    public enum CitizenPool { allCitizens, companyOnly, buildingOnly };

    //Called on setup
    public override void OnSetup()
    {
        searchString = string.Empty;
        list.OnNewSelection += UpdateSelected;
        UpdateSearch();
    }

    //Detect keystrokes
    private void Update()
    {
        if (Player.Instance.computerInteractable == controller.ic.interactable)
        {
            if (!InterfaceController.Instance.playerTextInputActive && InputController.Instance.mouseInputMode)
            {
                if (Input.GetKeyDown(KeyCode.A))
                {
                    KeyboardButton("A");
                }
                if (Input.GetKeyDown(KeyCode.B))
                {
                    KeyboardButton("B");
                }
                if (Input.GetKeyDown(KeyCode.C))
                {
                    KeyboardButton("C");
                }
                if (Input.GetKeyDown(KeyCode.D))
                {
                    KeyboardButton("D");
                }
                if (Input.GetKeyDown(KeyCode.E))
                {
                    KeyboardButton("E");
                }
                if (Input.GetKeyDown(KeyCode.F))
                {
                    KeyboardButton("F");
                }
                if (Input.GetKeyDown(KeyCode.G))
                {
                    KeyboardButton("G");
                }
                if (Input.GetKeyDown(KeyCode.H))
                {
                    KeyboardButton("H");
                }
                if (Input.GetKeyDown(KeyCode.I))
                {
                    KeyboardButton("I");
                }
                if (Input.GetKeyDown(KeyCode.J))
                {
                    KeyboardButton("J");
                }
                if (Input.GetKeyDown(KeyCode.K))
                {
                    KeyboardButton("K");
                }
                if (Input.GetKeyDown(KeyCode.L))
                {
                    KeyboardButton("L");
                }
                if (Input.GetKeyDown(KeyCode.M))
                {
                    KeyboardButton("M");
                }
                if (Input.GetKeyDown(KeyCode.N))
                {
                    KeyboardButton("N");
                }
                if (Input.GetKeyDown(KeyCode.O))
                {
                    KeyboardButton("O");
                }
                if (Input.GetKeyDown(KeyCode.P))
                {
                    KeyboardButton("P");
                }
                if (Input.GetKeyDown(KeyCode.Q))
                {
                    KeyboardButton("Q");
                }
                if (Input.GetKeyDown(KeyCode.R))
                {
                    KeyboardButton("R");
                }
                if (Input.GetKeyDown(KeyCode.S))
                {
                    KeyboardButton("S");
                }
                if (Input.GetKeyDown(KeyCode.T))
                {
                    KeyboardButton("T");
                }
                if (Input.GetKeyDown(KeyCode.U))
                {
                    KeyboardButton("U");
                }
                if (Input.GetKeyDown(KeyCode.V))
                {
                    KeyboardButton("V");
                }
                if (Input.GetKeyDown(KeyCode.W))
                {
                    KeyboardButton("W");
                }
                if (Input.GetKeyDown(KeyCode.X))
                {
                    KeyboardButton("X");
                }
                if (Input.GetKeyDown(KeyCode.Y))
                {
                    KeyboardButton("Y");
                }
                if (Input.GetKeyDown(KeyCode.Z))
                {
                    KeyboardButton("Z");
                }
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    KeyboardButton(" ");
                }
                if (Input.GetKeyDown(KeyCode.Backspace))
                {
                    BackspaceButton();
                }
            }
        }
    }

    private void OnDestroy()
    {
        list.OnNewSelection -= UpdateSelected;
    }

    public void UpdateSelected()
    {
        if(list.selected != null)
        {
            printButton.gameObject.SetActive(true);
        }
        else
        {
            printButton.gameObject.SetActive(false);
        }
    }

    public void KeyboardButton(string charStr)
    {
        searchString += charStr;

        //Play button audio
        AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerKeyboardKey, Player.Instance, Player.Instance.currentNode, this.transform.position);

        UpdateSearch();
    }

    public void BackspaceButton()
    {
        if(searchString.Length > 0) searchString = searchString.Substring(0, searchString.Length - 1);
        UpdateSearch();
    }

    public void UpdateSearch()
    {
        List<ComputerOSMultiSelect.OSMultiOption> newOptions = new List<ComputerOSMultiSelect.OSMultiOption>();
        string red = string.Empty;
        if (searchString.Length >= 2)
        {
            List<Citizen> foundCitizens = new List<Citizen>();

            if(citizenPool == CitizenPool.allCitizens)
            {
                foundCitizens = CityData.Instance.citizenDirectory.FindAll(item => item.GetCitizenName().ToLower().Contains(searchString.ToLower()));
            }
            else if(citizenPool == CitizenPool.companyOnly)
            {
                foundCitizens = CityData.Instance.citizenDirectory.FindAll(item => item.job != null && item.job.employer != null && item.job.employer.address == controller.ic.interactable.node.gameLocation && item.GetCitizenName().ToLower().Contains(searchString.ToLower()));
            }
            else if(citizenPool == CitizenPool.buildingOnly)
            {
                foundCitizens = CityData.Instance.citizenDirectory.FindAll(item => item.home != null && item.home.building == controller.ic.interactable.node.gameLocation.building && item.GetCitizenName().ToLower().Contains(searchString.ToLower()));
            }

            foreach(Citizen c in foundCitizens)
            {
                ComputerOSMultiSelect.OSMultiOption newOption = new ComputerOSMultiSelect.OSMultiOption();
                newOption.text = c.GetCitizenName();
                newOption.human = c;
                newOptions.Add(newOption);
            }
        }
        else
        {
            red = "<color=\"red\">";
        }

        searchText.text = Strings.Get("computer", "Database Search") + ": " + red + searchString + "_";

        list.UpdateElements(newOptions);
        UpdateSelected();
    }

    public void ExitButton()
    {
        controller.OnAppExit();
    }

    public void OnPrintEntry()
    {
        if(list.selected != null)
        {
            Game.Log("Print " + list.selected);

            controller.SetTimedLoading(Toolbox.Instance.Rand(0.5f, 1f));

            if (controller.printedDocument == null && controller.printTimer <= 0f)
            {
                controller.printTimer = 1f;
                controller.printerParent.localPosition = new Vector3(controller.printerParent.localPosition.x, controller.printerParent.localPosition.y, -0.05f); //Move printer paren back

                AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerPrint, Player.Instance, controller.ic.interactable.node, controller.ic.interactable.wPos);

                controller.printedDocument = InteractableCreator.Instance.CreateWorldInteractable(ddsPrintout, Player.Instance, list.selected.option.human, list.selected.option.human, controller.printerParent.position, controller.ic.transform.eulerAngles, null, null);
                if (controller.printedDocument != null) controller.printedDocument.MarkAsTrash(true);

                //Listen for on remove...
                controller.printedDocument.OnRemovedFromWorld += OnPlayerTakePrint;
            }
            else AudioController.Instance.PlayWorldOneShot(AudioControls.Instance.computerInvalidPasscode, Player.Instance, controller.ic.interactable.node, controller.ic.interactable.wPos);
        }
    }
}
