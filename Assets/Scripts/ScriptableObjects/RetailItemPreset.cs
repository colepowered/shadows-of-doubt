﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "retailitem_data", menuName = "Database/Retail Item")]

public class RetailItemPreset : SoCustomComparison
{
    [Header("Item")]
    public InteractablePreset itemPreset;
    [Tooltip("Can this be ranked by the citizen as a favourite item? If true this will be used to calculate favourite places, as well as appear on shopping lists etc.")]
    public bool canBeFavourite = true;
    [Tooltip("If true this item stays warm for 1 hour after it was purchased.")]
    public bool isHot = false;
    [Tooltip("A citizen can pick this to buy at a shop")]
    public bool isConsumable = true;
    [Tooltip("If this is entered, upon singleton creation the evidence will be named using this entry.")]
    public string brandName = string.Empty;
    public List<Tags> tags = new List<Tags>();

    public enum Tags { starchProduct };

    public enum MenuCategory { food, drinks, snacks, none };

    [Header("Menu")]
    public CompanyPreset.CompanyCategory desireCategory = CompanyPreset.CompanyCategory.snack;
    public MenuCategory menuCategory = MenuCategory.food;

    [Header("Ethnicity")]
    [Tooltip("Which ethnicity is this food (if any)")]
    [ReorderableList]
    public List<Descriptors.EthnicGroup> ethnicity = new List<Descriptors.EthnicGroup>();

    [Header("Citizen Suitability")]
    [Tooltip("Citizen's money must be higher than this to list this item in favourites")]
    [Range(0f, 1f)]
    public float minimumWealth = 0f;
    [ReorderableList]
    public List<CharacterTrait> mustFeatureTraits = new List<CharacterTrait>();
    [ReorderableList]
    public List<CharacterTrait> cantFeatureTrait = new List<CharacterTrait>();
    [ReorderableList]
    public List<CharacterTrait> preferredTraits = new List<CharacterTrait>();

    [Header("Stat Modifiers")]
    [Tooltip("This is applied as progress increases")]
    public float nourishment = 0f;
    [Tooltip("This is applied as progress increases")]
    public float hydration = 0f;
    [Tooltip("This is applied as progress increases")]
    public float alertness = 0f;
    [Tooltip("This is applied as progress increases")]
    public float energy = 0f;
    [Tooltip("This is applied as progress increases")]
    public float excitement = 0f;
    [Tooltip("This is applied as progress increases")]
    public float chores = 0f;
    [Tooltip("This is applied as progress increases")]
    public float hygiene = 0f;
    [Tooltip("This is applied as progress increases")]
    public float bladder = 0f;
    [Tooltip("This is applied as progress increases")]
    public float heat = 0f;
    [Tooltip("This is applied as progress increases")]
    public float drunk = 0f;
    [Tooltip("This is applied as progress increases")]
    public float sick = 0f;
    [Tooltip("This is applied as progress increases")]
    public float headache = 0f;
    [Tooltip("This is applied as progress increases")]
    public float wet = 0f;
    [Tooltip("This is applied as progress increases")]
    public float brokenLeg = 0f;
    [Tooltip("This is applied as progress increases")]
    public float bruised = 0f;
    [Tooltip("This is applied as progress increases")]
    public float blackEye = 0f;
    [Tooltip("This is applied as progress increases")]
    public float blackedOut = 0f;
    [Tooltip("This is applied as progress increases")]
    public float numb = 0f;
    [Tooltip("This is applied as progress increases")]
    public float bleeding = 0f;
    [Tooltip("This is applied as progress increases")]
    public float wellRested = 0f;
    [Tooltip("This is applied as progress increases")]
    public float breath = 0f;
    [Tooltip("This is applied as progress increases")]
    public float starchAddiction = 0f;
    [Tooltip("This is applied as progress increases")]
    public float poisoned = 0f;
    [Tooltip("This is applied as progress increases")]
    public float health = 0f;
}
