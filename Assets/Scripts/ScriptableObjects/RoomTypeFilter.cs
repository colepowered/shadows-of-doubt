﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEditor;
using System.Linq;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "roomtypefilter_data", menuName = "Database/Room Type Filter")]

public class RoomTypeFilter : SoCustomComparison
{
	[Header("Room Types")]
	public List<RoomClassPreset> roomClasses = new List<RoomClassPreset>();
}
