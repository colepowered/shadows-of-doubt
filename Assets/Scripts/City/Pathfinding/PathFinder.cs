﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Unity.Mathematics;
using System.Linq;
using UnityEditor;
using System.IO;
using System;

//using UnityEditor.SceneManagement;

//Generates paths and stores calculated routes in a dictionary
//Script pass 1
public class PathFinder : MonoBehaviour
{
    private const int INITIAL_COLLECTION_SIZE = 96; // Attempt to reduce frequent collection resize calls with larger initial allocations

    //Pre-calculated city size adjusted for pathfinding map
    public Vector3 tileSize; //Size of a tile
    public Vector3 nodeSize; //Size of a node
    public Vector2 citySizeReal; //Size of the city in unity units
    public Vector2 halfCitySizeReal; //Useful for location operaterions as zero point is 0,0
    public Vector2 tileCitySize; //Size of the city divided into tiles
    public Vector2 nodeCitySize; //Size of the city divided into nodes

    public Vector2 nodeRangeX; //Size of the city divided into nodes
    public Vector2 nodeRangeY; //Size of the city divided into nodes
    public Vector2 nodeRangeZ; //Size of the city divided into nodes

    [Header("Debug Data")]
    public int totalPathCalls = 0;
    public int calculatedRoomRoutes = 0;
    public int returnedCachedRoomRoutes = 0;
    public int calculatedInternalRoutes = 0;

    //Path data- the outputted path along with aproximate timing detail and other additional infomation
    public class PathData
    {
        //List of locations as coordinates
        public List<NewNode.NodeAccess> accessList = new List<NewNode.NodeAccess>(INITIAL_COLLECTION_SIZE);
    }

    //Pathfinding map: All nodes references by their Vector 3 key
    public Dictionary<Vector3, NewNode> nodeMap = new Dictionary<Vector3, NewNode>(INITIAL_COLLECTION_SIZE, FastVector3Comparer.SharedFastVector3Comparer);
    public Dictionary<Vector3, NewTile> tileMap = new Dictionary<Vector3, NewTile>(INITIAL_COLLECTION_SIZE, FastVector3Comparer.SharedFastVector3Comparer);

    //New pathfinding system using rooms as quadtrees...
    //Key used for finding existing routes between 2 rooms
    public struct RoomPathKey : IEquatable<RoomPathKey>
    {
        public NewRoom originRoom;
        public NewRoom destinationRoom;
        private bool hasHash;
        private int hash;

        public RoomPathKey(NewRoom locOne, NewRoom locTwo)
        {
            originRoom = locOne;
            destinationRoom = locTwo;
            hasHash = false;
            hash = 0;
        }

        public override int GetHashCode()
        {
            if (!hasHash)
            {
                hash = HashCode.Combine(originRoom, destinationRoom);
                hasHash = true;
            }
            return hash;
        }

        bool IEquatable<RoomPathKey>.Equals(RoomPathKey other)
        {
            return other.GetHashCode() == GetHashCode();
        }
    }

    public struct GameLocationPathKey : IEquatable<GameLocationPathKey>
    {
        public NewGameLocation originLocation;
        public NewGameLocation destinationLocation;
        private bool hasHash;
        private int hash;

        public GameLocationPathKey(NewGameLocation locOne, NewGameLocation locTwo)
        {
            originLocation = locOne;
            destinationLocation = locTwo;
            hasHash = false;
            hash = 0;
        }

        public override int GetHashCode()
        {
            if (!hasHash)
            {
                hash = HashCode.Combine(originLocation, destinationLocation);
                hasHash = true;
            }
            return hash;
        }

        bool IEquatable<GameLocationPathKey>.Equals(GameLocationPathKey other)
        {
            return other.GetHashCode() == GetHashCode();
        }
    }

    //List of routes from one room that outputs a list of entrances...
    //public Dictionary<RoomPathKey, List<NewNode.NodeAccess>> roomRoutes = new Dictionary<RoomPathKey, List<NewNode.NodeAccess>>();
    public Dictionary<GameLocationPathKey, List<NewNode.NodeAccess>> gameLocationRoutes = new Dictionary<GameLocationPathKey, List<NewNode.NodeAccess>>(INITIAL_COLLECTION_SIZE);

    [Header("AI Navigation")]
    public Dictionary<NewAddress.PathKey, List<NewNode.NodeAccess>> internalRoutes = new Dictionary<NewAddress.PathKey, List<NewNode.NodeAccess>>(INITIAL_COLLECTION_SIZE);

    //The street behaves as it's own gamelocation even though it's made of several. Keep track of it here...
    public List<NewNode.NodeAccess> streetEntrances = new List<NewNode.NodeAccess>(INITIAL_COLLECTION_SIZE); //Entrances/exits to this

    //The job system requires non-nullable values, so use a reference dictionary for nodeAccess
    public Dictionary<int, NewNode.NodeAccess> nodeAccessReference = new Dictionary<int, NewNode.NodeAccess>(INITIAL_COLLECTION_SIZE);

    //Used for job pathfinding
    public NativeMultiHashMap<float3, int> streetAccessRef; //Use a node coord to access multiple references to access
    public NativeHashMap<int, float3> streetAccessPositions; //Use a access reference to access node positions of the access (for heuristics)
    public NativeHashMap<int, float3> streetToNodeReference; //Use as a reference to the 'to' node
    public NativeList<float3> streetNoPassRef;

    //Street chunks
    public List<StreetChunk> streetChunks = new List<StreetChunk>(INITIAL_COLLECTION_SIZE);

    //[System.Serializable]
    public class StreetChunk
    {
        public string name;
        public int id = 0;
        public static int assignID = 0;
        public Vector3 anchorTile;
        public List<Vector3> allCoords = new List<Vector3>();
        public List<NewTile> allTiles = new List<NewTile>();
        public bool isJunction = false;
        public bool isHorizontal = true;
        public float xMagnitude = 0f;
        public float yMagnitude = 0f;
        public Vector2 streetMaxSizeX = new Vector2(9999, -9999);
        public Vector2 streetMaxSizeY = new Vector2(9999, -9999);

        //Traffic simulation
        public int footfall = 0;
        public float footfallNormalized = 0f;

        public StreetChunk(Vector3 newAnchor, List<Vector3> newList, bool newIsJunction)
        {
            id = assignID;
            assignID++;

            anchorTile = newAnchor;
            allCoords = newList;
            isJunction = newIsJunction;

            //Calculate horizontal
            //Set street chunks
            foreach (Vector3 v3 in allCoords)
            {
                streetMaxSizeX = new Vector2(Mathf.Min(streetMaxSizeX.x, v3.x), Mathf.Max(streetMaxSizeX.y, v3.x));
                streetMaxSizeY = new Vector2(Mathf.Min(streetMaxSizeY.x, v3.y), Mathf.Max(streetMaxSizeY.y, v3.y));

                NewTile foundTile;

                if (PathFinder.Instance.tileMap.TryGetValue(v3, out foundTile))
                {
                    allTiles.Add(foundTile);
                    foundTile.streetChunk = this;
                }
            }

            xMagnitude = Mathf.Abs(streetMaxSizeX.y - streetMaxSizeX.x);
            yMagnitude = Mathf.Abs(streetMaxSizeY.y - streetMaxSizeY.x);

            if (yMagnitude > xMagnitude) isHorizontal = false;
            else isHorizontal = true;

            if(isJunction)
            {
                name = "Junction " + id;
            }
            else
            {
                name = "Road " + id;

                if (isHorizontal)
                {
                    name = "Horizontal " + name;
                }
                else
                {
                    name = "Vertical " + name;
                }
            }
        }

        //Returns surrounding chunks and whether they are facing in the same direction
        public Dictionary<StreetChunk, bool> GetAdjacentChunks(bool horizontal)
        {
            Dictionary<StreetChunk, bool> ret = new Dictionary<StreetChunk, bool>(INITIAL_COLLECTION_SIZE);

            foreach(Vector3 t in allCoords)
            {
                foreach (Vector2 v2 in CityData.Instance.offsetArrayX4)
                {
                    Vector3 tileSearch = t + new Vector3(v2.x, v2.y, 0);
                    NewTile foundTile;

                    if(PathFinder.Instance.tileMap.TryGetValue(tileSearch, out foundTile))
                    {
                        if(foundTile.streetChunk != null)
                        {
                            if(!ret.ContainsKey(foundTile.streetChunk))
                            {
                                bool horiz = true;
                                if (v2.y != 0) horiz = false;

                                bool sameDir = false;
                                if (horizontal == horiz) sameDir = true;

                                ret.Add(foundTile.streetChunk, sameDir);
                            }
                        }
                    }
                }
            }

            return ret;
        }
    }

    //Singleton pattern
    private static PathFinder _instance;
    public static PathFinder Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }

    private void Start()
    {
        SetDimensions();
    }

    public void SetDimensions()
    {
        if(SessionData.Instance.isDialogEdit)
        {
            return;
        }

        //If this is the floor editor, override the city size to 1x1
        if (SessionData.Instance.isFloorEdit)
        {
            CityData.Instance.citySize = new Vector2(1, 1);
        }

        //Fill in the base for the pathfinding map
        //Pre-calculate city dimensions for the pathfind map.
        tileSize = new Vector3(CityControls.Instance.cityTileSize.x / CityControls.Instance.tileMultiplier, CityControls.Instance.cityTileSize.y / CityControls.Instance.tileMultiplier, CityControls.Instance.cityTileSize.z / CityControls.Instance.tileMultiplier);
        nodeSize = new Vector3(PathFinder.Instance.tileSize.x / CityControls.Instance.nodeMultiplier, PathFinder.Instance.tileSize.y / CityControls.Instance.nodeMultiplier, PathFinder.Instance.tileSize.z);

        citySizeReal = new Vector2(CityData.Instance.citySize.x * CityControls.Instance.cityTileSize.x, CityData.Instance.citySize.y * CityControls.Instance.cityTileSize.y);
        halfCitySizeReal = citySizeReal * 0.5f;
        tileCitySize = CityData.Instance.citySize * CityControls.Instance.tileMultiplier;
        nodeCitySize = PathFinder.Instance.tileCitySize * CityControls.Instance.nodeMultiplier;
    }

    //Compile the pathfinding map & generate neighbors
    public void CompilePathFindingMap()
    {
        Game.Log("CityGen: Compiling pathfinding map...");
        Game.Log("Pathfinder: Compiling pathfinding map...");

        //Fill pathfinding map with street locations
        for (int i = 0; i < CityData.Instance.citySize.x; i++)
        {
            for (int n = 0; n < CityData.Instance.citySize.y; n++)
            {
                CityTile currentCityTile = CityData.Instance.cityTiles[new Vector2Int(i, n)];

                //Get save data if relevent
                CitySaveData.CityTileCitySave saveData = null;
                if (!CityConstructor.Instance.generateNew) saveData = CityConstructor.Instance.currentData.cityTiles.Find(item => item.cityCoord == currentCityTile.cityCoord);

                //Zero point of the groundMap tile.
                Vector2Int localZero = new Vector2Int(Mathf.RoundToInt(i * CityControls.Instance.tileMultiplier), Mathf.RoundToInt(n * CityControls.Instance.tileMultiplier));

                //Fill streets pathfinding map...
                for (int r = 0; r < CityControls.Instance.tileMultiplier; r++)
                {
                    for (int p = 0; p < CityControls.Instance.tileMultiplier; p++)
                    {
                        //Is this the edge within range of 1, if so this is a street tile...
                        if ((r == 0 || r == CityControls.Instance.tileMultiplier - 1) || (p == 0 || p == CityControls.Instance.tileMultiplier - 1))
                        {
                            Vector3Int currentTileCoord = new Vector3Int(localZero.x + r, localZero.y + p, 0); //Current tile coord

                            //Do not create street if this is 4 or closer to the edge
                            if(currentTileCoord.x <= 5 || currentTileCoord.x >= CityData.Instance.citySize.x * CityControls.Instance.tileMultiplier - 6)
                            {
                                continue;
                            }
                            else if (currentTileCoord.y <= 5 || currentTileCoord.y >= CityData.Instance.citySize.y * CityControls.Instance.tileMultiplier - 6)
                            {
                                continue;
                            }

                            NewTile newTile = new NewTile();
                            newTile.name = "Pathfinder Created Tile " + currentTileCoord;

                            //Get save data if relevent
                            CitySaveData.TileCitySave tileData = null;

                            if (!CityConstructor.Instance.generateNew)
                            {
                                //Instead of running a setup here, leave that to the loading of buildings: Instead add to the pathfinding map so you can complete the rest of this setup...
                                tileData = saveData.outsideTiles.Find(item => item.globalTileCoord == currentTileCoord);
                                newTile.LoadPathfindTileData(tileData);
                            }
                            else
                            {
                                //Instead of running a setup here, leave that to the loading of buildings: Instead add to the pathfinding map so you can complete the rest of this setup...
                                newTile.globalTileCoord = currentTileCoord;
                            }

                            newTile.SetAsOutside(true);
                            newTile.cityTile = currentCityTile;
                            newTile.isEdge = true; //This is an edge tile
                            tileMap.Add(currentTileCoord, newTile);
                        }
                    }
                }
            }
        }

        if(CityConstructor.Instance.generateNew)
        {
            //Junctions need entranaces completed for detection.
            CreateStreetChunks();

            //Calculate which directions buildings should be facing: Only needs to be done on generation
            foreach (NewBuilding bc in CityData.Instance.buildingDirectory)
            {
                bc.CalculateFacing();
            }

            //Setup exterior tiles
            foreach (KeyValuePair<Vector3, NewTile> pair in tileMap)
            {
                if (!pair.Value.isSetup)
                {
                    if (pair.Value.streetController == null)
                    {
                        Game.LogError("CityGen: Tile " + pair.Value.globalTileCoord + " has no street controller assigned! Street count: " + CityData.Instance.streetDirectory.Count + " street chunk" + pair.Value.streetChunk);
                        continue;
                    }

                    pair.Value.SetupExterior(pair.Value.cityTile, pair.Value.globalTileCoord);
                    pair.Value.parent = pair.Value.streetController.transform;
                }
            }
        }
        //Load in streets
        else
        {
            foreach (CitySaveData.StreetCitySave street in CityConstructor.Instance.currentData.streets)
            {
                GameObject roadObj = Instantiate(PrefabControls.Instance.street, PrefabControls.Instance.cityContainer.transform);
                StreetController r = roadObj.GetComponent<StreetController>();
                r.Load(street); //This process will also load exterior tiles if needed...

                if (!CityData.Instance.streetDirectory.Contains(r))
                {
                    CityData.Instance.streetDirectory.Add(r);
                }
            }
        }

        //Load street tiles...
        foreach (StreetController st in CityData.Instance.streetDirectory)
        {
            st.LoadSections();
        }
    }

    //Split street areas into chunks; junctions and alleys
    public void CreateStreetChunks()
    {
        List<Vector3> junctionTiles = new List<Vector3>(INITIAL_COLLECTION_SIZE);
        List<Vector3> streetAreaTiles = new List<Vector3>(INITIAL_COLLECTION_SIZE);

        int tileSizeX = Mathf.RoundToInt(CityData.Instance.citySize.x * CityControls.Instance.tileMultiplier);
        int tileSizeY = Mathf.RoundToInt(CityData.Instance.citySize.y * CityControls.Instance.tileMultiplier);

        int junctCursorX = 0;
        int junctCursorY = 0;

        //Flood fill to determin separate chunks...
        HashSet<Vector3> openSet = new HashSet<Vector3>(INITIAL_COLLECTION_SIZE);
        HashSet<Vector3> closedSet = new HashSet<Vector3>(INITIAL_COLLECTION_SIZE);

        for (int i = 0; i < tileSizeX; i++)
        {
            for (int u = 0; u < tileSizeY; u++)
            {
                Vector3 tileMapCoord = new Vector3(i, u, 0);
                NewTile foundTile = null;

                if(tileMap.TryGetValue(tileMapCoord, out foundTile))
                {
                    //Junction cursor 0 or 4 means this is a junction area
                    if((junctCursorX == 0 || junctCursorX == 6) && (junctCursorY == 0 || junctCursorY == 6))
                    {
                        junctionTiles.Add(foundTile.globalTileCoord);
                    }
                    //Otherwise this is a street area
                    else
                    {
                        streetAreaTiles.Add(foundTile.globalTileCoord);
                    }

                    openSet.Add(foundTile.globalTileCoord);

                    if (Game.Instance.displayStreetChunks)
                    {
                        GameObject newObj = Instantiate(PrefabControls.Instance.streetChunkDebug, PrefabControls.Instance.mapContainer);
                        newObj.transform.position = CityData.Instance.TileToRealpos(foundTile.globalTileCoord);
                        newObj.name = "Street Area " + junctCursorX + ","+ junctCursorY + "("+ foundTile.globalTileCoord + ")";
                    }
                }

                junctCursorY++;
                if (junctCursorY >= CityControls.Instance.tileMultiplier) junctCursorY = 0;
            }

            junctCursorX++;
            if (junctCursorX >= CityControls.Instance.tileMultiplier) junctCursorX = 0;
        }

        //Flood fill to determin separate chunks...
        while(openSet.Count > 0)
        {
            Vector3 currentTile = openSet.First();
            closedSet.Add(currentTile);

            HashSet<Vector3> currentOpenChunk = new HashSet<Vector3>(INITIAL_COLLECTION_SIZE);
            HashSet<Vector3> currentClosedChunk = new HashSet<Vector3>(INITIAL_COLLECTION_SIZE);
            HashSet<Vector3> currentGroup = new HashSet<Vector3>(INITIAL_COLLECTION_SIZE);
            currentGroup.Add(currentTile);
            currentOpenChunk.Add(currentTile);

            //Is this a junction or street area?
            bool isJunction = false;

            if(junctionTiles.Contains(currentTile))
            {
                isJunction = true;
            }

            while(currentOpenChunk.Count > 0)
            {
                Vector3 currentFill = currentOpenChunk.First();

                foreach (Vector2 v2 in CityData.Instance.offsetArrayX4)
                {
                    Vector3 tileCoord = currentFill + new Vector3(v2.x, v2.y, 0);

                    if (!closedSet.Contains(tileCoord) && !currentClosedChunk.Contains(tileCoord) && !currentOpenChunk.Contains(tileCoord))
                    {
                        //Is found a matching type (junction/street)?
                        if (isJunction && junctionTiles.Contains(tileCoord))
                        {
                            currentOpenChunk.Add(tileCoord);
                            currentGroup.Add(tileCoord);

                            if (!closedSet.Contains(tileCoord)) closedSet.Add(tileCoord);
                            if (openSet.Contains(tileCoord)) openSet.Remove(tileCoord);
                        }
                        else if (!isJunction && streetAreaTiles.Contains(tileCoord))
                        {
                            currentOpenChunk.Add(tileCoord);
                            currentGroup.Add(tileCoord);

                            if (!closedSet.Contains(tileCoord)) closedSet.Add(tileCoord);
                            if (openSet.Contains(tileCoord)) openSet.Remove(tileCoord);
                        }
                    }

                    if(!currentClosedChunk.Contains(currentFill)) currentClosedChunk.Add(currentFill);
                }

                if (!currentClosedChunk.Contains(currentFill)) currentClosedChunk.Add(currentFill);
                currentOpenChunk.Remove(currentFill);

                if (!closedSet.Contains(currentFill)) closedSet.Add(currentFill);
                if (openSet.Contains(currentFill)) openSet.Remove(currentFill);
            }

            if(!closedSet.Contains(currentTile)) closedSet.Add(currentTile);
            openSet.Remove(currentTile);

            //Create new street chunk
            streetChunks.Add(new StreetChunk(currentTile, currentGroup.ToList(), isJunction));
        }

        //Simulate foot traffic among streets
        FootTrafficSimulation();

        //Spawn debug display
        if(Game.Instance.displayStreetAndJunctionChunks)
        {
            foreach(StreetChunk chunk in streetChunks)
            {
                GameObject newObj = null;

                if (chunk.isJunction)
                {
                    newObj = Instantiate(PrefabControls.Instance.junctionChunkDebug, PrefabControls.Instance.mapContainer);
                    newObj.transform.position = CityData.Instance.TileToRealpos(chunk.anchorTile);
                    newObj.name = "Junction " + chunk.anchorTile;

                    if (Game.Instance.displayTrafficSimulationResults)
                    {
                        MeshRenderer rend = newObj.GetComponentInChildren<MeshRenderer>();
                        rend.material = Instantiate(Game.Instance.debugtrafficSimMaterial);
                        rend.material.SetColor("_BaseColor", Color.Lerp(Color.white, Color.red, chunk.footfallNormalized));
                    }
                }
                else
                {
                    foreach (Vector3 v3 in chunk.allCoords)
                    {
                        newObj = Instantiate(PrefabControls.Instance.streetAreaChunkDebug, PrefabControls.Instance.mapContainer);
                        newObj.transform.position = CityData.Instance.TileToRealpos(v3);
                        newObj.name = "Street Area " + chunk.anchorTile;

                        if (Game.Instance.displayTrafficSimulationResults)
                        {
                            MeshRenderer rend = newObj.GetComponentInChildren<MeshRenderer>();
                            rend.material = Instantiate(Game.Instance.debugtrafficSimMaterial);
                            rend.material.SetColor("_BaseColor", Color.Lerp(Color.white, Color.red, chunk.footfallNormalized));
                        }
                    }
                }
            }
        }

        //Generate streets and block alleys...
        CreateStreets();
    }

    //Do a foot traffic simulation
    private void FootTrafficSimulation()
    {
        int simulationCount = Mathf.CeilToInt(CityData.Instance.citySize.x * CityData.Instance.citySize.y);

        //Pick starting points closer to the centre
        List<Vector3> startingPoints = new List<Vector3>(INITIAL_COLLECTION_SIZE);
        List<Vector3> allPoints = new List<Vector3>(INITIAL_COLLECTION_SIZE);

        Vector3 centre = new Vector3(CityData.Instance.citySize.x * CityControls.Instance.tileMultiplier * 0.5f, CityData.Instance.citySize.y * CityControls.Instance.tileMultiplier * 0.5f, 0);

        foreach(KeyValuePair<Vector3, NewTile> pair in tileMap)
        {
            allPoints.Add(pair.Key);

            //Add tiles near the centre multiple times...
            float distFromCentre = Vector3.Distance(pair.Key, centre);
            float distLerp = Mathf.InverseLerp(0f, Mathf.Max(CityData.Instance.citySize.x * 0.5f, CityData.Instance.citySize.y * 0.5f), distFromCentre);

            int freq = Mathf.Max(Mathf.CeilToInt((1f - distLerp) * 10), 1);

            for (int i = 0; i < freq; i++)
            {
                startingPoints.Add(pair.Key);
            }
        }

        Dictionary<Vector3, int> trafficCount = new Dictionary<Vector3, int>(INITIAL_COLLECTION_SIZE);

        string seed = CityData.Instance.seed;

        for (int i = 0; i < simulationCount; i++)
        {
            Vector3 start = startingPoints[Toolbox.Instance.GetPsuedoRandomNumberContained(0, startingPoints.Count, seed, out seed)];
            Vector3 end = allPoints[Toolbox.Instance.GetPsuedoRandomNumberContained(0, allPoints.Count, seed, out seed)];

            List<NewTile> path = GetTileRoute(tileMap[start], tileMap[end]);

            //Add traffic to tile
            foreach(NewTile t in path)
            {
                if(!trafficCount.ContainsKey(t.globalTileCoord))
                {
                    trafficCount.Add(t.globalTileCoord, 0);
                }

                trafficCount[t.globalTileCoord]++;
            }
        }

        int maxFootfall = 0;

        //Apply to street chunks
        foreach(StreetChunk chunk in streetChunks)
        {
            chunk.footfall = 0;

            foreach(Vector3 v3 in chunk.allCoords)
            {
                if(trafficCount.ContainsKey(v3))
                {
                    chunk.footfall += trafficCount[v3];
                }
            }

            maxFootfall = Mathf.Max(maxFootfall, chunk.footfall);
        }

        //Normalized footfall
        foreach(StreetChunk chunk in streetChunks)
        {
            chunk.footfallNormalized = (float)chunk.footfall / (float)maxFootfall;
        }
    }

    //Create streets
    private void CreateStreets()
    {
        int maxStreetSize = 4; //Max street size in chunks... (Original value = 6)

        string seed = CityData.Instance.seed;

        //Sort into junctions/street area
        List<StreetChunk> toBeAssigned = new List<StreetChunk>(streetChunks);

        //Sort by highest footfall
        toBeAssigned.Sort((p1, p2) => p2.footfall.CompareTo(p1.footfall)); //Highest first

        while (toBeAssigned.Count > 0)
        {
            StreetChunk startingChunk = toBeAssigned[0];

            List<StreetChunk> openSet = new List<StreetChunk>(INITIAL_COLLECTION_SIZE);
            openSet.Add(startingChunk);

            //Create street for this
            int streetSize = 1;
            StreetController currentStreet = NewRoad(tileMap[startingChunk.anchorTile].cityTile.district);

            while (openSet.Count > 0)
            {
                StreetChunk currentChunk = openSet[0];

                //Add current to street
                foreach (NewTile t in currentChunk.allTiles)
                {
                    currentStreet.AddTile(t);
                }

                currentStreet.AddChunk(currentChunk);

                //Set street footfall
                currentStreet.normalizedFootfall = Mathf.Max(currentStreet.normalizedFootfall, currentChunk.footfallNormalized);
                currentStreet.chunkSize++;

                toBeAssigned.Remove(currentChunk);

                if (streetSize < maxStreetSize)
                {
                    //Which way is the road facing so far?
                    bool horizonal = true;
                    Vector2 streetMaxSizeX = new Vector2(9999, -9999);
                    Vector2 streetMaxSizeY = new Vector2(9999, -9999);

                    foreach (NewTile t in currentStreet.tiles)
                    {
                        streetMaxSizeX = new Vector2(Mathf.Min(streetMaxSizeX.x, t.globalTileCoord.x), Mathf.Max(streetMaxSizeX.x, t.globalTileCoord.x));
                        streetMaxSizeY = new Vector2(Mathf.Min(streetMaxSizeY.y, t.globalTileCoord.y), Mathf.Max(streetMaxSizeY.y, t.globalTileCoord.y));
                    }

                    if (Mathf.Abs(streetMaxSizeY.y - streetMaxSizeY.x) > Mathf.Abs(streetMaxSizeX.y - streetMaxSizeX.x)) horizonal = false;
                    else horizonal = true;

                    Dictionary<StreetChunk, bool> surrounding = currentChunk.GetAdjacentChunks(horizonal);

                    StreetChunk nextChunk = null;
                    float nextChunkTrafficScore = -1;

                    foreach (KeyValuePair<StreetChunk, bool> surr in surrounding)
                    {
                        if (surr.Key == currentChunk) continue;

                        float extraScore = 0;

                        if (surr.Value)
                        {
                            extraScore = 0.2f;
                        }
                        //If not in same direction, consider skipping...
                        else if (Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) > 0.5f)
                        {
                            continue;
                        }

                        if (!toBeAssigned.Contains(surr.Key) || openSet.Contains(surr.Key))
                        {
                            continue;
                        }
                        else if (surr.Key.footfallNormalized + extraScore > nextChunkTrafficScore)
                        {
                            nextChunk = surr.Key;
                            nextChunkTrafficScore = surr.Key.footfall;
                        }
                    }

                    //Did we find a chunk to assign?
                    if (nextChunk != null)
                    {
                        openSet.Add(nextChunk);
                        streetSize++;
                    }
                }

                openSet.RemoveAt(0);
            }
        }

        //Assign streets as main streets, side streets and alleys
        Dictionary<StreetController, List<NewTile>> splitAlleys = new Dictionary<StreetController, List<NewTile>>(INITIAL_COLLECTION_SIZE);

        foreach (StreetController st in CityData.Instance.streetDirectory)
        {
            //If chunk size is 2 or less this is an alleyway.
            if (st.chunkSize <= 1)
            {
                if (st.tiles.Count > 6 && (st.normalizedFootfall < 0.6f || Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed) > 0.66f))
                {
                    //Split street to have a dead end...
                    List<NewTile> splitTiles = new List<NewTile>(INITIAL_COLLECTION_SIZE);
                    List<NewTile> validSplitHorizontal = new List<NewTile>(INITIAL_COLLECTION_SIZE);
                    List<NewTile> validSplitVertical = new List<NewTile>(INITIAL_COLLECTION_SIZE);

                    bool buildingPassCheck = true;

                    foreach (NewTile t in st.tiles)
                    {
                        if (!t.streetChunk.isJunction)
                        {
                            if (!validSplitHorizontal.Exists(item => item.globalTileCoord.x == t.globalTileCoord.x))
                            {
                                validSplitHorizontal.Add(t);
                            }
                            else if (!validSplitVertical.Exists(item => item.globalTileCoord.y == t.globalTileCoord.y))
                            {
                                validSplitVertical.Add(t);
                            }
                        }

                        if(t.cityTile.building != null && !t.cityTile.building.preset.enableAlleywayWalls)
                        {
                            buildingPassCheck = false;
                            break;
                        }
                    }

                    if(buildingPassCheck)
                    {
                        if (validSplitHorizontal.Count > validSplitVertical.Count && validSplitHorizontal.Count > 2)
                        {
                            //Split horizontally...
                            NewTile splitAtTile = validSplitHorizontal[Toolbox.Instance.GetPsuedoRandomNumberContained(1, validSplitHorizontal.Count - 1, seed, out seed)];

                            foreach (NewTile t in st.tiles)
                            {
                                if (t.globalTileCoord.x >= splitAtTile.globalTileCoord.x)
                                {
                                    splitTiles.Add(t);
                                }
                            }
                        }
                        else if (validSplitVertical.Count > 2)
                        {
                            //Split vertically...
                            NewTile splitAtTile = validSplitVertical[Toolbox.Instance.GetPsuedoRandomNumberContained(1, validSplitVertical.Count - 1, seed, out seed)];

                            foreach (NewTile t in st.tiles)
                            {
                                if (t.globalTileCoord.y >= splitAtTile.globalTileCoord.y)
                                {
                                    splitTiles.Add(t);
                                }
                            }
                        }

                        if (splitTiles.Count > 0)
                        {
                            splitAlleys.Add(st, splitTiles);
                        }

                        st.SetAsAlley();
                    }
                    else st.SetAsBackstreet();
                }
                else
                {
                    st.SetAsBackstreet();
                }
            }
            //Otherwise, it's a backstreet
            else if (st.normalizedFootfall >= 0.5f)
            {
                st.SetAsStreet();
            }
            else st.SetAsBackstreet();
        }

        //Calculate and load street tiles
        foreach(StreetController st in CityData.Instance.streetDirectory)
        {
            //Calculate and load-in street tiles
            st.LoadStreetTiles();
        }

        //Split off alley ways
        foreach (KeyValuePair<StreetController, List<NewTile>> pair in splitAlleys)
        {
            if (pair.Value.Count <= 0) continue;
            if (pair.Key.tiles.Count == pair.Value.Count) continue; //This is split at the end, meaning we don't have to make another street
            if (pair.Key.tiles.Count <= 6) continue; //Must be bigger than just a junction...

            //We need to check if a tile from each side of the split can still access other streets...
            bool pathPass = true;

            NewTile splitRandomTile = pair.Value[0];
            List<NewTile> originalTiles = pair.Key.tiles.FindAll(item => !pair.Value.Contains(item));

            foreach (StreetController otherStreet in CityData.Instance.streetDirectory)
            {
                if (otherStreet == pair.Key) continue;
                if (otherStreet.tiles.Count <= 0) continue;

                NewTile firstTile = otherStreet.tiles[0];

                if (originalTiles != null && originalTiles.Count > 0)
                {
                    List<NewTile> path = GetTileRoute(splitRandomTile, firstTile, originalTiles);

                    if (path == null || path.Count <= 0)
                    {
                        pathPass = false;
                        break;
                    }

                    path = GetTileRoute(originalTiles[0], firstTile, pair.Value);

                    if (path == null || path.Count <= 0)
                    {
                        pathPass = false;
                        break;
                    }
                }
            }

            if(pathPass)
            {
                StreetController newAlley = NewRoad(pair.Value[0].cityTile.district);

                //Add shared elements
                newAlley.sharedGroundElements.Add(pair.Key);
                pair.Key.sharedGroundElements.Add(newAlley);

                foreach (NewTile t in pair.Value)
                {
                    pair.Key.RemoveTile(t);
                    newAlley.AddTile(t);
                }

                newAlley.SetAsAlley();
            }
        }

        //Display streets
        if (Game.Instance.displayStreets)
        {
            foreach (StreetController st in CityData.Instance.streetDirectory)
            {
                foreach (NewTile t in st.tiles)
                {
                    GameObject newObj = Instantiate(PrefabControls.Instance.streetNodeDebug, PrefabControls.Instance.mapContainer);
                    newObj.transform.position = CityData.Instance.TileToRealpos(t.globalTileCoord);
                    newObj.name = "Street " + st.streetID + " (" + st.normalizedFootfall + ")";

                    MeshRenderer rend = newObj.GetComponentInChildren<MeshRenderer>();
                    rend.material = Instantiate(Game.Instance.debugStreetMaterial);
                    rend.material.SetColor("_BaseColor", Game.Instance.streetDebugColours[st.streetID % Game.Instance.streetDebugColours.Count]);
                }
            }
        }
    }

    //New Road
    private StreetController NewRoad(DistrictController dis)
    {
        GameObject roadObj = Instantiate(PrefabControls.Instance.street, PrefabControls.Instance.cityContainer.transform);
        StreetController r = roadObj.GetComponent<StreetController>();
        r.Setup(dis);

        if (!CityData.Instance.streetDirectory.Contains(r))
        {
            CityData.Instance.streetDirectory.Add(r);
        }

        return r;
    }

    //New pathfinding...
    public PathData GetPath(NewNode origin, NewNode destination, Human human, NewNode[] avoidNodes = null)
    {
        totalPathCalls++;

        if(origin == null)
        {
            Game.LogError("Pathfinder: Origin is null!");
            return null;
        }
        else if(destination == null)
        {
            Game.LogError("Pathfinder: destination is null");
            return null;
        }

        //If origin or destination is not walkable, pick the closest node instead...
        if(origin.isInaccessable || origin.noAccess)
        {
            List<NewNode> searchForOriginOpenSet = new List<NewNode>(INITIAL_COLLECTION_SIZE);
            List<NewNode> searchForOriginClosedSet = new List<NewNode>(INITIAL_COLLECTION_SIZE);
            int searchOriginSafety = 99;
            bool foundThis = false;

            searchForOriginOpenSet.Add(origin);

            while(searchForOriginOpenSet.Count > 0 && searchOriginSafety > 0)
            {
                NewNode searchNode = searchForOriginOpenSet[0];

                foreach (Vector2 v2 in CityData.Instance.offsetArrayX4)
                {
                    Vector3 searchVector = new Vector3(searchNode.nodeCoord.x + v2.x, searchNode.nodeCoord.y + v2.y, searchNode.nodeCoord.z);
                    NewNode foundNode = null;

                    if (nodeMap.TryGetValue(searchVector, out foundNode))
                    {
                        if (!foundNode.isInaccessable)
                        {
                            origin = foundNode;
                            foundThis = true;
                            break;
                        }

                        if(!searchForOriginOpenSet.Contains(foundNode) && !searchForOriginClosedSet.Contains(foundNode))
                        {
                            searchForOriginOpenSet.Add(foundNode);
                        }
                    }
                }

                if (foundThis)
                {
                    break;
                }
                else
                {
                    searchForOriginClosedSet.Add(searchNode);
                    searchForOriginOpenSet.RemoveAt(0);
                    searchOriginSafety--;
                }
            }
        }

        //If origin or destination is not walkable, pick the closest node instead...
        if (destination.isInaccessable || destination.noAccess)
        {
            List<NewNode> searchForDestOpenSet = new List<NewNode>(INITIAL_COLLECTION_SIZE);
            List<NewNode> searchForDestClosedSet = new List<NewNode>(INITIAL_COLLECTION_SIZE);
            int searchDestSafety = 99;
            bool foundThis = false;

            searchForDestOpenSet.Add(origin);

            while (searchForDestOpenSet.Count > 0 && searchDestSafety > 0)
            {
                NewNode searchNode = searchForDestOpenSet[0];

                foreach (Vector2 v2 in CityData.Instance.offsetArrayX4)
                {
                    Vector3 searchVector = new Vector3(searchNode.nodeCoord.x + v2.x, searchNode.nodeCoord.y + v2.y, searchNode.nodeCoord.z);
                    NewNode foundNode = null;

                    if (nodeMap.TryGetValue(searchVector, out foundNode))
                    {
                        if (!foundNode.isInaccessable)
                        {
                            destination = foundNode;
                            foundThis = true;
                            break;
                        }

                        if (!searchForDestOpenSet.Contains(foundNode) && !searchForDestClosedSet.Contains(foundNode))
                        {
                            searchForDestOpenSet.Add(foundNode);
                        }
                    }
                }
                if (foundThis)
                {
                    break;
                }
                else
                {
                    searchForDestClosedSet.Add(searchNode);
                    searchForDestOpenSet.RemoveAt(0);
                    searchDestSafety--;
                }
            }
        }

        //Phase 1: Which rooms will I need to pass through to get here?
        //Only bother doing this if the rooms are different...
        List<NewNode.NodeAccess> locationRoute = new List<NewNode.NodeAccess>(INITIAL_COLLECTION_SIZE);

        if (origin.gameLocation != destination.gameLocation)
        {
            //Get list of entrances from origin to desitnation
            locationRoute = GetGameLocationRoute(origin, destination);
        }

        if(locationRoute == null)
        {
            if (human.ai != null)
            {
                try
                {
                    Game.LogError("Location route is null for " + human.name + " on action " + human.ai.currentAction.preset.name + ": " + human.ai.currentAction.node.gameLocation.name + ", " + human.ai.currentAction.node.room.name + ", " + human.ai.currentAction.node.nodeCoord + " (" + human.ai.currentAction.node.position + ")");
                }
                catch
                {

                }
                
            }

            return null;
        }

        //We now have a list of rooms (in the form of entrances) that we need to go through to get to the destination
        //Phase 2: Using this list, split the journey into parts and use the same technique to compile a full route
        //Room routes can be saved in the room class

        //Create the output list, add found list elements to this...
        List<NewNode.NodeAccess> totalOutput = new List<NewNode.NodeAccess>(INITIAL_COLLECTION_SIZE);

        //foreach(NewNode.NodeAccess acc in roomRoute)
        //{
        //    Game.Log(acc.fromNode.name + " - > " + acc.toNode.name);
        //}

        NewNode currentNode = origin;

        //Start by finding the route from this node
        //This is ideal to be calculated using the job system as game location routes can be calculated in parallel.
        if(Game.Instance.useJobSystem)
        {
            NativeList<JobHandle> jobHandles = new NativeList<JobHandle>(Allocator.Temp); //List of job handles
            List<GetInternalRouteJob> jobReferences = new List<GetInternalRouteJob>(INITIAL_COLLECTION_SIZE);

            //List of internal routes found through dictionary cache
            Dictionary<int, List<NewNode.NodeAccess>> cachedRoutes = new Dictionary<int, List<NewNode.NodeAccess>>(INITIAL_COLLECTION_SIZE);

            //Because the location route consists of access between 2 gamelocations, the destination is sometimes not the same as where we end up: A final route calculation is often needed...
            for (int i = 0; i < locationRoute.Count + 1; i++)
            {
                //Game.Log(i + " == " + Mathf.Clamp(i, 0, Mathf.Max(locationRoute.Count - 1, 0)) + " collection: " + locationRoute.Count);

                NewNode.NodeAccess current = null;
                NewNode nextNode = null;

                if(i >= 0 && i < locationRoute.Count)
                {
                    current = locationRoute[i];
                    nextNode = current.GetOtherGameLocation(currentNode);
                }

                //If at the end...
                if (i == locationRoute.Count)
                {
                    if (currentNode != destination)
                    {
                        //Because the location route consists of access between 2 gamelocations, the destination is sometimes not the same as where we end up: A final route calculation is often needed...
                        nextNode = destination;
                    }
                    else break;
                }

                //Get from the origin to the inside of the next room
                if (currentNode != nextNode)
                {
                    NewAddress.PathKey pathKey = new NewAddress.PathKey(currentNode, nextNode);

                    //No need to start a job if path key already exists...
                    //Try regular key
                    bool found = false;

                    if (Game.Instance.useInternalRouteCaching && SessionData.Instance.startedGame)
                    {
                        List<NewNode.NodeAccess> cached = null;

                        if (currentNode.gameLocation.thisAsAddress != null)
                        {
                            if (currentNode.gameLocation.thisAsAddress.internalRoutes.TryGetValue(pathKey, out cached))
                            {
                                cachedRoutes.Add(i, cached);
                                found = true;
                            }
                        }
                        else
                        {
                            if (PathFinder.Instance.internalRoutes.TryGetValue(pathKey, out cached))
                            {
                                cachedRoutes.Add(i, cached);
                                found = true;
                            }
                        }
                    }

                    //If not found we need to make a job...
                    if (!found)
                    {
                        if (currentNode.gameLocation.thisAsAddress != null)
                        {
                            GetInternalRouteJob newJob = new GetInternalRouteJob
                            {
                                origin = Toolbox.Instance.ToFloat3(pathKey.origin.nodeCoord),
                                destination = Toolbox.Instance.ToFloat3(pathKey.destination.nodeCoord),
                                output = new NativeList<int>(Allocator.TempJob), //Read this for output
                                listIndex = i,

                                //Pass from the gamelocation
                                accessRef = currentNode.gameLocation.thisAsAddress.accessRef,
                                accessPositions = currentNode.gameLocation.thisAsAddress.accessPositions,
                                toNodeReference = currentNode.gameLocation.thisAsAddress.toNodeReference,
                                noPassRef = currentNode.gameLocation.thisAsAddress.noPassRef
                            };

                            jobReferences.Add(newJob);

                            //Create job handle
                            JobHandle newHandle = newJob.Schedule();
                            jobHandles.Add(newHandle); //Add to list
                        }
                        else
                        {
                            //Force this to be done on the main thread
                            if(!Game.Instance.forceStreetPathsOnMainThread)
                            {
                                GetInternalRouteJob newJob = new GetInternalRouteJob
                                {
                                    origin = Toolbox.Instance.ToFloat3(pathKey.origin.nodeCoord),
                                    destination = Toolbox.Instance.ToFloat3(pathKey.destination.nodeCoord),
                                    output = new NativeList<int>(Allocator.TempJob), //Read this for output
                                    listIndex = i,

                                    //Pass from the gamelocation
                                    accessRef = streetAccessRef,
                                    accessPositions = streetAccessPositions,
                                    toNodeReference = streetToNodeReference,
                                    noPassRef = streetNoPassRef
                                };

                                jobReferences.Add(newJob);

                                //Create job handle
                                JobHandle newHandle = newJob.Schedule();
                                jobHandles.Add(newHandle); //Add to list
                            }
                        }
                    }

                    currentNode = nextNode;
                }
            }

            if(currentNode != destination)
            {
                Game.LogError("PathFinder: Current node != desitnation. This shouldn't be happeneing.");
            }

            JobHandle.CompleteAll(jobHandles); //Complete all jobs on list

            //We should now have all our routes, compile them into one list...
            currentNode = origin;

            for (int i = 0; i < locationRoute.Count + 1; i++)
            {
                NewNode.NodeAccess current = null; //The current access in the list
                NewNode nextNode = null; //The next destination according to the above

                if (i >= 0 && i < locationRoute.Count)
                {
                    current = locationRoute[i];
                    nextNode = current.GetOtherGameLocation(currentNode);
                }

                //If at the end...
                if (i == locationRoute.Count)
                {
                    if (currentNode != destination)
                    {
                        //Because the location route consists of access between 2 gamelocations, the destination is sometimes not the same as where we end up: A final route calculation is often needed...
                        nextNode = destination;
                    }
                    else break;
                }

                if (currentNode != nextNode)
                {
                    //Does a job match this index?
                    int jobIndex = jobReferences.FindIndex(item => item.listIndex == i);

                    //The job here is at the valid point in the overall route
                    if (jobIndex > -1)
                    {
                        //Detect errors
                        if(jobReferences[jobIndex].output.Length <= 0)
                        {
                            for (int u = 0; u < locationRoute.Count; u++)
                            {
                                string thisIt = string.Empty;
                                if (u == i) thisIt = "*";
                                Game.Log("Pathfinder: " + thisIt + locationRoute[u].worldAccessPoint);
                            }

                            Game.LogError("Pathfinder: Internal pathfinding " + jobReferences[jobIndex].listIndex + ": " + nodeMap[jobReferences[jobIndex].origin].position + "->" + nodeMap[jobReferences[jobIndex].destination].position + " @ " + currentNode.gameLocation.name); ;
                        }

                        List<NewNode.NodeAccess> accessList = new List<NewNode.NodeAccess>(INITIAL_COLLECTION_SIZE);

                        for (int u = 0; u < jobReferences[jobIndex].output.Length; u++)
                        {
                            int accId = jobReferences[jobIndex].output[u];
                            accessList.Add(nodeAccessReference[accId]);
                        }

                        totalOutput.AddRange(accessList);

                        //Save this in the dictionary
                        if (Game.Instance.useInternalRouteCaching && SessionData.Instance.startedGame)
                        {
                            NewAddress.PathKey pathKey = new NewAddress.PathKey(currentNode, nextNode);

                            if (currentNode.gameLocation.thisAsAddress != null)
                            {
                                if (Game.Instance.unlimitedPathCaching || currentNode.gameLocation.thisAsAddress.internalRoutes.Count < Game.Instance.maxInternalCachedPaths)
                                {
                                    currentNode.gameLocation.thisAsAddress.internalRoutes.Add(pathKey, accessList);
                                }
                            }
                            else
                            {
                                //To save in the street cache this must go from building to building
                                if (currentNode.isIndoorsEntrance && nextNode.isIndoorsEntrance)
                                {
                                    if (Game.Instance.unlimitedPathCaching || internalRoutes.Count < Game.Instance.maxStreetCachedPaths)
                                    {
                                        internalRoutes.Add(pathKey, accessList);
                                    }
                                }
                            }
                        }

                        PathFinder.Instance.calculatedInternalRoutes++;
                        jobReferences[jobIndex].output.Dispose(); //Dispose of the job output list
                    }
                    else
                    {
                        if(Game.Instance.forceStreetPathsOnMainThread && currentNode.gameLocation.thisAsStreet != null)
                        {
                            List<NewNode.NodeAccess> internalRoute = GetInternalRoute(new NewAddress.PathKey(currentNode, nextNode), currentNode.gameLocation);
                            if(internalRoute != null) totalOutput.AddRange(internalRoute);
                        }
                        else
                        {
                            //Else this must be cached
                            List<NewNode.NodeAccess> cached = null;

                            if (cachedRoutes.TryGetValue(i, out cached))
                            {
                                totalOutput.AddRange(cached);
                            }
                        }
                    }

                    currentNode = nextNode;
                }
            }

            jobHandles.Dispose(); //You must manually dispose of native lists
                                  //The total output should now have a correct path.
        }
        else
        {
            for (int i = 0; i <= locationRoute.Count; i++)
            {
                //Game.Log(i + " == " + Mathf.Clamp(i, 0, locationRoute.Count - 1) + " collection: " + locationRoute.Count);
                NewNode.NodeAccess current = null;
                NewNode nextNode = null;

                if (i >= 0 && i < locationRoute.Count)
                {
                    current = locationRoute[i];
                    nextNode = current.GetOtherGameLocation(currentNode);
                }

                //If at the end...
                if (i == locationRoute.Count)
                {
                    if (currentNode != destination)
                    {
                        //Because the location route consists of access between 2 gamelocations, the destination is sometimes not the same as where we end up: A final route calculation is often needed...
                        nextNode = destination;
                    }
                    else break;
                }

                //Get from the origin to the inside of the next room
                if (currentNode != nextNode)
                {
                    if (Game.Instance.debugPathfinding)
                    {
                        bool accessError = false;

                        if (currentNode.noAccess)
                        {
                            Game.LogError("Pathfinder: Current node is not accessable at index " + i + "/" + locationRoute.Count);
                            accessError = true;
                        }

                        if (nextNode.noAccess)
                        {
                            Game.LogError("Pathfinder: Next node is not accessable at index " + i + "/" + locationRoute.Count);
                            accessError = true;
                        }

                        if(accessError)
                        {
                            for (int u = 0; u < locationRoute.Count; u++)
                            {
                                //Create origin and destination objects
                                GameObject locRoute = GameObject.CreatePrimitive(PrimitiveType.Cube);
                                locRoute.name = "Location route " + i + "/" + locationRoute.Count;
                                locRoute.transform.SetParent(PrefabControls.Instance.pathfindDebugParent);
                                locRoute.transform.position = locationRoute[u].worldAccessPoint;
                            }
                        }

                    }

                    List<NewNode.NodeAccess> acc = GetInternalRoute(new NewAddress.PathKey(currentNode, nextNode), currentNode.gameLocation);

                    if(acc != null)
                    {
                        totalOutput.AddRange(acc);
                        currentNode = nextNode;
                    }
                }
            }

            if (currentNode != destination)
            {
                Game.Log("Path Error: Current node != desitnation. This shouldn't be happeneing.");
                //totalOutput.AddRange(GetInternalRoute(new NewAddress.PathKey(currentNode, destination), currentNode.gameLocation));
            }
        }

        //You should now have a complete path to return
        return new PathData { accessList = totalOutput };
    }

    private List<NewNode.NodeAccess> GetGameLocationRoute(NewNode origin, NewNode destination)
    {
        //Create a key to search for this journey...
        GameLocationPathKey roomKey = new GameLocationPathKey(origin.gameLocation, destination.gameLocation);

        //Does this already exist in the dictionary?
        List<NewNode.NodeAccess> output = null;

        if(Game.Instance.useExternalRouteCaching)
        {
            //Try regular key
            if (gameLocationRoutes.TryGetValue(roomKey, out output))
            {
                returnedCachedRoomRoutes++;
                return output;
            }
            //Else create and try a reverse key
            else
            {
                GameLocationPathKey reverseKey = new GameLocationPathKey(destination.gameLocation, origin.gameLocation);

                if (gameLocationRoutes.TryGetValue(reverseKey, out output))
                {
                    output = new List<NewNode.NodeAccess>(output);
                    output.Reverse(); //Reverse output
                    returnedCachedRoomRoutes++;
                    return output;
                }
            }
        }

        //If this code is reached, we need to calculate the route...
        //Uses A* pathfinding... I don't *think* efficiency can be improved by another style...

        // The set of nodes already evaluated.
        Dictionary<NewNode.NodeAccess, bool> closedSet = new Dictionary<NewNode.NodeAccess, bool>(INITIAL_COLLECTION_SIZE);

        // The set of currently discovered nodes still to be evaluated.
        // Initially, only the start node is known.
        Dictionary<NewNode.NodeAccess, bool> openSet = new Dictionary<NewNode.NodeAccess, bool>(INITIAL_COLLECTION_SIZE);
        //Dictionary<NewRoom, bool> scannedRooms = new Dictionary<NewRoom, bool>();
        //scannedRooms.Add(roomKey.originRoom, true);

        // For each node, which node it can most efficiently be reached from.
        // If a node can be reached from many nodes, cameFrom will eventually contain the
        // most efficient previous step.
        Dictionary<NewNode.NodeAccess, NewNode.NodeAccess> cameFrom = new Dictionary<NewNode.NodeAccess, NewNode.NodeAccess>(INITIAL_COLLECTION_SIZE);

        // For each node, the cost of getting from the start node to that node.
        Dictionary<NewNode.NodeAccess, float> fromStartToThis = new Dictionary<NewNode.NodeAccess, float>(INITIAL_COLLECTION_SIZE);

        // For each node, the total cost of getting from the start node to the goal
        // by passing by that node. That value is partly known, partly heuristic.
        Dictionary<NewNode.NodeAccess, float> fromStartViaThis = new Dictionary<NewNode.NodeAccess, float>(INITIAL_COLLECTION_SIZE);

        //Add entrances of the starting room to the open set...
        List<NewNode.NodeAccess> startingEntrances = null;

        if (roomKey.originLocation.thisAsStreet != null)
        {
            //If the destination is a street, we need to treat as normal rooms...
            if (roomKey.destinationLocation.thisAsStreet != null)
            {
                startingEntrances = roomKey.originLocation.entrances;
            }
            else startingEntrances = streetEntrances;
        }
        else startingEntrances = roomKey.originLocation.entrances;

        //Store attempted routes
        Dictionary<NewGameLocation, List<DebugPathfind.DebugLocationLink>> debugRecord = null;

        if (Game.Instance.debugPathfinding)
        {
            debugRecord = new Dictionary<NewGameLocation, List<DebugPathfind.DebugLocationLink>>(INITIAL_COLLECTION_SIZE);
        }

        foreach (NewNode.NodeAccess accessNeighbor in startingEntrances)
        {
            if (accessNeighbor.toNode.noAccess)
            {
                if (Game.Instance.debugPathfinding)
                {
                    if (!debugRecord.ContainsKey(roomKey.originLocation)) debugRecord.Add(roomKey.originLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                    debugRecord[roomKey.originLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Fail: No access"));
                }

                continue; //Skip no walking access...
            }

            if (!accessNeighbor.walkingAccess)
            {
                if (Game.Instance.debugPathfinding)
                {
                    if (!debugRecord.ContainsKey(roomKey.originLocation)) debugRecord.Add(roomKey.originLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                    debugRecord[roomKey.originLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Fail: No walking access"));
                }

                continue; //Skip no walking access...
            }

            if (accessNeighbor.employeeDoor)
            {
                if (Game.Instance.debugPathfinding)
                {
                    if (!debugRecord.ContainsKey(roomKey.originLocation)) debugRecord.Add(roomKey.originLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                    debugRecord[roomKey.originLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Fail: Employee door"));
                }

                continue; //Skip employee doors...
            }

            if (accessNeighbor.toNode.noPassThrough && accessNeighbor.toNode != destination)
            {
                if (Game.Instance.debugPathfinding)
                {
                    if (!debugRecord.ContainsKey(roomKey.originLocation)) debugRecord.Add(roomKey.originLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                    debugRecord[roomKey.originLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Fail: No Pass-Through"));
                }

                continue; //Skip no pass through nodes
            }

            //This is not a vaiable route if we have to pass through somewhere
            if (accessNeighbor.toNode.gameLocation.thisAsAddress != null)
            {
                //This is a different address from destination and origin
                if (accessNeighbor.toNode.gameLocation != roomKey.originLocation && accessNeighbor.toNode.gameLocation != roomKey.destinationLocation)
                {
                    if (accessNeighbor.toNode.gameLocation.thisAsAddress.addressPreset != null)
                    {
                        if (!accessNeighbor.toNode.gameLocation.thisAsAddress.addressPreset.canPassThrough)
                        {
                            if (Game.Instance.debugPathfinding)
                            {
                                if (!debugRecord.ContainsKey(roomKey.originLocation)) debugRecord.Add(roomKey.originLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                                debugRecord[roomKey.originLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Fail: No Pass-Through"));
                            }

                            continue;
                        }
                    }
                }
            }

            if (Game.Instance.debugPathfinding)
            {
                if (!debugRecord.ContainsKey(roomKey.originLocation)) debugRecord.Add(roomKey.originLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                debugRecord[roomKey.originLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Success"));
            }

            openSet.Add(accessNeighbor, true);

            // Use the origin node to determin the distance to the first entrance
            float startToEnt = Vector3.Distance(origin.position, accessNeighbor.worldAccessPoint);
            if (!fromStartToThis.ContainsKey(accessNeighbor)) fromStartToThis.Add(accessNeighbor, startToEnt); //Use zero as we could be starting from anywhere in the room

            // For the first node, that value is completely heuristic.
            float fromHereToEnd = Vector3.Distance(accessNeighbor.worldAccessPoint, destination.position);//For heutristics, use distance for now
            if (!fromStartViaThis.ContainsKey(accessNeighbor)) fromStartViaThis.Add(accessNeighbor, fromHereToEnd);
        }

        int calculationLoops = 0;
        bool pathfindSuccessful = false;

        //Find the nearest entrance...
        bool onStreet = false;

        //while openSet is not empty
        while (openSet.Count > 0)
        {
            //current := the node in openSet having the lowest fScore[] value
            NewNode.NodeAccess current = null;
            float lowestFScore = Mathf.Infinity;

            foreach (KeyValuePair<NewNode.NodeAccess, bool> pair in openSet)
            {
                //If doesn't exist, assume infinity (ie not lower than lowest score)
                if (fromStartViaThis.ContainsKey(pair.Key))
                {
                    if (fromStartViaThis[pair.Key] < lowestFScore)
                    {
                        lowestFScore = fromStartViaThis[pair.Key];
                        current = pair.Key;
                    }
                }
            }

            //The next room is the destination!
            if (current.toNode.gameLocation == roomKey.destinationLocation && current.walkingAccess && !current.employeeDoor && !current.toNode.noAccess && (!current.toNode.noPassThrough || current.toNode == destination))
            {
                //Construct the path
                List<NewNode.NodeAccess> totalPath = new List<NewNode.NodeAccess>();
                NewNode.NodeAccess previous = null;

                //We're constructing the path backwards, so start with this as the first entry...
                totalPath.Add(current);

                //Loop back through the rooms and find the entrance closest to the last. At the moment it just picks the one using the direction of the destination...
                while (cameFrom.ContainsKey(current))
                {
                    if (!current.walkingAccess)
                    {
                        Game.LogError("Pathfinder: Returned route contains unwalkable access! " + current.worldAccessPoint + " path index " + totalPath.Count);
                    }
                    else if (current.employeeDoor)
                    {
                        Game.LogError("Pathfinder: Returned route contains employee door! " + current.worldAccessPoint + " path index " + totalPath.Count);
                    }

                    previous = current;
                    current = cameFrom[current];

                    //negative vector2 is used as a null, so don't add this.
                    if (current != null)
                    {
                        //The next node to add must be different
                        if (current != previous)
                        {
                            totalPath.Add(current);
                        }
                        else continue;
                    }
                    else
                    {
                        Game.Log("Pathfinder: Room pathfinder path construction error: current is null, invalid path");
                        break;
                    }
                }

                totalPath.Reverse();

                //Save this in the dictionary
                if(Game.Instance.useExternalRouteCaching && SessionData.Instance.startedGame)
                {
                    if(Game.Instance.unlimitedPathCaching || gameLocationRoutes.Count < Game.Instance.maxExternalCachedPaths)
                    {
                        gameLocationRoutes.Add(roomKey, totalPath);
                    }
                }
                
                calculatedRoomRoutes++;

                //DEBUG REMOVE ME
                if(Game.Instance.debugPathfinding)
                {
                    for (int i = 0; i < totalPath.Count; i++)
                    {
                        NewNode.NodeAccess acc = totalPath[i];

                        if(!acc.walkingAccess || acc.employeeDoor)
                        {
                            Game.LogError("Pathfinder: Faulty route pathfinder! No walking access/employee door " + acc.worldAccessPoint + " path index " + i);
                        }
                        else if (acc.toNode.noAccess)
                        {
                            Game.LogError("Pathfinder: Faulty route pathfinder! To node no access " + acc.worldAccessPoint + " path index " + i);
                        }
                        else if ((acc.toNode.noPassThrough && acc.toNode != destination))
                        {
                            Game.LogError("Pathfinder: Faulty route pathfinder! To node no pass through " + acc.worldAccessPoint + " path index " + i);
                        }
                    }
                }

                //Return
                pathfindSuccessful = true;
                return totalPath;
            }

            //Is this the street? If so don't bother checking any other buildings apart from my destination.
            //This will mean routes through one building to get to a different one are invalid, BUT should speed things up a lot.
            if (!onStreet && destination.building != null)
            {
                if (current.fromNode.gameLocation.thisAsStreet != null)
                {
                    onStreet = true;
                }
            }

            openSet.Remove(current);

            //Add to closed set...
            if (!closedSet.ContainsKey(current))
            {
                closedSet.Add(current, true); //Add the current room to closed
            }

            //Also add the opposite access to the closed set to avoid doubling back
            if (current.oppositeAccess != null)
            {
                if (!closedSet.ContainsKey(current.oppositeAccess))
                {
                    closedSet.Add(current.oppositeAccess, true); //Add the current room to closed
                }
            }

            //Scan for entrances in this room...
            List<NewNode.NodeAccess> scanList = null;

            if (current.toNode.gameLocation.thisAsStreet != null)
            {
                //If the destination is a street, we need to treat treats as normal rooms...
                if (roomKey.destinationLocation.thisAsStreet != null)
                {
                    scanList = current.toNode.gameLocation.entrances;
                    //scanList.AddRange(streetEntrances);
                    //scanList.AddRange(current.toNode.gameLocation.entrances);
                }
                else scanList = streetEntrances;
            }
            else scanList = current.toNode.gameLocation.entrances;

            foreach (NewNode.NodeAccess accessNeighbor in scanList)
            {
                if (!accessNeighbor.walkingAccess)
                {
                    if(Game.Instance.debugPathfinding)
                    {
                        if (!debugRecord.ContainsKey(current.toNode.gameLocation)) debugRecord.Add(current.toNode.gameLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                        debugRecord[current.toNode.gameLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Fail: No Walking Access"));
                    }

                    continue; //Skip no walking access...
                }

                if (accessNeighbor.toNode.noAccess)
                {
                    if (Game.Instance.debugPathfinding)
                    {
                        if (!debugRecord.ContainsKey(current.toNode.gameLocation)) debugRecord.Add(current.toNode.gameLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                        debugRecord[current.toNode.gameLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Fail: No Access"));
                    }

                    continue; //Skip no walking access...
                }

                if (accessNeighbor.employeeDoor)
                {
                    if (Game.Instance.debugPathfinding)
                    {
                        if (!debugRecord.ContainsKey(current.toNode.gameLocation)) debugRecord.Add(current.toNode.gameLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                        debugRecord[current.toNode.gameLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Fail: Employee Door"));
                    }

                    continue; //Skip employee doors (only do this on immediate exit of workplace)
                }
                if (accessNeighbor.toNode.noPassThrough && accessNeighbor.toNode != destination)
                {
                    if (Game.Instance.debugPathfinding)
                    {
                        if (!debugRecord.ContainsKey(current.toNode.gameLocation)) debugRecord.Add(current.toNode.gameLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                        debugRecord[current.toNode.gameLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Fail: No Pass-Through"));
                    }

                    continue; //Skip no pass through
                }

                //if neighbor in closedSet
                // Ignore the neighbor which is already evaluated.
                bool b = false;

                if (closedSet.TryGetValue(accessNeighbor, out b))
                {
                    if (Game.Instance.debugPathfinding)
                    {
                        if (!debugRecord.ContainsKey(current.toNode.gameLocation)) debugRecord.Add(current.toNode.gameLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                        debugRecord[current.toNode.gameLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Fail: Already Scanned"));
                    }

                    continue;
                }

                if (openSet.TryGetValue(accessNeighbor, out b))
                {
                    if (Game.Instance.debugPathfinding)
                    {
                        if (!debugRecord.ContainsKey(current.toNode.gameLocation)) debugRecord.Add(current.toNode.gameLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                        debugRecord[current.toNode.gameLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Fail: Already In Line For Scanning"));
                    }

                    continue;
                }

                //Don't both checking other buildings if on street & destination is in a building
                if (onStreet && destination.building != null)
                {
                    if (accessNeighbor.toNode.gameLocation.thisAsAddress != null)
                    {
                        if (accessNeighbor.toNode.building != destination.building)
                        {
                            if (Game.Instance.debugPathfinding)
                            {
                                if (!debugRecord.ContainsKey(current.toNode.gameLocation)) debugRecord.Add(current.toNode.gameLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                                debugRecord[current.toNode.gameLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Fail: Skip Other Buildings"));
                            }

                            continue;
                        }
                    }
                }
                else
                {
                    //If I have to exit this building, don't bother exploring floors without an exit...
                    if (current.toNode.building != null) //Currently in a building
                    {
                        if (destination.building == null || destination.building != current.toNode.building) //Destination is either not a building or a different building (IE I have to exit this)
                        {
                            //No entrances on this floor
                            if (accessNeighbor.toNode.floor != null)
                            {
                                if (accessNeighbor.toNode.floor.buildingEntrances.Count <= 0) //There are no building entrances on this floor
                                {
                                    if (accessNeighbor.toNode.floor != roomKey.originLocation.floor) //I'm not on the origin floor
                                    {
                                        if (!accessNeighbor.toNode.tile.isStairwell && !accessNeighbor.toNode.tile.isInvertedStairwell) //I can't move up or down here...
                                        {
                                            if (Game.Instance.debugPathfinding)
                                            {
                                                if (!debugRecord.ContainsKey(current.toNode.gameLocation)) debugRecord.Add(current.toNode.gameLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                                                debugRecord[current.toNode.gameLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Fail: This Floor Has No Building Exit"));
                                            }

                                            continue;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //This is not a vaiable route if we have to pass through somewhere...
                if (accessNeighbor.toNode.gameLocation.thisAsAddress != null)
                {
                    //This is a different address from destination and origin
                    if (accessNeighbor.toNode.gameLocation != roomKey.originLocation && accessNeighbor.toNode.gameLocation != roomKey.destinationLocation)
                    {
                        if (accessNeighbor.toNode.gameLocation.thisAsAddress.addressPreset != null)
                        {
                            if (!accessNeighbor.toNode.gameLocation.thisAsAddress.addressPreset.canPassThrough)
                            {
                                if (Game.Instance.debugPathfinding)
                                {
                                    if (!debugRecord.ContainsKey(current.toNode.gameLocation)) debugRecord.Add(current.toNode.gameLocation, new List<DebugPathfind.DebugLocationLink>(INITIAL_COLLECTION_SIZE));
                                    debugRecord[current.toNode.gameLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Fail: No Pass-Through Allowed @ Neighbor Address"));
                                }

                                continue;
                            }
                        }
                    }
                }

                if (Game.Instance.debugPathfinding)
                {
                    if (!debugRecord.ContainsKey(current.toNode.gameLocation)) debugRecord.Add(current.toNode.gameLocation, new List<DebugPathfind.DebugLocationLink>());
                    debugRecord[current.toNode.gameLocation].Add(new DebugPathfind.DebugLocationLink(accessNeighbor, "Success"));
                }

                //Discover a new node- add to open set
                //Check this room hasn't been reached prior...
                openSet.Add(accessNeighbor, true);

                float tentativeGScore = 0f;

                //The path weight from start to the neighbor
                //We're adding the pre-calculated weight from the node of the entrance we just walked into, to the possible next entrance to (exterior/to -> interior/from)
                float weight = 0f;

                if(current.entranceWeights.TryGetValue(accessNeighbor, out weight))
                {
                    tentativeGScore = fromStartToThis[current] + weight;
                }
                else
                {
                    //Caclulate a new entrance weight now...
                    //Game.Log("Pathfinder: Could not find entrance weight from " + current.toNode.nodeCoord + " to " + accessNeighbor.toNode.nodeCoord + " (from node: " + accessNeighbor.fromNode.nodeCoord + "). Calculating new one now...");
                    
                    float newWeight = Vector3.Distance(current.worldAccessPoint, accessNeighbor.worldAccessPoint);

                    current.entranceWeights[current] = newWeight;

                    tentativeGScore = fromStartToThis[current] + newWeight;
                }

                //Path hasn't been added (assume infinity) if tentative score is lower then this is automatically the best until now.
                if (!fromStartToThis.ContainsKey(accessNeighbor) || tentativeGScore < fromStartToThis[accessNeighbor])
                {
                    if (!cameFrom.ContainsKey(accessNeighbor)) cameFrom.Add(accessNeighbor, current);
                    else cameFrom[accessNeighbor] = current;

                    if (!fromStartToThis.ContainsKey(accessNeighbor)) fromStartToThis.Add(accessNeighbor, tentativeGScore);
                    else fromStartToThis[accessNeighbor] = tentativeGScore;

                    //Partly known, party guessed...
                    float viaThis = fromStartToThis[accessNeighbor] + Vector3.Distance(accessNeighbor.worldAccessPoint, destination.position);
                    if (!fromStartViaThis.ContainsKey(accessNeighbor)) fromStartViaThis.Add(accessNeighbor, viaThis);
                    else fromStartViaThis[accessNeighbor] = viaThis;
                }
            }

            calculationLoops++;

            if (calculationLoops > 9999)
            {
                Game.LogError("Pathfinder: GameLocation pathfinding failsafe reached, check for cascades! Dumping open set...");

                foreach (KeyValuePair<NewNode.NodeAccess, bool> pair in openSet)
                {
                    Game.Log("Pathfinder: " + pair.Key.fromNode.name + "(" + pair.Key.fromNode.room.roomID + ") -> " + pair.Key.toNode.name + "(" + pair.Key.toNode.room.roomID + ")");
                }

                return null;
            }
        }

        if (!pathfindSuccessful)
        {
            Game.LogError("Pathfinder: GameLocation pathfinding unsucessful- there is an unreachable route with " + closedSet.Count + " closed set count, information below...");

            if (Game.Instance.debugPathfinding)
            {
                Game.Log("Pathfinder: Outputting debug information for this error only...");

                foreach (KeyValuePair<NewNode.NodeAccess, bool> pair in closedSet)
                {
                    GameObject debugNode = Instantiate(PrefabControls.Instance.pathfindRoomDebug, PrefabControls.Instance.pathfindDebugParent);
                    DebugPathfind debugP = debugNode.GetComponent<DebugPathfind>();

                    List<DebugPathfind.DebugLocationLink> linkList = null;
                    debugRecord.TryGetValue(pair.Key.toNode.gameLocation, out linkList);

                    debugP.Setup(pair.Key, pair.Key.toNode.room, linkList);
                }

                Game.Instance.debugPathfinding = false;
            }

            Game.Log("Pathfinder: from " + roomKey.originLocation.name + ", " + origin.room.name + ", " + origin.name + ", " + origin.nodeCoord + " (" + origin.position + ")");
            Game.Log("Pathfinder: to " + roomKey.destinationLocation.name + ", " + destination.room.name + ", " + destination.name + ", " + destination.nodeCoord + " (" + destination.position + ")");
        }

        return null;
    }

    public List<NewNode.NodeAccess> GetInternalRoute(NewAddress.PathKey pathKey, NewGameLocation gameLocation)
    {
        //Game.Log("Pathfinder: (Internal)" + name + " : " + pathKey.origin.name + " > " + pathKey.destination.name);
        List<NewNode.NodeAccess> output = null;

        //Does this already exist in the dictionary?
        //Try regular key
        if(Game.Instance.useInternalRouteCaching)
        {
            if (gameLocation.thisAsAddress != null)
            {
                if (gameLocation.thisAsAddress.internalRoutes.TryGetValue(pathKey, out output))
                {
                    return output;
                }
            }
            else
            {
                if (internalRoutes.TryGetValue(pathKey, out output))
                {
                    return output;
                }
            }
        }

        //Disable reverse keys for now as they were creating confusion with to/from nodes...

        //If this code is reached, we need to calculate the route...
        //Uses A* pathfinding... I don't *think* efficiency can be improved by another style...

        // The set of nodes already evaluated.
        HashSet<NewNode.NodeAccess> closedSet = new HashSet<NewNode.NodeAccess>(INITIAL_COLLECTION_SIZE);

        // The set of currently discovered nodes still to be evaluated.
        // Initially, only the start node is known.
        HashSet<NewNode.NodeAccess> openSet = new HashSet<NewNode.NodeAccess>(INITIAL_COLLECTION_SIZE);
        HashSet<NewNode> scannedNodes = new HashSet<NewNode>(INITIAL_COLLECTION_SIZE);
        scannedNodes.Add(pathKey.origin);

        // For each node, which node it can most efficiently be reached from.
        // If a node can be reached from many nodes, cameFrom will eventually contain the
        // most efficient previous step.
        Dictionary<NewNode.NodeAccess, NewNode.NodeAccess> cameFrom = new Dictionary<NewNode.NodeAccess, NewNode.NodeAccess>(INITIAL_COLLECTION_SIZE);

        // For each node, the cost of getting from the start node to that node.
        Dictionary<NewNode.NodeAccess, float> fromStartToThis = new Dictionary<NewNode.NodeAccess, float>(INITIAL_COLLECTION_SIZE);

        // For each node, the total cost of getting from the start node to the goal
        // by passing by that node. That value is partly known, partly heuristic.
        Dictionary<NewNode.NodeAccess, float> fromStartViaThis = new Dictionary<NewNode.NodeAccess, float>(INITIAL_COLLECTION_SIZE);

        //Add access from origin
        foreach (KeyValuePair<NewNode, NewNode.NodeAccess> pair in pathKey.origin.accessToOtherNodes)
        {
            if (!pair.Value.walkingAccess) continue; //Skip no walking access...
            if (pair.Value.employeeDoor) continue; //Skip employee door...
            if (pair.Value.toNode.noPassThrough && pair.Value.toNode != pathKey.destination) continue; //Skip no pass through if not destination

            //Skip if in different gamelocation unless it's the destination gamelocation
            if ((gameLocation.thisAsAddress == null && pair.Key.gameLocation.thisAsAddress != null) || (gameLocation.thisAsAddress != null && pair.Key.gameLocation != gameLocation))
            {
                if (pair.Key.gameLocation != pathKey.origin.gameLocation && pair.Key.gameLocation != pathKey.destination.gameLocation)
                {
                    continue;
                }
            }

            openSet.Add(pair.Value);

            // The cost of going from start to start is zero.
            if (!fromStartToThis.ContainsKey(pair.Value)) fromStartToThis.Add(pair.Value, 0f);

            // For the first node, that value is completely heuristic.
            if (!fromStartViaThis.ContainsKey(pair.Value)) fromStartViaThis.Add(pair.Value, Vector3.Distance(pair.Value.worldAccessPoint, pathKey.destination.position));//For heutristics, use distance for now
        }

        int calculationLoops = 0;
        bool pathfindSuccessful = false;

        //while openSet is not empty
        while (openSet.Count > 0)
        {
            //current := the node in openSet having the lowest fScore[] value
            NewNode.NodeAccess current = null;
            float lowestFScore = Mathf.Infinity;

            foreach (NewNode.NodeAccess access in openSet)
            {
                //If doesn't exist, assume infinity (ie not lower than lowest score)
                if (fromStartViaThis.ContainsKey(access))
                {
                    if (fromStartViaThis[access] < lowestFScore)
                    {
                        lowestFScore = fromStartViaThis[access];
                        current = access;
                    }
                }
            }

            //if current == goal then we're done!
            if (current.toNode == pathKey.destination && current.walkingAccess && !current.toNode.noAccess && !current.employeeDoor)
            {
                //Construct the path
                List<NewNode.NodeAccess> totalPath = new List<NewNode.NodeAccess>(INITIAL_COLLECTION_SIZE);
                NewNode.NodeAccess previous = null;

                //We're constructing the path backwards, so start with this as the first entry...
                totalPath.Add(current);

                while (cameFrom.ContainsKey(current))
                {
                    previous = current;
                    current = cameFrom[current];

                    //negative vector2 is used as a null, so don't add this.
                    if (current != null)
                    {
                        //The next node to add must be different
                        if (current != previous)
                        {
                            totalPath.Add(current);
                        }
                        else continue;
                    }
                    else
                    {
                        Game.Log("PathFinder: Room pathfinder path construction error: current is null, invalid path");
                        break;
                    }
                }

                totalPath.Reverse();

                //Save this in the dictionary
                if(Game.Instance.useInternalRouteCaching && SessionData.Instance.startedGame)
                {
                    if (gameLocation.thisAsAddress != null)
                    {
                        if(Game.Instance.unlimitedPathCaching || gameLocation.thisAsAddress.internalRoutes.Count < Game.Instance.maxInternalCachedPaths)
                        {
                            gameLocation.thisAsAddress.internalRoutes.Add(pathKey, totalPath);
                        }
                    }
                    else
                    {
                        //To save in the street cache this must go from building to building
                        if (pathKey.origin.isIndoorsEntrance && pathKey.destination.isIndoorsEntrance)
                        {
                            if (Game.Instance.unlimitedPathCaching || internalRoutes.Count < Game.Instance.maxStreetCachedPaths)
                            {
                                internalRoutes.Add(pathKey, totalPath);
                            }
                        }
                    }
                }

                PathFinder.Instance.calculatedInternalRoutes++;

                //Return
                pathfindSuccessful = true;
                return totalPath;
            }
            else if(Game.Instance.debugPathfinding && current.toNode == pathKey.destination)
            {
                Game.LogError("Pathfinder: Final destination rejected! " + current.walkingAccess + " " + current.toNode.noAccess + " " + current.employeeDoor);
            }

            openSet.Remove(current);
            closedSet.Add(current);

            //Scan for entrances in this room...
            foreach (KeyValuePair<NewNode, NewNode.NodeAccess> pair in current.toNode.accessToOtherNodes)
            {
                if (!pair.Value.walkingAccess) continue; //Skip no walking access...
                if (pair.Value.employeeDoor) continue;
                if (pair.Value.toNode.noAccess) continue;
                if (pair.Value.toNode.noPassThrough && pair.Value.toNode != pathKey.destination) continue; //Skip no pass through if it's not the destination

                //Skip if in different gamelocation unless it's the destination gamelocation
                if ((gameLocation.thisAsAddress == null && pair.Key.gameLocation.thisAsAddress != null) || (gameLocation.thisAsAddress != null && pair.Key.gameLocation != gameLocation))
                {
                    if (pair.Key.gameLocation != pathKey.origin.gameLocation && pair.Key.gameLocation != pathKey.destination.gameLocation)
                    {
                        continue;
                    }
                }

                NewNode.NodeAccess accessNeighbor = pair.Value;

                //if neighbor in closedSet
                // Ignore the neighbor which is already evaluated.
                if (closedSet.Contains(accessNeighbor)) continue;
                if (openSet.Contains(accessNeighbor)) continue;
                if (scannedNodes.Contains(accessNeighbor.toNode)) continue;

                //The path weight from start to the neighbor
                //We're adding the pre-calculated weight from the node of the entrance we just walked into, to the possible next entrance to (exterior/to -> interior/from)
                float tentativeGScore = fromStartToThis[current] + pair.Value.weight;

                //Discover a new node- add to open set
                //Game.Log("Pathfinder: Add " + neighbor.pathCoord + " to open set, count: " + openSet.Count);
                openSet.Add(accessNeighbor);
                scannedNodes.Add(accessNeighbor.toNode);

                //Path hasn't been added (assume infinity) if tentative score is lower then this is automatically the best until now.
                if (!fromStartToThis.ContainsKey(accessNeighbor) || tentativeGScore < fromStartToThis[accessNeighbor])
                {
                    if (!cameFrom.ContainsKey(accessNeighbor)) cameFrom.Add(accessNeighbor, current);
                    else cameFrom[accessNeighbor] = current;

                    if (!fromStartToThis.ContainsKey(accessNeighbor)) fromStartToThis.Add(accessNeighbor, tentativeGScore);
                    else fromStartToThis[accessNeighbor] = tentativeGScore;

                    float viaThis = fromStartToThis[accessNeighbor] + Vector3.Distance(accessNeighbor.worldAccessPoint, pathKey.destination.position);
                    if (!fromStartViaThis.ContainsKey(accessNeighbor)) fromStartViaThis.Add(accessNeighbor, viaThis);
                    else fromStartViaThis[accessNeighbor] = viaThis;
                }
            }

            calculationLoops++;

            if (calculationLoops > 9999)
            {
                Game.LogError("PathFinder: Internal pathfinding failsafe reached, check for cascades! Dumping open set...");

                foreach (NewNode.NodeAccess access in openSet)
                {
                    Game.Log("Pathfinder: " + access.fromNode.name + "(" + access.fromNode.room.roomID + ") -> " + access.toNode.name + "(" + access.toNode.room.roomID + ")");
                }

                return null;
            }
        }

        if (!pathfindSuccessful)
        {
            if (Game.Instance.debugPathfinding)
            {
                Game.LogError("PathFinder: " + gameLocation.name + " Internal pathfinding unsucessful- there is an unreachable route from: " + pathKey.origin.position + " to " + pathKey.destination.position + ". Dumping closed set...");
                Game.Log("Pathfinder: Outputting debug information for this error only...");

                //Create origin and destination objects
                GameObject origin = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                origin.name = "Origin, noAccess: " + pathKey.origin.noAccess + " Access count: " + pathKey.origin.accessToOtherNodes.Count;
                origin.transform.SetParent(PrefabControls.Instance.pathfindDebugParent);
                origin.transform.position = pathKey.origin.position;

                foreach(KeyValuePair<NewNode, NewNode.NodeAccess> pair in pathKey.origin.accessToOtherNodes)
                {
                    GameObject debugNode = Instantiate(PrefabControls.Instance.pathfindInternalDebug, origin.transform);
                    debugNode.GetComponent<DebugPathfind>().Setup(pair.Value, pair.Value.fromNode.room, null);
                }

                GameObject dest = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                dest.name = "Destination, noAccess: " + pathKey.destination.noAccess + " Access count: " + pathKey.destination.accessToOtherNodes.Count;
                dest.transform.SetParent(PrefabControls.Instance.pathfindDebugParent);
                dest.transform.position = pathKey.destination.position;

                foreach (KeyValuePair<NewNode, NewNode.NodeAccess> pair in pathKey.destination.accessToOtherNodes)
                {
                    GameObject debugNode = Instantiate(PrefabControls.Instance.pathfindInternalDebug, dest.transform);
                    debugNode.GetComponent<DebugPathfind>().Setup(pair.Value, pair.Value.fromNode.room, null);
                }

                foreach (NewNode.NodeAccess access in closedSet)
                {
                    GameObject debugNode = Instantiate(PrefabControls.Instance.pathfindNodeDebug, PrefabControls.Instance.pathfindDebugParent);
                    debugNode.GetComponent<DebugPathfind>().Setup(access, access.fromNode.room, null);

                    if(access.toNode == pathKey.destination)
                    {
                        debugNode.name += " DESTINATION CONNECTION";
                    }

                    if(access.fromNode == pathKey.origin)
                    {
                        debugNode.name += " ORIGIN CONNECTION";
                    }
                }

                Game.Instance.debugPathfinding = false;
            }
            else Game.Log("Path Error: " + gameLocation.name + " Internal pathfinding unsucessful- there is an unreachable route from: " + pathKey.origin.position + " to " + pathKey.destination.position + ".");

            //foreach (KeyValuePair<NewNode.NodeAccess, bool> pair in closedSet)
            //{
            //    Game.Log("Pathfinder: " + pair.Key.fromNode.name + "(" + pair.Key.fromNode.room.roomID + ") -> " + pair.Key.toNode.name + "(" + pair.Key.toNode.room.roomID + ")");
            //}
        }
        return null;
    }

    //Create data containers needed to pass to pathfinding jobs
    public void GenerateJobPathingData()
    {
        streetAccessRef = new NativeMultiHashMap<float3, int>(0, Allocator.Persistent);
        streetAccessPositions = new NativeHashMap<int, float3>(0, Allocator.Persistent); //Use a access reference to access node positions of the access (for heuristics)
        streetToNodeReference = new NativeHashMap<int, float3>(0, Allocator.Persistent);
        streetNoPassRef = new NativeList<float3>(0, Allocator.Persistent);

        foreach(StreetController street in CityData.Instance.streetDirectory)
        {
            foreach (NewNode node in street.nodes)
            {
                if(node.noPassThrough)
                {
                    streetNoPassRef.Add(Toolbox.Instance.ToFloat3(node.nodeCoord));
                }

                foreach (KeyValuePair<NewNode, NewNode.NodeAccess> pair in node.accessToOtherNodes)
                {
                    if (!pair.Value.walkingAccess) continue;
                    if (pair.Value.employeeDoor) continue;
                    if (pair.Value.toNode.noAccess) continue;
                    if (pair.Value.fromNode.noAccess) continue;

                    streetAccessRef.Add(Toolbox.Instance.ToFloat3(node.nodeCoord), pair.Value.id);
                    streetAccessPositions.TryAdd(pair.Value.id, pair.Value.worldAccessPoint);
                    streetToNodeReference.TryAdd(pair.Value.id, Toolbox.Instance.ToFloat3(pair.Value.toNode.nodeCoord));
                }
            }
        }

        //Add entrances also
        foreach (NewNode.NodeAccess entrance in streetEntrances)
        {
            if (!entrance.walkingAccess) continue;
            if (entrance.employeeDoor) continue;
            if (entrance.toNode.noAccess) continue;
            if (entrance.fromNode.noAccess) continue;

            streetAccessRef.Add(Toolbox.Instance.ToFloat3(entrance.fromNode.nodeCoord), entrance.id);
            streetAccessPositions.TryAdd(entrance.id, entrance.worldAccessPoint);
            streetToNodeReference.TryAdd(entrance.id, Toolbox.Instance.ToFloat3(entrance.toNode.nodeCoord));
        }
    }

    //Simple pathing system for checking tile access. Only works on 1 Z plane.
    public List<NewTile> GetTileRoute(NewTile origin, NewTile destination, List<NewTile> avoidTiles = null)
    {
        //If this code is reached, we need to calculate the route...
        //Uses A* pathfinding... I don't *think* efficiency can be improved by another style...

        // The set of nodes already evaluated.
        Dictionary<NewTile, bool> closedSet = new Dictionary<NewTile, bool>(INITIAL_COLLECTION_SIZE);

        // The set of currently discovered nodes still to be evaluated.
        // Initially, only the start node is known.
        Dictionary<NewTile, bool> openSet = new Dictionary<NewTile, bool>(INITIAL_COLLECTION_SIZE);

        // For each node, which node it can most efficiently be reached from.
        // If a node can be reached from many nodes, cameFrom will eventually contain the
        // most efficient previous step.
        Dictionary<NewTile, NewTile> cameFrom = new Dictionary<NewTile, NewTile>(INITIAL_COLLECTION_SIZE);

        // For each node, the cost of getting from the start node to that node.
        Dictionary<NewTile, float> fromStartToThis = new Dictionary<NewTile, float>(INITIAL_COLLECTION_SIZE);

        // For each node, the total cost of getting from the start node to the goal
        // by passing by that node. That value is partly known, partly heuristic.
        Dictionary<NewTile, float> fromStartViaThis = new Dictionary<NewTile, float>(INITIAL_COLLECTION_SIZE);

        //Add access from origin
        openSet.Add(origin, true);
        fromStartToThis.Add(origin, 0f);
        fromStartViaThis.Add(origin, Vector3.Distance(origin.globalTileCoord, destination.globalTileCoord));

        int calculationLoops = 0;

        //while openSet is not empty
        while (openSet.Count > 0)
        {
            //current := the node in openSet having the lowest fScore[] value
            NewTile current = null;
            float lowestFScore = Mathf.Infinity;

            foreach (KeyValuePair<NewTile, bool> pair in openSet)
            {
                //If doesn't exist, assume infinity (ie not lower than lowest score)
                if (fromStartViaThis.ContainsKey(pair.Key))
                {
                    if (fromStartViaThis[pair.Key] < lowestFScore)
                    {
                        lowestFScore = fromStartViaThis[pair.Key];
                        current = pair.Key;
                    }
                }
            }

            //if current == goal then we're done!
            if (current == destination)
            {
                //Construct the path
                List<NewTile> totalPath = new List<NewTile>(INITIAL_COLLECTION_SIZE);
                NewTile previous = null;

                //We're constructing the path backwards, so start with this as the first entry...
                totalPath.Add(current);

                while (cameFrom.ContainsKey(current))
                {
                    previous = current;
                    current = cameFrom[current];

                    //negative vector2 is used as a null, so don't add this.
                    if (current != null)
                    {
                        //The next node to add must be different
                        if (current != previous)
                        {
                            totalPath.Add(current);
                        }
                        else continue;
                    }
                    else
                    {
                        Game.Log("Pathfinder: Tile pathfinder path construction error: current is null, invalid path");
                        break;
                    }
                }

                totalPath.Reverse();

                //Return
                return totalPath;
            }

            openSet.Remove(current);
            closedSet.Add(current, true);

            //Scan for entrances in this room...
            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
            {
                Vector3 accessNeighbor = current.globalTileCoord + new Vector3(v2.x, v2.y, 0);
                if (!tileMap.ContainsKey(accessNeighbor)) continue;

                NewTile neighbor = tileMap[accessNeighbor];
                if (!neighbor.isEdge) continue; //Must be valid to be a road tile

                if (avoidTiles != null && avoidTiles.Contains(neighbor)) continue; //Avoid these

                //if neighbor in closedSet
                // Ignore the neighbor which is already evaluated.
                bool b = false;
                if (closedSet.TryGetValue(neighbor, out b)) continue;

                //The path weight from start to the neighbor
                //We're adding the pre-calculated weight from the node of the entrance we just walked into, to the possible next entrance to (exterior/to -> interior/from)
                float tentativeGScore = fromStartToThis[current] + 1;

                //Discover a new node- add to open set
                if (!openSet.TryGetValue(neighbor, out b))
                {
                    //Game.Log("Pathfinder: Add " + neighbor.pathCoord + " to open set, count: " + openSet.Count);
                    openSet.Add(neighbor, true);
                }

                //Path hasn't been added (assume infinity) if tentative score is lower then this is automatically the best until now.
                if (!fromStartToThis.ContainsKey(neighbor) || tentativeGScore < fromStartToThis[neighbor])
                {
                    if (!cameFrom.ContainsKey(neighbor)) cameFrom.Add(neighbor, current);
                    else cameFrom[neighbor] = current;

                    if (!fromStartToThis.ContainsKey(neighbor)) fromStartToThis.Add(neighbor, tentativeGScore);
                    else fromStartToThis[neighbor] = tentativeGScore;

                    float viaThis = fromStartToThis[neighbor] + Vector3.Distance(neighbor.globalTileCoord, destination.globalTileCoord);
                    if (!fromStartViaThis.ContainsKey(neighbor)) fromStartViaThis.Add(neighbor, viaThis);
                    else fromStartViaThis[neighbor] = viaThis;
                }
            }

            calculationLoops++;

            if (calculationLoops > 9999)
            {
                return null;
            }
        }

        return null;
    }

    //Dispose of job containers when disabled
    private void OnDisable()
    {
        try
        {
            streetAccessRef.Dispose();
            streetAccessPositions.Dispose();
            streetToNodeReference.Dispose();
            streetNoPassRef.Dispose();
            streetAccessRef.Dispose();
        }
        catch
        {

        }
    }

    //Job System Pathfinding...
    //Multiple threads of the internal pathfinder can be run @ once (as we know the game location route before hand).
    [BurstCompile]
    public struct GetInternalRouteJob : IJob
    {
        //We have to use IDs here as the job can't use classes.
        [ReadOnly]
        public float3 origin; //Use node coords
        [ReadOnly]
        public float3 destination; //Use node coords
        [ReadOnly]
        public int listIndex; //Because some paths may be found through dictionary cache then it's important to know this...

        //Extra variables needed for pathfinding
        [ReadOnly]
        public NativeMultiHashMap<float3, int> accessRef; //Use a node coord to access multiple references to access
        [ReadOnly]
        public NativeHashMap<int, float3> accessPositions; //Use a access reference to access node positions of the access (for heuristics)
        [ReadOnly]
        public NativeHashMap<int, float3> toNodeReference; //Use as a reference to the 'to' node

        public NativeList<float3> noPassRef; //Use as a reference to the 'to' node

        //This is accessed by the main thread
        [WriteOnly]
        public NativeList<int> output; //Native lists can only hold structs or non-nullables, so use an ID reference...

        //[WriteOnly]
        //public NativeList<float> debugOutputWeights; //For debugging purposes, output weights

        public void Execute()
        {
            //Game.Log("Pathfinder: (Internal " + listIndex + ") From " + origin + " to " + destination);
            //Game.Log("Pathfinder: (Internal " + listIndex + ") AccRef: " + accessRef.Length + ", AccPos: " + accessPositions.Length + " toNodes: " + toNodeReference.Length);

            //If this code is reached, we need to calculate the route...
            //Uses A* pathfinding... I don't *think* efficiency can be improved by another style...
            int accessCount = accessRef.Count();

            // The set of nodes already evaluated.
            NativeHashMap<int, int> closedSet = new NativeHashMap<int, int>(accessCount, Allocator.Temp);

            // The set of currently discovered nodes still to be evaluated.
            // Initially, only the start node is known.
            NativeList<int> openSet = new NativeList<int>(Allocator.Temp);
            NativeHashMap<float3, int> scannedNodes = new NativeHashMap<float3, int>(accessCount, Allocator.Temp);
            scannedNodes.TryAdd(origin, 1);

            // For each node, which node it can most efficiently be reached from.
            // If a node can be reached from many nodes, cameFrom will eventually contain the
            // most efficient previous step.
            NativeHashMap<int, int> cameFrom = new NativeHashMap<int, int>(accessCount, Allocator.Temp);

            // For each node, the cost of getting from the start node to that node.
            NativeHashMap<int, float> fromStartToThis = new NativeHashMap<int, float>(accessCount, Allocator.Temp);

            // For each node, the total cost of getting from the start node to the goal
            // by passing by that node. That value is partly known, partly heuristic.
            NativeHashMap<int, float> fromStartViaThis = new NativeHashMap<int, float>(accessCount, Allocator.Temp);

            //Add access from origin
            //Iterate hash map
            NativeMultiHashMapIterator<float3> iterator;

            int value;

            if (accessRef.TryGetFirstValue(origin, out value, out iterator))
            {
                do
                {
                    //We cannot skip these with the data available, so instead simply don't include them in the passed data.
                    //if (!PathFinder.Instance.nodeAccessReference[pair.Value].walkingAccess) continue; //Skip no walking access...

                    //if (PathFinder.Instance.nodeAccessReference[pair.Value].employeeDoor) continue; //Skip employee door...
                    float3 accessTo = toNodeReference[value];

                    if (!noPassRef.Contains(accessTo) || (accessTo.x == destination.x && accessTo.y == destination.y && accessTo.z == destination.z))
                    {
                        //This is also irrelevent as only the address's access will be included
                        //Skip if in different gamelocation unless it's the destination gamelocation
                        openSet.Add(value);

                        // The cost of going from start to start is zero.
                        //fromStartToThis.TryAdd(value, 0f);

                        // For the first node, that value is completely heuristic.
                        fromStartToThis.TryAdd(value, Vector3.Distance(origin, accessTo));
                        fromStartViaThis.TryAdd(value, Vector3.Distance(accessTo, destination));//For heutristics, use distance for now

                        ////Score each position on distance to end
                        //if (accessPositions.TryGetValue(value, out accessPos))
                        //{
                        //    // The cost of going from start to this
                        //    fromStartToThis.TryAdd(value, Vector3.Distance(origin, accessPos));

                        //    fromStartViaThis.TryAdd(value, Vector3.Distance(accessPos, destination));//For heutristics, use distance for now
                        //}
                        //else
                        //{
                        //    fromStartToThis.TryAdd(value, 999f);
                        //}
                    }

                } while (accessRef.TryGetNextValue(out value, ref iterator));
            }

            int calculationLoops = 0;
            bool pathfindSuccessful = false;

            //while openSet is not empty
            while (openSet.Length > 0)
            {
                //current := the node in openSet having the lowest fScore[] value
                int current = -1;
                float lowestFScore = Mathf.Infinity;

                for (int i = 0; i < openSet.Length; i++)
                {
                    int thisVal = openSet[i];

                    //If doesn't exist, assume infinity (ie not lower than lowest score)
                    float score;

                    if(fromStartViaThis.TryGetValue(thisVal, out score))
                    {
                        if(score < lowestFScore)
                        {
                            lowestFScore = score;
                            current = thisVal;
                        }
                    }
                }

                //if current == goal then we're done!
                float3 toNode;
                toNodeReference.TryGetValue(current, out toNode);

                if (toNode.x == destination.x && toNode.y == destination.y && toNode.z == destination.z)
                {
                    //Construct the path
                    int previous;
                    NativeList<int> backwardsPath = new NativeList<int>(Allocator.Temp);
                    //NativeList<float> debugBackwardsPathWeights = new NativeList<float>(Allocator.Temp);

                    //We're constructing the path backwards, so start with this as the first entry...
                    backwardsPath.Add(current);
                    //debugBackwardsPathWeights

                    int cameFromVal;

                    while(cameFrom.TryGetValue(current, out cameFromVal))
                    {
                        previous = current;
                        current = cameFromVal;

                        //The next node to add must be different
                        if (current != previous)
                        {
                            backwardsPath.Add(current);
                        }
                        else continue;
                    }

                    //Copy to output
                    for (int u = backwardsPath.Length - 1; u >= 0; u--)
                    {
                        output.Add(backwardsPath[u]);
                    }

                    backwardsPath.Dispose(); //Get rid of backwards path

                    //Return
                    pathfindSuccessful = true;

                    //Dispose of created containers
                    closedSet.Dispose();
                    openSet.Dispose();
                    scannedNodes.Dispose();

                    cameFrom.Dispose();
                    fromStartToThis.Dispose();
                    fromStartViaThis.Dispose();

                    return;
                }

                //Remove from the open set. There isn't an easy way to do this with the native list, so make a new one.
                NativeList<int> tempOpenSet = new NativeList<int>(Allocator.Temp);

                for (int i = 0; i < openSet.Length; i++)
                {
                    int num = openSet[i];
                    if (num == current) continue;
                    tempOpenSet.Add(num);
                }

                //Clear open set and replace it with the new one
                openSet.Clear();
                openSet.AddRange(tempOpenSet);
                tempOpenSet.Dispose(); //Get rid of the temp list

                closedSet.TryAdd(current, 1); //Add to closed set

                //Scan for entrances in this room...
                //Iterate hash map
                NativeMultiHashMapIterator<float3> iteratorAcc;
                int accessNeighbor;
                if (accessRef.TryGetFirstValue(toNode, out accessNeighbor, out iteratorAcc))
                {
                    do
                    {
                        //if neighbor in closedSet
                        // Ignore the neighbor which is already evaluated.
                        if (closedSet.TryGetValue(accessNeighbor, out _)) continue;

                        float3 accessTo = toNodeReference[accessNeighbor];

                        //The path weight from start to the neighbor
                        float nodeDistance = Vector3.Distance(toNode, accessTo);

                        //We're adding the pre-calculated weight from the node of the entrance we just walked into, to the possible next entrance to (exterior/to -> interior/from)
                        float tentativeGScore = fromStartToThis[current] + nodeDistance; //TODO: Implement node weights- for now use distance value

                        //Discover a new node- add to open set
                        if (!openSet.Contains(accessNeighbor))
                        {
                            if (!scannedNodes.TryGetValue(accessTo, out _))
                            {   
                                //This isn't in the no pass through nodes (or is the desintation)
                                if (!noPassRef.Contains(accessTo) || (accessTo.x == destination.x && accessTo.y == destination.y && accessTo.z == destination.z))
                                {
                                    //Game.Log("Pathfinder: Add " + neighbor.pathCoord + " to open set, count: " + openSet.Count);
                                    openSet.Add(accessNeighbor);
                                    scannedNodes.TryAdd(accessTo, 1);
                                }
                            }
                        }

                        //Path hasn't been added (assume infinity) if tentative score is lower then this is automatically the best until now.
                        if (!fromStartToThis.TryGetValue(accessNeighbor, out _) || tentativeGScore < fromStartToThis[accessNeighbor])
                        {
                            if (!cameFrom.TryGetValue(accessNeighbor, out _)) cameFrom.TryAdd(accessNeighbor, current);
                            else
                            {
                                //Replace
                                cameFrom.Remove(accessNeighbor);
                                cameFrom.TryAdd(accessNeighbor, current);
                            }

                            if (!fromStartToThis.TryGetValue(accessNeighbor, out _)) fromStartToThis.TryAdd(accessNeighbor, tentativeGScore);
                            else
                            {
                                //Replace
                                fromStartToThis.Remove(accessNeighbor);
                                fromStartToThis.TryAdd(accessNeighbor, tentativeGScore);
                            }

                            //This is for checking which node to anylise next, the lowest will be picked...
                            float viaThis = tentativeGScore + Vector3.Distance(accessPositions[accessNeighbor], destination);

                            if (!fromStartViaThis.TryGetValue(accessNeighbor, out _)) fromStartViaThis.TryAdd(accessNeighbor, viaThis);
                            else
                            {
                                //Replace
                                fromStartViaThis.Remove(accessNeighbor);
                                fromStartViaThis.TryAdd(accessNeighbor, viaThis);
                            }
                        }

                    } while (accessRef.TryGetNextValue(out accessNeighbor, ref iteratorAcc));
                }

                calculationLoops++;

                if (calculationLoops > 99999)
                {
                    //Game.Log("PathFinder: Loop count = " + calculationLoops);
                    //Game.LogError("PathFinder: Internal pathfinding " + listIndex + " failsafe reached, check for cascades! " + origin + "->" + destination + " Closed: " +closedSet.Length);

                    //Dispose of created containers
                    closedSet.Dispose();
                    openSet.Dispose();
                    scannedNodes.Dispose();

                    cameFrom.Dispose();
                    fromStartToThis.Dispose();
                    fromStartViaThis.Dispose();

                    return;
                }
            }

            if (!pathfindSuccessful)
            {
                //Game.Log("PathFinder: Loop count = " + calculationLoops);
                //Game.LogError("PathFinder: Internal pathfinding " + listIndex + "unsucessful- there is an unreachable node at " + origin + "->" + destination + " Closed: " +closedSet.Length);
            }

            //Dispose of created containers
            closedSet.Dispose();
            openSet.Dispose();
            scannedNodes.Dispose();

            cameFrom.Dispose();
            fromStartToThis.Dispose();
            fromStartViaThis.Dispose();

            return;
        }
    }
}
