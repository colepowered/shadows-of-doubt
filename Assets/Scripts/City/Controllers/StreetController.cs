﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;
using NaughtyAttributes;

//A controller given to every road in the city.
//Script pass 1
public class StreetController : NewGameLocation, IComparable<StreetController>
{
    [Header("ID")]
    public int streetID = 0;
    public static int assignID = 1;

    [Header("Details")]
    public List<NewTile> tiles = new List<NewTile>();
    public string streetSuffix = string.Empty;
    public bool isAlley = false; //True if this road is an alleyway
    public bool isBackstreet = false; //True if this is street space behind a building
    public float normalizedFootfall = 0;
    public int chunkSize = 0; //Only available on creation, how many street chunks have been added to this

    //For static batching
    Dictionary<NewRoom.StaticBatchKey, List<GameObject>> staticBatchDictionary = new Dictionary<NewRoom.StaticBatchKey, List<GameObject>>();

    [Header("Road Tile Setup")]
    public List<PathFinder.StreetChunk> streetChunks = new List<PathFinder.StreetChunk>(); //Set on creation, can be used to determin road parts...
    public List<StreetTile> streetSections = new List<StreetTile>();
    public Dictionary<MeshRenderer, StreetTilePreset.StreetSectionModel> loadedModelReference = new Dictionary<MeshRenderer, StreetTilePreset.StreetSectionModel>();

    [System.Serializable]
    public class StreetTile
    {
        public string name;
        public Vector3 worldPos;
        public StreetTilePreset.StreetSection section;
        public int angle = 0;

        public StreetTile(string chunkName, Vector3 newWorldPos, StreetTilePreset.StreetSection newSection, int newAngle)
        {
            name = chunkName;
            worldPos = newWorldPos;
            section = newSection;
            angle = newAngle;
        }
    }

    //Needs this to be spawned to display the correct road ground elements
    public List<StreetController> sharedGroundElements = new List<StreetController>();

    //List of contents
    public List<string> debugAddressSet = new List<string>();

    public void Setup(DistrictController newDistrict)
    {
        //Assign ID
        streetID = assignID;
        assignID++;
        district = newDistrict;
        //baseSeed = CityData.Instance.seed + streetID.ToString();
        name = "Street " + streetID;
        this.transform.name = name;

        this.transform.SetParent(newDistrict.transform, true);

        //Create evidence file
        CreateEvidence();

        //Run base setup
        base.CommonSetup(true, newDistrict, CityControls.Instance.street);
    }

    //Load
    public void Load(CitySaveData.StreetCitySave data)
    {
        name = data.name;
        this.transform.name = name;
        residenceNumber = data.residenceNumber; //Residence number relative to this floor (set in addresses only)
        isLobby = data.isLobby; //Game Only: True if this is a building lobby (set in addresses only)
        //isMainLobby = data.isMainLobby; //Game Only: True if this lobby features the main entrance
        isOutside = data.isOutside; //Is this location outside?
        access = data.access;

        streetID = data.streetID;
        assignID = Mathf.Max(assignID, streetID + 1); //Make sure others won't overwrite this ID
        district = CityData.Instance.districtDirectory.Find(item => item.districtID == data.district);
        streetSuffix = data.streetSuffix;
        isAlley = data.isAlley; //True if this road is an alleyway
        isBackstreet = data.isBackstreet; //True if this is street space behind a building

        //For now, create evidence
        CreateEvidence();

        if (data.sharedGround != null)
        {
            sharedGroundElements.Clear();

            foreach(int stID in data.sharedGround)
            {
                StreetController foundStreet = CityData.Instance.streetDirectory.Find(item => item.streetID == stID);

                if(foundStreet != null)
                {
                    sharedGroundElements.Add(foundStreet);
                }
            }
        }

        this.transform.SetParent(district.transform, true);

        //Run base setup
        base.CommonSetup(true, district, CityControls.Instance.street);

        //Add tiles
        //Game.Log("Loading street with " + data.tiles.Count + " tiles...");

        foreach (Vector3Int tile in data.tiles)
        {
            NewTile foundTile = PathFinder.Instance.tileMap[tile];
            AddTile(foundTile);

            //Run setup if not done already
            if (!foundTile.isSetup)
            {
                //Game.Log("Setting up tile " + tile);

                CitySaveData.CityTileCitySave cityTileData = CityConstructor.Instance.currentData.cityTiles.Find(item => item.cityCoord == foundTile.cityTile.cityCoord);
                CitySaveData.TileCitySave tileData = cityTileData.outsideTiles.Find(item => item.tileID == foundTile.tileID);
                foundTile.LoadExterior(tileData);
                foundTile.parent = foundTile.streetController.transform;
            }
        }

        //Load rooms
        foreach(CitySaveData.RoomCitySave room in data.rooms)
        {
            NewRoom newRoom = null;

            if (!room.isBaseNullRoom)
            {
                //Create new room
                GameObject newObj = Instantiate(PrefabControls.Instance.room, this.transform);
                newRoom = newObj.GetComponent<NewRoom>();
            }
            else
            {
                //If this is the null room, it is loaded 'on top' of the existing created room...
                newRoom = nullRoom;
            }

            CitySaveData.RoomCitySave loadData = room;

            //Do we have decor override data for this?
            if (CityConstructor.Instance != null && CityConstructor.Instance.saveState != null)
            {
                StateSaveData.RoomStateSave rOvr = CityConstructor.Instance.saveState.rooms.Find(item => item.id == room.id && item.decorOverride != null && item.decorOverride.Count > 0);

                if(rOvr != null)
                {
                    Game.Log("CityGen: Found save state room decor override for: " + room.name);
                    loadData = rOvr.decorOverride[0];
                    newRoom.decorEdit = true;
                }
            }

            newRoom.Load(loadData, this);
        }

        //Load street tiles
        streetSections.AddRange(data.streetTiles);
    }

    //Add a tile
    public void AddTile(NewTile newTile)
    {
        if(!tiles.Contains(newTile))
        {
            tiles.Add(newTile);

            //Add to street
            newTile.streetController = this;
            newTile.parent = newTile.streetController.transform;

            foreach (NewNode node in newTile.nodes)
            {
                newTile.streetController.nullRoom.AddNewNode(node);
            }

            //Set to belong to largest street
            //if(newTile.streetController != newTile.streets[0])
            //{
            //    //Remove from previous street
            //    if(newTile.streetController != null)
            //    {
            //        newTile.streetController.RemoveTile(newTile);
            //    }

            //    newTile.streetController = newTile.streets[0];
            //    newTile.streetController.tiles.Add(newTile);
            //    newTile.parent = newTile.streetController.transform;

            //    //Transfer street objects
            //    List<Transform> foundObj = null;

            //    if (newTile.cityTile.streetObjectReference.TryGetValue(newTile.globalTileCoord, out foundObj))
            //    {
            //        if (newTile.streetController.nullRoom.streetObjectContainer == null)
            //        {
            //            newTile.streetController.nullRoom.streetObjectContainer = new GameObject();
            //            newTile.streetController.nullRoom.streetObjectContainer.transform.SetParent(newTile.streetController.nullRoom.transform, false);
            //            newTile.streetController.nullRoom.streetObjectContainer.transform.localPosition = Vector3.zero;
            //            newTile.streetController.nullRoom.streetObjectContainer.transform.name = "StreetObjectContainer";
            //        }

            //        foreach (Transform tr in foundObj)
            //        {
            //            tr.SetParent(newTile.streetController.nullRoom.streetObjectContainer.transform, true);
            //        }
            //    }

            //    //Also add nodes
            //    foreach (NewNode node in newTile.nodes)
            //    {
            //        newTile.streetController.nullRoom.AddNewNode(node);
            //        //newTile.streetController.AddNewNode(node);
            //    }
            //}

            //Add collider for debug purposes
            //newTile.gameObject.AddComponent<BoxCollider>();
            //newTile.gameObject.GetComponent<BoxCollider>().size = new Vector3(1, 1, 1);
        }
    }

    //Remove a tile
    public void RemoveTile(NewTile newTile)
    {
        if(tiles.Contains(newTile))
        {
            newTile.streetController = null;
            tiles.Remove(newTile);
        }
    }

    //Set as an alley way: This has a dead end with low footfall
    public void SetAsAlley()
    {
        isAlley = true;

        foreach(NewRoom room in rooms)
        {
            room.SetConfiguration(CityControls.Instance.alleyRoom);
        }
    }

    //This has a low footfall but isn't a dead end
    public void SetAsBackstreet()
    {
        isBackstreet = true;

        foreach (NewRoom room in rooms)
        {
            room.SetConfiguration(CityControls.Instance.backstreetRoom);
        }
    }

    //This is a 'normal' street
    public void SetAsStreet()
    {
        foreach (NewRoom room in rooms)
        {
            room.SetConfiguration(CityControls.Instance.streetRoom);
        }
    }

    //Generate name
    public void UpdateName()
    {
        Descriptors.EthnicGroup chosenGroup = district.EthnictiyBasedOnDominance();

        //Surname override
        SocialStatistics.EthnicityStats setting = SocialStatistics.Instance.ethnicityStats.Find(item => item.group == chosenGroup);
        string surname = chosenGroup.ToString();
        if (setting.overrideSur) surname = setting.overrideNameSur.ToString();

        string pKey = seed;

        //Generate a name using the district's ethnicity dominance
        name = NameGenerator.Instance.GenerateName(null, 0f, "names." + surname + ".sur", 1f, null, 0f, pKey);

        //The smaller the road, the more chance of getting a small road suffix.
        if (Toolbox.Instance.GetPsuedoRandomNumberContained(0, 8, pKey, out pKey) >= tiles.Count)
        {
            streetSuffix = NameGenerator.Instance.GenerateName(null, 0f, "names.street.suffix.small", 1f, null, 0f, pKey);
        }
        else
        {
            //Generally, all north/south facing streets are 'Avenues' (like NYC) and east/west facing are 'Streets'
            Vector2 xRange = new Vector2(Mathf.Infinity, -1);
            Vector2 yRange = new Vector2(Mathf.Infinity, -1);

            foreach (NewTile tile in tiles)
            {
                xRange = new Vector2(Mathf.Min(xRange.x, tile.globalTileCoord.x), Mathf.Max(xRange.x, tile.globalTileCoord.x));
                yRange = new Vector2(Mathf.Min(yRange.y, tile.globalTileCoord.y), Mathf.Max(yRange.y, tile.globalTileCoord.y));
            }

            //If a x range is fairly minimal, this is a north/south street
            if (Mathf.Abs(xRange.y - xRange.x) < 1f)
            {
                streetSuffix = Strings.Get("names.street.suffix", "avenue");
            }
            else if (Mathf.Abs(yRange.y - yRange.x) < 1f)
            {
                streetSuffix = Strings.Get("names.street.suffix", "street");
            }
            //Else is a boulevard or parade
            else
            {
                if (Toolbox.Instance.GetPsuedoRandomNumberContained(0, 100, pKey, out pKey) > 50)
                {
                    streetSuffix = Strings.Get("names.street.suffix", "boulevard");
                }
                else
                {
                    streetSuffix = Strings.Get("names.street.suffix", "parade");
                }
            }
        }

        //Put together for name
        name = name + " " + streetSuffix;

        //Make sure name is unique
        int safetyAttempts = 99;

        while (CityData.Instance.streetDirectory.Exists(item => item != this && item.name == name) && safetyAttempts > 0)
        {
            //Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, seed);

            //Generate a name using the district's ethnicity dominance
            name = NameGenerator.Instance.GenerateName(null, 0f, "names." + surname + ".sur", 1f, null, 0f, seed);

            //The smaller the road, the more chance of getting a small road suffix.
            if (Toolbox.Instance.GetPsuedoRandomNumber(0, 8, seed) >= tiles.Count)
            {
                //Toolbox.Instance.GetPsuedoRandomNumberContained(0f, 1f, seed, out seed);
                streetSuffix = NameGenerator.Instance.GenerateName(null, 0f, "names.street.suffix.small", 1f, null, 0f, seed);
            }
            else
            {
                //Generally, all north/south facing streets are 'Avenues' (like NYC) and east/west facing are 'Streets'
                Vector2 xRange = new Vector2(Mathf.Infinity, -1);
                Vector2 yRange = new Vector2(Mathf.Infinity, -1);

                foreach (NewTile tile in tiles)
                {
                    xRange = new Vector2(Mathf.Min(xRange.x, tile.globalTileCoord.x), Mathf.Max(xRange.x, tile.globalTileCoord.x));
                    yRange = new Vector2(Mathf.Min(yRange.y, tile.globalTileCoord.y), Mathf.Max(yRange.y, tile.globalTileCoord.y));
                }

                //If a x range is fairly minimal, this is a north/south street
                if (Mathf.Abs(xRange.y - xRange.x) < 1f)
                {
                    streetSuffix = Strings.Get("names.street.suffix", "avenue");
                }
                else if (Mathf.Abs(yRange.y - yRange.x) < 1f)
                {
                    streetSuffix = Strings.Get("names.street.suffix", "street");
                }
                //Else is a boulevard or parade
                else
                {
                    if (Toolbox.Instance.GetPsuedoRandomNumber(0, 100, seed) > 50)
                    {
                        streetSuffix = Strings.Get("names.street.suffix", "boulevard");
                    }
                    else
                    {
                        streetSuffix = Strings.Get("names.street.suffix", "parade");
                    }
                }
            }

            safetyAttempts--;
        }

        this.transform.name = name;
    }

    public override bool IsOutside()
    {
        return true;
    }

    //Add a chunk (on road creation only)
    public void AddChunk(PathFinder.StreetChunk newChunk)
    {
        if(!streetChunks.Contains(newChunk))
        {
            streetChunks.Add(newChunk);
        }
    }

    //Get neighboring streets...
    public List<StreetController> GetNeighboringStreets()
    {
        List<StreetController> ret = new List<StreetController>();

        foreach(NewTile t in tiles)
        {
            foreach(Vector2Int v2 in CityData.Instance.offsetArrayX4)
            {
                Vector3 searchCoord = t.globalTileCoord + new Vector3Int(v2.x, v2.y, 0);
                NewTile foundTile = null;

                if(PathFinder.Instance.tileMap.TryGetValue(searchCoord, out foundTile))
                {
                    if(foundTile.streetController != null && foundTile.streetController != this)
                    {
                        if(!ret.Contains(foundTile.streetController))
                        {
                            ret.Add(foundTile.streetController);
                        }
                    }
                }
            }
        }

        return ret;
    }

    //Default comparer (road size)
    public int CompareTo(StreetController otherObject)
    {
        return this.nullRoom.nodes.Count.CompareTo(otherObject.nullRoom.nodes.Count);
    }

    public override void CreateEvidence()
    {
        //Create citizen entry
        if (evidenceEntry == null)
        {
            evidenceEntry = EvidenceCreator.Instance.CreateEvidence("street", "Street" + streetID, this) as EvidenceLocation;
        }
    }

    //The evidence entry file should already be made- run this on creation
    public override void SetupEvidence()
    {
        base.SetupEvidence();
    }

    public CitySaveData.StreetCitySave GenerateSaveData()
    {
        CitySaveData.StreetCitySave output = new CitySaveData.StreetCitySave();

        output.name = name;
        output.district = district.districtID;
        output.residenceNumber = residenceNumber; //Residence number relative to this floor (set in addresses only)
        output.isLobby = isLobby; //Game Only: True if this is a building lobby (set in addresses only)
        //output.isMainLobby = isMainLobby; //Game Only: True if this lobby features the main entrance
        output.isOutside = isOutside; //Is this location outside?
        output.access = access;
        output.designStyle = designStyle.name; //Design style chosen for the address

        //foreach(StreetController str in visibleFromThis)
        //{
        //    output.visibleStreets.Add(str.streetID);
        //}

        output.streetID = streetID;
        output.streetTiles = new List<StreetTile>(streetSections);

        foreach(NewTile tile in tiles)
        {
            output.tiles.Add(Vector3Int.RoundToInt(tile.globalTileCoord));
        }
        
        output.streetSuffix = streetSuffix;
        output.isAlley = isAlley; //True if this road is an alleyway
        output.isBackstreet = isBackstreet; //True if this is street space behind a building

        foreach(NewRoom room in rooms)
        {
            output.rooms.Add(room.GenerateSaveData());
        }

        if(sharedGroundElements.Count > 0)
        {
            output.sharedGround = new List<int>();

            foreach(StreetController st in sharedGroundElements)
            {
                output.sharedGround.Add(st.streetID);
            }
        }

        return output;
    }

    //Load in street tiles
    public void LoadStreetTiles()
    {
        //Calculate street tiles per chunk
        foreach(PathFinder.StreetChunk chunk in streetChunks)
        {
            if (chunk.allTiles.Count <= 0) continue;

            //Calculate the average position...
            Vector3 avPos = Vector3.zero;

            foreach(NewTile t in chunk.allTiles)
            {
                avPos += CityData.Instance.TileToRealpos(t.globalTileCoord);
            }

            avPos /= (float)chunk.allTiles.Count;

            if(chunk.isJunction)
            {
                //Listed by offset (-1, 0) = left etc.
                Dictionary<Vector2, PathFinder.StreetChunk> adjacentChunks = new Dictionary<Vector2, PathFinder.StreetChunk>();
                Dictionary<Vector2, bool> adjacentRoads = new Dictionary<Vector2, bool>(); //Only true if featuring roads
                int adjacentRoadCount = 0;

                foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                {
                    adjacentRoads.Add(v2, false);
                }

                bool leftRoad = false;
                bool upRoad = false;
                bool rightRoad = false;
                bool downRoad = false;

                //Get adjoining chunks...
                foreach (Vector3 t in chunk.allCoords)
                {
                    foreach (Vector2Int v2 in CityData.Instance.offsetArrayX4)
                    {
                        if (adjacentChunks.ContainsKey(v2)) continue; //Already found this chunk

                        Vector3 tileSearch = t + new Vector3(v2.x, v2.y, 0);
                        NewTile foundTile;

                        if (PathFinder.Instance.tileMap.TryGetValue(tileSearch, out foundTile))
                        {
                            if (foundTile.streetChunk != null && foundTile.streetChunk != chunk)
                            {
                                adjacentChunks.Add(v2, foundTile.streetChunk);

                                //Detect road sections
                                if (!foundTile.streetController.isAlley && !foundTile.streetController.isBackstreet)
                                {
                                    adjacentRoads[v2] = true;
                                    adjacentRoadCount++;

                                    if(v2.x < 0)
                                    {
                                        leftRoad = true;
                                    }
                                    else if(v2.x > 0)
                                    {
                                        rightRoad = true;
                                    }
                                    else if(v2.y > 0)
                                    {
                                        upRoad = true;
                                    }
                                    else if(v2.y < 0)
                                    {
                                        downRoad = true;
                                    }
                                }
                            }
                        }
                    }
                }

                //All joiner (no road connections)
                if(adjacentRoadCount <= 0)
                {
                    //All 4 sections are joiner section
                    //Starts from top left, rotates around clockwise...
                    for (int i = 0; i < CityData.Instance.offsetArrayX4StreetJunction.Length; i++)
                    {
                        Vector2 v2 = CityData.Instance.offsetArrayX4StreetJunction[i];
                        Vector3 junctPos = avPos + new Vector3(PathFinder.Instance.tileSize.x * v2.x, 0, PathFinder.Instance.tileSize.y * v2.y);

                        streetSections.Add(new StreetTile(chunk.name, junctPos, StreetTilePreset.StreetSection.joinerShort, i * 90));
                    }
                }
                //All street (crossroads)
                else if(adjacentRoadCount >= 4)
                {
                    //All 4 sections are joiner section
                    //Starts from top left, rotates around clockwise...
                    for (int i = 0; i < CityData.Instance.offsetArrayX4StreetJunction.Length; i++)
                    {
                        Vector2 v2 = CityData.Instance.offsetArrayX4StreetJunction[i];
                        Vector3 junctPos = avPos + new Vector3(PathFinder.Instance.tileSize.x * v2.x, 0, PathFinder.Instance.tileSize.y * v2.y);

                        streetSections.Add(new StreetTile(chunk.name, junctPos, StreetTilePreset.StreetSection.streetJunctionCorner, i * 90));
                    }
                }
                //Dead ends
                else if(adjacentRoadCount == 1)
                {
                    for (int i = 0; i < CityData.Instance.offsetArrayX4StreetJunction.Length; i++)
                    {
                        StreetTilePreset.StreetSection section = StreetTilePreset.StreetSection.streetInsideCorner;
                        int rot = 0;

                        if(leftRoad)
                        {
                            if (i == 0)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 270;
                            }
                            else if(i == 1)
                            {
                                section = StreetTilePreset.StreetSection.streetOutsideCorner;
                                rot = 270;
                            }
                            else if (i == 2)
                            {
                                section = StreetTilePreset.StreetSection.streetOutsideCorner;
                                rot = 0;
                            }
                            else if (i == 3)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 90;
                            }
                        }
                        else if (rightRoad)
                        {
                            if (i == 0)
                            {
                                section = StreetTilePreset.StreetSection.streetOutsideCorner;
                                rot = 180;
                            }
                            else if (i == 1)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 270;
                            }
                            else if (i == 2)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 90;
                            }
                            else if (i == 3)
                            {
                                section = StreetTilePreset.StreetSection.streetOutsideCorner;
                                rot = 90;
                            }
                        }
                        else if (upRoad)
                        {
                            if (i == 0)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 180;
                            }
                            else if (i == 1)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 0;
                            }
                            else if (i == 2)
                            {
                                section = StreetTilePreset.StreetSection.streetOutsideCorner;
                                rot = 0;
                            }
                            else if (i == 3)
                            {
                                section = StreetTilePreset.StreetSection.streetOutsideCorner;
                                rot = 90;
                            }
                        }
                        else if (downRoad)
                        {
                            if (i == 0)
                            {
                                section = StreetTilePreset.StreetSection.streetOutsideCorner;
                                rot = 180;
                            }
                            else if (i == 1)
                            {
                                section = StreetTilePreset.StreetSection.streetOutsideCorner;
                                rot = 270;
                            }
                            else if (i == 2)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 0;
                            }
                            else if (i == 3)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 180;
                            }
                        }

                        Vector2 v2 = CityData.Instance.offsetArrayX4StreetJunction[i];
                        Vector3 junctPos = avPos + new Vector3(PathFinder.Instance.tileSize.x * v2.x, 0, PathFinder.Instance.tileSize.y * v2.y);

                        streetSections.Add(new StreetTile(chunk.name, junctPos, section, rot));
                    }
                }
                //Joins and corners
                else if(adjacentRoadCount == 2)
                {
                    for (int i = 0; i < CityData.Instance.offsetArrayX4StreetJunction.Length; i++)
                    {
                        StreetTilePreset.StreetSection section = StreetTilePreset.StreetSection.streetInsideCorner;
                        int rot = 0;

                        //Horizontal join
                        if (leftRoad && rightRoad)
                        {
                            if (i == 0)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 270;
                            }
                            else if (i == 1)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 270;
                            }
                            else if (i == 2)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 90;
                            }
                            else if (i == 3)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 90;
                            }
                        }
                        //Vertical join
                        else if (upRoad && downRoad)
                        {
                            if (i == 0)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 180;
                            }
                            else if (i == 1)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 0;
                            }
                            else if (i == 2)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 0;
                            }
                            else if (i == 3)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 180;
                            }
                        }
                        //Corners
                        else if (leftRoad && downRoad)
                        {
                            if (i == 0)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 270;
                            }
                            else if (i == 1)
                            {
                                section = StreetTilePreset.StreetSection.streetOutsideCorner;
                                rot = 270;
                            }
                            else if (i == 2)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 0;
                            }
                            else if (i == 3)
                            {
                                section = StreetTilePreset.StreetSection.streetInsideCorner;
                                rot = 270;
                            }
                        }
                        else if (leftRoad && upRoad)
                        {
                            if (i == 0)
                            {
                                section = StreetTilePreset.StreetSection.streetInsideCorner;
                                rot = 0;
                            }
                            else if (i == 1)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 0;
                            }
                            else if (i == 2)
                            {
                                section = StreetTilePreset.StreetSection.streetOutsideCorner;
                                rot = 0;
                            }
                            else if (i == 3)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 90;
                            }
                        }
                        else if (rightRoad && downRoad)
                        {
                            if (i == 0)
                            {
                                section = StreetTilePreset.StreetSection.streetOutsideCorner;
                                rot = 180;
                            }
                            else if (i == 1)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 270;
                            }
                            else if (i == 2)
                            {
                                section = StreetTilePreset.StreetSection.streetInsideCorner;
                                rot = 180;
                            }
                            else if (i == 3)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 180;
                            }
                        }
                        else if (rightRoad && upRoad)
                        {
                            if (i == 0)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 180;
                            }
                            else if (i == 1)
                            {
                                section = StreetTilePreset.StreetSection.streetInsideCorner;
                                rot = 90;
                            }
                            else if (i == 2)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 90;
                            }
                            else if (i == 3)
                            {
                                section = StreetTilePreset.StreetSection.streetOutsideCorner;
                                rot = 90;
                            }
                        }

                        Vector2 v2 = CityData.Instance.offsetArrayX4StreetJunction[i];
                        Vector3 junctPos = avPos + new Vector3(PathFinder.Instance.tileSize.x * v2.x, 0, PathFinder.Instance.tileSize.y * v2.y);

                        streetSections.Add(new StreetTile(chunk.name, junctPos, section, rot));
                    }
                }
                //T Junctions
                else if(adjacentRoadCount == 3)
                {
                    for (int i = 0; i < CityData.Instance.offsetArrayX4StreetJunction.Length; i++)
                    {
                        StreetTilePreset.StreetSection section = StreetTilePreset.StreetSection.streetInsideCorner;
                        int rot = 0;

                        if (!leftRoad)
                        {
                            if (i == 0)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 180;
                            }
                            else if (i == 1)
                            {
                                section = StreetTilePreset.StreetSection.streetJunctionCorner;
                                rot = 90;
                            }
                            else if (i == 2)
                            {
                                section = StreetTilePreset.StreetSection.streetJunctionCorner;
                                rot = 180;
                            }
                            else if (i == 3)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 180;
                            }
                        }
                        else if (!upRoad)
                        {
                            if (i == 0)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 270;
                            }
                            else if (i == 1)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 270;
                            }
                            else if (i == 2)
                            {
                                section = StreetTilePreset.StreetSection.streetJunctionCorner;
                                rot = 180;
                            }
                            else if (i == 3)
                            {
                                section = StreetTilePreset.StreetSection.streetJunctionCorner;
                                rot = 270;
                            }
                        }
                        else if (!rightRoad)
                        {
                            if (i == 0)
                            {
                                section = StreetTilePreset.StreetSection.streetJunctionCorner;
                                rot = 0;
                            }
                            else if (i == 1)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 0;
                            }
                            else if (i == 2)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 0;
                            }
                            else if (i == 3)
                            {
                                section = StreetTilePreset.StreetSection.streetJunctionCorner;
                                rot = 270;
                            }
                        }
                        else if (!downRoad)
                        {
                            if (i == 0)
                            {
                                section = StreetTilePreset.StreetSection.streetJunctionCorner;
                                rot = 0;
                            }
                            else if (i == 1)
                            {
                                section = StreetTilePreset.StreetSection.streetJunctionCorner;
                                rot = 90;
                            }
                            else if (i == 2)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 90;
                            }
                            else if (i == 3)
                            {
                                section = StreetTilePreset.StreetSection.streetShort;
                                rot = 90;
                            }
                        }

                        Vector2 v2 = CityData.Instance.offsetArrayX4StreetJunction[i];
                        Vector3 junctPos = avPos + new Vector3(PathFinder.Instance.tileSize.x * v2.x, 0, PathFinder.Instance.tileSize.y * v2.y);

                        streetSections.Add(new StreetTile(chunk.name, junctPos, section, rot));
                    }
                }
            }
            //Non junctions are always either road or not road...
            else
            {
                StreetTilePreset.StreetSection sectionType = StreetTilePreset.StreetSection.streetLong;
                if (isBackstreet || isAlley) sectionType = StreetTilePreset.StreetSection.joinerLong;

                if (chunk.isHorizontal)
                {
                    Vector3 horizontalPosTop = avPos + new Vector3(0, 0, PathFinder.Instance.tileSize.y * 0.5f);
                    Vector3 horizontalPosBottom = avPos - new Vector3(0, 0, PathFinder.Instance.tileSize.y * 0.5f);

                    streetSections.Add(new StreetTile(chunk.name, horizontalPosTop, sectionType, 90));
                    streetSections.Add(new StreetTile(chunk.name, horizontalPosBottom, sectionType, 270));
                }
                else
                {
                    Vector3 horizontalPosLeft = avPos - new Vector3(PathFinder.Instance.tileSize.x * 0.5f, 0, 0);
                    Vector3 horizontalPosRight = avPos + new Vector3(PathFinder.Instance.tileSize.x * 0.5f, 0, 0);

                    streetSections.Add(new StreetTile(chunk.name, horizontalPosLeft, sectionType, 0));
                    streetSections.Add(new StreetTile(chunk.name, horizontalPosRight, sectionType, 180));
                }
            }
        }
    }

    public void LoadSections()
    {
        foreach(StreetTile t in streetSections)
        {
            //Generate a string which will be repeatable to generate a random number
            string seed = t.worldPos.ToString() + t.angle.ToString();

            StreetTilePreset.StreetSectionModel model = GetModel(t.section, seed);

            //Load the object
            GameObject nObj = Instantiate(model.prefab, nullRoom.transform);
            nObj.transform.position = t.worldPos;
            nObj.transform.rotation = Quaternion.Euler(0, t.angle, 0);

            nObj.transform.tag = "Street";
            nObj.name = t.name;

            MeshRenderer rend = nObj.GetComponent<MeshRenderer>();
            MeshFilter mesh = nObj.GetComponent<MeshFilter>();

            loadedModelReference.Add(rend, model);

            //Change to rain material
            if (Game.Instance.enableRaindrops && model.rainMaterial != null)
            {
                rend.sharedMaterial = model.rainMaterial;
            }
            else
            {
                rend.sharedMaterial = model.normalMaterial;
            }

            AddForStaticBatching(nObj, mesh.sharedMesh, rend.sharedMaterial);
        }

        ExecuteStaticBatching();
    }

    //Potentially add this wall frontage for static batching (to be combined after)
    public void AddForStaticBatching(GameObject obj, Mesh objectMesh, Material objectMat)
    {
        if (Game.Instance.enableRuntimeStaticBatching)
        {
            //Create static batch key
            NewRoom.StaticBatchKey newKey = new NewRoom.StaticBatchKey { mesh = objectMesh, mat = objectMat };

            if (!staticBatchDictionary.ContainsKey(newKey))
            {
                staticBatchDictionary.Add(newKey, new List<GameObject>());
                staticBatchDictionary[newKey].Add(obj);
            }
            else
            {
                if (!staticBatchDictionary[newKey].Contains(obj))
                {
                    staticBatchDictionary[newKey].Add(obj);
                }
            }
        }
    }

    //Execute combine of compatible game objects in this room
    public void ExecuteStaticBatching()
    {
        if (Game.Instance.enableRuntimeStaticBatching)
        {
            foreach (KeyValuePair<NewRoom.StaticBatchKey, List<GameObject>> pair in staticBatchDictionary)
            {
                for (int i = 0; i < pair.Value.Count; i++)
                {
                    if (pair.Value[i] == null)
                    {
                        pair.Value.RemoveAt(i);
                        i--;
                    }
                }

                if (pair.Value.Count > 1)
                {
                    try
                    {
                        StaticBatchingUtility.Combine(pair.Value.ToArray(), pair.Value[0]);
                        //Game.Log("Runtime static batching combined " + pair.Value.Count + " objects using mesh " + pair.Key.mesh.name + " and material " + pair.Key.mat.name);
                    }
                    catch
                    {
                        Game.LogError("Error trying to execute static batching...");
                    }
                }
            }

            staticBatchDictionary.Clear(); //Clean up
        }
    }

    //Get a random tile using a seed that is replicable...
    private StreetTilePreset.StreetSectionModel GetModel(StreetTilePreset.StreetSection section, string seed)
    {
        List<StreetTilePreset> getPreset = Toolbox.Instance.allStreetTiles.FindAll(item => item.sectionType == section);

        if (getPreset.Count > 0)
        {
            int presetSeed = Toolbox.Instance.GetPsuedoRandomNumber((int)0, (int)getPreset.Count, seed);
            StreetTilePreset chosenPreset = getPreset[presetSeed];

            int modelSeed = Toolbox.Instance.GetPsuedoRandomNumber((int)0, (int)chosenPreset.prefabList.Count, seed);
            return chosenPreset.prefabList[modelSeed];
        }
        else
        {
            Game.LogError("CityGen: Unable to find street tile preset " + section.ToString());
            return null;
        }
    }

    [Button]
    public void Redecorate()
    {
        rooms[0].RemoveAllInhabitantFurniture(false, FurnitureClusterLocation.RemoveInteractablesOption.remove);
        GenerationController.Instance.FurnishRoom(rooms[0]);
    }
}
