﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.UIElements;

public class RainParticleController : MonoBehaviour
{
    [Header("Rain Settings")]
    [Tooltip("Rain speed")]
    public Vector2 speed = new Vector2(30, 38f);
    [Tooltip("Spawn particles this high (local position)")]
    public float spawnHeight = 20f;
    [Tooltip("The world Y position upon which the particles are set to the top of the simulation.")]
    public float rainFloorWorldYPos = -5f;
    [Tooltip("Minimum time between spawning particles (seconds)")]
    public Vector2 spawnInterval = new Vector2(1f, 1.5f);
    public GameObject particlePrefab;
    public Vector3 rotationEuler = new Vector3(0.01f, 0.01f, 0.01f);

    [Header("Snow Settings")]
    [Tooltip("Rain speed")]
    public Vector2 speedSnow = new Vector2(30, 38f);
    [Tooltip("Spawn particles this high (local position)")]
    public float spawnHeightSnow = 2f;
    [Tooltip("The world Y position upon which the particles are set to the top of the simulation.")]
    public float snowFloorWorldYPos = -5f;
    [Tooltip("Minimum time between spawning particles (seconds)")]
    public Vector2 spawnIntervalSnow = new Vector2(1f, 1.5f);
    public GameObject particlePrefabSnow;
    public Vector3 rotationEulerSnow = new Vector3(0.01f, 0.01f, 0.01f);
    public bool billboard = true;

    [Header("State")]
    [ReadOnly]
    public int desiredParticleCount = 20;
    [ReadOnly]
    public int actualParticleCount = 0;
    [ReadOnly]
    public List<RainParticle> particles = new List<RainParticle>();
    private float spawnIntervalTimer = 0f;
    public bool snowMode = false;

    [ReadOnly]
    public List<NewNode> validSpawnNodes = new List<NewNode>();

    public class RainParticle
    {
        public Transform trans;
        public float speed;
    }

    //Singleton pattern
    private static RainParticleController _instance;
    public static RainParticleController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        SetSnowMode(false, true);
    }

    public void SetSnowMode(bool val, bool forceUpdate = false)
    {
        if (val != snowMode || forceUpdate)
        {
            snowMode = val;
        }
    }

    //Called when the player changes nodes...
    public void UpdateValidSpawnNodes()
    {
        validSpawnNodes.Clear();

        if(SessionData.Instance.startedGame)
        {
            //Spawn on a per node basis around the player
            foreach (Vector2Int v2 in CityData.Instance.offsetArrayX24)
            {
                Vector3Int getNodeCoord = Player.Instance.currentNodeCoord + new Vector3Int(v2.x, v2.y, 0);
                NewNode foundNode = null;

                if (PathFinder.Instance.nodeMap.TryGetValue(getNodeCoord, out foundNode))
                {
                    if (foundNode.room == null) continue;
                    if (foundNode.room.gameLocation == null) continue;

                    try
                    {
                        if (foundNode.room.gameLocation.thisAsStreet != null || foundNode.room.preset.forceOutside == RoomConfiguration.OutsideSetting.forceOutside)
                        {
                            if (foundNode.floorType == NewNode.FloorTileType.floorOnly || foundNode.floorType == NewNode.FloorTileType.none)
                            {
                                validSpawnNodes.Add(foundNode);
                            }
                        }
                    }
                    catch
                    {

                    }
                }
            }
        }
    }

    private void Update()
    {
        if(SessionData.Instance.play && SessionData.Instance.startedGame)
        {
            if (validSpawnNodes.Count > 0 && (SessionData.Instance.currentSnow > 0f || SessionData.Instance.currentRain > 0f))
            {
                if (particles.Count < desiredParticleCount)
                {
                    //Spawn a new particle...
                    if (spawnIntervalTimer <= 0f)
                    {
                        Vector3 spawnPos = Vector3.zero;

                        if (snowMode)
                        {
                            if(GetSpawnPosition(out spawnPos))
                            {
                                GameObject newRain = Instantiate(particlePrefabSnow, this.transform.parent);

                                newRain.transform.position = spawnPos;
                                if (!billboard) newRain.transform.eulerAngles = this.transform.eulerAngles + new Vector3(Toolbox.Instance.Rand(-rotationEulerSnow.x, rotationEulerSnow.x), Toolbox.Instance.Rand(-rotationEulerSnow.y, rotationEulerSnow.y), Toolbox.Instance.Rand(-rotationEulerSnow.z, rotationEulerSnow.z));

                                spawnIntervalTimer += Toolbox.Instance.Rand(spawnIntervalSnow.x, spawnIntervalSnow.y);
                                particles.Add(new RainParticle { trans = newRain.transform, speed = Toolbox.Instance.Rand(speedSnow.x, speedSnow.y) });
                            }
                        }
                        else
                        {
                            if(GetSpawnPosition(out spawnPos))
                            {
                                GameObject newRain = Instantiate(particlePrefab, this.transform.parent);

                                newRain.transform.position = spawnPos;
                                newRain.transform.eulerAngles = this.transform.eulerAngles + new Vector3(Toolbox.Instance.Rand(-rotationEuler.x, rotationEuler.x), Toolbox.Instance.Rand(-rotationEuler.y, rotationEuler.y), Toolbox.Instance.Rand(-rotationEuler.z, rotationEuler.z));

                                spawnIntervalTimer += Toolbox.Instance.Rand(spawnInterval.x, spawnInterval.y);
                                particles.Add(new RainParticle { trans = newRain.transform, speed = Toolbox.Instance.Rand(speed.x, speed.y) });
                            }
                        }

                        actualParticleCount = particles.Count;
                    }
                    else
                    {
                        spawnIntervalTimer -= Time.deltaTime;
                    }
                }
            }

            //Move all particles down relative to the object
            if(snowMode)
            {
                for (int i = 0; i < particles.Count; i++)
                {
                    RainParticle r = particles[i];

                    if(snowMode && billboard)
                    {
                        r.trans.rotation = CameraController.Instance.cam.transform.rotation * Quaternion.Euler(rotationEulerSnow);
                        r.trans.position += Vector3.down * r.speed * Time.deltaTime * Mathf.Max(SessionData.Instance.currentSnow, 0.7f);
                    }
                    else  r.trans.position += -r.trans.up * r.speed * Time.deltaTime * Mathf.Max(SessionData.Instance.currentSnow, 0.7f);

                    //Use a Y position check instead of collider...
                    if (r.trans.position.y < snowFloorWorldYPos)
                    {
                        Vector3 spawnPos = Vector3.zero;

                        //Use this point as a chance to remove the particle
                        if (!GetSpawnPosition(out spawnPos) || particles.Count > desiredParticleCount)
                        {
                            Destroy(r.trans.gameObject);
                            particles.RemoveAt(i);
                            i--;
                            continue;
                        }

                        r.speed = Toolbox.Instance.Rand(speedSnow.x, speedSnow.y);

                        //Vector2 randomInCircle = UnityEngine.Random.insideUnitCircle * spawnCoverageRadius;
                        //r.trans.position = this.transform.TransformPoint(new Vector3(randomInCircle.x, spawnHeight, randomInCircle.y));

                        r.trans.position = spawnPos;
                        if (!billboard) r.trans.eulerAngles = this.transform.eulerAngles + new Vector3(Toolbox.Instance.Rand(-rotationEulerSnow.x, rotationEulerSnow.x), Toolbox.Instance.Rand(-rotationEulerSnow.y, rotationEulerSnow.y), Toolbox.Instance.Rand(-rotationEulerSnow.z, rotationEulerSnow.z));
                    }
                }
            }
            else
            {
                for (int i = 0; i < particles.Count; i++)
                {
                    RainParticle r = particles[i];

                    r.trans.position += -r.trans.up * r.speed * Time.deltaTime * Mathf.Max(SessionData.Instance.currentRain, 0.7f);

                    //Use a Y position check instead of collider...
                    if (r.trans.position.y < rainFloorWorldYPos)
                    {
                        Vector3 spawnPos = Vector3.zero;

                        //Use this point as a chance to remove the particle
                        if (!GetSpawnPosition(out spawnPos) || particles.Count > desiredParticleCount)
                        {
                            Destroy(r.trans.gameObject);
                            particles.RemoveAt(i);
                            i--;
                            continue;
                        }

                        r.speed = Toolbox.Instance.Rand(speed.x, speed.y);

                        //Vector2 randomInCircle = UnityEngine.Random.insideUnitCircle * spawnCoverageRadius;
                        //r.trans.position = this.transform.TransformPoint(new Vector3(randomInCircle.x, spawnHeight, randomInCircle.y));

                        r.trans.position = spawnPos;
                        r.trans.eulerAngles = this.transform.eulerAngles + new Vector3(Toolbox.Instance.Rand(-rotationEuler.x, rotationEuler.x), Toolbox.Instance.Rand(-rotationEuler.y, rotationEuler.y), Toolbox.Instance.Rand(-rotationEuler.z, rotationEuler.z));
                    }
                }
            }
        }
    }

    private bool GetSpawnPosition(out Vector3 spawn)
    {
        spawn = Vector3.zero;
        if (validSpawnNodes.Count <= 0) return false;

        NewNode randomNode = validSpawnNodes[Toolbox.Instance.Rand(0, validSpawnNodes.Count)];

        //Get random position inside node area...
        spawn = randomNode.position + new Vector3(Toolbox.Instance.Rand(PathFinder.Instance.nodeSize.x * -0.45f, PathFinder.Instance.nodeSize.x * 0.45f), 0, Toolbox.Instance.Rand(PathFinder.Instance.nodeSize.y * -0.45f, PathFinder.Instance.nodeSize.y * 0.45f));

        //Set height
        if(snowMode)
        {
            spawn.y = Player.Instance.transform.position.y + spawnHeightSnow;
        }
        else
        {
            spawn.y = Player.Instance.transform.position.y + spawnHeight;
        }

        return true;
    }
}
