﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CityInfoData
{
    //A small class that stores info for cities that can be loaded and previewed quickly in the menu, without having to load the entire data for the city...
    public string cityName = "New City";
    public string build;
    public string shareCode = "Abaos93rmnisf932";
    public Vector2 citySize = new Vector2(5, 5);
    //public CityData.PopulationAmount populationAmount = CityData.PopulationAmount.high;
    public int population = 0;
}
