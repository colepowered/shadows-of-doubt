using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class ScenePoserController : MonoBehaviour
{
    [Header("Components")]
    public CitizenOutfitController outfitController;

    [Header("State")]
    public Human human;
    public NewNode node;
    public ClothesPreset.OutfitCategory outfit;
    public GameObject spawnedLeft;
    public GameObject spawnedRight;

    public void SetupCitizen(SceneRecorder.ActorCapture newCapture)
    {
        this.transform.position = newCapture.pos;
        this.transform.eulerAngles = newCapture.rot;
        node = Toolbox.Instance.FindClosestValidNodeToWorldPosition(newCapture.pos);

        //Remove held objects: You need to call destroy immediate to have it happen at this specific point in the code...
        if (spawnedLeft != null && spawnedLeft.name != newCapture.lH.i)
        {
            DestroyImmediate(spawnedLeft);
        }

        if(spawnedRight != null && spawnedLeft.name != newCapture.rH.i)
        {
            DestroyImmediate(spawnedRight);
        }

        this.gameObject.SetActive(true);

        bool newCitizen = true;

        if (human != null && human.humanID == newCapture.id)
        {
            newCitizen = false;
        }

        if(Game.Instance.collectDebugData)
        {
            if (newCitizen)
            {
                Game.Log("Set up poser, old: " + human + " new " + newCapture.GetHuman() + " " + (ClothesPreset.OutfitCategory)newCapture.o);
            }
            else
            {
                Game.Log("Set up poser, new citizen " + newCapture.GetHuman() + " " + (ClothesPreset.OutfitCategory)newCapture.o);
            }
        }

        human = newCapture.GetHuman();

        this.name = "Poser: " + human.GetCitizenName();
        outfit = (ClothesPreset.OutfitCategory)newCapture.o;
        if(human.interactableController != null) this.transform.localScale = human.interactableController.transform.localScale;

        //Copy citizen outfit controller stats over...
        outfitController.outfits = new List<CitizenOutfitController.Outfit>();

        foreach (CitizenOutfitController.Outfit o in human.outfitController.outfits)
        {
            CitizenOutfitController.Outfit newO = new CitizenOutfitController.Outfit();
            newO.category = o.category;

            foreach(CitizenOutfitController.OutfitClothes cl in o.clothes)
            {
                CitizenOutfitController.OutfitClothes newCl = new CitizenOutfitController.OutfitClothes();
                newCl.clothes = cl.clothes;
                newCl.tags = cl.tags;
                newCl.baseColor = cl.baseColor;
                newCl.color1 = cl.color1;
                newCl.color2 = cl.color2;
                newCl.color3 = cl.color3;
                newCl.borrowed = cl.borrowed;

                newO.clothes.Add(newCl);
            }

            outfitController.outfits.Add(newO);
        }

        outfitController.human = human;

        outfitController.SetCurrentOutfit(outfit, true, newCitizen);

        //Load in limb positions
        if (newCapture.limb != null && newCapture.limb.Count > 0)
        {
            Game.Log("Loading limb positions for " + name);

            foreach (SceneRecorder.LimbCapture l in newCapture.limb)
            {
                Transform getLimb = outfitController.GetBodyAnchor((CitizenOutfitController.CharacterAnchor)l.a);

                if (getLimb != null)
                {
                    getLimb.rotation = l.wR;
                    getLimb.position = l.wP;
                }
            }
        }
        //If there is no limb capture, load in positions from animations...
        else
        {
            AnimationFrameReference.AnimationReference idleRef = AnimationFrameReference.Instance.GetAnimationReference((CitizenAnimationController.IdleAnimationState)newCapture.main, newCapture.pos.ToString());

            if(idleRef != null)
            {
                foreach(AnimationFrameReference.AnimationAnchorRef ar in idleRef.anim)
                {
                    Transform getLimb = outfitController.GetBodyAnchor(ar.anchor);

                    if (getLimb != null)
                    {
                        getLimb.localPosition = ar.localPos;
                        getLimb.localRotation = ar.localRot;
                    }
                }

                Game.Log("Loaded idle anim " + (CitizenAnimationController.IdleAnimationState)newCapture.main);
            }
            else
            {
                Game.Log("Could not find idle anim " + (CitizenAnimationController.IdleAnimationState)newCapture.main);
            }

            if(newCapture.main <= 0)
            {
                if(newCapture.sp == 1)
                {
                    AnimationFrameReference.AnimationReference walkRef = AnimationFrameReference.Instance.walkingReference[Toolbox.Instance.GetPsuedoRandomNumber(0, AnimationFrameReference.Instance.walkingReference.Count, newCapture.pos.ToString())];

                    if (walkRef != null)
                    {
                        foreach (AnimationFrameReference.AnimationAnchorRef ar in walkRef.anim)
                        {
                            Transform getLimb = outfitController.GetBodyAnchor(ar.anchor);

                            if (getLimb != null)
                            {
                                getLimb.localPosition = ar.localPos;
                                getLimb.localRotation = ar.localRot;
                            }
                        }
                    }
                }
                else if(newCapture.sp == 2)
                {
                    AnimationFrameReference.AnimationReference runRef = AnimationFrameReference.Instance.walkingReference[Toolbox.Instance.GetPsuedoRandomNumber(0, AnimationFrameReference.Instance.runningReference.Count, newCapture.pos.ToString())];

                    if (runRef != null)
                    {
                        foreach (AnimationFrameReference.AnimationAnchorRef ar in runRef.anim)
                        {
                            Transform getLimb = outfitController.GetBodyAnchor(ar.anchor);

                            if (getLimb != null)
                            {
                                getLimb.localPosition = ar.localPos;
                                getLimb.localRotation = ar.localRot;
                            }
                        }
                    }
                }
            }

            if(newCapture.arms > 0)
            {
                AnimationFrameReference.AnimationReference armsRef = AnimationFrameReference.Instance.GetAnimationReference((CitizenAnimationController.ArmsBoolSate)newCapture.arms, newCapture.pos.ToString());

                if (armsRef != null)
                {
                    foreach (AnimationFrameReference.AnimationAnchorRef ar in armsRef.anim)
                    {
                        Transform getLimb = outfitController.GetBodyAnchor(ar.anchor);

                        if (getLimb != null)
                        {
                            getLimb.localPosition = ar.localPos;
                            getLimb.localRotation = ar.localRot;
                        }
                    }

                    Game.Log("Loaded arms anim " + (CitizenAnimationController.ArmsBoolSate)newCapture.arms);
                }
                else Game.Log("Could not find arms anim " + (CitizenAnimationController.ArmsBoolSate)newCapture.arms);
            }
        }

        //Load hand items
        if (newCapture.lH != null && spawnedLeft == null)
        {
            foreach(KeyValuePair<string, InteractablePreset> pair in Toolbox.Instance.objectPresetDictionary)
            {
                if(pair.Value.prefab != null)
                {
                    if(pair.Value.prefab.name == newCapture.lH.i)
                    {
                        spawnedLeft = Toolbox.Instance.SpawnObject(pair.Value.prefab, human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.HandLeft));
                        spawnedLeft.name = pair.Value.prefab.name;
                        spawnedLeft.transform.localPosition = newCapture.lH.wP;
                        spawnedLeft.transform.localRotation = newCapture.lH.wR;

                        LODGroup lodG = spawnedLeft.GetComponentInChildren<LODGroup>(true);

                        if (lodG != null)
                        {
                            DestroyImmediate(lodG);
                        }

                        break;
                    }
                }
            }
        }

        if (newCapture.rH != null && spawnedRight == null)
        {
            foreach (KeyValuePair<string, InteractablePreset> pair in Toolbox.Instance.objectPresetDictionary)
            {
                if (pair.Value.prefab != null)
                {
                    if (pair.Value.prefab.name == newCapture.rH.i)
                    {
                        spawnedRight = Toolbox.Instance.SpawnObject(pair.Value.prefab, human.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.HandRight));
                        spawnedRight.name = pair.Value.prefab.name;
                        spawnedRight.transform.localPosition = newCapture.rH.wP;
                        spawnedRight.transform.localRotation = newCapture.rH.wR;

                        LODGroup lodG = spawnedRight.GetComponentInChildren<LODGroup>(true);

                        if (lodG != null)
                        {
                            DestroyImmediate(lodG);
                        }

                        break;
                    }
                }
            }
        }

        //Set active
        this.gameObject.SetActive(true);
    }
}
