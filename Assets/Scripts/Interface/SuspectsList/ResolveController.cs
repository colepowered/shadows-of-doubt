using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NaughtyAttributes;
using TMPro;
using UnityEngine.Video;
using UnityEditor;
using UnityEngine.UI;

public class ResolveController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public RectTransform pageRect;
    public WindowContentController wcc;
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI descriptionText;
    public GameObject inputFieldPrefab;
    public TextMeshProUGUI invalidText;
    public RectTransform lineBreak1;
    public ButtonController submitButton;
    public ButtonController changeLeadButton;
    public ButtonController closeCaseButton;
    public RectTransform lineBreak2;
    public LayoutGroup layout;
    
    [Header("State")]
    public bool isSetup = false;
    public bool isValid = false;
    public List<InputFieldController> spawnedInputFields = new List<InputFieldController>();

    private static ResolveController _instance;
    public static ResolveController Instance { get { return _instance; } }

    public void Setup(WindowContentController newContentController)
    {
        rect = this.gameObject.GetComponent<RectTransform>();

        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        wcc = newContentController;
        SetPageSize(new Vector2(740, 768));

        titleText.text = Strings.Get("ui.interface", "Resolve Case");
        descriptionText.text = Strings.Get("ui.interface", "resolvecase_description");

        if(CasePanelController.Instance.activeCase != null && CasePanelController.Instance.activeCase.caseType == Case.CaseType.mainStory)
        {
            ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

            if(ChapterController.Instance.currentPart >= 31 && ChapterController.Instance.currentPart < 58 && !intro.completed)
            {
                changeLeadButton.gameObject.SetActive(true);
                changeLeadButton.SetInteractable(true);
            }
            else
            {
                changeLeadButton.gameObject.SetActive(false);
                changeLeadButton.SetInteractable(false);
            }
        }
        else
        {
            Destroy(changeLeadButton.gameObject);
        }

        UpdateResolveFields();

        isSetup = true;
    }

    public void UpdateResolveFields()
    {
        CasePanelController.Instance.SetPickModeActive(false, null);

        while(spawnedInputFields.Count > 0)
        {
            Destroy(spawnedInputFields[0].gameObject);
            spawnedInputFields.RemoveAt(0);
        }

        if(CasePanelController.Instance.activeCase != null && (CasePanelController.Instance.activeCase.caseStatus == Case.CaseStatus.handInCollected || CasePanelController.Instance.activeCase.caseStatus == Case.CaseStatus.closable))
        {
            foreach (Case.ResolveQuestion rq in CasePanelController.Instance.activeCase.resolveQuestions)
            {
                if (rq.displayOnlyAtPhase && CasePanelController.Instance.activeCase.job != null)
                {
                    if (CasePanelController.Instance.activeCase.job.phase < rq.displayAtPhase)
                    {
                        continue;
                    }
                }

                GameObject newQ = Instantiate(inputFieldPrefab, pageRect);
                InputFieldController ifc = newQ.GetComponent<InputFieldController>();
                ifc.Setup(rq, CasePanelController.Instance.activeCase);
                spawnedInputFields.Add(ifc);
            }
        }

        //Make sure button is last
        invalidText.transform.SetAsLastSibling();
        lineBreak1.SetAsLastSibling();
        submitButton.rect.SetAsLastSibling();
        closeCaseButton.rect.SetAsLastSibling();
        lineBreak2.SetAsLastSibling();

        if (CasePanelController.Instance.activeCase != null)
        {
            CasePanelController.Instance.activeCase.ValidationCheck();
        }

        ValidationUpdate();

        //Make sure content is correct size
        SetPageSize(new Vector2(740, Mathf.Max(768, spawnedInputFields.Count * 152f + 500f)));
    }

    public void ValidationUpdate()
    {
        if(CasePanelController.Instance.activeCase == null)
        {
            isValid = false;
            wcc.window.CloseWindow(false);
            return;
        }

        isValid = CasePanelController.Instance.activeCase.handInValid;
        if(invalidText != null) invalidText.gameObject.SetActive(false);

        if (spawnedInputFields.Count <= 0) isValid = false;

        //Not all questions have been revealed yet...
        if(CasePanelController.Instance.activeCase.job != null)
        {
            if(CasePanelController.Instance.activeCase.resolveQuestions.Exists(item => item.displayOnlyAtPhase && CasePanelController.Instance.activeCase.job.phase < item.displayAtPhase))
            {
                invalidText.text = Strings.Get("ui.interface", "You cannot hand this case in yet as not all objectives have been revealed...");
                invalidText.gameObject.SetActive(true);
                isValid = false;
            }
        }

        int validCount = 0;

        if (!invalidText.gameObject.activeSelf)
        {
            foreach(InputFieldController ifc in spawnedInputFields)
            {
                if(!ifc.question.isValid && !ifc.question.isOptional)
                {
                    invalidText.text = Strings.Get("ui.interface", "Please answer the question") + ": " + ifc.titleText.text;
                    invalidText.gameObject.SetActive(true);
                    isValid = false;
                }

                if(ifc.question.isValid)
                {
                    validCount++;
                }
            }
        }

        submitButton.SetInteractable(isValid);
        CasePanelController.Instance.activeCase.handInValid = isValid;

        ResolveOptionsController resController = wcc.window.GetComponentInChildren<ResolveOptionsController>(true);

        if (resController != null)
        {
            resController.submitButton.SetInteractable(isValid);
        }

        submitButton.UpdateButtonText();
        submitButton.text.text += " (" + validCount + "/" + spawnedInputFields.Count+")";
    }

    public void SetPageSize(Vector2 newSize)
    {
        Game.Log("Set page size: " + newSize);
        rect.sizeDelta = newSize;
        wcc.normalSize = rect.sizeDelta;
    }

    private void OnDestroy()
    {
        CasePanelController.Instance.SetPickModeActive(false, null);
    }

    public void SubmitButton()
    {
        if (CasePanelController.Instance.activeCase != null)
        {
            Interactable closestHandIn = CasePanelController.Instance.activeCase.GetClosestHandIn();

            if (closestHandIn != null)
            {
                MapController.Instance.PlotPlayerRoute(closestHandIn.node, true);
            }
        }
    }

    public void ChangeLeadButton()
    {
        if(CasePanelController.Instance.activeCase != null && CasePanelController.Instance.activeCase.caseType == Case.CaseType.mainStory)
        {
            if (ChapterController.Instance != null && ChapterController.Instance.chapterScript != null)
            {
                if (ChapterController.Instance.currentPart >= 31 && ChapterController.Instance.currentPart < 58)
                {
                    //ChapterController.Instance.chapterScript.ClearAllObjectives();
                    ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                    if(intro != null)
                    {
                        intro.ExecuteChangeLeadsManual();
                        //ChapterController.Instance.LoadPart("DisplayLeads");
                        wcc.window.CloseWindow(false);
                    }
                }
            }
        }
    }

    public void CloseCaseButton()
    {
        if(CasePanelController.Instance.activeCase != null)
        {
            if (CasePanelController.Instance.activeCase.caseType == Case.CaseType.mainStory && !Game.Instance.sandboxMode)
            {
                PopupMessageController.Instance.PopupMessage("CloseCaseStory", true, false); //Can't close story case
            }
            else
            {
                PopupMessageController.Instance.PopupMessage("CloseCase", true, true, RButton: "Confirm");
                PopupMessageController.Instance.OnLeftButton += CancelCloseCase;
                PopupMessageController.Instance.OnRightButton += ConfirmCloseCurrentCase;
            }
        }
    }

    public void CancelCloseCase()
    {
        PopupMessageController.Instance.OnLeftButton -= CancelCloseCase;
        PopupMessageController.Instance.OnRightButton -= ConfirmCloseCurrentCase;
    }

    public void ConfirmCloseCurrentCase()
    {
        PopupMessageController.Instance.OnLeftButton -= CancelCloseCase;
        PopupMessageController.Instance.OnRightButton -= ConfirmCloseCurrentCase;

        if (CasePanelController.Instance.activeCase == null) return;
        Game.Log("Player: Close current case: " + CasePanelController.Instance.activeCase.name);

        if(CasePanelController.Instance.activeCase.job != null)
        {
            CasePanelController.Instance.activeCase.job.End();
        }

        wcc.window.CloseWindow(false); //Close the window
        CasePanelController.Instance.CloseCase(CasePanelController.Instance.activeCase);
    }
}
