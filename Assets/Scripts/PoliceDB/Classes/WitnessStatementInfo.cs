﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class WitnessStatementInfo
{
    public Citizen citizen;
    //public DBWitnessStatement statementClass;

	public enum StatementType {Alibi, knowVictim};
	public StatementType statementType;

	//Constructor- use this to create info, then re-run the regular DBDetial constructor.
	public WitnessStatementInfo(Citizen newCit, StatementType newStatementType/*, /*DBWitnessStatement newStatement*/)
    {
        citizen = newCit;
        statementType = newStatementType;
        //statementClass = newStatement;
        //CompileStatement();
    }

    //public void CompileStatement()
    //{
    //    //parse related events
    //    Dictionary<string, string> refDict = new Dictionary<string, string>();
    //    Dictionary<string, EvidenceDataSource> dataSources = new Dictionary<string, EvidenceDataSource>();

    //    //Add victim
    //    AddOrUpdate("victim", statementClass.caseRef.murdered.citizenName, statementClass.caseRef.murdered.evidenceEntry, EvidenceDataSource.VagueLink.none, refDict, dataSources);

    //    //Add time of death
    //    AddOrUpdate("time_of_death", SessionData.Instance.GetTimeAndDateString(statementClass.caseRef.murdered.timeOfDeath), null, EvidenceDataSource.VagueLink.none, refDict, dataSources);

    //    //Work
    //    AddOrUpdate("event_work", StringTables.Get("list.statement.generic", "work"), citizen.employer.address.evidenceEntry, EvidenceDataSource.VagueLink.none, refDict, dataSources);

    //    //Home
    //    AddOrUpdate("event_home", StringTables.Get("list.statement.generic", "home"), citizen.home.evidenceEntry, EvidenceDataSource.VagueLink.none, refDict, dataSources);

    //    StringBuilder sb = new StringBuilder();

    //    //Alibi: Where was I at the time?
    //    if (statementType == StatementType.Alibi)
    //    {
    //        Game.Log("Generate alibi...");

    //        //Start with police question
    //        sb.Append(InfoParse(StringTables.Get("list.statement.questions", "whereabouts"), refDict, dataSources));
    //        sb.Append("\n");

    //        //Work day?
    //        bool workDay = false;

    //        if (citizen.job != null)
    //        {
    //            workDay = RoutineCreator.Instance.IsWorkDay(statementClass.caseRef.murdered.timeOfDeath.dayInt, citizen);
    //        }

    //        if (workDay)
    //        {
    //            sb.Append(InfoParse(StringTables.Get("voice.normal.statement.alibi", "workday_" + Random.Range(0, 2).ToString()) + " ", refDict, dataSources));
    //        }

    //        DBTime dbTime = requiredEvidence as DBTime;
    //        GameTime gameTime = dbTime.gameTime;

    //        //Get events surrounding time
    //        GetMemoryEvents(gameTime);

    //        //List of sentence prefix/suffix words (only to be used once each)
    //        Dictionary<string, List<string>> prefixes = new Dictionary<string, List<string>>();
    //        prefixes.Add("low", new List<string>());
    //        prefixes.Add("med", new List<string>());
    //        prefixes.Add("high", new List<string>());

    //        Dictionary<string, List<string>> suffixes = new Dictionary<string, List<string>>();
    //        suffixes.Add("low", new List<string>());
    //        suffixes.Add("med", new List<string>());
    //        suffixes.Add("high", new List<string>());

    //        prefixes["low"].Add(StringTables.Get("voice.normal.statement.alibi", "pre_generic"));
    //        prefixes["med"].Add(StringTables.Get("voice.normal.statement.alibi", "pre_generic"));
    //        prefixes["high"].Add(StringTables.Get("voice.normal.statement.alibi", "pre_generic"));

    //        suffixes["low"].Add(StringTables.Get("voice.normal.statement.alibi", "time_generic"));
    //        suffixes["med"].Add(StringTables.Get("voice.normal.statement.alibi", "time_generic"));
    //        suffixes["high"].Add(StringTables.Get("voice.normal.statement.alibi", "time_generic"));

    //        for (int i = 0; i < 1; i++)
    //        {
    //            prefixes["low"].Add(StringTables.Get("voice.normal.statement.alibi", "pre_low_" + i.ToString()));
    //            prefixes["med"].Add(StringTables.Get("voice.normal.statement.alibi", "pre_med_" + i.ToString()));
    //            prefixes["high"].Add(StringTables.Get("voice.normal.statement.alibi", "pre_high_" + i.ToString()));

    //            suffixes["low"].Add(StringTables.Get("voice.normal.statement.alibi", "time_low_" + i.ToString()));
    //            suffixes["med"].Add(StringTables.Get("voice.normal.statement.alibi", "time_med_" + i.ToString()));
    //            suffixes["high"].Add(StringTables.Get("voice.normal.statement.alibi", "time_high_" + i.ToString()));
    //        }

    //        bool firstEntry = true;

    //        foreach (MemoryEvent me in durationMemories)
    //        {
    //            //Call upon event memory
    //            me.CallUponMemory();

    //            StringBuilder sub = new StringBuilder();

    //            //If not the first entry, add a space
    //            if (!firstEntry)
    //            {
    //                //Add space after sentence end
    //                sub.Append(" ");

    //                //If not self leaving, add a 'Then...'
    //                if (me.encounterType != MemoryEvent.MemoryType.selfLeaving)
    //                {
    //                    sub.Append(StringTables.Get("voice.normal.statement.alibi", "then_" + Random.Range(0, 3).ToString()) + " ");
    //                }
    //            }
    //            else firstEntry = false;

    //            //Pick a random prefix
    //            int pick = Random.Range(0, prefixes[me.recallAccuracy.ToString()].Count);

    //            //Pre ("I think I...")
    //            sub.Append(prefixes[me.recallAccuracy.ToString()][pick]);

    //            //Remove entry
    //            if (pick > 0) prefixes[me.recallAccuracy.ToString()].RemoveAt(pick);

    //            //motive ("left for work...")
    //            if (me.encounterType != MemoryEvent.MemoryType.selfLeaving)
    //            {
    //                sub.Append(" " + StringTables.Get("voice.normal.statement.alibi", me.routine.reason.ToString()));
    //            }
    //            //Else use left...
    //            else
    //            {
    //                sub.Append(" " + StringTables.Get("voice.normal.statement.alibi", "left_" + Random.Range(0, 2).ToString()));
    //            }

    //            //Pick a random prefix
    //            pick = Random.Range(0, suffixes[me.recallAccuracy.ToString()].Count);

    //            //time ("at about...")
    //            sub.Append(" " + suffixes[me.recallAccuracy.ToString()][pick]);

    //            //Remove entry
    //            if (pick > 0) suffixes[me.recallAccuracy.ToString()].RemoveAt(pick);

    //            //time ("8")
    //            //if the day is the same, only refer to the time
    //            if (me.calledUponTime.day == statementClass.caseRef.murdered.timeOfDeath.day)
    //            {
    //                sub.Append(" " + me.calledUponTime.GetTimeStringHoursMinutes());
    //            }
    //            //Else include the day
    //            else
    //            {
    //                sub.Append(" " + me.calledUponTime.GetTimeStringIncludingDay());
    //            }

    //            //End setence
    //            sub.Append(".");

    //            //Add routine elements
    //            if (me.routine != null)
    //            {
    //                //Routine location
    //                AddOrUpdate("routine_location", me.routine.destination.name, me.routine.destination.evidenceEntry, EvidenceDataSource.VagueLink.none, refDict, dataSources);
    //            }

    //            sb.Append(InfoParse(sub.ToString(), refDict, dataSources));
    //        }

    //        //Confirmation statement
    //        sb.Append("\n\n");
    //        sb.Append(InfoParse(StringTables.Get("list.statement.questions", "confirmation"), refDict, dataSources));
    //        sb.Append("\n");

    //        //I didn't see anybody else...
    //        if (listOfSeenCitizens.Count <= 0)
    //        {
    //            //Send through parsing
    //            sb.Append(InfoParse(" " + StringTables.Get("voice.normal.statement.alibi", "confirm_nobody_" + Random.Range(0, 2).ToString()), refDict, dataSources));
    //        }
    //        else
    //        {
    //            List<Acquaintance> knownPeople = new List<Acquaintance>();

    //            foreach (Acquaintance aq in citizen.acquaintances)
    //            {
    //                //I know the other person
    //                if (listOfSeenCitizens.Contains(aq.with))
    //                {
    //                    knownPeople.Add(aq);

    //                    //Arrange by best known
    //                    aq.customSort = aq.known;
    //                }
    //            }

    //            //I saw others but I don't know them...
    //            if (knownPeople.Count <= 0)
    //            {
    //                sb.Append(InfoParse(" " + StringTables.Get("voice.normal.statement.alibi", "confirm_noknown_" + Random.Range(0, 2).ToString()), refDict, dataSources));
    //            }
    //            else
    //            {
    //                //Sort by best known first
    //                knownPeople.Sort(Acquaintance.customComparison);
    //                knownPeople.Reverse();

    //                bool firstSentence = true;

    //                //List top 3
    //                for (int i = 0; i < Mathf.Min(knownPeople.Count, 3); i++)
    //                {
    //                    if (!firstSentence) sb.Append(" ");
    //                    else firstSentence = false;

    //                    AddOrUpdate("confirm_person", knownPeople[i].with.citizenName, knownPeople[i].with.evidenceEntry, EvidenceDataSource.VagueLink.none, refDict, dataSources);

    //                    sb.Append(InfoParse(" " + StringTables.Get("voice.normal.statement.alibi", "confirm_people_" + Random.Range(0, 2).ToString()), refDict, dataSources));
    //                }
    //            }
    //        }
    //    }

    //    //knowWitness: Did I know the witness?
    //    if (statementType == StatementType.knowVictim)
    //    {
    //        Game.Log("Generate knowVictim...");

    //        //Start with police question
    //        sb.Append(InfoParse(StringTables.Get("list.statement.questions", "knowvictim"), refDict, dataSources));
    //        sb.Append("\n");

    //        bool known = false;

    //        foreach (Acquaintance aq in citizen.acquaintances)
    //        {
    //            //Known
    //            if (aq.with == statementClass.caseRef.murdered)
    //            {
    //                known = true;
    //                sb.Append(InfoParse(StringTables.Get("voice.normal.statement.knowvictim", aq.connection.ToString()), refDict, dataSources));
    //                break;
    //            }
    //        }

    //        if (!known)
    //        {
    //            sb.Append(InfoParse(StringTables.Get("voice.normal.statement.knowvictim", "unknown_" + Random.Range(0, 2).ToString()), refDict, dataSources));
    //            known = false;
    //        }

    //        //List of sentence prefix/suffix words (only to be used once each)
    //        Dictionary<string, List<string>> prefixes = new Dictionary<string, List<string>>();
    //        prefixes.Add("low", new List<string>());
    //        prefixes.Add("med", new List<string>());
    //        prefixes.Add("high", new List<string>());

    //        Dictionary<string, List<string>> suffixes = new Dictionary<string, List<string>>();
    //        suffixes.Add("low", new List<string>());
    //        suffixes.Add("med", new List<string>());
    //        suffixes.Add("high", new List<string>());

    //        prefixes["low"].Add(StringTables.Get("voice.normal.statement.knowvictim", "pre_generic"));
    //        prefixes["med"].Add(StringTables.Get("voice.normal.statement.knowvictim", "pre_generic"));
    //        prefixes["high"].Add(StringTables.Get("voice.normal.statement.knowvictim", "pre_generic"));

    //        suffixes["low"].Add(StringTables.Get("voice.normal.statement.knowvictim", "time_generic"));
    //        suffixes["med"].Add(StringTables.Get("voice.normal.statement.knowvictim", "time_generic"));
    //        suffixes["high"].Add(StringTables.Get("voice.normal.statement.knowvictim", "time_generic"));

    //        for (int i = 0; i < 1; i++)
    //        {
    //            prefixes["low"].Add(StringTables.Get("voice.normal.statement.knowvictim", "pre_low_" + i.ToString()));
    //            prefixes["med"].Add(StringTables.Get("voice.normal.statement.knowvictim", "pre_med_" + i.ToString()));
    //            prefixes["high"].Add(StringTables.Get("voice.normal.statement.knowvictim", "pre_high_" + i.ToString()));

    //            suffixes["low"].Add(StringTables.Get("voice.normal.statement.knowvictim", "time_low_" + i.ToString()));
    //            suffixes["med"].Add(StringTables.Get("voice.normal.statement.knowvictim", "time_med_" + i.ToString()));
    //            suffixes["high"].Add(StringTables.Get("voice.normal.statement.knowvictim", "time_high_" + i.ToString()));
    //        }

    //        bool firstEntry = true;

    //        //Did I see anyone matching this description?
    //        foreach (MemoryEvent me in citizen.memoryEvents)
    //        {
    //            //Isn't self related
    //            if (!me.isSelfRelated)
    //            {
    //                //Witnessed victim
    //                if (me.witnessed == statementClass.caseRef.murdered)
    //                {
    //                    //Call upon event memory
    //                    me.CallUponMemory();

    //                    StringBuilder sub = new StringBuilder();

    //                    //If not the first entry, add a space
    //                    if (!firstEntry)
    //                    {
    //                        //Add space after sentence end
    //                        sub.Append(" ");

    //                        sub.Append(StringTables.Get("voice.normal.statement.knowvictim", "then_" + Random.Range(0, 3).ToString()) + " ");
    //                    }
    //                    else
    //                    {
    //                        //If not known, add a 'but'
    //                        if (!known)
    //                        {
    //                            sub.Append(StringTables.Get("voice.normal.statement.knowvictim", "but_" + Random.Range(0, 2).ToString()) + " ");
    //                        }

    //                        firstEntry = false;
    //                    }

    //                    //Pick a random prefix
    //                    int pick = Random.Range(0, prefixes[me.recallAccuracy.ToString()].Count);

    //                    //Pre ("I think I...")
    //                    sub.Append(prefixes[me.recallAccuracy.ToString()][pick]);

    //                    //Remove entry
    //                    if (pick > 0) prefixes[me.recallAccuracy.ToString()].RemoveAt(pick);

    //                    //sighting
    //                    sub.Append(" " + StringTables.Get("voice.normal.statement.knowvictim", me.encounterType.ToString() + "_" + Random.Range(0, 2).ToString()));

    //                    //Pick a random prefix
    //                    pick = Random.Range(0, suffixes[me.recallAccuracy.ToString()].Count);

    //                    //time ("at about...")
    //                    sub.Append(" " + suffixes[me.recallAccuracy.ToString()][pick]);

    //                    //Remove entry
    //                    if (pick > 0) suffixes[me.recallAccuracy.ToString()].RemoveAt(pick);

    //                    //time ("8")
    //                    //if the day is the same, only refer to the time
    //                    if (me.calledUponTime.day == statementClass.caseRef.murdered.timeOfDeath.day)
    //                    {
    //                        sub.Append(" " + me.calledUponTime.GetTimeStringHoursMinutes());
    //                    }
    //                    //Else include the day
    //                    else
    //                    {
    //                        sub.Append(" " + me.calledUponTime.GetTimeStringIncludingDay());
    //                    }

    //                    //While I was
    //                    if (me.wasRoutineCompleted)
    //                    {
    //                        sub.Append(" " + StringTables.Get("voice.normal.statement.knowvictim", me.routine.reason.ToString()));
    //                    }
    //                    else
    //                    {
    //                        sub.Append(" " + StringTables.Get("voice.normal.statement.knowvictim", "active_" + me.routine.reason.ToString()));
    //                    }

    //                    //End setence
    //                    sub.Append(".");

    //                    //Add elements
    //                    if (me.memoryLocation != null)
    //                    {
    //                        //event location
    //                        AddOrUpdate("event_location", me.memoryLocation.roomAddress, me.memoryLocation.evidenceEntry, EvidenceDataSource.VagueLink.none, refDict, dataSources);
    //                    }

    //                    sb.Append(InfoParse(sub.ToString(), refDict, dataSources));
    //                }
    //            }
    //        }

    //        //Didn't see anyone
    //        if (firstEntry)
    //        {
    //            if (known)
    //            {
    //                sb.Append(InfoParse(StringTables.Get("voice.normal.statement.knowvictim", "nosighting_know_" + Random.Range(0, 2).ToString()) + " ", refDict, dataSources));
    //            }
    //            else
    //            {
    //                sb.Append(InfoParse(StringTables.Get("voice.normal.statement.knowvictim", "nosighting_unknown_" + Random.Range(0, 2).ToString()) + " ", refDict, dataSources));
    //            }
    //        }
    //    }

    //    //Work around for line breaks
    //    sb.Replace("\\n", "\n");

    //    //Compile to string
    //    statementText = sb.ToString();

    //    Game.Log("Statement text length is " + statementText.Length);
    //}

    //public void AddOrUpdate(string key, string val, DBItem link, EvidenceDataSource.VagueLink vagueLink, Dictionary<string, string> refDict, Dictionary<string, EvidenceDataSource> dataSources)
    //{
    //    if (key == null || key.Length == 0) return;
    //    else if (val == null || val.Length == 0) return;

    //    //Add to reference dict (for displaying the link text)
    //    if (refDict.ContainsKey(key))
    //    {
    //        refDict[key] = val;
    //    }
    //    else
    //    {
    //        refDict.Add(key, val);
    //    }

    //    if (link != null)
    //    {
    //        //Add to datasource dictionary
    //        if (dataSources.ContainsKey(key))
    //        {
    //            dataSources[key] = new EvidenceDataSource(link, vagueLink, false);
    //        }
    //        else
    //        {
    //            dataSources.Add(key, new EvidenceDataSource(link, vagueLink, false));
    //        }
    //    }

    //    //Vague links: Don't link to specific evidence, but nevertheless is linked to a large number of articles
    //    else if (vagueLink != EvidenceDataSource.VagueLink.none)
    //    {
    //        //Add to datasource dictionary
    //        if (dataSources.ContainsKey(key))
    //        {
    //            dataSources[key] = new EvidenceDataSource(null, vagueLink, false);
    //        }
    //        else
    //        {
    //            dataSources.Add(key, new EvidenceDataSource(null, vagueLink, false));
    //        }
    //    }
    //}

    //private string InfoParse(string newParse, Dictionary<string, string> refDict, Dictionary<string, EvidenceDataSource> dataSources)
    //{
    //    StringBuilder sb = new StringBuilder();

    //    //Parse tags to input content from dictionary
    //    string[] parsedArray = newParse.Split('|');

    //    List<string> parsed = new List<string>();
    //    parsed.AddRange(parsedArray);

    //    //Search to see if any of the parsed entries are dictionary references, if not ignore
    //    for (int n = 0; n < parsed.Count; ++n)
    //    {
    //        string item = parsed[n];

    //        //To save scanning for the proper text items, put a limit on the dictionary references
    //        if (item.Length > 0 && item.Length <= 30)
    //        {
    //            //Get the thing to be inserted, along with the reliability and incrimination...
    //            //string[] subParsed = StringTables.CleanSplit(item, '#', true);
    //            string[] subParsed = item.Split('#');

    //            //Item is the first
    //            item = subParsed[0];

    //            bool discoveredLinkOverride = false;
    //            bool revealingLink = false;

    //            foreach (string character in subParsed)
    //            {
    //                //An 'd' means override the link discovery (true)
    //                if (character == "d") discoveredLinkOverride = true;

    //                //An 'r' means this link will be added to the destinations mystery evidence reveal
    //                if (character == "r") revealingLink = true;
    //            }

    //            if (refDict.ContainsKey(item))
    //            {
    //                //Record the item type needed to create a physical button and evidencelink class
    //                string itemType = item;

    //                //Get actual reference from dictionary
    //                item = refDict[item];

    //                //Find evidence link in passed dictionary
    //                if (dataSources.ContainsKey(itemType))
    //                {
    //                    //Get data source
    //                    EvidenceDataSource source = dataSources[itemType];

    //                    //Link to specific evidence entry
    //                    if (source.infoSource != null)
    //                    {
    //                        EvidenceLink newLink = null;

    //                        //Create evidence link class
    //                        newLink = EvidenceCreator.Instance.Link(statementClass as DBItem, source.infoSource, discoveredLinkOverride, revealingLink, EvidenceLink.LinkType.reference);

    //                        //Keep a reference to links in this document class
    //                        if (!statementClass.clickableEvidenceRef.Contains(newLink)) statementClass.clickableEvidenceRef.Add(newLink);

    //                        //Adjust link text to contain the reference
    //                        item = "<link=" + newLink.id.ToString() + ">" + item + "</link>";
    //                    }
    //                }
    //                else
    //                {
    //                    //Game.Log("Cannot find " + itemType + " in datasource");
    //                }
    //            }
    //            else
    //            {
    //                //Game.Log("Does not contain reference (" + item + ")");
    //            }
    //        }

    //        //Stringbuilder method
    //        sb.Append(item);
    //    }

    //    return sb.ToString();
    //}


}
