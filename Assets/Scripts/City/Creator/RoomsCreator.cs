﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This script contains a function that populates the map with buildings.
//Script pass 1
public class RoomsCreator : Creator
{
    //Chunks to load per frame
    public int loadChunk = 1;

	//Singleton pattern
	private static RoomsCreator _instance;
    public static RoomsCreator Instance { get { return _instance; } }

	void Awake ()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
	}

    public override void StartLoading()
    {
        Game.Log("CityGen: Generating interior layouts...");
        StartCoroutine("Load");
	}

	IEnumerator Load()
	{
        int cursor = 0;

        while (cursor < CityData.Instance.addressDirectory.Count)
        {
            for (int i = 0; i < loadChunk; i++)
            {
                //Break loop if complete
                if (cursor >= CityData.Instance.addressDirectory.Count) break;

                GenerationController.Instance.GenerateAddressLayout(CityData.Instance.addressDirectory[cursor]);

                cursor++;
            }

            CityConstructor.Instance.loadingProgress = ((float)cursor / (float)CityData.Instance.addressDirectory.Count);
            yield return null;
        }

        GenerationController.Instance.ClearCache();

        //Complete!
        SetComplete();
    }
}
