using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class MaterialKeyController : MonoBehaviour
{
    [Header("Components")]
    public RectTransform rect;
    public WindowContentController wcc;

    public ButtonController placementButton;
    public InfoWindow colourWindow;
    public ColourPickerController colourPick;

    [Header("Colour Select")]
    public TextMeshProUGUI mainColourSelectText;
    public ButtonController mainColourButton;
    public RectTransform mainColourUnused;
    public ButtonController colour1Button;
    public RectTransform colour1Unused;
    public ButtonController colour2Button;
    public RectTransform colour2Unused;
    public ButtonController colour3Button;
    public RectTransform colour3Unused;

    [Header("Details Select")]
    public TextMeshProUGUI detailsColourSelectText;

    [Header("Grub Select")]
    public SliderPickerType sliderType = SliderPickerType.grub;
    public TextMeshProUGUI grubSelectText;
    public SliderController grubSlider;

    [Header("State")]
    public bool isSetup = false;
    public Toolbox.MaterialKey matKey = new Toolbox.MaterialKey();
    private int colourEdit;

    public enum SliderPickerType { grub, plants, artPortrait, artLandscape, artSquare, artPoster, artLitter, artWallGrimeTop, artWallGrimeBottom, artDynamicClue, artGraffiti };

    public delegate void ColourKeyUpdate();
    public event ColourKeyUpdate OnColourKeyUpdate;

    //public delegate void Placement();
    //public event Placement OnPlacement;

    public void Setup(WindowContentController newContentController)
    {
        rect = this.gameObject.GetComponent<RectTransform>();

        wcc = newContentController;

        SetPageSize(new Vector2(640, 350));

        placementButton.text.text = string.Empty;

        mainColourSelectText.text = Strings.Get("ui.interface", "Colours");
        detailsColourSelectText.text = Strings.Get("ui.interface", "Multiply");
        grubSelectText.text = Strings.Get("ui.interface", "Dirt");

        isSetup = true;
    }

    public void SetPageSize(Vector2 newSize)
    {
        rect.sizeDelta = newSize;
        wcc.normalSize = rect.sizeDelta;
    }

    public void UpdateButtonsBasedOnFurniture(FurniturePreset furn)
    {
        sliderType = SliderPickerType.grub;

        if(furn.isArt)
        {
            sliderType = (SliderPickerType)(2 + (int)furn.artOrientation);
        }
        else if(furn.isPlant)
        {
            sliderType = SliderPickerType.plants;
        }

        List<MeshRenderer> meshes = furn.prefab.GetComponentsInChildren<MeshRenderer>().ToList();

        bool updatedKey = false;

        foreach (MeshRenderer r in meshes)
        {
            if (!r.transform.CompareTag("NoMatColour"))
            {
                UpdateButtonsBasedOnMaterial(r.sharedMaterial, true, sliderType);
                updatedKey = true;
                break;
            }
        }

        if(!updatedKey)
        {
            UpdateButtonsBasedOnMaterial(null, true, sliderType);
        }

        //if (furn.inheritColouringFromDecor)
        //{
        //    List<MeshRenderer> meshes = furn.prefab.GetComponentsInChildren<MeshRenderer>().ToList();

        //    foreach(MeshRenderer r in meshes)
        //    {
        //        if(!r.transform.CompareTag("NoMatColour"))
        //        {
        //            UpdateButtonsBasedOnMaterial(r.sharedMaterial, true);
        //            break;
        //        }
        //    }
        //}
        //else
        //{
        //    //Only main colour exists...
        //    mainColourButton.SetInteractable(true);
        //    mainColourUnused.gameObject.SetActive(false);
        //    matKey.mainColour = Color.white;
        //    colour1Button.SetButtonBaseColour(Color.white);
        //}

        UpdatePlacementText();
    }

    public void UpdateButtonsBasedOnMaterial(Material mat, bool setColour, SliderPickerType sliderType = SliderPickerType.grub)
    {
        bool uses1 = false;
        bool uses2 = false;
        bool uses3 = false;

        Game.Log("Decor: Updating buttons based on " + mat + ", set colours: " + setColour);

        //Attempt to read the colour map to determin which colours are used...
        if (mat != null && mat.HasProperty("_ColourMap"))
        {
            Texture2D colourMap = mat.GetTexture("_ColourMap") as Texture2D;

            if (colourMap != null)
            {
                if (colourMap.isReadable)
                {
                    for (int i = 0; i < colourMap.width; i++)
                    {
                        for (int l = 0; l < colourMap.height; l++)
                        {
                            Color TempColour = colourMap.GetPixel(i, l);

                            if (TempColour.r > 0.01f)
                            {
                                uses1 = true;
                            }

                            if (TempColour.g > 0.01f)
                            {
                                uses2 = true;
                            }

                            if (TempColour.b > 0.01f)
                            {
                                uses3 = true;
                            }

                            if (uses1 && uses2 && uses3) break;
                        }

                        if (uses1 && uses2 && uses3) break;
                    }

                    Game.Log("Decor: Colour map on material " + mat.name + " uses colours " + uses1 + " " + uses2 + " " + uses3);
                }
                else
                {
                    uses1 = true;
                    uses2 = true;
                    uses3 = true;
                    Game.Log("Decor: IMPORTANT/TODO: Colour map on material " + mat.name + " is set to unreadable. By default, enabling all 3 editable colours. To make this work as intended, set the material to readable!");
                }
            }
            else
            {
                Game.Log("Decor: Colour map on material " + mat.name + " is missing...");
            }
        }
        else
        {
            Game.Log("Decor: Material " + mat + " does not use the colour shader...");
        }

        //Main
        mainColourButton.SetButtonBaseColour(Color.white);
        mainColourButton.SetInteractable(true);
        mainColourUnused.gameObject.SetActive(false);

        if (setColour && mat != null)
        {
            matKey.mainColour = mat.GetColor("_BaseColor");
            mainColourButton.SetButtonBaseColour(matKey.mainColour);
            Game.Log("Decor: Set Button main Colour: " + matKey.mainColour.ToString());
        }

        if (uses1)
        {
            colour1Button.SetInteractable(true);
            colour1Unused.gameObject.SetActive(false);

            if (setColour)
            {
                matKey.colour1 = mat.GetColor("_Color1");
                colour1Button.SetButtonBaseColour(matKey.colour1);
                Game.Log("Decor: Set Button 1 Colour: " + matKey.colour1.ToString());
            }
        }
        else
        {
            matKey.colour1 = Color.white;
            colour1Button.SetButtonBaseColour(Color.white);
            colour1Button.SetInteractable(false);
            colour1Unused.gameObject.SetActive(true);
        }

        if (uses2)
        {
            colour2Button.SetInteractable(true);
            colour2Unused.gameObject.SetActive(false);

            if (setColour)
            {
                matKey.colour2 = mat.GetColor("_Color2");
                colour2Button.SetButtonBaseColour(matKey.colour2);
                Game.Log("Decor: Set Button 2 Colour: " + matKey.colour2.ToString());
            }
        }
        else
        {
            matKey.colour2 = Color.white;
            colour2Button.SetButtonBaseColour(Color.white);
            colour2Button.SetInteractable(false);
            colour2Unused.gameObject.SetActive(true);
        }

        if (uses3)
        {
            colour3Button.SetInteractable(true);
            colour3Unused.gameObject.SetActive(false);

            if (setColour)
            {
                matKey.colour3 = mat.GetColor("_Color3");
                colour3Button.SetButtonBaseColour(matKey.colour3);
                Game.Log("Decor: Set Button 3 Colour: " + matKey.colour3.ToString());
            }
        }
        else
        {
            matKey.colour3 = Color.white;
            colour3Button.SetButtonBaseColour(Color.white);
            colour3Button.SetInteractable(false);
            colour3Unused.gameObject.SetActive(true);
        }

        if(sliderType == SliderPickerType.grub)
        {
            grubSelectText.text = Strings.Get("ui.interface", "Dirt");

            if (mat != null && mat.HasProperty("_GrubAmount"))
            {
                if (setColour)
                {
                    grubSlider.slider.SetValueWithoutNotify(Mathf.RoundToInt(mat.GetFloat("_GrubAmount") * 10));
                }
            }
            else
            {
                grubSlider.slider.SetValueWithoutNotify(0);
            }
        }
        else if(sliderType == SliderPickerType.plants)
        {
            grubSelectText.text = Strings.Get("ui.interface", "Plants");

            //Get range

        }
        else
        {
            grubSelectText.text = Strings.Get("ui.interface", "Art");

            //Get range
            ArtPreset.ArtOrientation orientation = (ArtPreset.ArtOrientation)((int)sliderType - 2);
            List<ArtPreset> compatible = Toolbox.Instance.allArt.FindAll(item => item.orientationCompatibility.Contains(orientation) && !item.disable);

            grubSlider.slider.minValue = 0;
            grubSlider.slider.maxValue = compatible.Count - 1;
        }

        if (setColour)
        {
            ChangeColourKey();
        }

        UpdatePlacementText();
    }

    public void SetButtonsToKey(Toolbox.MaterialKey key)
    {
        if(key.mainColour != Color.clear)
        {
            matKey.mainColour = key.mainColour;
            mainColourButton.SetButtonBaseColour(key.mainColour);
            Game.Log("Decor: Set Button main Colour: " + key.mainColour.ToString());
        }
        else
        {
            mainColourButton.SetButtonBaseColour(Color.white);
        }

        if(key.colour1 != Color.clear)
        {
            matKey.colour1 = key.colour1;
            colour1Button.SetButtonBaseColour(matKey.colour1);
            Game.Log("Decor: Set Button 3 Colour: " + key.colour1.ToString());
        }
        else
        {
            colour1Button.SetButtonBaseColour(Color.white);
        }

        if (key.colour2 != Color.clear)
        {
            matKey.colour2 = key.colour2;
            colour2Button.SetButtonBaseColour(matKey.colour2);
            Game.Log("Decor: Set Button 3 Colour: " + key.colour2.ToString());
        }
        else
        {
            colour2Button.SetButtonBaseColour(Color.white);
        }

        if (key.colour3 != Color.clear)
        {
            matKey.colour3 = key.colour3;
            colour3Button.SetButtonBaseColour(matKey.colour3);
            Game.Log("Decor: Set Button 3 Colour: " + key.colour3.ToString());
        }
        else
        {
            colour3Button.SetButtonBaseColour(Color.white);
        }

        if (sliderType == SliderPickerType.grub)
        {
            matKey.grubiness = key.grubiness;
            grubSlider.slider.SetValueWithoutNotify(Mathf.RoundToInt(matKey.grubiness * 10));
        }
        else matKey.grubiness = 0;

        ChangeColourKey();
    }

    public void ColourSelectButton(int val)
    {
        if (colourWindow == null)
        {
            colourEdit = val;
            colourWindow = InterfaceController.Instance.SpawnWindow(null, Evidence.DataKey.name, null, "ColourPicker");

            colourPick = colourWindow.GetComponentInChildren<ColourPickerController>(true);
            colourPick.OnNewColour += OnNewColourSelect;
        }
    }

    public void OnNewColourSelect(Color newColour)
    {
        Game.Log("Decor: New colour select: " + newColour.ToString());

        if (colourEdit == 0)
        {
            matKey.mainColour = newColour;
            mainColourButton.SetButtonBaseColour(newColour);
            Game.Log("Decor: Set Button main Colour: " + newColour.ToString());
        }
        else if (colourEdit == 1)
        {
            matKey.colour1 = newColour;
            colour1Button.SetButtonBaseColour(newColour);
            Game.Log("Decor: Set Button 1 Colour: " + newColour.ToString());
        }
        else if (colourEdit == 2)
        {
            matKey.colour2 = newColour;
            colour2Button.SetButtonBaseColour(newColour);
            Game.Log("Decor: Set Button 2 Colour: " + newColour.ToString());
        }
        else if (colourEdit == 3)
        {
            matKey.colour3 = newColour;
            colour3Button.SetButtonBaseColour(newColour);
            Game.Log("Decor: Set Button 3 Colour: " + newColour.ToString());
        }
        //else if (colourEdit == 4)
        //{
        //    //detailColour = newColour;
        //    detailsButton.SetButtonBaseColour(newColour);
        //    Game.Log("Decor: Set Button Detail Colour: " + newColour.ToString());
        //}
        //else if (colourEdit == 5)
        //{
        //    //doorsColour = newColour;
        //    doorsButton.SetButtonBaseColour(newColour);
        //    Game.Log("Decor: Set Doors Colour: " + newColour.ToString());
        //}

        if (colourPick != null)
        {
            colourPick.OnNewColour -= OnNewColourSelect;
        }

        colourWindow.CloseWindow(false);
        ChangeColourKey();
    }

    public void OnGrubUpdate()
    {
        if(sliderType == SliderPickerType.grub)
        {
            matKey.grubiness = grubSlider.slider.value / 10f;
        }
        else if(sliderType == SliderPickerType.plants)
        {

        }
        else
        {

        }

        ChangeColourKey();
    }

    public void ChangeColourKey()
    {
        if ((int)sliderType >= 2)
        {
            ArtPreset.ArtOrientation orientation = (ArtPreset.ArtOrientation)((int)sliderType - 2);
            List<ArtPreset> compatible = Toolbox.Instance.allArt.FindAll(item => item.orientationCompatibility.Contains(orientation) && !item.disable);
            PlayerApartmentController.Instance.furnPlacement.art = compatible[Mathf.RoundToInt(grubSlider.slider.value)];
            Game.Log("Decor: Selected art " + PlayerApartmentController.Instance.furnPlacement.art);
        }
        else PlayerApartmentController.Instance.furnPlacement.art = null;

        //Update colour key for placement
        //furnishingController.placement.materialKey = matKey;
        PlayerApartmentController.Instance.furnPlacement.materialKey = matKey;
        PlayerApartmentController.Instance.UpdatePlacementColourKey();

        PlayerApartmentController.Instance.decoratingKey = matKey;
        PlayerApartmentController.Instance.UpdateDecorColourKey();

        //Fire event
        if (OnColourKeyUpdate != null)
        {
            OnColourKeyUpdate();
        }
    }

    public void PlacementButton()
    {
        //Execute furniture placement mode...
        //if(furnishingController != null)
        //{
        //    if (!PlayerApartmentController.Instance.furniturePlacementMode)
        //    {
        //        furnishingController.placement.materialKey = matKey;

        //        PlayerApartmentController.Instance.SetFurniturePlacementMode(true, furnishingController.placement, Player.Instance.currentRoom);
        //        SessionData.Instance.ResumeGame();
        //    }
        //}
        //else if(decorContoller != null)
        //{
        //    SessionData.Instance.ResumeGame();
        //}

        SessionData.Instance.ResumeGame();
    }

    public void UpdatePlacementText()
    {
        if(PlayerApartmentController.Instance.furniturePlacementMode)
        {
            placementButton.text.text = Strings.Get("ui.interface", "Preview") + " " + Strings.Get("evidence.names", PlayerApartmentController.Instance.furnPlacement.preset.name);
        }
        else if(PlayerApartmentController.Instance.decoratingMode)
        {
            placementButton.text.text = Strings.Get("ui.interface", "Preview") + " " + Strings.Get("evidence.names", PlayerApartmentController.Instance.decoratingMaterial.name);
        }
    }

    public void CancelButton()
    {
        wcc.window.CloseWindow(false);
        InteractionController.Instance.StartDecorEdit();
    }

    private void OnDestroy()
    {
        //Exit from furniture placement mode
        if(PlayerApartmentController.Instance.furniturePlacementMode)
        {
            Game.Log("Decor: Material key window is being closed, cancelling decor modes...");
            PlayerApartmentController.Instance.SetFurniturePlacementMode(false, null, null);
            InteractionController.Instance.StartDecorEdit(); //If this has been cancelled, start decor edit again
        }

        if (PlayerApartmentController.Instance.decoratingMode)
        {
            Game.Log("Decor: Material key window is being closed, cancelling decor modes...");
            PlayerApartmentController.Instance.SetDecoratingMode(false, null);
            InteractionController.Instance.StartDecorEdit(); //If this has been cancelled, start decor edit again
        }

        if (colourPick != null)
        {
            colourPick.OnNewColour -= OnNewColourSelect;
        }

        if(colourWindow != null)
        {
            colourWindow.CloseWindow(false);
        }
    }
}
