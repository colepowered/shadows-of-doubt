﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

//A controller given to every job in the city, even if no citizen is currently assigned to it.
//Script pass 1
public class Occupation : IComparable<Occupation>
{
	//Assign self id
	public int id = 0;
	[System.NonSerialized]
	public static int idAssign = 1; //ID 0 is reserved for unemployed

	//Preset
	public OccupationPreset preset;

	//Name of the job
	public string name = "worker";
	public Company employer;
	public bool isAgent = false;

	//Hierarchy
	public bool teamLeader = false;
	public Occupation boss;
	//public int rank;
    public float paygrade; //0-1 float to pass through pay curve
	public int teamID;
    public bool isOwner = false; //True if the highest position in the company

	//Job category
	public OccupationPreset.workCollar collar;
	public OccupationPreset.workType work;

    //Additional tags to describe position
    public List<OccupationPreset.workTags> tags = new List<OccupationPreset.workTags>();

    //Shift time
    public CompanyOpenHoursPreset.CompanyShift shift;

	//Job's work hour time
	public float workHours = 8f;
	public float startTimeDecimalHour = 9f;
	public float endTimeDecialHour = 17f;
    public bool lunchBreak = false; //Lunch break is allowed
    public float lunchBreakHoursAfterStart = 3.5f;

	[System.NonSerialized]
	public List<SessionData.WeekDay> workDaysList = new List<SessionData.WeekDay>();

	//Money!
	public float salary = 0f;
	public string salaryString = string.Empty;

	//Reference to the citizen with this job
	public Human employee = null;

	public void Setup()
	{
        id = idAssign;
        idAssign++;

        name = Strings.Get("jobs", preset.name);
		collar = preset.collar;
		work = preset.work;
        tags.AddRange(preset.tags);

		//calculate work hours
		if (employer != null)
		{
            startTimeDecimalHour = shift.decimalHours.x;
            endTimeDecialHour = shift.decimalHours.y;

            if (endTimeDecialHour >= startTimeDecimalHour) workHours = endTimeDecialHour - startTimeDecimalHour;
			else workHours = 24 + endTimeDecialHour - startTimeDecimalHour;

            workDaysList.Clear();
            if (shift.monday) workDaysList.Add(SessionData.WeekDay.monday);
            if (shift.tuesday) workDaysList.Add(SessionData.WeekDay.tuesday);
            if (shift.wednesday) workDaysList.Add(SessionData.WeekDay.wednesday);
            if (shift.thursday) workDaysList.Add(SessionData.WeekDay.thursday);
            if (shift.friday) workDaysList.Add(SessionData.WeekDay.friday);
            if (shift.saturday) workDaysList.Add(SessionData.WeekDay.saturday);
            if (shift.sunday) workDaysList.Add(SessionData.WeekDay.sunday);

            //Calculate lunch
            lunchBreak = preset.lunchBreakAllowed;

            if(lunchBreak)
            {
                lunchBreakHoursAfterStart = (Mathf.RoundToInt((workHours * 0.45f) * 2f) / 2f);
            }

            //Use salary curve to determin salary
            float salaryMultiplier = employer.preset.payGradeCurve.Evaluate(paygrade);

            //Small boost in pay for a team leader
            if (teamLeader) salaryMultiplier += Toolbox.Instance.GetPsuedoRandomNumber(salary * 0.1f, salary * 0.2f, id.ToString());

            //Fetch the accurate calculated salary
            salary = Mathf.Lerp(employer.minimumSalary, employer.topSalary, salaryMultiplier);

            //Round to 1dp
            salary = Mathf.RoundToInt(salary * 10f) / 10f;

			//Generate salary string
			salaryString = CityControls.Instance.cityCurrency + Mathf.RoundToInt(salary * 1000).ToString();
		}
	}

    public void Load(CitySaveData.OccupationCitySave data, Company newCompany)
    {
        id = data.id;
        employer = newCompany;

        Toolbox.Instance.LoadDataFromResources<OccupationPreset>(data.preset, out preset); //Design style chosen for the address

        name = data.name;
        collar = data.collar;
        work = data.work;
        tags = data.tags;;
        startTimeDecimalHour = data.startTime;
        endTimeDecialHour = data.endTime;

        if (endTimeDecialHour >= startTimeDecimalHour) workHours = endTimeDecialHour - startTimeDecimalHour;
        else workHours = 24 + endTimeDecialHour - startTimeDecimalHour;

        //Calculate lunch
        lunchBreak = preset.lunchBreakAllowed;

        if (lunchBreak)
        {
            lunchBreakHoursAfterStart = (Mathf.RoundToInt((workHours * 0.45f) * 2f) / 2f);
        }

        workDaysList = data.workDaysList;
        isOwner = data.isOwner;
        salary = data.salary;
        salaryString = data.salaryString;
        paygrade = data.paygrade;
        teamLeader = data.teamLeader;
        teamID = data.teamID;

        if(employer != null) shift = employer.shifts[data.shift];
    }

    //Am I at work at this time?
    public bool IsAtWork(float atTime)
    {
        if (employer == null) return false;

        float hour = 0f;
        int dayInt = 0;
        int dateOut = 0;
        int monthOut = 0;
        int yearOut = 0;

        SessionData.Instance.ParseTimeData(atTime, out hour, out dayInt, out dateOut, out monthOut, out yearOut);

        //Outside of work hours
        if (hour < startTimeDecimalHour - 0.25f) return false;
        else if (hour > endTimeDecialHour + 0.25f) return false;

        if(preset.lunchBreakAllowed)
        {
            if(hour >= (endTimeDecialHour - startTimeDecimalHour) * 0.5f)
            {
                if (hour <= (endTimeDecialHour - startTimeDecimalHour) * 0.5f + 1)
                {
                    return false;
                }
            }
        }

        if (dayInt == 0 && workDaysList.Contains(SessionData.WeekDay.monday))
        {
            return true;
        }
        else if (dayInt == 1 && workDaysList.Contains(SessionData.WeekDay.tuesday))
        {
            return true;
        }
        else if (dayInt == 2 && workDaysList.Contains(SessionData.WeekDay.wednesday))
        {
            return true;
        }
        else if (dayInt == 3 && workDaysList.Contains(SessionData.WeekDay.thursday))
        {
            return true;
        }
        else if (dayInt == 4 && workDaysList.Contains(SessionData.WeekDay.friday))
        {
            return true;
        }
        else if (dayInt == 5 && workDaysList.Contains(SessionData.WeekDay.saturday))
        {
            return true;
        }
        else if (dayInt == 6 && workDaysList.Contains(SessionData.WeekDay.sunday))
        {
            return true;
        }

        return false;
    }

    public CitySaveData.OccupationCitySave GenerateSaveData()
    {
        CitySaveData.OccupationCitySave output = new CitySaveData.OccupationCitySave();

        output.id = id;
        output.preset = preset.name;
        output.name = name;
        output.teamLeader = teamLeader;
        if(boss != null) output.boss = boss.id;
        output.paygrade = paygrade; //0-1 float to pass through pay curve
        output.teamID = teamID;
        output.collar = collar;
        output.isOwner = isOwner;
        output.work = work;
        output.tags = tags;

        if(employer != null)
        {
            output.shift = employer.shifts.FindIndex(item => item == shift);
        }

        output.startTime = startTimeDecimalHour;
        output.endTime = endTimeDecialHour;
        output.workDaysList = workDaysList;
        output.salary = salary;
        output.salaryString = salaryString;

        return output;
    }

    public string GetWorkingHoursString()
    {
        string workingHours = string.Empty;

        //Add hours
        workingHours += SessionData.Instance.DecimalToClockString(startTimeDecimalHour, false) + " - " + SessionData.Instance.DecimalToClockString(startTimeDecimalHour + workHours, false);

        //Add days
        //List consecutive work days
        Dictionary<int, int> consecutiveDays = new Dictionary<int, int>();
        int consecitiveRun = 1;

        for (int i = 0; i < workDaysList.Count; i++)
        {
            int day = (int)workDaysList[i];

            //Check to see if this follows any previous dictionary key
            if (consecutiveDays.ContainsKey(day - consecitiveRun))
            {
                //If it does, add this as the new value
                consecutiveDays[day - consecitiveRun] = day;
                consecitiveRun++;
            }
            else
            {
                consecutiveDays.Add(day, day);
                consecitiveRun = 1;
            }
        }

        foreach (KeyValuePair<int, int> pair in consecutiveDays)
        {
            //Just add day
            if (pair.Value == pair.Key)
            {
                workingHours += ", " + Strings.Get("ui.interface", ((SessionData.WeekDay)pair.Key).ToString());
            }
            else
            {
                workingHours += ", " + Strings.Get("ui.interface", ((SessionData.WeekDay)pair.Key).ToString()) + " - " + Strings.Get("ui.interface", ((SessionData.WeekDay)pair.Value).ToString());
            }
        }

        //Trim extra whitespace
        return workingHours.Trim();
    }

	// Default comparer(paygrade)
	public int CompareTo(Occupation paygrade)
	{
		return this.paygrade.CompareTo(paygrade.paygrade);
	}

	//Compare by salary
	public static Comparison<Occupation> SalaryComparison = delegate(Occupation object1, Occupation object2)
	{
		return object1.salary.CompareTo(object2.salary);
	};

    //Compare by priority
    public static Comparison<Occupation> FillPriorityComparison = delegate (Occupation object1, Occupation object2)
    {
        return object1.preset.jobFillPriority.CompareTo(object2.preset.jobFillPriority);
    };
}
