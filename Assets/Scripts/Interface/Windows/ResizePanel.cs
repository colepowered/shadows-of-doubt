﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//Controls resizing of a window
//Script pass 1
public class ResizePanel : MonoBehaviour, IPointerDownHandler, IEndDragHandler, IDragHandler, IPointerEnterHandler, IPointerExitHandler
{
	//References
    public InfoWindow controller;
    public bool resizingActive = false;

    private Vector2 currentPointerPosition;
    private Vector2 previousPointerPosition;

    //Pivot of this point
    public Vector2 pivot;
   
    public void OnPointerDown (PointerEventData data)
    {
        if (controller == null) controller = this.gameObject.GetComponentInParent<InfoWindow>();

        controller.rect.SetAsLastSibling();

        //Set the pivot point of the window so dragging works from the correct corner
        controller.SetPivot(pivot);
        RectTransformUtility.ScreenPointToLocalPointInRectangle (controller.rect, data.position, data.pressEventCamera, out previousPointerPosition);
    }

    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        InterfaceController.Instance.AddMouseOverElement(this);
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        if (!InterfaceController.Instance.StupidUnityChangeToTheWayOnPointerExitHandles(eventData, this.transform)) return;

        if (!resizingActive)
        {
            InterfaceController.Instance.RemoveMouseOverElement(this);
        }
    }

    void OnDisable()
    {
        resizingActive = false;
        InterfaceController.Instance.RemoveMouseOverElement(this);
    }

    public void OnEndDrag(PointerEventData data)
    {
        resizingActive = false;
        InterfaceController.Instance.RemoveMouseOverElement(this);
    }

    public void OnDrag (PointerEventData data)
    {
        if (controller == null) controller = this.gameObject.GetComponentInParent<InfoWindow>();

        if (controller.rect == null)
        {
            return;
        }

        resizingActive = true;

        InterfaceController.Instance.AddMouseOverElement(this);

        Vector2 sizeDelta = controller.rect.sizeDelta;

        RectTransformUtility.ScreenPointToLocalPointInRectangle (controller.rect, data.position, data.pressEventCamera, out currentPointerPosition);
        Vector2 resizeValue = currentPointerPosition - previousPointerPosition;

		sizeDelta += new Vector2 (resizeValue.x * (1 - (pivot.x *2)), resizeValue.y * (1 - (pivot.y *2)));
        sizeDelta = new Vector2 (
            Mathf.Clamp (sizeDelta.x, controller.preset.minSize.x, controller.preset.maxSize.x),
            Mathf.Clamp (sizeDelta.y, controller.preset.minSize.y, controller.preset.maxSize.y)
            );

        controller.rect.sizeDelta = sizeDelta;
        controller.OnResizeWindow();
        
        previousPointerPosition = currentPointerPosition;
    }

    void OnDestroy()
    {
        InterfaceController.Instance.RemoveMouseOverElement(this);
    }
}