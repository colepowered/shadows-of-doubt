﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This script contains a function that populates the map with buildings.
//Script pass 1
public class BlueprintsCreator : Creator
{
    //Chunks to load per frame
    public int loadChunk = 1;

	//Singleton pattern
	private static BlueprintsCreator _instance;
    public static BlueprintsCreator Instance { get { return _instance; } }

	void Awake ()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
	}

    public override void StartLoading()
    {
        //Create locks
        string evID = "Keypad";
        Evidence keypadLock = EvidenceCreator.Instance.CreateEvidence("Keypad", evID);
        keypadLock.Compile();

        Game.Log("CityGen: Generating interior blueprints...");
        StartCoroutine("Load");
	}

	IEnumerator Load()
	{
        int cursor = 0;

        while (cursor < CityData.Instance.buildingDirectory.Count)
        {
            for (int i = 0; i < loadChunk; i++)
            {
                //Break loop if complete
                if (cursor >= CityData.Instance.buildingDirectory.Count) break;

                CityData.Instance.buildingDirectory[cursor].LoadInterior();

                cursor++;
            }

            CityConstructor.Instance.loadingProgress = ((float)cursor / (float)CityData.Instance.buildingDirectory.Count);
            yield return null;
        }

        //Complete!
        SetComplete();
	}
}
