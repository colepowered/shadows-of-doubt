﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class CitizenAnimationController : MonoBehaviour
{
    public Human cit;
    public Animator mainAnimator;
    public GameObject spawnedUmbrella;
    public Transform umbrellaCanopy;

    //Transitions to the arms override layer
    public float armsLayerDesiredWeight = 0f;
    public float umbreallLayerDesiredWeight = 0f;
    public float oneShotUseReset = 0f;

    public enum ArmsBoolSate { none, armsResting, armsTyping, armsUse, armsLocking, armsCuffed, armsConsuming, armsOneShotUse, armsSmoking, armsSmokingPipe, armsReading, armsFleeing };
    public ArmsBoolSate armsBoolAnimationState = ArmsBoolSate.none;

    public enum IdleAnimationState { none, sitting, sweeping, warmingHands, telephone, washingHands, cleaningBar, bargingDoor, cookingChopping, cookingFrying, sitAgainstWall, leanAgainstWall, showering, rubbingEyes, cowering, checkPulse, brushingTeeth, pickUpFromFloor, danceTwist, danceWatusi, stackingObjects, stackingObjectsCrouching};
    public IdleAnimationState idleAnimationState = IdleAnimationState.none;

    //Left (false) or right (animation)
    public bool flipToRightAnimation = false;
    public bool paused = false;
    public bool umbrella = false;

    [Header("Human Components")]
    public Transform armsParent;
    public BoxCollider newBoxCollider;
    [System.NonSerialized]
    public Rigidbody upperTorsoRB;

    [System.Serializable]
    public class CitizenPhysics
    {
        public CitizenOutfitController.AnchorConfig anchorConfig;
        public Collider coll;
        public Rigidbody rb;
    }

    [System.Serializable]
    public class RagdollSnapshot
    {
        public CitizenOutfitController.AnchorConfig anchorConfig;
        public Vector3 localPos;
        public Quaternion localRot;
    }

    [Header("Ragdoll")]
    public Dictionary<CitizenOutfitController.CharacterAnchor, CitizenPhysics> physicsComponents = new Dictionary<CitizenOutfitController.CharacterAnchor, CitizenPhysics>();

    [System.NonSerialized]
    public List<Rigidbody> createdRBs = new List<Rigidbody>();
    [System.NonSerialized]
    public List<CharacterJoint> createdJoints = new List<CharacterJoint>();
    [System.NonSerialized]
    public List<Collider> createdColliders = new List<Collider>();
    [System.NonSerialized]
    public RagdollSFXController sfx;

    private CharacterJoint headJoint;

    private CharacterJoint upperTorsoJoint;
    private CharacterJoint midriffJoint;

    private CharacterJoint leftUpperArmJoint;
    private CharacterJoint leftLowerArmJoint;
    private CharacterJoint leftHandJoint;

    private CharacterJoint rightUpperArmJoint;
    private CharacterJoint rightLowerArmJoint;
    private CharacterJoint rightHandJoint;

    private CharacterJoint leftUpperLegJoint;
    private CharacterJoint leftLowerLegJoint;

    private CharacterJoint rightUpperLegJoint;
    private CharacterJoint rightLowerLegJoint;

    private CharacterJoint rightFootJoint;
    private CharacterJoint leftFootJoint;

    //Limbs ragdoll snapshot
    [System.NonSerialized]
    public List<RagdollSnapshot> ragdollSnapshot = new List<RagdollSnapshot>();

    [Header("Debug")]
    public float debugMainAnimatorSpeed = 0f;

    //Update the current animation state (called on becoming visible)
    public void ForceUpdateAnimationSate(bool onBecomeVisibile = false)
    {
        if (mainAnimator == null) return;

        FlipAnimationToRight(flipToRightAnimation);

        //Update the movement speed
        UpdateMovementSpeed();

        if (onBecomeVisibile)
        {
            mainAnimator.SetFloat("combatSkill", cit.combatSkill);

            SetIdleAnimationState(idleAnimationState); //Reset idle animation state
            SetArmsBoolState(armsBoolAnimationState);

            if (cit.ai != null)
            {
                SetCombatArmsOverride(cit.ai.combatMode);
            }

            SetInBed(cit.isInBed, onBecomeVisibile);
            SetInCombat(cit.ai.inCombat);
            SetDead(cit.isDead);
            SetUmbrella(umbrella);
        }

        if(cit.ai != null)
        {
            if(cit.ai.restrained)
            {
                mainAnimator.SetTrigger("restrain");
            }
        }
    }

    public void UpdateMovementSpeed()
    {
        //Update the movement speed
        if(mainAnimator != null)
        {
            mainAnimator.SetFloat("moveSpeed", cit.currentNormalizedSpeed);
            mainAnimator.SetFloat("walkAnimSpeed", Mathf.Clamp(cit.currentMovementSpeed, 2f, 3f));
            mainAnimator.SetFloat("drunk", cit.drunk);
            debugMainAnimatorSpeed = cit.currentNormalizedSpeed;
        }
    }

    public void SetArmsBoolState(ArmsBoolSate newState)
    {
        //Special case: Only consume if I have an item...
        if (newState == ArmsBoolSate.armsConsuming && (cit.ai.spawnedRightItem == null || (mainAnimator != null && !mainAnimator.GetBool("carryingItem"))))
        {
            newState = ArmsBoolSate.none;
        }

        //Don't change away from cuffed if restrained
        if(cit.ai.restrained && !cit.isDead)
        {
            newState = ArmsBoolSate.armsCuffed;
        }

        armsBoolAnimationState = newState;

        //cit.SelectedDebug("Set arms state: " + newState);

        if(armsBoolAnimationState == ArmsBoolSate.none)
        {
            armsLayerDesiredWeight = 0f; //Fade out arms override

            //Override arms state
            if(cit.ai != null)
            {
                if(cit.ai.inCombat && cit.ai.combatMode > 0)
                {
                    armsLayerDesiredWeight = 1f; //Override combat animation with appropriate arm animation
                }
                else if (cit.ai.usingCarryAnimation && !cit.ai.attackActive)
                {
                    armsLayerDesiredWeight = 1f; //Swap to carrying
                }
            }

            if(mainAnimator != null) mainAnimator.SetInteger("ArmsState", 0);
            //cit.SelectedDebug("Set animator arms weight: " + armsLayerDesiredWeight);
        }
        else
        {
            armsLayerDesiredWeight = 1f; //Fade in arms override
            if (mainAnimator != null) mainAnimator.SetInteger("ArmsState", (int)newState);
            //cit.SelectedDebug("Set animator arms weight: " + armsLayerDesiredWeight);

            //Manually reset one-shot use...
            if (armsBoolAnimationState == ArmsBoolSate.armsOneShotUse)
            {
                oneShotUseReset = 0.5f;
            }
        }

        //If not drawn then snap to weight
        if (!cit.visible)
        {
            if (mainAnimator != null) mainAnimator.SetLayerWeight(1, armsLayerDesiredWeight);
            //cit.SelectedDebug("Set current arms layer weight: " + armsLayerDesiredWeight + " (" + armsLayerDesiredWeight + ")");
        }
        else if (cit.ai != null)
        {
            cit.ai.SetUpdateEnabled(true); //Set enabled in order to allow animation layer lerping
        }
    }

    public void SetUmbrella(bool val)
    {
        if (cit.ai.inCombat || cit.ai.restrained && val) return;

        if(umbrella != val)
        {
            umbrella = val;

            if(umbrella)
            {
                umbreallLayerDesiredWeight = 1f;

                if(spawnedUmbrella == null)
                {
                    spawnedUmbrella = Toolbox.Instance.SpawnObject(CitizenControls.Instance.umbrella, cit.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.HandLeft));
                    umbrellaCanopy = spawnedUmbrella.transform.GetChild(0);
                    spawnedUmbrella.transform.localPosition = Vector3.zero;
                    umbrellaCanopy.localScale = new Vector3(0.15f, 0.15f, 3f);

                    //Add meshes
                    cit.AddMesh(spawnedUmbrella, true);
                }
            }
            else
            {
                umbreallLayerDesiredWeight = 0f;
            }

            //If not drawn then snap to weight
            if (!cit.visible)
            {
                mainAnimator.SetLayerWeight(2, umbreallLayerDesiredWeight);

                if(spawnedUmbrella != null)
                {
                    umbrellaCanopy.localScale = Vector3.Lerp(new Vector3(0.15f, 0.15f, 3f), Vector3.one, umbreallLayerDesiredWeight);

                    if (!umbrella)
                    {
                        Destroy(spawnedUmbrella);
                        cit.UpdateMeshList(); //Updating this will automatically remove any destroyed objects...
                    }
                }
            }
            else if (cit.ai != null)
            {
                cit.ai.SetUpdateEnabled(true); //Set enabled in order to allow animation layer lerping
            }
        }
    }

    public void SetCarryingItem(bool val)
    {
        mainAnimator.SetBool("carryingItem", val);
    }

    public void SetCarryItemType(int carryType)
    {
        mainAnimator.SetInteger("carryItem", carryType);
    }

    public void SetInCombat(bool val)
    {
        if (val)
        {
            mainAnimator.cullingMode = AnimatorCullingMode.AlwaysAnimate;

            //SetIdleAnimationState(IdleAnimationState.none);
            //SetArmsBoolState(ArmsBoolSate.none);
            SetInBed(false);
            SetUmbrella(false);

            //Make sure arms take priority for holding weapon
            if (cit.ai != null && cit.ai.combatMode > 0)
            {
                armsLayerDesiredWeight = 1f; //Override combat animation with appropriate arm animation
                if (Game.Instance.collectDebugData) cit.SelectedDebug("Set animator arms weight: " + armsLayerDesiredWeight, Actor.HumanDebug.misc);
            }
        }
        else mainAnimator.cullingMode = AnimatorCullingMode.CullCompletely;

        mainAnimator.SetBool("inCombat", val); //Set movement pose
    }

    public void SetCombatArmsOverride(int val)
    {
        if (Game.Instance.collectDebugData) cit.SelectedDebug("Set combat mode " + val, Actor.HumanDebug.attacks);

        //Game.Log(cit.name + " Set arms override " + val);
        mainAnimator.SetInteger("combatArmsOverride", val); //Override combat behaviours
    }

    public void SetRestrained(bool val)
    {
        if (val)
        {
            if (!cit.ai.isRagdoll)
            {
                SetArmsBoolState(ArmsBoolSate.armsCuffed);
            }
        }
        else
        {
            SetArmsBoolState(ArmsBoolSate.none);
        }

        mainAnimator.SetBool("restrained", val); //Set movement pose
    }

    public void SetIdleAnimationState(IdleAnimationState newState)
    {
        //Reset static from animation
        if (newState == IdleAnimationState.none)
        {
            cit.ai.staticAnimationSafetyTimer = 0f;
            cit.ai.SetStaticFromAnimation(false);
        }

        idleAnimationState = newState;
        mainAnimator.SetInteger("IdleAnimationState", (int)idleAnimationState);

        cit.SelectedDebug("Set idle state " + newState, Actor.HumanDebug.misc);
    }

    //Also pass whether this animation should be flipped
    public void SetInBed(bool val, bool instant = false)
    {
        //mainAnimator.ResetTrigger("skipAnimation");

        //cit.SelectedDebug("In bed " + val);
        mainAnimator.SetBool("isInBed", val);

        if (instant && val)
        {
            if(!flipToRightAnimation)
            {
                mainAnimator.Play("GetIntoBedLeft", 0, 1f);
            }
            else
            {
                mainAnimator.Play("GetIntoBedRight", 0, 1f);
            }
        }
    }

    public void FlipAnimationToRight(bool val)
    {
        flipToRightAnimation = val;
        mainAnimator.SetBool("flipAnimationRight", val);
    }

    //Set dead
    public void SetDead(bool val)
    {
        //cit.SelectedDebug("Dead " + val);
        mainAnimator.SetBool("isDead", val);
    }

    public void TriggerTrip()
    {
        mainAnimator.SetBool("trip", true);
    }

    public void CancelTrip()
    {
        if(mainAnimator != null) mainAnimator.SetBool("trip", false);
    }

    public void AttackTrigger()
    {
        //Make sure the animator is NOT culled, as attacking relies on an animation event...
        mainAnimator.cullingMode = AnimatorCullingMode.AlwaysAnimate;

        float rand = Toolbox.Instance.Rand(0f, 1f);

        //Only do other stuff if unarmed
        if (rand >= 0.5f || cit.ai.combatMode > 0)
        {
            if (Game.Instance.collectDebugData) cit.SelectedDebug("Trigger attack!", Actor.HumanDebug.attacks);
            mainAnimator.SetTrigger("attack");
        }
        else if (rand >= 0.25f)
        {
            if (Game.Instance.collectDebugData) cit.SelectedDebug("Trigger attack2!", Actor.HumanDebug.attacks);
            mainAnimator.SetTrigger("attack2");
        }
        else
        {
            if (Game.Instance.collectDebugData) cit.SelectedDebug("Trigger kick!", Actor.HumanDebug.attacks);
            mainAnimator.SetTrigger("kick");
        }
    }

    public void ThrowTrigger()
    {
        if (Game.Instance.collectDebugData) cit.SelectedDebug("Throw attack!", Actor.HumanDebug.attacks);

        mainAnimator.SetTrigger("throw");
        //mainAnimator.ResetTrigger("attack");
    }

    public void AbortAttackTrigger()
    {
        if (Game.Instance.collectDebugData) cit.SelectedDebug("Abort attack!", Actor.HumanDebug.attacks);

        mainAnimator.SetTrigger("abortAttack");
        //mainAnimator.ResetTrigger("abortAttack");
    }

    public void BlockTrigger(float blockDelay, bool perfect = false)
    {
        if(perfect)
        {
            mainAnimator.SetTrigger("blockPerfect");
            //mainAnimator.ResetTrigger("blockPerfect");
        }
        else
        {
            mainAnimator.SetTrigger("block");
            //mainAnimator.ResetTrigger("block");
        }
    }

    //Recoil from damage
    public void TakeDamageRecoil(Vector3 hitPosition)
    {
        //Calculate the force and use it to change the transition look multipliers...
        //Use relative position

        Vector3 relativeToMe = cit.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.upperTorso).InverseTransformPoint(hitPosition);
        Game.Log("AI: Incoming damage relative to UpperTorso: " + relativeToMe);

        //Pass this to the animation controller to player the proper animation response
        if(mainAnimator != null)
        {
            mainAnimator.SetFloat("hitX", relativeToMe.x);
            mainAnimator.SetFloat("hitY", relativeToMe.y);
            mainAnimator.SetTrigger("hit");
            //mainAnimator.ResetTrigger("hit");
        }
    }

    //I'm pretty sure this is irrelevent as the model gets turned off when citizen isn't visible...
    public void SetPauseAnimation(bool val)
    {
        paused = val;

        if (paused)
        {
            if(mainAnimator != null) mainAnimator.enabled = false;
        }
        else
        {
            if (mainAnimator != null) mainAnimator.enabled = true;
        }

        ForceUpdateAnimationSate(true);
    }

    //Trun this into a ragdoll
    public void SetRagdoll(bool val, bool dead = false)
    {
        if(cit.ai.isRagdoll != val)
        {
            Game.Log("AI: Set ragdoll " + val);

            cit.ai.isRagdoll = val;

            if(val)
            {
                if(!GameplayController.Instance.activeRagdolls.Contains(cit))
                {
                    GameplayController.Instance.activeRagdolls.Add(cit);
                    Player.Instance.UpdateCulling();
                }

                cit.ai.SetUpdateEnabled(true); //Make sure updates are enabled for ragdoll timeout

                cit.ai.SetExpression(CitizenOutfitController.Expression.asleep);
                SetArmsBoolState(ArmsBoolSate.none);

                foreach(CitizenOutfitController.AnchorConfig anchor in cit.outfitController.anchorConfig)
                {
                    if (anchor.anchor == CitizenOutfitController.CharacterAnchor.ArmsParent) continue; //Don't do this for arms
                    else if (anchor.anchor == CitizenOutfitController.CharacterAnchor.Hair) continue; //Don't do this for hair
                    //else if (anchor.anchor == CitizenOutfitController.CharacterAnchor.Midriff) continue; //Don't do this for midriff
                    else if (anchor.anchor == CitizenOutfitController.CharacterAnchor.beard) continue; //Don't do this for beard

                    //Create physics objects from parents
                    CitizenPhysics cp = new CitizenPhysics();
                    cp.anchorConfig = anchor;

                    //Use mesh colliders for certain parts...
                    if(
                        anchor.anchor == CitizenOutfitController.CharacterAnchor.Hat ||
                        anchor.anchor == CitizenOutfitController.CharacterAnchor.Glasses ||
                        anchor.anchor == CitizenOutfitController.CharacterAnchor.HandLeft ||
                        anchor.anchor == CitizenOutfitController.CharacterAnchor.HandRight ||
                        anchor.anchor == CitizenOutfitController.CharacterAnchor.LeftFoot ||
                        anchor.anchor == CitizenOutfitController.CharacterAnchor.RightFoot
                        )
                    {
                        foreach(CitizenOutfitController.OutfitClothes outfit in cit.outfitController.currentlyLoadedClothes)
                        {
                            if(outfit.spawned.ContainsKey(anchor.anchor))
                            {
                                foreach(MeshRenderer meshRend in outfit.spawned[anchor.anchor])
                                {
                                    MeshFilter mesh = meshRend.gameObject.GetComponent<MeshFilter>();
                                    if (mesh == null) continue;

                                    Collider existing = anchor.trans.gameObject.GetComponent<Collider>();

                                    if(existing != null)
                                    {
                                        //Game.LogError("Existing collider found on " + anchor.trans.gameObject.name + ", destroyed so we can create a mesh collider...");
                                        Destroy(existing);
                                    }

                                    MeshCollider meshColl = anchor.trans.gameObject.AddComponent<MeshCollider>();
                                    meshColl.sharedMesh = mesh.sharedMesh;
                                    meshColl.convex = true;
                                    cp.coll = meshColl;
                                    createdColliders.Add(cp.coll);
                                    break; //Only create 1...
                                }
                            }
                        }
                    }
                    //Otherwise create special capsule colliders
                    else
                    {
                        CapsuleCollider cap = anchor.trans.gameObject.AddComponent<CapsuleCollider>();

                        if(anchor.anchor == CitizenOutfitController.CharacterAnchor.Head)
                        {
                            cap.center = new Vector3(0f, 0.15f, 0.05f);
                            cap.radius = 0.1f;
                            cap.height = 0.33f;
                        }
                        else if(anchor.anchor == CitizenOutfitController.CharacterAnchor.upperTorso)
                        {
                            cap.center = new Vector3(0f, 0.15f, 0.02f);
                            cap.radius = 0.18f;
                            cap.height = 0.45f;
                            cap.direction = 0;
                        }
                        else if (anchor.anchor == CitizenOutfitController.CharacterAnchor.Midriff)
                        {
                            cap.center = new Vector3(0f, 0f, 0.01f);
                            cap.radius = 0.14f;
                            cap.height = 0.33f;
                            cap.direction = 0;
                        }
                        else if (anchor.anchor == CitizenOutfitController.CharacterAnchor.lowerTorso)
                        {
                            cap.center = new Vector3(0f, 0.18f, 0f);
                            cap.radius = 0.18f;
                            cap.height = 0.4f;
                            cap.direction = 0;
                        }
                        if (anchor.anchor == CitizenOutfitController.CharacterAnchor.UpperArmLeft || anchor.anchor == CitizenOutfitController.CharacterAnchor.UpperArmRight)
                        {
                            cap.center = new Vector3(0f, -0.12f, 0.025f);
                            cap.radius = 0.05f;
                            cap.height = 0.35f;
                        }
                        if (anchor.anchor == CitizenOutfitController.CharacterAnchor.LowerArmLeft || anchor.anchor == CitizenOutfitController.CharacterAnchor.LowerArmRight)
                        {
                            cap.center = new Vector3(0f, -0.1f, 0.025f);
                            cap.radius = 0.05f;
                            cap.height = 0.28f;
                        }
                        if (anchor.anchor == CitizenOutfitController.CharacterAnchor.UpperLegLeft || anchor.anchor == CitizenOutfitController.CharacterAnchor.UpperLegRight)
                        {
                            cap.center = new Vector3(0.11f, -0.15f, 0.02f);
                            cap.radius = 0.12f;
                            cap.height = 0.45f;
                        }
                        if (anchor.anchor == CitizenOutfitController.CharacterAnchor.LowerLegLeft || anchor.anchor == CitizenOutfitController.CharacterAnchor.LowerLegRight)
                        {
                            cap.center = new Vector3(0f, -0.16f, -0.02f);
                            cap.radius = 0.05f;
                            cap.height = 0.4f;
                        }

                        cp.coll = cap;
                        createdColliders.Add(cp.coll);
                    }

                    //Add rigidbodies to all limbs
                    cp.rb = anchor.trans.gameObject.AddComponent<Rigidbody>();

                    //Possibly this has already been addedd...
                    if(cp.rb == null)
                    {
                        cp.rb = anchor.trans.gameObject.GetComponent<Rigidbody>();
                    }

                    if(cp.rb != null)
                    {
                        cp.rb.collisionDetectionMode = CollisionDetectionMode.Continuous; //Make sure this is continuous to ensure it doesn't fall through objects
                        createdRBs.Add(cp.rb);
                    }

                    physicsComponents.Add(anchor.anchor, cp);

                    if(anchor.anchor == CitizenOutfitController.CharacterAnchor.lowerTorso)
                    {
                        //This script will play a sound when the body hits the floor
                        sfx = anchor.trans.gameObject.AddComponent<RagdollSFXController>();
                        sfx.actor = cit;
                    }
                    else if(anchor.anchor == CitizenOutfitController.CharacterAnchor.upperTorso)
                    {
                        if (cp.rb != null) upperTorsoRB = cp.rb;
                    }
                }

                //Disable character collider
                if(cit.interactableController != null && cit.interactableController.coll != null)
                {
                    cit.interactableController.coll.enabled = false;

                    //Add new collider box for interactable controller...
                    if(!cit.isDead)
                    {
                        //Game.Log("Create box collider for " + cit.name);
                        newBoxCollider = cit.interactableController.gameObject.AddComponent<BoxCollider>();
                        newBoxCollider.isTrigger = true;
                        cit.interactableController.coll = newBoxCollider;
                    }
                }

                //Add and configure character joints...
                CitizenPhysics hat = null;
                CitizenPhysics glasses = null;

                CitizenPhysics head = null;
                CitizenPhysics upperTorso = null;
                CitizenPhysics lowerTorso = null;
                CitizenPhysics midriff = null;

                CitizenPhysics upperRightArm = null;
                CitizenPhysics lowerRightArm = null;
                CitizenPhysics upperLeftArm = null;
                CitizenPhysics lowerLeftArm = null;
                CitizenPhysics rightHand = null;
                CitizenPhysics leftHand = null;

                CitizenPhysics upperRightLeg = null;
                CitizenPhysics lowerRightLeg = null;
                CitizenPhysics upperLeftLeg = null;
                CitizenPhysics lowerLeftLeg = null;
                CitizenPhysics rightFoot = null;
                CitizenPhysics leftFoot = null;

                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.Hat, out hat);
                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.Glasses, out glasses);

                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.Head, out head);
                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.upperTorso, out upperTorso);

                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.Midriff, out midriff);

                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.lowerTorso, out lowerTorso);
                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.UpperArmRight, out upperRightArm);
                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.UpperArmLeft, out upperLeftArm);
                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.LowerArmRight, out lowerRightArm);
                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.LowerArmLeft, out lowerLeftArm);
                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.HandRight, out rightHand);
                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.HandLeft, out leftHand);

                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.UpperLegRight, out upperRightLeg);
                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.UpperLegLeft, out upperLeftLeg);
                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.LowerLegRight, out lowerRightLeg);
                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.LowerLegLeft, out lowerLeftLeg);
                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.RightFoot, out rightFoot);
                physicsComponents.TryGetValue(CitizenOutfitController.CharacterAnchor.LeftFoot, out leftFoot);

                //Head connected to upper torso
                if (head != null && upperTorso != null && head.coll != null)
                {
                    headJoint = head.coll.transform.gameObject.AddComponent<CharacterJoint>();

                    if(headJoint != null)
                    {
                        headJoint.anchor = headJoint.transform.InverseTransformPoint(head.anchorConfig.trans.position);
                        headJoint.connectedBody = upperTorso.rb;
                        headJoint.axis = new Vector3(1, 1, 0);
                        createdJoints.Add(headJoint);
                    }
                }

                //Upper torso connected to midriff
                if (upperTorso != null && midriff != null)
                {
                    upperTorsoJoint = upperTorso.coll.transform.gameObject.AddComponent<CharacterJoint>();

                    if (upperTorsoJoint != null)
                    {
                        upperTorsoJoint.anchor = upperTorsoJoint.transform.InverseTransformPoint(upperTorso.anchorConfig.trans.position);
                        upperTorsoJoint.connectedBody = midriff.rb;
                        createdJoints.Add(upperTorsoJoint);
                    }
                }

                //Midriff connected to lower torso
                if (midriff != null && lowerTorso != null && midriff.coll != null)
                {
                    midriffJoint = midriff.coll.transform.gameObject.AddComponent<CharacterJoint>();

                    if (midriffJoint != null)
                    {
                        midriffJoint.anchor = midriffJoint.transform.InverseTransformPoint(midriff.anchorConfig.trans.position) - new Vector3(0f, -0.05f, 0f);
                        midriffJoint.connectedBody = lowerTorso.rb;
                        createdJoints.Add(midriffJoint);
                    }
                }

                //Upper torso connected to lower torso
                //if (upperTorso != null && lowerTorso != null && upperTorso.coll != null)
                //{
                //    //Connected to lower torso
                //    upperTorsoJoint = upperTorso.coll.transform.gameObject.AddComponent<CharacterJoint>();

                //    if (upperTorsoJoint != null)
                //    {
                //        upperTorsoJoint.anchor = upperTorsoJoint.transform.InverseTransformPoint(lowerTorso.anchorConfig.trans.position) - new Vector3(0f, -0.05f, 0f);
                //        upperTorsoJoint.connectedBody = lowerTorso.rb;
                //        createdJoints.Add(upperTorsoJoint);
                //    }
                //}

                //Upppr arms connected to upper torso
                if (upperTorso != null)
                {
                    if(upperLeftArm != null && upperLeftArm.coll != null)
                    {
                        leftUpperArmJoint = upperLeftArm.coll.transform.gameObject.AddComponent<CharacterJoint>();

                        if(leftUpperArmJoint != null)
                        {
                            leftUpperArmJoint.anchor = leftUpperArmJoint.transform.InverseTransformPoint(upperLeftArm.anchorConfig.trans.position);
                            leftUpperArmJoint.connectedBody = upperTorso.rb;
                            leftUpperArmJoint.axis = new Vector3(0, 1, 1);
                            createdJoints.Add(leftUpperArmJoint);
                        }

                        if(lowerLeftArm != null && lowerLeftArm.coll != null)
                        {
                            //Lower arms connected to upper arms
                            leftLowerArmJoint = lowerLeftArm.coll.transform.gameObject.AddComponent<CharacterJoint>();

                            if(leftLowerArmJoint != null)
                            {
                                leftLowerArmJoint.anchor = leftLowerArmJoint.transform.InverseTransformPoint(lowerLeftArm.anchorConfig.trans.position);
                                leftLowerArmJoint.connectedBody = upperLeftArm.rb;
                                leftLowerArmJoint.axis = new Vector3(0, 1, 1);
                                createdJoints.Add(leftLowerArmJoint);
                            }

                            if(leftHand != null && leftHand.coll != null)
                            {
                                //Hands are connected to upper arms
                                leftHandJoint = leftHand.coll.transform.gameObject.AddComponent<CharacterJoint>();

                                if(leftHandJoint != null)
                                {
                                    leftHandJoint.anchor = leftHandJoint.transform.InverseTransformPoint(leftHand.anchorConfig.trans.position);
                                    leftHandJoint.connectedBody = lowerLeftArm.rb;
                                    createdJoints.Add(leftHandJoint);
                                }
                            }
                        }
                    }

                    if(upperRightArm != null && upperRightArm.coll != null)
                    {
                        rightUpperArmJoint = upperRightArm.coll.transform.gameObject.AddComponent<CharacterJoint>();

                        if(rightUpperArmJoint != null)
                        {
                            rightUpperArmJoint.anchor = rightUpperArmJoint.transform.InverseTransformPoint(upperRightArm.anchorConfig.trans.position);
                            rightUpperArmJoint.connectedBody = upperTorso.rb;
                            rightUpperArmJoint.axis = new Vector3(0, 1, 1);
                            createdJoints.Add(rightUpperArmJoint);
                        }

                        if(lowerRightArm != null && lowerRightArm.coll != null)
                        {
                            rightLowerArmJoint = lowerRightArm.coll.transform.gameObject.AddComponent<CharacterJoint>();

                            if(rightLowerArmJoint != null)
                            {
                                rightLowerArmJoint.anchor = rightLowerArmJoint.transform.InverseTransformPoint(lowerRightArm.anchorConfig.trans.position);
                                rightLowerArmJoint.connectedBody = upperRightArm.rb;
                                rightLowerArmJoint.axis = new Vector3(0, 1, 1);
                                createdJoints.Add(rightLowerArmJoint);
                            }

                            if(rightHand != null && rightHand.coll != null)
                            {
                                rightHandJoint = rightHand.coll.transform.gameObject.AddComponent<CharacterJoint>();

                                if(rightHandJoint != null)
                                {
                                    rightHandJoint.anchor = rightHandJoint.transform.InverseTransformPoint(rightHand.anchorConfig.trans.position);
                                    rightHandJoint.connectedBody = lowerRightArm.rb;
                                    createdJoints.Add(rightHandJoint);
                                }
                            }
                        }
                    }
                }

                //Upper legs are connected to lower torso
                if(lowerTorso != null)
                {
                    if(upperLeftLeg != null && upperLeftLeg.coll != null)
                    {
                        leftUpperLegJoint = upperLeftLeg.coll.transform.gameObject.AddComponent<CharacterJoint>();

                        if(leftUpperLegJoint != null)
                        {
                            leftUpperLegJoint.anchor = leftUpperLegJoint.transform.InverseTransformPoint(upperLeftLeg.anchorConfig.trans.position);
                            leftUpperLegJoint.connectedBody = lowerTorso.rb;
                            createdJoints.Add(leftUpperLegJoint);
                        }

                        if(lowerLeftLeg != null && lowerLeftLeg.coll != null)
                        {
                            //Lower legs are connected to upper legs
                            leftLowerLegJoint = lowerLeftLeg.coll.transform.gameObject.AddComponent<CharacterJoint>();

                            if(leftLowerLegJoint != null)
                            {
                                leftLowerLegJoint.anchor = leftLowerLegJoint.transform.InverseTransformPoint(lowerLeftLeg.anchorConfig.trans.position);
                                leftLowerLegJoint.connectedBody = upperLeftLeg.rb;
                                createdJoints.Add(leftLowerLegJoint);
                            }

                            if(leftFoot != null && leftFoot.coll != null)
                            {
                                //Lower legs are connected to upper legs
                                leftFootJoint = leftFoot.coll.transform.gameObject.AddComponent<CharacterJoint>();

                                if(leftFootJoint != null)
                                {
                                    leftFootJoint.anchor = leftFootJoint.transform.InverseTransformPoint(leftFoot.anchorConfig.trans.position);
                                    leftFootJoint.connectedBody = lowerLeftLeg.rb;
                                    createdJoints.Add(leftFootJoint);
                                }
                            }
                        }
                    }

                    if(upperRightLeg != null && upperRightLeg.coll != null)
                    {
                        rightUpperLegJoint = upperRightLeg.coll.transform.gameObject.AddComponent<CharacterJoint>();

                        if(rightUpperLegJoint != null)
                        {
                            rightUpperLegJoint.anchor = rightUpperLegJoint.transform.InverseTransformPoint(upperRightLeg.anchorConfig.trans.position);
                            rightUpperLegJoint.connectedBody = lowerTorso.rb;
                            createdJoints.Add(rightUpperLegJoint);
                        }

                        if(lowerRightLeg != null && lowerRightLeg.coll != null)
                        {
                            rightLowerLegJoint = lowerRightLeg.coll.transform.gameObject.AddComponent<CharacterJoint>();

                            if(rightLowerLegJoint != null)
                            {
                                rightLowerLegJoint.anchor = rightLowerLegJoint.transform.InverseTransformPoint(lowerRightLeg.anchorConfig.trans.position);
                                rightLowerLegJoint.connectedBody = upperRightLeg.rb;
                                createdJoints.Add(rightLowerLegJoint);
                            }

                            if(rightFoot != null && rightFoot.coll != null)
                            {
                                //Lower legs are connected to upper legs
                                rightFootJoint = rightFoot.coll.transform.gameObject.AddComponent<CharacterJoint>();

                                if(rightFootJoint != null)
                                {
                                    rightFootJoint.anchor = rightFootJoint.transform.InverseTransformPoint(rightFoot.anchorConfig.trans.position);
                                    rightFootJoint.connectedBody = lowerRightLeg.rb;
                                    createdJoints.Add(rightFootJoint);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if(sfx != null)
                {
                    Destroy(sfx);
                }

                GameplayController.Instance.activeRagdolls.Remove(cit);

                cit.ai.SetExpression(CitizenOutfitController.Expression.surprised);
                SetArmsBoolState(ArmsBoolSate.none);

                //Remove joints in order of extremities
                if (rightFootJoint != null) Destroy(rightFootJoint);
                if (leftFootJoint != null) Destroy(leftFootJoint);

                if (rightLowerLegJoint != null) Destroy(rightLowerLegJoint);
                if (leftLowerLegJoint != null) Destroy(leftLowerLegJoint);

                if (rightUpperLegJoint != null) Destroy(rightUpperLegJoint);
                if (leftUpperLegJoint != null) Destroy(leftUpperLegJoint);

                if (rightHandJoint != null) Destroy(rightHandJoint);
                if (leftHandJoint != null) Destroy(leftHandJoint);

                if (rightLowerArmJoint != null) Destroy(rightLowerArmJoint);
                if (leftLowerArmJoint != null) Destroy(leftLowerArmJoint);

                if (rightUpperArmJoint != null) Destroy(rightUpperArmJoint);
                if (leftUpperArmJoint != null) Destroy(leftUpperArmJoint);

                if (headJoint != null) Destroy(headJoint);

                if (upperTorsoJoint != null) Destroy(upperTorsoJoint);
                if (midriffJoint != null) Destroy(midriffJoint);

                //Remove cycle
                int safety = 200;

                while((createdJoints.Count > 0 || createdRBs.Count > 0 || createdColliders.Count > 0) && safety > 0)
                {
                    if(createdJoints.Count > 0)
                    {
                        if (createdJoints[0] != null)
                        {
                            createdJoints[0].connectedBody = null;
                            Destroy(createdJoints[0]);
                        }

                        createdJoints.RemoveAt(0);
                    }

                    if(createdRBs.Count > 0)
                    {
                        Destroy(createdRBs[0]);
                        createdRBs.RemoveAt(0);
                    }

                    if(createdColliders.Count > 0)
                    {
                        Destroy(createdColliders[0]);
                        createdColliders.RemoveAt(0);
                    }

                    safety--;

                    if(safety <= 0)
                    {
                        Game.LogError("Reached safety event while trying to destroy ragdoll physics! Remaining: " + createdJoints.Count + " joints, " + createdRBs + "RBs, and " + createdColliders.Count + " colliders.");
                        break;
                    }
                    else if(createdJoints.Count <= 0 && createdRBs.Count <= 0 && createdColliders.Count <= 0)
                    {
                        Game.Log("Removed all ragdoll physics elements successfully");
                    }
                }

                //Disable character collider
                if (cit.interactableController != null && cit.interactableController.coll != null)
                {
                    if(newBoxCollider != null) Destroy(newBoxCollider);

                    cit.interactableController.coll = cit.interactableController.gameObject.GetComponent<Collider>();
                    cit.interactableController.coll.enabled = true;
                }

                ragdollSnapshot = GetLimbSnapshot();

                createdRBs.Clear();
                createdJoints.Clear();
                createdColliders.Clear();
                physicsComponents.Clear();

                //If dead, remove animation controllers...
                if(dead && mainAnimator != null)
                {
                    if (newBoxCollider != null) Destroy(newBoxCollider);
                    Destroy(mainAnimator);
                }

                if(dead)
                {
                    cit.ai.SetUpdateEnabled(false); //Disable update if dead
                }
            }

            cit.outfitController.HairHatCompatibilityCheck(); //Check for reveal of hat/hair
        }
    }

    //Used for transitioning from ragdoll to animation
    public List<RagdollSnapshot> GetLimbSnapshot()
    {
        List<RagdollSnapshot> newSnapshot = new List<RagdollSnapshot>();

        foreach(CitizenOutfitController.AnchorConfig anchor in cit.outfitController.anchorConfig)
        {
            RagdollSnapshot snapshot = new RagdollSnapshot();
            snapshot.anchorConfig = anchor;
            snapshot.localPos = anchor.trans.localPosition;
            snapshot.localRot = anchor.trans.localRotation;

            newSnapshot.Add(snapshot);
        }

        return newSnapshot;
    }

    public void LoadLimbSnapshot(List<RagdollSnapshot> snapshot)
    {
        foreach (RagdollSnapshot s in snapshot)
        {
            Transform anchor = null;

            cit.outfitController.anchorReference.TryGetValue(s.anchorConfig.anchor, out anchor);

            if (anchor != null)
            {
                anchor.localPosition = s.localPos;
                anchor.localRotation = s.localRot;
            }
        }
    }
}
