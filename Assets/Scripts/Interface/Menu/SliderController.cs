﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SliderController : MonoBehaviour
{
    [Header("Components")]
    public Slider slider;
    public ButtonController prevButton;
    public ButtonController nextButton;
    public TextMeshProUGUI label;

    [Header("Configuration")]
    public string labelDictRef;
    public string playerPrefsID;
    public bool displayValue = true;
    public bool isPercentage = true;
    private bool clickThisFrame = false;

    private void Start()
    {
        //Assign button events
        nextButton.button.onClick.AddListener(delegate { OnNextButton(); });
        prevButton.button.onClick.AddListener(delegate { OnPreviousButton(); });

        //Remove menu auto text controller as this is handled in this script...
        if(label != null)
        {
            MenuAutoTextController menuAuto = label.GetComponent<MenuAutoTextController>();
            if(menuAuto != null) Destroy(menuAuto);
        }

        UpdateDisplayValue();
    }

    public void OnNextButton()
    {
        if(!clickThisFrame)
        {
            slider.value++;
            clickThisFrame = true;
            StartCoroutine(RunEnd());
        }
    }

    public void OnPreviousButton()
    {
        if (!clickThisFrame)
        {
            slider.value--;
            clickThisFrame = true;
            StartCoroutine(RunEnd());
        }
    }

    IEnumerator RunEnd()
    {
        bool runOnce = true;

        while(runOnce)
        {
            runOnce = false;
            yield return new WaitForEndOfFrame();
        }

        clickThisFrame = false;
    }

    public void SetValueWithoutNotify(int newVal)
    {
        slider.SetValueWithoutNotify(newVal);
        UpdateDisplayValue();
    }

    public void OnValueChange()
    {
        //Game.Log("Value changed: " + slider.value);

        if (playerPrefsID != null && playerPrefsID.Length > 0)
        {
            PlayerPrefsController.Instance.OnToggleChanged(playerPrefsID, true, this);
        }

        UpdateDisplayValue();
    }

    public void UpdateDisplayValue()
    {
        if(label != null && labelDictRef != null && labelDictRef.Length > 0)
        {
            if(Strings.textFilesLoaded) label.text = Strings.Get("ui.interface", labelDictRef);

            if(displayValue)
            {
                string percent = string.Empty;
                if (isPercentage) percent = "%";

                label.text += ": " + slider.value + percent;
            }
        }
    }
}
