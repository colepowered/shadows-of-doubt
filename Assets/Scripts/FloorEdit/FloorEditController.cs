﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using TMPro;
using System.Linq;
using System;
using UnityEngine.EventSystems;
using NaughtyAttributes;
using UnityEngine.Tilemaps;

public class FloorEditController : MonoBehaviour
{
    [Header("General References")]
    public CityTile cityTile;
    public NewBuilding building;
    public Transform editorFloorParent;
    public GameObject enabledInScrollView;
    public RectTransform toolOptionsWindow;
    public enum EditorDisplayMode {normal, displayAddressDesignation, displayRoomSelection};
    public EditorDisplayMode displayMode = EditorDisplayMode.normal;
    public enum EditorSelectionMode { tile, wall, node};
    public EditorSelectionMode selectionModeType = EditorSelectionMode.tile;
    public InteractablePreset lightswitchPreset;
    public Transform fakeCitizenHolder;
    public bool heldDown = false;
    public NewNode heldDownOriginNode;
    public Transform heldDownTransform;

    private int recalculationDelay = 0;
    private string currentRecalculation = string.Empty;
    public bool isSaving = false;
    public bool loaded = false; //Has this loaded a floor?

    [Header("Movement")]
    public bool rightMouse = false;
    int selectionLayerMask;
    int wallsSelectionMask;

    [Header("New Floor")]
    public RectTransform newFloorWindow;
    public TMP_InputField newFloorName;
    public TMP_InputField newFloorSizeX;
    public TMP_InputField newFloorSizeY;
    public TMP_InputField newFloorFloorHeight;
    public TMP_InputField newFloorCeilingHeight;

    [Header("Save As")]
    public RectTransform saveAsFloorWindow;
    public InputField newSaveAsFloorName;

    [Header("Load")]
    public RectTransform loadFloorWindow;
    public TMP_Dropdown loadDropdown;
    private List<string> loadFilePaths = new List<string>();

    [Header("Map")]
    public GameObject mapParent;

    [Header("Current Data")]
    public NewFloor editFloor;

    [Header("Selection")]
    public bool selectionMode = true;
    public Transform selectionObject;
    public Transform floorSelectCursorObject;
    public Transform wallSelectCursorObject;
    public TextMeshProUGUI statusText;

    public NewTile tileSelection = null;
    public NewNode nodeSelection = null;
    public NewWall wallSelection = null;
    public Vector2 selectionCoord = new Vector2(-1, -1);
    public enum FloorEditTool { none, floorDesignation, addressDesignation, wallDesignation, rotateFloor, mainEntrance, secondaryEntrance, stairwell, elevator, forceRoom, roomDesignation};
    public FloorEditTool tool = FloorEditTool.none;
    public List<GameObject> wallTriggers = new List<GameObject>();

    [Header("Tools")]
    //Floor designation
    public RectTransform floorDesignationOptions;
    public TMP_Dropdown floorDesignationDropdown;
    public NewNode.FloorTileType floorDesignationTypeSelection = NewNode.FloorTileType.floorAndCeiling;
    [Space(5)]
    //Address designation
    public List<Color> editorAddressColours = new List<Color>();
    public Material adddressDesignationMaterial;
    public RectTransform addressDesignationOptions;
    public TMP_Dropdown addressDropdown;
    public TMP_Dropdown addressTypeDropdown;
    public NewAddress addressSelection;

    public LayoutConfiguration addressTypeSelection;
    public Image addressDesignationColourImage;
    public Image addressDesignationColourImage2;
    [Space(5)]
    //Room designation
    public RectTransform roomDesignationOptions;
    public TMP_Dropdown roomConfigAddressDropdown;
    public TMP_Dropdown roomConfigsDropdown;
    public TMP_Dropdown roomIDsDropdown;
    public TMP_Dropdown roomLayoutAssignDropdown;
    public NewRoom roomSelection;
    [Space(5)]
    //Doorways
    public RectTransform wallPairsOptions;
    public TMP_Dropdown wallPairsDropdown;
    public DoorPairPreset wallPairPresetSelection;
    [Space(5)]
    public RectTransform forceRoomOptions;
    public TMP_Dropdown forceRoomDropdown;
    [System.NonSerialized] //Must not be serialized or it cannot be null!
    public RoomConfiguration forceRoomSelection;
    [Space(5)]
    public Toggle forceBasementToggle;

    [Header("Materials")]
    public Material editorFloorMaterial;
    public Material editorFloorEdgeMaterial;
    public MaterialGroupPreset defaultFloorMaterial;
    public Toolbox.MaterialKey defaultMaterialKey;

    [Header("Scriptable Object Data")]
    public RoomTypePreset nullRoomType;
    private List<RoomTypePreset> allLayoutTypes;
    private List<LayoutConfiguration> allLayouts;
    private List<RoomConfiguration> allRoomConfigs;
    private List<DoorPairPreset> allDoorPairs;
    private List<DoorPairPreset> selectableDoorPairs;
    private List<RoomConfiguration> selectableRooms = new List<RoomConfiguration>();

    [Header("Debugging")]
    public Transform debugContainer;
    public GenerationDebugController currentlyDisplayingArea;

    //Singleton pattern
    private static FloorEditController _instance;
    public static FloorEditController Instance { get { return _instance; } }

    //Init
    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    //Start is called before the first frame update
    void Start()
    {
        PathFinder.Instance.SetDimensions(); //Make sure dimensions are set

        CityData.Instance.seed = Toolbox.Instance.GenerateSeed(16);

        selectionLayerMask = Toolbox.Instance.CreateLayerMask(Toolbox.LayerMaskMode.onlyCast, 29);
        wallsSelectionMask = Toolbox.Instance.CreateLayerMask(Toolbox.LayerMaskMode.onlyCast, 0);

        //Setup building
        building.globalTileCoords = CityData.Instance.CityTileToTile(Vector2Int.zero);

        //Setup options
        //Load layouts
        allLayouts = AssetLoader.Instance.GetAllLayoutConfigurations();
        allRoomConfigs = AssetLoader.Instance.GetAllRoomConfigurations();
        allLayoutTypes = AssetLoader.Instance.GetAllRoomTypePresets();

        addressTypeDropdown.ClearOptions();

        List<string> stringOptions = new List<string>();

        for (int i = 0; i < allLayouts.Count; i++)
        {
            stringOptions.Add(allLayouts[i].name.ToString());
        }

        addressTypeDropdown.AddOptions(stringOptions);

        roomLayoutAssignDropdown.ClearOptions();

        //Load door pairs
        allDoorPairs = AssetLoader.Instance.GetAllDoorPairPresets();
        wallPairsDropdown.ClearOptions();
        selectableDoorPairs = new List<DoorPairPreset>();

        foreach (DoorPairPreset dp in allDoorPairs)
        {
            if (dp.appearInEditor)
            {
                selectableDoorPairs.Add(dp);
            }
        }

        stringOptions.Clear();

        for (int i = 0; i < selectableDoorPairs.Count; i++)
        {
            stringOptions.Add(selectableDoorPairs[i].name.ToString());
        }

        wallPairsDropdown.AddOptions(stringOptions);
        stringOptions.Clear();

        //Load force room options
        forceRoomDropdown.options.Clear();

        foreach(RoomConfiguration room in allRoomConfigs)
        {
            selectableRooms.Add(room);
            stringOptions.Add(room.name.ToString());
        }

        forceRoomDropdown.AddOptions(stringOptions);

        StartGame();
        SetTool(FloorEditTool.none);
        SetDisplayMode(EditorDisplayMode.normal);
        SetSelectionMode(EditorSelectionMode.tile);

        //Search for restart commands...
        if (RestartSafeController.Instance.newFloor)
        {
            RestartSafeController.Instance.newFloor = false;
            CreateNewFloor();
        }
        else if (RestartSafeController.Instance.loadFloor)
        {
            //Make sure floor data is loaded...
            if (CityData.Instance.floorData.Count <= 0)
            {
                CityData.Instance.ParseFloorData();
            }

            RestartSafeController.Instance.loadFloor = false;
            Load();
        }
    }

    public void StartGame()
    {
        Player.Instance.fps.InitialiseController(true);

        //Toggle fog
        //if (CameraController.Instance.fogController != null) CameraController.Instance.fogController.enabled.value = true;

        //Misc UI Controls
        //TrayController.Instance.Show();

        //Set fps mode
        //CameraController.Instance.EnableFPSMode();

        //Force update of groundmap to update groundmap-based culling systems
        Player.Instance.OnCityTileChange();

        //Force update of position within building etc. Need to do this for proper lighting/culling update
        //Player.Instance.currentBuilding.SetPlayerPresentOnGroundmap(true);
        //Player.Instance.UpdatePlayerPosition();

        //Activate HUD canvas
        InterfaceController.Instance.SetInterfaceActive(true);

        //Started game flag
        SessionData.Instance.startedGame = true;

        //Toggle on play mode bool
        SessionData.Instance.ResumeGame();

        //Execute citizen behaviour
        if(CitizenBehaviour.Instance != null) CitizenBehaviour.Instance.StartGame();

        //Display location text
        InterfaceController.Instance.DisplayLocationText(4f, true);

        //Pause
        CameraController.Instance.SetupFPS();
        SessionData.Instance.OnPauseChange += OnPauseChange;
        SessionData.Instance.PauseGame(true, true);

        //Listen for options
        floorDesignationDropdown.onValueChanged.AddListener(delegate { OnNewFloorDesignationSetting(); });
        addressDropdown.onValueChanged.AddListener(delegate { OnNewAddressDesignationSelection(); });
        roomConfigAddressDropdown.onValueChanged.AddListener(delegate { OnNewAddressDesignationSelection2(); });
        addressTypeDropdown.onValueChanged.AddListener(delegate { OnNewAddressTypeSelection(); });
        wallPairsDropdown.onValueChanged.AddListener(delegate { OnNewWallDesignationSetting(); });
        forceRoomDropdown.onValueChanged.AddListener(delegate { OnNewForceRoomSetting(); });
        roomConfigsDropdown.onValueChanged.AddListener(delegate { OnNewRoomVariationSelection(); });
        roomIDsDropdown.onValueChanged.AddListener(delegate { OnNewRoomSelection(); });
        roomLayoutAssignDropdown.onValueChanged.AddListener(delegate { OnAssignNewRoom(); });

        //Fire game start event
        //if (OnGameStarted != null) OnGameStarted();
    }

    private void Update()
    {
        //Do nothing while saving...
        if (isSaving) return;

        //Perform recalculate all...
        if (RestartSafeController.Instance.recalculateAll)
        {
            //Finish
            if (RestartSafeController.Instance.floorList.Count <= 0)
            {
                recalculationDelay = 0;
                RestartSafeController.Instance.recalculateAll = false;
                Game.Log("Finished recalculating all floors.");
            }
            else
            {
                //Start loading a new floor...
                if (recalculationDelay <= 0 && currentRecalculation.Length <= 0)
                {
                    currentRecalculation = RestartSafeController.Instance.floorList[0];

                    Game.Log("Recalulating " + currentRecalculation + "...");

                    LoadData(CityData.Instance.floorData[currentRecalculation]);

                    //Load geometry
                    GenerationController.Instance.LoadGeometryFloor(editFloor);

                    recalculationDelay = 3;
                }
                //Delay after loading, then save...
                else if (recalculationDelay > 0 && !isSaving)
                {
                    recalculationDelay--;

                    //Save...
                    if (recalculationDelay <= 0)
                    {
                        SaveCurrentData(editFloor);
                    }
                }
                //Finished saving...
                else if (recalculationDelay <= 0 && currentRecalculation.Length > 0)
                {
                    //Remove this entry from the list...
                    RestartSafeController.Instance.floorList.RemoveAt(0);
                    currentRecalculation = string.Empty;

                    //Restart to continue...
                    AudioController.Instance.StopAllSounds();
                    UnityEngine.SceneManagement.SceneManager.LoadScene("FloorEditor");
                }
            }

            return; //Don't do anything while recalulating
        }

        //Set selection cube position
        if (selectionMode)
        {
            //Isn't over UI
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                //Raycast to find base object
                Ray ray = CameraController.Instance.cam.ScreenPointToRay(Input.mousePosition);

                RaycastHit hit;

                int mask = selectionLayerMask;
                if (selectionModeType == EditorSelectionMode.wall) mask = wallsSelectionMask;

                if (Physics.Raycast(ray, out hit, 200f, mask))
                {
                    if (selectionModeType == EditorSelectionMode.tile)
                    {
                        //If tile found in parent
                        EditorFloorTile editorTile = hit.transform.GetComponent<EditorFloorTile>();

                        if (editorTile != null)
                        {
                            //Find actual tile
                            if (editFloor != null)
                            {
                                if (editFloor.tileMap.ContainsKey(editorTile.floorCoord))
                                {
                                    NewTile foundTile = editFloor.tileMap[editorTile.floorCoord];

                                    if (foundTile != tileSelection)
                                    {
                                        SelectNewTile(foundTile);
                                    }
                                }
                            }
                        }
                    }
                    else if (selectionModeType == EditorSelectionMode.wall)
                    {
                        //If wall found in parent
                        if (hit.transform.parent != null && hit.collider.isTrigger)
                        {
                            //Game.Log(hit.transform.parent.name);
                            FloorEditWallDetector w = hit.transform.parent.gameObject.GetComponent<FloorEditWallDetector>();

                            if (w != null)
                            {
                                NewWall foundWall = w.wall;

                                //Is this a new selection
                                if (foundWall != null)
                                {
                                    //Determin the segment of the wall: If this is a segment already then nothing else is needed
                                    if (foundWall != wallSelection)
                                    {
                                        SelectNewWall(foundWall);
                                    }
                                }
                            }
                        }
                    }
                    else if (selectionModeType == EditorSelectionMode.node)
                    {
                        //Game.Log(hit.transform.name);

                        //Convert object world pos to node pos
                        Vector3 zeroPos = hit.point;
                        zeroPos.y = 0;

                        Vector3Int nodeCoord = CityData.Instance.RealPosToNodeInt(zeroPos);

                        NewNode foundNode = null;

                        //Is this a new selection
                        if (PathFinder.Instance.nodeMap.TryGetValue(nodeCoord, out foundNode))
                        {
                            //Determin the segment of the wall: If this is a segment already then nothing else is needed
                            if (foundNode != nodeSelection)
                            {
                                SelectNewNode(foundNode);
                            }
                        }
                    }
                }

                //Click
                if (Input.GetMouseButtonDown(0))
                {
                    if (tool == FloorEditTool.floorDesignation)
                    {
                        //Floor designation
                        if (nodeSelection != null && !nodeSelection.tile.isEdge)
                        {
                            nodeSelection.SetFloorType(floorDesignationTypeSelection);
                        }
                    }
                    else if (tool == FloorEditTool.wallDesignation)
                    {
                        //Address designation
                        if (wallSelection != null)
                        {
                            if (addressSelection != null)
                            {
                                if (wallSelection.node.gameLocation == addressSelection || wallSelection.otherWall.node.gameLocation == addressSelection)
                                {
                                    wallSelection.SetDoorPairPreset(wallPairPresetSelection);
                                    //wallSelection.placedByPlayer = true;
                                    SaveCurrentVariation();
                                }
                                else Game.Log("Cannot set wall: Invalid address selected!");
                            }
                            else Game.Log("Cannot set wall: No address selected!");
                        }
                    }
                    else if (tool == FloorEditTool.rotateFloor)
                    {
                        //Rotate floor
                        if (tileSelection != null && !tileSelection.isEdge)
                        {
                            tileSelection.SetRotation(tileSelection.rotation + 90);
                        }
                    }
                    else if (tool == FloorEditTool.mainEntrance)
                    {
                        //Rotate floor
                        if (tileSelection != null && tileSelection.isEdge)
                        {
                            if (!tileSelection.isEntrance)
                            {
                                tileSelection.SetAsEntrance(true, true, true);
                            }
                            else
                            {
                                tileSelection.SetAsEntrance(false, false, true);
                            }
                        }
                    }
                    else if (tool == FloorEditTool.secondaryEntrance)
                    {
                        //Rotate floor
                        if (tileSelection != null && tileSelection.isEdge)
                        {
                            if (!tileSelection.isEntrance)
                            {
                                tileSelection.SetAsEntrance(true, false, true);
                            }
                            else
                            {
                                tileSelection.SetAsEntrance(false, false, true);
                            }
                        }
                    }
                    else if (tool == FloorEditTool.stairwell || tool == FloorEditTool.elevator)
                    {
                        //Rotate floor
                        if (tileSelection != null && !tileSelection.isEdge)
                        {
                            if (!tileSelection.isStairwell)
                            {
                                bool inverted = false;
                                if (tool == FloorEditTool.elevator) inverted = true;

                                tileSelection.SetAsStairwell(true, true, inverted);
                            }
                            else if (tileSelection.stairwellRotation < 270)
                            {
                                tileSelection.SetStairwellRotation(tileSelection.stairwellRotation + 90);
                            }
                            else
                            {
                                tileSelection.SetAsStairwell(false, true, false);
                                tileSelection.SetStairwellRotation(0);
                            }
                        }
                    }
                    else if (tool == FloorEditTool.forceRoom)
                    {
                        //Floor designation
                        if (nodeSelection != null && !nodeSelection.tile.isEdge)
                        {
                            if(nodeSelection.forcedRoom == null)
                            {
                                nodeSelection.SetForcedRoom(forceRoomSelection);
                                nodeSelection.forcedRoomRef = forceRoomDropdown.options[forceRoomDropdown.value].text;
                            }
                            else
                            {
                                nodeSelection.SetForcedRoom(null);
                                nodeSelection.forcedRoomRef = null;
                            }
                        }
                    }

                    UpdateStatusText();

                    GenerationController.Instance.UpdateGeometryFloor(editFloor);
                    GenerationController.Instance.LoadGeometryFloor(editFloor);
                    SaveCurrentVariation();
                }

                //Click + Drag
                if(Input.GetMouseButton(0))
                {
                    if (tool == FloorEditTool.addressDesignation || tool == FloorEditTool.roomDesignation)
                    {
                        if (!heldDown)
                        {
                            heldDown = true;
                            heldDownOriginNode = nodeSelection;

                            if(heldDownTransform == null)
                            {
                                GameObject newSel = Instantiate(PrefabControls.Instance.heldSelector, this.transform);
                                heldDownTransform = newSel.transform;
                            }
                        }
                    }

                    if (heldDownTransform != null && heldDownOriginNode != null && nodeSelection != null)
                    {
                        heldDownTransform.transform.position = new Vector3((heldDownOriginNode.position.x + nodeSelection.position.x) / 2f, 0.1f, (heldDownOriginNode.position.z + nodeSelection.position.z) / 2f);
                        heldDownTransform.localScale = new Vector3((Mathf.Abs(heldDownOriginNode.floorCoord.x - nodeSelection.floorCoord.x) + 1) * 1.8f, (Mathf.Abs(heldDownOriginNode.floorCoord.y - nodeSelection.floorCoord.y) + 1) * 1.8f, 1f);
                    }
                }
                else if(heldDown)
                {
                    if(tool == FloorEditTool.addressDesignation || tool == FloorEditTool.roomDesignation)
                    {
                        if(heldDownOriginNode != null && nodeSelection != null)
                        {
                            for (int i = Mathf.Min((int)heldDownOriginNode.floorCoord.x, (int)nodeSelection.floorCoord.x); i <= Mathf.Max((int)heldDownOriginNode.floorCoord.x, (int)nodeSelection.floorCoord.x); i++)
                            {
                                for (int u = Mathf.Min((int)heldDownOriginNode.floorCoord.y, (int)nodeSelection.floorCoord.y); u <= Mathf.Max((int)heldDownOriginNode.floorCoord.y, (int)nodeSelection.floorCoord.y); u++)
                                {
                                    Vector2Int coord = new Vector2Int(i, u);

                                    if (editFloor.nodeMap.ContainsKey(coord))
                                    {
                                        if(tool == FloorEditTool.addressDesignation)
                                        {
                                            NewNode thisNode = editFloor.nodeMap[coord];

                                            //Address designation
                                            if (thisNode != null && !thisNode.tile.isEdge)
                                            {
                                                //Clear variations for this address
                                                //if(addressSelection.saveData != null) addressSelection.saveData.vs.Clear();
                                                //addressSelection.loadedVariation = null;

                                                //if(thisNode.gameLocation.thisAsAddress != null && thisNode.gameLocation.thisAsAddress.saveData != null)
                                                //{
                                                //    thisNode.gameLocation.thisAsAddress.saveData.vs.Clear();
                                                //    addressSelection.loadedVariation = null;
                                                //}

                                                addressSelection.AddNewNode(thisNode);
                                            }
                                        }
                                        else if(tool == FloorEditTool.roomDesignation)
                                        {
                                            NewNode n = editFloor.nodeMap[coord];

                                            if (n != null && addressSelection != null && roomSelection != null)
                                            {
                                                if (n.gameLocation == addressSelection)
                                                {
                                                    //Set node to room
                                                    if (roomSelection != n.room)
                                                    {
                                                        roomSelection.AddNewNode(n);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if(tool == FloorEditTool.roomDesignation)
                        {
                            SetTool(FloorEditTool.roomDesignation);
                            SaveCurrentVariation();
                        }
                    }

                    heldDown = false;
                    heldDownOriginNode = null;
                    if(heldDownTransform != null) Destroy(heldDownTransform.gameObject);

                    GenerationController.Instance.UpdateGeometryFloor(editFloor);
                    GenerationController.Instance.LoadGeometryFloor(editFloor);
                }
            }
            else
            {
                if (tileSelection != null) SelectNewTile(null);
                if (wallSelection != null) SelectNewWall(null);
                if (nodeSelection != null) SelectNewNode(null);
            }
        }
        else if(heldDown)
        {
            heldDown = false;
            heldDownOriginNode = null;
            if (heldDownTransform != null) Destroy(heldDownTransform.gameObject);

            GenerationController.Instance.UpdateGeometryFloor(editFloor);
            GenerationController.Instance.LoadGeometryFloor(editFloor);
        }

        if (!SessionData.Instance.play)
        {
            if (Input.GetMouseButton(1))
            {
                rightMouse = true;
                Player.Instance.EnablePlayerMouseLook(true);
            }
            else if(rightMouse)
            {
                rightMouse = false;
                Player.Instance.EnablePlayerMouseLook(false);
            }
        }
    }

    //Update status text
    private void UpdateStatusText()
    {
        try
        {
            if (nodeSelection != null)
            {
                statusText.transform.parent.gameObject.SetActive(true);

                statusText.text = "Coord " + nodeSelection.floorCoord.ToString();
                statusText.text += "\n" + "Address: " + nodeSelection.gameLocation.name;
                statusText.text += "\n" + "Address Type: " + nodeSelection.gameLocation.thisAsAddress.preset.name;
                if (nodeSelection.room != null && nodeSelection.room.preset != null) statusText.text += "\n" + "Room Type: " + nodeSelection.room.roomType.name;
                statusText.text += "\n" + "Floor Type: " + nodeSelection.floorType.ToString();
                statusText.text += "\n" + "Room ID: " + nodeSelection.room.roomID.ToString();

                if (nodeSelection.forcedRoom != null)
                {
                    statusText.text += "\n" + "Forced Room: " + nodeSelection.forcedRoomRef;
                }
                else
                {
                    statusText.text += "\n" + "Forced Room: None";
                }

                statusText.text += "\n" + "Allow new furniture: " + nodeSelection.allowNewFurniture.ToString();
                statusText.text += "\n" + "Access Directions: " + nodeSelection.accessToOtherNodes.Count.ToString();
                statusText.text += "\n" + "Optimized Floor: " + nodeSelection.tile.useOptimizedFloor.ToString();
                statusText.text += "\n" + "Optimized Ceiling: " + nodeSelection.tile.useOptimizedCeiling.ToString();
            }
            else if (tileSelection != null)
            {
                statusText.transform.parent.gameObject.SetActive(true);

                statusText.text = "Coord " + tileSelection.globalTileCoord.ToString();

                if (tileSelection.isEntrance)
                {
                    statusText.text += "\n" + "Entrance: Yes";
                }
                else
                {
                    statusText.text += "\n" + "Entrance: No";
                }

                if (tileSelection.isMainEntrance)
                {
                    statusText.text += "\n" + "Main Entrance: Yes";
                }
                else
                {
                    statusText.text += "\n" + "Main Entrance: No";
                }

                if (tileSelection.isInvertedStairwell)
                {
                    statusText.text += "\n" + "Is Elevator: Yes";
                }
                else
                {
                    statusText.text += "\n" + "Is Elevator: No";
                }

                if (tileSelection.isStairwell)
                {
                    statusText.text += "\n" + "Is Stairwell: Yes";
                }
                else
                {
                    statusText.text += "\n" + "Is Stairwell: No";
                }

                if (tileSelection.useOptimizedCeiling)
                {
                    statusText.text += "\n" + "Optimized Ceiling: Yes";
                }
                else
                {
                    statusText.text += "\n" + "Optimized Ceiling: No";
                }

                if (tileSelection.useOptimizedFloor)
                {
                    statusText.text += "\n" + "Optimized Floor: Yes";
                }
                else
                {
                    statusText.text += "\n" + "Optimized Floor: No";
                }
            }
            else
            {
                statusText.transform.parent.gameObject.SetActive(false);
            }
        }
        catch
        {

        }
    }

    //On new tile selected
    public void SelectNewTile(NewTile newSelect)
    {
        tileSelection = newSelect;

        if(tileSelection != null)
        {
            selectionObject.gameObject.SetActive(true);

            selectionCoord = newSelect.floorCoord;

            //Set position of selection cube
            selectionObject.transform.position = new Vector3(tileSelection.position.x, tileSelection.position.y, tileSelection.position.z);
        }
        else
        {
            selectionObject.gameObject.SetActive(false);
        }

        UpdateStatusText();
    }

    //On new node selected
    public void SelectNewNode(NewNode newSelect)
    {
        nodeSelection = newSelect;

        if (nodeSelection != null)
        {
            selectionObject.gameObject.SetActive(true);

            //Set position of selection cube
            selectionObject.transform.position = nodeSelection.position;
        }
        else
        {
            selectionObject.gameObject.SetActive(false);
        }

        UpdateStatusText();
    }

    //On new wall selected
    public void SelectNewWall(NewWall newSelect)
    {
        wallSelection = newSelect;

        if (wallSelection != null)
        {
            selectionObject.gameObject.SetActive(true);

            //Set position of selection cube
            selectionObject.transform.position = wallSelection.position;
        }
        else
        {
            selectionObject.gameObject.SetActive(false);
        }

        UpdateStatusText();
    }

    //Set the display mode
    public void SetDisplayMode(EditorDisplayMode newMode)
    {
        displayMode = newMode;
        if (editFloor == null) return;

        Game.Log("Update display mode: " + displayMode.ToString());

        //Display address designation
        if(displayMode == EditorDisplayMode.normal)
        {
            //Loop addresses
            foreach (NewAddress address in editFloor.addresses)
            {
                foreach(NewNode node in address.nodes)
                {
                    if (node.spawnedFloor != null)
                    {
                        if (node.room.floorMat != null)
                        {
                            MaterialsController.Instance.SetMaterialGroup(node.spawnedFloor, node.room.floorMaterial, node.room.floorMatKey); //Restore floor material
                        }
                        else
                        {
                            MaterialsController.Instance.SetMaterialGroup(node.spawnedFloor, defaultFloorMaterial, defaultMaterialKey); //Restore floor material
                        }
                    }
                }
            }
        }
        else if(displayMode == EditorDisplayMode.displayAddressDesignation)
        {
            //Loop addresses
            foreach(NewAddress address in editFloor.addresses)
            {
                foreach(NewNode node in address.nodes)
                {
                    if (node.spawnedFloor != null)
                    {
                        node.spawnedFloor.GetComponent<MeshRenderer>().material = adddressDesignationMaterial;
                        node.spawnedFloor.GetComponent<MeshRenderer>().material.SetColor("_UnlitColor", address.editorColour); //set the material equal to the clone
                    }
                }
            }
        }
        else if(displayMode == EditorDisplayMode.displayRoomSelection)
        {
            //Loop addresses
            foreach (NewAddress address in editFloor.addresses)
            {
                foreach (NewNode node in address.nodes)
                {
                    if (node.spawnedFloor != null)
                    {
                        if (addressSelection != null && roomSelection != null && roomSelection == node.room)
                        {
                            node.spawnedFloor.GetComponent<MeshRenderer>().material = adddressDesignationMaterial;
                            node.spawnedFloor.GetComponent<MeshRenderer>().material.SetColor("_UnlitColor", address.editorColour); //set the material equal to the clone
                        }
                        else if(node.room.floorMat != null)
                        {
                            MaterialsController.Instance.SetMaterialGroup(node.spawnedFloor, node.room.floorMaterial, node.room.floorMatKey); //Restore floor material
                        }
                        else
                        {
                            MaterialsController.Instance.SetMaterialGroup(node.spawnedFloor, defaultFloorMaterial, defaultMaterialKey); //Restore floor material
                        }
                    }
                }
            }
        }
    }

    //Set the selection mode
    public void SetSelectionMode(EditorSelectionMode newMode)
    {
        selectionModeType = newMode;

        if(selectionModeType == EditorSelectionMode.tile)
        {
            floorSelectCursorObject.gameObject.SetActive(true);
            wallSelectCursorObject.gameObject.SetActive(false);
        }
        else if(selectionModeType == EditorSelectionMode.wall)
        {
            floorSelectCursorObject.gameObject.SetActive(false);
            wallSelectCursorObject.gameObject.SetActive(true);
        }
        else if (selectionModeType == EditorSelectionMode.node)
        {
            floorSelectCursorObject.gameObject.SetActive(false);
            wallSelectCursorObject.gameObject.SetActive(true);
        }
    }

    //Pause change
    public void OnPauseChange(bool openDesktopMode)
    {
        Game.Log("OnPauseChange " + !SessionData.Instance.play);

        //Set first person or camera mode
        if (SessionData.Instance.play)
        {
            Player.Instance.EnableGhostMovement(false);

            foreach(GameObject obj in wallTriggers)
            {
                obj.SetActive(false);
            }

            //Position player == camera
            Player.Instance.transform.position = new Vector3(CameraController.Instance.transform.position.x, 2f, CameraController.Instance.transform.position.z);

            EnableSelectionMode(false);
            enabledInScrollView.SetActive(false);
            Player.Instance.EnablePlayerMouseLook(true);

            //Make ceiling visible
            foreach (KeyValuePair<int, NewFloor> pair in building.floors)
            {
                pair.Value.outsideAddress.nullRoom.SetVisible(false, true, "Floor Edit");

                foreach (NewRoom room in pair.Value.outsideAddress.rooms)
                {
                    room.SetVisible(false, true, "Floor Edit");
                }

                foreach (NewAddress address in pair.Value.addresses)
                {
                    address.nullRoom.SetVisible(false, true, "Floor Edit"); //Set null room to hidden

                    //Cull all rooms: The player controller should trigger a culling update to display appropriate ones...
                    foreach (NewRoom room in address.rooms)
                    {
                        room.SetVisible(false, true, "Floor Edit");
                    }

                    foreach(NewNode node in address.nodes)
                    {
                        if(node.spawnedCeiling != null)
                        {
                            node.spawnedCeiling.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                        }
                    }
                }
            }
        }
        else
        {
            Player.Instance.EnableGhostMovement(true);

            //Display all rooms
            foreach (KeyValuePair<int, NewFloor> pair in building.floors)
            {
                foreach (NewAddress address in pair.Value.addresses)
                {
                    //Cull all rooms: The player controller should trigger a culling update to display appropriate ones...
                    foreach (NewRoom room in address.rooms)
                    {
                        room.SetVisible(true, true, "Floor Edit");
                    }
                }
            }

            foreach (GameObject obj in wallTriggers)
            {
                obj.SetActive(true);
            }

            EnableSelectionMode(true);
            enabledInScrollView.SetActive(true);
            Player.Instance.EnablePlayerMouseLook(false);

            //Make ceiling visible
            foreach (KeyValuePair<int, NewFloor> pair in building.floors)
            {
                foreach (NewAddress address in pair.Value.addresses)
                {
                    foreach (NewNode node in address.nodes)
                    {
                        if (node.spawnedCeiling != null)
                        {
                            node.spawnedCeiling.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;
                        }
                    }
                }
            }
        }
    }

    //Set the tool
    public void SetTool(int newTool)
    {
        SetTool((FloorEditTool)newTool);
    }

    public void SetTool(FloorEditTool newTool, bool forceRefresh = false)
    {
        if (newTool != tool || forceRefresh)
        {
            if(tool == FloorEditTool.roomDesignation && tool != FloorEditTool.roomDesignation)
            {
                SaveCurrentVariation();
            }

            tool = newTool;

            //Set display mode
            SetDisplayMode(EditorDisplayMode.normal);

            //Set selection mode
            SetSelectionMode(EditorSelectionMode.tile);

            toolOptionsWindow.gameObject.SetActive(false);
            floorDesignationOptions.gameObject.SetActive(false);
            roomDesignationOptions.gameObject.SetActive(false);
            addressDesignationOptions.gameObject.SetActive(false);
            wallPairsOptions.gameObject.SetActive(false);
            forceRoomOptions.gameObject.SetActive(false);

            if (tool == FloorEditTool.floorDesignation)
            {
                SetSelectionMode(EditorSelectionMode.node);

                //Make parents active
                toolOptionsWindow.gameObject.SetActive(true);
                floorDesignationOptions.gameObject.SetActive(true);

                //Setup options
                floorDesignationDropdown.ClearOptions();

                List<NewNode.FloorTileType> allOptions = NewNode.FloorTileType.GetValues(typeof(NewNode.FloorTileType)).Cast<NewNode.FloorTileType>().ToList();
                List<string> stringOptions = new List<string>();
                int selectedOption = 0;

                for (int i = 0; i < allOptions.Count; i++)
                {
                    stringOptions.Add(allOptions[i].ToString());

                    if (floorDesignationTypeSelection == allOptions[i])
                    {
                        selectedOption = i;
                    }
                }

                floorDesignationDropdown.AddOptions(stringOptions);

                //Select currently selected
                floorDesignationDropdown.value = selectedOption;
            }
            else if(tool == FloorEditTool.roomDesignation)
            {
                SetSelectionMode(EditorSelectionMode.node);
                SetDisplayMode(EditorDisplayMode.displayRoomSelection);

                //Make parents active
                toolOptionsWindow.gameObject.SetActive(true);
                roomDesignationOptions.gameObject.SetActive(true);

                //Setup options
                UpdateAddressDropdowns();
                UpdateRoomConfigsDropdown();
                UpdateRoomDesignationIDsDropdown();
            }
            else if (tool == FloorEditTool.addressDesignation)
            {
                SetSelectionMode(EditorSelectionMode.node);

                //Set display mode
                SetDisplayMode(EditorDisplayMode.displayAddressDesignation);

                //Make parents active
                toolOptionsWindow.gameObject.SetActive(true);
                addressDesignationOptions.gameObject.SetActive(true);

                //Setup options
                UpdateAddressDropdowns();
            }
            else if (tool == FloorEditTool.wallDesignation)
            {
                //Make parents active
                toolOptionsWindow.gameObject.SetActive(true);
                wallPairsOptions.gameObject.SetActive(true);

                //Set selection mode
                SetSelectionMode(EditorSelectionMode.wall);

                //Select currently selected
                wallPairsDropdown.value = selectableDoorPairs.IndexOf(wallPairPresetSelection);
            }
            else if(tool == FloorEditTool.rotateFloor)
            {
                
            }
            else if (tool == FloorEditTool.forceRoom)
            {
                SetSelectionMode(EditorSelectionMode.node);

                //Set display mode
                SetDisplayMode(EditorDisplayMode.displayAddressDesignation);

                //Make parents active
                toolOptionsWindow.gameObject.SetActive(true);
                forceRoomOptions.gameObject.SetActive(true);
            }
        }
    }

    //Toolbar buttons
    //Create a new floor button
    public void NewFloorButton()
    {
        if (!newFloorWindow.gameObject.activeSelf)
        {
            newFloorWindow.gameObject.SetActive(true);
        }
        else
        {
            newFloorWindow.gameObject.SetActive(false);
        }
    }

    //Save the current data
    public void SaveFloorButton()
    {
        if(editFloor != null) SaveCurrentData(editFloor);
    }

    //Save the current data as a copy
    public void SaveAsFloorButton()
    {
        if (!saveAsFloorWindow.gameObject.activeSelf)
        {
            saveAsFloorWindow.gameObject.SetActive(true);
        }
        else
        {
            saveAsFloorWindow.gameObject.SetActive(false);
        }
    }

    //Enable/disable selection mode
    public void EnableSelectionMode(bool val)
    {
        selectionObject.gameObject.SetActive(val); //Enable/disable selection cube

        //Only do this is different
        if (selectionMode != val)
        {
            selectionMode = val;
            selectionCoord = new Vector2(-1, -1);
            tileSelection = null;

            if (selectionMode)
            {

            }
            else
            {

            }
        }
    }

    //Load a floor
    public void LoadFloorButton()
    {
        if (!loadFloorWindow.gameObject.activeSelf)
        {
            loadFloorWindow.gameObject.SetActive(true);

            //Fill dropdown with list of existing data
            loadDropdown.ClearOptions();
            loadFilePaths.Clear();

            //All files should be loaded by CityData
            foreach(KeyValuePair<string, FloorSaveData> pair in CityData.Instance.floorData)
            {
                loadFilePaths.Add(pair.Key);
            }

            loadDropdown.AddOptions(loadFilePaths);
        }
        else
        {
            loadFloorWindow.gameObject.SetActive(false);
        }
    }

    //Restart before creating a new floor
    public void CreateNewFloorTrigger()
    {
        RestartSafeController.Instance.newFloor = true;

        RestartSafeController.Instance.newFloorName = newFloorName.text;
        RestartSafeController.Instance.newFloorSize = new Vector2(int.Parse(newFloorSizeX.text), int.Parse(newFloorSizeY.text));
        RestartSafeController.Instance.newFloorFloorHeight = int.Parse(newFloorFloorHeight.text);
        RestartSafeController.Instance.newFloorCeilingHeight = int.Parse(newFloorCeilingHeight.text);

        AudioController.Instance.StopAllSounds();
        UnityEngine.SceneManagement.SceneManager.LoadScene("FloorEditor");
    }

    //Functions
    public void CreateNewFloor()
    {
        Game.Log("CityGen: Creating a new floor following restart...");

        //Reset IDs
        NewAddress.assignEditorID = 1;

        GameObject newFloor = Instantiate(PrefabControls.Instance.floor, this.transform);
        editFloor = newFloor.GetComponent<NewFloor>();
        editFloor.Setup(0, building, RestartSafeController.Instance.newFloorName, RestartSafeController.Instance.newFloorSize, RestartSafeController.Instance.newFloorFloorHeight, RestartSafeController.Instance.newFloorCeilingHeight);

        //Toggle window
        //NewFloorButton();

        //Load to world
        LoadEditorFloorToWorld();

        //Load geometry
        GenerationController.Instance.LoadGeometryFloor(editFloor);
    }

    public void SaveAs()
    {
        if (editFloor == null) return;
        editFloor.floorName = newSaveAsFloorName.text;
        SaveCurrentData(editFloor);

        //Toggle window
        SaveAsFloorButton();
    }

    public void LoadTrigger()
    {
        RestartSafeController.Instance.loadFloor = true;
        RestartSafeController.Instance.loadFloorString = loadDropdown.options[loadDropdown.value].text;

        AudioController.Instance.StopAllSounds();
        UnityEngine.SceneManagement.SceneManager.LoadScene("FloorEditor");
    }

    public void Load()
    {
        LoadData(CityData.Instance.floorData[RestartSafeController.Instance.loadFloorString]);

        //Toggle window
        //LoadFloorButton();

        //Load to world
        //LoadFloorToWorld();

        //Load geometry
        GenerationController.Instance.LoadGeometryFloor(editFloor);
    }

    public void RecalculateAllTrigger()
    {
        RestartSafeController.Instance.recalculateAll = true;
        RestartSafeController.Instance.floorList.AddRange(CityData.Instance.floorData.Keys);
    }

    //Save the current data to file
    public void SaveCurrentData(NewFloor data)
    {
        if (data == null) return;

        isSaving = true;

        Game.Log("Saving floor " + data.floorName + "...");

        //Get a save data class file
        data.OnSaveDataComplete += OnCompleteSaveData;
        data.GetSaveData();
    }

    public void OnCompleteSaveData(NewFloor floor, FloorSaveData newSaveData)
    {
        floor.OnSaveDataComplete -= OnCompleteSaveData;

        string path = Path.Combine(Application.dataPath + "/AddressableAssets/FloorData/", floor.floorName + ".csv");
        string jsonString = JsonUtility.ToJson(newSaveData);

        using (StreamWriter streamWriter = File.CreateText(path))
        {
            streamWriter.Write(jsonString);
        }

        //Update the copy of the data loaded into CityData
        if (!CityData.Instance.floorData.ContainsKey(floor.floorName))
        {
            CityData.Instance.floorData.Add(floor.floorName, null);
        }

        CityData.Instance.floorData[floor.floorName] = newSaveData;

        Game.Log("Completed saving " + floor.floorName);

        CityData.Instance.ParseFloorData(); //Reload floor data from files

        isSaving = false;
    }

    //Load a data file to editor
    //Effectively this creates a new floor and overwrites the saved variables into it.
    public void LoadData(FloorSaveData savedData)
    {
        if (savedData == null) return;
        loaded = false;

        //Reset pathmaps
        PathFinder.Instance.nodeMap.Clear();
        PathFinder.Instance.tileMap.Clear();

        //Reset IDs
        NewAddress.assignEditorID = 1;

        //Remove existing floor
        if(editFloor != null && editFloor.building.floors.ContainsKey(editFloor.floor))
        {
            editFloor.building.floors.Remove(editFloor.floor);
            Destroy(editFloor.gameObject);
        }

        //Create a new floor
        GameObject newFloor = Instantiate(PrefabControls.Instance.floor, this.transform);
        editFloor = newFloor.GetComponent<NewFloor>();
        editFloor.Setup(0, building, savedData.floorName, savedData.size, savedData.defaultFloorHeight, savedData.defaultCeilingHeight);

        LoadEditorFloorToWorld(); //Create editor ground tiles
        editFloor.LoadDataToFloor(savedData); //Load data to this floor

        //Refresh wall data
        GenerationController.Instance.UpdateGeometryFloor(editFloor);

        //Load geometry
        GenerationController.Instance.LoadGeometryFloor(editFloor);

        //Calculate culling: Editor only- game has it's own routines
        if(SessionData.Instance.isFloorEdit)
        {
            editFloor.ConnectNodesOnFloor();
        }

        loaded = true;
    }

    //Load from the current data to world
    public void LoadEditorFloorToWorld()
    {
        if (editFloor == null) return;

        //Load floor area
        //Point of origin (0, 0) is always bottom left
        for (int i = 0; i < editFloor.size.x; i++)
        {
            for (int u = 0; u < editFloor.size.y; u++)
            {
                //Load tile size == path map multiplier
                for (int l = 0; l < CityControls.Instance.tileMultiplier; l++)
                {
                    for (int k = 0; k < CityControls.Instance.tileMultiplier; k++)
                    {
                        Vector3Int loc = new Vector3Int((i * CityControls.Instance.tileMultiplier) + l, (u * CityControls.Instance.tileMultiplier) + k, 0);

                        //Spawn tile
                        GameObject newTile = Instantiate(PrefabControls.Instance.editorTile, editorFloorParent);
                        newTile.transform.position = CityData.Instance.TileToRealpos(loc);
                        newTile.transform.position = new Vector3(newTile.transform.position.x, -0.1f, newTile.transform.position.z);
                        newTile.layer = 29;

                        //Detect edge tile
                        bool edgeTile = false;

                        if (loc.x <= 0 || loc.y <= 0)
                        {
                            edgeTile = true;
                            newTile.GetComponent<MeshRenderer>().material = editorFloorEdgeMaterial;
                        }
                        else if (loc.x >= editFloor.size.x * CityControls.Instance.tileMultiplier - 1 || loc.y >= editFloor.size.y * CityControls.Instance.tileMultiplier - 1)
                        {
                            edgeTile = true;
                            newTile.GetComponent<MeshRenderer>().material = editorFloorEdgeMaterial;
                        }

                        EditorFloorTile editorTile = newTile.GetComponent<EditorFloorTile>();
                        editorTile.floorCoord = new Vector2Int(loc.x, loc.y);
                        editorTile.edgeTile = edgeTile;
                    }
                }
            }
        }
    }

    //On Pause
    public void OnPause()
    {

    }

    public void OnPlay()
    {

    }

    //Tool settings
    public void OnNewFloorDesignationSetting()
    {
        //Get enum from int value
        floorDesignationTypeSelection = (NewNode.FloorTileType)floorDesignationDropdown.value;
    }

    //A new address is selected
    public void OnNewAddressDesignationSelection()
    {
        //Update currently selected address
        addressSelection = editFloor.addresses[addressDropdown.value];
        roomConfigAddressDropdown.SetValueWithoutNotify(addressDropdown.value);
        if (addressSelection != null) Game.Log("New address selected:" + addressSelection.name);

        //Update current address type
        addressTypeSelection = addressSelection.preset;

        //Update the address type to match current
        addressTypeDropdown.value = allLayouts.FindIndex(item => item == addressTypeSelection);

        //Update colour shown
        addressDesignationColourImage.color = addressSelection.editorColour;
        addressDesignationColourImage2.color = addressSelection.editorColour;

        UpdateRoomConfigsDropdown();
        OnNewRoomVariationSelection();
    }

    public void OnNewAddressDesignationSelection2()
    {
        //Update currently selected address
        addressSelection = editFloor.addresses[roomConfigAddressDropdown.value];
        addressDropdown.SetValueWithoutNotify(roomConfigAddressDropdown.value);
        if (addressSelection != null) Game.Log("New address selected:" + addressSelection.name);

        //Update current address type
        addressTypeSelection = addressSelection.preset;

        //Update the address type to match current
        addressTypeDropdown.value = allLayouts.FindIndex(item => item == addressTypeSelection);

        //Update colour shown
        addressDesignationColourImage.color = addressSelection.editorColour;
        addressDesignationColourImage2.color = addressSelection.editorColour;

        UpdateRoomConfigsDropdown();
        OnNewRoomVariationSelection();
    }

    //A new address tpye is selected
    public void OnNewAddressTypeSelection()
    {
        //Set the currently selected address to this type
        addressSelection.SetAddressType(allLayouts[addressTypeDropdown.value]);

        if(tool == FloorEditTool.addressDesignation) SetTool(FloorEditTool.addressDesignation, true); //Force refresh to update names
        else if (tool == FloorEditTool.roomDesignation) SetTool(FloorEditTool.roomDesignation, true); //Force refresh to update names
    }

    public void AddNewAddressButton()
    {
        editFloor.CreateNewAddress(addressTypeSelection, CityControls.Instance.defaultStyle);
    }

    public void RemoveAddress()
    {
        //Set all nodes in this address to default address
        for (int i = 0; i < editFloor.addresses[addressDropdown.value].nodes.Count; i++)
        {
            editFloor.lobbyAddress.AddNewNode(editFloor.addresses[addressDropdown.value].nodes[i]);
            i--;
        }

        //Remove the address object completely
        Destroy(editFloor.addresses[addressDropdown.value].gameObject);

        //Once the nodes are removed, remove the address entry
        editFloor.addresses.RemoveAt(addressDropdown.value);

        //Force refresh
        SetTool(FloorEditTool.addressDesignation, true);
    }

    //A new wall pair is selected
    public void OnNewWallDesignationSetting()
    {
        //Get enum from int value
        wallPairPresetSelection = selectableDoorPairs[wallPairsDropdown.value];
    }

    //A new force room is selected
    public void OnNewForceRoomSetting()
    {
        //Get enum from int value
        //Find the address config...
        string[] layoutNames = forceRoomDropdown.options[forceRoomDropdown.value].text.Split('.');

        string lastElement = layoutNames[layoutNames.Length - 1];

        forceRoomSelection = allRoomConfigs.Find(item => item.name == lastElement);

        Game.Log("Set force room selection to " + forceRoomDropdown.options[forceRoomDropdown.value].text);
    }

    public void GenerateAddressLayoutButton()
    {
        CityData.Instance.seed = Toolbox.Instance.GenerateSeed(16);
        GenerationController.Instance.GenerateAddressLayout(addressSelection);
        GenerationController.Instance.LoadGeometryFloor(editFloor);
        editFloor.ConnectNodesOnFloor(); //Connect nodes and generate cull trees
    }

    public void GenerateAddressDecorButton()
    {
        addressSelection.CalculateLandValue();

        //if (addressSelection.isLobby)
        //{
        //    if (ad.building.preset != null && addressSelection.building.preset.lobbyPreset != null)
        //    {
        //        addressSelection.addressPreset = addressSelection.building.preset.lobbyPreset;
        //    }
        //    else addressSelection.addressPreset = InteriorControls.Instance.lobbyAddressPreset;
        //}

        //Assign an appropriate purpose & setup configs
        addressSelection.AssignPurpose();

        CityData.Instance.seed = Toolbox.Instance.GenerateSeed(16);
        GenerationController.Instance.GenerateGeometry(addressSelection);
        GenerationController.Instance.GenerateAddressDecor(addressSelection);
        GenerationController.Instance.LoadGeometryFloor(editFloor);
    }

    public void GenerateAddressLayoutAll()
    {
        foreach(NewAddress ad in editFloor.addresses)
        {
            if (ad.preset.roomLayout.Count <= 0) continue;
            CityData.Instance.seed = Toolbox.Instance.GenerateSeed(16);

            //Game.Log("Floor Edit: Generate layout for " + ad.name);

            GenerationController.Instance.GenerateAddressLayout(ad);
        }

        GenerationController.Instance.LoadGeometryFloor(editFloor);
        editFloor.ConnectNodesOnFloor(); //Connect nodes and generate cull trees
    }

    public void GenerateAddressDecorAll()
    {
        //Reset decorated flag
        if (SessionData.Instance.isFloorEdit)
        {
            foreach (NewAddress ad in editFloor.addresses)
            {
                foreach (NewRoom room in ad.rooms)
                {
                    room.hasBeenDecorated = false;
                }
            }
        }

        foreach (NewAddress ad in editFloor.addresses)
        {
            if (ad.preset.roomLayout.Count <= 0) continue;

            ad.CalculateLandValue();

            //if (ad.isLobby)
            //{
            //    if (ad.building.preset != null && ad.building.preset.lobbyPreset != null)
            //    {
            //        ad.addressPreset = ad.building.preset.lobbyPreset;
            //    }
            //    else ad.addressPreset = InteriorControls.Instance.lobbyAddressPreset;
            //}

            //Assign an appropriate purpose & generate configs
            ad.AssignPurpose();

            CityData.Instance.seed = Toolbox.Instance.GenerateSeed(16);
            GenerationController.Instance.GenerateGeometry(ad);
            GenerationController.Instance.GenerateAddressDecor(ad);
        }

        GenerationController.Instance.LoadGeometryFloor(editFloor);
    }

    //Remove all forced rooms
    public void RemoveAllForcedRooms()
    {
        foreach(NewAddress ad in editFloor.addresses)
        {
            foreach (NewRoom room in ad.rooms)
            {
                foreach (NewNode node in room.nodes)
                {
                    node.SetForcedRoom(null);
                }
            }
        }
    }

    [Button]
    public void ResetAllEntrances()
    {
        foreach(KeyValuePair<Vector2Int, NewTile> pair in editFloor.tileMap)
        {
            pair.Value.SetAsEntrance(false, false, true);
        }
    }

    public void UpdateAddressDropdowns()
    {
        //Setup options
        addressDropdown.ClearOptions();
        roomConfigAddressDropdown.ClearOptions();

        List<string> stringOptions = new List<string>();
        int selectedOption = 0;

        if(editFloor != null)
        {
            for (int i = 0; i < editFloor.addresses.Count; i++)
            {
                stringOptions.Add(editFloor.addresses[i].name);

                if (addressSelection == editFloor.addresses[i])
                {
                    selectedOption = i;
                }
            }
        }

        addressDropdown.AddOptions(stringOptions);
        roomConfigAddressDropdown.AddOptions(stringOptions);

        //Select currently selected
        addressDropdown.value = selectedOption;
        roomConfigAddressDropdown.value = selectedOption;

        //Force update
        OnNewAddressDesignationSelection();
        UpdateRoomConfigsDropdown();
    }

    public void UpdateRoomConfigsDropdown()
    {
        //Room configs; saved configurations of rooms inside this address
        roomConfigsDropdown.ClearOptions();

        if(editFloor != null && addressSelection != null && addressSelection.saveData != null)
        {
            List<string> stringOptions = new List<string>();
            int current = 0;

            for (int i = 0; i < addressSelection.saveData.vs.Count; i++)
            {
                stringOptions.Add(i.ToString());
                if (i == addressSelection.loadedVarIndex) current = i;
            }

            roomConfigsDropdown.AddOptions(stringOptions);
            roomConfigsDropdown.value = current;
        }
    }

    //Triggered when a new variation is selected via the dropdown
    public void OnNewRoomVariationSelection()
    {
        if (editFloor != null && addressSelection != null && addressSelection.saveData != null)
        {
            if(roomConfigsDropdown.value >= 0 && roomConfigsDropdown.value < addressSelection.saveData.vs.Count)
            {
                SaveCurrentVariation();
                Game.Log("Load variation #" + roomConfigsDropdown.value + " for " + addressSelection.name);

                //Load the selected variation
                GenerationController.Instance.ResetLayout(addressSelection, out _);

                editFloor.LoadVariation(addressSelection, addressSelection.saveData.vs[roomConfigsDropdown.value]);
                editFloor.FinalizeLoadingIn();

                GenerationController.Instance.LoadGeometryFloor(editFloor);
                editFloor.ConnectNodesOnFloor(); //Connect nodes and generate cull trees

                //Refresh walls immediately before doing the following...
                //GenerationController.Instance.UpdateFloorCeilingFloor(editFloor);
                //GenerationController.Instance.UpdateWallsFloor(editFloor);

                //Refresh the room wall data
                //GenerationController.Instance.UpdateGeometryFloor(editFloor);

                UpdateRoomDesignationIDsDropdown();
            }
        }
    }

    //Don't think these are needed (delete if not)
    ////Save whenever the current variation is edited
    //public void SaveRoomVariation()
    //{
    //    if (editFloor != null && addressSelection != null && addressSelection.saveData != null)
    //    {
    //        Game.Log("Save current room variation to address save data reference");
    //    }
    //}

    ////Save the address along with configurations to file
    //public void SaveAddressRoomConfigs()
    //{
    //    if (editFloor != null && addressSelection != null && addressSelection.saveData != null)
    //    {
    //        Game.Log("Save address room configs to file");
    //    }
    //}

    public void UpdateRoomDesignationIDsDropdown()
    {
        //Room IDs; the different rooms inside this address
        roomIDsDropdown.ClearOptions();

        if (editFloor != null && addressSelection != null && addressSelection.saveData != null)
        {
            List<string> stringOptions = new List<string>();
            int current = 0;

            for (int i = 0; i < addressSelection.rooms.Count; i++)
            {
                stringOptions.Add(addressSelection.rooms[i].roomID + ": " + addressSelection.rooms[i].roomType.name);

                if(roomSelection != null && roomSelection == addressSelection.rooms[i])
                {
                    current = i;
                }
            }

            roomIDsDropdown.AddOptions(stringOptions);
            roomIDsDropdown.value = current;
        }
    }

    public void OnNewRoomSelection()
    {
        if (editFloor != null && addressSelection != null && addressSelection.saveData != null)
        {
            int getID = -1;

            TMP_Dropdown.OptionData strOpt = roomIDsDropdown.options[roomIDsDropdown.value];
            string[] sp = strOpt.text.Split(':');

            if(sp.Length > 0)
            {
                int.TryParse(sp[0], out getID);
            }

            roomSelection = addressSelection.rooms.Find(item => item.roomID == getID);
            if (roomSelection != null) Game.Log("New room selected: " + roomSelection.name + " (" + roomSelection.roomID + ")");

            SetDisplayMode(EditorDisplayMode.displayRoomSelection);

            UpdateRoomLayoutAssignDropdown();
        }
    }

    public void UpdateRoomLayoutAssignDropdown()
    {
        //Load room types
        roomLayoutAssignDropdown.ClearOptions();

        if(editFloor != null && addressSelection != null && roomSelection != null)
        {
            List<string> stringOptions = new List<string>();
            stringOptions.Add(nullRoomType.name); //Add null

            int cursor = 1;
            int current = -1;

            if (roomSelection.roomType == nullRoomType)
            {
                current = 0;
            }

            for (int i = 0; i < addressSelection.preset.roomLayout.Count; i++)
            {
                if(!stringOptions.Contains(addressSelection.preset.roomLayout[i].name.ToString()))
                {
                    stringOptions.Add(addressSelection.preset.roomLayout[i].name.ToString());

                    if (roomSelection.roomType == addressSelection.preset.roomLayout[i])
                    {
                        Game.Log("Detected current room: " + roomSelection.roomType.ToString() + " (" + cursor + ")");
                        current = cursor;
                    }

                    cursor++;
                }
            }

            roomLayoutAssignDropdown.AddOptions(stringOptions);
            if(current > -1) roomLayoutAssignDropdown.value = current;
        }
    }

    public void OnAssignNewRoom()
    {
        if (editFloor != null && addressSelection != null && roomSelection != null)
        {
            RoomTypePreset typePreset = allLayoutTypes.Find(item => item.name == roomLayoutAssignDropdown.options[roomLayoutAssignDropdown.value].text);

            if(typePreset != null)
            {
                if(roomSelection.roomType != typePreset)
                {
                    Game.Log("Set new type " + typePreset.name + " for room " + roomSelection.roomID);
                    roomSelection.SetType(typePreset);
                    SaveCurrentVariation();

                    UpdateRoomDesignationIDsDropdown();
                }
            }
        }
    }

    public void SaveCurrentVariation()
    {
        if(addressSelection != null && GetLoadedVariation(addressSelection) != null)
        {
            SaveLoadedAddressVariation(addressSelection);
        }
    }

    public void SaveLoadedAddressVariation(NewAddress add)
    {
        if (add != null && loaded && GetLoadedVariation(add) != null)
        {
            GetLoadedVariation(add).r_d = new List<RoomSaveData>();

            int nonDefaultWalls = 0;

            //Create room data
            foreach (NewRoom room in add.rooms)
            {
                RoomSaveData newRoomData = new RoomSaveData();
                newRoomData.id = room.roomFloorID;
                newRoomData.l = room.roomType.name;

                //Create node data
                foreach (NewNode node in room.nodes)
                {
                    NodeSaveData newNodeData = new NodeSaveData();
                    newNodeData.f_c = node.floorCoord;
                    //newNodeData.c_h = node.ceilingHeight;
                    newNodeData.f_h = node.floorHeight;
                    newNodeData.f_t = node.floorType;

                    if (node.forcedRoom != null)
                    {
                        newNodeData.f_r = node.forcedRoomRef;
                    }

                    //Create wall data
                    foreach (NewWall wall in node.walls)
                    {
                        WallSaveData newWallData = new WallSaveData();
                        newWallData.w_o = wall.wallOffset;
                        newWallData.p_n = wall.preset.id;
                        if (wall.preset.id != "0") nonDefaultWalls++;

                        newNodeData.w_d.Add(newWallData);
                    }

                    //Add to base class container
                    newRoomData.n_d.Add(newNodeData);
                }

                GetLoadedVariation(add).r_d.Add(newRoomData);
            }

            Game.Log("Saving the current variation of " + add.name + " (Index " + add.loadedVarIndex + ") Non default walls: " + nonDefaultWalls);
        }
    }

    public void AddVariationConfiguration()
    {
        //Add a new blank variation and select it...
        if(addressSelection != null && addressSelection.saveData != null)
        {
            Game.Log("Creating new duplicated address variation...");

            //Create new variation
            AddressLayoutVariation newVariation = new AddressLayoutVariation();

            //If no existing, generate new layout
            if(addressSelection.saveData.vs.Count <= 0)
            {
                CityData.Instance.seed = Toolbox.Instance.GenerateSeed(16);
                GenerationController.Instance.GenerateAddressLayout(addressSelection);
                GenerationController.Instance.LoadGeometryFloor(editFloor);
                editFloor.ConnectNodesOnFloor(); //Connect nodes and generate cull trees
            }

            //Create room data
            foreach (NewRoom room in addressSelection.rooms)
            {
                RoomSaveData newRoomData = new RoomSaveData();
                newRoomData.id = room.roomFloorID;
                newRoomData.l = room.roomType.name;

                //Create node data
                foreach (NewNode node in room.nodes)
                {
                    NodeSaveData newNodeData = new NodeSaveData();
                    newNodeData.f_c = node.floorCoord;
                    //newNodeData.c_h = node.ceilingHeight;
                    newNodeData.f_h = node.floorHeight;
                    newNodeData.f_t = node.floorType;

                    if (node.forcedRoom != null)
                    {
                        newNodeData.f_r = node.forcedRoomRef;
                    }

                    //Create wall data
                    foreach (NewWall wall in node.walls)
                    {
                        WallSaveData newWallData = new WallSaveData();
                        newWallData.w_o = wall.wallOffset;
                        newWallData.p_n = wall.preset.id;

                        newNodeData.w_d.Add(newWallData);
                    }

                    //Add to base class container
                    newRoomData.n_d.Add(newNodeData);
                }

                newVariation.r_d.Add(newRoomData);
            }

            addressSelection.saveData.vs.Add(newVariation);
            UpdateRoomConfigsDropdown();

            //Select new
            Game.Log("Selecting new duplicated address variation...");
            roomConfigsDropdown.value = addressSelection.saveData.vs.Count - 1;

            SaveCurrentVariation();
        }
    }

    public void RemoveVariationConfiguration()
    {
        //Remove the current variation and default to the first...
        if(addressSelection != null && addressSelection.saveData != null)
        {
            if(addressSelection.saveData.vs.Contains(GetLoadedVariation(addressSelection)))
            {
                Game.Log("Removing variation from address data...");
                addressSelection.saveData.vs.Remove(GetLoadedVariation(addressSelection));
                
                UpdateRoomConfigsDropdown(); //This should force select another variation

                SaveCurrentVariation();
            }
            else
            {
                Game.Log("Unable to find the selected variation in data!");
            }
        }
    }

    public void AddRoom()
    {
        //Add a new room to the selected variation...
        if (addressSelection != null)
        {
            //Create room
            GameObject newObj = Instantiate(PrefabControls.Instance.room, addressSelection.transform);
            NewRoom newRoom = newObj.GetComponent<NewRoom>();
            newRoom.SetupLayoutOnly(addressSelection, nullRoomType);

            addressSelection.AddNewRoom(newRoom);

            UpdateRoomDesignationIDsDropdown(); //The update uses class data, so we need to save it first
            SaveCurrentVariation();
        }
    }

    public void RemoveRoom()
    {
        //Remove a room from the selected variation
        if (roomSelection != null && addressSelection != null)
        {
            //Assign these nodes to the null room at this addresss...
            if (addressSelection.nullRoom != null)
            {
                Game.Log("Removing room " + roomSelection.roomID + " from " + addressSelection.name + "...");
                List<NewNode> nodesToTransfer = new List<NewNode>(roomSelection.nodes);

                foreach (NewNode n in nodesToTransfer)
                {
                    addressSelection.nullRoom.AddNewNode(n);
                }

                addressSelection.RemoveRoom(roomSelection);

                UpdateRoomDesignationIDsDropdown(); //The update uses class data, so we need to save it first
                SaveCurrentVariation();
            }
        }
    }

    public AddressLayoutVariation GetLoadedVariation(NewAddress forAddress)
    {
        if(forAddress != null && forAddress.loadedVarIndex > -1 && forAddress.saveData != null)
        {
            try
            {
                return forAddress.saveData.vs[forAddress.loadedVarIndex];
            }
            catch
            {
                return null;
            }
        }

        return null;
    }
}
