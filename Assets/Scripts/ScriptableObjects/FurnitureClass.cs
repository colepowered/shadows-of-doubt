﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "furnitureclass_data", menuName = "Database/Decor/Furniture Class")]

public class FurnitureClass : SoCustomComparison
{
    public enum FurnitureRuleOption { mustFeature, cantFeature, canFeature };
    public enum WallRule { nothing, wall, window, windowLarge, entrance, ventUpper, ventLower, wallOrUpperVent, ventTop, entranceDoorOnly, entranceToRoomOfType, anyWindow, entraceDivider, securityDoorDivider, fence, addressEntrance, lightswitch };

    [System.Serializable]
    public class FurniureWalkSubLocations
    {
        [Tooltip("This rule is applied at this offset")]
        public Vector2 offset = Vector2.zero;
        public List<Vector3> sublocations = new List<Vector3>();
    }

    [System.Serializable]
    public class FurnitureNodeRule
    {
        [Tooltip("This rule is applied at this offset")]
        public Vector2 offset = Vector2.zero;
        [Tooltip("Type of rule to apply")]
        public FurnitureRuleOption option;
        public bool anyOccupiedTile = false;
        [HideIf("anyOccupiedTile")]
        [Tooltip("What should be found at this node?")]
        public FurnitureClass furnitureClass;
        [Tooltip("If 'Can Feature' add this to the location score")]
        [Range(-10, 10)]
        public int addScore = 0;
    }

    [System.Serializable]
    public class FurnitureWallRule
    {
        [Tooltip("This rule is applied at this offset")]
        public Vector2 nodeOffset = Vector2.zero;
        [Tooltip("This rule is applied at this offset")]
        public CityData.BlockingDirection wallDirection = CityData.BlockingDirection.none;
        [Tooltip("Type of rule to apply")]
        public FurnitureRuleOption option;
        [Tooltip("What should be found at this offset?")]
        public WallRule tag;
        [Tooltip("If the tag is 'room to'")]
        public RoomConfiguration roomType;
        [Tooltip("If 'Can Feature' add this to the location score")]
        [Range(-10, 10)]
        public int addScore = 0;
    }

    [Header("Rules")]
    [Tooltip("List of rules this furniture must follow")]
    public List<FurnitureWallRule> wallRules = new List<FurnitureWallRule>();
    [Space(7)]
    [Tooltip("List of rules this furniture must follow")]
    public List<FurnitureNodeRule> nodeRules = new List<FurnitureNodeRule>();
    [Space(7)]
    [Tooltip("Which points between nodes are blocked (no walking access)")]
    public List<BlockedAccess> blockedAccess = new List<BlockedAccess>();

    [System.Serializable]
    public class BlockedAccess
    {
        public bool disabled = false;
        public Vector2 nodeOffset = Vector2.zero;
        [Tooltip("Block diagonals on adjacent tiles")]
        public bool blockExteriorDiagonals = false;
        public List<CityData.BlockingDirection> blocked = new List<CityData.BlockingDirection>();
    }

    [System.Serializable]
    public class SubObject
    {
        public SubObjectClassPreset preset;
        public string parent;
        public Vector3 localPos;
        public Vector3 localRot;
    }

    [Header("PreCalculated Limits")]
    [OnValueChanged("UpdatePreCalculatedLimits")]
    public bool updatePreCalculated = false;
    [ReadOnly]
    public int minimumZeroNodeWallCount = 0;
    [ReadOnly]
    public int maximumZeroNodeWallCount = 4;

    [Header("Behaviour")]
    //[Tooltip("If true this furniture cannot half be against a wall")]
    //public bool allowJutting = false;
    [Tooltip("If true, face the furniture diagonally if in corner")]
    public bool canFaceDiagonally = false;

    [Space(7)]
    [Tooltip("Maximum number per room")]
    public bool limitPerRoom = true;
    [EnableIf("limitPerRoom")]
    [Range(1, 20)]
    public int maximumNumberPerRoom = 8;

    [Tooltip("Maximum number per address")]
    public bool limitPerAddress = true;
    [EnableIf("limitPerAddress")]
    [Range(1, 20)]
    public int maximumNumberPerAddress = 8;

    [Tooltip("Allow only on this floor")]
    public bool limitToFloor = false;
    [EnableIf("limitToFloor")]
    public int allowedOnFloor = 0;

    [DisableIf("limitToFloor")]
    [Tooltip("Allow only on this range")]
    public bool limitToFloorRange = false;
    [EnableIf("limitToFloorRange")]
    public Vector2 allowedOnFloorRange = Vector2.zero;

    //public bool limitPerBuilding = false;
    //[Tooltip("Maximum number per building")]
    //[EnableIf("limitPerBuilding")]
    //[Range(1, 20)]
    //public int maximumNumberPerBuilding = 1;

    //public bool limitPerCity = false;
    //[Tooltip("Maximum number per city")]
    //[EnableIf("limitPerCity")]
    //[Range(1, 20)]
    //public int maximumNumberPerCity = 1;

    public bool limitPerBuildingResidence = false;
    [Tooltip("Limit to 1 per below number of residences in the building")]
    [EnableIf("limitPerBuildingResidence")]
    public int perBuildingResidences = 30;

    public bool limitPerJobs = false;
    [Tooltip("Limit to 1 per below number of residences in the building")]
    [EnableIf("limitPerJobs")]
    public int perJobs = 6;

    [Space(7)]

    [Tooltip("Must be at least this distance (nodes) from these classes...")]
    public List<FurnitureClass> awayFromClasses = new List<FurnitureClass>();
    [Tooltip("Minimum node distance from these classes. A diagonal is about 1.8")]
    public float minimumNodeDistance = 0;

    [Header("Visuals")]
    public Vector2 objectSize = new Vector2(1, 1);
    [Tooltip("If true this would cover up items on the wall such as lightswitches or block windows")]
    public bool tall = false;
    [Tooltip("Use the corresponding wall rules to place wall pieces")]
    public bool wallPiece = false;
    [HideIf("wallPiece")]
    [Tooltip("If being placed in decor mode, snap to nearby walls")]
    public bool useWallSnappingInDecorMode = false;
    [Tooltip("Allow one of these per window")]
    public bool windowPiece = false;
    [Tooltip("Does this block the placement of other furniture (if this flag is true on them also)?")]
    public bool occupiesTile = true;
    [Tooltip("If this furniture allowed on stairwell tiles?")]
    public bool allowedOnStairwell = false;
    [ShowIf("allowedOnStairwell")]
    public bool onlyOnStairwell = false;
    [Tooltip("Determins allowed if no floor")]
    public bool allowIfNoFloor = false;
    [Tooltip("Is this a ceiling piece?")]
    public bool ceilingPiece = false;
    [DisableIf("ceilingPiece")]
    [Tooltip("Does this require a ceiling above it?")]
    public bool requiresCeiling = false;
    [Tooltip("Does this block ceiling pieces from being placed?")]
    public bool blocksCeiling = false;
    [Tooltip("This is allowed to be placed if there is a lightswitch on this tile")]
    public bool allowLightswitch = true;
    [EnableIf("allowLightswitch")]
    [Tooltip("If a lightswitch exists here, raise the height slightly")]
    public bool raiseLightswitch = false;
    [EnableIf("raiseLightswitch")]
    public float lightswitchYOffset = 1.5f;

    [Header("AI")]
    [Tooltip("If true this object doesn't need access and won't ever block anything; use on minor objects to optimize placement checks")]
    public bool noBlocking = false;
    [Tooltip("If true the AI can access this node, but not pass through it on the way to something else. Can be used for 1 node items such as tables where you can't normally block access. IMPORTANT: Usually true if all but 1-3 directions are blocked.")]
    public bool noPassThrough = false;
    [Tooltip("If true the AI can't access this node, this node is effectively exluded from access checks. IMPORTANT: Make sure this is enabled if all directions are blocked.")]
    public bool noAccessNeeded = false;
    //[Tooltip("If true this furniture must be able to be accessed by all entrances to the room, not just any entrance...")]
    //public bool allAccessRequired = false;
    [Tooltip("If true then default sublocations will be blocked completely (usually they are added if there are no custom ones on a node). Default sublocations will be used if there are no custom ones, and there is no furniture class with a blocking flag.")]
    public bool blockDefaultSublocations = false;
    [Tooltip("If true then the physics check will ignore colliders that are not citizens")]
    public bool ignoreGeometryInPhysicsCheck = false;
    [Tooltip("These will be added to the tile's sublocations")]
    public List<FurniureWalkSubLocations> sublocations = new List<FurniureWalkSubLocations>();
    [Tooltip("If the AI is robbing a location, use this to compare the likihood of valuable contents...")]
    [Range(0, 10)]
    public int aiRobberyPriority = 3;
    public bool isSecurityCamera = false;

    [Header("Ownership")]
    [Tooltip("Dictates what class of ownership (ie each person living here needs 1 bed)")]
    public OwnershipClass ownershipClass = OwnershipClass.none;
    public enum OwnershipClass { none, bed, desk, locker, drawers, noticeBoard, safe, mailboxes };

    [Tooltip("From where does this derrive the owners pool?")]
    public OwnershipSource ownershipSource = OwnershipSource.addressInhabitants;

    public enum OwnershipSource { addressInhabitants, buildingResidences};
    [Tooltip("Assign owners to this furniture")]
    public int assignBelongsToOwners = 0;
    [Tooltip("If this is checked the game will assign this object to a couple")]
    public bool preferCouples = false;
    [Tooltip("If this is true, the object will try to copy ownership from previously placed items in the cluster")]
    public bool copyFromPreviouslyPlacedInCluster = false;
    [Tooltip("If this is true, this will only pick from the owners of the room (if there are any)")]
    public bool onlyPickFromRoomOwners = false;
    [Tooltip("Skip placement of this if there are no address owners")]
    public bool skipIfNoAddressInhabitants = false;
    [Tooltip("Assign homeless owners")]
    public bool assignHomelessOwners = false;
    [Tooltip("Make sure ownership is only assigned if mailbox not already assigned to an apartment")]
    public bool assignMailbox = false;
    [Tooltip("Don't allow mission photographs on this furniture")]
    public bool discourageMissionPhotos = false;

    [Header("Copy")]
    public FurnitureClass copyFrom;

    [Button]
    public void CopyBlockedAccess()
    {
        if(copyFrom != null)
        {
            blockedAccess.Clear();
            blockedAccess.AddRange(copyFrom.blockedAccess);

            #if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            #endif
        }
    }

    [Button]
    public void CopySublocations()
    {
        if (copyFrom != null)
        {
            sublocations.Clear();
            sublocations.AddRange(copyFrom.sublocations);

            #if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
            #endif
        }
    }

    [Button]
    public void BlockSolid()
    {
        blockedAccess.Clear();

        for (int i = 0; i < objectSize.x; i++)
        {
            for (int u = 0; u < objectSize.y; u++)
            {
                Vector2 v2 = new Vector2(-i, -u);

                BlockedAccess ba = new BlockedAccess();
                ba.nodeOffset = v2;

                ba.blocked.Add(CityData.BlockingDirection.behind);
                ba.blocked.Add(CityData.BlockingDirection.behindLeft);
                ba.blocked.Add(CityData.BlockingDirection.behindRight);
                ba.blocked.Add(CityData.BlockingDirection.front);
                ba.blocked.Add(CityData.BlockingDirection.frontLeft);
                ba.blocked.Add(CityData.BlockingDirection.frontRight);
                ba.blocked.Add(CityData.BlockingDirection.left);
                ba.blocked.Add(CityData.BlockingDirection.right);

                blockedAccess.Add(ba);
            }
        }
    }

    [Button]
    public void BlockAllButFront()
    {
        blockedAccess.Clear();

        for (int i = 0; i < objectSize.x; i++)
        {
            for (int u = 0; u < objectSize.y; u++)
            {
                Vector2 v2 = new Vector2(-i, -u);

                BlockedAccess ba = new BlockedAccess();
                ba.nodeOffset = v2;

                ba.blocked.Add(CityData.BlockingDirection.behind);
                ba.blocked.Add(CityData.BlockingDirection.behindLeft);
                ba.blocked.Add(CityData.BlockingDirection.behindRight);
                ba.blocked.Add(CityData.BlockingDirection.frontLeft);
                ba.blocked.Add(CityData.BlockingDirection.frontRight);
                ba.blocked.Add(CityData.BlockingDirection.left);
                ba.blocked.Add(CityData.BlockingDirection.right);

                blockedAccess.Add(ba);
            }
        }
    }

    [Button]
    public void UpdatePreCalculatedLimits()
    {
        minimumZeroNodeWallCount = 0;
        maximumZeroNodeWallCount = 4;

        HashSet<CityData.BlockingDirection> wallRequired = new HashSet<CityData.BlockingDirection>();
        HashSet<CityData.BlockingDirection> noWallRequired = new HashSet<CityData.BlockingDirection>();

        //Run through wall rules in class...
        foreach (FurnitureWallRule f in wallRules)
        {
            if (Mathf.RoundToInt(f.nodeOffset.x) == 0 && Mathf.RoundToInt(f.nodeOffset.y) == 0)
            {
                if (f.option == FurnitureClass.FurnitureRuleOption.mustFeature)
                {
                    if (f.tag != FurnitureClass.WallRule.nothing)
                    {
                        if (!wallRequired.Contains(f.wallDirection))
                        {
                            wallRequired.Add(f.wallDirection);
                        }
                    }
                    else
                    {
                        if (!noWallRequired.Contains(f.wallDirection))
                        {
                            noWallRequired.Add(f.wallDirection);
                        }
                    }
                }
                else if (f.option == FurnitureClass.FurnitureRuleOption.cantFeature)
                {
                    if (f.tag == FurnitureClass.WallRule.nothing)
                    {
                        if (!wallRequired.Contains(f.wallDirection))
                        {
                            wallRequired.Add(f.wallDirection);
                        }
                    }
                }
            }
        }

        minimumZeroNodeWallCount = Mathf.Max(minimumZeroNodeWallCount, wallRequired.Count);
        maximumZeroNodeWallCount = Mathf.Min(maximumZeroNodeWallCount, 4 - noWallRequired.Count);

        if (!noAccessNeeded)
        {
            maximumZeroNodeWallCount = Mathf.Min(maximumZeroNodeWallCount, 3);
        }

        //Save changes
#if UNITY_EDITOR
        UnityEditor.EditorUtility.SetDirty(this);
#endif
    }
}
