﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "furniture_data", menuName = "Database/Decor/Furniture Preset")]

public class FurniturePreset : SoCustomComparison
{
    [Header("Rules")]
    [Space(7)]
    [Tooltip("Classes that this furniture belongs to")]
    public List<FurnitureClass> classes = new List<FurnitureClass>();

    public enum SubObjectOwnership { nobody, everybody, person0, person1, person2, person3, person4, person5, person6, person7, person8, person9, person10, person11, person12, person13, person14, person15, person16, person17, person18, person19, person20, person21, person22, person23, person24, person25, person26, person27, person28, person29 };

    [System.Serializable]
    public class SubObject
    {
        public SubObjectClassPreset preset;
        public string parent;
        public Vector3 localPos;
        public Vector3 localRot;
        //This corresponds to the ownership map
        public SubObjectOwnership belongsTo = SubObjectOwnership.nobody; //-2 == belongs to nobody
        public int security = 0; //Security modifier (+ onto room's)
    }

    [System.Serializable]
    public class IntegratedInteractable
    {
        public InteractablePreset preset;
        public InteractableController.InteractableID pairToController;
        public FurniturePreset.SubObjectOwnership belongsTo;
    }


    [Header("Visuals")]
    public GameObject prefab;
    [Tooltip("If true this will seach for identical furniture in a room to batch with")]
    public bool allowStaticBatching = false;
    public ObjectPoolingController.ObjectLoadRange spawnRange = ObjectPoolingController.ObjectLoadRange.far;

    [Header("AI Interaction")]
    [Tooltip("What interatables will be instanced on this? These won't be spawned but created and searched for within the furniture prefab")]
    public List<IntegratedInteractable> integratedInteractables = new List<IntegratedInteractable>();

    [Header("Decor Settings")]
    [Tooltip("If true use across all design styles")]
    public bool universalDesignStyle = false;
    public List<DesignStylePreset> designStyles = new List<DesignStylePreset>();
    [Space(7)]
    public bool inheritColouringFromDecor = false;
    [Tooltip("If true the same material colours will be shared over all instances of this furniture for the room")]
    public ShareColours shareColours = ShareColours.none;
    [Tooltip("If true this furniture will inherit a grub value from the decor/room")]
    public bool inheritGrubFromDecor = true;
    public enum ShareColours { none, seating, wallFrontage, cabinets, cubicles, curtains, telephone, wood, doors, shelving, bins, blinds };
    public List<MaterialGroupPreset.MaterialVariation> variations = new List<MaterialGroupPreset.MaterialVariation>();

    [Space(7)]
    [Tooltip("If this is a part of a group, furntiure of the same group will be chosen in this room.")]
    public FurnitureGroup furnitureGroup = FurnitureGroup.none;
    public int groupID = 0;
    public enum FurnitureGroup { none, seating, windowDecor};

    [Header("Material Composition")]
    [Range(0f, 1f)]
    public float concrete = 1f;
    [Range(0f, 1f)]
    public float plaster = 0f;
    [Range(0f, 1f)]
    public float wood = 0f;
    [Range(0f, 1f)]
    public float carpet = 0f;
    [Range(0f, 1f)]
    public float tile = 0f;
    [Range(0f, 1f)]
    public float metal = 0f;
    [Range(0f, 1f)]
    public float glass = 0f;
    [Range(0f, 1f)]
    public float fabric = 0f;

    [Header("Suitability")]
    [Tooltip("This is secondary to the same property in the class preset.")]
    public int minimumRoomSize = 99;
    [Tooltip("Is this allowed in open plan rooms?")]
    public FurnitureCluster.AllowedOpenPlan allowedInOpenPlan = FurnitureCluster.AllowedOpenPlan.yes;

    [Space(7)]
    [Tooltip("Only allow this in certain inhabitant presets")]
    public bool onlyAllowInFollowing = false;
    public List<AddressPreset> allowedInAddressesOfType = new List<AddressPreset>();

    [Tooltip("Ban this in certain inhabitant presets")]
    public bool banInFollowing = false;
    public List<AddressPreset> bannedInAddressesOfType = new List<AddressPreset>();

    [Space(7)]
    [Tooltip("Only allow this in certain buildings")]
    public bool OnlyAllowInBuildings = false;
    [EnableIf("OnlyAllowInBuildings")]
    public List<BuildingPreset> allowedInBuildings = new List<BuildingPreset>();
    public bool banFromBuildings = false;
    [EnableIf("banFromBuildings")]
    public List<BuildingPreset> notAllowedInBuildings = new List<BuildingPreset>();

    [Space(7)]
    [Tooltip("Only allow this in certain districts")]
    public bool OnlyAllowInDistricts = false;
    [EnableIf("OnlyAllowInDistricts")]
    public List<DistrictPreset> allowedInDistricts = new List<DistrictPreset>();
    public bool banFromDistricts = false;
    [EnableIf("banFromDistricts")]
    public List<DistrictPreset> notAllowedInDistricts = new List<DistrictPreset>();

    [Space(7)]
    public bool requiresGenderedInhabitants = false;
    public List<Human.Gender> enableIfGenderPresent = new List<Human.Gender>();

    [Space(7)]
    [Tooltip("The furniture is only allowed in these room types")]
    public List<RoomTypeFilter> allowedRoomFilters = new List<RoomTypeFilter>();

    [Space(7)]
    [Range(0f, 1f)]
    public float minimumWealth = 0f;
    [Space(5)]
    public bool usePersonalityWeighting = false;
    [Tooltip("0 = old fashioned/conservative, 1 = modern/liberal: Driven by the design style")]
    [EnableIf("usePersonalityWeighting")]
    [Range(0, 10)]
    public int modernity = 5;
    [Tooltip("0 = informal/cosy, 1 = clean/souless: Driven by the room type.")]
    [EnableIf("usePersonalityWeighting")]
    [Range(0, 10)]
    public int cleanness = 5;
    [Tooltip("0 = understated/quiet, 1 = loud/bold: Driven by the owner's personality")]
    [EnableIf("usePersonalityWeighting")]
    [Range(0, 10)]
    public int loudness = 5;
    [Tooltip("0 = cold/hard, 1 = warm/sensitive: Driven by the owner's personality")]
    [EnableIf("usePersonalityWeighting")]
    [Range(0, 10)]
    public int emotive = 5;

    [Header("Sub Objects")]
    public List<SubObject> subObjects = new List<SubObject>();

    public enum ModifierTest { none, testOwner, testInhbitants };
    [Tooltip("Use this setting to test for subobject spawn modifiers (see 'SubObjectClassPreset')")]
    public ModifierTest testForModifiers = ModifierTest.none;
    [Tooltip("Objects with illegal actions on this will override the public area allowance set in the interactable setup")]
    public bool forcePublicIllegal = false;

    [Header("Hiding")]
    public PlayerTransitionPreset hidingEnterTransition;
    public PlayerTransitionPreset hidingExitTransition;
    public PlayerTransitionPreset hidingEnterTransition2;
    public PlayerTransitionPreset hidingExitTransition2;

    [Header("Map")]
    public Texture2D map;
    public bool drawUnderWalls = false;
    public bool ignoreDirection = false;

    [Header("Fingerprints")]
    [Tooltip("Should there be fingerprints here?")]
    public bool fingerprintsEnabled = true;
    [Tooltip("The source of the prints")]
    public RoomConfiguration.PrintsSource printsSource = RoomConfiguration.PrintsSource.inhabitants;
    [Tooltip("Fingerprint density")]
    [Range(0f, 5f)]
    public float fingerprintDensity = 1f;

    [Header("Environment")]
    [Tooltip("Change area colours")]
    public bool alterAreaLighting = false;
    [EnableIf("alterAreaLighting")]
    public List<Color> possibleColours = new List<Color>();
    [Tooltip("This is used in combination with the following to adjust street area lighting")]
    [EnableIf("alterAreaLighting")]
    public DistrictPreset.AffectStreetAreaLights lightOperation = DistrictPreset.AffectStreetAreaLights.lerp;
    [EnableIf("alterAreaLighting")]
    public float lightAmount = 0.12f;
    [Tooltip("This is added to brightness")]
    [EnableIf("alterAreaLighting")]
    public float brightnessModifier = 10f;

    [Header("Decor Edit")]
    public bool purchasable = true;
    public int cost = 100;
    public DecorClass decorClass = DecorClass.misc;

    [Space(7)]
    [ShowAssetPreview]
    public Sprite staticImage;
    [ReadOnly]
    public Vector3 imagePos;
    [ReadOnly]
    public Vector3 imageRot;
    [ReadOnly]
    public float imageScale = 1f;
    [ReadOnly]
    public GameObject imagePrefabOverride;

    public enum DecorClass { chairs, tables, units, electronics, structural, decoration, misc };

    [Header("Special")]
    [Tooltip("Is this a board where jobs can be posted?")]
    public bool isJobBoard = false;
    //[Tooltip("Is this a counter (for work)?")]
    //public bool isCounter = false;
    [Tooltip("Is this a desk (for work)? If true furniture ownership will be assigned based on jobs.")]
    public bool isWorkPosition = false;
    [Tooltip("Can spawn a variety of plants")]
    public bool isPlant = false;
    [Tooltip("Does this require the game to pick a piece of art to fit this?")]
    public bool isArt = false;
    [Tooltip("Is this a security camera? (Special limitations)")]
    public bool isSecurityCamera = false;
    [EnableIf("isArt")]
    public ArtPreset.ArtOrientation artOrientation = ArtPreset.ArtOrientation.portrait;
    [Tooltip("Does this require a special self employed job?")]
    public CompanyPreset createSelfEmployed;
    [Tooltip("If above is true: Which slot contains the work position?")]
    public InteractableController.InteractableID workPositionID = InteractableController.InteractableID.A;
    [Tooltip("Chance to spawn the below objects; works on a item-by-item basis")]
    public float spawnObjectOnChance = 1f;
    [Tooltip("Spawns these objects once placed")]
    public List<InteractablePreset> spawnObjectsOnPlacement = new List<InteractablePreset>();
}
