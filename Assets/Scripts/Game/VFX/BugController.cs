﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugController : MonoBehaviour
{
    public NewRoom room;
    List<NewNode> nodes;
    public float speed = 1f;
    public float turnSpeed = 6f;

    bool newJourney = true;
    NewNode destinationNode;
    Vector3 destinationPos;

    public void Setup(NewRoom newRoom)
    {
        room = newRoom;

        //Pick a starting node
        nodes = new List<NewNode>();

        //Only spawn on nodes with a floor
        foreach (NewNode n in room.nodes)
        {
            if (n.floorType == NewNode.FloorTileType.floorAndCeiling || n.floorType == NewNode.FloorTileType.floorOnly)
            {
                nodes.Add(n);
            }
        }

        if (nodes.Count > 0)
        {
            NewNode startNode = nodes[Toolbox.Instance.Rand(0, nodes.Count)];

            this.transform.position = startNode.position + new Vector3(Toolbox.Instance.Rand(-0.7f, 0.7f), 0, Toolbox.Instance.Rand(-0.7f, 0.7f));
            newJourney = true;
        }
        else this.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!SessionData.Instance.startedGame || !SessionData.Instance.play || nodes.Count <= 0) return;

        //Random movement
        if(newJourney)
        {
            List<NewNode> valid = new List<NewNode>();

            foreach(NewNode n in nodes)
            {
                if(n.floorType == NewNode.FloorTileType.floorOnly || n.floorType == NewNode.FloorTileType.floorAndCeiling)
                {
                    valid.Add(n);
                }
            }

            if(valid.Count > 0)
            {
                destinationNode = valid[Toolbox.Instance.Rand(0, valid.Count)];
                destinationPos = destinationNode.position + new Vector3(Toolbox.Instance.Rand(-0.7f, 0.7f), 0, Toolbox.Instance.Rand(-0.7f, 0.7f));
                newJourney = false;
            }
        }
        else if(destinationNode != null)
        {
            //Face the destination
            Quaternion facingQuat = Quaternion.identity;
            Vector3 relativePos = destinationPos - transform.position;
            relativePos.y = 0;

            //Avoid rotation error
            if (relativePos != Vector3.zero)
            {
                facingQuat = Quaternion.LookRotation(relativePos, Vector3.up);
            }

            transform.rotation = Quaternion.Slerp(transform.rotation, facingQuat, turnSpeed * Time.deltaTime);

            //Pick a new destination
            float dist = Vector3.Distance(transform.position, destinationPos);

            if (dist <= 0.05f)
            {
                newJourney = true;
                return;
            }

            speed = Mathf.Clamp(1f, 0.3f, dist / 0.5f);

            //Move forwards
            //transform.position = Vector3.MoveTowards(transform.position, destinationPos, speed * Time.deltaTime);
            transform.position += this.transform.forward * speed * Time.deltaTime;
        }
    }
}
