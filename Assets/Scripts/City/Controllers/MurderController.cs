﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class MurderController : MonoBehaviour
{
    //These values are saved in save state
    [Header("State")]
    public bool procGenLoopActive = false;
    public bool murderRoutineActive = false;
    public Human currentMurderer;
    public Human currentVictim;
    [System.NonSerialized]
    public Case currentActiveCase;
    public MurderPreset murderPreset;
    public MurderMO chosenMO;
    public List<Human> previousMurderers = new List<Human>();
    public float pauseBetweenMurders = 0f;
    private float locationUpdateTimer = 0f;
    public int maxDifficultyLevel = 1;

    [Header("DDS")]
    public List<MurderMethod> methodTypes = new List<MurderMethod>();

    [System.Serializable]
    public class MurderMethod
    {
        public MurderWeaponPreset.WeaponType type;
        public string blockDDS;
    }

    [Header("Murder")]
    public int assignMurderID = 1;
    public List<Murder> activeMurders = new List<Murder>();
    public List<Murder> inactiveMurders = new List<Murder>();

    [Header("References")]
    public AIGoalPreset murderGoalPreset;

    //private int locationUpdateCounter = 0;

    [Header("Debug")]
    public List<MurderPick> debugLastMurderPicks = new List<MurderPick>();

    public enum MurderState { none, acquireEuipment, research, waitForLocation, travellingTo, executing, post, escaping, unsolved, solved };

    [System.Serializable]
    public class MurderPick
    {
        public Human person;
        public MurderMO mo;
        public float score;
    }

    [System.Serializable]
    public class Murder
    {
        [Header("Serializable Data")]
        public string presetStr;
        public string moStr;
        public int murderID = -1;
        public int murdererID = -1;
        public int victimID = -1;
        public int streetID = -1;
        public MurderState state = MurderState.none;
        public int addressID = -1;
        public float waitingTimestamp; //Time stamp since at waiting for location state triggered
        public float time;
        public string monkierPre;
        public string monkierPost;

        [Space(7)]
        public string weaponStr;
        public string ammoStr;
        public int weaponID = -1; //Actual weapon reference
        public int ammoID = -1; //Actual weapon reference
        public int weaponSourceID = -1; //Company ID ref
        public bool acquiredEquipment = false;
        public float dropChance = 0f; //Is the killer allowed to drop their weapon after the murder?

        [Space(7)]
        public string callingCardStr;
        public MurderMO.CallingCardOrigin callingCardOrigin;
        public int callingCardID = -1;
        public List<int> graffitiIDs = new List<int>();
        public string graffitiMsg;

        [Space(7)]
        //When updating culling, make sure these rooms are also drawn...
        public List<int> cullingActiveRooms = new List<int>();

        [Header("NonSerialzed Data")]
        [System.NonSerialized]
        public MurderPreset preset;
        [System.NonSerialized]
        public MurderMO mo;
        [System.NonSerialized]
        public Human murderer;
        [System.NonSerialized]
        public Human victim;
        [System.NonSerialized]
        public NewAIGoal murderGoal;
        [System.NonSerialized]
        public NewGameLocation location;
        [System.NonSerialized]
        public Human.Death death;
        [System.NonSerialized]
        public Dictionary<JobPreset.JobTag, Interactable> activeMurderItems = new Dictionary<JobPreset.JobTag, Interactable>(); //Items that have been spawned because of this job...
        [System.NonSerialized]
        public InteractablePreset weaponPreset;
        [System.NonSerialized]
        public InteractablePreset ammoPreset;
        [System.NonSerialized]
        public Interactable weapon;
        [System.NonSerialized]
        public Interactable ammo;
        [System.NonSerialized]
        public InteractablePreset callingCardPreset;
        [System.NonSerialized]
        public Interactable callingCard;
        [System.NonSerialized]
        public Company weaponSource;
        [System.NonSerialized]
        public List<Interactable> graffiti = new List<Interactable>();

        //Events
        public delegate void OnMurderStateChange(MurderState newState);
        public event OnMurderStateChange OnStateChanged;

        public Murder(Human newMurderer, Human newVictim, MurderPreset newPreset, MurderMO newMotive)
        {
            murderID = Instance.assignMurderID;
            Instance.assignMurderID++;

            murderer = newMurderer;
            murdererID = murderer.humanID;

            victim = newVictim;
            victimID = victim.humanID;

            preset = newPreset;
            presetStr = preset.name;

            mo = newMotive;
            moStr = newMotive.name;

            //Pick muder weapon & calling card; use predictable values based on killer so they are the same per killer
            PickNewMurderWeapon();
            PickNewCallingCard();

            Instance.activeMurders.Add(this);

            Game.Log("Murder: Creating new murder of " + victim.GetCitizenName() + " by " + murderer.GetCitizenName());

            EuipmentCheck();
            SetMurderState(MurderState.acquireEuipment);

            //Set tick rates and disable udaptes
            murderer.ai.SetDesiredTickRate(NewAIController.AITickRate.veryHigh, true);
            murderer.ai.disableTickRateUpdate = true;
            victim.ai.SetDesiredTickRate(NewAIController.AITickRate.veryHigh, true);
            victim.ai.disableTickRateUpdate = true;

            //Add murder goal...
            murderGoal = murderer.ai.CreateNewGoal(MurderController.Instance.murderGoalPreset, SessionData.Instance.gameTime, 0f, newPassedInteractable: victim.interactable, newMurderRef: this);
        }

        public void LoadSerializedData()
        {
            if (presetStr != null && presetStr.Length > 0) Toolbox.Instance.LoadDataFromResources<MurderPreset>(presetStr, out preset);
            if (moStr != null && moStr.Length > 0) Toolbox.Instance.LoadDataFromResources<MurderMO>(moStr, out mo);

            if (weaponStr != null && weaponStr.Length > 0) Toolbox.Instance.LoadDataFromResources<InteractablePreset>(weaponStr, out weaponPreset);
            if (callingCardStr != null && ammoStr.Length > 0) Toolbox.Instance.LoadDataFromResources<InteractablePreset>(ammoStr, out ammoPreset);

            if(callingCardStr != null && callingCardStr.Length > 0) Toolbox.Instance.LoadDataFromResources<InteractablePreset>(callingCardStr, out callingCardPreset);

            CityData.Instance.GetHuman(murdererID, out murderer);
            CityData.Instance.GetHuman(victimID, out victim);

            if (Game.Instance.debugMurdererOnStart && Game.Instance.collectDebugData)
            {
                if(!Game.Instance.debugHuman.Contains(murderer)) murderer.ai.ToggleHumanDebug();
            }

            if (streetID > -1) location = CityData.Instance.streetDirectory.Find(item => item.streetID == streetID);
            else if (addressID > -1)
            {
                NewAddress ad = null;
                CityData.Instance.addressDictionary.TryGetValue(addressID, out ad);
                location = ad;
            }

            if(weaponID > -1)
            {
                CityData.Instance.savableInteractableDictionary.TryGetValue(weaponID, out weapon);
            }

            if (ammoID > -1)
            {
                CityData.Instance.savableInteractableDictionary.TryGetValue(ammoID, out ammo);
            }

            if (callingCardID > -1)
            {
                CityData.Instance.savableInteractableDictionary.TryGetValue(callingCardID, out callingCard);
            }

            if(weaponSourceID > -1)
            {
                weaponSource = CityData.Instance.companyDirectory.Find(item => item.companyID == weaponSourceID);
            }

            graffiti = new List<Interactable>();

            foreach(int i in graffitiIDs)
            {
                Interactable foundGraff = null;

                if(CityData.Instance.savableInteractableDictionary.TryGetValue(i, out foundGraff))
                {
                    graffiti.Add(foundGraff);
                }
            }

            //Give murderer a key
            if(location != null && location.thisAsAddress != null) murderer.AddToKeyring(location.thisAsAddress, false);

            //Add murder goal if needed
            if (murderGoal == null && state != MurderState.solved && state != MurderState.unsolved)
            {
                murderGoal = murderer.ai.goals.Find(item => item.preset == MurderController.Instance.murderGoalPreset);

                if(murderGoal == null)
                {
                    Game.Log("Murder: Creating new murder goal with victim passed interactable: " + victim.interactable);
                    murderGoal = murderer.ai.CreateNewGoal(MurderController.Instance.murderGoalPreset, SessionData.Instance.gameTime, 0f, newPassedInteractable: victim.interactable, newMurderRef: this);

                    //Set tick rates and disable udaptes
                    murderer.ai.SetDesiredTickRate(NewAIController.AITickRate.veryHigh, true);
                    murderer.ai.disableTickRateUpdate = true;
                    victim.ai.SetDesiredTickRate(NewAIController.AITickRate.veryHigh, true);
                    victim.ai.disableTickRateUpdate = true;
                }
            }
        }

        public void SetMurderState(MurderState newState, bool force = false)
        {
            if (state != newState || force)
            {
                state = newState;
                MurderController.Instance.SpawnItemsCheck(this);

                Game.Log("Murder: Set murder state: " + state);

                if(state == MurderState.waitForLocation)
                {
                    waitingTimestamp = SessionData.Instance.gameTime;
                }
                else if(state == MurderState.travellingTo)
                {
                    //Create calling card here if needed
                    if(callingCardPreset != null && callingCard == null && callingCardOrigin == MurderMO.CallingCardOrigin.createOnGoToLocation)
                    {
                        Game.Log("Murder: Creating new calling card: " + callingCardPreset);
                        callingCard = InteractableCreator.Instance.CreateWorldInteractable(callingCardPreset, murderer, murderer, victim, murderer.transform.position, Vector3.zero, null, null);

                        if (callingCard != null)
                        {
                            callingCard.SetInInventory(murderer);
                            callingCardID = callingCard.id;
                        }
                    }

                    //Set game location
                    murderGoal.gameLocation = location;

                    //Set as victim at this point
                    victim.ai.SetAsVictim(this);

                    murderer.ai.AITick(true);
                }
                else if(state == MurderState.executing)
                {
                    foreach(NewRoom r in location.rooms)
                    {
                        if(!cullingActiveRooms.Contains(r.roomID))
                        {
                            cullingActiveRooms.Add(r.roomID);
                        }
                    }

                    Player.Instance.UpdateCulling();

                    //Set as victim at this point
                    murderer.ai.ResetInvestigate();

                    victim.ai.SetAsVictim(this);
                    murderer.ai.SetAsMurderer(this);
                }
                else if(state == MurderState.post)
                {
                    victim.ai.SetAsVictim(this);
                    murderer.ai.SetAsMurderer(this);

                    PlaceCallingCard();
                    GenerateGraffiti();
                    WeaponDisposal();

                    //Remove previous actions
                    for (int i = 0; i < murderGoal.actions.Count; i++)
                    {
                        Game.Log("Murder: Removing " + murderGoal.actions[i].preset.name);
                        murderGoal.actions[i].Remove();
                        i--;
                    }

                    //Cancel investigate
                    murderer.ai.ResetInvestigate();
                }
                else if(state == MurderState.escaping)
                {

                }
                else if(state == MurderState.solved)
                {
                    if(MurderController.Instance.activeMurders.Contains(this))
                    {
                        MurderController.Instance.activeMurders.Remove(this);
                    }

                    if (!MurderController.Instance.inactiveMurders.Contains(this))
                    {
                        MurderController.Instance.inactiveMurders.Add(this);
                    }
                }

                if (murderGoal != null)
                {
                    if(murderGoal.isActive)
                    {
                        murderGoal.RefreshActions(true); //Refresh to load new actions
                        murderGoal.AITick();
                    }
                    else
                    {
                        murderer.ai.AITick(true);
                    }

                    murderer.ai.UpdateCurrentWeapon();
                }

                MurderController.Instance.SetUpdateEnabled(true); //Enable for updates

                //Fire event
                if(OnStateChanged != null)
                {
                    OnStateChanged(state);
                }
            }
        }

        public void CancelCurrentMurder()
        {
            if (murderGoal != null)
            {
                murderGoal.OnDeactivate(0f);
                murderGoal.murderRef = null;
                murderGoal.Remove();
            }

            //Set tick rates and disable udaptes
            murderer.ai.disableTickRateUpdate = false;
            victim.ai.disableTickRateUpdate = false;

            SetMurderState(MurderState.unsolved);
            cullingActiveRooms.Clear(); //No need to draw these rooms now

            for (int i = 0; i < victim.ai.victimsForMurders.Count; i++)
            {
                Murder m = victim.ai.victimsForMurders[i];

                if(m == this)
                {
                    victim.ai.victimsForMurders.RemoveAt(i);
                    i--;
                }
            }

            for (int i = 0; i < murderer.ai.killerForMurders.Count; i++)
            {
                Murder m = murderer.ai.killerForMurders[i];

                if (m == this)
                {
                    murderer.ai.killerForMurders.RemoveAt(i);
                    i--;
                }
            }

            murderer.ai.AITick(true);

            //murderer = null;
            //victim = null;
        }

        public bool IsValidLocation(NewGameLocation newLoc)
        {
            if ((mo.allowHome || mo.allowAnywhere) && victim.home == newLoc) return true;

            bool ret = false;

            if (mo.allowAnywhere) ret = true;
            else if (mo.allowWork && victim.job != null && victim.job.employer != null && victim.job.employer.placeOfBusiness == newLoc) ret = true;
            else if (mo.allowPublic && newLoc.thisAsAddress != null && newLoc.thisAsAddress.preset.publicFacing) ret = true;
            else if (mo.allowStreets && newLoc.thisAsStreet != null) ret = true;

            if (ret)
            {
                if(newLoc.currentOccupants.Count > preset.nonHomeMaximumOccupantsTrigger)
                {
                    return false;
                }
            }

            return ret;
        }

        public void PickNewMurderWeapon()
        {
            Game.Log("Murder: Picking new murder weapon...");

            float scoreToBeat = -99999f;
            weaponPreset = null;
            ammoPreset = null;

            foreach(MurderWeaponsPool pool in mo.weaponsPool)
            {
                foreach (MurderWeaponsPool.MurderWeaponPick wp in pool.murderWeaponPool)
                {
                    float score = Toolbox.Instance.GetPsuedoRandomNumber(wp.randomScoreRange.x, wp.randomScoreRange.y, CityData.Instance.seed + murdererID + murderer.citizenName + wp.weapon.name);

                    float traitModifier = 0f;

                    if (wp.traitModifiers.Count > 0)
                    {
                        if (MurderController.Instance.TraitTest(murderer as Citizen, ref wp.traitModifiers, out traitModifier))
                        {
                            score += traitModifier;
                        }
                        else continue; //Didn't pass the trait test so skip this...
                    }

                    //Weight weapon so it doesn't break the max difficulty level
                    if (mo.baseDifficulty + wp.weapon.weapon.murderDifficultyModifier <= MurderController.Instance.maxDifficultyLevel) score += 50;

                    //Weight towards existing items
                    if(murderer.inventory.Exists(item => item.preset == wp.weapon))
                    {
                        score += 100;
                    }
                    else if(murderer.ai.putDownItems.Exists(item => item.preset == wp.weapon))
                    {
                        score += 50;
                    }

                    if (score > scoreToBeat)
                    {
                        weaponPreset = wp.weapon;
                        scoreToBeat = score;
                        dropChance = wp.chanceOfDroppingAtScene;
                    }
                }
            }

            if(weaponPreset != null)
            {
                Game.Log("Murder: Chosen murder weapon " + weaponPreset.weapon.name + ", choosing ammo...");
                weaponStr = weaponPreset.name;

                if(weaponPreset.weapon.ammunition.Count > 0)
                {
                    ammoPreset = weaponPreset.weapon.ammunition[Toolbox.Instance.GetPsuedoRandomNumber(0, weaponPreset.weapon.ammunition.Count, CityData.Instance.seed + murdererID + murderer.citizenName + weaponPreset.name)];
                    ammoStr = ammoPreset.name;

                    Game.Log("Murder: Chosen ammo is " + ammoPreset.name + "...");
                }
            }
            else
            {
                Game.LogError("Unable to choose weapon for murder!");
            }
        }

        public void PickNewCallingCard()
        {
            Game.Log("Murder: Picking new calling card...");

            float scoreToBeat = -99999f;
            callingCardPreset = null;

            foreach (MurderMO.CallingCardPick wp in mo.callingCardPool)
            {
                float score = Toolbox.Instance.Rand(wp.randomScoreRange.x, wp.randomScoreRange.y);

                float traitModifier = 0f;

                if (wp.traitModifiers.Count > 0)
                {
                    if (MurderController.Instance.TraitTest(murderer as Citizen, ref wp.traitModifiers, out traitModifier))
                    {
                        score += traitModifier;
                    }
                    else continue; //Didn't pass the trait test so skip this...
                }

                if (score > scoreToBeat)
                {
                    callingCardPreset = wp.item;
                    callingCardStr = callingCardPreset.name;
                    callingCardOrigin = wp.origin;
                    scoreToBeat = score;
                }
            }

            if(callingCardPreset != null) Game.Log("Murder: Chosen calling card " + callingCardPreset.name + "...");
        }

        public void SetMurderWeaponActual(Interactable newObj)
        {
            weapon = newObj;
            if (newObj != null) weaponID = newObj.id;
            GenerateMoniker();
        }

        public void SetMurderLocation(NewGameLocation newLoc)
        {
            location = newLoc;

            if(location != null)
            {
                Game.Log("Murder: Set new murder location: " + newLoc.name);

                if (location.thisAsAddress != null)
                {
                    addressID = location.thisAsAddress.id;
                    murderer.AddToKeyring(location.thisAsAddress, false);
                }
                else if (location.thisAsStreet != null) streetID = location.thisAsStreet.streetID;

                if (murderGoal != null) murderGoal.gameLocation = newLoc;
            }
        }

        //Have I got the equipment?
        public void EuipmentCheck()
        {
            //Do I have weapon on my person?
            bool hasWeaponInInventory = murderer.inventory.Exists(item => item.preset == weaponPreset);
            bool hasAmmoInInventory = (ammoPreset == null || murderer.inventory.Exists(item => item.preset == ammoPreset));

            if (hasWeaponInInventory && hasAmmoInInventory)
            {
                acquiredEquipment = true;
            }
            else
            {
                acquiredEquipment = false;
            }

            if(acquiredEquipment)
            {
                if(state == MurderState.acquireEuipment)
                {
                    //If so, proceed to next stage...
                    SetMurderState(MurderState.research);
                }
            }
            else
            {
                if(murderGoal != null && murderGoal.isActive)
                {
                    if(state != MurderState.acquireEuipment)
                    {
                        SetMurderState(MurderState.acquireEuipment);
                    }
                }
            }
        }

        public string GetMonkier()
        {
            if(Strings.loadedLanguage != null && Strings.loadedLanguage.staticKillerMoniker && weapon != null)
            {
                int n = Toolbox.Instance.GetPsuedoRandomNumber(1, 10, murderer.citizenName);
                return Strings.Get("misc", "static_killer_ref_" + n.ToString());
            }

            if (monkierPre != null && monkierPre.Length > 0 && monkierPost != null && monkierPost.Length > 0)
            {
                return monkierPre + " " + monkierPost;
            }
            else return Strings.Get("misc", "static_killer_ref_1");
        }

        public void GenerateMoniker()
        {
            if (weapon != null)
            {
                //Get pre
                DDSSaveClasses.DDSMessageSave genericPre = null;

                if (mo.monkierDDSMessageList != null && mo.monkierDDSMessageList.Length > 0 && Toolbox.Instance.allDDSMessages.TryGetValue(mo.monkierDDSMessageList, out genericPre))
                {
                    monkierPre = Strings.Get("dds.blocks", genericPre.blocks[Toolbox.Instance.Rand(0, genericPre.blocks.Count)].blockID);
                }
                else
                {
                    monkierPre = CityData.Instance.cityName;
                }

                //Get appendage
                List<string> appendageBlock = new List<string>();

                //Get generic
                DDSSaveClasses.DDSMessageSave genericAppend = null;

                if (Toolbox.Instance.allDDSMessages.TryGetValue("84c57fc6-4a6f-4ea8-b84b-b268ea4044ee", out genericAppend))
                {
                    foreach (DDSSaveClasses.DDSBlockCondition cond in genericAppend.blocks)
                    {
                        appendageBlock.Add(cond.blockID);
                    }
                }

                DDSSaveClasses.DDSMessageSave weaponAppend = null;
                string wid = "84c57fc6-4a6f-4ea8-b84b-b268ea4044ee";

                if (weapon.preset.weapon.type == MurderWeaponPreset.WeaponType.blade) wid = "17669eb6-227e-4009-bded-2f2978dca87b";
                else if (weapon.preset.weapon.type == MurderWeaponPreset.WeaponType.poison) wid = "5eeaeae7-4388-4879-907a-6a10ef911238";
                else if (weapon.preset.weapon.type == MurderWeaponPreset.WeaponType.bluntObject) wid = "901e0290-7c47-4c40-b5e6-7957a4b8f97a";
                else if (weapon.preset.weapon.type == MurderWeaponPreset.WeaponType.handgun) wid = "869d9e21-1385-4b04-a796-65e9c02724ce";
                else if (weapon.preset.weapon.type == MurderWeaponPreset.WeaponType.rifle) wid = "2eb67819-2717-44ed-aa16-0abdd35ef77f";
                else if (weapon.preset.weapon.type == MurderWeaponPreset.WeaponType.shotgun) wid = "b6cf2693-b187-40b9-b11f-0b1734cd0564";
                else if (weapon.preset.weapon.type == MurderWeaponPreset.WeaponType.strangulation) wid = "999ffe45-9a78-43f3-8b8b-f9a049521ca0";

                if (Toolbox.Instance.allDDSMessages.TryGetValue(wid, out weaponAppend))
                {
                    foreach (DDSSaveClasses.DDSBlockCondition cond in weaponAppend.blocks)
                    {
                        if (!MurderController.Instance.inactiveMurders.Exists(item => item.monkierPost == cond.blockID))
                        {
                            for (int i = 0; i < 2; i++)
                            {
                                appendageBlock.Add(cond.blockID);
                            }
                        }
                    }
                }

                //Try and use alliteration...
                List<string> alliteration = new List<string>();

                foreach(string bID in appendageBlock)
                {
                    string blk = Strings.Get("dds.blocks", bID);

                    if(blk.Length > 0 && monkierPre.Length > 0)
                    {
                        if(blk.Substring(0, 1).ToLower() == monkierPre.Substring(0, 1).ToLower())
                        {
                            alliteration.Add(blk);
                        }
                    }
                }

                if (alliteration.Count > 0)
                {
                    monkierPost = alliteration[Toolbox.Instance.Rand(0, alliteration.Count)];
                }
                else if(appendageBlock.Count > 0)
                {
                    string pick = appendageBlock[Toolbox.Instance.Rand(0, appendageBlock.Count)];
                    monkierPost = Strings.Get("dds.blocks", pick);
                }

                Game.Log("Murder: Assigned monkier: " + GetMonkier());
            }
        }

        public void PlaceCallingCard()
        {
            if(callingCardPreset != null)
            {
                Vector3 position = Vector3.zero;
                Vector3 euler = Vector3.zero;

                if (victim != null)
                {
                    Transform upperTorso = victim.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.upperTorso);

                    if (upperTorso != null)
                    {
                        position = upperTorso.position + new Vector3(0, 1, 0);
                        euler = upperTorso.eulerAngles;
                    }
                    else return;
                }
                else return;

                if(callingCard != null && callingCardOrigin != MurderMO.CallingCardOrigin.createAtScene)
                {
                    Game.Log("Murder: Dropping existing calling card at " + position + ", euler " + euler);
                    callingCard.SetAsNotInventory(victim.currentNode);
                    callingCard.MoveInteractable(position, euler, true);
                    callingCard.ForcePhysicsActive(true, true, new Vector3(Toolbox.Instance.Rand(0.5f, 2f), 0, Toolbox.Instance.Rand(0.5f, 2f)));
                }
                else if(callingCardOrigin == MurderMO.CallingCardOrigin.createAtScene)
                {
                    Game.Log("Murder: Creating new calling card at " + position + ", euler " + euler);
                    List<Interactable.Passed> passed = new List<Interactable.Passed>();
                    passed.Add(new Interactable.Passed(Interactable.PassedVarType.murderID, murderID));

                    callingCard = InteractableCreator.Instance.CreateWorldInteractable(callingCardPreset, murderer, murderer, victim, position, euler, passed, null);

                    if(callingCard != null)
                    {
                        callingCard.ForcePhysicsActive(true, true, new Vector3(Toolbox.Instance.Rand(0.5f, 2f), 0, Toolbox.Instance.Rand(0.5f, 2f)));
                        callingCardID = callingCard.id;
                    }
                }
            }
        }

        public void WeaponDisposal()
        {
            if(dropChance > 0f && weapon != null && !mo.blockDroppingWeapons)
            {
                //Drop the weapon at the scene
                if(Toolbox.Instance.Rand(0f, 1f) <= dropChance)
                {
                    Game.Log("Murder: Dropping weapon at scene...");

                    Vector3 position = murderer.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.HandRight).position;
                    Vector3 euler = murderer.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.HandRight).eulerAngles;

                    weapon.SetAsNotInventory(murderer.currentNode);
                    weapon.MoveInteractable(position, euler, true);
                    weapon.ForcePhysicsActive(true, true, new Vector3(Toolbox.Instance.Rand(0.5f, 2f), 0, Toolbox.Instance.Rand(0.5f, 2f)));
                }
            }
        }

        public void GenerateGraffiti()
        {
            if(graffiti.Count <= 0)
            {
                foreach(MurderMO.Graffiti graf in mo.graffiti)
                {
                    Vector3 position = Vector3.zero;
                    Vector3 euler = Vector3.zero;

                    //Get position and rotation
                    if(graf.pos == MurderMO.Graffiti.GraffitiPosition.victim)
                    {
                        if (victim != null)
                        {
                            Transform upperTorso = victim.outfitController.GetBodyAnchor(CitizenOutfitController.CharacterAnchor.upperTorso);

                            if (upperTorso != null)
                            {
                                position = upperTorso.position;
                                position.y = victim.currentNode.position.y + 0.001f;
                                euler = new Vector3(0, upperTorso.eulerAngles.y, 0);
                            }
                            else return;
                        }
                        else return;
                    }
                    else if(graf.pos == MurderMO.Graffiti.GraffitiPosition.nearbyWall)
                    {
                        NewWall bestWall = null;
                        float wallScore = -9999f;

                        if (location != null)
                        {
                            foreach (NewRoom r in location.rooms)
                            {
                                foreach(NewNode n in r.nodes)
                                {
                                    if (n.individualFurniture.Exists(item => item.furnitureClasses[0].tall)) continue; //Exclude tall objects against this

                                    foreach (NewWall w in n.walls)
                                    {
                                        if (w.preset.sectionClass == DoorPairPreset.WallSectionClass.entrance) continue;
                                        if (w.preset.sectionClass == DoorPairPreset.WallSectionClass.window) continue;

                                        //Add random
                                        float score = Toolbox.Instance.Rand(0f, 1f);

                                        //The closer the better
                                        score += 10f - Vector3.Distance(w.position, victim.transform.position);

                                        //No blocking bonus
                                        if (!n.individualFurniture.Exists(item => item.furnitureClasses[0].noBlocking)) score -= 20f;
                                        if (n.individualFurniture.Count <= 0) score += 5;

                                        if(score > wallScore)
                                        {
                                            wallScore = score;
                                            bestWall = w;
                                        }
                                    }
                                }
                            }

                            if (bestWall != null)
                            {
                                position = bestWall.node.position;
                                euler = bestWall.localEulerAngles;
                            }
                            else return;
                        }
                        else return;
                    }

                    //Parse image
                    List<Interactable.Passed> pv = new List<Interactable.Passed>();

                    if (graf.artImage != null)
                    {
                        pv.Add(new Interactable.Passed (Interactable.PassedVarType.decal, 0f, graf.artImage.name ));
                    }
                    else
                    {
                        graffitiMsg = Strings.GetTextForComponent(graf.ddsMessageTextList, this);

                        //Put generated data into a string for this...
                        //TEXT | FONT | SIZE | COLOUR
                        string getText = graffitiMsg + "|" + murderer.handwriting.fontAsset.name + "|" + graf.size + "|" + ColorUtility.ToHtmlStringRGB(graf.color);

                        pv.Add(new Interactable.Passed(Interactable.PassedVarType.decalDynamicText, 0f, getText));
                    }

                    Game.Log("Murder: Creating new graffiti at " + position + ", euler " + euler);
                    Interactable newGraf = InteractableCreator.Instance.CreateWorldInteractable(graf.preset, murderer, murderer, null, position, euler, pv, null);

                    if(newGraf != null)
                    {
                        graffiti.Add(newGraf);
                        graffitiIDs.Add(newGraf.id);
                    }
                }
            }
        }

        public void OnCleanCrimeScene()
        {
            //Remove graffiti
            for (int i = 0; i < graffiti.Count; i++)
            {
                graffiti[i].Delete();
            }
            
            //Remove calling card when ready
            if(callingCard != null)
            {
                callingCard.MarkAsTrash(true);

                if (!callingCard.inInventory && !FirstPersonItemController.Instance.slots.Exists(item => item.GetInteractable() == callingCard))
                {
                    callingCard.RemoveFromPlacement();
                }
            }
        }
    }

    //Singleton pattern
    private static MurderController _instance;
    public static MurderController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void SetProcGenKillerLoop(bool val)
    {
        if(procGenLoopActive != val)
        {
            procGenLoopActive = val;

            if (procGenLoopActive)
            {
                Game.Log("Murder: Starting murder proc gen loop...");
                SetUpdateEnabled(true);
            }
            else
            {
                Game.Log("Murder: Stopping murder proc gen loop...");
            }
        }
    }

    //Updates with behaviour loop, passed time passed since last loop...
    public void Tick(float timePassed)
    {
        if(procGenLoopActive)
        {
            //Make sure we have a murderer...
            if (currentMurderer == null)
            {
                PickNewMurderer();
            }
            else
            {
                if(pauseBetweenMurders <= 0f)
                {
                    if (!murderRoutineActive)
                    {
                        //Wait for player to explore crime scene
                        if (currentActiveCase == null || !currentActiveCase.currentActiveObjectives.Exists(item => item.queueElement.entryRef == "Explore Reported Crime Scene"))
                        {
                            //Make sure we have a victim...
                            if (currentVictim == null)
                            {
                                PickNewVictim();
                            }
                            //Once we have both, we can start a new murder!
                            else
                            {
                                ExecuteNewMurder(currentMurderer, currentVictim, murderPreset, chosenMO);
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < activeMurders.Count; i++)
                        {
                            Murder m = activeMurders[i];

                            //Need/lost equipment...
                            if ((int)m.state <= 5)
                            {
                                m.EuipmentCheck();
                            }

                            if(m.murderGoal != null && !m.murderGoal.isActive)
                            {
                                if (m.state == MurderState.acquireEuipment || m.state == MurderState.travellingTo)
                                {
                                    if (m.murderer != null)
                                    {
                                        m.murderer.ai.AITick(true);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    pauseBetweenMurders -= timePassed; //Wait between murders
                }
            }
        }
    }

    public void PickNewMurderer()
    {
        Game.Log("Murder: Picking new murderer...");

        List<MurderPreset> possiblePresets = new List<MurderPreset>();

        foreach(MurderPreset mp in Toolbox.Instance.allMurderPresets)
        {
            for (int i = 0; i < mp.frequency; i++)
            {
                possiblePresets.Add(mp);
            }
        }

        murderPreset = possiblePresets[Toolbox.Instance.Rand(0, possiblePresets.Count)];

        Game.Log("Murder: ...Picked " + murderPreset.name + " as a preset...");

        List<MurderPick> validPicks = new List<MurderPick>();

        foreach (Citizen cit in CityData.Instance.citizenDirectory)
        {
            if (cit.isPlayer) continue;
            if (cit.isDead) continue;
            if (cit.isHomeless) continue; //For now, don't pick homeless citizens
            if (previousMurderers.Contains(cit as Human)) continue;
            if (currentVictim == cit) continue;

            //Begin scoring
            float score = Toolbox.Instance.Rand(murderPreset.murdererRandomScoreRange.x, murderPreset.murdererRandomScoreRange.y);

            float traitModifier = 0f;

            if(murderPreset.murdererTraitModifiers.Count > 0)
            {
                if (TraitTest(cit, ref murderPreset.murdererTraitModifiers, out traitModifier))
                {
                    score += traitModifier;
                }
                else continue; //Didn't pass the trait test so skip this...
            }

            if (murderPreset.useHexaco)
            {
                score += cit.GetHexacoScore(ref murderPreset.hexaco);
            }

            foreach (MurderMO wp in Toolbox.Instance.allMurderMOs)
            {
                if (wp.disabled) continue;
                if (!wp.compatibleWith.Contains(murderPreset)) continue;

                float scoreMO = score + Toolbox.Instance.Rand(wp.pickRandomScoreRange.x, wp.pickRandomScoreRange.y);

                float traitModifierMO = 0f;

                if (wp.murdererTraitModifiers.Count > 0)
                {
                    if (TraitTest(cit, ref wp.murdererTraitModifiers, out traitModifierMO))
                    {
                        scoreMO += traitModifierMO;
                    }
                    else continue; //Didn't pass the trait test so skip this...
                }

                if(wp.useHexaco)
                {
                    scoreMO += cit.GetHexacoScore(ref wp.hexaco);
                }

                //Apply job modifiers
                foreach (MurderMO.JobModifier j in wp.murdererJobModifiers)
                {
                    if (cit.job != null && j.jobs.Contains(cit.job.preset))
                    {
                        scoreMO += j.jobBoost;
                    }
                }

                foreach (MurderMO.CompanyModifier j in wp.murdererCompanyModifiers)
                {
                    if (cit.job != null && cit.job.employer != null && j.companies.Contains(cit.job.employer.preset))
                    {
                        List<Occupation> employees = cit.job.employer.companyRoster.FindAll(item => item.employee != null);

                        if (employees.Count >= j.mininumEmployees)
                        {
                            scoreMO += j.companyBoost;
                            scoreMO += (employees.Count - j.mininumEmployees) * j.boostPerEmployeeOverMinimum;
                        }
                    }
                }

                //Apply class range
                if (wp.useMurdererSocialClassRange)
                {
                    if (cit.societalClass >= wp.murdererClassRange.x && cit.societalClass <= wp.murdererClassRange.y)
                    {
                        scoreMO += wp.murdererClassRangeBoost;
                    }
                }

                //Is this below the maximum difficulty level?
                if (wp.baseDifficulty <= MurderController.Instance.maxDifficultyLevel) scoreMO += 12;

                //Penalty for previously picked MOs
                if (MurderController.Instance.inactiveMurders.Exists(item => item.mo == wp)) scoreMO -= 50;

                MurderPick newPick = new MurderPick { person = cit as Human, score = scoreMO, mo = wp,  };
                validPicks.Add(newPick);
            }
        }

        //Hopefully we have some good picks now, sort them.
        validPicks.Sort((p1, p2) => p2.score.CompareTo(p1.score)); //Using P2 first gives highest first

        if(Game.Instance.devMode && Game.Instance.collectDebugData)
        {
            debugLastMurderPicks = validPicks;
        }

        if(validPicks.Count <= 0)
        {
            Game.LogError("Cannot find new valid murderer!");
        }
        else
        {
            currentMurderer = validPicks[0].person;
            chosenMO = validPicks[0].mo;

            Game.Log("Murder: Chosen " + currentMurderer.GetCitizenName() + " to be new murderer of type " + murderPreset.name + " with MO " + chosenMO.name);

            if (Game.Instance.debugMurdererOnStart && Game.Instance.collectDebugData)
            {
                currentMurderer.ai.ToggleHumanDebug();
            }

            UpdateResolveQuestions(true);
        }
    }

    //Is a new case needed?
    public void OnVictimDiscovery()
    {
        if (Game.Instance.sandboxMode || (ChapterController.Instance != null && (ChapterController.Instance.chapterScript as ChapterIntro) != null && (ChapterController.Instance.chapterScript as ChapterIntro).completed))
        { 
            //Do we need to start a new case?
            if (!CasePanelController.Instance.activeCases.Exists(item => item.caseType == Case.CaseType.murder && (item.caseStatus != Case.CaseStatus.closable && item.caseStatus != Case.CaseStatus.submitted && item.caseStatus != Case.CaseStatus.archived)))
            {
                Case newMainCase = CasePanelController.Instance.CreateNewCase(Case.CaseType.murder, Case.CaseStatus.handInNotCollected, true, caseName: Strings.Get("ui.interface", "New Murder Case"));
                AssignActiveCase(newMainCase);

                SessionData.Instance.TutorialTrigger("murders");
            }
        }
    }

    public void AssignActiveCase(Case newCase)
    {
        if(currentActiveCase != newCase)
        {
            currentActiveCase = newCase;
        }

        UpdateResolveQuestions(true);
    }

    //Update the answers to the resolve questions...
    public void UpdateCorrectResolveAnswers()
    {
        if(currentActiveCase != null)
        {
            Game.Log("Chapter: Updating resolve answers on " + currentActiveCase.name + " with " + activeMurders.Count + " active murders...");

            foreach(Case.ResolveQuestion q in currentActiveCase.resolveQuestions)
            {
                q.correctAnswers.Clear();

                Game.Log("Chapter: Getting new correct answer for " + q.name + "...");

                //Murderer's name
                if (q.tag == JobPreset.JobTag.A)
                {
                    foreach(Murder m in activeMurders)
                    {
                        if(!q.correctAnswers.Contains(m.murdererID.ToString()))
                        {
                            Game.Log("Chapter: Question A: Found murderer ID " + m.murdererID + "...");
                            q.correctAnswers.Add(m.murdererID.ToString());
                        }
                    }
                }
                //Arrest location... (OLD)
                //else if(q.tag == JobPreset.JobTag.B)
                //{
                //    foreach (Murder m in activeMurders)
                //    {
                //        if (m.murderer != null && m.murderer.ai.restrained)
                //        {
                //            Game.Log("Chapter: Question B: Found restrained murderer " + m.murderer.name + "...");

                //            if (m.murderer.currentGameLocation.thisAsAddress != null)
                //            {
                //                if(!q.correctAnswers.Contains(m.murderer.currentGameLocation.thisAsAddress.id.ToString())) q.correctAnswers.Add(m.murderer.currentGameLocation.thisAsAddress.id.ToString());
                //            }
                //            else if (m.murderer.currentGameLocation.thisAsStreet != null)
                //            {
                //                if (!q.correctAnswers.Contains(m.murderer.currentGameLocation.thisAsStreet.streetID.ToString())) q.correctAnswers.Add(m.murderer.currentGameLocation.thisAsStreet.streetID.ToString());
                //            }
                //        }
                //    }
                //}
                //Murderer's address...
                else if (q.tag == JobPreset.JobTag.C)
                {
                    foreach (Murder m in activeMurders)
                    {
                        Game.Log("Chapter: Question C: Found murderer " + m.murderer.GetCitizenName() + "...");

                        if (m.murderer != null && m.murderer.home != null && !q.correctAnswers.Contains(m.murderer.home.id.ToString()))
                        {
                            q.correctAnswers.Add(m.murderer.home.id.ToString());
                        }
                    }
                }
                //Murderer's next target... (OLD)
                //else if (q.tag == JobPreset.JobTag.D)
                //{
                //    //If this is story mode...
                //    if (currentActiveCase.caseType == Case.CaseType.mainStory)
                //    {
                //        q.correctAnswers.Add((ChapterController.Instance.chapterScript as ChapterIntro).noteWriterID.ToString());
                //    }
                //    else if(currentVictim != null)
                //    {
                //        q.correctAnswers.Add(currentVictim.humanID.ToString());
                //    }
                //}
                //Located murder weapon...
                else if (q.tag == JobPreset.JobTag.E)
                {
                    if (currentActiveCase.caseType == Case.CaseType.mainStory)
                    {
                        if(ChapterController.Instance.chapterScript != null)
                        {
                            ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                            if (intro != null && intro.murderWeapon != null)
                            {
                                q.correctAnswers.Add(intro.murderWeapon.evidence.evID);
                                Game.Log("Chapter: Question E: Found murder weapon...");
                            }
                        }
                    }
                    else
                    {
                        foreach (Murder m in activeMurders)
                        {
                            if(m.weapon != null)
                            {
                                Evidence weapEv = m.weapon.evidence;

                                if(weapEv != null)
                                {
                                    if (!q.correctAnswers.Contains(weapEv.evID))
                                    {
                                        q.correctAnswers.Add(weapEv.evID);
                                        Game.Log("Chapter: Question E: Found murder weapon...");
                                    }
                                }
                                else
                                {
                                    if (!q.correctAnswers.Contains(m.weapon.id.ToString()))
                                    {
                                        q.correctAnswers.Add(m.weapon.id.ToString());
                                        Game.Log("Chapter: Question E: Found murder weapon...");
                                    }
                                }
                            }
                        }
                    }
                }
                //Evidence that places killer at the crime scene...
                else if (q.tag == JobPreset.JobTag.F)
                {
                    //Is this a fingerprint?
                    if (q.inputtedEvidence != null && q.inputtedEvidence.Length > 0)
                    {
                        Evidence found = null;

                        //To know finerprints, the player must have tied this to the killer's name
                        if (GameplayController.Instance.evidenceDictionary.TryGetValue(q.inputtedEvidence, out found))
                        {
                            EvidenceFingerprint print = found as EvidenceFingerprint;

                            //Is fingerprint...
                            if(print != null)
                            {
                                if (currentActiveCase.caseType == Case.CaseType.mainStory)
                                {
                                    if (ChapterController.Instance.chapterScript != null)
                                    {
                                        ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                                        //Fingerprints
                                        if(intro != null && intro.killer != null && intro.killer.evidenceEntry.GetTiedKeys(Evidence.DataKey.name).Contains(Evidence.DataKey.fingerprints))
                                        {
                                            if (intro.killer == print.belongsTo)
                                            {
                                                q.correctAnswers.Add(q.inputtedEvidence);
                                                Game.Log("Chapter: Question F: Found valid print...");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (Murder m in activeMurders)
                                    {
                                        //Fingerprints
                                        if (m.murderer != null && m.murderer.evidenceEntry.GetTiedKeys(Evidence.DataKey.name).Contains(Evidence.DataKey.fingerprints))
                                        {
                                            if (print.belongsTo == m.murderer)
                                            {
                                                q.correctAnswers.Add(q.inputtedEvidence);
                                                Game.Log("Chapter: Question F: Found valid print...");
                                            }
                                        }
                                    }
                                }
                            }

                            if(found.interactable != null)
                            {
                                foreach (Murder m in activeMurders)
                                {
                                    if (m.murderer != null && m.murderer.evidenceEntry.GetTiedKeys(Evidence.DataKey.name).Contains(Evidence.DataKey.fingerprints))
                                    {
                                        if (m.callingCardID == found.interactable.id)
                                        {
                                            q.correctAnswers.Add(q.inputtedEvidence);
                                            Game.Log("Chapter: Question F: Found valid calling card...");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                q.UpdateValid(currentActiveCase);
            }
        }
    }

    public void UpdateResolveQuestions(bool clearExisting)
    {
        if (currentActiveCase == null)
        {
            Game.Log("Gameplay: Cannot update resolve questions; no active case");
            return;
        }

        Game.Log("Gameplay: Updating resolve questions...");

        if (clearExisting)
        {
            currentActiveCase.resolveQuestions.Clear();
        }

        foreach (Case.ResolveQuestion rq in GameplayControls.Instance.murderResolveQuestions)
        {
            Case.ResolveQuestion newRq = new Case.ResolveQuestion();
            newRq.name = rq.name;
            newRq.inputType = rq.inputType;
            newRq.tag = rq.tag;
            newRq.rewardRange = rq.rewardRange;
            newRq.isOptional = rq.isOptional;
            newRq.displayObjective = rq.displayObjective;
            newRq.displayAtPhase = rq.displayAtPhase;
            newRq.displayOnlyAtPhase = rq.displayOnlyAtPhase;
            newRq.objectiveDelay = rq.objectiveDelay;
            newRq.revengeObjective = rq.revengeObjective;

            //Pick reward
            newRq.reward = Mathf.RoundToInt((Toolbox.Instance.VectorToRandom(rq.rewardRange) * Game.Instance.jobRewardMultiplier) / 50f) * 50;
            newRq.penalty = Mathf.RoundToInt((Toolbox.Instance.VectorToRandom(rq.penaltyRange) * Game.Instance.jobPenaltyMultiplier) / 50f) * 50;

            Game.Log("Adding resolve question: " + newRq.name);
            currentActiveCase.resolveQuestions.Add(newRq);
        }

        UpdateCorrectResolveAnswers();
    }

    public void PickNewVictim()
    {
        Game.Log("Murder: Picking new victim...");

        float scoreToBeat = -99999f;
        currentVictim = null;

        foreach (Citizen cit in CityData.Instance.citizenDirectory)
        {
            if (cit.isPlayer) continue;
            if (cit.isDead) continue;
            if (currentMurderer == cit) continue;
            if (cit.isHomeless) continue; //For now, don't pick homeless citizens
            if (previousMurderers.Contains(cit as Human)) continue;

            float score = Toolbox.Instance.Rand(chosenMO.victimRandomScoreRange.x, chosenMO.victimRandomScoreRange.y);

            //Test killer acquaintances
            if (currentMurderer.attractedTo.Contains(cit.gender))
            {
                score += chosenMO.attractedToSuitabilityBoost;
            }

            Acquaintance aq = null;

            if (currentMurderer.FindAcquaintanceExists(cit as Human, out aq))
            {
                score += chosenMO.acquaintedSuitabilityBoost;
                score += chosenMO.likeSuitabilityBoost * aq.like;
            }

            if(cit.job != null && currentMurderer != null && currentMurderer.job != null)
            {
                if(cit.job.employer != null)
                {
                    if(cit.job.employer == currentMurderer.job.employer)
                    {
                        score += chosenMO.sameWorkplaceBoost;
                    }
                }
            }

            if(chosenMO.murdererIsTenantBoost != 0)
            {
                if (cit.GetLandlord() == currentMurderer)
                {
                    score += chosenMO.murdererIsTenantBoost;
                }
            }

            float traitScore = 0f;

            //test against the chosen victim rule
            if (TraitTest(cit, ref chosenMO.victimTraitModifiers, out traitScore))
            {
                score += traitScore;
            }
            else continue;

            //Apply job modifiers
            foreach(MurderMO.JobModifier j in chosenMO.victimJobModifiers)
            {
                if(cit.job != null && j.jobs.Contains(cit.job.preset))
                {
                    score += j.jobBoost;
                }
            }

            foreach (MurderMO.CompanyModifier j in chosenMO.victimCompanyModifiers)
            {
                if (cit.job != null && cit.job.employer != null && j.companies.Contains(cit.job.employer.preset))
                {
                    List<Occupation> employees = cit.job.employer.companyRoster.FindAll(item => item.employee != null);

                    if (employees.Count >= j.mininumEmployees)
                    {
                        score += j.companyBoost;
                        score += (employees.Count - j.mininumEmployees) * j.boostPerEmployeeOverMinimum;
                    }
                }
            }

            //Apply class range
            if (chosenMO.useVictimSocialClassRange)
            {
                if (cit.societalClass >= chosenMO.victimClassRange.x && cit.societalClass <= chosenMO.victimClassRange.y)
                {
                    score += chosenMO.victimClassRangeBoost;
                }
            }

            if (score > scoreToBeat)
            {
                currentVictim = cit as Human;
                scoreToBeat = score;
            }
        }

        if (currentVictim == null)
        {
            Game.LogError("Cannot find new valid victim!");
        }
        else
        {
            Game.Log("Murder: Chosen " + currentVictim.GetCitizenName() + " to be new victim...");
        }
    }

    //Returns bool: Whether this should be added; output float modifier;
    public bool TraitTest(Citizen cit, ref List<MurderPreset.MurdererModifierRule> rules, out float output)
    {
        output = 1f;

        //Check picking rules
        bool passRules = true;

        foreach (MurderPreset.MurdererModifierRule rule in rules)
        {
            bool pass = false;

            if (rule.rule == CharacterTrait.RuleType.ifAnyOfThese)
            {
                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (searchTrait == null) continue;

                    if (cit.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = true;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifAllOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (searchTrait == null) continue;

                    if (!cit.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifNoneOfThese)
            {
                pass = true;

                foreach (CharacterTrait searchTrait in rule.traitList)
                {
                    if (searchTrait == null) continue;

                    if (cit.characterTraits.Exists(item => item.trait == searchTrait))
                    {
                        pass = false;
                        break;
                    }
                }
            }
            else if (rule.rule == CharacterTrait.RuleType.ifPartnerAnyOfThese)
            {
                if (cit.partner != null)
                {
                    foreach (CharacterTrait searchTrait in rule.traitList)
                    {
                        if (searchTrait == null) continue;

                        if (cit.partner.characterTraits.Exists(item => item.trait == searchTrait))
                        {
                            pass = true;
                            break;
                        }
                    }
                }
                else pass = false;
            }

            if (pass)
            {
                output += rule.scoreModifier;
            }
            else if (rule.mustPassForApplication)
            {
                passRules = false;
            }
        }

        return passRules;
    }

    //Execute a murder with passed murderer/victim
    public Murder ExecuteNewMurder(Human newMurderer, Human newVictim, MurderPreset preset, MurderMO motive)
    {
        Game.Log("Murder: Creating new murder of type " + preset.name + " with murderer " + newMurderer.GetCitizenName() + " and victim " + newVictim.GetCitizenName());

        Murder newMurder = new Murder(newMurderer, newVictim, preset, motive);
        murderRoutineActive = true;

        //Enable this if a murder is active
        SetUpdateEnabled(true);

        return newMurder;
    }

    private void Update()
    {
        int unsolvedState = 0;

        //Update murders
        for (int i = 0; i < activeMurders.Count; i++)
        {
            Murder m = activeMurders[i];

            if (m.state == MurderState.acquireEuipment)
            {
                //If not required, skip to travel phase...
                if (!m.preset.requiresAcquirePhase)
                {
                    m.SetMurderState(MurderState.research);
                }
            }

            //Research phase...
            if (m.state == MurderState.research)
            {
                //If not required, skip to travel phase...
                if (!m.preset.requiresResearchPhase)
                {
                    m.SetMurderState(MurderState.waitForLocation);
                }
                else
                {
                    //Move onto next phase
                    //If not in inventory?
                    if (m.murderer.inventory.Exists(item => item.preset == m.weaponPreset))
                    {
                        if (m.ammoPreset != null)
                        {
                            if (m.murderer.inventory.Exists(item => item.preset == m.ammoPreset))
                            {
                                m.SetMurderState(MurderState.waitForLocation);
                            }
                        }
                        else m.SetMurderState(MurderState.waitForLocation);
                    }
                }
            }

            //Wait for a valid crime location...
            if (m.state == MurderState.waitForLocation)
            {
                if(SessionData.Instance.play)
                {
                    //A valid location...
                    if(m.IsValidLocation(m.victim.currentGameLocation))
                    {
                        m.SetMurderLocation(m.victim.currentGameLocation);
                        m.SetMurderState(MurderState.travellingTo);
                    }
                    else
                    {
                        //How long have I been waiting?
                        float timeWaiting = SessionData.Instance.gameTime - m.waitingTimestamp;

                        if(timeWaiting > 0.25f)
                        {
                            if((m.mo.allowAnywhere || m.mo.allowHome) && m.victim.home != null)
                            {
                                //Otherwise send victim to location
                                if (!m.victim.ai.goals.Exists(item => item.preset == RoutineControls.Instance.toGoGoal && item.passedNode != null && item.passedNode.gameLocation == m.victim.home))
                                {
                                    Game.Log("Murder: Waiting for too long! Creating GoTo home routine for victim " + m.victim.GetCitizenName() + " to: " + m.victim.home.name);
                                    m.victim.ai.CreateNewGoal(RoutineControls.Instance.toGoGoal, 0f, 0f, newPassedGameLocation: m.victim.home, newPassedNode: m.victim.FindSafeTeleport(m.victim.home));
                                }
                            }
                            else if ((m.mo.allowAnywhere || m.mo.allowWork) && m.victim.job != null && m.victim.job.employer != null && m.victim.job.employer.placeOfBusiness != null)
                            {
                                //Otherwise send victim to location
                                if (!m.victim.ai.goals.Exists(item => item.preset == RoutineControls.Instance.toGoGoal && item.passedNode != null && item.passedNode.gameLocation == m.victim.job.employer.placeOfBusiness))
                                {
                                    Game.Log("Murder: Waiting for too long! Creating GoTo work routine for victim " + m.victim.GetCitizenName() + " to: " + m.victim.job.employer.placeOfBusiness.name);
                                    m.victim.ai.CreateNewGoal(RoutineControls.Instance.toGoGoal, 0f, 0f, newPassedGameLocation: m.victim.job.employer.placeOfBusiness, newPassedNode: m.victim.FindSafeTeleport(m.victim.job.employer.placeOfBusiness));
                                }
                            }
                            else if(m.mo.allowAnywhere || m.mo.allowStreets)
                            {
                                StreetController street = null;

                                foreach (StreetController st in CityData.Instance.streetDirectory)
                                {
                                    if(street == null || (st.currentOccupants.Count <= m.preset.nonHomeMaximumOccupantsTrigger && st.currentOccupants.Count < street.currentOccupants.Count))
                                    {
                                        street = st;
                                    }
                                }

                                if(street != null)
                                {
                                    //Otherwise send victim to location
                                    if (!m.victim.ai.goals.Exists(item => item.preset == RoutineControls.Instance.toGoGoal))
                                    {
                                        Game.Log("Murder: Waiting for too long! Creating GoTo street routine for victim " + m.victim.GetCitizenName() + " to: " + street.name);
                                        m.victim.ai.CreateNewGoal(RoutineControls.Instance.toGoGoal, 0f, 0f, newPassedGameLocation: street, newPassedNode: m.victim.FindSafeTeleport(street));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Travel to
            if (m.state == MurderState.travellingTo)
            {
                if (SessionData.Instance.play)
                {
                    //The current location is a valid location...
                    if (m.IsValidLocation(m.victim.currentGameLocation))
                    {
                        //Check for at same valid location
                        if (m.murderer.currentGameLocation == m.victim.currentGameLocation)
                        {
                            Game.Log("Murder: Murderer location: " + m.murderer.currentGameLocation.name + " victim location: " + m.victim.currentGameLocation.name);
                            m.SetMurderLocation(m.murderer.currentGameLocation);
                            m.SetMurderState(MurderState.executing);
                        }
                    }
                    else
                    {
                        //Game.Log("Murder: Location " + m.victim.currentGameLocation.name + " has too many occupants for the murder to trigger (" + m.victim.currentGameLocation.currentOccupants.Count + "/" + m.preset.nonHomeMaximumOccupantsTrigger + ")");

                        //Cancel murder
                        if (m.location != null && m.location != m.victim.home && m.location.currentOccupants.Count > m.preset.nonHomeMaximumOccupantsCancel)
                        {
                            Game.Log("Murder: Cancelled murder because of too many occupants at " + m.location.name + " (" + m.location.currentOccupants.Count + "/" + m.preset.nonHomeMaximumOccupantsTrigger + ")");
                            m.SetMurderLocation(null);
                            m.SetMurderState(MurderState.waitForLocation);
                        }
                        else if (m.murderer.currentGameLocation == m.location && m.victim.currentGameLocation != m.location)
                        {
                            Game.Log("Murder: Cancelled murder because victim is not at " + m.location.name + " when killer arrived.");
                            m.SetMurderLocation(null);
                            m.SetMurderState(MurderState.waitForLocation);
                        }
                    }
                }
            }

            if (m.state == MurderState.executing)
            {
                //Once dead, move on...
                if (m.victim.isDead)
                {
                    m.SetMurderState(MurderState.post);
                }
                else
                {
                    if(SessionData.Instance.play)
                    {
                        if (m.murderer.currentNerve < m.murderer.maxNerve) m.murderer.SetNerve(m.murderer.maxNerve); //Make sure the killer doesn't lose nerve

                        //Behave slightly differently if posioned; all it takes is one posioning to do the job...
                        if (m.weaponPreset.weapon.type != MurderWeaponPreset.WeaponType.poison || m.victim.poisoner != m.murderer)
                        {
                            //Make sure the killer 'always sees' the victim to enable persuit no matter what
                            if (!m.murderer.seesIllegal.ContainsKey(m.victim))
                            {
                                m.murderer.seesIllegal.Add(m.victim, 1);
                            }
                            else m.murderer.seesIllegal[m.victim] = 1;

                            if (locationUpdateTimer <= 0)
                            {
                                if (m.victim.currentNerve > 0f)
                                {
                                    m.victim.SetNerve(0f);
                                    m.victim.OnZeroNerveReached();
                                }

                                m.murderer.ai.SetPersue(m.victim, false, 2, true);
                                locationUpdateTimer = 1.5f;
                            }
                            else
                            {
                                locationUpdateTimer -= Time.deltaTime;
                            }
                        }
                        else if (m.weaponPreset.weapon.type == MurderWeaponPreset.WeaponType.poison && m.victim.poisoner == m.murderer)
                        {
                            if (m.murderer.ai.persuit)
                            {
                                Game.Log("Murder: Victim has been poisioned! Wait for them to die...");
                                m.murderer.ai.CancelPersue();
                            }
                        }
                    }
                }
            }

            if (m.state == MurderState.post)
            {
                if (m.murderGoal.isActive && m.murderGoal.actions.Count <= 0)
                {
                    m.SetMurderState(MurderState.escaping);
                }
            }

            if (m.state == MurderState.escaping)
            {
                //Escaped when out of gamelocation
                if (m.murderer.currentGameLocation != m.location)
                {
                    //Must also be out of any adjoining addresses...
                    bool clearOfAdjoining = true;

                    foreach (NewNode.NodeAccess acc in m.location.entrances)
                    {
                        if (m.murderer.currentGameLocation == acc.fromNode.gameLocation)
                        {
                            clearOfAdjoining = false;
                            break;
                        }

                        if (m.murderer.currentGameLocation == acc.toNode.gameLocation)
                        {
                            clearOfAdjoining = false;
                            break;
                        }
                    }

                    if (clearOfAdjoining)
                    {
                        m.CancelCurrentMurder();
                        i--;
                    }
                }
            }
            else if (m.state == MurderState.unsolved)
            {
                unsolvedState++;
            }
        }

        //Disable if all inactive
        if(activeMurders.Count <= 0 || (unsolvedState > 0 && activeMurders.Count == unsolvedState))
        {
            Game.Log("Murder: All murders completed or in unsolved state, deactivating murder controller update...");
            currentVictim = null; //Set victim to null so we pick another...
            if((previousMurderers.Count > 0 || activeMurders.Count > 0) && murderPreset != null) pauseBetweenMurders = murderPreset.minimumTimeBetweenMurders; //Set minimum time between murders
            murderRoutineActive = false;
            SetUpdateEnabled(false);
        }
    }

    public void SetUpdateEnabled(bool val)
    {
        Game.Log("Murder: Update Enabled: " + val);
        this.enabled = val;
    }

    public void OnStartGame()
    {
        //Update murderer priorities
        for (int i = 0; i < activeMurders.Count; i++)
        {
            Murder m = activeMurders[i];

            if(m != null)
            {
                if(m.murderer != null)
                {
                    Game.Log("Murder: Forcing update of priorities for " + m.murderer.citizenName);
                    m.murderer.ai.AITick(true);
                }
            }
        }
    }

    [Button]
    public void TriggerNextMurder()
    {
        pauseBetweenMurders = 0f;

        if (currentActiveCase != null)
        {
            Objective ex = currentActiveCase.currentActiveObjectives.Find(item => item.queueElement.entryRef == "Explore Reported Crime Scene");

            //Force complete
            if (ex != null)
            {
                ex.progress = 1f;
                ex.Complete();
            }
        }
    }

    public virtual void SpawnItemsCheck(Murder murder)
    {
        if (murderPreset == null) return;

        List<MurderPreset.MurderLeadItem> toSpawn = murderPreset.leads.FindAll(item => item.spawnOnPhase == murder.state);
        toSpawn.AddRange(murder.mo.MOleads.FindAll(item => item.spawnOnPhase == murder.state)); //Add MO leads too

        List<MurderPreset.MurderLeadItem> successsfullySpawned = new List<MurderPreset.MurderLeadItem>();

        int safety = 999;

        while (toSpawn.Count > 0 && safety > 0)
        {
            MurderPreset.MurderLeadItem spawn = toSpawn[0];

            //Check compatible with motive
            if (!SpawnItemIsValid(murder, spawn, ref successsfullySpawned, !spawn.useOrGroup))
            {
                toSpawn.RemoveAt(0);
                safety--;
                continue;
            }

            List<MurderPreset.MurderLeadItem> orGroupPool = null;

            //Only spawn one of these type...
            if (spawn.useOrGroup)
            {
                orGroupPool = new List<MurderPreset.MurderLeadItem>();
                List<MurderPreset.MurderLeadItem> groupSinglePool = toSpawn.FindAll(item => item.useOrGroup && item.orGroup == spawn.orGroup && SpawnItemIsValid(murder, item, ref successsfullySpawned, false));

                //Create a pool using chances...
                for (int u = 0; u < groupSinglePool.Count; u++)
                {
                    for (int l = 0; l < Mathf.RoundToInt(groupSinglePool[u].chanceRatio); l++)
                    {
                        orGroupPool.Add(groupSinglePool[u]);
                    }
                }

                spawn = orGroupPool[Toolbox.Instance.Rand(0, orGroupPool.Count)]; //Change the object to one of these...
            }

            //Spawn object...
            if (spawn.spawnItem != null)
            {
                Game.Log("Murder: " + murderPreset.name + ": Spawn item " + spawn.name + ", tag: " + spawn.itemTag.ToString() + ", use if " + spawn.useIf + ": " + spawn.ifTag.ToString() + ", use or group " + spawn.useOrGroup + ": " + spawn.orGroup.ToString() + "...");
                Interactable newItem = SpawnItem(murder, spawn.spawnItem, spawn.where, spawn.belongsTo, spawn.writer, spawn.receiver, spawn.security, spawn.ownershipRule, spawn.priority, spawn.itemTag);

                successsfullySpawned.Add(spawn);
            }

            //Start VMAIL thread
            if (spawn.vmailThread != null && spawn.vmailThread.Length > 0)
            {
                Human belongsTo = null;

                if (spawn.belongsTo == MurderPreset.LeadCitizen.victim) belongsTo = murder.victim;
                else if (spawn.belongsTo == MurderPreset.LeadCitizen.killer) belongsTo = murder.murderer;
                else if (spawn.belongsTo == MurderPreset.LeadCitizen.victimsClosest)
                {
                    float like = -9999;

                    foreach(Acquaintance aq in murder.victim.acquaintances)
                    {
                        if(aq.like + aq.known > like)
                        {
                            belongsTo = aq.with;
                            like = aq.like + aq.known;
                        }
                    }
                }
                else if (spawn.belongsTo == MurderPreset.LeadCitizen.killersClosest)
                {
                    float like = -9999;

                    foreach (Acquaintance aq in murder.murderer.acquaintances)
                    {
                        if (aq.like + aq.known > like)
                        {
                            belongsTo = aq.with;
                            like = aq.like + aq.known;
                        }
                    }
                }
                else if (spawn.belongsTo == MurderPreset.LeadCitizen.victimsDoctor)
                {
                    belongsTo = murder.victim.GetDoctor();
                }
                else if (spawn.belongsTo == MurderPreset.LeadCitizen.killersDoctor)
                {
                    belongsTo = murder.murderer.GetDoctor();
                }

                Toolbox.Instance.NewVmailThread(belongsTo, new List<Human>(), spawn.vmailThread, SessionData.Instance.gameTime + Toolbox.Instance.Rand(-12f, -6f), Mathf.RoundToInt(Toolbox.Instance.Rand(spawn.vmailProgressThreshold.x, spawn.vmailProgressThreshold.y)));
            }

            //Remove from pool...
            toSpawn.Remove(spawn);

            if (orGroupPool != null)
            {
                foreach (MurderPreset.MurderLeadItem sp in orGroupPool)
                {
                    toSpawn.Remove(sp);
                }
            }

            safety--;
        }
    }

    private bool SpawnItemIsValid(Murder murder, MurderPreset.MurderLeadItem spawn, ref List<MurderPreset.MurderLeadItem> successsfullySpawned, bool useChance)
    {
        if (useChance)
        {
            float chance = spawn.chance;

            //Check picking rules
            bool passRules = true;

            if (spawn.useTraits)
            {
                foreach (MurderPreset.MurderModifierRule rule in spawn.traitModifiers)
                {
                    bool pass = false;

                    Human cit = null;
                    if (rule.who == MurderPreset.LeadCitizen.killer) cit = murder.murderer;
                    else if (rule.who == MurderPreset.LeadCitizen.victim) cit = murder.victim;

                    if (cit != null)
                    {
                        if (rule.rule == CharacterTrait.RuleType.ifAnyOfThese)
                        {
                            foreach (CharacterTrait searchTrait in rule.traitList)
                            {
                                if (cit.characterTraits.Exists(item => item.trait == searchTrait))
                                {
                                    pass = true;
                                    break;
                                }
                            }
                        }
                        else if (rule.rule == CharacterTrait.RuleType.ifAllOfThese)
                        {
                            pass = true;

                            foreach (CharacterTrait searchTrait in rule.traitList)
                            {
                                if (!cit.characterTraits.Exists(item => item.trait == searchTrait))
                                {
                                    pass = false;
                                    break;
                                }
                            }
                        }
                        else if (rule.rule == CharacterTrait.RuleType.ifNoneOfThese)
                        {
                            pass = true;

                            foreach (CharacterTrait searchTrait in rule.traitList)
                            {
                                if (cit.characterTraits.Exists(item => item.trait == searchTrait))
                                {
                                    pass = false;
                                    break;
                                }
                            }
                        }
                        else if (rule.rule == CharacterTrait.RuleType.ifPartnerAnyOfThese)
                        {
                            if (cit.partner != null)
                            {
                                foreach (CharacterTrait searchTrait in rule.traitList)
                                {
                                    if (cit.partner.characterTraits.Exists(item => item.trait == searchTrait))
                                    {
                                        pass = true;
                                        break;
                                    }
                                }
                            }
                            else pass = false;
                        }
                    }

                    if (pass)
                    {
                        chance += rule.chanceModifier;
                    }
                    else if (rule.mustPassForApplication)
                    {
                        passRules = false;
                    }
                }
            }

            if (!passRules || Toolbox.Instance.Rand(0f, 1f) > chance) return false;
        }

        //Check compatible with motive
        if (!spawn.compatibleWithMotives.Contains(chosenMO))
        {
            return false;
        }

        //Only spawn if a certain other item has been spawned already...
        if (spawn.useIf)
        {
            if (!successsfullySpawned.Exists(item => item.itemTag == spawn.itemTag))
            {
                return false;
            }
        }

        return true;
    }

    public Interactable SpawnItem(Murder murder, InteractablePreset spawnItem, MurderPreset.LeadSpawnWhere spawnWhere, MurderPreset.LeadCitizen spawnBelongsTo, MurderPreset.LeadCitizen spawnWriter, MurderPreset.LeadCitizen spawnReceiver, int security, InteractablePreset.OwnedPlacementRule ownedRule, int priority, JobPreset.JobTag itemTag)
    {
        Human belongsTo = null;
        Human writer = null;
        Human receiver = null;

        if (spawnBelongsTo == MurderPreset.LeadCitizen.victim) belongsTo = murder.victim;
        else if (spawnBelongsTo == MurderPreset.LeadCitizen.killer) belongsTo = murder.murderer;
        else if (spawnBelongsTo == MurderPreset.LeadCitizen.victimsClosest)
        {
            float like = -9999;

            foreach (Acquaintance aq in murder.victim.acquaintances)
            {
                if (aq.like + aq.known > like)
                {
                    belongsTo = aq.with;
                    like = aq.like + aq.known;
                }
            }
        }
        else if (spawnBelongsTo == MurderPreset.LeadCitizen.killersClosest)
        {
            float like = -9999;

            foreach (Acquaintance aq in murder.murderer.acquaintances)
            {
                if (aq.like + aq.known > like)
                {
                    belongsTo = aq.with;
                    like = aq.like + aq.known;
                }
            }
        }
        else if (spawnBelongsTo == MurderPreset.LeadCitizen.victimsDoctor)
        {
            belongsTo = murder.victim.GetDoctor();
        }
        else if (spawnBelongsTo == MurderPreset.LeadCitizen.killersDoctor)
        {
            belongsTo = murder.murderer.GetDoctor();
        }

        if (spawnWriter == MurderPreset.LeadCitizen.victim) writer = murder.victim;
        else if (spawnWriter == MurderPreset.LeadCitizen.killer) writer = murder.murderer;
        else if (spawnWriter == MurderPreset.LeadCitizen.victimsClosest)
        {
            float like = -9999;

            foreach (Acquaintance aq in murder.victim.acquaintances)
            {
                if (aq.like + aq.known > like)
                {
                    writer = aq.with;
                    like = aq.like + aq.known;
                }
            }
        }
        else if (spawnWriter == MurderPreset.LeadCitizen.killersClosest)
        {
            float like = -9999;

            foreach (Acquaintance aq in murder.murderer.acquaintances)
            {
                if (aq.like + aq.known > like)
                {
                    writer = aq.with;
                    like = aq.like + aq.known;
                }
            }
        }
        else if (spawnWriter == MurderPreset.LeadCitizen.victimsDoctor)
        {
            writer = murder.victim.GetDoctor();
        }
        else if (spawnWriter == MurderPreset.LeadCitizen.killersDoctor)
        {
            writer = murder.murderer.GetDoctor();
        }

        if (spawnReceiver == MurderPreset.LeadCitizen.victim) receiver = murder.victim;
        else if (spawnReceiver == MurderPreset.LeadCitizen.killer) receiver = murder.murderer;
        else if (spawnReceiver == MurderPreset.LeadCitizen.victimsClosest)
        {
            float like = -9999;

            foreach (Acquaintance aq in murder.victim.acquaintances)
            {
                if (aq.like + aq.known > like)
                {
                    receiver = aq.with;
                    like = aq.like + aq.known;
                }
            }
        }
        else if (spawnReceiver == MurderPreset.LeadCitizen.killersClosest)
        {
            float like = -9999;

            foreach (Acquaintance aq in murder.murderer.acquaintances)
            {
                if (aq.like + aq.known > like)
                {
                    receiver = aq.with;
                    like = aq.like + aq.known;
                }
            }
        }
        else if (spawnReceiver == MurderPreset.LeadCitizen.victimsDoctor)
        {
            receiver = murder.victim.GetDoctor();
        }
        else if (spawnReceiver == MurderPreset.LeadCitizen.killersDoctor)
        {
            receiver = murder.murderer.GetDoctor();
        }

        Interactable spawned = null;

        List<Interactable.Passed> passed = new List<Interactable.Passed>();
        passed.Add(new Interactable.Passed(Interactable.PassedVarType.murderID, murder.murderID));
        passed.Add(new Interactable.Passed(Interactable.PassedVarType.jobTag, (int)itemTag));

        if ((spawnWhere == MurderPreset.LeadSpawnWhere.victimHome && murder.victim != null && murder.victim.home != null) || (spawnWhere == MurderPreset.LeadSpawnWhere.victimWork && murder.victim.home != null && murder.victim.job.employer == null))
        {
            if (spawned == null) spawned = murder.victim.home.PlaceObject(spawnItem, belongsTo, writer, receiver, out _, passedVars: passed, forceSecuritySettings: true, forcedSecurity: security, forcedOwnership: ownedRule, forcedPriority: priority);
        }
        else if (spawnWhere == MurderPreset.LeadSpawnWhere.victimWork && murder.victim != null && murder.victim.job.employer != null)
        {
            if (spawned == null) spawned = murder.victim.job.employer.address.PlaceObject(spawnItem, belongsTo, writer, receiver, out _, passedVars: passed, forceSecuritySettings: true, forcedSecurity: security, forcedOwnership: ownedRule, forcedPriority: priority);
        }
        else if ((spawnWhere == MurderPreset.LeadSpawnWhere.killerHome && murder.murderer != null && murder.murderer.home != null) || (spawnWhere == MurderPreset.LeadSpawnWhere.killerWork && murder.murderer.home != null && murder.murderer.job.employer == null))
        {
            if (spawned == null) spawned = murder.murderer.home.PlaceObject(spawnItem, belongsTo, writer, receiver, out _, passedVars: passed, forceSecuritySettings: true, forcedSecurity: security, forcedOwnership: ownedRule, forcedPriority: priority);
        }
        else if (spawnWhere == MurderPreset.LeadSpawnWhere.killerWork && murder.murderer != null && murder.murderer.job.employer != null)
        {
            if (spawned == null) spawned = murder.murderer.job.employer.address.PlaceObject(spawnItem, belongsTo, writer, receiver, out _, passedVars: passed, forceSecuritySettings: true, forcedSecurity: security, forcedOwnership: ownedRule, forcedPriority: priority);
        }

        if (spawned != null)
        {
            Game.Log("Murder: Successfully spawned item " + spawned.name + " for at " + spawned.wPos + " (" + spawned.node.room.name + ")");
            if (murder.activeMurderItems == null) murder.activeMurderItems = new Dictionary<JobPreset.JobTag, Interactable>();

            //This is also called by the interactble setup itself, so don't panic if the below doesn't trigger (it shouldn't)
            if (!murder.activeMurderItems.ContainsKey(itemTag))
            {
                murder.activeMurderItems.Add(itemTag, spawned);
            }
        }
        else
        {
            Game.Log("Murder: Unable to spawn item " + spawnItem.name);
        }

        return spawned;
    }

    [Button]
    public void LastMurderLocation()
    {
        Game.Log(Strings.ComposeText("Last murder location: |killer.lastmurder.victim.name| by |killer.lastmurder.killer.name| at |killer.lastmurder.location.name|", null));
    }

}
