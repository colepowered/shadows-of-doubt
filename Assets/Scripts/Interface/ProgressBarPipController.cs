﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ProgressBarPipController : ButtonController
{
    public Image img;
    public Color unfilledColour = Color.grey;
    public Color filledColour = Color.cyan;
    public Color secondaryColour = Color.yellow;
    public ProgressBarController bar;

    public bool filled = false;
    public bool secondaryFilled = false;

    void Awake()
    {
        rect = this.gameObject.GetComponent<RectTransform>();
        button = this.gameObject.GetComponent<Button>();
        img = this.gameObject.GetComponent<Image>();
    }

    //When hovering, highlight the agent in the pool
    public override void OnHoverStart()
    {
        if (button.interactable)
        {
            bar.hoverOverPip = this;
        }
    }

    public override void OnHoverEnd()
    {
        bar.hoverOverPip = null;
    }

    public void SetFilled(bool newVal, bool secondaryFilled)
    {
        filled = newVal;

        if(filled)
        {
            img.color = filledColour;
            if (button != null)
            {
                button.interactable = true;
                button.image.raycastTarget = true;
            }
        }
        else
        {
            if(secondaryFilled)
            {
                img.color = secondaryColour;
                if (button != null) 
                {
                    button.interactable = true;
                    button.image.raycastTarget = true;
                }
            }
            else
            {
                img.color = unfilledColour;

                //If unfilled, this counts as uninteractable
                if (button != null)
                {
                    button.interactable = false;
                    button.image.raycastTarget = false;
                }
            }
        }

        UpdateAdditionalHighlight();
    }

    //Get this pip's number in the bar
    public int GetPipNumber()
    {
        return bar.pips.FindIndex(item => item == this);
    }
}
