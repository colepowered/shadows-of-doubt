using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShardController : MonoBehaviour
{
    [Header("Setup")]
    public float baseTime = 3f;

    [Header("State")]
    public float timer = 0f;
    public static int shardCounter = 0;

    private void Awake()
    {
        ShardController.shardCounter++;

        //Destroy immediately if we reach this shard count
        if(ShardController.shardCounter > 150)
        {
            Destroy(this.gameObject);
        }
    }

    private void Update()
    {
        timer += Time.deltaTime * Toolbox.Instance.Rand(0.2f, 1f);

        if(timer >= baseTime)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnDisable()
    {
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        ShardController.shardCounter--;
    }
}
