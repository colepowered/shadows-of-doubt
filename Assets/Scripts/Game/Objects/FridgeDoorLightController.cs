﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FridgeDoorLightController : MonoBehaviour
{
    public GameObject lightContainer;
    public InteractableController ic;

    private void Start()
    {
        //Listen for fridge door open
        if(ic != null && ic.interactable != null) ic.interactable.OnSwitchChange += OnSwitchStateChange;

        //Disable on start
        lightContainer.gameObject.SetActive(false);
        OnSwitchStateChange();
    }

    private void OnDestroy()
    {
        if (ic != null && ic.interactable != null) ic.interactable.OnSwitchChange -= OnSwitchStateChange;
    }

    public void OnSwitchStateChange()
    {
        StopAllCoroutines();

        if (ic.interactable != null && ic.interactable.sw0)
        {
            lightContainer.gameObject.SetActive(true);
        }
        else
        {
            if(this.isActiveAndEnabled)
            {
                StartCoroutine(LightOffDelay());
            }
            else
            {
                lightContainer.gameObject.SetActive(false);
            }
        }
    }

    //A delay between the door closing, and truning the light off...
    IEnumerator LightOffDelay()
    {
        float timer = 0f;

        while(timer < 0.8f)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        lightContainer.gameObject.SetActive(false);
    }
}
