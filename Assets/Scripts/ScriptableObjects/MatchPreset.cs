﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

//A scriptable object that can be used to make evidence presets.
//Script pass 1
[CreateAssetMenu(fileName = "match_data", menuName = "Database/Match Type")]

public class MatchPreset : SoCustomComparison
{
    //Enums
    public enum MatchCondition { bloodGroup, fingerprint, time, visualDescriptors, retailPresetMatch, murderWeapon};

    [Header("Matching Conditions")]
    [Tooltip("True if this match preset can only be matched with")]
    public bool canOnlyBeMatchedWith = false;
    [Tooltip("These conditions must return true for it to register as a match. No conditions will result in a match between evidence with this match preset.")]
    public List<MatchCondition> matchConditions = new List<MatchCondition>();
    [Tooltip("Only match with a match parent, and not with non-parents")]
    public bool onlyMatchWithMatchParents = false;
    [Tooltip("Can this match with evidence that is technically itself?")]
    public bool canMatchWithItself = false;
    [Tooltip("Only match with evidence with this other match condition")]
    public MatchPreset onlyMatchWithThis;
    [Tooltip("Link from data key")]
    public List<Evidence.DataKey> linkFromKeys = new List<Evidence.DataKey>();
    [Tooltip("Link to data key")]
    public List<Evidence.DataKey> linkToKeys = new List<Evidence.DataKey>();
}
