using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using System.IO;
using System.Linq;
using TMPro;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine.Networking;

public class StreamingOptionsController : MonoBehaviour
{
    [Header("Settings")]
    public bool enableTwitchAudienceCitizens = false;
    public int updateFrequency = 1600;
    public int maxListCount = 2000;

    [Header("Components")]
    public TextMeshProUGUI twitchConnectStatusText;
    public ButtonController connectToTwitchButton;
    public ToggleController enableTwitchAudienceToggle;
    public TextMeshProUGUI citizenUpdateText;
    public List<ButtonController> disabledIfNoConnection = new List<ButtonController>();

    [Header("State")] public bool grabbedAudience = false; //Have we grabbed the audience since the game booted?
    public bool grabbingAudenceInProgress = false;
    public bool loginNameSet = false;
    public TwitchAudienceData audienceData;
    public float autoUpdateTime = 0f;
    public List<CitizenReplacement> customNames = new List<CitizenReplacement>();
    public List<CitizenReplacement> customNamesReserves = new List<CitizenReplacement>();

    private bool _hasAuth = false; //Used to detect changes in update vs TwitchOAuth script
    private bool _hasValidToken;
    private bool _fetchingDataInProgress;

    private List<string> namePool = new List<string>();

    private List<string>
        activeKnownBots =
            new List<string>(); // We can skip this step, but it allows us to sanitize our list. The API chatters endpoint can return bots who are not visible in chat.

    private HashSet<string> finalNamePool = new HashSet<string>();

    // We must be validate every hour after our application reaches out to the API endpoints.
    private const string twitchValidationEndpoint = "https://id.twitch.tv/oauth2/validate";
    private const string twitchChatterEndpoint = "https://api.twitch.tv/helix/chat/chatters?broadcaster_id=";
    private const string twitchModeratorEndpoint = "https://api.twitch.tv/helix/moderation/moderators?broadcaster_id=";
    private const string twitchVipEndpoint = "https://api.twitch.tv/helix/channels/vips?broadcaster_id=";
    private const string knownBotsEndpoints = "https://api.twitchinsights.net/v1/bots/online";

    [Header("Custom Name List")]
    [InfoBox("Can be used for debugging to test the name parsing functionality")]
    public TextAsset customNameList = null;

    //Singleton pattern
    private static StreamingOptionsController _instance;

    public static StreamingOptionsController Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        //Set to display default state as disconnected- this should do that as we won't have auth at game start
        OnAuthChange();

        // With the new oauth, we don't necessarily need this. 
        // We could utilize the incoming username as a way to show the player we have a valid connection

        audienceData = new TwitchAudienceData();

        StartCoroutine(GrabKnownOnlineBots());
    }

    private void OnDisable()
    {
        // Clean up in case we disable the component.
        StopAllCoroutines();
    }

    private void Update()
    {
        //Poll for auth change but only fire if it's different
        if(TwitchOAuthController.Instance._hasAuth != _hasAuth)
        {
            _hasAuth = TwitchOAuthController.Instance._hasAuth;
            OnAuthChange();
        }

        if (!TwitchOAuthController.Instance._hasAuth)
        {
            return;
        }

        if (enableTwitchAudienceCitizens)
        {
            if (autoUpdateTime >= updateFrequency)
            {
                UpdateTwitchCitizens();
                autoUpdateTime = 0;
            }
            else
            {
                autoUpdateTime += Time.deltaTime;
            }
        }
    }

    public void OnConnectButton()
    {
        // Disable the button and resolve it within the OAuthChange
        connectToTwitchButton.SetInteractable(false);
        //Note: Connect button becomes disconnect when connected (visually handled in OnAuthChange)
        if(!TwitchOAuthController.Instance._hasAuth)
        {
            StreamingOptionsController.Instance.SetStatusText(Strings.Get("ui.interface", "Connecting"));
            TwitchOAuthController.Instance.TryTwitchAuthorization();
        }
        else
        {
            ResetTwitchAuthFlushGeneratedData();
        }
    }

    //Triggered when auth status changes
    public async void OnAuthChange()
    {
        Game.Log("Streaming: Auth change: " + TwitchOAuthController.Instance._hasAuth);
        
        //Switch connect button to disconnect & vice versa based on auth state
        if (TwitchOAuthController.Instance._hasAuth)
        {
            connectToTwitchButton.text.text = Strings.Get("ui.interface", "Disconnect");
            SetStatusText(Strings.Get("ui.interface", "Connected"));

            //Get a valid token (this also updates the status with the username right?)
            _hasValidToken = await ValidateTokenUpdated();
            
            await Task.Delay(1500);
            // Allow interaction again after we've polled for the authorization
            connectToTwitchButton.SetInteractable(true);
        }
        else
        {
            connectToTwitchButton.text.text = Strings.Get("ui.interface", "Connect");
            SetStatusText(Strings.Get("ui.interface", "Disconnected"));

            //Force the streamer citizens option to off if we're not connected so it will always default to regular citizen names
            ForceTwitchAudienceCitizensToOff();
            await Task.Delay(1500);
            connectToTwitchButton.SetInteractable(true);
        }

        //Disable/enable certain buttons based on auth status
        foreach (ButtonController bc in disabledIfNoConnection)
        {
            bc.SetInteractable(TwitchOAuthController.Instance._hasAuth);
        }
    }
    
    public void ResetTwitchAuthFlushGeneratedData()
    {
        // Forcing auth to reset
        TwitchOAuthController.Instance._hasAuth = false;
        
        // Reset our initial audience fetch
        grabbedAudience = false;

        // Create new data objects for future connections
        audienceData = new TwitchAudienceData();
        customNames = new List<CitizenReplacement>();
        customNamesReserves = new List<CitizenReplacement>();
    }

    //Wrote a function for this for ease of execution; needs to save to player prefs to properly be considered 'off'
    private void ForceTwitchAudienceCitizensToOff()
    {
        try
        {
            PlayerPrefs.SetInt("twitchAudienceCitizens", 0); //Save to player prefs

            PlayerPrefsController.GameSetting findSetting = PlayerPrefsController.Instance.gameSettingControls.Find(item => item.identifier == "twitchAudienceCitizens"); //Case insensitive find
            findSetting.intValue = 0;
            enableTwitchAudienceToggle.SetOff();

            PlayerPrefsController.Instance.OnToggleChanged("twitchAudienceCitizens", false);
        }
        catch
        {
            //I don't know why this wouldn't work but I'm paranoid it could cause some kind of null exception on previous versions that didn't have this option...
        }
    }

    // TODO: We may want to disable any saving of menu selections here/force players to manually update each time they play.
    // We could also save their setting in a pref and give them a contextual pop up on the main menu. i.e: Twitch setting enabled, reconnect audience now? 
    public void SetEnableTwitchAudienceCitizens(bool val)
    {
        enableTwitchAudienceCitizens = val;
        Game.Log("Streaming: Enable twitch audience citizens: " + enableTwitchAudienceCitizens);

        if (enableTwitchAudienceCitizens)
        {
            autoUpdateTime = updateFrequency - .25f;

            if (grabbedAudience)
            {
                citizenUpdateText.text = Strings.Get("ui.interface", "Last updated with audience names: ") +
                                         customNames.Count;
            }
            else
            {
                citizenUpdateText.text = Strings.Get("ui.interface",
                    "Press the update button to gather Twitch audience names...");
            }
        }
        else
        {
            autoUpdateTime = 0f;
            citizenUpdateText.text = Strings.Get("ui.interface", "Audience citizen names disabled");
        }

        //Update city directory text
        if (SessionData.Instance.startedGame)
        {
            CityData.Instance.CreateCityDirectory();
        }
    }

    public void SetUpdateFrequency(int val)
    {
        updateFrequency = val;
    }

    public async void UpdateTwitchCitizens()
    {
        if (!TwitchOAuthController.Instance._hasAuth)
        {
            Game.Log("Streaming: No twitch auth, cannot update citizens!");
            return;
        }

        _hasValidToken = await ValidateTokenUpdated();
        
        if (_hasValidToken && !grabbingAudenceInProgress && enableTwitchAudienceCitizens)
        {
            namePool.Clear(); //Do we want to start afresh?

            _fetchingDataInProgress = true;
            await GetChattersUpdated(); 
            await GetModeratorsUpdated();
            await GetVipsUpdated();
            _fetchingDataInProgress = false;
            
            FinalizeNamePool();
            ProcessNamePool();
        }
    }

    private void AddUsersDataToNamePool(TwitchRootObject userData)
    {
        foreach (var user in userData.data)
        {
            namePool.Add(user.user_name);
        }
    }

    // Process this after gathering all necessary data (Chatters, Vips, Mods, etc)
    private void FinalizeNamePool()
    {
        AddUsersDataToNamePool(audienceData.chattersNew);
        AddUsersDataToNamePool(audienceData.moderatorsNew);
        AddUsersDataToNamePool(audienceData.vipsNew);

        // This is rather expensive. Believe we could parse this manually for a perf gain to keep the bots out.
        List<string> botFreeList = namePool.Except(activeKnownBots, StringComparer.OrdinalIgnoreCase).ToList();
        namePool = botFreeList;
    }

    //Process the name pool into list of custom names the game uses as name replacements for citizens
    private void ProcessNamePool()
    {
        int viewerCount = 0;
        int fullCount = 0;
        int reserveCount = 0;

        //Note: We don't want to clear existing here, as otherwise the populated data may be collected in a different order, creating inconsistencies
        //customNames.Clear();
        //customNamesReserves.Clear();

        if (audienceData != null)
        {
            viewerCount = namePool.Count;

            while (customNames.Count < maxListCount && namePool.Count > 0)
            {
                int indexPick = Toolbox.Instance.Rand(0, namePool.Count);
                TryAddCustomName(namePool[indexPick]);
                namePool.RemoveAt(indexPick);

                if (namePool.Count <= 0) break;
            }

            fullCount = customNames.Count;

            //Add reserves to make up to the limit...
            while (customNames.Count < maxListCount && customNamesReserves.Count > 0)
            {
                int indexPick = Toolbox.Instance.Rand(0, customNamesReserves.Count);

                customNames.Add(customNamesReserves[indexPick]);
                customNamesReserves.RemoveAt(indexPick);
                reserveCount++;
            }

            customNamesReserves =
                new List<CitizenReplacement>();
        }

        grabbedAudience = true;
        grabbingAudenceInProgress = false;
        autoUpdateTime = 0f;

        Game.Log("Streaming: Created name list of " + customNames.Count + " with " + fullCount +
                 " successful full names and " + reserveCount + " reserves");

        if (enableTwitchAudienceCitizens)
        {
            //Update city directory text
            if (SessionData.Instance.startedGame)
            {
                CityData.Instance.CreateCityDirectory();
            }

            citizenUpdateText.text = Strings.Get("ui.interface", "Last updated with audience names: ") +
                                     customNames.Count + "/" + viewerCount + " ";
        }
    }

    public async UniTask<bool> ValidateTokenUpdated()
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(twitchValidationEndpoint))
        {
            webRequest.SetRequestHeader("Authorization", "Bearer " + TwitchOAuthController.Instance.GetAuthToken());
            webRequest.SetRequestHeader("Client-Id", TwitchOAuthController.Instance.GetClientID());

            try
            {
                var operation = await webRequest.SendWebRequest();
                var json = operation.downloadHandler.text;
                
                audienceData = JsonUtility.FromJson<TwitchAudienceData>(json);
                SetStatusText(Strings.Get("ui.interface", "Connected") + ": " + audienceData.login);
                return true;
            }
            catch (Exception e)
            {
                //SetStatusText(Strings.Get("ui.interface", "Disconnected"));
                ForceTwitchAudienceCitizensToOff();
                return false;
            }
        }
    }

    public void SetStatusText(string newText)
    {
        Game.Log("Streaming: Change status text: " + newText);
        twitchConnectStatusText.text = newText;
    }

    public async UniTask GetChattersUpdated()
    {
        using (UnityWebRequest webRequest =
               UnityWebRequest.Get(
                   $"{twitchChatterEndpoint}{audienceData.user_id}&moderator_id={audienceData.user_id}"))
        {
            webRequest.SetRequestHeader("Authorization", "Bearer " + TwitchOAuthController.Instance.GetAuthToken());
            webRequest.SetRequestHeader("Client-Id", TwitchOAuthController.Instance.GetClientID());
            var operation = await webRequest.SendWebRequest();
            var json = operation.downloadHandler.text;

            if (operation.error == null)
            {
                audienceData.chattersNew = JsonUtility.FromJson<TwitchRootObject>(json);
            }
        }
    }

    public async UniTask GetModeratorsUpdated()
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get($"{twitchModeratorEndpoint}{audienceData.user_id}"))
        {
            webRequest.SetRequestHeader("Authorization", "Bearer " + TwitchOAuthController.Instance.GetAuthToken());
            webRequest.SetRequestHeader("Client-Id", TwitchOAuthController.Instance.GetClientID());
            var operation = await webRequest.SendWebRequest();
            var json = operation.downloadHandler.text;

            if (operation.error == null)
            {
                audienceData.moderatorsNew = JsonUtility.FromJson<TwitchRootObject>(json);
            }
        }
    }

    public async UniTask GetVipsUpdated()
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get($"{twitchVipEndpoint}{audienceData.user_id}"))
        {
            webRequest.SetRequestHeader("Authorization", "Bearer " + TwitchOAuthController.Instance.GetAuthToken());
            webRequest.SetRequestHeader("Client-Id", TwitchOAuthController.Instance.GetClientID());
            var operation = await webRequest.SendWebRequest();
            var json = operation.downloadHandler.text;

            if (operation.error == null)
            {
                audienceData.vipsNew = JsonUtility.FromJson<TwitchRootObject>(json);
            }

            grabbedAudience = true;
        }
    }
    
    // TODO: This could be generalized in the future, currently this a specific parse following a public endpoints response
    IEnumerator GrabKnownOnlineBots()
    {
        string uri = knownBotsEndpoints;

        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.error == null)
            {
                var jsonString = webRequest.downloadHandler.text;
                var jobj = JObject.Parse(jsonString);

                var array = jobj["bots"];

                foreach (var name in array)
                {
                    activeKnownBots.Add(name[0].ToString());
                }
            }
            else
            {
                Game.Log(webRequest.error);
            }
        }
    }

    //Parses a string containing a custom name or twitch username and attempts to output a full citizen name. If there are blank fields, the game will use the original citizen name.
    private bool TryAddCustomName(string input)
    {
        string userName = input.Trim(); //Grab original string purely for debug purposes

        string firstName = string.Empty;
        string surName = string.Empty;
        int successLevel = 0;

        //Convert numbers characters that will later be split
        input = Regex.Replace(input, @"[\d-]", "_", RegexOptions.Compiled);

        //Treat camel casing as characters that will later be split
        input = string.Join("_", Regex.Split(input, @"(?<!^)(?=[A-Z](?![A-Z]|$))"));

        /*Attempt to parse this into a name compatible with the game (first name + surname)
        Attempt 1: Use common chars to split
        NOTE: Twitch usernames only allow underscores as a special character (I didn't realise this when I first wrote this).
        I'm going to keep the other deliminators though, as if we use text list input at a later date, they could be useful as we generally don't want these chars in names.*/
        string[] commonSplit = input.Split(new char[] {' ', '_', '.', ',', '-'});

        //Loop through split components of usernames
        for (int i = 0; i < commonSplit.Length; i++)
        {
            string trimmed = commonSplit[i].Trim(); //Remove extra whitespace

            if (trimmed.ToLower() == "the") continue; //This is a common string in usernames that's best ignored

            //Valid names must contain at least 2 chars
            if (firstName.Length <= 0)
            {
                if (trimmed.Length >= 2) firstName = trimmed; //We've found a valid first name here
            }
            else if (surName.Length <= 0)
            {
                if (trimmed.Length >= 2) surName = trimmed; //We've found a valid sur name here
            }
            
            //We're done!
            if(firstName.Length >= 2 && surName.Length >= 2)
            {
                break;
            }
        }

        //Successful parse
        if (firstName.Length > 0 && surName.Length > 0) successLevel = 2; //Found valid first and sur
        else if (firstName.Length > 0 || surName.Length > 0) successLevel = 1; //Only found one
        else
        {
            //The name is too short to use, return out
            return false;
        }

        //Capitalize first letter
        firstName = Strings.ApplyCasing(firstName, Strings.Casing.firstLetterCaptial);
        surName = Strings.ApplyCasing(surName, Strings.Casing.firstLetterCaptial);

        //In this case we've found both and can write to the main 'streamer audience' list
        if (successLevel == 2 && customNames.Count < maxListCount)
        {
            CitizenReplacement newRep = new CitizenReplacement();
            newRep.name = firstName + " " + surName;
            newRep.firstName = firstName;
            newRep.surName = surName;

            //Ignore duplicates within the existing custom names list
            var existsInList = customNames.Where(p => newRep.firstName == p.firstName && newRep.surName == p.surName).ToList();
            
            if (existsInList.Count <= 0)
            {
                Game.Log("Streaming: Writing parsed " + userName + " = " + newRep.name + " to StreamerAudience list...");
                customNames.Add(newRep);
            }
        }
        //In this case we've found one and can write to the backup 'streamer audience reserves' list
        else if (successLevel == 1 && customNamesReserves.Count < maxListCount)
        {
            CitizenReplacement newRep = new CitizenReplacement();

            //Assign what we were able to parse to the surname so it's obvious this feature has worked
            if(firstName.Length > 0 && surName.Length <= 0)
            {
                newRep.surName = firstName;
            }
            else if(surName.Length > 0 && firstName.Length <= 0)
            {
                newRep.surName = surName;
            }

            newRep.name = firstName + " " + surName;

            //Ignore duplicates within the existing reserve names list
            var existsInList = customNamesReserves.Where(p => newRep.firstName == p.firstName && newRep.surName == p.surName).ToList();

            if (existsInList.Count <= 0)
            {
                //Ignore duplicates within the existing custom names list
                existsInList = customNames.Where(p => newRep.firstName == p.firstName && newRep.surName == p.surName).ToList();

                if (existsInList.Count <= 0)
                {
                    Game.Log("Streaming: Writing parsed " + userName + " = " + newRep.name + " to StreamerAudienceReserves list...");
                    customNamesReserves.Add(newRep);
                }
            }
        }

        return true;
    }

    [Button]
    public void ParseNamesFromNameList()
    {
        if(customNameList != null)
        {
            namePool.Clear();

            //Parse from text file
            var arrayString = customNameList.text.Split('\n');

            foreach (var line in arrayString)
            {
                namePool.Add(line);
            }

            ProcessNamePool();
        }
    }
}