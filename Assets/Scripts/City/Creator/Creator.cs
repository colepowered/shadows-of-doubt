﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creator : MonoBehaviour
{
    //Call to start loading this
    public virtual void StartLoading()
    {
        SetComplete();
    }

    //Set complete
    public void SetComplete()
    {
        CityConstructor.Instance.loadingProgress = 1f;
        CityConstructor.Instance.stateComplete = true;

        //Disable this
        StopAllCoroutines();
        this.enabled = false;
    }
}
