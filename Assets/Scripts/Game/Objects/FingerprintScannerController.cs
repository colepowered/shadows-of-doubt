﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;
using UnityEngine.ProBuilder;
using UnityStandardAssets.Utility;

public class FingerprintScannerController : MonoBehaviour
{
    [Header("Components")]
    public TextMeshPro screenText;
    public Transform progressBar;
    public MeshRenderer screen;
    public Transform printTransform;
    public GameObject pixelPrefab;
    private List<GameObject> pixels = new List<GameObject>();
    public List<GameObject> blockedPixelsActive = new List<GameObject>();
    public GameObject screenLight;
    public Light scanLight;

    public bool isOn = false;
    public float screenOnDelay = 1.65f;

    [Header("Audio")]
    public AudioEvent progressLoop;
    public AudioEvent detect;
    public AudioEvent detectExisting;
    public AudioEvent success;
    public AudioEvent hoverOff;
    private AudioController.LoopingSoundInfo progressLoopEvent;

    [Header("Prints")]
    [Tooltip("List of valid objects being looked at")]
    public List<Transform> lookingAt = new List<Transform>();
    [Tooltip("Spawned prints")]
    public List<PrintController> spawnedPrints = new List<PrintController>();
    [Tooltip("Hovered over this print")]
    public PrintController hoverPrint;
    [Tooltip("Hovered over this footprint")]
    public FootprintController hoverFootPrint;
    [Tooltip("How fast a print is scanned (seconds)")]
    public float scanSpeed = 1f;
    [Tooltip("Flash the screen")]
    private bool flashActive = false;
    private float flashSpeed = 10f;
    [ColorUsage(true, true)]
    public Color flashColour;
    private int cycle = 0;
    private float flashProgress = 0f;
    private float flashF = 0f;
    private int flashRepeat = 3;

    //Cached list of prints on transforms...
    private Dictionary<Transform, HashSet<Print>> cachedStaticPrints = new Dictionary<Transform, HashSet<Print>>();
    private Dictionary<Interactable, HashSet<Print>> cachedDynamicPrints = new Dictionary<Interactable, HashSet<Print>>();

    [System.Serializable]
    public class Print
    {
        [Header("Serialized")]
        public Vector3 worldPos;
        public Vector3 normal;
        public enum PrintType { fingerPrint, footPrint };
        public PrintType type;
        public RoomConfiguration.PrintsSource source;

        [Header("Non-Serialized")]
        [System.NonSerialized]
        public Transform parentTranform;
        [System.NonSerialized]
        public NewRoom room;
        [System.NonSerialized]
        public Interactable interactable;
        [System.NonSerialized]
        public FurnitureLocation furniture;
        [System.NonSerialized]
        public Human dynamicOwner; //Only relevent in dynamic prints where the owner is already chosen

        public Human GetOwner()
        {
            if (dynamicOwner == null)
            {
                //Get possible owners...
                List<Human> ownerPool = Toolbox.Instance.GetFingerprintOwnerPool(room, furniture, interactable, source, worldPos, true);

                //Pick owner...
                //Use random based on world position so it's predictable...
                return ownerPool[Toolbox.Instance.GetPsuedoRandomNumber(0, ownerPool.Count, worldPos.ToString())];
            }
            else
            {
                return dynamicOwner; //Print is dynamic and already has owner assigned...
            }
        }
    }

    private void Start()
    {
        FirstPersonItemController.Instance.fingerprintLights.SetActive(false);
        screenLight.SetActive(false);
        SetOn(false);

        while (spawnedPrints.Count > 0)
        {
            Destroy(spawnedPrints[0].gameObject);
            spawnedPrints.RemoveAt(0);
        }

        OnHoverOnNewPrint();

        //Setup pixels
        for (int i = -4; i <= 4; i++)
        {
            for (int u = -5; u <= 5; u++)
            {
                GameObject newPixel = Instantiate(pixelPrefab, printTransform);
                newPixel.transform.localPosition = new Vector3(i * 0.1f, u * 0.1f, -0.01f);
                pixels.Add(newPixel);
                blockedPixelsActive.Add(newPixel);
            }
        }
    }

    public void SetOn(bool val)
    {
        isOn = val;

        printTransform.gameObject.SetActive(isOn);
        progressBar.gameObject.SetActive(isOn);
        screenText.gameObject.SetActive(isOn);
        FirstPersonItemController.Instance.fingerprintLights.SetActive(isOn);
        screenLight.SetActive(isOn);
        scanLight.enabled = isOn;

        if (isOn)
        {
            Flash(1, false, speed: 2f);
        }
    }

    private void OnDestroy()
    {
        if (progressLoopEvent != null)
        {
            AudioController.Instance.StopSound(progressLoopEvent, AudioController.StopType.immediate, "end print scan");
        }

        if (FirstPersonItemController.Instance != null && FirstPersonItemController.Instance.fingerprintLights != null) FirstPersonItemController.Instance.fingerprintLights.SetActive(false);

        while(spawnedPrints.Count > 0)
        {
            if(spawnedPrints[0] != null) Destroy(spawnedPrints[0].gameObject);
            spawnedPrints.RemoveAt(0);
        }
    }

    private void Update()
    {
        //What am I pointing at?
        //Ray ray = CameraController.Instance.cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        Ray ray = new Ray(CameraController.Instance.cam.transform.position, CameraController.Instance.cam.transform.forward);

        RaycastHit hit;

        HashSet<Transform> lookHitThisFrame = new HashSet<Transform>();
        HashSet<Interactable> lookAtInteractablesThisFrame = new HashSet<Interactable>();
        HashSet<Print> requiredPrints = new HashSet<Print>();
        HashSet<Vector3> requiredVectorHash = new HashSet<Vector3>(); //As classes might be different, do a hash check with exact positions instead...

        //Animate boot up...
        if(screenOnDelay > 0f)
        {
            screenOnDelay -= Time.deltaTime;

            if(screenOnDelay <= 0f)
            {
                SetOn(true);
                screenOnDelay = 0;
            }
        }

        if(isOn)
        {
            //Detect over prints
            if (Physics.Raycast(ray, out hit, 1.7f, Toolbox.Instance.interactionRayLayerMask))
            {
                PrintController hovPrint = hit.transform.GetComponent<PrintController>();

                if (hovPrint != hoverPrint)
                {
                    if (hoverPrint != null)
                    {
                        hoverPrint.ResetScan();
                    }

                    hoverPrint = hovPrint;
                    OnHoverOnNewPrint();
                }

                FootprintController hovFootPrint = hit.transform.GetComponent<FootprintController>();

                if(hovFootPrint != hoverFootPrint)
                {
                    if(hoverFootPrint != null)
                    {
                        hoverFootPrint.ResetScan();
                    }

                    hoverFootPrint = hovFootPrint;
                    OnHoverOnNewPrint();
                }
            }
            else
            {
                if (hoverPrint != null)
                {
                    hoverPrint.ResetScan();
                    hoverPrint = null;
                    OnHoverOnNewPrint();
                }

                if (hoverFootPrint != null)
                {
                    hoverFootPrint.ResetScan();
                    hoverFootPrint = null;
                    OnHoverOnNewPrint();
                }
            }

            //Must hit something...
            if (Physics.Raycast(ray, out hit, 1.7f, Toolbox.Instance.printDetectionRayLayerMask))
            {
                FirstPersonItemController.Instance.scannerRayPoint = hit.point;

                //Detect colliders within sphere...
                Collider[] hitColliders = Physics.OverlapSphere(hit.point, FirstPersonItemController.Instance.printDetectionRadius);

                foreach (Collider col in hitColliders)
                {
                    lookHitThisFrame.Add(col.transform);

                    if (!lookingAt.Contains(col.transform))
                    {
                        lookingAt.Add(col.transform);
                    }

                    //Cache static prints
                    if (!cachedStaticPrints.ContainsKey(col.transform))
                    {
                        cachedStaticPrints.Add(col.transform, GetPrintPoints(col.transform));
                    }

                    //Check for prints to display
                    foreach (Print p in cachedStaticPrints[col.transform])
                    {
                        //Is this within sphere?
                        if (Vector3.Distance(p.worldPos, hit.point) <= FirstPersonItemController.Instance.printDetectionRadius)
                        {
                            //Story mode; only detect kidnapper and killer's prints in kidnapper's apartment
                            if (ChapterController.Instance != null && ChapterController.Instance.chapterScript != null)
                            {
                                ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                                if(intro != null)
                                {
                                    if(Player.Instance.currentGameLocation == intro.kidnapper.home)
                                    {
                                        Human pOwner = p.GetOwner();

                                        if (pOwner != intro.kidnapper && pOwner != intro.killer)
                                        {
                                            Game.Log("Chapter: Skipping print as it belong to " + pOwner);
                                            continue;
                                        }
                                    }
                                }
                            }

                            if (!requiredVectorHash.Contains(p.worldPos))
                            {
                                requiredPrints.Add(p);
                                requiredVectorHash.Add(p.worldPos);
                            }
                        }
                    }

                    //Get dynamic prints
                    InteractableController interactableC = col.transform.GetComponent<InteractableController>();

                    if (interactableC != null)
                    {
                        if(interactableC.interactable != null)
                        {
                            lookAtInteractablesThisFrame.Add(interactableC.interactable);

                            if (interactableC.interactable.preset.enableDynamicFingerprints && interactableC.interactable.df.Count > 0)
                            {
                                //Cache dynamic prints
                                if (!cachedDynamicPrints.ContainsKey(interactableC.interactable))
                                {
                                    Game.Log("Player: Cache new dynamic prints for " + interactableC.interactable.name);
                                    cachedDynamicPrints.Add(interactableC.interactable, GetDynamicPrints(interactableC));
                                }

                                //Check for prints to display
                                foreach (Print p in cachedDynamicPrints[interactableC.interactable])
                                {
                                    //Story mode; only detect kidnapper and killer's prints in kidnapper's apartment
                                    if (ChapterController.Instance != null && ChapterController.Instance.chapterScript != null)
                                    {
                                        ChapterIntro intro = ChapterController.Instance.chapterScript as ChapterIntro;

                                        if (intro != null)
                                        {
                                            if (Player.Instance.currentGameLocation == intro.kidnapper.home)
                                            {
                                                Human pOwner = p.GetOwner();

                                                if (pOwner != intro.kidnapper && pOwner != intro.killer)
                                                {
                                                    Game.Log("Chapter: Skipping print as it belong to " + pOwner);
                                                    continue;
                                                }
                                            }
                                        }
                                    }

                                    //Is this within sphere?
                                    if (Vector3.Distance(p.worldPos, hit.point) <= FirstPersonItemController.Instance.printDetectionRadius)
                                    {
                                        if (!requiredVectorHash.Contains(p.worldPos))
                                        {
                                            requiredPrints.Add(p);
                                            requiredVectorHash.Add(p.worldPos);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            Game.LogError("No interatable has been assigned to " + col.transform.name);
                        }

                    }
                }
            }
            else
            {
                FirstPersonItemController.Instance.scannerRayPoint = new Vector3(0, -99999, 0);
            }
        }

        //Remove non-looked at colliders from cache...
        List<Interactable> toRemove = new List<Interactable>();

        foreach(KeyValuePair<Interactable, HashSet<Print>> pair in cachedDynamicPrints)
        {
            if(pair.Key.controller == null)
            {
                //This interactable is no longer drawn...
                toRemove.Add(pair.Key);
            }
            else if(!lookAtInteractablesThisFrame.Contains(pair.Key))
            {
                toRemove.Add(pair.Key);
            }
        }

        while(toRemove.Count > 0)
        {
            if(cachedDynamicPrints.ContainsKey(toRemove[0]))
            {
                Game.Log("Player: Removing dynamic prints cache for " + toRemove[0].name);
                cachedDynamicPrints.Remove(toRemove[0]);
            }
            
            toRemove.RemoveAt(0);
        }

        float scanLightIntensity = 5;
        float scanFrontLight = 1;

        //Add to hoverprint scan
        if(hoverPrint != null)
        {
            hoverPrint.scanProgress += scanSpeed * Time.deltaTime;

            if(hoverPrint.scanProgress >= 1f)
            {
                if(!hoverPrint.printConfirmed)
                {
                    hoverPrint.PrintConfirmed();
                    Flash(2, false);
                    screenText.text = Strings.Get("misc", "Confirmed");

                    foreach (GameObject go in pixels)
                    {
                        go.SetActive(false);
                    }

                    blockedPixelsActive.Clear();

                    AudioController.Instance.Play2DSound(success);
                }

                progressBar.localScale = new Vector3(0.9f, 0.1f, 1);

                if (progressLoopEvent != null)
                {
                    AudioController.Instance.StopSound(progressLoopEvent, AudioController.StopType.immediate, "end print scan");
                }
            }
            else
            {
                screenText.text = Strings.Get("misc", "Scan") + " " + Mathf.FloorToInt(hoverPrint.scanProgress * 100) + "%";
                progressBar.localScale = new Vector3(0.9f * hoverPrint.scanProgress, 0.1f, 1);

                int removeBlockedPixels = Mathf.CeilToInt(hoverPrint.scanProgress * 99f);
                
                while(blockedPixelsActive.Count > 99 - removeBlockedPixels)
                {
                    int ranIndex = UnityEngine.Random.Range(0, blockedPixelsActive.Count);
                    blockedPixelsActive[ranIndex].SetActive(false);
                    blockedPixelsActive.RemoveAt(ranIndex);
                }

                //Pulsate scan light
                scanLightIntensity = UnityEngine.Random.Range(0f, Mathf.Max(350f * hoverPrint.scanProgress, 100f));
                scanFrontLight = scanLightIntensity * 0.1f;
            }
        }

        //Add to hoverprint scan (footprint)
        if (hoverFootPrint != null)
        {
            hoverFootPrint.scanProgress += scanSpeed * Time.deltaTime;

            if (hoverFootPrint.scanProgress >= 1f)
            {
                if (!hoverFootPrint.printConfirmed)
                {
                    hoverFootPrint.PrintConfirmed();
                    Flash(2, false);
                    screenText.text = Strings.Get("misc", "Confirmed");

                    foreach (GameObject go in pixels)
                    {
                        go.SetActive(false);
                    }

                    blockedPixelsActive.Clear();

                    AudioController.Instance.Play2DSound(success);
                }

                progressBar.localScale = new Vector3(0.9f, 0.1f, 1);

                if (progressLoopEvent != null)
                {
                    AudioController.Instance.StopSound(progressLoopEvent, AudioController.StopType.immediate, "end print scan");
                }
            }
            else
            {
                screenText.text = Strings.Get("misc", "Scan") + " " + Mathf.FloorToInt(hoverFootPrint.scanProgress * 100) + "%";
                progressBar.localScale = new Vector3(0.9f * hoverFootPrint.scanProgress, 0.1f, 1);

                int removeBlockedPixels = Mathf.CeilToInt(hoverFootPrint.scanProgress * 99f);

                while (blockedPixelsActive.Count > 99 - removeBlockedPixels)
                {
                    int ranIndex = UnityEngine.Random.Range(0, blockedPixelsActive.Count);
                    blockedPixelsActive[ranIndex].SetActive(false);
                    blockedPixelsActive.RemoveAt(ranIndex);
                }

                //Pulsate scan light
                scanLightIntensity = UnityEngine.Random.Range(0f, Mathf.Max(350f * hoverFootPrint.scanProgress, 100f));
                scanFrontLight = scanLightIntensity * 0.1f;
            }
        }

        FirstPersonItemController.Instance.printScannerPulseLight.intensity = scanLightIntensity;
        scanLight.intensity = scanFrontLight;

        //Remove transforms outside of sphere...
        for (int i = 0; i < lookingAt.Count; i++)
        {
            Transform t = lookingAt[i];

            if(!lookHitThisFrame.Contains(t))
            {
                lookingAt.RemoveAt(i);
                i--;
            }
        }

        for (int i = 0; i < spawnedPrints.Count; i++)
        {
            PrintController p = spawnedPrints[i];

            if(p == null)
            {
                spawnedPrints.RemoveAt(i);
                i--;
                continue;
            }

            if(!lookHitThisFrame.Contains(p.printData.parentTranform))
            {
                if(p.gameObject != null) Destroy(p.gameObject);
                spawnedPrints.RemoveAt(i);
                i--;
                continue;
            }

            if (requiredVectorHash.Contains(p.printData.worldPos))
            {
                requiredPrints.Remove(p.printData);
                requiredVectorHash.Remove(p.printData.worldPos);
            }
            else
            {
                if (p.gameObject != null)  Destroy(p.gameObject);
                spawnedPrints.RemoveAt(i);
                i--;
                continue;
            }
        }

        //Spawn remaining required...
        foreach(Print p in requiredPrints)
        {
            GameObject newObj = Instantiate(PrefabControls.Instance.fingerprint, p.parentTranform);
            newObj.transform.position = p.worldPos;

            PrintController pc = newObj.GetComponent<PrintController>();
            pc.Setup(p);
            spawnedPrints.Add(pc);
        }

        if (flashActive)
        {
            if (cycle < flashRepeat && flashProgress < 2f)
            {
                flashProgress += flashSpeed * Time.deltaTime;

                if (flashProgress <= 1f) flashF = flashProgress;
                else flashF = 2f - flashProgress;

                screen.sharedMaterial.SetColor("_EmissiveColor", Color.Lerp(Color.black, flashColour, flashF));

                if (flashProgress >= 2f)
                {
                    cycle++;
                    flashProgress = 0f;
                }
            }
            else
            {
                flashActive = false;
            }
        }
    }

    //Flash the image with this colour (multiply) and repeat at this speed
    public void Flash(int newRepeat, bool colourOverride, Color colour = new Color(), float speed = 10f)
    {
        if (colourOverride)
        {
            flashColour = colour;
        }

        //Game.Log("Flash");
        flashSpeed = speed;
        flashRepeat = newRepeat;

        //If already active, add repeats to current coroutine
        if (flashActive)
        {
            flashRepeat += flashRepeat;
        }
        else
        {
            flashActive = true;
            cycle = 0;
            flashProgress = 0f;
            flashF = 0f;
            this.enabled = true;
        }
    }

    public void OnHoverOnNewPrint()
    {
        blockedPixelsActive.Clear();

        if(progressLoopEvent != null)
        {
            AudioController.Instance.StopSound(progressLoopEvent, AudioController.StopType.immediate, "end print scan");
        }

        foreach (GameObject go in pixels)
        {
            go.SetActive(true);
            blockedPixelsActive.Add(go);
        }

        if (hoverPrint == null && hoverFootPrint == null)
        {
            screenText.text = Strings.Get("misc", "Null");
            progressBar.localScale = new Vector3(0f, 0.1f, 1);

            if(isOn)
            {
                AudioController.Instance.Play2DSound(hoverOff);
            }
        }
        else if((hoverPrint != null && hoverPrint.printConfirmed) || (hoverFootPrint != null && hoverFootPrint.printConfirmed))
        {
            screenText.text = Strings.Get("misc", "Confirmed");

            foreach (GameObject go in pixels)
            {
                go.SetActive(false);
            }

            blockedPixelsActive.Clear();
            AudioController.Instance.Play2DSound(detectExisting);
            Flash(1, false, speed: 12);
        }
        else
        {
            AudioController.Instance.Play2DSound(detect);
            progressLoopEvent = AudioController.Instance.Play2DLooping(progressLoop);
        }
    }

    //Get dynamic prints from interactable
    private HashSet<Print> GetDynamicPrints(InteractableController interactable)
    {
        HashSet<Print> ret = new HashSet<Print>();
        if (interactable == null) return ret;
        MeshFilter filter = interactable.transform.GetComponent<MeshFilter>();
        if (filter == null) return ret;
        if (filter.mesh == null) return ret;

        List<Vector3> normals = new List<Vector3>();
        List<Human> owners = new List<Human>(); //List of owners to assign to picked print locations...
        List<string> seeds = new List<string>(); //List of seeds to give to print location generator to make sure they're the same each time...

        //Gather list of owners to assign
        foreach (Interactable.DynamicFingerprint f in interactable.interactable.df)
        {
            Human found = null;

            if(CityData.Instance.GetHuman(f.id, out found))
            {
                owners.Add(found);
                seeds.Add(f.seed);
            }
            else
            {
                Game.LogError("Cannot find citizen " + f.id);
            }
        }

        Game.Log("Player: Getting " + owners.Count + " dynamic prints for " + interactable.name);

        List<Vector3> printLocations = GetPrintLocationsOnMesh(filter, owners.Count, out normals, seeds: seeds);

        for (int i = 0; i < printLocations.Count; i++)
        {
            Vector3 v3 = printLocations[i];

            Print newPrint = new Print();
            newPrint.parentTranform = interactable.transform;
            newPrint.worldPos = interactable.transform.TransformPoint(v3);
            newPrint.normal = normals[i];
            newPrint.interactable = interactable.interactable;
            newPrint.type = Print.PrintType.fingerPrint;
            newPrint.dynamicOwner = owners[0];
            owners.RemoveAt(0);

            ret.Add(newPrint);
        }

        return ret;
    }

    //Get print points for non-dynamic prints only
    private HashSet<Print> GetPrintPoints(Transform checkTransform)
    {
        HashSet<Print> ret = new HashSet<Print>();
        MeshFilter filter = checkTransform.GetComponent<MeshFilter>();
        if (filter == null) return ret;
        if (filter.mesh == null) return ret;

        Game.Log("Player: Attempt to get prints for " + checkTransform.name+"...");

        List<Vector3> printLocations = new List<Vector3>();
        List<Vector3> normals = new List<Vector3>();
        NewRoom room = null;
        Interactable interactable = null;
        FurnitureLocation furniture = null;
        Print.PrintType type = Print.PrintType.fingerPrint;
        RoomConfiguration.PrintsSource source = RoomConfiguration.PrintsSource.inhabitants;

        //Get the category: Room, furniture or interactable...
        //Is on layer room mesh?
        if(checkTransform.gameObject.layer == 29)
        {
            Game.Log("Player: ...Object is on combined meshes layer...");

            if (checkTransform.CompareTag("WallsMesh"))
            {
                //Search visible rooms for this wall...
                foreach(NewRoom r in CityData.Instance.visibleRooms)
                {
                    if (r.combinedWalls != null && r.combinedWalls.transform == checkTransform)
                    {
                        room = r;
                        break;
                    }
                    else
                    {
                        foreach(KeyValuePair<NewBuilding, GameObject> pair in r.additionalWalls)
                        {
                            if(pair.Value.transform == checkTransform)
                            {
                                room = r;
                                break;
                            }
                        }

                        if (room != null)
                        {
                            break;
                        }
                    }
                }

                //Go ahead if we've found the correct room...
                if(room != null && room.preset.fingerprintsEnabled)
                {
                    printLocations = GetPrintLocationsOnMeshNonDynamic(filter, room.preset.fingerprintWallDensity, out normals, true, room);
                    type = Print.PrintType.fingerPrint;
                    source = room.preset.printsSource;
                }

            }
        }
        //Is interactable?
        else
        {
            InteractableController interactableC = checkTransform.GetComponent<InteractableController>();

            if (interactableC != null)
            {
                Game.Log("Player: Found interactable to scan...");
                interactable = interactableC.interactable;

                if (interactable != null && interactable.preset.fingerprintsEnabled)
                {
                    printLocations = GetPrintLocationsOnMeshNonDynamic(filter, interactable.preset.fingerprintDensity, out normals);
                    if(printLocations != null) Game.Log("Player: ... Found " + printLocations.Count + " print locations...");
                    type = Print.PrintType.fingerPrint;
                    source = interactable.preset.printsSource;
                }
            }
            else
            {
                //Get furniture within room
                foreach (NewRoom r in CityData.Instance.visibleRooms)
                {
                    foreach(FurnitureLocation loc in r.individualFurniture)
                    {
                        if (loc == null || loc.spawnedObject == null) continue;

                        if(loc.spawnedObject.transform == checkTransform)
                        {
                            furniture = loc;
                            break;
                        }
                    }

                    if (furniture != null) break;
                }

                if (furniture != null && furniture.furniture.fingerprintsEnabled)
                {
                    Game.Log("Player: Found furniture to scan...");
                    printLocations = GetPrintLocationsOnMeshNonDynamic(filter, furniture.furniture.fingerprintDensity, out normals, true, furniture.anchorNode.room);
                    type = Print.PrintType.fingerPrint;
                    source = furniture.furniture.printsSource;
                }
                else
                {
                    Game.Log("Player: ... Unable to find furniture spawned that matches this transform");
                }
            }
        }

        for (int i = 0; i < printLocations.Count; i++)
        {
            Vector3 v3 = printLocations[i];

            Print newPrint = new Print();
            newPrint.parentTranform = checkTransform;
            newPrint.worldPos = checkTransform.TransformPoint(v3);
            newPrint.normal = normals[i];
            newPrint.room = room;
            newPrint.interactable = interactable;
            newPrint.furniture = furniture;
            newPrint.type = type;
            newPrint.source = source;

            ret.Add(newPrint);
        }

        return ret;
    }

    //Get print points for non-dynamic prints only
    List<Vector3> GetPrintLocationsOnMeshNonDynamic(MeshFilter meshFilter, float printDensityPerUnit, out List<Vector3> normals, bool useHeightThreshold = false, NewRoom heightThresholdRoom = null)
    {
        normals = new List<Vector3>();

        if(meshFilter == null) return new List<Vector3>();

        Mesh mesh = meshFilter.mesh;
        List<Vector3> returnedPoints = new List<Vector3>();

        if (mesh == null) return returnedPoints;

        if (!mesh.isReadable)
        {
            Game.Log("Mesh " + mesh.name + " is not readable, this could be because of static batching. Searching for collider mesh we can use instead...");

            MeshCollider coll = meshFilter.gameObject.GetComponent<MeshCollider>();

            if(coll != null)
            {
                if(coll.sharedMesh != null && coll.sharedMesh.isReadable)
                {
                    Game.Log("...Using mesh from mesh collider instead: " + coll.sharedMesh.name);
                    mesh = coll.sharedMesh;
                }
                else
                {
                    Game.LogError("Mesh " + mesh.name + " is not readable! Fingerprints cannot be gathered as the verts aren't readable...");
                    return returnedPoints;
                }
            }
            else
            {
                Game.LogError("Mesh " + mesh.name + " is not readable! Fingerprints cannot be gathered as the verts aren't readable...");
                return returnedPoints;
            }
        }

        //if you're repeatedly doing this on a single mesh, you'll likely want to cache cumulativeSizes and total
        float[] sizes = GetTriSizes(mesh.triangles, mesh.vertices);
        float[] cumulativeSizes = new float[sizes.Length];
        float total = 0;

        for (int i = 0; i < sizes.Length; i++)
        {
            total += sizes[i];
            cumulativeSizes[i] = total;
        }

        int prints = Mathf.Max(Mathf.RoundToInt(total * printDensityPerUnit), 1);

        return GetPrintLocationsOnMesh(meshFilter, prints, out normals, useHeightThreshold, heightThresholdRoom);
    }

    //Get this many points on mesh
    List<Vector3> GetPrintLocationsOnMesh(MeshFilter meshFilter, int prints, out List<Vector3> normals, bool useHeightThreshold = false, NewRoom heightThresholdRoom = null, List<string> seeds = null)
    {
        Game.Log("Player: Looking for " + prints + " on mesh " + meshFilter.sharedMesh.name + "...");

        normals = new List<Vector3>();

        if (meshFilter == null) return new List<Vector3>();

        Mesh mesh = meshFilter.mesh;
        List<Vector3> returnedPoints = new List<Vector3>();

        if (mesh == null) return returnedPoints;

        if (!mesh.isReadable)
        {
            Game.Log("Mesh " + mesh.name + " is not readable, this could be because of static batching. Searching for collider mesh we can use instead...");

            MeshCollider coll = meshFilter.gameObject.GetComponent<MeshCollider>();

            if (coll != null)
            {
                if (coll.sharedMesh != null && coll.sharedMesh.isReadable)
                {
                    Game.Log("...Using mesh from mesh collider instead: " + coll.sharedMesh.name);
                    mesh = coll.sharedMesh;
                }
                else
                {
                    Game.LogError("Mesh " + mesh.name + " is not readable! Fingerprints cannot be gathered as the verts aren't readable...");
                    return returnedPoints;
                }
            }
            else
            {
                Game.LogError("Mesh " + mesh.name + " is not readable! Fingerprints cannot be gathered as the verts aren't readable...");
                return returnedPoints;
            }
        }

        //if you're repeatedly doing this on a single mesh, you'll likely want to cache cumulativeSizes and total
        float[] sizes = GetTriSizes(mesh.triangles, mesh.vertices);
        float[] cumulativeSizes = new float[sizes.Length];
        float total = 0;

        for (int i = 0; i < sizes.Length; i++)
        {
            total += sizes[i];
            cumulativeSizes[i] = total;
        }

        string predictableSeed = meshFilter.gameObject.transform.position.ToString();

        for (int u = 0; u < prints; u++)
        {
            //Use list of predictable seeds...
            if(seeds != null && seeds.Count > 0)
            {
                predictableSeed = seeds[0];
                seeds.RemoveAt(0);
            }

            float rand = Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, predictableSeed);
            predictableSeed = rand.ToString();

            float randomsample = rand * total;

            int triIndex = -1;

            for (int i = 0; i < sizes.Length; i++)
            {
                if (randomsample <= cumulativeSizes[i])
                {
                    triIndex = i;
                    break;
                }
            }

            if (triIndex == -1) Game.LogError("triIndex should never be -1");

            Vector3 a = mesh.vertices[mesh.triangles[triIndex * 3]];
            Vector3 b = mesh.vertices[mesh.triangles[triIndex * 3 + 1]];
            Vector3 c = mesh.vertices[mesh.triangles[triIndex * 3 + 2]];


            //generate random barycentric coordinates
            float r = Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, predictableSeed);
            predictableSeed = r.ToString();
            float s = Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, predictableSeed);

            if (r + s >= 1)
            {
                r = 1 - r;
                s = 1 - s;
            }

            //and then turn them back to a Vector3
            Vector3 pointOnMesh = a + r * (b - a) + s * (c - a);

            //Calculate normal
            Vector3 normalA = mesh.normals[mesh.triangles[triIndex * 3]];
            Vector3 normalB = mesh.normals[mesh.triangles[triIndex * 3 + 1]];
            Vector3 normalC = mesh.normals[mesh.triangles[triIndex * 3 + 2]];

            Vector3 faceNormal = (normalA + normalB + normalC) / 3.0f;
            faceNormal.Normalize();

            //Scale point
            pointOnMesh = new Vector3(pointOnMesh.x * meshFilter.transform.lossyScale.x, pointOnMesh.y * meshFilter.transform.lossyScale.y, pointOnMesh.z * meshFilter.transform.lossyScale.z);

            //If this point on mesh is above the height threshold, discard...
            if (useHeightThreshold)
            {
                if (pointOnMesh.y > heightThresholdRoom.nodes.First().position.y + 1.8f && pointOnMesh.y < heightThresholdRoom.nodes.First().position.y + 0.35f)
                {
                    continue;
                }
            }

            //Set this position to the new seed to generate predictable...
            predictableSeed = pointOnMesh.ToString();

            returnedPoints.Add(pointOnMesh);
            normals.Add(faceNormal);
        }

        return returnedPoints;
    }

    float[] GetTriSizes(int[] tris, Vector3[] verts)
    {
        int triCount = tris.Length / 3;

        float[] sizes = new float[triCount];

        for (int i = 0; i < triCount; i++)
        {
            sizes[i] = .5f * Vector3.Cross(verts[tris[i * 3 + 1]] - verts[tris[i * 3]], verts[tris[i * 3 + 2]] - verts[tris[i * 3]]).magnitude;
        }

        return sizes;
    }
}
