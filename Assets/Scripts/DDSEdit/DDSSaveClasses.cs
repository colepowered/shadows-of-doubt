﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DDSSaveClasses
{
    public enum TreeTriggers { awake, dead, asleep, unconscious, noReactionState, investigating, investigatingVisual, investigatingSound, persuing, searching, notInCombat, inCombat, legal, illegal, travelling, sat, employee, nonEmployee, carrying, notCarrying, privateLocation, publicLocation, onStreet, atHome, atWork, lightOnAny, lightOnMain, allLightsOff, rain, indoors, brokenSign, travellingToWork, notPresent, atEatery, hasJob, unemployed, homeIntenseWallpaper, homeBrightSign, enforcerOnDuty, notEnforcerOnDuty, trespassing, locationOfAuthority, drunk, restrained, sober, hasRoomAtHotel, hotelPaymentDue, hasNoRoomAtHotel }

    public enum RepeatSetting { oneHour, sixHours, twelveHours, oneDay, twoDays, threeDays, oneWeek, never, noLimit };
    public enum TriggerPoint { onNewTrackTarget, onNewAction, whileTickOnTrackTarget, vmail, telephone, never, newspaperMurder, newspaperArticle };

    public enum TraitConditionType { IfAnyOfThese, IfAllOfThese, IfNoneOfThese, otherAnyOfThese, otherAllOfThese, otherNoneOfThese };

    [System.Serializable]
    public class DDSComponent
    {
        public string name;
        public string id;
    }

    [System.Serializable]
    public class DDSBlockSave : DDSComponent
    {
        public List<DDSReplacement> replacements = new List<DDSReplacement>();

        public DDSReplacement AddReplacement()
        {
            Game.Log("Add replacement");

            //Convert replace to use \n instead of breaks
            DDSReplacement rep = new DDSReplacement { replaceWithID = Toolbox.Instance.GenerateUniqueID() };
            replacements.Add(rep);

            //Write replacement to dictionary
            Strings.WriteToDictionary("dds.blocks", rep.replaceWithID, string.Empty, "New Replacement", string.Empty, 0, false, string.Empty);

            return rep;
        }
    }

    [System.Serializable]
    public class DDSReplacement
    {
        public string replaceWithID;

        public bool useConnection = false;
        public Acquaintance.ConnectionType connection = Acquaintance.ConnectionType.anyoneNotPlayer;

        public bool useDislikeLike = false;
        public float strangerKnown = 0.5f;
        public float dislikeLike = 0.5f;

        public bool useTraits = false;
        public TraitConditionType traitCondition = TraitConditionType.IfAnyOfThese;
        public List<string> traits = new List<string>();
    }

    [System.Serializable]
    public class DDSMessageSave : DDSComponent
    {
        public List<DDSBlockCondition> blocks = new List<DDSBlockCondition>();

        public void AddBlock(string newBlockID)
        {
            string newID = Toolbox.Instance.GenerateUniqueID();
            blocks.Add(new DDSBlockCondition { blockID = newBlockID, instanceID = newID });
            Game.Log("DDS: Added new block to message with instance: " + newID);
        }

        public void RemoveBlock(string instID)
        {
            int index = blocks.FindIndex(item => item.instanceID == instID);

            if (index > -1)
            {
                Game.Log("DDS: Removed block with instance " + instID + " from message");
                blocks.RemoveAt(index);
            }
        }
    }

    [System.Serializable]
    public class DDSBlockCondition
    {
        public string blockID;
        public string instanceID;
        public bool alwaysDisplay = true;
        public int group = 0;

        public bool useTraits = false;
        public TraitConditionType traitConditions = TraitConditionType.IfAnyOfThese;
        public List<string> traits = new List<string>();
    }

    public enum TreeType { conversation, vmail, document, newspaper, misc };

    [System.Serializable]
    public class DDSTreeSave : DDSComponent
    {
        public DDSParticipant participantA = new DDSParticipant();
        public DDSParticipant participantB = new DDSParticipant();
        public DDSParticipant participantC = new DDSParticipant();
        public DDSParticipant participantD = new DDSParticipant();

        public RepeatSetting repeat = RepeatSetting.sixHours;
        public TriggerPoint triggerPoint = TriggerPoint.onNewTrackTarget;

        public List<DDSMessageSettings> messages = new List<DDSMessageSettings>();

        public bool stopMovement = true;
        public bool ignoreGlobalRepeat = false;

        public TreeType treeType = TreeType.conversation;

        public DDSDocument document; //Document data

        public string startingMessage; //NOTE: This is now the INSTANCE Id

        public float treeChance = 1f;
        public int priority = 3;

        //This isn't saved, but in-game it is created upon serialization for speed
        [System.NonSerialized]
        public Dictionary<string, DDSMessageSettings> messageRef;

        public string AddMessage(string newMsgID)
        {
            string newInstID = Toolbox.Instance.GenerateUniqueID();
            Game.Log("DDS: New message instance: " + newInstID); 
            messages.Add(new DDSMessageSettings { msgID = newMsgID, instanceID = newInstID, order = messages.Count });

            return newInstID;
        }

        public void RemoveMessage(string instID)
        {
            int index = messages.FindIndex(item => item.instanceID == instID);

            if (index > -1)
            {
                Game.Log("DDS: Removing message/element");
                messages.RemoveAt(index);
            }
        }

        public string AddElement(string elementName)
        {
            string newInstID = Toolbox.Instance.GenerateUniqueID();
            Game.Log("DDS: New element instance: " + newInstID);
            messages.Add(new DDSMessageSettings { elementName = elementName, instanceID = newInstID, col = Color.white, order = messages.Count });
            return newInstID;
        }
    }

    [System.Serializable]
    public class DDSDocument
    {
        public string background;
        public Image.Type fill = Image.Type.Sliced;
        public Vector2 size;
        public Color colour = Color.white;
    }

    public enum ElementType { messageText, special};

    [System.Serializable]
    public class DDSMessageSettings
    {
        public string msgID;
        public string elementName;
        public string instanceID;
        public int saidBy = 0;
        public int saidTo = 1;
        public Vector2 pos = new Vector2(0f, -64f);
        public Vector2 size = new Vector2(320f, 300f);
        public float rot = 0f;
        public string font = "Halogen";
        public Color col = Color.black;
        public float fontSize = 22;
        public float charSpace = 0;
        public float wordSpace = 0;
        public float lineSpace = 16;
        public float paraSpace = 0;
        public int alignH = 0;
        public int alignV = 0;
        public int fontStyle;
        public int order = 0;
        public bool usePages = false;
        public bool isHandwriting = false;

        public List<DDSMessageLink> links = new List<DDSMessageLink>();
    }

    [System.Serializable]
    public class DDSMessageLink
    {
        public string from;
        public string to;

        public Vector2 delayInterval = new Vector2(0f, 0.01f);

        public bool useWeights = false;
        public float choiceWeight = 0.5f;

        public bool useKnowLike = false;
        public float know = 0.5f;
        public float like = 0.5f;

        public bool useTraits = false;
        public List<string> traits = new List<string>();
        public TraitConditionType traitConditions = TraitConditionType.IfAnyOfThese;
    }

    [System.Serializable]
    public class DDSParticipant
    {
        public bool required = false;
        public Acquaintance.ConnectionType connection = Acquaintance.ConnectionType.anyoneNotPlayer;

        public bool useJobs = false;
        public bool disableInbox = false;
        public List<string> jobs = new List<string>();

        public bool useTraits = false;
        public List<string> traits = new List<string>();
        public TraitConditionType traitConditions = TraitConditionType.IfAnyOfThese;

        public List<TreeTriggers> triggers = new List<TreeTriggers>();
    }
}
