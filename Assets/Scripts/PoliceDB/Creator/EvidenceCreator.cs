﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;

//Creates evidence
//Script pass 1
public class EvidenceCreator : MonoBehaviour
{
	//Check if static references have been added
	public bool globalEntries = false;

	//Singleton pattern
	private static EvidenceCreator _instance;
	public static EvidenceCreator Instance { get { return _instance; } }

	void Awake()
	{
		//Setup singleton
		if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }

    //Get evidence for date (or make it)
    public Evidence GetDateEvidence(string date, string evidenceType = "date", string parentID = "", int owner = -1, int writer = -1, int reciever = -1)
    {
        string generateEvidenceKey = evidenceType + "|" + date + "|" + parentID + "|" + writer + "|" + reciever;

        Evidence findDate = GameplayController.Instance.dateEvidence.Find(item => item.evID == generateEvidenceKey);

        //If we can't find it, make it
        if(findDate == null)
        {
            Evidence parent = null;
            GameplayController.Instance.evidenceDictionary.TryGetValue(parentID, out parent);

            Human ownerH = null;
            CityData.Instance.GetHuman(owner, out ownerH);

            Human writerH = null;
            CityData.Instance.GetHuman(writer, out writerH);

            Human receiverH = null;
            CityData.Instance.GetHuman(reciever, out writerH);

            findDate = EvidenceCreator.Instance.CreateEvidence(evidenceType, generateEvidenceKey, newOwner: ownerH, newWriter: writerH, newReciever: receiverH, newParent: parent) as Evidence;
        }

        return findDate;
    }

    //Get evidence for time while making a time range
    public EvidenceTime GetTimeEvidenceRange(float time, float accuracyRange, bool limitToNow, bool round, int roundToMinutes, string evidenceType = "time", string parentID = "", int writer = -1, int receiver = -1 )
    {
        Vector2 timeRange = Toolbox.Instance.CreateTimeRange(time, accuracyRange, limitToNow, round, roundToMinutes);

        return GetTimeEvidence(timeRange.x, timeRange.y, evidenceType, parentID, writer, receiver);
    }

    public EvidenceTime GetTimeEvidence(string evidenceID)
    {
        EvidenceTime findTime = GameplayController.Instance.timeEvidence.Find(item => item.evID == evidenceID);

        //If we can't find it, make it
        if (findTime == null)
        {
            //Parse key...
            string[] keyParse = evidenceID.Split('|');
            if (keyParse.Length <= 1) return null;

            Evidence parent = null;

            if(keyParse.Length > 3)
            {
                GameplayController.Instance.evidenceDictionary.TryGetValue(keyParse[3], out parent);
            }

            Human writerH = null;

            if(keyParse.Length > 4)
            {
                int w = -1;
                int.TryParse(keyParse[4], out w);
                CityData.Instance.GetHuman(w, out writerH);
            }

            Human receiverH = null;

            if(keyParse.Length > 5)
            {
                int r = -1;
                int.TryParse(keyParse[5], out r);
                CityData.Instance.GetHuman(r, out writerH);
            }

            findTime = EvidenceCreator.Instance.CreateEvidence(keyParse[0], evidenceID, newWriter: writerH, newReciever: receiverH, newParent: parent) as EvidenceTime;
        }

        return findTime;
    }


    //Get evidence for time (or make it)
    public EvidenceTime GetTimeEvidence(float from, float to, string evidenceType = "time", string parentID = "", int writer = -1, int reciever = -1)
    {
        string generateEvidenceKey = evidenceType + "|" + from + "|" + to + "|" + parentID + "|" + writer + "|" + reciever;

        EvidenceTime findTime = GameplayController.Instance.timeEvidence.Find(item => item.evID == generateEvidenceKey);

        //If we can't find it, make it
        if (findTime == null)
        {
            Evidence parent = null;
            GameplayController.Instance.evidenceDictionary.TryGetValue(parentID, out parent);

            Human writerH = null;
            CityData.Instance.GetHuman(writer, out writerH);

            Human receiverH = null;
            CityData.Instance.GetHuman(reciever, out writerH);

            findTime = EvidenceCreator.Instance.CreateEvidence(evidenceType, generateEvidenceKey, newWriter: writerH, newReciever: receiverH, newParent: parent) as EvidenceTime;
        }

        return findTime;
    }

    //New create functions
    public Evidence CreateEvidence(string presetName,
                                    string newID,
                                    Controller newController = null, //Controller passed to the evidence
                                    Human newOwner = null,
                                    Human newWriter = null, //Belongs to/written by
                                    Human newReciever = null, //Sent to
                                    Evidence newParent = null, //Optional parent
                                    bool forceDiscoveryOnCreate = false, //Optionally force discovery on creation
                                    List<object> passedObjects = null //Optionally pass a list of objects
                                    )
    {
        EvidencePreset preset = null;

        presetName = presetName.ToLower();

        if (Toolbox.Instance.evidencePresetDictionary.TryGetValue(presetName, out preset))
        {
            return CreateEvidence(preset, newID, newController, newOwner, newWriter, newReciever, newParent, forceDiscoveryOnCreate, passedObjects);
        }
        else
        {
            Game.LogError("Cannot find evidence " + presetName);
            return null;
        }
    }

    public Evidence CreateEvidence( EvidencePreset preset,
                                    string newID,
                                    Controller newController = null, //Controller passed to the evidence
                                    Human newOwner = null,
                                    Human newWriter = null, //Belongs to/written by
                                    Human newReciever = null, //Sent to
                                    Evidence newParent = null, //Optional parent
                                    bool forceDiscoveryOnCreate = false, //Optionally force discovery on creation
                                    List<object> passedObjects = null //Optionally pass a list of objects
                                   )
    {
        if(preset == null)
        {
            if(newController != null && newParent != null)
            {
                Game.LogError("Null evidence preset (Controller: " + newController.name + " Parent: " + newParent.GetNameForDataKey(Evidence.DataKey.name) + ")");
            }
            else if(newController != null)
            {
                Game.LogError("Null evidence preset (Controller: " + newController.name + ")");
            }
            else if(newParent != null)
            {
                Game.LogError("Null evidence preset (Parent: " + newParent.GetNameForDataKey(Evidence.DataKey.name) + ")");
            }
            else
            {
                Game.LogError("Null evidence preset");
            }
        }

        //Get the class to create using the subclass string
        string searchString = "Evidence";
        if(preset.subClass.Length > 0) searchString = "Evidence" + preset.subClass;

        //Collection of parameters to pass to the created class
        object[] p = { preset, newID, newController, passedObjects };

        Evidence created = null;

        //Create this class by string
        try
        {
            created = System.Activator.CreateInstance(System.Type.GetType(searchString), p) as Evidence;
        }
        catch
        {
            Game.Log("Misc Error: Unable to create subclass " + searchString);
            return null;
        }

        //Ste optional parent
        if (newParent != null)
        {
            created.SetParent(newParent);
        }

        if (newOwner != null)
        {
            created.SetBelongsTo(newOwner);
        }

        if (newWriter != null)
        {
            created.SetWriter(newWriter);
        }

        if(newReciever != null)
        {
            created.SetReciever(newReciever);
        }

        //If game has started, compile now
        if (StartingEvidenceCreator.Instance == null || StartingEvidenceCreator.Instance.called)
        {
            created.Compile();

            //Forcing discovery on create only works in real-time
            if (forceDiscoveryOnCreate) created.SetFound(true);
        }
        //Else add to compiler to be executed on load
        else
        {
            CityConstructor.Instance.evidenceToCompile.Add(created);
        }

        return created;
    }

    //Create fact
    public Fact CreateFact( string presetName,
                            Evidence fromEvidenceSingular = null, //Optional from evidence
                            Evidence toEvidenceSingular = null, //Optional to evidence
                            List<Evidence> fromEvidence = null, //Optional list of from evidence
                            List<Evidence> toEvidence = null, //Optional list of to evidence
                            bool forceDiscoveryOnCreate = false, //Optionally force discovery on creation
                            List<object> passedObjects = null, //Optionally pass a list of objects
                            List<Evidence.DataKey> overrideFromKeys = null, //Optionally override data keys found in fact preset
                            List<Evidence.DataKey> overrideToKeys = null, //Optionally override data keys found in fact preset
                            bool isCustomFact = false //True if this has been created by the player
                            )
    {
        //Get from/to evidence
        if (fromEvidence == null)
        {
            fromEvidence = new List<Evidence>();
            fromEvidence.Add(fromEvidenceSingular);
        }

        if (toEvidence == null)
        {
            toEvidence = new List<Evidence>();
            toEvidence.Add(toEvidenceSingular);
        }

        FactPreset preset = null;

        try
        {
            presetName = presetName.ToLower();
            preset = Toolbox.Instance.factPresetDictionary[presetName];
        }
        catch
        {
            Game.LogError("Cannot find fact " + presetName);
            return null;
        }

        //Search for duplicates (only works for singular from/to evidence)
        if(!preset.allowDuplicates)
        {
            Fact duplicate = GameplayController.Instance.factList.Find(item => item.preset == preset && item.fromEvidence.Count > 0 && item.toEvidence.Count > 0 && item.fromEvidence[0] == fromEvidenceSingular && item.toEvidence[0] == toEvidenceSingular);
            if (duplicate != null)
            {
                Game.Log("Evidence: (Duplicate evidence detected for " + duplicate.GetName() + ", returning duplicate)");
                if (forceDiscoveryOnCreate) duplicate.SetFound(true);
                return duplicate;
            }
        }

        if (!preset.allowReverseDuplicates)
        {
            Fact duplicate = GameplayController.Instance.factList.Find(item => item.preset == preset && item.fromEvidence.Count > 0 && item.toEvidence.Count > 0 && item.fromEvidence[0] == toEvidenceSingular && item.toEvidence[0] == fromEvidenceSingular);
            if (duplicate != null)
            {
                Game.Log("Evidence: (Duplicate evidence detected for " + duplicate.GetName() + ", returning duplicate)");
                if (forceDiscoveryOnCreate) duplicate.SetFound(true);
                return duplicate;
            }
        }

        //Proceed with creation
        //Get the class to create using the subclass string
        string searchString = "Fact" + preset.subClass;

        //Collection of parameters to pass to the created class
        object[] p = { preset, fromEvidence, toEvidence, passedObjects, overrideFromKeys, overrideToKeys, isCustomFact};

        Fact created = null;

        //Create this class by string
        created = System.Activator.CreateInstance(System.Type.GetType(searchString), p) as Fact;

        if (created == null)
        {
            Game.LogError("Unable to create subclass " + searchString);
            return null;
        }

        if (forceDiscoveryOnCreate) created.SetFound(true);

        return created;
    }

    //Create fact from serialized string
    public Fact CreateFactFromSerializedString(string str)
    {
        string[] split = str.Split('|');

        //1st: Preset
        string preset = string.Empty;
        if (split.Length > 0) preset = split[0];

        List<Evidence> fromEvidence = new List<Evidence>();

        //2nd: From evidence
        if(split.Length > 1)
        {
            string[] fromSplit = split[1].Split(',');

            foreach(string s in fromSplit)
            {
                if (s == null || s.Length <= 0) continue;
                Evidence found = null;

                if (Toolbox.Instance.TryGetEvidence(s, out found))
                {
                    fromEvidence.Add(found);
                }
            }
        }

        List<Evidence> toEvidence = new List<Evidence>();

        //3rd: To evidence
        if (split.Length > 2)
        {
            string[] toSplit = split[2].Split(',');

            foreach (string s in toSplit)
            {
                if (s == null || s.Length <= 0) continue;
                Evidence found = null;

                if (Toolbox.Instance.TryGetEvidence(s, out found))
                {
                    toEvidence.Add(found);
                }
            }
        }

        List<Evidence.DataKey> fromDataKeys = new List<Evidence.DataKey>();

        //4th: From data keys
        if (split.Length > 3)
        {
            string[] dkSplit = split[3].Split(',');

            foreach (string s in dkSplit)
            {
                if (s == null || s.Length <= 0) continue;
                int p = -1;

                if(int.TryParse(s, out p))
                {
                    fromDataKeys.Add((Evidence.DataKey)p);
                }
            }
        }

        List<Evidence.DataKey> toDataKeys = new List<Evidence.DataKey>();

        //5th: To data keys
        if (split.Length > 4)
        {
            string[] dkSplit = split[4].Split(',');

            foreach (string s in dkSplit)
            {
                if (s == null || s.Length <= 0) continue;
                int p = -1;

                if (int.TryParse(s, out p))
                {
                    toDataKeys.Add((Evidence.DataKey)p);
                }
            }
        }

        bool isFound = false;
        
        if(split.Length > 5)
        {
            int p = -1;

            if (int.TryParse(split[5], out p))
            {
                isFound = Convert.ToBoolean(p);
            }
        }

        bool isSeen = false;

        if (split.Length > 6)
        {
            int p = -1;

            if (int.TryParse(split[6], out p))
            {
                isSeen = Convert.ToBoolean(p);
            }
        }

        bool isCustom = false;

        if (split.Length > 7)
        {
            int p = -1;

            if (int.TryParse(split[7], out p))
            {
                isCustom = Convert.ToBoolean(p);
            }
        }

        string customName = string.Empty;

        if(isCustom && split.Length > 8)
        {
            customName = split[8];
        }

        Fact newFact = CreateFact(preset, null, null, fromEvidence, toEvidence, isFound, null, fromDataKeys, toDataKeys, isCustom);
        if(isSeen) newFact.SetSeen();
        if (isCustom) newFact.SetCustomName(customName);

        return newFact;
    }
}
