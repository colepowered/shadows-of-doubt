﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "broadcastschedule_data", menuName = "Database/Broadcast Schedule")]

public class BroadcastSchedule : SoCustomComparison
{
	[Header("Contents")]
	public List<BroadcastPreset> broadcasts = new List<BroadcastPreset>();
}
