﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEditor;

//A scriptable object that can be used to make occupation presets.
//Script pass 1
[CreateAssetMenu(fileName = "occupation_data", menuName = "Database/Company/Occupation Preset")]

public class OccupationPreset : SoCustomComparison
{
    public enum workCollar { blueCollar, whiteCollar, pinkCollar, redCollar, goldCollar, orangeCollar, scarletCollar, blackCollar, noCollar };
    public enum workType { Office, Management, Labourer, Janitorial, Retail, Service, Driver, PublicSector, Enforcer, Criminal, Creative, Other, Student, Unemployed, Retired, Illegal };
    public enum ShiftType { morningShift, dayShift, eveningShift, nightShift };
    public enum JobAI { workPosition, random, randomBuilding, passedCompanyPosition};

    //Additional tags to describe position
    public enum workTags
    {
        none, /*1*/dull, /*2*/exciting, /*3*/dangerous, /*4*/menial, /*5*/intern, /*6*/stressful,
        /*7*/
        cushy, /*8*/technical, /*9*/ceo, /*10*/social, /*11*/isolated, /*12*/professional
    };
    public enum Overtime { none, low, medium, high, veryHigh};

    [Header("Category")]
    [Tooltip("Collar colour")]
    //Job category
    //Blue: Labourer
    //White: Office
    //Pink: Service
    //Red: Government
    //Gold: Highly skilled
    //Orange: Prisoner
    //Scarlet: Sex Industry
    //Black: Criminal
    //None: Artist/unemployed
	public workCollar collar;
    [Tooltip("Type of work")]
    public workType work;
    [Tooltip("Additional tags to describe this work")]
    public List<workTags> tags = new List<workTags>();
    [Tooltip("The higher the priority, the more chance the position will be filled")]
    [Range(0, 4)]
    public int jobFillPriority = 2;

    [Header("Outfit")]
    [Tooltip("If this job requires a certain work outfit, list it here...")]
    public List<ClothesPreset> workOutfit;

    [Header("Special Cases")]
    public bool selfEmployed = false;
    public bool receptionist = false;
    public bool canAskAboutJob = true;
    public bool janitor = false;
    public bool security = false;
    public bool isCriminal = false;
    public bool isPublicFacing = false;
    [EnableIf("isCriminal")]
    public int minimumPerCity = 0;
    [EnableIf("isCriminal")]
    public float societalClass = 0.5f;

    [Header("Personality Fit")]
    [Tooltip("Personality is calculated after job assign; how much to scew personality towards this...")]
    public float skewPersonalityTowardsJobFit = 0.4f;

    public bool skewHumility = false;
    [Tooltip("Honesty-Humility (H): sincere, honest, faithful, loyal, modest/unassuming versus sly, deceitful, greedy, pretentious, hypocritical, boastful, pompous")]
    [EnableIf("skewHumility")]
    [Range(0f, 1f)]
    public float humility = 0f;

    public bool skewEmotionality = false;
    [Tooltip("Emotionality (E): emotional, oversensitive, sentimental, fearful, anxious, vulnerable versus brave, tough, independent, self-assured, stable")]
    [EnableIf("skewEmotionality")]
    [Range(0f, 1f)]
    public float emotionality = 0f;

    public bool skewExtraversion = false;
    [Tooltip("Extraversion (X): outgoing, lively, extraverted, sociable, talkative, cheerful, active versus shy, passive, withdrawn, introverted, quiet, reserved")]
    [EnableIf("skewExtraversion")]
    [Range(0f, 1f)]
    public float extraversion = 0f;

    public bool skewAgreeableness = false;
    [Tooltip("Agreeableness (A): patient, tolerant, peaceful, mild, agreeable, lenient, gentle versus ill-tempered, quarrelsome, stubborn, choleric")]
    [EnableIf("skewAgreeableness")]
    [Range(0f, 1f)]
    public float agreeableness = 0f;

    public bool skewConscientiousness = false;
    [Tooltip("Conscientiousness (C): organized, disciplined, diligent, careful, thorough, precise versus sloppy, negligent, reckless, lazy, irresponsible, absent-minded")]
    [EnableIf("skewConscientiousness")]
    [Range(0f, 1f)]
    public float conscientiousness = 0f;

    public bool skewCreativity = false;
    [Tooltip("Openness to Experience (O): intellectual, creative, unconventional, innovative, ironic versus shallow, unimaginative, conventional")]
    [EnableIf("skewCreativity")]
    [Range(0f, 1f)]
    public float creativity = 0f;

    [Header("Work Hours")]
    [Tooltip("Find a shift matching the below enum")]
    public bool shiftTimeIsImportant = false;
    [Tooltip("The employee works this shift (if available)")]
    public ShiftType shiftType = ShiftType.dayShift;
    [Tooltip("Does this job count towards the open coverage of the shift they have?")]
    public bool countsTowardsOpenHoursCoverage = true;
    [Tooltip("The employee can take a break half way through their shift")]
    public bool lunchBreakAllowed = true;

    [Header("AI Behaviour")]
    [Tooltip("Where should the AI go to upon starting the goal?")]
    public JobAI jobAIPosition = JobAI.workPosition;
    [Tooltip("If AI behaviour is set to patrol, are there any rooms in which it is not allowed?")]
    [ReorderableList]
    public List<RoomConfiguration> bannedRooms = new List<RoomConfiguration>();
    [Tooltip("The list of actions the AI will perform inside the 'Work' goal.")]
    public List<AIGoalPreset.GoalActionSetup> actionSetup = new List<AIGoalPreset.GoalActionSetup>();

    [Tooltip("What interactable will the AI work from?")]
    public InteractablePreset.SpecialCase jobPostion = InteractablePreset.SpecialCase.workDesk;
    [Tooltip("Does the AI own their own version of above? If not they will use free available ones.")]
    public bool ownsWorkPosition = false;
    [Tooltip("Where should the AI start to search for their work place?")]
    [ReorderableList]
    public List<RoomConfiguration> preferredRooms = new List<RoomConfiguration>();
    [Space(5)]
    [Tooltip("How often should the AI get up and do other tasks. Time range (game time)")]
    public Vector2 potterFrequency = new Vector2(0.5f, 1f);
    [Tooltip("If true the AI will only potter if there is at least 1 other staff memeber on it's work postion interactable type")]
    public bool onlyPotterIfSomebodyElseWorking = false;
    [Tooltip("List of other tasks to randomly do while on the job.")]
    [ReorderableList]
    public List<AIActionPreset> potterActions = new List<AIActionPreset>();

    [Header("Items")]
    [Tooltip("This person has a name placard at work")]
    public bool namePlacard = true;
    [Tooltip("This person has an employee photo")]
    public bool employeePhoto = true;
    [Tooltip("This person has business cards")]
    public bool businessCards = true;
    [Tooltip("This person has a work rota")]
    public bool workRota = true;
    [Tooltip("This person has an employment contract")]
    public bool employmentContract = true;
    [Tooltip("List of items to add once this job position is filled")]
    public List<InteractablePreset> jobItems = new List<InteractablePreset>();
    [Tooltip("List of items to add to inventory")]
    public List<InteractablePreset> inventoryItems = new List<InteractablePreset>();
    public List<GroupPreset> joinGroups = new List<GroupPreset>();

    [Header("Dialog Options")]
    public List<DialogPreset> addDialog = new List<DialogPreset>();

    [Header("Debug")]
    public OccupationPreset selectedPreset;

    [Button]
    public void CopyOutfitFromSelectedPreset()
    {
        if(selectedPreset != null)
        {
            workOutfit.Clear();
            workOutfit.AddRange(selectedPreset.workOutfit);

#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }
    }
}
