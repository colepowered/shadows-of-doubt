﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "revenge_data", menuName = "Database/Revenge Objective")]

public class RevengeObjective : SoCustomComparison
{
	public bool disabled = false;

    [Header("Trait Weighting")]
	[Range(0, 10)]
	public int baseChance = 1;
	[Space(7)]

	[InfoBox("If enabled: The below HEXACO values will combine for a score of 1 to 10: this will be used to calculate the likihood of this being chosen vs others.")]
	[Tooltip("Use the below hexaco values to match to personality.")]
	public bool useHEXACO = false;
	[Range(0, 10)]
	[EnableIf("useHEXACO")]
	public int feminineMasculine = 5;
	[Tooltip("Honesty-Humility (H): sincere, honest, faithful, loyal, modest/unassuming versus sly, deceitful, greedy, pretentious, hypocritical, boastful, pompous")]
	[Range(0, 10)]
	[EnableIf("useHEXACO")]
	public int humility = 5;
	[Tooltip("Emotionality (E): emotional, oversensitive, sentimental, fearful, anxious, vulnerable versus brave, tough, independent, self-assured, stable")]
	[Range(0, 10)]
	[EnableIf("useHEXACO")]
	public int emotionality = 5;
	[Tooltip("Extraversion (X): outgoing, lively, extraverted, sociable, talkative, cheerful, active versus shy, passive, withdrawn, introverted, quiet, reserved")]
	[Range(0, 10)]
	[EnableIf("useHEXACO")]
	public int extraversion = 5;
	[Tooltip("Agreeableness (A): patient, tolerant, peaceful, mild, agreeable, lenient, gentle versus ill-tempered, quarrelsome, stubborn, choleric")]
	[Range(0, 10)]
	[EnableIf("useHEXACO")]
	public int agreeableness = 5;
	[Tooltip("Conscientiousness (C): organized, disciplined, diligent, careful, thorough, precise versus sloppy, negligent, reckless, lazy, irresponsible, absent-minded")]
	[Range(0, 10)]
	[EnableIf("useHEXACO")]
	public int conscientiousness = 5;
	[Tooltip("Openness to Experience (O): intellectual, creative, unconventional, innovative, ironic versus shallow, unimaginative, conventional")]
	[Range(0, 10)]
	[EnableIf("useHEXACO")]
	public int creativity = 5;

	[Space(7)]
	[InfoBox("If enabled: The below traits will be used to calculate the likihood of this being chosen vs others.")]
	[Tooltip("Use character traits to match to personality.")]
	public bool useTraits = false;
	public List<ClothesPreset.TraitPickRule> characterTraitsPoster = new List<ClothesPreset.TraitPickRule>();
	public List<ClothesPreset.TraitPickRule> characterTraitsPurp = new List<ClothesPreset.TraitPickRule>();
	public List<SpecialConditions> specialConditions = new List<SpecialConditions>();

	public enum SpecialConditions { mustHaveWindows, trackProgressFromAddressQuestion, trackProgressFromNameQuestion };

	[Header("Setup")]
	public string d0Name;
	public string d1Name;
	public string idTargetName;
	public JobPreset.JobTag tag;
	[Space(7)]
	//public JobPreset.LeadCitizen target;
	//public JobPreset.JobSpawnWhere location;
	public InterfaceControls.Icon icon = InterfaceControls.Icon.resolve;
	[InfoBox("This can be used to dictate an amount; eg how much damage to cause at a property")]
	public Vector2 passedNumberRange = Vector2.zero;
	[Tooltip("Multiplies the rewards based on the above number")]
	public Vector2 rewardMultiplier = new Vector2(1f, 1.5f);
	[Tooltip("Name as part of resolve questions")]
	public string resolveQuestionName;
	public string resolveQuestionNameAlternate;
	[Space(10)]
	[Tooltip("Refers to an answer method within this script that is used to check")]
	public string answerMethod = "Vandalism";

	public float Vandalism(int target, int location, float amount)
    {
		NewAddress add = null;
		float dmg = 0f;

		if (CityData.Instance.addressDictionary.TryGetValue(location, out add))
		{
			dmg = add.GetVandalismDamage();

			Game.Log("Jobs: Vandalism damage at " + add.name + " is " + dmg);
		}
		else Game.Log("Jobs: Unable to get vandalism damage from address ID " + location);

		return dmg;
    }

	public float VandalismTrash(int target, int location, float amount)
	{
		NewAddress add = null;
		float dmg = 0f;

		if (CityData.Instance.addressDictionary.TryGetValue(location, out add))
		{
			dmg = add.GetVandalismDamage(false, false, true);

			Game.Log("Jobs: Vandalism damage (trash only) at " + add.name + " is " + dmg);
		}
		else Game.Log("Jobs: Unable to get vandalism damage from address ID " + location);

		return dmg;
	}

	public float VandalismWindow(int target, int location, float amount)
	{
		NewAddress add = null;
		float dmg = 0f;

		if (CityData.Instance.addressDictionary.TryGetValue(location, out add))
		{
			dmg = add.GetVandalismDamage(false, true, false);

			Game.Log("Jobs: Vandalism damage (windows only) at " + add.name + " is " + dmg);
		}
		else Game.Log("Jobs: Unable to get vandalism damage from address ID " + location);

		return dmg;
	}

	public bool Handcuff(int target, int location, float amount)
    {
		Human cit = null;

		if (CityData.Instance.GetHuman(target, out cit))
		{
			//Restrained
			if(cit.ai != null && cit.ai.restrained)
            {
				//Somewhere quiet
				if(!cit.currentGameLocation.IsPublicallyOpen(false))
                {
					return true;
                }
            }
		}

		return false;
    }

	public bool BeatUp(int target, int location, float amount)
	{
		Human cit = null;

		if (CityData.Instance.GetHuman(target, out cit))
		{
			//KO'd
			if (cit.ai != null && cit.ai.ko && !cit.isDead)
			{
				return true;
			}
		}

		return false;
	}

	public bool KickDownDoor(int target, int location, float amount)
	{
		NewAddress add = null;

		if (CityData.Instance.addressDictionary.TryGetValue(location, out add))
		{
			if(add.entrances.Exists(item => item.door != null && item.door.wall.currentDoorStrength <= 0.01f))
            {
				return true;
            }
		}

		return false;
	}

	public bool ManualTrigger(int target, int location, float amount)
	{
		//This needs to be triggered manually...

		return false;
	}
}
