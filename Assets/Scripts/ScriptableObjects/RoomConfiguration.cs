﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine.Rendering;

//A scriptable object that can be used to make window style presets.
//Script pass 1
[CreateAssetMenu(fileName = "roomconfig_data", menuName = "Database/Decor/Room Configuration")]

public class RoomConfiguration : SoCustomComparison
{
    public enum DecorSetting { ownStyle, borrowFromAdjoining, borrowFromBuilding, borrowFromBelow };
    public enum RoomZoning { lobby, residential, commerical, industrial, municpial, park };
    public enum Forbidden { alwaysAllowed, alwaysForbidden, allowedDuringOpenHours};

    [Header("Type")]
    [Tooltip("The room type: Dictates layout parameters")]
    public RoomTypePreset roomType;
    [Tooltip("The room class: Dictates what decor and furniture this has")]
    public RoomClassPreset roomClass;

    [Header("Integration Rules")]
    [Tooltip("If there's not enough room, section off this room to include its vital elements")]
    public bool canBeOpenPlan = false;
    [EnableIf("canBeOpenPlan")]
    public RoomTypePreset openPlanRoom;

    [Header("Doors")]
    [Tooltip("Allow security doors to spawn on exits for this room?")]
    public SecurityDoorRule securityDoors = SecurityDoorRule.never;

    public enum SecurityDoorRule { never, allAdjoining, onlyToOtherAddress, onlyToStairwell};

    [Header("Special Rules")]
    [Tooltip("Limit security camera")]
    public bool limitSecurityCameras = false;
    [EnableIf("limitSecurityCameras")]
    [Range(0, 5)]
    public int securityCameraLimit = 1;

    [Header("Lighting")]
    [Tooltip("Use main lights")]
    public bool useMainLights = true;
    [Tooltip("If set to false then this room will use timer lights")]
    public bool useLightSwitches = true;
    [Tooltip("At the start of the game, are the main lights on or off?")]
    public bool lightsOnAtStart = true;
    //[Tooltip("If true the lights will be kept on when an AI leaves the room")]
    //public bool keepLightsOn = true;
    [Tooltip("If true, boost the amount of light from main lights in this room")]
    public bool wellLit = false;
    [Tooltip("If true, the game will automatically disable lights on floors that are 2 or more floors away from the player, or out of their vicinity.")]
    public bool autoDisableLightsOutOfVicinity = true;
    [EnableIf("autoDisableLightsOutOfVicinity")]
    [Tooltip("If true, the game will automatically disable lights on floors that are 2 or more floors away from the player, or out of their vicinity (but not if a stairwell)")]
    public bool onlyAutoDisableInNonStairwell = false;
    [Tooltip("If true use an area light per zone in addition to normal lights")]
    public bool useAdditionalAreaLights = false;
    [Tooltip("If true, use district settings as a base for colour and brightness settings")]
    public bool useDistrictSettingsAsBase = false;
    [EnableIf("useAdditionalAreaLights")]
    public int minimumLightZoneSizeForAreaLights = 4;
    [EnableIf("useAdditionalAreaLights")]
    public Vector3 areaLightOffset;
    [EnableIf("useAdditionalAreaLights")]
    public float areaLightBrightness = 1000f;
    [EnableIf("useAdditionalAreaLights")]
    public Color areaLightColor = Color.white;
    [EnableIf("useAdditionalAreaLights")]
    public float areaLightRange = 10f;
    [Tooltip("Multiply the area light size by this")]
    [EnableIf("useAdditionalAreaLights")]
    public float areaLightCoverageMultiplier = 1f;
    [Tooltip("If true, boost the ceiling emission by this colour when main lights are on")]
    public bool boostCeilingEmission = false;
    [Tooltip("If true, boost the ceiling emission by this colour when main lights are on")]
    public Color ceilingEmissionBoost = Color.black;
    [Tooltip("Chance of having a ceiling fan on light fittings")]
    [Range(0f, 1f)]
    public float chanceOfCeilingFans = 0f;
    [Tooltip("Give the base lighting a shadow tint?")]
    public bool baseLightingShadowTint = true;
    [Tooltip("Fake caustics by lerping the shadow tint to decor and time of day colours...")]
    [Range(0f, 1f)]
    public float baseLightingShadowTintIntensity = 0.05f;
    [Tooltip("Give the base lighting a shadow tint?")]
    [EnableIf("useAdditionalAreaLights")]
    public bool areaLightingShadowTint = true;
    [Tooltip("Fake caustics by lerping the shadow tint to decor and time of day colours...")]
    [Range(0f, 1f)]
    [EnableIf("areaLightingShadowTint")]
    public float areaLightingShadowTintIntensity = 0.3f;
    [EnableIf("areaLightingShadowTint")]
    public bool overrideAreaLightShadowTint = false;
    [EnableIf("areaLightingShadowTint")]
    public Color areaLightShadowTintOverride;
    [EnableIf("useAdditionalAreaLights")]
    [Range(0f, 1f)]
    public float areaLightShadowDimmer = 1f;

    [Header("Lighting AI Behaviour")]
    [InfoBox("These settings can be overriden by AI actions and goals", EInfoBoxType.Normal)]
    public List<AILightingBehaviour> lightingBehaviour = new List<AILightingBehaviour>();

    [System.Serializable]
    public class AILightingBehaviour
    {
        public enum TimeOfDay { always, daytime, evening};
        public enum LightingPreference { mainOn, secondaryOn, eitherPriorityMain, eitherPrioritySecondary, allOff, mainOff, secondaryOff, none, mainOnSecondaryAny };

        public TimeOfDay dayRule = TimeOfDay.always;
        public LightingPreference passthroughBehaviour = LightingPreference.eitherPriorityMain;
        public LightingPreference destinationBehaviour = LightingPreference.mainOn;
        public LightingPreference exitRoomBehaviour = LightingPreference.none;
        public LightingPreference exitGameLocationBehaviour = LightingPreference.allOff;
    }

    [Header("Colour Scheme")]
    [Tooltip("Used when picking a colour scheme for this: How clean/corporate/soulless is this room?")]
    [Range(0, 10)]
    public int cleanness = 1;
    [Tooltip("Force a selection of these colour schemes...")]
    public List<ColourSchemePreset> forceColourSchemes = new List<ColourSchemePreset>();
    [Tooltip("Minimum level of grubiness this room can have....")]
    [Range(0f, 1f)]
    public float minimumGrubiness = 0f;
    [Tooltip("Maximum level of grubiness this room can have....")]
    [Range(0f, 1f)]
    public float maximumGrubiness = 1f;
    public DecorSetting decorSetting = DecorSetting.ownStyle;
    [Tooltip("If true other adjacent rooms with the 'borrow from adjacent' setting won't copy this style.")]
    public bool excludeFromOthersCopyingDecorStyle = false;
    [Tooltip("Use an override material if this is on the ground floor (picked from this list, saved in building class.)")]
    public float chanceOfOverrideMatIfGroundFloor = 0f;
    [Tooltip("Use an override material if this is in the basement (picked from this list, saved in building class.)")]
    public float chanceOfOverrideMatIfBasement = 0f;
    [Tooltip("Use an override material if this room contains stairs (picked from this list, saved in building class.)")]
    public float chanceOfOverrideMatIfStairwell = 0f;
    [Tooltip("List of override materials")]
    [ReorderableList]
    public List<MaterialGroupPreset> floorOverrides = new List<MaterialGroupPreset>();
    [ReorderableList]
    public List<MaterialGroupPreset> wallOverrides = new List<MaterialGroupPreset>();
    [ReorderableList]
    public List<MaterialGroupPreset> ceilingOverrides = new List<MaterialGroupPreset>();
    [Space(7)]
    [Tooltip("The priority given to decorating: Higher priority rooms will override size variables of others.")]
    [Range(0, 10)]
    public int decorationPriority = 5;

    [Header("Ownership")]
    [Tooltip("Can this room be owned by anyone?")]
    public bool useOwnership = false;
    [Tooltip("Assign owners to this furniture")]
    [EnableIf("useOwnership")]
    public int assignBelongsToOwners = 0;
    [Tooltip("If this is checked the game will assign this object to a couple")]
    [EnableIf("useOwnership")]
    public bool preferCouples = false;
    [Tooltip("If this isn't null, the game will use a job to assign ownership to this room")]
    [EnableIf("useOwnership")]
    [ReorderableList]
    public List<OccupationPreset> belongsToJob;

    [Header("Doors")]
    [Tooltip("If this features a door to the outside, use this preset")]
    public DoorPreset exteriorDoor;
    [Tooltip("If this features a door to outside this address, use this preset")]
    public DoorPreset addressDoor;
    [Tooltip("If this features a door to another room in this address, use this preset")]
    public DoorPreset internalDoor;
    [Tooltip("Which room should be the passworded room, ie the place to store the key in?")]
    [Range(0, 10)]
    public int passwordPriority = 1;
    [Tooltip("For doors belonging to this room, prefer the password from...")]
    public RoomPasswordPreference preferredPassword = RoomPasswordPreference.thisRoom;
    public enum RoomPasswordPreference { interactableBelongsTo, thisRoom, thisAddress};
    [Tooltip("If this spawns a door that requires a key, place it here...")]
    public List<KeyPlacement> placeKey = new List<KeyPlacement>();
    public enum KeyPlacement { thisAddress, belongsToHome, belongsToWork};
    public InteractablePreset.OwnedPlacementRule keyOwnershipPlacement = InteractablePreset.OwnedPlacementRule.both;
    [Tooltip("Use these steps")]
    public GameObject steps;

    [Header("Custom Walls")]
    public DoorPairPreset replaceWindows;
    public DoorPairPreset replaceWalls;
    public DoorPairPreset replaceEntrance;
    [Tooltip("By default, only outside walls are replaced here. Check to replace inside walls...")]
    public bool replaceInsideAlso = false;
    [Tooltip("Only replace above if the other side is one of these rooms...")]
    public bool replaceOnlyIfOtherIs = false;
    [EnableIf("replaceOnlyIfOtherIs")]
    public List<RoomTypePreset> onlyReplaceIf = new List<RoomTypePreset>();

    [Tooltip("Force inclusion on the street light lighting layer.")]
    public bool forceStreetLightLayer = false;
    [Tooltip("Draw the current building model when in this room.")]
    public bool drawBuildingModel = false;

    [Header("Wall Frontage")]
    [ReorderableList]
    public List<WallFrontage> wallFrontage = new List<WallFrontage>();

    [System.Serializable]
    public class WallFrontage
    {
        public string name;
        public DoorPairPreset wallPreset;
        public List<WallFrontageClass> insideFrontage;
        public List<WallFrontageClass> outsideFrontage;
        [Tooltip("This entry is only valid if the wall faces onto the outside")]
        public bool onlyIfBorderingOutside = false;
        //[Tooltip("If true this will allow frontages from both rooms (either side). False will allow just one.")]
        //public bool allowDualFrontages = false;
        public Vector3 localOffset = Vector3.zero;
        public bool limitToBuildingTypes = false;
        public List<BuildingPreset> limitedToBuildings;
    }

    [Tooltip("Used for fake roofs for things like rooftop air vents. Only one wall frontage allowed per node")]
    public bool oneFrontagePerNode = false;

    [Header("Air Vents")]
    public int maximumVents = 1;
    [Range(0, 10)]
    public int chanceOfRoofVent = 0;
    [Range(0, 10)]
    public int chanceOfWallVentUpper = 0;
    [Range(0, 10)]
    public int chanceOfWallVentLower = 0;
    [Tooltip("If true this room allows upper-wall level air ducts (below ceiling height)")]
    public bool allowUpperWallLevelDucts = false;
    [Tooltip("Only allow upper wall level ducts if floor height is 0")]
    [EnableIf("allowUpperWallLevelDucts")]
    public bool onlyAllowUpperIfFloorLevelIsZero = true;
    [Tooltip("Limit the number of upper level ducts")]
    public int limitUpperLevelDucts = 99;
    [Tooltip("If true this room allows lower-wall level air ducts (below standing height)")]
    public bool allowLowerWallLevelDucts = false;

    [Header("Environment")]
    [Tooltip("Use a specific profile for this room")]
    public bool overrideAddressEnvironment = false;
    [EnableIf("overrideAddressEnvironment")]
    public SessionData.SceneProfile sceneClean = SessionData.SceneProfile.indoors;
    [EnableIf("overrideAddressEnvironment")]
    public SessionData.SceneProfile sceneDirty = SessionData.SceneProfile.grimey;
    //public VolumeProfile environmentProfile;
    [Tooltip("Affects lighting volumetrics; creating a smokey atmosphere with a higher value.")]
    [Range(0, 1)]
    public float baseRoomAtmosphere = 0.5f;
    [Tooltip("Force the nodes in this room to register as outside or inside...")]
    public OutsideSetting forceOutside = OutsideSetting.dontChange;

    public enum OutsideSetting { dontChange, forceOutside, forceInside};

    [Header("Audio")]
    public AmbientZone ambientZone;

    [Header("Fingerprints")]
    [Tooltip("Should there be fingerprints here?")]
    public bool fingerprintsEnabled = true;
    [Tooltip("Should there be footprints here?")]
    public bool footprintsEnabled = true;
    [Tooltip("The source of the prints")]
    public PrintsSource printsSource = PrintsSource.inhabitants;
    [Tooltip("Fingerprint density on walls")]
    [Range(0f, 2f)]
    public float fingerprintWallDensity = 1f;
    public enum PrintsSource { owners, inhabitants, buildingResidents, customersAll, customersMale, customersFemale, publicAll, inhabitantsAndCustomers, writers, receivers, ownersAndWriters, ownersWritersReceivers};

    [Header("Other")]
    public bool allowCoving = false;
    [Tooltip("Allow bugs to be spawned in this room")]
    public bool allowBugs = false;
    [EnableIf("allowBugs")]
    [Tooltip("Number of bugs = number of nodes * grubiness * this")]
    public float bugAmountMultiplier = 0.75f;

    [Tooltip("If true the player will be tresspassing when here")]
    public Forbidden forbidden = Forbidden.alwaysAllowed;
    [Tooltip("The player is allowed here after they have given a correct password (if set on the address preset)")]
    public bool allowedIfGivenCorrectPassword = true;
    [Tooltip("Allow AI here if the password setting is on in the address preset")]
    public bool AIknowPassword = true;

    [Tooltip("Severity for being caught when this is forbidden (0 = asked to leave, 2 = combat on sight)")]
    [Range(0, 2)]
    public int escalationLevelNormal = 0;
    [Tooltip("Severity for being caught when this is forbidden when after hours (0 = asked to leave, 2 = combat on sight)")]
    [Range(0, 2)]
    public int escalationLevelAfterHours = 0;

    [Tooltip("All object placements in this room have this increased security level")]
    [Range(0, 4)]
    public int securityLevel = 0;

    [Tooltip("If true, personal affects of citizens can be placed in this room")]
    public bool allowPersonalAffects = true;

    public bool overrideMaxFurnitureClusters = false;
    [EnableIf("overrideMaxFurnitureClusters")]
    public int overridenMaxFurniture = 42;

    public bool overrideAttemptsPerNodeMultiplier = false;
    [EnableIf("overrideAttemptsPerNodeMultiplier")]
    public float overridenAttemptsPerNode = 1.75f;

    [Tooltip("When ranking shadiness for certain jobs/systems the base value")]
    [Range(0, 10)]
    public int shadinessValue = 0;

    [Tooltip("AI can mug here")]
    public bool allowMuggings = false;
    [Tooltip("Player can awaken here after mugging")]
    public bool muggingAwakenRoom = false;

    [Header("Debug")]
    public RoomConfiguration debugRoom;

    [Button]
    public void CopyWallFrontage()
    {
        if(debugRoom != null)
        {
            wallFrontage = new List<WallFrontage>(debugRoom.wallFrontage);

            //Save changes
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
#endif
        }
    }

    [Button]
    public void AddWallFrontage()
    {
        if (debugRoom != null)
        {
            wallFrontage.AddRange(debugRoom.wallFrontage);

            //Save changes
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this);
#endif
        }
    }
}
