using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System;
using System.IO;
using System.Linq;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Rendering;

public class MaterialsController : MonoBehaviour
{
    //Material librar

    [Header("Materials Library")]
    public Dictionary<Toolbox.MaterialKey, Material> commonMaterialsLibrary = new Dictionary<Toolbox.MaterialKey, Material>();
    public Dictionary<Toolbox.MaterialKey, List<Material>> uniqueMaterialsLibrary = new Dictionary<Toolbox.MaterialKey, List<Material>>();
    public Dictionary<FootprintMaterialKey, Material> footprintMaterialLibrary = new Dictionary<FootprintMaterialKey, Material>();

    [Space(5)]
    [ReadOnly]
    [InfoBox("How many total materials have been created through this material library system")]
    public int materialCount = 0;
    [Space(5)]
    [ReadOnly]
    [InfoBox("How many material instances have been 'saved' from new instantiation afresh because of the dictionary")]
    public int materialInstancesAvertedByCommonDictionary = 0;
    [Space(5)]
    [ReadOnly]
    [InfoBox("How many non-instances can be used (ie direct use of the base material)")]
    public int useOfBaseMaterials = 0;
    [Space(5)]
    [ReadOnly]
    [InfoBox("How many material instances are created by lights? (On/off instanced of materials assigned to lights)")]
    public int lightMaterialInstances = 0;
    [Space(5)]
    [ReadOnly]
    public int footprintMaterials = 0;
    [Space(5)]
    [ReadOnly]
    [InfoBox("How many material instances have been 'saved' from new instantiation afresh because of the dictionary")]
    public int footprintInstancesAvertedByDictionary = 0;

    [Header("Footprint Settings")]
    public Material footprintMaterialShoe;
    public Material footprintMaterialBoot;
    public Material footprintMaterialHeel;
    public Color dirtColour = Color.black;
    public Color bloodColour = Color.red;

    [Space(7)]
    public List<MaterialDebug> commonMaterialsDebug = new List<MaterialDebug>();
    public List<MaterialDebug> uniqueMaterialsDebug = new List<MaterialDebug>();

    [System.Serializable]
    public class MaterialDebug
    {
        public string name;
        public Toolbox.MaterialKey key;
        public Material mat;
    }

    [System.Serializable]
    public struct FootprintMaterialKey
    {
        public int type;
        public float strength;
        public float blood;

        public bool Equals(FootprintMaterialKey other)
        {
            return Equals(other, this);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var objectToCompareWith = (FootprintMaterialKey)obj;

            return objectToCompareWith.type == type && Mathf.Approximately(objectToCompareWith.strength, strength) && Mathf.Approximately(objectToCompareWith.blood, blood);
        }

        public override int GetHashCode()
        {
            HashCode hc = new HashCode();
            hc.Add(type);
            hc.Add(strength);
            hc.Add(blood);
            return hc.ToHashCode();
        }

        public static bool operator ==(FootprintMaterialKey c1, FootprintMaterialKey c2)
        {
            return c1.Equals(c2);
        }

        public static bool operator !=(FootprintMaterialKey c1, FootprintMaterialKey c2)
        {
            return !c1.Equals(c2);
        }
    }

    //Singleton pattern
    private static MaterialsController _instance;
    public static MaterialsController Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private static readonly string MATERIAL_NO_MAT_COLOUR_KEY = "NoMatColour";
    private static readonly string MATERIAL_RAIN_WINDOW_GLASS_KEY = "RainWindowGlass";
    private static readonly string MATERIAL_BASE_COLOR_KEY = "_BaseColor";
    private static readonly string MATERIAL_COLOR1_KEY = "_Color1";
    private static readonly string MATERIAL_COLOR2_KEY = "_Color2";
    private static readonly string MATERIAL_COLOR3_KEY = "_Color3";
    private static readonly string MATERIAL_GRUB_AMOUNT_KEY = "_GrubAmount";

    //Use a material group to attempt to replace the material of a prefab
    public Material SetMaterialGroup(GameObject model, MaterialGroupPreset preset, Toolbox.MaterialKey key, bool forceUniqueInstance = false, MeshRenderer renderer = null)
    {
        if (model == null || preset == null) return null;

        Material retMaterial = preset.material;

        //Will this be different to the default material?
        bool isDifferentFromDefault = false;
        bool isUsingCustomShader = true;

        //Main colour
        Color mainColour = retMaterial.GetColor(MATERIAL_BASE_COLOR_KEY);

        if (key.mainColour != Color.clear && key.mainColour != mainColour)
        {
            mainColour = key.mainColour;
            isDifferentFromDefault = true;
        }

        //Colour 1
        Color colour1 = Color.clear;
        Color colour2 = Color.clear;
        Color colour3 = Color.clear;

        //Check if this is my custom shader...
        if (retMaterial.HasProperty(MATERIAL_COLOR1_KEY))
        {
            isUsingCustomShader = true;
        }
        else isUsingCustomShader = false;

        if (isUsingCustomShader)
        {
            colour1 = retMaterial.GetColor(MATERIAL_COLOR1_KEY);

            if (key.colour1 != Color.clear)
            {
                colour1 = key.colour1;
                isDifferentFromDefault = true;
            }

            //Colour 2
            colour2 = retMaterial.GetColor(MATERIAL_COLOR2_KEY);

            if (key.colour2 != Color.clear)
            {
                colour2 = key.colour2;
                isDifferentFromDefault = true;
            }

            //Colour 3
            colour3 = retMaterial.GetColor(MATERIAL_COLOR3_KEY);

            if (key.colour3 != Color.clear)
            {
                colour3 = key.colour3;
                isDifferentFromDefault = true;
            }
        }

        //Create a new material instance
        if (isDifferentFromDefault || forceUniqueInstance)
        {
            //Create a key; we can then look for this in the common library or use it to create a new material...
            Toolbox.MaterialKey newKey = new Toolbox.MaterialKey();
            newKey.baseMaterial = preset.material;
            newKey.mainColour = key.mainColour;
            newKey.colour1 = key.colour1;
            newKey.colour2 = key.colour2;
            newKey.colour3 = key.colour3;
            newKey.grubiness = Toolbox.Instance.RoundToPlaces(key.grubiness, 1);

            //Does this material exist already?
            Material foundMaterial = null;

            if (!forceUniqueInstance && commonMaterialsLibrary.TryGetValue(newKey, out foundMaterial))
            {
                if (renderer != null) renderer.sharedMaterial = foundMaterial;
                else
                {
                    MeshRenderer r = model.GetComponent<MeshRenderer>();
                    if (r != null) r.sharedMaterial = foundMaterial; //Apply material
                }

                materialInstancesAvertedByCommonDictionary++;

                return retMaterial;
            }
            else
            {
                //Create a new material and save it in the dictionary
                Material newMat = Instantiate(preset.material);
                newMat.name = preset.material.name + " [" + newKey.GetHashCode() + "]";

                //Set colours
                newMat.SetColor(MATERIAL_BASE_COLOR_KEY, mainColour);

                if (isUsingCustomShader)
                {
                    newMat.SetColor(MATERIAL_COLOR1_KEY, colour1);
                    newMat.SetColor(MATERIAL_COLOR2_KEY, colour2);
                    newMat.SetColor(MATERIAL_COLOR3_KEY, colour3);
                    newMat.SetFloat(MATERIAL_GRUB_AMOUNT_KEY, key.grubiness);
                    //newMat.SetVector("_GrubAmount", new Vector4(key.grubiness, 0, 0, 0));
                }

                if(forceUniqueInstance)
                {
                    if (Game.Instance.devMode && Game.Instance.collectDebugData)
                    {
                        if(!uniqueMaterialsLibrary.ContainsKey(newKey))
                        {
                            uniqueMaterialsLibrary.Add(newKey, new List<Material>());
                        }

                        uniqueMaterialsLibrary[newKey].Add(newMat); //We might want to debug this at some point, so save it here...
                    }
                }
                else
                {
                    //If this isn't a forced unique instance, add it to the common materials
                    if (!commonMaterialsLibrary.ContainsKey(newKey))
                    {
                        commonMaterialsLibrary.Add(newKey, newMat);
                    }
                }

                materialCount++;

                if (renderer != null) renderer.sharedMaterial = newMat;
                else model.GetComponent<MeshRenderer>().sharedMaterial = newMat; //Apply material

                return newMat;
            }
        }
        //Use the base material only
        else
        {
            if (renderer != null) renderer.sharedMaterial = preset.material;
            else
            {
                try
                {
                    model.GetComponent<MeshRenderer>().sharedMaterial = preset.material; //Apply material
                }
                catch
                {

                }
            }

            useOfBaseMaterials++;
            return preset.material;
        }
    }

    //Apply a material key to a model
    public Material ApplyMaterialKey(GameObject model, Toolbox.MaterialKey key)
    {
        if (model == null) return null;
        if (model.CompareTag(MATERIAL_NO_MAT_COLOUR_KEY)) return null;

        MeshRenderer renderer = model.GetComponent<MeshRenderer>();

        return ApplyMaterialKey(renderer, key);
    }

    //Apply a material key to a mesh
    public Material ApplyMaterialKey(MeshRenderer renderer, Toolbox.MaterialKey key)
    {
        if (renderer == null) return null;
        if (renderer.gameObject.CompareTag(MATERIAL_NO_MAT_COLOUR_KEY)) return null;
        if (renderer.gameObject.CompareTag(MATERIAL_RAIN_WINDOW_GLASS_KEY)) return null;

        //Game.Log("Applying material to " + renderer.transform.name);

        Material retMaterial = renderer.sharedMaterial;
        if (retMaterial == null) return null;
        if (!retMaterial.HasProperty(MATERIAL_BASE_COLOR_KEY)) return null; //This must be another shader...

        //Will this be different to the default material?
        bool isDifferentFromDefault = false;
        bool isUsingCustomShader = true;

        //Main colour
        Color mainColour = Color.clear;

        //Game.Log("Getting _BaseColor in material " + retMaterial.name);
        mainColour = retMaterial.GetColor(MATERIAL_BASE_COLOR_KEY);

        if (key.mainColour != Color.clear && key.mainColour != mainColour)
        {
            //Game.Log("Material: " + key.baseMaterial + " main color key for + " + renderer.transform.name + " is " + key.mainColour + " (different from default)");
            mainColour = key.mainColour;
            isDifferentFromDefault = true;
        }

        //Colour 1
        Color colour1 = Color.clear;
        Color colour2 = Color.clear;
        Color colour3 = Color.clear;

        //Check if this is my custom shader...
        if (retMaterial.HasProperty(MATERIAL_COLOR1_KEY))
        {
            isUsingCustomShader = true;
        }
        else isUsingCustomShader = false;

        if (isUsingCustomShader)
        {
            colour1 = retMaterial.GetColor(MATERIAL_COLOR1_KEY);

            if (key.colour1 != Color.clear)
            {
                colour1 = key.colour1;
                isDifferentFromDefault = true;
            }

            //Colour 2
            colour2 = retMaterial.GetColor(MATERIAL_COLOR2_KEY);

            if (key.colour2 != Color.clear)
            {
                colour2 = key.colour2;
                isDifferentFromDefault = true;
            }

            //Colour 3
            colour3 = retMaterial.GetColor(MATERIAL_COLOR3_KEY);

            if (key.colour3 != Color.clear)
            {
                colour3 = key.colour3;
                isDifferentFromDefault = true;
            }

            if (key.grubiness != 0f)
            {
                isDifferentFromDefault = true;
            }
        }

        if (isDifferentFromDefault)
        {
            //Create a key
            Toolbox.MaterialKey newKey = new Toolbox.MaterialKey();
            newKey.baseMaterial = retMaterial;
            newKey.mainColour = key.mainColour;
            newKey.colour1 = key.colour1;
            newKey.colour2 = key.colour2;
            newKey.colour3 = key.colour3;
            newKey.grubiness = Toolbox.Instance.RoundToPlaces(key.grubiness, 1);

            //Does this material exist already?
            Material foundMaterial = null;

            if (commonMaterialsLibrary.TryGetValue(newKey, out foundMaterial))
            {
                //Game.Log("Material: " + key.baseMaterial + " main color key for + " + renderer.transform.name + " is " + key.mainColour + " (different from default) FOUND MAT");

                renderer.sharedMaterial = foundMaterial; //Apply material
                materialInstancesAvertedByCommonDictionary++;

                return retMaterial;
            }
            else
            {
                //Game.Log("Material: " + key.baseMaterial + " main color key for + " + renderer.transform.name + " is " + key.mainColour + " (different from default) NEW MAT");

                //Create a new material and save it in the dictionary
                Material newMat = Instantiate(retMaterial);
                newMat.name = retMaterial.name + " [" + newKey.GetHashCode() + "]";

                //Set colours
                newMat.SetColor(MATERIAL_BASE_COLOR_KEY, mainColour);

                if (isUsingCustomShader)
                {
                    newMat.SetColor(MATERIAL_COLOR1_KEY, colour1);
                    newMat.SetColor(MATERIAL_COLOR2_KEY, colour2);
                    newMat.SetColor(MATERIAL_COLOR3_KEY, colour3);
                    newMat.SetFloat(MATERIAL_GRUB_AMOUNT_KEY, key.grubiness);
                    //newMat.SetVector("_GrubAmount", new Vector4(key.grubiness, 0, 0, 0));
                }

                commonMaterialsLibrary.Add(newKey, newMat);
                materialCount++;

                renderer.sharedMaterial = newMat; //Apply material
                return newMat;
            }
        }
        else
        {
            renderer.sharedMaterial = retMaterial; //Apply material
            useOfBaseMaterials++;
            return retMaterial;
        }
    }

    //Generate a colour key for a room for use with materials (does not generate a material)
    public Toolbox.MaterialKey GenerateMaterialKey(MaterialGroupPreset.MaterialVariation variation, ColourSchemePreset scheme, NewRoom room, bool useGrubiness, NewBuilding building = null)
    {
        //Create a key
        Toolbox.MaterialKey newKey = new Toolbox.MaterialKey();
        newKey.baseMaterial = null;
        newKey.mainColour = GetColourFromScheme(scheme, variation.main, room, building);
        newKey.colour1 = GetColourFromScheme(scheme, variation.colour1, room, building);
        newKey.colour2 = GetColourFromScheme(scheme, variation.colour2, room, building);
        newKey.colour3 = GetColourFromScheme(scheme, variation.colour3, room, building);

        //Get building froom room
        if (building == null && room != null)
        {
            building = room.building;
        }

        if (useGrubiness)
        {
            if (building != null && building.preset != null && building.preset.overrideGrubiness)
            {
                newKey.grubiness = building.preset.grubinessOverride;
            }
            else if (room != null && !room.gameLocation.isOutside)
            {
                if (room.gameLocation.thisAsAddress != null)
                {
                    if (SessionData.Instance.isFloorEdit)
                    {
                        newKey.grubiness = 0;
                    }
                    //If this is a residence, base this on the inhabitant's average conscientiousness...
                    else if (room.gameLocation.thisAsAddress.residence != null && room.gameLocation.thisAsAddress.inhabitants.Count > 0)
                    {
                        if (Player.Instance.home == room.gameLocation)
                        {
                            newKey.grubiness = 0;
                        }
                        else
                        {
                            newKey.grubiness = Mathf.RoundToInt(Mathf.Min((1f - room.gameLocation.thisAsAddress.maxConscientiousness), 1f - room.gameLocation.thisAsAddress.normalizedLandValue) * 10f) / 10f; //Round grubiness to 1dp, so there is more of a chance of this material being re-used
                        }
                    }
                    //If lobby use building land value
                    else if (room.gameLocation.isLobby)
                    {
                        newKey.grubiness = Mathf.RoundToInt(1f - ((int)building.cityTile.landValue * 0.25f) * 10f) / 10f; //Use inverse land value for grubiness
                    }
                    //Otherwise base this on the address land value...
                    else
                    {
                        newKey.grubiness = Mathf.RoundToInt((1f - room.gameLocation.thisAsAddress.normalizedLandValue) * 10f) / 10f; //Use inverse land value
                    }
                }
                else newKey.grubiness = 0;
            }
            else if (room != null && room.gameLocation.isOutside && room.gameLocation.thisAsStreet != null)
            {
                newKey.grubiness = Mathf.RoundToInt((1f - room.gameLocation.thisAsStreet.normalizedFootfall) * 10f) / 10f; //Use inverse footfall as grub value
            }
            else if (building != null)
            {
                newKey.grubiness = ((int)building.cityTile.landValue * 2.5f) / 10f; //Use inverse land value for grubiness
            }
            else
            {
                newKey.grubiness = 0;
            }

            if (room != null && room.preset != null)
            {
                newKey.grubiness = Mathf.Clamp(newKey.grubiness, room.preset.minimumGrubiness, room.preset.maximumGrubiness);
            }

        }
        else newKey.grubiness = 0;

        return newKey;
    }

    //Apply a material directly to a model
    public void ApplyMaterial(GameObject model, Material mat)
    {
        if (model == null) return;
        MeshRenderer renderer = model.GetComponent<MeshRenderer>();
        ApplyMaterial(renderer, mat);
    }

    //Apply a material directly to a mesh
    public void ApplyMaterial(MeshRenderer renderer, Material mat)
    {
        if (renderer != null) renderer.sharedMaterial = mat;
    }

    //Retrieve a colour from a room's colour scheme
    public Color GetColourFromScheme(ColourSchemePreset scheme, MaterialGroupPreset.MaterialColour colourType, NewRoom room, NewBuilding building = null)
    {
        if (SessionData.Instance.isTestScene)
        {
            return Color.white;
        }

        //Sometimes we just want clear: We don't need a scheme for this
        if (colourType == MaterialGroupPreset.MaterialColour.none)
        {
            return Color.clear;
        }

        List<Color> possibleColours = new List<Color>();

        if (scheme == null && colourType != MaterialGroupPreset.MaterialColour.wood)
        {
            Game.LogError("Trying to get colour from null scheme at " + room.name + "( " + room.preset + ") returning white..." + room.colourScheme);
            return Color.white;
        }

        if (colourType == MaterialGroupPreset.MaterialColour.any)
        {
            possibleColours.Add(scheme.primary1);
            possibleColours.Add(scheme.primary2);
            possibleColours.Add(scheme.secondary1);
            possibleColours.Add(scheme.secondary2);
            possibleColours.Add(scheme.neutral);
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.anyPrimary)
        {
            possibleColours.Add(scheme.primary1);
            possibleColours.Add(scheme.primary2);
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.anyPrimaryOrSecondary)
        {
            possibleColours.Add(scheme.primary1);
            possibleColours.Add(scheme.primary2);
            possibleColours.Add(scheme.secondary1);
            possibleColours.Add(scheme.secondary2);
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.anyPrimaryOrNeutral)
        {
            possibleColours.Add(scheme.primary1);
            possibleColours.Add(scheme.primary2);
            possibleColours.Add(scheme.neutral);
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.anySecondary)
        {
            possibleColours.Add(scheme.secondary1);
            possibleColours.Add(scheme.secondary2);
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.anySecondaryOrNeutral)
        {
            possibleColours.Add(scheme.secondary1);
            possibleColours.Add(scheme.secondary2);
            possibleColours.Add(scheme.neutral);
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.any1)
        {
            possibleColours.Add(scheme.primary1);
            possibleColours.Add(scheme.secondary1);
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.any2)
        {
            possibleColours.Add(scheme.primary2);
            possibleColours.Add(scheme.secondary2);
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.any1OrNeutral)
        {
            possibleColours.Add(scheme.primary1);
            possibleColours.Add(scheme.secondary1);
            possibleColours.Add(scheme.neutral);
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.any2OrNeutral)
        {
            possibleColours.Add(scheme.primary2);
            possibleColours.Add(scheme.secondary2);
            possibleColours.Add(scheme.neutral);
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.neutral)
        {
            possibleColours.Add(scheme.neutral);
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.primary1)
        {
            return scheme.primary1;
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.primary2)
        {
            return scheme.primary2;
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.secondary1)
        {
            return scheme.secondary1;
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.secondary2)
        {
            return scheme.secondary2;
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.neutral)
        {
            return scheme.neutral;
        }
        else if (colourType == MaterialGroupPreset.MaterialColour.wood)
        {
            //Select from room/address
            if (room != null && room.gameLocation != null && room.gameLocation.thisAsAddress != null)
            {
                return room.gameLocation.thisAsAddress.wood;
            }
            //Select from building
            else
            {
                if (building != null)
                {
                    return building.wood;
                }
                else return Color.cyan;
            }
        }

        if (possibleColours.Count > 0)
        {
            if (room != null)
            {
                return possibleColours[Toolbox.Instance.GetPsuedoRandomNumber(0, possibleColours.Count, (room.roomID * possibleColours.Count).ToString())];
            }
            else
            {
                string randomSeed = string.Empty;
                if (building != null) randomSeed = building.buildingID * building.buildingID + building.cityTile.cityCoord.ToString() + CityData.Instance.seed + possibleColours.Count * possibleColours.Count;
                else randomSeed = CityData.Instance.seed + possibleColours.Count * possibleColours.Count;

                return possibleColours[Toolbox.Instance.RandContained(0, possibleColours.Count, randomSeed, out _)];
            }
        }
        else return Color.clear;
    }

    public Material GetFootprintMaterial(FootprintController fc)
    {
        if (fc == null) return null;

        FootprintMaterialKey newKey = new FootprintMaterialKey();

        if(fc.human != null)
        {
            newKey.type = (int)fc.human.descriptors.footwear;
            newKey.blood = fc.footprint.bl;
            newKey.strength = fc.footprint.str;

            //Search existing materials for this key
            if(footprintMaterialLibrary.ContainsKey(newKey))
            {
                footprintInstancesAvertedByDictionary++;
                return footprintMaterialLibrary[newKey];
            }
            //Create new material and add it to the library
            else
            {
                Material ret = null;

                if (fc.human.descriptors.footwear == Human.ShoeType.normal)
                {
                    ret = Instantiate(footprintMaterialShoe);
                }
                else if (fc.human.descriptors.footwear == Human.ShoeType.boots)
                {
                    ret = Instantiate(footprintMaterialBoot);
                }
                else if (fc.human.descriptors.footwear == Human.ShoeType.heel)
                {
                    ret = Instantiate(footprintMaterialHeel);
                }

                if (ret != null)
                {
                    //Set colour
                    Color col = Color.Lerp(dirtColour, bloodColour, fc.footprint.bl);

                    //The alpha to highest dirt or blood value
                    col.a = Mathf.Max(fc.footprint.str, fc.footprint.bl);

                    if (ret != null) ret.SetColor("_BaseColor", col);

                    footprintMaterialLibrary.Add(newKey, ret);
                    footprintMaterials++;

                    return ret;
                }
            }
        }

        return null;
    }

    [Button]
    public void PopulateDebugData()
    {
        commonMaterialsDebug.Clear();

        foreach(KeyValuePair<Toolbox.MaterialKey, Material> pair in commonMaterialsLibrary)
        {
            MaterialDebug newDebug = new MaterialDebug { name = pair.Value.name, key = pair.Key, mat = pair.Value };
            commonMaterialsDebug.Add(newDebug);
        }

        commonMaterialsDebug.Sort((p1, p2) => p1.name.CompareTo(p2.name));

        uniqueMaterialsDebug.Clear();

        foreach (KeyValuePair<Toolbox.MaterialKey, List<Material>> pair in uniqueMaterialsLibrary)
        {
            foreach(Material m in pair.Value)
            {
                if (m == null) continue;
                MaterialDebug newDebug = new MaterialDebug { name = m.name, key = pair.Key, mat = m };
                uniqueMaterialsDebug.Add(newDebug);
            }
        }

        uniqueMaterialsDebug.Sort((p1, p2) => p1.name.CompareTo(p2.name));
    }
}
