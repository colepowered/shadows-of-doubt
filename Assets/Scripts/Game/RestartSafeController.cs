﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class RestartSafeController : MonoBehaviour
{
    public bool loadFromDirty = false;

    [Header("New Game")]
    public bool generateNew = false;
    public bool newGameLoadCity = false;
    public FileInfo loadCityFileInfo;
    public string cityName;
    public int cityX = 5;
    public int cityY = 4;
    //public int population = 2;
    public string seed = "NewSeed";
    public bool sandbox = false;

    [Header("New Character")]
    public string newGamePlayerFirstName;
    public string newGamePlayerSurname;
    public Human.Gender newGamePlayerGender;
    public Human.Gender newGamePartnerGender;
    public Color newGamePlayerSkinTone;

    [Header("Load Save")]
    public bool loadSaveGame = false;
    public FileInfo saveStateFileInfo;

    [Header("Floor Edit New")]
    public bool newFloor = false;
    public string newFloorName;
    public Vector2 newFloorSize;
    public int newFloorFloorHeight;
    public int newFloorCeilingHeight;

    [Header("Floor Edit Load")]
    public bool loadFloor = false;
    public string loadFloorString;

    [Header("Floor Edit Recalculate All")]
    public bool recalculateAll = false;
    public List<string> floorList = new List<string>();

    //Singleton pattern
    private static RestartSafeController _instance;
    public static RestartSafeController Instance { get { return _instance; } }

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);

        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
}
