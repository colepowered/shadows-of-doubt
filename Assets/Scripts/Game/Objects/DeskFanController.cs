﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeskFanController : SwitchSyncBehaviour
{
    public InteractableController ic;
    public Transform fanBlade;
    public float speedProgress = 0f;
    public float fanSpeed = 5f;

    public override void SetOn(bool val)
    {
        base.SetOn(val);

        if(speedProgress > 0f || isOn)
        {
            this.enabled = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(isOn && speedProgress < 1f)
        {
            speedProgress += Time.deltaTime * 0.4f;
            speedProgress = Mathf.Clamp01(speedProgress);
        }
        else if(!isOn && speedProgress > 0f)
        {
            speedProgress -= Time.deltaTime * 0.15f;
            speedProgress = Mathf.Clamp01(speedProgress);
        }

        fanBlade.localEulerAngles = new Vector3(0f, 0f, fanBlade.localEulerAngles.z + (fanSpeed * speedProgress));

        //Turn off update
        if(!isOn && speedProgress <= 0f)
        {
            this.enabled = false;
        }
    }
}
