﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Threading;

public class InteriorCreator : Creator
{
    //Chunks to load per frame
    public int loadChunk = 1;

    public bool threadedInteriorCreationActive = false;

    [System.NonSerialized]
    public List<LoaderThread> threads = new List<LoaderThread>();

    public class LoaderThread
    {
        public Coroutine thread;

        //Threads take on either a street, or a floor of a building
        public StreetController street;
        public NewFloor floor;
        //public NewGameLocation location;
        public bool isDone = false;
    }

    //Singleton pattern
    private static InteriorCreator _instance;
    public static InteriorCreator Instance { get { return _instance; } }

    void Awake()
    {
        //Setup singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    //Create evidence files
    public override void StartLoading()
    {
        Game.Log("CityGen: Generating building interiors...");
        StartCoroutine("GenChunk");
    }

    IEnumerator GenChunk()
    {
        int cursor = 0;

        //Distribute sync disks
        //Get city size as a ratio...
        float citySizeRatio = ((CityData.Instance.citySize.x * CityData.Instance.citySize.y) - 36f) / 100f;

        //Create a random list of sync disks
        List<SyncDiskPreset> randomizedList = new List<SyncDiskPreset>();

        foreach(SyncDiskPreset s in Toolbox.Instance.allSyncDisks)
        {
            if (s.disabled) continue;
            randomizedList.Insert(Toolbox.Instance.SeedRand(0, randomizedList.Count), s);
        }

        foreach(SyncDiskPreset s in randomizedList)
        {
            float per200 = GameplayControls.Instance.commonSyncDisksPer200Citizens;

            if (s.rarity == SyncDiskPreset.Rarity.medium) per200 = GameplayControls.Instance.mediumSyncDisksPer200Citizens;
            else if (s.rarity == SyncDiskPreset.Rarity.rare) per200 = GameplayControls.Instance.rareSyncDisksPer200Citizens;
            else if (s.rarity == SyncDiskPreset.Rarity.veryRare) per200 = GameplayControls.Instance.veryRareSyncDisksPer200Citizens;

            int diskCount = Mathf.Max(Mathf.RoundToInt((CityData.Instance.citizenDirectory.Count / 200f) * per200), 1);

            //Get owner pool
            HashSet<Citizen> differentCitizenPool = new HashSet<Citizen>();
            List<Citizen> ownerPool = new List<Citizen>();

            foreach(Citizen h in CityData.Instance.citizenDirectory)
            {
                if (h.societalClass < s.minimumWealthLevel) continue;

                //Pass trait check...
                //Check picking rules
                int freqModMax = 0;
                bool passRules = true;

                foreach (SyncDiskPreset.TraitPick rule in s.traits)
                {
                    bool pass = false;

                    if (rule.rule == CharacterTrait.RuleType.ifAnyOfThese)
                    {
                        foreach (CharacterTrait searchTrait in rule.traitList)
                        {
                            if (h.characterTraits.Exists(item => item.trait == searchTrait))
                            {
                                pass = true;
                                break;
                            }
                        }
                    }
                    else if (rule.rule == CharacterTrait.RuleType.ifAllOfThese)
                    {
                        pass = true;

                        foreach (CharacterTrait searchTrait in rule.traitList)
                        {
                            if (!h.characterTraits.Exists(item => item.trait == searchTrait))
                            {
                                pass = false;
                                break;
                            }
                        }
                    }
                    else if (rule.rule == CharacterTrait.RuleType.ifNoneOfThese)
                    {
                        pass = true;

                        foreach (CharacterTrait searchTrait in rule.traitList)
                        {
                            if (h.characterTraits.Exists(item => item.trait == searchTrait))
                            {
                                pass = false;
                                break;
                            }
                        }
                    }
                    else if (rule.rule == CharacterTrait.RuleType.ifPartnerAnyOfThese)
                    {
                        if (h.partner != null)
                        {
                            foreach (CharacterTrait searchTrait in rule.traitList)
                            {
                                if (h.partner.characterTraits.Exists(item => item.trait == searchTrait))
                                {
                                    pass = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (!pass && rule.mustPassForApplication)
                    {
                        passRules = false;
                        freqModMax = 0;
                        break;
                    }
                    else if (pass)
                    {
                        freqModMax += rule.appliedFrequency;
                    }
                }

                if (passRules)
                {
                    freqModMax = Mathf.RoundToInt(freqModMax * s.traitWeight);

                    for (int i = 0; i < freqModMax; i++)
                    {
                        ownerPool.Add(h);

                        if(!differentCitizenPool.Contains(h))
                        {
                            differentCitizenPool.Add(h);
                        }
                    }

                    if (h.job != null && h.job.employer != null)
                    {
                        if (s.occupation.Contains(h.job.preset))
                        {
                            for (int i = 0; i < s.occupationWeight; i++)
                            {
                                ownerPool.Add(h);

                                if (!differentCitizenPool.Contains(h))
                                {
                                    differentCitizenPool.Add(h);
                                }
                            }
                        }
                    }
                }
            }

            //Backup plans for citizen pool...
            if(differentCitizenPool.Count <= 0)
            {
                //Add from wealth level
                foreach (Citizen c in CityData.Instance.citizenDirectory)
                {
                    if (c.societalClass < s.minimumWealthLevel) continue;

                    ownerPool.Add(c);
                }

                //If all else fails, add completely random
                if(differentCitizenPool.Count <= 0)
                {
                    ownerPool.Add(CityData.Instance.citizenDirectory[Toolbox.Instance.SeedRand(0, CityData.Instance.citizenDirectory.Count)]);
                }
            }

            for (int i = 0; i < diskCount; i++)
            {
                //Pick an owner
                int ownerIndex = Toolbox.Instance.SeedRand(0, ownerPool.Count);
                Citizen owner = ownerPool[ownerIndex];

                bool isWork = false;

                //Chance of work placement...
                if(owner.job != null && owner.job.employer != null)
                {
                    if(owner.job.preset.ownsWorkPosition)
                    {
                        if(Toolbox.Instance.GetPsuedoRandomNumber(0f, 1f, owner.seed) > 0.75f)
                        {
                            isWork = true;
                        }
                    }
                }

                if(isWork)
                {
                    owner.job.employer.address.AddToPlacementPool(s.interactable, owner, owner, null, security: s.interactable.securityLevel, ownership: InteractablePreset.OwnedPlacementRule.prioritiseOwned, priority: 10, passedObject: s);
                }
                else if(owner.home != null)
                {
                    owner.home.AddToPlacementPool(s.interactable, owner, owner, null, security: s.interactable.securityLevel, ownership: InteractablePreset.OwnedPlacementRule.prioritiseOwned, priority: 10, passedObject: s);
                }

                if(differentCitizenPool.Count > 1)
                {
                    ownerPool.RemoveAll(item => item == owner);
                    differentCitizenPool.Remove(owner);
                }
            }
        }

        Game.Log("CityGen: Starting interior generation, load state key: " + Toolbox.Instance.lastRandomNumberKey);

        threadedInteriorCreationActive = true;

        //Generate interiors
        while (cursor < CityData.Instance.floorDirectory.Count + CityData.Instance.streetDirectory.Count || threads.Count > 0)
        {
            //Multi-threaded loading
            if(cursor < CityData.Instance.floorDirectory.Count + CityData.Instance.streetDirectory.Count)
            {
                for (int i = threads.Count; i < Game.Instance.maxThreads; i++)
                {
                    //Break loop if complete
                    if (cursor >= CityData.Instance.floorDirectory.Count + CityData.Instance.streetDirectory.Count) break;

                    //Streets first
                    if (cursor < CityData.Instance.streetDirectory.Count)
                    {
                        LoaderThread newThread = new LoaderThread { street = CityData.Instance.streetDirectory[cursor] };
                        newThread.thread = StartCoroutine(ThreadedInteriorGeneration(newThread));
                    }
                    //Then floors
                    else
                    {
                        LoaderThread newThread = new LoaderThread { floor = CityData.Instance.floorDirectory[cursor - CityData.Instance.streetDirectory.Count] };
                        newThread.thread = StartCoroutine(ThreadedInteriorGeneration(newThread));
                    }

                    ////Addresses first
                    //if (cursor < CityData.Instance.addressDirectory.Count)
                    //{
                    //    LoaderThread newThread = new LoaderThread { location = CityData.Instance.addressDirectory[cursor] as NewGameLocation };
                    //    newThread.thread = StartCoroutine(ThreadedInteriorGeneration(newThread));
                    //}
                    //else
                    //{
                    //    LoaderThread newThread = new LoaderThread { location = CityData.Instance.streetDirectory[cursor - CityData.Instance.addressDirectory.Count] as NewGameLocation};
                    //    newThread.thread = StartCoroutine(ThreadedInteriorGeneration(newThread));
                    //}

                    cursor++;
                }
            }

            CityConstructor.Instance.loadingProgress = ((float)cursor / (float)(CityData.Instance.floorDirectory.Count + CityData.Instance.streetDirectory.Count));
            yield return null;
        }

        threadedInteriorCreationActive = false;

        Game.Log("CityGen: Load state key at end of interior generation: " + Toolbox.Instance.lastRandomNumberKey);

        //Apply blocked access after furniture is placed
        foreach (NewRoom r in CityData.Instance.roomDirectory)
        {
            //Create furniture interactables...
            foreach (FurnitureClusterLocation clust in r.furniture)
            {
                foreach (KeyValuePair<NewNode, List<FurnitureLocation>> pair in clust.clusterObjectMap)
                {
                    foreach (FurnitureLocation furniture in pair.Value)
                    {
                        if (furniture == null) continue;

                        //Create self employed from furniture (non thread safe)
                        if (furniture.furniture != null && furniture.furniture.createSelfEmployed != null && furniture.ownerMap.Count <= 0)
                        {
                            Game.Log("CityGen: Creating self employed from furniture " + furniture.furniture.name + "...");

                            Occupation unemp = CityData.Instance.unemployedDirectory.Find(item => item.employee != null && item.employee.home != null);

                            if (unemp == null)
                            {
                                Game.Log("CityGen: ... Could not find valid unemployed from pool of: " + CityData.Instance.unemployedDirectory.Count);
                            }
                            else
                            {
                                Interactable workPos = furniture.integratedInteractables.Find(item => item.pt == (int)furniture.furniture.workPositionID);

                                if (workPos == null)
                                {
                                    Game.Log("CityGen: ... Could not find work position: " + furniture.furniture.workPositionID + " interactables: " + furniture.integratedInteractables.Count);
                                }
                                else
                                {
                                    CityConstructor.Instance.CreateSelfEmployed(furniture.furniture.createSelfEmployed, unemp.employee, workPos);
                                }
                            }
                        }
                    }
                }
            }

            r.ApplyBlockedAccess();
        }

        //Generate entrance weights & job pathing data
        foreach(NewAddress add in CityData.Instance.addressDirectory)
        {
            if (!add.generatedEntranceWeights)
            {
                //Precompute entrance weights...
                foreach (NewNode.NodeAccess access in add.entrances)
                {
                    access.PreComputeEntranceWeights();
                }

                add.generatedEntranceWeights = true;

                //Generate job system data
                if (Game.Instance.useJobSystem)
                {
                    add.GenerateJobPathingData();
                }
            }
        }

        //Check to see if everyone has a desk?
        foreach (Citizen cit in CityData.Instance.citizenDirectory)
        {
            if(cit.job.employer != null)
            {
                if(cit.job.preset.jobAIPosition == OccupationPreset.JobAI.workPosition)
                {
                    if(cit.job.preset.ownsWorkPosition)
                    {
                        if(cit.workPosition == null)
                        {
                            int workPositions = 0;

                            List<string> belongsTo = new List<string>();

                            foreach (NewRoom room in cit.job.employer.address.rooms)
                            {
                                if (room.specialCaseInteractables.ContainsKey(cit.job.preset.jobPostion))
                                {
                                    workPositions += room.specialCaseInteractables[cit.job.preset.jobPostion].Count;

                                    //Find owners
                                    foreach(Interactable d in room.specialCaseInteractables[cit.job.preset.jobPostion])
                                    {
                                        Occupation j = cit.job.employer.companyRoster.Find(item => item.employee != null && item.employee.workPosition == d);

                                        if(j != null)
                                        {
                                            belongsTo.Add(d.preset.name + " " + j.employee.GetCitizenName() + " (" + j.preset.name + ")");
                                        }
                                        else
                                        {
                                            if(d.belongsTo != null && d.belongsTo.job != null && d.belongsTo.job.employer != null)
                                            {
                                                belongsTo.Add(d.preset.name + " Owned by " + d.belongsTo + " ("+d.belongsTo.job.preset.name + " at " + d.belongsTo.job.employer.name + ")");
                                            }
                                        }
                                    }
                                }
                            }

                            Game.Log("CityGen: " + cit.GetCitizenName() + "(" + cit.job.preset.name + ") doesn't have a desk at " + cit.job.employer.name + ", " + cit.job.employer.address.building.name + " (" + cit.job.employer.GetNumberOfFilledJobs() + " employees, " + workPositions + " valid work positions). Removing from position...");

                            //foreach(string str in belongsTo)
                            //{
                            //    Game.Log("CityGen: (Work position: " + str + ")");
                            //}

                            //Remove from position
                            cit.SetJob(CitizenCreator.Instance.CreateUnemployed());
                        }
                    }
                }
            }
        }

        //Place door keys
        foreach(KeyValuePair<int, NewDoor> door in CityData.Instance.doorDictionary)
        {
            door.Value.PlaceKeys();
        }

        //Complete!
        SetComplete();
    }

    //Threaded interior generation
    IEnumerator ThreadedInteriorGeneration(LoaderThread loaderReference)
    {
        //Game.Log("CityGen: Started new thread for room cursor " + roomCursor);
        loaderReference.isDone = false;

        float timeStamp = Time.realtimeSinceStartup;

        //Generate geometry on main thread
        if (loaderReference.floor != null)
        {
            for (int i = 0; i < loaderReference.floor.addresses.Count; i++)
            {
                GenerationController.Instance.GenerateGeometry(loaderReference.floor.addresses[i]);
            }
        }
        else if (loaderReference.street != null)
        {
            //Generate light zones for streets
            foreach (NewRoom r in loaderReference.street.rooms)
            {
                r.UpdateWorldPositionAndBoundsSize();
                GenerationController.Instance.GenerateLightZones(r);
            }
        }

        threads.Add(loaderReference);
        Game.Log("CityGen: Interior creator threads: " + threads.Count);

        Thread thread = new Thread(() =>
        {
            if (loaderReference.floor != null)
            {
                for (int i = 0; i < loaderReference.floor.addresses.Count; i++)
                {
                    GenerationController.Instance.GenerateAddressDecor(loaderReference.floor.addresses[i]);
                }
            }
            else if (loaderReference.street != null)
            {
                GenerationController.Instance.FurnishRoom(loaderReference.street.rooms[0]);
            }

            loaderReference.isDone = true;
        });

        thread.Start();

        //Do nothing on each frame until the thread is done
        while (!loaderReference.isDone || thread.IsAlive)
        {
            yield return null;
        }

        //Create interactables on main thread
        List<NewGameLocation> locations = new List<NewGameLocation>();

        if(loaderReference.street != null)
        {
            locations.Add(loaderReference.street);
        }
        else if(loaderReference.floor != null)
        {
            locations.AddRange(loaderReference.floor.addresses);
        }

        foreach(NewGameLocation loc in locations)
        {
            foreach(NewRoom r in loc.rooms)
            {
                //Create lights
                //Moved to post interior generation so it can run on the main thread...
                for (int i = 0; i < r.lightZones.Count; i++)
                {
                    NewRoom.LightZoneData lightZone = r.lightZones[i];

                    if (lightZone.nodeList == null || lightZone.nodeList.Count <= 0)
                    {
                        r.lightZones.Remove(lightZone);
                        i--;
                        continue;
                    }

                    //Is the middle far enough away from another light source?
                    bool distanceCheck = true;

                    foreach (Interactable light in r.mainLights)
                    {
                        if (Vector3.Distance(light.node.position, lightZone.centreWorldPosition) < InteriorControls.Instance.lightZoneMinDistance)
                        {
                            distanceCheck = false;
                            break;
                        }
                    }

                    if (!distanceCheck)
                    {
                        continue; //If distance check fails, move to next light zone
                    }

                    //You now have everything you need to spawn a light!
                    //Do this for main lights as they are interactables that will be saved. Area lights are created on spawn of geometry.
                    if(r.preset.useMainLights)
                    {
                        lightZone.CreateMainLight();
                    }
                }

                //Create furniture interactables...
                foreach (FurnitureClusterLocation clust in r.furniture)
                {
                    foreach (KeyValuePair<NewNode, List<FurnitureLocation>> pair in clust.clusterObjectMap)
                    {
                        foreach (FurnitureLocation furniture in pair.Value)
                        {
                            if (furniture == null) continue;

                            if (!furniture.createdInteractables)
                            {
                                furniture.CreateInteractables();
                            }
                        }
                    }
                }
            }

            loc.PlaceObjects();
        }

        Game.Log("CityGen: Interior creator threads: " + threads.Count);

        threads.Remove(loaderReference); //Remove thread reference
                                            //Game.Log("CityGen: Finished thread for room cursor " + roomCursor);
    }
}
