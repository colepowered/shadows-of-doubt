﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class EvidenceWitness : Evidence
{
    //Dialog options
    [System.Serializable]
    public class DialogOption
    {
        public DialogPreset preset;
        public SideJob jobRef; //Reference to a side job (ie if this is a hand-in dialog option).
        public NewRoom roomRef;
    }

    public Dictionary<Evidence.DataKey, List<DialogOption>> dialogOptions = new Dictionary<DataKey, List<DialogOption>>();

    //Constructor
    public EvidenceWitness(EvidencePreset newPreset, string evID, Controller newController, List<object> newPassedObjects) : base(newPreset, evID, newController, newPassedObjects)
    {
        //Setup default dialog
        foreach (DialogPreset dia in Toolbox.Instance.defaultDialogOptions)
        {
            if(!dia.telephoneCallOption && !dia.hospitalDecisionOption)
            {
                AddDialogOption(dia.tiedToKey, dia);
            }
        }
    }

    //Dialog
    public DialogOption AddDialogOption(Evidence.DataKey key, DialogPreset newPreset, SideJob newSideJob = null, NewRoom roomRef = null, bool allowPresetDuplicates = true)
    {
        //Don't allow multiple types of a specific preset...
        if(!allowPresetDuplicates)
        {
            if(dialogOptions.ContainsKey(key))
            {
                foreach (DialogOption opt in dialogOptions[key])
                {
                    if (opt.preset == newPreset)
                    {
                        Game.Log("Found existing dialog " + newPreset + " for key " + key + " in " + evID);
                        return opt;
                    }
                }
            }
        }

        DialogOption newOpt = new DialogOption();
        newOpt.preset = newPreset;
        newOpt.jobRef = newSideJob;
        newOpt.roomRef = roomRef;

        if (!dialogOptions.ContainsKey(key))
        {
            dialogOptions.Add(key, new List<DialogOption>());
        }

        dialogOptions[key].Add(newOpt);

        return newOpt;
    }

    public void RemoveDialogOption(Evidence.DataKey key, DialogPreset newPreset, SideJob newSideJob = null, NewRoom roomRef = null)
    {
        if (dialogOptions.ContainsKey(key))
        {
            Game.Log("Remove dialog " + newPreset.name);
            int index = dialogOptions[key].FindIndex(item => item.preset == newPreset && item.jobRef == newSideJob && item.roomRef == roomRef);

            if(index > -1)
            {
                dialogOptions[key].RemoveAt(index);
            }
        }
    }

    public List<EvidenceWitness.DialogOption> GetDialogOptions(Evidence.DataKey key)
    {
        List<Evidence.DataKey> keys = new List<DataKey>();
        keys.Add(key);

        return GetDialogOptions(keys);
    }

    public List<EvidenceWitness.DialogOption> GetDialogOptions(List<Evidence.DataKey> keys)
    {
        //Get tied keys
        List<Evidence.DataKey> tiedKeys = GetTiedKeys(keys);

        List<EvidenceWitness.DialogOption> returnOptions = new List<EvidenceWitness.DialogOption>();

        foreach (Evidence.DataKey key in tiedKeys)
        {
            Game.Log("Debug: Getting dialog options for key: " + key.ToString());

            if (dialogOptions.ContainsKey(key))
            {
                foreach (EvidenceWitness.DialogOption dia in dialogOptions[key])
                {
                    //If there is a job reference, this job needs to be active...
                    if(dia.preset.ignoreActiveJobRequirement || dia.jobRef == null || (CasePanelController.Instance.activeCase != null && dia.jobRef == CasePanelController.Instance.activeCase.job))
                    {
                        if (!returnOptions.Contains(dia))
                        {
                            Game.Log("Debug: Returning option: " + dia.preset.name);
                            returnOptions.Add(dia);
                        }
                    }
                    else
                    {
                        Game.Log("Skipped dialog option with job reference " + dia.jobRef);
                    }
                }
            }
        }

        return returnOptions;
    }

    public override List<DataKey> GetTiedKeys(List<DataKey> inputKeys)
    {
        inputKeys = base.GetTiedKeys(inputKeys);

        if(inputKeys.Contains(DataKey.photo))
        {
            if (UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.agePerception) > 0f)
            {
                if(!inputKeys.Contains(DataKey.age))
                {
                    inputKeys.Add(DataKey.age);
                }
            }

            if (UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.footSizePerception) > 0f)
            {
                if (!inputKeys.Contains(DataKey.shoeSize))
                {
                    inputKeys.Add(DataKey.shoeSize);
                }
            }

            if (UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.heightPerception) > 0f)
            {
                if (!inputKeys.Contains(DataKey.height))
                {
                    inputKeys.Add(DataKey.height);
                }
            }

            if (UpgradeEffectController.Instance.GetUpgradeEffect(SyncDiskPreset.Effect.salaryPerception) > 0f)
            {
                if (!inputKeys.Contains(DataKey.salary))
                {
                    inputKeys.Add(DataKey.salary);
                }
            }
        }

        return inputKeys;
    }
}
