using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NaughtyAttributes;
using UnityEngine.Video;

public class InterfaceVideoController : MonoBehaviour
{
    [Header("Components")]
    public VideoPlayer player;
    public RawImage image;

    [Header("Settings")]
    public RenderTexture renderTexturePrefab;
    private RenderTexture renderTextureInstance;

    public void Setup(VideoClip clip, Texture2D img)
    {
        if(clip == null || clip != player.clip)
        {
            if (renderTextureInstance != null)
            {
                player.targetTexture.Release(); //Release rt
            }

            Destroy(renderTextureInstance);

            if (clip != null)
            {
                renderTextureInstance = Instantiate(renderTexturePrefab);
                player.targetTexture = renderTextureInstance;
                image.texture = renderTextureInstance;
                player.clip = clip;
            }
            else if (img != null)
            {
                image.texture = img;
            }
        }
    }

    private void OnDestroy()
    {
        if (renderTextureInstance != null) renderTextureInstance.Release(); //Release rt
        Destroy(renderTextureInstance);
    }
}
